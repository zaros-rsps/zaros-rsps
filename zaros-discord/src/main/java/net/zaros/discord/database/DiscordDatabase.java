package net.zaros.discord.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import lombok.Getter;

/**
 * Represents our Discord dot database.
 * 
 * @author Walied K. Yassen
 */
public final class DiscordDatabase {

	/**
	 * The database data source.
	 */
	@Getter
	private final HikariDataSource source;

	/**
	 * Constructs a new {@link DiscordDatabase} type object instance.
	 * 
	 * @param name
	 *             the bot name which this database is for.
	 * @param path
	 *             the database path.
	 */
	public DiscordDatabase(String name, String path) {
		// create the data source configurations.
		var config = new HikariConfig();
		config.setPoolName(name);
		config.setDriverClassName("org.sqlite.JDBC");
		config.setJdbcUrl("jdbc:sqlite:" + path);
		config.setConnectionTestQuery("SELECT 1");
		config.setInitializationFailFast(false);
		config.setMaxLifetime(60000);
		config.setIdleTimeout(30000);
		config.setMaximumPoolSize(25);
		config.setAutoCommit(true);
		config.addDataSourceProperty("prepStmtCacheSize", 250);
		config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
		config.addDataSourceProperty("cachePrepStmts", true);
		config.addDataSourceProperty("useServerPrepStmts", true);
		config.addDataSourceProperty("useLocalSessionState", true);
		config.addDataSourceProperty("rewriteBatchedStatements", true);
		config.addDataSourceProperty("cacheResultSetMetadata", true);
		config.addDataSourceProperty("cacheServerConfiguration", true);
		config.addDataSourceProperty("maintainTimeStats", true);
		// create the data source object.
		source = new HikariDataSource(config);
	}

	/**
	 * @param query
	 * @return
	 * @throws SQLException
	 */
	public ResultSet executeQuery(String query) throws SQLException {
		var statement = createStatement();
		return statement.executeQuery(query);
	}

	/**
	 * @param query
	 * @return
	 * @throws SQLException
	 */
	public int executeUpdate(String query) throws SQLException {
		var statement = createStatement();
		return statement.executeUpdate(query);
	}

	/**
	 * @param sql
	 * @param columns
	 * @return
	 * @throws SQLException
	 */
	public PreparedStatement prepareStatement(String sql, int... columns) throws SQLException {
		var connection = getConnection();
		return connection.prepareStatement(sql, columns);
	}

	/**
	 * @param sql
	 * @param columns
	 * @return
	 * @throws SQLException
	 */
	public PreparedStatement prepareStatement(String sql, String... columns) throws SQLException {
		var connection = getConnection();
		return connection.prepareStatement(sql, columns);
	}

	/**
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		var connection = getConnection();
		return connection.prepareStatement(sql);
	}

	/**
	 * Creates an empty {@link Statement} object.
	 * 
	 * @return the created {@link Statement} object.
	 * @throws SQLException
	 *                      if anything occurs during the statement object creation.
	 */
	public Statement createStatement() throws SQLException {
		var connection = getConnection();
		return connection.createStatement();
	}

	/**
	 * Gets a free and ready to use {@link Connection} object from the database
	 * connections pool.
	 * 
	 * @return the {@link Connection} object.
	 * @throws SQLException
	 *                      if anything occurs during retrieving or creating the
	 *                      connection object.
	 */
	public Connection getConnection() throws SQLException {
		return source.getConnection();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		try {
			try {
				if (!source.isClosed()) {
					source.close();
				}
			} catch (Throwable e) {
				// NOOP
			}
		} finally {
			super.finalize();
		}
	}
}
