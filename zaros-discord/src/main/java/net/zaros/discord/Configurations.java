package net.zaros.discord;

import lombok.Builder;
import lombok.Value;

/**
 * Represents the Discord bot configurations.
 * 
 * @author Walied K. Yassen
 */
@Builder
@Value
public final class Configurations {

	/**
	 * The Discord bot name.
	 */
	private final String name;

	/**
	 * The Discord API Token.
	 */
	private final String apiToken;

	/**
	 * The Discord database path.
	 */
	private final String database;
}
