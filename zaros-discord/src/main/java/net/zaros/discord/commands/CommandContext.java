package net.zaros.discord.commands;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zaros.discord.bot.ZarosBot;
import net.zaros.discord.bot.profile.Profile;

/**
 * Represents a command execution context.
 * 
 * @author Walied K. Yassen
 */
@AllArgsConstructor
public final class CommandContext {

	/**
	 * The embed line separator.
	 */
	public static final String LINE_SEPARATOR = "\n--------------------------------------------------------------------------------------";

	/**
	 * The message timestamp format.
	 */
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("E, dd MMM YYY 'at' KK:mm:ss a");

	/**
	 * The success embed {@link Color colour} objet.
	 */
	private static final Color SUCCESS_COLOR = new Color(42, 222, 66);

	/**
	 * The error embed {@link Color colour} object.
	 */
	private static final Color ERROR_COLOR = new Color(222, 42, 66);

	/**
	 * The {@link ZarosBot} instance.
	 */
	@Getter
	private final ZarosBot bot;

	/**
	 * The message event of the command.
	 */
	@Getter
	private final MessageCreateEvent event;

	/**
	 * The executor user profile.
	 */
	@Getter
	private final Profile profile;

	/**
	 * The command execution alias.
	 */
	@Getter
	private final String alias;

	/**
	 * The command execution arguments,
	 */
	@Getter
	private final String[] arguments;

	/**
	 * @param iindex
	 * @param class1
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T requireArgument(int index, Class<T> type) {
		if (index < 0 || index >= arguments.length) {
			throw new CommandException("Invalid command arguments provided");
		}
		// grab the raw argument.
		var raw = arguments[index].trim();
		if (raw.length() < 1) {
			throw new CommandException("Invalid command arguments provided");
		}
		if (type == String.class) {
			return (T) raw;
		}
		return null;
	}

	/**
	 * Sends a basic mention message with the specified {@code content}.
	 * 
	 * @param content
	 *                the message content to append after the mention tag.
	 */
	public void basicMention(String content) {
		event.getChannel().sendMessage(profile.getUser().getMentionTag() + " " + content).thenAcceptAsync(Message::delete, CompletableFuture.delayedExecutor(15, TimeUnit.SECONDS));
	}

	/**
	 * Sends a successful embed message to the channel where the command was
	 * executed in.
	 * 
	 * @param title
	 *                the message title.
	 * @param content
	 *                the message content.
	 */
	public void success(String title, String content) {
		var builder = new EmbedBuilder();
		builder.setColor(SUCCESS_COLOR);
		builder.setAuthor(title, null, "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/72/twitter/154/white-heavy-check-mark_2705.png");
		builder.setDescription(content + LINE_SEPARATOR);
		builder.addInlineField("Command", alias);
		builder.addInlineField("Executor", profile.getUser().getMentionTag());
		powerEmbed(builder);
		event.getChannel().sendMessage(builder).thenAcceptAsync(Message::delete, CompletableFuture.delayedExecutor(15, TimeUnit.SECONDS));
	}

	/**
	 * Sends an error embed message to the channel where the command was executed
	 * in.
	 * 
	 * @param title
	 *                the message title.
	 * @param content
	 *                the message content.
	 */
	public void error(String title, String content) {
		var builder = new EmbedBuilder();
		builder.setColor(ERROR_COLOR);
		builder.setAuthor(title, null, "https://emojipedia-us.s3.amazonaws.com/thumbs/240/twitter/131/cross-mark_274c.png");
		builder.setDescription(content + LINE_SEPARATOR);
		builder.addInlineField("Command", alias);
		builder.addInlineField("Executor", profile.getUser().getMentionTag());
		powerEmbed(builder);
		event.getChannel().sendMessage(builder).thenAcceptAsync(Message::delete, CompletableFuture.delayedExecutor(15, TimeUnit.SECONDS));

	}

	protected void powerEmbed(EmbedBuilder builder) {
		builder.setFooter("Executed in " + DATE_FORMAT.format(System.currentTimeMillis()) + " | Powered by Illuminati.");
	}
}
