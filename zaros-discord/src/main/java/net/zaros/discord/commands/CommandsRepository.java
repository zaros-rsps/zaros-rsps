package net.zaros.discord.commands;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

/**
 * Represents the commands repository.
 * 
 * @author Walied K. Yassen
 */
@Slf4j
public final class CommandsRepository {

	/**
	 * The commands map.
	 */
	private final Map<String, Command> commands = new HashMap<String, Command>();

	/**
	 * Registers the specified {@link Command} in our repository.
	 * 
	 * @param command
	 *                the command to register.
	 */
	public void register(Command command) {
		command.getAliases().forEach(alias -> register(alias, command));
	}

	/**
	 * Registers the specified {@link Command} in our repository.
	 * 
	 * @param alias
	 *                the command alias to register by.
	 * @param command
	 *                the command to register.
	 */
	public void register(String alias, Command command) {
		// trim and format the alias.
		alias = alias.trim().toLowerCase();
		// check if the alias is already taken.
		if (commands.containsKey(alias)) {
			log.warn("Tried to register a command that is already registered, command alias: {}.", alias);
			return;
		}
		// register the command.
		commands.put(alias, command);
	}

	/**
	 * Unregisters the specified {@link Command} from our repository.
	 * 
	 * @param command
	 *                the command to unregister.
	 */
	public void unregister(Command command) {
		command.getAliases().forEach(this::unregister);
	}

	/**
	 * Unregisters the command with the specified {@code alias} from our repository.
	 * 
	 * @param alias
	 *              the command alias to unregister.
	 */
	public void unregister(String alias) {
		// trim and format the alias.
		alias = alias.trim().toLowerCase();
		// search for the command in the commands map.
		if (!commands.containsKey(alias)) {
			log.warn("Tried to unregister a command that is not registered, command alias: {}.", alias);
			return;
		}
		commands.remove(alias);
	}

	/**
	 * Gets the {@link command} with the specified {@code alias}.
	 * 
	 * @param alias
	 * @return the {@link Command} object if it was present otherwise {@code null}.
	 */
	public Command get(String alias) {
		return commands.get(alias);
	}
}
