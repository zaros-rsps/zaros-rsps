package net.zaros.discord.commands;

/**
 * Represents an exception that has occurred during a command execution.
 * 
 * @author Walied K. Yassen
 */
public final class CommandException extends RuntimeException {

	/**
	 * The serialisation key of the {@link CommandException} type.
	 */
	private static final long serialVersionUID = -1002176940717272405L;

	/**
	 * Constructs a new {@link CommandException} type object instance.
	 * 
	 * @param message
	 *                the command error message.
	 */
	public CommandException(String message) {
		super(message);
	}
}
