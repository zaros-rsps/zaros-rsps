package net.zaros.discord.commands;

import java.util.Set;

import org.javacord.api.entity.permission.Role;

import com.google.common.collect.ImmutableSet;

/**
 * Created at: Mar 6, 2018 12:13:26 AM
 * 
 * @author Walied K. Yassen
 * @since 0.1
 */
public abstract class Command {

	/**
	 * The required roles.
	 */
	protected final Role requiredRole;

	/**
	 * The aliases list.
	 */
	private final Set<String> aliases;

	/**
	 * Constructs a new {@link Command} object instance.
	 * 
	 * @param role
	 *                the command required role.
	 * @param aliases
	 *                the command aliases.
	 */
	public Command(Role requiredRole, String[] aliases) {
		this.requiredRole = requiredRole;
		this.aliases = ImmutableSet.copyOf(aliases);
	}

	/**
	 * Attempts to execute this command using the specified {@code alias} and the
	 * specified input {@code arguments}.
	 * 
	 * @param context
	 *                the command execution context.
	 */
	public void tryExecute(CommandContext context) {
		if (requiredRole != null && !context.getProfile().getUser().getRoles(context.getEvent().getServer().get()).contains(requiredRole)) {
			context.error("Permissions", "You do not have enough permissions to execute this command.");
			return;
		}
		try {
			execute(context);
		} catch (CommandException e) {
			context.error("Malformed Command", e.getMessage());
		}
	}

	/**
	 * Performs a command execution using the specified {@code alias} and
	 * {@code arguments}.
	 * 
	 * @param context
	 *                the command execution context.
	 * @throws CommandException
	 *                          if anything occurs during the command execution.
	 */
	protected abstract void execute(CommandContext context) throws CommandException;

	/**
	 * Gets the command aliases list.
	 * 
	 * @return the command aliases list.
	 */
	public Set<String> getAliases() {
		return aliases;
	}
}
