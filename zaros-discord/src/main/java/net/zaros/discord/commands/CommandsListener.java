package net.zaros.discord.commands;

import java.util.Arrays;

import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;

import net.zaros.discord.bot.ZarosBot;

/**
 * Created at: Mar 6, 2018 12:07:55 AM
 * 
 * @author Walied K. Yassen
 * @since 0.1
 */
public class CommandsListener implements MessageCreateListener {

	/**
	 * The owner bot instance.
	 */
	private final ZarosBot bot;

	/**
	 * The commands prefix.
	 */
	private final String prefix;

	/**
	 * The commands repository.
	 */
	private final CommandsRepository repository;

	/**
	 * Constructs a new {@link CommandsListener} object instance.
	 * 
	 * @param bot
	 *                   the owner bot instance.
	 * @param repository
	 *                   the commands repository.
	 * @param prefix
	 *                   the commands prefix.
	 */
	public CommandsListener(ZarosBot bot, CommandsRepository repository, String prefix) {
		this.prefix = prefix;
		this.repository = repository;
		this.bot = bot;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.javacord.listener.message.MessageCreateListener#onMessageCreate(org.
	 * javacord.event.message. MessageCreateEvent)
	 */
	@Override
	public void onMessageCreate(MessageCreateEvent event) {
		var content = event.getMessage().getContent();
		// checks whether it is a command or not.
		if (!content.startsWith(prefix)) {
			return;
		}
		// grab the author user.
		var user = event.getMessage().getUserAuthor().get();
		// grab the user profile.
		var profile = bot.getProfiles().lookupProfile(user, true);
		// strip the command prefix.
		content = content.substring(prefix.length());
		// split the command arguments.
		var arguments = content.split(" ");
		// parse the command alias.
		var alias = arguments[0];
		// grab the associated command.
		var command = repository.get(alias);
		// creat the context.
		var context = new CommandContext(bot, event, profile, alias, Arrays.copyOfRange(arguments, 1, arguments.length));
		// check whether the command exist or not.
		if (command == null) {
			// notify the user.
			context.error("Commands", "Unfotunately, the command '" + alias + "' was not recognised by " + bot.getConfiguration().getName());
		} else {
			// execute the command.
			command.tryExecute(context);
		}
		// delete the command message.
		event.deleteMessage();
	}

}
