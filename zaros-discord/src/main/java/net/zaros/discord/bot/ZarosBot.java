package net.zaros.discord.bot;

import org.javacord.api.AccountType;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.activity.ActivityType;
import org.javacord.api.entity.user.UserStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import net.zaros.discord.Configurations;
import net.zaros.discord.bot.linking.LinkingManager;
import net.zaros.discord.bot.profile.ProfileManager;
import net.zaros.discord.commands.CommandsListener;
import net.zaros.discord.commands.CommandsRepository;
import net.zaros.discord.database.DiscordDatabase;

/**
 * Represents the Zaros Discord bot main class.
 * 
 * @author Walied K. Yassen
 */
public final class ZarosBot {

	/**
	 * The {@link ZarosBot} class type {@link Logger} instance.
	 */
	public static final Logger logger = LoggerFactory.getLogger(ZarosBot.class);

	/**
	 * The {@link DiscordApi} object.
	 */
	@Getter
	private final DiscordApi api;

	/**
	 * The bot configurations list.
	 */
	@Getter
	private final Configurations configuration;

	/**
	 * The database of our bot.
	 */
	@Getter
	private final DiscordDatabase database;

	/**
	 * The user profiles manager.
	 */
	@Getter
	private final ProfileManager profiles;

	/**
	 * The user linking manager.
	 */
	@Getter
	private final LinkingManager linking;

	/**
	 * The commands repository.
	 */
	@Getter
	private final CommandsRepository commands;

	/**
	 * Constructs a new {@link ZarosBot} object instance.
	 * 
	 * @param configuration
	 *                      the Discord API token.
	 */
	public ZarosBot(Configurations configuration) {
		this.configuration = configuration;
		// create the API builder.
		var builder = new DiscordApiBuilder() {
			{
				setToken(configuration.getApiToken());
				setAccountType(AccountType.BOT);
				setRecommendedTotalShards().join();
				setWaitForServersOnStartup(true);
			}
		};
		// create the API object.
		api = builder.login().join();
		// create the bot database.
		database = new DiscordDatabase(configuration.getName(), configuration.getDatabase());
		// create the profile manager.
		profiles = new ProfileManager(this);
		// create the linking manager/
		linking = new LinkingManager(this);
		// initialise the commands repository.
		commands = new CommandsRepository();
	}

	/**
	 * Initialises the bot system.
	 */
	public void initialise() {
		// initialise the profiles manager.
		profiles.initialise();
		// initialise the bot activity and status.
		api.updateActivity(ActivityType.PLAYING, "with Nomac");
		api.updateStatus(UserStatus.ONLINE);
	}

	/**
	 * Starts the bot system.
	 */
	public void start() {
		api.addMessageCreateListener(new CommandsListener(this, commands, "::"));
	}
}
