package net.zaros.discord.bot.linking;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import lombok.Getter;
import net.zaros.discord.bot.ZarosBot;

/**
 * Represents the linking manager.
 * 
 * @author Walied K. Yassen
 */
public final class LinkingManager {

	/**
	 * The pending linking requests.
	 */
	// @formatter:off
	private final Cache<String, String> pending = CacheBuilder.newBuilder()
			.expireAfterWrite(5, TimeUnit.MINUTES)
			.initialCapacity(1)
			.maximumSize(500)
			.build();
	//@formatter:on

	/**
	 * The bot instance.
	 */
	@Getter
	private final ZarosBot bot;

	/**
	 * Constructs a new {@link LinkingManager} type object instance.
	 * 
	 * @param bot
	 *            the bot instance.
	 */
	public LinkingManager(ZarosBot bot) {
		this.bot = bot;
	}

	/**
	 * Tests the specified authentication {@code code}. If the test was successful,
	 * the code will be removed from the cache and the associated username will be
	 * returned, otherwise a {@code null} will be turned.
	 * 
	 * @param code
	 *             the authentication code to test.
	 * @return the authenticated username.
	 */
	public String test(String code) {
		var username = pending.getIfPresent(code);
		if (username != null) {
			pending.invalidate(code);
		}
		return username;
	}

	/**
	 * Requests a new linking authentication code for the specified
	 * {@code username}.
	 * 
	 * @param username
	 *                 the player username.
	 * @return the authentication code.
	 */
	public String request(String username) {
		var code = RandomStringUtils.random(6, true, false);
		pending.put(code, username);
		return code;
	}

	/**
	 * Retrieves the authentication code for the specified {@code username}.
	 * 
	 * @param username
	 *                 the username to retrieve for.
	 * @return the retrieved authentication code if it was present otherwise
	 *         {@code null}.
	 */
	public String retrieve(String username) {
		var map = pending.asMap();
		for (var code : map.keySet()) {
			if (map.get(code).equals(username)) {
				return code;
			}
		}
		return null;
	}

}
