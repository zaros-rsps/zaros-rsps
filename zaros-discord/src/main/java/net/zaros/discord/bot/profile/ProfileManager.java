package net.zaros.discord.bot.profile;

import java.sql.SQLException;
import java.time.Duration;

import org.javacord.api.entity.user.User;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import lombok.extern.slf4j.Slf4j;
import net.zaros.discord.bot.ZarosBot;

/**
 * Represents our user profile manager.
 * 
 * @author Walied K. Yassen
 */
@Slf4j
public final class ProfileManager {

	/**
	 * The table creation SQL command.
	 */
	private static final String TABLE_SQL;

	/**
	 * The user profiles cache.
	 */
	// @formatter:off
	private final Cache<Long, Profile> profiles = CacheBuilder
			.newBuilder()
			.initialCapacity(10)
			.maximumSize(100)
			.expireAfterAccess(Duration.ofMinutes(3))
			.expireAfterWrite(Duration.ofMinutes(3))
			.softValues()
			.build();
	// @formatter:on

	/**
	 * The {@link ZarosBot} object instance.
	 */
	private final ZarosBot bot;

	/**
	 * Constructs a new {@link ProfileManager} type object instance.
	 * 
	 * @param bot
	 *            the {@link ZarosBot} object which owns this object.
	 */
	public ProfileManager(ZarosBot bot) {
		this.bot = bot;
	}

	static {
		// create the table creation query.
		var builder = new StringBuilder();
		builder.append("CREATE TABLE IF NOT EXISTS `profiles` (");
		builder.append("`id` INTEGER NOT NULL PRIMARY KEY");
		builder.append(",`username` VARCHAR(16)");
		builder.append(",`points` INTEGER");
		builder.append(");");
		// store the query into a constant.
		TABLE_SQL = builder.toString();
	}

	/**
	 * Initialises the profiles manager.
	 */
	public void initialise() {
		try {
			bot.getDatabase().executeUpdate(TABLE_SQL);
		} catch (SQLException e) {
			log.error("Error while creating the profiles table", e);
		}
	}

	/**
	 * @param user
	 * @return
	 */
	public Profile lookupProfile(User user) {
		return lookupProfile(user, false);
	}

	/**
	 * @param user
	 * @param create
	 * @return
	 */
	public Profile lookupProfile(User user, boolean create) {
		Profile profile = profiles.getIfPresent(user.getId());
		if (profile != null) {
			return profile;
		}
		profile = lookupProfile0(user);
		if (profile != null) {
			return profile;
		}
		if (create) {
			profile = createProfile(user);
		}
		return profile;
	}

	/**
	 * @param user
	 * @return
	 */
	private Profile lookupProfile0(User user) {
		try (var statement = bot.getDatabase().prepareStatement("SELECT * FROM profiles WHERE id = ?")) {
			statement.setLong(1, user.getId());
			var result = statement.executeQuery();
			if (!result.next()) {
				return null;
			}
			var profile = new Profile(user);
			profile.fetch(result);
			return profile;
		} catch (SQLException e) {
			log.error("Error while looking up profile: {}", user.getId(), e);
			return null;
		}
	}

	/**
	 * @param profile
	 * @return
	 * @throws SQLException
	 */
	public boolean storeProfile(Profile profile) {
		var id = profile.getUser().getId();
		try (var statement = bot.getDatabase().prepareStatement("UPDATE profiles SET username = ?, points = ? WHERE id = ?")) {
			statement.setString(1, profile.getUsername());
			statement.setInt(2, profile.getPoints());
			statement.setLong(3, id);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			log.error("Error while creating profile: {}", id, e);
			return false;
		}
	}

	/**
	 * @param id
	 * @return
	 */
	public Profile createProfile(User user) {
		var profile = new Profile(user);
		if (!createProfile0(user.getId())) {
			return null;
		}
		profiles.put(user.getId(), profile);
		return profile;
	}

	/**
	 * @param id
	 * @return
	 */
	private boolean createProfile0(long id) {
		try (var statement = bot.getDatabase().prepareStatement("INSERT INTO profiles (id, username, points) VALUES (?, ?, ?)")) {
			statement.setLong(1, id);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			log.error("Error while creating profile: {}", id, e);
			return false;
		}
	}

}
