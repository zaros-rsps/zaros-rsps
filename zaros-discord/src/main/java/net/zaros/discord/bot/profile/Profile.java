package net.zaros.discord.bot.profile;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.javacord.api.entity.user.User;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents a user profile for one of our players.
 * 
 * @author Walied K. Yassen
 */
public final class Profile {

	/**
	 * The Discord API {@link User} object of the user.
	 */
	@Getter
	private final User user;

	/**
	 * The {@link Player} object of the user.
	 */
	@Getter
	@Setter
	private String username;

	/**
	 * The current Discord points.
	 */
	@Getter
	@Setter
	private int points;

	/**
	 * Constructs a new {@link Profile} type object instance.
	 * 
	 * @param user
	 *             the Discord API {@link User} object of this profile.
	 */
	public Profile(User user) {
		this.user = user;
	}

	/**
	 * Fetches the profile data from the specified query {@link ResultSet}.
	 * 
	 * @param result
	 *               the query result set to fetch from.
	 * @throws SQLException
	 *                      if anything occurs during the fetching process.
	 */
	public void fetch(ResultSet result) throws SQLException {
		username = result.getString("username");
		points = result.getInt("points");
	}

	/**
	 * Checks whether or not this user has linked their account to their in-game
	 * profile.
	 * 
	 * @return <code>true</code> if they did otherwise <code>false</code>.
	 */
	public boolean isLinked() {
		return username != null;
	}
}
