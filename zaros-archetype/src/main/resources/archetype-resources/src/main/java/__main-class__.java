package $package;

public class ${main-class} extends Plugin {

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.plugin.java.Plugin#onEnable()
	 */
	@Override
	public void onEnable() {
		logger.info("Plugin Enabled!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.plugin.java.Plugin#onDisable()
	 */
	@Override
	public void onDisable() {
		logger.info("Plugin Disabled!");
	}

	@EventListener
	public void onPlayerLogin(PlayerLoginEventPost event) {
		Player player = event.getPlayer();
		logger.info("Player has logged in {}", player.getDetails().getDisplayName());
	}
}
