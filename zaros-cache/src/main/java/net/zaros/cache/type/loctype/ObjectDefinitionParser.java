package net.zaros.cache.type.loctype;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import net.zaros.cache.type.ConfigLoader;
import net.zaros.cache.util.Utils;
import net.zaros.cache.util.buffer.FixedBuffer;

public final class ObjectDefinitionParser {

	/**
	 * The map of all object definitions, with the key being the id of the object
	 */
	private static final ConcurrentHashMap<Integer, ObjectDefinition> OBJECT_DEFINITIONS = new ConcurrentHashMap<>();

	/**
	 * Gets an object definition by the id of the object
	 *
	 * @param objectId
	 *                     The id of the object
	 * @return An {@code ObjectDefinition} {@code Object}
	 */
	public static ObjectDefinition forId(int objectId) {
		ObjectDefinition objectDef = OBJECT_DEFINITIONS.get(objectId);
		if (objectDef != null) {
			return objectDef;
		}
		byte[] data = null;
		try {
			data = ConfigLoader.store.getIndexes()[16].getFile(objectId >>> 8, objectId & 0xff);
		} catch (Exception e) {
			// System.out.println("Could not grab object " + objectId);
			// e.printStackTrace();
			return null;
		}
		objectDef = new ObjectDefinition();
		objectDef.setId(objectId);
		if (data != null) {
			try {
				boolean newObject = data[0] == 'N' && data[1] == 'E' && data[2] == 'W';
				objectDef.readValueLoop(new FixedBuffer(data), newObject);
			} catch (IOException e) {
				// System.out.println("Could not load object " + objectId);
				e.printStackTrace();
			}
		}
		objectDef.method3287();
		// bar, bank booth
		final String name = objectDef.getName().toLowerCase();

		if (objectId == 11763 || name.equalsIgnoreCase("bank booth") || name.equalsIgnoreCase("counter")) {
			// falador bar & bank booths
			objectDef.setNotClipped(false);
			objectDef.setProjectileClipped(true);
			if (objectDef.getBlockingType() == 0) {
				objectDef.setBlockingType(1);
			}
		}
		// set flag data now
		if (objectDef.isNotClipped()) {
			objectDef.setProjectileClipped(false);
			objectDef.setBlockingType(0);
		}
		OBJECT_DEFINITIONS.put(objectId, objectDef);
		return objectDef;
	}

	public static ObjectDefinition forName(String objectName) {
		for (int objectId = 0; objectId < Utils.getObjectDefinitionsSize(ConfigLoader.store); objectId++) {
			// the definition instance
			ObjectDefinition definition = ObjectDefinitionParser.forId(objectId);
			if (definition == null) {
				continue;
			}
			// the name of the item
			final String name = definition.getName().toLowerCase();
			// the name has the identifier we want
			if (name.equalsIgnoreCase(objectName.replace("_", " "))) {
				return definition;
			}
		}
		return null;
	}

}