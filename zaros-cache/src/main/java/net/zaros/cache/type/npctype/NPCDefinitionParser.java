package net.zaros.cache.type.npctype;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public final class NPCDefinitionParser {

    /**
     * The cached map of npc definitions, the key being the npc id
     */
    private static final ConcurrentHashMap<Integer, NPCDefinition> npcDefinitions = new ConcurrentHashMap<>();

    /**
     * Gets an npc definition by the id of the npc
     *
     * @param npcId The id of the npc
     */
    public static NPCDefinition forId(int npcId) {
        NPCDefinition def = npcDefinitions.get(npcId);
        if (def != null) {
            return def;
        }
        try {
            def = NPCDefinition.readDefinitions(npcId);
        } catch (IOException e) {
            System.out.println("Unable to parse npc definitions (" + npcId + ")");
            e.printStackTrace();
            return null;
        }
        npcDefinitions.put(npcId, def);
        return def;
    }

    public static NPCDefinition forName(String npcName) {
        for (int npcId = 0; npcId < 14362/*CacheFileStore.getNPCDefinitionsSize()*/; npcId++) {
            // the definition instance
            NPCDefinition definition = NPCDefinitionParser.forId(npcId);
            if (definition == null) {
                continue;
            }
            // the name of the item
            final String name = definition.getName().toLowerCase();
            // the name has the identifier we want
            if (name.equalsIgnoreCase(npcName.replace("_", " "))) {
                return definition;
            }
        }
        return null;
    }

    public static List<NPCDefinition> forNameList(String npcName) {
        List<NPCDefinition> list = new ArrayList<>();
        for (int npcId = 0; npcId < 14362/*CacheFileStore.getNPCDefinitionsSize()*/; npcId++) {
            // the definition instance
            NPCDefinition definition = NPCDefinitionParser.forId(npcId);
            if (definition == null) {
                continue;
            }
            // the name of the item
            final String name = definition.getName().toLowerCase();
            // the name has the identifier we want
            if (name.equalsIgnoreCase(npcName.replace("_", " "))) {
                list.add(definition);
            }
        }
        return list;
    }

    /**
     * Clears all npc definitions
     */
    public static void clearNPCDefinitions() {
        npcDefinitions.clear();
    }

}