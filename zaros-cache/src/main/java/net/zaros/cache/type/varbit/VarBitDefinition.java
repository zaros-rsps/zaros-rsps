package net.zaros.cache.type.varbit;

import java.nio.ByteBuffer;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/19/2017
 */
public final class VarBitDefinition {
	
	/**
	 * The id of the varbit
	 */
	@Getter
	@Setter
	private int id;
	
	/**
	 * The base var of the varbit
	 */
	@Getter
	@Setter
	private int baseVar;
	
	/**
	 * The start bit of the varbit
	 */
	@Getter
	@Setter
	private int bitShift;
	
	/**
	 * The end bit of the varbit
	 */
	@Getter
	@Setter
	private int bitSize;
	
	/**
	 * Reads the value loop of the varp
	 *
	 * @param stream
	 * 		The stream
	 */
	public void readValueLoop(ByteBuffer stream) {
		for (; ; ) {
			int opcode = stream.get() & 0xFF;
			if (opcode == 0) {
				break;
			}
			readValues(stream, opcode);
		}
	}
	
	/**
	 * Reads the values from the stream
	 *
	 * @param stream
	 * 		The stream
	 * @param opcode
	 * 		The opcode
	 */
	private void readValues(ByteBuffer stream, int opcode) {
		if (opcode == 1) {
			baseVar = stream.getShort() & 0xFFFF;
			bitShift = stream.get();
			bitSize = stream.get();
		}
	}

	private static final int[] BITS = new int[32];

	public int getValue(int config) {
		int size = BITS[bitSize - bitShift];
		return size & config >> bitShift;
	}

	public int getMask() {
		return BITS[bitSize - bitShift];
	}
	static {
		int flag = 2;
		for (int i = 0; i < 32; i++) {
			BITS[i] = flag - 1;
			flag += flag;
		}
	}
}
