package net.zaros.cache.type.objtype;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import lombok.extern.slf4j.Slf4j;
import net.zaros.cache.type.ConfigLoader;
import net.zaros.cache.util.Utils;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/18/2017
 */
@Slf4j
public class ItemDefinitionParser {

	/**
	 * The map of definitions
	 */
	private static final ConcurrentHashMap<Integer, ItemDefinition> ITEM_DEFINITIONS = new ConcurrentHashMap<>();

	/**
	 * Gets the definitions of an item by the id
	 *
	 * @param itemId
	 *               The id of the item
	 * @return An {@code ItemDefinition} {@code Object}
	 */
	public static ItemDefinition forId(int itemId) {
		ItemDefinition definitions = ITEM_DEFINITIONS.get(itemId);
		if (definitions != null) {
			return definitions;
		} else {
			ItemDefinition def = new ItemDefinition(itemId);
			try {
				def.loadItemDefinition();
			} catch (IOException e) {
				log.error("Unable to parse item definitions", e);
			}
			ITEM_DEFINITIONS.put(itemId, def);
			return def;
		}
	}

	public static ItemDefinition forName(String itemName) {
		for (int itemId = 0; itemId < Utils.getItemDefinitionsSize(ConfigLoader.store); itemId++) {
			// the definition instance
			ItemDefinition definition = ItemDefinitionParser.forId(itemId);
			if (definition == null) {
				continue;
			}
			// the name of the item
			final String name = definition.getName().toLowerCase();
			// the name has the identifier we want
			if (name.equalsIgnoreCase(itemName.replace("_", " "))) {
				return definition;
			}
		}
		return null;
	}

	public static List<ItemDefinition> forNameList(String itemName) {
		List<ItemDefinition> list = new ArrayList<>();
		for (int npcId = 0; npcId < Utils.getItemDefinitionsSize(ConfigLoader.store); npcId++) {
			// the definition instance
			ItemDefinition definition = ItemDefinitionParser.forId(npcId);
			if (definition == null) {
				continue;
			}
			// the name of the item
			final String name = definition.getName().toLowerCase();
			// the name has the identifier we want
			if (name.contains(itemName.replace("_", " "))) {
				list.add(definition);
			}
		}
		return list;
	}

	public static void loadEquipData() {

	}

}