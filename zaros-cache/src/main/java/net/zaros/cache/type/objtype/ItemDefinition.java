package net.zaros.cache.type.objtype;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import net.zaros.cache.type.ConfigLoader;
import net.zaros.cache.util.SkillConstants;
import net.zaros.cache.util.buffer.Buffer;
import net.zaros.cache.util.buffer.FixedBuffer;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/19/2017
 */
@SuppressWarnings("unused")
public final class ItemDefinition {

	private static final int[] ITEM_REQS = { 540, 542, 697, 538 };

	@Getter
	private int id;

	@Getter
	@Setter
	private int equipId;

	@Getter
	@Setter
	private String name = "null";

	@Getter
	private String examine;

	// options
	@Getter
	private String[] groundOptions = new String[] { null, null, "take", null, null };

	@Getter
	private String[] inventoryOptions = new String[] { null, null, null, null, "drop" };

	// extra information
	@Getter
	private int stackable;

	@Getter
	@Setter
	private int value;

	@Getter
	private boolean membersOnly;

	private int interfaceModelId;

	@Getter
	@Setter
	private int equipSlot;

	@Getter
	@Setter
	private int equipType;

	// wearing model information
	@Getter
	private int maleWornModelId1 = -1;

	@Getter
	private int femaleWornModelId1;

	@Getter
	private int maleWornModelId2 = -1;

	private int femaleWornModelId2;

	// model information
	@Getter
	private int[] originalModelColors;

	@Getter
	private int[] modifiedModelColors;

	// model size information
	private int modelZoom;

	private int modelRotation1;

	private int modelRotation2;

	private int modelOffset1;

	private int modelOffset2;
	private float weight;
	private short[] textureColour1;

	private short[] textureColour2;

	private byte[] unknownArray1;

	private int[] unknownArray2;

	// extra information, not used for newer items
	@Getter
	private boolean stockmarket;

	private int colourEquip1;

	private int colourEquip2;

	@Getter
	private int certLink = -1;

	@Getter
	private int certTemplate = -1;

	private int[] stackIds;

	private int[] stackAmounts;

	private int teamId;

	@Getter
	private int lentLink = -1;

	private int lentTemplate = -1;

	private int boughtLink = -1; // not sure

	private int boughtTemplate = -1;

	@Getter
	private HashMap<Integer, Object> params;

	private Map<Integer, Integer> levelRequirements;

	private Map<Integer, Integer> itemRequirements = new HashMap<>();

	public ItemDefinition(int id, byte[] data) {
		this.id = id;
	}

	public ItemDefinition(int id) {
		this.id = id;
	}

	public void loadItemDefinition() throws IOException {
		var data = ConfigLoader.store.getIndexes()[19].getFile(id >>> 8, id & 0xFF);
		if (data != null) {
			decode(new FixedBuffer(data));
			if (certTemplate != -1) // done
			{
				makeCert(ItemDefinitionParser.forId(certLink), ItemDefinitionParser.forId(certTemplate));
			}
			if (lentTemplate != -1) // done
			{
				makeLent(ItemDefinitionParser.forId(lentLink), ItemDefinitionParser.forId(lentTemplate));
			}
			if (boughtTemplate != -1) // need to finish
			{
				makeBought(ItemDefinitionParser.forId(boughtLink), ItemDefinitionParser.forId(boughtTemplate));
			}
		}
		if (params == null) {
			return;
		}
		int levelId = -1, levelReq = -1, itemId = -1;
		for (int opcode2 : params.keySet()) {
			Object value = params.get(opcode2);
			if (value instanceof String) {
				continue;
			}
			int val = (Integer) value;
			if (opcode2 >= 749 && opcode2 < 797) {
				if (opcode2 % 2 == 0) {
					levelReq = val;
				} else {
					levelId = val;
				}
				if (levelId != -1 && levelReq != -1) {
					if (levelRequirements == null) {
						levelRequirements = new HashMap<>();
					}
					if (name.toLowerCase().contains("infinity")) {
						levelRequirements.put(1, 25);
						levelRequirements.put(6, 50);
					} else if (id == 2503) {
						levelRequirements.put((int) SkillConstants.DEFENCE, 40);
						levelRequirements.put((int) SkillConstants.RANGE, 70);
					} else if (id == 7462) {
						levelRequirements.put((int) SkillConstants.DEFENCE, 45);
					} else if (id == 20072) {
						levelRequirements.put((int) SkillConstants.ATTACK, 60);
						levelRequirements.put((int) SkillConstants.DEFENCE, 60);
					} else {
						levelRequirements.put(levelId, levelReq);
					}
					levelId = levelReq = -1;
				}
			}
			for (int reqOpcode : ITEM_REQS) {
				if (opcode2 == reqOpcode) {
					itemId = val;
				} else if (opcode2 - 1 == reqOpcode && itemId != -1) {
					if (itemRequirements == null) {
						itemRequirements = new HashMap<>();
					}
					itemRequirements.put(itemId, val);
					itemId = -1;
				}
			}
			switch (opcode2) {
			case 394:
				if (levelRequirements == null) {
					levelRequirements = new HashMap<>();
				}
				levelRequirements.put((int) SkillConstants.SUMMONING, val);
				break;
			}
		}
	}

	private void setDefaultsVariableValules() {

	}

	private void setDefaultOptions() {
		groundOptions = new String[] { null, null, "take", null, null };
		inventoryOptions = new String[] { null, null, null, null, "drop" };
	}

	private void decode(Buffer buffer) throws IOException {
		while (true) {
			int opcode = buffer.readUnsignedByte();
			if (opcode == 0) {
				break;
			}
			decode(buffer, opcode);
		}
	}

	@Getter
	private ObjForm form = ObjForm.NONE;

	private void makeCert(ItemDefinition reference, ItemDefinition templateReference) {
		membersOnly = reference.membersOnly;
		interfaceModelId = templateReference.interfaceModelId;
		originalModelColors = templateReference.originalModelColors;
		name = reference.name;
		modelOffset2 = templateReference.modelOffset2;
		textureColour1 = templateReference.textureColour1;
		value = reference.value;
		modelRotation2 = templateReference.modelRotation2;
		stackable = 1;
		modifiedModelColors = templateReference.modifiedModelColors;
		modelRotation1 = templateReference.modelRotation1;
		modelZoom = templateReference.modelZoom;
		textureColour1 = templateReference.textureColour1;
		form = ObjForm.CERT;
	}

	private void makeLent(ItemDefinition reference, ItemDefinition templateReference) {
		femaleWornModelId1 = reference.femaleWornModelId1;
		maleWornModelId2 = reference.maleWornModelId2;
		membersOnly = reference.membersOnly;
		interfaceModelId = templateReference.interfaceModelId;
		textureColour2 = reference.textureColour2;
		groundOptions = reference.groundOptions;
		unknownArray1 = reference.unknownArray1;
		modelRotation1 = templateReference.modelRotation1;
		modelRotation2 = templateReference.modelRotation2;
		originalModelColors = reference.originalModelColors;
		name = reference.name;
		maleWornModelId1 = reference.maleWornModelId1;
		colourEquip1 = reference.colourEquip1;
		teamId = reference.teamId;
		modelOffset2 = templateReference.modelOffset2;
		params = reference.params;
		modifiedModelColors = reference.modifiedModelColors;
		colourEquip2 = reference.colourEquip2;
		modelOffset1 = templateReference.modelOffset1;
		textureColour1 = reference.textureColour1;
		value = 0;
		modelZoom = templateReference.modelZoom;
		inventoryOptions = new String[5];
		femaleWornModelId2 = reference.femaleWornModelId2;
		levelRequirements = reference.levelRequirements;
		itemRequirements = reference.itemRequirements;
		if (reference.inventoryOptions != null) {
			inventoryOptions = reference.inventoryOptions.clone();
		}
		inventoryOptions[4] = "Discard";
		form = ObjForm.LENT;
	}

	private void makeBought(ItemDefinition reference, ItemDefinition templateReference) {
		femaleWornModelId2 = reference.femaleWornModelId2;
		inventoryOptions = new String[5];
		modelRotation2 = templateReference.modelRotation2;
		name = reference.name;
		maleWornModelId1 = reference.maleWornModelId1;
		modelOffset2 = templateReference.modelOffset2;
		femaleWornModelId1 = reference.femaleWornModelId1;
		maleWornModelId2 = reference.maleWornModelId2;
		modelOffset1 = templateReference.modelOffset1;
		unknownArray1 = reference.unknownArray1;
		stackable = reference.stackable;
		modelRotation1 = templateReference.modelRotation1;
		textureColour1 = reference.textureColour1;
		colourEquip1 = reference.colourEquip1;
		textureColour2 = reference.textureColour2;
		modifiedModelColors = reference.modifiedModelColors;
		modelZoom = templateReference.modelZoom;
		colourEquip2 = reference.colourEquip2;
		teamId = reference.teamId;
		value = 0;
		groundOptions = reference.groundOptions;
		originalModelColors = reference.originalModelColors;
		membersOnly = reference.membersOnly;
		params = reference.params;
		interfaceModelId = templateReference.interfaceModelId;
		if (reference.inventoryOptions != null) {
			inventoryOptions = reference.inventoryOptions.clone();
		}
		form = ObjForm.BOUGHT;
	}

	private void decode(Buffer buffer, int opcode) throws IOException {
		if (opcode == 1) {
			interfaceModelId = buffer.readSmart2or4();
		} else if (opcode == 2) {
			name = buffer.readString();
		} else if (opcode == 3) {
			examine = buffer.readString();
		} else if (opcode == 4) {
			modelZoom = buffer.readUnsignedShort();
		} else if (opcode == 5) {
			modelRotation1 = buffer.readUnsignedShort();
		} else if (opcode == 6) {
			modelRotation2 = buffer.readUnsignedShort();
		} else if (opcode == 7) {
			modelOffset1 = buffer.readUnsignedShort();
			if (modelOffset1 > 32767) {
				modelOffset1 -= 65536;
			}
		} else if (opcode == 8) {
			modelOffset2 = buffer.readUnsignedShort();
			if (modelOffset2 > 32767) {
				modelOffset2 -= 65536;
			}
		} else if (opcode == 9) {
			weight = buffer.readFloat();
		} else if (opcode == 11) {
			stackable = 1;
		} else if (13 == opcode) {
			equipSlot = buffer.readUnsignedByte();
		} else if (opcode == 14) {
			equipType = buffer.readUnsignedByte();
		} else if (opcode == 12) {
			value = buffer.readInt();
		} else if (opcode == 16) {
			membersOnly = true;
		} else if (opcode == 18) {
			buffer.readShort();
		} else if (opcode == 23) {
			maleWornModelId1 = buffer.readSmart2or4();
		} else if (opcode == 24) {
			femaleWornModelId1 = buffer.readSmart2or4();
		} else if (opcode == 25) {
			maleWornModelId2 = buffer.readSmart2or4();
		} else if (opcode == 26) {
			femaleWornModelId2 = buffer.readSmart2or4();
		} else if (opcode >= 30 && opcode < 35) {
			groundOptions[opcode - 30] = buffer.readString();
		} else if (opcode >= 35 && opcode < 40) {
			inventoryOptions[opcode - 35] = buffer.readString();
		} else if (opcode == 40) {
			int length = buffer.readUnsignedByte();
			originalModelColors = new int[length];
			modifiedModelColors = new int[length];
			for (int index = 0; index < length; index++) {
				originalModelColors[index] = buffer.readShort();
				modifiedModelColors[index] = buffer.readShort();
			}
		} else if (opcode == 41) {
			int length = buffer.readUnsignedByte();
			textureColour1 = new short[length];
			textureColour2 = new short[length];
			for (int index = 0; index < length; index++) {
				textureColour1[index] = (short) buffer.readShort();
				textureColour2[index] = (short) buffer.readShort();
			}
		} else if (opcode == 42) {
			int length = buffer.readUnsignedByte();
			unknownArray1 = new byte[length];
			for (int index = 0; index < length; index++) {
				unknownArray1[index] = (byte) buffer.readByte();
			}
		} else if (opcode == 65) {
			stockmarket = true;
		} else if (opcode == 78) {
			colourEquip1 = buffer.readSmart2or4();
		} else if (opcode == 79) {
			colourEquip2 = buffer.readSmart2or4();
		} else if (opcode == 87) {

		} else if (opcode == 90) {
			buffer.readShort();
		} else if (opcode == 91) {
			buffer.readShort();
		} else if (opcode == 92) {
			buffer.readShort();
		} else if (opcode == 93) {
			buffer.readShort();
		} else if (opcode == 95) {
			buffer.readShort();
		} else if (opcode == 96) {
			buffer.readUnsignedByte();
		} else if (opcode == 97) {
			certLink = buffer.readUnsignedShort();
		} else if (opcode == 98) {
			certTemplate = buffer.readUnsignedShort();
		} else if (opcode >= 100 && opcode < 110) {
			if (stackIds == null) {
				stackIds = new int[10];
				stackAmounts = new int[10];
			}
			stackIds[opcode - 100] = buffer.readUnsignedShort();
			stackAmounts[opcode - 100] = buffer.readUnsignedShort();
		} else if (opcode == 110) {
			buffer.readShort();
		} else if (opcode == 111) {
			buffer.readShort();
		} else if (opcode == 112) {
			buffer.readShort();
		} else if (opcode == 113) {
			buffer.readUnsignedByte();
		} else if (opcode == 114) {
			buffer.readUnsignedByte();
		} else if (opcode == 115) {
			teamId = buffer.readUnsignedByte();
		} else if (opcode == 121) {
			lentLink = buffer.readUnsignedShort();
		} else if (opcode == 122) {
			lentTemplate = buffer.readUnsignedShort();
		} else if (opcode == 125) {
			buffer.readUnsignedByte();
			buffer.readUnsignedByte();
			buffer.readUnsignedByte();
		} else if (opcode == 126) {
			buffer.readUnsignedByte();
			buffer.readUnsignedByte();
			buffer.readUnsignedByte();
		} else if (opcode == 127) {
			buffer.readUnsignedByte();
			buffer.readShort();
		} else if (opcode == 128) {
			buffer.readUnsignedByte();
			buffer.readShort();
		} else if (opcode == 129) {
			buffer.readUnsignedByte();
			buffer.readShort();
		} else if (opcode == 130) {
			buffer.readUnsignedByte();
			buffer.readShort();
		} else if (opcode == 132) {
			int length = buffer.readUnsignedByte();
			unknownArray2 = new int[length];
			for (int index = 0; index < length; index++) {
				unknownArray2[index] = buffer.readUnsignedShort();
			}
		} else if (opcode == 134) {
			buffer.readUnsignedByte();
		} else if (opcode == 139) {
			boughtLink = buffer.readUnsignedShort();
		} else if (opcode == 140) {
			boughtTemplate = buffer.readUnsignedShort();
		} else if (opcode >= 142 && opcode < 147) {
			buffer.readUnsignedShort();
		} else if (opcode >= 150 && opcode < 155) {
			buffer.readUnsignedShort();
		} else if (opcode == 249) {
			int length = buffer.readUnsignedByte();
			if (params == null) {
				params = new HashMap<>();
			}
			for (int index = 0; index < length; index++) {
				boolean string = buffer.readUnsignedByte() == 1;
				int key = buffer.read24BitInt();
				Object value = string ? buffer.readString() : buffer.readInt();
				params.put(key, value);
			}
		} else {
			// throw new RuntimeException("Unhandled opcode! opcode:" + opcode);
		}
	}

	public boolean hasSpecialBar() {
		if (params == null) {
			return false;
		}
		Object specialBar = params.get(687);
		return specialBar != null && specialBar instanceof Integer && (Integer) specialBar == 1;
	}

	public int getGroupId() {
		if (params == null) {
			return 0;
		}
		Object specialBar = params.get(686);
		if (specialBar != null && specialBar instanceof Integer) {
			return (Integer) specialBar;
		}
		return 0;
	}

	public int getRenderAnimId() {
		if (params == null) {
			return 1426;
		}
		Object animId = params.get(644);
		if (animId != null && animId instanceof Integer) {
			return (Integer) animId;
		}
		return 1426;
	}

	public int getQuestId() {
		if (params == null) {
			return -1;
		}
		Object questId = params.get(861);
		if (questId != null && questId instanceof Integer) {
			return (Integer) questId;
		}
		return -1;
	}

	/**
	 * Prints all fields in this class.
	 */
	public void printFields() {
		for (Field field : getClass().getDeclaredFields()) {
			if ((field.getModifiers() & 8) != 0) {
				continue;
			}
			try {
				System.out.println(field.getName() + ": " + getValue(field));
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		System.out.println("-- CS2ScriptData --");
		for (int key : params.keySet()) {
			Object o = params.get(key);
			System.out.println("CS2Script - [key=" + key + ", value=" + o + "].");
		}
		System.out.println("-- end of " + getClass().getSimpleName() + " fields --");
	}

	private Object getValue(Field field) throws Throwable {
		field.setAccessible(true);
		Class<?> type = field.getType();
		if (type == int[][].class) {
			return Arrays.toString((int[][]) field.get(this));
		} else if (type == int[].class) {
			return Arrays.toString((int[]) field.get(this));
		} else if (type == byte[].class) {
			return Arrays.toString((byte[]) field.get(this));
		} else if (type == short[].class) {
			return Arrays.toString((short[]) field.get(this));
		} else if (type == double[].class) {
			return Arrays.toString((double[]) field.get(this));
		} else if (type == float[].class) {
			return Arrays.toString((float[]) field.get(this));
		} else if (type == Object[].class) {
			return Arrays.toString((Object[]) field.get(this));
		}
		return field.get(this);
	}

	public Map<Integer, Object> getParams() {
		return params;
	}

	public void getParamsString() {
		for (int key : params.keySet()) {
			System.out.println(key + ", " + params.get(key));
		}
	}

	public HashMap<Integer, Integer> getWearingRequirements() {
		HashMap<Integer, Integer> skills = new HashMap<>();
		if (params == null) {
			return skills;
		}
		int nextLevel = -1;
		int nextSkill = -1;
		for (int key : params.keySet()) {
			Object value = params.get(key);
			if (value instanceof String) {
				continue;
			}
			if (key >= 749 && key < 797) {
				if (key % 2 == 0) {
					nextLevel = (Integer) value;
				} else {
					nextSkill = (Integer) value;
				}
				if (nextLevel != -1 && nextSkill != -1) {
					if (nextSkill >= SkillConstants.SKILL_NAME.length) {
						skills.put(nextLevel, nextSkill);
					} else {
						skills.put(nextSkill, nextLevel);
					}
					nextLevel = -1;
					nextSkill = -1;
				}
			}
		}
		return skills;
	}

	public boolean isStackable() {
		return stackable == 1;
	}

	public boolean isCert() {
		return certTemplate != -1;
	}

	public boolean isWearItem() {
		if (inventoryOptions == null) {
			return false;
		}
		for (String option : inventoryOptions) {
			if (option == null) {
				continue;
			}
			if (option.equalsIgnoreCase("wield") || option.equalsIgnoreCase("wear") || option.equalsIgnoreCase("equip")) {
				return equipSlot != -1;
			}
		}
		return false;
	}

	public boolean isWearItem(boolean male) {
		if (inventoryOptions == null) {
			return false;
		}
		if (male ? getMaleWornModelId1() == -1 : getFemaleWornModelId1() == -1) {
			return false;
		}
		for (String option : inventoryOptions) {
			if (option == null) {
				continue;
			}
			if (option.equalsIgnoreCase("wield") || option.equalsIgnoreCase("wear") || option.equalsIgnoreCase("equip")) {
				return equipSlot != -1;
			}
		}
		return false;
	}

	/**
	 * Checks if the item has the selected inventory option
	 *
	 * @param option
	 *               The option to look for
	 */
	public boolean hasOption(String option) {
		for (String inventoryOption : inventoryOptions) {
			if (inventoryOption != null && inventoryOption.equalsIgnoreCase(option)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if these are the definitions of an edible item
	 */
	public boolean isEdible() {
		return hasOption("eat") || hasOption("drink");
	}


	private static String filterName(String name) {
		name = name.replace("(", "");
		name = name.replace(")", "");
		name = name.replace("'", "");
		name = name.toUpperCase();
		name = name.trim();
		return name;
	}

	/**
	 * @author Walied K. Yassen
	 */
	public static enum ObjForm {
		NONE,
		CERT,
		LENT,
		BOUGHT
	}
}