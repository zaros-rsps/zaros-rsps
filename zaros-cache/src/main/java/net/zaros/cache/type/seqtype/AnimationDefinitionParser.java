package net.zaros.cache.type.seqtype;

import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentHashMap;

import net.zaros.cache.type.ConfigLoader;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/1/2017
 */
public final class AnimationDefinitionParser {

	/**
	 * The map of definitions
	 */
	private static final ConcurrentHashMap<Integer, AnimationDefinition> DEFINITIONS = new ConcurrentHashMap<>();

	/**
	 * Gets animation definitions by an animation id
	 *
	 * @param animationId
	 *                    The animation id
	 */
	public static AnimationDefinition forId(int animationId) {
		if (animationId < 0) {
			return null;
		}
		AnimationDefinition definitions = DEFINITIONS.get(animationId);
		if (definitions != null) {
			return definitions;
		} else {
			byte[] is = null;
			try {
				is = ConfigLoader.store.getIndexes()[20].getFile(animationId >>> 7, animationId & 0x7f);
			} catch (Exception e) {
				e.printStackTrace();
			}
			definitions = new AnimationDefinition();
			try {
				if (is != null) {
					definitions.init(ByteBuffer.wrap(is));
				}
			} catch (Exception e) {
				throw new IllegalStateException("Unable to find definition for animation id " + animationId + ".", e);
			}
			definitions.setId(animationId);
			definitions.method544();
			DEFINITIONS.put(animationId, definitions);
			return definitions;
		}
	}

}