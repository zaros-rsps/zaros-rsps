package net.zaros.cache.type.defaults;

import java.io.IOException;

import lombok.Getter;
import lombok.Setter;
import net.zaros.cache.type.ConfigLoader;
import net.zaros.cache.util.buffer.Buffer;
import net.zaros.cache.util.buffer.FixedBuffer;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/21/2017
 */
public final class BodyDataParser {
	
	/**
	 * The array of body data
	 */
	@Getter
	@Setter
	private static int[] bodyData;
	
	public static void loadAll() {
		BodyData read = null;
		try {
			read = BodyDataParser.read();
		} catch (IOException e) {
			System.out.println("Unable to parse body data!");
			e.printStackTrace();
		}
		if (read == null) {
			return;
		}
		setBodyData(read.getPartsData());
	}
	
	/**
	 * Reads body data from the cache.
	 *
	 * @return The body data object, or null if it failed.
	 */
	public static BodyData read() throws IOException {
		BodyData data = new BodyData();
		byte[] buff = ConfigLoader.store.getIndexes()[28].getFile(6, 0);
		Buffer reader = new FixedBuffer(buff);
		data.parse(reader);
		return data;
	}
}
