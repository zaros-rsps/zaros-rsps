package net.zaros.cache.type.defaults;

import lombok.Getter;
import lombok.Setter;
import net.zaros.cache.util.buffer.Buffer;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/21/2017
 */
public class BodyData {

	/**
	 * The parts data.
	 */
	@Getter
	private int[] partsData;

	/**
	 * Contains weapon data.
	 */
	@Getter
	private int[] weaponData;

	/**
	 * Contains shield data.
	 */
	@Getter
	private int[] shieldData;

	/**
	 * Something to do with weapon display.
	 */
	@Getter
	@Setter
	private int somethingWithWeaponDisplay;

	/**
	 * Something to do with shield display.
	 */
	@Getter
	@Setter
	private int somethingWithShieldDisplay;

	/**
	 * Constructs a new {@code BodyData} {@code Object}.
	 */
	public BodyData() {
		partsData = new int[0];
		somethingWithWeaponDisplay = -1;
		somethingWithShieldDisplay = -1;
	}

	/**
	 * Parses body data from the given buffer.
	 *
	 * @param buffer
	 *               The buffer.
	 */
	public void parse(Buffer buffer) {
		for (;;) {
			int opcode = (byte) buffer.readUnsignedByte();
			if (opcode == 0) {
				break;
			}
			this.parse(opcode, buffer);
		}
	}

	/**
	 * Parses the current opcode.
	 *
	 * @param opcode
	 *               The opcode.
	 * @param buffer
	 *               The buffer to parse from.
	 */
	public void parse(int opcode, Buffer buffer) {
		if (opcode == 1) {
			int length = buffer.readUnsignedByte();
			partsData = new int[length];
			for (int i = 0; i < partsData.length; i++) {
				partsData[i] = buffer.readUnsignedByte();
			}
		} else if (opcode == 3) {
			setSomethingWithShieldDisplay(buffer.readUnsignedByte());
		} else if (opcode == 4) {
			setSomethingWithWeaponDisplay(buffer.readUnsignedByte());
		} else if (opcode == 5) {
			int length = buffer.readUnsignedByte();
			shieldData = new int[length];
			for (int i = 0; i < shieldData.length; i++) {
				shieldData[i] = buffer.readUnsignedByte();
			}
		} else if (opcode == 6) {
			int length = buffer.readUnsignedByte();
			weaponData = new int[length];
			for (int i = 0; i < weaponData.length; i++) {
				weaponData[i] = buffer.readUnsignedByte();
			}
		} else {
			throw new RuntimeException("Unknown opcode:" + opcode);
		}
	}

}