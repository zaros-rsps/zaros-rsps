package net.zaros.discord.listener;

import org.javacord.api.event.message.reaction.ReactionAddEvent;
import org.javacord.api.listener.message.reaction.ReactionAddListener;

import net.zaros.discord.util.ZarosChannels;

/**
 * @author Walied K. Yassen
 */
public final class TodoListener implements ReactionAddListener {

	private static final char CHECKED = '\u2705';
	private static final char UNCHECKED = '\u274C';

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.javacord.api.listener.message.reaction.ReactionAddListener#onReactionAdd(
	 * org.javacord.api.event.message.reaction.ReactionAddEvent)
	 */
	@Override
	public void onReactionAdd(ReactionAddEvent event) {
		var channel = event.getChannel();
		if (!channel.equals(ZarosChannels.getTodoChannel())) {
			return;
		}
		var message = event.getApi().getMessageById(event.getMessageId(), event.getChannel()).join();
		var content = extractMessage(message.getContent());
		if (content == null) {
			return;
		}
		var author = message.getAuthor();
		if (!author.isYourself()) {
			return;
		}
		var reaction = event.getReaction().isPresent() ? event.getReaction().get() : message.getReactions().get(0);
		var emoji = event.getEmoji();
		if (!emoji.isUnicodeEmoji()) {
			return;
		}
		var unicode = emoji.asUnicodeEmoji().get();
		var ch = unicode.charAt(0);
		reaction.remove().join();
		if (ch == CHECKED) {
			message.edit(":white_check_mark: " + content).join();
		} else if (ch == UNCHECKED) {
			message.edit(":x: " + content).join();
		}
	}

	private static String extractMessage(String content) {
		if (content.startsWith(":x: ")) {
			return content.substring(4);
		} else if (content.startsWith(":white_check_mark: ")) {
			return content.substring(19);
		}
		return null;
	}

}
