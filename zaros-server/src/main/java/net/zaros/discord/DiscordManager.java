package net.zaros.discord;

import lombok.Getter;
import net.zaros.discord.bot.ZarosBot;
import net.zaros.discord.commands.InvestigateCommand;
import net.zaros.discord.commands.LinkCommand;
import net.zaros.discord.commands.PlayersCommand;
import net.zaros.discord.commands.TodoCommand;
import net.zaros.discord.commands.UptimeCommand;
import net.zaros.discord.listener.TodoListener;
import net.zaros.discord.util.ZarosChannels;
import net.zaros.discord.util.ZarosRoles;
import net.zaros.server.Configuration;
import net.zaros.server.Configuration.Discord;
import net.zaros.server.utility.newrepository.Repository;

/**
 * Represents the Discord integration manager.
 * 
 * @author Walied K. Yassen
 */
public final class DiscordManager {

	/**
	 * The {@link ZarosBot} object instance.
	 */
	@Getter
	private static ZarosBot bot;

	/**
	 * Initialises the Discord integration in our system.
	 */
	public static void initialise() {
		if (!System.getProperty("user.name").equals("WaliedKYassen")) {
			return;
		}
		// @formatter:off
		bot = new ZarosBot(Configurations.builder()
				.name(Discord.NAME)
				.apiToken(Discord.TOKEN)
				.database(Repository.getDatabaseDirectory().getDirectory(Configuration.Repository.DISCORD_DATABASE).getPath().toFile().getAbsolutePath())
				.build());
		bot.initialise();
		bot.start();
		// @formatter:on
		registerCommands();
		// initialise the channels
		ZarosChannels.initialie(bot);
		// initialise the roles.
		ZarosRoles.initialie(bot);
		// register the todo listener.
		bot.getApi().addReactionAddListener(new TodoListener());
	}

	/**
	 * Registers the commands that are specific to our server.
	 */
	private static void registerCommands() {
		var repository = bot.getCommands();
		repository.register(new UptimeCommand());
		repository.register(new PlayersCommand());
		repository.register(new LinkCommand());
		repository.register(new InvestigateCommand());
		repository.register(new TodoCommand());
	}
}
