package net.zaros.discord.commands;

import net.zaros.server.game.world.World;

/**
 * Represents the players server command.
 * 
 * @author Walied K. Yassen
 */
public final class PlayersCommand extends Command {

	/**
	 * Constructs a new {@link PlayersCommand} type object instance.
	 */
	public PlayersCommand() {
		super(null, new String[] { "players" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.discord.commands.Command#execute(net.zaros.discord.commands.
	 * CommandContext)
	 */
	@Override
	protected void execute(CommandContext context) {
		context.basicMention("there are currently " + World.getPlayerCount() + " players online.");
	}
}
