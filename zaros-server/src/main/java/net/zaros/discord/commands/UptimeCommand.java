package net.zaros.discord.commands;

import java.time.Duration;

import net.zaros.server.game.world.World;

/**
 * Represents the uptime server command.
 * 
 * @author Walied K. Yassen
 */
public final class UptimeCommand extends Command {

	/**
	 * Constructs a new {@link UptimeCommand} type object instance.
	 */
	public UptimeCommand() {
		super(null, new String[] { "uptime" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.discord.commands.Command#execute(net.zaros.discord.commands.
	 * CommandContext)
	 */
	@Override
	protected void execute(CommandContext context) {
		var duration = World.getStopwatch().elapsed();
		context.basicMention("the server has been up for" + formatDuration(duration) + ".");
	}

	private static String formatDuration(Duration duration) {
		var days = duration.toDaysPart();
		duration.minusDays(days);
		var hours = duration.toHoursPart();
		duration.minusHours(hours);
		var minutes = duration.toMinutesPart();
		duration.minusMinutes(minutes);
		var seconds = duration.toSecondsPart();
		duration.minusSeconds(seconds);
		var builder = new StringBuilder();
		boolean added = false;
		if (days > 0) {
			builder.append(" ").append(days).append(" day");
			if (days > 1) {
				builder.append("s");
			}
			added = true;
		}
		if (hours > 0) {
			if (added) {
				builder.append(",");
				added = false;
			}
			builder.append(" ").append(hours).append(" hour");
			if (hours > 1) {
				builder.append("s");
			}
			added = true;
		}
		if (minutes > 0) {
			if (added) {
				builder.append(",");
				added = false;
			}
			builder.append(" ").append(minutes).append(" min");
			if (minutes > 1) {
				builder.append("s");
			}
			added = true;
		}
		if (seconds > 0) {
			if (added) {
				builder.append(", and");
				added = false;
			}
			builder.append(" ").append(seconds).append(" sec");
			if (seconds > 1) {
				builder.append("s");
			}
		}
		return builder.toString();
	}
}
