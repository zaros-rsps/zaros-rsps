package net.zaros.discord.commands;

import net.zaros.server.game.world.World;

/**
 * @author Walied K. Yassen
 */
public class LinkCommand extends Command {

	/**
	 * Constructs a new {@link LinkCommand} type object instance.
	 */
	public LinkCommand() {
		super(null, new String[] { "link" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.discord.commands.Command#execute(net.zaros.discord.commands.
	 * CommandContext)
	 */
	@Override
	protected void execute(CommandContext context) {
		if (context.getProfile().isLinked()) {
			context.error("Linking", "Your account has been already linked to a profile: " + context.getProfile().getUsername() + ".");
			return;
		}
		var code = context.requireArgument(0, String.class);
		var result = context.getBot().getLinking().test(code);
		if (result != null) {
			var profile = context.getProfile();
			profile.setUsername(result);
			context.getBot().getProfiles().storeProfile(profile);
			context.success("Linking", "Your account has been successfully linked to your in-game profile: " + context.getProfile().getUsername());
			World.getPlayerByUsername(context.getProfile().getUsername()).ifPresent(player -> {
				player.setDiscordId(context.getProfile().getUser().getId());
				player.getTransmitter().sendMessage("<col=0000ee>Your account has been successfully linked to your Discord account.");
			});
		} else {
			context.error("Linking", "Failed to verify your link request authentication code.");
		}
	}

}
