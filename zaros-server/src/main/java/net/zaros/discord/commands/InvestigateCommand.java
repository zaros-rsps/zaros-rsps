package net.zaros.discord.commands;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.stream.Collectors;

import org.javacord.api.entity.message.embed.EmbedBuilder;

import net.zaros.discord.util.ZarosRoles;
import net.zaros.server.game.world.World;

/**
 * @author Walied K. Yassen
 */
public class InvestigateCommand extends Command {

	private static final DecimalFormat FORMAT = new DecimalFormat("#.##");

	/**
	 * Constructs a new {@link InvestigateCommand} type object instance.
	 */
	public InvestigateCommand() {
		super(ZarosRoles.getModeratorRole(), new String[] { "investigate", "invs" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.discord.commands.Command#execute(net.zaros.discord.commands.
	 * CommandContext)
	 */
	@Override
	protected void execute(CommandContext context) throws CommandException {
		var username = context.requireArgument(0, String.class);
		var optional = World.getPlayerByUsername(username);
		if (!optional.isPresent()) {
			context.error("Error", "Could not find an online player with the username: " + username);
			return;
		}
		var player = optional.get();
		var builder = new EmbedBuilder();
		builder.setColor(new Color(0x800080));
		builder.setAuthor("Investigating " + username, null, "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/72/twitter/154/right-pointing-magnifying-glass_1f50e.png");
		builder.addInlineField("Username", player.getDetails().getUsername());
		builder.addInlineField("Display", player.getDetails().getDisplayName());
		builder.addInlineField("Rank", String.join(",", player.getDetails().getRights().stream().map(String::valueOf).collect(Collectors.toList())));
		builder.addInlineField("Inventory Wealth", formatAmount(player.getInventory().getItems().getWealth()) + " gp");
		builder.addInlineField("Equipment Wealth", formatAmount(player.getEquipment().getItems().getWealth()) + " gp");
		builder.addInlineField("Bank Wealth", formatAmount(player.getBank().getWealth()) + " gp");
		builder.addInlineField("Location", player.getLocation().toString());
		builder.addInlineField("Current Health", String.valueOf(player.getHealthPoints()));
		builder.addInlineField("Max Health", String.valueOf(player.getMaxHealth()));
		builder.addInlineField("Total Level", String.valueOf(player.getSkills().getTotalLevel()));
		builder.addInlineField("Total Exp.", String.valueOf(player.getSkills().getTotalExp()));
		builder.addInlineField("Combat Level", String.valueOf(player.getSkills().getCombatLevel()));
		builder.addInlineField("Discord", player.hasLinkedDiscord() ? context.getBot().getApi().getUserById(player.getDiscordId()).join().getMentionTag() : "Unlinked");
		builder.addInlineField("Points", context.getProfile().getPoints() + " dp");
		context.powerEmbed(builder);
		context.getEvent().getChannel().sendMessage(builder).join();

	}

	/**
	 * @param wealth
	 * @return
	 */
	private String formatAmount(long amount) {
		var formatted = "";
		if (amount > 1000000000) {
			formatted = FORMAT.format(amount / 1000000000D) + "B";
		} else if (amount > 1000000) {
			formatted = FORMAT.format(amount / 1000000D) + "M";
		} else if (amount > 100000) {
			formatted = FORMAT.format(amount / 1000000D) + "K";
		} else {
			formatted = String.valueOf(amount);
		}
		return formatted;
	}

}
