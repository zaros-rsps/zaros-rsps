package net.zaros.discord.commands;

import net.zaros.discord.util.ZarosChannels;
import net.zaros.discord.util.ZarosRoles;

/**
 * @author Walied K. Yassen
 */
public class TodoCommand extends Command {

	/**
	 * Constructs a new {@link TodoCommand} type object instance.
	 */
	public TodoCommand() {
		super(ZarosRoles.getDeveloperRole(), new String[] { "todo" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.discord.commands.Command#execute(net.zaros.discord.commands.
	 * CommandContext)
	 */
	@Override
	protected void execute(CommandContext context) throws CommandException {
		var channel = ZarosChannels.getTodoChannel();
		context.requireArgument(0, String.class);
		var message = String.join(" ", context.getArguments());
		channel.sendMessage(":x: " + message + ".").join();
	}

}
