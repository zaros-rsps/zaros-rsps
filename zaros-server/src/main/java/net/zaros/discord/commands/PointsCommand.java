package net.zaros.discord.commands;

import org.javacord.api.entity.permission.Role;

/**
 * @author Walied K. Yassen
 */
public final class PointsCommand extends Command {

	/**
	 * Constructs a new {@link PointsCommand} type object instance.
	 * 
	 * @param requiredRole
	 * @param aliases
	 */
	public PointsCommand(Role requiredRole, String[] aliases) {
		super(requiredRole, aliases);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.discord.commands.Command#execute(net.zaros.discord.commands.
	 * CommandContext)
	 */
	@Override
	protected void execute(CommandContext context) throws CommandException {
		context.basicMention("you currently have " + context.getProfile().getPoints() + " discord points.");
	}

}
