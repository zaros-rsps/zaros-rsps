package net.zaros.discord.util;

import org.javacord.api.entity.permission.Role;

import lombok.Getter;
import net.zaros.discord.bot.ZarosBot;

/**
 * Contains the Zaros roles Discord channels.
 * 
 * @author Walied K. Yassen
 */
public final class ZarosRoles {

	/**
	 * The moderator role.
	 */
	@Getter
	private static Role moderatorRole;

	/**
	 * The developer role.
	 */
	@Getter
	private static Role developerRole;

	/**
	 * Initialises the channels list.
	 * 
	 * @param bot
	 *            the bot to initialise from.
	 */
	public static void initialie(ZarosBot bot) {
		var api = bot.getApi();
//		moderatorRole = api.getRoleById(507182125540442123L).get();
		developerRole = api.getRoleById(506172055117365250L).get();
	}

	private ZarosRoles() {
		// NOOP
	}
}
