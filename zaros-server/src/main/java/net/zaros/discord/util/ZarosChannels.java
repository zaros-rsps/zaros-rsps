package net.zaros.discord.util;

import org.javacord.api.entity.channel.TextChannel;

import lombok.Getter;
import net.zaros.discord.bot.ZarosBot;

/**
 * Contains the Zaros server Discord channels.
 * 
 * @author Walied K. Yassen
 */
public final class ZarosChannels {

	/**
	 * The yell channel.
	 */
	@Getter
	private static TextChannel yellChannel;

	/**
	 * The todo channel.
	 */
	@Getter
	private static TextChannel todoChannel;

	/**
	 * Initialises the channels list.
	 * 
	 * @param bot
	 *            the bot to initialise from.
	 */
	public static void initialie(ZarosBot bot) {
		var api = bot.getApi();
		yellChannel = api.getTextChannelById(507182125540442123L).get();
		todoChannel = api.getTextChannelById(508163641154207749L).get();
	}

	private ZarosChannels() {
		// NOOP
	}
}
