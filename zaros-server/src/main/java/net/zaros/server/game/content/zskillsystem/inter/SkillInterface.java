package net.zaros.server.game.content.zskillsystem.inter;

import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Jacob Rhiel
 */
public interface SkillInterface {

    /** Constructs a new skill interface for said player. **/
    boolean constructSkillInterface(Player player);

    /** Handles the interface actions. IE: Button pressed. **/
    boolean handleInterfaceAction(Player player, int button);

    /** Handles what happens when the skill interface is closed. **/
    boolean handleSkillInterfaceClose(Player player);

}
