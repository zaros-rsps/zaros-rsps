package net.zaros.server.game.content.activity;

import net.zaros.server.core.event.player.area.AreaActivityEvent;
import net.zaros.server.game.GameConstants;
import net.zaros.server.game.GameFlags;
import net.zaros.server.game.content.activity.impl.pvp.PvPAreaActivity;
import net.zaros.server.game.content.activity.impl.pvp.PvPLocation;
import net.zaros.server.game.content.activity.impl.pvp.WildernessActivity;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/5/2017
 */
public class ActivitySystem {

	/**
	 * Fires a location update to the player's activity
	 *
	 * @param player   The player
	 * @param location The location of the player, parameterized because we might
	 *                 have a new location before the actual location is updated
	 *                 [teleporting instance]
	 */
	public static void fireLocationUpdate(Player player, Location location) {
		fireAreaActivity(player, location);
		player.getManager().getActivities().getActivity().ifPresent(Activity::updateLocation);
	}

	/**
	 * Fires the area activity
	 *
	 * @param player   The player
	 * @param location The location of the player, parameterized because we might
	 *                 have a new location before the actual location is updated
	 *                 [teleporting instance]
	 */
	public static void fireAreaActivity(Player player, Location location) {
		// we have an activity so we dont force another to start
		if (player.getManager().getActivities().getActivity().isPresent()) {
			return;
		}
		boolean cancelled = World.getEventManager().post(new AreaActivityEvent(player, location));
		if (cancelled) {
			return;
		}
		// starting the pvp activity if we're in a pvp world and a pvp location
		if (GameFlags.worldId == GameConstants.PVP_WORLD_ID) {
			if (PvPLocation.isAtPvpLocation(location)) {
				startActivity(player, new PvPAreaActivity());
			}
		} else {
			if (PvPLocation.isAtWild(location)) {
				startActivity(player, new WildernessActivity());
			}
		}
	}

	/**
	 * Starts an activity and ends the previous one
	 *
	 * @param player   The player
	 * @param activity The activity
	 */
	public static void startActivity(Player player, Activity activity) {
		player.getManager().getActivities().getActivity().ifPresent(Activity::end);
		player.getManager().getActivities().startActivity(activity);
	}

}
