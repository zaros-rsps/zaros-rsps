package net.zaros.server.game.content.activity.impl;

import net.zaros.server.game.content.activity.Activity;
import net.zaros.server.game.node.Node;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/9/2017
 */
public class BarrowsActivity extends Activity {
	
	@Override
	public void start() {
		super.start();
	}
	
	@Override
	public void end() {
		super.end();
	}
	
	@Override
	public boolean handleNodeInteraction(Node node, InteractionOption option) {
		return super.handleNodeInteraction(node, option);
	}
	
	
}
