package net.zaros.server.game.node.entity.data.weapon.impl.ammunitions;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.Ammunition;
import net.zaros.server.game.node.entity.render.flag.impl.GraphicHeight;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.EquipConstants;

public class Arrows implements Ammunition {

    /**
     * The arrow's poison damage.
     */
    private int poisonDamage;

    public Arrows() {
    }

    public Arrows(int poisonDamage) {
        this.poisonDamage = poisonDamage;
    }

    @Override
    public int ammoSlot() {
        return EquipConstants.SLOT_ARROWS;
    }

    @Override
    public int poisonDamage() {
        return poisonDamage;
    }

    @Override
    public boolean accumulatorSupport() {
        return true;
    }

    @Override
    public boolean usable(Entity entity) {
        return true;
    }

    @Override
    public void performGraphics(Entity entity) {
        int graphicId = -1;
        int height = GraphicHeight.HIGH.toInt();


        var arrowID = entity.isPlayer() ? entity.toPlayer().getEquipment().getIdInSlot(ammoSlot()) : -1;

        switch (arrowID) {
            case Items.RUNE_ARROW:
                graphicId = 24;
                break;
        }

        if (graphicId > 0) {
            entity.sendGraphics(graphicId, height, 0);
        }
    }

    @Override
    public void sendProjetile(Entity attacker, Entity target) {
        int projectileID = -1;

        var arrowID = attacker.isPlayer() ? attacker.toPlayer().getEquipment().getIdInSlot(ammoSlot()) : -1;

        switch (arrowID) {
            case Items.RUNE_ARROW:
                projectileID = 15;
                break;
        }

        if (projectileID > 0) {
            ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(attacker, target, projectileID, 43, 31, 40, 0, 0));
        }
    }
}
