package net.zaros.server.game.node.item.degrade;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.node.entity.player.Player;

public class DegradeManager {

    public DegradeManager() {

    }

    @Setter
    @Getter
    private transient Player player;

    public DegradableEquipment getDegradableEquipment() {
        return DegradableRegistry.getDegradable(getPlayer());
    }

}
