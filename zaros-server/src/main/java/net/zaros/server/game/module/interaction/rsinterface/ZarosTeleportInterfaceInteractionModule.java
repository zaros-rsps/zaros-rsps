package net.zaros.server.game.module.interaction.rsinterface;

import net.zaros.server.game.module.type.InterfaceInteractionModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Jacob Rhiel
 */
public class ZarosTeleportInterfaceInteractionModule implements InterfaceInteractionModule {

    @Override
    public int[] interfaceSubscriptionIds() {
        return new int[] { 1083 };
    }

    @Override
    public boolean handle(Player player, int interfaceId, int componentId, int itemId, int slotId, int packetId) {
        return false;
    }

    /**
     * Displays the supplies interface
     *
     * @param player player
     */
    public static void display(Player player) {



    }

}
