/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames.types.multi;

import lombok.Getter;
import net.zaros.server.game.content.minigames.MinigameBase;
import net.zaros.server.game.content.minigames.MinigameProperties;
import net.zaros.server.game.content.minigames.MultiareaProperties;
import net.zaros.server.game.content.minigames.controller.MinigameActivity;
import net.zaros.server.game.content.minigames.util.JoinLeaveOptions;
import net.zaros.server.game.node.entity.player.Player;

/**
 * Represents a minigame that has multiple area for the minigame which are the
 * lobby area, the waiting area, and the game area.
 * 
 * @author Walied K. Yassen
 */
public abstract class MultiareaMinigame<L extends MinigameLobby<?>, W extends MinigameWaiting<?>, A extends MinigameArena<?>> extends MinigameBase {

	/**
	 * The minigame lobby lobby.
	 */
	@Getter
	protected L lobbyArea;

	/**
	 * The minigame waiting area.
	 */
	@Getter
	protected W waitingArea;

	/**
	 * The minigame game area.
	 */
	@Getter
	protected A gameArea;

	/**
	 * Construct a new {@link MultiareaMinigame} type object instance.
	 * 
	 * @param properties
	 *                   the minigame properties.
	 */
	public MultiareaMinigame(MinigameProperties properties) {
		super(properties);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.content.minigames.MinigameBase#process()
	 */
	@Override
	public void process() {
		super.process();
		if (lobbyArea != null) {
			lobbyArea.tick();
		}
		if (waitingArea != null) {
			waitingArea.tick();
		}
		if (gameArea != null) {
			gameArea.tick();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.content.minigames.MinigameBase#enter(net.zaros.server.
	 * game.node.entity.player.Player,
	 * net.zaros.server.game.content.minigames.util.JoinLeaveOptions[])
	 */
	@Override
	public boolean enter(Player player, JoinLeaveOptions... options) {
		if (!lobbyArea.tryJoin(player, options)) {
			return false;
		}
		player.getManager().getActivities().startActivity(new MinigameActivity(this));
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.content.minigames.MinigameBase#exit(net.zaros.server.
	 * game.node.entity.player.Player)
	 */
	@Override
	public boolean exit(Player player) {
		boolean done = false;
		if (gameArea.contains(player)) {
			gameArea.tryLeave(player, JoinLeaveOptions.EFFECTLESS);
			done = true;
		}
		if (waitingArea.contains(player)) {
			waitingArea.tryLeave(player, JoinLeaveOptions.EFFECTLESS);
			done = true;
		}
		if (!done || lobbyArea.contains(player)) {
			lobbyArea.tryLeave(player, JoinLeaveOptions.FORCE);
		}
		player.getManager().getActivities().end();
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.content.minigames.MinigameBase#handlePostDisconnect(net
	 * .zaros.server.game.node.entity.player.Player)
	 */
	@Override
	public void handlePostDisconnect(Player player) {
		if (gameArea.contains(player)) {
			gameArea.tryLeave(player, JoinLeaveOptions.DONT_MOVE);
		}
		if (waitingArea.contains(player)) {
			waitingArea.tryLeave(player, JoinLeaveOptions.DONT_MOVE);
		}
		player.getManager().getActivities().end();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.content.minigames.MinigameBase#getProperties()
	 */
	@Override
	public MultiareaProperties getProperties() {
		return (MultiareaProperties) super.getProperties();
	}
}
