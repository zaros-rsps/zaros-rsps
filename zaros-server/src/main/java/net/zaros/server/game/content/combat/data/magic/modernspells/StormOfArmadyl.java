package net.zaros.server.game.content.combat.data.magic.modernspells;

import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitModifier;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

import java.util.List;

/**
 * The storm of armadyl spell on the regular spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class StormOfArmadyl extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 99;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.ARMADYL_RUNE));

    @Override
    public void playGraphics(Entity source, Entity target) {
        source.sendGraphics(457);
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(10546);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
        // ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(source, target, 1019, 0, 0, 52, 0, 0));
    }

    @Override
    public HitModifier getModifier(Entity source, Entity target) {
        return null;
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return null;
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(1019);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int autocastConfigID() {
        return 145;
    }

    @Override
    public int magicLevelRequirement() {
        return 77;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 160 + minimumHit(entity);
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 42.5;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.REGULAR;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }

    @Override
    public int minimumHit(Entity entity) {
        int start = 77;
        int level = entity.getSkills().getLevelForXp(SkillConstants.MAGIC);
        int difference = level - start;
        return (difference / 2) * 10;
    }
}
