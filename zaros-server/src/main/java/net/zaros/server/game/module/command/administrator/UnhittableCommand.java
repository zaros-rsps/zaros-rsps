package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/9/2017
 */
@CommandManifest(description = "Toggles whether u can be hit.")
public class UnhittableCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("togglehittable");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		player.putTemporaryAttribute("unhittable", !player.getTemporaryAttribute("unhittable", false));
		player.getTransmitter().sendMessage("You are now " + (player.getTemporaryAttribute("unhittable", false) ? "un" : "") + "hittable.");
	}
}
