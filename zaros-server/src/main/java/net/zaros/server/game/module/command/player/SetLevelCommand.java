package net.zaros.server.game.module.command.player;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.Skills;
import net.zaros.server.game.node.entity.player.render.flag.impl.AppearanceUpdate;
import net.zaros.server.utility.rs.constant.SkillConstants;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/7/2017
 */
@CommandManifest(description = "Sets the level of a skill", types = { String.class, Integer.class })
public class SetLevelCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("setlevel");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		String skillId = stringParamOrDefault(args, 1, "");
		int level = intParam(args, 2);
		if (level <= 0) {
			level = 1;
		} else if (level > 99) {
			level = 99;
		}
		int exp = Skills.getXPForLevel(level);
		for(int skill = 0; skill < SkillConstants.SKILL_NAME.length; skill++) {
			String name = SkillConstants.SKILL_NAME[skill];
			if(name.equalsIgnoreCase(skillId)) {
				player.getSkills().setLevel(skill, level);
				player.getSkills().setXp(skill, exp);
				player.getUpdateMasks().register(new AppearanceUpdate(player));
				return;
			}
		}
	}
}
