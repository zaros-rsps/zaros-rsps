package net.zaros.server.game;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/18/2017
 */
public interface GameConstants {

	/**
	 * The path of the z-store
	 */
	String Z_STORE_PATH = "./data/repository/z-store/";

	/**
	 * The key used for player-file encryption
	 */
	String FILE_ENCRYPTION_KEY = "MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgFoPEhAf5C/G1AsCURrAXBAKKxLV";

	/**
	 * The path to the file with sql configuration
	 */
	String SQL_CONFIGURATION_FILE = "./data/sql.conf";

	/**
	 * The id of the pvp world
	 */
	int PVP_WORLD_ID = 2;

	/**
	 * The url that the players can change their email at
	 */
	String EMAIL_MODIFICATION_URL = "http://redrune.org/settings/email/";

	/**
	 * The url that players can see their inbox at
	 */
	String INBOX_URL = "http://redrune.org/messenger/";

	/**
	 * The url that players can donate at
	 */
	String DONATION_URL = "http://redrune.org/donate";
}
