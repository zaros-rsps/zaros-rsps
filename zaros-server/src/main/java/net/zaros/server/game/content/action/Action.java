package net.zaros.server.game.content.action;

import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;

/**
 * This interface provides methods to handle an action that is processed at a continuous tick rate.
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/29/2017
 */
public interface Action {
	
	/**
	 * Handles the start of the event
	 *
	 * @param entity
	 * 		The Entity
	 * @return True if we can start
	 */
	boolean start(Entity entity);
	
	/**
	 * Handles the processing of the action
	 *
	 * @param entity
	 * 		The Entity
	 * @return True if we process successfully
	 */
	boolean process(Entity entity);
	
	/**
	 * Separately handles processing the action in a timely (ticks) fashion
	 *
	 * @param entity
	 * 		The Entity
	 * @return -1 if we should stop
	 */
	int processOnTicks(Entity entity);
	
	/**
	 * Handles what to do when the event is stopped
	 *
	 * @param entity
	 * 		The Entity
	 */
	void stop(Entity entity);
	
	/**
	 * Sets the delay
	 *
	 * @param entity
	 * 		The Entity
	 * @param delay
	 * 		The delay
	 */
	default void setDelay(Entity entity, int delay) {
		if(entity instanceof Player)
			((Player) entity).getManager().getActions().addDelay(delay);
	}

	default int getDelay(Entity entity) {
		if(entity instanceof Player)
			return ((Player) entity).getManager().getActions().getDelay();
		return -1;
	}
	
}
