package net.zaros.server.game.content.combat.data.magic.ancientspells;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.effects.impl.poison.PoisonType;
import net.zaros.server.game.node.entity.hit.*;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;
import net.zaros.server.utility.tool.RandomFunction;

import java.util.List;

/**
 * The smoke burst spell on the ancient spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class SmokeBurst extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 30;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.FIRE_RUNE1, 2), new Item(Items.AIR_RUNE1, 2), new Item(Items.CHAOS_RUNE1 , 4), new Item(Items.DEATH_RUNE1, 2));

    @Override
    public void playGraphics(Entity source, Entity target) {
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(1979);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return new HitEffect(HitEffectType.POISON, HitInstant.CALCULATION, PoisonType.WEAK.getDamage());
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(389);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int autocastConfigID() {
        return 71;
    }

    @Override
    public int magicLevelRequirement() {
        return 62;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 190;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 30;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.ANCIENTS;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }

    @Override
    public boolean multi() {
        return true;
    }
}
