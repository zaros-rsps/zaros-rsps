package net.zaros.server.game.module.interaction.object;

import net.zaros.server.game.content.dialogue.impl.misc.BookSwapDialogue;
import net.zaros.server.game.module.type.ObjectInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/9/2017
 */
public class BookAltarInteractionModule extends ObjectInteractionModule {

	public BookAltarInteractionModule() {
		register(53739);
	}

	@Override
	public boolean handle(Player player, GameObject object, InteractionOption option) {
		player.getManager().getDialogues().startDialogue(new BookSwapDialogue());
		return true;
	}
}
