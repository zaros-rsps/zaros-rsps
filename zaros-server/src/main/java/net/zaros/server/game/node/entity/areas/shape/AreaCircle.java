package net.zaros.server.game.node.entity.areas.shape;

import com.google.common.base.Preconditions;

import net.zaros.server.game.node.Location;

/**
 * Represents a circular area shape.
 * 
 * @author Walied K. Yassen
 */
public final class AreaCircle implements AreaShape {

	/**
	 * The centre point x component.
	 */
	private final int center_x;

	/**
	 * The centre point y component.
	 */
	private final int center_y;

	/**
	 * The circle radius squared.
	 */
	private final int radius_sq;

	/**
	 * Constructs a new {@link AreaCircle} type object instance.
	 * 
	 * @param location
	 *                 the centre location point.
	 * @param radius
	 *                 the circle radius.
	 */
	public AreaCircle(Location location, int radius) {
		this(location.getX(), location.getY(), radius);
	}

	/**
	 * Constructs a new {@link AreaCircle} type object instance.
	 * 
	 * @param center_x
	 *                 the centre point x component.
	 * @param center_y
	 *                 the centre point y component.
	 * @param radius
	 *                 the circle radius.
	 */
	public AreaCircle(int center_x, int center_y, int radius) {
		Preconditions.checkArgument(radius > 0, "the circle radius must be greater than zero.");
		this.center_x = center_x;
		this.center_y = center_y;
		radius_sq = radius * radius;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.node.entity.areas.shape.AreaShape#intersects(int,
	 * int)
	 */
	@Override
	public boolean intersects(int x, int y) {
		var x_diff = x - center_x;
		var y_diff = y - center_y;
		return x_diff * x_diff + y_diff * y_diff < radius_sq;
	}
}
