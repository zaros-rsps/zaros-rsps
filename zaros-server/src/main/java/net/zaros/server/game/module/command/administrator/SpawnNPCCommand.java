package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.utility.rs.constant.Directions.Direction;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/31/2017
 */
@CommandManifest(description = "Spawns an npc by its id", types = { Integer.class })
public class SpawnNPCCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("npc");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		NPC npc = World.addNPC(intParam(args, 1), player.getLocation(), Direction.NORTH);
		npc.setRespawnable(false);
	}
}
