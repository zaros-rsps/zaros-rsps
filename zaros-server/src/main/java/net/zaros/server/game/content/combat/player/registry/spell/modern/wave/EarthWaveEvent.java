package net.zaros.server.game.content.combat.player.registry.spell.modern.wave;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.player.registry.wrapper.context.CombatSpellContext;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.CombatSpellEvent;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.constant.MagicConstants.MagicBook;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/7/2017
 */
public class EarthWaveEvent implements CombatSpellEvent{
	
	@Override
	public int delay(Player player) {
		return 5;
	}
	
	@Override
	public int animationId() {
		return 14222;
	}
	
	@Override
	public int hitGfx() {
		return 2726;
	}
	
	@Override
	public int maxHit(Player player, Entity target) {
		return 190;
	}
	
	@Override
	public int spellId() {
		return 77;
	}
	
	@Override
	public double exp() {
		return 42.5;
	}
	
	@Override
	public MagicBook book() {
		return MagicBook.REGULAR;
	}
	
	@Override
	public void cast(Player player, CombatSpellContext context) {
		player.sendGraphics(2716);
		ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(player, context.getTarget(), 2721, 30, 26, 52, 0, 0));
		context.getSwing().sendSpell(player, context.getTarget(), this);
	}
}
