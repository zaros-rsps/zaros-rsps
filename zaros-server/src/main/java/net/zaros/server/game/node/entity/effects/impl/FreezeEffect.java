package net.zaros.server.game.node.entity.effects.impl;

import lombok.Getter;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.effects.Duration;
import net.zaros.server.game.node.entity.effects.Effect;
import net.zaros.server.game.node.entity.effects.WalkPolicy;
import net.zaros.server.utility.tool.SecondsTimer;

/**
 * The effect for freezing entities.
 *
 * @author Gabriel || Wolfsdarker
 */
public class FreezeEffect implements Effect {

    /**
     * The effect's name.
     */
    @Getter
    private static final String name = "freeze";

    /**
     * The entity who is receiving the effect.
     */
    private Entity entity;

    /**
     * The entity that is applying the effect.
     */
    private Entity source;

    /**
     * The effect's timer.
     */
    private SecondsTimer timer;

    /**
     * Constructor for the freeze effect.
     *
     * @param entity
     * @param source
     * @param seconds
     */
    public FreezeEffect(Entity entity, Entity source, int seconds) {
        this.entity = entity;
        this.source = source;
        this.timer = new SecondsTimer();
        this.timer.start(seconds);
    }

    @Override
    public boolean over() {
        return timer.finished();
    }

    @Override
    public boolean renewable() {
        return false;
    }

    @Override
    public boolean immune(Entity entity) {
        return !entity.getEffectManager().getFreezeImmunityTimer().finished();
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String nonWalkableMessage() {
        return "A magical force prevents you from moving.";
    }

    @Override
    public WalkPolicy walkPolicy() {
        return WalkPolicy.NON_WALKABLE;
    }

    @Override
    public Duration duration() {
        return Duration.TEMPORARY;
    }

    @Override
    public void preExecution(boolean isLogin) {
        if (isLogin) {
            return;
        }

        entity.getEffectManager().getFreezeImmunityTimer().start(timer.secondsRemaining() + 3);

        if (entity.isPlayer()) {
            entity.toPlayer().getTransmitter().sendMessage("<col=990000>You have been frozen!</col>");
        }
    }

    @Override
    public void onTick() {

        if (source == entity) {
            return;
        }

        if (source.isDead() || source.getHealthPoints() < 1 || source.getLocation().getDistance(entity.getLocation()) > 15) {
            timer.stop();
            entity.getEffectManager().getFreezeImmunityTimer().stop();
        }
    }

    @Override
    public void renew() {
    }

    @Override
    public void end() {
    }
}
