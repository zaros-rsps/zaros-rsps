package net.zaros.server.game.module.interaction.rsinterface;

import net.zaros.server.game.module.type.InterfaceInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.link.slayer.SlayerManager;

public class SlayerRewardsInterfaceModule implements InterfaceInteractionModule {

    @Override
    public int[] interfaceSubscriptionIds() {
        return arguments(161, 163, 164);
    }

    @Override
    public boolean handle(Player player, int interfaceId, int componentId, int itemId, int slotId, int packetId) {
        SlayerManager slayer = player.getManager().getSlayer();
        switch (interfaceId) {
            case 164:
                switch(componentId) {
                    case 16:
                        player.getManager().getInterfaces().sendInterface(163, true);
                        slayer.refreshSlayerRewardInterface(player, 163);
                        break;
                    case 17:
                        player.getManager().getInterfaces().sendInterface(161, true);
                        slayer.refreshSlayerRewardInterface(player, 161);
                        break;
                        default:
                            player.getManager().getSlayer().purchaseReward(player, interfaceId, componentId);
                            break;
                }
                break;
            case 163:
                switch(componentId) {
                    case 14:
                        player.getManager().getInterfaces().sendInterface(161, true);
                        slayer.refreshSlayerRewardInterface(player, 161);
                        break;
                    case 15:
                        player.getManager().getInterfaces().sendInterface(164, true);
                        slayer.refreshSlayerRewardInterface(player, 164);
                        break;
                    default:
                        player.getManager().getSlayer().purchaseReward(player, interfaceId, componentId);
                        break;
                }
                break;
            case 161:
                switch(componentId) {
                    case 14:
                        player.getManager().getInterfaces().sendInterface(163, true);
                        slayer.refreshSlayerRewardInterface(player, 163);
                        break;
                    case 15:
                        player.getManager().getInterfaces().sendInterface(164, true);
                        slayer.refreshSlayerRewardInterface(player, 164);
                        break;
                    default:
                        player.getManager().getSlayer().purchaseReward(player, interfaceId, componentId);
                        break;
                }
                break;
        }
        return true;
    }
}
