package net.zaros.server.game.node.entity.data.weapon;

import lombok.Getter;
import net.zaros.server.utility.rs.constant.BonusConstants;

/**
 * The types of attack a weapon has.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum AttackType {

    LONG_ACCURATE(426, BonusConstants.CRUSH_ATTACK, AttackStyle.ACCURATE, 9, 4),
    LONG_RAPID(426, BonusConstants.CRUSH_ATTACK, AttackStyle.AGGRESSIVE, 9, 3),
    LONG_LONGRANGE(426, BonusConstants.CRUSH_ATTACK, AttackStyle.DEFENSIVE, 11, 4),

    SHORTBOW_ACCURATE(426, BonusConstants.CRUSH_ATTACK, AttackStyle.ACCURATE, 8, 4),
    SHORTBOW_RAPID(426, BonusConstants.CRUSH_ATTACK, AttackStyle.AGGRESSIVE, 8, 3),
    SHORTBOW_LONGRANGE(426, BonusConstants.CRUSH_ATTACK, AttackStyle.DEFENSIVE, 10, 4),

    DAGGER_STAB(400, BonusConstants.STAB_ATTACK, AttackStyle.ACCURATE, 4),
    DAGGER_LUNGE(400, BonusConstants.STAB_ATTACK, AttackStyle.AGGRESSIVE, 4),
    DAGGER_SLASH(400, BonusConstants.STAB_ATTACK, AttackStyle.AGGRESSIVE, 4),
    DAGGER_BLOCK(400, BonusConstants.STAB_ATTACK, AttackStyle.DEFENSIVE, 4),

    DRAGON_DAGGER_STAB(376, BonusConstants.STAB_ATTACK, AttackStyle.ACCURATE, 4),
    DRAGON_DAGGER_LUNGE(376, BonusConstants.STAB_ATTACK, AttackStyle.AGGRESSIVE, 4),
    DRAGON_DAGGER_SLASH(377, BonusConstants.STAB_ATTACK, AttackStyle.AGGRESSIVE, 4),
    DRAGON_DAGGER_BLOCK(376, BonusConstants.STAB_ATTACK, AttackStyle.DEFENSIVE, 4),

    HALBERD_JAB(440, BonusConstants.STAB_ATTACK, AttackStyle.CONTROLLED, 2, 7),
    HALBERD_SWIPE(440, BonusConstants.SLASH_ATTACK, AttackStyle.AGGRESSIVE, 2, 7),
    HALBERD_FEND(440, BonusConstants.STAB_ATTACK, AttackStyle.DEFENSIVE, 2, 7),

    LONGSWORD_STAB(12311, BonusConstants.STAB_ATTACK, AttackStyle.ACCURATE, 4),
    LONGSWORD_LUNGE(12311, BonusConstants.STAB_ATTACK, AttackStyle.AGGRESSIVE, 4),
    LONGSWORD_SLASH(12310, BonusConstants.SLASH_ATTACK, AttackStyle.AGGRESSIVE, 4),
    LONGSWORD_BLOCK(12311, BonusConstants.STAB_ATTACK, AttackStyle.DEFENSIVE, 4),

    MACE_POUND(401, BonusConstants.CRUSH_ATTACK, AttackStyle.ACCURATE, 5),
    MACE_PUMMEL(401, BonusConstants.CRUSH_ATTACK, AttackStyle.AGGRESSIVE, 5),
    MACE_SPIKE(401, BonusConstants.STAB_ATTACK, AttackStyle.CONTROLLED, 5),
    MACE_BLOCK(401, BonusConstants.CRUSH_ATTACK, AttackStyle.DEFENSIVE, 5),

    PICKAXE_SPIKE(401, BonusConstants.STAB_ATTACK, AttackStyle.ACCURATE, 5),
    PICKAXE_IMPALE(401, BonusConstants.STAB_ATTACK, AttackStyle.AGGRESSIVE, 5),
    PICKAXE_SMASH(401, BonusConstants.CRUSH_ATTACK, AttackStyle.AGGRESSIVE, 5),
    PICKAXE_BLOCK(400, BonusConstants.STAB_ATTACK, AttackStyle.DEFENSIVE, 5),

    SCIMITAR_CHOP(15071, BonusConstants.SLASH_ATTACK, AttackStyle.ACCURATE, 4),
    SCIMITAR_SLASH(15071, BonusConstants.SLASH_ATTACK, AttackStyle.AGGRESSIVE, 4),
    SCIMITAR_LUNGE(15072, BonusConstants.STAB_ATTACK, AttackStyle.CONTROLLED, 4),
    SCIMITAR_BLOCK(15071, BonusConstants.SLASH_ATTACK, AttackStyle.DEFENSIVE, 4),

    SPEAR_LUNGE(2080, BonusConstants.STAB_ATTACK, AttackStyle.CONTROLLED, 5),
    SPEAR_SWIPE(2081, BonusConstants.SLASH_ATTACK, AttackStyle.CONTROLLED, 5),
    SPEAR_POUND(2082, BonusConstants.CRUSH_ATTACK, AttackStyle.CONTROLLED, 5),
    SPEAR_BLOCK(2080, BonusConstants.STAB_ATTACK, AttackStyle.DEFENSIVE, 5),

    SWORD_STAB(12311, BonusConstants.STAB_ATTACK, AttackStyle.ACCURATE, 4),
    SWORD_LUNGE(12310, BonusConstants.STAB_ATTACK, AttackStyle.AGGRESSIVE, 4),
    SWORD_SLASH(12311, BonusConstants.SLASH_ATTACK, AttackStyle.AGGRESSIVE, 4),
    SWORD_BLOCK(12311, BonusConstants.STAB_ATTACK, AttackStyle.DEFENSIVE, 4),

    ZAMORAKIAN_SPEAR_LUNGE(12006, BonusConstants.STAB_ATTACK, AttackStyle.CONTROLLED, 6),
    ZAMORAKIAN_SPEAR_SWIPE(12005, BonusConstants.SLASH_ATTACK, AttackStyle.CONTROLLED, 6),
    ZAMORAKIAN_SPEAR_POUND(12009, BonusConstants.CRUSH_ATTACK, AttackStyle.CONTROLLED, 6),
    ZAMORAKIAN_SPEAR_BLOCK(12006, BonusConstants.STAB_ATTACK, AttackStyle.DEFENSIVE, 6),

    UNARMED_PUNCH(422, BonusConstants.CRUSH_ATTACK, AttackStyle.ACCURATE, 4),
    UNARMED_KICK(423, BonusConstants.CRUSH_ATTACK, AttackStyle.AGGRESSIVE, 4),
    UNARMED_BLOCK(422, BonusConstants.CRUSH_ATTACK, AttackStyle.DEFENSIVE, 4),

    TWOHANDED_SWORD_CHOP(7041, BonusConstants.SLASH_ATTACK, AttackStyle.ACCURATE, 6),
    TWOHANDED_SWORD_SLASH(7041, BonusConstants.SLASH_ATTACK, AttackStyle.AGGRESSIVE, 6),
    TWOHANDED_SWORD_SMASH(7048, BonusConstants.CRUSH_ATTACK, AttackStyle.AGGRESSIVE, 6),
    TWOHANDED_SWORD_BLOCK(7049, BonusConstants.SLASH_ATTACK, AttackStyle.DEFENSIVE, 6),

    WARHAMMER_POUND(401, BonusConstants.CRUSH_ATTACK, AttackStyle.ACCURATE, 6),
    WARHAMMER_PUMMEL(401, BonusConstants.CRUSH_ATTACK, AttackStyle.AGGRESSIVE, 6),
    WARHAMMER_BLOCK(401, BonusConstants.CRUSH_ATTACK, AttackStyle.DEFENSIVE, 6),

    WHIP_FLICK(11968, BonusConstants.SLASH_ATTACK, AttackStyle.ACCURATE, 4),
    WHIP_LASH(11969, BonusConstants.SLASH_ATTACK, AttackStyle.CONTROLLED, 4),
    WHIP_DEFLECT(11970, BonusConstants.SLASH_ATTACK, AttackStyle.DEFENSIVE, 4);

    /**
     * The attack's animation id.
     */
    @Getter
    int animationId;

    /**
     * The attack's bonus style.
     */
    @Getter
    int bonusStyle;

    /**
     * The attack's style.
     */
    @Getter
    AttackStyle attackStyle;

    /**
     * The required distance to use the attack.
     */
    @Getter
    int attackDistance;

    /**
     * The time between each attack.
     */
    @Getter
    int attackSpeed;

    /**
     * Constructor for the attack type.
     *
     * @param animationId
     * @param bonusStyle
     * @param attackStyle
     * @param attackDistance
     * @param attackSpeed
     */
    AttackType(int animationId, int bonusStyle, AttackStyle attackStyle, int attackDistance, int attackSpeed) {
        this.animationId = animationId;
        this.bonusStyle = bonusStyle;
        this.attackStyle = attackStyle;
        this.attackDistance = attackDistance;
        this.attackSpeed = attackSpeed;
    }

    /**
     * Constructor for the attack type.
     *
     * @param animationId
     * @param bonusStyle
     * @param attackStyle
     * @param attackSpeed
     */
    AttackType(int animationId, int bonusStyle, AttackStyle attackStyle, int attackSpeed) {
        this.animationId = animationId;
        this.bonusStyle = bonusStyle;
        this.attackStyle = attackStyle;
        this.attackDistance = 1;
        this.attackSpeed = attackSpeed;
    }


}
