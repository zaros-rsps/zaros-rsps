package net.zaros.server.game.content.system.zskillsystem.farming.actions;

import net.zaros.cache.parse.ItemDefinitionParser;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.DiseaseState;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.FarmingConstants;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.PatchState;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.PatchTreatment;
import net.zaros.server.game.node.entity.player.Player;

/**
 * Handles the inspection action.
 *
 * @author Gabriel || Wolfsdarker
 */
public class InspectionAction extends PlayerTask {

	/**
	 * Constructor for the action.
	 *
	 * @param player
	 * @param state
	 */
	public InspectionAction(Player player, PatchState state) {
		super(1, player, () -> {

			if(state == null) {
				player.endCurrentTask();
				return;
			}

			if (!state.isUsed()) {
				if (state.getWeedStage() < 3) {
					player.getTransmitter().sendMessage("The patch needs weeding.");
				} else {
					player.getTransmitter().sendMessage("The patch is empty and weeded.");
				}
				player.endCurrentTask();
				return;
			}

			if (state.getTreatment() != PatchTreatment.NOT_TREATED) {
				player.getTransmitter().sendMessage("The soil has been treated with " + ItemDefinitionParser.forId(state.getTreatment().getItemId())
					.getName().toLowerCase() + ".");
			} else {
				player.getTransmitter().sendMessage("The soil has not been treated.");
			}

			if (FarmingConstants.isFullyGrown(state)) {
				player.getTransmitter().sendMessage("The patch is fully grown.");
			} else if (state.getDiseaseState() == DiseaseState.PRESENT) {
				player.getTransmitter().sendMessage("The patch is diseased and needs attending before it dies.");
			} else if (state.isDead()) {
				player.getTransmitter().sendMessage("The patch has become infected by disease and died.");
			} else if (state.isWatered()) {
				player.getTransmitter().sendMessage("The patch is watered and has something growing in it.");
			} else {
				player.getTransmitter().sendMessage("The patch has something growing in it.");
			}

			player.endCurrentTask();
		});

		onEnd(() -> {
			player.getMovementQueue().reset();
		});

		stopUpon(MovementPacketListener.class);
	}


}
