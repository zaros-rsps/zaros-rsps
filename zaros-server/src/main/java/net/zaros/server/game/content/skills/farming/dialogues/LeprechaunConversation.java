package net.zaros.server.game.content.system.zskillsystem.farming.dialogues;

import com.ahoypk.definitions.ItemDefinition;
import com.ahoypk.engine.task.Task;
import com.ahoypk.engine.task.TaskManager;
import com.ahoypk.world.content.conversation.Conversation;
import com.ahoypk.world.content.skills.impl.Herblore;
import com.ahoypk.world.content.skills.impl.farming.impl.Seeds;
import com.ahoypk.world.content.skills.impl.farming.impl.ToolLeprechaun;
import com.ahoypk.world.entity.impl.npc.NPC;
import com.ahoypk.world.entity.impl.player.Player;

public class LeprechaunConversation extends Conversation {

	public static final int INVENTORY_INTERFACE_ID = 15593;

	public static final int TOOLS_INTERFACE_ID = 15614;

	private NPC leprechaun;

	private int item_id;

	public LeprechaunConversation(Player player, NPC leprechaun) {
		super(player);
		this.leprechaun = leprechaun;
	}

	public LeprechaunConversation(Player player, NPC leprechaun, int item_id) {
		super(player);
		this.leprechaun = leprechaun;
		this.item_id = item_id;
	}

	@Override
	public void init() {

		if (item_id > 0) {
			noteItems();
		} else {
			npc(leprechaun, "Ah, 'tis a foine day to be sure! Were yez wantin' me to", "store yer tools, or maybe ye might be wantin' yer stuff",
				"back from me?");

			options("Yes please.", this::openStorage, "What can you store?", this::whatCanYouStore, "What do you do with the tools you're storing?",
				this::storageOfTools, "No thanks, I'll keep hold of my stuff.", this::noThanks);
		}
	}

	private void openStorage() {
		TaskManager.submit(new Task() {
			@Override
			protected void execute() {
				ToolLeprechaun.open(player);
				this.stop();
			}
		});
	}

	/**
	 * Handles the option to ask the leprachaun what he can store.
	 */
	private void whatCanYouStore() {
		player("What can you store?");

		npc(leprechaun, "We'll hold onto yer rake, yer seed dibber, yer spade,", "yer secateurs, yer waterin' can and yer trowel - but",
			"mind it's not one of them fancy trowels only archaeologists use!");

		npc(leprechaun, "We'll take a few buckets off yer hands too, and even", "yer compost, supercompost an' ultracompost! Also plant",
			"cure vials.");

		npc(leprechaun, "Aside from that, if ye hands me yer farming produce, I", "can mebbe change it into banknotes for ye.",
			"cure vials.");

		npc(leprechaun, "So... do ye want to be using the store?");

		options("Yes please.", this::openStorage, "What do you do with the tools you're storing?",
			this::storageOfTools, "No thanks, I'll keep hold of my stuff.", this::noThanks);
	}

	/**
	 * Handles the option to ask how the leprachaun stores all the tools.
	 */
	private void storageOfTools() {
		player("What do you do with the tools you're storing?", "They can't possibly all fit in your pockets!");

		npc(leprechaun, "We leprechauns have a shed where we keep 'em. It's a", "magic shed, so ye can get yer items back from any of",
			"us leprechauns whenever ye want. Saves ye havin' to", "carry loads of stuff around the country!");

		npc(leprechaun, "So... do ye want to be using the store?");

		options("Yes please.", this::openStorage, "What do you do with the tools you're storing?",
			this::storageOfTools, "No thanks, I'll keep hold of my stuff.", this::noThanks);
	}

	/**
	 * Handles the no thanks option to end dialogue.
	 */
	private void noThanks() {
		player("No thanks, I'll keep hold of my stuff.");

		npc(leprechaun, "Ye must be dafter than ye look if ye likes luggin' yer tools everywhere ye goes!");
	}

	/**
	 * Dialogue to note items.
	 */
	private void noteItems() {

		if (Seeds.forProductID(item_id) == null && !Herblore.Herb.forCleanId(item_id).isPresent()) {
			return;
		}

		int noted_id = ItemDefinition.forID(item_id).getNoteID();

		if (noted_id == -1) {
			item(item_id, "The leprechaun can't note this item.");
			return;
		}

		int amount = player.getInventory().getAmount(item_id);

		player.getInventory().delete(item_id, amount);
		player.getInventory().add(noted_id, amount);
		item(item_id, "The leprechaun exchanges your items for banknotes.");
	}

}
