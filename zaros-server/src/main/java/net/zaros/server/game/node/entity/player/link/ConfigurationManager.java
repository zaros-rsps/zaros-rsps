package net.zaros.server.game.node.entity.player.link;

import lombok.Getter;
import lombok.Setter;
import net.zaros.cache.type.varbit.VarBitDefinition;
import net.zaros.cache.type.varbit.VarBitDefinitionParser;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigFilePacketBuilder;

public final class ConfigurationManager {

    /**
     * The amount of configurations.
     */
    public transient static final int SIZE = 2200;

    /**
     * The player.
     */
    @Getter
    @Setter
    private transient Player player;

    /**
     * The configurations.
     */
    private transient final int[] configurations = new int[SIZE];

    /**
     * The configurations.
     */
    private transient final int[] savedConfigurations = new int[SIZE];

/*    @Override
    public void save(ByteBuffer buffer) {
        for (int index = 0; index < savedConfigurations.length; index++) {
            int value = savedConfigurations[index];
            if (value != 0) {
                buffer.putShort((short) index);
                buffer.putInt(value);
            }
        }
        buffer.putShort((short) -1);
    }*/

   /* @Override
    public void parse(ByteBuffer buffer) {
        int index = 0;
        while ((index = buffer.getShort()) != -1) {
            savedConfigurations[index] = buffer.getInt();
        }
    }

    *//**
     * Initializes the configurations.
     *//*
    public void init() {
        for (int i = 0; i < savedConfigurations.length; i++) {
            int value = savedConfigurations[i];
            if (value != 0) {
                set(i, value, false);
            }
        }
    }*/

    /**
     * Resets the configurations.
     */
    public void reset() {
        for (int i = 0; i < configurations.length; i++) {
            configurations[i] = 0;
        }
    }

    /**
     * Sets a configuration.
     *
     * @param config The configuration.
     * @param value  The value.
     */
    public void set(Configuration config, boolean value) {
        set(config.id, value);
    }

    /**
     * Sets a configuration.
     *
     * @param config The configuration.
     * @param value  The value.
     */
    public void set(int id, boolean value) {
        set(id, value ? 1 : 0);
    }

    /**
     * Sets a configuration.
     *
     * @param config The configuration.
     * @param value  The value.
     */
    public void set(Configuration config, int value) {
        set(config, value, false);
    }

    /**
     * Sets the configuration.
     *
     * @param config The configuration id.
     * @param value  The value.
     * @param saved  If the configuration should be saved.
     */
    public void set(Configuration config, int value, boolean saved) {
        set(config.id, value, saved);
    }

    /**
     * Sets a configuration.
     *
     * @param id    The configuration id.
     * @param value The value.
     */
    public void set(int id, int value) {
        set(id, value, false);
    }

    /**
     * Sets a configuration for a set amount of time.
     *
     * @param id    the id.
     * @param value the value.
     * @param delay the delay.
     */
    public void set(final int id, final int value, int delay) {
        set(id, value);
        /*GameWorld.submit(new Pulse(delay, player) {
            @Override
            public boolean pulse() {
                set(id, 0);
                return true;
            }
        });*/
    }

    /**
     * Sets a configuration.
     *
     * @param id    The configuration id.
     * @param value The value.
     */
    public void set(int id, int value, boolean saved) {
        if (configurations[id] != value) {
            player.getTransmitter().send(new ConfigFilePacketBuilder(id, configurations[id] = value).build(player));
        }
        if (saved) {
            savedConfigurations[id] = value;
        }
    }

    /**
     * Sends the configuration without caching.
     */
    @Deprecated
    public void send(int id, int value) {
        player.getTransmitter().send(new ConfigFilePacketBuilder(id, configurations[id] = value).build(player));
    }


    public void sendGlobal(int id, int value) {
       // PacketRepository.send(GlobalConfig.class, new GlobalConfigContext(player, id, value));
    }

    public void sendGlobalString(int id, String value) {
       // PacketRepository.send(GlobalString.class, new GlobalStringContext(player, id, value));
    }

    /**
     * Gets the configuration value.
     *
     * @param id The config id.
     * @return The value.
     */
    public int get(int id) {
        return configurations[id];
    }

    public void sendVarbit(int id, int value) {
        if (id == -1) {
			return;
		}
        VarBitDefinition defs = VarBitDefinitionParser.forId(id);
        int mask = defs.getMask();
        if (value < 0 || value > mask) {
			value = 0;
		}
        mask <<= defs.getBitShift();
        int varpValue = configurations[defs.getId()] & (mask ^ 0xffffffff) | value << defs.getBitShift() & mask;
        set(defs.getId(), varpValue);
    }

    /**
     * Holds the configurations.
     *
     * @author Emperor
     */
    public enum Configuration {

        BRIGHTNESS(166), MUSIC_VOLUME(168), EFFECT_VOLUME(169), MOUSE_BUTTON(170), CHAT_EFFECT(171), RETALIATE(172), RUNNING(173), SPLIT_PRIVATE(287), ACCEPT_AID(427), PC_PORTALS(719), SURROUNDING_VOLUME(872), CLAN_WAR_DATA(1147),;

        /**
         * The config id.
         */
        private transient final int id;

        /**
         * Constructs a new {@code Configuration} {@code Object}.
         *
         * @param id The config id.
         */
        private Configuration(int id) {
            this.id = id;
        }

        /**
         * Gets the id.
         *
         * @return The id.
         */
        public int getId() {
            return id;
        }
    }
}