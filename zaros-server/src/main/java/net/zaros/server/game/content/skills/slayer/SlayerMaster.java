package net.zaros.server.game.content.skills.slayer;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.link.slayer.Task;
import net.zaros.server.utility.rs.constant.SkillConstants;

public enum SlayerMaster {
    TURAEL(8273, new int[] {15, 50}, new int[] {0, 0, 0}),
    MAZCHNA(8274, new int[] {30, 70}, new int[] {2, 5, 15}) {
        @Override
        public boolean hasRequirment(Player player) {
            return player.getSkills().getCombatLevel() >= 20;
        }
    },
    VANNAKA(1597, new int[] {30, 80}, new int[] {4, 20, 60}) {
        @Override
        public boolean hasRequirment(Player player) {
            return player.getSkills().getCombatLevel() >= 40;
        }
    },
    CHAELDAR(1598, new int[] {110, 170}, new int[] {10, 50, 150}) {
        @Override
        public boolean hasRequirment(Player player) {
            return player.getSkills().getCombatLevel() >= 70 && player.getSkills().getLevel(SkillConstants.SLAYER) >= 25;
        }
    },

    NIEVE(8649, new int[] {120, 185}, new int[] {12, 60, 180}) {
        @Override
        public boolean hasRequirment(Player player) {
            return player.getSkills().getCombatLevel() >= 85;
        }
    },

    DURADEL(8275, new int[] {50, 199}, new int[] {15, 75, 225}) {
        @Override
        public boolean hasRequirment(Player player) {
            return player.getSkills().getCombatLevel() >= 100 && player.getSkills().getLevel(SkillConstants.SLAYER) >= 50;
        }
    },
    WISE_OLD_MAN(3820, new int[] {20, 55}, new int[] {25, 90, 235}) {
        @Override
        public boolean hasRequirment(Player player) {
            return player.getSkills().getCombatLevel() >= 105 && player.getSkills().getLevel(SkillConstants.SLAYER) >= 75;
        }
    };

    /**
     * Represents the npc id.
     */
    private final int npc;

    /**
     * The ranges of task amts.
     */
    private final int[] ranges;

    /**
     * The points rewarded at task milestones.
     */
    private final int[] taskPoints;

    /**
     * Constructs a new {@Code SlayerMaster} {@Code Object}
     * @param npc The npc id.
     * @param ranges The ranges.
     * @param taskPoints The task points.
     */
    SlayerMaster(int npc, int[] ranges, int[] taskPoints) {
        this.npc = npc;
        this.ranges = ranges;
        this.taskPoints = taskPoints;
    }

    /**
     * Checks if the player has the requiremnts.
     * @param player the player.
     * @return {@code True} if so.
     */
    public boolean hasRequirment(Player player) {
        return true;
    }

    /**
     * Gets the npc.
     * @return The npc.
     */
    public int getNpc() {
        return npc;
    }

    /**
     * returns the value from the integer specification.
     * @param id the id.
     * @return @app value.
     */
    public static SlayerMaster forId(int id) {
        for (SlayerMaster slayerMaster : SlayerMaster.values()) {
            if (slayerMaster == null) {
                continue;
            }
            if (slayerMaster.getNpc() == id) {
                return slayerMaster;
            }
        }
        return null;
    }

    /**
     * Gets the ranges.
     * @return The ranges.
     */
    public int[] getRanges() {
        return ranges;
    }

    /**
     * Checks if two masters share the same task.
     * @param slayerMaster the slayerMaster.
     * @param mySlayerMaster the players slayerMaster.
     * @param player the player.
     * @return {@code True} if so.
     */
    public static boolean hasSameTask(SlayerMaster slayerMaster, SlayerMaster mySlayerMaster, Player player) {
        Task task = player.getManager().getSlayer().getTask();
        if (slayerMaster == mySlayerMaster) {
            return true;
        }
        if (task.hasMaster(slayerMaster)) {
            return true;
        }
        return false;
    }


    /**
     * Gets the taskPoints.
     * @return the taskPoints.
     */
    public int[] getTaskPoints() {
        return taskPoints;
    }

}
