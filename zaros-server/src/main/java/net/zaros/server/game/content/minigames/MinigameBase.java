/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames;

import lombok.Getter;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.TeleportType;
import net.zaros.server.game.content.minigames.util.JoinLeaveOptions;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.ButtonOption;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * Represents the minigame types base, each minigame type must be a sub-class of
 * this class.
 * 
 * @author Walied K. Yassen
 */
public abstract class MinigameBase {

	/**
	 * The game properties.
	 */
	@Getter
	protected final MinigameProperties properties;

	/**
	 * The current minigame tick.
	 */
	@Getter
	protected int tick;

	/**
	 * Construct a new {@link MinigameProperties} type object instance.
	 */
	public MinigameBase(MinigameProperties properties) {
		this.properties = properties;
		initialise();
	}

	/**
	 * Initialises the minigame instance.
	 */
	private final void initialise() {
		setup();
	}

	/**
	 * Set-ups the minigame implementation.
	 */
	public abstract void setup();

	/**
	 * Processes the logical side of the minigame, this method will be called once
	 * every tick. Any tick based procedures should be done within this method, it
	 * is synchronised to our world tick.
	 */
	public void process() {
		tick++;
		tick();
	}

	/**
	 * Processes the logical side of the minigame, this method will be called once
	 * every tick. Any tick based procedures should be done within this method, it
	 * is synchronised to our world tick.
	 * 
	 * @see #process()
	 */
	protected abstract void tick();

	/**
	 * Attempts to enter this {@link MinigameBase minigame} area.
	 * 
	 * @param player
	 *                the player who will do the attempt.
	 * @param options
	 *                the join or leave options.
	 * @return <code>true</code> if it was successful otherwise <code>false</code>.
	 */
	public abstract boolean enter(Player player, JoinLeaveOptions... options);

	/**
	 * Attempts to exit this {@link MinigameBase minigame} area.
	 * 
	 * @param player
	 *                the player who will do the attempt.
	 * @param options
	 *                the join or leave options.
	 * @return <code>true</code> if it was successful otherwise <code>false</code>.
	 */
	public abstract boolean exit(Player player);

	/**
	 * Handles the specified {@link Player} post disconnect event.
	 * 
	 * @param player
	 *               the player to handle their post disconnnect event.
	 */
	public abstract void handlePostDisconnect(Player player);

	/**
	 * Handles the specified {@link Player} pre-disconnect event for the minigame.
	 * 
	 * @param player
	 *               the player to handle their pre-disconnect event.
	 * @return <code>true</code> if the event was handled otherwise
	 *         <code>false</code>.
	 */
	public abstract boolean handlePreDisconnect(Player player);

	/**
	 * Handles the specified {@link Entity} pre-death event.
	 * 
	 * @param entity
	 *               the entity to handle the event for.
	 * @return <code>true</code> if the event was handled otherwise
	 *         <code>false</code>.
	 */
	public abstract boolean handlePreDeath(Entity entity);

	/**
	 * Handles the specified {@link Entity} post-death event.
	 * 
	 * @param entity
	 *               the entity to handle the event for.
	 * @return <code>true</code> if the event was handled otherwise
	 *         <code>false</code>.
	 */
	public abstract boolean handlePostDeath(Entity entity);

	/**
	 * Handles an interface button interaction option for the minigame.
	 * 
	 * @param player
	 *                    the player who did the interaction.
	 * @param interfaceId
	 *                    the interface id.
	 * @param buttonId
	 *                    the interface button id.
	 * @param slotId
	 *                    the button slot id.
	 * @param itemId
	 *                    the item attached to that button.
	 * @param option
	 *                    the option that was used for the interaction.
	 * @return <code>true</code> if it was handled otherwise <code>false</code>.
	 */
	public abstract boolean handleButton(Player player, int interfaceId, int buttonId, int slotId, int itemId, ButtonOption option);

	/**
	 * Handles an inventory item interaction option for the minigame.
	 * 
	 * @param player
	 *               the player who did the interaction.
	 * @param item
	 *               the item which we interacted with.
	 * @param slot
	 *               the option that was used for the interaction.
	 * @return whether the interaction was handled or not.
	 */
	public abstract boolean handleInventory(Player player, Item item, int slot, InteractionOption option);

	/**
	 * Handles a modal interface closing event for the minigame.
	 * 
	 * @param player
	 *                    TODO
	 * @param interfaceId
	 *                    the interface id which will be closed.
	 * @return <code>true</code> if the event was handled otherwise
	 *         <code>false</code>.
	 */
	public abstract boolean handleModalClosing(Player player, int interfaceId);

	/**
	 * Handles an object interaction option for the minigame.
	 * 
	 * @param player
	 *               the player who did the interaction.
	 * @param object
	 *               the object that we interacted with.
	 * @param option
	 *               the option that was used for the interaction.
	 * @return whether the interaction was handled or not.
	 */
	public abstract boolean handleObject(Player player, GameObject object, InteractionOption option);

	/**
	 * Handles an NPC interaction option for the minigame.
	 * 
	 * @param player
	 *               the player who did the interaction.
	 * @param npc
	 *               the NPC who we interacted with.
	 * @param option
	 *               the option that was used for the interaction.
	 * @return whether the interaction was handled or not.
	 */
	public abstract boolean handleNPC(Player player, NPC npc, InteractionOption option);

	/**
	 * Handles a player interaction event for the minigame.
	 * 
	 * @param player
	 *               the player who did the interaction.
	 * @param other
	 *               the other player who we interacted with.
	 * @param option
	 *               the option that was used for the interaction.
	 * @return <code>true</code> if the event was handled otherwise <a>.
	 */
	public abstract boolean handlePlayer(Player player, Player other, InteractionOption option);

	/**
	 * Checks whether or not the specified {@link TeleportType} is allowed.
	 * 
	 * @param player
	 *               player who is trying to teleport.
	 * @param type
	 *               the teleport type to check.
	 * @return <code>true</code> if it is otherwise <code>false</code>.
	 */
	public abstract boolean isTeleportAllowed(Player player, TeleportType type);

}
