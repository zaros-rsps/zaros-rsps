package net.zaros.server.game.node.entity.effects.impl.poison;

/**
 * Holds all of the different strengths of poisons.
 */
public enum PoisonType {
    WEAK(2),
    MILD(4),
    EXTRA(5),
    SUPER(6),
    KARAMB_POISON(12);

    /**
     * The starting damage for this poison type.
     */
    private int damage;

    /**
     * Create a new {@link PoisonType}.
     *
     * @param damage the starting damage for this poison type.
     */
    PoisonType(int damage) {
        this.damage = damage;
    }

    /**
     * Gets the starting damage for this poison type.
     *
     * @return the starting damage for this poison type.
     */
    public int getDamage() {
        return damage;
    }
}