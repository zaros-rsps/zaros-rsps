package net.zaros.server.game.world.punishment;

import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/17/2017
 */
public enum PunishmentType {
	
	MUTE {
		@Override
		public boolean add(Player player, Punishment punishment) {
			if (player.getVariables().addPunishment(punishment)) {
				player.getTransmitter().sendMessage("You have been muted.");
				return true;
			}
			return false;
		}
		
		@Override
		public boolean remove(Player player, Punishment punishment) {
			if (player.getVariables().removePunishment(punishment)) {
				player.getTransmitter().sendMessage("You have been unmuted.");
				return true;
			}
			return false;
		}
	},
	BAN {
		@Override
		public boolean add(Player player, Punishment punishment) {
			if (player.getVariables().addPunishment(punishment)) {
				//player.deregister();
				player.getTransmitter().sendLogout(false);
				return true;
			}
			return false;
		}
		
		@Override
		public boolean remove(Player player, Punishment punishment) {
			// we will only be unbanning players who are logged off
			if (player.getVariables().removePunishment(punishment)) {
				player.save();
				return true;
			}
			return false;
		}
	},
	ADDRESS_MUTE {
		@Override
		public boolean add(Player player, Punishment punishment) {
			return false;
		}
		
		@Override
		public boolean remove(Player player, Punishment punishment) {
			return false;
		}
	},
	ADDRESS_BAN {
		@Override
		public boolean add(Player player, Punishment punishment) {
			return false;
		}
		
		@Override
		public boolean remove(Player player, Punishment punishment) {
			return false;
		}
	};
	
	/**
	 * Handles the addition of a punishment to the world
	 * @param player
	 * 		The player
	 * @param punishment
	 * 		The punishment to add
	 */
	public abstract boolean add(Player player, Punishment punishment);
	
	/**
	 * Handles the removal of a punishment
	 * @param player
	 * 		The player
	 * @param punishment
	 * 		The punishment to add
	 */
	public abstract boolean remove(Player player, Punishment punishment);
}
