package net.zaros.server.game.content.combat.player.registry.range;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.player.registry.wrapper.BowFireEvent;
import net.zaros.server.game.content.combat.player.swing.RangeCombatSwing;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/22/2017
 */
public class CrystalBowEvent implements BowFireEvent {
	
	@Override
	public String[] bowNames() {
		return arguments("crystal bow");
	}
	
	@Override
	public void fire(Player attacker, Entity target, RangeCombatSwing swing, int weaponId, int ammoId) {
		sendDamage(attacker, target, swing, weaponId);
		ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(attacker, target, 249, 40, 30, 41, 15, 0));
	}
}
