package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigPacketBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/31/2017
 */
@CommandManifest(description = "Sends a config to the client", types = { Integer.class, Integer.class })
public class SendConfigCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("sendconfig");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		player.getTransmitter().send(new ConfigPacketBuilder(intParam(args, 1), intParam(args, 2)).build(player));
	}
}
