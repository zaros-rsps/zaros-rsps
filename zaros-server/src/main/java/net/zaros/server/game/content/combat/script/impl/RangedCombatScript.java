package net.zaros.server.game.content.combat.script.impl;

import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.content.combat.script.impl.formulas.HitDelayFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.Ammunition;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitModifier;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.Hit;

public class RangedCombatScript implements CombatScript {

    @Override
    public boolean executable(Entity attacker, Entity target) {

        WeaponInterface weapon = attacker.getTemporaryAttribute(AttributeKey.WEAPON);

        if (weapon == null || weapon.ammunition() == null) {
            return true;
        }

        if (!weapon.ammunition().usable(attacker)) {
            return false;
        }

        return Ammunition.hasAmmo(attacker, 1);
    }

    @Override
    public int getAttackDistance(Entity attacker, Entity target) {

        WeaponInterface weapon = attacker.getTemporaryAttribute(AttributeKey.WEAPON);

        if (weapon == null) {
            return 4;
        }

        return weapon.attackStyles().get(attacker.getCombatDefinitions().getAttackStyle()).getAttackDistance();
    }

    @Override
    public void preExecution(Entity attacker, Entity target) {

    }

    @Override
    public void execute(Entity attacker, Entity target) {

        WeaponInterface weapon = attacker.getTemporaryAttribute(AttributeKey.WEAPON);

        if (weapon == null) {
            return;
        }

        var animId = weapon.attackStyles().get(attacker.getCombatDefinitions().getAttackStyle()).getAnimationId();

        if (animId != -1) {
            attacker.sendAnimation(animId);
        }

        weapon.ammunition().performGraphics(attacker);
        weapon.ammunition().sendProjetile(attacker, target);
        weapon.ammunition().decrementAmmo(attacker, 1);
    }

    @Override
    public Hit.HitSplat getHitSplat(Entity attacker, Entity target) {
        return Hit.HitSplat.RANGE_DAMAGE;
    }

    @Override
    public Hit[] getHits(Entity attacker, Entity target) {

        var hitDelay = HitDelayFormula.getRangeDelay(attacker, target);

        return new Hit[]{new CombatHit(attacker, target, this, Hit.HitSplat.RANGE_DAMAGE, hitDelay)};
    }

    @Override
    public int getSpeed(Entity attacker, Entity target) {

        WeaponInterface weapon = attacker.getTemporaryAttribute(AttributeKey.WEAPON);

        if (weapon != null) {
            return weapon.attackStyles().get(attacker.getCombatDefinitions().getAttackStyle()).getAttackSpeed();
        }

        return 5;
    }

    @Override
    public boolean specialAttack(Entity attacker, Entity target) {
        return false;
    }

    @Override
    public void postExecution(Entity attacker, Entity target) {

    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.getModifier() != null && hit.getSource() != null) {

            WeaponInterface weapon = hit.getSource().getTemporaryAttribute(AttributeKey.WEAPON);

            if (weapon == null || weapon.ammunition() == null) {
                return;
            }

            weapon.ammunition().applySpecialEffect(hit.getSource(), hit.getTarget());
        }
    }

    @Override
    public HitModifier getModifier(Entity attacker, Entity target) {
        WeaponInterface weapon = attacker.getTemporaryAttribute(AttributeKey.WEAPON);

        if (weapon == null || weapon.ammunition() == null) {
            return null;
        }
        return weapon.ammunition().hitModifier(attacker, target);
    }
}
