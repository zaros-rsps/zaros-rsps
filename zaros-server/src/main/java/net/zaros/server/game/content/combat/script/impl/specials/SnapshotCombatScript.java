package net.zaros.server.game.content.combat.script.impl.specials;

import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.content.combat.script.impl.formulas.HitDelayFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.Ammunition;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.game.node.entity.data.weapon.impl.Shortbow;
import net.zaros.server.game.node.entity.data.weapon.impl.TwoHanded;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.render.flag.impl.GraphicHeight;
import net.zaros.server.utility.rs.Hit;

public class SnapshotCombatScript implements CombatScript {

    private static final WeaponInterface weapon = Shortbow.MAGIC_SHORTBOW;

    @Override
    public boolean executable(Entity attacker, Entity target) {
        if (weapon.ammunition() == null) {
            return true;
        }

        if (!weapon.ammunition().usable(attacker)) {
            return false;
        }

        return Ammunition.hasAmmo(attacker, 2);
    }

    @Override
    public int getAttackDistance(Entity attacker, Entity target) {
        return weapon.attackStyles().get(attacker.getCombatDefinitions().getAttackStyle()).getAttackDistance();
    }

    @Override
    public void preExecution(Entity attacker, Entity target) {

    }

    @Override
    public void execute(Entity attacker, Entity target) {
        attacker.sendAnimation(weapon.specialAttack().attackAnimation());

        if (weapon.specialAttack().attackGraphics() > 0) {
            attacker.sendGraphics(weapon.specialAttack().attackGraphics(), GraphicHeight.HIGH.toInt(), 0);
        }

        weapon.specialAttack().sendProjectile(attacker, target);
        weapon.ammunition().decrementAmmo(attacker, 2);

        WeaponSpecial.drainEnergy(attacker, weapon.specialAttack().specialRequired(), true);

    }

    @Override
    public Hit.HitSplat getHitSplat(Entity attacker, Entity target) {
        return Hit.HitSplat.RANGE_DAMAGE;
    }

    @Override
    public Hit[] getHits(Entity attacker, Entity target) {
        var hitDelay = HitDelayFormula.getRangeDelay(attacker, target);
        return new Hit[]{new CombatHit(attacker, target, this, Hit.HitSplat.RANGE_DAMAGE, hitDelay),
                new CombatHit(attacker, target, this, Hit.HitSplat.RANGE_DAMAGE, hitDelay)};
    }

    @Override
    public int getSpeed(Entity attacker, Entity target) {
        return weapon.specialAttack().attackSpeed();
    }

    @Override
    public boolean specialAttack(Entity attacker, Entity target) {
        return true;
    }

    @Override
    public void postExecution(Entity attacker, Entity target) {

    }

    @Override
    public void postHitExecution(CombatHit hit) {
    }
}
