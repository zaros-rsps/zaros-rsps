package net.zaros.server.game.content.event.context.magic;

import lombok.Getter;
import net.zaros.server.game.content.event.EventContext;

public class MagicOnItemContext implements EventContext {

    /**
     * The interface being utilized
     */
    @Getter
    private final int interfaceId;

    /**
     * The slot of the item used
     */
    @Getter
    private final int spellId;

    /**
     * The slot of the item used with
     */
    @Getter
    private final int itemSlot;

    public MagicOnItemContext(int interfaceId, int spellId, int itemSlot) {
        this.interfaceId = interfaceId;
        this.spellId = spellId;
        this.itemSlot = itemSlot;
    }
}

