package net.zaros.server.game.module.command.server_moderator;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.punishment.PunishmentHandler;
import net.zaros.server.game.world.punishment.PunishmentType;
import net.zaros.server.utility.rs.input.InputResponse;
import net.zaros.server.utility.rs.input.InputType;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/17/2017
 */
@CommandManifest(description = "Bans a player on any world.", types = { String.class})
public class BanCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("ban");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		final String name = Misc.formatPlayerNameForProtocol(getCompleted(args, 1));
		player.getTransmitter().requestInput(input -> PunishmentHandler.addPunishment(player, name, InputResponse.getInput(input), PunishmentType.BAN), InputType.INTEGER, "Enter hours (0 = infinite):");
	}
}
