package net.zaros.server.game.content.combat.script;

import net.zaros.server.game.content.combat.CombatManager;
import net.zaros.server.game.content.combat.script.impl.MagicCombatScript;
import net.zaros.server.game.content.combat.script.impl.MeleeCombatScript;
import net.zaros.server.game.content.combat.script.impl.RangedCombatScript;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.Hit;

/**
 * Handles the selection of combat scripts for entities.
 *
 * @author Gabriel || Wolfsdarker
 */
public class CombatScriptSelection {

    /**
     * Returns the most suitable script for the entity at this moment.
     *
     * @param entity
     * @return the scrip
     */
    public static CombatScript get(Entity entity) {
        return (entity.isPlayer() ? getPlayerScript(entity.toPlayer()) : getNPCScript(entity.toNPC()));
    }

    /**
     * Returns the most suitable script for the player at this moment.
     *
     * @param player
     * @return the script
     */
    public static CombatScript getPlayerScript(Player player) {

        if (player.getTemporaryAttribute(AttributeKey.CURRENT_SPELL) != null || player.getTemporaryAttribute(AttributeKey.AUTOCAST_SPELL) != null) {
            return new MagicCombatScript();
        }

        WeaponInterface weapon = player.getTemporaryAttribute(AttributeKey.WEAPON);

        if (weapon != null) {
            if (player.getCombatDefinitions().isSpecialActivated()) {
                if (weapon.specialAttack() != null) {
                    if (player.getCombatDefinitions().getSpecialEnergy() < weapon.specialAttack().specialRequired()) {
                        CombatManager.endCombat(player);
                        player.getTransmitter().sendMessage("You do not have enough special attack energy left!");
                    } else {
                        return weapon.specialAttack().combatScript();
                    }
                }
            }

            if (weapon.attackType() == Hit.HitSplat.RANGE_DAMAGE) {
                return new RangedCombatScript();
            }
        }

        return new MeleeCombatScript();
    }

    /**
     * Returns the most suitable script for the npc at this moment.
     *
     * @param npc
     * @return the script
     */
    public static CombatScript getNPCScript(NPC npc) {

        CombatScript script = npc.getTemporaryAttribute(AttributeKey.COMBAT_SCRIPT, null);

        if (script != null) {
            return script;
        }

        if (npc.getTemporaryAttribute(AttributeKey.CURRENT_SPELL) != null) {
            return new MagicCombatScript();
        }

        return new MeleeCombatScript();
    }

}
