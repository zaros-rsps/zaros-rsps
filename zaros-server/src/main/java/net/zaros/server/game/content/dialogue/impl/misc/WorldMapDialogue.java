package net.zaros.server.game.content.dialogue.impl.misc;

import net.zaros.server.game.content.dialogue.Dialogue;
import net.zaros.server.game.content.dialogue.messages.OptionDialogueMessage;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/8/2017
 */
public class WorldMapDialogue extends Dialogue {
	
	@Override
	public void constructMessages(Player player) {
		construct(new OptionDialogueMessage("Open World Map?", new String[] { "Yes", "No" }, () -> {
			action(() -> player.getManager().getInterfaces().openWorldMap());
		}, () -> {
			action(() -> end(player));
		}));
	}
}
