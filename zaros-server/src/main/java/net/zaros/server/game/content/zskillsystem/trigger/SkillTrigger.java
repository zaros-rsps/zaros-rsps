package net.zaros.server.game.content.zskillsystem.trigger;

/**
 * @author Jacob Rhiel
 */
public interface SkillTrigger {

    boolean trigger();

}
