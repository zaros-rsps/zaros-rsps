package net.zaros.server.game.node.entity.data.weapon.impl;

import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;

import java.util.List;

/**
 * The interface for whips.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum Whip implements WeaponInterface {

    /**
     * The abyssal whip weapon interface.
     */
    ABYSSAL() {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    };

    /**
     * The attack types.
     */
    private static final List<AttackType> attackTypes =
            List.of(AttackType.WARHAMMER_POUND, AttackType.WARHAMMER_PUMMEL, AttackType.WARHAMMER_PUMMEL, AttackType.WARHAMMER_BLOCK);

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }
}
