package net.zaros.server.game.content.combat.script.impl.formulas.impl;

import net.zaros.server.game.content.combat.StaticCombatFormulae;
import net.zaros.server.game.content.combat.script.impl.formulas.CombatFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.item.CombatEquipments;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.BonusConstants;
import net.zaros.server.utility.rs.constant.EquipConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

public class RangeMaxHit implements CombatFormula {

    @Override
    public int get(Entity attacker, Entity target, boolean specialAttack) {

        var rangeLevel = attacker.getSkills().getLevel(SkillConstants.RANGE);

        final double prayerBoost = attacker.getPrayers().getBasePrayerBoost(SkillConstants.RANGE);
        rangeLevel *= prayerBoost;

        final var attackerStyle = attacker.isPlayer() ? attacker.toPlayer().getCombatDefinitions().getAttackStyle() :
                attacker.toNPC().getCombatDefinitions().getAttackStyle();

        if (attackerStyle == 0) {
            rangeLevel += 3;
        }

        rangeLevel += 8.0;

        if (attacker.isPlayer() && StaticCombatFormulae.fullVoidEquipped(attacker.toPlayer(), 11664, 11675)) {
            rangeLevel *= 1.10;
        }

        var rangeBonus = attacker.getBonuses()[BonusConstants.RANGED_STRENGTH_BONUS];

        var maxHit = (0.5 + ((rangeLevel * (rangeBonus + 64.0)) / 640.0)) * 10;

        if (attacker.isNPC()) {
            maxHit = attacker.toNPC().getCombatDefinitions().getMaxHit();
        }

        maxHit = Math.round(maxHit);

        if (attacker.isPlayer() && specialAttack) {

            int weaponId = attacker.toPlayer().getEquipment().getWeaponId();

            switch (weaponId) {
                case Items.DARK_BOW1:
                    if (attacker.toPlayer().getEquipment().getItem(EquipConstants.SLOT_ARROWS).getId() == 11212) {
                        maxHit *= 1.50;
                    } else {
                        maxHit *= 1.30;
                    }
                    break;
                case Items.MAGIC_SHORTBOW1:
                    maxHit = (0.5 + (((attacker.getSkills().getLevel(SkillConstants.RANGE) + 10) * (rangeBonus + 64.0)) / 640.0)) * 10;
                    break;
            }
        }

        return (int) Math.round(maxHit);
    }
}
