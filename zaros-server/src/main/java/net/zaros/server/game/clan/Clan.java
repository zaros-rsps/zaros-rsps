package net.zaros.server.game.clan;

import java.util.function.LongPredicate;

import lombok.Getter;
import net.zaros.server.game.clan.channel.ClanChannel;
import net.zaros.server.game.clan.settings.ClanSettings;

/**
 * Represents a single clan in our clan system. This class holds all the
 * information about one specific clan including members list, the clan channel
 * data, and the clan settings data.
 * </p>
 * As well, it is responsible for all of the operations that can be performed
 * for clan updating, such as adding a new member, removing a new member, etc..
 * 
 * @author Walied K. Yassen
 */
public final class Clan {

	/**
	 * The clan unique id.
	 */
	@Getter
	private long id;

	/**
	 * The clan name.
	 */
	@Getter
	private String name;

	/**
	 * The clan settings.
	 */
	@Getter
	private ClanSettings settings;

	/**
	 * The clan creation time.
	 */
	@Getter
	private long creationTime;

	/**
	 * The clan chat channel.
	 */
	@Getter
	private transient ClanChannel channel;

	/**
	 * Constructs a new {@link Clan} type object instance.
	 * 
	 * @param name
	 *             the clan name.
	 */
	public Clan(String name) {
		this.name = name;
		settings = new ClanSettings();
		id = ClanUtil.generateUniqueId();
		creationTime = System.currentTimeMillis();
	}

	/**
	 * Initialises this channel instance.
	 */
	public void initialise() {
		channel = new ClanChannel(id, name, ClanRank.RECRUIT, ClanRank.RECRUIT);
		settings.initialise(this);
	}

	/**
	 * Updates the member with the specified {@code userId} with the new provided
	 * data.
	 * 
	 * @param userId
	 *               the user id which is linked to the member we want to update.
	 * @param world
	 *               the member's current world id.
	 */
	public void updateMember(long userId, int worldId) {
		settings.updateMember(userId, worldId);
		channel.updateMember(userId, worldId);
	}

	/**
	 * Creates a member user id filter, the filter returns true if the specified
	 * {@code UserID} is equals to the test {@code UserID}.
	 * 
	 * @param userId
	 *               the member user id to create the filter for.
	 * @return the created filter as a {@link LongPredicate} object.
	 */
	public static final LongPredicate memberFilter(final long userId) {
		return (testId) -> testId == userId;
	}
}
