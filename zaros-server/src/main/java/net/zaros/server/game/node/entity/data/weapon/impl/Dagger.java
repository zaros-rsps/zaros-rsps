package net.zaros.server.game.node.entity.data.weapon.impl;

import lombok.Getter;
import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.game.node.entity.data.weapon.impl.specials.Puncture;
import net.zaros.server.utility.rs.Hit;

import java.util.List;

/**
 * The interface for daggers.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum Dagger implements WeaponInterface {

    /**
     * The dagger weapon interface.
     */
    REGULAR(),

    /**
     * The dagger(P) weapon interface.
     */
    REGULAR_P(40),

    /**
     * The dagger(P+) weapon interface.
     */
    REGULAR_PP(50),

    /**
     * The dagger(P++) weapon interface.
     */
    REGULAR_PPP(60),

    /**
     * The dragon dagger weapon interface.
     */
    DRAGON_DAGGER() {
        @Override
        public List<AttackType> attackStyles() {
            return List.of(AttackType.DRAGON_DAGGER_STAB, AttackType.DRAGON_DAGGER_LUNGE, AttackType.DRAGON_DAGGER_SLASH, AttackType.DRAGON_DAGGER_BLOCK);
        }

        @Override
        public WeaponSpecial specialAttack() {
            return Puncture.getInstance();
        }
    },

    /**
     * The dragon dagger(P) weapon interface.
     */
    DRAGON_DAGGER_P(40) {
        @Override
        public List<AttackType> attackStyles() {
            return List.of(AttackType.DRAGON_DAGGER_STAB, AttackType.DRAGON_DAGGER_LUNGE, AttackType.DRAGON_DAGGER_SLASH, AttackType.DRAGON_DAGGER_BLOCK);
        }

        @Override
        public WeaponSpecial specialAttack() {
            return Puncture.getInstance();
        }
    },

    /**
     * The dragon dagger(P+) weapon interface.
     */
    DRAGON_DAGGER_PP(50) {
        @Override
        public List<AttackType> attackStyles() {
            return List.of(AttackType.DRAGON_DAGGER_STAB, AttackType.DRAGON_DAGGER_LUNGE, AttackType.DRAGON_DAGGER_SLASH, AttackType.DRAGON_DAGGER_BLOCK);
        }

        @Override
        public WeaponSpecial specialAttack() {
            return Puncture.getInstance();
        }
    },

    /**
     * The dragon dagger(P++) weapon interface.
     */
    DRAGON_DAGGER_PPP(60) {
        @Override
        public List<AttackType> attackStyles() {
            return List.of(AttackType.DRAGON_DAGGER_STAB, AttackType.DRAGON_DAGGER_LUNGE, AttackType.DRAGON_DAGGER_SLASH, AttackType.DRAGON_DAGGER_BLOCK);
        }

        @Override
        public WeaponSpecial specialAttack() {
            return Puncture.getInstance();
        }
    };

    /**
     * The dagger's poison damage.
     */
    private int poisonDamage = 0;

    /**
     * Constructor for the dagger with no poison.
     */
    Dagger() {
    }

    /**
     * Constructor for the dagger with poison damage.
     *
     * @param poisonDamage
     */
    Dagger(int poisonDamage) {
        this.poisonDamage = poisonDamage;
    }

    /**
     * The attack types.
     */
    @Getter
    private static final List<AttackType> attackTypes =
            List.of(AttackType.DAGGER_STAB, AttackType.DAGGER_LUNGE, AttackType.DAGGER_SLASH, AttackType.DAGGER_BLOCK);

    @Override
    public int poisonDamage() {
        return poisonDamage;
    }

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }
}
