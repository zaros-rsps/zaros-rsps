/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames;

/**
 * Represents the minigame player status.
 * 
 * @author Walied K. Yassen
 */
public enum MinigameStatus {

	/**
	 * Tells that the player is currently in the lobby area.
	 */
	LOBBY,

	/**
	 * Tells that the player is currently in the waiting area.
	 */
	WAITING,

	/**
	 * Tells that the player is currently in the game area.
	 */
	INGAME,
}
