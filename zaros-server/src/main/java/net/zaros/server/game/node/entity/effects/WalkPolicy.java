package net.zaros.server.game.node.entity.effects;

/**
 * A simple walk policy for systems.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum WalkPolicy {

    /**
     * The policy where walking is allowed.
     */
    WALKABLE,

    /**
     * The policy where walking is disabled and must not happen.
     */
    NON_WALKABLE

}
