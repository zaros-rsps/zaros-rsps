package net.zaros.server.game.node.entity.data.weapon.impl;

import lombok.Getter;
import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.game.node.entity.data.weapon.impl.specials.IceCleave;
import net.zaros.server.game.node.entity.data.weapon.impl.specials.TheJudgment;
import net.zaros.server.utility.rs.Hit;

import java.util.List;

/**
 * The interface for two-handed weapons.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum TwoHanded implements WeaponInterface {

    /**
     * The interface for regular two-handed weapons.
     */
    REGULAR(),

    /**
     * The interface for dragon two-handed weapon.
     */
    DRAGON() {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },

    /**
     * The saradomin sword weapon interface.
     */
    SARADOMIN_SWORD() {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },

    /**
     * The armadyl godsword weapon interface.
     */
    ARMADYL_GODSWORD() {
        @Override
        public WeaponSpecial specialAttack() {
            return TheJudgment.getInstance();
        }
    },

    /**
     * The bandos godsword weapon interface.
     */
    BANDOS_GODSWORD() {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },

    /**
     * The saradomin godsword weapon interface.
     */
    SARADOMIN_GODSWORD() {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },

    /**
     * The zamorak godsword weapon interface.
     */
    ZAMORAK_GODSWORD() {
        @Override
        public WeaponSpecial specialAttack() {
            return IceCleave.getInstance();
        }
    };

    /**
     * The attack types.
     */
    private static final List<AttackType> attackTypes =
            List.of(AttackType.TWOHANDED_SWORD_CHOP, AttackType.TWOHANDED_SWORD_SLASH, AttackType.TWOHANDED_SWORD_SMASH, AttackType.TWOHANDED_SWORD_BLOCK);

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }
}
