package net.zaros.server.game.node.entity.data.weapon.impl.specials;

import lombok.Getter;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.content.combat.script.impl.specials.IceCleaveCombatScript;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;

public class IceCleave implements WeaponSpecial {

    /**
     * The special's combat script.
     */
    private static final IceCleaveCombatScript script = new IceCleaveCombatScript();

    /**
     * The special's instance.
     */
    @Getter
    private static final IceCleave instance = new IceCleave();

    @Override
    public int attackAnimation() {
        return 7070;
    }

    @Override
    public int attackGraphics() {
        return 1221;
    }

    @Override
    public int targetGraphics() {
        return 2104;
    }

    @Override
    public int attackSpeed() {
        return 6;
    }

    @Override
    public int specialRequired() {
        return 50;
    }

    @Override
    public Hit.HitSplat hitStyle() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public CombatScript combatScript() {
        return script;
    }
}
