package net.zaros.server.game.content.zone;


import net.zaros.server.game.node.entity.player.Player;

public interface ZoneListener {

    boolean objectInteraction(Player player, int objectID, int optionID);

    boolean itemToObjectInteraction(Player player, int objectID, int itemID);

    boolean npcInteraction(Player player, int npcID, int optionID);

    boolean onNPCDeath(Player player, int deadNPCID);
}
