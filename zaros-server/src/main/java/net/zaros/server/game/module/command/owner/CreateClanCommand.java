package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.clan.manager.ClanManager;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Walied K. Yassen
 */
public final class CreateClanCommand extends CommandModule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.module.command.CommandModule#handle(net.zaros.server.
	 * game.node.entity.player.Player, java.lang.String[], boolean)
	 */
	@Override
	public void handle(Player player, String[] args, boolean console) {
		var name = stringParamOrDefault(args, 1, null);
		if (name == null) {
			sendResponse(player, "You must specify a clan name before creating.", console);
			return;
		}
		ClanManager.requestCreate(player, name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.module.command.CommandModule#identifiers()
	 */
	@Override
	public String[] identifiers() {
		return arguments("createclan");
	}
}
