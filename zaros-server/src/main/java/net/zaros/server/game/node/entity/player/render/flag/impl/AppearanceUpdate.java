package net.zaros.server.game.node.entity.player.render.flag.impl;

import net.zaros.cache.type.defaults.BodyDataParser;
import net.zaros.cache.type.npctype.NPCDefinition;
import net.zaros.cache.type.npctype.NPCDefinitionParser;
import net.zaros.cache.type.objtype.ItemDefinitionParser;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.PlayerAppearance;
import net.zaros.server.game.node.entity.render.flag.UpdateFlag;
import net.zaros.server.game.world.World;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.CS2ConfigBuilder;
import net.zaros.server.utility.rs.constant.EquipConstants;

/**
 * Represents a player's appearance update flag.
 *
 * @author Emperor
 */
public class AppearanceUpdate extends UpdateFlag {
	
	/**
	 * The player.
	 */
	private final Player player;
	
	/**
	 * The player's appearance.
	 */
	private final PlayerAppearance appearance;
	
	/**
	 * Constructs a new {@code AppearanceUpdate} {@code Object}.
	 *
	 * @param player
	 * 		The player.
	 */
	public AppearanceUpdate(Player player) {
		this.player = player;
		appearance = player.getDetails().getAppearance();
		// updating the combat level on the styles tab
		player.getTransmitter().send(new CS2ConfigBuilder(1000, player.getSkills().getCombatLevelWithSummoning()).build(player));
	}
	
	@Override
	public void write(Player outgoing, PacketBuilder bldr) {
		PacketBuilder playerUpdate = new PacketBuilder();
		final int npcId = appearance.getNpcId();
		int bitSet = 0;
		//bitSet |= 0x4; we are in a pvp area
		if (!appearance.isMale()) {
			bitSet |= 0x1;
		}
		final NPCDefinition definition = npcId >= 0 ? NPCDefinitionParser.forId(npcId) : null;
		if (npcId != -1 && definition != null) {
			bitSet |= 0x2;
		}
		playerUpdate.writeByte(bitSet);
		playerUpdate.writeByte(0); // title
		playerUpdate.writeByte(player.getVariables().getSkullIcon().getId()); //skull icon
		playerUpdate.writeByte(player.getPrayers().getIcon().getId());
		playerUpdate.writeByte(0);
		if (definition == null || npcId == -1) {
			for (int i = 0; i < BodyDataParser.getBodyData().length; i++) {
				if (BodyDataParser.getBodyData()[i] != 1) {
					int d = appearance.getBodyPart(i);
					if (d == 0) {
						playerUpdate.writeByte(0);
					} else {
						playerUpdate.writeShort((short) d);
					}
				}
			}
			bitSet = 0;
			int part = 0;
			int slotHash = 0;
			int pos = playerUpdate.position();
			playerUpdate.writeShort(0);
			for (int i = 0; i < BodyDataParser.getBodyData().length; i++) {
				if (BodyDataParser.getBodyData()[i] != 1) {
					int itemId = player.getEquipment().getItem(i) == null ? -1 : player.getEquipment().getItem(i).getId();
					if (i == 1) {
						if (itemId == 20767 || itemId == 20769 || itemId == 20771) {
							bitSet |= 1 << part;
							slotHash |= 0x1;
							playerUpdate.writeByte(0x4); // modify 4 model colors
							int slots = 0 | 1 << 4 | 2 << 8 | 3 << 12;
							playerUpdate.writeShort(slots);
							int[] colors = player.getManager().getCompletionist().getModifiedMaxCapeColors() == null ? player.getManager().getCompletionist().getModifiedCompletionistCapeColors()
									: player.getManager().getCompletionist().getModifiedMaxCapeColors();
							if (colors == null) {
								colors = ItemDefinitionParser.forId(player.getEquipment().getIdInSlot(EquipConstants.SLOT_CAPE)).getOriginalModelColors();
							}
							for (int i2 = 0; i2 < 4; i2++) {
								playerUpdate.writeShort(colors[i2]);
							}
						}
					}
					/*if (i == 14) { //Only with aura.
						*//*if(itemId == aur)
						bitSet |= 1 << part;
						slotHash |= 0x1;
						playerUpdate.writeByte(0x1); // modify model ids
						playerUpdate.writeIntSmart(8719);
						playerUpdate.writeIntSmart(8719);*//*
					}*/
				}
				part++;
			}
			int pos2 = playerUpdate.position();
			if(bitSet != 0) {
				playerUpdate.setPosition(pos);
				playerUpdate.writeShort(bitSet);
				playerUpdate.setPosition(pos2);
			}
		} else {
			playerUpdate.writeShort(-1);
			playerUpdate.writeShort(npcId);
			playerUpdate.writeByte(0);
		}
		for (byte i = 0; i < 10; i++) {
			playerUpdate.writeByte(appearance.getColor(i));
		}
		playerUpdate.writeShort(appearance.getRenderEmote());
		playerUpdate.writeString(player.getDetails().getDisplayName());
		
		boolean pvpArea = World.isPvpArea(player.getLocation());
		
		// this also updates combat lvl in tab
		playerUpdate.writeByte(pvpArea ? player.getSkills().getCombatLevel() : player.getSkills().getCombatLevelWithSummoning());
		playerUpdate.writeByte(pvpArea ? player.getSkills().getCombatLevelWithSummoning() : 0);
		
		playerUpdate.writeByte(-1);
		
		// npc morph
		playerUpdate.writeByte(npcId >= 0 ? 1 : 0);
		if (npcId >= 0 && definition != null) {
			playerUpdate.writeShort(definition.getAnInt876());
			playerUpdate.writeShort(definition.getAnInt842());
			playerUpdate.writeShort(definition.getAnInt884());
			playerUpdate.writeShort(definition.getAnInt875());
			playerUpdate.writeByte(definition.getAnInt875());
		}
		bldr.writeByte(playerUpdate.getBuffer().writerIndex());
		bldr.writeBytesA(playerUpdate.getBuffer().array(), 0, playerUpdate.getBuffer().writerIndex());
	}
	
	@Override
	public int getOrdinal() {
		return 6;
	}
	
	@Override
	public int getMaskData() {
		return 0x2;
	}
	
}