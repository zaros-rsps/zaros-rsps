package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.content.market.shop.ShopRepository;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

public class ReloadShopCommand extends CommandModule {

    @Override
    public String[] identifiers() {
        return arguments("reloadshops, reloadshop");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {
        int shopId = Integer.valueOf(args[1]);
        ShopRepository.reload();
    }
}
