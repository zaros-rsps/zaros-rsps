package net.zaros.server.game.node.entity.effects.impl;

import lombok.Getter;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.effects.Duration;
import net.zaros.server.game.node.entity.effects.Effect;
import net.zaros.server.game.node.entity.effects.WalkPolicy;
import net.zaros.server.utility.tool.SecondsTimer;

/**
 * The stun effect for entities.
 *
 * @author Gabriel || Wolfsdarker
 */
public class StunEffect implements Effect {

    /**
     * The effect's name.
     */
    @Getter
    private static final String name = "stun";

    /**
     * The entity who is receiving the effect.
     */
    private Entity entity;

    /**
     * The entity that is applying the effect.
     */
    private Entity source;

    /**
     * The effect's timer.
     */
    private SecondsTimer timer;

    /**
     * Constructor for the stun effect.
     *
     * @param entity
     * @param source
     * @param seconds
     */
    public StunEffect(Entity entity, Entity source, int seconds) {
        this.entity = entity;
        this.source = source;
        this.timer = new SecondsTimer();
        this.timer.start(seconds);
    }

    @Override
    public boolean over() {
        return timer.finished();
    }

    @Override
    public boolean renewable() {
        return false;
    }

    @Override
    public boolean immune(Entity entity) {
        return !entity.getEffectManager().getStunImmunityTimer().finished();
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String nonWalkableMessage() {
        return "You are stunned and cannot walk!";
    }

    @Override
    public WalkPolicy walkPolicy() {
        return WalkPolicy.NON_WALKABLE;
    }

    @Override
    public Duration duration() {
        return Duration.TEMPORARY;
    }

    @Override
    public void preExecution(boolean isLogin) {
        if (isLogin) {
            return;
        }

        entity.getEffectManager().getStunImmunityTimer().start(timer.secondsRemaining() + 3);

        if (entity.isPlayer()) {
            entity.toPlayer().getTransmitter().sendMessage("<col=990000>You have been stunned!</col>");
        }
    }

    @Override
    public void onTick() {
    }

    @Override
    public void renew() {

    }

    @Override
    public void end() {

    }
}
