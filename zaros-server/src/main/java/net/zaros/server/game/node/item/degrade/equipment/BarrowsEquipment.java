package net.zaros.server.game.node.item.degrade.equipment;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.degrade.DegradableEquipment;

public class BarrowsEquipment extends DegradableEquipment {

	public BarrowsEquipment() {
		register(4708, 4856, 4857, 4858, 4859, 4716, 4880, 4881, 4882, 4883, 4724, 4904, 4905, 4906, 4907, 4732, 4928, 4929, 4930, 4931, 4745, 4952, 4953, 4954, 4955, 4753, 4976, 4977, 4978, 4979);
		register(4710, 4862, 4863, 4864, 4865, 4718, 4886, 4887, 4888, 4889, 4726, 4910, 4911, 4912, 4913, 4734, 4934, 4935, 4936, 4937, 4747, 4958, 4959, 4960, 4961, 4755, 4982, 4983, 4984, 4985);
		register(4712, 4868, 4869, 4870, 4871, 4720, 4892, 4893, 4894, 4895, 4728, 4916, 4917, 4918, 4919, 4736, 4940, 4941, 4942, 4943, 4749, 4964, 4965, 4966, 4967, 4757, 4988, 4989, 4990, 4991);
		register(4714, 4874, 4875, 4876, 4877, 4722, 4898, 4899, 4900, 4901, 4730, 4922, 4923, 4924, 4925, 4738, 4946, 4947, 4948, 4949, 4751, 4970, 4971, 4972, 4973, 4759, 4994, 4995, 4996, 4997);
	}

	@Override
	public boolean degrade(Player player, Item item) {
		int index = -1;
		for (int slot = 0; slot < getItems().length; slot++) {
			if (item.getId() == getItems()[slot]) {
				index = slot;
				break;
			}
		}
		if (index < 0) {
			return false;
		}
		if (item.getCharge() > 0) {
			item.setCharge(item.getCharge() - 1);
		}
		if (index % 5 == 0 || item.getCharge() < 1) {
			String name = item.getName();
			int ch = -1;
			if ((ch = name.indexOf("1")) > -1) {
				name = name.substring(0, ch - 1);
			} else if ((ch = name.indexOf("2")) > -1) {
				name = name.substring(0, ch - 1);
			} else if ((ch = name.indexOf("5")) > -1) {
				name = name.substring(0, ch - 1);
			} else if ((ch = name.indexOf("7")) > -1) {
				name = name.substring(0, ch - 1);
			}
			player.getTransmitter().sendMessage("Your " + name + " has degraded.");
			int replaceId;
			if (index % 5 == 4) {
				if (player.getInventory().getItems().add(new Item(item.getId() + 1))) {
					player.getEquipment().getItems().remove(player.getEquipment().getItems().get(getSlot()));
					return true;
				}
				replaceId = item.getId() + 1;
			} else {
				replaceId = getItems()[index + 1];
			}
			player.getEquipment().getItems().replace(player.getEquipment().getItems().getThisItemSlot(item), replaceId);
		}
		player.getEquipment().refreshAll();
		return true;
	}

	@Override
	public int getDegradedDropItem(int itemId) {
		int index = -1;
		for (int i = 0; i < getItems().length; i++) {
			if (itemId == getItems()[i]) {
				index = i;
				break;
			}
		}
		if (index % 5 < 1) {
			return itemId;
		}
		return getItems()[index + 4 - index % 5] + 1;
	}

}
