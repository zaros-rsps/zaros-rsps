package net.zaros.server.game.content.perks;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.node.entity.player.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public final class PlayerPerk {

    @Setter
    private transient Player player;

    @Getter
    private List<Perk> ownedPerks;

    public PlayerPerk() {
        this.ownedPerks = new ArrayList<>();
    }

    /**
     * Adds a Perk to the players owned perks
     * @param perk The perk to be added
     */
    public void addPerk(Perk perk) {
        Optional<Perk> optional = getOwnedPerks().stream().filter(foundPerk -> getOwnedPerks().contains(perk)).findAny();
        if(!optional.isPresent()) {
            getOwnedPerks().add(perk);
        }
    }

    /**
     * Checks whether or not the player has the requested perk
     * @param perk The perk requested
     * @return whether or not it is present
     */
    public boolean hasPerk(Perk perk) {
        Optional<Perk> optional = getOwnedPerks().stream().filter(foundPerk -> getOwnedPerks().contains(perk)).findAny();
        if(optional.isPresent()) return true;
        return false;
    }

    @Override
    public String toString() {
        return " " + Arrays.toString(getOwnedPerks().toArray()) + " ";
    }

}
