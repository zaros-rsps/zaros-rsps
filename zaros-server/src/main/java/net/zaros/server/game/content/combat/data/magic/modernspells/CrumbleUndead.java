package net.zaros.server.game.content.combat.data.magic.modernspells;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitModifier;
import net.zaros.server.game.node.entity.render.flag.impl.GraphicHeight;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;

import java.util.List;

/**
 * The crumble undead spell on the regular spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class CrumbleUndead extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 47;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.AIR_RUNE1, 2), new Item(Items.EARTH_RUNE1, 2), new Item(Items.CHAOS_RUNE1));

    @Override
    public void playGraphics(Entity source, Entity target) {
        source.sendGraphics(145, GraphicHeight.HIGH.toInt(), 0);
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(724);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
        ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(source, target, 146, 30, 26, 52, 0, 0));
    }

    @Override
    public HitModifier getModifier(Entity source, Entity target) {
        return null;
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return null;
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(147);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int autocastConfigID() {
        return 35;
    }

    @Override
    public int magicLevelRequirement() {
        return 39;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 160;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 24;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.REGULAR;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }

    @Override
    public boolean ableToCastOnEntity(Entity source, Entity target) {
        if (!super.ableToCastOnEntity(source, target)) {
            return false;
        }

        if (target.isNPC()) {
            String targetName = target.toNPC().getDefinitions().getName().toLowerCase();
            if (targetName.contains("ghost") || targetName.contains("skeleton") || targetName.contains("shade") || targetName.contains("zombi")) {
                return true;
            }
        }

        if (source.isPlayer()) {
            source.toPlayer().getTransmitter().sendMessage("You can only cast this on undead enemies.");
        }
        return false;
    }
}
