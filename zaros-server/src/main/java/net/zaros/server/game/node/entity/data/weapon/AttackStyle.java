package net.zaros.server.game.node.entity.data.weapon;

/**
 * The attack styles for weapons.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum AttackStyle {

    ACCURATE,
    AGGRESSIVE,
    DEFENSIVE,
    CONTROLLED

}
