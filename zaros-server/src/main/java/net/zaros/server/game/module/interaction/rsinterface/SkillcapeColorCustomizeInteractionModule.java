package net.zaros.server.game.module.interaction.rsinterface;

import net.zaros.server.game.module.type.InterfaceInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.tool.Misc;

public class SkillcapeColorCustomizeInteractionModule implements InterfaceInteractionModule {

    @Override
    public int[] interfaceSubscriptionIds() {
        return Misc.arguments(19, 20);
    }

    @Override
    public boolean handle(Player player, int interfaceId, int componentId, int itemId, int slotId, int packetId) {
        player.getManager().getCompletionist().handleInterface(player, interfaceId, componentId, itemId);
        return true;
    }
}
