package net.zaros.server.game.content.market.moneypouch;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.PlayerInventory;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.network.world.packet.outgoing.impl.CS2ScriptBuilder;
import net.zaros.server.utility.rs.input.InputResponse;
import net.zaros.server.utility.rs.input.InputType;

public class MoneyPouch {

    @Getter
    @Setter
    private transient Player player;

    @Getter
    @Setter
    private int amount = 0;

    @Getter
    @Setter
    private transient boolean open = false;

    public void display() {
        if(!open) {
            player.getTransmitter().send(new CS2ScriptBuilder(5557, "i", new Object[] { 1 }).build(player));
            player.getTransmitter().send(new CS2ScriptBuilder(5560, "i", new Object[] { getAmount() }).build(player));
            setOpen(true);
        } else {
            player.getTransmitter().send(new CS2ScriptBuilder(5557, "i", new Object[] { 1 }).build(player));
            setOpen(false);
        }
    }

    public void withdraw() {
        player.getTransmitter().requestInput(input -> {
            if(player.getManager().getMoneyPouch().update(true, InputResponse.getInput(input)))
                player.getManager().getMoneyPouch().updateCoins(true, InputResponse.getInput(input));
        }, InputType.INTEGER, "Withdraw amount:");
    }

    public void deposit() {
        player.getTransmitter().requestInput(input -> {
            if(player.getManager().getMoneyPouch().update(false, InputResponse.getInput(input)))
                player.getManager().getMoneyPouch().updateCoins(false, InputResponse.getInput(input));
        }, InputType.INTEGER, "Deposit amount:");
    }

    public boolean update(boolean remove, int amount) {
        if(check(remove, amount)) {
            if (remove) {
                player.getTransmitter().send(new CS2ScriptBuilder(5561, "ii", new Object[]{0, amount}).build(player));
                setAmount(getAmount() - amount);
            } else {
                player.getTransmitter().send(new CS2ScriptBuilder(5561, "ii", new Object[]{1, amount}).build(player));
                setAmount(getAmount() + amount);
            }
            player.getTransmitter().send(new CS2ScriptBuilder(5560, "i", new Object[]{getAmount()}).build(player));
            return true;
        }
        return false;
    }

    public boolean check(boolean withdraw, int amount) {
        PlayerInventory inventory = getPlayer().getInventory();
        boolean possible = withdraw ? getAmount() >= amount : inventory.getItems().getNumberOf(995) >= amount;
        if(!possible)
            player.getTransmitter().sendMessage(withdraw ? "You do not have enough coins in your pouch." : "You do not have enough coins to deposit.");
        return possible;
    }

    public boolean updateCoins(boolean add, int amount) {
        return add ? player.getInventory().addItem(new Item(995, amount)) : player.getInventory().deleteItem(995, amount);
    }

}
