package net.zaros.server.game.plugin.java;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents the Java {@link Plugin} {@link ClassLoader} implementation.
 * 
 * @author Walied K. Yassen
 */
public final class PluginClassLoader extends URLClassLoader {

	/**
	 * The cached classes in our memory which were loaded through this class loader.
	 */
	private final Map<String, Class<?>> classes = new HashMap<>();

	/**
	 * The {@link PluginLoader} instance.
	 */
	private final PluginLoader loader;

	/**
	 * Construct a new {@link PluginClassLoader} type object instance.
	 * 
	 * @param loader
	 *                   the plugin loader used to access other plugins.
	 * @param parent
	 *                   the parent class loader object. the parent class loader.
	 * @param path
	 *                   the plugin {@code .jar} file path.
	 * @throws MalformedURLException
	 *                                   if the given path is malformed.
	 */
	public PluginClassLoader(PluginLoader loader, ClassLoader parent, File file) throws MalformedURLException {
		super(new URL[] { file.toURI().toURL() }, parent);
		this.loader = loader;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.net.URLClassLoader#findClass(java.lang.String)
	 */
	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		Class<?> result = classes.get(name);
		if (result != null) {
			return result;
		}
		result = super.findClass(name);
		classes.put(name, result);
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.net.URLClassLoader#findResource(java.lang.String)
	 */
	@Override
	public URL findResource(String name) {
		return super.findResource(name);
	}
}
