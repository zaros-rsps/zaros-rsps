package net.zaros.server.game.node.item;

import lombok.Getter;
import lombok.Setter;
import net.zaros.cache.type.objtype.ItemDefinition;
import net.zaros.cache.type.objtype.ItemDefinitionParser;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.Node;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/21/2017
 */
public class Item extends Node {
	
	/**
	 * The item id.
	 */
	@Getter
	@Setter
	private int id;
	
	/**
	 * The item amount.
	 */
	@Getter
	@Setter
	private int amount;

	@Getter
	@Setter
	private int charge;

	@Getter
	@Setter
	private transient int slot;

	/**
	 * Constructs an item with a location. This is only to be used for floor item child class management
	 *
	 * @param location
	 * 		The location of the item
	 */
	public Item(int id, Location location) {
		super(id, location);
		this.id = -1;
		amount = -1;
	}
	
	/**
	 * Constructs a new {@code Item} {@code Object}.
	 *
	 * @param id
	 * 		The item id.
	 */
	public Item(int id) {
		this(id, 1);
	}
	
	/**
	 * Constructs a new {@code Item} {@code Object}.
	 *
	 * @param id
	 * 		The item id.
	 * @param amount
	 * 		The amount of this item.
	 */
	public Item(int id, int amount) {
		this(id, amount, 1000);
	}

	public Item(int id, int amount, int charge) {
		super(id, null);
		this.id = id;
		this.amount = amount;
		this.charge = charge;
	}
	
	@Override
	public boolean equals(Object o) {
		return o.getClass() == Item.class && ((Item) o).id == id && ((Item) o).getAmount() == amount;
	}
	
	@Override
	public String toString() {
		return "[id=" + id + ", name=" + getName() + ", amount=" + amount + "]";
	}
	
	/**
	 * Gets the name of the item
	 */
	public String getName() {
		return getDefinitions().getName();
	}
	
	@Override
	public Item toItem() {
		return this;
	}
	
	@Override
	public int getSize() {
		return 1;
	}
	
	/**
	 * Reduces the amount of the item
	 *
	 * @param amount
	 * 		The amount to reduce by
	 */
	public Item reduceAmount(int amount) {
		this.amount -= amount;
		// make sure we're not less than 0
		if (this.amount < 0) {
			this.amount = 0;
		}
		return this;
	}
	
	/**
	 * Gets the definitions, cached.
	 */
	public ItemDefinition getDefinitions() {
		return ItemDefinitionParser.forId(id);
	}
}
