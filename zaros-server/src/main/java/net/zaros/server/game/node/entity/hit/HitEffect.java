package net.zaros.server.game.node.entity.hit;

/**
 * The effects to be applied in hit calculation or execution.
 *
 * @author Gabriel || Wolfsdarker
 */
public class HitEffect {

    /**
     * The hit's type.
     */
    private HitEffectType type;

    /**
     * The instant the effect should be applied.
     */
    private HitInstant instant;

    /**
     * The amount for the effect. (@Ex: poison damage, freeze in seconds).
     */
    private double amount;

    /**
     * The action to be executed.
     */
    private Runnable action;

    /**
     * Constructor for the hit effect.
     *
     * @param type
     * @param instant
     * @param amount
     */
    public HitEffect(HitEffectType type, HitInstant instant, double amount) {
        this.type = type;
        this.amount = amount;
        this.instant = instant;
    }

    /**
     * Constructor for the hit effect.
     *
     * @param type
     * @param instant
     * @param action
     */
    public HitEffect(HitEffectType type, HitInstant instant, Runnable action) {
        this.type = type;
        this.instant = instant;
        this.action = action;
    }

    /**
     * Constructor for the hit effect.
     *
     * @param instant
     * @param action
     */
    public HitEffect(HitInstant instant, Runnable action) {
        this.type = HitEffectType.RUNNABLE_ACTION;
        this.instant = instant;
        this.action = action;
    }


    /**
     * Returns the hit effect's type.
     *
     * @return
     */
    public HitEffectType getType() {
        return type;
    }

    /**
     * Returns the instant the effect should be applied.
     *
     * @return
     */
    public HitInstant getInstant() {
        return instant;
    }

    /**
     * Returns the amount for the type. (@Ex: poison damage, freeze in seconds).
     *
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Returns the action to be executed.
     *
     * @return the action
     */
    public Runnable getAction() {
        return action;
    }

}
