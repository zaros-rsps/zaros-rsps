/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames.controller;

import lombok.Getter;
import net.zaros.server.game.content.activity.Activity;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.TeleportType;
import net.zaros.server.game.content.minigames.MinigameBase;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.ButtonOption;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * Represents the activity for any minigame.
 * 
 * @author Walied K. Yassen
 */
public final class MinigameActivity extends Activity {

	/**
	 * The minigame object which this activity is for.
	 */
	@Getter
	private final MinigameBase minigame;

	/**
	 * Construct a new {@link MinigameActivity} type object instance.
	 * 
	 * @param type
	 *             the minigame object.
	 */
	public MinigameActivity(MinigameBase minigame) {
		super(minigame);
		this.minigame = minigame;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.content.activity.Activity#start()
	 */
	@Override
	public void start() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.activity.Activity#handleEntityDeath(org.redrune.game
	 * .node.entity.Entity)
	 */
	@Override
	public boolean handlePreDeath(Entity entity) {
		return minigame.handlePreDeath(entity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.activity.Activity#handlePostDeath(org.redrune.game.
	 * node.entity.Entity)
	 */
	@Override
	public boolean handlePostDeath(Entity entity) {
		return minigame.handlePostDeath(entity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.content.activity.Activity#handlePreDisconnect()
	 */
	@Override
	public boolean handlePreDisconnect() {
		return minigame.handlePreDisconnect(player);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.content.activity.Activity#handlePostDisconnect()
	 */
	@Override
	public void handlePostDisconnect() {
		minigame.handlePostDisconnect(player);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.content.activity.Activity#handleButton(int, int, int,
	 * int, org.redrune.utility.rs.ButtonOption)
	 */
	@Override
	public boolean handleButton(int interfaceId, int buttonId, int slotId, int itemId, ButtonOption option) {
		return minigame.handleButton(player, interfaceId, buttonId, slotId, itemId, option);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.activity.Activity#handleInventory(org.redrune.game.
	 * node.item.Item, int, org.redrune.utility.rs.InteractionOption)
	 */
	@Override
	public boolean handleInventory(Item item, int slot, InteractionOption option) {
		return minigame.handleInventory(player, item, slot, option);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.content.activity.Activity#handleModalClosing(int)
	 */
	@Override
	public boolean handleModalClosing(int interfaceId) {
		return minigame.handleModalClosing(player, interfaceId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.activity.Activity#handleObject(org.redrune.game.node
	 * .object.GameObject, org.redrune.utility.rs.InteractionOption)
	 */
	@Override
	public boolean handleObject(GameObject object, InteractionOption option) {
		return minigame.handleObject(player, object, option);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.activity.Activity#handleNPCOption(org.redrune.game.
	 * node.entity.npc.NPC, org.redrune.utility.rs.InteractionOption)
	 */
	@Override
	protected boolean handleNPCOption(NPC npc, InteractionOption option) {
		return minigame.handleNPC(player, npc, option);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.activity.Activity#handlePlayerOption(org.redrune.
	 * game.node.entity.player.Player, org.redrune.utility.rs.InteractionOption)
	 */
	@Override
	protected boolean handlePlayerOption(Player target, InteractionOption option) {
		return minigame.handlePlayer(target, target, option);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.activity.Activity#teleportationAllowed(org.redrune.
	 * game.content.combat.player.registry.wrapper.magic.TeleportType)
	 */
	@Override
	public boolean teleportationAllowed(TeleportType type) {
		return minigame.isTeleportAllowed(player, type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.content.activity.Activity#savesOnLogout()
	 */
	@Override
	public boolean savesOnLogout() {
		return false;
	}
}
