package net.zaros.server.game.plugin;

/**
 * Represents an exception or error that occurred during any plug-in related
 * operation.
 *
 * @author Walied K. Yassen
 * @since 0.1
 */
public class PluginException extends RuntimeException {

	/**
	 * The serialisation key of the {@link PluginException} class type.
	 */
	private static final long serialVersionUID = -5517803083301735390L;

	/**
	 * Constructs a new {@link PluginException} object instance.
	 */
	public PluginException() {
		super();
	}

	/**
	 * Constructs a new {@link PluginException} object instance.
	 *
	 * @param message the detailed message of the exception.
	 * @param cause   the original cause of the exception.
	 */
	public PluginException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new {@link PluginException} object instance.
	 *
	 * @param message the detailed message of the exception.
	 */
	public PluginException(String message) {
		super(message);
	}

	/**
	 * Constructs a new {@link PluginException} object instance.
	 *
	 * @param cause the original cause of the exception.
	 */
	public PluginException(Throwable cause) {
		super(cause);
	}

}
