package net.zaros.server.game.content.event.context;

import lombok.Getter;
import net.zaros.server.game.content.event.EventContext;

public class ColorChooseEventContext implements EventContext {

    @Getter
    private final int color;

    public ColorChooseEventContext(int color) {
        this.color = color;
    }
}
