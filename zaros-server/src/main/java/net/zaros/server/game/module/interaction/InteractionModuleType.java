package net.zaros.server.game.module.interaction;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/27/2017
 */
public enum InteractionModuleType {
	
	INTERFACE,
	OBJECT,
	NPC,
	ITEM
}
