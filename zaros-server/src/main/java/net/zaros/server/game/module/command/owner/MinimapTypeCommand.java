/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.MinimapTypeBuilder;

/**
 * @author Walied K. Yassen
 */
public class MinimapTypeCommand extends CommandModule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.module.command.CommandModule#identifiers()
	 */
	@Override
	public String[] identifiers() {
		return new String[] { "minimaptype" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.module.command.CommandModule#handle(org.redrune.game.node.
	 * entity.player.Player, java.lang.String[], boolean)
	 */
	@Override
	public void handle(Player player, String[] args, boolean console) {
		player.getTransmitter().send(new MinimapTypeBuilder(intParam(args, 1)).build(player));
	}

}
