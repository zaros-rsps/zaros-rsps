/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames.util;

/**
 * Represents the minigame join or leave area options.
 * 
 * @author Walied K. Yassen
 */
public enum JoinLeaveOptions {

	/**
	 * Specifies that the player should not be moved from their location during the
	 * joining or leaving operations.
	 */
	DONT_MOVE,

	/**
	 * Specifies that the player should be forced to perform the operation
	 * regardless of the existence check.
	 */
	FORCE,

	/**
	 * Specifies that we should not apply the minigame implementation effect when
	 * leaving or joining, that includes any kind of modification etc..
	 */
	EFFECTLESS,
}
