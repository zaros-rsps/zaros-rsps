package net.zaros.server.game.world.region.route;

/**
 * Class, controlling the exit point of a route.
 */
public abstract class RouteStrategy {

	public static final int BLOCK_FLAG_NORTH = 0x1;

	public static final int BLOCK_FLAG_EAST = 0x2;

	public static final int BLOCK_FLAG_SOUTH = 0x4;

	public static final int BLOCK_FLAG_WEST = 0x8;

	/**
	 * Whether we can exit at specific x and y.
	 */
	public abstract boolean canExit(int currentX, int currentY, int sizeXY, int[][] clip, int clipBaseX, int clipBaseY);

	/**
	 * Get's approximated destination position X.
	 */
	public abstract int getApproxDestinationX();

	/**
	 * Get's approximated destination position Y.
	 */
	public abstract int getApproxDestinationY();

	/**
	 * Get's approximated destination size X.
	 */
	public abstract int getApproxDestinationSizeX();

	/**
	 * Get's approximated destination size Y.
	 */
	public abstract int getApproxDestinationSizeY();

	/**
	 * Whether this strategy equals to other object.
	 */
	@Override
	public abstract boolean equals(Object other);

	/**
	 * Check's if we can interact wall decoration from current position.
	 */
	protected static boolean checkWallDecorationInteract(int[][] clip, int currentX, int currentY, int sizeXY, int targetX, int targetY, int targetType, int targetRotation) {
		if (currentX == targetX && currentY == targetY) {
			return true;
		}
		if (targetType == 6 || targetType == 7) {
			if (targetType == 7) {
				targetRotation = targetRotation + 2 & 0x3;
			}
			if (targetRotation == 0) {
				if (currentX == targetX + 1 && currentY == targetY && (clip[currentX][currentY] & Flags.WALLOBJ_WEST) == 0) {
					return true;
				}
				if (currentX == targetX && currentY == targetY - 1 && (clip[currentX][currentY] & Flags.WALLOBJ_NORTH) == 0) {
					return true;
				}
			} else if (targetRotation == 1) {
				if (currentX == targetX - 1 && currentY == targetY && (clip[currentX][currentY] & Flags.WALLOBJ_EAST) == 0) {
					return true;
				}
				if (currentX == targetX && currentY == targetY - 1 && (clip[currentX][currentY] & Flags.WALLOBJ_NORTH) == 0) {
					return true;
				}
			} else if (targetRotation == 2) {
				if (currentX == targetX - 1 && currentY == targetY && (clip[currentX][currentY] & Flags.WALLOBJ_EAST) == 0) {
					return true;
				}
				if (currentX == targetX && currentY == targetY + 1 && (clip[currentX][currentY] & Flags.WALLOBJ_SOUTH) == 0) {
					return true;
				}
			} else if (targetRotation == 3) {
				if (currentX == targetX + 1 && currentY == targetY && (clip[currentX][currentY] & Flags.WALLOBJ_WEST) == 0) {
					return true;
				}
				if (currentX == targetX && currentY == targetY + 1 && (clip[currentX][currentY] & Flags.WALLOBJ_SOUTH) == 0) {
					return true;
				}
			}
		} else if (targetType == 8) {
			if (currentX == targetX && currentY == targetY + 1 && (clip[currentX][currentY] & Flags.WALLOBJ_SOUTH) == 0) {
				return true;
			}
			if (currentX == targetX && currentY == targetY - 1 && (clip[currentX][currentY] & Flags.WALLOBJ_NORTH) == 0) {
				return true;
			}
			if (currentX == targetX - 1 && currentY == targetY && (clip[currentX][currentY] & Flags.WALLOBJ_EAST) == 0) {
				return true;
			}
			if (currentX == targetX + 1 && currentY == targetY && (clip[currentX][currentY] & Flags.WALLOBJ_WEST) == 0) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkWallInteract(int[][] flags, int x, int y, int sizeXY, int destX, int destY, int shape, int rotation) {
		if (1 == sizeXY) {
			if (x == destX && y == destY) {
				return true;
			}
		} else if (destX >= x && destX <= sizeXY + x - 1 && destY >= destY && destY <= sizeXY + destY - 1) {
			return true;
		}
		if (sizeXY == 1) {
			if (shape == 0) {
				if (rotation == 0) {
					if (destX - 1 == x && y == destY) {
						return true;
					}
					if (x == destX && 1 + destY == y && (flags[x][y] & 0x2c0120) == 0) {
						return true;
					}
					if (x == destX && destY - 1 == y && (flags[x][y] & 0x2c0102) == 0) {
						return true;
					}
				} else if (rotation == 1) {
					if (destX == x && 1 + destY == y) {
						return true;
					}
					if (destX - 1 == x && destY == y && (flags[x][y] & 0x2c0108) == 0) {
						return true;
					}
					if (destX + 1 == x && y == destY && 0 == (flags[x][y] & 0x2c0180)) {
						return true;
					}
				} else if (rotation == 2) {
					if (x == destX + 1 && destY == y) {
						return true;
					}
					if (destX == x && 1 + destY == y && 0 == (flags[x][y] & 0x2c0120)) {
						return true;
					}
					if (destX == x && destY - 1 == y && (flags[x][y] & 0x2c0102) == 0) {
						return true;
					}
				} else if (rotation == 3) {
					if (x == destX && y == destY - 1) {
						return true;
					}
					if (x == destX - 1 && destY == y && (flags[x][y] & 0x2c0108) == 0) {
						return true;
					}
					if (x == 1 + destX && y == destY && (flags[x][y] & 0x2c0180) == 0) {
						return true;
					}
				}
			}
			if (shape == 2) {
				if (rotation == 0) {
					if (x == destX - 1 && destY == y) {
						return true;
					}
					if (destX == x && y == 1 + destY) {
						return true;
					}
					if (destX + 1 == x && destY == y && (flags[x][y] & 0x2c0180) == 0) {
						return true;
					}
					if (x == destX && y == destY - 1 && 0 == (flags[x][y] & 0x2c0102)) {
						return true;
					}
				} else if (rotation == 1) {
					if (destX - 1 == x && y == destY && 0 == (flags[x][y] & 0x2c0108)) {
						return true;
					}
					if (x == destX && 1 + destY == y) {
						return true;
					}
					if (x == destX + 1 && destY == y) {
						return true;
					}
					if (destX == x && y == destY - 1 && (flags[x][y] & 0x2c0102) == 0) {
						return true;
					}
				} else if (rotation == 2) {
					if (destX - 1 == x && destY == y && (flags[x][y] & 0x2c0108) == 0) {
						return true;
					}
					if (x == destX && y == 1 + destY && 0 == (flags[x][y] & 0x2c0120)) {
						return true;
					}
					if (x == 1 + destX && y == destY) {
						return true;
					}
					if (destX == x && y == destY - 1) {
						return true;
					}
				} else if (rotation == 3) {
					if (x == destX - 1 && destY == y) {
						return true;
					}
					if (x == destX && 1 + destY == y && (flags[x][y] & 0x2c0120) == 0) {
						return true;
					}
					if (1 + destX == x && destY == y && (flags[x][y] & 0x2c0180) == 0) {
						return true;
					}
					if (x == destX && y == destY - 1) {
						return true;
					}
				}
			}
			if (shape == 9) {
				if (destX == x && y == destY + 1 && 0 == (flags[x][y] & 0x20)) {
					return true;
				}
				if (x == destX && y == destY - 1 && (flags[x][y] & 0x2) == 0) {
					return true;
				}
				if (destX - 1 == x && destY == y && (flags[x][y] & 0x8) == 0) {
					return true;
				}
				if (x == destX + 1 && y == destY && 0 == (flags[x][y] & 0x80)) {
					return true;
				}
			}
		} else {
			int endX = x + sizeXY - 1;
			int endY = y + sizeXY - 1;
			if (shape == 0) {
				if (rotation == 0) {
					if (x == destX - sizeXY && destY >= y && destY <= endY) {
						return true;
					}
					if (destX >= x && destX <= endX && y == 1 + destY && (flags[destX][y] & 0x2c0120) == 0) {
						return true;
					}
					if (destX >= x && destX <= endX && y == destY - sizeXY && (flags[destX][endY] & 0x2c0102) == 0) {
						return true;
					}
				} else if (rotation == 1) {
					if (destX >= x && destX <= endX && y == 1 + destY) {
						return true;
					}
					if (x == destX - sizeXY && destY >= y && destY <= endY && (flags[endX][destY] & 0x2c0108) == 0) {
						return true;
					}
					if (x == 1 + destX && destY >= y && destY <= endY && (flags[x][destY] & 0x2c0180) == 0) {
						return true;
					}
				} else if (rotation == 2) {
					if (1 + destX == x && destY >= y && destY <= endY) {
						return true;
					}
					if (destX >= x && destX <= endX && y == destY + 1 && 0 == (flags[destX][y] & 0x2c0120)) {
						return true;
					}
					if (destX >= x && destX <= endX && destY - sizeXY == y && 0 == (flags[destX][endY] & 0x2c0102)) {
						return true;
					}
				} else if (rotation == 3) {
					if (destX >= x && destX <= endX && y == destY - sizeXY) {
						return true;
					}
					if (x == destX - sizeXY && destY >= y && destY <= endY && 0 == (flags[endX][destY] & 0x2c0108)) {
						return true;
					}
					if (x == destX + 1 && destY >= y && destY <= endY && 0 == (flags[x][destY] & 0x2c0180)) {
						return true;
					}
				}
			}
			if (shape == 2) {
				if (rotation == 0) {
					if (x == destX - sizeXY && destY >= y && destY <= endY) {
						return true;
					}
					if (destX >= x && destX <= endX && 1 + destY == y) {
						return true;
					}
					if (1 + destX == x && destY >= y && destY <= endY && 0 == (flags[x][destY] & 0x2c0180)) {
						return true;
					}
					if (destX >= x && destX <= endX && destY - sizeXY == y && (flags[destX][endY] & 0x2c0102) == 0) {
						return true;
					}
				} else if (rotation == 1) {
					if (x == destX - sizeXY && destY >= y && destY <= endY && 0 == (flags[endX][destY] & 0x2c0108)) {
						return true;
					}
					if (destX >= x && destX <= endX && y == destY + 1) {
						return true;
					}
					if (x == destX + 1 && destY >= y && destY <= endY) {
						return true;
					}
					if (destX >= x && destX <= endX && y == destY - sizeXY && (flags[destX][endY] & 0x2c0102) == 0) {
						return true;
					}
				} else if (rotation == 2) {
					if (x == destX - sizeXY && destY >= y && destY <= endY && (flags[endX][destY] & 0x2c0108) == 0) {
						return true;
					}
					if (destX >= x && destX <= endX && y == destY + 1 && 0 == (flags[destX][y] & 0x2c0120)) {
						return true;
					}
					if (x == 1 + destX && destY >= y && destY <= endY) {
						return true;
					}
					if (destX >= x && destX <= endX && y == destY - sizeXY) {
						return true;
					}
				} else if (rotation == 3) {
					if (destX - sizeXY == x && destY >= y && destY <= endY) {
						return true;
					}
					if (destX >= x && destX <= endX && destY + 1 == y && 0 == (flags[destX][y] & 0x2c0120)) {
						return true;
					}
					if (x == destX + 1 && destY >= y && destY <= endY && 0 == (flags[x][destY] & 0x2c0180)) {
						return true;
					}
					if (destX >= x && destX <= endX && y == destY - sizeXY) {
						return true;
					}
				}
			}
			if (shape == 9) {
				if (destX >= x && destX <= endX && y == destY + 1 && (flags[destX][y] & 0x2c0120) == 0) {
					return true;
				}
				if (destX >= x && destX <= endX && destY - sizeXY == y && 0 == (flags[destX][endY] & 0x2c0102)) {
					return true;
				}
				if (destX - sizeXY == x && destY >= y && destY <= endY && (flags[endX][destY] & 0x2c0108) == 0) {
					return true;
				}
				if (1 + destX == x && destY >= y && destY <= endY && (flags[x][destY] & 0x2c0180) == 0) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Check's if we can interact filled rectangular (Might be ground object or npc
	 * or player etc) from current position.
	 */
	protected static boolean checkFilledRectangularInteract(int[][] clip, int currentX, int currentY, int sizeX, int sizeY, int targetX, int targetY, int targetSizeX, int targetSizeY, int accessBlockFlag) {
		int srcEndX = currentX + sizeX;
		int srcEndY = currentY + sizeY;
		int destEndX = targetX + targetSizeX;
		int destEndY = targetY + targetSizeY;
		if (destEndX == currentX && (accessBlockFlag & 0x2) == 0) { // can we enter from east ?
			int i_12_ = currentY > targetY ? currentY : targetY;
			for (int i_13_ = srcEndY < destEndY ? srcEndY : destEndY; i_12_ < i_13_; i_12_++) {
				if ((clip[destEndX - 1][i_12_] & 0x8) == 0) {
					return true;
				}
			}
		} else if (srcEndX == targetX && (accessBlockFlag & 0x8) == 0) { // can we enter from west ?
			int i_14_ = currentY > targetY ? currentY : targetY;
			for (int i_15_ = srcEndY < destEndY ? srcEndY : destEndY; i_14_ < i_15_; i_14_++) {
				if ((clip[targetX][i_14_] & 0x80) == 0) {
					return true;
				}
			}
		} else if (currentY == destEndY && (accessBlockFlag & 0x1) == 0) { // can we enter from north?
			int i_16_ = currentX > targetX ? currentX : targetX;
			for (int i_17_ = srcEndX < destEndX ? srcEndX : destEndX; i_16_ < i_17_; i_16_++) {
				if ((clip[i_16_][destEndY - 1] & 0x2) == 0) {
					return true;
				}
			}
		} else if (targetY == srcEndY && (accessBlockFlag & 0x4) == 0) { // can we enter from south?
			int i_18_ = currentX > targetX ? currentX : targetX;
			for (int i_19_ = srcEndX < destEndX ? srcEndX : destEndX; i_18_ < i_19_; i_18_++) {
				if ((clip[i_18_][targetY] & 0x20) == 0) {
					return true;
				}
			}
		}
		return false;
	}

}
