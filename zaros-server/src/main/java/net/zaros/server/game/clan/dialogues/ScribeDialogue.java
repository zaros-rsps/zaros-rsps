package net.zaros.server.game.clan.dialogues;

import lombok.RequiredArgsConstructor;
import net.zaros.server.game.clan.manager.ClanManager;
import net.zaros.server.game.content.dialogue.Dialogue;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.repository.npc.Npcs;

/**
 * Represents the clan camp npc 'Scribe' dialogue.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class ScribeDialogue extends Dialogue {

	/**
	 * The scribe npc id.
	 */
	private static final int npcId = Npcs.SCRIBE;

	/**
	 * Whether or not this dialogue is for 'Get-vexillum' option.
	 */
	private final boolean vexillum;

	/**
	 * The player which this dialogue is initated by.
	 */
	private Player player;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.content.dialogue.Dialogue#constructMessages(net.zaros.
	 * server.game.node.entity.player.Player)
	 */
	@Override
	public void constructMessages(Player player) {
		this.player = player;
		if (vexillum) {
			give_vexillum();
		} else {
			npc(npcId, E9847, "Good day, " + madamOrSir(player) + ". How may I be of assistance?");
			main_options();
		}
	}

	private void main_options() {
		if (player.hasClan()) {
			options(DEFAULT_OPTION, str("I'd like more information about clans.", "Tell me about yourself.", "Goodbye."), this::more_information, this::about_yourself, this::good_bye);
		} else {
			options(DEFAULT_OPTION, str("I'd like to start a clan.", "I'd like more information about clans.", "Tell me about yourself.", "Goodbye."), this::start_a_clan, this::more_information, this::about_yourself, this::good_bye);
		}
	}

	private void start_a_clan() {
		if (has_charter()) {
			var founders = ClanManager.getFounders(player);
			if (founders.size() != ClanManager.FOUNDERS_REQUIRED) {
				npc(npcId, E9759, "It appears you have an insufficient number of signatures", "on your charter, " + madamOrSir(player) + ". You will have to use your", "charter on " + (ClanManager.FOUNDERS_REQUIRED - founders.size()) + " more players before I can register your clan.");
				main_options();
			} else {
				// TODO:
			}
		} else {
			npc(npcId, E9848, "Very good, " + madamOrSir(player) + ". Would you like a demonstration of the", "process, or do you simply want a charter?");
			options(DEFAULT_OPTION, str("I'd like a demonstration, please.", "Just a charter, thanks."), this::demonstrate, this::give_charter);
		}
	}

	private void demonstrate() {
		npc(npcId, E9848, "TODO: The cutscene in here");
		main_options();
		// TODO:
	}

	private void give_charter() {
		npc(npcId, E9847, "Certainly. Here you are.");
		if (player.getInventory().hasFreeSlots()) {
			action(() -> player.getInventory().addItem(20707, 1));
			item(20707, 400, "The scribe gives you a charter.");
		} else {
			not_enough_space();
		}
	}

	private void more_information() {
		if (player.hasClan()) {
			options(DEFAULT_OPTION, str("What are the benefits of being in a clan?", "How do I recruit more clanmates?", "How do I leave a clan?", "More...", "Back..."), this::benefits, this::recruit, this::leave, () -> {
				options(DEFAULT_OPTION, str("What is a clan cloak?", "What is clan vexillum?", "Can I have a vexillum?", "Back..."), this::what_is_cloak, this::what_is_vexillum, this::give_vexillum, this::main_options);
			}, this::main_options);
		} else if (has_charter()) {
			options(DEFAULT_OPTION, str("What are the benefits of being in a clan?", "How do I join a clan?", "What is a clan cloak?", "What is a vexillum?", "Back..."), this::benefits, this::how_to_join, this::what_is_cloak, this::what_is_vexillum, this::main_options);
		} else {
			options(DEFAULT_OPTION, str("What are the benefits of being in a clan?", "How do I join a clan?", "Can I have a clan charter?", "More...", "Back..."), this::benefits, this::how_to_join, this::give_charter, () -> {
				options(DEFAULT_OPTION, str("What is a clan cloak?", "What is a vexillum?", "Back..."), this::what_is_cloak, this::what_is_vexillum, this::main_options);
			}, this::main_options);
		}
	}

	private void recruit() {
		// TODO:
	}

	private void leave() {
		npc(npcId, E9808, "To leave your clan, you must click the 'leave clan' button", "on the clan list.");
		npc(npcId, E9759, "Remember, however, that if you are the last remaining", "member of the clan, leaving will permanently dissolve the", "clan.");
		npc(npcId, E9808, "Alternatively, I can remove you from your clan now, if", "you wish");
		options("Do you want to leave your clan?", str("Yes.", "No."), this::perform_leave, this::main_options);
	}

	private void perform_leave() {
		// TODO:
	}

	private void benefits() {
		npc(npcId, E9848, "I'm so glad you asked me that, madam. Allow me to", "explain.");
		npc(npcId, E9848, "TODO: The cutscene in here");
		main_options();
	}

	private void what_is_cloak() {
		npc(npcId, E9848, "A clan cloak identifies you as a member of a clan, and", "displays your clan's motif");
		npc(npcId, E9844, "The Captain of the Guard can provide you with one, if you", "wish.");
		main_options();
	}

	private void what_is_vexillum() {
		npc(npcId, E9848, "A vexillum is a banner that displays your clan's colours and", "emblem.");
		npc(npcId, E9848, "It can be carried, displayed, or used to recruit new", "clanmates.");
		npc(npcId, E9809, "If you place the vexillum in the ground, others can use it", "to find out more about your clan, and contact you if they", "are interested in joining.");
		npc(npcId, E9844, "You can also use the vexillum directly on other players to", "invite them to join your clan.");
		npc(npcId, E9847, "Finally, you can use the vexillum to teleport directly here.");
		main_options();
	}

	private void how_to_join() {
		npc(npcId, E9848, "To join a clan you must be invited to do so by one of the", "clan's recruiters.");
		npc(npcId, E9844, "You can only be a member of one clan at a time, so", "joining is a serious commitment.");
		main_options();
	}

	private void about_yourself() {
		npc(npcId, E9847, "Clan scribe Amos Twinly at your service, " + madamOrSir(player) + ".");
		npc(npcId, E9844, "I keep a record of all clans, and distribute clan charters", "to those who wish to start a clan.");
		npc(npcId, E9848, "If you are a member of a clan, I can also provide you with", "a vexillum - a banner displaying your clan's motif.");
		main_options();
	}

	private void give_vexillum() {
		player(E9847, "Can I have a clan vexillum?");
		if (player.hasClan()) {
			npc(npcId, E9827, "It would be my pleasure, " + madamOrSir(player) + ".");
			if (player.getInventory().getFreeSlots() > 1) {
				action(() -> player.getInventory().addItem(20709, 1));
				item(20709, 1, "The scribe gives you a vexillum.");
			} else {
				not_enough_space();
			}
		} else {
			npc(npcId, E9827, "I'm afraid I can only give a vexillum to a member of a", "clan. You will have to join a clan before I can give you a", "vexillum, " + madamOrSir(player) + ".");
		}
	}

	private void good_bye() {
		npc(npcId, E9843, "Goodbye, " + madamOrSir(player) + ".");
	}

	private void not_enough_space() {
		npc(npcId, E9827, "I would be happy to oblige, " + madamOrSir(player) + ", but it appears you do not", "have enough room in your backpack. Come back when you", "have more space.");
	}

	private boolean has_charter() {
		return player.getInventory().containsItem(Items.CLAN_CHARTER);
	}
}
