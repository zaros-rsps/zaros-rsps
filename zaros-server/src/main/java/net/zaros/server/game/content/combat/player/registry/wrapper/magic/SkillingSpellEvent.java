package net.zaros.server.game.content.combat.player.registry.wrapper.magic;

import net.zaros.server.game.content.combat.player.registry.CombatRegistryEvent;
import net.zaros.server.utility.rs.constant.MagicConstants;

public interface SkillingSpellEvent extends MagicSpellEvent, CombatRegistryEvent, MagicConstants {

    /**
     * The level required to use the spell
     */
    int levelRequired();

    /**
     * The runes that are required
     */
    int[] runesRequired();

}
