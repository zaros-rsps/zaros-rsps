package net.zaros.server.game.content.market.lending;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.node.item.Item;

import java.util.Date;

public class LentItem {

    public LentItem(String playerLent, Item lentItem, int duration) {
        this.playerLent = playerLent;
        this.lentItem = lentItem;
        this.lentTime = new Date().getTime();
        this.duration = duration;
    }

    @Getter
    @Setter
    public String playerLent;

    @Getter
    @Setter
    public Item lentItem;

    @Getter
    @Setter
    public long lentTime, endTime;

    @Getter
    @Setter
    public int duration;

}
