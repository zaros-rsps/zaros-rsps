package net.zaros.server.game.content.skills;

import lombok.Getter;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;
import net.zaros.server.game.world.World;
import net.zaros.server.utility.rs.constant.SkillConstants;

import java.util.HashMap;
import java.util.Map;

public enum SkillingResource {

    /**
     * Standard tree (Woodcutting).
     */
    STANDARD_TREE_1(1276, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_2(1277, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1343, SkillConstants.WOODCUTTING), STANDARD_TREE_3(1278, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_4(1279, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1345, SkillConstants.WOODCUTTING), STANDARD_TREE_5(1280, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1343, SkillConstants.WOODCUTTING), STANDARD_TREE_6(1330, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1341, SkillConstants.WOODCUTTING), STANDARD_TREE_7(1331, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1341, SkillConstants.WOODCUTTING), STANDARD_TREE_8(1332, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1341, SkillConstants.WOODCUTTING), STANDARD_TREE_9(2409, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_10(3033, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1345, SkillConstants.WOODCUTTING), STANDARD_TREE_11(3034, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1345, SkillConstants.WOODCUTTING), STANDARD_TREE_12(3035, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1347, SkillConstants.WOODCUTTING), STANDARD_TREE_13(3036, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1351, SkillConstants.WOODCUTTING), STANDARD_TREE_14(3879, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 3880, SkillConstants.WOODCUTTING), STANDARD_TREE_15(3881, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 3880, SkillConstants.WOODCUTTING), STANDARD_TREE_16(3882, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 3880, SkillConstants.WOODCUTTING), STANDARD_TREE_17(3883, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 3884, SkillConstants.WOODCUTTING), STANDARD_TREE_18(10041, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_19(14308, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_20(14309, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_21(16264, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_22(16265, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_23(30132, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_24(30133, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_25(37477, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 1342, SkillConstants.WOODCUTTING), STANDARD_TREE_26(37478, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 37653, SkillConstants.WOODCUTTING), STANDARD_TREE_27(37652, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "tree", null, 37653, SkillConstants.WOODCUTTING),

    IVY(46324, 60, 0.10, 50 | 100 << 16, 125.0, -1, 1, "Ivy", null, 46325, SkillConstants.WOODCUTTING),

    /**
     * Fruit trees.
     */
    APPLE_TREE(7941, 1, 0.05, 50 | 100 << 16, 25.0, -1, 1, "tree", null, 37653, SkillConstants.WOODCUTTING, true), BANANA_TREE(8000, 1, 0.05, 50 | 100 << 16, 25.0, -1, 1, "tree", null, 37653, SkillConstants.WOODCUTTING, true), ORANGE_TREE(8057, 1, 0.05, 50 | 100 << 16, 25.0, -1, 1, "tree", null, 37653, SkillConstants.WOODCUTTING, true), CURRY_TREE(8026, 1, 0.05, 50 | 100 << 16, 25.0, -1, 1, "tree", null, 37653, SkillConstants.WOODCUTTING, true), PINEAPPLE_TREE(7972, 1, 0.05, 50 | 100 << 16, 25.0, -1, 1, "tree", null, 37653, SkillConstants.WOODCUTTING, true), PAPAYA_TREE(8111, 1, 0.05, 50 | 100 << 16, 25.0, -1, 1, "tree", null, 37653, SkillConstants.WOODCUTTING, true), PALM_TREE(8084, 1, 0.05, 50 | 100 << 16, 25.0, -1, 1, "tree", null, 37653, SkillConstants.WOODCUTTING, true),

    /**
     * Dead tree (Woodcutting).
     */
    DEAD_TREE_1(1282, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1347, SkillConstants.WOODCUTTING), DEAD_TREE_2(1283, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1347, SkillConstants.WOODCUTTING), DEAD_TREE_3(1284, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1348, SkillConstants.WOODCUTTING), DEAD_TREE_4(1285, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1349, SkillConstants.WOODCUTTING), DEAD_TREE_5(1286, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1351, SkillConstants.WOODCUTTING), DEAD_TREE_6(1289, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1353, SkillConstants.WOODCUTTING), DEAD_TREE_7(1290, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1354, SkillConstants.WOODCUTTING), DEAD_TREE_8(1291, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 23054, SkillConstants.WOODCUTTING), DEAD_TREE_9(1365, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1352, SkillConstants.WOODCUTTING), DEAD_TREE_10(1383, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1358, SkillConstants.WOODCUTTING), DEAD_TREE_11(1384, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1359, SkillConstants.WOODCUTTING), DEAD_TREE_12(5902, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1347, SkillConstants.WOODCUTTING), DEAD_TREE_13(5903, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1353, SkillConstants.WOODCUTTING), DEAD_TREE_14(5904, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1353, SkillConstants.WOODCUTTING), DEAD_TREE_15(32294, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1353, SkillConstants.WOODCUTTING), DEAD_TREE_16(37481, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1347, SkillConstants.WOODCUTTING), DEAD_TREE_17(37482, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1351, SkillConstants.WOODCUTTING), DEAD_TREE_18(37483, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dead tree", null, 1358, SkillConstants.WOODCUTTING), DEAD_TREE_19(24168, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "dying tree", null, 24169, SkillConstants.WOODCUTTING),

    /**
     * Dramen tree (Woodcutting/Lost city quest).
     */
    DRAMEN_TREE(1292, 1, 0.05, -1, 25.0, 771, Integer.MAX_VALUE, "dramen tree", null, -1, SkillConstants.WOODCUTTING),

    /**
     * Evergreen (Woodcutting).
     */
    EVERGREEN_1(1315, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "evergreen", null, 1342, SkillConstants.WOODCUTTING), EVERGREEN_2(1316, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "evergreen", null, 1355, SkillConstants.WOODCUTTING), EVERGREEN_3(1318, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "evergreen", null, 1355, SkillConstants.WOODCUTTING), EVERGREEN_4(1319, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 1, "evergreen", null, 1355, SkillConstants.WOODCUTTING),

    /**
     * Jungle tree (Woodcutting).
     */
    JUNGLE_TREE_1(2887, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 2, "jungle tree", null, 0, SkillConstants.WOODCUTTING), JUNGLE_TREE_2(2889, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 2, "jungle tree", null, 0, SkillConstants.WOODCUTTING), JUNGLE_TREE_3(2890, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 2, "jungle tree", null, 0, SkillConstants.WOODCUTTING), JUNGLE_TREE_4(4818, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 2, "jungle tree", null, 0, SkillConstants.WOODCUTTING), JUNGLE_TREE_5(4820, 1, 0.05, 50 | 100 << 16, 25.0, 1511, 2, "jungle tree", null, 0, SkillConstants.WOODCUTTING),

    JUNGLE_BUSH_1(2892, 1, 0.15, 50 | 100 << 16, 100.0, 1511, 1, "jungle bush", null, 2894, SkillConstants.WOODCUTTING), JUNGLE_BUSH_2(2893, 1, 0.15, 50 | 100 << 16, 100.0, 1511, 1, "jungle bush", null, 2895, SkillConstants.WOODCUTTING),

    /**
     * Achey tree (Woodcutting).
     */
    ACHEY_TREE(2023, 1, 0.05, 50 | 100 << 16, 25.0, 2862, 1, "achey tree", null, 3371, SkillConstants.WOODCUTTING),

    /**
     * Oak tree (Woodcutting).
     */
    OAK_TREE_1(1281, 15, 0.15, 14 | 22 << 16, 37.5, 1521, 10, "oak tree", null, 1356, SkillConstants.WOODCUTTING), OAK_TREE_2(3037, 15, 0.15, 14 | 22 << 16, 37.5, 1521, 10, "oak tree", null, 1357, SkillConstants.WOODCUTTING), OAK_TREE_3(37479, 15, 0.15, 14 | 22 << 16, 37.5, 1521, 10, "oak tree", null, 1356, SkillConstants.WOODCUTTING), OAK_TREE_4(8467, 15, 0.15, 14 | 22 << 16, 37.5, 1521, 10, "oak tree", null, 1356, SkillConstants.WOODCUTTING, true),

    /**
     * Willow tree (Woodcutting).
     */
    WILLOW_TREE_1(1308, 30, 0.3, 14 | 22 << 16, 67.8, 1519, 20, "willow tree", null, 7399, SkillConstants.WOODCUTTING), WILLOW_TREE_2(5551, 30, 0.3, 14 | 22 << 16, 67.8, 1519, 20, "willow tree", null, 5554, SkillConstants.WOODCUTTING), WILLOW_TREE_3(5552, 30, 0.3, 14 | 22 << 16, 67.8, 1519, 20, "willow tree", null, 5554, SkillConstants.WOODCUTTING), WILLOW_TREE_4(5553, 30, 0.3, 14 | 22 << 16, 67.8, 1519, 20, "willow tree", null, 5554, SkillConstants.WOODCUTTING), WILLOW_TREE_5(37480, 30, 0.3, 14 | 22 << 16, 67.8, 1519, 20, "willow tree", null, 7399, SkillConstants.WOODCUTTING), WILLOW_TREE_6(8488, 30, 0.3, 14 | 22 << 16, 67.8, 1519, 20, "willow tree", null, 7399, SkillConstants.WOODCUTTING, true),

    /**
     * Teak (Woodcutting).
     */
    TEAK_1(9036, 35, 0.7, 35 | 60 << 16, 85.0, 6333, 25, "teak", null, 9037, SkillConstants.WOODCUTTING), TEAK_2(15062, 35, 0.7, 35 | 60 << 16, 85.0, 6333, 25, "teak", null, 9037, SkillConstants.WOODCUTTING),

    /**
     * Maple tree (Woodcutting).
     */
    MAPLE_TREE_1(1307, 45, 0.65, 58 | 100 << 16, 100.0, 1517, 30, "maple tree", null, 7400, SkillConstants.WOODCUTTING), MAPLE_TREE_2(4674, 45, 0.65, 58 | 100 << 16, 100.0, 1517, 30, "maple tree", null, 7400, SkillConstants.WOODCUTTING), MAPLE_TREE_3(8444, 45, 0.65, 58 | 100 << 16, 100.0, 1517, 30, "maple tree", null, 7400, SkillConstants.WOODCUTTING, true),

    /**
     * Hollow tree (Woodcutting).
     */
    HOLLOW_TREE_1(2289, 45, 0.6, 58 | 100 << 16, 82.5, 3239, 30, "hollow tree", null, 2310, SkillConstants.WOODCUTTING), HOLLOW_TREE_2(4060, 45, 0.6, 58 | 100 << 16, 82.5, 3239, 30, "hollow tree", null, 4061, SkillConstants.WOODCUTTING),

    /**
     * Mahogany (Woodcutting).
     */
    MAHOGANY(9034, 50, 0.7, 62 | 115 << 16, 125.0, 6332, 35, "mahogany", null, 9035, SkillConstants.WOODCUTTING),

    /**
     * Arctic pine (Woodcutting).
     */
    ARCTIC_PINE(21273, 54, 0.73, 75 | 130 << 16, 140.2, 10810, 35, "arctic pine", null, 21274, SkillConstants.WOODCUTTING),

    /**
     * Eucalyptus tree (Woodcutting).
     */
    EUCALYPTUS_1(28951, 58, 0.77, 80 | 140 << 16, 165.0, 12581, 35, "eucalyptus tree", null, 28954, SkillConstants.WOODCUTTING), EUCALYPTUS_2(28952, 58, 0.77, 80 | 140 << 16, 165.0, 12581, 35, "eucalyptus tree", null, 28955, SkillConstants.WOODCUTTING), EUCALYPTUS_3(28953, 58, 0.77, 80 | 140 << 16, 165.0, 12581, 35, "eucalyptus tree", null, 28956, SkillConstants.WOODCUTTING),

    /**
     * Yew tree (Woodcutting).
     */
    YEW(1309, 60, 0.8, 100 | 162 << 16, 175.0, 1515, 40, "yew", null, 7402, SkillConstants.WOODCUTTING), YEW_1(8513, 60, 0.8, 100 | 162 << 16, 175.0, 1515, 40, "yew", null, 7402, SkillConstants.WOODCUTTING, true),

    /**
     * Magic tree (Woodcutting).
     */
    MAGIC_TREE_1(1306, 75, 0.9, 200 | 317 << 16, 250.0, 1513, 50, "magic tree", null, 7401, SkillConstants.WOODCUTTING), MAGIC_TREE_2(37823, 75, 0.9, 200 | 317 << 16, 250.0, 1513, 50, "magic tree", null, 37824, SkillConstants.WOODCUTTING), MAGIC_TREE_3(8409, 75, 0.9, 200 | 317 << 16, 250.0, 1513, 50, "magic tree", null, 37824, SkillConstants.WOODCUTTING, true),

    CURSED_MAGIC_TREE(37821, 82, 0.95, 200 | 317 << 16, 275.0, 1513, 50, "magic tree", null, 37822, SkillConstants.WOODCUTTING),

    /**
     * Copper ore (Mining).
     */
    COPPER_ORE_0(2090, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 450, SkillConstants.MINING), COPPER_ORE_1(2091, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 452, SkillConstants.MINING), COPPER_ORE_2(4976, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 4994, SkillConstants.MINING), COPPER_ORE_3(4977, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 4995, SkillConstants.MINING), COPPER_ORE_4(4978, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 4996, SkillConstants.MINING), COPPER_ORE_5(9710, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 18954, SkillConstants.MINING), COPPER_ORE_6(9709, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 32448, SkillConstants.MINING), COPPER_ORE_7(9708, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 32447, SkillConstants.MINING), COPPER_ORE_8(11960, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 11555, SkillConstants.MINING), COPPER_ORE_9(11961, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 11556, SkillConstants.MINING), COPPER_ORE_10(11962, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 11557, SkillConstants.MINING), COPPER_ORE_11(11937, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 11553, SkillConstants.MINING), COPPER_ORE_12(11936, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 11552, SkillConstants.MINING), COPPER_ORE_13(11938, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 11554, SkillConstants.MINING), COPPER_ORE_14(12746, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 450, SkillConstants.MINING), COPPER_ORE_15(14906, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 14894, SkillConstants.MINING), COPPER_ORE_16(14907, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 14895, SkillConstants.MINING), COPPER_ORE_17(20448, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 20445, SkillConstants.MINING), COPPER_ORE_18(20451, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 20445, SkillConstants.MINING), COPPER_ORE_19(20446, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 20443, SkillConstants.MINING), COPPER_ORE_20(20447, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 20444, SkillConstants.MINING), COPPER_ORE_21(20408, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 20407, SkillConstants.MINING), COPPER_ORE_22(18993, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 19005, SkillConstants.MINING), COPPER_ORE_23(18992, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 19004, SkillConstants.MINING), COPPER_ORE_24(19007, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 19016, SkillConstants.MINING), COPPER_ORE_25(19006, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 19021, SkillConstants.MINING), COPPER_ORE_26(18991, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 19003, SkillConstants.MINING), COPPER_ORE_27(19008, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 19017, SkillConstants.MINING), COPPER_ORE_28(21285, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 21297, SkillConstants.MINING), COPPER_ORE_29(21284, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 21296, SkillConstants.MINING), COPPER_ORE_30(21286, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 21298, SkillConstants.MINING), COPPER_ORE_31(29231, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 29219, SkillConstants.MINING), COPPER_ORE_32(29230, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 29218, SkillConstants.MINING), COPPER_ORE_33(29232, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 29220, SkillConstants.MINING), COPPER_ORE_34(31082, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 37650, SkillConstants.MINING), COPPER_ORE_35(31081, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 37649, SkillConstants.MINING), COPPER_ORE_36(31080, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 37639, SkillConstants.MINING), COPPER_ORE_37(37647, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 37650, SkillConstants.MINING), COPPER_ORE_38(37646, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 37649, SkillConstants.MINING), COPPER_ORE_39(37645, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 37639, SkillConstants.MINING), COPPER_ORE_40(37637, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 37639, SkillConstants.MINING), COPPER_ORE_41(37688, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 21298, SkillConstants.MINING), COPPER_ORE_42(37686, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 21296, SkillConstants.MINING), COPPER_ORE_43(37687, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 21297, SkillConstants.MINING), COPPER_ORE_44(3042, 1, 0.05, 4 | 8 << 16, 17.5, 436, 1, "copper rocks", null, 450, SkillConstants.MINING),

    /**
     * Tin ore (Mining).
     */
    TIN_ORE_0(2094, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 450, SkillConstants.MINING), TIN_ORE_1(2095, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 452, SkillConstants.MINING), TIN_ORE_2(3043, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 450, SkillConstants.MINING), TIN_ORE_3(4979, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 4994, SkillConstants.MINING), TIN_ORE_4(4980, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 4995, SkillConstants.MINING), TIN_ORE_5(4981, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 4996, SkillConstants.MINING), TIN_ORE_6(11957, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 11555, SkillConstants.MINING), TIN_ORE_7(11958, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 11556, SkillConstants.MINING), TIN_ORE_8(11959, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 11557, SkillConstants.MINING), TIN_ORE_9(11934, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 11553, SkillConstants.MINING), TIN_ORE_10(11935, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 11554, SkillConstants.MINING), TIN_ORE_11(11933, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 11552, SkillConstants.MINING), TIN_ORE_12(14902, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 14894, SkillConstants.MINING), TIN_ORE_13(14903, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 14895, SkillConstants.MINING), TIN_ORE_14(18995, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 19004, SkillConstants.MINING), TIN_ORE_15(18994, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 19003, SkillConstants.MINING), TIN_ORE_16(18996, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 19005, SkillConstants.MINING), TIN_ORE_17(19025, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 19016, SkillConstants.MINING), TIN_ORE_18(19024, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 19021, SkillConstants.MINING), TIN_ORE_19(19026, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 19017, SkillConstants.MINING), TIN_ORE_20(21293, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 21296, SkillConstants.MINING), TIN_ORE_21(21295, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 21298, SkillConstants.MINING), TIN_ORE_22(21294, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 21297, SkillConstants.MINING), TIN_ORE_23(29227, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 29218, SkillConstants.MINING), TIN_ORE_24(29229, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 29220, SkillConstants.MINING), TIN_ORE_25(29228, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 29219, SkillConstants.MINING), TIN_ORE_26(31079, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 37650, SkillConstants.MINING), TIN_ORE_27(31078, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 37649, SkillConstants.MINING), TIN_ORE_28(31077, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 37639, SkillConstants.MINING), TIN_ORE_29(37644, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 37650, SkillConstants.MINING), TIN_ORE_30(37643, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 37649, SkillConstants.MINING), TIN_ORE_31(37642, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 37639, SkillConstants.MINING), TIN_ORE_32(37638, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 37639, SkillConstants.MINING), TIN_ORE_33(37685, 1, 0.05, 4 | 8 << 16, 17.5, 438, 1, "tin rocks", null, 21298, SkillConstants.MINING),

    /**
     * Rune/Pure essence (Mining).
     */
    RUNE_ESSENCE(2491, 1, 0.1, 1 | 1 << 16, 5.0, 1436, Integer.MAX_VALUE, "rune essence", null, -1, SkillConstants.MINING),

    /**
     * Clay (Mining).
     */
    CLAY_0(2109, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 452, SkillConstants.MINING), CLAY_1(2108, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 450, SkillConstants.MINING), CLAY_2(9712, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 32448, SkillConstants.MINING), CLAY_3(9713, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 18954, SkillConstants.MINING), CLAY_4(9711, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 32447, SkillConstants.MINING), CLAY_5(10949, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 10945, SkillConstants.MINING), CLAY_6(11190, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 21297, SkillConstants.MINING), CLAY_7(11191, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 21298, SkillConstants.MINING), CLAY_8(11189, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 21296, SkillConstants.MINING), CLAY_9(12942, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 4995, SkillConstants.MINING), CLAY_10(12943, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 4996, SkillConstants.MINING), CLAY_11(12941, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 4994, SkillConstants.MINING), CLAY_12(14904, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 14894, SkillConstants.MINING), CLAY_13(14905, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 14895, SkillConstants.MINING), CLAY_14(15505, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 11557, SkillConstants.MINING), CLAY_15(15504, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 11556, SkillConstants.MINING), CLAY_16(15503, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 11555, SkillConstants.MINING), CLAY_17(20449, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 20443, SkillConstants.MINING), CLAY_18(20450, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 20444, SkillConstants.MINING), CLAY_19(20409, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 20407, SkillConstants.MINING), CLAY_20(32429, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 33400, SkillConstants.MINING), CLAY_21(32430, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 33401, SkillConstants.MINING), CLAY_22(32431, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 33402, SkillConstants.MINING), CLAY_23(31062, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 37639, SkillConstants.MINING), CLAY_24(31063, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 37649, SkillConstants.MINING), CLAY_25(31064, 1, 0.1, 1 | 1 << 16, 5.0, 434, 1, "clay", null, 37650, SkillConstants.MINING),

    /**
     * Limestone.
     */
    LIMESTONE_0(4027, 10, 0.2, 10 | 20 << 16, 26.5, 3211, 1, "limestone", null, 12564, SkillConstants.MINING), LIMESTONE_1(4028, 10, 0.2, 10 | 20 << 16, 26.5, 3211, 1, "limestone", null, 12565, SkillConstants.MINING), LIMESTONE_2(4029, 10, 0.2, 10 | 20 << 16, 26.5, 3211, 1, "limestone", null, 12566, SkillConstants.MINING), LIMESTONE_3(4030, 10, 0.2, 10 | 20 << 16, 26.5, 3211, 1, "limestone", null, 12567, SkillConstants.MINING),

    /**
     * Blurite ore.
     */
    BLURITE_ORE_0(33220, 10, 0.2, 10 | 20 << 16, 17.5, 668, 1, "blurite rocks", null, 33222, SkillConstants.MINING), BLURITE_ORE_1(33221, 10, 0.2, 10 | 20 << 16, 17.5, 668, 1, "blurite rocks", null, 33223, SkillConstants.MINING),

    /**
     * Iron ore.
     */
    IRON_ORE_0(2092, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 450, SkillConstants.MINING), IRON_ORE_1(2093, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 452, SkillConstants.MINING), IRON_ORE_2(4982, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 4994, SkillConstants.MINING), IRON_ORE_3(4983, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 4995, SkillConstants.MINING), IRON_ORE_4(4984, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 4996, SkillConstants.MINING), IRON_ORE_5(6943, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 21296, SkillConstants.MINING), IRON_ORE_6(6944, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 21297, SkillConstants.MINING), IRON_ORE_7(9718, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 32448, SkillConstants.MINING), IRON_ORE_8(9719, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 18954, SkillConstants.MINING), IRON_ORE_9(9717, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 32447, SkillConstants.MINING), IRON_ORE_10(11956, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 11557, SkillConstants.MINING), IRON_ORE_11(11954, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 11555, SkillConstants.MINING), IRON_ORE_12(11955, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 11556, SkillConstants.MINING), IRON_ORE_13(14914, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 14895, SkillConstants.MINING), IRON_ORE_14(14913, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 14894, SkillConstants.MINING), IRON_ORE_15(14858, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 25373, SkillConstants.MINING), IRON_ORE_16(14857, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 25372, SkillConstants.MINING), IRON_ORE_17(14856, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 25371, SkillConstants.MINING), IRON_ORE_18(14900, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 14894, SkillConstants.MINING), IRON_ORE_19(14901, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 14895, SkillConstants.MINING), IRON_ORE_20(20423, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 20444, SkillConstants.MINING), IRON_ORE_21(20422, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 20443, SkillConstants.MINING), IRON_ORE_22(20425, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 20407, SkillConstants.MINING), IRON_ORE_23(20424, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 20445, SkillConstants.MINING), IRON_ORE_24(19002, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 19005, SkillConstants.MINING), IRON_ORE_25(19001, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 19004, SkillConstants.MINING), IRON_ORE_26(19000, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 19003, SkillConstants.MINING), IRON_ORE_27(21281, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 21296, SkillConstants.MINING), IRON_ORE_28(21283, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 21298, SkillConstants.MINING), IRON_ORE_29(21282, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 21297, SkillConstants.MINING), IRON_ORE_30(29221, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 29218, SkillConstants.MINING), IRON_ORE_31(29223, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 29220, SkillConstants.MINING), IRON_ORE_32(29222, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 29219, SkillConstants.MINING), IRON_ORE_33(32441, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 33400, SkillConstants.MINING), IRON_ORE_34(32443, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 33402, SkillConstants.MINING), IRON_ORE_35(32442, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 33401, SkillConstants.MINING), IRON_ORE_36(32452, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 32448, SkillConstants.MINING), IRON_ORE_37(32451, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 32447, SkillConstants.MINING), IRON_ORE_38(31073, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 37650, SkillConstants.MINING), IRON_ORE_39(31072, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 37649, SkillConstants.MINING), IRON_ORE_40(31071, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 37639, SkillConstants.MINING), IRON_ORE_41(37307, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 11552, SkillConstants.MINING), IRON_ORE_42(37309, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 11554, SkillConstants.MINING), IRON_ORE_43(37308, 15, 0.2, 15 | 25 << 16, 35.0, 440, 1, "iron rocks", null, 11553, SkillConstants.MINING),

    /**
     * Silver ore.
     */
    SILVER_ORE_0(2101, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 452, SkillConstants.MINING), SILVER_ORE_1(2100, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 450, SkillConstants.MINING), SILVER_ORE_2(6945, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 21296, SkillConstants.MINING), SILVER_ORE_3(6946, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 21297, SkillConstants.MINING), SILVER_ORE_4(9716, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 18954, SkillConstants.MINING), SILVER_ORE_5(9714, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 32447, SkillConstants.MINING), SILVER_ORE_6(9715, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 32448, SkillConstants.MINING), SILVER_ORE_7(11188, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 21298, SkillConstants.MINING), SILVER_ORE_8(11186, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 21296, SkillConstants.MINING), SILVER_ORE_9(11187, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 21297, SkillConstants.MINING), SILVER_ORE_10(15581, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 14834, SkillConstants.MINING), SILVER_ORE_11(15580, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 14833, SkillConstants.MINING), SILVER_ORE_12(15579, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 14832, SkillConstants.MINING), SILVER_ORE_13(16998, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 14915, SkillConstants.MINING), SILVER_ORE_14(16999, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 14916, SkillConstants.MINING), SILVER_ORE_15(17007, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 14915, SkillConstants.MINING), SILVER_ORE_16(17000, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 31061, SkillConstants.MINING), SILVER_ORE_17(17009, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 31061, SkillConstants.MINING), SILVER_ORE_18(17008, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 14916, SkillConstants.MINING), SILVER_ORE_19(17385, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 32447, SkillConstants.MINING), SILVER_ORE_20(17387, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 18954, SkillConstants.MINING), SILVER_ORE_21(17386, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 32448, SkillConstants.MINING), SILVER_ORE_22(29225, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 29219, SkillConstants.MINING), SILVER_ORE_23(29224, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 29218, SkillConstants.MINING), SILVER_ORE_24(29226, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 29220, SkillConstants.MINING), SILVER_ORE_25(32445, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 33401, SkillConstants.MINING), SILVER_ORE_26(32444, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 33400, SkillConstants.MINING), SILVER_ORE_27(32446, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 33402, SkillConstants.MINING), SILVER_ORE_28(31075, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 37649, SkillConstants.MINING), SILVER_ORE_29(31074, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 37639, SkillConstants.MINING), SILVER_ORE_30(31076, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 37650, SkillConstants.MINING), SILVER_ORE_31(37305, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 11553, SkillConstants.MINING), SILVER_ORE_32(37304, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 11552, SkillConstants.MINING), SILVER_ORE_33(37306, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 11554, SkillConstants.MINING), SILVER_ORE_34(37670, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 11552, SkillConstants.MINING), SILVER_ORE_35(11948, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 11555, SkillConstants.MINING), SILVER_ORE_36(11949, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 11556, SkillConstants.MINING), SILVER_ORE_37(11950, 20, 0.3, 100 | 200 << 16, 40.0, 442, 1, "silver rocks", null, 11557, SkillConstants.MINING),

    /**
     * Coal.
     */
    COAL_0(2097, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 452, SkillConstants.MINING), COAL_1(2096, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 450, SkillConstants.MINING), COAL_2(4985, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 4994, SkillConstants.MINING), COAL_3(4986, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 4995, SkillConstants.MINING), COAL_4(4987, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 4996, SkillConstants.MINING), COAL_5(4676, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 450, SkillConstants.MINING), COAL_6(10948, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 10944, SkillConstants.MINING), COAL_7(11964, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 11556, SkillConstants.MINING), COAL_8(11965, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 11557, SkillConstants.MINING), COAL_9(11963, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 11555, SkillConstants.MINING), COAL_10(11932, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 11554, SkillConstants.MINING), COAL_11(11930, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 11552, SkillConstants.MINING), COAL_12(11931, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 11553, SkillConstants.MINING), COAL_13(15246, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 15249, SkillConstants.MINING), COAL_14(15247, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 15250, SkillConstants.MINING), COAL_15(15248, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 15251, SkillConstants.MINING), COAL_16(14852, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 25373, SkillConstants.MINING), COAL_17(14851, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 25372, SkillConstants.MINING), COAL_18(14850, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 25371, SkillConstants.MINING), COAL_19(20410, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 20443, SkillConstants.MINING), COAL_20(20411, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 20444, SkillConstants.MINING), COAL_21(20412, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 20445, SkillConstants.MINING), COAL_22(20413, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 20407, SkillConstants.MINING), COAL_23(18999, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 19005, SkillConstants.MINING), COAL_24(18998, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 19004, SkillConstants.MINING), COAL_25(18997, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 19003, SkillConstants.MINING), COAL_26(21287, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 21296, SkillConstants.MINING), COAL_27(21289, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 21298, SkillConstants.MINING), COAL_28(21288, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 21297, SkillConstants.MINING), COAL_29(23565, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 21298, SkillConstants.MINING), COAL_30(23564, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 21297, SkillConstants.MINING), COAL_31(23563, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 21296, SkillConstants.MINING), COAL_32(29215, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 29218, SkillConstants.MINING), COAL_33(29217, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 29220, SkillConstants.MINING), COAL_34(29216, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 29219, SkillConstants.MINING), COAL_35(32426, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 33400, SkillConstants.MINING), COAL_36(32427, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 33401, SkillConstants.MINING), COAL_37(32428, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 33402, SkillConstants.MINING), COAL_38(32450, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 32448, SkillConstants.MINING), COAL_39(32449, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 32447, SkillConstants.MINING), COAL_40(31068, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 37639, SkillConstants.MINING), COAL_41(31069, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 37649, SkillConstants.MINING), COAL_42(31070, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 37650, SkillConstants.MINING), COAL_43(31168, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 14833, SkillConstants.MINING), COAL_44(31169, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 14834, SkillConstants.MINING), COAL_45(31167, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 14832, SkillConstants.MINING), COAL_46(37699, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 21298, SkillConstants.MINING), COAL_47(37698, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 21297, SkillConstants.MINING), COAL_48(37697, 30, 0.4, 50 | 100 << 16, 50.0, 453, 1, "coal", null, 21296, SkillConstants.MINING),

    /**
     * Sandstone.
     */
    SANDSTONE(10946, 35, 0.2, 30 | 60 << 16, 30.0, 6971, 1, "sandstone", null, 10944, SkillConstants.MINING),

    /**
     * Gold ore.
     */
    GOLD_ORE_0(2099, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 452, SkillConstants.MINING), GOLD_ORE_1(2098, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 450, SkillConstants.MINING), GOLD_ORE_2(2611, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 21298, SkillConstants.MINING), GOLD_ORE_3(2610, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 21297, SkillConstants.MINING), GOLD_ORE_4(2609, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 21296, SkillConstants.MINING), GOLD_ORE_5(9722, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 18954, SkillConstants.MINING), GOLD_ORE_6(9720, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 32447, SkillConstants.MINING), GOLD_ORE_7(9721, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 32448, SkillConstants.MINING), GOLD_ORE_8(11183, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 21296, SkillConstants.MINING), GOLD_ORE_9(11184, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 21297, SkillConstants.MINING), GOLD_ORE_10(11185, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 21298, SkillConstants.MINING), GOLD_ORE_11(11952, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 11556, SkillConstants.MINING), GOLD_ORE_12(11953, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 11557, SkillConstants.MINING), GOLD_ORE_13(11951, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 11555, SkillConstants.MINING), GOLD_ORE_14(15578, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 14834, SkillConstants.MINING), GOLD_ORE_15(15577, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 14833, SkillConstants.MINING), GOLD_ORE_16(15576, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 14832, SkillConstants.MINING), GOLD_ORE_17(17002, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 14916, SkillConstants.MINING), GOLD_ORE_18(17003, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 31061, SkillConstants.MINING), GOLD_ORE_19(17001, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 14915, SkillConstants.MINING), GOLD_ORE_20(21291, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 21297, SkillConstants.MINING), GOLD_ORE_21(21290, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 21296, SkillConstants.MINING), GOLD_ORE_22(21292, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 21298, SkillConstants.MINING), GOLD_ORE_23(32433, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 33401, SkillConstants.MINING), GOLD_ORE_24(32432, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 33400, SkillConstants.MINING), GOLD_ORE_25(32434, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 33402, SkillConstants.MINING), GOLD_ORE_26(31065, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 37639, SkillConstants.MINING), GOLD_ORE_27(31066, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 37649, SkillConstants.MINING), GOLD_ORE_28(31067, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 37650, SkillConstants.MINING), GOLD_ORE_29(37311, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 11553, SkillConstants.MINING), GOLD_ORE_30(37310, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 11552, SkillConstants.MINING), GOLD_ORE_31(37312, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 11554, SkillConstants.MINING), GOLD_ORE_32(37471, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 15249, SkillConstants.MINING), GOLD_ORE_33(37473, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 15251, SkillConstants.MINING), GOLD_ORE_34(37472, 40, 0.6, 100 | 200 << 16, 65.0, 444, 1, "gold rocks", null, 15250, SkillConstants.MINING),

    /**
     * Granite.
     */
    GRANITE(10947, 45, 0.2, 10 | 20 << 16, 50.0, 6979, 1, "granite", null, 10945, SkillConstants.MINING),

    /**
     * Rubium.
     */
    RUBIUM(29746, 46, 0.6, 50 | 100 << 16, 17.5, 12630, 1, "rubium", null, 29747, SkillConstants.MINING),

    /**
     * Mithril ore.
     */
    MITHRIL_ORE_0(2103, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 452, SkillConstants.MINING), MITHRIL_ORE_1(2102, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 450, SkillConstants.MINING), MITHRIL_ORE_2(4988, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 4994, SkillConstants.MINING), MITHRIL_ORE_3(4989, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 4995, SkillConstants.MINING), MITHRIL_ORE_4(4990, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 4996, SkillConstants.MINING), MITHRIL_ORE_5(11943, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 11553, SkillConstants.MINING), MITHRIL_ORE_6(11942, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 11552, SkillConstants.MINING), MITHRIL_ORE_7(11945, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 11555, SkillConstants.MINING), MITHRIL_ORE_8(11944, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 11554, SkillConstants.MINING), MITHRIL_ORE_9(11947, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 11557, SkillConstants.MINING), MITHRIL_ORE_10(11946, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 11556, SkillConstants.MINING), MITHRIL_ORE_11(14855, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 25373, SkillConstants.MINING), MITHRIL_ORE_12(14854, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 25372, SkillConstants.MINING), MITHRIL_ORE_13(14853, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 25371, SkillConstants.MINING), MITHRIL_ORE_14(16687, 50, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 450, SkillConstants.MINING), MITHRIL_ORE_15(20421, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 20407, SkillConstants.MINING), MITHRIL_ORE_16(20420, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 20445, SkillConstants.MINING), MITHRIL_ORE_17(20419, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 20444, SkillConstants.MINING), MITHRIL_ORE_18(20418, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 20443, SkillConstants.MINING), MITHRIL_ORE_19(19012, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 19021, SkillConstants.MINING), MITHRIL_ORE_20(19013, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 19016, SkillConstants.MINING), MITHRIL_ORE_21(19014, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 19017, SkillConstants.MINING), MITHRIL_ORE_22(21278, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 21296, SkillConstants.MINING), MITHRIL_ORE_23(21279, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 21297, SkillConstants.MINING), MITHRIL_ORE_24(21280, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 21298, SkillConstants.MINING), MITHRIL_ORE_25(25369, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 10586, SkillConstants.MINING), MITHRIL_ORE_26(25368, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 10585, SkillConstants.MINING), MITHRIL_ORE_27(25370, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 10587, SkillConstants.MINING), MITHRIL_ORE_28(29236, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 29218, SkillConstants.MINING), MITHRIL_ORE_29(29237, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 29219, SkillConstants.MINING), MITHRIL_ORE_30(29238, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 29220, SkillConstants.MINING), MITHRIL_ORE_31(32439, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 33401, SkillConstants.MINING), MITHRIL_ORE_32(32438, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 33400, SkillConstants.MINING), MITHRIL_ORE_33(32440, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 33402, SkillConstants.MINING), MITHRIL_ORE_34(31087, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 37649, SkillConstants.MINING), MITHRIL_ORE_35(31086, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 37639, SkillConstants.MINING), MITHRIL_ORE_36(31088, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 37650, SkillConstants.MINING), MITHRIL_ORE_37(31170, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 14832, SkillConstants.MINING), MITHRIL_ORE_38(31171, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 14833, SkillConstants.MINING), MITHRIL_ORE_39(31172, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 14834, SkillConstants.MINING), MITHRIL_ORE_40(37692, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 21296, SkillConstants.MINING), MITHRIL_ORE_41(37693, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 21297, SkillConstants.MINING), MITHRIL_ORE_42(37694, 55, 0.70, 200 | 400 << 16, 80.0, 447, 1, "mithril rocks", null, 21298, SkillConstants.MINING),

    /**
     * Adamantite ore.
     */
    ADAMANTITE_ORE_0(2105, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 452, SkillConstants.MINING), ADAMANTITE_ORE_1(2104, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 450, SkillConstants.MINING), ADAMANTITE_ORE_2(4991, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 4994, SkillConstants.MINING), ADAMANTITE_ORE_3(4992, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 4995, SkillConstants.MINING), ADAMANTITE_ORE_4(4993, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 4996, SkillConstants.MINING), ADAMANTITE_ORE_5(11941, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 11554, SkillConstants.MINING), ADAMANTITE_ORE_6(11940, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 11553, SkillConstants.MINING), ADAMANTITE_ORE_7(11939, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 11552, SkillConstants.MINING), ADAMANTITE_ORE_8(14864, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 25373, SkillConstants.MINING), ADAMANTITE_ORE_9(14863, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 25372, SkillConstants.MINING), ADAMANTITE_ORE_10(14862, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 25371, SkillConstants.MINING), ADAMANTITE_ORE_11(20417, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 20407, SkillConstants.MINING), ADAMANTITE_ORE_12(20416, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 20445, SkillConstants.MINING), ADAMANTITE_ORE_13(20414, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 20443, SkillConstants.MINING), ADAMANTITE_ORE_14(20415, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 20444, SkillConstants.MINING), ADAMANTITE_ORE_15(19020, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 19017, SkillConstants.MINING), ADAMANTITE_ORE_16(19018, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 19021, SkillConstants.MINING), ADAMANTITE_ORE_17(19019, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 19016, SkillConstants.MINING), ADAMANTITE_ORE_18(21275, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 21296, SkillConstants.MINING), ADAMANTITE_ORE_19(21276, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 21297, SkillConstants.MINING), ADAMANTITE_ORE_20(21277, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 21298, SkillConstants.MINING), ADAMANTITE_ORE_21(29233, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 29218, SkillConstants.MINING), ADAMANTITE_ORE_22(29234, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 29219, SkillConstants.MINING), ADAMANTITE_ORE_23(29235, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 29220, SkillConstants.MINING), ADAMANTITE_ORE_24(32435, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 33400, SkillConstants.MINING), ADAMANTITE_ORE_25(32437, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 33402, SkillConstants.MINING), ADAMANTITE_ORE_26(32436, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 33401, SkillConstants.MINING), ADAMANTITE_ORE_27(31083, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 37639, SkillConstants.MINING), ADAMANTITE_ORE_28(31085, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 37650, SkillConstants.MINING), ADAMANTITE_ORE_29(31084, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 37649, SkillConstants.MINING), ADAMANTITE_ORE_30(31173, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 14832, SkillConstants.MINING), ADAMANTITE_ORE_31(31174, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 14833, SkillConstants.MINING), ADAMANTITE_ORE_32(31175, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 14834, SkillConstants.MINING), ADAMANTITE_ORE_33(37468, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 15249, SkillConstants.MINING), ADAMANTITE_ORE_34(37469, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 15250, SkillConstants.MINING), ADAMANTITE_ORE_35(37470, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 15251, SkillConstants.MINING), ADAMANTITE_ORE_36(37689, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 21296, SkillConstants.MINING), ADAMANTITE_ORE_37(37690, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 21297, SkillConstants.MINING), ADAMANTITE_ORE_38(37691, 70, 0.85, 400 | 800 << 16, 95.0, 449, 1, "adamant rocks", null, 21298, SkillConstants.MINING),

    /**
     * Runite ore.
     */
    RUNITE_ORE_0(2107, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 452, SkillConstants.MINING), RUNITE_ORE_1(2106, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 450, SkillConstants.MINING), RUNITE_ORE_2(6669, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 21296, SkillConstants.MINING), RUNITE_ORE_3(6671, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 21298, SkillConstants.MINING), RUNITE_ORE_4(6670, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 21297, SkillConstants.MINING), RUNITE_ORE_5(14861, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 25373, SkillConstants.MINING), RUNITE_ORE_6(14860, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 25372, SkillConstants.MINING), RUNITE_ORE_7(14859, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 25371, SkillConstants.MINING), RUNITE_ORE_8(33079, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 33401, SkillConstants.MINING), RUNITE_ORE_9(33078, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 33400, SkillConstants.MINING), RUNITE_ORE_10(37208, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 21296, SkillConstants.MINING), RUNITE_ORE_11(37465, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 15249, SkillConstants.MINING), RUNITE_ORE_12(37466, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 15250, SkillConstants.MINING), RUNITE_ORE_13(37467, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 15251, SkillConstants.MINING), RUNITE_ORE_14(37695, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 21297, SkillConstants.MINING), RUNITE_ORE_15(37696, 85, 0.95, 1250 | 2500 << 16, 125.0, 451, 1, "runite rocks", null, 21298, SkillConstants.MINING),

    /**
     * Gem rocks.
     */
    GEM_ROCK_0(23567, 40, 0.95, 166 | 175 << 16, 65, 1625, 1, "gem rocks", null, 21297, SkillConstants.MINING), GEM_ROCK_1(23566, 40, 0.95, 166 | 175 << 16, 65, 1625, 1, "gem rocks", null, 21296, SkillConstants.MINING), GEM_ROCK_2(23568, 40, 0.95, 166 | 175 << 16, 65, 1625, 1, "gem rocks", null, 21298, SkillConstants.MINING);

    /**
     * The resources mapping.
     */
    private static final Map<Integer, SkillingResource> RESOURCES = new HashMap<>();

    /**
     * Populate the mapping.
     */
    static {
        for (SkillingResource resource : SkillingResource.values()) {
            if (RESOURCES.containsKey(resource.id)) {
                System.err.println("Already contained resource " + resource.getId() + " - " + resource.name());
            }
            RESOURCES.put(resource.id, resource);
        }
    }

    /**
     * The resource id.
     */
    @Getter
    private final int id;

    /**
     * The level required.
     */
    @Getter
    private final int level;

    /**
     * The rate.
     */
    @Getter
    private final double rate;

    /**
     * The respawn rate.
     */
    @Getter
    private final int respawnRate;

    /**
     * The experience to be rewarded.
     */
    @Getter
    private final double experience;

    /**
     * The reward item id.
     */
    @Getter
    private final int reward;

    /**
     * The amount to reward.
     */
    @Getter
    private final int rewardAmount;

    /**
     * The name.
     */
    @Getter
    private final String name;

    /**
     * The animation.
     */
    @Getter
    private final Animation animation;

    /**
     * The id to replace this id with when the resource runs out.
     */
    @Getter
    private final int emptyId;

    /**
     * The skill id.
     */
    @Getter
    private final int skillId;

    /**
     * Represents if its a farming resource.
     */
    @Getter
    private final boolean farming;

    /**
     * Constructs a new {@code SkillingResource} {@code Object}.
     *
     * @param id           The id.
     * @param level        The level required.
     * @param rate         The rate.
     * @param respawnRate  The respawn rate (lowest ticks | highest ticks << 16).
     * @param experience   The experience.
     * @param reward       The reward item id.
     * @param rewardAmount The reward amount.
     * @param name         The name.
     * @param animation    The animation.
     * @param emptyId      The id to replace this id with when the resource runs out.
     * @param skillId      The skill id.
     */
    SkillingResource(int id, int level, double rate, int respawnRate, double experience, int reward, int rewardAmount, String name, Animation animation, int emptyId, int skillId) {
        this.id = id;
        this.level = level;
        this.rate = rate;
        this.respawnRate = respawnRate;
        this.experience = experience;
        this.reward = reward;
        this.rewardAmount = rewardAmount;
        this.name = name;
        this.animation = animation;
        this.emptyId = emptyId;
        this.skillId = skillId;
        this.farming = false;
    }

    /**
     * Constructs a new {@code SkillingResource} {@code Object}.
     *
     * @param id           The id.
     * @param level        The level required.
     * @param rate         The rate.
     * @param respawnRate  The respawn rate (lowest ticks | highest ticks << 16).
     * @param experience   The experience.
     * @param reward       The reward item id.
     * @param rewardAmount The reward amount.
     * @param name         The name.
     * @param animation    The animation.
     * @param emptyId      The id to replace this id with when the resource runs out.
     * @param skillId      The skill id.
     */
    SkillingResource(int id, int level, double rate, int respawnRate, double experience, int reward, int rewardAmount, String name, Animation animation, int emptyId, int skillId, boolean farming) {
        this.id = id;
        this.level = level;
        this.rate = rate;
        this.respawnRate = respawnRate;
        this.experience = experience;
        this.reward = reward;
        this.rewardAmount = rewardAmount;
        this.name = name;
        this.animation = animation;
        this.emptyId = emptyId;
        this.skillId = skillId;
        this.farming = farming;
    }

    /**
     * Gets the skilling resource for the given id.
     *
     * @param id The id.
     * @return The resource.
     */
    public static SkillingResource forId(int id) {
        return RESOURCES.get(id);
    }

    /**
     * Checks if the object id is an empty resource.
     *
     * @param id The object id.
     * @return {@code True} if so.
     */
    public static boolean isEmpty(int id) {
        for (SkillingResource r : SkillingResource.values()) {
            if (r.getEmptyId() == id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the current respawn duration (in ticks).
     *
     * @return The respawn duration.
     */
    public int getRespawnDuration() {
        int minimum = respawnRate & 0xFFFF;
        int maximum = (respawnRate >> 16) & 0xFFFF;
        double playerRatio = 2000 / World.getPlayers().size(); //TODO: 2000 represents max players.
        return (int) (minimum + ((maximum - minimum) / playerRatio));
    }

    /**
     * Gets the maximum respawn time (in ticks).
     *
     * @return The maximum respawn time.
     */
    public int getMaximumRespawn() {
        return (respawnRate >> 16) & 0xFFFF;
    }

    /**
     * Gets the minimum respawn time (in ticks).
     *
     * @return The minimum respawn time.
     */
    public int getMinimumRespawn() {
        return respawnRate & 0xFFFF;
    }
    
}
