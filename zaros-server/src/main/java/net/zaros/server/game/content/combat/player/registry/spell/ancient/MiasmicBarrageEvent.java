package net.zaros.server.game.content.combat.player.registry.spell.ancient;

import net.zaros.server.core.system.SystemManager;
import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.content.combat.player.registry.wrapper.context.CombatSpellContext;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.CombatSpellEvent;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.constant.MagicConstants.MagicBook;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/7/2017
 */
public class MiasmicBarrageEvent implements CombatSpellEvent{
	
	@Override
	public int delay(Player player) {
		return 4;
	}
	
	@Override
	public int animationId() {
		return 10518;
	}
	
	@Override
	public int hitGfx() {
		return 1854;
	}
	
	@Override
	public int maxHit(Player player, Entity target) {
		return 320;
	}
	
	@Override
	public int spellId() {
		return 39;
	}
	
	@Override
	public double exp() {
		return 54;
	}
	
	@Override
	public MagicBook book() {
		return MagicBook.ANCIENTS;
	}
	
	@Override
	public void cast(Player player, CombatSpellContext context) {
		context.getSwing().sendMultiSpell(player, context.getTarget(), this, null, null).forEach(spellContext -> {
			Entity target = spellContext.getTarget();
			if (!target.isPlayer() || target.getTemporaryAttribute(AttributeKey.MIASMIC_IMMUNITY, false)) {
				return;
			}
			Player p = target.toPlayer();
			p.getTransmitter().sendMessage("You feel slowed down.");
			target.putTemporaryAttribute(AttributeKey.MIASMIC_IMMUNITY, true);
			target.putTemporaryAttribute(AttributeKey.MIASMIC_EFFECT, true);
			SystemManager.getScheduler().schedule(new ScheduledTask(1, 95) {
				@Override
				public void run() {
					if (getTicksPassed() == 80) {
						target.removeTemporaryAttribute(AttributeKey.MIASMIC_EFFECT);
					} else if (getTicksPassed() == 95) {
						target.removeTemporaryAttribute(AttributeKey.MIASMIC_IMMUNITY);
					}
				}
			});
		});
	}
}
