package net.zaros.server.game.content.combat;

import net.zaros.server.core.system.SystemManager;
import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.content.action.interaction.PlayerFollowAction;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.EntityMovement;
import net.zaros.server.game.node.entity.effects.impl.FreezeEffect;
import net.zaros.server.game.node.entity.npc.link.NPCFollowAction;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.Hit;

public class CombatManager {

    /**
     * Starts the combat between two character's.
     *
     * @param attacker
     * @param target
     */
    public static void startCombat(Entity attacker, Entity target) {
        attacker.putTemporaryAttribute(AttributeKey.TARGET, target);
        if (attacker.isPlayer()) {
            attacker.toPlayer().getManager().getActions().startAction(new PlayerFollowAction(target));
        } else {
            NPCFollowAction follow = new NPCFollowAction(attacker.toNPC());
            follow.setEntity(target);
            attacker.toNPC().getActions().startAction(follow);
        }
    }

    /**
     * Ends the combat script execution for that character.
     *
     * @param character
     */
    public static void endCombat(Entity character) {
        character.removeTemporaryAttribute(AttributeKey.TARGET);
        character.faceEntity(null);
        if (character.isPlayer()) {
            character.toPlayer().getManager().getActions().stopAction();
        } else {
            character.toNPC().getActions().stopAction();
        }
    }

    /**
     * Returns if the target and the source is valid.
     *
     * @param source The source
     * @param target The target
     */
    public static boolean validCombat(Entity source, Entity target) {
        boolean targetInvalid = target == null || target.isDead() || !target.isRenderable() /*|| !target.attackable(source)*/;
        boolean sourceInvalid = source == null || source.isDead() || !source.isRenderable() /*|| !source.attackable(target)*/;

        if (sourceInvalid || targetInvalid || !source.getLocation().withinDistance(target.getLocation(), 16)) {
            return false;
        }
        if (target.isNPC() && target.toNPC().isForceWalking() || source.isNPC() && source.toNPC().isForceWalking()) {
            return false;
        }
        return true;
    }

    /**
     * Returns if the character has a target.
     *
     * @param character
     * @return if has a target
     */
    public static boolean hasTarget(Entity character) {
        return character.getTemporaryAttribute(AttributeKey.TARGET, null) != null;
    }

    /**
     * Returns if the character is under attack.
     *
     * @param character
     * @return if is under attack
     */
    public static boolean isUnderAttack(Entity character) {
        long pj_delay = 6000;
        long last_attack_taken = character.getTemporaryAttribute(AttributeKey.LAST_HIT_TAKEN_INSTANT, 0L);
        return (System.currentTimeMillis() - last_attack_taken < pj_delay) || character.getTemporaryAttribute(AttributeKey.ATTACKER) != null;
    }

    /**
     * Returns if the characters
     *
     * @param character
     * @return if is under attack
     */
    public static boolean underAttackTimer(Entity character) {
        long pj_delay = 6000;
        long last_attack_taken = character.getTemporaryAttribute(AttributeKey.LAST_HIT_TAKEN_INSTANT, 0L);
        return (System.currentTimeMillis() - last_attack_taken < pj_delay);
    }

    /**
     * Returns if the character is attacking another character.
     *
     * @param character
     * @return if is attacking
     */
    public static boolean isAttacking(Entity character) {
        long pj_delay = 6000;
        long last_attack_taken = character.getTemporaryAttribute(AttributeKey.LAST_ATTACK_INSTANT, 0L);
        return (last_attack_taken > 0 && System.currentTimeMillis() - last_attack_taken < pj_delay);
    }

    /**
     * Decreases the character's attack delay.
     *
     * @param character
     */
    public static void decreaseAttackDelay(Entity character) {
        int attack_delay = character.getTemporaryAttribute(AttributeKey.ATTACK_DELAY, 0);
        if (attack_delay > 0) {
            attack_delay -= 1;
            character.putTemporaryAttribute(AttributeKey.ATTACK_DELAY, attack_delay);
        }
    }

    /**
     * Returns if the target can be attacked.
     *
     * @param attacker
     * @param target
     * @return if can be attacked
     */
    public static boolean canAttackCharacter(Entity attacker, Entity target) {

        if (attacker.isDying() || attacker.getHealthPoints() < 0 || target.isDying() || target.getHealthPoints() < 0) {
            endCombat(attacker);
            return false;
        }

        if (!validCombat(attacker, target)) {
            endCombat(attacker);
            return false;
        }

        /*if (attacker.isStunned()) {
            if (attacker.isPlayer()) {
                attacker.getAsPlayer().sendMessage("You're currently stunned and cannot attack.");
            }
            endCombat(attacker);
            return false;
        }*/

        /*if (attacker.isPlayer() && target.isNpc()) {
            if (!SlayerMonsters.canAttack(attacker.getAsPlayer(), target.getAsNpc())) {
                endCombat(attacker);
                return false;
            }
            Character spawnedFor = target.getAttributeOr(AttributeKey.SPAWNED_FOR, null);

            if (spawnedFor != null && spawnedFor != attacker) {
                attacker.getAsPlayer().sendMessage("This is not your fight.");
                endCombat(attacker);
                return false;
            }
        }*/

        /*if (attacker.getCombatDefinitions().isSpecialActivated()) {

            WeaponSpecials special = attacker.getAttribute(AttributeKey.SPECIAL_WEAPON);

            if (special == null) {
                return false;
            }

            Player player = attacker.getAsPlayer();

            if (player.getSpecialPercentage() < special.getDrainAmount()) {
                player.sendMessage("You do not have enough special attack energy left!");
                player.setSpecialActivated(false);
                WeaponSpecials.updateBar(player);
                return false;
            }
        }*/

        if (!attacker.getArea().isMulti() || !target.getArea().isMulti()) {

            boolean PvP = target.isPlayer() && attacker.isPlayer();

            Entity att = attacker.getTemporaryAttribute(AttributeKey.ATTACKER);

            if (att != null && att != target && (PvP || att.getHealthPoints() > 0) && underAttackTimer(attacker)) {
                if (attacker.isPlayer()) {
                    attacker.toPlayer().getTransmitter().sendMessage("You are already under attack!");
                }
                endCombat(attacker);
                return false;
            }

            att = target.getTemporaryAttribute(AttributeKey.ATTACKER);

            if (att != null && att != attacker && (PvP || att.getHealthPoints() > 0) && underAttackTimer(target)) {
                if (attacker.isPlayer()) {
                    attacker.toPlayer().getTransmitter().sendMessage("They are already under attack!");
                }
                endCombat(attacker);
                return false;
            }
        }

        if (!attacker.getArea().canAttack(attacker, target)) {
            endCombat(attacker);
            return false;
        }

        return true;
    }

    /**
     * Returns if the attacker is within distance of the target.
     *
     * @param attacker
     * @param target
     * @param script
     * @return
     */
    public static boolean isWithinDistance(Entity attacker, Entity target, CombatScript script, boolean followCheck) {

        if (!validCombat(attacker, target)) {
            return false;
        }

        //TODO: NPC retreat
       /* if (attacker.isNPC()) {
            NPC npc = attacker.toNPC();
            if (npc.getDefinitions().doesRetreat() && npc.getMovementCoordinator().getCoordinateState() == NPCMovementCoordinator.CoordinateState.RETREATING) {
                return false;
            }
        }*/

        Hit.HitSplat hitSplat = script.getHitSplat(attacker, target);

        //TODO: effect system
        boolean canWalk = true; //attacker.getEffectManager().canWalk(false);

        if (EntityMovement.colides(attacker, target)) {
            return hitSplat == Hit.HitSplat.MAGIC_DAMAGE && canWalk;
        }

        int scriptDistance = getScriptFollowDistance(attacker, target, script, followCheck);

        boolean ignorePathChecker = false;
        boolean ignoreAccessibility = false;


        if (!ignorePathChecker) {

            /*boolean pathClear = attacker.getMovement().clippedProjectileToNode(target, hitSplat == Hit.HitSplat.MELEE_DAMAGE);

            if (hitSplat != Hit.HitSplat.MELEE_DAMAGE && hitSplat != Hit.HitSplat.MAGICAL_MELEE_DAMAGE) {
                if (!EntityMovement.accessable(attacker.getLocation(), target.getLocation())) {
                    CombatManager.endCombat(attacker);
                }
            } else {
                if (!pathClear) {
                    if (!EntityMovement.accessable(attacker.getLocation(), target.getLocation())) {
                        CombatManager.endCombat(attacker);
                        return false;
                    }
                }
            }*/

        }

        if (scriptDistance == 1 && hitSplat == Hit.HitSplat.MELEE_DAMAGE) {
            return EntityMovement.isWithinDistance(attacker, target, scriptDistance);
        } else {
            return EntityMovement.isWithinDiagonalDistance(attacker, target, scriptDistance);
        }
    }

    /**
     * Returns the combat script follow distance.
     *
     * @param attacker
     * @param target
     * @param script
     * @param followCheck
     * @return the distance
     */
    public static int getScriptFollowDistance(Entity attacker, Entity target, CombatScript script, boolean followCheck) {
        int scriptDistance = followCheck ? script.getFollowDistance(attacker, target) : script.getAttackDistance(attacker, target);

        //TODO: effect system
        boolean canWalk = true; //attacker.getEffectManager().canWalk(false);

        Hit.HitSplat damageType = script.getHitSplat(attacker, target);

        if (canWalk && !followCheck && attacker.getMovement().isMoving()) {
            if (target.isPlayer() && target.getMovement().isMoving()) {

                scriptDistance += 1;
            }

            if (damageType == Hit.HitSplat.MELEE_DAMAGE) {
                scriptDistance += attacker.getMovement().isRunning() ? 2 : 1;
            }
        }
        return scriptDistance;
    }

    /**
     * Forces the target to find into combat with the source, 1 tick after the calling of the method. This requires the
     * target not to be moving and not to be fighting already
     *
     * @param source The starter of combat
     * @param target The target of combat
     */
    public static void autoRetaliate(Entity source, Entity target) {
        // as long as the target isn't moving or fighting already, they'll retaliate to us
        if (target.isNPC() || target.isPlayer() && target.toPlayer().getCombatDefinitions().isRetaliating() && !target.fighting() && !target.getMovement().isMoving()) {
            SystemManager.getScheduler().schedule(new ScheduledTask(1) {
                @Override
                public void run() {
                    startCombat(target, source);
                }
            });
        }
    }

    /**
     * Freezes the entity.
     *
     * @param target
     * @param seconds
     */
    public static void freeze(Entity target, Entity aggressor, int seconds) {

        if (!target.getEffectManager().getFreezeImmunityTimer().finished()) {
            return;
        }

        FreezeEffect effect = new FreezeEffect(target, aggressor, seconds);

        if (effect.immune(target) && aggressor.isPlayer()) {
            aggressor.toPlayer().getTransmitter().sendMessage("This " + (target.isNPC() ? "monster" : "player") +
                    " seems to be immune to freeze effects.");
            return;
        }

        target.getEffectManager().apply(new FreezeEffect(target, aggressor, seconds));

        target.getMovement().resetWalkSteps();
    }

}
