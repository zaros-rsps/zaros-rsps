package net.zaros.server.game.content.combat.data.magic.ancientspells;

import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitEffectType;
import net.zaros.server.game.node.entity.hit.HitInstant;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;

import java.util.List;

/**
 * The ice blitz spell on the ancient spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class IceBlitz extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 21;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.WATER_RUNE1, 3), new Item(Items.BLOOD_RUNE1, 2), new Item(Items.DEATH_RUNE1, 2));

    @Override
    public void playGraphics(Entity source, Entity target) {
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(1978);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return new HitEffect(HitEffectType.FREEZE, HitInstant.CALCULATION, 15);
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(367);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int autocastConfigID() {
        return 85;
    }

    @Override
    public int magicLevelRequirement() {
        return 82;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 260;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 46;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.ANCIENTS;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }
}
