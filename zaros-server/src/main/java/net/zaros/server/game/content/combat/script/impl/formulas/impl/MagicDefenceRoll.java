package net.zaros.server.game.content.combat.script.impl.formulas.impl;

import net.zaros.server.game.content.combat.script.impl.formulas.CombatFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.rs.constant.BonusConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

public class MagicDefenceRoll implements CombatFormula {

    @Override
    public int get(Entity attacker, Entity target, boolean specialAttack) {

        var defenceLevel = target.getSkills().getLevel(SkillConstants.DEFENCE) * 1.0;
        var magicLevel = target.getSkills().getLevel(SkillConstants.DEFENCE) * 1.0;

        final double defencePrayerBoost = attacker.getPrayers().getBasePrayerBoost(SkillConstants.DEFENCE);
        defenceLevel *= defencePrayerBoost;
        final double magicPrayerBoost = attacker.getPrayers().getBasePrayerBoost(SkillConstants.DEFENCE);
        magicLevel *= magicPrayerBoost;

        defenceLevel = Math.round(defenceLevel);
        magicLevel = Math.round(magicLevel);

        defenceLevel += 8;
        magicLevel += 8;

        defenceLevel *= 0.3;
        magicLevel *= 0.7;

        var defenceBonus = target.getBonuses()[BonusConstants.MAGIC_DEFENCE];
        var defenceRoll = Math.round((defenceLevel + magicLevel) * (64 + defenceBonus)) * 1.0;

        return (int) defenceRoll;
    }
}