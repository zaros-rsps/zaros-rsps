package net.zaros.server.game.node.item;

import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.repository.npc.Npcs;

public class CombatEquipments {

    /**
     * Determines if the entity is wearing full veracs.
     *
     * @param entity the entity to determine this for.
     * @return true if the player is wearing full veracs.
     */
    public static boolean fullVeracs(Entity entity) {
        if (entity.isNPC()) {
            return entity.toNPC().getId() == Npcs.VERAC_THE_DEFILED1;
        }
        return false;//entity.toPlayer().getEquipment().containsAll(VERACS_HELM, VERACS_BRASSARD, VERACS_PLATESKIRT, VERACS_FLAIL);
    }

    /**
     * Determines if the entity is wearing full dharoks.
     *
     * @param entity the entity to determine this for.
     * @return true if the player is wearing full dharoks.
     */
    public static boolean fullDharoks(Entity entity) {
        if (entity.isNPC()) {
            return entity.toNPC().getId() == Npcs.DHAROK_THE_WRETCHED1;
        }
        return false;//entity.getEquipment().containsAll(DHAROKS_HELM, DHAROKS_PLATEBODY, DHAROKS_PLATELEGS, DHAROKS_GREATAXE);
    }

    /**
     * Determines if the entity is wearing full ahrims.
     *
     * @param entity the entity to determine this for.
     * @return true if the player is wearing full ahrims.
     */
    public static boolean fullAhrim(Entity entity) {
        if (entity.isNPC()) {
            return entity.toNPC().getId() == Npcs.AHRIM_THE_BLIGHTED1;
        }
        return false;//entity.getEquipment().containsAll(AHRIMS_HOOD, AHRIMS_ROBETOP, AHRIMS_ROBESKIRT, AHRIMS_STAFF);
    }

    /**
     * Determines if the entity is wearing full guthans.
     *
     * @param entity the entity to determine this for.
     * @return true if the player is wearing full guthans.
     */
    public static boolean fullGuthans(Entity entity) {
        if (entity.isNPC()) {
            return entity.toNPC().getId() == Npcs.GUTHAN_THE_INFESTED1;
        }
        return false;//entity.getEquipment().containsAll(GUTHANS_HELM, GUTHANS_PLATEBODY, GUTHANS_CHAINSKIRT, GUTHANS_WARSPEAR);
    }

}
