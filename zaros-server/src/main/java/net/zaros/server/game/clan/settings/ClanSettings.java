package net.zaros.server.game.clan.settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.LongPredicate;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.zaros.cache.util.buffer.DynamicBuffer;
import net.zaros.server.game.clan.Clan;
import net.zaros.server.game.clan.ClanRank;
import net.zaros.server.game.clan.settings.delta.ClanSettingsDelta;
import net.zaros.server.game.clan.settings.delta.impl.AddMember;
import net.zaros.server.utility.RunedayUtil;
import net.zaros.server.utility.TimeUtil;

/**
 * Represents the clan settings container.
 * 
 * @author Walied K. Yassen
 */
public final class ClanSettings {

	/**
	 * The members list of this settings.
	 */
	@Getter
	private final List<ClanMember> members = new ArrayList<ClanMember>();

	/**
	 * The owner clan instance.
	 */
	@Getter
	private transient Clan clan;

	/**
	 * The members by world cache.
	 */
	@Getter
	private transient Map<Integer, Set<Long>> membersByWorld;

	/**
	 * The clan settings delta updater.
	 */
	@Getter
	private transient ClanSettingsDelta delta;

	/**
	 * The clan settings update block.
	 */
	@Getter
	private transient ClanSettingsBlock updateBlock;

	/**
	 * The current update number.
	 */
	@Getter
	private transient int updateNumber;

	/**
	 * Initialises the clan settings for protocol use.
	 */
	public void initialise(Clan clan) {
		this.clan = clan;
		delta = new ClanSettingsDelta(this);
		updateBlock = new ClanSettingsBlock(this);
		membersByWorld = new HashMap<Integer, Set<Long>>();
	}

	/**
	 * Posts the current delta update to all of the online users.
	 * 
	 * @param filter
	 *               the filter which if members passed it, they don't get sent the
	 *               updates.
	 */
	public void postUpdate(LongPredicate filter) {
		delta.post(filter);
		updateNumber++;
	}

	/**
	 * Adds a new member to this members list.
	 * 
	 * @param userId
	 *               the member user id.
	 * @param name
	 *               the member display name.
	 * @param rank
	 *               the member rank.
	 */
	public void addMember(long userId, String name, ClanRank rank) {
		var member = new ClanMember(userId, name, rank);
		members.add(member);
		delta.enqueue(new AddMember(member));
	}

	/**
	 * Updates the member with the specified {@code userId} with the new provided
	 * data.
	 * 
	 * @param userId
	 *               the user id which is linked to the member we want to update.
	 * @param world
	 *               the member's current world id.
	 */
	public void updateMember(long userId, int world) {
		var member = getMemberById(userId);
		if (member == null) {
			return;
		}
		if (member.getWorld() != 0) {
			uncacheMember(member.getWorld(), member.getUserId());
		}
		cacheMember(world, member.getUserId());
		member.setWorld(world);
	}

	/**
	 * Gets the {@link ClanMember} with the specified {@code userId}.
	 * 
	 * @param userId
	 *               the user id of the member.
	 * @return the {@link ClanMember} if it was present otherwise {@code null}.w
	 */
	public ClanMember getMemberById(long userId) {
		for (var member : members) {
			if (member.getUserId() == userId) {
				return member;
			}
		}
		return null;
	}

	/**
	 * Caches the specified member {@code userId} in the members by world map.
	 * 
	 * @param worldId
	 *                the world which the user is currently or was in.
	 * @param userId
	 *                the user id to cache.
	 */
	private void cacheMember(int worldId, long userId) {
		var worldMembers = membersByWorld.get(worldId);
		if (worldMembers == null) {
			membersByWorld.put(worldId, worldMembers = new HashSet<Long>());
		}
		worldMembers.add(userId);
	}

	/**
	 * Uncaches the specified member {@code userId} from the members by world map.
	 * 
	 * @param worldId
	 *                the world which the user is currently or was in.
	 * @param userId
	 *                the user id to uncache.
	 */
	private void uncacheMember(int worldId, long userId) {
		var worldMembers = membersByWorld.get(worldId);
		if (worldMembers != null) {
			worldMembers.remove(userId);
			if (worldMembers.size() < 1) {
				membersByWorld.remove(worldId);
			}
		}
	}

	/**
	 * Represents a clan settings update block.
	 * 
	 * @author Walied K. Yassen
	 */
	@RequiredArgsConstructor
	public static final class ClanSettingsBlock {

		/**
		 * The clan settings instance which this block is for.
		 */
		private final ClanSettings settings;

		/**
		 * The last update number.
		 */
		@Getter
		private long updateNumber;

		/**
		 * The last update block.
		 */
		private byte[] updateBlock;

		/**
		 * Rebuilds the update block.
		 */
		public void rebuild() {
			// create the block builder.
			var builder = new DynamicBuffer(128);
			// write the clan version.
			builder.writeByte(5);
			// write the settings flag.
			// 0x1 - use_memberhashes
			// 0x2 - use_displaynames
			builder.writeByte(0x2);
			// write the update number, it must be the current
			// update number, which is the latest.
			builder.writeInt(settings.getUpdateNumber());
			// write the clan creation time in hours since epoch.
			builder.writeInt((int) TimeUtil.toHours(settings.getClan().getCreationTime()));
			// write the amount of members.
			var members = settings.getMembers();
			builder.writeShort(members.size());
			// write the amount of bans.
			builder.writeByte(0);
			// write the clan name.
			builder.writeString(settings.getClan().getName());
			// unknown and unused.
			builder.writeInt(0);
			// write whether we allow guests or not.
			builder.writeBoolean(false);
			// write the required rank to talk.
			builder.writeByte(0);
			// write the required rank to kick.
			builder.writeByte(0);
			// write the required rank to lootshare.
			builder.writeByte(0);
			// write an unknown requirement rank.
			builder.writeByte(0);
			for (var member : members) {
				// write the members display name.
				builder.writeString(member.getDisplayName());
				// write the members clan rank id.
				builder.writeByte(member.getRank().getId());
				// write the members extra info.
				builder.writeInt(member.getExtraInfo());
				// write the members join runeday.
				builder.writeShort(RunedayUtil.toRuneDay(member.getJoinTime()));
			}
			// write the amount of variables of this clan.
			builder.writeShort(0);
			// store the built update block.
			updateBlock = builder.getDataTrimmed();
			// update our cached update number.
			updateNumber = settings.getUpdateNumber();
		}

		/**
		 * Gets the update block data. If the data is outdated, we rebuild the data
		 * using {@link #rebuild()} method.
		 * 
		 * @return the update block data as {@link byte} array.
		 */
		public byte[] getBytes() {
			if (updateNumber != settings.getUpdateNumber()) {
				rebuild();
			}
			return updateBlock;
		}
	}

	/**
	 * Contains all the id(s) of the clan variables.
	 * 
	 * @author Walied K. Yassen
	 */
	public static interface ClanVar {

	}
}
