package net.zaros.server.game.module.interaction.npc;

import net.zaros.server.core.newtask.TaskManager;
import net.zaros.server.game.content.skills.fishing.plugin.FishingSpot;
import net.zaros.server.game.content.skills.fishing.plugin.FishingTask;
import net.zaros.server.game.content.skills.fishing.plugin.FishingTool;
import net.zaros.server.game.module.type.NPCInteractionModule;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Jacob Rhiel
 */
public class FishingSpotInteractionModule extends NPCInteractionModule {

    public FishingSpotInteractionModule() {
        register(FishingSpot.allSpots());
    }

    @Override
    public boolean handle(Player player, NPC npc, InteractionOption option) {

        FishingSpot spot = FishingSpot.forSpot(npc.getId());

        if(spot == null) return false;

        FishingTool tool = FishingTool.forSpot(spot, option);

        FishingTask task = new FishingTask(player, spot, tool, tool.getBait());

        task.setPeriod(1);

        if(task.hasRequirements()) {
            TaskManager.getTickScheduler().runAsync(task);
        }
        return true;
    }
}

