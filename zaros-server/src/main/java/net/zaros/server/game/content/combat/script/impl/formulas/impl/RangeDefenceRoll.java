package net.zaros.server.game.content.combat.script.impl.formulas.impl;

import net.zaros.server.game.content.combat.script.impl.formulas.CombatFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.rs.constant.BonusConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

public class RangeDefenceRoll implements CombatFormula {

    @Override
    public int get(Entity attacker, Entity target, boolean specialAttack) {

        var defenceLevel = target.getSkills().getLevel(SkillConstants.DEFENCE) * 1.0;

        final double prayerBoost = attacker.getPrayers().getBasePrayerBoost(SkillConstants.DEFENCE);
        defenceLevel *= prayerBoost;

        defenceLevel = Math.round(defenceLevel);

        final var targetStyle = target.isPlayer() ? target.toPlayer().getCombatDefinitions().getAttackStyle() :
                target.toNPC().getCombatDefinitions().getAttackStyle();

        if (targetStyle == 3) {
            defenceLevel += 3;
        }

        if (targetStyle == 2) {
            defenceLevel = 1;
        }

        defenceLevel += 8;

        var defenceBonus = target.getBonuses()[BonusConstants.RANGE_DEFENCE];

        defenceLevel = Math.round(defenceLevel);

        var defenceRoll = Math.round(defenceLevel * (64 + defenceBonus)) * 1.0;

        return (int) defenceRoll;
    }

}