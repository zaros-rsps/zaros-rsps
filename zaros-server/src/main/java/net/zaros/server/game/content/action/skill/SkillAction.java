package net.zaros.server.game.content.action.skill;

import net.zaros.server.game.content.action.Action;
import net.zaros.server.game.content.skills.SkillingResource;
import net.zaros.server.game.content.skills.SkillingTool;
import net.zaros.server.game.node.Node;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;

public abstract class SkillAction<T extends Node> implements Action {

    /**
     * The player.
     */
    protected final Player player;

    /**
     * The node.
     */
    protected T node;

    /**
     * The tool used.
     */
    protected SkillingTool tool;

    /**
     * The resource.
     */
    protected SkillingResource resource;

    /**
     * If we should reset the anim at the end.
     */
    protected boolean resetAnimation = true;

    public SkillAction(Player player, T node) {
        this.player = player;
        this.node = node;
    }

    @Override
    public boolean start(Entity entity) {
        if(checkRequirements())
            return true;
        return false;
    }

    @Override
    public boolean process(Entity entity) {
        if(checkRequirements())
            return true;
        return false;
    }

    @Override
    public int processOnTicks(Entity entity) {
        if(!checkRequirements())
            return -1;
        animate();
        return reward() == true ? -1 : 1;
    }

    @Override
    public void stop(Entity entity) {
        if(resetAnimation) {
            if(entity.isPlayer())
                entity.sendAnimation(-1);
        }
        message(1);
    }

    /**
     * Checks if the player meets all the requirements.
     *
     * @return {@code True} if the player can continue, {@code false} if not.
     */
    public abstract boolean checkRequirements();

    /**
     * Sends the animations related to the actions the player is doing.
     */
    public abstract void animate();

    /**
     * Rewards the player.
     *
     * @return {@code True} if the player should stop this skilling pulse.
     */
    public abstract boolean reward();

    /**
     * Sends a message to the player.
     *
     * @param type The message type (0: start message, 1: stop message).
     */
    public void message(int type) {

    }

}
