package net.zaros.server.game.content.combat.data.magic;

import net.zaros.server.game.content.skills.magic.Spell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitModifier;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.constant.SkillConstants;

/**
 * A abstract class for combat spells.
 *
 * @author Gabriel || Wolfsdarker
 */
public abstract class CombatSpell implements Spell {

    /**
     * Plays a graphic update mask on the source or target.
     *
     * @param source
     * @param target
     */
    public abstract void playGraphics(Entity source, Entity target);

    /**
     * Plays a animation update mask on the source or target.
     *
     * @param source
     * @param target
     */
    public abstract void playAnimation(Entity source, Entity target);

    /**
     * Sends a projectile from the source to the target.
     *
     * @param source
     * @param target
     */
    public abstract void sendProjectile(Entity source, Entity target);

    /**
     * Returns the spell's hit effect.
     *
     * @param source
     * @param target
     * @return the effect
     */
    public abstract HitEffect getEffect(Entity source, Entity target);

    /**
     * Executed when the hit is executed on the target.
     *
     * @param hit
     */
    public abstract void postHitExecution(CombatHit hit);

    /**
     * Returns the spell's hit modifier.
     *
     * @param source
     * @param target
     * @return the modifier
     */
    public HitModifier getModifier(Entity source, Entity target) {
        return null;
    }

    @Override
    public boolean ableToCastOnEntity(Entity source, Entity target) {

        if (source.getSkills().getLevel(SkillConstants.MAGIC) < magicLevelRequirement()) {
            if (source.isPlayer()) {
                source.toPlayer().getTransmitter().sendMessage("You need a Magic level of " + magicLevelRequirement() + " to cast this.");
                return false;
            }
        }

        if (source.isPlayer()) {

            var player = source.toPlayer();

            if (player.getCombatDefinitions().getSpellbook() != magicBook()) {
                player.getTransmitter().sendMessage("Your current spellbook does not match with the spell.");
                return false;
            }

            var items = requiredItems();

            if (!items.isEmpty()) {
                for (Item item : items) {
                    if (!player.getInventory().containsItem(item.getId(), item.getAmount())) {
                        player.getTransmitter().sendMessage("You do not have the required items to cast this spell.");
                        return false;
                    }
                }
            }

            var equipments = requiredEquipments();

            if (!equipments.isEmpty()) {

                var contains = false;

                for (Item item : equipments) {
                    if (player.getEquipment().getItems().contains(item)) {
                        contains = true;
                        break;
                    }
                }

                if (!contains) {
                    player.getTransmitter().sendMessage("You do not have the required equipment to cast this spell.");
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public void castOnEntity(Entity source, Entity target) {

        if (!ableToCastOnEntity(source, target)) {
            return;
        }

        if (source.isPlayer()) {
            deleteRequiredItems(source.toPlayer());
        }

        playAnimation(source, target);
        playGraphics(source, target);
        sendProjectile(source, target);

    }

    @Override
    public boolean ableToCast(Entity source) {
        return true;
    }

    @Override
    public void cast(Entity source) {
    }
}
