package net.zaros.server.game.module.command.administrator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.zaros.cache.Cache;
import net.zaros.cache.type.objtype.ItemDefinition;
import net.zaros.cache.type.objtype.ItemDefinitionParser;
import net.zaros.cache.util.Utils;
import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.tool.ColorConstants;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/27/2017
 */
@CommandManifest(description = "Finds an item by the name we want", types = { String.class })
public class FindItemByNameCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("finditem", "itemn");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		// the identifier to search by
		final String identifier = args[1].replaceAll("_", " ");
		// the option flag
		final String optionFlag = stringParamOrDefault(args, 2, null);
		
		// the list of items that were found
		List<String> found = new ArrayList<>();
		for (int itemId = 0; itemId < Utils.getItemDefinitionsSize(Cache.getStore()); itemId++) {
			// the definition instance
			ItemDefinition definition = ItemDefinitionParser.forId(itemId);
			if (definition == null) {
				continue;
			}
			// the name of the item
			final String name = definition.getName().toLowerCase();
			boolean added = false;
			// the name has the identifier we want
			if (name.contains(identifier)) {
				// we only want to find items by the identifier
				if (optionFlag == null) {
					added = true;
				} else {
					// we found that the definition has the option we want
					if (definition.hasOption(optionFlag)) {
						added = true;
					}
				}
			}
			// the item was not added because it was not found by filter
			if (!added) {
				continue;
			}
			found.add("[<col=FF0000>" + itemId+ "</col>] <col=" + ColorConstants.LIGHT_BLUE + ">" + definition.getName() + "</col> inventory=" + Arrays.toString(definition.getInventoryOptions()) + " ground=" + Arrays.toString(definition.getGroundOptions()));
		}
		// shows the entries found
		found.forEach(entry -> player.getTransmitter().sendConsoleMessage(entry));
		// sends a response with the amount of entries found
		sendResponse(player, "Found " + found.size() + " items by name '" + identifier + "'.", console);
	}
	
}
