package net.zaros.server.game.content.zone;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;

public abstract class Zone {

    @Getter
    @Setter
    public Region ZoneRegion;

    /**
     * The Unique 'Zone' will need these properties for be a zone
     * @param topRight
     * @param bottomLeft
     */
    public Zone(Location topRight, Location bottomLeft) {
        ZoneRegion = new Region(topRight, bottomLeft);
    }

    /**
     * Checks if user is in the referenced Zone
     * @param location
     * @return
     */
    public boolean isInZone(Location location) {
        return ZoneRegion != null ? ZoneRegion.isInRegion(location): false;
    }

    /**
     * Sends an Item to all of the players in the specified Zone
     * @param itemID
     * @param amount
     * @param optionalMessage
     */
    public void sendItemToAllZonePlayers(int itemID, int amount, String optionalMessage) {
        for (Player player: World.getPlayers()) {
            if (isInZone(player.getLocation())) {
                if (player.getInventory().hasFreeSlots()) {
                    player.getInventory().addItem(itemID, amount);
                    if (optionalMessage != null) player.getTransmitter().sendMessage(optionalMessage);
                } else {
                    player.getTransmitter().sendMessage("You've received a reward; inventory full it has been dropped under you!");
                    //drop item under their feet
                }
            }
        }
    }

    /**
     * Broadcasts a message to all users in specificed zone
     * @param message
     */
    public void sendMessageToZonePlayers(String message) {
        for (Player player: World.getPlayers()) {
            if (isInZone(player.getLocation())) {
                player.getTransmitter().sendMessage(message);
            }
        }
    }

    @Getter
    @Setter
    private Location topRight;

    @Getter
    @Setter
    private Location bottomLeft;
}
