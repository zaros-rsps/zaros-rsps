package net.zaros.server.game.content.combat.data.magic.modernspells;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.*;
import net.zaros.server.game.node.entity.render.flag.impl.GraphicHeight;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;

import java.util.List;

/**
 * The snare spell on the regular spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class Snare extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 55;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.WATER_RUNE1, 4), new Item(Items.EARTH_RUNE1, 4), new Item(Items.NATURE_RUNE1, 3));

    @Override
    public void playGraphics(Entity source, Entity target) {
        source.sendGraphics(177, GraphicHeight.HIGH.toInt(), 0);
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(710);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
        ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(source, target, 178, 30, 26, 52, 0, 0));
    }

    @Override
    public HitModifier getModifier(Entity source, Entity target) {
        return null;
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return new HitEffect(HitEffectType.FREEZE, HitInstant.CALCULATION, 10);
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(180, GraphicHeight.HIGH.toInt(), 0);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int magicLevelRequirement() {
        return 50;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 10;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 60.5;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.REGULAR;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }

}
