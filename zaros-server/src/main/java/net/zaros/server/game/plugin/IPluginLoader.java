package net.zaros.server.game.plugin;

import java.io.File;

/**
 * Represents a plug-in loader that can only load a specific type of plug-ins
 * such JAR plug-ins.
 *
 * @author Walied K. Yassen
 * @since 0.1
 */
public interface IPluginLoader<T extends IPlugin> {

	/**
	 * Loads the plug-in which is located at the specified {@code path}.
	 *
	 * @param file
	 *                 the file which leads to the plug-in file.
	 * @return the loaded {@link IPlugin} object.
	 * @throws PluginException
	 *                             if anything goes wrong during the loading
	 *                             process.
	 */
	public T load(File file);
}
