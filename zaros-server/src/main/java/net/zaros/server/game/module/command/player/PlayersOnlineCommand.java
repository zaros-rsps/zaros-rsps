package net.zaros.server.game.module.command.player;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/31/2017
 */
@CommandManifest(description = "View the amount of players online")
public class PlayersOnlineCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("players", "size");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		sendResponse(player, "There are currently " + World.getPlayers().size() + " players online.", console);
	}
}
