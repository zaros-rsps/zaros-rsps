package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.constant.MagicConstants.MagicBook;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/27/2017
 */
@CommandManifest(description = "Sets the magic book you're on", types = { Integer.class })
public class SetMagicBookCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("setmagicbook");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		player.getCombatDefinitions().setSpellbook(MagicBook.values()[intParam(args, 1)]);
	}
}
