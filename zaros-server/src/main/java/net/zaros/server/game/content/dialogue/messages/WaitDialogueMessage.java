/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.dialogue.messages;

import lombok.AllArgsConstructor;
import net.zaros.server.game.content.dialogue.DialogueMessage;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Walied K. Yassen
 */
@AllArgsConstructor
public class WaitDialogueMessage extends DialogueMessage {

	/**
	 * How many ticks do we wait before executing the next action?
	 */
	private final int ticks;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.dialogue.DialogueMessage#send(org.redrune.game.node.
	 * entity.player.Player)
	 */
	@Override
	public void send(Player player) {
		player.setDialogueWaitTicks(ticks);
	}

}
