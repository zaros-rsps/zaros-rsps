package net.zaros.server.game.clan.channel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.LongPredicate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.zaros.cache.util.buffer.DynamicBuffer;
import net.zaros.server.game.clan.ClanInfo;
import net.zaros.server.game.clan.ClanRank;
import net.zaros.server.game.clan.channel.delta.ClanChannelDelta;
import net.zaros.server.game.clan.channel.delta.impl.AddMember;
import net.zaros.server.game.clan.channel.delta.impl.DeleteMember;
import net.zaros.server.game.clan.channel.delta.impl.UpdateMember;
import net.zaros.server.network.master.server.network.packet.out.ClanChannelMessageResponsePacketOut;
import net.zaros.server.network.master.server.world.MSClans.ClanChannelMessageResponse;
import net.zaros.server.network.master.server.world.MSRepository;

/**
 * Represents the clan channel.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class ClanChannel {

	/**
	 * The current members list within the clan channel.
	 */
	@Getter
	private final List<ChannelMember> members = new ArrayList<ChannelMember>();

	/**
	 * The members by world cache.
	 */
	@Getter
	private final Map<Integer, Set<Long>> membersByWorld = new HashMap<Integer, Set<Long>>();

	/**
	 * The clan channel delta updater.
	 */
	@Getter
	private final ClanChannelDelta delta = new ClanChannelDelta(this);

	/**
	 * The clan channel update block.
	 */
	@Getter
	private final ClanChannelBlock updateBlock = new ClanChannelBlock(this);

	/**
	 * The clan hash.
	 */
	@Getter
	private final long clanId;

	/**
	 * The clan display name.
	 */
	@Getter
	private final String name;

	/**
	 * The clan talk rank.
	 */
	@Getter
	private final ClanRank talkRank;

	/**
	 * The clan kick rank.
	 */
	@Getter
	private final ClanRank kickRank;

	/**
	 * The current update number.
	 */
	@Getter
	private long updateNumber;

	/**
	 * Sends a clan channel message to all of the channel members.
	 * 
	 * @param displayName
	 *                    the message sender player diisplay name.
	 * @param modLevel
	 *                    the message sender player moderator level.
	 * @param message
	 *                    the clan channel message text content.
	 */
	public void sendMessage(String displayName, int modLevel, String message) {
		membersByWorld.forEach((worldId, ids) -> {
			MSRepository.getWorld(worldId).ifPresent(world -> {
				world.getSession().write(new ClanChannelMessageResponsePacketOut(ClanChannelMessageResponse.SUCCESSFUL, ids, displayName, modLevel, message));
			});
		});
	}

	/**
	 * Posts a delta channel update.
	 */
	public void postUpdate(LongPredicate filter) {
		delta.post(filter);
		// increment the current update number.
		updateNumber++;
	}

	/**
	 * Adds a new member to the clan channel.
	 * 
	 * @param userId
	 *               the member user id.
	 * @param name
	 *               the member name.
	 * @param rank
	 *               the member rank.
	 * @param world
	 *               the member world id.
	 */
	public void addMember(long userId, String name, ClanRank rank, int world) {
		addMember(new ChannelMember(userId, name, rank, world));
	}

	/**
	 * Adds the specified {@link ChannelMember} to our clan channel.
	 * 
	 * @param member
	 *               the channel member to add.
	 */
	public void addMember(ChannelMember member) {
		// cache the members by world.
		cacheMember(member.getWorld(), member.getUserId());
		// attempts to add the member to the members list.
		members.add(member);
		// tell the delta updater to add a new member.
		delta.enqueue(new AddMember(member));
	}

	/**
	 * Removes the specified {@link ChannelMember} from our channel.
	 * 
	 * @param member
	 *               the member to remove.
	 */
	public void removeMember(ChannelMember member) {
		var slot = members.indexOf(member);
		if (slot == -1) {
			return;
		}
		uncacheMember(member.getWorld(), member.getUserId());
		members.remove(slot);
		delta.enqueue(new DeleteMember(slot));
	}

	/**
	 * Updates the member with the specified {@code userId} with the new provided
	 * data.
	 * 
	 * @param userId
	 *                    the user id which is linked to the member we want to
	 *                    update.
	 * @param displayName
	 *                    the member's new display name.
	 */
	public void updateMember(long userId, String displayName) {
		var member = getMemberById(userId);
		if (member == null) {
			return;
		}
		member.displayName = displayName;
		updateMember(member);
	}

	/**
	 * Updates the member with the specified {@code userId} with the new provided
	 * data.
	 * 
	 * @param userId
	 *               the user id which is linked to the member we want to update.
	 * @param world
	 *               the member's current world id.
	 */
	public void updateMember(long userId, int world) {
		var member = getMemberById(userId);
		if (member == null) {
			return;
		}
		if (member.getWorld() != 0) {
			uncacheMember(member.getWorld(), member.getUserId());
		}
		cacheMember(world, member.getUserId());
		member.world = world;
		updateMember(member);
	}

	/**
	 * Updates the member with the specified {@code userId} with the new provided
	 * data.
	 * 
	 * @param userId
	 *               the user id which is linked to the member we want to update.
	 * @param world
	 *               the member's current world id.
	 * @param rank
	 *               the member's new rank.
	 */
	public void updateMember(long userId, ClanRank rank) {
		var member = getMemberById(userId);
		if (member == null) {
			return;
		}
		member.rank = rank;
		updateMember(member);
	}

	/**
	 * Updates the member with the specified {@code userId} with the new provided
	 * data.
	 * 
	 * @param userId
	 *                    the user id which is linked to the member we want to
	 *                    update.
	 * @param displayName
	 *                    the member's new display name.
	 * @param world
	 *                    the member's current world id.
	 * @param rank
	 *                    the member's new rank.
	 */
	public void updateMember(long userId, String displayName, int world, ClanRank rank) {
		var member = getMemberById(userId);
		if (member == null) {
			return;
		}
		if (member.getWorld() != 0) {
			uncacheMember(member.getWorld(), member.getUserId());
		}
		cacheMember(world, member.getUserId());
		member.displayName = displayName;
		member.world = world;
		member.rank = rank;
		updateMember(member);
	}

	/**
	 * Updates the client with specified {@link ChannelMember} data.
	 * 
	 * @param member
	 *               the member to update.
	 */
	public void updateMember(ChannelMember member) {
		var slot = members.indexOf(member);
		if (slot == -1) {
			return;
		}
		delta.enqueue(new UpdateMember(slot, member));
	}

	/**
	 * Gets the member with the specified {@code userID}.
	 * 
	 * @param userId
	 *               the user id that should match the member's user id.
	 * @return the member if it was found otherwise {@code null}.
	 */
	public ChannelMember getMemberById(long userId) {
		for (var member : members) {
			if (member.getUserId() == userId) {
				return member;
			}
		}
		return null;
	}

	/**
	 * Caches the specified member {@code userId} in the members by world map.
	 * 
	 * @param worldId
	 *                the world which the user is currently or was in.
	 * @param userId
	 *                the user id to cache.
	 */
	private void cacheMember(int worldId, long userId) {
		var worldMembers = membersByWorld.get(worldId);
		if (worldMembers == null) {
			membersByWorld.put(worldId, worldMembers = new HashSet<Long>());
		}
		worldMembers.add(userId);
	}

	/**
	 * Uncaches the specified member {@code userId} from the members by world map.
	 * 
	 * @param worldId
	 *                the world which the user is currently or was in.
	 * @param userId
	 *                the user id to uncache.
	 */
	private void uncacheMember(int worldId, long userId) {
		var worldMembers = membersByWorld.get(worldId);
		if (worldMembers != null) {
			worldMembers.remove(userId);
			if (worldMembers.size() < 1) {
				membersByWorld.remove(worldId);
			}
		}
	}

	/**
	 * Represents a clan channel update block.
	 * 
	 * @author Walied K. Yassen
	 */
	@RequiredArgsConstructor
	public static final class ClanChannelBlock {

		/**
		 * The clan channel instance which this block is for.
		 */
		private final ClanChannel channel;

		/**
		 * The last update number.
		 */
		@Getter
		private long updateNumber;

		/**
		 * The last update block.
		 */
		private byte[] updateBlock;

		/**
		 * Rebuilds the update block.
		 */
		public void rebuild() {
			// create the block builder.
			var builder = new DynamicBuffer(128);
			// write the settings flag.
			// 0x1 - use_memberhashes
			// 0x2 - use_displaynames
			builder.writeByte(0x2);
			// write the clan hash.
			builder.writeLong(channel.getClanId());
			// write the update number, it must be the current
			// update number, which is the latest.
			builder.writeLong(channel.getUpdateNumber());
			// write the clan name.
			builder.writeString(channel.getName());
			// redundant value, 0 for now.
			builder.writeByte(0);
			// write the channel permissions.
			builder.writeByte(channel.getKickRank().getId());
			builder.writeByte(channel.getTalkRank().getId());
			// write the clan permissions.
			var members = channel.getMembers();
			// write the amount of members.
			builder.writeShort(members.size());
			// write each members data.
			for (var member : members) {
				// write the members display name.
				builder.writeString(member.getDisplayName());
				// write the members clan rank id.
				builder.writeByte(member.getRank().getId());
				// write the members world.
				builder.writeShort(member.getWorld());
			}
			// store the built update block.
			updateBlock = builder.getDataTrimmed();
			// update our cached update number.
			updateNumber = channel.getUpdateNumber();
		}

		/**
		 * Gets the update block data. If the data is outdated, we rebuild the data
		 * using {@link #rebuild()} method.
		 * 
		 * @return the update block data as {@link byte} array.
		 */
		public byte[] getBytes() {
			if (updateNumber != channel.updateNumber) {
				rebuild();
			}
			return updateBlock;
		}
	}

	/**
	 * Represents a single channel member.
	 * 
	 * @author Walied K. Yassen
	 */
	@AllArgsConstructor
	public static final class ChannelMember {

		/**
		 * The member user id.
		 */
		@Getter
		private final long userId;

		/**
		 * The member display name.
		 */
		@Getter
		private String displayName;

		/**
		 * The member clan rank.
		 */
		@Getter
		private ClanRank rank;

		/**
		 * The world in which the member is currently in.
		 */
		@Getter
		private int world;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (userId ^ userId >>> 32);
			return result;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			ChannelMember other = (ChannelMember) obj;
			if (userId != other.userId) {
				return false;
			}
			return true;
		}
	}
}
