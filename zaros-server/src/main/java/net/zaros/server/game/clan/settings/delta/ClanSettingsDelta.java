package net.zaros.server.game.clan.settings.delta;

import java.util.LinkedList;
import java.util.Queue;
import java.util.function.LongPredicate;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import net.zaros.cache.util.buffer.DynamicBuffer;
import net.zaros.server.game.clan.settings.ClanSettings;
import net.zaros.server.network.master.server.world.MSRepository;

/**
 * Represents the clan settings delta update block manager.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class ClanSettingsDelta {

	/**
	 * The pending actions list.
	 */
	private final Queue<ClanSettingsAction> actions = new LinkedList<>();

	/**
	 * The owner clan settings.
	 */
	private final ClanSettings settings;

	/**
	 * Posts the current delta block update to all of the online settings members.
	 * 
	 * @param filter
	 *               the clan settings member filter, when a member is filtered, the
	 *               delta update will not be sent to that filtered member.
	 */
	public void post(LongPredicate filter) {
		var block = encode();
		settings.getMembersByWorld().forEach((worldId, ids) -> {
			var userIds = filter == null ? ids : ids.stream().filter(f -> !filter.test(f)).collect(Collectors.toSet());
			MSRepository.getWorld(worldId).ifPresent(world -> {
//				world.getSession().write(new ClanSettingsDeltaPacketOut(userIds, block));
			});
		});
	}

	/**
	 * Encodes the clan channel delta update block.
	 * </p>
	 * The resulted block is ready to be transmitted over to the client.
	 * 
	 * @return the encoded update block as a {@code byte} array.
	 */
	private byte[] encode() {
		var buffer = new DynamicBuffer(128);
		buffer.writeLong(settings.getClan().getId());
		buffer.writeInt(settings.getUpdateNumber());
		do {
			var action = dequeue();
			if (action == null) {
				break;
			}
			buffer.writeByte(action.getId());
			action.encode(buffer);
		} while (true);
		// termination byte, tells that the actions queue is over.
		buffer.writeByte(0);
		return buffer.getDataTrimmed();
	}

	/**
	 * Enqueues a new {@link ClanSettingsAction} for the next update.
	 * 
	 * @param action
	 *               the clan settings action to enqueue.
	 */
	public void enqueue(ClanSettingsAction action) {
		synchronized (actions) {
			actions.add(action);
		}
	}

	/**
	 * Dequeues the next {@link ClanSettingsAction} from the queue.
	 * 
	 * @return the dequeued {@link ClanSettingsAction} object or {@code null} if
	 *         there was non to dequeue.
	 */
	private ClanSettingsAction dequeue() {
		synchronized (actions) {
			return actions.poll();
		}
	}
}
