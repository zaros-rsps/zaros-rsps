package net.zaros.server.game.content.combat.script.impl;

import net.zaros.server.game.content.combat.CombatManager;
import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.content.combat.script.impl.formulas.HitDelayFormula;
import net.zaros.server.game.content.skills.magic.Spell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitModifier;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.Hit;

public class MagicCombatScript implements CombatScript {

    @Override
    public boolean executable(Entity attacker, Entity target) {

        if (attacker.isNPC()) {
            return true;
        }

        Spell currentSpell = attacker.getTemporaryAttribute(AttributeKey.CURRENT_SPELL, null);

        if (currentSpell == null) {

            Spell autocast = attacker.getTemporaryAttribute(AttributeKey.AUTOCAST_SPELL);

            if (autocast != null) {
                attacker.putTemporaryAttribute(AttributeKey.CURRENT_SPELL, autocast);
                currentSpell = autocast;
            }
        }

        if (currentSpell != null) {
            return currentSpell.ableToCastOnEntity(attacker, target);
        }

        return false;
    }

    @Override
    public int getAttackDistance(Entity attacker, Entity target) {
        return 10;
    }

    @Override
    public void preExecution(Entity attacker, Entity target) {

    }

    @Override
    public void execute(Entity attacker, Entity target) {
        Spell currentSpell = attacker.getTemporaryAttribute(AttributeKey.CURRENT_SPELL, null);

        if (currentSpell == null) {
            return;
        }

        currentSpell.castOnEntity(attacker, target);
    }

    @Override
    public Hit.HitSplat getHitSplat(Entity attacker, Entity target) {
        return Hit.HitSplat.MAGIC_DAMAGE;
    }

    @Override
    public Hit[] getHits(Entity attacker, Entity target) {

        CombatSpell currentSpell = attacker.getTemporaryAttribute(AttributeKey.CURRENT_SPELL, null);

        if (currentSpell == null) {
            return new Hit[0];
        }

        int delay = HitDelayFormula.getMagicDelay(attacker, target);

        HitEffect effect = currentSpell.getEffect(attacker, target);
        HitModifier modifier = currentSpell.getModifier(attacker, target);

        CombatHit hit = new CombatHit(attacker, target, this, Hit.HitSplat.MAGIC_DAMAGE, delay, effect, modifier);

        return new Hit[]{hit};
    }

    @Override
    public int getSpeed(Entity attacker, Entity target) {

        CombatSpell currentSpell = attacker.getTemporaryAttribute(AttributeKey.CURRENT_SPELL, null);

        if (currentSpell == null) {
            return 5;
        }

        return currentSpell.attackDelay();
    }

    @Override
    public boolean specialAttack(Entity attacker, Entity target) {
        return false;
    }

    @Override
    public void postExecution(Entity attacker, Entity target) {
        CombatSpell currentSpell = attacker.getTemporaryAttribute(AttributeKey.CURRENT_SPELL, null);

        if (currentSpell != null) {
            attacker.putTemporaryAttribute(AttributeKey.PREVIOUS_SPELL, currentSpell);
            attacker.removeTemporaryAttribute(AttributeKey.CURRENT_SPELL);
        }

        if (attacker.getTemporaryAttribute(AttributeKey.AUTOCAST_SPELL, null) == null) {
            CombatManager.endCombat(attacker);
        }
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.getSource() == null) {
            return;
        }

        CombatSpell currentSpell = hit.getSource().getTemporaryAttribute(AttributeKey.PREVIOUS_SPELL, null);

        if (currentSpell != null) {
            currentSpell.postHitExecution(hit);
        }
    }
}
