package net.zaros.server.game.node.entity.link.interaction;

import net.zaros.server.game.content.event.EventListener;
import net.zaros.server.game.content.event.EventListener.EventType;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.utility.rs.constant.NPCConstants;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/3/2017
 */
public class NPCInteraction extends Interaction {

	public NPCInteraction(Entity source, Entity target) {
		super(source, target);
	}

	@Override
	public void start() {
		source.faceEntity(target);
		source.getMovement().getWalkSteps().clear();
		if ((((NPC) target).getWalkType() & NPCConstants.NORMAL_WALK) != 0) {
			target.faceEntity(source);
		}
		target.getMovement().getWalkSteps().clear();
		EventListener.setListener(source, this::end, EventType.MOVE, EventType.DAMAGE, EventType.SCREEN_INTERFACE_CLOSE);
	}

	@Override
	public void request() {

	}

	@Override
	public void end() {
		source.getInteractionManager().end();
		target.getInteractionManager().end();
	}
}
