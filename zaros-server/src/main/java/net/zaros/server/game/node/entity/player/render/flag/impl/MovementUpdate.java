package net.zaros.server.game.node.entity.player.render.flag.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.UpdateFlag;
import net.zaros.server.network.world.packet.PacketBuilder;

/**
 * Represents the movement update flag.
 *
 * @author Emperor
 */
public class MovementUpdate extends UpdateFlag {
	
	/**
	 * The speed to move.
	 */
	private final int type;
	
	/**
	 * Constructs a new {@code MovementUpdate} {@code Object}.
	 *
	 * @param player
	 * 		The player.
	 */
	public MovementUpdate(Player player) {
		if (player.getMovement().getNextRunDirection() != -1) {
			type = 2;
		} else {
			type = 1;
		}
	}
	
	@Override
	public void write(Player outgoing, PacketBuilder packet) {
		packet.writeByteS(type);
	}
	
	@Override
	public int getOrdinal() {
		return 17;
	}
	
	@Override
	public int getMaskData() {
		return 0x40;
	}
	
}