package net.zaros.server.game.content.skills.summoning;

import lombok.Getter;

public enum FamiliarData {

    PACK_YAK(96, 10, 3480000, 12093, 6874, 422.2, "Baroo!"),

    ;

    @Getter
    int level, cost, duration, pouch, familiarId;

    @Getter
    double experience;

    @Getter
    String shout;

    FamiliarData(int level, int cost, int duration, int pouch, int familiarId, double experience, String shout) {
        this.level = level;
        this.cost = cost;
        this.duration = duration;
        this.pouch = pouch;
        this.familiarId = familiarId;
        this.experience = experience;
        this.shout = shout;
    }

}
