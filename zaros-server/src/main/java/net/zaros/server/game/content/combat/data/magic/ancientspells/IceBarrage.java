package net.zaros.server.game.content.combat.data.magic.ancientspells;

import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.effects.impl.FreezeEffect;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitEffectType;
import net.zaros.server.game.node.entity.hit.HitInstant;
import net.zaros.server.game.node.entity.render.flag.impl.GraphicHeight;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;

import java.util.List;

/**
 * The ice barrage spell on the ancient spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class IceBarrage extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 23;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.WATER_RUNE1, 6), new Item(Items.BLOOD_RUNE1, 2), new Item(Items.DEATH_RUNE1, 4));

    @Override
    public void playGraphics(Entity source, Entity target) {
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(1979);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return new HitEffect(HitEffectType.FREEZE, HitInstant.CALCULATION, 20);
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            if (hit.getTarget().getEffectManager().isEffectPresent(FreezeEffect.getName())) {
                hit.getTarget().sendGraphics(1677, GraphicHeight.HIGH.toInt(), 0);
            } else {
                hit.getTarget().sendGraphics(369);
            }
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int autocastConfigID() {
        return 93;
    }

    @Override
    public int magicLevelRequirement() {
        return 94;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 300;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 52;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.ANCIENTS;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }

    @Override
    public boolean multi() {
        return true;
    }
}
