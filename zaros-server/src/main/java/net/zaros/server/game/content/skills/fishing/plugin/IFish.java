package net.zaros.server.game.content.skills.fishing.plugin;

/**
 * @author Jacob Rhiel
 */
public interface IFish {

    default void spotSpecific(FishingSpot spot) {

    }

    default void special() {

    }

}
