package net.zaros.server.game.clan;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Represents a clan member rank.
 * 
 * @author Walied K. Yassen
 */
@AllArgsConstructor
public enum ClanRank {

	/**
	 * The recruit clan rank.
	 */
	RECRUIT(0),

	/**
	 * The corporal clan rank.
	 */
	CORPORAL(1),

	/**
	 * The sergeant clan rank.
	 */
	SERGEANT(2),

	/**
	 * The lieutenant clan rank.
	 */
	LIEUTENANT(3),

	/**
	 * The captain clan rank.
	 */
	CAPTAIN(4),

	/**
	 * The general clan rank.
	 */
	GENERAL(5),

	/**
	 * The admin clan rank.
	 */
	ADMIN(100),

	/**
	 * The organiser clan rank.
	 */
	ORGANISER(101),

	/**
	 * The coordinator clan rank.
	 */
	COORDINATOR(102),

	/**
	 * The overseer clan rank.
	 */
	OVERSEER(103),

	/**
	 * The deputy owner clan rank.
	 */
	DEPUTY_OWNER(125),

	/**
	 * The owner clan rank.
	 */
	OWNER(126),

	/**
	 * A special rank for server staff members.
	 */
	JMOD(127);

	/**
	 * The rank id.
	 */
	@Getter
	private final int id;
}
