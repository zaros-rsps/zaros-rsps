package net.zaros.server.game.module.interaction.rsinterface;

import net.zaros.server.game.module.type.InterfaceInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigFilePacketBuilder;

public class SkillChatboxInterfaceInteractionModule implements InterfaceInteractionModule {

    @Override
    public int[] interfaceSubscriptionIds() {
        return arguments(905, 916);
    }

    @Override
    public boolean handle(Player player, int interfaceId, int componentId, int itemId, int slotId, int packetId) {
        /*if (player.getManager().getSkillManager() != null) {
            SkillSegment segment = player.getManager().getSkillManager().getSkillSegment();
            System.out.println(segment);
            int maximum = segment.getQuantity(player, segment.getTriggerNode());
            if (componentId == 5) {
                //segment.setAmount(1);
                sendQuantityUpdate(player, 1, 1);
            } else if (componentId == 6) {
                //segment.setAmount(5);
                sendQuantityUpdate(player, 5, maximum);
            } else if (componentId == 7) {
                //segment.setAmount(10);
                sendQuantityUpdate(player, 10, maximum);
            }// else if (componentId == 8)
                //segment.setAmount(28);//TODO temporary
            else if (componentId == 14 || componentId == 15 || componentId == 16 || componentId == 17) {
				segment.executeSegment(player, segment.getTriggerNode());
			} else if (componentId == 19) {
                sendQuantityUpdate(player, getQuantity(player) + 1, maximum);
                //segment.setAmount(getQuantity(player));
            } else if (componentId == 20) {
                sendQuantityUpdate(player, getQuantity(player) - 1, maximum);
                //segment.setAmount(getQuantity(player));
            }
            return true;
        }*/
        return false;
    }

    public void sendQuantityUpdate(Player player, int quantity, int maximum) {
        if(quantity < 1) {
            quantity = 1;
        } else if(quantity > maximum) {
            quantity = maximum;
        }
        /*if (player.getManager().getSkillManager().getLastSkillInterfaceAmount() != quantity) {
			player.getManager().getSkillManager().setLastSkillInterfaceAmount(quantity);
		}*/
        player.getTransmitter().send(new ConfigFilePacketBuilder(8095, quantity).build(player));
    }

    public int getQuantity(Player player) {
        return -1;//player.getManager().getSkillManager().getLastSkillInterfaceAmount();
    }

}
