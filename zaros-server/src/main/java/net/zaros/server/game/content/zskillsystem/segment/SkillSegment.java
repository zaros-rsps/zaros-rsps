package net.zaros.server.game.content.zskillsystem.segment;

import net.zaros.server.game.content.zskillsystem.trigger.SkillTrigger;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Jacob Rhiel
 */
public interface SkillSegment {

    boolean hasRequirements(Player player);

    boolean trigger(Player player, SkillTrigger trigger);

}
