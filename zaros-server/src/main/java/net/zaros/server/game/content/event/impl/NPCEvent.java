package net.zaros.server.game.content.event.impl;

import net.zaros.server.core.event.npc.NpcEvent;
import net.zaros.server.core.event.npc.NpcOption;
import net.zaros.server.game.content.dialogue.DialogueRepository;
import net.zaros.server.game.content.event.Event;
import net.zaros.server.game.content.event.context.NPCEventContext;
import net.zaros.server.game.module.ModuleRepository;
import net.zaros.server.game.node.entity.link.interaction.NPCInteraction;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.link.LockManager.LockType;
import net.zaros.server.game.world.World;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/1/2017
 */
public class NPCEvent extends Event<NPCEventContext> {

	@Override
	public void run(Player player, NPCEventContext context) {
		NPC npc = context.getNpc();
		InteractionOption option = context.getOption();
		player.getInteractionManager().startInteraction(new NPCInteraction(player, npc));
		npc.getInteractionManager().startInteraction(new NPCInteraction(player, npc));
		if (player.getManager().getActivities().handlesNodeInteraction(npc, option)) {
			return;
		}
		if (World.getEventManager().post(new NpcEvent(player, npc, toNpcOption(option)))) {
			return;
		}
		if (option == InteractionOption.FIRST_OPTION && DialogueRepository.handleNPC(player, npc)) {
			return;
		}
		if (ModuleRepository.handle(player, npc, option)) {
			return;
		}
		player.getTransmitter().sendMessage("Nothing interesting happens.");
	}

	@Override
	public boolean canStart(Player player, NPCEventContext context) {
		return !player.getManager().getLocks().isLocked(LockType.NPC_INTERACTION);
	}

	private static NpcOption toNpcOption(InteractionOption option) {
		switch (option) {
		case FIRST_OPTION:
			return NpcOption.OPTION1;
		case SECOND_OPTION:
			return NpcOption.OPTION2;
		case THIRD_OPTION:
			return NpcOption.OPTION3;
		case FOURTH_OPTION:
			return NpcOption.OPTION4;
		case FIFTH_OPTION:
			return NpcOption.OPTION5;
		default:
			return null;
		}
	}
}
