package net.zaros.server.game.module.interaction.rsinterface;

import net.zaros.server.game.content.event.impl.item.ItemEvent;
import net.zaros.server.game.content.market.shop.Shop;
import net.zaros.server.game.module.type.InterfaceInteractionModule;
import net.zaros.server.game.node.entity.player.Player;

import static net.zaros.server.game.content.market.shop.Shop.INTERFACE_ID;
import static net.zaros.server.network.NetworkConstants.*;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/15/2017
 */
public class ShopInterfaceInteractionModule implements InterfaceInteractionModule {
	
	@Override
	public int[] interfaceSubscriptionIds() {
		return arguments(INTERFACE_ID, INVENTORY_INTERFACE_ID);
	}
	
	@Override
	public boolean handle(Player player, int interfaceId, int componentId, int itemId, int slotId, int packetId) {
		Shop shop = player.getTemporaryAttribute("open_shop");
		if (shop == null) {
			return true;
		}
		if (interfaceId == INTERFACE_ID) {
			if (componentId == 25) {
				switch (packetId) {
					case FIRST_PACKET_ID:
						shop.value(player, slotId, false);
						break;
					case SECOND_PACKET_ID:
						shop.buy(player, slotId, 1);
						break;
					case THIRD_PACKET_ID:
						shop.buy(player, slotId, 5);
						break;
					case LAST_PACKET_ID:
						shop.buy(player, slotId, 10);
						break;
					case FIFTH_PACKET_ID:
						shop.buy(player, slotId, 50);
						break;
					case SIXTH_PACKET_ID:
						shop.buy(player, slotId, 500);
						break;
					case EXAMINE_PACKET_ID:
						ItemEvent.handleItemExamining(player, shop.getItem(slotId / 6));
						break;
				}
				return true;
			}
		} else if (interfaceId == INVENTORY_INTERFACE_ID) {
			if (componentId == 0) {
				switch (packetId) {
					case FIRST_PACKET_ID:
						shop.value(player, slotId, true);
						break;
					case SECOND_PACKET_ID:
						shop.sell(player, slotId, 1);
						break;
					case THIRD_PACKET_ID:
						shop.sell(player, slotId, 5);
						break;
					case LAST_PACKET_ID:
						shop.sell(player, slotId, 10);
						break;
					case FIFTH_PACKET_ID:
						shop.sell(player, slotId, 50);
						break;
					case EXAMINE_PACKET_ID:
						ItemEvent.handleItemExamining(player, player.getInventory().getItems().get(slotId));
						break;
				}
				return true;
			}
		}
		return true;
	}
}
