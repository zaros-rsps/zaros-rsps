/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.module.interaction.object;

import net.zaros.server.game.module.type.ObjectInteractionModule;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Walied K. Yassen
 */
public class GamersGrottoInteractionModule extends ObjectInteractionModule {

	/**
	 * Construct a new {@link GamersGrottoInteractionModule} type object instance.
	 */
	public GamersGrottoInteractionModule() {
		register(20602, 20604);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.module.type.ObjectInteractionModule#handle(org.redrune.game.
	 * node.entity.player.Player, org.redrune.game.node.object.GameObject,
	 * org.redrune.utility.rs.InteractionOption)
	 */
	@Override
	public boolean handle(Player player, GameObject object, InteractionOption option) {
		final int id = object.getId();
		if (id == 20602) {
			player.teleport(new Location(2954, 9675, 0));
		} else if (id == 20604) {
			player.teleport(new Location(3018, 3404, 0));
		}
		return true;
	}

}
