package net.zaros.server.game.content.combat.player.registry.spell.modern.god;

import net.zaros.server.core.system.SystemManager;
import net.zaros.server.game.content.combat.player.registry.wrapper.context.CombatSpellContext;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.CombatSpellEvent;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.constant.MagicConstants.MagicBook;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/7/2017
 */
public class ClawsOfGuthixEvent implements CombatSpellEvent {
	
	@Override
	public int delay(Player player) {
		return 5;
	}
	
	@Override
	public int animationId() {
		return 811;
	}
	
	@Override
	public int hitGfx() {
		return 77;
	}
	
	@Override
	public int maxHit(Player player, Entity target) {
		if (player.getVariables().getAttribute(AttributeKey.GOD_CHARGED, -1L) >= SystemManager.getUpdateWorker().getTicks()) {
			return 300;
		} else {
			return 200;
		}
	}
	
	@Override
	public int spellId() {
		return 67;
	}
	
	@Override
	public double exp() {
		return 34.5;
	}
	
	@Override
	public MagicBook book() {
		return MagicBook.REGULAR;
	}
	
	@Override
	public void cast(Player player, CombatSpellContext context) {
		context.getSwing().sendSpell(player, context.getTarget(), this);
	}
}
