package net.zaros.server.game.node.entity.data.weapon;

import lombok.Getter;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.impl.ammunitions.Arrows;
import net.zaros.server.game.node.entity.hit.HitModifier;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.constant.EquipConstants;

import java.util.HashMap;
import java.util.Map;

/**
 * The interface for weapon ammunition.
 *
 * @author Gabriel || Wolfsdarker
 */
public interface Ammunition {

    /**
     * The map of ammunition.
     */
    Map<Integer, Ammunition> ammunition = new HashMap<>();

    /**
     * The slot where the ammunition is located.
     *
     * @return the equipment slot
     */
    int ammoSlot();

    /**
     * The ammo's poison damage.
     *
     * @return the damage
     */
    int poisonDamage();

    /**
     * If it has ava'as accumulator support.
     *
     * @return if its supported
     */
    boolean accumulatorSupport();

    /**
     * Returns if the entity can use the ammo.
     *
     * @param entity
     * @return if its usable
     */
    boolean usable(Entity entity);

    /**
     * Makes the entity play a graphics.
     *
     * @param entity
     */
    void performGraphics(Entity entity);

    /**
     * Sends the projectile from the attacker to the target.
     *
     * @param attacker
     * @param target
     */
    void sendProjetile(Entity attacker, Entity target);

    /**
     * The ammunition's hit modifier.
     *
     * @param attacker
     * @param target
     * @return the modifier
     */
    default HitModifier hitModifier(Entity attacker, Entity target) {
        return null;
    }


    /**
     * Applies the ammo's special effect, if the hit modifier is present.
     *
     * @param attacker
     * @param target
     */
    default void applySpecialEffect(Entity attacker, Entity target) {
    }

    /**
     * Returns if the entity has the ammunition.
     *
     * @param entity
     * @return if its present
     */
    static boolean hasAmmo(Entity entity, int amount) {
        if (entity.isNPC()) {
            return true;
        }

        WeaponInterface weapon = entity.getTemporaryAttribute(AttributeKey.WEAPON);
        var player = entity.toPlayer();

        if (weapon == null || weapon.ammunition() == null || weapon.compatibleAmmunitionIDs().isEmpty()) {
            return true;
        }

        var ammo = entity.toPlayer().getEquipment().getItem(weapon.ammunition().ammoSlot());

        if (ammo == null || ammo.getAmount() < amount) {
            entity.toPlayer().getTransmitter().sendMessage("You don't have the required amount of ammunition to fire that.");
            return false;
        }

        if (!weapon.compatibleAmmunitionIDs().contains(ammo.getId())) {
            player.getTransmitter().sendMessage(player.getEquipment().getItem(EquipConstants.SLOT_WEAPON).getName() +
                    " can not be used with " + ammo.getName() + ".");
            return false;
        }

        return true;
    }

    /**
     * Returns if the entity has a poisonous ammunition.
     *
     * @param entity
     * @return if its poisonous
     */
    static boolean hasPoisonousAmmo(Entity entity) {
        if (entity.isNPC()) {
            return true;
        }

        WeaponInterface weapon = entity.getTemporaryAttribute(AttributeKey.WEAPON);

        if (weapon == null || weapon.ammunition() == null || weapon.compatibleAmmunitionIDs().isEmpty()) {
            return true;
        }

        var ammo = entity.toPlayer().getEquipment().getItem(weapon.ammunition().ammoSlot());

        if (ammo == null) {
            return false;
        }

        Ammunition ammoData = ammunition.get(ammo.getId());

        return ammoData != null && ammoData.poisonDamage() > 0;
    }

    /**
     * Decrements the entity's ammunition.
     *
     * @param entity
     */
    default void decrementAmmo(Entity entity, int amount) {
        if (entity.isNPC()) {
            return;
        }

        if (!hasAmmo(entity, amount)) {
            return;
        }

        Player player = entity.toPlayer();
        Item current = player.getEquipment().getItem(ammoSlot());

        if (current != null && current.getAmount() >= amount) {
            if (current.getAmount() > amount) {
                player.getEquipment().getItems().set(ammoSlot(), new Item(current.getId(), current.getAmount() - amount));
            } else {
                player.getEquipment().getItems().set(ammoSlot(), null);
            }
        }
    }

    /**
     * Loads all the ammunition.
     */
    static void load() {
        ammunition.put(Items.BRONZE_ARROW, new Arrows());
        ammunition.put(Items.BRONZE_ARROW_P, new Arrows(40));
        ammunition.put(Items.BRONZE_ARROW_PP, new Arrows(50));
        ammunition.put(Items.BRONZE_ARROW_PPP, new Arrows(60));

        ammunition.put(Items.IRON_ARROW, new Arrows());
        ammunition.put(Items.IRON_ARROW_P, new Arrows(40));
        ammunition.put(Items.IRON_ARROW_PP, new Arrows(50));
        ammunition.put(Items.IRON_ARROW_PPP, new Arrows(60));

        ammunition.put(Items.STEEL_ARROW, new Arrows());
        ammunition.put(Items.STEEL_ARROW_P, new Arrows(40));
        ammunition.put(Items.STEEL_ARROW_PP, new Arrows(50));
        ammunition.put(Items.STEEL_ARROW_PPP, new Arrows(60));

        ammunition.put(Items.MITHRIL_ARROW1, new Arrows());
        ammunition.put(Items.MITHRIL_ARROW_P, new Arrows(40));
        ammunition.put(Items.MITHRIL_ARROW_PP, new Arrows(50));
        ammunition.put(Items.MITHRIL_ARROW_PPP, new Arrows(60));

        ammunition.put(Items.ADAMANT_ARROW, new Arrows());
        ammunition.put(Items.ADAMANT_ARROW_P, new Arrows(40));
        ammunition.put(Items.ADAMANT_ARROW_PP, new Arrows(50));
        ammunition.put(Items.ADAMANT_ARROW_PPP, new Arrows(60));

        ammunition.put(Items.RUNE_ARROW, new Arrows());
        ammunition.put(Items.RUNE_ARROW_P, new Arrows(40));
        ammunition.put(Items.RUNE_ARROW_PP, new Arrows(50));
        ammunition.put(Items.RUNE_ARROW_PPP, new Arrows(60));

        ammunition.put(Items.DRAGON_ARROW, new Arrows());
        ammunition.put(Items.DRAGON_ARROW_P, new Arrows(40));
        ammunition.put(Items.DRAGON_ARROW_PP, new Arrows(50));
        ammunition.put(Items.DRAGON_ARROW_PPP, new Arrows(60));

        ammunition.put(Items.BROAD_ARROW, new Arrows());
    }

}
