package net.zaros.server.game.content.combat.player.registry.spell.ancient;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.player.registry.wrapper.context.CombatSpellContext;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.CombatSpellEvent;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.effects.impl.FreezeEffect;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.constant.MagicConstants.MagicBook;

import java.util.concurrent.TimeUnit;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/1/2017
 */
public class IceBurstEvent  implements CombatSpellEvent {
	
	@Override
	public int delay(Player player) {
		return 4;
	}
	
	@Override
	public int animationId() {
		return 1979;
	}
	
	@Override
	public int hitGfx() {
		return 363;
	}
	
	@Override
	public int maxHit(Player player, Entity target) {
		return 220;
	}
	
	@Override
	public int spellId() {
		return 22;
	}
	
	@Override
	public double exp() {
		return 46;
	}
	
	@Override
	public int gfxHeight() {
		return 0;
	}
	
	@Override
	public MagicBook book() {
		return MagicBook.ANCIENTS;
	}
	
	@Override
	public void cast(Player player, CombatSpellContext context) {
		// storing vars before spell is cast
		final Entity target = context.getTarget();
		ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(player, target, 366, 43, 21, 52, 15, 0));
		context.getSwing().sendMultiSpell(player, context.getTarget(), this, () -> {
			// only freeze the player if they are unfreezeable when the spell is cast.
			context.getTarget().getEffectManager().apply(new FreezeEffect(context.getTarget(), player, 10));
		}, null);
	
	}
}
