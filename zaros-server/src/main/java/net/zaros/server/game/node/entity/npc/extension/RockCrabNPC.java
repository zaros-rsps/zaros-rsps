package net.zaros.server.game.node.entity.npc.extension;

import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.utility.rs.constant.Directions.Direction;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/25/2017
 */
public class RockCrabNPC extends NPC {
	
	/**
	 * The original id of the rock crab
	 */
	private final int originalId;
	
	/**
	 * Constructs a new {@code Entity}
	 *
	 * @param id
	 * 		The id of the npc
	 */
	public RockCrabNPC(int id, Location location, Direction direction) {
		super(id, location, direction);
		this.originalId = id;
		getCombatDefinitions().setAggressiveForced(true);
		getCombatDefinitions().setFindTargetRadius(1);
	}
	
	@Override
	public void reset() {
		super.reset();
		// so the npc that re-spawns is the rock crab
		setId(originalId);
		// the that the original only finds 1 tile close-by
		getCombatDefinitions().setFindTargetRadius(1);
	}

}