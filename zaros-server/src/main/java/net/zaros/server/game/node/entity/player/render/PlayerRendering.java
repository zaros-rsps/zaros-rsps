package net.zaros.server.game.node.entity.player.render;

import java.util.PriorityQueue;

import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.RenderInformation;
import net.zaros.server.game.node.entity.player.render.flag.impl.AppearanceUpdate;
import net.zaros.server.game.node.entity.player.render.update.GlobalUpdateStage;
import net.zaros.server.game.node.entity.player.render.update.LocalUpdateStage;
import net.zaros.server.game.node.entity.render.flag.UpdateFlag;
import net.zaros.server.game.world.World;
import net.zaros.server.game.world.region.RegionManager;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.Packet.PacketType;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;
import net.zaros.server.utility.rs.constant.Directions.DirectionUtilities;
import net.zaros.server.utility.tool.Misc;

/**
 * Represents the player rendering outgoing packet.
 *
 * @author Emperor
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/21/17
 */
public class PlayerRendering implements OutgoingPacketBuilder {

	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(112, PacketType.VAR_SHORT);
		writePlayerRendering(player, bldr);
		return bldr.toPacket();
	}

	/**
	 * Writes the player rendering on the packet.
	 *
	 * @param player
	 *               The player.
	 * @param packet
	 *               The packet.
	 */
	private static void writePlayerRendering(Player player, PacketBuilder packet) {
		RenderInformation info = player.getRenderInformation();
		PacketBuilder flagBased = new PacketBuilder();
		int skipCount = -1;
		packet.startBitAccess();
		for (int i = 0; i < info.localsCount; i++) {
			int index = info.getLocals()[i];
			LocalUpdateStage stage = LocalUpdateStage.getStage(player, World.getPlayers().get(index));
			if (stage == null) {
				skipCount++;
			} else {
				putSkip(skipCount, packet);
				skipCount = -1;
				updateLocalPlayer(player, World.getPlayers().get(index), packet, stage, flagBased, index);
			}
		}
		putSkip(skipCount, packet);
		skipCount = -1;
		packet.finishBitAccess();
		packet.startBitAccess();
		for (int i = 0; i < info.globalsCount; i++) {
			int index = info.getGlobals()[i];
			GlobalUpdateStage stage = GlobalUpdateStage.getStage(player, World.getPlayers().get(index));
			if (stage == null) {
				skipCount++;
			} else {
				putSkip(skipCount, packet);
				skipCount = -1;
				updateGlobalPlayer(player, World.getPlayers().get(index), packet, stage, flagBased);
			}
		}
		putSkip(skipCount, packet);
		packet.finishBitAccess();
		packet.writeBytes(flagBased.getBuffer());
	}

	/**
	 * Updates a local player
	 *
	 * @param player
	 *                  The player we're writing for
	 * @param p
	 *                  The player to update
	 * @param buffer
	 *                  The buffer
	 * @param stage
	 *                  The stage
	 * @param flagBased
	 *                  The flag based buffer
	 * @param index
	 *                  The index
	 */
	private static void updateLocalPlayer(Player player, Player p, PacketBuilder buffer, LocalUpdateStage stage, PacketBuilder flagBased, int index) {
		//System.out.println("updating local: " + player.getDetails().getUsername() + ", " + p.getDetails().getUsername() + ", " + stage);
		if (stage != LocalUpdateStage.WALKING && stage != LocalUpdateStage.RUNNING) {
			buffer.writeBits(1, 1);
			buffer.writeBits(1, stage.ordinal() == 0 ? 0 : p.getUpdateMasks().isUpdateRequired() ? 1 : 0);
			buffer.writeBits(2, stage.ordinal() % 4);
		}
		switch (stage) {
		case REMOVE_PLAYER:
			if (p != null) {
				int updateType = getGlobalUpdateType(p);
				if (updateType != 0) {
					updateGlobalPlayer(player, p, buffer, GlobalUpdateStage.values()[updateType], flagBased);
				} else {
					buffer.writeBits(1, 0);
				}
			} else {
				buffer.writeBits(1, 0);
			}
			player.getRenderInformation().getIsLocal()[index] = false;
			break;
		case WALKING:
		case RUNNING:
			int dx = RegionManager.DIRECTION_DELTA_X[p.getMovement().getNextWalkDirection()];
			int dy = RegionManager.DIRECTION_DELTA_Y[p.getMovement().getNextWalkDirection()];
			boolean running;
			int opcode;
			boolean needUpdate = p.getUpdateMasks().isUpdateRequired();
			if (p.getMovement().getNextRunDirection() != -1) {
				dx += RegionManager.DIRECTION_DELTA_X[p.getMovement().getNextRunDirection()];
				dy += RegionManager.DIRECTION_DELTA_Y[p.getMovement().getNextRunDirection()];
				opcode = Misc.getPlayerRunningDirection(dx, dy);
				if (opcode == -1) {
					running = false;
					opcode = Misc.getPlayerWalkingDirection(dx, dy);
				} else {
					running = true;
				}
			} else {
				running = false;
				opcode = Misc.getPlayerWalkingDirection(dx, dy);
			}
			p.putTemporaryAttribute("last_direction", opcode);
			buffer.writeBits(1, 1);
			buffer.writeBits(1, needUpdate ? 1 : 0);
			buffer.writeBits(2, running ? 2 : 1);
			buffer.writeBits(running ? 4 : 3, opcode);
			break;
		case TELEPORTED:
			Location delta = Location.getDelta(p.getRenderInformation().getLastLocation(), p.getLocation());
			int deltaX = delta.getX() < 0 ? -delta.getX() : delta.getX();
			int deltaY = delta.getY() < 0 ? -delta.getY() : delta.getY();
			if (deltaX <= 15 && deltaY <= 15) {
				buffer.writeBits(1, 0);
				int deltaZ;
				deltaX = delta.getX() < 0 ? delta.getX() + 32 : delta.getX();
				deltaY = delta.getY() < 0 ? delta.getY() + 32 : delta.getY();
				deltaZ = delta.getPlane();
				buffer.writeBits(12, deltaY | deltaX << 5 | deltaZ << 10);
			} else {
				buffer.writeBits(1, 1);
				buffer.writeBits(30, delta.getY() & 0x3fff | (delta.getX() & 0x3fff) << 14 | (delta.getPlane() & 0x3) << 28);
			}
			break;
		default:
			break;
		}
		if (p != null && stage != LocalUpdateStage.REMOVE_PLAYER && p.getUpdateMasks().isUpdateRequired()) {
			writeMasks(player, p, flagBased, false);
		}
	}

	private static byte getGlobalUpdateType(Player player) {
		Location delta = Location.getDelta(World.getHash((short) player.getIndex()), player.getLocation().getRegionLocation());
		if (delta.getX() == 0 && delta.getY() == 0 && delta.getPlane() == 0) {
			return 0; // no update needed
		} else if (delta.getX() == 0 && delta.getY() == 0 && delta.getPlane() != 0) {
			return 1; // z update requested
		} else if (delta.getX() >= -1 && delta.getX() <= 1 && delta.getY() >= -1 && delta.getY() <= 1 && DirectionUtilities.RegionMovementType[delta.getX() + 1][delta.getY() + 1] != -1) {
			return 2; // map direction update requed
		} else {
			return 3; // full update requed
		}
	}

	/**
	 * Updates the global player
	 *
	 * @param player
	 *                  The player
	 * @param updatable
	 *                  The player to update
	 * @param buffer
	 *                  The buffer
	 * @param stage
	 *                  The global update stage
	 * @param flagBased
	 *                  The flag based buffer
	 */
	private static void updateGlobalPlayer(Player player, Player updatable, PacketBuilder buffer, GlobalUpdateStage stage, PacketBuilder flagBased) {
		buffer.writeBits(1, 1);
		buffer.writeBits(2, stage.ordinal());
		if (stage == GlobalUpdateStage.ADD_PLAYER) {
			byte updateType = getGlobalUpdateType(updatable);
			if (updateType != 0) {
				//buffer.writeBits(1, 1);
				updateGlobalPlayer(player, updatable, buffer, GlobalUpdateStage.values()[updateType], flagBased);
			} else {
				buffer.writeBits(1, 0);
			}
			buffer.writeBits(6, updatable.getLocation().getXInRegion());
			buffer.writeBits(6, updatable.getLocation().getYInRegion());
			buffer.writeBits(1, 1);
			player.getRenderInformation().getIsLocal()[updatable.getIndex()] = true;
			writeMasks(player, updatable, flagBased, true);
		} else if (stage == GlobalUpdateStage.HEIGHT_UPDATED) {
			Location hashLoc = World.getHash((short) updatable.getIndex());
			int z = updatable.getLocation().getPlane() - hashLoc.getPlane();
			buffer.writeBits(2, z & 0x3);
		} else if (stage == GlobalUpdateStage.MAP_REGION_DIRECTION) {
		    int oldHash = updatable.getRenderInformation().getLastLocation().get18BitsHash();
		    int newHash = updatable.getLocation().get18BitsHash();
			Location hashLoc = World.getHash((short) updatable.getIndex());
			Location delta = Location.getDelta(hashLoc, updatable.getLocation().getRegionLocation());
			int z = updatable.getLocation().getPlane() - hashLoc.getPlane();
			int oldPlane = oldHash >> 16;
			int newPlane = newHash >> 16;
			int planeOffset = newPlane - oldPlane;
            int oldX = (oldHash & 0xff72) >> 8;
            int oldY = 0xff & oldHash;
            int newX = (newHash & 0xff72) >> 8;
            int newY = 0xff & newHash;
            int opcode = 0;
            if (Math.abs(newX - oldX) <= 1 && Math.abs(newY - oldY) <= 1) {
                int dx = newX - oldX;
                int dy = newY - oldY;
                if (dx == -1 && dy == -1) {
					opcode = 0;
				} else if (dx == 1 && dy == -1) {
					opcode = 2;
				} else if (dx == -1 && dy == 1) {
					opcode = 5;
				} else if (dx == 1 && dy == 1) {
					opcode = 7;
				} else if (dy == -1) {
					opcode = 1;
				} else if (dx == -1) {
					opcode = 3;
				} else if (dx == 1) {
					opcode = 4;
				} else {
					opcode = 6;
				}
            }
			//(planeOffset << 3) + (opcode & 0x7));
			buffer.writeBits(5, ((planeOffset & 0x3) << 3) + (opcode & 0x7));//(z & 0x3) << 3 | DirectionUtilities.RegionMovementType[delta.getX() + 1][delta.getY() + 1] & 0x7);
		} else if (stage == GlobalUpdateStage.TELEPORTED) {
			Location hashLoc = World.getHash((short) updatable.getIndex());
			Location delta = Location.getDelta(hashLoc, updatable.getLocation().getRegionLocation());
			int hash = delta.getY() & 0xFF | (delta.getX() & 0xFF) << 8 | (delta.getPlane() & 0x3) << 16;
			buffer.writeBits(18, hash);
		}
	}

	private static void updateMapRegionDirection(PacketBuilder stream, int oldHash, int newHash) {
		System.out.println(oldHash + ", " + newHash);
		int oldX = (oldHash & 0xff72) >> 8;
		int oldY = 0xff & oldHash;
		int oldPlane = oldHash >> 16;
		int newX = (newHash & 0xff72) >> 8;
		int newY = 0xff & newHash;
		int newPlane = newHash >> 16;
		int planeOffset = newPlane - oldPlane;
		if (oldX == newX && oldY == newY) {
			//stream.writeBits(2, 1);
			stream.writeBits(2, planeOffset);
			System.out.println("plane update");
		} else if (Math.abs(newX - oldX) <= 1 && Math.abs(newY - oldY) <= 1) {
			int opcode;
			int dx = newX - oldX;
			int dy = newY - oldY;
			if (dx == -1 && dy == -1) {
				opcode = 0;
			} else if (dx == 1 && dy == -1) {
				opcode = 2;
			} else if (dx == -1 && dy == 1) {
				opcode = 5;
			} else if (dx == 1 && dy == 1) {
				opcode = 7;
			} else if (dy == -1) {
				opcode = 1;
			} else if (dx == -1) {
				opcode = 3;
			} else if (dx == 1) {
				opcode = 4;
			} else {
				opcode = 6;
			}
			//stream.writeBits(2, 2);
			System.out.println("teleport");
			stream.writeBits(5, (planeOffset << 3) + (opcode & 0x7));
		} else {
			int xOffset = newX - oldX;
			int yOffset = newY - oldY;
			//stream.writeBits(2, 3);
			System.out.println("hash");
			stream.writeBits(18, (yOffset & 0xff) + ((xOffset & 0xff) << 8)
					+ (planeOffset << 16));
		}
	}

	/**
	 * Writes the update masks for a player on this packet.
	 *
	 * @param writingFor
	 *                   The player we're writing for.
	 * @param updatable
	 *                   The player to update.
	 * @param composer
	 *                   The packet to write on.
	 * @param forceSync
	 *                   If we should force the appearance update mask.
	 */
	private static void writeMasks(Player writingFor, Player updatable, PacketBuilder composer, boolean forceSync) {
		int maskData = 0;
		PriorityQueue<UpdateFlag> flags = new PriorityQueue<>(updatable.getUpdateMasks().getFlagQueue());
		for (UpdateFlag flag : flags) {
			maskData |= flag.getMaskData();
		}
		if (forceSync && (maskData & 0x2) == 0) {
			maskData |= 0x2;
			flags.add(new AppearanceUpdate(updatable));
		}
		if (maskData > 128) {
			maskData |= 0x1;
		}
		if (maskData > 32768) {
			maskData |= 0x200;
		}
		composer.writeByte((byte) maskData);
		if (maskData > 128) {
			composer.writeByte((byte) (maskData >> 8));
		}
		if (maskData > 32768) {
			composer.writeByte((byte) (maskData >> 16));
		}
		while (!flags.isEmpty()) {
			flags.poll().write(writingFor, composer);
		}
	}

	/**
	 * Puts the skipcount on the packet.
	 *
	 * @param skipCount
	 *                  The skip count.
	 * @param packet
	 *                  The packet to write on.
	 */
	private static void putSkip(int skipCount, PacketBuilder packet) {
		if (skipCount > -1) {
			packet.writeBits(1, 0);
			if (skipCount == 0) {
				packet.writeBits(2, 0);
			} else if (skipCount < 32) {
				packet.writeBits(2, 1);
				packet.writeBits(5, skipCount);
			} else if (skipCount < 256) {
				packet.writeBits(2, 2);
				packet.writeBits(8, skipCount);
			} else if (skipCount < 2048) {
				packet.writeBits(2, 3);
				packet.writeBits(11, skipCount);
			}
		}
	}

}