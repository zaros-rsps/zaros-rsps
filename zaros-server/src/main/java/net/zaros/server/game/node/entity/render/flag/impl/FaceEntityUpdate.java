package net.zaros.server.game.node.entity.render.flag.impl;

import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.UpdateFlag;
import net.zaros.server.network.world.packet.PacketBuilder;

/**
 * Represents a face entity update flag.
 *
 * @author Emperor
 */
public class FaceEntityUpdate extends UpdateFlag {

	/**
	 * If the entity using this update flag is an NPC.
	 */
	private final boolean npc;

	/**
	 * The client index of the entity to face.
	 */
	private final int index;

	/**
	 * Constructs a new {@code FaceEntityUpdate} {@code Object}.
	 *
	 * @param entity
	 *               The entity to be faced
	 * @param npc
	 *               If the entity using this update flag is an NPC.
	 */
	public FaceEntityUpdate(Entity entity, boolean npc) {
		this.npc = npc;
		int index = -1;
		if (entity != null) {
			index = entity.getIndex();
			if (entity instanceof Player) {
				index += +32768;
			}
		}
		this.index = index;
	}

	@Override
	public void write(Player outgoing, PacketBuilder bldr) {
		if (npc) {
			bldr.writeLEShortA(index);
		} else {
			bldr.writeShort(index);
		}
	}

	@Override
	public int getOrdinal() {
		return npc ? 1 : 18;
	}

	@Override
	public int getMaskData() {
		return npc ? 0x8 : 0x4;
	}

}
