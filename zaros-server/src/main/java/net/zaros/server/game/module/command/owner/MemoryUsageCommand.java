package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/9/2017
 */
@CommandManifest(description = "Shows how much memory is used.")
public class MemoryUsageCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("memused");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		String info = Misc.getMemoryUsageInformation();
		System.out.println(info);
		player.getTransmitter().sendMessage(info, false);
	}
}
