package net.zaros.server.game.content.combat.data.magic;

import net.zaros.server.game.content.combat.data.magic.ancientspells.*;
import net.zaros.server.game.content.combat.data.magic.modernspells.*;
import net.zaros.server.game.content.skills.magic.Spell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigPacketBuilder;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.constant.MagicConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

import java.util.HashMap;
import java.util.Map;

/**
 * Handles spell selection for entities.
 *
 * @author Gabriel || Wolfsdarker
 */
public class SpellSelection {

    /**
     * The map of spells with their IDs as key.
     */
    private static Map<Integer, Spell> modernSpellMap = new HashMap<>();

    /**
     * The map of spells with their IDs as key.
     */
    private static Map<Integer, Spell> ancientSpellMap = new HashMap<>();

    /**
     * Loads all the spells into the map.
     */
    public static void loadSpells() {
        modernSpellMap.put(WindStrike.spellID, new WindStrike());
        modernSpellMap.put(WaterStrike.spellID, new WaterStrike());
        modernSpellMap.put(Confuse.spellID, new Confuse());
        modernSpellMap.put(EarthStrike.spellID, new EarthStrike());
        modernSpellMap.put(Weaken.spellID, new Weaken());
        modernSpellMap.put(FireStrike.spellID, new FireStrike());
        modernSpellMap.put(WindBolt.spellID, new WindBolt());
        modernSpellMap.put(Curse.spellID, new Curse());
        modernSpellMap.put(Bind.spellID, new Bind());
        modernSpellMap.put(WaterBolt.spellID, new WaterBolt());
        modernSpellMap.put(EarthBolt.spellID, new EarthBolt());
        modernSpellMap.put(FireBolt.spellID, new FireBolt());
        modernSpellMap.put(CrumbleUndead.spellID, new CrumbleUndead());
        modernSpellMap.put(WindBlast.spellID, new WindBlast());
        modernSpellMap.put(WaterBlast.spellID, new WaterBlast());
        modernSpellMap.put(IbanBlast.spellID, new IbanBlast());
        modernSpellMap.put(Snare.spellID, new Snare());
        modernSpellMap.put(MagicDart.spellID, new MagicDart());
        modernSpellMap.put(EarthBlast.spellID, new EarthBlast());
        modernSpellMap.put(FireBlast.spellID, new FireBlast());
        modernSpellMap.put(SaradominStrike.spellID, new SaradominStrike());
        modernSpellMap.put(ClawsOfGuthix.spellID, new ClawsOfGuthix());
        modernSpellMap.put(FlamesOfZamorak.spellID, new FlamesOfZamorak());
        modernSpellMap.put(WindWave.spellID, new WindWave());
        modernSpellMap.put(WaterWave.spellID, new WaterWave());
        modernSpellMap.put(Vulnerability.spellID, new Vulnerability());
        modernSpellMap.put(EarthWave.spellID, new EarthWave());
        modernSpellMap.put(Enfeeble.spellID, new Enfeeble());
        modernSpellMap.put(FireWave.spellID, new FireWave());
        modernSpellMap.put(StormOfArmadyl.spellID, new StormOfArmadyl());
        modernSpellMap.put(Entangle.spellID, new Entangle());
        modernSpellMap.put(Stun.spellID, new Stun());
        modernSpellMap.put(WindSurge.spellID, new WindSurge());
        modernSpellMap.put(TeleportBlock.spellID, new TeleportBlock());
        modernSpellMap.put(WaterSurge.spellID, new WaterSurge());
        modernSpellMap.put(EarthSurge.spellID, new EarthSurge());
        modernSpellMap.put(FireSurge.spellID, new FireSurge());

        ancientSpellMap.put(SmokeRush.spellID, new SmokeRush());
        ancientSpellMap.put(SmokeBurst.spellID, new SmokeBurst());
        ancientSpellMap.put(SmokeBlitz.spellID, new SmokeBlitz());
        ancientSpellMap.put(SmokeBarrage.spellID, new SmokeBarrage());

        ancientSpellMap.put(ShadowRush.spellID, new ShadowRush());
        ancientSpellMap.put(ShadowBurst.spellID, new ShadowBurst());
        ancientSpellMap.put(ShadowBlitz.spellID, new ShadowBlitz());
        ancientSpellMap.put(ShadowBarrage.spellID, new ShadowBarrage());

        ancientSpellMap.put(BloodRush.spellID, new BloodRush());
        ancientSpellMap.put(BloodBurst.spellID, new BloodBurst());
        ancientSpellMap.put(BloodBlitz.spellID, new BloodBlitz());
        ancientSpellMap.put(BloodBarrage.spellID, new BloodBarrage());

        ancientSpellMap.put(IceRush.spellID, new IceRush());
        ancientSpellMap.put(IceBurst.spellID, new IceBurst());
        ancientSpellMap.put(IceBlitz.spellID, new IceBlitz());
        ancientSpellMap.put(IceBarrage.spellID, new IceBarrage());

        ancientSpellMap.put(MiasmicRush.spellID, new MiasmicRush());
        ancientSpellMap.put(MiasmicBurst.spellID, new MiasmicBurst());
        ancientSpellMap.put(MiasmicBlitz.spellID, new MiasmicBlitz());
        ancientSpellMap.put(MiasmicBarrage.spellID, new MiasmicBarrage());
    }

    /**
     * Returns the spell's information based on the entity's spellbook and the spell ID.
     *
     * @param entity
     * @param spellID
     * @return the spell
     */
    public static Spell getSpell(Entity entity, int spellID) {
        return entity.getCombatDefinitions().getSpellbook() == MagicConstants.MagicBook.ANCIENTS ? ancientSpellMap.get(spellID) : modernSpellMap.get(spellID);
    }

    /**
     * Returns the spell's information, if the map contains it.
     *
     * @param spellID
     * @return the spell
     */
    public static Spell getModernSpell(int spellID) {
        return modernSpellMap.get(spellID);
    }

    /**
     * Returns the ancient spell's information, if the map contains it.
     *
     * @param spellID
     * @return the spell
     */
    public static Spell getAncientSpell(int spellID) {
        return ancientSpellMap.get(spellID);
    }

    /**
     * Sets the entity's autocast spell.
     *
     * @param entity
     * @param componentID
     */
    public static void setAutocastSpell(Entity entity, int componentID) {

        Spell spell;

        if (entity.getCombatDefinitions().getSpellbook() == MagicConstants.MagicBook.ANCIENTS) {
            spell = ancientSpellMap.get(componentID);
        } else {
            spell = modernSpellMap.get(componentID);
        }

        if (spell == null) {
            return;
        }

        if (spell.magicLevelRequirement() > entity.getSkills().getLevel(SkillConstants.MAGIC)) {
            if (entity.isPlayer()) {
                entity.toPlayer().getTransmitter().sendMessage("You need a magic level of " + spell.magicLevelRequirement() + " to cast this.");
            }
            return;
        }

        if (entity.isPlayer()) {
            if (entity.toPlayer().getCombatDefinitions().getSpellbook() != spell.magicBook()) {
                return;
            }
        }

        int configId = 0;

        Spell current = entity.getTemporaryAttribute(AttributeKey.AUTOCAST_SPELL);

        if (current == spell) {
            entity.removeTemporaryAttribute(AttributeKey.AUTOCAST_SPELL);
        } else {
            entity.putTemporaryAttribute(AttributeKey.AUTOCAST_SPELL, spell);
            configId = spell.autocastConfigID();
        }

        if (entity.isPlayer()) {
            entity.toPlayer().getTransmitter().send(new ConfigPacketBuilder(108, configId).build(entity.toPlayer()));
        }
    }

}
