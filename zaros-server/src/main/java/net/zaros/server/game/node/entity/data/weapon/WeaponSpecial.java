package net.zaros.server.game.node.entity.data.weapon;

import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.rs.Hit;

/**
 * A interface for weapon's specials.
 *
 * @author Gabriel || Wolfsdarker
 */
public interface WeaponSpecial {

    /**
     * The animation performed by the attacker.
     *
     * @return the animation id
     */
    int attackAnimation();

    /**
     * The graphics performed by the attacker.
     *
     * @return the graphic id
     */
    int attackGraphics();

    /**
     * The graphics that the entity receiving the special will play.
     *
     * @return the graphic id
     */
    int targetGraphics();

    /**
     * The attack cooldown after the special.
     *
     * @return the time in ticks
     */
    int attackSpeed();

    /**
     * The amount of energy required to cast the special.
     *
     * @return the amount
     */
    int specialRequired();

    /**
     * The special damage's hit style.
     *
     * @return the style
     */
    Hit.HitSplat hitStyle();

    /**
     * The combat script that will execute the special.
     *
     * @return
     */
    CombatScript combatScript();

    /**
     * If the entity can cast the special on the target.
     *
     * @param attacker
     * @param target
     * @return if its cast-able
     */
    default boolean canSpecial(Entity attacker, Entity target) {
        return true;
    }

    /**
     * Sends the projectile from attacker to target.
     *
     * @param attacker
     * @param target
     */
    default void sendProjectile(Entity attacker, Entity target) {

    }

    /**
     * Drain's the entity's special energy.
     *
     * @param entity
     * @param amount
     * @param resetSpecialAttack
     */
    static void drainEnergy(Entity entity, int amount, boolean resetSpecialAttack) {
        if (entity.getCombatDefinitions().getSpecialEnergy() < amount) {
            amount = entity.getCombatDefinitions().getSpecialEnergy();
        }
        entity.getCombatDefinitions().setSpecialEnergy(entity.getCombatDefinitions().getSpecialEnergy() - amount);

        if (resetSpecialAttack) {
            entity.getCombatDefinitions().setSpecialActivated(false);
        }
    }

}
