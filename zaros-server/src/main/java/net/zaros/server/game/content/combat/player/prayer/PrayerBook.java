package net.zaros.server.game.content.combat.player.prayer;

/**
 * Created by Jacob Rhiel on 5/20/2017.
 */
public enum PrayerBook {

    NORMAL, CURSES

}
