package net.zaros.server.game.content.combat.player.prayer;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Jacob on 5/20/2017.
 */
@AllArgsConstructor
@Getter
public enum PrayerIcon {

    NONE(-1),

    PROTECT_FROM_MELEE(0),

    PROTECT_FROM_RANGE(1),

    PROTECT_FROM_MAGIC(2),

    PROTECT_FROM_SUMMONING(7),

    PROTECT_FROM_SUMMONING_AND_MELEE(8),

    PROTECT_FROM_SUMMONING_AND_RANGE(9),

    PROTECT_FROM_SUMMONING_AND_MAGIC(10),

    SMITE(4),

    REDEMPTION(5),

    RETRIBUTION(3),

    DEFLECT_SUMMONING(15),

    DEFLECT_SUMMONING_AND_MELEE(16),

    DEFLECT_SUMMONING_AND_RANGE(17),

    DEFLECT_SUMMONING_AND_MAGIC(18),

    DEFLECT_MAGIC(14),

    DEFLECT_MISSILES(13),

    DEFLECT_MELEE(12),

    LEECH_SPECIAL_ATTACK(18),//TODO: Does this exist?

    WRATH(19),

    SOUL_SPLIT(20)

    ;

    private final int icon;

}
