package net.zaros.server.game.content.combat.data.magic.modernspells;

import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitModifier;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;

import java.util.List;

/**
 * The saradomin strike spell on the regular spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class SaradominStrike extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 66;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.FIRE_RUNE1, 2), new Item(Items.BLOOD_RUNE1, 2), new Item(Items.AIR_RUNE1, 4));

    /**
     * The spell's required equipments.
     */
    private static final List<Item> requiredEquipments = List.of(new Item(Items.SARADOMIN_STAFF));

    @Override
    public void playGraphics(Entity source, Entity target) {
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(811);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
    }

    @Override
    public HitModifier getModifier(Entity source, Entity target) {
        return null;
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return null;
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(76);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int autocastConfigID() {
        return 41;
    }

    @Override
    public int magicLevelRequirement() {
        return 60;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 200;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 35;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.REGULAR;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }

    @Override
    public List<Item> requiredEquipments() {
        return requiredEquipments;
    }
}
