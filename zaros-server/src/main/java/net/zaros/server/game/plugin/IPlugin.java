package net.zaros.server.game.plugin;

import java.io.File;

/**
 * Represents any plug-in in our system. This interface will have all the
 * methods related to a plug-in, including enabling, and disabling of the
 * plug-in.
 *
 * @author Walied K. Yassen
 * @since 0.1
 */
public interface IPlugin {

	/**
	 * Checks whether the plug-in is enabled or not.
	 *
	 * @return <code>true</code> if it is otherwise <code>false</code>.
	 */
	boolean isEnabled();

	/**
	 * Gets the plug-in manifest.
	 *
	 * @return the plug-in manifest.
	 */
	IPluginManifest getManifest();

	/**
	 * Gets the plug-in {@link IRepositoryDirectory directory}.
	 * <p>
	 * The plug-in directory is dedicated for this plug-in and only this plug-in.
	 *
	 * @return the plug-in {@link IRepositoryDirectory} object.
	 */
	File getDirectory();
}
