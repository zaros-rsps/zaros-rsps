package net.zaros.server.game.node.item;

public enum DropFrequency {

    /**
     * This gets dropped all the time.
     */
    ALWAYS,

    /**
     * This gets commonly dropped.
     */
    COMMON,

    /**
     * This drop is uncommon.
     */
    UNCOMMON,

    /**
     * This gets rarely dropped.
     */
    RARE,

    /**
     * This gets very rarely dropped.
     */
    VERY_RARE;

}