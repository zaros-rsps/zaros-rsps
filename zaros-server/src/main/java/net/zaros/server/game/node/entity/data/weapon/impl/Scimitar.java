package net.zaros.server.game.node.entity.data.weapon.impl;

import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;

import java.util.List;

/**
 * The interface for scimitars.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum Scimitar implements WeaponInterface {

    /**
     * The regular scimitar weapon interface.
     */
    REGULAR(),

    /**
     * The dragon scimitar weapon interface.
     */
    DRAGON() {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    };

    /**
     * The attack types.
     */
    private static final List<AttackType> attackTypes =
            List.of(AttackType.SCIMITAR_CHOP, AttackType.SCIMITAR_SLASH, AttackType.SCIMITAR_LUNGE, AttackType.SCIMITAR_BLOCK);

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }
}
