package net.zaros.server.game;

/**
 * The flags that can be altered for the game are stored here.
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/18/2017
 */
public class GameFlags {

	/**
	 * The id of the world that will be ran
	 */
	public static int worldId;

	/**
	 * If the game is running on developer mode (debug)
	 */
	public static boolean debugMode;

	/**
	 * If sql is enabled
	 */
	public static boolean webIntegrated;

	public static boolean isLobbyWorld() {
		return worldId > 1099;
	}

}
