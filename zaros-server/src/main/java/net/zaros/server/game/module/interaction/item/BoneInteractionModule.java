package net.zaros.server.game.module.interaction.item;

import net.zaros.server.game.content.skills.prayer.Bone;
import net.zaros.server.game.content.skills.prayer.BoneBuryingAction;
import net.zaros.server.game.module.type.ItemInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/1/2017
 */
public class BoneInteractionModule extends ItemInteractionModule {

	public BoneInteractionModule() {
		register(Bone.getItemIds());
	}
	
	@Override
	public boolean handle(Player player, Item item, int slotId, InteractionOption option) {
		if (option != InteractionOption.FIRST_OPTION) {
			return false;
		} else {
			Bone bone = Bone.getBone(item.getId());
			if (bone == null) {
				return false;
			}
			player.getManager().getActions().startAction(new BoneBuryingAction(bone, slotId));
			return true;
		}
	}
	
}