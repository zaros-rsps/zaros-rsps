package net.zaros.server.game.content.combat.data.magic.ancientspells;

import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.effects.impl.MiasmicEffect;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitInstant;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;

import java.util.List;

/**
 * The miasmic barrage spell on the ancient spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class MiasmicBarrage extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 39;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.EARTH_RUNE1, 4), new Item(Items.BLOOD_RUNE1, 4), new Item(Items.SOUL_RUNE1, 4));

    /**
     * The spell's required equipments.
     */
    private static final List<Item> requiredEquipments = List.of(new Item(Items.ZURIELS_STAFF1));

    @Override
    public void playGraphics(Entity source, Entity target) {
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(10518);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return new HitEffect(HitInstant.CALCULATION, () -> target.getEffectManager().apply(new MiasmicEffect(target, source)));
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(1854);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int autocastConfigID() {
        return 101;
    }

    @Override
    public int magicLevelRequirement() {
        return 97;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 320;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 54;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.ANCIENTS;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }

    @Override
    public List<Item> requiredEquipments() {
        return requiredEquipments;
    }

    @Override
    public boolean multi() {
        return true;
    }
}
