package net.zaros.server.game.node.entity.data.weapon.impl;

import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;

import java.util.List;

public class Unarmed implements WeaponInterface {

    /**
     * The attack types.
     */
    private static final List<AttackType> attackTypes =
            List.of(AttackType.UNARMED_PUNCH, AttackType.UNARMED_KICK, AttackType.UNARMED_PUNCH, AttackType.UNARMED_BLOCK);

    @Override
    public int poisonDamage() {
        return 0;
    }

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }
}
