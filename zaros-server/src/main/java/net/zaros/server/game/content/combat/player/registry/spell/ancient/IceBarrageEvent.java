package net.zaros.server.game.content.combat.player.registry.spell.ancient;

import net.zaros.server.game.content.combat.player.registry.wrapper.context.CombatSpellContext;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.CombatSpellEvent;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.effects.impl.FreezeEffect;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.constant.MagicConstants.MagicBook;

import java.util.concurrent.TimeUnit;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/28/2017
 */
public class IceBarrageEvent implements CombatSpellEvent {
	
	@Override
	public int spellId() {
		return 23;
	}
	
	@Override
	public int delay(Player player) {
		return 4;
	}
	
	@Override
	public int animationId() {
		return 1979;
	}
	
	// we don't store a static gfx because it is modifiable
	@Override
	public int hitGfx() {
		return -1;
	}
	
	@Override
	public int maxHit(Player player, Entity target) {
		return 300;
	}
	
	@Override
	public double exp() {
		return 52;
	}
	
	@Override
	public MagicBook book() {
		return MagicBook.ANCIENTS;
	}
	
	@Override
	public void cast(Player player, CombatSpellContext context) {
		// storing vars before spell is cast
		final Entity target = context.getTarget();
		final boolean frozenTarget = target.getEffectManager().isEffectPresent(FreezeEffect.getName());
		context.getSwing().sendMultiSpell(player, context.getTarget(), this, () -> {
			if (frozenTarget) {
				return;
			}
			// only freeze the player if they are unfreezeable when the spell is cast.
			context.getTarget().getEffectManager().apply(new FreezeEffect(context.getTarget(), player, 20));
		}, () -> {
			int gfx;
			int height;
			if (target.getSize() >= 2 || frozenTarget) {
				gfx = 1677;
				height = 100;
			} else {
				gfx = 369;
				height = 0;
			}
			target.sendGraphics(gfx, height, 0);
		});
	}
}
