package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.content.event.EventRepository;
import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/27/2017
 */
@CommandManifest(description = "Reloads all the events in the repository")
public class ReloadEventRepositoryCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("reloadevents");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		EventRepository.registerEvents(false);
	}
}
