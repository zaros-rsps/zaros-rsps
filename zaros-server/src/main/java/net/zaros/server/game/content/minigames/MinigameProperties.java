/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames;

import lombok.Getter;

/**
 * Contains the basic minigame properties that every minigame type has, such as
 * name etc..
 * 
 * @author Walied K. Yassen
 */
public class MinigameProperties {

	/**
	 * The minigame name.
	 */
	@Getter
	private final String name;

	/**
	 * Construct a new {@link MinigameProperties} type object instance.
	 * 
	 * @param name
	 *             the minigame name.
	 */
	public MinigameProperties(String name) {
		this.name = name;
	}
}
