package net.zaros.server.game.content.zskillsystem.segment;

import lombok.AllArgsConstructor;
import lombok.Getter;

import net.zaros.server.core.newtask.scheduler.tick.TickTask;
import net.zaros.server.game.content.zskillsystem.properties.SkillProperties;

/**
 * @author Jacob Rhiel
 */
@AllArgsConstructor
@Getter
public abstract class AbstractSkillSegment extends TickTask implements SkillSegment {

    private final SkillProperties properties;

}
