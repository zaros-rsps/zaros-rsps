package net.zaros.server.game.content.system.zskillsystem.farming.actions;

import net.zaros.server.game.content.system.zskillsystem.farming.impl.FarmingConstants;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.FarmingPatchType;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.PatchState;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.Patches;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.constant.SkillConstants;

/**
 * Handles the action to harvest the crops.
 *
 * @author Gabriel || Wolfsdarker
 */
public class CropHarvestAction extends PlayerTask {

	/**
	 * Constructor for the action.
	 *
	 * @param player
	 * @param data
	 * @param state
	 * @param position
	 */
	private CropHarvestAction(Player player, Patches data, PatchState state, Location position) {
		super(4, player, () -> {

			if (data.getPatchType() == FarmingPatchType.ALLOTMENT) {
				if (!player.getInventory().containsItem(FarmingConstants.SPADE)) {
					player.endCurrentTask();
					return;
				}
			}

			if (player.getInventory().isFull()) {
				player.endCurrentTask();
				player.getTransmitter().sendMessage("You don't have enough space in your inventory to continue this.");
				return;
			}

			if (state.getLivesAmount() <= 0) {
				state.resetPatch();
				player.endCurrentTask();
				player.getFarming().updatePatches(player);
				return;
			}

			player.sendAnimation(data.getPatchType().getYieldAnimation());
			player.faceLocation(position);
			player.getInventory().addItem(new Item(state.getSeed().getProduct()));
			player.getSkills().addExperienceWithMultiplier(SkillConstants.FARMING, state.getSeed().getExperience() / 10);

			if (FarmingConstants.hasToLoseLife(state, player)) {
				state.setLivesAmount(state.getLivesAmount() - 1);
			}

			player.getFarming().updatePatches(player);
		});

		onEnd(() -> {
			player.sendAnimation(new Animation(65535));
			player.getMovementQueue().reset();
		});

		stopUpon(MovementPacketListener.class);
	}

	/**
	 * Handles the action to the crops.
	 *
	 * @param player
	 * @param data
	 * @param position
	 */
	public static void harvestPatch(Player player, Patches data, Location position) {

		PatchState state = player.getFarming().getPatchStates().get(data.name());

		if (state.getLivesAmount() <= 0) {
			state.resetPatch();
			player.getFarming().updatePatches(player);
			return;
		}

		if (data.getPatchType() == FarmingPatchType.ALLOTMENT) {
			if (!player.getInventory().containsItem(FarmingConstants.SPADE)) {
				player.getTransmitter().sendMessage("You need a spade to do that.");
				return;
			}
		}

		player.getTransmitter().sendMessage("You begin to harvest the " + data.getPatchType().toString().toLowerCase() + ".");

		TaskManager.submit(new CropHarvestAction(player, data, state, position));
	}
}
