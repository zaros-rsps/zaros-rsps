package net.zaros.server.game.module.command.administrator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.zaros.cache.type.npctype.NPCDefinition;
import net.zaros.cache.type.npctype.NPCDefinitionParser;
import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.tool.ColorConstants;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/2/2017
 */
@CommandManifest(description = "Finds an npc by the name you enter", types = { String.class })
public class FindNPCByNameCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("findnpc", "nn");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		// the identifier to search by
		final String identifier = args[1].replaceAll("_", " ");
		// the option flag
		final String optionFlag = stringParamOrDefault(args, 2, null);
		// the list of items that were found
		List<String> found = new ArrayList<>();
		//TODO: why isnt it loading full cache
		for (int npcId = 0; npcId < 14362/*CacheFileStore.getNPCDefinitionsSize()*/; npcId++) {
			// the definition instance
			NPCDefinition definition = NPCDefinitionParser.forId(npcId);
			if (definition == null) {
				continue;
			}
			// the name of the item
			final String name = definition.getName().toLowerCase();
			boolean added = false;
			// the name has the identifier we want
			if (name.contains(identifier)) {
				// we only want to find items by the identifier
				if (optionFlag == null) {
					added = true;
				} else {
					// we found that the definition has the option we want
					if (definition.hasOption(optionFlag)) {
						added = true;
					}
				}
			}
			// the item was not added because it was not found by filter
			if (!added) {
				continue;
			}
			found.add("[<col=FF0000>" + npcId + "</col>] <col=" + ColorConstants.LIGHT_BLUE + ">" + definition.getName() + "</col> options=" + Arrays.toString(definition.getOptions()));
		}
		// shows the entries found
		found.forEach(entry -> player.getTransmitter().sendConsoleMessage(entry));
		// sends a response with the amount of entries found
		sendResponse(player, "Found " + found.size() + " npcs by name '" + identifier + "'.", console);
	}
}
