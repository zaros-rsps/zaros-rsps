package net.zaros.server.game.module.interaction.object;

import net.zaros.server.core.system.SystemManager;
import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.module.type.ObjectInteractionModule;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.impl.ForceMovement;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/8/2017
 */
public class WildernessDitchInteractionModule extends ObjectInteractionModule {

	public WildernessDitchInteractionModule() {
		register("Wilderness wall");
	}

	@Override
	public boolean handle(Player player, GameObject object, InteractionOption option) {
		final boolean leaving = player.getLocation().getY() > object.getLocation().getY();
		final Location original = new Location(player.getLocation());
		final Location destination = new Location(player.getLocation().getX(), object.getLocation().getY() + (leaving ? -1 : 2), player.getLocation().getPlane());
		
		player.getManager().getLocks().lockAll();
		player.sendAnimation(6132);
		player.getUpdateMasks().register(new ForceMovement(player, new int[] { destination.getX(), destination.getY(), 33, 60, leaving ? ForceMovement.SOUTH : ForceMovement.NORTH }));
		
		SystemManager.getScheduler().schedule(new ScheduledTask(2) {
			@Override
			public void run() {
				player.teleport(destination);
				player.faceLocation(original);
				SystemManager.getScheduler().schedule(new ScheduledTask(1) {
					@Override
					public void run() {
						player.getManager().getLocks().unlockAll();
						stop();
					}
				});
			}
		});
		return true;
	}
}
