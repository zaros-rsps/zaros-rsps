package net.zaros.server.game.content.zskillsystem.tool;

/**
 * @author Jacob Rhiel
 */
public interface ITool {

    boolean primary();

    boolean auxillary();

}
