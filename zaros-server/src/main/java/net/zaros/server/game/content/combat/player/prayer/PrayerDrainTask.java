package net.zaros.server.game.content.combat.player.prayer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zaros.server.core.newtask.scheduler.tick.TickTask;
import net.zaros.server.game.content.combat.player.prayer.PrayerDefinition.*;

/**
 * @author Jacob Rhiel
 */
@AllArgsConstructor
@Getter
public class PrayerDrainTask extends TickTask {

    private final PrayerNode prayerNode;

    @Override
    protected void execute() {

    }

    private double getDrainRate(Prayer prayer) {
        double amountDrain = 0;
        for (PrayerNode type : prayer.getActivatedPrayers()) {
            double drain = type.getDrain();
            double bonus = 0.035;// * prayer.getEntity().toPlayer().getProperties().getBonuses()[12];
            drain = drain * (1 + bonus);
            drain = 0.6 / drain;
            amountDrain += drain;
        }
        /*if (!prayer.getEntity().toPlayer().getSkullManager().isWilderness() && prayer.getEntity().asPlayer().hasPerk(Perks.PRAYER_BETRAYER)) {
            amountDrain -= amountDrain * 0.50;
        }*/
        return amountDrain;
    }

}
