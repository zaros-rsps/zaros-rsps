package net.zaros.server.game.content.zskillsystem.properties.type;

/**
 * @author Jacob Rhiel
 * Defines the type of skill.
 */
public enum SkillType {

    /** Receiving a resource by collection; AKA: Fishing, Hunter, Farming. **/
    GATHERING,

    /** Receiving resources by creation; AKA: Smithing, Crafting, Construction.. **/
    ARTISAN,

    /** Supporting skill; AKA: Slayer, Agility, Dungeoneering. **/
    SUPPORT

}
