package net.zaros.server.game.clan.channel.delta.impl;

import lombok.AllArgsConstructor;
import net.zaros.cache.util.buffer.Buffer;
import net.zaros.server.game.clan.channel.ClanChannel.ChannelMember;
import net.zaros.server.game.clan.channel.delta.ClanChannelAction;

/**
 * Represents an add member clan channel action.
 * 
 * @author Walied K. Yassen
 */
@AllArgsConstructor
public final class AddMember extends ClanChannelAction {

	/**
	 * The channel member we are adding.
	 */
	private final ChannelMember member;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.clan.channel.delta.ClanChannelAction#encode(net.zaros.
	 * cache.util.buffer.Buffer)
	 */
	@Override
	public void encode(Buffer buffer) {
		buffer.writeByte(-1);
		buffer.writeString(member.getDisplayName());
		buffer.writeShort(member.getWorld());
		buffer.writeByte(member.getRank().getId());
		buffer.writeLong(0L);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.clan.channel.delta.ClanChannelAction#getId()
	 */
	@Override
	public int getId() {
		return 1;
	}
}
