package net.zaros.server.game.content.market.lending;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.network.world.packet.outgoing.impl.ContainerPacketBuilder;

public class ItemLending {

    public ItemLending(Player player) {
        this.player = player;
    }

    public void updateLendingScreen() {

        player.getTransmitter().send(new ContainerPacketBuilder(90, new Item[0]).build(player));
        player.getTransmitter().send(new ContainerPacketBuilder(90, new Item[0], true).build(player));

    }

    private final Player player;

}
