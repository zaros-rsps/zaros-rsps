package net.zaros.server.game.content.zone;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.node.Location;

public class Region {

    public Region(Location firstPoint, Location secondPoint) {
        minLocation = new Location(
                min(firstPoint.getX(), secondPoint.getX()),
                min(firstPoint.getY(), secondPoint.getY()));

        maxLocation = new Location(
                max(firstPoint.getX(), secondPoint.getX()),
                max(firstPoint.getY(), secondPoint.getY()));
    }

    public boolean isInRegion(Location loc) {
        return (minLocation.getX() <= loc.getX()
                && minLocation.getY() <= loc.getY()
                && maxLocation.getX() >= loc.getX()
                && maxLocation.getY() >= loc.getY());
    }

    /**
     *
     * @param a
     * @param b
     * @return
     *      Smallest of the two integers
     */
    private int min(int a, int b) {
        return a < b ? a : b;
    }

    /**
     *
     * @param a
     * @param b
     * @return
     *      higher of the two integers
     */
    private int max(int a, int b) {
        return a > b ? a : b;
    }

    @Getter
    @Setter
    private Location minLocation;

    @Getter
    @Setter
    private Location maxLocation;

}