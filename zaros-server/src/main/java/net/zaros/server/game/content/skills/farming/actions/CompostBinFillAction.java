package net.zaros.server.game.content.system.zskillsystem.farming.actions;


import net.zaros.server.game.content.system.zskillsystem.farming.compostbin.CompostBin;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;

/**
 * Handles the action of filling the compost bin.
 */
public class CompostBinFillAction extends PlayerTask {

	/**
	 * Constructor of compost bin fill action.
	 *
	 * @param player
	 * @param bin
	 * @param item_used
	 */
	public CompostBinFillAction(Player player, CompostBin bin, int item_used) {
		super(2, player, () -> {

			if (!player.getInventory().containsItem(item_used) || bin.getInventory().is()) {
				player.endCurrentTask();
				return;
			}

			player.getInventory().remove(item_used, 1);
			bin.getInventory().add(item_used, 1);
			player.sendAnimation(new Animation(832));
			player.getFarming().getCompostManager().updateBin(player, bin);

		});

		onEnd(() -> {
			player.sendAnimation(new Animation(65535));
			player.getMovementQueue().reset();
		});

		stopUpon(MovementPacketListener.class);
	}

}
