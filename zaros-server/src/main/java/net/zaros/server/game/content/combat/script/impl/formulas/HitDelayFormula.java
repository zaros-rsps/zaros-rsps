package net.zaros.server.game.content.combat.script.impl.formulas;

import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.EntityMovement;

/**
 * The formulas for hit delays.
 *
 * @author Gabriel || Wolfsdarker
 */
public class HitDelayFormula {

    /**
     * Returns the delay for magic spells to hit.
     *
     * @param attacker
     * @param target
     * @return the delay in ticks
     */
    public static int getMagicDelay(Entity attacker, Entity target) {
        int distance = EntityMovement.getManhattanDistance(attacker.getLocation(), target.getLocation());
        return (int) Math.round(2 + Math.floor((1.0 + distance) / 3.0));
    }


    /**
     * Returns the delay for ranged attacks to hit the target.
     *
     * @param attacker
     * @param target
     * @return the delay in ticks
     */
    public static int getRangeDelay(Entity attacker, Entity target) {
        int distance = EntityMovement.getManhattanDistance(attacker.getLocation(), target.getLocation());
        return (int) Math.round(2 + Math.floor((3.0 + distance) / 6.0));
    }

}
