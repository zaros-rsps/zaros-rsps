package net.zaros.server.game.plugin;

import java.io.File;
import java.util.Collection;

/**
 * Represents a plug-in manager. It manages the loaded plug-ins,their enabling
 * and disabling procedures, loading and unloading from the repository.
 *
 * @author Walied K. Yassen
 * @since 0.1
 */
public interface IPluginManager<T extends IPlugin> {

	/**
	 * Registers the specified {@code plugin} in the manager.
	 * 
	 * @param plugin
	 *               the plugin to register.
	 */
	void register(T plugin);

	/**
	 * Unregisters the specified {@code plugin} from the manager.
	 * 
	 * @param plugin
	 *               the plugin to unregister.
	 */
	void unregister(T plugin);

	/**
	 * Loads all plugins that are currently present at our repository.
	 */
	void loadAll(File directory);

	/**
	 * Unloads all the loaded plug-ins.
	 */
	void unloadAll();

	/**
	 * Enables all the currently disabled plug-ins. If no plug-ins were disabled,
	 * nothing will happen.
	 */
	void enableAll();

	/**
	 * Enables the specified {@link IPlugin}.
	 * <p>
	 * If the plug-in is already enabled, a {@link PluginException} will be thrown.
	 *
	 * @param plugin
	 *               the plug-in to enable.
	 * @throws PluginException
	 *                         if the plug-in is already enabled.
	 */
	void enablePlugin(T plugin);

	/**
	 * Disables all the currently enabled plug-ins. If no plug-ins were enabled,
	 * nothing will happens.
	 */
	void disableAll();

	/**
	 * Disables the specified {@link IPlugin}. When disabled, all the scheduled
	 * tasks or registered events will be removed.
	 * <p>
	 * If the plug-in is already disabled, a {@link PluginException} will be thrown.
	 *
	 * @param plugin
	 *               the plug-in to disable.
	 * @throws PluginException
	 *                         if the plug-in is already disabled.
	 */
	void disablePlugin(T plugin);

	/**
	 * Looks for the {@link IPlugin} object with the specified {@code id}.
	 *
	 * @param id
	 *           the plug-in id to look-for.
	 * @return the {@link IPlugin} if the plug-in was found otherwise {@code null}.
	 */
	T getPlugin(String id);

	/**
	 * Gets an immutable {@link Collection} filled with the loaded {@link IPlugin}s.
	 *
	 * @return an immutable {@link Collection} filled with the loaded
	 *         {@link IPlugin}s.
	 */
	Collection<T> getPlugins();
}
