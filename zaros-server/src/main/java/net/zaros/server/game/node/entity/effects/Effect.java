package net.zaros.server.game.node.entity.effects;

import net.zaros.server.game.node.entity.Entity;

/**
 * A simple effect interface for in-game effects on entities.
 *
 * @author Gabriel || Wolfsdarker
 */
public interface Effect {

    /**
     * If the effect is over and should be purged.
     *
     * @return true if is over
     */
    boolean over();

    /**
     * If the effect can be renewed and the time reset.
     *
     * @return true if renew is allowed
     */
    boolean renewable();

    /**
     * Returns if the entity is immune to this effect.
     *
     * @param entity
     * @return
     */
    boolean immune(Entity entity);

    /**
     * The effect's name.
     *
     * @return the name
     */
    String name();

    /**
     * Returns the message that will be displayed when the entity attempt to walk and the walk policy is NON_WALKABLE.
     *
     * @return the message
     */
    String nonWalkableMessage();

    /**
     * The effect's walk policy.
     *
     * @return the policy
     */
    WalkPolicy walkPolicy();

    /**
     * The effect's duration type, either PERMANENT or TEMPORARY.
     *
     * @return the duration
     */
    Duration duration();

    /**
     * Handles the pre-execution for the effect.
     *
     * @param isLogin
     */
    void preExecution(boolean isLogin);

    /**
     * The method that ticks synchronized with the entity that the effect is applied to.
     */
    void onTick();

    /**
     * Renews the effect.
     */
    void renew();

    /**
     * When the effect is over, this is to remove purge everything.
     */
    void end();
}
