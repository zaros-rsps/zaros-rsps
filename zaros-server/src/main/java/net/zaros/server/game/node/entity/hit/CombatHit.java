package net.zaros.server.game.node.entity.hit;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.content.combat.CombatManager;
import net.zaros.server.game.content.combat.script.CombatConstants;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.Ammunition;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.effects.impl.PoisonEffect;
import net.zaros.server.game.node.entity.effects.impl.StunEffect;
import net.zaros.server.game.node.entity.player.data.PlayerEquipment;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.repository.npc.Npcs;
import net.zaros.server.utility.rs.Hit;
import net.zaros.server.utility.rs.constant.BonusConstants;
import net.zaros.server.utility.rs.constant.EquipConstants;
import net.zaros.server.utility.tool.RandomFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A type of hit that is used on combat and calculates the damage.
 *
 * @author Gabriel || Wolfsdarker
 */
public class CombatHit extends Hit {

    /**
     * The logger for combat hits.
     */
    private static final Logger log = LoggerFactory.getLogger(CombatHit.class);

    /**
     * The hit's combat script.
     */
    @Getter
    private CombatScript combatScript;

    /**
     * The hit's target.
     */
    @Getter
    private Entity target;

    /**
     * If the hit was accurate.
     */
    @Getter
    private boolean accurate;

    /**
     * The hit's effect.
     */
    @Getter
    private HitEffect effect;

    /**
     * The hit's modifier.
     */
    @Getter
    private HitModifier modifier;

    /**
     * Constructor for a damage with no delay.
     *
     * @param source
     * @param target
     * @param script
     * @param splat
     */
    public CombatHit(Entity source, Entity target, CombatScript script, HitSplat splat) {
        super(source, -1, splat);
        this.combatScript = script;
        this.target = target;
        this.effect = combatScript.getEffect(source, target);
        this.modifier = combatScript.getModifier(source, target);
        this.calculateDamage();
    }

    /**
     * Constructor for a damage with delay.
     *
     * @param source
     * @param target
     * @param script
     * @param splat
     * @param delay
     */
    public CombatHit(Entity source, Entity target, CombatScript script, HitSplat splat, int delay) {
        super(source, -1, splat, delay);
        this.target = target;
        this.combatScript = script;
        this.effect = combatScript.getEffect(source, target);
        this.modifier = combatScript.getModifier(source, target);
        this.calculateDamage();
    }

    /**
     * Constructor for a damage with delay.
     *
     * @param source
     * @param target
     * @param script
     * @param splat
     * @param delay
     */
    public CombatHit(Entity source, Entity target, CombatScript script, HitSplat splat, int delay, HitEffect effect) {
        super(source, -1, splat, delay);
        this.target = target;
        this.combatScript = script;
        this.effect = effect;
        this.modifier = combatScript.getModifier(source, target);
        this.calculateDamage();
    }

    /**
     * Constructor for a damage with delay.
     *
     * @param source
     * @param target
     * @param script
     * @param splat
     * @param delay
     */
    public CombatHit(Entity source, Entity target, CombatScript script, HitSplat splat, int delay, HitModifier modifier) {
        super(source, -1, splat, delay);
        this.target = target;
        this.combatScript = script;
        this.modifier = modifier;
        this.effect = combatScript.getEffect(source, target);
        this.calculateDamage();
    }

    /**
     * Constructor for a damage with delay.
     *
     * @param source
     * @param target
     * @param script
     * @param splat
     * @param delay
     */
    public CombatHit(Entity source, Entity target, CombatScript script, HitSplat splat, int delay, HitEffect effect, HitModifier modifier) {
        super(source, -1, splat, delay);
        this.target = target;
        this.combatScript = script;
        this.effect = effect;
        this.modifier = modifier;
        this.calculateDamage();
    }

    /**
     * Calculates the damage.
     */
    private void calculateDamage() {

        int attackRoll = 0;
        int defenceRoll = 0;
        int maxDamage = 0;

        if (combatScript == null) {
            log.error("{} attempted to hit without a combat script.",
                    (source.isNPC() ? source.toNPC().getDefinitions().getName() : source.toPlayer().getDetails().getUsername()));
            setDamage(0);
            return;
        }

        boolean specialAttack = combatScript.specialAttack(source, target);
        HitModifier modifier = combatScript.getModifier(source, target);

        switch (getSplat()) {
            case MELEE_DAMAGE:
                attackRoll = CombatConstants.getMeleeAttackRoll().get(source, target, specialAttack);
                defenceRoll = CombatConstants.getMeleeDefenceRoll().get(source, target, specialAttack);
                maxDamage = CombatConstants.getMeleeMaxHit().get(source, target, specialAttack);
                break;
            case RANGE_DAMAGE:
                attackRoll = CombatConstants.getRangeAttackRoll().get(source, target, specialAttack);
                defenceRoll = CombatConstants.getRangeDefenceRoll().get(source, target, specialAttack);
                maxDamage = CombatConstants.getRangeMaxHit().get(source, target, specialAttack);
                break;
            case MAGIC_DAMAGE:
                attackRoll = CombatConstants.getMagicAttackRoll().get(source, target, specialAttack);
                defenceRoll = CombatConstants.getMagicDefenceRoll().get(source, target, specialAttack);
                maxDamage = CombatConstants.getMagicMaxHit().get(source, target, true);
                break;
            case MAGICAL_MELEE_DAMAGE:
                attackRoll = CombatConstants.getMeleeAttackRoll().get(source, target, specialAttack);
                defenceRoll = CombatConstants.getMagicDefenceRoll().get(source, target, specialAttack);
                maxDamage = CombatConstants.getMeleeMaxHit().get(source, target, specialAttack);
                break;
            case MAGICAL_RANGED_DAMAGE:
                attackRoll = CombatConstants.getMeleeAttackRoll().get(source, target, specialAttack);
                defenceRoll = CombatConstants.getMagicDefenceRoll().get(source, target, specialAttack);
                maxDamage = CombatConstants.getRangeMaxHit().get(source, target, specialAttack);
                break;
            case DRAGONFIRE:
                maxDamage = 50;
                //TODO: Dragon fire damage reduction
                int fireDamage = maxDamage; //CombatConstants.getDragonFireDamage(target, damageType, maxDamage);
                if (fireDamage > 0) {
                    accurate = true;
                    setDamage(fireDamage);
                    setSplat(HitSplat.DRAGONFIRE);
                } else {
                    setDamage(0);
                    accurate = false;
                }
                break;
        }

        //TODO: immunity for monsters
        /*if (target.isNPC() && ImmuneMonsters.isImmune(target.toNPC().getId(), damageType)) {
            damage = 0;
            accurate = false;
            if (source.isPlayer()) {
                source.toPlayer().getTransmitter().sendMessage("This monster is immune to " + damageType.toString() + ".");
            }
        }*/

        if (getDamage() == -1) {

            double accuracy;

            if (source.isPlayer() && target.isPlayer()) {
                attackRoll *= 1.1;
            }

            if (attackRoll > defenceRoll) {
                accuracy = 1 - ((defenceRoll + 2.0D) / (2.0D * (attackRoll + 1.0D)));
            } else {
                accuracy = (attackRoll) / (2.0D * (defenceRoll + 1));
            }

            double hitChance = RandomFunction.randomFloat();

            boolean ignoreAccuracy = combatScript.ignoreAccuracy(source, target) || (modifier != null && modifier.ignoreAccuracy());

            if (getAttribute(HitAttributes.VERACS_EFFECT, false)) {
                ignoreAccuracy = true;
            }

            accurate = hitChance < accuracy || ignoreAccuracy;

            if (accurate && maxDamage > 0) {
                setDamage(RandomFunction.random(0, maxDamage));
            } else {
                setDamage(0);
            }

            if (modifier != null && accurate) {
                switch (modifier.getType()) {
                    case ADD:
                        setDamage((int) (getDamage() + modifier.getModifier()));
                        break;
                    case REMOVE:
                        setDamage((int) (getDamage() - modifier.getModifier()));
                        break;
                    case DIVIDE:
                        setDamage((int) (getDamage() / modifier.getModifier()));
                        break;
                    case MULTIPLY:
                        setDamage((int) (getDamage() * modifier.getModifier()));
                        break;
                    case EQUAL:
                        setDamage((int) (modifier.getModifier()));
                        break;
                }
            }
        }

        boolean pvpHit = source.isPlayer() && target.isPlayer();

        if (!combatScript.ignorePrayerProtection(source, target) && pvpHit) {
            if (target.getPrayers().isProtectedFromDamage(getSplat())) {
                setDamage(0);
            }
        }

        applyModifiers(HitInstant.CALCULATION);

        if (target.isNPC() && target.toNPC().getId() == Npcs.COMBAT_STONE1) {
            accurate = true;
            setDamage(maxDamage);
        }

        if (target.isPlayer()) {
            target.toPlayer().getEquipment().handleAbsorption(this);
        }
    }

    /**
     * Apply the effects based on the hit's instant.
     *
     * @param instant
     */
    public void applyModifiers(HitInstant instant) {

        if (target == null || source == null || combatScript == null) {
            return;
        }

        if (isAccurate()) {

            double damageModifier = 1.0D;

            boolean pvpHit = source.isPlayer() && target.isPlayer();

            if (!combatScript.ignorePrayerProtection(source, target) && !pvpHit && instant == HitInstant.APPLICATION) {
                if (target.getPrayers().isProtectedFromDamage(getSplat())) {
                    setDamage(0);
                }
            }

            applyEffect(instant);

            WeaponInterface weapon = source.getTemporaryAttribute(AttributeKey.WEAPON);

            if (RandomFunction.randomFloat() <= 0.25) {
                if (weapon.poisonDamage() > 0 || Ammunition.hasPoisonousAmmo(source)) {
                    target.getEffectManager().apply(new PoisonEffect(target, source, weapon.poisonDamage()));
                }
            }

            if (source.isNPC()) {
                //TODO: Poisonous NPCs
            }

            if (target.isPlayer() && RandomFunction.random(100) < 70) {
                final int shieldId = target.toPlayer().getEquipment().getIdInSlot(EquipConstants.SLOT_SHIELD);
                if (shieldId == Items.ELYSIAN_SPIRIT_SHIELD1) {
                    damageModifier -= 0.25D;
                    target.sendGraphics(321, 40, 0);
                }
            }

            if (target.isNPC() && instant == HitInstant.APPLICATION) {

                //TODO: COrporeal beast
                /*if (getDamage() > 32 && RandomFunction.random(0, 8) == 0 && target instanceof CorporealBeast) {
                    CorporealBeast beast = (CorporealBeast) target;
                    beast.spawnEnergyCore();
                }*/
            }

            setDamage((int) (getDamage() * damageModifier));

        }
    }

    /**
     * Applies the hit effect.
     *
     * @param instant
     */
    public void applyEffect(HitInstant instant) {
        applyEffect(instant, effect);
    }

    /**
     * Applies the hit effect.
     *
     * @param instant
     * @param effect
     */
    public void applyEffect(HitInstant instant, HitEffect effect) {

        if (effect != null && effect.getInstant() == instant) {

            switch (effect.getType()) {
                case FREEZE:
                    CombatManager.freeze(target, source, (int) effect.getAmount());
                    break;
                case STUN:
                    target.getEffectManager().apply(new StunEffect(target, source, (int) effect.getAmount()));
                    break;
                case POISON:
                    target.getEffectManager().apply(new PoisonEffect(target, source, (int) effect.getAmount()));
                    break;
                case HEAL_ON_DAMAGE:
                    source.heal((int) (getDamage() * effect.getAmount()));
                    break;
                case HEAL_FIXED_AMOUNT:
                    source.heal((int) (effect.getAmount()));
                    break;
                case RUNNABLE_ACTION:
                    if (effect.getAction() != null) {
                        effect.getAction().run();
                    }
                    break;
            }
        }

    }

}
