package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/2/2017
 */
public class RemoveNPCSpawnsCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("rspns");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		final String key = "remove_npc_spawns";
		player.putTemporaryAttribute(key, !player.getTemporaryAttribute(key, false));
		player.getTransmitter().sendMessage("You are now " + (player.getTemporaryAttribute(key, false) ? "removing" : "examining") + " npcs.");
	}
}
