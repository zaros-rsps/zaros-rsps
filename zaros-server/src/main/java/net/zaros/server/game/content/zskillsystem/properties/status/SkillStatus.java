package net.zaros.server.game.content.zskillsystem.properties.status;

/**
 * @author Jacob Rhiel
 * Represents the status of the skill, whether it be usable or not, and by whom.
 */
public enum SkillStatus {

    /** Only available to developers. **/
    INACCESSIBLE,

    /** Available for development and bug-checking roles. **/
    DEVELOPMENTAL,

    /** Testable content for potential 'test' world. **/
    TESTABLE,

    /** Available for use for live in-game. **/
    AVAILABLE

}
