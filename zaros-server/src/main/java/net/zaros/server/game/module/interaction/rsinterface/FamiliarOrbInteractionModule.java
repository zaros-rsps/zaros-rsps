package net.zaros.server.game.module.interaction.rsinterface;

import net.zaros.server.game.module.type.InterfaceInteractionModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * Created by Jacob on 12/5/2017.
 */
public class FamiliarOrbInteractionModule implements InterfaceInteractionModule {

    @Override
    public int[] interfaceSubscriptionIds() {
        return arguments(747);
    }

    @Override
    public boolean handle(Player player, int interfaceId, int componentId, int itemId, int slotId, int packetId) {
        if(componentId == 7) {
            player.getManager().getSummoning().destructFamiliar();
        }
        return false;
    }
}
