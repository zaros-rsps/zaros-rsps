package net.zaros.server.game.clan.channel.delta;

import java.util.LinkedList;
import java.util.Queue;
import java.util.function.LongPredicate;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import net.zaros.cache.util.buffer.DynamicBuffer;
import net.zaros.server.game.clan.channel.ClanChannel;
import net.zaros.server.network.master.server.network.packet.out.ClanChannelDeltaPacketOut;
import net.zaros.server.network.master.server.world.MSRepository;

/**
 * Represents the clan channel delta update block manager.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class ClanChannelDelta {

	/**
	 * The actions list.
	 */
	private final Queue<ClanChannelAction> actions = new LinkedList<>();

	/**
	 * The owner clan channel.
	 */
	private final ClanChannel channel;

	/**
	 * Posts the current delta block update to all of the channel members.
	 * </p>
	 * The update list is sent once to each world with the clan user id(s), this was
	 * done to save time sending it to each individual clan member within each
	 * world.
	 * 
	 * @param filter
	 *               the clan channel member filter, when a member is filtered, the
	 *               delta update will not be sent to that filtered member.
	 */
	public void post(LongPredicate filter) {
		var block = encode();
		channel.getMembersByWorld().forEach((worldId, ids) -> {
			var userIds = filter == null ? ids : ids.stream().filter(f -> !filter.test(f)).collect(Collectors.toSet());
			MSRepository.getWorld(worldId).ifPresent(world -> {
				world.getSession().write(new ClanChannelDeltaPacketOut(userIds, block));
			});
		});
	}

	/**
	 * Encodes the clan channel delta update block.
	 * </p>
	 * The resulted block is ready to be transmitted over to the client.
	 * 
	 * @return the encoded update block as a {@code byte} array.
	 */
	private byte[] encode() {
		var buffer = new DynamicBuffer(128);
		buffer.writeLong(channel.getClanId());
		buffer.writeLong(channel.getUpdateNumber());
		do {
			var action = dequeue();
			if (action == null) {
				break;
			}
			buffer.writeByte(action.getId());
			action.encode(buffer);
		} while (true);
		// termination byte, tells that the actions queue is over.
		buffer.writeByte(0);
		return buffer.getDataTrimmed();
	}

	/**
	 * Enqueues a new {@link ClanChannelAction} for the next update.
	 * 
	 * @param action
	 *               the clan channel action to enqueue.
	 */
	public void enqueue(ClanChannelAction action) {
		synchronized (actions) {
			actions.add(action);
		}
	}

	/**
	 * Dequeues the next {@link ClanChannelAction} from the queue.
	 * 
	 * @return the dequeued {@link ClanChannelAction} object or {@code null} if
	 *         there was non to dequeue.
	 */
	private ClanChannelAction dequeue() {
		synchronized (actions) {
			return actions.poll();
		}
	}
}
