package net.zaros.server.game.node.entity.areas.impl;

import java.util.List;

import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.areas.Area;
import net.zaros.server.game.node.entity.areas.shape.AreaShape;

public class DefaultArea implements Area {

	@Override
	public boolean isMulti() {
		return false;
	}

	@Override
	public boolean isSummoningAllowed() {
		return true;
	}

	@Override
	public boolean isMultiCannonAllowed() {
		return true;
	}

	@Override
	public boolean isSkillTrainable(int skillId) {
		return true;
	}

	@Override
	public AreaShape getShape() {
		return null;
	}

	@Override
	public List<Integer> getRegionIds() {
		return List.of();
	}

	@Override
	public boolean canAttack(Entity attacker, Entity target) {
		return attacker.isNPC() || target.isNPC();
	}

	@Override
	public boolean canTeleport(Entity entity) {
		return true;
	}
}
