package net.zaros.server.game.content.combat.script.impl.formulas.impl;

import net.zaros.server.game.content.combat.StaticCombatFormulae;
import net.zaros.server.game.content.combat.script.impl.formulas.CombatFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.rs.constant.BonusConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

public class RangeAttackRoll implements CombatFormula {

    @Override
    public int get(Entity attacker, Entity target, boolean specialAttack) {

        var rangeLevel = attacker.getSkills().getLevel(SkillConstants.RANGE) * 1.0;

        final double prayerBoost = attacker.getPrayers().getBasePrayerBoost(SkillConstants.RANGE);
        rangeLevel *= prayerBoost;

        rangeLevel = Math.round(rangeLevel);

        final var style = attacker.isPlayer() ? attacker.toPlayer().getCombatDefinitions().getAttackStyle() :
                attacker.toNPC().getCombatDefinitions().getAttackStyle();

        if (style == 0) {
            rangeLevel += 3;
        }

        if (style == 2) {
            rangeLevel = 1;
        }

        rangeLevel += 8;

        if (attacker.isPlayer() && StaticCombatFormulae.fullVoidEquipped(attacker.toPlayer(), 11664)) {
            rangeLevel *= 1.10;
        }

        var attackBonus = attacker.getBonuses()[BonusConstants.RANGE_ATTACK];

        rangeLevel = Math.round(rangeLevel);

        var attackRoll = Math.round(rangeLevel * (64 + attackBonus)) * 1.0;

        if (attacker.isPlayer() && attacker.toPlayer().getCombatDefinitions().isSpecialActivated()) {

            int weaponId = attacker.toPlayer().getEquipment().getWeaponId();

            switch (weaponId) {
                case 861:
                    attackRoll *= 0.9;
                    break;
            }
        }

        attackRoll = Math.round(attackRoll);

        return (int) attackRoll;
    }
}
