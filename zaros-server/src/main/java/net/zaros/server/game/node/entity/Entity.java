package net.zaros.server.game.node.entity;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import lombok.Getter;
import lombok.Setter;
import net.zaros.cache.type.loctype.ObjectDefinition;
import net.zaros.cache.type.seqtype.AnimationDefinition;
import net.zaros.cache.type.seqtype.AnimationDefinitionParser;
import net.zaros.server.game.content.combat.CombatManager;
import net.zaros.server.game.content.combat.script.CombatScriptExecutor;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.Node;
import net.zaros.server.game.node.entity.areas.Area;
import net.zaros.server.game.node.entity.areas.AreaManager;
import net.zaros.server.game.node.entity.data.CombatDefinitons;
import net.zaros.server.game.node.entity.data.EntityDetails;
import net.zaros.server.game.node.entity.data.EntityHitMap;
import net.zaros.server.game.node.entity.data.EntityMovement;
import net.zaros.server.game.node.entity.effects.EffectManager;
import net.zaros.server.game.node.entity.hit.HitQueue;
import net.zaros.server.game.node.entity.link.EntityInteractionManager;
import net.zaros.server.game.node.entity.player.data.Skills;
import net.zaros.server.game.node.entity.player.link.prayer.Prayer;
import net.zaros.server.game.node.entity.player.link.prayer.PrayerManager;
import net.zaros.server.game.node.entity.render.UpdateMasks;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;
import net.zaros.server.game.node.entity.render.flag.impl.FaceEntityUpdate;
import net.zaros.server.game.node.entity.render.flag.impl.FaceLocationUpdate;
import net.zaros.server.game.node.entity.render.flag.impl.ForceTextUpdate;
import net.zaros.server.game.node.entity.render.flag.impl.Graphic;
import net.zaros.server.game.node.entity.render.flag.impl.Graphic2;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.game.world.region.Region;
import net.zaros.server.game.world.region.RegionManager;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.backend.Priority;
import net.zaros.server.utility.tool.RandomFunction;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/18/2017
 */
public abstract class Entity extends Node implements EntityDetails {

	/**
	 * The size of the map
	 */
	@Getter
	@Setter
	private int mapSize = 0;

	/**
	 * If we're at a multi area
	 */
	@Getter
	@Setter
	private boolean atMultiArea;

	/**
	 * The {@code HitMap} {@code Object} instance for this entity
	 */
	@Getter
	private transient EntityHitMap hitMap;

	/**
	 * The index of the entity
	 */
	@Getter
	@Setter
	private transient int index;

	/**
	 * The map of temporary attributes
	 */
	private transient ConcurrentHashMap<Object, Object> temporaryAttributes;

	/**
	 * The instance of the update masks
	 */
	@Getter
	private transient UpdateMasks updateMasks;

	/**
	 * The travel manager
	 */
	@Getter
	private transient EntityMovement movement;

	/**
	 * The region ids we are in
	 */
	@Getter
	private transient CopyOnWriteArrayList<Integer> mapRegionsIds;

	/**
	 * The last region the entity was in.
	 */
	@Getter
	@Setter
	private transient Region lastRegion;

	/**
	 * The location the entity was at when the maps were last loaded
	 */
	@Getter
	@Setter
	private transient Location lastLoadedLocation;

	/**
	 * If we're at a dynamic region
	 */
	@Getter
	@Setter
	private transient boolean isAtDynamicRegion;

	/**
	 * The entity who we were last attacked by
	 */
	@Getter
	@Setter
	private transient Entity attackedBy;

	/**
	 * The attacked by delay
	 */
	@Getter
	@Setter
	private transient long attackedByDelay;

	/**
	 * The entity's current area.
	 */
	@Getter
	@Setter
	private transient Area area;

	/**
	 * The interaction manager
	 */
	@Getter
	private transient EntityInteractionManager interactionManager;

	protected Entity(Location location) {
		this(-1, location);
	}

	/**
	 * The entity's hit queue.
	 */
	@Getter
	@Setter
	private transient HitQueue hitQueue;

	/**
	 * The skills of the entity
	 */
	@Getter
	private transient Skills skills;

	/**
	 * The prayer manager
	 */
	@Getter
	private transient PrayerManager prayers;

	/**
	 * The effects manager instance.
	 */
	@Getter
	private transient EffectManager effectManager;

	/**
	 * Constructs a new {@code Entity}
	 *
	 * @param location
	 *                 The location of the entity
	 */
	protected Entity(int id, Location location) {
		super(id, location);
	}

	@Override
	public void tick() {

		checkDeathEvent();

		if (isDead() || isDying()) {
			movement.resetWalkSteps();
			return;
		}

		effectManager.processEffects();

		movement.processMovement();

		hitQueue.processHits();

		AreaManager.tick(this);

		CombatScriptExecutor.run(this);

		CombatManager.decreaseAttackDelay(this);

		if (getTemporaryAttribute(AttributeKey.ATTACKER) != null && !CombatManager.underAttackTimer(this)) {
			removeTemporaryAttribute(AttributeKey.ATTACKER);
		}

		if (getTemporaryAttribute(AttributeKey.LAST_TARGET) != null && !CombatManager.isAttacking(this)) {
			removeTemporaryAttribute(AttributeKey.LAST_TARGET);
		}

		getPrayers().process();

	}

	@Override
	public String toString() {
		return "Entity{" + "index=" + index + ", isPlayer=" + isPlayer() + ", lastRegion=" + lastRegion + ", lastLoadedLocation=" + lastLoadedLocation + '}';
	}

	/**
	 * Registers all transient variables
	 */
	public void registerTransients() {
		hitMap = new EntityHitMap(this);
		updateMasks = new UpdateMasks();
		temporaryAttributes = new ConcurrentHashMap<>();
		movement = new EntityMovement(this);
		mapRegionsIds = new CopyOnWriteArrayList<>();
		interactionManager = new EntityInteractionManager();
		interactionManager.setEntity(this);
		hitQueue = new HitQueue(this);
		skills = new Skills();
		prayers = new PrayerManager();
		skills.setEntity(this);
		prayers.setEntity(this);
		area = AreaManager.getDefaultArea();
		effectManager = new EffectManager(this);
	}

	/**
	 * Removes an attribute from the {@link #temporaryAttributes} map, if it doesn't
	 * exist, the default value is returned,
	 *
	 * @param key
	 *                     The key of the attribute to remove
	 * @param defaultValue
	 *                     The default value to return
	 * @param              <T>
	 *                     The return type
	 */
	@SuppressWarnings("unchecked")
	public <T> T removeAttribute(Object key, T defaultValue) {
		T value = (T) temporaryAttributes.remove(key);
		if (value == null) {
			return defaultValue;
		}
		return value;
	}

	/**
	 * Loads all the map regions
	 */
	public void loadMapRegions() {
		mapRegionsIds.clear();
		isAtDynamicRegion = false;
		int chunkX = getLocation().getRegionX();
		int chunkY = getLocation().getRegionY();
		int mapHash = Location.VIEWPORT_SIZES[0] >> 4;
		int minRegionX = (chunkX - mapHash) / 8;
		int minRegionY = (chunkY - mapHash) / 8;
		for (int xCalc = minRegionX < 0 ? 0 : minRegionX; xCalc <= (chunkX + mapHash) / 8; xCalc++) {
			for (int yCalc = minRegionY < 0 ? 0 : minRegionY; yCalc <= (chunkY + mapHash) / 8; yCalc++) {
				// the id of the region
				int regionId = yCalc + (xCalc << 8);
				// the region will only load if we are a entity
				final Region region = isPlayer() ? RegionManager.getRegionAndLoad(regionId) : RegionManager.getRegion(regionId);
				// if the region is dynamic
				if (region.isDynamic()) {
					isAtDynamicRegion = true;
				}
				mapRegionsIds.add(regionId);
			}
		}
		lastLoadedLocation = new Location(getLocation().getX(), getLocation().getY(), getLocation().getPlane());
	}

	/**
	 * If the client needs a map update.
	 */
	public boolean needsMapUpdate() {
		int lastMapRegionX = lastLoadedLocation.getRegionX();
		int lastMapRegionY = lastLoadedLocation.getRegionY();
		int regionX = getLocation().getRegionX();
		int regionY = getLocation().getRegionY();
		int size = (Location.VIEWPORT_SIZES[0] >> 3) / 2 - 1;
		return Math.abs(lastMapRegionX - regionX) >= size || Math.abs(lastMapRegionY - regionY) >= size;
	}

	/**
	 * Teleports to a destination
	 *
	 * @param destination
	 *                    The destination
	 */
	public void teleport(Location destination) {
		putTemporaryAttribute(AttributeKey.TELEPORT_LOCATION, destination);
	}

	/**
	 * Puts the key into the attributes map
	 *
	 * @param key
	 *              The key
	 * @param value
	 *              The value
	 */
	public <T> T putTemporaryAttribute(Object key, T value) {
		temporaryAttributes.put(key, value);
		return value;
	}

	/**
	 * If we are currently teleporting in the game tick.
	 */
	public boolean teleporting() {
		return getTemporaryAttribute(AttributeKey.TELEPORTED, false);
	}

	/**
	 * Gets the attribute from the {@link #temporaryAttributes} map, and if it
	 * doesn't exist, we return the default value
	 *
	 * @param key
	 *                     The key of the attribute
	 * @param defaultValue
	 *                     The default value
	 * @param              <T>
	 *                     The return type
	 */
	@SuppressWarnings("unchecked")
	public <T> T getTemporaryAttribute(Object key, T defaultValue) {
		T value = (T) temporaryAttributes.get(key);
		if (value == null) {
			return defaultValue;
		}
		return value;
	}

	/**
	 * Resets the mask defaults
	 */
	public void resetMasks() {
		sendAnimation(-1);
		sendGraphics(-1);
		// faceEntity(null);
	}

	/**
	 * Sends an animation mask
	 *
	 * @param animationId
	 *                    The id of the animation
	 */
	public void sendAnimation(int animationId) {
		updateMasks.register(new Animation(animationId, 0, isNPC(), Priority.NORMAL));
		// lastAnimationEnd = Utils.currentTimeMillis() +
		// AnimationDefinitions.getAnimationDefinitions(nextAnimation.getIds()[0]).getDurationMS();
		AnimationDefinition definition = AnimationDefinitionParser.forId(animationId);
		if (definition != null) {
			updateMasks.setLastAnimationEndTime(System.currentTimeMillis() + definition.getDurationMS());
		}
	}

	/**
	 * Sends a graphic mask
	 *
	 * @param graphicsId
	 *                   The id of the graphic
	 */
	public void sendGraphics(int graphicsId) {
		updateMasks.register(new Graphic(graphicsId, 0, 0, isNPC()));
	}

	/**
	 * Turns this entity to the locked on entity.
	 *
	 * @param entity
	 *               The locked on entity.
	 */
	public void faceEntity(Entity entity) {
		updateMasks.register(new FaceEntityUpdate(entity, isNPC()));
	}

	/**
	 * Sends an animation
	 *
	 * @param animation
	 *                  The animation
	 */
	public void sendAnimation(Animation animation) {
		updateMasks.register(animation);
		// lastAnimationEnd = Utils.currentTimeMillis() +
		// AnimationDefinitions.getAnimationDefinitions(nextAnimation.getIds()[0]).getDurationMS();
		AnimationDefinition definition = AnimationDefinitionParser.forId(animation.getId());
		if (definition != null) {
			updateMasks.setLastAnimationEndTime(System.currentTimeMillis() + definition.getDurationMS());
		}
	}

	/**
	 * Sends an animation that makes sure that we are no longer animation
	 *
	 * @param animationId
	 *                    The animation
	 */
	public void sendAwaitedAnimation(int animationId) {
		updateMasks.register(new Animation(animationId, 0, isNPC(), Priority.LOWEST));

		AnimationDefinition definition = AnimationDefinitionParser.forId(animationId);
		if (definition != null) {
			updateMasks.setLastAnimationEndTime(System.currentTimeMillis() + definition.getDurationMS());
		}
	}

	/**
	 * Sends an animation mask
	 *
	 * @param animationId
	 *                    The id of the animation
	 * @param speed
	 *                    The speed of the animation
	 */
	public void sendAnimation(int animationId, int speed) {
		updateMasks.register(new Animation(animationId, speed, isNPC(), Priority.NORMAL));
	}

	/**
	 * Sends an animation mask
	 *
	 * @param animationId
	 *                    The id of the animation
	 * @param speed
	 *                    The speed of the animation
	 * @param priority
	 *                    The priority of the animation
	 */
	public void sendAnimation(int animationId, int speed, Priority priority) {
		updateMasks.register(new Animation(animationId, speed, isNPC(), priority));
	}

	/**
	 * Sends a graphics mask
	 *
	 * @param graphicsId
	 *                   The id of the graphic
	 * @param height
	 *                   The height to send the graphic
	 * @param speed
	 *                   The speed to send the graphic
	 */
	public void sendGraphics(int graphicsId, int height, int speed) {
		updateMasks.register(new Graphic(graphicsId, height, speed, isNPC()));
	}

	public void sendGraphics2(int graphicsId, int height, int speed) {
		updateMasks.register(new Graphic2(graphicsId, height, speed, isNPC()));
	}

	/**
	 * Sends the force text message mask
	 *
	 * @param message
	 *                The message to send
	 */
	public void sendForcedChat(String message) {
		updateMasks.register(new ForceTextUpdate(message, isNPC()));
	}

	/**
	 * If we are dead
	 */
	public boolean isDead() {
		return getHealthPoints() <= 0;
	}

	/**
	 * If we are currently dying
	 */
	public boolean isDying() {
		return getTemporaryAttribute("dying", false);
	}

	/**
	 * Checks if we are attackable by an entity..
	 *
	 * @param entity
	 *               The entity
	 */
	public boolean attackable(Entity entity) {
		return true;
	}

	/**
	 * Restores the entity's hitpoints by 1
	 */
	public boolean restoreHitPoints() {
		int maxHp = getMaxHealth();
		int healthPoints = getHealthPoints();
		if (healthPoints > maxHp) {
			if (getPrayers().prayerOn(Prayer.BERSERKER) && RandomFunction.getRandom(100) <= 15) {
				return false;
			}
			setHealthPoints(healthPoints - 1);
			return true;
		} else if (healthPoints < maxHp) {
			setHealthPoints(healthPoints + 1);
			if (getPrayers().prayerOn(Prayer.RAPID_HEAL) && healthPoints < maxHp) {
				setHealthPoints(healthPoints + 1);
			} else if (getPrayers().prayerOn(Prayer.RAPID_RENEWAL) && healthPoints < maxHp) {
				setHealthPoints(healthPoints + (healthPoints + 4 > maxHp ? maxHp - healthPoints : 4));
			}
			return true;
		}
		return false;
	}

	/**
	 * Gets the hitpoints of the entity
	 */
	public int getHealthPoints() {
		return isPlayer() ? toPlayer().getHealthPoints() : toNPC().getHealthPoints();
	}

	/**
	 * Checks if we were in combat recently.
	 */
	public boolean combatRecently() {
		long lastTimeHit = getTemporaryAttribute(AttributeKey.ATTACKED_BY_TIME, -1L);
		return TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastTimeHit) <= 10;
	}

	/**
	 * Gets an attribute from the {@link #temporaryAttributes} map
	 *
	 * @param key
	 *            The key of the attribute
	 * @param     <T>
	 *            The return type
	 */
	@SuppressWarnings("unchecked")
	public <T> T getTemporaryAttribute(Object key) {
		return (T) temporaryAttributes.get(key);
	}

	/**
	 * Removes an attribute from the {@link #temporaryAttributes} map
	 *
	 * @param key
	 *            The key
	 * @param     <T>
	 *            The return type
	 */
	@SuppressWarnings("unchecked")
	public <T> T removeTemporaryAttribute(Object key) {
		if (temporaryAttributes.containsKey(key)) {
			return (T) temporaryAttributes.remove(key);
		} else {
			return null;
		}
	}

	/**
	 * Removes an attribute from the {@link #temporaryAttributes} map
	 *
	 * @param key
	 *                    The key
	 * @param defaultValue
	 *                    the default value to return if the specified key is not
	 *                    present in the attributes.
	 * @param             <T>
	 *                    The return type
	 * @return the removed value if it was present otherwise the specified default
	 *         value.
	 */
	@SuppressWarnings("unchecked")
	public <T> T removeTemporaryAttribute(Object key, T defaultValue) {
		if (temporaryAttributes.containsKey(key)) {
			return (T) temporaryAttributes.remove(key);
		} else {
			return defaultValue;
		}
	}

	/**
	 * Checks to see if we're dead (lifepoints <=0 ), and if we are, and we haven't
	 * yet started the death event, we start it.
	 */
	protected void checkDeathEvent() {
		// if we arent dead
		if (!isDead()) {
			return;
		}
		// avoid duplicates of this method being fired
		if (isDying()) {
			return;
		}
		// flag the death
		putTemporaryAttribute("dying", true);
		// send the death
		fireDeathEvent();
	}

	/**
	 * Healths the amount
	 *
	 * @param amount
	 *               The amount
	 */
	public void heal(int amount) {
		heal(amount, 0);
	}

	/**
	 * Heals an amount
	 *
	 * @param amount
	 *               The amount
	 * @param boost
	 *               The boost health amount
	 */
	public void heal(int amount, int boost) {
		final int adjusted = getHealthPoints() + amount;
		final int boostAdjust = getMaxHealth() + boost;
		setHealthPoints(adjusted >= boostAdjust ? boostAdjust : adjusted);
	}

	/**
	 * Adds attacked by information
	 *
	 * @param target
	 *               The entity who attacked us
	 */
	public void addAttackedByDelay(Entity target) {
		setAttackedBy(target);
		if (isNPC()) {
			setAttackedByDelay(System.currentTimeMillis() + toNPC().getCombatDefinitions().getAttackDelay() * 600 + 600);
		} else {
			setAttackedByDelay(System.currentTimeMillis() + 8000);
		}
		putTemporaryAttribute(AttributeKey.ATTACKED_BY_TIME, System.currentTimeMillis());
	}

	/**
	 * Removes attacked by information
	 *
	 * @param entity
	 *               The entity who attacked us
	 */
	public void removeAttackedByDelay(Entity entity) {
		if (entity == null) {
			return;
		}
		setAttackedBy(null);
		setAttackedByDelay(-1L);
	}

	/**
	 * Turns the entity to the object
	 *
	 * @param object
	 *               The object to turn to
	 */
	public void faceObject(GameObject object) {
		Location location = object.getLocation();
		int x = -1, y = -1;
		int sizeX = 1, sizeY = 1;
		if (object.getType() == 0) { // wall
			if (object.getRotation() == 0) { // west
				x = location.getX() - 1;
				y = location.getY();
			} else if (object.getRotation() == 1) { // north
				x = location.getX();
				y = location.getY() + 1;
			} else if (object.getRotation() == 2) { // east
				x = location.getX() + 1;
				y = location.getY();
			} else if (object.getRotation() == 3) { // south
				x = location.getX();
				y = location.getY() - 1;
			}
		} else if (object.getType() == 1 || object.getType() == 2) { // corner and cornerwall
			if (object.getRotation() == 0) { // nw
				x = location.getX() - 1;
				y = location.getY() + 1;
			} else if (object.getRotation() == 1) { // ne
				x = location.getX() + 1;
				y = location.getY() + 1;
			} else if (object.getRotation() == 2) { // se
				x = location.getX() + 1;
				y = location.getY() - 1;
			} else if (object.getRotation() == 3) { // sw
				x = location.getX() - 1;
				y = location.getY() - 1;
			}
		} else if (object.getType() == 3) { // inverted corner
			if (object.getRotation() == 0) { // se
				x = location.getX() + 1;
				y = location.getY() - 1;
			} else if (object.getRotation() == 1) { // sw
				x = location.getX() - 1;
				y = location.getY() - 1;
			} else if (object.getRotation() == 2) { // nw
				x = location.getX() - 1;
				y = location.getY() + 1;
			} else if (object.getRotation() == 3) { // ne
				x = location.getX() + 1;
				y = location.getY() + 1;
			}
		} else if (object.getType() < 10) { // walldeco's
			if (object.getRotation() == 0) { // west
				x = location.getX() - 1;
				y = location.getY();
			} else if (object.getRotation() == 1) { // north
				x = location.getX();
				y = location.getY() + 1;
			} else if (object.getRotation() == 2) { // east
				x = location.getX() + 1;
				y = location.getY();
			} else if (object.getRotation() == 3) { // south
				x = location.getX();
				y = location.getY() - 1;
			}
		} else if (object.getType() == 10 || object.getType() == 11 || object.getType() == 22) { // multisized rect objs
			ObjectDefinition def = object.getDefinitions();
			if (object.getRotation() == 0 || object.getRotation() == 2) {
				x = location.getX();
				y = location.getY();
				sizeX = def.getSizeX();
				sizeY = def.getSizeY();
			} else {
				x = location.getX();
				y = location.getY();
				sizeX = def.getSizeY();
				sizeY = def.getSizeX();
			}
		} else {
			// rest
			x = location.getX();
			y = location.getY();
		}
		setFaceLocationPrecise(new Location(x, y, location.getPlane()), sizeX, sizeY);
	}

	/**
	 * Turns the entity to a location
	 *
	 * @param location
	 *                 The location
	 */
	public void faceLocation(Location location) {
		setFaceLocation(location);
	}

	public void setFaceLocation(Location nextFaceWorldTile) {
		setFaceLocationPrecise(nextFaceWorldTile, 1, 1);
	}

	public void setFaceLocationPrecise(Location base, int sizeX, int sizeY) {
		getUpdateMasks().register(new FaceLocationUpdate(this, base, sizeX, sizeY));
	}

	/**
	 * If we were in combat recently
	 */
	public boolean isUnderCombat() {
		return getAttackedByDelay() + 10000 >= System.currentTimeMillis();
	}

	/**
	 * Returns the entity's equipment bonuses.
	 * 
	 * @return the bonuses
	 */
	public abstract int[] getBonuses();

	/**
	 * Returns the entity's combat definitions.
	 *
	 * @return the definitions
	 */
	public abstract CombatDefinitons getCombatDefinitions();
}