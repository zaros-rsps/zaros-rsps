package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.utility.tool.Misc;

import java.util.Optional;

public class SlayerPointCommand extends CommandModule {

    @Override
    public String[] identifiers() {
        return arguments("slayerpoints");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {
        String points = args[1];
        final String name = Misc.formatPlayerNameForProtocol(getCompleted(args, 2));
        Optional<Player> optional = World.getPlayerByUsername(name);
        if(name == null) {
            player.getManager().getSlayer().setSlayerPoints(player.getManager().getSlayer().getSlayerPoints() + Integer.valueOf(points));
            return;
        }
        Player otherPlayer;
        if(optional.isPresent()) {
            otherPlayer = optional.get();
            otherPlayer.getManager().getSlayer().setSlayerPoints(otherPlayer.getManager().getSlayer().getSlayerPoints() + Integer.valueOf(points));
            player.getTransmitter().sendMessage("Granted " + name + ", " + points + ", slayer points.");
            return;
        }
    }
}
