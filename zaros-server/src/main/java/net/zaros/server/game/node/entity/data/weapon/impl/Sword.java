package net.zaros.server.game.node.entity.data.weapon.impl;

import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;

import java.util.List;

/**
 * The interface for swords.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum Sword implements WeaponInterface {
    /**
     * The regular sword weapon interface.
     */
    REGULAR();

    /**
     * The attack types.
     */
    private static final List<AttackType> attackTypes =
            List.of(AttackType.SWORD_STAB, AttackType.SWORD_LUNGE, AttackType.SWORD_SLASH, AttackType.SWORD_BLOCK);

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }
}
