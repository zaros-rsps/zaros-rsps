package net.zaros.server.game.content.event.impl.item;

import net.zaros.server.game.content.event.Event;
import net.zaros.server.game.content.event.EventPolicy.ActionPolicy;
import net.zaros.server.game.content.event.EventPolicy.AnimationPolicy;
import net.zaros.server.game.content.event.EventPolicy.InterfacePolicy;
import net.zaros.server.game.content.event.EventPolicy.WalkablePolicy;
import net.zaros.server.game.content.event.context.item.ItemOnItemContext;
import net.zaros.server.game.content.zskillsystem.SkillExecutor;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/8/2017
 */
public class ItemOnItemEvent extends Event<ItemOnItemContext> {
	
	/**
	 * Constructs a new event
	 */
	public ItemOnItemEvent() {
		setInterfacePolicy(InterfacePolicy.CLOSE);
		setAnimationPolicy(AnimationPolicy.RESET);
		setWalkablePolicy(WalkablePolicy.RESET);
		setActionPolicy(ActionPolicy.RESET);
	}
	
	@Override
	public void run(Player player, ItemOnItemContext context) {

		SkillExecutor executor = new SkillExecutor();

		int usedSlot = context.getUsedSlot();
		int withSlot = context.getWithSlot();
		
		Item usedItem = player.getInventory().getItems().get(usedSlot);
		Item withItem = player.getInventory().getItems().get(withSlot);

		if (usedItem == null || withItem == null) {
			return;
		}

		executor.execute(context);


	}
}
