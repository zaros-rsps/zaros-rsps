package net.zaros.server.game.clan.channel.delta;

import net.zaros.cache.util.buffer.Buffer;

/**
 * Represents an abstract clan channel action. A channel action is an action
 * that modifies the channel in the client side to synchronise it with the
 * server version.
 * 
 * @author Walied K. Yassen
 */
public abstract class ClanChannelAction {

	/**
	 * Encodes the action data into the specified {@link Buffer buffer}.
	 * 
	 * @param buffer
	 *               the buffer to encode into.
	 */
	public abstract void encode(Buffer buffer);

	/**
	 * Gets the channel action id.
	 * 
	 * @return the channel action id.
	 */
	public abstract int getId();
}
