package net.zaros.server.game.node.entity.areas;

import java.util.List;

import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.areas.shape.AreaShape;
import net.zaros.server.game.node.entity.player.Player;

/**
 * A interface for ingame areas and control over them.
 *
 * @author Gabriel || Wolfsdarker
 * @author Walied K. Yassen
 */
public interface Area {

	/**
	 * Returns if this is a multi combat area.
	 *
	 * @return if is multi
	 */
	boolean isMulti();

	/**
	 * Returns if the area accepts summoning.
	 *
	 * @return if accepts summoning
	 */
	boolean isSummoningAllowed();

	/**
	 * Returns if the area accepts dwarf multi cannons.
	 *
	 * @return if this accepts cannons
	 */
	boolean isMultiCannonAllowed();

	/**
	 * Returns if certain skill can be trained inside that area.
	 *
	 * @param skillId
	 * @return if its trainable
	 */
	boolean isSkillTrainable(int skillId);

	/**
	 * Gets the {@link AreaShape} object of this area.
	 * 
	 * @return the {@link AreaShape} object or {@code null} if there was none
	 *         present.
	 */
	AreaShape getShape();

	/**
	 * The list of regions that are part of the area.
	 *
	 * @return the regions
	 */
	List<Integer> getRegionIds();

	/**
	 * Returns if the attacker can attack the target inside the area.
	 *
	 * @param attacker
	 * @param target
	 * @return if attacking is possible
	 */
	boolean canAttack(Entity attacker, Entity target);

	/**
	 * Returns if the entity can teleport out the area.
	 *
	 * @param entity
	 * @return if teleports are possible
	 */
	boolean canTeleport(Entity entity);

	/**
	 * Returns if the player is within the area.
	 *
	 * @param location
	 * @return if is within
	 */
	default boolean isWithin(Location location) {
		var shape = getShape();
		if (shape != null) {
			if (shape.intersects(location)) {
				return true;
			}
		}
		var regions = getRegionIds();
		if (!regions.isEmpty()) {
			if (regions.contains(location.getRegionId())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns if the player is allowed to logout inside the area.
	 *
	 * @param player
	 * @return if logout is allowed
	 */
	default boolean isLogoutAllowed(Player player) {
		return true;
	}

	/**
	 * The hook called when the entity enters the area.
	 *
	 * @param entity
	 */
	default void enterAreaHook(Entity entity) {
	}

	/**
	 * The hook called when the entity leaves the area without logging out.
	 *
	 * @param entity
	 */
	default void leaveAreaHook(Entity entity) {
	}

	/**
	 * The hook called when the player log inside the area.
	 *
	 * @param player
	 */
	default void loginHook(Player player) {
	}

	/**
	 * The hook called when the player leaves the area by logging out.
	 *
	 * @param player
	 */
	default void logoutHook(Player player) {
	}

	/**
	 * The hook for entity's death.
	 *
	 * @param killer
	 * @param victim
	 * @return true/false to ignore default death methods.
	 */
	default boolean handleEntityDeath(Entity killer, Entity victim) {
		return false;
	}

	/**
	 * Updates the area every tick with the entity inside it.
	 *
	 * @param entity
	 */
	default void onTick(Entity entity) {
	}

	/**
	 * Creates a new {@link Location} object instance.
	 * 
	 * @param x
	 *          the location x coordinate.
	 * @param y
	 *          the location y coordinate.
	 * @return the created {@link Location} object.
	 */
	static Location loc(int x, int y) {
		return new Location(x, y);
	}
}
