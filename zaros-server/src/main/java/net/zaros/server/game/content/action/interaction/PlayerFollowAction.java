package net.zaros.server.game.content.action.interaction;

import net.zaros.server.game.content.action.Action;
import net.zaros.server.game.content.combat.CombatManager;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.game.world.region.route.RouteFinder;
import net.zaros.server.game.world.region.route.strategy.EntityStrategy;
import net.zaros.server.utility.AttributeKey;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/20/2017
 */
public class PlayerFollowAction implements Action {
	
	/**
	 * The partner we're following
	 */
	private final Entity partner;
	
	public PlayerFollowAction(Entity partner) {
		this.partner = partner;
	}
	
	@Override
	public boolean start(Entity entity) {
		Player player = (Player) entity;
		if (!canContinue(player)) {
			return false;
		}
		player.faceEntity(partner);
		traverseCalculatePath(player);
		return true;
	}
	
	@Override
	public boolean process(Entity entity) {
		Player player = (Player) entity;
		if (!canContinue(player)) {
			entity.getMovement().resetWalkSteps();
			return false;
		}
		if (withinDistance(player)) {
			return true;
		}
		int direction = partner.getTemporaryAttribute("direction", 0);
		switch (direction) {
			case 0: //north
				player.getMovement().addWalkSteps(partner.getLocation().getX(), partner.getLocation().getY() + 1, 25, true);
				break;
			case 2048: //north-east
				player.getMovement().addWalkSteps(partner.getLocation().getX() + 1, partner.getLocation().getY() + 1, 25, true);
				break;
			case 4096: //east
				player.getMovement().addWalkSteps(partner.getLocation().getX() + 1, partner.getLocation().getY(), 25, true);
				break;
			case 6144: //south-east
				player.getMovement().addWalkSteps(partner.getLocation().getX() + 1, partner.getLocation().getY() - 1, 25, true);
				break;
			case 8192: //south
				player.getMovement().addWalkSteps(partner.getLocation().getX(), partner.getLocation().getY() - 1, 25, true);
				break;
			case 10240: //south-west
				player.getMovement().addWalkSteps(partner.getLocation().getX() - 1, partner.getLocation().getY() - 1, 25, true);
				break;
			case 12288: //west
				player.getMovement().addWalkSteps(partner.getLocation().getX() - 1, partner.getLocation().getY(), 25, true);
				break;
			case 14336: //north-west
				player.getMovement().addWalkSteps(partner.getLocation().getX() - 1, partner.getLocation().getY() + 1, 25, true);
				break;
		}
		return true;
	}
	
	@Override
	public int processOnTicks(Entity entity) {
		Player player = (Player) entity;
		return canContinue(player) ? 0 : -1;
	}
	
	@Override
	public void stop(Entity entity) {
		Player player = (Player) entity;
		player.faceEntity(null);
	}
	
	/**
	 * Verify that we can continue following the partner
	 *
	 * @param player
	 * 		The player
	 */
	private boolean canContinue(Player player) {

		if (partner == null || !partner.isRenderable()) {
			return false;
		}

		if (!player.getEffectManager().isWalkEnabled(false) || player.isDead() || player.getHealthPoints() < 1) {
			return false;
		}

		if (!partner.getLocation().isWithinDistance(player.getLocation())) {
			return false;
		}

		if (partner.isPlayer() && !World.getPlayers().contains(partner.toPlayer())) {
			return false;
		}

		return true;
	}

	/**
	 * If the player is within the partner's distance.
	 * @param player
	 * @return if is within distance
	 */
	private boolean withinDistance(Player player) {

		if(CombatManager.isAttacking(player)) {

			CombatScript script = player.getTemporaryAttribute(AttributeKey.COMBAT_SCRIPT);

			if(script == null) {
				return false;
			}

			return CombatManager.isWithinDistance(player, partner, script, true);
		}
		return false;
	}
	
	/**
	 * Uses entity route strategizing to path to the partner
	 */
	private void traverseCalculatePath(Player player) {
		int steps = RouteFinder.findRoute(RouteFinder.WALK_ROUTEFINDER, player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getPlane(), player.getSize(), new EntityStrategy(partner), false);
		if (steps == -1) {
			return;
		}
		int[] bufferX = RouteFinder.getLastPathBufferX();
		int[] bufferY = RouteFinder.getLastPathBufferY();
		
		Location last = new Location(bufferX[0], bufferY[0], player.getLocation().getPlane());
		player.getMovement().resetWalkSteps();
		player.getTransmitter().sendMinimapFlag(last.getLocalX(player.getLastLoadedLocation()), last.getLocalY(player.getLastLoadedLocation()));
		for (int step = steps - 1; step >= 0; step--) {
			if (!player.getMovement().addWalkSteps(bufferX[step], bufferY[step], 25, true)) {
				break;
			}
		}
	}
	
}
