package net.zaros.server.game.content.combat.player.swing;

import net.zaros.server.core.system.SystemManager;
import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.StaticCombatFormulae;
import net.zaros.server.game.content.combat.player.CombatRegistry;
import net.zaros.server.game.content.combat.player.CombatTypeSwing;
import net.zaros.server.game.content.combat.player.calc.RangeCombatCalculator;
import net.zaros.server.game.content.combat.player.registry.wrapper.BowFireEvent;
import net.zaros.server.game.content.combat.player.registry.wrapper.CombatSwingDetail;
import net.zaros.server.game.content.combat.player.registry.wrapper.SpecialAttackEvent;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.render.flag.impl.AppearanceUpdate;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.world.region.RegionManager;
import net.zaros.server.utility.rs.Hit;
import net.zaros.server.utility.rs.Hit.HitAttributes;
import net.zaros.server.utility.rs.Hit.HitSplat;
import net.zaros.server.utility.rs.constant.EquipConstants;
import net.zaros.server.utility.rs.constant.ItemConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;
import net.zaros.server.utility.tool.Misc;

import java.util.Optional;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/22/2017
 */
public class RangeCombatSwing extends CombatTypeSwing {
	
	public RangeCombatSwing() {
		super(new RangeCombatCalculator());
	}
	
	@Override
	public boolean run(Player player, Entity target, int weaponId, int combatStyle, SpecialAttackEvent special) {
		int response = StaticCombatFormulae.getRangeResponse(player);
		if (response == 3) {
			player.getTransmitter().sendMessage("You don't have any more ammo left to use.");
			return false;
		} else if (response == 1) {
			player.getTransmitter().sendMessage("The ammo you're using is ineffective with your bow.");
			return false;
		}
		// we check the special attacks
		boolean usingSpecial = special != null;
		// the energy required
		final int energyRequired = ItemConstants.getSpecialEnergy(weaponId);
		if (usingSpecial) {
			// set the special attack off now...
			player.getCombatDefinitions().setSpecialActivated(false);
			if (player.getCombatDefinitions().getSpecialEnergy() < energyRequired) {
				player.getTransmitter().sendMessage("You don't have enough special attack energy.");
				usingSpecial = false;
			}
		}
		// custom attack send
		if (usingSpecial) {
			special.fire(player, target, this, combatStyle);
			player.getCombatDefinitions().reduceSpecial(energyRequired);
		} else {
			Optional<BowFireEvent> optional = CombatRegistry.getBow(weaponId);
			if (!optional.isPresent()) {
				player.getTransmitter().sendMessage("This bow has not yet been configured, please report it on the forums.");
				return false;
			}
			BowFireEvent event = optional.get();
			player.sendAnimation(StaticCombatFormulae.getWeaponAttackEmote(weaponId, player.getCombatDefinitions().getAttackStyle()));
			event.fire(player, target, this, weaponId, player.getEquipment().getIdInSlot(EquipConstants.SLOT_ARROWS));
		}
		return true;
	}
	
	@Override
	public double getAttackBonus(Player player, int weaponId, int combatStyle, boolean specialAttack) {
		return calculator.totalAggressiveBoost(player, combatStyle, specialAttack);
	}
	
	@Override
	public double getDefenceBonus(Entity entity, int weaponId, int combatStyle) {
		return calculator.totalDefensiveBoost(entity, weaponId, combatStyle);
	}
	
	@Override
	public double getMaxHit(Player player, int weaponId, int combatStyle, double multiplier) {
		return calculator.maximumDamageAppendable(player, weaponId, combatStyle, multiplier);
	}
	
	@Override
	public void applyHit(Player attacker, Entity receiver, Hit hit, int itemId, int combatStyle, int delay) {
		appendExperience(attacker, receiver, hit.getDamage());
		// handles the leeches aspect of the hit
		if (receiver.isPlayer()) {
			receiver.toPlayer().getPrayers().handlePrayerEffects(hit);
		} else {
			receiver.toNPC().handlePrayerEffects(hit);
		}
		SystemManager.getScheduler().schedule(new ScheduledTask(delay) {
			@Override
			public void run() {
				// the attribute is put when the hit actually appears
				hit.getAttributes().put(HitAttributes.WEAPON_USED, itemId);
				// and the hit is applied to the receiver
				receiver.getHitQueue().add(hit);
			}
		});
	}
	
	@Override
	public void appendExperience(Player player, Entity target, Object... params) {
		int damage = (int) params[0];
		
		if (damage > 0) {
			double combatXp = damage / 2.5;
			int attackStyle = player.getCombatDefinitions().getAttackStyle();
			if (attackStyle == 2) {
				if (target.isPlayer()) {
					player.getSkills().addExperienceNoMultiplier(SkillConstants.RANGE, combatXp / 2);
					player.getSkills().addExperienceNoMultiplier(SkillConstants.DEFENCE, combatXp / 2);
				} else {
					player.getSkills().addExperienceWithMultiplier(SkillConstants.RANGE, combatXp / 2);
					player.getSkills().addExperienceWithMultiplier(SkillConstants.DEFENCE, combatXp / 2);
				}
			} else {
				if (target.isPlayer()) {
					player.getSkills().addExperienceNoMultiplier(SkillConstants.RANGE, combatXp);
				} else {
					player.getSkills().addExperienceWithMultiplier(SkillConstants.RANGE, combatXp);
				}
			}
		}
	}
	
	/**
	 * Drops ammo on the ground
	 *
	 * @param player
	 * 		The entity to drop ammo for
	 * @param location
	 * 		The location of the target
	 * @param ammoSlot
	 * 		The slot of the ammo
	 * @param ammoId
	 * 		The id of the ammo to use
	 */
	public void dropAmmo(Player player, Location location, int ammoSlot, int ammoId, boolean delete) {
		Item ammo = player.getEquipment().getItem(ammoSlot);
		// no ammo here, safe check [this shouldn't happen anyway]
		if (ammo == null || ammo.getId() != ammoId) {
			return;
		}
		// the entity doesn't lose ammo in the case they're lucky with a cape that saves ammo
		if (player.getEquipment().capeSavesAmmo() && Misc.getRandom(3) == 2) {
			return;
		}
		// the new amount to set
		int newAmount = ammo.getAmount() - 1;
		// if we should remove the item from the equipment
		final boolean removed = newAmount <= 0;
		// removes the ammo from the equipment
		player.getEquipment().getItems().set(ammoSlot, removed ? null : new Item(ammoId, newAmount));
		// we aren't deleting so it must be dropped on ground...
		if (!delete) {
			RegionManager.addStackableFloorItem(ammoId, 1, 180, location, player.getDetails().getUsername());
		}
		player.getEquipment().refresh(ammoSlot);
		// if the item is removed
		if (removed) {
			player.getUpdateMasks().register(new AppearanceUpdate(player));
		}
	}
	
	/**
	 * Sends the damage to the target
	 *
	 * @param attacker
	 * 		The attacker
	 * @param target
	 * 		The target
	 * @param swing
	 * 		The swing
	 * @param weaponId
	 * 		The weapon id
	 * @param modifier
	 * 		The max hit modifier
	 * @param specialAttack
	 * 		If we are using a special attack
	 */
	public static CombatSwingDetail sendDamage(Player attacker, Entity target, RangeCombatSwing swing, int weaponId, double modifier, boolean specialAttack) {
		final int style = attacker.getCombatDefinitions().getAttackStyle();
		final int delay = ProjectileManager.getProjectileDelay(attacker, target);
		final double maxHit = swing.getMaxHit(attacker, weaponId, style, modifier);
		final int damage = swing.randomizeHit(maxHit, swing.getAttackBonus(attacker, weaponId, style, specialAttack), swing.getDefenceBonus(target, weaponId, style));
		return sendDamage(attacker, target, swing, weaponId, style, delay, maxHit, damage, null);
	}
	
	/**
	 * Sends the damage to the target with predefined damage
	 *
	 * @param attacker
	 * 		The attacker
	 * @param target
	 * 		The target
	 * @param swing
	 * 		The swing
	 * @param weaponId
	 * 		The weapon id
	 * @param damage
	 * 		The damage to append
	 */
	public static CombatSwingDetail sendDamage(Player attacker, Entity target, RangeCombatSwing swing, int weaponId, int damage) {
		final int style = attacker.getCombatDefinitions().getAttackStyle();
		final int delay = ProjectileManager.getProjectileDelay(attacker, target);
		return sendDamage(attacker, target, swing, weaponId, style, delay, damage, damage, null);
	}
	
	/**
	 * Sends damage to the target
	 *
	 * @param attacker
	 * 		The attacker
	 * @param target
	 * 		The target
	 * @param swing
	 * 		The swing
	 * @param weaponId
	 * 		The id of the weapon
	 * @param style
	 * 		The style we're using
	 * @param delay
	 * 		The delay to use
	 * @param maxHit
	 * 		The max hit
	 * @param damage
	 * 		The damage
	 */
	public static CombatSwingDetail sendDamage(Player attacker, Entity target, RangeCombatSwing swing, int weaponId, int style, int delay, double maxHit, int damage, Runnable landTask) {
		// hit, ammo, defend
		final Hit hit = new Hit(attacker, damage, HitSplat.RANGE_DAMAGE).setMaxHit(maxHit);
		swing.applyHit(attacker, target, hit, weaponId, style, delay);
		sendBlockEmote(target, delay);
		if (landTask != null) {
			SystemManager.getScheduler().schedule(new ScheduledTask(delay) {
				@Override
				public void run() {
					landTask.run();
				}
			});
		}
		return new CombatSwingDetail(attacker, target, hit);
	}
	
	/**
	 * Makes the target do their block emote a processEffects before the delay
	 *
	 * @param target
	 * 		The target
	 * @param delay
	 * 		Their block emote
	 */
	private static void sendBlockEmote(Entity target, int delay) {
		SystemManager.getScheduler().schedule(new ScheduledTask(delay - 1) {
			@Override
			public void run() {
				target.sendAwaitedAnimation(StaticCombatFormulae.getDefenceEmote(target));
			}
		});
	}
	
}
