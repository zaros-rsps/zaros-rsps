package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.content.skills.slayer.SlayerMaster;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

public class SlayerTaskCommand extends CommandModule {

    @Override
    public String[] identifiers() {
        return arguments("task");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {
        player.getManager().getSlayer().generate(SlayerMaster.DURADEL);
    }
}

