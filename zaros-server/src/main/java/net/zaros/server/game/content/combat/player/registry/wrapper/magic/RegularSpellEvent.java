package net.zaros.server.game.content.combat.player.registry.wrapper.magic;

import net.zaros.server.utility.rs.constant.MagicConstants;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/27/2017
 */
public interface RegularSpellEvent extends MagicSpellEvent, MagicConstants {
	
}
