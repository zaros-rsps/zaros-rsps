package net.zaros.server.game.module.interaction.item;

import net.zaros.server.game.module.type.ItemInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.InteractionOption;

public class CoinInteractionModule extends ItemInteractionModule {

    public CoinInteractionModule() {
        register(995);
    }

    @Override
    public boolean handle(Player player, Item item, int slotId, InteractionOption option) {
        if (option == InteractionOption.FOURTH_OPTION) {
            player.getManager().getMoneyPouch().deposit();
            return true;
        }
        return false;
    }

}