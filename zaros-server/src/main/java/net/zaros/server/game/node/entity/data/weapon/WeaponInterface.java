package net.zaros.server.game.node.entity.data.weapon;

import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.impl.*;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.Hit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple weapon interface.
 *
 * @author Gabriel || Wolfsdarker
 */
public interface WeaponInterface {

    /**
     * The default weapon interface.
     */
    WeaponInterface unarmed = new Unarmed();

    /**
     * The map of weapon interfaces.
     */
    Map<Integer, WeaponInterface> interfaces = new HashMap<>();

    /**
     * The weapon's attack type.
     *
     * @return the hitsplat
     */
    Hit.HitSplat attackType();

    /**
     * The weapon's attack styles.
     *
     * @return the styles
     */
    List<AttackType> attackStyles();

    /**
     * The weapon's special attack.
     *
     * @return the special attack.
     */
    WeaponSpecial specialAttack();

    /**
     * The weapon's poison damage, when poison is present.
     *
     * @return the damage
     */
    default int poisonDamage() {
        return 0;
    }

    /**
     * The weapon's ammunition.
     *
     * @return the ammunition
     */
    default Ammunition ammunition() {
        return null;
    }

    /**
     * The weapon's compatible ammunition IDs.
     *
     * @return the item IDs
     */
    default List<Integer> compatibleAmmunitionIDs() {
        return List.of();
    }


    /**
     * Returns the interface for the entity's weapon.
     *
     * @param entity
     * @return the interface
     */
    static WeaponInterface get(Entity entity) {

        if (entity.isNPC()) {
            return null;
        }

        return interfaces.getOrDefault(entity.toPlayer().getEquipment().getWeaponId(), unarmed);
    }

    /**
     * Loads all the weapon interfaces.
     */
    static void load() {
        interfaces.put(Items.BRONZE_DAGGER1, Dagger.REGULAR);
        interfaces.put(Items.IRON_DAGGER1, Dagger.REGULAR);
        interfaces.put(Items.STEEL_DAGGER1, Dagger.REGULAR);
        interfaces.put(Items.BLACK_DAGGER1, Dagger.REGULAR);
        interfaces.put(Items.WHITE_DAGGER1, Dagger.REGULAR);
        interfaces.put(Items.MITHRIL_DAGGER1, Dagger.REGULAR);
        interfaces.put(Items.ADAMANT_DAGGER1, Dagger.REGULAR);
        interfaces.put(Items.RUNE_DAGGER1, Dagger.REGULAR);
        interfaces.put(Items.DRAGON_DAGGER1, Dagger.DRAGON_DAGGER);

        interfaces.put(Items.BRONZE_DAGGER_P1, Dagger.REGULAR_P);
        interfaces.put(Items.IRON_DAGGER_P1, Dagger.REGULAR_P);
        interfaces.put(Items.STEEL_DAGGER_P1, Dagger.REGULAR_P);
        interfaces.put(Items.BLACK_DAGGER_P1, Dagger.REGULAR_P);
        interfaces.put(Items.WHITE_DAGGER_P1, Dagger.REGULAR_P);
        interfaces.put(Items.MITHRIL_DAGGER_P1, Dagger.REGULAR_P);
        interfaces.put(Items.ADAMANT_DAGGER_P1, Dagger.REGULAR_P);
        interfaces.put(Items.RUNE_DAGGER_P1, Dagger.REGULAR_P);
        interfaces.put(Items.DRAGON_DAGGER_P1, Dagger.DRAGON_DAGGER_P);

        interfaces.put(Items.BRONZE_DAGGER_PP1, Dagger.REGULAR_PP);
        interfaces.put(Items.IRON_DAGGER_PP1, Dagger.REGULAR_PP);
        interfaces.put(Items.STEEL_DAGGER_PP1, Dagger.REGULAR_PP);
        interfaces.put(Items.BLACK_DAGGER_PP1, Dagger.REGULAR_PP);
        interfaces.put(Items.WHITE_DAGGER_PP1, Dagger.REGULAR_PP);
        interfaces.put(Items.MITHRIL_DAGGER_PP1, Dagger.REGULAR_PP);
        interfaces.put(Items.ADAMANT_DAGGER_PP1, Dagger.REGULAR_PP);
        interfaces.put(Items.RUNE_DAGGER_PP1, Dagger.REGULAR_PP);
        interfaces.put(Items.DRAGON_DAGGER_PP1, Dagger.DRAGON_DAGGER_PP);

        interfaces.put(Items.BRONZE_DAGGER_PPP1, Dagger.REGULAR_PPP);
        interfaces.put(Items.IRON_DAGGER_PPP1, Dagger.REGULAR_PPP);
        interfaces.put(Items.STEEL_DAGGER_PPP1, Dagger.REGULAR_PPP);
        interfaces.put(Items.BLACK_DAGGER_PPP1, Dagger.REGULAR_PPP);
        interfaces.put(Items.WHITE_DAGGER_PPP1, Dagger.REGULAR_PPP);
        interfaces.put(Items.MITHRIL_DAGGER_PPP1, Dagger.REGULAR_PPP);
        interfaces.put(Items.ADAMANT_DAGGER_PPP1, Dagger.REGULAR_PPP);
        interfaces.put(Items.RUNE_DAGGER_PPP1, Dagger.REGULAR_PPP);
        interfaces.put(Items.DRAGON_DAGGER_PPP1, Dagger.DRAGON_DAGGER_PPP);

        interfaces.put(Items.BRONZE_HALBERD1, Halberd.REGULAR);
        interfaces.put(Items.IRON_HALBERD1, Halberd.REGULAR);
        interfaces.put(Items.STEEL_HALBERD1, Halberd.REGULAR);
        interfaces.put(Items.BLACK_HALBERD1, Halberd.REGULAR);
        interfaces.put(Items.WHITE_HALBERD1, Halberd.REGULAR);
        interfaces.put(Items.MITHRIL_HALBERD1, Halberd.REGULAR);
        interfaces.put(Items.ADAMANT_HALBERD1, Halberd.REGULAR);
        interfaces.put(Items.RUNE_HALBERD1, Halberd.REGULAR);
        interfaces.put(Items.DRAGON_HALBERD1, Halberd.DRAGON);

        interfaces.put(Items.BRONZE_LONGSWORD1, Longsword.REGULAR);
        interfaces.put(Items.IRON_LONGSWORD1, Longsword.REGULAR);
        interfaces.put(Items.STEEL_LONGSWORD1, Longsword.REGULAR);
        interfaces.put(Items.BLACK_LONGSWORD1, Longsword.REGULAR);
        interfaces.put(Items.WHITE_LONGSWORD1, Longsword.REGULAR);
        interfaces.put(Items.MITHRIL_LONGSWORD1, Longsword.REGULAR);
        interfaces.put(Items.ADAMANT_LONGSWORD1, Longsword.REGULAR);
        interfaces.put(Items.CORRUPT_VESTAS_LONGSWORD1, Longsword.REGULAR);
        interfaces.put(Items.RUNE_LONGSWORD1, Longsword.REGULAR);
        interfaces.put(Items.DRAGON_LONGSWORD1, Longsword.DRAGON);
        interfaces.put(Items.VESTAS_LONGSWORD1, Longsword.VESTA);

        interfaces.put(Items.BRONZE_MACE1, Mace.REGULAR);
        interfaces.put(Items.IRON_MACE1, Mace.REGULAR);
        interfaces.put(Items.STEEL_MACE1, Mace.REGULAR);
        interfaces.put(Items.BLACK_MACE1, Mace.REGULAR);
        interfaces.put(Items.WHITE_MACE1, Mace.REGULAR);
        interfaces.put(Items.MITHRIL_MACE1, Mace.REGULAR);
        interfaces.put(Items.ADAMANT_MACE1, Mace.REGULAR);
        interfaces.put(Items.RUNE_MACE1, Mace.REGULAR);
        interfaces.put(Items.ANCIENT_MACE1, Mace.ANCIENT);
        interfaces.put(Items.DRAGON_MACE1, Mace.DRAGON);

        interfaces.put(Items.BRONZE_PICKAXE1, Pickaxe.REGULAR);
        interfaces.put(Items.IRON_PICKAXE1, Pickaxe.REGULAR);
        interfaces.put(Items.STEEL_PICKAXE1, Pickaxe.REGULAR);
        interfaces.put(Items.MITHRIL_PICKAXE1, Pickaxe.REGULAR);
        interfaces.put(Items.ADAMANT_PICKAXE1, Pickaxe.REGULAR);
        interfaces.put(Items.RUNE_PICKAXE1, Pickaxe.REGULAR);
        interfaces.put(Items.INFERNO_ADZE, Pickaxe.REGULAR);
        interfaces.put(Items.DRAGON_PICKAXE1, Pickaxe.DRAGON);

        interfaces.put(Items.BRONZE_SCIMITAR1, Scimitar.REGULAR);
        interfaces.put(Items.IRON_SCIMITAR1, Scimitar.REGULAR);
        interfaces.put(Items.STEEL_SCIMITAR1, Scimitar.REGULAR);
        interfaces.put(Items.BLACK_SCIMITAR1, Scimitar.REGULAR);
        interfaces.put(Items.WHITE_SCIMITAR1, Scimitar.REGULAR);
        interfaces.put(Items.MITHRIL_SCIMITAR1, Scimitar.REGULAR);
        interfaces.put(Items.ADAMANT_SCIMITAR1, Scimitar.REGULAR);
        interfaces.put(Items.RUNE_SCIMITAR1, Scimitar.REGULAR);
        interfaces.put(Items.DRAGON_SCIMITAR1, Scimitar.DRAGON);

        interfaces.put(Items.BRONZE_SPEAR1, Spear.REGULAR);
        interfaces.put(Items.IRON_SPEAR1, Spear.REGULAR);
        interfaces.put(Items.STEEL_SPEAR1, Spear.REGULAR);
        interfaces.put(Items.BLACK_SPEAR1, Spear.REGULAR);
        interfaces.put(Items.MITHRIL_SPEAR1, Spear.REGULAR);
        interfaces.put(Items.ADAMANT_SPEAR1, Spear.REGULAR);
        interfaces.put(Items.CORRUPT_VESTAS_SPEAR1, Spear.REGULAR);
        interfaces.put(Items.RUNE_SPEAR1, Spear.REGULAR);
        interfaces.put(Items.DRAGON_SPEAR1, Spear.DRAGON);
        interfaces.put(Items.ZAMORAKIAN_SPEAR1, Spear.ZAMORAKIAN);
        interfaces.put(Items.VESTAS_SPEAR1, Spear.VESTA);

        interfaces.put(Items.BRONZE_SPEAR_P1, Spear.REGULAR_P);
        interfaces.put(Items.IRON_SPEAR_P1, Spear.REGULAR_P);
        interfaces.put(Items.STEEL_SPEAR_P1, Spear.REGULAR_P);
        interfaces.put(Items.BLACK_SPEAR_P1, Spear.REGULAR_P);
        interfaces.put(Items.MITHRIL_SPEAR_P1, Spear.REGULAR_P);
        interfaces.put(Items.ADAMANT_SPEAR_P1, Spear.REGULAR_P);
        interfaces.put(Items.RUNE_SPEAR_P1, Spear.REGULAR_P);
        interfaces.put(Items.DRAGON_SPEAR_P1, Spear.DRAGON_P);

        interfaces.put(Items.BRONZE_SPEAR_PP1, Spear.REGULAR_PP);
        interfaces.put(Items.IRON_SPEAR_PP1, Spear.REGULAR_PP);
        interfaces.put(Items.STEEL_SPEAR_PP1, Spear.REGULAR_PP);
        interfaces.put(Items.BLACK_SPEAR_PP1, Spear.REGULAR_PP);
        interfaces.put(Items.MITHRIL_SPEAR_PP1, Spear.REGULAR_PP);
        interfaces.put(Items.ADAMANT_SPEAR_PP1, Spear.REGULAR_PP);
        interfaces.put(Items.RUNE_SPEAR_PP1, Spear.REGULAR_PP);
        interfaces.put(Items.DRAGON_SPEAR_PP1, Spear.DRAGON_PP);

        interfaces.put(Items.BRONZE_SPEAR_PPP1, Spear.REGULAR_PPP);
        interfaces.put(Items.IRON_SPEAR_PPP1, Spear.REGULAR_PPP);
        interfaces.put(Items.STEEL_SPEAR_PPP1, Spear.REGULAR_PPP);
        interfaces.put(Items.BLACK_SPEAR_PPP1, Spear.REGULAR_PPP);
        interfaces.put(Items.MITHRIL_SPEAR_PPP1, Spear.REGULAR_PPP);
        interfaces.put(Items.ADAMANT_SPEAR_PPP1, Spear.REGULAR_PPP);
        interfaces.put(Items.RUNE_SPEAR_PPP1, Spear.REGULAR_PPP);
        interfaces.put(Items.DRAGON_SPEAR_PPP1, Spear.DRAGON_PPP);

        interfaces.put(Items.BRONZE_SPEAR_KP, Spear.REGULAR_KP);
        interfaces.put(Items.IRON_SPEAR_KP, Spear.REGULAR_KP);
        interfaces.put(Items.STEEL_SPEAR_KP, Spear.REGULAR_KP);
        interfaces.put(Items.BLACK_SPEAR_KP, Spear.REGULAR_KP);
        interfaces.put(Items.MITHRIL_SPEAR_KP, Spear.REGULAR_KP);
        interfaces.put(Items.ADAMANT_SPEAR_KP, Spear.REGULAR_KP);
        interfaces.put(Items.RUNE_SPEAR_KP, Spear.REGULAR_KP);
        interfaces.put(Items.DRAGON_SPEAR_KP, Spear.DRAGON_KP);

        interfaces.put(Items.BRONZE_SWORD1, Sword.REGULAR);
        interfaces.put(Items.IRON_SWORD1, Sword.REGULAR);
        interfaces.put(Items.STEEL_SWORD1, Sword.REGULAR);
        interfaces.put(Items.BLACK_SWORD1, Sword.REGULAR);
        interfaces.put(Items.WHITE_SWORD1, Sword.REGULAR);
        interfaces.put(Items.MITHRIL_SWORD1, Sword.REGULAR);
        interfaces.put(Items.ADAMANT_SWORD1, Sword.REGULAR);
        interfaces.put(Items.RUNE_SWORD1, Sword.REGULAR);

        interfaces.put(Items.BRONZE_2H_SWORD1, TwoHanded.REGULAR);
        interfaces.put(Items.IRON_2H_SWORD1, TwoHanded.REGULAR);
        interfaces.put(Items.STEEL_2H_SWORD1, TwoHanded.REGULAR);
        interfaces.put(Items.BLACK_2H_SWORD1, TwoHanded.REGULAR);
        interfaces.put(Items.WHITE_2H_SWORD1, TwoHanded.REGULAR);
        interfaces.put(Items.MITHRIL_2H_SWORD1, TwoHanded.REGULAR);
        interfaces.put(Items.ADAMANT_2H_SWORD1, TwoHanded.REGULAR);
        interfaces.put(Items.RUNE_2H_SWORD1, TwoHanded.REGULAR);
        interfaces.put(Items.DRAGON_2H_SWORD1, TwoHanded.DRAGON);

        interfaces.put(Items.BRONZE_WARHAMMER1, Warhammer.REGULAR);
        interfaces.put(Items.IRON_WARHAMMER1, Warhammer.REGULAR);
        interfaces.put(Items.STEEL_WARHAMMER1, Warhammer.REGULAR);
        interfaces.put(Items.BLACK_WARHAMMER1, Warhammer.REGULAR);
        interfaces.put(Items.WHITE_WARHAMMER1, Warhammer.REGULAR);
        interfaces.put(Items.MITHRIL_WARHAMMER1, Warhammer.REGULAR);
        interfaces.put(Items.ADAMANT_WARHAMMER1, Warhammer.REGULAR);
        interfaces.put(Items.RUNE_WARHAMMER1, Warhammer.REGULAR);
        interfaces.put(Items.STATIUSS_WARHAMMER1, Warhammer.STATIUS);

        interfaces.put(Items.ABYSSAL_WHIP1, Whip.ABYSSAL);
        interfaces.put(Items.ABYSSAL_WHIP6, Whip.ABYSSAL);
        interfaces.put(Items.ABYSSAL_WHIP7, Whip.ABYSSAL);
        interfaces.put(Items.ABYSSAL_WHIP8, Whip.ABYSSAL);

        interfaces.put(Items.SARADOMIN_SWORD1, TwoHanded.SARADOMIN_SWORD);

        interfaces.put(Items.ARMADYL_GODSWORD1, TwoHanded.ARMADYL_GODSWORD);
        interfaces.put(Items.BANDOS_GODSWORD1, TwoHanded.BANDOS_GODSWORD);
        interfaces.put(Items.SARADOMIN_GODSWORD1, TwoHanded.SARADOMIN_GODSWORD);
        interfaces.put(Items.ZAMORAK_GODSWORD1, TwoHanded.ZAMORAK_GODSWORD);

        interfaces.put(Items.SHORTBOW1, Shortbow.SHORTBOW);
        interfaces.put(Items.OAK_SHORTBOW1, Shortbow.OAK_SHORTBOW);
        interfaces.put(Items.WILLOW_SHORTBOW1, Shortbow.WILLOW_SHORTBOW);
        interfaces.put(Items.MAPLE_SHORTBOW1, Shortbow.MAPLE_SHORTBOW);
        interfaces.put(Items.YEW_SHORTBOW1, Shortbow.YEW_SHORTBOW);
        interfaces.put(Items.MAGIC_SHORTBOW1, Shortbow.MAGIC_SHORTBOW);

        interfaces.put(Items.LONGBOW1, Longbow.LONGBOW);
        interfaces.put(Items.OAK_LONGBOW1, Longbow.OAK_LONGBOW);
        interfaces.put(Items.WILLOW_LONGBOW1, Longbow.WILLOW_LONGBOW);
        interfaces.put(Items.MAPLE_LONGBOW1, Longbow.MAPLE_LONGBOW);
        interfaces.put(Items.YEW_LONGBOW1, Longbow.YEW_LONGBOW);
        interfaces.put(Items.MAGIC_LONGBOW1, Longbow.MAGIC_LONGBOW);
    }
}
