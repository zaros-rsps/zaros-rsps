package net.zaros.server.game.node.entity.areas.shape;

import net.zaros.server.game.node.Location;

/**
 * @author Walied K. Yassen
 */
public interface AreaShape {

	/**
	 * Checks whether or not the specified {@link Location point} intersects with
	 * this area or not.
	 * 
	 * @param point
	 *              the location point to check.
	 * @return <code>true</code> if it does otherwise <code>false</code>.
	 */
	default boolean intersects(Location point) {
		return intersects(point.getX(), point.getY());
	}

	boolean intersects(int x, int y);
}
