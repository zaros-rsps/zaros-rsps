package net.zaros.server.game.node.entity.areas.shape;

import net.zaros.server.game.node.Location;

/**
 * Represents a rectangular area shape.
 * 
 * @author Walied K. Yassen
 */
public final class AreaRectangle implements AreaShape {

	/**
	 * The minimum point x component.
	 */
	private final int min_x;

	/**
	 * The minimum point y component.
	 */
	private final int min_y;

	/**
	 * The maximum point x component.
	 */
	private final int max_x;

	/**
	 * The maximum point y component.
	 */
	private final int max_y;

	/**
	 * Constructs a new {@link AreaRectangle} type object instance.
	 * 
	 * @param min
	 *            the minimum location point.
	 * @param max
	 *            the maximum location point.
	 */
	public AreaRectangle(Location min, Location max) {
		this(min.getX(), min.getY(), max.getX(), max.getY());
	}

	/**
	 * Constructs a new {@link AreaRectangle} type object instance.
	 * 
	 * @param min_x
	 *              the minimum point x component.
	 * @param min_y
	 *              the minimum point y component.
	 * @param max_x
	 *              the maximum point x component.
	 * @param max_y
	 *              the maximum point y component.
	 */
	public AreaRectangle(int min_x, int min_y, int max_x, int max_y) {
		this.min_x = min_x;
		this.min_y = min_y;
		this.max_x = max_x;
		this.max_y = max_y;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.node.entity.areas.shape.AreaShape#intersects(int,
	 * int)
	 */
	@Override
	public boolean intersects(int x, int y) {
		return x >= min_x && y >= min_y && x <= max_x && y <= max_y;
	}
}
