package net.zaros.server.game.module.command.player;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.render.flag.impl.AppearanceUpdate;
import net.zaros.server.game.world.World;

import java.util.Optional;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/20/2017
 */
@CommandManifest(description = "Copies the gear of a player online.", types = { String.class })
public class CopyCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("copy");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		
		Optional<Player> optional = World.getPlayerByUsername(getCompleted(args, 1));
		if (!optional.isPresent()) {
			player.getTransmitter().sendMessage("Couldn't find player...");
			return;
		}
		Player target = optional.get();
		System.arraycopy(target.getInventory().getItems().toArray(), 0, player.getInventory().getItems().toArray(), 0, player.getInventory().getItems().toArray().length);
		System.arraycopy(target.getEquipment().getItems().toArray(), 0, player.getEquipment().getItems().toArray(), 0, player.getEquipment().getItems().toArray().length);
		player.getInventory().refresh();
		player.getEquipment().refreshAll();
		player.getSkills().passLevels(target);
		
		player.getSkills().updateAllSkills();
		player.getUpdateMasks().register(new AppearanceUpdate(player));
	}
}
