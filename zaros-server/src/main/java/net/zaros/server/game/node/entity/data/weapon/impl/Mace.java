package net.zaros.server.game.node.entity.data.weapon.impl;

import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;

import java.util.List;

/**
 * The interface for maces.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum Mace implements WeaponInterface {

    /**
     * The regular mace weapon interface.
     */
    REGULAR(),

    /**
     * The ancient mace weapon interface.
     */
    ANCIENT(),

    /**
     * The dragon mace weapon interface.
     */
    DRAGON();

    /**
     * The attack types.
     */
    private static final List<AttackType> attackTypes =
            List.of(AttackType.MACE_POUND, AttackType.MACE_PUMMEL, AttackType.MACE_SPIKE, AttackType.MACE_BLOCK);

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }
}
