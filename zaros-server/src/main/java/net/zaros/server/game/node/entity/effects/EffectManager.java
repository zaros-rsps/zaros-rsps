package net.zaros.server.game.node.entity.effects;

import lombok.Getter;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.tool.SecondsTimer;

import java.util.LinkedList;
import java.util.List;

/**
 * The manager for entity effects.
 *
 * @author Gabriel || Wolfsdarker
 */
public class EffectManager {

    /**
     * The entity who will have the effects attached to.
     */
    private Entity entity;

    /**
     * The list of current effects.
     */
    private final List<Effect> effects;

    /**
     * The freeze immunity timer.
     */
    @Getter
    private final SecondsTimer freezeImmunityTimer;

    /**
     * The stun immunity timer.
     */
    @Getter
    private final SecondsTimer stunImmunityTimer;

    /**
     * The poison immunity timer.
     */
    @Getter
    private final SecondsTimer poisonImmunityTimer;

    /**
     * Constructor for the effect manager.
     *
     * @param entity
     */
    public EffectManager(Entity entity) {
        this.entity = entity;
        this.effects = new LinkedList<>();
        this.freezeImmunityTimer = new SecondsTimer();
        this.poisonImmunityTimer = new SecondsTimer();
        this.stunImmunityTimer = new SecondsTimer();
    }

    /**
     * Returns the effect by searching its name.
     *
     * @param name
     * @return the effect
     */
    public Effect get(String name) {
        if (effects.isEmpty()) {
            return null;
        }

        for (Effect effect : effects) {
            if (effect.name().equalsIgnoreCase(name)) {
                return effect;
            }
        }
        return null;
    }


    /**
     * Returns if certain effect is listed in the effects list.
     *
     * @param name
     * @return if it is present
     */
    public boolean isEffectPresent(String name) {

        if (effects.isEmpty()) {
            return false;
        }

        for (Effect effect : effects) {
            if (effect.name().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns if the entity can walk based on its effects.
     *
     * @param sendMessage
     * @return if walking is possible
     */
    public boolean isWalkEnabled(boolean sendMessage) {

        if (effects.isEmpty()) {
            return true;
        }

        for (Effect effect : effects) {
            if (effect.walkPolicy() == WalkPolicy.NON_WALKABLE) {
                if (sendMessage && entity.isPlayer() && effect.nonWalkableMessage() != null) {
                    entity.toPlayer().getTransmitter().sendMessage(effect.nonWalkableMessage());
                }
                return false;
            }
        }
        return true;
    }

    /**
     * Applies a effect.
     *
     * @param effect
     * @return if it was applied
     */
    public boolean apply(Effect effect) {

        if (effect.immune(entity)) {
            return false;
        }

        Effect current = get(effect.name());

        if (current != null) {
            if (current.renewable()) {
                current.renew();
            }
        } else {
            effects.add(effect);
            effect.preExecution(false);
        }
        return true;
    }

    /**
     * Purges and removes a effect from the list.
     *
     * @param name
     * @return if it was purged
     */
    public boolean purge(String name) {

        if (effects.isEmpty()) {
            return false;
        }

        Effect purge = null;

        for (Effect effect : effects) {
            if (effect.name().equalsIgnoreCase(name)) {
                purge = effect;
            }
        }

        if (purge == null) {
            return false;
        }

        purge.end();
        effects.remove(purge);

        return true;
    }

    /**
     * Process all current effects.
     */
    public void processEffects() {
        if (effects.isEmpty()) {
            return;
        }
        for (Effect effect : effects) {
            effect.onTick();
            if (effect.duration() != Duration.PERMANENT && effect.over()) {
                effects.remove(effect);
            }
        }
    }

    /**
     * Removes all the non walkable effects.
     */
    public void purgeNonWalkableEffects() {
        for (Effect effect : effects) {
            if (effect.walkPolicy() == WalkPolicy.NON_WALKABLE) {
                effects.remove(effect);
            }
        }
    }

    /**
     * Removes all the temporary effects.
     */
    public void purgeTemporaryEffects() {
        for (Effect effect : effects) {
            if (effect.duration() == Duration.TEMPORARY) {
                effects.remove(effect);
            }
        }
    }

}
