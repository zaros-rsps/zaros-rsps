package net.zaros.server.game.content.perks;

import lombok.Getter;

public enum Perk {

    GRIMY_HERB_AUTO(new String[] { "grimy", "grimyherbs" }),

    ;

    @Getter
    private transient String[] aliases;

    Perk(String[] aliases) {
        this.aliases = aliases;
    }

    public static Perk getPerk(String alias) {
        for(Perk perk : Perk.values()) {
            for(String foundAlias : perk.getAliases())
                if(foundAlias.equalsIgnoreCase(alias))
                    return perk;
        }
        return null;
    }


}
