package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/27/2017
 */
public class OpenBankCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("bank");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		player.getBank().open();
	}
}
