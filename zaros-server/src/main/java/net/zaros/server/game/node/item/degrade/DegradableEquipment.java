package net.zaros.server.game.node.item.degrade;

import lombok.Getter;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.PlayerEquipment;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.constant.EquipConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public abstract class DegradableEquipment {

	@Getter
	private List<Integer> degradableItems = new ArrayList<>();

	@Getter
	private int slot;

	protected void register(int... ids) {
		Arrays.stream(ids).forEach(degradableItems::add);
	}

	public boolean degrade(Player player, boolean attack) {
		if (attack) {
			return degradable(player, EquipConstants.SLOT_WEAPON);
		}
		IntStream.range(0, 14)
					.filter(slot -> slot != EquipConstants.SLOT_WEAPON && slot != EquipConstants.SLOT_ARROWS)
					.forEachOrdered(slot -> degradable(player, slot));
		return true;// TODO
	}

	private boolean degradable(Player player, int slot) {
		PlayerEquipment equipment = player.getEquipment();
		Item item = equipment.getItem(slot);
		if (item == null) {
			return false;
		}
		for (int degradable : degradableItems) {
			if (degradable == item.getId()) {
				return degrade(player, item);
			}
		}
		return false;
	}

	/**
	 * Called when the player receives a hit.
	 *
	 * @param player
	 *               The player.
	 * @param item
	 *               The degrading equipment item.
	 */
	public abstract boolean degrade(Player player, Item item);

	/**
	 * Gets the item to drop when this equipment is getting dropped.
	 *
	 * @param itemId
	 *               The drop item.
	 * @return The item to drop.
	 */
	public abstract int getDegradedDropItem(int itemId);

	public Integer[] getItems() {
		return degradableItems.toArray(new Integer[degradableItems.size()]);
	}

}
