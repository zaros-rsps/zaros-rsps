package net.zaros.server.game.module.interaction.object;

import net.zaros.server.game.content.combat.player.registry.wrapper.magic.TeleportationSpellEvent;
import net.zaros.server.game.module.type.ObjectInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.Coordinates;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/9/2017
 */
public class EdgevilleLeverInteractionModule extends ObjectInteractionModule {

	public EdgevilleLeverInteractionModule() {
		register(1814, 1815);
	}

	@Override
	public boolean handle(Player player, GameObject object, InteractionOption option) {
		switch(object.getId()) {
			case 1814:
				TeleportationSpellEvent.sendLeverTeleport(player, Coordinates.DESERTED_KEEP);
				break;
			case 1815:
				TeleportationSpellEvent.sendLeverTeleport(player, Coordinates.EDGEVILLE_LEVER);
				break;
		}
		return true;
	}
}
