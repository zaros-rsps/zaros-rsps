package net.zaros.server.game.node.entity.data.weapon.impl;

import lombok.Getter;
import net.zaros.server.game.node.entity.data.weapon.Ammunition;
import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.game.node.entity.data.weapon.impl.ammunitions.Arrows;
import net.zaros.server.game.node.entity.data.weapon.impl.specials.Snapshot;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.Hit;

import java.util.ArrayList;
import java.util.List;

/**
 * The storage for shortbows.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum Shortbow implements WeaponInterface {

    /**
     * The regular shortbow weapon interface.
     */
    SHORTBOW(),

    /**
     * The oak shortbow weapon interface.
     */
    OAK_SHORTBOW(Items.STEEL_ARROW, Items.STEEL_ARROW_P, Items.STEEL_ARROW_PP, Items.STEEL_ARROW_PPP),

    /**
     * The willow shortbow weapon interface.
     */
    WILLOW_SHORTBOW(Items.STEEL_ARROW, Items.STEEL_ARROW_P, Items.STEEL_ARROW_PP, Items.STEEL_ARROW_PPP,
            Items.MITHRIL_ARROW1, Items.MITHRIL_ARROW_P, Items.MITHRIL_ARROW_PP, Items.MITHRIL_ARROW_PPP),

    /**
     * The maple shortbow weapon interface.
     */
    MAPLE_SHORTBOW(Items.STEEL_ARROW, Items.STEEL_ARROW_P, Items.STEEL_ARROW_PP, Items.STEEL_ARROW_PPP,
            Items.MITHRIL_ARROW1, Items.MITHRIL_ARROW_P, Items.MITHRIL_ARROW_PP, Items.MITHRIL_ARROW_PPP,
            Items.ADAMANT_ARROW, Items.ADAMANT_ARROW_P, Items.ADAMANT_ARROW_PP, Items.ADAMANT_ARROW_PPP),

    /**
     * The yew shortbow weapon interface.
     */
    YEW_SHORTBOW(Items.STEEL_ARROW, Items.STEEL_ARROW_P, Items.STEEL_ARROW_PP, Items.STEEL_ARROW_PPP,
            Items.MITHRIL_ARROW1, Items.MITHRIL_ARROW_P, Items.MITHRIL_ARROW_PP, Items.MITHRIL_ARROW_PPP,
            Items.ADAMANT_ARROW, Items.ADAMANT_ARROW_P, Items.ADAMANT_ARROW_PP, Items.ADAMANT_ARROW_PPP,
            Items.RUNE_ARROW, Items.RUNE_ARROW_P, Items.RUNE_ARROW_PP, Items.RUNE_ARROW_PPP),

    /**
     * The magic shortbow weapon interface.
     */
    MAGIC_SHORTBOW(Items.STEEL_ARROW, Items.STEEL_ARROW_P, Items.STEEL_ARROW_PP, Items.STEEL_ARROW_PPP,
            Items.MITHRIL_ARROW1, Items.MITHRIL_ARROW_P, Items.MITHRIL_ARROW_PP, Items.MITHRIL_ARROW_PPP,
            Items.ADAMANT_ARROW, Items.ADAMANT_ARROW_P, Items.ADAMANT_ARROW_PP, Items.ADAMANT_ARROW_PPP,
            Items.RUNE_ARROW, Items.RUNE_ARROW_P, Items.RUNE_ARROW_PP, Items.RUNE_ARROW_PPP, Items.BROAD_ARROW) {
        @Override
        public WeaponSpecial specialAttack() {
            return Snapshot.getInstance();
        }
    };

    /**
     * The list of compatible ammunition.
     */
    private List<Integer> compatibleAmmo = new ArrayList<>();

    /**
     * Constructor for the shortbow.
     *
     * @param compatibleAmmo
     */
    Shortbow(int... compatibleAmmo) {
        this.compatibleAmmo.add(Items.BRONZE_ARROW);
        this.compatibleAmmo.add(Items.BRONZE_ARROW_P);
        this.compatibleAmmo.add(Items.BRONZE_ARROW_PP);
        this.compatibleAmmo.add(Items.BRONZE_ARROW_PPP);
        this.compatibleAmmo.add(Items.IRON_ARROW);
        this.compatibleAmmo.add(Items.IRON_ARROW_P);
        this.compatibleAmmo.add(Items.IRON_ARROW_PP);
        this.compatibleAmmo.add(Items.IRON_ARROW_PPP);
        for (int arrow : compatibleAmmo) {
            this.compatibleAmmo.add(arrow);
        }
    }

    /**
     * The type of ammo the shortbow fires.
     */
    private static final Arrows ammo = new Arrows();

    /**
     * The attack types.
     */
    private static final List<AttackType> attackTypes =
            List.of(AttackType.SHORTBOW_ACCURATE, AttackType.SHORTBOW_RAPID, AttackType.SHORTBOW_RAPID, AttackType.SHORTBOW_LONGRANGE);

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.RANGE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }

    @Override
    public List<Integer> compatibleAmmunitionIDs() {
        return compatibleAmmo;
    }

    @Override
    public Ammunition ammunition() {
        return ammo;
    }
}
