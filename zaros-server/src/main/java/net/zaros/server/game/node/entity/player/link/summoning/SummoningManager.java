package net.zaros.server.game.node.entity.player.link.summoning;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.content.skills.summoning.FamiliarData;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.npc.extension.familiar.SummoningFamiliar;
import net.zaros.server.game.node.entity.npc.link.NPCFollowAction;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.game.world.region.RegionManager;
import net.zaros.server.network.world.packet.outgoing.impl.AccessMaskBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.CS2ConfigBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.CS2StringBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigPacketBuilder;
import net.zaros.server.utility.rs.GameTab;
import net.zaros.server.utility.rs.constant.NPCConstants;
import net.zaros.server.utility.tool.Misc;

public final class SummoningManager {

    public void create(Player player, SummoningFamiliar familiar) {
        if(!summon()) return;
        createOrb();
        createTab();
        refreshTab();
    }

    /**
     * Summons the familiar for the corresponding entity.
     * @return Whether or not the familiar was successfully spawned.
     */
    public boolean summon() {
        SummoningFamiliar npc = new SummoningFamiliar(FamiliarData.PACK_YAK, getPlayer().getLocation());
        setFamiliar(npc);
        if(!checkSummonLocation(getFamiliar())) {
            getPlayer().getTransmitter().sendMessage("Your familiar is to big to be summoning here.");
            return false;
        }
        npc.setLocation(getSummonLocation());
        npc.faceEntity(getPlayer());
        World.addNPC(npc);
        player.getTransmitter().sendMessage("Summoning npc: " + npc.getLocation() + ", " + npc.getDefinitions().getSize());
        NPCFollowAction action = new NPCFollowAction(getFamiliar().toNPC());
        action.setEntity(getPlayer());
        getFamiliar().getActions().startAction(action);
        npc.setWalkType(NPCConstants.NO_WALK);
        npc.sendGraphics(npc.getSize() > 1 ? 1315 : 1314);
        return true;
    }

    /**
     * Checks if there is enough room for the npc to spawn around the entity location.
     * @param npc
     * @return If there is room for the npc to spawn.
     */
    private boolean checkSummonLocation(SummoningFamiliar npc) {
        Location location = RegionManager.getOffsetLocation(getPlayer(), npc.getSize());
        if(location != null) setSummonLocation(location);
        return getSummonLocation() == null ? false : true;
    }

    /**
     * Calls the familiar to the players location.
     */
    public void call() {
        if(!checkSummonLocation(getFamiliar())) {
            getPlayer().getTransmitter().sendMessage("Your familiar is to big to be summoning here.");
            return;
        }
        getFamiliar().teleport(getSummonLocation());
        getFamiliar().setLocation(getSummonLocation());
        getFamiliar().faceEntity(getPlayer());
        getFamiliar().getActions().startAction(new NPCFollowAction(getFamiliar()));
       // getFamiliar().getFollowingManager().setPlayer(getPlayer());
        getFamiliar().setWalkType(NPCConstants.NO_WALK);
        getFamiliar().sendGraphics(getFamiliar().getSize() > 1 ? 1315 : 1314);
    }

    /**
     * Creates the summoning orb
     */
    private void createOrb() {
        displayOrbComponents();
        //getPlayer().getTransmitter().refreshSummoningOrbStatus();
    }

    /**
     * Displays the components of the summoning orb
     */
    private void displayOrbComponents() {
        getPlayer().getManager().getInterfaces().sendInterfaceComponentHidden(747, 8, false);
        getPlayer().getManager().getInterfaces().sendInterfaceComponentHidden(747, 9, false);
    }

    /**
     * Creates the summoning tab
     */
    private void createTab() {
        player.getTransmitter().send(new ConfigPacketBuilder(448, 12093).build(player));

        player.getTransmitter().send(new ConfigPacketBuilder(1160, 243269632).build(player));

        player.getTransmitter().send(new ConfigPacketBuilder(1175, 100 << 23).build(player));

        player.getTransmitter().send(new CS2StringBuilder(204, "Test").build(player));

        player.getTransmitter().send(new CS2StringBuilder(205, "Test").build(player));

        player.getTransmitter().send(new CS2ConfigBuilder(1436, 1).build(player));

        player.getTransmitter().send(new CS2ConfigBuilder(1000, 66).build(player));

        player.getTransmitter().send(new ConfigPacketBuilder(1171, 262144).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1171, 262144).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1176, 1920).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(2044, 0).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1801, 5990636).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1231, 0).build(player));
        //player.getTransmitter().send(new ConfigPacketBuilder(1160, 595968).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1175, 8388608).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1231, 595968).build(player));

        player.getTransmitter().send(new ConfigPacketBuilder(1174, 1).build(player));

        player.getManager().getInterfaces().sendTab(GameTab.SUMMONING, 662);

        player.getManager().getInterfaces().sendInterfaceComponentHidden(662, 44, true);
        player.getManager().getInterfaces().sendInterfaceComponentHidden(662, 45, true);
        player.getManager().getInterfaces().sendInterfaceComponentHidden(662, 46, true);
        player.getManager().getInterfaces().sendInterfaceComponentHidden(662, 47, true);
        player.getManager().getInterfaces().sendInterfaceComponentHidden(662, 48, true);
        player.getManager().getInterfaces().sendInterfaceComponentHidden(662, 71, false);
        player.getManager().getInterfaces().sendInterfaceComponentHidden(662, 71, false);


        player.getTransmitter().send(new ConfigPacketBuilder(823, 1).build(player));

        player.getTransmitter().send(new ConfigPacketBuilder(168, 8).build(player));

        player.getTransmitter().send(new AccessMaskBuilder(662, 74, 0, 0, 0, 0).build(player));
        player.getTransmitter().send(new AccessMaskBuilder(747, 17, 0, 1, 20480).build(player));
        player.getTransmitter().send(new AccessMaskBuilder(662, 74, 0, 1, 20480).build(player));

        player.getTransmitter().send(new CS2ConfigBuilder(1436, 0).build(player));

        player.getTransmitter().send(new CS2StringBuilder(204, "").build(player));

        player.getTransmitter().send(new CS2StringBuilder(205, "").build(player));
    }

    public void randomShout() {
        int random = Misc.random(1000);
        if(random % 50 == 0) {
            getFamiliar().sendForcedChat(FamiliarData.PACK_YAK.getShout());
        }
    }

    public void refreshTab() {
/*

        player.getTransmitter().send(new ConfigPacketBuilder(448, 12093).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1174, 6874).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1175, 100 << 23).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1175, 100 << 23).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1171, 262144).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1171, 262144).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1176, 768).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(2044, 0).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1801, 5990631).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1878, 0).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1231, 595968).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1160, 8388608).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1175, 100 << 23).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1175, 100 << 23).build(player));
        player.getTransmitter().send(new ConfigPacketBuilder(1160, 243269632).build(player)); // ?
*/

       // player.getManager().getInterfaces().sendTab(GameTab.SUMMONING, 662);
        // owner.getIOSession().write(new InterfacePacket(owner, res ? 746 : 548, res ? 104 : 219, 662,true));
/*
        player.getTransmitter().send(new AccessMaskBuilder(747, 17, 0, 0, 20480).build(player));
        player.getTransmitter().send(new AccessMaskBuilder(662, 74, 0, 0, 20480).build(player));

       // player.getTransmitter().send(new CS2ConfigBuilder(1436, 1).build(player));
        player.getTransmitter().send(new CS2ConfigBuilder(1000, 66).build(player));

        player.getTransmitter().send(new ConfigPacketBuilder(1160, 8388608).build(player)); // ?
        player.getTransmitter().send(new ConfigPacketBuilder(1175, 100 << 23).build(player)); // ?
        player.getTransmitter().send(new ConfigPacketBuilder(1175, 100 << 23).build(player)); // ?
        player.getTransmitter().send(new ConfigPacketBuilder(1160, 243269632).build(player)); // ?

       // player.getManager().getInterfaces().sendTab(GameTab.SUMMONING, 662);


        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 44, true);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 45, true);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 46, true);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 47, true);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 48, true);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 71, false);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 71, false);


        player.getTransmitter().send(new AccessMaskBuilder(747, 17, 0, 1, 20480).build(player));
        player.getTransmitter().send(new AccessMaskBuilder(662, 74, 0, 1, 20480).build(player));

       // player.getTransmitter().send(new CS2ConfigBuilder(1436, 0).build(player));

        player.getTransmitter().send(new CS2StringBuilder(204, "").build(player));

        player.getTransmitter().send(new CS2StringBuilder(205, "").build(player));

        player.getTransmitter().send(new CS2ConfigBuilder(168, 8).build(player));


        //player.getTransmitter().send(new ConfigPacketBuilder(1174, 1).build(player));

       // player.getManager().getInterfaces().sendTab(GameTab.SUMMONING, 662);

        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 44, true);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 45, true);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 46, true);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 47, true);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 48, true);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 71, false);
        player.getManager().getInterfaces().sendInterfaceComponentChange(662, 71, false);

        player.getTransmitter().send(new ConfigPacketBuilder(168, 8).build(player));

        player.getManager().getInterfaces().sendInterfaceComponentChange(747, 9, false);

        player.getManager().getInterfaces().sendInterfaceComponentChange(747, 8, false);

        player.getTransmitter().send(new AccessMaskBuilder(662, 74, 0, 0, 0, 0).build(player));*/

    }

    public void destructFamiliar() {
        getFamiliar().fireDeathEvent();
    }

    @Getter
    @Setter
    private transient Player player;

    @Getter
    @Setter
    private SummoningFamiliar familiar;

    @Getter
    @Setter
    private transient Location summonLocation;

    public boolean hasFamiliar() { return getFamiliar() != null; }

}
