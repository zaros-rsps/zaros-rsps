package net.zaros.server.game.content.combat.data.magic.modernspells;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitModifier;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;

import java.util.List;

/**
 * The water surge spell on the regular spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class WaterSurge extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 87;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.AIR_RUNE1, 7), new Item(Items.WATER_RUNE1, 10), new Item(Items.BLOOD_RUNE1), new Item(Items.DEATH_RUNE1));

    @Override
    public void playGraphics(Entity source, Entity target) {
        source.sendGraphics(2702);
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(14220);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
        ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(source, target, 2707, 30, 26, 52, 0, 0));
    }

    @Override
    public HitModifier getModifier(Entity source, Entity target) {
        return null;
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return null;
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(2712);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int autocastConfigID() {
        return 49;
    }

    @Override
    public int magicLevelRequirement() {
        return 85;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 220;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 46.5;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.REGULAR;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }
}
