package net.zaros.server.game.module.interaction.rsinterface;

import java.util.Arrays;
import java.util.List;

import net.zaros.cache.type.npctype.NPCDefinition;
import net.zaros.cache.type.npctype.NPCDefinitionParser;
import net.zaros.server.game.module.type.InterfaceInteractionModule;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.link.InterfaceManager;
import net.zaros.server.game.node.item.Drop;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.network.world.Transmitter;
import net.zaros.server.network.world.packet.outgoing.impl.AccessMaskBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.CS2ScriptBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.ContainerPacketBuilder;
import net.zaros.server.utility.rs.constant.Directions;

public class NPCDropInterfaceInteractionModule implements InterfaceInteractionModule {

    private static int npc;

    /**
     * The id of the supplies interface
     */
    public static final int INTERFACE_ID = 860;

    @Override
    public int[] interfaceSubscriptionIds() {
        return arguments(INTERFACE_ID);
    }

    @Override
    public boolean handle(Player player, int interfaceId, int componentId, int itemId, int slotId, int packetId) {
        return true;
    }

    /**
     * Displays the supplies interface
     *
     * @param player player
     */
    public static void display(Player player, int npc) {

        NPCDropInterfaceInteractionModule.npc = npc;

        int[] hideComponents = {20, 21, 26};
        int titleLine = 18;
        int descriptionLine = 19;
        int itemsKey = 91;

        NPCDefinition definition = NPCDefinitionParser.forId(npc);

        NPC n = new NPC(definition.getId(), player.getLocation(), Directions.Direction.NORTH);

        List<Drop> dropList = n.getCharacteristics().getDrops();

        dropList.addAll(n.getCharacteristics().getCharmDrops());

        Item[] itemArray = new Item[dropList.size()];

        int index = 0;

        for(Drop drop : dropList) {
            itemArray[index] = new Item(drop.getItemId(), drop.getMaxAmount());
            index++;
        }

        InterfaceManager interfaces = player.getManager().getInterfaces();
        Transmitter transmitter = player.getTransmitter();

        Arrays.stream(hideComponents).forEach(value -> interfaces.sendInterfaceComponentHidden(INTERFACE_ID, value, true));

        transmitter.send(new ContainerPacketBuilder(itemsKey, itemArray).build(player));
        transmitter.send(new AccessMaskBuilder(INTERFACE_ID, 23, 0, itemArray.length, 0, 1, 2, 3, 4, 5, 6).build(player));
        transmitter.send(CS2ScriptBuilder.getInterfaceUnlockScript(INTERFACE_ID, 23, 91, 8, 150, "Rarity", "Kill-1", "Kill-10", "Kill-100", "Kill-X").build(player));

        interfaces.sendInterfaceText(INTERFACE_ID, titleLine, "Npc drop table for: " + definition.getName());
        interfaces.sendInterfaceText(INTERFACE_ID, descriptionLine, "Drops received by the specified npc.");
        interfaces.sendInterface(INTERFACE_ID, true);
    }

}
