package net.zaros.server.game.content.combat.npc.swing;

import net.zaros.server.game.content.combat.npc.NPCCombatSwing;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.npc.data.NPCCombatDefinitions;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.tool.RandomFunction;

public class DragonCombatSwing implements NPCCombatSwing {

    @Override
    public int attack(NPC npc, Entity target) {
        NPCCombatDefinitions defs = npc.getCombatDefinitions();
        Player player = target.isPlayer() ? target.toPlayer() : null;
        int attack_style = RandomFunction.random(5);
        int npc_size = npc.getSize();
        if (attack_style == 0) {
            // basic melee attack chance 20%
            // check if we are not beneath it basically and not behind it, we need to be
            // in-front of it to hit us using melee.
            int distance_x = target.getLocation().getX() - npc.getLocation().getX();
            int distance_y = target.getLocation().getY() - npc.getLocation().getY();
            if (distance_x > npc_size || distance_x < -1 || distance_y > npc_size || distance_y < -1) {
                // NOOP
            } else {
                delayHit(npc, 0, target, constructMeleeHit(npc, randomMaxHit(npc, defs.getMaxHit(), MELEE_COMBAT_STYLE, target)));
                npc.sendAnimation(defs.getAttackGfx());
            }
            return defs.getAttackDelay();
        } else if (attack_style == 1 || attack_style == 2) {
            // basic magic attack, chance 40%
            int damage = randomMaxHit(npc, defs.getMaxHit(), MAGIC_COMBAT_STYLE, target);
            // clip the damage to 25.
            if (damage > 35) {
                damage = 35;
            }
            delayHit(npc, 0, target, constructMagicHit(npc, damage));
            return defs.getAttackDelay();
        } else if (attack_style == 3) {
            // special range attack
            // damage is used in random and not in randomMaxHit due to that we always need
            // to hit a random amount between 0 and 60, no matter
            // what the player is wearing or anything, it is a special attack.
            int damage = RandomFunction.getRandom(60);
            // perform our special attack animation.
            npc.sendAnimation(81);
            // 2 under there is when the animation attack phase begins, like we send an
            // animation and it needs 2 ticks of performing
            // to reach the attack thingy.
            delayHit(npc, 2, target, constructRangeHit(npc, damage));
        }
        return defs.getAttackDelay();
    }

    @Override
    public Object[] bindings() {
        return new Object[] { 50 };
    }
}