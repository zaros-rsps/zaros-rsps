package net.zaros.server.game.content.combat.player.registry.spell.modern.curses;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.player.registry.wrapper.context.CombatSpellContext;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.CombatSpellEvent;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.constant.MagicConstants.MagicBook;
import net.zaros.server.utility.rs.constant.SkillConstants;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/7/2017
 */
public class VulernabilityEvent implements CombatSpellEvent {
	
	@Override
	public int delay(Player player) {
		return 5;
	}
	
	@Override
	public int animationId() {
		return 729;
	}
	
	@Override
	public int hitGfx() {
		return 169;
	}
	
	@Override
	public int maxHit(Player player, Entity target) {
		return 10;
	}
	
	@Override
	public int spellId() {
		return 75;
	}
	
	@Override
	public double exp() {
		return 76;
	}
	
	@Override
	public MagicBook book() {
		return MagicBook.REGULAR;
	}
	
	@Override
	public void cast(Player player, CombatSpellContext context) {
		player.sendGraphics(169);
		ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(player, context.getTarget(), 168, 30, 26, 52, 0, 0));
		context.getSwing().sendSpell(player, context.getTarget(), this, () -> {
			Entity target = context.getTarget();
			if (target.isPlayer()) {
				target.toPlayer().getSkills().drainLevel(SkillConstants.DEFENCE, 0.10, 0.10);
			}
		}, null);
	}
}
