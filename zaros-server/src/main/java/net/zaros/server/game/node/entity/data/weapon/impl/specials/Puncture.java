package net.zaros.server.game.node.entity.data.weapon.impl.specials;

import lombok.Getter;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.content.combat.script.impl.specials.PunctureCombatScript;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;

public class Puncture implements WeaponSpecial {

    /**
     * The instance's combat script.
     */
    private static final PunctureCombatScript script = new PunctureCombatScript();

    /**
     * The instance's instance.
     */
    @Getter
    private static final Puncture instance = new Puncture();

    @Override
    public int attackAnimation() {
        return 1062;
    }

    @Override
    public int attackGraphics() {
        return 252;
    }

    @Override
    public int targetGraphics() {
        return 0;
    }

    @Override
    public int attackSpeed() {
        return 4;
    }

    @Override
    public int specialRequired() {
        return 25;
    }

    @Override
    public Hit.HitSplat hitStyle() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public CombatScript combatScript() {
        return script;
    }
}
