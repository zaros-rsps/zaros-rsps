package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.CS2ConfigBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/8/2017
 */
@CommandManifest(description = "Sends a CS2 Config", types = { Integer.class, Integer.class })
public class SendCS2ConfigCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("sendcs2config");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		player.getTransmitter().send(new CS2ConfigBuilder(intParam(args, 1), intParam(args, 2)).build(player));
	}
}
