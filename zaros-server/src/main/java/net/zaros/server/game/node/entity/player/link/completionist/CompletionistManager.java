package net.zaros.server.game.node.entity.player.link.completionist;

import java.util.Arrays;

import lombok.Getter;
import lombok.Setter;
import net.zaros.cache.type.objtype.ItemDefinitionParser;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.render.flag.impl.AppearanceUpdate;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigFilePacketBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigPacketBuilder;

public class CompletionistManager {

    public void start(Player player, int itemId) {
        cape = itemId;
        int[] colors = cape == 20767 ? originalMaxCapeColors : originalCompletionistCapeColors;
        player.getManager().getInterfaces().sendInterface(20, false);
        for (int i = 0; i < 4; i++) {
			player.getTransmitter().send(new ConfigFilePacketBuilder(9254 + i, colors[i]).build(player));
		}
        boolean male = player.getDetails().getAppearance().isMale();
        int modelId = male ? ItemDefinitionParser.forId(itemId).getMaleWornModelId1() : ItemDefinitionParser.forId(itemId).getFemaleWornModelId1();
        //player.getManager().getInterfaces().sendInterfaceModel(20, 55, modelId);
    }

    public void handleInterface(Player player, int interfaceId, int componentId, int cape) {
        int[] colors = cape == 20767 ? originalMaxCapeColors : originalCompletionistCapeColors;
        switch(interfaceId) {
            case 20:
            switch (componentId) {
                case 34:
                    capeIndex = 0;
                    player.getManager().getInterfaces().sendInterface(19, true);
                    player.getTransmitter().send(new ConfigPacketBuilder(2174, colors[0]).build(player));
                    break;
                case 71:
                    capeIndex = 1;
                    player.getManager().getInterfaces().sendInterface(19, true);
                    player.getTransmitter().send(new ConfigPacketBuilder(2174, colors[1]).build(player));
                    break;
                case 83:
                    capeIndex = 2;
                    player.getManager().getInterfaces().sendInterface(19, true);
                    player.getTransmitter().send(new ConfigPacketBuilder(2174, colors[2]).build(player));
                    break;
                case 95:
                    capeIndex = 3;
                    player.getManager().getInterfaces().sendInterface(19, true);
                    player.getTransmitter().send(new ConfigPacketBuilder(2174, colors[3]).build(player));
                    break;
                case 114:
                case 142:
                    player.getUpdateMasks().register(new AppearanceUpdate(player));
                    player.getManager().getInterfaces().closeScreenInterface();
                    break;
            }
            break;
            case 19:
                switch(componentId) {
                    case 20:
                        player.getManager().getInterfaces().sendInterface(20, true);
                        break;
                }
                break;
        }
    }

    public void customizeColor(Player player, int color) {
        colors[capeIndex] = color;
        player.getTransmitter().send(new ConfigFilePacketBuilder(9254 + capeIndex, color).build(player));
        player.getManager().getInterfaces().sendInterface(20, true);
        if(cape == 20771 || cape == 20767) {
			setModifiedCompletionistCapeColors(colors);
		} else {
			setModifiedMaxCapeColors(colors);
		}
    }

    private transient int cape;//transient doesnt save the var, everything else saves.. so

    private transient int capeIndex;

    private transient int[] colors = new int[4];

    private transient int[] originalMaxCapeColors = Arrays.copyOf(ItemDefinitionParser.forId(20767).getOriginalModelColors(),4);

    private transient int[] originalCompletionistCapeColors = Arrays.copyOf(ItemDefinitionParser.forId(20769).getOriginalModelColors(),4);

    @Getter
    @Setter
    private int[] modifiedMaxCapeColors = originalMaxCapeColors;

    @Getter
    @Setter
    private int[] modifiedCompletionistCapeColors = originalCompletionistCapeColors;

    public int[] getModifiedMaxCapeColors() { return modifiedMaxCapeColors; }

    public int[] getModifiedCompletionistCapeColors() { return modifiedCompletionistCapeColors; }

}
