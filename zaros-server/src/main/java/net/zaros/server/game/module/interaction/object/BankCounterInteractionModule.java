package net.zaros.server.game.module.interaction.object;

import net.zaros.server.game.module.type.ObjectInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/1/2017
 */
public class BankCounterInteractionModule extends ObjectInteractionModule {
	
	public BankCounterInteractionModule() {
		register("Bank booth");
		registerOption("Use", "Use-quickly", "Collect");
	}

	@Override
	public boolean handle(Player player, GameObject object, InteractionOption option) {
		player.getBank().open();
		return true;
	}
}
