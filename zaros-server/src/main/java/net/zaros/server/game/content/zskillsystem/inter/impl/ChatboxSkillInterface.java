package net.zaros.server.game.content.zskillsystem.inter.impl;

import net.zaros.server.game.content.zskillsystem.inter.AbstractSkillInterface;
import net.zaros.server.game.content.zskillsystem.segment.AbstractSkillSegment;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Jacob Rhiel
 */
public class ChatboxSkillInterface extends AbstractSkillInterface {

    public ChatboxSkillInterface(AbstractSkillSegment segment) {
        super(segment);
    }

    @Override
    public boolean constructSkillInterface(Player player) {
        return false;
    }

    @Override
    public boolean handleInterfaceAction(Player player, int button) {
        return false;
    }

    @Override
    public boolean handleSkillInterfaceClose(Player player) {
        return false;
    }

}
