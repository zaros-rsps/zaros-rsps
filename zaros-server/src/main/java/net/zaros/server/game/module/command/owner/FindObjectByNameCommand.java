package net.zaros.server.game.module.command.owner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.zaros.cache.Cache;
import net.zaros.cache.type.loctype.ObjectDefinition;
import net.zaros.cache.type.loctype.ObjectDefinitionParser;
import net.zaros.cache.util.Utils;
import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.tool.ColorConstants;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/2/2017
 */
@CommandManifest(description = "Finds an object by the entered name", types = { String.class })
public class FindObjectByNameCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("findobject", "objn");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		// the identifier to search by
		final String identifier = args[1].replaceAll("_", " ");
		// the option flag
		final String optionFlag = stringParamOrDefault(args, 2, null);
		// the list of items that were found
		List<String> found = new ArrayList<>();
		for (int objectId = 0; objectId < Utils.getObjectDefinitionsSize(Cache.getStore()); objectId++) {
			// the definition instance
			ObjectDefinition definition = ObjectDefinitionParser.forId(objectId);
			if (definition == null) {
				System.out.println("NPC #" + objectId + " had no definitions.");
				continue;
			}
			// the name of the item
			final String name = definition.getName().toLowerCase();
			boolean added = false;
			// the name has the identifier we want
			if (name.contains(identifier)) {
				// we only want to find items by the identifier
				if (optionFlag == null) {
					added = true;
				} else {
					// we found that the definition has the option we want
					if (definition.hasOption(optionFlag)) {
						added = true;
					}
				}
			}
			// the item was not added because it was not found by filter
			if (!added) {
				continue;
			}
			found.add("[<col=FF0000>" + objectId + "</col>] <col=" + ColorConstants.LIGHT_BLUE + ">" + definition.getName() + "</col> options=" + Arrays.toString(definition.getOptions()) + " sizeX=[" + definition.getSizeX() + "], sizeY=[" + definition.getSizeY() + "]");
		}
		// shows the entries found
		found.forEach(entry -> player.getTransmitter().sendConsoleMessage(entry));
		// sends a response with the amount of entries found
		sendResponse(player, "Found " + found.size() + " objects by name '" + identifier + "'.", console);
	}
}
