package net.zaros.server.game.content.system.zskillsystem.farming.impl;

import com.ahoypk.definitions.constant.Items;
import com.ahoypk.world.entity.impl.player.Player;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * The states of disease for plants.
 */
public enum DiseaseState {

	NOT_PRESENT,

	PRESENT,

	IMMUNE;


	/**
	 * The seeds that are immune to disease when a specific flower is planted near them.
	 */
	private static Map<Integer, Integer> flower_immunity = new HashMap<>();

	static {
		flower_immunity.put(Items.POTATO_SEED, Items.MARIGOLD_SEED);
		flower_immunity.put(Items.ONION_SEED, Items.MARIGOLD_SEED);
		flower_immunity.put(Items.TOMATO_SEED, Items.MARIGOLD_SEED);
		flower_immunity.put(Items.CABBAGE_SEED, Items.ROSEMARY_SEED);
		flower_immunity.put(Items.WATERMELON_SEED, Items.NASTURTIUM_SEED);
	}

	/**
	 * Checks if the seed to be planted is immune.
	 *
	 * @param player
	 * @param seed_id
	 * @param data
	 * @return
	 */
	public static DiseaseState getImmunity(Player player, int seed_id, Patches data) {

		Integer value = flower_immunity.get(seed_id);

		if (value == null) {
			return DiseaseState.NOT_PRESENT;
		}

		Patches patch_data = Arrays.stream(Patches.values()).filter(patch -> patch.getPatchType() ==
			FarmingPatchType.FLOWER_PATCH).filter(patch ->
			patch.getAllotmentPosition()[0].getDistance(data.getAllotmentPosition()[0]) <= 30).findAny().orElse(null);

		if (patch_data != null) {

			PatchState state = player.getFarming().getPatchStates().get(patch_data.name());

			if (state != null && state.isUsed() && state.getSeed().getSeedItemId() == value.intValue()) {
				return DiseaseState.IMMUNE;
			}
		}

		return DiseaseState.NOT_PRESENT;
	}
}
