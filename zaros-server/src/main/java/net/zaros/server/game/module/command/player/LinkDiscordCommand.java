package net.zaros.server.game.module.command.player;

import net.zaros.discord.DiscordManager;
import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Walied K. Yassen
 */
@CommandManifest(description = "Links your in-game account with your Discord account.")
public final class LinkDiscordCommand extends CommandModule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.module.command.CommandModule#identifiers()
	 */
	@Override
	public String[] identifiers() {
		return arguments("link");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.module.command.CommandModule#handle(net.zaros.server.
	 * game.node.entity.player.Player, java.lang.String[], boolean)
	 */
	@Override
	public void handle(Player player, String[] args, boolean console) {
		var username = player.getDetails().getUsername();
		if (player.hasLinkedDiscord()) {
			player.getTransmitter().sendMessage("<col=ee0000>Your account has already been linked to a Discord account.");
			return;
		}
		var linking = DiscordManager.getBot().getLinking();
		var code = linking.retrieve(username);
		if (code != null) {
			player.getTransmitter().sendMessage("<col=00ff55>You already have a pending linking request, your code is: <col=ff5500>" + code + "<col=ee0000>.</col>");
		} else {
			code = linking.request(username);
			player.getTransmitter().sendMessage("<col=00ff55>You have requested an authentication code for linking your Discord account..");
			player.getTransmitter().sendMessage("<col=00ff55>Your authentication code is: <col=ff5500>" + code + "<col=00ff55>.</col>");
		}
		player.getTransmitter().sendMessage("<col=00ff55>Use <col=005fff>::link <col=ff5500>" + code + "<col=00ff55> in <col=005fff>#bot-commands<col=00ff55> at our Discord server to link your account.");
	}

}
