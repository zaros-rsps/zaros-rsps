package net.zaros.server.game.content.system.zskillsystem.farming.actions;

import net.zaros.cache.parse.ItemDefinitionParser;
import net.zaros.server.game.content.system.zskillsystem.farming.compostbin.CompostBin;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.CompostType;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;
import net.zaros.server.game.node.item.Item;

/**
 * Handles the action of collecting the products from composting.
 *
 * @author Gabriel || Wolfsdarker
 */
public class CompostBinCollectAction extends PlayerTask {

	/**
	 * Constructor for compost bin collect action.
	 *
	 * @param player
	 * @param bin
	 */
	public CompostBinCollectAction(Player player, CompostBin bin) {
		super(2, player, () -> {

			if (bin.getInventory().isEmpty() || player.getInventory().isFull() ||
				(bin.getCompostType() != CompostType.ROTTEN_TOMATOES && !player.getInventory().contains(Items.BUCKET))) {
				player.endCurrentTask();
				return;
			}

			player.sendAnimation(new Animation(832));

			bin.getInventory().remove(new Item(bin.getCompostType().getOutcomeItemID()));

			player.getInventory().remove(Items.BUCKET, 1);
			player.getInventory().addItem(new Item(bin.getCompostType().getOutcomeItemID()));

			if (bin.getCompostType() == CompostType.ROTTEN_TOMATOES) {
				player.getTransmitter().sendMessage("You collect a rotten tomato.");
			} else {
				player.getTransmitter().sendMessage("You fill the bucket with " + ItemDefinitionParser.forId(bin.getCompostType().getOutcomeItemID()).getName().toLowerCase() + ".");
			}

			player.getFarming().getCompostManager().updateBin(player, bin);
		});

		onEnd(() -> {
			player.sendAnimation(new Animation(65535));
			player.getMovementQueue().reset();

			if (bin.getInventory().isEmpty()) {
				bin.reset();
			}

		});

		stopUpon(MovementPacketListener.class);
	}
}
