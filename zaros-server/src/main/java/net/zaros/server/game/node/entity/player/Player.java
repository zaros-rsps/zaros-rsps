package net.zaros.server.game.node.entity.player;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.zaros.server.Configuration;
import net.zaros.server.core.SequencialUpdate;
import net.zaros.server.core.event.player.PlayerLoginEventPost;
import net.zaros.server.core.newtask.TaskManager;
import net.zaros.server.core.service.ServiceManager;
import net.zaros.server.core.system.SystemManager;
import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.GameFlags;
import net.zaros.server.game.clan.manager.ClanManager;
import net.zaros.server.game.content.action.interaction.PlayerCombatAction;
import net.zaros.server.game.content.activity.impl.pvp.WildernessActivity;
import net.zaros.server.game.content.combat.CombatManager;
import net.zaros.server.game.content.perks.PlayerPerk;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.NodeInteractionTask;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.areas.AreaManager;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.effects.impl.FreezeEffect;
import net.zaros.server.game.node.entity.npc.render.NPCRendering;
import net.zaros.server.game.node.entity.player.data.PlayerBank;
import net.zaros.server.game.node.entity.player.data.PlayerCombatDefinitions;
import net.zaros.server.game.node.entity.player.data.PlayerDetails;
import net.zaros.server.game.node.entity.player.data.PlayerEquipment;
import net.zaros.server.game.node.entity.player.data.PlayerInventory;
import net.zaros.server.game.node.entity.player.data.PlayerVariables;
import net.zaros.server.game.node.entity.player.data.RenderInformation;
import net.zaros.server.game.node.entity.player.link.prayer.Prayer;
import net.zaros.server.game.node.entity.player.render.PlayerRendering;
import net.zaros.server.game.node.entity.player.render.flag.impl.AppearanceUpdate;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.world.World;
import net.zaros.server.game.world.region.RegionManager;
import net.zaros.server.network.lobby.packet.outgoing.LobbyResponseBuilder;
import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.master.client.packet.out.PlayerFilePacketOut;
import net.zaros.server.network.master.client.packet.out.PlayerLoginCompletePacketOut;
import net.zaros.server.network.world.Transmitter;
import net.zaros.server.network.world.WorldSession;
import net.zaros.server.network.world.packet.outgoing.impl.CS2ConfigBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.DynamicMapRegionBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.LoginCredentialsBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.MapRegionBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.PlayerOptionPacketBuilder;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.newrepository.database.index.IndexTables;
import net.zaros.server.utility.newrepository.database.index.impl.UniqueIndexTable.UniqueDomain;
import net.zaros.server.utility.newrepository.database.players.PlayerRepository;
import net.zaros.server.utility.rs.Hit;
import net.zaros.server.utility.rs.Hit.HitSplat;
import net.zaros.server.utility.rs.constant.HeadIcons.SkullIcon;
import net.zaros.server.utility.rs.constant.SkillConstants;

/**
 * The player that renderable in the game.
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/18/2017
 */
@Slf4j
public final class Player extends Entity {

	/**
	 * The player unique user id.
	 */
	@Getter
	private final long userId;

	/**
	 * The credentials of the player
	 */
	@Getter
	private final PlayerDetails details;

	/**
	 * The inventory of the player
	 */
	@Getter
	private final PlayerInventory inventory;

	/**
	 * The equipment of the player
	 */
	@Getter
	private final PlayerEquipment equipment;

	/**
	 * The bank of the player
	 */
	@Getter
	private final PlayerBank bank;

	/**
	 * The perks of the player
	 */
	@Getter
	private final PlayerPerk perks;

	/**
	 * The manager instance
	 */
	@Getter
	private final PlayerManager manager;

	/**
	 * The variables of the player that are saved
	 */
	@Getter
	private final PlayerVariables variables;

	/**
	 * The combat definitions of the entity. These are saved
	 */
	@Getter
	private PlayerCombatDefinitions combatDefinitions;

	/**
	 * The networkSession attached to the player
	 */
	@Getter
	@Setter
	private transient WorldSession session;

	/**
	 * The network transmitter object
	 */
	@Getter
	private transient Transmitter transmitter;

	/**
	 * The render information object
	 */
	@Getter
	private transient RenderInformation renderInformation;

	/**
	 * The path event
	 */
	@Getter
	@Setter
	private transient NodeInteractionTask interactionTask;

	/**
	 * The dialouge waiting ticks.
	 */
	@Getter
	@Setter
	private transient int dialogueWaitTicks;

	/**
	 * The Fist of Guthix minigame rating.
	 */
	@Getter
	private int fistOfGuthixRating;

	/**
	 * The linked Discord user id.
	 */
	@Getter
	@Setter
	private long discordId;

	/**
	 * The current clan id.
	 */
	@Getter
	@Setter
	private long currentClanId;

	/**
	 * Whether or not the player has joined the clan channel.
	 */
	@Getter
	@Setter
	private boolean joinedClanChannel;

	/**
	 * Constructs a new {@link Player} type object instance.
	 * 
	 * @param username
	 */
	public Player(String username) {
		super(Configuration.World.SPAWN_LOCATION);
		details = new PlayerDetails(username);
		equipment = new PlayerEquipment();
		inventory = new PlayerInventory();
		variables = new PlayerVariables();
		manager = new PlayerManager();
		bank = new PlayerBank();
		perks = new PlayerPerk();
		userId = IndexTables.getUnique().generateId(UniqueDomain.PLAYER);
	}

	public void register() {
		registerTransients();
		World.getPlayers().add(this);
		if (session.isInLobby()) {
			session.write(new LobbyResponseBuilder().build(this));
		} else {
			session.write(new LoginCredentialsBuilder().build(this));
			loadMapRegions();
			setRenderable(true);
		}
		ServiceManager.getPlayerInitialisationService().enqueue(this);
	}

	public void initialise() {
		if (session.isInLobby()) {
			manager.getWebManager().handleLogin();
		} else {
			transmitter.sendLoginComponents();
			getUpdateMasks().register(new AppearanceUpdate(this));
			SequencialUpdate.getRenderablePlayers().add(this);
			RegionManager.updateEntityRegion(this);
			World.getEventManager().post(new PlayerLoginEventPost(this));
			AreaManager.tick(this);
			getArea().loginHook(this);
			putTemporaryAttribute(AttributeKey.WEAPON, WeaponInterface.get(this));
		}
		// removes the black screen for one tick
		session.flushPackets();
		MasterCommunication.write(new PlayerLoginCompletePacketOut(this));
		manager.getContacts().sendLogin();
		manager.getContacts().pushLoginStatusChange();
		ClanManager.requestInitialise(this);
	}

	public void deregister() {
		getArea().logoutHook(this);
		manager.getActivities().logout();
		save();
		setRenderable(false);
		if (joinedClanChannel) {
			ClanManager.requestChannelLeave(this, currentClanId);
		}
		// manager.getContacts().sendMyStatusChange(false);
		session.notifyDisconnection(GameFlags.worldId);
		World.removePlayer(this);
		RegionManager.updateEntityRegion(this);
		SequencialUpdate.getRenderablePlayers().remove(this);
		log.debug("Unregistered player: {}", details.getDisplayName());

	}

	/**
	 * De-registers a player from the lobby
	 */
	public void deregisterLobby() {
		session.notifyDisconnection(GameFlags.worldId);
		if (joinedClanChannel) {
			ClanManager.requestChannelLeave(this, currentClanId);
		}
		setRenderable(false);
		World.removePlayer(this);
		log.debug("Unregistered player: {} from game to lobby.", details.getDisplayName());
	}

	@Override
	public Player toPlayer() {
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (userId ^ userId >>> 32);
		return result;
	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public int getMaxHealth() {
		return getSkills().getLevelForXp(SkillConstants.HITPOINTS) * 10 + equipment.getMaxHealthBoost();
	}

	@Override
	public int getPrayerPoints() {
		return getVariables().getPrayerPoints();
	}

	@Override
	public void setPrayerPoints(int prayerPoints) {
		getVariables().setPrayerPoints(prayerPoints);
	}

	@Override
	public void tick() {
		if (isDead() || isDying()) {
			return;
		}
		super.tick();

		// Process effects TODO: effects
		// getEffectManager().updateEffects();

		checkInteractionTask();
		getRegion().increaseTimeSpent(this);
		manager.getActions().process();
		manager.getHintIcons().process();
		manager.getActivities().process();
		processSkull();
		if (dialogueWaitTicks > 0) {
			dialogueWaitTicks--;
			if (dialogueWaitTicks < 1) {
				dialogueWaitTicks = -1;
				manager.getDialogues().handleOption(-1, -1);
			}
		}
		// refreshTimers();
	}

	@Override
	public String toString() {
		return "[username=" + details.getUsername() + ", loc=" + getLocation() + ", right=" + details.getDominantRight() + "]";
	}

	/**
	 * Generates the transient objects (objects which will not save)
	 */
	@Override
	public void registerTransients() {
		super.registerTransients();

		manager.registerTransients(this);
		transmitter = new Transmitter(this);
		renderInformation = new RenderInformation(this);

		// actual player objects
		equipment.setPlayer(this);
		inventory.setPlayer(this);
		bank.setPlayer(this);
		perks.setPlayer(this);
		combatDefinitions = new PlayerCombatDefinitions(this);
	}

	@Override
	public void loadMapRegions() {
		if (!session.isActive()) {
			return;
		}
		boolean wasAtDynamicRegion = isAtDynamicRegion();
		super.loadMapRegions();
		if (isAtDynamicRegion()) {
			transmitter.send(new DynamicMapRegionBuilder().build(this));
			if (!wasAtDynamicRegion) {
				getRenderInformation().getLocalNpcs().clear();
			}
		} else {
			transmitter.send(new MapRegionBuilder(!isRenderable()).build(this));
			if (wasAtDynamicRegion) {
				getRenderInformation().getLocalNpcs().clear();
			}
		}
		removeTemporaryAttribute(AttributeKey.FORCE_NEXT_MAP_LOAD);
	}

	@Override
	public boolean restoreHitPoints() {
		boolean update = super.restoreHitPoints();
		if (update) {
			if (getPrayers().prayerOn(Prayer.RAPID_HEAL)) {
				super.restoreHitPoints();
			}
			if (getTemporaryAttribute("resting", false)) {
				super.restoreHitPoints();
			}
			transmitter.refreshHealthPoints();
		}
		return update;
	}

	/**
	 * Gets the amount of health points we have
	 */
	@Override
	public int getHealthPoints() {
		return variables.getHealthPoints();
	}

	@Override
	public void setHealthPoints(int healthPoints) {
		variables.setHealthPoints(healthPoints);
		transmitter.refreshHealthPoints();
	}

	@Override
	public void receiveHit(Hit hit) {
		// only hitsplats we care about are combat ones
		// if (!hit.getSplat().isDefaultCombatSplat()) {
		// return;
		// }
		if (hit.getSource() != null) {
			CombatManager.autoRetaliate(hit.getSource(), this);
		}
		// adjust hit so we don't hit too high
		if (hit.getDamage() > getHealthPoints()) {
			hit.setDamage(getHealthPoints());
		}
		// prayers handle the hit first
		getPrayers().handleHit(hit);
		// absorption after prayer so the actual hit isn't affected
		equipment.handleAbsorption(hit);
		// the player is unhittable so we don't do this
		if (!getTemporaryAttribute("unhittable", false)) {
			// if we should die after damage lands
			if (variables.reduceHealth(hit.getDamage())) {
				checkDeathEvent();
			}
		}
		// we don't remove the attribute when the hit is received in case the damage < 4
		if (getTemporaryAttribute("cast_veng", false) && hit.getDamage() >= 4) {
			removeTemporaryAttribute("cast_veng");
			sendForcedChat("Taste vengeance!");
			hit.getSource().getHitQueue().add(new Hit(this, (int) Math.floor(hit.getDamage() * 0.75), HitSplat.REGULAR_DAMAGE));
		}
		// the hit is no longer modifiable, the actual damage received will be stored
		// now.
		transmitter.refreshHealthPoints();
	}

	@Override
	public boolean fighting() {
		return manager.getActions().getAction() instanceof PlayerCombatAction;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.node.entity.Entity#teleport(org.redrune.game.node.Location)
	 */
	@Override
	public void teleport(Location destination) {
		if (!session.isActive()) {
			setLocation(destination);
		} else {
			super.teleport(destination);
		}
	}

	/**
	 * Fire this later
	 */
	@Override
	public void fireDeathEvent() {
		// if the activity should handle death instead
		if (manager.getActivities().handlePreDeath(this)) {
			return;
		}
		// vars
		final Player player = this;
		final Entity killer = getHitMap().getMostDamageEntity();

		// we can't walk anymore
		getMovement().resetWalkSteps();
		// we can't do anything while dying.
		manager.getLocks().lockAll();
		// the event
		SystemManager.getScheduler().schedule(new ScheduledTask(1, 6) {
			@Override
			public void run() {
				if (getTicksPassed() == 1) {
					sendAnimation(836);
				} else if (getTicksPassed() == 2) {
					transmitter.sendMessage("Oh dear, you have died.");
					/*
					 * if (source instanceof Player) { Player killer = (Player) source;
					 * killer.setAttackedByDelay(4); }
					 */
				} else if (getTicksPassed() == 5) {
					if (manager.getActivities().handlePostDeath(Player.this)) {
						stop();
						return;
					} else {
						Item[] kept = WildernessActivity.sendDeathContainer(player, killer);
						player.equipment.getItems().clear();
						player.inventory.getItems().clear();
						player.equipment.sendContainer();
						player.inventory.sendContainer();
						player.restoreAll();

						teleport(Configuration.World.DEATH_LOCATION);
						sendAnimation(-1);

						// add the items we should've kept to our inventory...
						if (kept != null) {
							for (Item item : kept) {
								player.inventory.addItem(item.getId(), item.getAmount());
							}
						}
					}
				} else if (getTicksPassed() == 6) {
					// TODO: getPackets().sendMusicEffect(90);
				}
			}
		});
	}

	@Override
	public void restoreAll() {
		getSkills().restoreAll();
		manager.getHintIcons().removeAll();
		manager.getActions().stopAction();
		TaskManager.getTickScheduler().cancelAll();
		variables.setRunEnergy(100);
		setAttackedBy(null);
		removeSkull();
		removeTemporaryAttribute("dying");

		getCombatDefinitions().setSpecialEnergy(100);
		getCombatDefinitions().setSpecialActivated(false);
		getCombatDefinitions().resetSpells(true);
		getEffectManager().purgeNonWalkableEffects();
		getEffectManager().purgeTemporaryEffects();
		transmitter.sendSettings();

		getUpdateMasks().register(new AppearanceUpdate(this));
		manager.getLocks().unlockAll();
	}

	public void sendMultiArea() {
		transmitter.send(new CS2ConfigBuilder(616, getArea().isMulti() ? 1 : 0).build(this));
	}

	/**
	 * Checks if the interaction task should be removed, after processing it.
	 */
	public void checkInteractionTask() {
		if (interactionTask != null && interactionTask.process(this)) {
			setInteractionTask(null);
		}
	}

	/**
	 * Processes the skull
	 */
	private void processSkull() {
		// we must have a skull
		if (variables.getSkullIconTimer() == -1L) {
			return;
		}
		// skull timer has lapsed so we reset it
		if (System.currentTimeMillis() > variables.getSkullIconTimer()) {
			removeSkull();
		}
	}

	/**
	 * Refreshes the onscreen timers
	 */
	public void refreshTimers() {
		Long lastTimeCast = getTemporaryAttribute("LAST_VENG", -1L);
		boolean showVeng = lastTimeCast != -1 && lastTimeCast + 30_000 > System.currentTimeMillis();
		boolean showFreeze = getEffectManager().isEffectPresent(FreezeEffect.getName());
		int interfaceId = 614;
		int combatOverlayInterface = manager.getInterfaces().getCombatOverlayInterface();
		if (showVeng || showFreeze) {
			if (combatOverlayInterface == -1) {
				manager.getInterfaces().sendCombatOverlay(interfaceId);
				for (int i = 3; i <= 6; i++) {
					manager.getInterfaces().sendInterfaceComponentHidden(interfaceId, i, true);
				}
				manager.getInterfaces().sendInterfaceText(interfaceId, 7, "");
				manager.getInterfaces().sendInterfaceText(interfaceId, 8, "");
			}
		} else {
			if (combatOverlayInterface != -1) {
				manager.getInterfaces().closeCombatOverlay();
			}
		}
		if (combatOverlayInterface == interfaceId) {
			if (showVeng) {
				long difference = lastTimeCast + 30_000 - System.currentTimeMillis();
				long seconds = TimeUnit.MILLISECONDS.toSeconds(difference);

				manager.getInterfaces().sendInterfaceComponentHidden(interfaceId, 5, false);
				manager.getInterfaces().sendInterfaceText(interfaceId, 8, "" + seconds);
			} else {
				manager.getInterfaces().sendInterfaceComponentHidden(interfaceId, 5, true);
				manager.getInterfaces().sendInterfaceText(interfaceId, 8, "");
			}
			if (showFreeze) {
				long difference = 0;
				long seconds = TimeUnit.MILLISECONDS.toSeconds(difference);

				manager.getInterfaces().sendInterfaceComponentHidden(interfaceId, 4, false);
				manager.getInterfaces().sendInterfaceText(interfaceId, 7, "" + seconds);
			} else {

				manager.getInterfaces().sendInterfaceComponentHidden(interfaceId, 4, true);
				manager.getInterfaces().sendInterfaceText(interfaceId, 7, "");
			}
		}
	}

	/**
	 * Removes the skull
	 */
	private void removeSkull() {
		variables.setSkullIcon(this, SkullIcon.NONE);
		variables.setSkullIconTimer(-1L);
	}

	/**
	 * Calls the termination of a player to start, using an attempt-based system
	 */
	public void terminate() {
		terminate(0);
	}

	/**
	 * Terminates the player, using an attempt-based system
	 *
	 * @param attempt
	 *                The attempt
	 */
	private void terminate(final int attempt) {
		if ((isDead() || isDying() || isUnderCombat()) && attempt < 6) {
			SystemManager.getScheduler().schedule(new ScheduledTask(16) {
				@Override
				public void run() {
					terminate(attempt + 1);
					stop();
				}
			});
			return;
		}
		deregister();
	}

	/**
	 * Sends the updating required
	 */
	public void sendUpdating() {
		PlayerRendering playerRendering = getTemporaryAttribute("player_rendering", null);
		NPCRendering npcRendering = getTemporaryAttribute("npc_rendering", null);
		if (playerRendering == null) {
			putTemporaryAttribute("player_rendering", playerRendering = new PlayerRendering());
		}
		if (npcRendering == null) {
			putTemporaryAttribute("npc_rendering", npcRendering = new NPCRendering());
		}
		transmitter.send(playerRendering.build(this));
		transmitter.send(npcRendering.build(this));
	}

	/**
	 * Stops specified events
	 *
	 * @param actions
	 *                   If we should stop actions
	 * @param travel
	 *                   If we should stop travels
	 * @param interfaces
	 *                   If we should stop interfaces
	 * @param animations
	 *                   If we should stop animations
	 */
	public void stop(boolean actions, boolean travel, boolean interfaces, boolean animations) {
		if (animations) {
			sendAnimation(-1);
		}
		if (interfaces) {
			manager.getInterfaces().closeAll();
		}
		if (travel) {
			setInteractionTask(null);
			getMovement().resetWalkSteps();
			transmitter.sendMinimapFlagReset();
		}
		if (actions) {
			getManager().getActions().stopAction();
		}
		TaskManager.getTickScheduler().cancelAll();
		// faceEntity(null);
	}

	/**
	 * Restores the run energy by 1.
	 */
	public void restoreRunEnergy() {
		if (getMovement().getNextRunDirection() != -1 || getVariables().getRunEnergy() >= 100) {
			return;
		}
		getVariables().setRunEnergy(getVariables().getRunEnergy() + 1);
		transmitter.refreshEnergy();
		transmitter.refreshRunOrbStatus();
	}

	/**
	 * Updates the fight area flag
	 */
	public void setInFightArea(boolean inFightArea) {
		variables.setInFightArea(inFightArea);
		session.write(new PlayerOptionPacketBuilder(inFightArea ? "Attack" : "null", true, 1).build(this));
		// TODO: getPackets().sendPlayerUnderNPCPriority(inFightArea);
	}

	/**
	 * This method finds all the items that the player contains on them.
	 */
	public CopyOnWriteArrayList<Item> findContainedItems() {
		CopyOnWriteArrayList<Item> containedItems = new CopyOnWriteArrayList<>();
		for (int i = 0; i < 14; i++) {
			final Item item = equipment.getItem(i);
			if (item != null && item.getId() != -1 && item.getAmount() != -1) {
				containedItems.add(new Item(item.getId(), item.getAmount()));
			}
		}
		for (int i = 0; i < 28; i++) {
			final Item item = inventory.getItems().get(i);
			if (item != null && item.getId() != -1 && item.getAmount() != -1) {
				containedItems.add(new Item(item.getId(), item.getAmount()));
			}
		}
		return containedItems;
	}

	/**
	 * Saves the player
	 */
	public void save() {
		MasterCommunication.write(new PlayerFilePacketOut(details.getUsername(), PlayerRepository.serialise(this)));
	}

	/**
	 * Handles the logging out of a player
	 *
	 * @param lobby
	 *              If the player should be sent to the lobby
	 */
	public void logout(boolean lobby) {
		long currentTime = System.currentTimeMillis();
		if (getAttackedByDelay() + 10000 > currentTime) {
			transmitter.sendUnrepeatingMessages("You can't log out until 10 seconds after the end of combat.");
			return;
		}
		if (!getArea().isLogoutAllowed(this)) {
			return;
		}
		if (manager.getActivities().handlePreDisconnect()) {
			return;
		}
		manager.getDialogues().end();
		transmitter.sendLogout(lobby);
	}

	/**
	 * Sets the skull for the duration
	 *
	 * @param icon
	 *              The icon
	 * @param delay
	 *              The delay
	 */
	public void setSkull(SkullIcon icon, long delay) {
		variables.setSkullIcon(this, icon);
		variables.setSkullIconTimer(System.currentTimeMillis() + delay);
	}

	/**
	 * Sets data for the first time an account is made
	 */
	public void setCreationData() {
		manager.getActivities().setDefaultActivity();
		bank.setDefaultBank();
	}

	/**
	 * @param fistOfGuthixRating
	 *                           the fistOfGuthixRating to set
	 */
	public void setFistOfGuthixRating(int fistOfGuthixRating) {
		if (fistOfGuthixRating < 0) {
			fistOfGuthixRating = 0;
		}
		this.fistOfGuthixRating = fistOfGuthixRating;

	}

	@Override
	public int[] getBonuses() {
		return equipment.getBonuses();
	}

	public boolean hasLinkedDiscord() {
		return discordId > 0;
	}

	public boolean hasClan() {
		return currentClanId > 0;
	}

}