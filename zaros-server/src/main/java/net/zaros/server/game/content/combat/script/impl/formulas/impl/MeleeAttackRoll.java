package net.zaros.server.game.content.combat.script.impl.formulas.impl;

import net.zaros.server.game.content.combat.StaticCombatFormulae;
import net.zaros.server.game.content.combat.script.impl.formulas.CombatFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.constant.BonusConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

public class MeleeAttackRoll implements CombatFormula {

    @Override
    public int get(Entity attacker, Entity target, boolean specialAttack) {

        var attackLevel = attacker.getSkills().getLevel(SkillConstants.ATTACK) * 1.0;

        final double prayerBoost = attacker.getPrayers().getBasePrayerBoost(SkillConstants.ATTACK);
        attackLevel *= prayerBoost;

        attackLevel = Math.round(attackLevel);

        final var style = attacker.isPlayer() ? attacker.toPlayer().getCombatDefinitions().getAttackStyle() :
                attacker.toNPC().getCombatDefinitions().getAttackStyle();

        if (style == 0) {
            attackLevel += 3;
        }

        if (style == 2) {
            attackLevel = 1;
        }

        attackLevel += 8;

        if (attacker.isPlayer() && StaticCombatFormulae.fullVoidEquipped(attacker.toPlayer(), 11665)) {
            attackLevel *= 1.10;
        }

        var attackBonus = 0.0;

        switch (style) {
            case BonusConstants.STAB_ATTACK:
                attackBonus = attacker.getBonuses()[BonusConstants.STAB_ATTACK];
                break;
            case BonusConstants.SLASH_ATTACK:
                attackBonus = attacker.getBonuses()[BonusConstants.SLASH_ATTACK];
                break;
            case BonusConstants.CRUSH_ATTACK:
                attackBonus = attacker.getBonuses()[BonusConstants.CRUSH_ATTACK];
                break;
        }

        attackLevel = Math.round(attackLevel);
        attackBonus = Math.round(attackBonus);

        var attackRoll = Math.round(attackLevel * (64 + attackBonus)) * 1.0;

        if (attacker.isPlayer() && attacker.toPlayer().getCombatDefinitions().isSpecialActivated()) {

            int weaponId = attacker.toPlayer().getEquipment().getWeaponId();

            switch (weaponId) {
                case 11694:
                    attackRoll *= 2.0;
                    break;
            }
        }

        attackRoll = Math.round(attackRoll);

        return (int) attackRoll;
    }
}
