package net.zaros.server.game.content.combat.data.magic.modernspells;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitModifier;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;

import java.util.List;

/**
 * The wind wave spell on the regular spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class WindWave extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 70;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.AIR_RUNE1, 5), new Item(Items.BLOOD_RUNE1));

    @Override
    public void playGraphics(Entity source, Entity target) {
        source.sendGraphics(457);
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(14221);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
        ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(source, target, 461, 30, 26, 52, 0, 0));
    }

    @Override
    public HitModifier getModifier(Entity source, Entity target) {
        return null;
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return null;
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(2700);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int autocastConfigID() {
        return 27;
    }

    @Override
    public int magicLevelRequirement() {
        return 62;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 170;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 36;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.REGULAR;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }
}
