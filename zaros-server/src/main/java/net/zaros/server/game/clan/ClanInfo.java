package net.zaros.server.game.clan;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents basic information about clan, used in the player profile to make
 * things go faster and fast-failing support. These are not stored anywhere on
 * the disk, it will be transmitted over the network from the actual clan
 * profile when the user is connecting. Represents the clan info.
 * 
 * @author Walied K. Yassen
 */
@AllArgsConstructor
public final class ClanInfo {

	/**
	 * The clan name.
	 */
	@Getter
	@Setter
	private String name;

	/**
	 * The required talk rank.
	 */
	@Getter
	@Setter
	private ClanRank talkRank;

	/**
	 * The required kick rank.
	 */
	@Getter
	@Setter
	private ClanRank kickRank;
}
