package net.zaros.server.game.plugin.java;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.zaros.server.game.plugin.IPluginManager;
import net.zaros.server.game.world.World;

/**
 * Represents the Java plugin implementation of the {@link IPluginManager}
 * interface.
 * 
 * @author Walied K. Yassen
 * @since 0.1
 */
public final class PluginManager implements IPluginManager<Plugin> {

	/**
	 * The {@link Logger} instance of the {@link PluginManager} class type.
	 */
	private static final Logger logger = LoggerFactory.getLogger(PluginManager.class);

	/**
	 * The plugins list.
	 */
	private final Map<String, Plugin> plugins = new LinkedHashMap<String, Plugin>();

	/**
	 * The plugin class loader.
	 */
	private final PluginLoader loader = new PluginLoader();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.plugin.IPluginManager#register(net.zaros.server.game.
	 * plugin.IPlugin)
	 */
	@Override
	public void register(Plugin plugin) {
		if (plugins.containsValue(plugin)) {
			throw new IllegalArgumentException("The specified plugin is already registered to this manager.");
		}
		// check whether or not we need to generate a manifest for the specified plugin
		// the generated manifest should never be dependent on, it is for development
		// purposes
		// only.
		if (plugin.getManifest() == null) {
			plugin.setManifest(new PluginManifest(plugin.getClass().getName(), plugin.getClass().getSimpleName(), "An auto generated manifest.", Collections.emptySet(), plugin.getClass().getName()));
		}
		// register the plugin in our plug-ins list.
		plugins.put(plugin.getManifest().getId(), plugin);
		// enable the plug-in.
		enablePlugin(plugin);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.plugin.IPluginManager#unregister(net.zaros.server.game.
	 * plugin.IPlugin)
	 */
	@Override
	public void unregister(Plugin plugin) {
		if (!plugins.containsValue(plugin)) {
			throw new IllegalArgumentException("The specified plugin is not registered to this manager.");
		}
		if (plugin.isEnabled()) {
			disablePlugin(plugin);
		}
		plugins.remove(plugin.getManifest().getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.praesul.core.plugin.IPluginManager#loadAll(java.io.File)
	 */
	@Override
	public void loadAll(File directory) {
		var numLoaded = loadDirectory(directory);
		logger.info("Loaded {} plugins..", numLoaded);
	}

	/**
	 * Loads the plugin(s) in the specified {@code directory}.
	 * 
	 * @param directory
	 *                  the directory to load the plugins from.
	 * @return the amount of plugins that were loaded.
	 */
	private int loadDirectory(File directory) {
		int numLoaded = 0;
		for (File file : directory.listFiles()) {
			if (file.isDirectory()) {
				numLoaded += loadDirectory(file);
			} else if (file.getName().endsWith(".jar")) {
				load(file);
				numLoaded++;
			}
		}
		return numLoaded;
	}

	/**
	 * Loads the plugin from the specified {@link File} which leads to a
	 * {@code .JAR} file.
	 * 
	 * @param file
	 *             the {@code .JAR} file of the plugin.
	 */
	private void load(File file) {
		// attempt to load the plugin from the file.
		var plugin = loader.load(file);
		register(plugin);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPluginManager#unloadAll()
	 */
	@Override
	public void unloadAll() {
		for (Plugin plugin : getPlugins()) {
			unload(plugin);
		}
	}

	/**
	 * Unloads the specified {@link Plugin} from our system.
	 * 
	 * @param plugin
	 *               the plugin to unload.
	 */
	private void unload(Plugin plugin) {
		unregister(plugin);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPluginManager#enableAll()
	 */
	@Override
	public void enableAll() {
		for (Plugin plugin : getPlugins()) {
			enablePlugin(plugin);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.runekit.api.plugin.IPluginManager#enablePlugin(org.runekit.api.plugin.
	 * IPlugin)
	 */
	@Override
	public void enablePlugin(Plugin plugin) {
		// check whether the plugin is already enabled or not.
		if (plugin.isEnabled()) {
			return;
		}
		// register the plugin event.
		World.getEventManager().register(plugin.getEventsKey(), plugin);
		// fires the plugin enable event.
		plugin.onEnable();
		// mark the plugin as enabled.
		plugin.setEnabled(true);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPluginManager#disableAll()
	 */
	@Override
	public void disableAll() {
		for (Plugin plugin : getPlugins()) {
			disablePlugin(plugin);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.runekit.api.plugin.IPluginManager#disablePlugin(org.runekit.api.plugin.
	 * IPlugin)
	 */
	@Override
	public void disablePlugin(Plugin plugin) {
		// check whether if the plugin is alreay dsiabled or not.
		if (!plugin.isEnabled()) {
			return;
		}
		// unregister all the plugin events.
		World.getEventManager().unregister(plugin.getEventsKey());
		// fire the plugin disable event.
		plugin.onDisable();
		// mark the plugin as disabled.
		plugin.setEnabled(false);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPluginManager#getPlugin(java.lang.String)
	 */
	@Override
	public Plugin getPlugin(String id) {
		return plugins.get(id);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPluginManager#getPlugins()
	 */
	@Override
	public Collection<Plugin> getPlugins() {
		return Collections.unmodifiableCollection(plugins.values());
	}

}
