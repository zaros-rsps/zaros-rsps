package net.zaros.server.game.module.command.server_moderator;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.render.flag.impl.AppearanceUpdate;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/31/2017
 */
@CommandManifest(description = "Transforms you into an npc", types = { Integer.class })
public class PNPCCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("pnpc");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		player.getDetails().getAppearance().setNpcId(intParam(args, 1));
		player.getUpdateMasks().register(new AppearanceUpdate(player));
	}
}
