package net.zaros.server.game.node.entity.render.flag.impl;

import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.Rectangle;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.UpdateFlag;
import net.zaros.server.network.world.packet.PacketBuilder;

/**
 * Represents the face location update mask.
 *
 * @author Walied K. Yassen
 */
public class FaceLocationUpdate extends UpdateFlag {

	/**
	 * Whether this is an NPC or not.
	 */
	private final boolean npc;

	/**
	 * The facing rectangle.
	 */
	private final Rectangle rectangle;

	/**
	 * The direction to face.
	 */
	private final int direction;

	/**
	 * Construct a new {@link FaceLocationUpdate} type object instance.
	 * 
	 * @param entity
	 * @param base
	 * @param sizeX
	 * @param sizeY
	 */
	public FaceLocationUpdate(Entity entity, Location base, int sizeX, int sizeY) {
		this.npc = entity.isNPC();
		rectangle = new Rectangle(base.getX(), base.getY(), sizeX, sizeY);
		Location from = entity.getLocation();
		int srcX = from.getX() * 512 + entity.getSize() * 256;
		int srcY = from.getY() * 512 + entity.getSize() * 256;
		int dstX = base.getX() * 512 + sizeX * 256;
		int dstY = base.getY() * 512 + sizeY * 256;
		int deltaX = srcX - dstX;
		int deltaY = srcY - dstY;
		if (deltaX != 0 || deltaY != 0) {
			direction = (int) (Math.atan2(deltaX, deltaY) * 2607.5945876176133) & 0x3FFF;
		} else {
			direction = 0;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.node.entity.render.flag.UpdateFlag#write(org.redrune.game.
	 * node.entity.player.Player, org.redrune.network.world.packet.PacketBuilder)
	 */
	@Override
	public void write(Player outgoing, PacketBuilder bldr) {
		if (npc) {
			bldr.writeLEShortA(rectangle.getX() * 2 + rectangle.getSizeX());
			bldr.writeLEShortA(rectangle.getY() * 2 + rectangle.getSizeY());
		} else {
			bldr.writeLEShortA(direction);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.node.entity.render.flag.UpdateFlag#getOrdinal()
	 */
	@Override
	public int getOrdinal() {
		return npc ? 13 : 14;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.node.entity.render.flag.UpdateFlag#getMaskData()
	 */
	@Override
	public int getMaskData() {
		return 0x20;
	}
}