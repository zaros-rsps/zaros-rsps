package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;


public class TestSegmentCommand extends CommandModule {

    @Override
    public String[] identifiers() {
        return arguments("segment");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {
		player.getManager().getInterfaces().sendInterface(1143, false);
		player.getTransmitter().sendVarp(intParam(args, 1), intParam(args, 2));
    }

    @Override
    public boolean consoleUsageOnly() {
        return true;
    }
}
