package net.zaros.server.game.node.entity.player.data;

import lombok.Getter;
import lombok.Setter;
import net.zaros.cache.type.ConfigLoader;
import net.zaros.cache.util.Utils;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.ItemsContainer;
import net.zaros.server.network.world.packet.outgoing.impl.AccessMaskBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.ContainerPacketBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.ContainerUpdateBuilder;
import net.zaros.server.utility.newrepository.items.ItemRepository;
import net.zaros.server.utility.rs.constant.InterfaceConstants;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/27/2017
 */
public class PlayerInventory {

	/**
	 * The items in the container
	 */
	@Getter
	private ItemsContainer<Item> items;

	/**
	 * The player
	 */
	@Setter
	private transient Player player;

	/**
	 * The weight of the items carried in the inventory
	 */
	@Getter
	@Setter
	private transient double weight;

	public PlayerInventory() {
		items = new ItemsContainer<>(28, false);
	}

	/**
	 * Initializes the inventory container
	 */
	public void initialize() {
		player.getTransmitter().send(new AccessMaskBuilder(InterfaceConstants.INVENTORY_INTERFACE_ID, 0, 0, 27, 4554126).build(player));
		player.getTransmitter().send(new AccessMaskBuilder(InterfaceConstants.INVENTORY_INTERFACE_ID, 0, 28, 55, 2097152).build(player));
		sendContainer();
	}

	/**
	 * Sends the container items
	 */
	public void sendContainer() {
		player.getTransmitter().send(new ContainerPacketBuilder(93, items.toArray(), false).build(player));
		calculateWeight();
	}

	/**
	 * Recalculates the total weight of items in the inventory
	 */
	private void calculateWeight() {
		double weight = 0;
		for (Item item : items.toArray()) {
			if (item == null) {
				continue;
			}
			weight += ItemRepository.lookup(item.getId()).getWeight();
		}
		this.weight = weight;
		player.getTransmitter().sendWeight();
	}

	/**
	 * Adds an item to the container
	 *
	 * @param item
	 *             The item instance
	 */
	public boolean addItem(Item item) {
		if (item.getId() < 0 || item.getAmount() < 0 || item.getId() > Utils.getItemDefinitionsSize(ConfigLoader.store)) {
			return false;
		}
		Item[] itemsBefore = items.getItemsCopy();
		if (!items.add(item)) {
			items.add(new Item(item.getId(), items.getFreeSlots()));
			player.getTransmitter().sendMessage("Not enough space in your inventory.", true);
			refreshItems(itemsBefore);
			return false;
		}
		refreshItems(itemsBefore);
		return true;
	}

	/**
	 * Adds an item to the container
	 *
	 * @param itemId
	 *               The id of the item
	 * @param amount
	 *               The amount of the item
	 */
	public boolean addItem(int itemId, int amount) {
		return addItem(new Item(itemId, amount));
	}

	/**
	 * Refreshes the items
	 *
	 * @param itemsBefore
	 *                    The items array
	 */
	private void refreshItems(Item[] itemsBefore) {
		int[] changedSlots = new int[itemsBefore.length];
		int count = 0;
		for (int index = 0; index < itemsBefore.length; index++) {
			if (itemsBefore[index] != items.getItems()[index]) {
				changedSlots[count++] = index;
			}
		}
		int[] finalChangedSlots = new int[count];
		System.arraycopy(changedSlots, 0, finalChangedSlots, 0, count);
		refresh(finalChangedSlots);
	}

	/**
	 * Gets the maximum of two items.
	 * @param item1
	 * @param item2
	 * @return
	 */
	public int maximumQuantityOf(int item1, int item2) {
		return Math.max(items.getNumberOf(item1), items.getNumberOf(item2));
	}

	/**
	 * Gets the minimum of two items.
	 * @param item1
	 * @param item2
	 * @return
	 */
	public int minimumQuanitityOf(int item1, int item2) {
		return Math.min(items.getNumberOf(item1), items.getNumberOf(item2));
	}

	/**
	 * Replaces item in a slot
	 * 
	 * @param slot
	 * @param itemId
	 * @param amount
	 * @return
	 */
	public void replace(int slot, int itemId, int amount) {
		items.replace(slot, itemId, amount);
		refreshAll();
	}

	/**
	 * Replaces the first of occurrence of the item
	 * 
	 * @param itemId
	 *                      The item id to find
	 * @param itemReplaceId
	 *                      The item to replace with
	 * @param amount
	 *                      The amount to replace with
	 */
	public void replaceFirstOccurence(int itemId, int itemReplaceId, int amount) {
		items.replace(getItems().getThisItemSlot(new Item(itemId)), itemReplaceId, amount);
		refreshAll();
	}

	/**
	 * Refreshes the items in the slots
	 *
	 * @param slots
	 *              The slots
	 */
	public void refresh(int... slots) {
		player.getTransmitter().send(new ContainerUpdateBuilder(93, items.toArray(), slots).build(player));
		calculateWeight();
	}

	/**
	 * Refreshes all the items
	 */
	public void refreshAll() {
		refresh(items.toArray().length);
	}

	public boolean hasSpaceFor(Item item) {
		return item.getAmount() <= getItems().freeSlots();
	}

	public boolean remove(int itemId) {
		return deleteItem(itemId, 1);
	}

	public boolean remove(int itemId, int amount) {
		return deleteItem(itemId, amount);
	}

	/**
	 * Deletes an item from the container
	 *
	 * @param itemId
	 *               The id of the item
	 * @param amount
	 *               The amount of the item to delete
	 */
	public boolean deleteItem(int itemId, int amount) {
		Item[] itemsBefore = items.getItemsCopy();
		items.remove(new Item(itemId, amount));
		refreshItems(itemsBefore);
		return true;
	}

	public boolean deleteItemAll(int itemId) {
		return deleteItem(itemId, Integer.MAX_VALUE);
	}

	/**
	 * Delets an item from the slot
	 *
	 * @param slot
	 *             The slot
	 * @param item
	 *             The item
	 */
	public void deleteItem(int slot, Item item) {
		Item[] itemsBefore = items.getItemsCopy();
		items.remove(slot, item);
		refreshItems(itemsBefore);
	}

	/**
	 * Deletes the item from the slot
	 *
	 * @param slotId
	 *               The slot
	 */
	public void deleteSlotItem(int slotId) {
		Item[] itemsBefore = items.getItemsCopy();
		items.set(slotId, null);
		refreshItems(itemsBefore);
	}

	/**
	 * Switches items in slots
	 *
	 * @param fromSlot
	 *                 The slot the item is comign from
	 * @param toSlot
	 *                 The slot the item is going to
	 */
	public void switchItem(int fromSlot, int toSlot) {
		Item[] itemsBefore = items.getItemsCopy();
		Item fromItem = items.get(fromSlot);
		Item toItem = items.get(toSlot);
		items.set(fromSlot, toItem);
		items.set(toSlot, fromItem);
		refreshItems(itemsBefore);
	}

	/**
	 * Checks to make sure there are empty slots
	 */
	public boolean hasFreeSlots() {
		return items.getFreeSlot() != -1;
	}

	public int getFreeSlots() {
		int slots = 0;
		for (Item item : items.getItems()) {
			if (item == null) {
				slots++;
			}
		}
		return slots;
	}

	/**
	 * Gets the item that has the id
	 *
	 * @param itemId
	 *               The id we want
	 * @param item1
	 *               The item to check
	 * @param item2
	 *               The second item to check
	 */
	public Item getItem(int itemId, Item item1, Item item2) {
		if (item1.getId() == itemId) {
			return item1;
		} else if (item2.getId() == itemId) {
			return item2;
		} else {
			return null;
		}
	}

	/**
	 * Checks if the container contains an item
	 *
	 * @param itemId
	 *               The id of the item
	 * @param amount
	 *               The amount we need to have
	 */
	public boolean containsItem(int itemId, int amount) {
		return items.contains(new Item(itemId, amount));
	}

	/**
	 * Checks if the container contains an item
	 *
	 * @param itemId
	 *               The id of the item
	 * @param amount
	 *               The amount we need to have
	 */
	public boolean containsItem(int itemId) {
		return items.contains(new Item(itemId, 1));
	}

	public boolean containsItem(int... itemIds) {
		for(int item : itemIds) {
			if(items.contains(item)) {
				return true;
			}
		}
		return false;
	}

}
