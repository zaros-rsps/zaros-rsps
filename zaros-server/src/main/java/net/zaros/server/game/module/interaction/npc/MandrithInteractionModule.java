package net.zaros.server.game.module.interaction.npc;

import net.zaros.server.game.module.interaction.rsinterface.SuppliesInterfaceInteractionModule;
import net.zaros.server.game.module.type.NPCInteractionModule;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/19/2017
 */
public class MandrithInteractionModule extends NPCInteractionModule {

	public MandrithInteractionModule() {
		register(6537);
	}

	@Override
	public boolean handle(Player player, NPC npc, InteractionOption option) {
		if (option == InteractionOption.FIRST_OPTION) {
			SuppliesInterfaceInteractionModule.display(player);
			return true;
		} else if (option == InteractionOption.SECOND_OPTION) {
			return false;
		}
		return false;
	}
}
