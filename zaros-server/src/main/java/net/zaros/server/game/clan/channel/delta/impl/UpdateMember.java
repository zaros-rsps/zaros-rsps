package net.zaros.server.game.clan.channel.delta.impl;

import lombok.RequiredArgsConstructor;
import net.zaros.cache.util.buffer.Buffer;
import net.zaros.server.game.clan.channel.ClanChannel.ChannelMember;
import net.zaros.server.game.clan.channel.delta.ClanChannelAction;

/**
 * Represents a ckab channel member data delta update action.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public class UpdateMember extends ClanChannelAction {

	/**
	 * The members slot.
	 */
	private final int slot;

	/**
	 * The members data.
	 */
	private final ChannelMember member;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.clan.channel.delta.ClanChannelAction#encode(net.zaros.
	 * cache.util.buffer.Buffer)
	 */
	@Override
	public void encode(Buffer buffer) {
		buffer.writeShort(slot);
		buffer.writeByte(member.getRank().getId());
		buffer.writeShort(member.getWorld());
		buffer.writeLong(member.getUserId());
		buffer.writeString(member.getDisplayName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.clan.channel.delta.ClanChannelAction#getId()
	 */
	@Override
	public int getId() {
		return 3;
	}
}
