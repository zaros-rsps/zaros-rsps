package net.zaros.server.game.module.type;

import lombok.Getter;
import net.zaros.server.game.module.interaction.InteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.InteractionOption;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/27/2017
 */
public abstract class ItemInteractionModule implements InteractionModule {

	/**
	 * The item ids that are subscribed to the module.
	 */
	@Getter
	List<Integer> itemIds = new ArrayList<>();

	/**
	 * The item names that are subscribed to the module.
	 */
	@Getter
	List<String> itemNames = new ArrayList<>();

	/**
	 * The item options that are subscribed to the module.
	 */
	@Getter
	List<String> itemOptions = new ArrayList<>();

	/**
	 * Registers the item id to this module.
	 * @param id The id of the item
	 */
	public void register(int... id) {
		Arrays.stream(id).forEach(itemId -> {
			if(!ArrayUtils.contains(getItemIds().toArray(), itemId))
				getItemIds().add(itemId);
		});
	}

	/**
	 * Registers the item name to this module.
	 * @param name The name of the item.
	 */
	public void register(String... name) {
		Arrays.stream(name).forEach(itemName -> {
			if(!ArrayUtils.contains(getItemNames().toArray(), itemName))
				getItemNames().add(itemName);
		});
	}

	/**
	 * Registers an item option this module.
	 * @param option The option of the item
	 */
	public void registerOption(String... option) {
		Arrays.stream(option).forEach(itemOption -> {
			if(!ArrayUtils.contains(getItemOptions().toArray(), itemOption))
				getItemOptions().add(itemOption);
		});
	}
	
	/**
	 * Handles the interaction with an item
	 *
	 * @param player
	 * 		The player
	 * @param item
	 * 		The item we're interacting with
	 * @param slotId
	 * 		The slot id in the inventory the item was clicked on
	 * @param option
	 * 		The option we clicked  @return {@code True} if successfully interacted.
	 */
	public abstract boolean handle(Player player, Item item, int slotId, InteractionOption option);
}
