package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;

import java.util.ArrayList;
import java.util.List;

public class TestSkillInterfaceCommand extends CommandModule {

    @Override
    public String[] identifiers() {
        return arguments("skillinter");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {
        List<Item> testItems = new ArrayList<>();
        testItems.add(new Item(4151));
        //SkillsChatboxInterface.open(player, SkillInterfaceData.COOK_INTERFACE);
    }

    @Override
    public boolean consoleUsageOnly() {
        return true;
    }
}

