package net.zaros.server.game.content.zskillsystem.crafting;

import net.zaros.server.game.content.zskillsystem.impl.ArtisanSkill;
import net.zaros.server.game.content.zskillsystem.properties.status.SkillStatus;

import net.zaros.server.game.content.zskillsystem.crafting.segments.BattleStaffSegment;
import net.zaros.server.game.node.entity.player.data.Skills;

/**
 * @author Jacob Rhiel
 */
public class CraftingSkill extends ArtisanSkill {

    public CraftingSkill() {
        super(true, Skills.CRAFTING, SkillStatus.DEVELOPMENTAL);
    }

    @Override
    public void instantiateSegments() {

        addSegment(new BattleStaffSegment(this));

    }

}
