/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames;

import lombok.Getter;

/**
 * @author Walied K. Yassen
 */
public class MultiareaProperties extends MinigameProperties {

	/**
	 * The lobby area player capacity.
	 */
	@Getter
	private final int lobbyCapacity;

	/**
	 * The waiting arena player capacity.
	 */
	@Getter
	private final int waitingCapacity;

	/**
	 * The play arena player capacity.
	 */
	@Getter
	private final int gameCapacity;

	/**
	 * Construct a new {@link MultiareaProperties} type object instance.
	 * 
	 * @param name
	 *                        the minigame name.
	 * @param lobbyCapacity
	 *                        the minigame lobby area capacity.
	 * @param waitingCapacity
	 *                        the minigame waiting area capacity.
	 * @param gameCapacity
	 *                        the minigame game area capacity.
	 */
	public MultiareaProperties(String name, int lobbyCapacity, int waitingCapacity, int gameCapacity) {
		super(name);
		this.lobbyCapacity = lobbyCapacity;
		this.waitingCapacity = waitingCapacity;
		this.gameCapacity = gameCapacity;
	}

}
