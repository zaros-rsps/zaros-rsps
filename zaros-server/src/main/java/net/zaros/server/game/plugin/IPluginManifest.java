package net.zaros.server.game.plugin;

import java.util.Collection;

/**
 * Represents a plug-in manifest, the manifest contains all the information
 * about the plug-in or it's author.
 *
 * @author Walied K. Yassen
 * @since 0.1
 */
public interface IPluginManifest {

	/**
	 * Gets the plug-in identifier. The plug-in identifier should be unique to each
	 * plug-in, therefore no duplicated plug-in identifier is allowed, and will
	 * cause issues in the running server.
	 *
	 * @return the plug-in identifier.
	 */
	String getId();

	/**
	 * Gets the plug-in name.
	 *
	 * @return the plug-in name.
	 */
	String getName();

	/**
	 * Gets the plug-in description.
	 *
	 * @return the plug-in description.
	 */
	String getDescription();

	/**
	 * Gets a collection filled with the author names.
	 *
	 * @return a {@link Collection} filled with the author names.
	 */
	Collection<String> getAuthors();

	/**
	 * Gets the plug-in main class fully qualified name. The main-class should be
	 * leading to a {@link IPlugin} sub-class.
	 *
	 * @return the plug-in main class fully qualified name.
	 */
	String getMainClass();
}
