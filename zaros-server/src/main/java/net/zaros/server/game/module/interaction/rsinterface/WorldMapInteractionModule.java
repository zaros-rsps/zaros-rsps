package net.zaros.server.game.module.interaction.rsinterface;

import net.zaros.server.game.module.type.InterfaceInteractionModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/8/2017
 */
public class WorldMapInteractionModule implements InterfaceInteractionModule {
	
	@Override
	public int[] interfaceSubscriptionIds() {
		return arguments(755);
	}
	
	@Override
	public boolean handle(Player player, int interfaceId, int componentId, int itemId, int slotId, int packetId) {
		if (componentId == 44) {
			player.getManager().getInterfaces().sendWindowPane(player.getManager().getInterfaces().usingFixedMode() ? SCREEN_FIXED_WINDOW_ID : SCREEN_RESIZABLE_WINDOW_ID, 2);
		}
		return true;
	}
}
