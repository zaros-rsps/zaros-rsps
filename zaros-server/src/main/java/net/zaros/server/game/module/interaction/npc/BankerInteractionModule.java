package net.zaros.server.game.module.interaction.npc;

import net.zaros.server.game.module.type.NPCInteractionModule;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/31/2017
 */
public class BankerInteractionModule extends NPCInteractionModule {

	public BankerInteractionModule() {
		register("banker");
	}

	@Override
	public boolean handle(Player player, NPC npc, InteractionOption option) {
		player.getBank().open();
		return true;
	}
}
