package net.zaros.server.game.content.combat.script.impl.formulas.impl;

import net.zaros.server.game.content.combat.script.impl.formulas.CombatFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.rs.constant.BonusConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

public class MeleeDefenceRoll implements CombatFormula {

    @Override
    public int get(Entity attacker, Entity target, boolean specialAttack) {

        var defenceLevel = target.getSkills().getLevel(SkillConstants.DEFENCE) * 1.0;

        final double prayerBoost = attacker.getPrayers().getBasePrayerBoost(SkillConstants.DEFENCE);
        defenceLevel *= prayerBoost;

        defenceLevel = Math.round(defenceLevel);

        final var attackerStyle = attacker.isPlayer() ? attacker.toPlayer().getCombatDefinitions().getAttackStyle() :
                attacker.toNPC().getCombatDefinitions().getAttackStyle();

        final var targetStyle = target.isPlayer() ? target.toPlayer().getCombatDefinitions().getAttackStyle() :
                target.toNPC().getCombatDefinitions().getAttackStyle();

        if (targetStyle == 3) {
            defenceLevel += 3;
        }

        if (targetStyle == 2) {
            defenceLevel = 1;
        }

        defenceLevel += 8;

        var defenceBonus = 0.0;

        switch (attackerStyle) {
            case BonusConstants.STAB_ATTACK:
                defenceBonus = target.getBonuses()[BonusConstants.STAB_DEFENCE];
                break;
            case BonusConstants.SLASH_ATTACK:
                defenceBonus = target.getBonuses()[BonusConstants.SLASH_DEFENCE];
                break;
            case BonusConstants.CRUSH_ATTACK:
                defenceBonus = target.getBonuses()[BonusConstants.CRUSH_DEFENCE];
                break;
        }

        defenceLevel = Math.round(defenceLevel);
        defenceBonus = Math.round(defenceBonus);

        var defenceRoll = Math.round(defenceLevel * (64 + defenceBonus)) * 1.0;

        return (int) defenceRoll;
    }
}
