package net.zaros.server.game.content.skills.fishing.plugin;

import lombok.Getter;
import net.zaros.server.game.content.skills.skills.SkillBase;

/**
 * @author Jacob Rhiel
 */
public class Fishing extends SkillBase {

    @Getter
    private static final SkillBase singleton = new Fishing();

}
