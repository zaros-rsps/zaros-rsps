package net.zaros.server.game.module.command.player;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.Skills;
import net.zaros.server.game.node.entity.player.render.flag.impl.AppearanceUpdate;
import net.zaros.server.utility.rs.constant.SkillConstants;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/7/2017
 */
@CommandManifest(description = "Masters you out like a pimp")
public class MasterCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("master");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		for (int skillId = 0; skillId < SkillConstants.SKILL_NAME.length; skillId++) {
			int level = 99;
			int exp = Skills.getXPForLevel(level);
			player.getSkills().setLevel(skillId, level);
			player.getSkills().setXp(skillId, exp);
		}
		player.getUpdateMasks().register(new AppearanceUpdate(player));
		player.setHealthPoints(player.getSkills().getLevelForXp(SkillConstants.HITPOINTS) * 10);
		player.getVariables().setPrayerPoints(player.getSkills().getLevelForXp(SkillConstants.PRAYER) * 10);
		player.getTransmitter().sendSettings();
	}
}
