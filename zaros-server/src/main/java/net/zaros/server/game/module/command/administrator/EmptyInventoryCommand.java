package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

public class EmptyInventoryCommand extends CommandModule {

    @Override
    public String[] identifiers() {
        return arguments("empty", "clear", "clearinvent");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {
        player.getInventory().getItems().clear();
        player.getInventory().refreshAll();
    }
}

