package net.zaros.server.game.content.system.zskillsystem.farming.actions;

import net.zaros.server.game.content.system.zskillsystem.farming.impl.FarmingConstants;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.PatchState;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.Patches;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;

/**
 * Handles the action to clear the patch.
 *
 * @author Gabriel || Wolfsdarker
 */
public class ClearPatchAction extends PlayerTask {

	/**
	 * Constructor for the action.
	 *
	 * @param player
	 * @param state
	 */
	public ClearPatchAction(Player player, PatchState state) {
		super(2, player, () -> {

			player.getTransmitter().sendMessage("You clear the farming patch.");
			state.resetPatch();
			player.getFarming().updatePatches(player);
			player.endCurrentTask();

		});
	}

	/**
	 * Handles the action of clearing the patch.
	 *
	 * @param player
	 * @param data
	 */
	public static void clearPatch(Player player, Patches data, Location pos) {

		PatchState state = player.getFarming().getPatchStates().get(data.name());

		if (state == null) {
			return;
		}

		if (state.getWeedStage() < 3) {
			player.getTransmitter().sendMessage("You must clear the weed with the rake");
			return;
		}

		if (!state.isUsed()) {
			player.getTransmitter().sendMessage("This patch is already clear.");
			return;
		}

		if (!player.getInventory().getItems().contains(FarmingConstants.SPADE)) {
			player.getTransmitter().sendMessage("You need a spade to clear this patch.");
			return;
		}

		if(!state.isDead()) {
			player.getTransmitter().sendMessage("You can only clear dead patches.");
			return;
		}

		player.startConversation(new WarningOption(player, "CLEAR_FARMING_PATCH", () -> {

			player.faceLocation(pos);
			player.sendAnimation(new Animation(FarmingConstants.SPADE_ANIM));
			TaskManager.submit(new ClearPatchAction(player, state));

		}, "This will remove all the crops from the patch.", "Are you sure you want to do this?"));
	}
}
