package net.zaros.server.game.content.zskillsystem;

import lombok.Getter;
import net.zaros.server.game.content.event.EventContext;
import net.zaros.server.game.content.zskillsystem.crafting.CraftingSkill;
import net.zaros.server.game.content.zskillsystem.segment.AbstractSkillSegment;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author Jacob Rhiel
 */
@Getter
public class SkillExecutor {

    public Set<AbstractSkill> skills = new CopyOnWriteArraySet<>();

    public SkillExecutor() {
        this.skills.add(new CraftingSkill());
    }

    public void execute(EventContext context) {

        for (AbstractSkill a : this.skills) {
            for(AbstractSkillSegment segment : a.getSegments()) {

            }
        }

    }

}
