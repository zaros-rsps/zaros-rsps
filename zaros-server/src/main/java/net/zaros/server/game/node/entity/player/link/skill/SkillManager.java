package net.zaros.server.game.node.entity.player.link.skill;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.content.minigames.MinigameBase;
import net.zaros.server.game.content.skills.skills.SkillBase;
import net.zaros.server.game.node.entity.player.Player;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SkillManager {

    private final List<SkillBase> skills = new CopyOnWriteArrayList<>();

    public void register(SkillBase skill) {
        skills.add(skill);
    }

    public void unregister(SkillBase skill) {
        skills.remove(skill);
    }

    @Setter
    @Getter
    private int lastSkillInterfaceAmount;

    @Setter
    @Getter
    private transient Player player;

}
