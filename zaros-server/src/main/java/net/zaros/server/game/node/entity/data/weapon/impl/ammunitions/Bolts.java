package net.zaros.server.game.node.entity.data.weapon.impl.ammunitions;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.Ammunition;
import net.zaros.server.game.node.entity.render.flag.impl.GraphicHeight;
import net.zaros.server.utility.rs.constant.EquipConstants;

public class Bolts implements Ammunition {

    /**
     * The bolt's poison damage.
     */
    private int poisonDamage;

    public Bolts() {
    }

    public Bolts(int poisonDamage) {
        this.poisonDamage = poisonDamage;
    }

    @Override
    public int ammoSlot() {
        return EquipConstants.SLOT_ARROWS;
    }

    @Override
    public int poisonDamage() {
        return poisonDamage;
    }

    @Override
    public boolean accumulatorSupport() {
        return true;
    }

    @Override
    public boolean usable(Entity entity) {
        return true;
    }

    @Override
    public void performGraphics(Entity entity) {
        entity.sendGraphics(955, GraphicHeight.HIGH.toInt(), 0);
    }

    @Override
    public void sendProjetile(Entity attacker, Entity target) {
        int projectileID = 27;
        ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(attacker, target, projectileID, 43, 41, 44, 0, 0));
    }
}
