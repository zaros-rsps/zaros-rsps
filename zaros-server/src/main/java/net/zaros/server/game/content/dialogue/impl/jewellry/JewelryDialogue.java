package net.zaros.server.game.content.dialogue.impl.jewellry;

import lombok.Getter;
import net.zaros.server.game.content.dialogue.Dialogue;
import net.zaros.server.game.content.teleportation.EnchantedJewellery;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;

import java.util.stream.IntStream;

public class JewelryDialogue extends Dialogue {

    @Getter
    private final Item item;

    @Getter
    private final int slotId;

    public JewelryDialogue(Item item, int slotId) {
        this.item = item;
        this.slotId = slotId;
    }

    @Override
    public void constructMessages(Player player) {
        EnchantedJewellery jewelry = EnchantedJewellery.forItem(item);
        int options = jewelry.getOptions().length;
        Runnable[] tasks = new Runnable[options];
        IntStream.range(0, options).forEach(i -> tasks[i] = () -> jewelry.use(player, item, slotId, i, false));
        options("Where would you like to go?", jewelry.getOptions(), tasks);
    }
}
