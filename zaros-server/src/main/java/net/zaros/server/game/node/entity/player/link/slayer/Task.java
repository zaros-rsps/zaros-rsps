package net.zaros.server.game.node.entity.player.link.slayer;

import net.zaros.cache.type.npctype.NPCDefinitionParser;
import net.zaros.server.game.content.skills.slayer.SlayerMaster;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.constant.SkillConstants;

public class Task {

    /**
     * Represents the npc ids reated to the task.
     */
    private final int[] npcs;

    /**
     * Represents the tip to give.
     */
    private final Object[] tip;

    /**
     * Represents the slayerMasters able to assign this task.
     */
    private final SlayerMaster[] slayerMasters;

    /**
     * The required slayer level.
     */
    private final int level;

    /**
     * If the task is killing undead NPCs.
     */
    private final boolean undead;

    /**
     * Any requirement equipment for this task.
     */
    private final SlayerEquipment[] equipment;

    /**
     * The task amount hash.
     */
    private int taskAmtHash;

    /**
     * Constructs a new {@code Task} {@code Object}.
     * @param npcs the npcs.
     * @param tip the tips.
     * @param level the level.
     * @param slayerMasters the slayerMasters.
     * @param undead if undead.
     * @param equipment the equipment.
     */
    public Task(int[] npcs, Object[] tip, int level, SlayerMaster[] slayerMasters, boolean undead, SlayerEquipment... equipment) {
        this.npcs = npcs;
        this.tip = tip;
        this.level = level;
        this.slayerMasters = slayerMasters;
        this.undead = undead;
        this.equipment = equipment;
    }

    /**
     * Constructs a new {@code Task} {@code Object}.
     * @param npcs the npcs.
     * @param tip the tips.
     * @param level the level.
     * @param slayerMasters the slayerMasters.
     * @param undead if undead.
     * @param taskAmtHash hash of amounts to recieve.
     * @param equipment the equipment.
     */
    public Task(int[] npcs, Object[] tip, int level, SlayerMaster[] slayerMasters, boolean undead, int taskAmtHash, SlayerEquipment... equipment) {
        this(npcs, tip, level, slayerMasters, undead, equipment);
        this.taskAmtHash = taskAmtHash;
    }

    /**
     * Checks if this task can be assigned to the player.
     * @param player the player.
     * @param slayerMaster the slayerMaster.
     * @return {@code True} if so.
     */
    public boolean canAssign(Player player, SlayerMaster slayerMaster) {
        final int slayerLevel = player.getSkills().getLevel(SkillConstants.SLAYER);
        if (isDisabled()) {
            return false;
        }
        if (getLevel() > slayerLevel) {
            return false;
        }
        return true;
    }

    /**
     * Checks if the task contains the slayerMaster given.
     * @param slayerMaster the slayerMaster.
     * @return the {@code True} if so
     */
    public boolean hasMaster(SlayerMaster slayerMaster) {
        for (SlayerMaster slayerMaster2 : slayerMasters) {
            if (slayerMaster2 == slayerMaster) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the task is disabled.
     * @return {@code True} if so.
     */
    public boolean isDisabled() {
        return false;
    }

    /**
     * Gets the ranges.
     * @param slayerMaster the slayerMaster.
     * @return the ranges.
     */
    public int[] getRanges(SlayerMaster slayerMaster) {
        if (taskAmtHash != 0) {
            int min = taskAmtHash & 0xFFFF;
            int max = taskAmtHash >> 16 & 0xFFFF;
            return new int[] { min, max };
        }
        return slayerMaster.getRanges();
    }

    /**
     * Gets the name.
     * @return the name.
     */
    public String getName() {
        return NPCDefinitionParser.forId(npcs[0]).getName();
    }

    /**
     * Gets the npcs.
     * @return The npcs.
     */
    public int[] getNpcs() {
        return npcs;
    }

    /**
     * Gets the slayerMasters.
     * @return The slayerMasters.
     */
    public SlayerMaster[] getSlayerMasters() {
        return slayerMasters;
    }

    /**
     * Gets the level.
     * @return The level.
     */
    public int getLevel() {
        return level;
    }

    /**
     * Gets the tip.
     * @return The tip.
     */
    public String[] getTip() {
        return (String[]) tip;
    }

    /**
     * Gets the undead.
     * @return The undead.
     */
    public boolean isUndead() {
        return undead;
    }

    /**
     * Gets the equipment.
     * @return The equipment.
     */
    public SlayerEquipment[] getEquipment() {
        return equipment;
    }

}

