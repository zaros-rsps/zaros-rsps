package net.zaros.server.game.node.entity.npc.extension.familiar.impl;

import net.zaros.server.game.content.skills.summoning.FamiliarData;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.npc.extension.familiar.SummoningFamiliar;

public class TestFamiliar extends SummoningFamiliar {

    public TestFamiliar(Location location) {
        super(FamiliarData.PACK_YAK, location);
    }

}
