package net.zaros.server.game.content.combat.script.impl.formulas.impl;

import net.zaros.server.game.content.combat.StaticCombatFormulae;
import net.zaros.server.game.content.combat.script.impl.formulas.CombatFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.item.CombatEquipments;
import net.zaros.server.utility.rs.constant.BonusConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

public class MeleeMaxHit implements CombatFormula {

    @Override
    public int get(Entity attacker, Entity target, boolean specialAttack) {

        var strengthLevel = attacker.getSkills().getLevel(SkillConstants.STRENGTH);

        final double prayerBoost = attacker.getPrayers().getBasePrayerBoost(SkillConstants.STRENGTH);
        strengthLevel *= prayerBoost;

        final var attackerStyle = attacker.isPlayer() ? attacker.toPlayer().getCombatDefinitions().getAttackStyle() :
                attacker.toNPC().getCombatDefinitions().getAttackStyle();

        if (attackerStyle == 1) {
            strengthLevel += 3;
        }

        if (attackerStyle == 2) {
            strengthLevel = 1;
        }

        strengthLevel += 8.0;

        //Void effect
        if (attacker.isPlayer() && StaticCombatFormulae.fullVoidEquipped(attacker.toPlayer(), 11665)) {
            strengthLevel *= 1.10;
        }

        var strengthBonus = attacker.getBonuses()[BonusConstants.STRENGTH_BONUS];

        var maxHit = (0.5 + ((strengthLevel * (strengthBonus + 64.0)) / 640.0)) * 10.0;

        if (attacker.isNPC()) {
            maxHit = attacker.toNPC().getCombatDefinitions().getMaxHit();
        }

        maxHit = Math.round(maxHit);

        //Special attack modifiers
        if (attacker.isPlayer() && specialAttack) {

            int weaponId = attacker.toPlayer().getEquipment().getWeaponId();

            switch (weaponId) {
                case 11694:
                    maxHit = Math.round(1.10 * maxHit);
                    maxHit *= 1.25;
                    break;
            }
        }

        maxHit = Math.round(maxHit);

        //Dharok's full armour effect
        if (CombatEquipments.fullDharoks(attacker)) {
            var hp = attacker.getSkills().getLevel(SkillConstants.HITPOINTS);
            var maxHp = attacker.getSkills().getLevelForXp(SkillConstants.HITPOINTS);

            if (hp > maxHp) {
                hp = maxHp;
            }

            var modifier = 1 + ((maxHp - hp) / 100.0);

            maxHit *= modifier;
        }

        return (int) Math.round(maxHit);
    }
}
