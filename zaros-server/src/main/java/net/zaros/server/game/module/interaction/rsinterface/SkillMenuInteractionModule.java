package net.zaros.server.game.module.interaction.rsinterface;

import net.zaros.server.game.module.type.InterfaceInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigPacketBuilder;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/28/2017
 */
public class SkillMenuInteractionModule implements InterfaceInteractionModule {
	
	@Override
	public int[] interfaceSubscriptionIds() {
		return Misc.arguments(499);
	}
	
	@Override
	public boolean handle(Player player, int interfaceId, int componentId, int itemId, int slotId, int packetId) {
		int skillMenu = player.getTemporaryAttribute(AttributeKey.SKILL_MENU, -1);
		if (componentId >= 10 && componentId <= 25) {
			player.getTransmitter().send(new ConfigPacketBuilder(965, ((componentId - 10) * 1024) + skillMenu).build(player));
		} else if (componentId == 29) {
			//
		}
		return true;
	}
}
