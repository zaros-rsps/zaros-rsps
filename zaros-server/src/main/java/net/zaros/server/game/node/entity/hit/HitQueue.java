package net.zaros.server.game.node.entity.hit;

import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.rs.Hit;

import java.util.ArrayList;
import java.util.List;

/**
 * The character's incoming hits queue.
 *
 * @author Gabriel || Wolfsdarker
 */
public class HitQueue {

	/**
	 * The instace who owns the damage queue.
	 */
	private Entity character;

	/**
	 * The character's hit queue.
	 */
	private List<Hit> hitQueue = new ArrayList<>();

	/**
	 * Constructor for the hit queue.
	 *
	 * @param character
	 */
	public HitQueue(Entity character) {
		this.character = character;
	}

	/**
	 * Executes the pending hits with delay = 0 and decreases the delay for pending hits.
	 */
	public void processHits() {

		if (hitQueue.isEmpty()) {
			return;
		}

		//TODO: Untargetable
		/*if (character.isUntargetable()) {
			hitQueue.clear();
			return;
		}*/

		List<Hit> hits = new ArrayList<>(hitQueue);

		for (Hit hit : hits) {

			hit.decreaseDelay();

			if (hit.getDelay() == 0) {
				hitQueue.remove(hit);
				character.getHitMap().applyHit(hit);
			}
		}
	}

	/**
	 * Adds a hit to the queue.
	 *
	 * @param hit
	 */
	public void add(Hit hit) {

		//TODO: Untargetable
		/*if (character.isUntargetable()) {
			return;
		}*/

		if (hit.getDelay() == 0) {
			character.getHitMap().applyHit(hit);
		} else {
			hitQueue.add(hit);
		}
	}

}
