package net.zaros.server.game.content.combat.script.impl.formulas;

import net.zaros.server.game.node.entity.Entity;

/**
 * A simple interface for combat formulas.
 */
public interface CombatFormula {

    /**
     * Returns the formula's expected value.
     *
     * @param attacker
     * @param target
     * @param specialAttack
     * @return the value
     */
    int get(Entity attacker, Entity target, boolean specialAttack);

}
