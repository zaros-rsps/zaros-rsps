package net.zaros.server.game.content.zone.impl;

import net.zaros.server.game.content.zone.Zone;
import net.zaros.server.game.node.Location;

public class EdgevilleBank extends Zone {
    /**
     * The Unique 'Zone' will need these properties for be a zone
     *
     * @param topRight
     * @param bottomLeft
     */
    public EdgevilleBank(Location topRight, Location bottomLeft) {
        super(topRight, bottomLeft);
    }
}
