package net.zaros.server.game.content.system.zskillsystem.farming.impl;

import net.zaros.server.game.node.Location;

import java.util.Arrays;

/**
 * The compost bin's location and information.
 */
public enum CompostBinPositions {

	NORTH_ARDOUGNE(7839, new Location(2661, 3375)),

	FALADOR(7836, new Location(3056, 3312)),

	CATHERBY(7837, new Location(2804, 3464)),

	PHASMATYS(7838, new Location(3610, 3522));

	/***
	 * The bin's object ID.
	 */
	private int object_id;

	/**
	 * The coordinates for the object.
	 */
	private Location bin_Location;

	CompostBinPositions(int object_id, Location bin_Location) {
		this.object_id = object_id;
		this.bin_Location = bin_Location;
	}

	/**
	 * Returns the bin's object ID.
	 *
	 * @return the ID
	 */
	public int getObjectID() {
		return object_id;
	}

	/**
	 * Returns the compost bin's Location on map.
	 *
	 * @return the Location
	 */
	public Location getLocation() {
		return bin_Location;
	}

	/**
	 * Returns the compost bin's impl by its Location on map.
	 *
	 * @param pos
	 * @return the impl
	 */
	public static CompostBinPositions get(Location pos) {
		return get(pos.getX(), pos.getY());
	}

	/**
	 * Returns the compost bin's impl by its Location on map.
	 *
	 * @param x
	 * @param y
	 * @return the impl
	 */
	public static CompostBinPositions get(int x, int y) {
		return Arrays.stream(CompostBinPositions.values()).filter((bin) -> (bin.getLocation().getX() == x && bin
			.getLocation().getY() == y)).findAny().orElse(null);
	}

	/**
	 * Returns the compost bin's impl by its object ID.
	 *
	 * @param ID
	 * @return the impl
	 */
	public static CompostBinPositions get(int ID) {
		return Arrays.stream(CompostBinPositions.values()).filter((bin) -> (bin.getObjectID() == ID)).findAny()
			.orElse(null);
	}
}
