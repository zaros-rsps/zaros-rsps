package net.zaros.server.game.clan.settings;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.clan.ClanRank;
import net.zaros.server.game.clan.channel.ClanChannel.ChannelMember;

/**
 * @author Walied K. Yassen
 */
public final class ClanMember {

	/**
	 * The player user id of this member.
	 */
	@Getter
	private final long userId;

	/**
	 * The member join time.
	 */
	@Getter
	private final long joinTime;

	/**
	 * The player display name.
	 */
	@Getter
	private String displayName;

	/**
	 * The rank of this member.
	 */
	@Getter
	private ClanRank rank;

	/**
	 * The extra info of this member.
	 */
	@Getter
	@Setter
	private int extraInfo;

	/**
	 * The current world of the member.
	 */
	@Getter
	@Setter
	private transient int world;

	/**
	 * Constructs a new {@link ClanMember} type object instance.
	 * 
	 * @param userId
	 *               the player user id of this member.
	 * @param name
	 *               the player display name.
	 * @param rank
	 *               the initial rank for this member.
	 */
	public ClanMember(long userId, String name, ClanRank rank) {
		this.userId = userId;
		displayName = name;
		this.rank = rank;
		joinTime = System.currentTimeMillis();
	}

	/**
	 * Converts this {@link ClanMember} into a {@link ChannelMember}.
	 * 
	 * @return the converted {@link ChannelMember} object.
	 */
	public ChannelMember toChannelMember() {
		return new ChannelMember(userId, displayName, rank, world);
	}
}
