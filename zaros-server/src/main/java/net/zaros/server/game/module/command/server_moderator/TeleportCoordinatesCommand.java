package net.zaros.server.game.module.command.server_moderator;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.MessageBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/1/2017
 */
@CommandManifest(description = "Teleports you to specific coordinates", types = { Integer.class, Integer.class })
public class TeleportCoordinatesCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("xtele");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		try {
			Integer x = intParam(args, 1);
			Integer y = intParam(args, 2);
			Integer plane = args.length == 4 ? intParam(args, 3) : player.getLocation().getPlane();
			
			player.teleport(Location.create(x, y, plane));
		} catch (NumberFormatException e) {
			player.getTransmitter().send(new MessageBuilder("Invalid parameters...").build(player));
		}
	}
}
