package net.zaros.server.game.node.entity.player.link.slayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import net.zaros.cache.type.npctype.NPCDefinitionParser;
import net.zaros.server.game.content.skills.slayer.SlayerMaster;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.PlayerEquipment;
import net.zaros.server.utility.rs.constant.SkillConstants;
import net.zaros.server.utility.tool.RandomFunction;

public class SlayerManager {

    public void openSlayerRewards(Player player) {
        player.getManager().getInterfaces().sendInterface(164, true);
        /*for(int i = 0; i < 40; i++) {
            player.getManager().getInterfaces().sendInterfaceText(164, i, "" + i);
            player.getManager().getInterfaces().sendInterfaceText(163, i, "" + i);
            player.getManager().getInterfaces().sendInterfaceText(161, i, "" + i);
        }*/
        player.getManager().getInterfaces().sendInterfaceText(164, 20, String.valueOf(player.getManager().getSlayer().getSlayerPoints()));
        player.getManager().getInterfaces().sendInterfaceText(163, 18, String.valueOf(player.getManager().getSlayer().getSlayerPoints()));
        player.getManager().getInterfaces().sendInterfaceText(161, 19, String.valueOf(player.getManager().getSlayer().getSlayerPoints()));
        //player.getManager().getInterfaces().sendInterfaceText(164, 24, "<col=d80803>Test</col>");
        refreshSlayerRewardInterface(player, 161);
        refreshSlayerRewardInterface(player, 163);
        refreshSlayerRewardInterface(player, 164);
    }

    public void refreshSlayerRewardInterface(Player player, int interfaceId) {
        SlayerManager slayer = player.getManager().getSlayer();
        int points = player.getManager().getSlayer().getSlayerPoints();
        switch(interfaceId) {
            case 164:
                player.getManager().getInterfaces().sendInterfaceText(164, 20, String.valueOf(slayer.getSlayerPoints()));
                if(points < 400) {
                    player.getManager().getInterfaces().sendInterfaceText(164, 24, "<col=d80803>Buy Slayer XP</col>");
                    player.getManager().getInterfaces().sendInterfaceText(164, 32, "<col=d80803>400 points</col>");
                }
                if(points < 75) {
                    player.getManager().getInterfaces().sendInterfaceText(164, 26, "<col=d80803>Buy ring of slaying</col>");
                    player.getManager().getInterfaces().sendInterfaceText(164, 33, "<col=d80803>75 points</col>");
                }
                if(points < 35) {
                    player.getManager().getInterfaces().sendInterfaceText(164, 28, "<col=d80803>Buy runes for Slayer Dart</col>");
                    player.getManager().getInterfaces().sendInterfaceText(164, 37, "<col=d80803>Buy broad bolts</col>");
                    player.getManager().getInterfaces().sendInterfaceText(164, 39, "<col=d80803>Buy broad arrows</col>");
                    player.getManager().getInterfaces().sendInterfaceText(164, 34, "<col=d80803>35 points</col>");
                    player.getManager().getInterfaces().sendInterfaceText(164, 35, "<col=d80803>35 points</col>");
                    player.getManager().getInterfaces().sendInterfaceText(164, 36, "<col=d80803>35 points</col>");
                }
                break;
            case 163:
                player.getManager().getInterfaces().sendInterfaceText(163, 18, String.valueOf(slayer.getSlayerPoints()));
                for (int i = 0; i < slayer.getLearned().length; i++) {
                    if(!slayer.getLearned()[i]) {
						player.getManager().getInterfaces().sendInterfaceText(163, 25 + i, "");
					}
                }
                if(points < 300) {
                    player.getManager().getInterfaces().sendInterfaceText(163, 22, "<col=d80803>Learn how to fletch broad arrows/bolts</col>");
                    player.getManager().getInterfaces().sendInterfaceText(163, 23, "<col=d80803>Learn how to craft rings of slaying</col>");
                    player.getManager().getInterfaces().sendInterfaceText(163, 24, "<col=d80803>Learn how to craft Slayer helmets</col>");
                    player.getManager().getInterfaces().sendInterfaceText(163, 29, "<col=d80803>300 points</col>");
                    player.getManager().getInterfaces().sendInterfaceText(163, 30, "<col=d80803>300 points</col>");
                    player.getManager().getInterfaces().sendInterfaceText(163, 31, "<col=d80803>400 points</col>");
                }
                break;
            case 161:
                player.getManager().getInterfaces().sendInterfaceText(161, 19, String.valueOf(slayer.getSlayerPoints()));
                int children[] = new int[]{36, 30, 31, 32, 33};
                String[] letters = new String[]{"A", "B", "C", "D", "E"};
                Task task;
                for (int i = 0; i < 4; i++) {
                    task = i > slayer.getRemoved().size() - 1 ? null : slayer.getRemoved().get(i);
                    player.getManager().getInterfaces().sendInterfaceText(161,  children[i], task == null ? letters[i] : task.getName());
                }
                break;
        }
        player.getManager().getInterfaces().sendInterface(interfaceId, true);
    }

    public void purchaseReward(Player player, int interfaceId, int childId) {
        switch(interfaceId) {
            case 164:
                switch(childId) {
                    case 24://slayer exp
                    case 32:
                        if (purchase(player, 164, 400)) {
                            player.getSkills().addExperienceWithMultiplier(SkillConstants.SLAYER, 10000);
                        }
                        break;
                    case 26://ring of slaying
                    case 33:
                        if (player.getInventory().getItems().freeSlots() < 1 && player.getManager().getSlayer().getSlayerPoints() >= 75) {
                            player.getTransmitter().sendMessage("You don't have enough inventory space.");
                            break;
                        }
                        if (purchase(player, 164, 75)) {
                            player.getInventory().addItem(13281, 1);
                        }
                        break;
                    case 28:
                    case 36:
                        if (purchase(player, 164,35)) {
                            player.getInventory().addItem(558, 750);
                            player.getInventory().addItem(560, 250);
                        }
                        break;
                    case 34:
                    case 37:
                        if (purchase(player, 164,35)) {
                            player.getInventory().addItem(13280, 250);
                        }
                        break;
                    case 35:
                    case 39:
                        if (purchase(player, 164,35)) {
                            player.getInventory().addItem(4172, 250);
                        }
                        break;
                }
                break;
            case 163:
                switch(childId) {
                    case 22://Broad arrows
                    case 29:
                        if (player.getManager().getSlayer().getLearned()[0]) {
                            player.getTransmitter().sendMessage("You don't need to learn this ability again.");
                            break;
                        }
                        if (purchase(player, 163,300)) {
                            player.getManager().getSlayer().getLearned()[0] = true;
                            refreshSlayerRewardInterface(player, 163);
                        }
                        break;
                    case 23://Slayer ring
                    case 30:
                        if (player.getManager().getSlayer().getLearned()[1]) {
                            player.getTransmitter().sendMessage("You don't need to learn this ability again.");
                            break;
                        }
                        if (purchase(player, 163, 300)) {
                            player.getManager().getSlayer().getLearned()[1] = true;
                            refreshSlayerRewardInterface(player, 163);
                        }
                        break;
                    case 24://Slayer helm
                    case 31:
                        if (player.getManager().getSlayer().getLearned()[2]) {
                            player.getTransmitter().sendMessage("You don't need to learn this ability again.");
                            break;
                        }
                        if (purchase(player, 163,400)) {
                            player.getManager().getSlayer().getLearned()[2] = true;
                            refreshSlayerRewardInterface(player, 163);
                        }
                        break;
                }
                break;
            case 161:
                switch(childId) {
                    case 23://reassign
                    case 26:
                        if (!player.getManager().getSlayer().hasTask()) {
                            player.getTransmitter().sendMessage("You don't have an active task right now.");
                            break;
                        }
                        if (purchase(player, 161,30)) {
                            player.getManager().getSlayer().clear();
                            player.getTransmitter().sendMessage("You have canceled your current task.");
                        }
                        break;
                    case 24:
                    case 27:
                        if (player.getManager().getSlayer().getTask() == null) {
                            player.getTransmitter().sendMessage("You don't have a slayer task.");
                            break;
                        }
                        if (player.getManager().getSlayer().getRemoved().size() >= 4) {
                            player.getTransmitter().sendMessage("You can't remove anymore tasks.");
                            break;
                        }
                        /*if (player.getManager().getSlayer().getSlayerPoints() >= 30) {
                            int size = player.getManager().getSlayer().getRemoved().size();
                            *//*int qp = player.getQuestRepository().getAvailablePoints();
                            if (size == 0 && qp < 50) {
                                player.sendMessage("You need 50 quest points as a requirement in order to block one task.");
                                break;
                            } else if (size == 1 && qp < 100) {
                                player.sendMessage("You need 100 quest points as a requirement in order to block two tasks.");
                                break;
                            } else if (size == 2 && qp < 150) {
                                player.sendMessage("You need 150 quest points as a requirement in order to block three tasks.");
                                break;
                            } else if (size == 3 && qp < 200) {
                                player.sendMessage("You need 200 quest points as a requirement in order to block four tasks.");
                                break;
                            }*//*
                        }*/
                        if (purchase(player, 161,100)) {
                            player.getManager().getSlayer().getRemoved().add(player.getManager().getSlayer().getTask());
                            player.getManager().getSlayer().clear();
                            refreshSlayerRewardInterface(player, 161);
                        }
                        break;
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                        int index = childId - 37;
                        if (player.getManager().getSlayer().getRemoved().isEmpty() || index > player.getManager().getSlayer().getRemoved().size() - 1 || player.getManager().getSlayer().getRemoved().get(index) == null) {
                            break;
                        }
                        player.getManager().getSlayer().getRemoved().remove(index);
                        refreshSlayerRewardInterface(player, 161);
                        break;
                }
                break;
        }
    }

    private boolean purchase(Player player, int interfaceId, int amount) {
        if (player.getManager().getSlayer().getSlayerPoints() < amount) {
            player.getTransmitter().sendMessage("You need " + amount + " slayer points in order to purchase this reward.");
            return false;
        }
        player.getManager().getSlayer().setSlayerPoints(player.getManager().getSlayer().getSlayerPoints() - amount);
        refreshSlayerRewardInterface(player, interfaceId);
        return true;
    }

    public void buyRewards(int interfaceId, int childId) {
        switch(interfaceId) {
            case 163:
                switch(childId) {
                    case 29:
                    case 33:

                        break;
                }
                break;
        }
    }

    public void finalizeDeath(Player player, NPC npc) {
        player.getSkills().addExperienceWithMultiplier(SkillConstants.SLAYER, 1000);//TODO
        decrementAmount(1);
        if (!hasTask()) {
            clear();
            setCompletedTasks(getCompletedTasks() + 1);
            if (getCompletedTasks() > 4 && master != SlayerMaster.TURAEL && slayerPoints < 64000) {
                int points = getMaster().getTaskPoints()[0];
                if (getCompletedTasks() % 10 == 0) {
                    points = getMaster().getTaskPoints()[1];
                } else if (getCompletedTasks() % 50 == 0) {
                    points = getMaster().getTaskPoints()[2];
                }
                setSlayerPoints(getSlayerPoints() + points);
                if (getSlayerPoints() > 64000) {
                    setSlayerPoints(64000);
                }
                player.getTransmitter().sendMessage("You've completed " + getCompletedTasks() + " tasks in a row and received " + points + " points; return to a Slayer master.");
            } else {
                player.getTransmitter().sendMessage("You've completed your task; Complete " + (4 - getCompletedTasks()) + " more task(s) to start gaining points; return to a Slayer master.", false);
            }
        } else {
            sendRemaining(true);
        }
    }

    private boolean hasAutoSlayerMessageEquipment(Player player) {
        PlayerEquipment equipment = player.getEquipment();
        int[] items = new int[] { 11118, 11120, 11122, 11124 };
        for(int item : items) {
			if(equipment.getItems().contains(item)) {
				return true;
			}
		}
        return false;
    }

    public void sendRemaining(boolean death) {
        if(death) {
            if (hasAutoSlayerMessageEquipment(player) && getTaskAmount() % 10 == 0) {
				player.getTransmitter().sendMessage("You're assigned to kill " + getTask().getName() + "s; Only " + getTaskAmount() + " more to go.");
			}
        } else {
			player.getTransmitter().sendMessage("You're assigned to kill " + getTask().getName() + "s; Only " + getTaskAmount() + " more to go.");
		}
    }

    /**
     * Method used to assign a new task for a player.
     * @param master the master to give the task.
     */
    public void generate(SlayerMaster master) {
        final List<Task> tasks = Arrays.asList(SlayerTask.getTasks(master));
        Collections.shuffle(tasks, RandomFunction.RANDOM);
        for (Task task : tasks) {
            if (!task.canAssign(player, master)) {
                continue;
            }
            assign(task, master);
            break;
        }
    }

    /**
     * Assigns a task to the manager.
     * @param task the task.
     * @param master the master.
     */
    public void assign(Task task, final SlayerMaster master) {
        setMaster(master);
        setTask(task);
        setTaskAmount(getRandomAmount(task.getRanges(master)));
        player.getTransmitter().sendMessage("Assigned task of : " + getTaskAmount() + " " + getTask().getName());
    }


    /**
     * Gets a random amount.
     * @param ranges the ranges.
     * @return the amt.
     */
    private int getRandomAmount(int[] ranges) {
        return RandomFunction.random(ranges[0], ranges[1]);
    }

    public void decrementAmount(int amount) {
        setTaskAmount(getTaskAmount() - amount);
    }

    public void clear() {
        setTask(null);
        setTaskAmount(0);
    }

    /**
     * Checks if a <b>Player</b> contains a task.
     * @return {@code True} if so.
     */
    public boolean hasTask() {
        if (getTask() == null) {
            return false;
        }
        if (getTaskAmount() == 0) {
            return false;
        }
        return true;
    }


    /**
     * Method used to check if the task is completed.
     * @return <code>True</code> if so.
     */
    public boolean isCompleted() {
        return getTaskAmount() <= 0;
    }

    /**
     * Gets the task name.
     * @return the name.
     */
    public String getTaskName() {
        if (getTask() == null) {
            return "null";
        }
        if (getTask().getNpcs() == null) {
            return "no npcs report me";
        }
        if (getTask().getNpcs().length < 1) {
            return "npc length to small report me";
        }
        return NPCDefinitionParser.forId(getTask().getNpcs()[0]).getName().toLowerCase();
    }

    @Setter
    private transient Player player;

    @Getter
    @Setter
    private SlayerMaster master;

    @Getter
    @Setter
    private Task task;

    @Getter
    @Setter
    private int taskAmount;

    @Getter
    @Setter
    private int completedTasks;

    @Getter
    @Setter
    private int slayerPoints;

    @Getter
    private final List<Task> removed = new ArrayList<>(4);

    @Getter
    private final boolean[] learned = new boolean[3];

}
