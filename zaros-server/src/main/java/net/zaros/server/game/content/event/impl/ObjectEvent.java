package net.zaros.server.game.content.event.impl;

import java.util.ArrayList;
import java.util.List;

import net.zaros.cache.type.loctype.ObjectDefinition;
import net.zaros.server.game.content.dialogue.DialogueRepository;
import net.zaros.server.game.content.event.Event;
import net.zaros.server.game.content.event.context.ObjectEventContext;

import net.zaros.server.game.module.ModuleRepository;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.link.LockManager.LockType;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/1/2017
 */
public class ObjectEvent extends Event<ObjectEventContext> {

	@Override
	public void run(Player player, ObjectEventContext context) {
		GameObject object = context.getObject();
		ObjectDefinition objectDef = object.getDefinitions();
		player.faceObject(object);
		if (player.getDetails().isStaff()) {
			player.getTransmitter().sendMessage("[<col=FF5733>ObjectEvent</col>] <col=355EF6>ClickOption: " + context.getOption().toString() + " - ObjectID: " + object.getId() + "</col>");
			player.getTransmitter().sendMessage("[<col=FF5733>ObjectEvent</col>] <col=355EF6>ObjectLocation: " + object.getLocation().toString() + " </col>");
		}
		if (player.getManager().getActivities().handlesNodeInteraction(object, context.getOption())) {
			return;
		}
		if (ModuleRepository.handle(player, object, context.getOption())) {
			return;
		}
		if (context.getOption() == InteractionOption.FIRST_OPTION && DialogueRepository.handleObject(player, object)) {
			return;
		}
		if (context.getOption() == InteractionOption.ITEM_ON_OBJECT) {

		}
		if (context.getOption().equals(InteractionOption.FIRST_OPTION)) {

		} else {
			player.getTransmitter().sendMessage("Nothing interesting happens.");
		}
	}

	@Override
	public boolean canStart(Player player, ObjectEventContext context) {
		return !player.getManager().getLocks().isLocked(LockType.OBJECT_INTERACTION);
	}
}
