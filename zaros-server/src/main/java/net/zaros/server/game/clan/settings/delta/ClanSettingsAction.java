package net.zaros.server.game.clan.settings.delta;

import net.zaros.cache.util.buffer.Buffer;

/**
 * Represents an abstract clan settings action. A settings action is an action
 * that modifies the settings in the client side to synchronise it with the
 * server version.
 * 
 * @author Walied K. Yassen
 */
public abstract class ClanSettingsAction {

	/**
	 * Encodes the action data into the specified {@link Buffer buffer}.
	 * 
	 * @param buffer
	 *               the buffer to encode into.
	 */
	public abstract void encode(Buffer buffer);

	/**
	 * Gets the channel action id.
	 * 
	 * @return the channel action id.
	 */
	public abstract int getId();
}
