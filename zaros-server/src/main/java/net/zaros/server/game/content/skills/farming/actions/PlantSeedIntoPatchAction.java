package net.zaros.server.game.content.system.zskillsystem.farming.actions;

import net.zaros.server.game.content.system.zskillsystem.farming.impl.*;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.constant.SkillConstants;

/**
 * Handles the action of planting the seed into the patch.
 *
 * @author Gabriel || Wolfsdarker
 */
public class PlantSeedIntoPatchAction extends PlayerTask {

	/**
	 * Constructor for the action.
	 *
	 * @param player
	 * @param data
	 * @param state
	 * @param seed_data
	 */
	private PlantSeedIntoPatchAction(Player player, Patches data, PatchState state, Seeds seed_data) {
		super(3, player, () -> {

			Item seed = new Item(seed_data.getSeedItemId());

			player.getSkills().addExperienceWithMultiplier(SkillConstants.FARMING, seed_data.getExperience() * 2);

			player.getInventory().remove(seed_data.getSeedItemId(), 1);
			player.getTransmitter().sendMessage("You plant a " + seed.getDefinitions().getName().toLowerCase() + " in the " + data.getPatchType().toString().toLowerCase() + ".");

			state.resetLastStageGrowthMoment();
			state.setStage(seed_data.getMinGrowth());
			state.setDiseaseState(DiseaseState.getImmunity(player, seed_data.getSeedItemId(), data));
			state.setSeed(seed_data);

			player.endCurrentTask();
		});

		onEnd(() -> {
			player.getFarming().updatePatches(player);
			player.sendAnimation(new Animation(65535));
			player.getMovementQueue().reset();
		});

		stopUpon(MovementPacketListener.class);
	}

	/**
	 * Plants the seed into the patch.
	 *
	 * @param player
	 * @param data
	 * @param seed_id
	 * @param pos
	 * @return if it was a valid seed
	 */
	public static boolean plantSeed(Player player, Patches data, int seed_id, Location pos) {

		PatchState state = player.getFarming().getPatchStates().get(data.name());
		Seeds seed = Seeds.get(seed_id);

		if (state == null || seed == null) {
			return false;
		}

		if (state.isUsed()) {
			player.getTransmitter().sendMessage("This patch already has a seed planted in it.");
			return false;
		}

		if (data.getPatchType() != seed.getType()) {
			player.getTransmitter().sendMessage("You can't plant this seed in this patch.");
			return false;
		}

		if (state.getWeedStage() < 3) {
			player.getTransmitter().sendMessage("You must rake this patch before planting on it.");
			return false;
		}

		if (!player.getInventory().containsItem(FarmingConstants.SEED_DIBBER)) {
			player.getTransmitter().getPacketSender().sendMessage("You need a seed dibber to plant seeds.");
			return true;
		}

		if (seed.getLevelReq() > player.getSkills().getLevel(SkillConstants.FARMING)) {
			player.getTransmitter().sendMessage("You need at least " + seed.getLevelReq() + " in farming to do this.");
			return true;
		}

		player.faceLocation(pos);
		player.sendAnimation(new Animation(FarmingConstants.SEED_DIBBING));
		TaskManager.submit(new PlantSeedIntoPatchAction(player, data, state, seed));
		return true;
	}
}
