package net.zaros.server.game.node.entity.hit;

/**
 * Handles the hit modifiers.
 *
 * @author Gabriel || Wolfsdarker
 */
public class HitModifier {

	/**
	 * The modifier for the damage.
	 */
	private double modifier;

	/**
	 * If the hit should ignore accuracy.
	 */
	private boolean ignoreAccuracy;

	/**
	 * The modifier type.
	 */
	private HitModifierType modifierType;

	/**
	 * Constructor for hit modifier.
	 *
	 * @param modifier
	 * @param type
	 */
	public HitModifier(double modifier, HitModifierType type, boolean ignoreAccuracy) {
		this.modifier = modifier;
		this.modifierType = type;
		this.ignoreAccuracy = ignoreAccuracy;
	}

	/**
	 * Returns the modifier.
	 *
	 * @return the modifier
	 */
	public double getModifier() {
		return modifier;
	}

	/**
	 * Returns if the hit should be ignoreAccuracy.
	 *
	 * @return if is ignoreAccuracy
	 */
	public boolean ignoreAccuracy() {
		return ignoreAccuracy;
	}

	/**
	 * Returns the hit's modifier type.
	 *
	 * @return the type
	 */
	public HitModifierType getType() {
		return modifierType;
	}

}
