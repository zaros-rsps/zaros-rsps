package net.zaros.server.game.content.zskillsystem;

import net.zaros.server.game.content.zskillsystem.segment.AbstractSkillSegment;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author Jacob Rhiel
 */
public interface ISkill {

    /** A set contained all of the {@link AbstractSkillSegment} for the {@link AbstractSkill}. **/
    Set<AbstractSkillSegment> segments = new CopyOnWriteArraySet<>();

    /**
     * Adds the {@link AbstractSkillSegment} to the set.
     * @param segment The {@link AbstractSkillSegment} to add.
     * @return Whether the segment was added or not.
     */
    default boolean addSegment(AbstractSkillSegment segment) {
        return segments.contains(segment) ? false : segments.add(segment);
    }

    /**
     * Removes the {@link AbstractSkillSegment} from the set.
     * @param segment The {@link AbstractSkillSegment} to remove.
     * @return Whether the segment was removed or not.
     */
    default boolean removeSegment(AbstractSkillSegment segment) {
        return segments.contains(segment) ? segments.remove(segment) : false;
    }

    default AbstractSkillSegment[] getSegments() {
        return (AbstractSkillSegment[]) segments.toArray();
    }

    /** Instantiates the {@link AbstractSkillSegment}'s for use. **/
    void instantiateSegments();

}
