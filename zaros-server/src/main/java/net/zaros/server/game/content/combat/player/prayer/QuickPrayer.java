package net.zaros.server.game.content.combat.player.prayer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.AccessMaskBuilder;

/**
 * @author Jacob Rhiel
 */
@AllArgsConstructor
@Getter
public class QuickPrayer {

    private final Player player;

    public void openQuickPrayerConfiguration() {
        player.getTransmitter().send(new AccessMaskBuilder(271, 42, 0, 2, 0, 29).build(player));
    }

}
