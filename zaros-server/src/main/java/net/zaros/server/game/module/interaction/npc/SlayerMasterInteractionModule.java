package net.zaros.server.game.module.interaction.npc;

import net.zaros.server.game.content.market.shop.ShopRepository;
import net.zaros.server.game.module.type.NPCInteractionModule;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.InteractionOption;

public class SlayerMasterInteractionModule extends NPCInteractionModule {

    public SlayerMasterInteractionModule() {
        register("Vannaka", "Duradel", "Mazchna", "Chaeldar");
    }

    @Override
    public boolean handle(Player player, NPC npc, InteractionOption option) {
        String nodeOption = npc.getDefinitions().getOptions()[option.ordinal() == 1 ? 0 : option.ordinal()];
        if(nodeOption.equalsIgnoreCase("rewards")) {
            player.getManager().getSlayer().openSlayerRewards(player);
        }
        if(nodeOption.equalsIgnoreCase("trade")) {
            ShopRepository.open(player, 3);
        }
        return true;
    }
}
