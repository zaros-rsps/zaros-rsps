package net.zaros.server.game.node.entity.player.link;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.content.action.Action;
import net.zaros.server.game.node.entity.Entity;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/1/2017
 */
public final class ActionManager {
	
	/**
	 * The player
	 */
	@Setter
	private transient Entity entity;
	
	/**
	 * The action
	 */
	@Getter
	private Action action;
	
	/**
	 * The delay until the action is processed
	 */
	@Getter
	private int delay;
	
	/**
	 * Sets the action
	 *
	 * @param action
	 * 		The action
	 */
	public void startAction(Action action) {
		stopAction();
		if (!action.start(entity)) {
			return;
		}
		if(entity.isPlayer()) {
			entity.toPlayer().getManager().getInterfaces().closeChatboxInterface();
		}
		this.action = action;
	}
	
	/**
	 * Forces the action to stop
	 */
	public void stopAction() {
		if (action == null) {
			return;
		}
		action.stop(entity);
		action = null;
	}
	
	/**
	 * Handles the processing of the action
	 */
	public void process() {
		if (action != null) {
			if (entity.isDead() || !action.process(entity)) {
				stopAction();
			}
		}
		if (delay > 0) {
			--delay;
			return;
		}
		if (action == null) {
			return;
		}
		int tickDelay = action.processOnTicks(entity);
		if (tickDelay == -1) {
			stopAction();
			return;
		}
		this.delay += tickDelay;
	}
	
	/**
	 * Adds onto the existing delay
	 *
	 * @param delay
	 * 		The delay
	 */
	public void addDelay(int delay) {
		this.delay = this.delay + delay;
	}
	
}