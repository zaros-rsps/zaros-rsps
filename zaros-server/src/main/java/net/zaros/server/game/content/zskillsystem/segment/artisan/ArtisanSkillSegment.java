package net.zaros.server.game.content.zskillsystem.segment.artisan;

import net.zaros.server.game.content.zskillsystem.properties.SkillProperties;
import net.zaros.server.game.content.zskillsystem.properties.status.SkillStatus;
import net.zaros.server.game.content.zskillsystem.properties.type.SkillType;
import net.zaros.server.game.content.zskillsystem.segment.AbstractSkillSegment;
import net.zaros.server.game.node.entity.player.Player;

/**
 * The artisan segment definition.
 * @author Jacob Rhiel
 */
public abstract class ArtisanSkillSegment extends AbstractSkillSegment {

    /**
     * Sets the default constructor defining the {@link SkillProperties} per segment.
     * @param enabled If the segment is enabled by default.
     * @param skillId The skill id of the 'parent' of the segment.
     * @param status The accessibility status of the segment.
     */
    public ArtisanSkillSegment(boolean enabled, int skillId, SkillStatus status) {
        super(new SkillProperties(enabled, skillId, status, SkillType.ARTISAN));
    }

}
