package net.zaros.server.game.node.entity.data.weapon.impl.specials;

import lombok.Getter;
import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.content.combat.script.impl.specials.SnapshotCombatScript;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;
import net.zaros.server.utility.rs.Projectile;

public class Snapshot implements WeaponSpecial {

    /**
     * The special's combat script.
     */
    private static final SnapshotCombatScript script = new SnapshotCombatScript();

    /**
     * The special's instance.
     */
    @Getter
    private static final Snapshot instance = new Snapshot();

    @Override
    public int attackAnimation() {
        return 1074;
    }

    @Override
    public int attackGraphics() {
        return 256;
    }

    @Override
    public int targetGraphics() {
        return 0;
    }

    @Override
    public int attackSpeed() {
        return 4;
    }

    @Override
    public int specialRequired() {
        return 55;
    }

    @Override
    public Hit.HitSplat hitStyle() {
        return Hit.HitSplat.RANGE_DAMAGE;
    }

    @Override
    public CombatScript combatScript() {
        return script;
    }

    @Override
    public void sendProjectile(Entity attacker, Entity target) {
        ProjectileManager.sendProjectile(new Projectile(attacker, target, 249, 43, 31, 21, 49, 0, 0));
        ProjectileManager.sendProjectile(new Projectile(attacker, target, 249, 43, 31, 49, 74, 0, 0));
    }
}
