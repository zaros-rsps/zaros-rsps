package net.zaros.server.game.content.dialogue.messages;

import net.zaros.server.game.content.dialogue.DialogueMessage;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/10/2017
 */
public class ActionDialogueMessage extends DialogueMessage {
	
	/**
	 * The action to perform
	 */
	private final Runnable action;
	
	public ActionDialogueMessage(Runnable action) {
		this.action = action;
	}
	
	@Override
	public void send(Player player) {
		action.run();
		player.getManager().getDialogues().handleOption(-1, -1);
	}
}
