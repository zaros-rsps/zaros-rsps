package net.zaros.server.game.content.system.zskillsystem.farming.impl;

/**
 * Handles the patch protection states.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum PatchProtection {

	/**
	 * The regular state of the patch, not protected by a farmer.
	 */
	NOT_PROTECTED,

	/**
	 * The state where the farmer will watch over your crops.
	 */
	PROTECTED

}
