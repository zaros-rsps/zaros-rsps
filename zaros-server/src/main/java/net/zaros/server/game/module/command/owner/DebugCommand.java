package net.zaros.server.game.module.command.owner;

import net.zaros.cache.type.ConfigLoader;
import net.zaros.cache.util.Utils;
import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.AccessMaskBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.CS2ScriptBuilder;

@CommandManifest(description = "You never know what this will do!")
public class DebugCommand extends CommandModule {

	@Override
	public String[] identifiers() {
		return arguments("dbg");
	}

	@Override
	public void handle(Player player, String[] args, boolean console) {
		player.getTransmitter().send(new CS2ScriptBuilder(4314).build(player));
		for (int i = 0; i < Utils.getInterfaceDefinitionsComponentsSize(ConfigLoader.store, 1096); i++) {
			player.getTransmitter().send(new AccessMaskBuilder(1096, i, 0, 5, 0x2).build(player)); // after 0 ms.
		}
	}
}