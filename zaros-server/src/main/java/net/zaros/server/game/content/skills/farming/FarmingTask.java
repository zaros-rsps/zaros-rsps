package net.zaros.server.game.content.system.zskillsystem.farming;

import net.zaros.server.game.node.entity.player.Player;

/**
 * The task who manages the player's farming timer when online.
 *
 * @author Gabriel || Wolfsdarker
 */
public class FarmingTask extends Task {

	private Player player;

	public FarmingTask(Player player) {
		super(501, player, false);
		this.player = player;
	}

	@Override
	protected void execute() {

		if (player == null) {
			stop();
			return;
		}

		player.getFarming().updateCropStages(player);
		player.getFarming().updatePatches(player);
	}
}
