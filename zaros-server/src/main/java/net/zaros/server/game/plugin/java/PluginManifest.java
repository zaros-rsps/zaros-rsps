package net.zaros.server.game.plugin.java;

import com.google.gson.annotations.SerializedName;
import net.zaros.server.game.plugin.IPluginManifest;

import java.util.Collection;
import java.util.Set;

/**
 * Represents the default implementation of the {@link IPluginManifest} type.
 *
 * @author Walied K. Yassen
 * @since 0.1
 */
public final class PluginManifest implements IPluginManifest {

	/**
	 * The plug-in identifier.
	 */
	@SerializedName("id")
	protected final String id;

	/**
	 * The plug-in name.
	 */
	@SerializedName("name")
	protected final String name;

	/**
	 * The plug-in description.
	 */
	@SerializedName("description")
	protected final String description;

	/**
	 * The plug-in authors.
	 */
	@SerializedName("authors")
	protected final Set<String> authors;

	/**
	 * The plug-in main-class qualified name.
	 */
	@SerializedName("main-class")
	protected final String mainClass;

	/**
	 * Constructs a new {@link PluginManifest} object instance.
	 *
	 * @param id
	 *                        the plug-in id.
	 * @param name
	 *                        the plug-in name.
	 * @param description
	 *                        the plug-in description.
	 * @param version
	 *                        the plug-in version.
	 * @param authors
	 *                        the plug-in authors list.
	 * @param mainClass
	 *                        the plug-in main-class name.
	 */
	public PluginManifest(String id, String name, String description, Set<String> authors, String mainClass) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.authors = authors;
		this.mainClass = mainClass;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPluginManifest#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPluginManifest#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPluginManifest#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPluginManifest#getAuthors()
	 */
	@Override
	public Collection<String> getAuthors() {
		return authors;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPluginManifest#getMainClass()
	 */
	@Override
	public String getMainClass() {
		return mainClass;
	}
}
