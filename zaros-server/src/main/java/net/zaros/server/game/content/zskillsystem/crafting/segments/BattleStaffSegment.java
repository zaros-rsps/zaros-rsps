package net.zaros.server.game.content.zskillsystem.crafting.segments;

import net.zaros.server.game.content.zskillsystem.impl.ArtisanSkill;
import net.zaros.server.game.content.zskillsystem.properties.SkillProperties;
import net.zaros.server.game.content.zskillsystem.properties.status.SkillStatus;
import net.zaros.server.game.content.zskillsystem.segment.artisan.ArtisanSkillSegment;
import net.zaros.server.game.content.zskillsystem.trigger.SkillTrigger;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.PlayerInventory;

/**
 * @author Jacob Rhiel
 */
public class BattleStaffSegment extends ArtisanSkillSegment {

    /**
     * Sets the default constructor defining the {@link SkillProperties} per segment.
     */
    public BattleStaffSegment(ArtisanSkill parent) {
        super(true, parent.getSkillId(), SkillStatus.DEVELOPMENTAL);
    }

    @Override
    public boolean hasRequirements(Player player) {
        PlayerInventory inventory = player.getInventory();
        return false;
    }

    @Override
    public boolean trigger(Player player, SkillTrigger trigger) {
        return false;
    }

    @Override
    protected void execute() {

    }
}
