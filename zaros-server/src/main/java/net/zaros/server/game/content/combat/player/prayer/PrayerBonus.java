package net.zaros.server.game.content.combat.player.prayer;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Jacob Rhiel
 */
@AllArgsConstructor
@Getter
public class PrayerBonus {

    private final int stat;

    private int base;

    private final double adjustment;

    public PrayerBonus(int stat, double adjustment) {
        this(stat, 0, adjustment);
    }

}
