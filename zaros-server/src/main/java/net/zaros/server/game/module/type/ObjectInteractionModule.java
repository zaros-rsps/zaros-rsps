package net.zaros.server.game.module.type;

import lombok.Getter;
import net.zaros.server.game.module.interaction.InteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.InteractionOption;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/27/2017
 */
public abstract class ObjectInteractionModule implements InteractionModule {
	
	/**
	 * The object ids that are subscribed to the module.
	 */
	@Getter
	List<Integer> objectIds = new ArrayList<>();

	/**
	 * The object names that are subscribed to the module.
	 */
	@Getter
	List<String> objectNames = new ArrayList<>();

	/**
	 * The object options that are subscribed to the module.
	 */
	@Getter
	List<String> objectOptions = new ArrayList<>();

	/**
	 * Registers the object id to this module.
	 * @param id The id of the object
	 */
	public void register(int... id) {
		Arrays.stream(id).forEach(objectId -> {
			if(!ArrayUtils.contains(getObjectIds().toArray(), objectId))
				getObjectIds().add(objectId);
		});
	}

	/**
	 * Registers the object name to this module.
	 * @param name The name of the object.
	 */
	public void register(String... name) {
		Arrays.stream(name).forEach(objectName -> {
			if(!ArrayUtils.contains(getObjectNames().toArray(), objectName))
				getObjectNames().add(objectName);
		});
	}

	/**
	 * Registers an object option this module.
	 * @param option The option of the object
	 */
	public void registerOption(String... option) {
		Arrays.stream(option).forEach(objectOption -> {
			if(!ArrayUtils.contains(getObjectOptions().toArray(), objectOption))
				getObjectOptions().add(objectOption);
		});
	}

	/**
	 * Handles the interaction with the module
	 *
	 * @param player
	 * 		The player interacting
	 * @param object
	 * 		The object interacting with
	 * @param option
	 * 		The option clicked on the object
	 * @return {@code True} if the interaction was successful
	 */
	public abstract boolean handle(Player player, GameObject object, InteractionOption option);
	
}
