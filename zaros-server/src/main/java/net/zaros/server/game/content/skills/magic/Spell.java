package net.zaros.server.game.content.skills.magic;

import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.constant.MagicConstants;

import java.util.List;

/**
 * A simple interface for spells in game.
 *
 * @author Gabriel || Wolfsdarker
 */
public interface Spell {

    /**
     * The required equipments empty list.
     */
    List<Item> requiredEquipments = List.of();

    /**
     * The spell's ID.
     *
     * @return the ID
     */
    int spellID();

    /**
     * The spell's magic level requirement.
     *
     * @return the level
     */
    int magicLevelRequirement();

    /**
     * The spell's maximum hit.
     *
     * @return the maximum hit amount
     */
    int maximumHit(Entity entity);

    /**
     * The delay between each spell.
     *
     * @return the delay
     */
    int attackDelay();

    /**
     * The spell's base experience to be received on cast.
     *
     * @return the experience
     */
    double baseExperience();

    /**
     * The magic book the spell belongs to.
     *
     * @return the magic book
     */
    MagicConstants.MagicBook magicBook();

    /**
     * The list of required items to cast the spell. (Runes included).
     *
     * @return the items
     */
    List<Item> requiredItems();

    /**
     * Returns if the entity is able to cast the spell.
     *
     * @param source
     * @return if its possible
     */
    boolean ableToCast(Entity source);

    /**
     * Returns if the source is able to cast the spell on another entity.
     *
     * @param source
     * @param target
     * @return if its possible
     */
    default boolean ableToCastOnEntity(Entity source, Entity target) {
        return false;
    }

    /**
     * Returns if the source is able to cast the spell on an item.
     *
     * @param source
     * @param targetItem
     * @return if is possible
     */
    default boolean ableToCastItem(Entity source, Item targetItem) {
        return false;
    }

    /**
     * Returns if the source is able to cast the spell on an object.
     *
     * @param source
     * @param targetObject
     * @return if is possible
     */
    default boolean ableToCastObject(Entity source, GameObject targetObject) {
        return false;
    }

    /**
     * Casts the spell without a target.
     *
     * @param source
     */
    void cast(Entity source);

    /**
     * If the effect can be casted on more than one entity at the same time.
     *
     * @return if its multi
     */
    default boolean multi() {
        return false;
    }

    /**
     * The spell's autocast config ID.
     *
     * @return the config ID
     */
    default int autocastConfigID() {
        return 0;
    }

    /**
     * The spell's minimum hit.
     *
     * @return the minimum hit amount
     */
    default int minimumHit(Entity entity) {
        return 0;
    }

    /**
     * The required equipments to cast the spell.
     *
     * @return the equipments
     */
    default List<Item> requiredEquipments() {
        return requiredEquipments;
    }

    /**
     * Casts the spell on a entity target.
     *
     * @param source
     * @param target
     */
    default void castOnEntity(Entity source, Entity target) {

    }

    /**
     * Casts the spell on a item target.
     *
     * @param source
     * @param targetItem
     */
    default void castOnItem(Entity source, Item targetItem) {

    }

    /**
     * Casts the spell on a object target.
     *
     * @param source
     * @param targetObject
     */
    default void castOnObject(Entity source, GameObject targetObject) {

    }

    /**
     * Deletes the required items.
     *
     * @param player
     */
    default void deleteRequiredItems(Player player) {
        var items = requiredItems();
        if (!items.isEmpty()) {
            for (Item item : items) {
                player.getInventory().deleteItem(item.getId(), item.getAmount());
            }
        }
    }
}
