package net.zaros.server.game.content.combat.script;

import net.zaros.server.game.content.combat.CombatManager;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.Hit;
import net.zaros.server.utility.rs.constant.SkillConstants;

/**
 * Handles the execution of combat scripts.
 *
 * @author Gabriel || Wolfsdarker
 */
public class CombatScriptExecutor {

    /**
     * Selects and runs the script for the character.
     *
     * @param entity
     */
    public static void run(Entity entity) {

        if (entity.getSkills().getLevel(SkillConstants.HITPOINTS) < 1 || entity.isDying()) {
            return;
        }

        int attackDelay = entity.getTemporaryAttribute(AttributeKey.ATTACK_DELAY, 0);

        if (attackDelay > 0) {
            return;
        }

        Entity target = entity.getTemporaryAttribute(AttributeKey.TARGET);

        if (target == null) {
            return;
        }

        if (!CombatManager.canAttackCharacter(entity, target)) {
            return;
        }

        CombatScript script = CombatScriptSelection.get(entity);

        script.preExecution(entity, target);

        if (!CombatManager.isWithinDistance(entity, target, script, false)) {
            return;
        }

        if (!script.executable(entity, target)) {
            CombatManager.endCombat(entity);
            return;
        }

        entity.putTemporaryAttribute(AttributeKey.COMBAT_SCRIPT, script);

        // entity.getBlockTimer().start(1);

        script.execute(entity, target);

        entity.putTemporaryAttribute(AttributeKey.LAST_ATTACK_INSTANT, System.currentTimeMillis());

       /* if (entity.isPlayer() && target.isPlayer()) {
            CombatManager.handleSkull(entity.getAsPlayer(), target.getAsPlayer());
        }*/

        Hit[] hits = script.getHits(entity, target);

        if (hits != null && hits.length > 0) {
            for (Hit hit : hits) {
                target.getHitQueue().add(hit);
            }
        }

        int scriptDelay = script.getSpeed(entity, target);

        target.putTemporaryAttribute(AttributeKey.ATTACKER, entity);

        entity.putTemporaryAttribute(AttributeKey.LAST_TARGET, target);

        entity.putTemporaryAttribute(AttributeKey.ATTACK_DELAY, scriptDelay);

        script.postExecution(entity, target);
    }

}
