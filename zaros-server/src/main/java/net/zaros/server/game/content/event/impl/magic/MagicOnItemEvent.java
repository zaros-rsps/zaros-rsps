package net.zaros.server.game.content.event.impl.magic;

import net.zaros.server.game.content.combat.player.CombatRegistry;
import net.zaros.server.game.content.event.Event;
import net.zaros.server.game.content.event.EventPolicy;
import net.zaros.server.game.content.event.context.magic.MagicOnItemContext;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;

public class MagicOnItemEvent extends Event<MagicOnItemContext> {

    /**
     * Constructs a new event
     */
    public MagicOnItemEvent() {
        setInterfacePolicy(EventPolicy.InterfacePolicy.CLOSE);
        setAnimationPolicy(EventPolicy.AnimationPolicy.RESET);
        setWalkablePolicy(EventPolicy.WalkablePolicy.RESET);
        setActionPolicy(EventPolicy.ActionPolicy.RESET);
    }

    @Override
    public void run(Player player, MagicOnItemContext context) {
        int interface_id = context.getInterfaceId();
        int spell_id = context.getSpellId();
        int item_slot = context.getItemSlot();

        Item item = player.getInventory().getItems().get(item_slot);

        if (item == null)
            return;

        player.getTransmitter().sendMessage("Spell #: " + spell_id + ", interface: " + interface_id);

        CombatRegistry.processSpell(player, spell_id);

    }
}
