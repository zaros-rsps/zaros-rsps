package net.zaros.server.game.content.combat.script.impl.formulas.impl;

import net.zaros.server.game.content.combat.StaticCombatFormulae;
import net.zaros.server.game.content.combat.script.impl.formulas.CombatFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.rs.constant.BonusConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

public class MagicAttackRoll implements CombatFormula {

    @Override
    public int get(Entity attacker, Entity target, boolean specialAttack) {

        var magicLevel = attacker.getSkills().getLevel(SkillConstants.MAGIC) * 1.0;

        final double prayerBoost = attacker.getPrayers().getBasePrayerBoost(SkillConstants.MAGIC);
        magicLevel *= prayerBoost;

        magicLevel = Math.round(magicLevel);

        magicLevel += 8;

        if (attacker.isPlayer() && StaticCombatFormulae.fullVoidEquipped(attacker.toPlayer(), 11663)) {
            magicLevel *= 1.45;
        }

        var attackBonus = attacker.getBonuses()[BonusConstants.MAGIC_ATTACK];

        magicLevel = Math.round(magicLevel);
        attackBonus = Math.round(attackBonus);

        var attackRoll = Math.round(magicLevel * (64 + attackBonus)) * 1.0;

        return (int) Math.round(attackRoll);
    }
}