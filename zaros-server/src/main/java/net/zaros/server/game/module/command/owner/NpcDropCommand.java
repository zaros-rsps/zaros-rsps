package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.module.interaction.rsinterface.NPCDropInterfaceInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.tool.Misc;

public class NpcDropCommand extends CommandModule {

    @Override
    public String[] identifiers() {
        return arguments("npcdrops");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {
        NPCDropInterfaceInteractionModule.display(player, Integer.valueOf(Misc.getArrayEntry(args, 1)));
    }
}
