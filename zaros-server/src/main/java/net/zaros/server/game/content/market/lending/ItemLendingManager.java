package net.zaros.server.game.content.market.lending;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.network.world.packet.outgoing.impl.AccessMaskBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigFilePacketBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.ContainerPacketBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.InterfaceItemBuilder;

public class ItemLendingManager {

    @Getter
    @Setter
    public transient Player player;

    @Getter
    @Setter
    public LentItem lentItem;

    @Getter
    @Setter
    public int recentDuration = 0;

    public void lend(Player target, Item item, int slot) {

        if (item == null) {
			return;
		}

        boolean lendable = item.getDefinitions().getLentLink() != -1;

        if(!lendable) {
            player.getTransmitter().sendMessage("You cannot lend this item.");
            return;
        }

        if (lentItem != null) {
            player.getTransmitter().sendMessage("You can only lend one item at a time");
            return;
        }

        //TODO: Target has item

        //TODO: bounds for areas cannot lend.

        sendUpdateMasks(target);

        sendDurationConfigs(target);

        buildItems(target, item);

        buildContainers(target, item, false);

        setLentItem(new LentItem(target.getDetails().getDisplayName(), item, recentDuration));
        player.getInventory().deleteItem(slot, item);

    }

    public void completeLend(Player target, int slot) {
        Item item = getLentItem().getLentItem();
        target.getInventory().addItem(item.getDefinitions().getLentLink(), 1);
    }

    public void reset(Player target) {
        removeLendItem(target, false);
    }

    public void cancel(Player target) {
        removeLendItem(target, true);
    }

    private void removeLendItem(Player target, boolean returnItem) {
        if(getLentItem() != null && returnItem) {
			player.getInventory().addItem(getLentItem().getLentItem());
		}
        buildContainers(target, null, true);
        setLentItem(null);
    }

    public void updateLendTime(Player target, int time) {

        if (time > 24) {
			time = 24;
		}

        player.getTransmitter().send(new ConfigFilePacketBuilder(5026, time).build(player));//TODO: 0-24

        target.getTransmitter().send(new ConfigFilePacketBuilder(5070, time).build(target));

        player.getManager().getLendingManager().getLentItem().setDuration(time);

    }

    private void sendUpdateMasks(Player target) {
        player.getTransmitter().send(new AccessMaskBuilder(335, 56, -1, 6, false, 0, 1, 9).build(player));
        player.getTransmitter().send(new AccessMaskBuilder(335, 57, -1, 6, false, 0, 1, 9).build(player));
        target.getTransmitter().send(new AccessMaskBuilder(335, 52, -1, 6, false, 9).build(target));
    }

    private void sendDurationConfigs(Player target) {
        player.getTransmitter().send(new ConfigFilePacketBuilder(5026, recentDuration).build(player));
        target.getTransmitter().send(new ConfigFilePacketBuilder(5070, recentDuration).build(target));
    }

    private void buildItems(Player target, Item item) {
        target.getTransmitter().send(new InterfaceItemBuilder(335, 52, item.getId(), 1).build(target));
    }

    private void buildContainers(Player target, Item item, boolean remove) {
        player.getTransmitter().send(new ContainerPacketBuilder(541, remove ? new Item[0] : new Item[]{item}).build(player));
        target.getTransmitter().send(new ContainerPacketBuilder(541, remove ? new Item[0] : new Item[]{item}, true).build(target));
    }

}
