package net.zaros.server.game.content.combat.script;

import lombok.Getter;
import net.zaros.server.game.content.combat.script.impl.formulas.CombatFormula;
import net.zaros.server.game.content.combat.script.impl.formulas.impl.*;

public class CombatConstants {

    /**
     * The instance for melee max hit formula.
     */
    @Getter
    private static final CombatFormula meleeMaxHit = new MeleeMaxHit();

    /**
     * The instance for ranged max hit formula.
     */
    @Getter
    private static final CombatFormula rangeMaxHit = new RangeMaxHit();

    /**
     * The instance for magic max hit formula.
     */
    @Getter
    private static final CombatFormula magicMaxHit = new MagicMaxHit();

    /**
     * The instance for melee attack roll.
     */
    @Getter
    private static final CombatFormula meleeAttackRoll = new MeleeAttackRoll();

    /**
     * The instance for range attack roll.
     */
    @Getter
    private static final CombatFormula rangeAttackRoll = new RangeAttackRoll();

    /**
     * The instance for magic attack roll.
     */
    @Getter
    private static final CombatFormula magicAttackRoll = new MagicAttackRoll();

    /**
     * The instance for melee defence roll.
     */
    @Getter
    private static final CombatFormula meleeDefenceRoll = new MeleeDefenceRoll();

    /**
     * The instance for range defence roll.
     */
    @Getter
    private static final CombatFormula rangeDefenceRoll = new RangeDefenceRoll();

    /**
     * The instance for magic defence roll.
     */
    @Getter
    private static final CombatFormula magicDefenceRoll = new MagicDefenceRoll();

}
