package net.zaros.server.game.content.combat.script.impl;

import net.zaros.server.game.content.combat.StaticCombatFormulae;
import net.zaros.server.game.content.combat.player.CombatType;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.CombatEquipments;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.repository.npc.Npcs;
import net.zaros.server.utility.rs.Hit;
import net.zaros.server.utility.tool.RandomFunction;

public class MeleeCombatScript implements CombatScript {

    @Override
    public boolean executable(Entity attacker, Entity target) {
        if (target.isNPC() && target.toNPC().getId() == Npcs.KREEARRA) {
            if (attacker.isPlayer()) {
                attacker.toPlayer().getTransmitter().sendMessage("You cannot reach Kree'arra.");
            }
            return false;
        }
        return true;
    }

    @Override
    public int getAttackDistance(Entity attacker, Entity target) {
        return 1;
    }

    @Override
    public void preExecution(Entity attacker, Entity target) {

    }

    @Override
    public void execute(Entity attacker, Entity target) {
        int animation = attacker.isNPC() ? attacker.toNPC().getCombatDefinitions().getAttackAnim() : -1;

        WeaponInterface weapon = attacker.getTemporaryAttribute(AttributeKey.WEAPON);

        if(weapon != null) {
            animation = weapon.attackStyles().get(attacker.getCombatDefinitions().getAttackStyle()).getAnimationId();
        }

        if (animation != -1) {
            attacker.sendAnimation(animation);
        }
    }

    @Override
    public Hit.HitSplat getHitSplat(Entity attacker, Entity target) {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public Hit[] getHits(Entity attacker, Entity target) {

        CombatHit hit = new CombatHit(attacker, target, this, Hit.HitSplat.MELEE_DAMAGE);

        if (CombatEquipments.fullVeracs(attacker) && RandomFunction.randomFloat() <= 0.25F) {
            hit.getAttributes().put(Hit.HitAttributes.VERACS_EFFECT, true);
            hit.setDamage(hit.getDamage() + 1);
        }

        if (CombatEquipments.fullGuthans(attacker) && RandomFunction.randomFloat() <= 0.25F) {
            hit.getAttributes().put(Hit.HitAttributes.GUTHANS_EFFECT, true);
        }

        return new Hit[] { hit };
    }

    @Override
    public int getSpeed(Entity attacker, Entity target) {

        WeaponInterface weapon = attacker.getTemporaryAttribute(AttributeKey.WEAPON);

        if(weapon != null) {
            return weapon.attackStyles().get(attacker.getCombatDefinitions().getAttackStyle()).getAttackSpeed();
        }

        return attacker.isNPC() ?
                attacker.toNPC().getCombatDefinitions().getAttackDelay() :
                CombatType.MELEE.getDelay(attacker.toPlayer(), attacker.toPlayer().getEquipment().getWeaponId());
    }

    @Override
    public boolean specialAttack(Entity attacker, Entity target) {
        return false;
    }

    @Override
    public void postExecution(Entity attacker, Entity target) {
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.getSource() != null) {
            if (hit.getAttribute(Hit.HitAttributes.GUTHANS_EFFECT, false)) {
                if (hit.getDamage() > 0) {
                    //TODO: Guthans effect
                    //PostHitEffects.handleGuthans(hit.attacker, hit.target, hit.damage.value)
                }
            }
        }
    }
}
