package net.zaros.server.game.content.combat.player.registry.spell.lunar.teleport;

import net.zaros.server.Configuration;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.TeleportationSpellEvent;
import net.zaros.server.game.node.Location;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/27/2017
 */
public class HomeTeleportSpell implements TeleportationSpellEvent {

	@Override
	public int levelRequired() {
		return 0;
	}

	@Override
	public int[] runesRequired() {
		return new int[0];
	}

	@Override
	public Location destination() {
		return Configuration.World.SPAWN_LOCATION;
	}

	@Override
	public int spellId() {
		return 39;
	}

	@Override
	public double exp() {
		return 0;
	}

	@Override
	public MagicBook book() {
		return MagicBook.LUNARS;
	}
}
