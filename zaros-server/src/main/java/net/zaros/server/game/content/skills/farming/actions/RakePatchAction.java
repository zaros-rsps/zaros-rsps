package net.zaros.server.game.content.system.zskillsystem.farming.actions;

import net.zaros.server.game.content.system.zskillsystem.farming.impl.FarmingConstants;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.PatchState;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;
import net.zaros.server.utility.rs.constant.SkillConstants;

/**
 * Handles the action of removing weeds from patches.
 *
 * @author Gabriel || Wolfsdarker
 */
public class RakePatchAction extends PlayerTask {
	/**
	 * Constructor for the action.
	 *
	 * @param player
	 * @param state
	 */
	private RakePatchAction(Player player, PatchState state, Location pos) {
		super(5, player, () -> {

			player.faceLocation(pos);
			player.sendAnimation(new Animation(FarmingConstants.RAKING_ANIM));

			if (state.getWeedStage() < 3) {

				state.setWeedStage(state.getWeedStage() + 1);
				state.resetLastStageGrowthMoment();
				player.getInventory().addItem(6055, 1);
				player.getSkills().addExperienceWithMultiplier(SkillConstants.FARMING, 10);

			}

			if (state.getWeedStage() >= 3) {
				state.setWeedStage(3);
				state.resetLastStageGrowthMoment();
				player.getTransmitter().sendMessage("You have successfully cleared this patch for new crops.");
				player.endCurrentTask();

			}

			player.getFarming().updatePatches(player);
		});

		onEnd(() -> {
			player.sendAnimation(new Animation(65535));
			player.getMovementQueue().reset();
		});

		stopUpon(MovementPacketListener.class);
	}

	/**
	 * Returns if the player can rake the patch.
	 *
	 * @param player
	 * @return if it can be done
	 */
	public static void rakePatch(Player player, PatchState state, Location position) {
		if (!player.getInventory().containsItem(FarmingConstants.RAKE)) {
			player.getTransmitter().sendMessage("You need a rake to clear this patch.");
			return;
		}
		if(state.getWeedStage() >= 3) {
			return;
		}

		player.faceLocation(position);
		player.sendAnimation(new Animation(FarmingConstants.RAKING_ANIM));
		TaskManager.submit(new RakePatchAction(player, state, position));
	}
}
