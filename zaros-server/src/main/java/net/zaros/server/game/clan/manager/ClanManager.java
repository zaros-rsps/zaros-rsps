package net.zaros.server.game.clan.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import net.zaros.server.game.node.entity.areas.impl.ClanCampArea;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.master.client.packet.out.ClanChannelRequestPacketOut;
import net.zaros.server.network.master.client.packet.out.ClanCreateRequestPacketOut;
import net.zaros.server.network.master.client.packet.out.ClanInitialisePacketOut;
import net.zaros.server.utility.AttributeKey;

/**
 * Represents the clan manager.
 * 
 * @author Walied K. Yassen
 */
public final class ClanManager {

	/**
	 * The amount of founders that are required to initiate a clan.
	 */
	public static final int FOUNDERS_REQUIRED = 1;

	public static void handleButton(Player player, int interfaceId, int buttonId, int slotId) {
		if (interfaceId == 1110) {
			if (buttonId == 85) {
			}
		}
	}

	/**
	 * Requests to initialise the clan system for the specified {@code player}.
	 * 
	 * @param player
	 *               the player to initialise for.
	 */
	public static void requestInitialise(Player player) {
		MasterCommunication.write(new ClanInitialisePacketOut(player.getUserId(), player.getCurrentClanId(), player.isJoinedClanChannel()));
	}

	/**
	 * Sends a clan channel join request to the master server.
	 * 
	 * @param player
	 *               the player who wants to join the channel.
	 * @param clan
	 *               the clan id of the channel we want to join.
	 */
	public static void requestChannelJoin(Player player, long clanId) {
		MasterCommunication.write(new ClanChannelRequestPacketOut(player, clanId, false));
	}

	/**
	 * Sends a clan channel join request to the master server.
	 * 
	 * @param player
	 *               the player who wants to join the channel.
	 * @param clan
	 *               the clan id of the channel we want to join.
	 */
	public static void requestChannelLeave(Player player, long clanId) {
		MasterCommunication.write(new ClanChannelRequestPacketOut(player, clanId, true));
	}

	/**
	 * Sends a clan create request to the master server.
	 * 
	 * @param player
	 *               the player who is creating the clan (owner).
	 * @param name
	 *               the name of the clan.
	 */
	public static void requestCreate(Player player, String name) {
		MasterCommunication.write(new ClanCreateRequestPacketOut(player, name));
	}

	/**
	 * Creates a list of the current valid {@link Player} players that has accepted
	 * to be founders of the {@code owner} clan.
	 * 
	 * @return the {@link List} of {@link Player}s who accepted to be founders and
	 *         meet the requirements.
	 */
	public static List<Player> getFounders(Player owner) {
		List<Long> userIds = owner.getTemporaryAttribute(AttributeKey.CLAN_FOUNDERS);
		if (userIds == null) {
			return Collections.emptyList();
		}
		var players = userIds.stream().map(World::getPlayerById).filter(Optional::isPresent).map(Optional::get).toArray(Player[]::new);
		var founders = new ArrayList<Player>();
		for (var player : players) {
			if (!player.getSession().isActive() || player.getTemporaryAttribute(AttributeKey.CLAN_IS_FOUNDER, 0L) != owner.getUserId()) {
				userIds.remove(player.getUserId());
				continue;
			}
			if (!ClanCampArea.getSingleton().isWithin(player.getLocation())) {
				userIds.remove(player.getUserId());
				continue;
			}
			founders.add(player);
		}
		return founders;
	}

	private ClanManager() {
		// NOOP
	}

}
