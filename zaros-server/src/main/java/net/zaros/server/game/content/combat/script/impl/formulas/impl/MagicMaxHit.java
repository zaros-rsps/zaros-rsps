package net.zaros.server.game.content.combat.script.impl.formulas.impl;

import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.content.combat.script.impl.formulas.CombatFormula;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.AttributeKey;

public class MagicMaxHit implements CombatFormula {

    @Override
    public int get(Entity attacker, Entity target, boolean specialAttack) {

        var maxHit = 0.0;

        CombatSpell currentSpell = attacker.getTemporaryAttribute(AttributeKey.CURRENT_SPELL);

        if (currentSpell != null) {
            maxHit = currentSpell.maximumHit(attacker);
        } else {
            //If the NPC uses magic but has no spell attached, the definition's max hit will be used.
            if (attacker.isNPC()) {
                maxHit = attacker.toNPC().getCombatDefinitions().getMaxHit();
            } else {
                //If the current spell is null for players, the damage is 0.
                return 0;
            }
        }

        var damageBoost = 1.0;

        maxHit *= damageBoost;

        return (int) Math.round(maxHit);
    }
}