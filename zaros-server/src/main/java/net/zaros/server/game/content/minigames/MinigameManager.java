/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames;

import net.zaros.server.game.content.minigames.types.multi.MultiareaMinigame;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Represents the minigame manager for our server.
 * 
 * @author Walied K. Yassen
 */
public final class MinigameManager {

	/**
	 * The current active minigames list.
	 */
	private final List<MinigameBase> games = new CopyOnWriteArrayList<>();

	/**
	 * Construct a new {@link MinigameManager} type object instance.
	 */
	public MinigameManager() {
		initialise();
	}

	/**
	 * Initialises the minigame manager.
	 */
	public void initialise() {
		// empty
	}

	/**
	 * Processes one tick for all of our minigames.
	 */
	public void process() {
		for (MinigameBase minigame : games) {
			minigame.process();
		}
	}

	/**
	 * Registers the specified {@link MultiareaMinigame} in this
	 * {@link MinigameManager}.
	 * 
	 * @param minigame
	 *                 the minigame to register.
	 */
	public void register(MinigameBase minigame) {
		games.add(minigame);
	}

	/**
	 * Unregisters the specified {@link MultiareaMinigame} from this
	 * {@link MinigameManager}.
	 * 
	 * @param minigame
	 *                 the minigame to unregister.
	 */
	public void unregister(MinigameBase minigame) {
		games.remove(minigame);
	}
}
