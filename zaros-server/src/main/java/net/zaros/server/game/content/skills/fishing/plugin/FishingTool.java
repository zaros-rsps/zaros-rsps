package net.zaros.server.game.content.skills.fishing.plugin;

import lombok.Getter;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Jacob Rhiel
 */
@Getter
public enum FishingTool implements IFishingTool {

    NONE,

    SMALL_NET(Items.SMALL_FISHING_NET1, "You cast out your net...", 621, 1, Fish.SHRIMP, Fish.ANCHOVIES, Fish.KARAMBWANJI),

    CRAYFISH_CAGE(Items.CRAYFISH_CAGE1, "You attempt to catch a crayfish", 619, 1, Fish.CRAYFISH),

    BIG_NET(Items.BIG_FISHING_NET1, "You cast out your net...", 621, 16, Fish.MACKEREL, Fish.OYSTER, Fish.CASKET, Fish.LEATHER_BOOTS, Fish.LEATHER_GLOVES, Fish.COD, Fish.SEAWEED),

    FISHING_ROD(Items.FISHING_ROD1, "You cast out your line...", 622, 5, FishingBait.BAIT, Fish.SARDINE, Fish.HERRING, Fish.PIKE, Fish.SLIMY_EEL),

    FLY_FISHING_ROD(Items.FLY_FISHING_ROD1, "You cast out your line...", 622, 20, FishingBait.FEATHERS, Fish.TROUT, Fish.SALMON, Fish.RAINBOW_FISH),

    CAGE(Items.LOBSTER_POT1, "You attempt to catch a lobster", 619, 40, Fish.LOBSTER),

    HARPOON("You start harpooning fish", 618, 35, Fish.TUNA, Fish.SWORDFISH, Fish.SHARK, Fish.BARON_SHARK) {

        @Override
        public int[] toolIds() {
            return new int[] { Items.HARPOON1, Items.BARBTAIL_HARPOON1, Items.SACRED_CLAY_HARPOON, Items.VOLATILE_CLAY_HARPOON };
        }

    },

    STEALING_CREATION_HARPOON(618, 1) {

        @Override
        public int[] toolIds() {
            return new int[] { Items.HARPOON_CLASS_11, Items.HARPOON_CLASS_21, Items.HARPOON_CLASS_31, Items.HARPOON_CLASS_41, Items.HARPOON_CLASS_51 };
        }

    },

    BARBARIAN_ROD(),//You cast out your line

    VESSEL(Items.KARAMBWAN_VESSEL1, "You lower the Karambwan vessel.", -1, 65, FishingBait.VESSEL, Fish.KARAMBWAN),

    SMALL_CASTING_NET,

    BIG_CASTING_NET,

    ;

    private final int itemId, animation, minimumLevel;

    private final String message;

    private final FishingBait bait;

    private final Fish[] fish;

    FishingTool() {
        this(-1, "", -1, 1, FishingBait.NONE, new Fish[0]);
    }

    FishingTool(String message, int animation, int minimumLevel, Fish... fish) {
        this(-1, message, animation, minimumLevel, FishingBait.NONE, fish);
    }

    FishingTool(int animation, int minimumLevel) {
        this(-1, "", animation, minimumLevel, new Fish[0]);
    }

    FishingTool(int itemId, String message, int animation, int minimumLevel, Fish... fish) {
        this(itemId, message, animation, minimumLevel, FishingBait.NONE, fish);
    }

    FishingTool(int itemId, String message, int animation, int minimumLevel, FishingBait bait, Fish... fish) {
        this.itemId = itemId;
        this.message = message;
        this.animation = animation;
        this.minimumLevel = minimumLevel;
        this.bait = bait;
        this.fish = fish;
    }

    public static FishingTool getTool(int itemId) {
        for(FishingTool tool : FishingTool.values()) {
            for(int id : tool.toolIds()) {
                if(id == itemId) {
                    return tool;
                }
            }
        }
        return null;
    }

    public static FishingTool forSpot(FishingSpot spot, InteractionOption option) {
        boolean first = option.equals(InteractionOption.FIRST_OPTION);
        boolean second = option.equals(InteractionOption.SECOND_OPTION);
        return first ? spot.getFirstOptionTool() : second ? spot.getSecondOptionTool() : FishingTool.NONE;
    }

}
