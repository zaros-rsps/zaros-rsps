package net.zaros.server.game.module.interaction.rsinterface;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.core.event.item.ObjEvent;
import net.zaros.server.core.event.item.ObjOption;
import net.zaros.server.game.content.event.EventRepository;
import net.zaros.server.game.content.event.context.item.ItemEventContext;
import net.zaros.server.game.content.event.impl.item.ItemDropEvent;
import net.zaros.server.game.content.event.impl.item.ItemEvent;
import net.zaros.server.game.module.type.InterfaceInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.world.World;
import net.zaros.server.network.NetworkConstants;
import net.zaros.server.utility.rs.InteractionOption;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/27/2017
 */
@Slf4j
public class InventoryInteractionModule implements InterfaceInteractionModule {

	@Override
	public int[] interfaceSubscriptionIds() {
		return Misc.arguments(INVENTORY_INTERFACE_ID);
	}

	@Override
	public boolean handle(Player player, int interfaceId, int componentId, int itemId, int slotId, int packetId) {
		Item item = player.getInventory().getItems().get(slotId);
		if (item == null) {
			return true;
		}
		InteractionOption option = getOption(packetId);
		if (option == null) {
			log.warn("Unable to find interaction option for packet: " + packetId);
			return true;
		}
		if (player.getManager().getActivities().handleInventory(item, slotId, option)) {
			return true;
		}
		if (World.getEventManager().post(new ObjEvent(player, item, slotId, toObjOption(option)))) {
			return true;
		}
		EventRepository.executeEvent(player, option == InteractionOption.DROP ? ItemDropEvent.class : ItemEvent.class, new ItemEventContext(item, slotId, option));
		return true;
	}

	/**
	 * Gets the {@code InteractionOption} {code Object} by the packet id
	 *
	 * @param packetId
	 *                 The id of the packet
	 */
	private InteractionOption getOption(int packetId) {
		switch (packetId) {
		case NetworkConstants.FIRST_PACKET_ID:
			return InteractionOption.FIRST_OPTION;
		case NetworkConstants.EQUIP_PACKET_ID:
			return InteractionOption.SECOND_OPTION;
		case NetworkConstants.OPERATE_PACKET_ID:
			return InteractionOption.THIRD_OPTION;
		case NetworkConstants.FOURTH_PACKET_ID:
			return InteractionOption.FOURTH_OPTION;
		case NetworkConstants.DROP_PACKET_ID:
			return InteractionOption.DROP;
		case NetworkConstants.EXAMINE_PACKET_ID:
			return InteractionOption.EXAMINE;
		default:
			return null;
		}
	}

	private ObjOption toObjOption(InteractionOption option) {
		switch (option) {
		case FIRST_OPTION:
			return ObjOption.FIFTH;
		case SECOND_OPTION:
			return ObjOption.SECOND;
		case THIRD_OPTION:
			return ObjOption.THIRD;
		case FOURTH_OPTION:
			return ObjOption.FOURTH;
		case FIFTH_OPTION:
			return ObjOption.FIFTH;
		case EXAMINE:
			return ObjOption.EXAMINE;
		default:
			return null;
		}
	}
}
