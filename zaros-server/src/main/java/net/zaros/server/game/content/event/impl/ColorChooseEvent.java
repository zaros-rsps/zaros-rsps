package net.zaros.server.game.content.event.impl;

import net.zaros.server.game.content.event.Event;
import net.zaros.server.game.content.event.EventPolicy;
import net.zaros.server.game.content.event.context.ColorChooseEventContext;
import net.zaros.server.game.node.entity.player.Player;

public class ColorChooseEvent extends Event<ColorChooseEventContext> {

    public ColorChooseEvent() {
        setInterfacePolicy(EventPolicy.InterfacePolicy.CLOSE);
        setWalkablePolicy(EventPolicy.WalkablePolicy.RESET);
        setActionPolicy(EventPolicy.ActionPolicy.RESET);
    }

    @Override
    public void run(Player player, ColorChooseEventContext context) {
        player.getManager().getCompletionist().customizeColor(player, context.getColor());
    }
}
