package net.zaros.server.game.content.combat.player.registry.spell.modern.alch;

import net.zaros.server.game.content.combat.player.registry.wrapper.context.MagicSpellContext;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.SkillingSpellEvent;
import net.zaros.server.game.node.entity.player.Player;

public class LowAlchemySpell implements SkillingSpellEvent {

    @Override
    public int spellId() {
        return 38;
    }

    @Override
    public int levelRequired() {
        return 21;
    }

    @Override
    public int[] runesRequired() {
        return arguments(FIRE_RUNE, 3, NATURE_RUNE, 1);
    }

    @Override
    public double exp() {
        return 31;
    }

    @Override
    public MagicBook book() {
        return MagicBook.REGULAR;
    }

    @Override
    public void cast(Player player, MagicSpellContext context) {

        player.getTransmitter().sendMessage("firing low alchemy spell..", false);

    }

}
