package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/29/2017
 */
@CommandManifest(description = "Spawns every rune")
public class SpawnRunesCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("runes");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		final int amount = 10_000;
		for (int i = 554; i <= 566; i++) {
			player.getInventory().addItem(i, amount);
		}
		player.getInventory().addItem(9075, amount);
	}
}
