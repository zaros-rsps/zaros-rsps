package net.zaros.server.game.node.entity.data.weapon.impl;

import net.zaros.server.game.node.entity.data.weapon.Ammunition;
import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.game.node.entity.data.weapon.impl.ammunitions.Arrows;
import net.zaros.server.game.node.entity.data.weapon.impl.specials.Snapshot;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.Hit;

import java.util.ArrayList;
import java.util.List;

/**
 * The interface for longbows.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum Longbow implements WeaponInterface {

    /**
     * The regular longbow weapon interface.
     */
    LONGBOW(),

    /**
     * The oak longbow weapon interface.
     */
    OAK_LONGBOW(Items.STEEL_ARROW, Items.STEEL_ARROW_P, Items.STEEL_ARROW_PP, Items.STEEL_ARROW_PPP),

    /**
     * The willow longbow weapon interface.
     */
    WILLOW_LONGBOW(Items.STEEL_ARROW, Items.STEEL_ARROW_P, Items.STEEL_ARROW_PP, Items.STEEL_ARROW_PPP,
            Items.MITHRIL_ARROW1, Items.MITHRIL_ARROW_P, Items.MITHRIL_ARROW_PP, Items.MITHRIL_ARROW_PPP),

    /**
     * The maple longbow weapon interface.
     */
    MAPLE_LONGBOW(Items.STEEL_ARROW, Items.STEEL_ARROW_P, Items.STEEL_ARROW_PP, Items.STEEL_ARROW_PPP,
            Items.MITHRIL_ARROW1, Items.MITHRIL_ARROW_P, Items.MITHRIL_ARROW_PP, Items.MITHRIL_ARROW_PPP,
            Items.ADAMANT_ARROW, Items.ADAMANT_ARROW_P, Items.ADAMANT_ARROW_PP, Items.ADAMANT_ARROW_PPP),

    /**
     * The yew longbow weapon interface.
     */
    YEW_LONGBOW(Items.STEEL_ARROW, Items.STEEL_ARROW_P, Items.STEEL_ARROW_PP, Items.STEEL_ARROW_PPP,
            Items.MITHRIL_ARROW1, Items.MITHRIL_ARROW_P, Items.MITHRIL_ARROW_PP, Items.MITHRIL_ARROW_PPP,
            Items.ADAMANT_ARROW, Items.ADAMANT_ARROW_P, Items.ADAMANT_ARROW_PP, Items.ADAMANT_ARROW_PPP,
            Items.RUNE_ARROW, Items.RUNE_ARROW_P, Items.RUNE_ARROW_PP, Items.RUNE_ARROW_PPP),

    /**
     * The magic longbow weapon interface.
     */
    MAGIC_LONGBOW(Items.STEEL_ARROW, Items.STEEL_ARROW_P, Items.STEEL_ARROW_PP, Items.STEEL_ARROW_PPP,
            Items.MITHRIL_ARROW1, Items.MITHRIL_ARROW_P, Items.MITHRIL_ARROW_PP, Items.MITHRIL_ARROW_PPP,
            Items.ADAMANT_ARROW, Items.ADAMANT_ARROW_P, Items.ADAMANT_ARROW_PP, Items.ADAMANT_ARROW_PPP,
            Items.RUNE_ARROW, Items.RUNE_ARROW_P, Items.RUNE_ARROW_PP, Items.RUNE_ARROW_PPP, Items.BROAD_ARROW) {
        @Override
        public WeaponSpecial specialAttack() {
            return Snapshot.getInstance();
        }
    };;

    /**
     * The list of compatible ammunition.
     */
    private List<Integer> compatibleAmmo = new ArrayList<>();

    /**
     * Constructor for the longbow.
     *
     * @param compatibleAmmo
     */
    Longbow(int... compatibleAmmo) {
        this.compatibleAmmo.add(Items.BRONZE_ARROW);
        this.compatibleAmmo.add(Items.BRONZE_ARROW_P);
        this.compatibleAmmo.add(Items.BRONZE_ARROW_PP);
        this.compatibleAmmo.add(Items.BRONZE_ARROW_PPP);
        this.compatibleAmmo.add(Items.IRON_ARROW);
        this.compatibleAmmo.add(Items.IRON_ARROW_P);
        this.compatibleAmmo.add(Items.IRON_ARROW_PP);
        this.compatibleAmmo.add(Items.IRON_ARROW_PPP);
        for (int arrow : compatibleAmmo) {
            this.compatibleAmmo.add(arrow);
        }
    }

    /**
     * The type of ammo the longbow fires.
     */
    private static final Arrows ammo = new Arrows();

    /**
     * The attack types.
     */
    private static final List<AttackType> attackTypes =
            List.of(AttackType.LONG_ACCURATE, AttackType.LONG_RAPID, AttackType.LONG_RAPID, AttackType.LONG_LONGRANGE);

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.RANGE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }

    @Override
    public Ammunition ammunition() {
        return ammo;
    }

    @Override
    public List<Integer> compatibleAmmunitionIDs() {
        return compatibleAmmo;
    }
}
