package net.zaros.server.game.module.command.server_moderator;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.punishment.PunishmentHandler;
import net.zaros.server.game.world.punishment.PunishmentType;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/17/2017
 */
@CommandManifest(description = "Unmutes a player on any world.", types = { String.class })
public class UnmuteCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("unmute");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		final String name = Misc.formatPlayerNameForProtocol(getCompleted(args, 1));
		PunishmentHandler.removePunishment(player, name, PunishmentType.MUTE);
	}
}
