package net.zaros.server.game.content.market.shop.currency;

import net.zaros.server.game.content.market.shop.ShopCurrency;
import net.zaros.server.game.node.entity.player.Player;

public class GoldCurrency implements ShopCurrency {

    @Override
    public String name() {
        return "coins";
    }

    @Override
    public int getCurrencyAmount(Player player) {
        return player.getInventory().getItems().getNumberOf(COINS);
    }

    @Override
    public void reduceCurrency(Player player, int amount) {
        player.getInventory().deleteItem(COINS, amount);
    }

    @Override
    public int getBuyPrice(int itemId) {
        switch (itemId) {
            case 11694:
                return 200;
            case 14484:
                return 300;
            case 6585:
                return 50;
            case 15273:
                return 20;
        }
        return 1;
    }

    @Override
    public int stockAmount(int itemId) {
        switch (itemId) {
            case 15273:
                return 10;
        }
        return 1;
    }

    @Override
    public int itemId() {
        return COINS;
    }
}
