package net.zaros.server.game.clan.settings.delta.impl;

import lombok.RequiredArgsConstructor;
import net.zaros.cache.util.buffer.Buffer;
import net.zaros.server.game.clan.settings.ClanMember;
import net.zaros.server.game.clan.settings.delta.ClanSettingsAction;
import net.zaros.server.utility.RunedayUtil;

/**
 * Represents the member addition clan settings action.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class AddMember extends ClanSettingsAction {

	/**
	 * The added member.
	 */
	private final ClanMember member;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.clan.settings.delta.ClanSettingsAction#encode(net.zaros
	 * .cache.util.buffer.Buffer)
	 */
	@Override
	public void encode(Buffer buffer) {
		buffer.writeByte(-1);
		buffer.writeString(member.getDisplayName());
		buffer.writeShort(RunedayUtil.toRuneDay(member.getJoinTime()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.clan.settings.delta.ClanSettingsAction#getId()
	 */
	@Override
	public int getId() {
		return 13;
	}
}
