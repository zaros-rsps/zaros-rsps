package net.zaros.server.game.module.interaction.item;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.game.content.action.interaction.PlayerFoodAction;
import net.zaros.server.game.module.type.ItemInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.InteractionOption;
import net.zaros.server.utility.rs.constant.FoodConstants.Food;

import java.util.Optional;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/27/2017
 */
@Slf4j
public class FoodInteractionModule extends ItemInteractionModule {

	public FoodInteractionModule() {
		register(Food.getAllFoodIds());
	}
	
	@Override
	public boolean handle(Player player, Item item, int slotId, InteractionOption option) {
		if (option != InteractionOption.FIRST_OPTION) {
			return true;
		}
		Optional<Food> optional = Food.forId(item.getId());
		if (!optional.isPresent()) {
			log.warn("Unable to find food... [" + item + "][" + option + "]");
			return true;
		}
		player.getManager().getActions().startAction(new PlayerFoodAction(optional.get(), item, slotId));
		return true;
	}
}
