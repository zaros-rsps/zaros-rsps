package net.zaros.server.game.content.zskillsystem.trigger;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zaros.server.game.content.zskillsystem.tool.SkillTool;

/**
 * @author Jacob Rhiel
 */
@AllArgsConstructor
@Getter
public class ItemSkillTrigger implements SkillTrigger {

    private final SkillTool tool;

    private final int[] items;

    @Override
    public boolean trigger() {
        return false;
    }

}
