package net.zaros.server.game.content.skills.fishing.plugin;

import lombok.Getter;
import net.zaros.server.game.node.item.Items;

/**
 * @author Jacob Rhiel
 */
@Getter
public enum FishingBait {

    NONE,

    BAIT(Items.FISHING_BAIT),

    FEATHERS(Items.FEATHER1),

    STRIPY_FEATHERS(Items.STRIPY_FEATHER),

    VESSEL(Items.RAW_KARAMBWANJI),

    LIVING_MINERAL(Items.LIVING_MINERALS),

    BARBARIAN(Items.FISHING_BAIT, Items.FEATHER1, Items.FISH_OFFCUTS, Items.ROE1, Items.CAVIAR1),

    ;

    private final int[] baitItems;

    FishingBait(int... baitItems) {
        this.baitItems = baitItems;
    }

    /**
     * Gets the first found bait.
     * For instance for barbarian fishing, you can catch the fish with either of the bait but,
     * it depicts the bait in order of the enum array, so it'll find the first, first being least significant.
     * @param bait The bait to compare too.
     * @return The found bait.
     */
    public static int bait(int bait) {
        for (FishingBait fishingBait : FishingBait.values()) {
            for (int baitId : fishingBait.getBaitItems()) {
                if (baitId == bait) {
                    return baitId;
                }
            }
        }
        return -1;
    }

}
