package net.zaros.server.game.content.system.zskillsystem.farming.compostbin;

import com.ahoypk.definitions.ItemDefinition;
import com.ahoypk.definitions.constant.Items;
import com.ahoypk.engine.task.TaskManager;
import com.ahoypk.world.content.skills.impl.farming.actions.CompostBinCollectAction;
import com.ahoypk.world.content.skills.impl.farming.actions.CompostBinFillAction;
import com.ahoypk.world.content.skills.impl.farming.impl.CompostType;
import com.ahoypk.world.content.skills.impl.farming.impl.FarmingConstants;
import com.ahoypk.world.entity.impl.player.Player;
import com.ahoypk.world.model.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages all the player's compost bin.
 */
public class CompostBinManager {

	/**
	 * The player's compost bins.
	 */
	private List<CompostBin> compost_bins = new ArrayList<>();

	/**
	 * Returns the compost bins.
	 *
	 * @return the bins
	 */
	public List<CompostBin> getCompostBins() {
		return compost_bins;
	}

	/**
	 * Returns the compost bin by its location.
	 *
	 * @param position
	 * @return the compost bin.
	 */
	public CompostBin get(Position position) {
		return compost_bins.stream().filter(bin -> bin.getPosition().getPosition().equals(position)).findAny().orElse(null);
	}

	/**
	 * Attempts to fill the compost bin.
	 *
	 * @param position
	 * @return if a compost bin and ingredient was found with that position and item
	 */
	public boolean fillCompostBin(Player player, int item_used, Position position) {

		CompostBin bin = get(position);

		if (bin == null) {
			return false;
		}

		if ((bin.getCompostType() != null && (bin.getCompostType().getNextState() == null || bin.getInventory().isFull()))) {
			return false;
		}

		if (!CompostType.isValidIngredient(item_used)) {
			player.sendMessage("You can't use this item as compost material!");
			return false;
		}

		if (bin.getInventory().isFull()) {
			player.sendMessage("This compost bin is already full!");
			return true;
		}

		if (bin.isClosed()) {
			player.sendMessage("This compost bin is currently closed!");
			return true;
		}

		player.sendMessage("You attempt to fill the compost bin with " + ItemDefinition.forID(item_used).getName().toLowerCase() +
			".");

		TaskManager.submit(new CompostBinFillAction(player, bin, item_used));

		return true;
	}

	/**
	 * Updates the compost bin's object ID.
	 *
	 * @param bin
	 */
	public void updateBin(Player player, CompostBin bin) {

		if (bin == null) {
			player.getPacketSender().sendConfig(FarmingConstants.COMPOST_BIN_CONFIG_ID, 0);
			return;
		}

		if (bin.isClosed()) {
			player.getPacketSender().sendConfig(FarmingConstants.COMPOST_BIN_CONFIG_ID, 70);
			return;
		}

		bin.updateCompostType();

		if (bin.getCompostType() == null) {
			player.getPacketSender().sendConfig(FarmingConstants.COMPOST_BIN_CONFIG_ID, 0);
			return;
		}

		int real_state = bin.getCompostType().getMinConfig() + (bin.getInventory().capacity() - bin.getInventory()
			.getFreeSlots() - 1);

		player.getPacketSender().sendConfig(FarmingConstants.COMPOST_BIN_CONFIG_ID, real_state);
	}

	/**
	 * Changes the bin's state.
	 *
	 * @param position
	 * @return if a bin with that position was found.
	 */
	public boolean changeClosedState(Player player, Position position) {

		CompostBin bin = get(position);

		if (bin == null || bin.getCompostType() == null) {
			updateBin(player, bin);
			return false;
		}

		if (bin.canChangeState()) {
			bin.setClosedState(!bin.isClosed());
			if (bin.isClosed() && bin.getCompostType().getNextState() != null) {
				player.sendMessage("This bin will produce " + ItemDefinition.forID(bin.getCompostType().getNextState()
					.getOutcomeItemID()).getName().toLowerCase() + ".");
			}
		} else {
			player.sendMessage("This bin is already full and composting.");
		}

		updateBin(player, bin);

		return true;
	}

	/**
	 * Collects the products of composting.
	 *
	 * @param player
	 * @param position
	 * @return if a bin with that position was found.
	 */
	public boolean collectProducts(Player player, Position position) {

		CompostBin bin = get(position);

		if (bin == null) {
			return false;
		}

		bin.updateCompostType();

		if (bin.getCompostType() == null || bin.isClosed() || bin.getCompostType().getNextState() !=
			null) {
			return false;
		}

		if (bin.getCompostType() != CompostType.ROTTEN_TOMATOES && !player.getInventory().contains
			(Items.BUCKET)) {
			player.sendMessage("You need a bucket to empty this!");
			return true;
		}

		TaskManager.submit(new CompostBinCollectAction(player, bin));

		return true;
	}

}
