package net.zaros.server.game.content.dialogue.impl.misc;

import net.zaros.server.game.content.dialogue.Dialogue;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/10/2017
 */
public class SimpleNPCMessage extends Dialogue {

	/**
	 * Construct a new {@link SimpleNPCMessage} type object instance.
	 */
	public SimpleNPCMessage() {
		super();
	}

	/**
	 * Construct a new {@link SimpleNPCMessage} type object instance.
	 * 
	 * @param npcid
	 *                 the npc id.
	 * @param messages
	 *                 the dialogue messages.
	 */
	public SimpleNPCMessage(int npcid, String... messages) {
		this(npcid, NORMAL, messages);
	}

	/**
	 * Construct a new {@link SimpleNPCMessage} type object instance.
	 * 
	 * @param npcid
	 *                   the npc id.
	 * @param expression
	 *                   the npc expression.
	 * @param messages
	 *                   the dialogue messages.
	 */
	public SimpleNPCMessage(int npcid, int expression, String... messages) {
		Object[] parameters = new Object[messages.length + 2];
		parameters[0] = npcid;
		parameters[1] = expression;
		for (int index = 0; index < messages.length; index++) {
			parameters[index + 2] = messages[index];
		}
		setParameters(parameters);
	}

	@Override
	public void constructMessages(Player player) {
		this.chattingId = parameter(0);
		String[] messages = new String[getParameters().length - 2];
		for (int index = 0; index < messages.length; index++) {
			messages[index] = (String) getParameters()[index + 2];
		}
		npc(chattingId, parameter(1, NORMAL), messages);
	}
}
