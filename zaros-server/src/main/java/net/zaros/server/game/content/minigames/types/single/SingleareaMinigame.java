/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames.types.single;

import net.zaros.server.game.content.minigames.MinigameBase;
import net.zaros.server.game.content.minigames.MinigameProperties;

/**
 * Represents a single area multi-player minigame.
 * 
 * @author Walied K. Yassen
 */
public abstract class SingleareaMinigame extends MinigameBase {

	/**
	 * Construct a new {@link SingleareaMinigame} type object instance.
	 * 
	 * @param properties
	 */
	public SingleareaMinigame(MinigameProperties properties) {
		super(properties);
	}
}
