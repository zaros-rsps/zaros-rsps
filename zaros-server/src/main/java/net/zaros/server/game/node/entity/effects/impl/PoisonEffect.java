package net.zaros.server.game.node.entity.effects.impl;

import lombok.Getter;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.effects.Duration;
import net.zaros.server.game.node.entity.effects.Effect;
import net.zaros.server.game.node.entity.effects.WalkPolicy;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigPacketBuilder;
import net.zaros.server.utility.rs.Hit;
import net.zaros.server.utility.tool.SecondsTimer;

/**
 * The poison effect for entities.
 *
 * @author Gabriel || Wolfsdarker
 */
public class PoisonEffect implements Effect {

    /**
     * The effect's name.
     */
    @Getter
    private static final String name = "poison";

    /**
     * The poison's damage.
     */
    private int damage;

    /**
     * The entity who is receiving the effect.
     */
    private Entity entity;

    /**
     * The entity that is applying the effect.
     */
    private Entity source;

    /**
     * The effect's timer.
     */
    private SecondsTimer timer;

    /**
     * Constructor for the poison effect.
     *
     * @param entity
     * @param source
     * @param damage
     */
    public PoisonEffect(Entity entity, Entity source, int damage) {
        this.entity = entity;
        this.source = source;
        this.timer = new SecondsTimer();
        this.damage = damage;
        this.timer.start(15);
    }

    @Override
    public boolean over() {
        return damage == 0;
    }

    @Override
    public boolean renewable() {
        return false;
    }

    @Override
    public boolean immune(Entity entity) {
        return false;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String nonWalkableMessage() {
        return null;
    }

    @Override
    public WalkPolicy walkPolicy() {
        return WalkPolicy.WALKABLE;
    }

    @Override
    public Duration duration() {
        return Duration.TEMPORARY;
    }

    @Override
    public void preExecution(boolean isLogin) {
        if (isLogin) {
            return;
        }
        if (entity.isPlayer()) {
            entity.toPlayer().getTransmitter().sendMessage("<col=990000>You have been poisoned!</col>");
        }
    }

    @Override
    public void onTick() {
        if (timer.secondsRemaining() == 0) {

            if (entity.isPlayer()) {

                entity.toPlayer().getTransmitter().send(new ConfigPacketBuilder(102, 1).build(entity.toPlayer()));

                if (entity.toPlayer().getManager().getInterfaces().getScreenInterface() != -1) {
                    return;
                }
            }

            entity.getHitQueue().add(new Hit(source, damage, Hit.HitSplat.POISON_DAMAGE));

            damage -= 10;

            if (damage > 0) {
                timer.start(15);
            }
        }

        if (entity.isDying()) {
            damage = 0;
        }
    }

    @Override
    public void renew() {

    }

    @Override
    public void end() {
        if (entity.isPlayer()) {
            entity.toPlayer().getTransmitter().sendMessage("The poison in your system fades away.");
            entity.toPlayer().getTransmitter().send(new ConfigPacketBuilder(102, 0).build(entity.toPlayer()));
        }
    }
}
