package net.zaros.server.game.content.skills.fishing.plugin;

import net.zaros.server.core.event.npc.NpcEvent;
import net.zaros.server.core.event.npc.NpcListener;
import net.zaros.server.game.plugin.java.Plugin;
import net.zaros.server.game.world.World;

/**
 * @author Jacob Rhiel
 */
public class FishingPlugin extends Plugin {

    @Override
    public void onEnable() {
        World.getSkillManager().register(Fishing.getSingleton());
    }

    @Override
    public void onDisable() {
        World.getSkillManager().unregister(Fishing.getSingleton());
    }

    @NpcListener(ids = { 312 })
    public void fish(NpcEvent event) {
        System.out.println(event.getPlayer() + ", " + event.getNpc() + ", " + event.getOption());
    }

}
