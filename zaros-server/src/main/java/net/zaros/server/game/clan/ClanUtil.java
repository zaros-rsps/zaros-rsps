package net.zaros.server.game.clan;

import net.zaros.server.utility.newrepository.database.index.IndexTables;
import net.zaros.server.utility.newrepository.database.index.impl.UniqueIndexTable.UniqueDomain;

/**
 * @author Walied K. Yassen
 */
public final class ClanUtil {

	/**
	 * Generates a unique clan id.
	 * 
	 * @return the generated clan unique id.
	 */
	public static long generateUniqueId() {
		return IndexTables.getUnique().generateId(UniqueDomain.CLAN);
	}

	private ClanUtil() {
		// NOOP
	}
}
