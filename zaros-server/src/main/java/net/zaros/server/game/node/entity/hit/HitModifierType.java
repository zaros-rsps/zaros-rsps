package net.zaros.server.game.node.entity.hit;

/**
 * The types of hit modifiers.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum HitModifierType {

	/**
	 * The modifier will multiply with the damage.
	 */
	MULTIPLY,

	/**
	 * The modifier will divide the damage.
	 */
	DIVIDE,

	/**
	 * The modifier will be added to the damage.
	 */
	ADD,

	/**
	 * The damage will be subtracted by the modifier.
	 */
	REMOVE,

	/**
	 * The damage will be set as the modifier.
	 */
	EQUAL


}
