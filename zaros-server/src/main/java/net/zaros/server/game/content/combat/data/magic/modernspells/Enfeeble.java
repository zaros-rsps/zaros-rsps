package net.zaros.server.game.content.combat.data.magic.modernspells;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.*;
import net.zaros.server.game.node.entity.render.flag.impl.GraphicHeight;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;
import net.zaros.server.utility.rs.constant.SkillConstants;

import java.util.List;

/**
 * The enfeeble spell on the regular spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class Enfeeble extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 78;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.WATER_RUNE1, 8), new Item(Items.EARTH_RUNE1, 8), new Item(Items.SOUL_RUNE1));

    @Override
    public void playGraphics(Entity source, Entity target) {
        source.sendGraphics(170, GraphicHeight.HIGH.toInt(), 0);
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(729);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
        ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(source, target, 171, 30, 26, 52, 0, 0));
    }

    @Override
    public HitModifier getModifier(Entity source, Entity target) {
        return null;
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return new HitEffect(HitEffectType.RUNNABLE_ACTION, HitInstant.CALCULATION, () -> {
            int drainAmount = (int) (source.getSkills().getLevel(SkillConstants.STRENGTH) * 0.1);
            source.getSkills().drainLevel(SkillConstants.STRENGTH, drainAmount, 1);
        });
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(172);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int magicLevelRequirement() {
        return 73;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 10;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 83;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.REGULAR;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }

}
