package net.zaros.server.game.world;

import static java.util.Arrays.fill;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.google.common.base.Stopwatch;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.zaros.cache.Cache;
import net.zaros.cache.type.defaults.BodyDataParser;
import net.zaros.discord.DiscordManager;
import net.zaros.server.Configuration;
import net.zaros.server.core.boot.BootHandler;
import net.zaros.server.core.event.EventManager;
import net.zaros.server.core.service.ServiceManager;
import net.zaros.server.core.system.SystemManager;
import net.zaros.server.core.task.impl.EnergyRestorationTask;
import net.zaros.server.core.task.impl.HitpointsRestorationTask;
import net.zaros.server.core.task.impl.InformationTabTask;
import net.zaros.server.core.task.impl.MinigamePulseTask;
import net.zaros.server.core.task.impl.PlayerSavingTask;
import net.zaros.server.core.task.impl.SkillRestorationTask;
import net.zaros.server.core.task.impl.SpecialEnergyRestorationTask;
import net.zaros.server.game.GameConstants;
import net.zaros.server.game.GameFlags;
import net.zaros.server.game.content.activity.impl.pvp.PvPLocation;
import net.zaros.server.game.content.combat.data.magic.SpellSelection;
import net.zaros.server.game.content.combat.player.CombatRegistry;
import net.zaros.server.game.content.dialogue.DialogueRepository;
import net.zaros.server.game.content.event.EventRepository;
import net.zaros.server.game.content.market.lending.ItemLendingTask;
import net.zaros.server.game.content.market.shop.ShopRepository;
import net.zaros.server.game.content.minigames.MinigameManager;
import net.zaros.server.game.module.ModuleRepository;
import net.zaros.server.game.module.command.CommandRepository;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.EntityList;
import net.zaros.server.game.node.entity.areas.AreaManager;
import net.zaros.server.game.node.entity.data.weapon.Ammunition;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.npc.extension.RockCrabNPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.link.skill.SkillManager;
import net.zaros.server.game.node.item.degrade.DegradableRegistry;
import net.zaros.server.game.plugin.java.PluginManager;
import net.zaros.server.game.world.region.RegionBuilder;
import net.zaros.server.game.world.region.RegionDeletion;
import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.web.sql.SQLRepository;
import net.zaros.server.network.world.WorldNetwork;
import net.zaros.server.network.world.packet.incoming.IncomingPacketRepository;
import net.zaros.server.network.world.packet.incoming.impl.WalkPacketDecoder;
import net.zaros.server.utility.backend.UnexpectedArgsException;
import net.zaros.server.utility.database.DatabaseManager;
import net.zaros.server.utility.newrepository.Repository;
import net.zaros.server.utility.repository.npc.combat.NPCCombatSwingRepository;
import net.zaros.server.utility.repository.npc.spawn.NPCSpawn;
import net.zaros.server.utility.repository.object.ObjectSpawnRepository;
import net.zaros.server.utility.rs.constant.Directions.Direction;

/**
 * Contains all the collections and data to handle a world.
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/18/2017
 */
@Slf4j
public final class World {

	/**
	 * The minigame handler
	 */
	@Getter
	private static final MinigameManager minigameManager = new MinigameManager();

	@Getter
	private static final SkillManager skillManager = new SkillManager();

	/**
	 * The plugins manager.
	 */
	@Getter
	private static final PluginManager pluginManager = new PluginManager();

	/**
	 * The events manager.
	 */
	@Getter
	private static final EventManager eventManager = new EventManager();

	/**
	 * The list of all players in the world
	 */
	@Getter
	private static final EntityList<Player> players = new EntityList<>(Configuration.World.LIMIT_PLAYERS, true);

	/**
	 * The {@code EntityList} of all npcs that exist.
	 */
	@Getter
	private static final EntityList<NPC> npcs = new EntityList<>(Configuration.World.LIMIT_NPCS, false);

	/**
	 * The instance of the packet repository
	 */
	@Getter
	private static final IncomingPacketRepository packetRepository = new IncomingPacketRepository(WalkPacketDecoder.class.getPackage().getName());

	/**
	 * The instance of the stopwatch
	 */
	@Getter
	private static final Stopwatch stopwatch = Stopwatch.createUnstarted();

	/**
	 * The array of global player hash information
	 */
	private static final Location[] locationHashInformation = new Location[2048];

	/**
	 * If the world is alive
	 */
	@Getter
	@Setter
	private static boolean isAlive;

	public static void initialise(String[] args) {
		try {
			SystemManager.setDefaults(args);// BootstrapType.MAIN_WORLD);//TODO Update for multi worlds
		} catch (UnexpectedArgsException e) {
			UnexpectedArgsException.push();
			System.exit(1);
		}
		if (!GameFlags.isLobbyWorld()) {
			World.packetRepository.storeAll();
		}
		fill(locationHashInformation, Location.create(0, 0, 0));
		setAlive(true);
		start();
		cleanup();
	}

	private static void start() {
		stopwatch.start();
		if (!GameFlags.isLobbyWorld()) {
			BootHandler.addWork(() -> {
				DatabaseManager.initialise();
				Repository.initialise();
				Cache.initialise(Repository.getCacheDirectory());
				BodyDataParser.loadAll();
				RegionBuilder.init();
				AreaManager.init();
				CombatRegistry.registerAll();
				DegradableRegistry.registerAll();
				ShopRepository.load();
			}, () -> {
				RegionDeletion.prepare();
				ObjectSpawnRepository.get().loadAll();
			}, () -> {
				DialogueRepository.loadSubscriptions();
				NPCCombatSwingRepository.loadAll();
			}, () -> {
				EventRepository.registerEvents(false);
				SQLRepository.storeConfiguration();
			}, () -> {
				ModuleRepository.registerAllModules(false);
				CommandRepository.populate(false);
			}, () -> {
				pluginManager.loadAll(Repository.getPluginsDirectory().getPath().toFile());
			},
			SpellSelection::loadSpells,
			WeaponInterface::load,
			Ammunition::load,
			ServiceManager::initialise);
			// TODO: Temporary, until the time scheduler api is done.
			new Thread(DiscordManager::initialise).start();
		} else {
			BootHandler.addWork(ServiceManager::initialise);
		}
		BootHandler.await();
	}

	public static void cleanup() {
		SystemManager.start();
		if (GameFlags.isLobbyWorld()) {
			return;
		}
		log.info("Started world " + GameFlags.worldId + " in " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " ms.");
		// master server can now listen
		MasterCommunication.start();
		// start the world tasks now that everything has loaded
		generateWorldTasks();
		try {
			// this waits for the session to close, so anything after this method will not
			// execute until shutdown
			WorldNetwork.bind();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Generates tasks that operate for the world
	 */
	private static void generateWorldTasks() {
		SystemManager.getScheduler().schedule(new EnergyRestorationTask());
		SystemManager.getScheduler().schedule(new SkillRestorationTask());
		SystemManager.getScheduler().schedule(new HitpointsRestorationTask());
		SystemManager.getScheduler().schedule(new PlayerSavingTask());
		SystemManager.getScheduler().schedule(new InformationTabTask());
		SystemManager.getScheduler().schedule(new SpecialEnergyRestorationTask());
		SystemManager.getScheduler().schedule(new MinigamePulseTask());
		SystemManager.getScheduler().schedule(new ItemLendingTask());
	}

	/**
	 * Gets the hash of a player
	 *
	 * @param playerIndex
	 *                    The index of the player
	 */
	public static Location getHash(short playerIndex) {
		return World.locationHashInformation[playerIndex];
	}

	/**
	 * Updates the hash of the player
	 *
	 * @param playerIndex
	 *                    The index of the player
	 * @param loc
	 *                    The location of the player
	 */
	public static void updateHash(short playerIndex, Location loc) {
		World.locationHashInformation[playerIndex] = loc;
	}

	/**
	 * Adds an npc to the world, in the form of a {@link NPCSpawn} {@code Object}
	 *
	 * @param spawn
	 *              The {@code NPCSpawn} object
	 */
	public static void addSpawn(NPCSpawn spawn) {
		addNPC(spawn.getNpcId(), spawn.getTile(), spawn.getDirection());
	}

	/**
	 * Adds an npc to the world
	 * 
	 * @return The npc that was constructed
	 */
	public static NPC addNPC(NPC npc) {
		if (npc != null) {
			npc.register();
			npcs.add(npc);
		}
		return npc;
	}

	/**
	 * Adds an npc to the world
	 *
	 * @param id
	 *                 The id of the npc
	 * @param location
	 *                 The location of the npc
	 * @return The npc that was constructed
	 */
	public static NPC addNPC(int id, Location location, Direction direction) {
		NPC npc;
		if (id == 1266 || id == 1268 || id == 2453 || id == 2886) {
			npc = new RockCrabNPC(id, location, direction);
			/*
			 * } else if ( id == 13447 || id == 13448 || id == 13446) { npc = new Nex(id,
			 * location, direction);
			 */
		} else {
			npc = new NPC(id, location, direction);
		}
		npc.register();
		npcs.add(npc);
		return npc;
	}

	/**
	 * Handles the removal of a player
	 *
	 * @param player
	 *               The player to remove
	 */
	public static void removePlayer(Player player) {
		players.remove(player);
	}

	/**
	 * Finds a player by their username
	 *
	 * @param username
	 *                 The username of the player
	 */
	public static Optional<Player> getPlayerByUsername(String username) {
		return players.stream().filter(player -> player.getDetails().getUsername().equalsIgnoreCase(username)).findAny();
	}

	/**
	 * Finds a player by their id
	 *
	 * @param id
	 *           The id of the player
	 */
	public static Optional<Player> getPlayerById(long id) {
		return players.stream().filter(player -> player.getUserId() == id).findAny();
	}

	/**
	 * Finds a player by their id
	 *
	 * @param id
	 *           The id of the player
	 */
	public static Set<Player> getPlayersByIds(Set<Long> ids) {
		return players.stream().filter(player -> ids.contains(player.getUserId())).collect(Collectors.toSet());

	}

	/**
	 * Checks if the tile is a pvp area
	 *
	 * @param location
	 *                 The tile
	 */
	public static boolean isPvpArea(Location location) {
		if (GameFlags.worldId == GameConstants.PVP_WORLD_ID) {
			return PvPLocation.isAtPvpLocation(location);
		} else {
			return PvPLocation.isAtWild(location);
		}
	}

	/**
	 * Gets the amount of players
	 */
	public static int getPlayerCount() {
		return players.size();
	}
}
