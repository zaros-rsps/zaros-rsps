package net.zaros.server.game.node.entity.effects;

/**
 * A simple enum to handle duration for systems.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum Duration {

    /**
     * The duration will be temporary and will end eventually.
     */
    TEMPORARY,

    /**
     * The duration will be permanent and endless.
     */
    PERMANENT

}
