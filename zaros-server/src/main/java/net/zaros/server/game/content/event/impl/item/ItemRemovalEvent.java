package net.zaros.server.game.content.event.impl.item;

import net.zaros.server.game.content.event.Event;
import net.zaros.server.game.content.event.EventPolicy.ActionPolicy;
import net.zaros.server.game.content.event.EventPolicy.AnimationPolicy;
import net.zaros.server.game.content.event.EventPolicy.InterfacePolicy;
import net.zaros.server.game.content.event.EventPolicy.WalkablePolicy;
import net.zaros.server.game.content.event.context.item.ItemRemovalContext;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.link.LockManager.LockType;
import net.zaros.server.game.node.entity.player.render.flag.impl.AppearanceUpdate;
import net.zaros.server.game.node.item.Item;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/28/2017
 */
public class ItemRemovalEvent extends Event<ItemRemovalContext> {
	
	public ItemRemovalEvent() {
		setWalkablePolicy(WalkablePolicy.RESET);
		setInterfacePolicy(InterfacePolicy.CLOSE);
		setAnimationPolicy(AnimationPolicy.RESET);
		setActionPolicy(ActionPolicy.RESET);
	}
	
	@Override
	public void run(Player player, ItemRemovalContext context) {
		int slotId = context.getSlotId();
		if (slotId >= 15) {
			return;
		}
		Item item = player.getEquipment().getItem(slotId);
		if (item == null || !player.getInventory().addItem(item.getId(), item.getAmount())) {
			return;
		}
		player.getEquipment().getItems().set(slotId, null);
		player.getEquipment().refresh(slotId);
		player.getUpdateMasks().register(new AppearanceUpdate(player));
		if (slotId == 3) {
			player.getCombatDefinitions().setSpecialActivated(false);
		}
	}
	
	@Override
	public boolean canStart(Player player, ItemRemovalContext context) {
		return !player.getManager().getLocks().isLocked(LockType.ITEM_INTERACTION);
	}
}
