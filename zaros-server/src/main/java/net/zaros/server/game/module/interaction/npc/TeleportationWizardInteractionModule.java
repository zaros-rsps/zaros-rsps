package net.zaros.server.game.module.interaction.npc;

import net.zaros.server.game.module.interaction.rsinterface.TeleportationInterfaceModule;
import net.zaros.server.game.module.interaction.rsinterface.TeleportationInterfaceModule.TransportationLocation;
import net.zaros.server.game.module.type.NPCInteractionModule;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/9/2017
 */
public class TeleportationWizardInteractionModule extends NPCInteractionModule {

	public TeleportationWizardInteractionModule() {
		register(14332);
	}
	
	@Override
	public boolean handle(Player player, NPC npc, InteractionOption option) {
		if (option == InteractionOption.FIRST_OPTION) {
			TeleportationInterfaceModule.displaySelectionInterface(player, true);
		} else if (option == InteractionOption.SECOND_OPTION) {
			TransportationLocation last = player.getVariables().getAttribute(AttributeKey.LAST_SELECTED_TELEPORT);
			if (last == null) {
				return true;
			}
			TeleportationInterfaceModule.teleportPlayer(player, last.getDestination(), () -> last.getLocations().handlePostTeleportation(player, last.getOptionIndex()));
		}
		return true;
	}
}
