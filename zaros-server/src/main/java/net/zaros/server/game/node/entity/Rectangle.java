package net.zaros.server.game.node.entity;

public class Rectangle {
	private final int x;
	private final int y;
	private final int sizeX;
	private final int sizeY;

	public Rectangle(int x, int y, int sizeX, int sizeY) {
		this.x = x;
		this.y = y;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getSizeX() {
		return sizeX;
	}

	public int getSizeY() {
		return sizeY;
	}

}
