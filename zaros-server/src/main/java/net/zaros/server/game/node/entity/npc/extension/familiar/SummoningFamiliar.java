package net.zaros.server.game.node.entity.npc.extension.familiar;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.content.skills.summoning.FamiliarData;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.constant.Directions;

public class SummoningFamiliar extends NPC {

    public SummoningFamiliar(FamiliarData familiar, Location location) {
        super(familiar.getFamiliarId(), location, Directions.Direction.NORTH);
        this.familiar = familiar.getFamiliarId();
    }

    @Getter
    @Setter
    private int familiar;

    @Getter
    @Setter
    private double followerDuration;

    @Getter
    private Item[] familiarStorage;

    @Getter
    @Setter
    private transient FamiliarSpecial familiarSpecial;

}
