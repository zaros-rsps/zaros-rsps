package net.zaros.server.game.content.skills.fishing.plugin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zaros.cache.type.objtype.ItemDefinitionParser;
import net.zaros.cache.type.seqtype.AnimationDefinitionParser;
import net.zaros.server.core.newtask.scheduler.tick.TickTask;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.Skills;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.tool.RandomFunction;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jacob Rhiel
 */
@AllArgsConstructor
@Getter
public class FishingTask extends TickTask {

    private final Player player;

    private final FishingSpot spot;

    private final FishingTool tool;

    private final FishingBait bait;

    public boolean hasRequirements() {
        boolean toolRequired = !tool.equals(FishingTool.NONE);
        boolean baitRequierd = !bait.equals(FishingBait.NONE);
        int minimumLevel = tool.getMinimumLevel();
        if (toolRequired) {
            if (player.getInventory().containsItem(tool.getItemId()) || player.getInventory().containsItem(tool.toolIds())) {
                if (baitRequierd) {
                    if (player.getInventory().containsItem(bait.getBaitItems())) {
                        player.getTransmitter().sendMessage("You meet the requirements");
                        return true;
                    } else {
                        player.getTransmitter().sendMessage("You need " + Arrays.toString(bait.getBaitItems()) + " to fish here");
                        return false;
                    }
                }
            } else {
                player.getTransmitter().sendMessage("You need " + tool + " to fish here");
                return false;
            }
            return true;
        }
        return true;
    }

    @Override
    protected void execute() {

        int animationDuration = AnimationDefinitionParser.forId(tool.getAnimation()).getDurationTicks();

        boolean cycleAnimation = tick == 0 || tick % animationDuration == 0;

        Fish caught;

        // The animation duration has equalled tick duration, therefore we re-execute the animation for this task.
        if(cycleAnimation) {
            player.sendAnimation(tool.getAnimation());
        }

        // The starting tick is 0, send the initial message.
        if (tick == 0) {
            player.getTransmitter().sendMessage(tool.getMessage());
        }

        // If bait is required, remove it from the inventory.
        if (!bait.equals(FishingBait.NONE)) {
            player.getInventory().remove(findFirst());
        }

        // If it is a successful catch, send the message for catching.
        if(catchable(caught = randomFish())) {
            sendSuccessfulCatchMessage(caught);
        }

        if (tick == 15) {
            player.getTransmitter().sendMessage("canceling fishing task.");
            player.sendAnimation(-1);
            //setPeriod(-1);
            //setNextExecution(-1);
            getFuture().cancel();
        }

        /*player.getTransmitter().sendMessage("Obtainable fish: " + Arrays.toString(obtainableFish().toArray()));

        player.getTransmitter().sendMessage("<col=412213> Random fish : " + randomFish() + ".</col>");

        player.getTransmitter().sendMessage("Fishing for: " + Arrays.toString(tool.getFish()));*/

    }

    /**
     * Finds the first type of bait used. Since in this case the bait is listed from first->last acceptable bait.
     * Therefor this works perfectly to get the first bait found in the players inventory.
     * @return The first bait found in the players inventory.
     */
    private int findFirst() {
        for (Item item : player.getInventory().getItems().toArray()) {
            for (int id : bait.getBaitItems()) {
                if (item.getId() == id) {
                    return item.getId();
                }
            }
        }
        return -1;
    }

    /**
     * Sends a successful catch message to the player.
     * @param fish The fish that is being caught.
     */
    private void sendSuccessfulCatchMessage(Fish fish) {
        /** The name of the fish **/
        String name = ItemDefinitionParser.forId(fish.getRawFishId()).getName();
        /** If the fish is plural **/
        boolean plural = name.contains("anchovies") || name.contains("shrimp");
        /** Format the name for accuracy **/
        String formattedName = name.replace("raw", "").trim();
        /** Format the plural name for accuracy if is plural **/
        String formattedPlural = name.substring(name.lastIndexOf("s")).replace("s", "").trim();
        player.getTransmitter().sendMessage("You catch " + (plural ? "some" : "a") + (plural ? formattedPlural : formattedName));
    }

    /**
     * Gets a random fish based on the obtainable fish the player can catch.
     * @return A random fish.
     */
    private Fish randomFish() {
        /** The fish the player can actually catch. **/
        var obtainableFish = obtainableFish();
        /** Mix it up so it's completely random each time. **/
        Collections.shuffle(obtainableFish);
        /** Again randomize which fish the player gets. **/
        return (Fish) RandomFunction.getRandomElement(obtainableFish.toArray());
    }

    /**
     * Returns the list of fish the player can catch.
     * @return The obtainable fish.
     */
    private List<Fish> obtainableFish() {
        var level = player.getSkills().getLevel(Skills.FISHING);
        return Arrays.stream(tool.getFish()).filter(fish -> level >= fish.getRequiredLevel()).collect(Collectors.toList());
    }

    /**
     * Returns if the fish is catchable at this time, based on a formula to randomize the catch "speed" and success rate.
     * @param fish The fish to try and 'catch'.
     * @return Whether or not the fish is catchable at this time.
     */
    private boolean catchable(Fish fish) {
        int level = 1 + player.getSkills().getLevel(Skills.FISHING);// + player.getFamiliarManager().getBoost(Skills.FISHING);
        double hostRatio = Math.random() * fish.getRequiredLevel();
        double clientRatio = Math.random() * ((level * 1.25 - fish.getRequiredLevel()));
        return hostRatio < clientRatio;
    }

}
