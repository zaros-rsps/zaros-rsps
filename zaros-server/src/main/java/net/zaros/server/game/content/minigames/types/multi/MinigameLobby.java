/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames.types.multi;

import net.zaros.server.game.content.minigames.MinigameStatus;
import net.zaros.server.game.content.minigames.util.JoinLeaveOptions;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.AttributeKey;

/**
 * Represents a minigame lobby area. The lobby area is where players just
 * standing still nor queued or playing.
 * 
 * @author Walied K. Yassen
 */
public abstract class MinigameLobby<M extends MultiareaMinigame<?, ?, ?>> extends MinigameArea<M> {

	/**
	 * Construct a new {@link MinigameLobby} type object instance.
	 * 
	 * @param minigame
	 *                 the owner minigame.
	 */
	public MinigameLobby(M minigame) {
		super(minigame);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.arena.MinigameArea#tryJoin(org.redrune.
	 * game.node.entity.player.Player,
	 * org.redrune.game.content.minigames.options.JoinLeaveOptions[])
	 */
	@Override
	public boolean tryJoin(Player player, JoinLeaveOptions... options) {
		if (players.size() >= minigame.getProperties().getLobbyCapacity()) {
			player.getTransmitter().sendMessage("The minigame lobby area is currently full.");
			return false;
		}
		if (!super.tryJoin(player, options)) {
			return false;
		}
		player.putTemporaryAttribute(AttributeKey.MINIGAME_STATUS, MinigameStatus.LOBBY);
		return true;

	}
}
