/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames.types.multi;

import lombok.Getter;
import net.zaros.server.game.content.minigames.util.JoinLeaveOptions;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.AttributeKey;

import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Represents a minigame area, contains the basic functions for minigame area
 * implementations.
 * 
 * @author Walied K. Yassen
 */
abstract class MinigameArea<M extends MultiareaMinigame<?, ?, ?>> {

	/**
	 * The players list within this area.
	 */
	@Getter
	protected final Set<Player> players = new CopyOnWriteArraySet<>();

	/**
	 * The owner minigame.
	 */
	@Getter
	protected final M minigame;

	/**
	 * Construct a new {@link MinigameArea} type object instance.
	 * 
	 * @param minigame
	 *                 the owner minigame.
	 */
	public MinigameArea(M minigame) {
		this.minigame = minigame;
	}

	/**
	 * Processes one tick for this area.
	 */
	public abstract void tick();

	/**
	 * @param player
	 * @return
	 */
	public boolean tryJoin(Player player, JoinLeaveOptions... options) {
		EnumSet<JoinLeaveOptions> _options = toSet(options);
		if (!players.contains(player) || _options.contains(JoinLeaveOptions.FORCE)) {
			if (_options.contains(JoinLeaveOptions.EFFECTLESS) || join(player, _options)) {
				players.add(player);
				return true;
			}
		}
		return false;
	}

	/**
	 * Makes the specified {@link Player} join this area.
	 * 
	 * @param player
	 *               the player which we want them to join this area.
	 * @return <code>true</code> if they have joined otherwise <code>false</code>.
	 */
	public abstract boolean join(Player player, EnumSet<JoinLeaveOptions> options);

	/**
	 * Attempts to make the specified {@link Player} leave this area.
	 * 
	 * @param player
	 * @return
	 */
	public boolean tryLeave(Player player, JoinLeaveOptions... options) {
		EnumSet<JoinLeaveOptions> _options = toSet(options);
		if (players.contains(player) || _options.contains(JoinLeaveOptions.FORCE)) {
			if (_options.contains(JoinLeaveOptions.EFFECTLESS) || leave(player, _options)) {
				player.removeTemporaryAttribute(AttributeKey.MINIGAME_STATUS);
				players.remove(player);
				return true;
			}
		}
		return false;
	}

	/**
	 * Makes the specified {@link Player} leave this area.
	 * 
	 * @param player
	 *               the player which we want them to leave this area.
	 * @return <code>true</code> if they have left otherwise <code>false</code>.
	 */
	public abstract boolean leave(Player player, EnumSet<JoinLeaveOptions> options);

	/**
	 * Checks whether or not this area contains the specified {@link Player} or not.
	 * 
	 * @param player
	 *               the player to check if it does contain them or not.
	 * @return <code>true</code> if it does otherwise <code>false</code>.
	 */
	public boolean contains(Player player) {
		return players.contains(player);
	}

	/**
	 * Gets the amount of players within this arena.
	 * 
	 * @return the amount of players within this arena.
	 */
	public int size() {
		return players.size();
	}

	private static EnumSet<JoinLeaveOptions> toSet(JoinLeaveOptions... options) {
		EnumSet<JoinLeaveOptions> set = EnumSet.noneOf(JoinLeaveOptions.class);
		for (JoinLeaveOptions option : options) {
			set.add(option);
		}
		return set;
	}
}
