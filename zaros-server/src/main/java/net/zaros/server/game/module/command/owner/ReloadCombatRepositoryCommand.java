package net.zaros.server.game.module.command.owner;

import net.zaros.server.game.content.combat.player.CombatRegistry;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/23/2017
 */
public class ReloadCombatRepositoryCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("reloadcombat");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		CombatRegistry.clearAll();
		CombatRegistry.registerAll();
	}
}
