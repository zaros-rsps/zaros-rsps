package net.zaros.server.game.module.command.owner;

import net.zaros.cache.Cache;
import net.zaros.cache.util.Utils;
import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.InterfaceChangeBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/31/2017
 */
@CommandManifest(description = "Debugs an interface", types = { Integer.class })
public class DebugInterfaceCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("dbi");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		int interId = intParam(args, 1);
		int length = Utils.getInterfaceDefinitionsComponentsSize(Cache.getStore(), interId);
		for (int index = 0; index < length; index++) {
			player.getManager().getInterfaces().sendInterfaceText(interId, index, "" + index);
		}
		for (int index = 0; index < length; index++) {
			player.getTransmitter().send(new InterfaceChangeBuilder(interId, index, false).build(player));
		}
		player.getManager().getInterfaces().sendInterface(interId, true);
		String text = "Interface #" + interId + " has " + length + " component length";
		player.getTransmitter().sendMessage(text);
		System.out.println(text);
	}
}
