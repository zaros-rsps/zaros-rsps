package net.zaros.server.game.node.entity.areas.impl;

import static net.zaros.server.game.node.entity.areas.Area.loc;

import java.util.Collections;
import java.util.List;

import lombok.Getter;
import net.zaros.server.core.event.npc.NpcEvent;
import net.zaros.server.core.event.npc.NpcListener;
import net.zaros.server.core.event.npc.NpcOption;
import net.zaros.server.game.clan.dialogues.ScribeDialogue;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.areas.Area;
import net.zaros.server.game.node.entity.areas.shape.AreaPolygon;
import net.zaros.server.game.node.entity.areas.shape.AreaShape;
import net.zaros.server.game.world.World;
import net.zaros.server.utility.repository.npc.Npcs;

/**
 * Represents the clan camp area handler.
 * 
 * @author Walied K. Yassen
 */
public final class ClanCampArea implements Area {

	/**
	 * The shape of this area.
	 */
	private static final AreaShape SHAPE = new AreaPolygon(new Location[] { loc(3002, 3313), loc(2991, 3313), loc(2985, 3308), loc(2956, 3308), loc(2952, 3304), loc(2952, 3280), loc(2961, 3271), loc(2967, 3271), loc(2969, 3273), loc(2987, 3273), loc(2992, 3278), loc(2998, 3278), loc(3002, 3283) });

	/**
	 * The singleton instance of this class type.
	 */
	@Getter
	private static final ClanCampArea singleton = new ClanCampArea();

	/**
	 * Constructs a new {@link ClanCampArea} type object instance.
	 */
	public ClanCampArea() {
		initialise();
	}

	/**
	 * Initialises this area instance.
	 */
	private void initialise() {
		World.getEventManager().register(this);
	}

	@NpcListener(ids = { Npcs.SCRIBE })
	public void handleScribe(NpcEvent event) {
		var player = event.getPlayer();
		var option = event.getOption();
		if (option == NpcOption.OPTION1) {
			player.getManager().getDialogues().startDialogue(new ScribeDialogue(false));
		} else if (option == NpcOption.OPTION2) {
			player.getManager().getDialogues().startDialogue(new ScribeDialogue(true));
		}
		event.cancel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.node.entity.areas.Areas#enterAreaHook(net.zaros.server.
	 * game.node.entity.Entity)
	 */
	@Override
	public void enterAreaHook(Entity entity) {
		if (!entity.isPlayer()) {
			return;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.node.entity.areas.Areas#leaveAreaHook(net.zaros.server.
	 * game.node.entity.Entity)
	 */
	@Override
	public void leaveAreaHook(Entity entity) {
		if (!entity.isPlayer()) {
			return;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.node.entity.areas.Areas#isMulti()
	 */
	@Override
	public boolean isMulti() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.node.entity.areas.Areas#isSummoningAllowed()
	 */
	@Override
	public boolean isSummoningAllowed() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.node.entity.areas.Areas#isMultiCannonAllowed()
	 */
	@Override
	public boolean isMultiCannonAllowed() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.node.entity.areas.Areas#isSkillTrainable(int)
	 */
	@Override
	public boolean isSkillTrainable(int skillId) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.node.entity.areas.Area#getShape()
	 */
	@Override
	public AreaShape getShape() {
		return SHAPE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.node.entity.areas.Areas#getRegionIds()
	 */
	@Override
	public List<Integer> getRegionIds() {
		return Collections.emptyList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.node.entity.areas.Areas#canTeleport(net.zaros.server.
	 * game.node.entity.Entity)
	 */
	@Override
	public boolean canTeleport(Entity entity) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.node.entity.areas.Areas#canAttack(net.zaros.server.game
	 * .node.entity.Entity, net.zaros.server.game.node.entity.Entity)
	 */
	@Override
	public boolean canAttack(Entity attacker, Entity target) {
		return false;
	}
}
