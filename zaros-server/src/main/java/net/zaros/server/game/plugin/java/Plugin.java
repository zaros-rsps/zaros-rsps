package net.zaros.server.game.plugin.java;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.zaros.server.core.event.EventIdentifier;
import net.zaros.server.game.plugin.IPlugin;
import net.zaros.server.game.plugin.IPluginManifest;

/**
 * Represents the Java plugins base class, this class basically the Java
 * implementation of the {@link IPlugin} interface.
 * 
 * @author Walied K. Yassen
 * @since 0.1
 */
public abstract class Plugin implements IPlugin {

	/**
	 * The plugin's event key.
	 */
	protected final EventIdentifier<Plugin> eventsKey = new EventIdentifier<Plugin>(this);

	/**
	 * The plugin specific logger.
	 */
	protected Logger logger;

	/**
	 * The plug-in information manifest.
	 */
	protected IPluginManifest manifest;

	/**
	 * The plug-in configurations directory.
	 */
	protected File directory;

	/**
	 * Tells whether the plugin is currently enabled or not.
	 */
	private boolean enabled;

	/**
	 * Initialises this {@link Plugin} instance.
	 * 
	 * @param manifest
	 *                  the plugin manifest.
	 * @param directory
	 *                  the plugin configuration directory.
	 */
	public void initialise(PluginManifest manifest, File directory) {
		this.manifest = manifest;
		this.directory = directory;
		logger = LoggerFactory.getLogger(manifest.id);
	}

	/**
	 * Gets called when the plugin is enabled.
	 */
	public abstract void onEnable();

	/**
	 * Gets called when the plugin is disabled.
	 */
	public abstract void onDisable();

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPlugin#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled
	 */
	final void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPlugin#getManifest()
	 */
	@Override
	public IPluginManifest getManifest() {
		return manifest;
	}

	/**
	 * Sets the plugin manifest.
	 * 
	 * @param manifest
	 *                 the new manifest to set.
	 */
	void setManifest(IPluginManifest manifest) {
		this.manifest = manifest;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.runekit.api.plugin.IPlugin#getDirectory()
	 */
	@Override
	public File getDirectory() {
		return directory;
	}

	/**
	 * Gets the events identifier key.
	 * 
	 * @return the events identifier key.
	 */
	public EventIdentifier<Plugin> getEventsKey() {
		return eventsKey;
	}

}
