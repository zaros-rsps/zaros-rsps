package net.zaros.server.game.content.zskillsystem.inter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zaros.server.game.content.zskillsystem.segment.AbstractSkillSegment;

/**
 * @author Jacob Rhiel
 */
@AllArgsConstructor
@Getter
public abstract class AbstractSkillInterface implements SkillInterface {

    private final AbstractSkillSegment segment;

}
