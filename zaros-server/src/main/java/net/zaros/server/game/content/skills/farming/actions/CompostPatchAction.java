package net.zaros.server.game.content.system.zskillsystem.farming.actions;

import net.zaros.cache.parse.ItemDefinitionParser;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.FarmingConstants;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.PatchState;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.PatchTreatment;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.Patches;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;
import net.zaros.server.game.node.item.Item;

/**
 * Handles the action to add compost to the patch.
 *
 * @author Gabriel || Wolfsdarker
 */
public class CompostPatchAction extends PlayerTask {

	/**
	 * Constructor for the action.
	 *
	 * @param player
	 * @param treatment
	 * @param state
	 */
	private CompostPatchAction(Player player, PatchTreatment treatment, PatchState state) {
		super(5, player, () -> {

			state.setTreatment(treatment);

			player.getInventory().remove(treatment.getItemId(), 1);
			player.getInventory().addItem(new Item(Items.BUCKET));

			player.getTransmitter().sendMessage("You add " + ItemDefinitionParser.forId(treatment.getItemId()).getName().toLowerCase() + " " +
				"to the patch.");
			player.getFarming().updatePatches(player);
			player.endCurrentTask();

		});

		onEnd(() -> {
			player.sendAnimation(new Animation(65535));
			player.getMovementQueue().reset();
		});

		stopUpon(MovementPacketListener.class);
	}

	/**
	 * Handles the action to compost the patch.
	 *
	 * @param player
	 * @param data
	 * @param item_id
	 * @param position
	 * @return
	 */
	public static boolean compostPatch(Player player, Patches data, int item_id, Location position) {

		PatchState state = player.getFarming().getPatchStates().get(data.name());

		if (state == null) {
			return false;
		}

		PatchTreatment treatment = PatchTreatment.get(item_id);

		if (treatment == null) {
			return false;
		}

		if (state.getWeedStage() < 3) {
			player.getTransmitter().sendMessage("You must clear this patch before this.");
			return true;
		}

		if (state.getTreatment() != PatchTreatment.NOT_TREATED) {
			player.getTransmitter().sendMessage("This patch is already treated.");
			return true;
		}

		if (state.isUsed()) {
			player.getTransmitter().sendMessage("You can't use the compost after planting the seed.");
			return true;
		}

		player.faceLocation(position);
		player.sendAnimation(new Animation(FarmingConstants.PUTTING_COMPOST));

		TaskManager.submit(new CompostPatchAction(player, treatment, state));

		return true;
	}
}
