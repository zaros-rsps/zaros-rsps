package net.zaros.server.game.clan.channel.delta.impl;

import lombok.RequiredArgsConstructor;
import net.zaros.cache.util.buffer.Buffer;
import net.zaros.server.game.clan.channel.delta.ClanChannelAction;

/**
 * Represents the delte member delta channel update.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class DeleteMember extends ClanChannelAction {

	/**
	 * The member index to remove.
	 */
	private final int member;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.clan.channel.delta.ClanChannelAction#encode(net.zaros.
	 * cache.util.buffer.Buffer)
	 */
	@Override
	public void encode(Buffer buffer) {
		buffer.writeShort(member);
		buffer.writeByte(0);
		buffer.writeByte(-1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.clan.channel.delta.ClanChannelAction#getId()
	 */
	@Override
	public int getId() {
		return 3;
	}
}
