package net.zaros.server.game.node.entity.effects.impl;

import lombok.Getter;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.effects.Duration;
import net.zaros.server.game.node.entity.effects.Effect;
import net.zaros.server.game.node.entity.effects.WalkPolicy;
import net.zaros.server.utility.tool.SecondsTimer;

/**
 * The miasmic effect on entities.
 */
public class MiasmicEffect implements Effect {

    /**
     * The effect's name.
     */
    @Getter
    private static final String name = "miasmic";

    /**
     * The entity who is receiving the effect.
     */
    private Entity entity;

    /**
     * The entity that is applying the effect.
     */
    private Entity source;

    /**
     * The effect's timer.
     */
    private SecondsTimer timer;

    /**
     * Constructor for the miasmic effect.
     *
     * @param entity
     * @param source
     */
    public MiasmicEffect(Entity entity, Entity source) {
        this.entity = entity;
        this.source = source;
        this.timer = new SecondsTimer();
        this.timer.start(48);
    }

    @Override
    public boolean over() {
        return timer.finished();
    }

    @Override
    public boolean renewable() {
        return false;
    }

    @Override
    public boolean immune(Entity entity) {
        return false;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String nonWalkableMessage() {
        return null;
    }

    @Override
    public WalkPolicy walkPolicy() {
        return WalkPolicy.WALKABLE;
    }

    @Override
    public Duration duration() {
        return Duration.TEMPORARY;
    }

    @Override
    public void preExecution(boolean isLogin) {
    }

    @Override
    public void onTick() {
    }

    @Override
    public void renew() {
    }

    @Override
    public void end() {
    }
}
