package net.zaros.server.game.content.combat.script.impl.specials;

import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.game.node.entity.data.weapon.impl.TwoHanded;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.utility.rs.Hit;

public class TheJudgmentCombatScript implements CombatScript {

    private static final WeaponInterface weapon = TwoHanded.ARMADYL_GODSWORD;

    @Override
    public boolean executable(Entity attacker, Entity target) {
        return weapon.specialAttack().canSpecial(attacker, target);
    }

    @Override
    public int getAttackDistance(Entity attacker, Entity target) {
        return weapon.attackStyles().get(attacker.getCombatDefinitions().getAttackStyle()).getAttackDistance();
    }

    @Override
    public void preExecution(Entity attacker, Entity target) {
    }

    @Override
    public void execute(Entity attacker, Entity target) {
        attacker.sendAnimation(weapon.specialAttack().attackAnimation());
        if (weapon.specialAttack().attackGraphics() > 0) {
            attacker.sendGraphics(weapon.specialAttack().attackGraphics());
        }
        WeaponSpecial.drainEnergy(attacker, weapon.specialAttack().specialRequired(), true);
    }

    @Override
    public Hit.HitSplat getHitSplat(Entity attacker, Entity target) {
        return weapon.specialAttack().hitStyle();
    }

    @Override
    public Hit[] getHits(Entity attacker, Entity target) {
        return new Hit[]{new CombatHit(attacker, target, this, weapon.specialAttack().hitStyle(), 1)};
    }

    @Override
    public int getSpeed(Entity attacker, Entity target) {
        return weapon.specialAttack().attackSpeed();
    }

    @Override
    public boolean specialAttack(Entity attacker, Entity target) {
        return true;
    }

    @Override
    public void postExecution(Entity attacker, Entity target) {

    }

    @Override
    public void postHitExecution(CombatHit hit) {

    }
}
