package net.zaros.server.game.node.entity.player;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.content.market.lending.ItemLendingManager;
import net.zaros.server.game.content.market.moneypouch.MoneyPouch;
import net.zaros.server.game.node.entity.player.link.*;
import net.zaros.server.game.node.entity.player.link.completionist.CompletionistManager;

import net.zaros.server.game.node.entity.player.link.slayer.SlayerManager;
import net.zaros.server.game.node.entity.player.link.summoning.SummoningManager;
import net.zaros.server.game.node.item.degrade.DegradeManager;

import java.util.HashMap;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/29/2017
 */
public final class PlayerManager {
	
	/**
	 * The note manager instance for the player
	 */
	@Getter
	private final NoteManager notes;
	
	/**
	 * The chat manager
	 */
	@Getter
	private final ContactManager contacts;
	
	/**
	 * The activity manager instance
	 */
	@Getter
	private final ActivityManager activities;
	
	/**
	 * The interface manager of the player
	 */
	@Getter
	@Setter
	private transient InterfaceManager interfaces;
	
	/**
	 * The hint icon manager of the player
	 */
	@Getter
	@Setter
	private transient HintIconManager hintIcons;
	
	/**
	 * The lock manager object
	 */
	@Getter
	@Setter
	private transient LockManager locks;
	
	/**
	 * The action manager object
	 */
	@Getter
	@Setter
	private transient ActionManager actions;

	/**
	 * The slayer manager
	 */
	@Getter
	private final SlayerManager slayer;

	/**
	 * The summoning manager
	 */
	@Getter
	private final SummoningManager summoning;

	/**
	 * Skillcape customizer
	 */
	@Getter
	private final CompletionistManager completionist;
	
	/**
	 * The dialogue manager object
	 */
	@Getter
	@Setter
	private transient DialogueManager dialogues;
	
	/**
	 * The web manager
	 */
	@Getter
	private transient WebManager webManager;

	@Getter
	private final ConfigurationManager configurationManager;

	@Getter
	private final DegradeManager degradeManager;

	@Getter
	private final MoneyPouch moneyPouch;

	@Getter
	private final ItemLendingManager lendingManager;

	public PlayerManager() {
		this.notes = new NoteManager();
		this.contacts = new ContactManager();
		this.activities = new ActivityManager();
		this.slayer = new SlayerManager();
		this.completionist = new CompletionistManager();
		this.summoning = new SummoningManager();
		this.webManager = new WebManager();
		this.configurationManager = new ConfigurationManager();
		this.degradeManager = new DegradeManager();
		this.moneyPouch = new MoneyPouch();
		this.lendingManager = new ItemLendingManager();
	}
	
	/**
	 * Registers the transient variables
	 *
	 * @param player
	 * 		The player
	 */
	void registerTransients(Player player) {
		this.setInterfaces(new InterfaceManager());
		this.setActions(new ActionManager());
		this.setLocks(new LockManager());
		this.setDialogues(new DialogueManager(player));
		this.setHintIcons(new HintIconManager());
		this.activities.setPlayer(player);
		this.webManager.setPlayer(player);
		this.interfaces.setPlayer(player);
		this.notes.setPlayer(player);
		this.actions.setEntity(player);
		this.hintIcons.setPlayer(player);
		this.contacts.setPlayer(player);
		this.slayer.setPlayer(player);
		this.summoning.setPlayer(player);
		this.configurationManager.setPlayer(player);
		this.degradeManager.setPlayer(player);
		this.moneyPouch.setPlayer(player);
		this.lendingManager.setPlayer(player);
		player.getSkills().setLevelsAdvanced(new HashMap<>());
	}
	
}