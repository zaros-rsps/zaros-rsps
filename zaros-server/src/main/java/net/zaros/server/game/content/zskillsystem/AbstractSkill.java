package net.zaros.server.game.content.zskillsystem;

import net.zaros.server.game.content.zskillsystem.properties.SkillProperties;
import net.zaros.server.game.content.zskillsystem.properties.status.SkillStatus;
import net.zaros.server.game.content.zskillsystem.properties.type.SkillType;

/**
 * @author Jacob Rhiel
 */
public abstract class AbstractSkill extends SkillProperties implements ISkill {

    public AbstractSkill(boolean enabled, int skillId, SkillStatus status, SkillType type) {
        super(enabled, skillId, status, type);
    }
}
