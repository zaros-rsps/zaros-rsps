package net.zaros.server.game.node.entity.render.flag.impl;

/**
 * A simple enum for graphic heights.
 */
public enum GraphicHeight {

    LOW,

    MIDDLE,

    HIGH,

    ABOVE_HEAD,

    HIGH_3,

    HIGH_4,

    HIGH_5,

    HIGH_6,

    HIGH_7,

    HIGH_8;

    /**
     * Returns the height's integer value.
     *
     * @return the value
     */
    public int toInt() {
        return ordinal() * 50;
    }

}
