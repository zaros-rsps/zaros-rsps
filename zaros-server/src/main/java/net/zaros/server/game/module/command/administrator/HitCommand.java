package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.utility.rs.Hit;
import net.zaros.server.utility.rs.Hit.HitSplat;
import net.zaros.server.utility.tool.Misc;

import java.util.Optional;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/15/2017
 */
@CommandManifest(description = "Adds a hit to yourself", types = { Integer.class })
public class HitCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("hit", "dmg");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		String target = Misc.getArrayEntry(args, 2);
		if (target == null) {
			player.getHitQueue().add(new Hit(player, intParam(args, 1), HitSplat.MELEE_DAMAGE));
		} else {
			Optional<Player> o = World.getPlayerByUsername(target);
			if (!o.isPresent()) {
				player.getTransmitter().sendMessage("Unable to find entity by name '" + target + "'");
				return;
			}
			o.get().getHitQueue().add(new Hit(player, intParam(args, 1)));
		}
	}
}
