package net.zaros.server.game.content.dialogue;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.content.dialogue.messages.ActionDialogueMessage;
import net.zaros.server.game.content.dialogue.messages.ChatDialogueMessage;
import net.zaros.server.game.content.dialogue.messages.ItemDialogueMessage;
import net.zaros.server.game.content.dialogue.messages.NPCDialogueMessage;
import net.zaros.server.game.content.dialogue.messages.OptionDialogueMessage;
import net.zaros.server.game.content.dialogue.messages.PlayerDialogueMessage;
import net.zaros.server.game.content.dialogue.messages.WaitDialogueMessage;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.constant.DialogueConstants;

/**
 * @author Tyluur
 * @author Walied K. Yassen
 */
public abstract class Dialogue implements DialogueConstants {

	/**
	 * An empty runnable.
	 */
	protected static final Runnable EMPTY = () -> {
	};

	/**
	 * Generates the list of messages to use in this dialogue
	 *
	 * @param player
	 *               The player
	 */
	public abstract void constructMessages(Player player);

	/**
	 * The list of messages that will be sent
	 */
	private final Map<Integer, DialogueMessage> messageMap = new HashMap<>();

	/**
	 * The id of the entity we are interacting with
	 */
	@Getter
	@Setter
	protected int chattingId;

	/**
	 * The stage of the dialogue
	 */
	@Getter
	@Setter
	private int stage;

	/**
	 * The parameters that are sent
	 */
	@Getter
	@Setter
	private Object[] parameters;

	/**
	 * Constructs a new dialogue
	 */
	public Dialogue() {
		stage = 1;
	}

	/**
	 * Constructs a new dialogue
	 *
	 * @param message
	 *                The message to add
	 */
	protected void construct(DialogueMessage message) {
		int size = messageMap.size();
		messageMap.put(size + 1, message);
	}

	/**
	 * Gets the message the player should do at the stage
	 *
	 * @param stage
	 *              The stage
	 */
	public Optional<DialogueMessage> getMessageAtStage(int stage) {
		DialogueMessage message = messageMap.get(stage);
		if (message == null) {
			return Optional.empty();
		} else {
			return Optional.of(message);
		}
	}

	/**
	 * Sends the stage to the next one
	 */
	public void nextStage() {
		stage++;
	}

	/**
	 * Gets a parameter from the {@link #parameters} array and casts the type to it
	 *
	 * @param indexId
	 *                The index in the parameters array
	 */
	@SuppressWarnings("unchecked")
	protected <K> K parameter(int indexId) {
		return (K) parameters[indexId];
	}

	/**
	 * Gets a parameter from the {@link #parameters} array and casts the type to it
	 *
	 * @param indexId
	 *                   The index in the parameters array
	 * @param defaultKey
	 *                   if the key is not present, we return the value provided in
	 *                   this parameter.
	 */
	@SuppressWarnings("unchecked")
	protected <K> K parameter(int indexId, Object defaultKey) {
		return (K) (indexId < parameters.length ? parameters[indexId] : defaultKey);
	}

	/**
	 * Ends the dialogue
	 */
	public void end(Player player) {
		player.getManager().getInterfaces().closeChatboxInterface();
		stage = -1;
	}

	/**
	 * If the dialogue is now over.
	 */
	public boolean isOver() {
		return stage > getLastStage();
	}

	/**
	 * Gets the last stage
	 */
	public int getLastStage() {
		return messageMap.size();
	}

	/**
	 * Constructs a wait dialogue message.
	 * 
	 * @param ticks
	 *              how many ticks do we wait before executing the next action.
	 * @author Walied K. Yassen
	 */
	protected void wait(int ticks) {
		construct(new WaitDialogueMessage(ticks));
	}

	/**
	 * Executes a player dialogue
	 *
	 * @param animation
	 *                  The animation to use
	 * @param messages
	 *                  The messages to send
	 */
	protected void player(int animation, String... messages) {
		construct(new PlayerDialogueMessage(animation, messages));
	}

	/**
	 * Executes an npc dialogue
	 *
	 * @param npcId
	 *                  The id of the npc
	 * @param animation
	 *                  The animation of the npc
	 * @param messages
	 *                  The messages of the npc
	 */
	protected void npc(int npcId, int animation, String... messages) {
		construct(new NPCDialogueMessage(npcId, animation, messages));
	}

	/**
	 * Executes an options dialogue
	 *
	 * @param title
	 *                 The title of the dialgoue
	 * @param messages
	 *                 The messages of the dialogue
	 * @param tasks
	 *                 The tasks to execute when we click successfully
	 */
	protected void options(String title, String[] messages, Runnable... tasks) {
		construct(new OptionDialogueMessage(title, messages, tasks));
	}

	/**
	 * Executes a chatbox dialogue
	 *
	 * @param messages
	 *                 The messages on the dialogue
	 */
	protected void chatbox(String... messages) {
		construct(new ChatDialogueMessage(messages));
	}

	/**
	 * Executes an item dialogue
	 *
	 * @param itemId
	 *                   The id of the item
	 * @param itemAmount
	 *                   The amount of the item
	 * @param messages
	 *                   The messages to send
	 */
	protected void item(int itemId, int itemAmount, String... messages) {
		construct(new ItemDialogueMessage(itemId, itemAmount, messages));
	}

	/**
	 * Adds an action dialogue
	 *
	 * @param action
	 *               The action to perform
	 */
	protected void action(Runnable action) {
		construct(new ActionDialogueMessage(action));
	}

	protected static String[] str(String... elements) {
		return elements;
	}

	protected static String madamOrSir(Player player) {
		return genderString(player, "sir", "madam");
	}

	protected static String genderString(Player player, String male, String female) {
		return player.getDetails().getAppearance().isMale() ? male : female;
	}
}
