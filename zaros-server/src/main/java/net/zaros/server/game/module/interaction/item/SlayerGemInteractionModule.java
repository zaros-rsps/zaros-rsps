package net.zaros.server.game.module.interaction.item;

import net.zaros.server.game.module.type.ItemInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.InteractionOption;

public class SlayerGemInteractionModule extends ItemInteractionModule {

    public SlayerGemInteractionModule() {
        register(4155);
    }

    @Override
    public boolean handle(Player player, Item item, int slotId, InteractionOption option) {
        if(option.equals(InteractionOption.SECOND_OPTION))
            player.getManager().getSlayer().sendRemaining(false);
        return true;
    }
}
