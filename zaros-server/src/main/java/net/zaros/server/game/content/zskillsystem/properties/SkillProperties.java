package net.zaros.server.game.content.zskillsystem.properties;

import lombok.AllArgsConstructor;
import lombok.Getter;

import net.zaros.server.game.content.zskillsystem.properties.status.SkillStatus;
import net.zaros.server.game.content.zskillsystem.properties.type.SkillType;

/**
 * Defines the skill.
 * @author Jacob Rhiel
 */
@AllArgsConstructor
@Getter
public class SkillProperties {

    /** Mainly use for skill segments as the over-all skill is technically a plugin. (may be changed) **/
    private final boolean enabled;

    /** The ID of the skill 'parent'. **/
    private final int skillId;

    /** The status of the skills usability. **/
    private SkillStatus status = SkillStatus.DEVELOPMENTAL;

    /** Sets the type of skill. **/
    private final SkillType type;

}
