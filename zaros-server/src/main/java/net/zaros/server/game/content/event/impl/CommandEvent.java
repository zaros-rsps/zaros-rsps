package net.zaros.server.game.content.event.impl;

import net.zaros.server.game.content.event.Event;
import net.zaros.server.game.content.event.EventPolicy.ActionPolicy;
import net.zaros.server.game.content.event.EventPolicy.WalkablePolicy;
import net.zaros.server.game.content.event.context.CommandEventContext;
import net.zaros.server.game.module.command.CommandRepository;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/29/2017
 */
public class CommandEvent extends Event<CommandEventContext> {
	
	public CommandEvent() {
//		setInterfacePolicy(InterfacePolicy.CLOSE);
		setWalkablePolicy(WalkablePolicy.RESET);
		setActionPolicy(ActionPolicy.RESET);
	}
	
	@Override
	public void run(Player player, CommandEventContext context) {
		CommandRepository.processEntry(player, context.getArguments(), context.isConsole());
	}
}
