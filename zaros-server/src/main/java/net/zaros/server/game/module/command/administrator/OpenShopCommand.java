package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.content.market.shop.ShopRepository;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

public class OpenShopCommand extends CommandModule {
    @Override
    public String[] identifiers() {
        return arguments("openshop", "shop");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {

        ShopRepository.open(player, Integer.valueOf(args[1] == null ? 1 : Integer.valueOf(args[1])));

    }
}
