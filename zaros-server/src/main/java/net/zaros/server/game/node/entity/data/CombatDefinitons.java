package net.zaros.server.game.node.entity.data;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.utility.rs.constant.MagicConstants;

/**
 * A simple abstraction for combat definitions.
 *
 * @author Gabriel || Wolfsdarker
 */
public abstract class CombatDefinitons {

    /**
     * The id of the spellbook
     */
    @Getter
    @Setter
    private MagicConstants.MagicBook spellbook = MagicConstants.MagicBook.REGULAR;

    /**
     * The entity's attack style.
     */
    @Getter
    @Setter
    private int attackStyle = 0;

    /**
     * The entity's special energy amount.
     */
    @Getter
    @Setter
    private int specialEnergy = 100;

    /**
     * If the entity attacks upon receiving damage.
     */
    @Getter
    @Setter
    private boolean retaliating = true;

    /**
     * Sets the special activated flag
     */
    @Getter
    @Setter
    private transient boolean specialActivated = false;

    /**
     * The entity's instance.
     */
    @Getter
    private transient Entity entity;

    /**
     * Constructor for the combat definitions.
     *
     * @param entity
     */
    public CombatDefinitons(Entity entity) {
        this.entity = entity;
    }

    /**
     * Reduces the special attack energy by the given amount. This also verifies that we never have < 0 special energy.
     * This can be used with a negative number because we have upper and lower bounds [to add instead of reduce]
     *
     * @param amount The amount to reduce it by.
     */
    public void reduceSpecial(int amount) {
        setSpecialEnergy(getSpecialEnergy() - amount);
        if (this.getSpecialEnergy() <= 0) {
            setSpecialEnergy(0);
        } else if (this.getSpecialEnergy() >= 100) {
            setSpecialEnergy(100);
        }
    }
}
