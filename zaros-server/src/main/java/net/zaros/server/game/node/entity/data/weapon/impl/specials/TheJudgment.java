package net.zaros.server.game.node.entity.data.weapon.impl.specials;

import lombok.Getter;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.content.combat.script.impl.specials.TheJudgmentCombatScript;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;

/**
 * The special attack for armadyl godsword.
 *
 * @author Gabriel || Wolfsdarker
 */
public class TheJudgment implements WeaponSpecial {

    /**
     * The special's combat script.
     */
    private static final TheJudgmentCombatScript script = new TheJudgmentCombatScript();

    /**
     * The special's instance.
     */
    @Getter
    private static final TheJudgment instance = new TheJudgment();

    @Override
    public int attackAnimation() {
        return 11989;
    }

    @Override
    public int attackGraphics() {
        return 2113;
    }

    @Override
    public int targetGraphics() {
        return -1;
    }

    @Override
    public int attackSpeed() {
        return 6;
    }

    @Override
    public int specialRequired() {
        return 50;
    }

    @Override
    public Hit.HitSplat hitStyle() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public CombatScript combatScript() {
        return script;
    }
}
