package net.zaros.server.game.content.skills.fishing.plugin;

import com.google.common.primitives.Ints;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jacob Rhiel
 */
@Getter
public enum FishingSpot {

    NET(FishingTool.SMALL_NET, 952, 332, 952, 1174),

    //TODO 1176, 1177, 1178 / fish ? 2859, 13274, 13275, 13276

    BAIT(FishingTool.FISHING_ROD, 233, 234, 235, 236, 800, 1236, 1237, 1238),

    //TODO: use-rod ? barbarian? 2722

    NET_BAIT(FishingTool.SMALL_NET, FishingTool.FISHING_ROD, 319, 320, 323, 325, 326, 327, 330, 1331, 2067, 2068, 2724, 4908, 5748, 5749, 7636),

    CAGE(FishingTool.CAGE, 6267, 6996, 7862, 7863),

    LURE_BAIT(FishingTool.FLY_FISHING_ROD, FishingTool.FISHING_ROD, 309, 310, 311, 314, 315, 316, 317, 318, 328, 329, 927, 1189, 1190, 3019, 8647),

    CAGE_HARPOON(FishingTool.CAGE, FishingTool.HARPOON, 312, 321, 324, 333, 1332, 1399, 3804, 5470),

    NET_HARPOON(FishingTool.BIG_NET, FishingTool.HARPOON, 313, 322, 334, 1191, 1333, 1405, 1406, 3574, 3475, 3848, 5471, 7044),

    ;

    private final FishingTool firstOptionTool, secondOptionTool;

    private final int[] spotIds;

    FishingSpot(FishingTool firstOptionTool, int... spotIds) {
        this(firstOptionTool, FishingTool.NONE, spotIds);
    }

    FishingSpot(FishingTool firstOptionTool, FishingTool secondOptionTool, int... spotIds) {
        this.firstOptionTool = firstOptionTool;
        this.secondOptionTool = secondOptionTool;
        this.spotIds = spotIds;
    }

    public static int[] allSpots() {
        List<Integer> spots = new ArrayList<>();
        for(FishingSpot spot : FishingSpot.values()) {
            for(int id : spot.getSpotIds()) {
                spots.add(id);
            }
        }
        return Ints.toArray(spots);
    }

    /**
     * Retrieves the fishing spot based on the npc id the player clicks.
     * @param spotId The spot id to find.
     * @return The found fishing spot.
     */
    public static FishingSpot forSpot(int spotId) {
        for(FishingSpot spot : FishingSpot.values()) {
            for(int id : spot.getSpotIds()) {
                if(id == spotId) {
                    return spot;
                }
            }
        }
        return null;
    }

}
