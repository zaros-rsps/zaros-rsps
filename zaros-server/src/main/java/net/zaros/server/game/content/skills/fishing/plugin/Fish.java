package net.zaros.server.game.content.skills.fishing.plugin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.Skills;
import net.zaros.server.game.node.item.Items;

/**
 * @author Jacob Rhiel
 */
@AllArgsConstructor
@Getter
public enum Fish implements IFish {

    SHRIMP(Items.RAW_SHRIMPS1, 1, 10),

    CRAYFISH(Items.RAW_CRAYFISH1, 1, 10),

    SARDINE(Items.RAW_SARDINE1, 5, 20),

    KARAMBWANJI(Items.RAW_KARAMBWANJI, 5, 5) {

        @Override
        public void spotSpecific(FishingSpot spot) {
            //TODO:
        }

    },

    HERRING(Items.RAW_HERRING1, 10, 30),

    ANCHOVIES(Items.RAW_ANCHOVIES1, 15, 40),

    MACKEREL(Items.RAW_MACKEREL1, 16, 20),

    OYSTER(Items.OYSTER1, 16, 10),

    CASKET(Items.CASKET1, 16, 0),

    SEAWEED(Items.SEAWEED1, 16, 1),

    LEATHER_BOOTS(Items.LEATHER_BOOTS1, 16, 1),

    LEATHER_GLOVES(Items.LEATHER_GLOVES1, 16, 1),

    TROUT(Items.RAW_TROUT1, 20, 50),

    COD(Items.RAW_COD1, 23, 45),

    PIKE(Items.RAW_PIKE1, 25, 60),

    SLIMY_EEL(Items.SLIMY_EEL1, 28, 65) {

        @Override
        public void spotSpecific(FishingSpot spot) {
            //TODO:
        }

    },

    SALMON(Items.RAW_SALMON1, 30, 70),

    GIANT_FROGSPAWN(Items.FROG_SPAWN, 33, 75) {

        @Override
        public void spotSpecific(FishingSpot spot) {
            //TODO:
        }

    },

    TUNA(Items.RAW_TUNA1, 35, 80),

    RAINBOW_FISH(Items.RAW_RAINBOW_FISH1, 38, 80) {

        @Override
        public void spotSpecific(FishingSpot spot) {
            System.out.println("test");
        }

    },

    CAVE_EEL(Items.RAW_CAVE_EEL1, 38, 80),

    LOBSTER(Items.RAW_LOBSTER1, 40, 90),

    BASS(Items.RAW_BASS1, 46, 100),

    SWORDFISH(Items.RAW_SWORDFISH1, 50, 100),

    LAVA_EEL(Items.RAW_LAVA_EEL, 53, 30),

    MONKFISH(Items.RAW_MONKFISH1, 62, 120),

    KARAMBWAN(Items.RAW_KARAMBWAN1, 65, 105),

    SHARK(Items.RAW_SHARK1, 76, 110) {
        @Override
        public void special() {
            //TODO: Baron shark shizz.
        }

    },

    /**
     * Juju-fishing potion grants 5 minutes to catch this shark
     **/
    BARON_SHARK(Items.RAW_BARON_SHARK, 76, 110),

    SEAT_TURTLE(Items.RAW_SEA_TURTLE1, 79, 38),

    MANTA_RAY(Items.RAW_MANTA_RAY1, 81, 46),

    CAVEFISH(Items.RAW_CAVEFISH1, 85, 300),

    ROCKTAIL(Items.RAW_ROCKTAIL1, 90, 380),

    TIGER_SHARK(Items.RAW_TIGER_SHARK, 95, 80),

    ;

    private final int rawFishId, requiredLevel, experience;

    /**
     * Gets the fish the player is catching.
     * @param rawFishId The fish the player attempted to catch.
     * @return Whether the fish exists.
     */
    public static Fish getFish(int rawFishId) {
        for(Fish fish : Fish.values()) {
            if(fish.getRawFishId() == rawFishId) {
                return fish;
            }
        }
        return null;
    }

    /**
     * Adds experience to the player for the fish caught.
     * @param player The player receiving the experience.
     * @param fish The fish the player caught.
     * @return Whether the experience was added.
     */
    public static boolean addExperience(Player player, Fish fish) {
        for(Fish f : Fish.values()) {
            if(f.equals(fish)) {
                player.getSkills().addExperienceWithMultiplier(Skills.FISHING, f.getExperience());
                return true;
            }
        }
        return false;
    }

    @AllArgsConstructor
    @Getter
    public enum BarbarianFish implements IFish {

        LEAPING_TROUT(Items.LEAPING_TROUT1,
                Skills.FISHING, 48, 50,
                Skills.STRENGTH, 15, 5,
                Skills.AGILITY, 15, 5),

        TUNA(Items.RAW_TUNA1,
                Skills.FISHING, 55, 80,
                Skills.STRENGTH, 35, 8),

        LEAPING_SALMON(Items.LEAPING_SALMON1,
                Skills.FISHING, 58, 70,
                Skills.STRENGTH, 30, 6,
                Skills.AGILITY, 30, 6),

        LEAPING_STURGEON(Items.LEAPING_STURGEON1,
                Skills.FISHING, 70, 80,
                Skills.STRENGTH, 45, 7,
                Skills.AGILITY, 45, 7),

        SWORDFISH(Items.RAW_SWORDFISH1,
                Skills.FISHING, 70, 100,
                Skills.STRENGTH, 50, 10),

        SHARK(Items.RAW_SHARK1,
                Skills.FISHING, 96, 110,
                Skills.STRENGTH, 76, 11) {
            @Override
            public void special() {
                //TODO: Baron shark shizz.
            }

        },

        BARON_SHARK(Items.RAW_BARON_SHARK,
                Skills.FISHING, 96, 110,
                Skills.STRENGTH, 76, 11),

        ;

        private final int rawFishId, fishSkillId, requiredFishingLevel, fishExperience,
                strengthSkillId, requiredStrengthLevel, strengthExperience,
                agilitySkillId, requiredAgilityLevel, agilityExperience;

        BarbarianFish(int rawFishId, int fishSkillId, int requiredFishingLevel, int fishExperience,
                      int strengthSkillId, int requiredStrengthLevel, int strengthExperience) {
            this.rawFishId = rawFishId;
            this.fishSkillId = fishSkillId;
            this.requiredFishingLevel = requiredFishingLevel;
            this.fishExperience = fishExperience;
            this.strengthSkillId = strengthSkillId;
            this.requiredStrengthLevel = requiredStrengthLevel;
            this.strengthExperience = strengthExperience;
            this.agilitySkillId = Skills.AGILITY;
            this.requiredAgilityLevel = -1;
            this.agilityExperience = 0;
        }

        /**
         * Gets the fish the player is catching.
         * @param rawFishId The fish the player attempted to catch.
         * @return Whether the fish exists.
         */
        public static BarbarianFish getFish(int rawFishId) {
            for(BarbarianFish fish : BarbarianFish.values()) {
                if(fish.getRawFishId() == rawFishId) {
                    return fish;
                }
            }
            return null;
        }

        /**
         * Adds the experience for the fish caught.
         * @param player The player to add experience too.
         * @param fish The fish the player has caught.
         * @return If the experience was able to be added or not.
         */
        public static boolean addExperience(Player player, BarbarianFish fish) {
            for (BarbarianFish barbarianFish : BarbarianFish.values()) {
                if (barbarianFish.equals(fish)) {
                    player.getSkills().addExperienceWithMultiplier(barbarianFish.fishSkillId, barbarianFish.fishExperience);
                    player.getSkills().addExperienceWithMultiplier(barbarianFish.strengthSkillId, barbarianFish.strengthExperience);
                    if(barbarianFish.getRequiredAgilityLevel() != -1) {
                        player.getSkills().addExperienceWithMultiplier(barbarianFish.agilitySkillId, barbarianFish.agilityExperience);
                    }
                    return true;
                }
            }
            return false;
        }

    }

    @AllArgsConstructor
    @Getter
    public enum DungeoneeringFish implements IFish {

        HEIM_CRAB(Items.RAW_HEIM_CRAB1, 1, 9),

        RED_EYE(Items.RAW_REDEYE1, 10, 27),

        DUSK_EEL(Items.DUSK_EEL1, 20, 45),

        GIANT_FLATFISH(Items.RAW_GIANT_FLATFISH1, 30, 63),

        SHORT_FINNED_EEL(Items.RAW_SHORTFINNED_EEL1, 40, 81),

        WEB_SNIPPER(Items.RAW_WEB_SNIPPER1, 50, 99),

        BOULDABASS(Items.RAW_BOULDABASS1, 60, 117),

        SALVE_EEL(Items.RAW_SALVE_EEL1, 70, 135),

        BLUE_CRAB(Items.RAW_BLUE_CRAB1, 80, 153),

        CAVE_MORAY(Items.RAW_CAVE_MORAY1, 90, 171),

        VILE_FISH(Items.RAW_VILE_FISH, -1, 255) {
            @Override
            public void special() {

                //TODO: Level scaling.

            }

        },

        ;

        private final int rawFishId, requiredLevel, experience;

        /**
         * Gets the fish the player is catching.
         * @param rawFishId The fish the player attempted to catch.
         * @return Whether the fish exists.
         */
        public static DungeoneeringFish getFish(int rawFishId) {
            for(DungeoneeringFish fish : DungeoneeringFish.values()) {
                if(fish.getRawFishId() == rawFishId) {
                    return fish;
                }
            }
            return null;
        }

        /**
         * Adds experience to the player for the fish caught.
         * @param player The player receiving the experience.
         * @param fish The fish the player caught.
         * @return Whether the experience was added.
         */
        public static boolean addExperience(Player player, DungeoneeringFish fish) {
            for(DungeoneeringFish f : DungeoneeringFish.values()) {
                if(f.equals(fish)) {
                    player.getSkills().addExperienceWithMultiplier(Skills.FISHING, f.getExperience());
                    return true;
                }
            }
            return false;
        }


    }


}
