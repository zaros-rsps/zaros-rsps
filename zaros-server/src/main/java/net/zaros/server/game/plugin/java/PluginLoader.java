package net.zaros.server.game.plugin.java;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.zaros.server.game.plugin.IPluginLoader;
import net.zaros.server.game.plugin.PluginException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


/**
 * Represents the plugin file name.
 * 
 * @author Walied K. Yassen
 * @since 0.1
 */
public final class PluginLoader implements IPluginLoader<Plugin> {

	/**
	 * The manifest plugin file name.
	 */
	private static final String MANIFEST_FILE = "manifest.json";

	/**
	 * The {@link Gson} object which is used to load or read the
	 * {@link PluginManifest} content.
	 */
	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

	/**
	 * The initialised plugins' class loaders.
	 */
	private final Map<String, PluginClassLoader> loaders = new HashMap<String, PluginClassLoader>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.praesul.core.plugin.IPluginLoader#load(java.io.File)
	 */
	@Override
	public Plugin load(File file) {
		if (!file.exists()) {
			throw new PluginException("The specified plugin file does not exist!");
		}
		PluginManifest manifest = extractManifest(file);
		PluginClassLoader loader = null;
		try {
			loader = new PluginClassLoader(this, getClass().getClassLoader(), file);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		Plugin plugin = instantiatePlugin(loader, manifest);
		plugin.initialise(manifest, null);
		loaders.put(manifest.id, loader);
		return plugin;
	}

	/**
	 * Instantiates the specified plug-in manifest. It looks for the main-class as
	 * specified in the {@link PluginManifest#getMainClass()} using the specified
	 * {@link ClassLoader} then looks for the expected constructor and instantiates
	 * it to a {@link Plugin} object, if anything goes wrong during this process, an
	 * {@link PluginException} will be thrown describing what is wrong.
	 *
	 * @param loader
	 *                     the plug-in class loader, usually refers to the JAR class
	 *                     loader.
	 * @param manifest
	 *                     the plug-in manifest, which holds the plug-in data.
	 * @return the instantiated {@link Plugin} object.
	 * @throws PluginException
	 *                             if the main-class could not be found or
	 *                             instantiated.
	 */
	private Plugin instantiatePlugin(ClassLoader loader, PluginManifest manifest) {
		// look-up for the main-class.
		Class<?> rawClass;
		try {
			rawClass = loader.loadClass(manifest.getMainClass());
		} catch (ClassNotFoundException e) {
			throw new PluginException("Could not find or load the main-class: " + manifest.getMainClass());
		}
		// check if the main-class is applicable or not.
		if (!Plugin.class.isAssignableFrom(rawClass)) {
			throw new PluginException("The main-class does not refer to a Plugin class, make sure you are extending the org.runekit.plugins.Plugin class");
		}
		// cast the class type to org.runekit.plugins.Plugin class type.
		Class<? extends Plugin> mainClass;
		try {
			mainClass = rawClass.asSubclass(Plugin.class);
		} catch (ClassCastException e) {
			throw new PluginException("The plugin main-class does not extend Plugin (main-class: " + rawClass + ")");
		}
		// find the constructor we want to use to instantiate the object.
		Constructor<? extends Plugin> constructor;
		try {
			constructor = mainClass.getConstructor();
		} catch (NoSuchMethodException e) {
			throw new PluginException("Could not find a matching public constructor.");
		}
		// instantiate the Plugin object.
		try {
			return constructor.newInstance();
		} catch (Throwable e) {
			throw new PluginException("An error occurred while instantiating the plugin", e);
		}
	}

	/**
	 * Extracts and verifies the {@link PluginManifest} object from the given plugin
	 * {@code .jar} file path.
	 * 
	 * @param file
	 *                 the fi
	 * @return the verified {@link PluginManifest} object.
	 * @throws PluginException
	 *                             if anything occurrs during the verification
	 *                             process or the extracting process.
	 */
	private PluginManifest extractManifest(File file) {
		try (JarFile jar = new JarFile(file)) {
			JarEntry entry = jar.getJarEntry(MANIFEST_FILE);
			if (entry == null) {
				throw new PluginException("Could not find the manifest file in the plugin .jar file");
			}
			InputStream stream = jar.getInputStream(entry);
			PluginManifest manifest = GSON.fromJson(new InputStreamReader(stream), PluginManifest.class);
			if (manifest == null) {
				throw new PluginException("Could not parse the manifest file in the plugin .jar file");
			}
			return verifyManifest(manifest);
		} catch (IOException e) {
			throw new PluginException("Error while accessing the manifest file in plugin: " + file.getName(), e);
		}
	}

	private PluginManifest verifyManifest(PluginManifest manifest) {
		if (manifest.id == null || manifest.id.trim().length() < 12) {
			throw new PluginException("The manifest \"id\" field must be at-least 12 characters long");
		}
		String id = manifest.id.trim();
		if (manifest.name == null || manifest.name.trim().length() < 6) {
			throw new PluginException("The manifest \"id\" field must be at-least 6 characters long");
		}
		String name = manifest.name.trim();
		if (manifest.mainClass == null || manifest.mainClass.trim().length() < 6) {
			throw new PluginException("The manifest \"id\" field must be at-least 6 characters long");
		}
		String mainClass = manifest.mainClass.trim();
		return new PluginManifest(id, name, manifest.description, manifest.authors, mainClass);
	}
}
