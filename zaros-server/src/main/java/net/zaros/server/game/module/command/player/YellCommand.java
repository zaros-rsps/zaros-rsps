package net.zaros.server.game.module.command.player;

import net.zaros.discord.util.ZarosChannels;
import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.utility.tool.ColorConstants;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/20/2017
 */
@CommandManifest(description = "Yells a message", types = { String.class })
public class YellCommand extends CommandModule {

	@Override
	public String[] identifiers() {
		return arguments("yell");
	}

	@Override
	public void handle(Player player, String[] args, boolean console) {
		String message = getCompleted(args, 1);
		if (message == null || message.equalsIgnoreCase("null")) {
			return;
		}
		sendYellMessage(player, message);
	}

	/**
	 * Sends a yell message
	 *
	 * @param player
	 *                The player yelling
	 * @param message
	 *                The message
	 */
	public static void sendYellMessage(Player player, String message) {
		message = Misc.fixChatMessage(message.replaceAll("<", "")).trim();
		StringBuilder tag = new StringBuilder();
		tag.append("[<col=" + ColorConstants.BLUE + ">RR</col>] ");
		tag.append(player.getDetails().getDisplayName()).append(": ").append(message);
		for (Player pl : World.getPlayers()) {
			if (pl == null) {
				continue;
			}
			pl.getTransmitter().sendMessage(tag.toString());
		}
		ZarosChannels.getYellChannel().sendMessage(player.getDetails().getDisplayName() + ": " + message).join();
	}
}
