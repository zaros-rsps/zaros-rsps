package net.zaros.server.game.content.combat.player.prayer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zaros.server.game.node.entity.player.data.Skills;

import java.util.Arrays;

/**
 * @author Jacob Rhiel
 */
public class PrayerDefinition {

    @AllArgsConstructor
    @Getter
    public enum PrayerType {

        NONE(0),

        ATTACK_PRAYERS((1 << 3) | (1 << 8) | (1 << 16)),

        STRENGTH_PRAYERS((1 << 2) | (1 << 7) | (1 << 15)),

        DEFENCE_PRAYERS((1 << 1) | (1 << 6) | (1 << 14)),

        RANGED_PRAYERS((1 << 4) | (1 << 12) | (1 << 21)),

        MAGIC_PRAYERS((1 << 5) | (1 << 13) | (1 << 22)),

        RESTORE_PRAYERS((1 << 9) | (1 << 10) | (1 << 27)),

        PROTECTION_PRAYERS((1 << 18) | (1 << 19) | (1 << 20)),

        DEFLECTION_PRAYERS((1 << 38) | (1 << 39) | (1 << 40)),

        OVERHEAD_PRAYERS((1 << 23) | (1 << 24) | (1 << 25)),

        OVERHEAD_PRAYERS_CURSES(PrayerType.DEFLECTION_PRAYERS.prayerFlag | (1 << 47) | (1 << 48) | (1 << 49)),

        OVERHEAD_AND_PROTECTION_PRAYERS(PrayerType.OVERHEAD_PRAYERS.prayerFlag | PrayerType.PROTECTION_PRAYERS.prayerFlag),

        OVERHEAD_AND_PROTECTION_CURSES(PrayerType.OVERHEAD_PRAYERS_CURSES.prayerFlag | PrayerType.DEFLECTION_PRAYERS.prayerFlag),

        MULTIPLE_BONUS_PRAYERS((1 << 26) | (1 << 28) | (1 << 29) | (1 << 30)),

        MULTIPLE_BONUS_AND_DEFENCE_PRAYERS(PrayerType.DEFENCE_PRAYERS.prayerFlag | PrayerType.MULTIPLE_BONUS_PRAYERS.prayerFlag),

        MULTIPLE_BONUS_AND_STRENGTH_PRAYERS(PrayerType.STRENGTH_PRAYERS.prayerFlag | PrayerType.MULTIPLE_BONUS_PRAYERS.prayerFlag),

        MULTIPLE_BONUS_AND_ATTACK_PRAYERS(PrayerType.ATTACK_PRAYERS.prayerFlag | PrayerType.MULTIPLE_BONUS_PRAYERS.prayerFlag),

        MULTIPLE_BONUS_AND_RANGED_PRAYERS(PrayerType.RANGED_PRAYERS.prayerFlag | PrayerType.MULTIPLE_BONUS_PRAYERS.prayerFlag),

        MULTIPLE_BONUS_AND_MAGIC_PRAYERS(PrayerType.MAGIC_PRAYERS.prayerFlag | PrayerType.MULTIPLE_BONUS_PRAYERS.prayerFlag),

        NON_STACKABLE_PRAYERS(PrayerType.OVERHEAD_AND_PROTECTION_PRAYERS.prayerFlag | (1 << 17)),

        NON_STACKABLE_CURSES(PrayerType.OVERHEAD_PRAYERS_CURSES.prayerFlag | (1 << 37)),

        SINGLE_BOOST_PRAYERS(PrayerType.DEFENCE_PRAYERS.prayerFlag | PrayerType.ATTACK_PRAYERS.prayerFlag | PrayerType.STRENGTH_PRAYERS.prayerFlag | PrayerType.RANGED_PRAYERS.prayerFlag | PrayerType.MAGIC_PRAYERS.prayerFlag),

        SINGLE_BOOST_AND_MULTIPLE_BONUS_PRAYERS(PrayerType.MULTIPLE_BONUS_PRAYERS.prayerFlag | PrayerType.SINGLE_BOOST_PRAYERS.prayerFlag),

        SAP_CURSES((1 << 32) | (1 << 33) | (1 << 34) | (1 << 35) | (1 << 50)),

        LEECH_CURSES((1 << 41) | (1 << 42) | (1 << 43) | (1 << 44) | (1 << 45) | (1 << 46) | (1 << 47) | (1 << 50)),

        SAP_AND_LEECH_CURSES(PrayerType.SAP_CURSES.prayerFlag | PrayerType.LEECH_CURSES.prayerFlag),

        NON_STACKABLE_LEECH(PrayerType.LEECH_CURSES.prayerFlag | PrayerType.NON_STACKABLE_CURSES.prayerFlag),

        NON_STACKABLE_SAP(PrayerType.SAP_CURSES.prayerFlag | PrayerType.NON_STACKABLE_CURSES.prayerFlag),

        ;

        private final int prayerFlag;

    }

    @Getter
    public enum PrayerNode {

        THICK_SKIN             ("Thick Skin"             , 0  , 1 << 1  , PrayerType.MULTIPLE_BONUS_AND_DEFENCE_PRAYERS,      1,  12, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.DEFENCE , 0.05)), // Tier 1 Defence   +5%
        BURST_OF_STRENGTH      ("Burst of Strength"      , 1  , 1 << 2  , PrayerType.MULTIPLE_BONUS_AND_STRENGTH_PRAYERS,     4,  12, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.STRENGTH, 0.05)), // Tier 1 Strength  +5%
        CLARITY_OF_THOUGHT     ("Clarity of Thought"     , 2  , 1 << 3  , PrayerType.MULTIPLE_BONUS_AND_ATTACK_PRAYERS,       7,  12, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.ATTACK  , 0.05)), // Tier 1 Attack    +5%
        ROCK_SKIN              ("Rock Skin"              , 5  , 1 << 6  , PrayerType.MULTIPLE_BONUS_AND_DEFENCE_PRAYERS,      10,  6, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.DEFENCE , 0.10)), // Tier 2 Defence   +10%
        SUPERHUMAN_STRENGTH    ("Superhuman Strength"    , 6  , 1 << 7  , PrayerType.MULTIPLE_BONUS_AND_STRENGTH_PRAYERS,     13,  6, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.STRENGTH, 0.10)), // Tier 2 Strength  +10%
        IMPROVED_REFLEXES      ("Improved Reflexes"      , 7  , 1 << 8  , PrayerType.MULTIPLE_BONUS_AND_ATTACK_PRAYERS,       16,  6, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.ATTACK  , 0.10)), // Tier 2 Attack    +10%
        RAPID_RESTORE          ("Rapid Restore"          , 8  , 1 << 9  , PrayerType.RESTORE_PRAYERS,                         19, 26, PrayerIcon.NONE,                   PrayerBook.NORMAL ),
        RAPID_HEAL             ("Rapid Heal"             , 9  , 1 << 10 , PrayerType.RESTORE_PRAYERS,                         19, 18, PrayerIcon.NONE,                   PrayerBook.NORMAL ),
        PROTECT_ITEM           ("Protect Item"           , 10 , 1 << 11 , PrayerType.NONE,                                    25, 18, PrayerIcon.NONE,                   PrayerBook.NORMAL ),
        STEEL_SKIN             ("Steel Skin"             , 13 , 1 << 14 , PrayerType.MULTIPLE_BONUS_AND_DEFENCE_PRAYERS,      28,  3, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.DEFENCE , 0.15)), // Tier 3 Defence   +15%
        ULTIMATE_STRENGTH      ("Ultimate Strength"      , 14 , 1 << 15 , PrayerType.MULTIPLE_BONUS_AND_STRENGTH_PRAYERS,     31,  3, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.STRENGTH, 0.15)), // Tier 3 Strength  +15%
        INCREDIBLE_REFLEXES    ("Incredible Reflexes"    , 15 , 1 << 16 , PrayerType.MULTIPLE_BONUS_AND_ATTACK_PRAYERS,       34,  3, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.ATTACK  , 0.15)), // Tier 3 Attack    +15%
        PROTECT_FROM_MAGIC     ("Protect from Magic"     , 17 , 1 << 18 , PrayerType.OVERHEAD_AND_PROTECTION_PRAYERS,         37,  3, PrayerIcon.PROTECT_FROM_MAGIC,     PrayerBook.NORMAL ),
        PROTECT_FROM_MISSILES  ("Protect from Missiles"  , 18 , 1 << 19 , PrayerType.OVERHEAD_AND_PROTECTION_PRAYERS,         40,  3, PrayerIcon.PROTECT_FROM_RANGE,     PrayerBook.NORMAL ),
        PROTECT_FROM_MELEE     ("Protect from Melee"     , 19 , 1 << 20 , PrayerType.OVERHEAD_AND_PROTECTION_PRAYERS,         43,  3, PrayerIcon.PROTECT_FROM_MELEE,     PrayerBook.NORMAL ),
        RETRIBUTION            ("Retribution"            , 22 , 1 << 23 , PrayerType.NON_STACKABLE_PRAYERS,                   46, 12, PrayerIcon.RETRIBUTION,            PrayerBook.NORMAL ),
        REDEMPTION             ("Redemption"             , 23 , 1 << 24 , PrayerType.NON_STACKABLE_PRAYERS,                   49,  6, PrayerIcon.REDEMPTION,             PrayerBook.NORMAL ),
        SMITE                  ("Smite"                  , 24 , 1 << 25 , PrayerType.NON_STACKABLE_PRAYERS,                   52,  2, PrayerIcon.SMITE,                  PrayerBook.NORMAL ),
        SHARP_EYE              ("Sharp Eye"              , 3  , 1 << 4  , PrayerType.MULTIPLE_BONUS_AND_RANGED_PRAYERS,       8,  12, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.RANGE   , 0.05)), // Tier 1 Range     +5%
        MYSTIC_WILL            ("Mystic Will"            , 4  , 1 << 5  , PrayerType.MULTIPLE_BONUS_AND_MAGIC_PRAYERS,        9,  12, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.MAGIC   , 0.05)), // Tier 1 Magic     +5%
        HAWK_EYE               ("Hawk Eye"               , 11 , 1 << 12 , PrayerType.MULTIPLE_BONUS_AND_RANGED_PRAYERS,       26,  6, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.RANGE   , 0.10)), // Tier 2 Range     +10%
        MYSTIC_LURE            ("Mystic Lore"            , 12 , 1 << 13 , PrayerType.MULTIPLE_BONUS_AND_MAGIC_PRAYERS,        27,  6, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.MAGIC   , 0.10)), // Tier 2 Magic     +10%
        EAGLE_EYE              ("Eagle Eye"              , 20 , 1 << 21 , PrayerType.MULTIPLE_BONUS_AND_RANGED_PRAYERS,       44,  3, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.RANGE   , 0.15)), // Tier 3 Range    +15%
        MYSTIC_MIGHT           ("Mystic Might"           , 21 , 1 << 22 , PrayerType.MULTIPLE_BONUS_AND_MAGIC_PRAYERS,        45,  3, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.MAGIC   , 0.15)), // Tier 3 Magic    +15%
        PROTECT_FROM_SUMMONING ("Protect from Summoning" , 16 , 1 << 17 , PrayerType.OVERHEAD_PRAYERS,                        35,  3, PrayerIcon.PROTECT_FROM_SUMMONING, PrayerBook.NORMAL ),
        CHIVALRY               ("Chivalry"               , 25 , 1 << 26 , PrayerType.SINGLE_BOOST_AND_MULTIPLE_BONUS_PRAYERS, 60,  3, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.DEFENCE, 0.20), new PrayerBonus(Skills.STRENGTH, 0.18), new PrayerBonus(Skills.ATTACK, 0.15) ), // Tier 1 Multi-Melee +20% Defence, +18% Strength, +15% Attack
        PIETY                  ("Piety"                  , 27 , 1 << 28 , PrayerType.SINGLE_BOOST_AND_MULTIPLE_BONUS_PRAYERS, 70,  3, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.DEFENCE, 0.25), new PrayerBonus(Skills.STRENGTH, 0.23), new PrayerBonus(Skills.ATTACK, 0.20) ), // Tier 2 Multi-Melee +25% Defence, +23% Strength, +20% Attack
        RAPID_RENEWAL          ("Rapid Renewal"          , 26 , 1 << 27 , PrayerType.RESTORE_PRAYERS,                         65,  2, PrayerIcon.NONE,                   PrayerBook.NORMAL ),
        AUGURY                 ("Augury"                 , 29 , 1 << 30 , PrayerType.SINGLE_BOOST_AND_MULTIPLE_BONUS_PRAYERS, 70,  2, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.RANGE  , 0.25), new PrayerBonus(Skills.DEFENCE, 0.20) ), // Tier 1 Multi-Range +25% Defence, +20% Range
        RIGOUR                 ("Rigour"                 , 28 , 1 << 29 , PrayerType.SINGLE_BOOST_AND_MULTIPLE_BONUS_PRAYERS, 70,  2, PrayerIcon.NONE,                   PrayerBook.NORMAL, new PrayerBonus(Skills.MAGIC  , 0.25), new PrayerBonus(Skills.DEFENCE, 0.20) ), // Tier 1 Multi-Magic +25% Defence, +20% Magic
        PROTECT_ITEM_CURSES    ("Protect Item"           , 0  , 1 << 31 , PrayerType.NONE,                                    50, 18, PrayerIcon.NONE,                   PrayerBook.CURSES),
        SAP_WARRIOR            ("Sap Warrior"            , 1  , 1 << 32 , PrayerType.LEECH_CURSES,                            50,  3, PrayerIcon.NONE,                   PrayerBook.CURSES),
        SAP_RANGER             ("Sap Ranger"             , 2  , 1 << 33 , PrayerType.LEECH_CURSES,                            52,  3, PrayerIcon.NONE,                   PrayerBook.CURSES),
        SAP_MAGE               ("Sap Mage"               , 3  , 1 << 34 , PrayerType.LEECH_CURSES,                            54,  3, PrayerIcon.NONE,                   PrayerBook.CURSES),
        SAP_SPIRIT             ("Sap Spirit"             , 4  , 1 << 35 , PrayerType.LEECH_CURSES,                            56,  3, PrayerIcon.NONE,                   PrayerBook.CURSES),
        BERSERKER              ("Berserker"              , 5  , 1 << 36 , PrayerType.NONE,                                    59, 18, PrayerIcon.NONE,                   PrayerBook.CURSES),
        DEFLECT_SUMMONING      ("Deflect Summoning"      , 6  , 1 << 37 , PrayerType.OVERHEAD_PRAYERS_CURSES,                 62,  3, PrayerIcon.DEFLECT_SUMMONING,      PrayerBook.CURSES),
        DEFLECT_MAGIC          ("Deflect Magic"          , 7  , 1 << 38 , PrayerType.OVERHEAD_AND_PROTECTION_CURSES,          65,  3, PrayerIcon.DEFLECT_MAGIC,          PrayerBook.CURSES),
        DEFLECT_MISSILES       ("Deflect Missiles"       , 8  , 1 << 39 , PrayerType.OVERHEAD_AND_PROTECTION_CURSES,          68,  3, PrayerIcon.DEFLECT_MISSILES,       PrayerBook.CURSES),
        DEFLECT_MELEE          ("Deflect Melee"          , 9  , 1 << 40 , PrayerType.OVERHEAD_AND_PROTECTION_CURSES,          71,  3, PrayerIcon.DEFLECT_MELEE,          PrayerBook.CURSES),
        LEECH_ATTACK           ("Leech Attack"           , 10 , 1 << 41 , PrayerType.SAP_CURSES,                              74,  4, PrayerIcon.NONE,                   PrayerBook.CURSES),
        LEECH_RANGED           ("Leech Ranged"           , 11 , 1 << 42 , PrayerType.SAP_CURSES,                              76,  4, PrayerIcon.NONE,                   PrayerBook.CURSES),
        LEECH_MAGIC            ("Leech Magic"            , 12 , 1 << 43 , PrayerType.SAP_CURSES,                              78,  4, PrayerIcon.NONE,                   PrayerBook.CURSES),
        LEECH_DEFENCE          ("Leech Defence"          , 13 , 1 << 44 , PrayerType.SAP_CURSES,                              80,  4, PrayerIcon.NONE,                   PrayerBook.CURSES),
        LEECH_STRENGTH         ("Leech Strength"         , 14 , 1 << 45 , PrayerType.SAP_CURSES,                              82,  4, PrayerIcon.NONE,                   PrayerBook.CURSES),
        LEECH_ENERGY           ("Leech Energy"           , 15 , 1 << 46 , PrayerType.SAP_CURSES,                              84,  4, PrayerIcon.NONE,                   PrayerBook.CURSES),
        LEECH_SPECIAL_ATTACK   ("Leech Special Attack"   , 16 , 1 << 47 , PrayerType.NON_STACKABLE_SAP,                       86,  4, PrayerIcon.LEECH_SPECIAL_ATTACK,   PrayerBook.CURSES),
        WRATH                  ("Wrath"                  , 17 , 1 << 48 , PrayerType.NON_STACKABLE_CURSES,                    89, 12, PrayerIcon.WRATH,                  PrayerBook.CURSES),
        SOUL_SPLIT             ("Soul Split"             , 18 , 1 << 49 , PrayerType.NON_STACKABLE_CURSES,                    92,  2, PrayerIcon.SOUL_SPLIT,             PrayerBook.CURSES),
        TURMOIL                ("TURMOIL"                , 19 , 1 << 50 , PrayerType.SAP_AND_LEECH_CURSES,                    95,  2, PrayerIcon.NONE,                   PrayerBook.CURSES),

        ;

        final String name;    // The name of this prayer
        final int id;         // Ordinal
        final int flag;       // Bit flag assigned to this prayer, maximum of 31 prayers per book
        final PrayerType prayerType;    // The prayers that are to be disabled before this prayer is activated
        final int requiredLevel;   // The minimal level required to use this prayer
        final double drain;   // The rate of prayer-point drain every second
        final PrayerIcon headIcon;  // The overhead icon sprite assigned to this prayer
        final PrayerBook book;// The prayer book where this prayer exists
        final PrayerBonus[] skillBonuses;//Skill bonuses and or negations for the relevant prayer.

        PrayerNode(String name, int id, int flag, PrayerType prayerType, int requiredLevel, double drain, PrayerIcon headIcon, PrayerBook book, PrayerBonus... skillBonuses) {
            this.name = name;
            this.id = id;
            this.flag = flag;
            this.prayerType = prayerType;
            this.requiredLevel = requiredLevel;
            this.drain = drain;
            this.headIcon = headIcon;
            this.book = book;
            this.skillBonuses = skillBonuses;
        }

        public boolean hasPrayerIcon() {
            return (getHeadIcon() != PrayerIcon.NONE);
        }

        public static PrayerNode forId(int prayerId, boolean curses) {
            for(PrayerNode prayer : PrayerNode.values()) {
                if (!curses && prayer.getId() == prayerId)
                    return prayer;
                if(curses && prayer.ordinal() == prayerId)
                    return prayer;
            }
            return null;
        }

        public static PrayerNode forId(PrayerNode prayerNode) {
            return Arrays.stream(PrayerNode.values()).filter(prayer -> prayerNode.getId() == prayerNode.getId()).findFirst().orElse(null);
        }

    }

}
