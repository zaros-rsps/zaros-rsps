package net.zaros.server.game.module.interaction.item;

import net.zaros.server.game.module.type.ItemInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.InteractionOption;

public class SkillcapeInteractionModule extends ItemInteractionModule {

    public SkillcapeInteractionModule() {
        register(20767, 20769, 20771);
    }

    @Override
    public boolean handle(Player player, Item item, int slotId, InteractionOption option) {
        if (option != InteractionOption.THIRD_OPTION) {
            return false;
        } else {
            player.getManager().getCompletionist().start(player, item.getId());
            return true;
        }
    }
}
