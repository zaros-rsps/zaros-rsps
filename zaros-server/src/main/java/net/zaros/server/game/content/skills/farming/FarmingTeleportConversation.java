package net.zaros.server.game.content.system.zskillsystem.farming;


import java.util.EnumSet;
import java.util.Set;

public class FarmingTeleportConversation extends Conversation {

    public FarmingTeleportConversation(Player player) {
        super(player);
    }

    public enum Destination {

		CATHERBY(new Position(2807, 3464, 0)),
        FALADOR(new Position(3051, 3304, 0)),
		PHASMATYS(new Position(3603, 3532, 0)),
		ARDOUGNE(new Position(2673, 3374, 0))
        ;

        public static final Set<Destination> SET = EnumSet.allOf(Destination.class);

        private final Position position;

        Destination(Position position) {
            this.position = position;
        }

        String getName() {
            String initial = name().toLowerCase();
            return initial.substring(0, 1).toUpperCase() + initial.substring(1).replaceAll(("_"), " ");
        }
    }

    @Override
    public void init() {
        DialogueOption[] options = Destination.SET.stream().map(destination -> new DialogueOption(destination.getName(), () -> TeleportsInterface.
                teleport(player, TeleportsInterface.TeleportsData.FARMING, destination.position)))
                .toArray(DialogueOption[]::new);
        options(options);
    }
}
