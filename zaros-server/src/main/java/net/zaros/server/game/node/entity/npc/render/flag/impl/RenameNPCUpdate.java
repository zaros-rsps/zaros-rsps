package net.zaros.server.game.node.entity.npc.render.flag.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.UpdateFlag;
import net.zaros.server.network.world.packet.PacketBuilder;

/**
 * Represents the rename NPC update mask.
 *
 * @author Emperor
 */
public class RenameNPCUpdate extends UpdateFlag {
	
	/**
	 * The name to set.
	 */
	private final String name;
	
	/**
	 * Constructs a new {@code RenameNPCUpdate} {@code Object}.
	 *
	 * @param name
	 * 		The name to set.
	 */
	public RenameNPCUpdate(String name) {
		this.name = name;
	}
	
	@Override
	public void write(Player outgoing, PacketBuilder bldr) {
		bldr.writeString(name);
	}
	
	@Override
	public int getOrdinal() {
		return 17;
	}
	
	@Override
	public int getMaskData() {
		return 0x100000;
	}
	
}
