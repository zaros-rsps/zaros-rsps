package net.zaros.server.game.node.entity.hit;

/**
 * The instant the hit should be applied.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum HitInstant {

	/**
	 * The hit should be calculated at the the creation of the pending hit.
	 */
	CALCULATION,

	/**
	 * The hit should be created at the execution of the damage.
	 */
	APPLICATION

}
