package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.content.perks.Perk;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;

import java.util.Optional;

public class PlayerPerkCommand extends CommandModule {

    @Override
    public String[] identifiers() {
        return arguments("giveperk", "perk", "getperks");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {

        String playerToGive = args[1];

        Optional<Player> sentPlayer = World.getPlayerByUsername(playerToGive);

        if(args[0].equalsIgnoreCase("getperks")) {
            sentPlayer.ifPresent(p -> player.getTransmitter().sendMessage("Player: " + playerToGive + " has " + p.getPerks().toString() + "perks."));
            return;
        }

        String perk = args[2];

        if(sentPlayer.isPresent()) {
            sentPlayer.ifPresent(p -> p.getPerks().addPerk(Perk.getPerk(perk)));
            player.getTransmitter().sendMessage("Successfully gave perk: " + perk + " to " + playerToGive);
        } else player.getTransmitter().sendMessage("Could not find player: " + playerToGive);


    }

}

