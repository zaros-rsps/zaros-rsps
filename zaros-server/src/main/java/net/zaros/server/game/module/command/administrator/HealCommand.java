package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.module.command.CommandManifest;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/7/2017
 */
@CommandManifest(description = "Heals you to max health")
public class HealCommand extends CommandModule {
	
	@Override
	public String[] identifiers() {
		return arguments("heal");
	}
	
	@Override
	public void handle(Player player, String[] args, boolean console) {
		player.restoreAll();
	}
}
