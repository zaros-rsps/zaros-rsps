package net.zaros.server.game.module.interaction.item;

import net.zaros.server.game.content.dialogue.impl.jewellry.JewelryDialogue;
import net.zaros.server.game.content.teleportation.EnchantedJewellery;
import net.zaros.server.game.module.type.ItemInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.InteractionOption;

public class JewelryInteractionModule extends ItemInteractionModule {

    public JewelryInteractionModule() {
        register(EnchantedJewellery.getJewellryItems());
    }

    @Override
    public boolean handle(Player player, Item item, int slotId, InteractionOption option) {
        player.getManager().getDialogues().startDialogue(new JewelryDialogue(item, slotId));
        return false;
    }

}
