package net.zaros.server.game.content.combat.data.magic.ancientspells;

import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.combat.data.magic.CombatSpell;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitEffectType;
import net.zaros.server.game.node.entity.hit.HitInstant;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.item.Items;
import net.zaros.server.utility.rs.constant.MagicConstants;

import java.util.List;

/**
 * The blood blitz spell on the ancient spellbook.
 *
 * @author Gabriel || Wolfsdarker
 */
public class BloodBlitz extends CombatSpell {

    /**
     * The spell's ID.
     */
    public static final int spellID = 25;

    /**
     * The spell's required items.
     */
    private static final List<Item> requiredItems = List.of(new Item(Items.BLOOD_RUNE1, 4), new Item(Items.DEATH_RUNE1, 2));

    @Override
    public void playGraphics(Entity source, Entity target) {
    }

    @Override
    public void playAnimation(Entity source, Entity target) {
        source.sendAnimation(1978);
    }

    @Override
    public void sendProjectile(Entity source, Entity target) {
        ProjectileManager.sendProjectile(ProjectileManager.createSpeedDefinedProjectile(source, target, 374, 30, 26, 52, 0, 0));
    }

    @Override
    public HitEffect getEffect(Entity source, Entity target) {
        return new HitEffect(HitEffectType.HEAL_ON_DAMAGE, HitInstant.CALCULATION, 0.25);
    }

    @Override
    public void postHitExecution(CombatHit hit) {
        if (hit.isAccurate()) {
            hit.getTarget().sendGraphics(375);
        }
    }

    @Override
    public int spellID() {
        return spellID;
    }

    @Override
    public int autocastConfigID() {
        return 83;
    }

    @Override
    public int magicLevelRequirement() {
        return 80;
    }

    @Override
    public int maximumHit(Entity entity) {
        return 250;
    }

    @Override
    public int attackDelay() {
        return 5;
    }

    @Override
    public double baseExperience() {
        return 45;
    }

    @Override
    public MagicConstants.MagicBook magicBook() {
        return MagicConstants.MagicBook.ANCIENTS;
    }

    @Override
    public List<Item> requiredItems() {
        return requiredItems;
    }
}
