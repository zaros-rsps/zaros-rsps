package net.zaros.server.game.content.system.zskillsystem.farming.actions;

import net.zaros.server.game.content.system.zskillsystem.farming.impl.FarmingConstants;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.PatchState;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.Patches;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;
import net.zaros.server.game.node.item.Items;

/**
 * Handles the action of watering the patch.
 *
 * @author Gabriel || Wolfsdarker
 */
public class WaterPatchAction extends PlayerTask {

	/**
	 * Constructor for the action.
	 *
	 * @param player
	 * @param state
	 * @param item_id
	 */
	public WaterPatchAction(Player player, PatchState state, int item_id) {
		super(5, player, () -> {

			state.setWatered(true);

			player.getInventory().remove(item_id, 1);
			player.getInventory().addItem(item_id == Items.WATERING_CAN_1 ? item_id - 2 : item_id - 1, 1);

			player.getFarming().updatePatches(player);
			player.endCurrentTask();
		});

		onEnd(() -> {
			player.sendAnimation(new Animation(65535));
			player.getMovementQueue().reset();
		});

		stopUpon(MovementPacketListener.class);
	}

	/**
	 * Handles the action to water the patch.
	 *
	 * @param player
	 * @param data
	 * @param position
	 * @param item_id
	 * @return if the patch was found and could be watered
	 */
	public static boolean waterPatch(Player player, Patches data, Location position, int item_id) {

		PatchState state = player.getFarming().getPatchStates().get(data.name());

		if (state == null) {
			return false;
		}

		if (!FarmingConstants.isWateringCan(item_id)) {
			return false;
		}

		if (!data.getPatchType().isWaterable()) {
			player.getTransmitter().sendMessage("You can't water this patch.");
			return true;
		}

		if (!state.isUsed()) {
			player.getTransmitter().sendMessage("You must plant a seed before watering the patch.");
			return false;
		}

		if (state.isWatered()) {
			player.getTransmitter().sendMessage("This patch is already watered.");
			return true;
		}

		if (FarmingConstants.isFullyGrown(state)) {
			player.getTransmitter().sendMessage("This patch does not need watering.");
			return true;
		}

		player.faceLocation(position);
		player.sendAnimation(new Animation(FarmingConstants.WATERING_CAN_ANIM));
		TaskManager.submit(new WaterPatchAction(player, state, item_id));

		return true;
	}
}
