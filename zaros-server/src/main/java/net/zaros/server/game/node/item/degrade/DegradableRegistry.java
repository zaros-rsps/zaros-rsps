package net.zaros.server.game.node.item.degrade;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.PlayerEquipment;
import net.zaros.server.utility.tool.Misc;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class DegradableRegistry {

    public static final List<DegradableEquipment> DEGRADABLES = new ArrayList<>();

    public static void registerAll() {
        Misc.getClassesInDirectory(DegradableRegistry.class.getPackage().getName() + ".equipment").stream().filter(DegradableEquipment.class::isInstance).forEach(clazz -> {
            DegradableEquipment degradable = (DegradableEquipment) clazz;
            DEGRADABLES.add(degradable);
        });
		log.info("Registered " + DEGRADABLES.size() + " degradable handlers.");
    }

    public static DegradableEquipment getDegradable(Player player) {
        PlayerEquipment equipment = player.getEquipment();
        for (DegradableEquipment degradableEquipment : DEGRADABLES) {
            for (int item : degradableEquipment.getItems()) {
                if (equipment.getItems().contains(item)) {
                    return degradableEquipment;
                }
            }
        }
        return null;
    }

}
