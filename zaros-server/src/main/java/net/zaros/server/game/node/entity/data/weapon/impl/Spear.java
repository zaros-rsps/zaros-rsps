package net.zaros.server.game.node.entity.data.weapon.impl;

import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;

import java.util.List;

/**
 * The interface for spears.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum Spear implements WeaponInterface {

    /**
     * The regular spear weapon interface.
     */
    REGULAR(),

    /**
     * The regular(P) spear weapon interface.
     */
    REGULAR_P(40),

    /**
     * The regular(P+) spear weapon interface.
     */
    REGULAR_PP(50),

    /**
     * The regular(P++) spear weapon interface.
     */
    REGULAR_PPP(60),

    /**
     * The regular(KP) spear weapon interface.
     */
    REGULAR_KP(150),

    /**
     * The dragon spear spear weapon interface.
     */
    DRAGON() {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },

    /**
     * The dragon spear(P) spear weapon interface.
     */
    DRAGON_P(40) {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },

    /**
     * The dragon spear(P+) spear weapon interface.
     */
    DRAGON_PP(50) {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },

    /**
     * The dragon spear(P++) spear weapon interface.
     */
    DRAGON_PPP(60) {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },

    /**
     * The dragon spear(KP) spear weapon interface.
     */
    DRAGON_KP(150) {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },

    /**
     * The zamorakian spear weapon interface.
     */
    ZAMORAKIAN(List.of(AttackType.ZAMORAKIAN_SPEAR_LUNGE, AttackType.ZAMORAKIAN_SPEAR_SWIPE, AttackType.ZAMORAKIAN_SPEAR_POUND, AttackType.ZAMORAKIAN_SPEAR_BLOCK)) {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },
    /**
     * The vesta spear weapon interface.
     */
    VESTA() {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    };

    /**
     * The spear's poison damage.
     */
    private int poisonDamage = 0;

    /**
     * The attack types.
     */
    private List<AttackType> attackTypes = List.of(AttackType.SPEAR_LUNGE, AttackType.SPEAR_SWIPE, AttackType.SPEAR_POUND, AttackType.SPEAR_BLOCK);

    /**
     * Constructor for the spear with no poison.
     */
    Spear() {
    }

    /**
     * Constructor for the spear with poison damage.
     *
     * @param poisonDamage
     */
    Spear(int poisonDamage) {
        this.poisonDamage = poisonDamage;
    }

    /**
     * Constructor for the spear with different animations.
     *
     * @param attackTypes
     */
    Spear(List<AttackType> attackTypes) {
        this.attackTypes = attackTypes;
    }

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }

    @Override
    public int poisonDamage() {
        return poisonDamage;
    }
}
