package net.zaros.server.game.module.interaction.item;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.game.content.action.interaction.PlayerPotionAction;
import net.zaros.server.game.module.type.ItemInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.rs.InteractionOption;
import net.zaros.server.utility.rs.constant.PotionConstants.Potion;

import java.util.Optional;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/29/2017
 */
@Slf4j
public class PotionDrinkInteractionModule extends ItemInteractionModule {

	public PotionDrinkInteractionModule() {
		register(Potion.getAllPotionIds());
	}
	
	@Override
	public boolean handle(Player player, Item item, int slotId, InteractionOption option) {
		if (option != InteractionOption.FIRST_OPTION) {
			return true;
		}
		Optional<Potion> optional = Potion.getPotion(item.getId());
		if (!optional.isPresent()) {
			log.warn("Unable to find potion... [" + item + "][" + option + "]");
			return true;
		}
		player.getManager().getActions().startAction(new PlayerPotionAction(item, slotId, optional.get()));
		return true;
	}
}
