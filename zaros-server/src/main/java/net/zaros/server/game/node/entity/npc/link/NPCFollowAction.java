package net.zaros.server.game.node.entity.npc.link;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.content.action.Action;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.npc.extension.familiar.SummoningFamiliar;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.game.world.region.route.RouteFinder;
import net.zaros.server.game.world.region.route.strategy.EntityStrategy;
import net.zaros.server.utility.tool.Misc;

public class NPCFollowAction implements Action {

    public NPCFollowAction(NPC npc) {
        this.npc = npc;
    }

    @Override
    public boolean process(Entity entity) {
        if(getNpc() instanceof SummoningFamiliar)
            if(!getNpc().getLocation().withinDistance(getEntity().getLocation(), 16) && getEntity().isPlayer())
                getEntity().toPlayer().getManager().getSummoning().call();
        else if (!canFollow()) return false;
        if(!continuation()) return false;
        return true;
    }

    public boolean canFollow() {
        return !(getEntity() == null) || !getEntity().isRenderable() || (getEntity().isPlayer() && !World.getPlayers().contains(getEntity().toPlayer())) || !getNpc().getEffectManager().isWalkEnabled(false);
    }

    public boolean continuation() {
        int size = getNpc().getSize();
        int targetSize = getEntity().getSize();
        int distanceX = getEntity().getLocation().getX() - npc.getLocation().getX();
        int distanceY = getEntity().getLocation().getY() - npc.getLocation().getY();
        if (distanceX < size && distanceX > -targetSize && distanceY < size && distanceY > -targetSize && !getEntity().getMovement().hasWalkSteps()) {
            npc.getMovement().resetWalkSteps();
            if (!npc.getMovement().addWalkSteps(getEntity().getLocation().getX() + 1, npc.getLocation().getY())) {
                npc.getMovement().resetWalkSteps();
                if (!npc.getMovement().addWalkSteps(getEntity().getLocation().getX() - size, npc.getLocation().getY())) {
                    npc.getMovement().resetWalkSteps();
                    if (!npc.getMovement().addWalkSteps(npc.getLocation().getX(), getEntity().getLocation().getY() + 1)) {
                        npc.getMovement().resetWalkSteps();
                        if (!npc.getMovement().addWalkSteps(npc.getLocation().getX(), getEntity().getLocation().getY() - size)) {
                            npc.getMovement().resetWalkSteps();
                            npc.getMovement().addWalkSteps(npc.getLocation().getX(), getEntity().getLocation().getY() - size);
                        }
                    }
                }
            }
            return false;
        }
        if (targetSize == 1 && size == 1 && Math.abs(npc.getLocation().getX() - getEntity().getLocation().getX()) == 1 && Math.abs(npc.getLocation().getY() - getEntity().getLocation().getY()) == 1 && !getEntity().getMovement().hasWalkSteps()) {
            if (!npc.getMovement().addWalkSteps(getEntity().getLocation().getX(), npc.getLocation().getY())) {
                npc.getMovement().addWalkSteps(npc.getLocation().getX(), getEntity().getLocation().getY());
            }
            return true;
        }

        npc.getMovement().resetWalkSteps();
        boolean clippedProjectileToNode = npc.getMovement().clippedProjectileToNode(getEntity(), false);
        boolean onRange = !Misc.isOnRange(npc.getLocation().getX(), npc.getLocation().getY(), size, getEntity().getLocation().getX(), getEntity().getLocation().getY(), targetSize, 0);
        // check if we need to walk to the target
        boolean distanced = !Misc.isOnRange(npc, getEntity(), 0 + (npc.getMovement().hasWalkSteps() && getEntity().getMovement().hasWalkSteps() ? (getEntity().getMovement().isRunning() ? 2 : 1) : 0));
        // if the unfollowable state is set
        boolean unfollowable = !npc.getCombatDefinitions().isCombatFollowDisabled() && Misc.colides(npc, getEntity());
        if ((!clippedProjectileToNode) || distanced) {
            traverseCalculatePath();
            return true;
        }
        return true;
    }

    /**
     * Uses entity route strategizing to path to the partner
     */
    private void traverseCalculatePath() {
        int steps = RouteFinder.findRoute(RouteFinder.WALK_ROUTEFINDER, getNpc().getLocation().getX(), getNpc().getLocation().getY(), getNpc().getLocation().getPlane(), getNpc().getSize(), new EntityStrategy(getEntity()), false);
        if (steps == -1) {
            return;
        }
        int[] bufferX = RouteFinder.getLastPathBufferX();
        int[] bufferY = RouteFinder.getLastPathBufferY();

        getNpc().getMovement().resetWalkSteps();
        for (int step = steps - 1; step >= 0; step--) {
            if (!getNpc().getMovement().addWalkSteps(bufferX[step], bufferY[step], 25, true)) {
                break;
            }
        }
    }

    @Getter
    private final NPC npc;

    @Getter
    @Setter
    private Entity entity;

    @Getter
    @Setter
    private boolean following;

    @Override
    public boolean start(Entity entity) {
        if(getEntity() == null) return false;
        if(!canFollow()) return false;
        setFollowing(true);
        getNpc().faceEntity(getEntity());
        traverseCalculatePath();
        return true;
    }

    @Override
    public int processOnTicks(Entity entity) {
        return canFollow() ? 0 : -1;
    }

    @Override
    public void stop(Entity entity) {

    }
}
