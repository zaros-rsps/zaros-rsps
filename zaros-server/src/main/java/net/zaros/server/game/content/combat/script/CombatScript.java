package net.zaros.server.game.content.combat.script;

import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.hit.CombatHit;
import net.zaros.server.game.node.entity.hit.HitEffect;
import net.zaros.server.game.node.entity.hit.HitModifier;
import net.zaros.server.utility.rs.Hit;

/**
 * The combat script for characters.
 *
 * @author Gabriel || Wolfsdarker
 */
public interface CombatScript {

    /**
     * Returns if the attacker can start the script with the target.
     *
     * @param attacker
     * @param target
     * @return if he can start
     */
    boolean executable(Entity attacker, Entity target);

    /**
     * Returns the distance required to execute the script.
     *
     * @param attacker
     * @param target
     * @return the distance
     */
    int getAttackDistance(Entity attacker, Entity target);

    /**
     * Executes before the script itself to select attributes.
     *
     * @param attacker
     * @param target
     */
    void preExecution(Entity attacker, Entity target);

    /**
     * Executes the main script.
     *
     * @param attacker
     * @param target
     */
    void execute(Entity attacker, Entity target);

    /**
     * Returns the type of damage the script will do.
     *
     * @return the type
     */
    Hit.HitSplat getHitSplat(Entity attacker, Entity target);

    /**
     * Returns the hits created by the script.
     *
     * @param attacker
     * @param target
     * @return the hits
     */
    Hit[] getHits(Entity attacker, Entity target);

    /**
     * Returns the script's speed in ticks.
     *
     * @param attacker
     * @param target
     * @return the speed
     */
    int getSpeed(Entity attacker, Entity target);

    /**
     * Returns if the script is a special attack.
     *
     * @param attacker
     * @param target
     * @return if is special attack
     */
    boolean specialAttack(Entity attacker, Entity target);

    /**
     * Executes after the entire script is executed.
     */
    void postExecution(Entity attacker, Entity target);

    /**
     * Executes after the hit is done.
     *
     * @param hit
     */
    void postHitExecution(CombatHit hit);

    /**
     * Returns if the hit has to check accuracy.
     *
     * @param attacker
     * @param target
     * @return if has to check accuracy
     */
    default boolean ignoreAccuracy(Entity attacker, Entity target) {
        return false;
    }

    /**
     * Returns if the script should ignore prayer protection.
     *
     * @param attacker
     * @param target
     * @return if prayer will be ignored
     */
    default boolean ignorePrayerProtection(Entity attacker, Entity target) {
        return false;
    }

    /**
     * Returns the hit's effect.
     *
     * @param attacker
     * @param target
     * @return the effect
     */
    default HitEffect getEffect(Entity attacker, Entity target) {
        return null;
    }

    /**
     * Returns the hit's modifier.
     *
     * @param attacker
     * @param target
     * @return the modifier
     */
    default HitModifier getModifier(Entity attacker, Entity target) {
        return null;
    }

    /**
     * Returns the follow distance.
     *
     * @param attacker
     * @param target
     * @return the distance
     */
    default int getFollowDistance(Entity attacker, Entity target) {
        return getAttackDistance(attacker, target);
    }

}
