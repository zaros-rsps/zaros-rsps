package net.zaros.server.game.module.command.administrator;

import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.npc.extension.familiar.impl.TestFamiliar;
import net.zaros.server.game.node.entity.player.Player;

public class SummonCommand extends CommandModule {

    @Override
    public String[] identifiers() {
        return arguments("summon");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {
       player.getManager().getSummoning().create(player, new TestFamiliar(player.getLocation()));
    }
}

