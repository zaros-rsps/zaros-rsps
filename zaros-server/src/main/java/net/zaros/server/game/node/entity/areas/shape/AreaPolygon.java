package net.zaros.server.game.node.entity.areas.shape;

import com.google.common.base.Preconditions;

import net.zaros.server.game.node.Location;

/**
 * Represents a polygon shaped area.
 * 
 * @author Walied K. Yassen
 */
public final class AreaPolygon implements AreaShape {

	/**
	 * The surrounded AABB (Axis-Aligned-Bounding-Box) object. This object is only
	 * used to boost fail-fast performance of the algorithm.
	 */
	private final AreaRectangle aabb;

	/**
	 * The polygon points x component.
	 */
	private final int[] points_x;

	/**
	 * the polygon points y component.
	 */
	private final int[] points_y;

	/**
	 * Constructs a new {@link AreaPolygon} type object instance.
	 * 
	 * @param points
	 *               the polygon location points.
	 */
	public AreaPolygon(Location[] points) {
		Preconditions.checkArgument(points.length > 2, "you must provide at-least one point for the polygon.");
		var points_x = new int[points.length];
		var points_y = new int[points.length];
		for (var index = 0; index < points.length; index++) {
			var point = points[index];
			points_x[index] = point.getX();
			points_y[index] = point.getY();
		}
		this.points_x = points_x;
		this.points_y = points_y;
		aabb = createAabb(points_x, points_y);
	}

	/**
	 * Constructs a new {@link AreaPolygon} type object instance.
	 * 
	 * @param points_x
	 *                 the polygon points x component.
	 * @param points_y
	 *                 the polygon points y component.
	 */
	public AreaPolygon(int[] points_x, int[] points_y) {
		Preconditions.checkArgument(points_x.length == points_y.length, "you must provide components that are equal in size.");
		Preconditions.checkArgument(points_x.length > 2, "you must provide at-least three values for each component.");
		this.points_x = points_x;
		this.points_y = points_y;
		aabb = createAabb(points_x, points_y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.node.entity.areas.shape.AreaShape#intersects(int,
	 * int)
	 */
	@Override
	public boolean intersects(int x, int y) {
		if (!aabb.intersects(x, y)) {
			return false;
		}
		final var count = points_x.length;
		var inside = false;
		for (int i = 0, j = count - 1; i < count; j = i++) {
			if (points_y[i] > y != points_y[j] > y && x < (points_x[j] - points_x[i]) * (y - points_y[i]) / (points_y[j] - points_y[i]) + points_x[i]) {
				inside = !inside;
			}
		}
		return inside;
	}

	/**
	 * Creates an {@link AreaRectangle) object that represents the specified polygon
	 * points AABB (Axis-Aligned-Bounding-Box).
	 * 
	 * @param points_x
	 *                 the polygon points x component.
	 * @param points_y
	 *                 the polygon points y component.
	 * @return the created {@link AreaRectangle} object.
	 */
	private static AreaRectangle createAabb(int[] points_x, int[] points_y) {
		var min_x = 32767;
		var min_y = 32767;
		var max_x = -32768;
		var max_y = -32768;
		final var count = points_x.length;
		for (var index = 0; index < count; index++) {
			var x = points_x[index];
			var y = points_y[index];
			if (x < min_x) {
				min_x = x;
			}
			if (x > max_x) {
				max_x = x;
			}
			if (y < min_y) {
				min_y = y;
			}
			if (y > max_y) {
				max_y = y;
			}
		}
		return new AreaRectangle(min_x, min_y, max_x, max_y);
	}
}
