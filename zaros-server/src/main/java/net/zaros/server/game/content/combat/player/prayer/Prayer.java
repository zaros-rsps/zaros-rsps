package net.zaros.server.game.content.combat.player.prayer;

import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.Skills;

import net.zaros.server.game.content.combat.player.prayer.PrayerDefinition.*;
import net.zaros.server.network.world.packet.outgoing.impl.AccessMaskBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.ConfigPacketBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacob on 5/21/2017.
 */
public class Prayer {

    public Prayer(Entity entity) {
        this.entity = entity;
    }

    public boolean sendPrayerBook(PrayerBook book) {
        if (entity.isPlayer()) {
            entity.toPlayer().getTransmitter().send(new ConfigPacketBuilder(1584, book == PrayerBook.CURSES ? 1 : 0).build(entity.toPlayer()));
            entity.toPlayer().getTransmitter().send(new AccessMaskBuilder(271, 8, 0, 2, 0, 30).build(entity.toPlayer()));
            return true;
        }
        return false;
    }

    /**
     * Returns if the requirements are met for the specified {@link PrayerNode}.
     * @param prayerNode   The {@link PrayerNode}.
     * @return If the requirements have been met.
     */
    public boolean requirements(PrayerNode prayerNode) {
        if(getEntity().isPlayer()) {
            if (getEntity().toPlayer().getVariables().getPrayerPoints() > 0) {
                if (getEntity().toPlayer().getSkills().getLevel(Skills.PRAYER) >= prayerNode.getRequiredLevel()) {
                    return true;
                } else return sendPrayerActivationFailure(true, false, false, prayerNode);
            } else return sendPrayerActivationFailure(false, true, false, prayerNode);
        } else return true;
    }

    /**
     * Toggles the prayer on or off.
     * @param prayerNode The {@link PrayerNode}.
     * @return Whether it was toggled on or off.
     */
    public boolean toggle(PrayerNode prayerNode) {
        if(getActivatedPrayers().contains(prayerNode)) {
            deactivate(prayerNode);
            sendPrayerConfig(prayerNode);
            removeIcon();
            return true;
        }
        if(requirements(prayerNode) && activate(prayerNode)) {
            handlePrayerConflicts(prayerNode);
            sendPrayerConfig(prayerNode);
            displayIcon(prayerNode);
            //getEntity().asPlayer().getAudioManager().send(prayer_node.getSound());
            //getDrainTask().init(getEntity().asPlayer());
            return true;
        }
        return false;
    }

    /**
     * Deactivates and/or doesn't allow activation of a {@link PrayerNode}.
     * @param level_requirement Whether the failure is in correspondence of level met requirement.
     * @param prayer_points     Whether the failure is in correspondence of invalid prayer point amount.
     * @param quest             Whether the failure is in correspondence of a quest not being completed.
     * @param prayer_node       The {@link PrayerNode} we are referencing.
     */
    public boolean sendPrayerActivationFailure(boolean level_requirement, boolean prayer_points, boolean quest, PrayerNode prayer_node) {
        if(getEntity().isPlayer()) {
            //getEntity().toPlayer().getAudioManager().send(2673);
            //if (level_requirement)
               // getEntity().toPlayer().getDialogueInterpreter().sendPlaneMessage(false, "You need a <col=08088A>Prayer level of " + prayer_node.getRequiredLevel() + " to use " + StringUtils.formatDisplayName(prayer_node.name().toLowerCase().replace("_", " ")) + ".");
            if (prayer_points) getEntity().toPlayer().getTransmitter().sendMessage("Please recharge your prayer points at any altar.");
        }
        return false;
    }

    /**
     * Activates a {@link PrayerNode}.
     * @param prayer_node The prayer attempting to be activated.
     */
    public boolean activate(PrayerNode prayer_node) {
        if(!getActivatedPrayers().contains(prayer_node)) {
            return getActivatedPrayers().add(prayer_node);
        }
        return false;
    }

    /**
     * Deactivates a {@link PrayerNode}.
     * @param prayer_node The prayer attempting to be deactivated.
     */
    public boolean deactivate(PrayerNode prayer_node) {
        if(!getActivatedPrayers().contains(prayer_node)) return false;
        getActivatedPrayers().remove(prayer_node);
        if(getActivatedPrayers().isEmpty() && getEntity().isPlayer()) {
            reset();
        }
        return true;
    }

    /**
     * Checks if there is a conflict with the active {@link PrayerNode} and the currently selected {@link PrayerNode}.
     * @param prayerNode   The {@link PrayerNode} being used.
     */
    public void handlePrayerConflicts(PrayerNode prayerNode) {
        int disableHash = prayerNode.getPrayerType().getPrayerFlag();
        getActivatedPrayers().removeIf(activated -> (disableHash & activated.getFlag()) != 0 && activated != prayerNode);
    }

    /**
     * Sends the config to light up the prayer icon in the prayer book.
     */
    public void sendPrayerConfig(PrayerNode prayerNode) {
        if(!getEntity().isPlayer()) return;
        Player player = getEntity().toPlayer();
        int configValue = 0;
        var book = prayerNode.getBook();
        boolean quickPrayer;
        int configID = book == PrayerBook.NORMAL ? 1395 : 1582;//TODO: Quickprayer and curses.
        for (PrayerNode node : getActivatedPrayers()) {
            configValue += (1 << node.ordinal());
        }
        //player.getConfigManager().set(configID, configValue);
    }

    public boolean hasDisplayIcon(PrayerNode prayer_node) {
        return prayer_node.hasPrayerIcon();
    }

    /**
     * Displays the {@link PrayerIcon} based on the {@link PrayerNode}.
     * @param prayer_node The {@link PrayerNode} being referenced.
     * @return If the icon is displayed or not.
     */
    public boolean displayIcon(PrayerNode prayer_node) {
        if(!hasDisplayIcon(prayer_node)) return false;
        int icon = getPrayerIcon(prayer_node);
        if(getEntity().isPlayer()) {
            //getEntity().toPlayer().getAppearance().setHeadIcon(icon);
            //getEntity().toPlayer().getAppearance().sync();
        }// else getEntity().toNPC().getDefinition().setHeadIcon(icon);
        return true;
    }

    public int getPrayerIcon(PrayerNode prayerNode) {
        if(!hasDisplayIcon(prayerNode)) return -1;
        if(prayerNode == PrayerNode.PROTECT_FROM_SUMMONING) {
            for (PrayerNode active : getActivatedPrayers()) {
                if (active.getPrayerType() == PrayerType.OVERHEAD_AND_PROTECTION_PRAYERS) {
                    return active.getHeadIcon().getIcon() + 8;
                }
            }
        }
        if(prayerNode.getPrayerType() == PrayerType.OVERHEAD_AND_PROTECTION_PRAYERS && getActivatedPrayers().contains(PrayerNode.PROTECT_FROM_SUMMONING)) {
            return prayerNode.getHeadIcon().getIcon() + 8;
        }
        return prayerNode.getHeadIcon().getIcon();
    }

    public void findNextIcon() {
        getActivatedPrayers().stream().filter(PrayerNode::hasPrayerIcon).forEach(this::displayIcon);
    }

    public boolean removeIcon() {
        if(getEntity().isPlayer()) {
            //getEntity().asPlayer().getAppearance().setHeadIcon(-1);
            //getEntity().asPlayer().getAppearance().sync();
            findNextIcon();
        }// else getEntity().asNpc().getDefinition().setHeadIcon(-1);
        return true;
    }

    /**
     * Gets the bonuses to be applied by the {@link PrayerNode}.
     * @param skill The corresponding skill for the {@link PrayerBonus}.
     * @return The {@link PrayerBonus}.
     */
    public double getSkillBonus(int skill) {
        double bonus = 0.0;
        for (PrayerNode node : getActivatedPrayers()) {
            for (PrayerBonus skill_bonus : node.getSkillBonuses()) {
                if (skill_bonus.getStat() == skill) {
                    bonus += skill_bonus.getAdjustment();
                }
            }
        }
        return bonus;
    }

    /**
     * Method used to reset this prayer managers cached prayers.
     */
    public void reset() {
        if(!getActivatedPrayers().isEmpty())
            getActivatedPrayers().clear();
        if(getEntity().isPlayer()) {
            //getEntity().asPlayer().getConfigManager().set(1395, 0);
            //getEntity().asPlayer().getAppearance().setHeadIcon(-1);
            //getEntity().asPlayer().getAppearance().sync();
            //getDrainTask().stop(getEntity());
        } else {
            //getEntity().asNpc().getDefinition().setHeadIcon(-1);
        }
    }

    private final Entity entity;

    private final PrayerTasks tasks = new PrayerTasks(this);

    private List<PrayerNode> activated_prayers = new ArrayList<>();

    private boolean ancientCurses;

    public Entity getEntity() { return entity; }

    public PrayerTasks getTasks() { return tasks; }

    //private final DrainTask drainTask = new DrainTask();

    public List<PrayerNode> getActivatedPrayers() { return activated_prayers; }

    public boolean hasActivatedPrayer(PrayerNode prayer_node) { return getActivatedPrayers().contains(prayer_node); }

    /**
     * Gets the task.
     *
     * @return The task.
     */
   // public DrainTask getDrainTask() {
    //    return drainTask;
   // }

    public boolean isUsingCurses() {
        return ancientCurses;
    }

    public void setUsingCurses(boolean curses) {
        this.ancientCurses = curses;
    }

    public boolean usingQuickPrayers() {
        return false;
    }

}
