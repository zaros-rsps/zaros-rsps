package net.zaros.server.game.content.combat.player.registry.spell.lunar.teleport;

import net.zaros.server.game.content.combat.player.registry.wrapper.magic.TeleportationSpellEvent;
import net.zaros.server.game.node.Location;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/28/2017
 */
public class SouthFaladorTeleportSpell implements TeleportationSpellEvent {
	
	@Override
	public int levelRequired() {
		return 72;
	}
	
	@Override
	public int[] runesRequired() {
		return arguments(ASTRAL_RUNE, 2, LAW_RUNE, 1, AIR_RUNE, 2);
	}
	
	@Override
	public Location destination() {
		return new Location(3005, 3327, 0);
	}
	
	@Override
	public int spellId() {
		return 67;
	}
	
	@Override
	public double exp() {
		return 70;
	}
	
	@Override
	public MagicBook book() {
		return MagicBook.LUNARS;
	}
}
