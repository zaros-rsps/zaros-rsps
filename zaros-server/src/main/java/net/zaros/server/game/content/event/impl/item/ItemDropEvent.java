package net.zaros.server.game.content.event.impl.item;

import java.util.Objects;

import net.zaros.server.game.content.event.EventPolicy.ActionPolicy;
import net.zaros.server.game.content.event.context.item.ItemEventContext;
import net.zaros.server.game.module.interaction.rsinterface.DestroyItemInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.world.World;
import net.zaros.server.game.world.region.RegionManager;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.newrepository.items.ItemRepository;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/27/2017
 */
public class ItemDropEvent extends ItemEvent {

	/**
	 * Constructs a new event
	 */
	public ItemDropEvent() {
		super();
		setActionPolicy(ActionPolicy.RESET);
	}

	@Override
	public void run(Player player, ItemEventContext context) {
		final Item item = context.getItem();
		// if for some reason the items in the slot dont match
		if (!Objects.equals(player.getInventory().getItems().get(context.getSlotId()), item)) {
			return;
		}
		// if its a destroyable item
		boolean destroy = item.getDefinitions().isCert() || item.getDefinitions().hasOption("destroy") || !ItemRepository.lookup(item.getId()).isTradeable();
		// if the item should be destroyed we show the interface
		if (destroy) {
			// sends the type of management for the destroy interface
			// it will be used for different things
			player.putTemporaryAttribute(AttributeKey.DESTROY_INTERFACE_TYPE, "delete");
			// sends the slot that the item was in
			player.putTemporaryAttribute("destroy_slot_id", context.getSlotId());
			// shows the interface
			var earnLine = ItemRepository.lookup(item.getId()).getDestroy();
			if (earnLine.isEmpty()) {
				earnLine = "If you destroy this item, you will have to earn it again.";
			}
			DestroyItemInteractionModule.show(player, item, "Are you sure you want to destroy this item?", earnLine);
			return;
		}
		// otherwise we just delete the item
		player.getInventory().deleteItem(context.getSlotId(), item);
		addFloorItem(player, item);
	}

	/**
	 * Handles the addition of the item to the floor
	 *
	 * @param player
	 *               The player dropping the item
	 * @param item
	 *               The item being dropped
	 */
	private void addFloorItem(Player player, Item item) {
		boolean shouldPublicize = false;
		boolean isEdible = false;
		if (item.getDefinitions().isEdible()) {
			isEdible = true;
		}
		// if we are in a pvp area we will auto publicize all items that aren't edible
		if (World.isPvpArea(player.getLocation())) {
			shouldPublicize = true;
		}
		// edible items should not be automatically public
		if (isEdible && shouldPublicize) {
			shouldPublicize = false;
		}
		if (shouldPublicize) {
			RegionManager.addPublicFloorItem(item.getId(), item.getAmount(), 200, player.getLocation());
		} else {
			RegionManager.addFloorItem(item.getId(), item.getAmount(), 200, player.getLocation(), player.getDetails().getUsername());
		}
	}

}
