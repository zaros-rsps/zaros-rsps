package net.zaros.server.game.content.market.grandexchange;

import lombok.Getter;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.outgoing.impl.AccessMaskBuilder;
import net.zaros.server.network.world.packet.outgoing.impl.CS2ScriptBuilder;

public class GrandExchange {

    public GrandExchange(Player player) {
        this.player = player;
    }

    public boolean open() {
        player.getManager().getInterfaces().sendInterface(105, true);
        player.getTransmitter().send(new AccessMaskBuilder(true,105, 206, -1, 0, 0, 1).build(player));
        player.getTransmitter().send(new AccessMaskBuilder(true,105, 208, -1, 0, 0, 1).build(player));

        player.getTransmitter().send(new CS2ScriptBuilder(571).build(player));

        player.getManager().getInterfaces().closeInterface(752, 7);
        return true;
    }

    @Getter
    private final Player player;

}
