package net.zaros.server.game.content.system.zskillsystem.farming.actions;


import net.zaros.server.game.content.system.zskillsystem.farming.impl.DiseaseState;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.FarmingConstants;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.PatchState;
import net.zaros.server.game.content.system.zskillsystem.farming.impl.Patches;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.render.flag.impl.Animation;
import net.zaros.server.game.node.item.Item;

/**
 * Handles the action to cure the crop.
 *
 * @author Gabriel || Wolfsdarker
 */
public class CureCropAction extends PlayerTask {

	/**
	 * Constructor for the action.
	 *
	 * @param player
	 * @param state
	 */
	private CureCropAction(Player player, PatchState state) {
		super(3, player, () -> {

			if (player.getInventory().containsItem(FarmingConstants.ITEM_PLANT_CURE)) {

				state.setDiseaseState(DiseaseState.NOT_PRESENT);

				player.getInventory().remove(FarmingConstants.ITEM_PLANT_CURE);
				player.getInventory().addItem(new Item(Items.EMPTY_VIAL));

				player.getTransmitter().sendMessage("You apply the cure to the patch.");
				player.getFarming().updatePatches(player);
			}

			player.endCurrentTask();
		});
	}

	/**
	 * Handles the action to cure the crop.
	 *
	 * @param player
	 * @param data
	 * @param pos
	 * @param item_id
	 * @return
	 */
	public static boolean curePlant(Player player, Patches data, Location pos, int item_id) {

		PatchState state = player.getFarming().getPatchStates().get(data.name());

		if (state == null) {
			return false;
		}

		if (item_id != FarmingConstants.ITEM_PLANT_CURE.getId()) {
			return false;
		}

		if (state.getDiseaseState() != DiseaseState.PRESENT) {
			player.getTransmitter().sendMessage("This crop is not diseased.");
			return true;
		}

		if (!player.getInventory().containsItem(FarmingConstants.ITEM_PLANT_CURE)) {
			player.getTransmitter().sendMessage("You need plant cure to cure this patch.");
			return true;
		}

		player.faceLocation(pos);
		player.sendAnimation(new Animation(FarmingConstants.CURING_ANIM));
		TaskManager.submit(new CureCropAction(player, state));
		return true;
	}
}
