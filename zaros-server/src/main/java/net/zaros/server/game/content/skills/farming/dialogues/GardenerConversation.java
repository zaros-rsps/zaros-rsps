package net.zaros.server.game.content.system.zskillsystem.farming.dialogues;

import com.ahoypk.definitions.constant.Items;
import com.ahoypk.engine.task.Task;
import com.ahoypk.engine.task.TaskManager;
import com.ahoypk.world.content.conversation.Conversation;
import com.ahoypk.world.content.skills.impl.farming.impl.*;
import com.ahoypk.world.entity.impl.npc.NPC;
import com.ahoypk.world.entity.impl.player.Player;
import com.ahoypk.world.model.Item;
import com.ahoypk.world.model.container.shop.Shop;

import java.util.List;

import static com.ahoypk.util.Random.random;

/**
 * Handles the conversation between player and gardeners.
 *
 * @author Gabriel || Wolfsdarker
 */
public class GardenerConversation extends Conversation {

	/**
	 * Begin of the conversation.
	 */
	private static final int OPTIONS_STAGE = 0;

	/**
	 * Sends the cost of protection directly.
	 */
	private static final int SEND_COST_STAGE = 1;

	/**
	 * The gardener's NPC instance.
	 */
	private NPC gardener;

	/**
	 * The patch that will be protected.
	 */
	private Patches patch;

	/**
	 * The conversation stage.
	 */
	private int conversationStage;


	/**
	 * Constructor to start the conversation from the options.
	 *
	 * @param player
	 * @param gardener
	 */
	public GardenerConversation(Player player, NPC gardener) {
		super(player);
		this.gardener = gardener;
		this.conversationStage = OPTIONS_STAGE;
	}

	/**
	 * Constructor to start the conversation by the cost of protection.
	 *
	 * @param player
	 * @param gardener
	 * @param patch
	 */
	public GardenerConversation(Player player, NPC gardener, Patches patch) {
		super(player);
		this.gardener = gardener;
		this.conversationStage = SEND_COST_STAGE;
		this.patch = patch;
	}

	@Override
	public void init() {
		switch (conversationStage) {
			case OPTIONS_STAGE:
				sendOptions();
				break;

			case SEND_COST_STAGE:
				sendCropProtectionCost();
				break;
		}
	}

	/**
	 * Sends the options to start the dialogue.
	 */
	private void sendOptions() {
		options("Would you look after my crops for me?", this::lookAfterCrop,
			"Can you give me any farming advice?", this::farmingAdvice,
			"Can you sell me something?", this::sellSomething, "I'll come back another time.", this::finish);
	}

	/**
	 * The player asks if the gardener can protect his crops.
	 */
	private void lookAfterCrop() {
		player("Would you look after my crops for me?");
		npc(gardener, "I might - 	which one were you thinking of?");

		Gardener f = Gardener.get(gardener.getID());
		String[] options = f.getDialogueOptions();

		options(options[0], () -> {
			this.patch = f.getPatches().get(0);
			sendCropProtectionCost();
		}, options[1], () -> {
			this.patch = f.getPatches().get(1);
			sendCropProtectionCost();
		});
	}

	/**
	 * Sends the protection cost to the player.
	 */
	private void sendCropProtectionCost() {
		PatchState state = player.getFarming().getPatchStates().get(patch.name());

		if (state == null || !state.isUsed()) {
			npc(gardener, "You don't have any seeds planted in that patch. Plant", "some and I might agree to look after it for you.");
			return;
		}

		if (state.getProtection() == PatchProtection.PROTECTED) {
			npc(gardener, "You already paid the protection for this patch.");
			return;
		}

		if (FarmingConstants.isFullyGrown(state)) {
			npc(gardener, "This patch is already fully grown.");
			return;
		}

		if (state.isDead() || state.getDiseaseState() == DiseaseState.PRESENT) {
			npc(gardener, "This patch is already infected with something, nothing I can do.");
			return;
		}

		List<Item> cost = state.getSeed().getProtectionCost();

		if (cost == null || cost.isEmpty()) {
			npc(gardener, "It seems I can't protect this seed.");
			return;
		}

		var firstItem = cost.get(0);
		var nameOfCost = firstItem.getID() == Items.COMPOST ? "buckets of compost" : firstItem.getName().toLowerCase();

		if (firstItem.getAmount() > 1) {
			nameOfCost = firstItem.getAmount() + " " + nameOfCost;
			nameOfCost = nameOfCost + ((firstItem.getID() == Items.POTATO || firstItem.getID() == Items.TOMATO) ? "es" : "s");
		}

		npc(gardener, "Ye if you like, but I want " + nameOfCost + " for that.");
		options("Yes", this::acceptCropProtectionCost, "No", this::finish);
	}

	/**
	 * When the player accepts the proteciton cost.
	 */
	private void acceptCropProtectionCost() {
		PatchState state = player.getFarming().getPatchStates().get(patch.name());

		if (state == null || !state.isUsed()) {
			npc(gardener, "You don't have any seeds planted in that patch. Plant", "some and I might agree to look after it for you.");
			return;
		}

		var costList = state.getSeed().getProtectionCost();
		if (costList == null || costList.isEmpty()) {
			npc(gardener, "It seems I can't protect this seed.");
			return;
		}

		boolean hasItem = false;

		for (Item cost : costList) {
			if (player.getInventory().contains(cost)) {
				player.getInventory().delete(cost);
				hasItem = true;
				break;
			} else if (player.getInventory().contains(new Item(cost.getDefinition().getNoteID(), cost.getAmount()))) {
				player.getInventory().delete(new Item(cost.getDefinition().getNoteID(), cost.getAmount()));
				hasItem = true;
				break;
			}
		}

		if (!hasItem) {
			npc(gardener, "It seems you can't afford, talk to me once you can.");
			return;
		}

		state.setProtection(PatchProtection.PROTECTED);
		state.setWatered(true);
		player.getFarming().updatePatches(player);
		npc(gardener, "That'll do nicely, sir. Leave it with me - I'll make sure", "the patch grows for you.");
	}

	/**
	 * Handles the farming advice option.
	 */
	private void farmingAdvice() {
		player("Can you give me any farming advice?");

		switch (random(11)) {
			case 1:
				npc(gardener, "You can put up to five tomatoes, strawberries, apples,", "bananas or oranges into a fruit basket, although you",
					"can't have a mix in the same basket.");
				break;
			case 2:
				npc(gardener, "There is a special patch for growing Belladonna - I", "believe that it is somewhere near Draynor Manor,",
					"where the ground is a tad 'unblessed'.");
				break;
			case 3:
				npc(gardener, "You don't have to buy all your plantpots you know,", "you can make them yourself on a pottery wheel. If",
					"you're a good enough craftsman, that is.");
				break;
			case 4:
				npc(gardener, "You can put up to ten potatoes, cabbages or onions in", "vegetable sacks, although you can't have a mix in the",
					"same sack.");
				break;
			case 5:
				npc(gardener, "Bittercap mushrooms can only be grown in a special", "patch in Morytania, near the Mort Myre swamp. There",
					"the ground is especially dank and suited to growing", "poisonous fungii.");
				break;
			case 6:
				npc(gardener, "Hops are good for brewing ales. I believe there's a", "brewery up in Keldagrim somewhere, and I've heard",
					"rumours that a place called Phasmatys used to be good", "for that type of thing. 'Fore they all died, of course.");
				break;
			case 7:
				npc(gardener, "Applying compost to a patch will not only reduce the", "chance that your crops will get diseased, but you will",
					"also grow more crops to harvest");
				break;
			case 8:
				npc(gardener, "Tree seeds must be grown in a plantpot of soil into a", "sapling, and then transferred to a tree patch to",
					"continue growing to adulthood.");
				break;
			case 9:
				npc(gardener, "If you want to grow fruit trees you could try a few", "places: Catherby and Brimhaven have a couple of fruit",
					"tree patches, and I hear that the gnomes are big on", "that sort of thing.");
				break;
			default:
				npc(gardener, "Supercompost is far better than normal compost, but", "more expensive to make. You need to rot the right type",
					"of item;show me an item, and I'll tell you if it's super-", "compostable or not.");
				break;
		}

		sendOptions();
	}

	/**
	 * Handles the action to open the shop.
	 */
	private void sellSomething() {
		TaskManager.submit(new Task() {
			@Override
			protected void execute() {
				Shop.open(player, 8);
				this.stop();
			}
		});
	}

	/**
	 * Finishes the chat.
	 */
	private void finish() {
		player.getPacketSender().sendInterfaceRemoval();
	}

}
