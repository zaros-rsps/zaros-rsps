package net.zaros.server.game.node.entity.areas;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.areas.impl.ClanCampArea;
import net.zaros.server.game.node.entity.areas.impl.DefaultArea;

/**
 * @author Walied K. Yassen
 */
public final class AreaManager {

	/**
	 * The in-game areas.
	 */
	@Getter
	private static final Map<String, Area> areas = new HashMap<>();

	/**
	 * The default area.
	 */
	@Getter
	private static final Area defaultArea = new DefaultArea();

	/**
	 * Load all in-game areas.
	 */
	public static void init() {
		areas.put("Clan Camp Area", ClanCampArea.getSingleton());
	}

	/**
	 * Ticks the location checking for entities.
	 *
	 * @param entity
	 */
	public static void tick(Entity entity) {
		Area previous = entity.getArea();
		Area current = AreaManager.getArea(entity.getLocation());

		if (previous == null) {
			previous = AreaManager.defaultArea;
		}

		if (current != previous) {
			previous.leaveAreaHook(entity);
			current.enterAreaHook(entity);
			entity.setArea(current);
			if (entity.isPlayer()) {
				entity.toPlayer().sendMultiArea();
			}
		} else {
			previous.onTick(entity);
		}
	}

	/**
	 * Returns the area that is part of the the location to be searched.
	 *
	 * @param location
	 * @return the area
	 */
	public static Area getArea(Location location) {
		for (var area : AreaManager.areas.values()) {
			if (area.isWithin(location)) {
				return area;
			}
		}
		return AreaManager.defaultArea;
	}

	/**
	 * Returns the area that is part of the the region to be searched.
	 *
	 * @param regionId
	 * @return the area
	 */
	public static Area getArea(int regionId) {
		for (Area area : AreaManager.areas.values()) {
			if (area.getRegionIds().contains(regionId)) {
				return area;
			}
		}
		return AreaManager.defaultArea;
	}

}
