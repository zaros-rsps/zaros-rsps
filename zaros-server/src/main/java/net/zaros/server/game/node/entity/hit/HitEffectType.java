package net.zaros.server.game.node.entity.hit;

/**
 * The effects that a hit can apply.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum HitEffectType {

	/**
	 * The effect to freeze.
	 */
	FREEZE,

	/**
	 * The effect to poison.
	 */
	POISON,

	/**
	 * The effect to stun.
	 */
	STUN,

	/**
	 * The effect to heal based on the damage.
	 */
	HEAL_ON_DAMAGE,

	/**
	 * The effect to heal on a fixed amount.
	 */
	HEAL_FIXED_AMOUNT,

	/**
	 * The effect that will execute a runnable action.
	 */
	RUNNABLE_ACTION

}
