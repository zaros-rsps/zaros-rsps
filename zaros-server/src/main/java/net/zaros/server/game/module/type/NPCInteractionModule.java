package net.zaros.server.game.module.type;

import lombok.Getter;
import net.zaros.server.game.module.interaction.InteractionModule;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.InteractionOption;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/27/2017
 */
public abstract class NPCInteractionModule implements InteractionModule {

	/**
	 * The item ids that are subscribed to the module.
	 */
	@Getter
	List<Integer> npcIds = new ArrayList<>();

	/**
	 * The item names that are subscribed to the module.
	 */
	@Getter
	List<String> npcNames = new ArrayList<>();

	/**
	 * The item options that are subscribed to the module.
	 */
	@Getter
	List<String> npcOptions = new ArrayList<>();


	/**
	 * Registers the item id to this module.
	 * @param id The id of the item
	 */
	public void register(int... id) {
		Arrays.stream(id).forEach(itemId -> {
			if(!ArrayUtils.contains(getNpcIds().toArray(), itemId))
				getNpcIds().add(itemId);
		});
	}

	/**
	 * Registers the item name to this module.
	 * @param name The name of the item.
	 */
	public void register(String... name) {
		Arrays.stream(name).forEach(itemName -> {
			itemName.replace("_", " ").toLowerCase();
			if(!ArrayUtils.contains(getNpcNames().toArray(), itemName))
				getNpcNames().add(itemName);
		});
	}

	/**
	 * Registers an item option this module.
	 * @param option The option of the item
	 */
	public void registerOption(String... option) {
		Arrays.stream(option).forEach(itemOption -> {
			if(!ArrayUtils.contains(getNpcOptions().toArray(), itemOption))
				getNpcOptions().add(itemOption);
		});
	}

	/**
	 * Handles the interaction with an npc
	 *
	 * @param player
	 * 		The player interacting.
	 * @param npc
	 * 		The npc interacting with.
	 * @param option
	 * 		The option clicked on the npc.
	 * @return {@code True} if successfully interacted.
	 */
	public abstract boolean handle(Player player, NPC npc, InteractionOption option);
}
