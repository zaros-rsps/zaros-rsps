/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.game.content.minigames.types.solo;

import net.zaros.server.game.content.minigames.MinigameBase;
import net.zaros.server.game.content.minigames.MinigameProperties;

/**
 * Represents a solo-player minigame.
 * 
 * @author Walied K. Yassen
 */
public abstract class SoloMinigame extends MinigameBase {

	/**
	 * Construct a new {@link SoloMinigame} type object instance.
	 * 
	 * @param properties
	 *                   the minigame properties.
	 */
	public SoloMinigame(MinigameProperties properties) {
		super(properties);
	}
}
