package net.zaros.server.game.content.skills.fishing.plugin;

/**
 * @author Jacob Rhiel
 */
public interface IFishingTool {

    default int[] toolIds() {
        return new int[0];
    }

}
