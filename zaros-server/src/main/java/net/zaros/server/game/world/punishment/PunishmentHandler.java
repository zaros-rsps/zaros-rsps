package net.zaros.server.game.world.punishment;

import static net.zaros.server.game.world.punishment.PunishmentType.ADDRESS_BAN;
import static net.zaros.server.game.world.punishment.PunishmentType.ADDRESS_MUTE;
import static net.zaros.server.game.world.punishment.PunishmentType.BAN;
import static net.zaros.server.game.world.punishment.PunishmentType.MUTE;
import static net.zaros.server.utility.tool.ColorConstants.BLUE;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.master.client.packet.out.PunishmentAdditionRequestPacketOut;
import net.zaros.server.network.master.client.packet.out.PunishmentRemovalRequestPacketOut;
import net.zaros.server.utility.newrepository.database.players.PlayerRepository;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/17/2017
 */
public class PunishmentHandler {
	
	/**
	 * Adds a punishment
	 *
	 * @param player
	 * 		The player punishing
	 * @param name
	 * 		The punished users name
	 * @param type
	 * 		The type of punishment
	 * @param hours
	 * 		The duration of the punishment
	 */
	public static void addPunishment(Player player, String name, int hours, PunishmentType type) {
		// the time in horus for the punishment
		long time = hours == 0 ? Long.MAX_VALUE : System.currentTimeMillis() + TimeUnit.HOURS.toMillis(hours);
		// the puishment instance
		Punishment punishment = new Punishment(player.getDetails().getUsername(), name, type, time);
		// writing the punishment to the servers
		MasterCommunication.write(new PunishmentAdditionRequestPacketOut(punishment));
		// sending the message
		String message = String.format("[<col=%s>Attempting to %s %s for%s</col>.]", BLUE, type.name().toLowerCase(), name, hours == 0 ? "ever" : " " + Misc.format(hours) + " hours");
		player.getTransmitter().sendMessage(message);
		System.out.println(message);
	}
	
	/**
	 * Requests the removal of a punishment
	 *
	 * @param player
	 * 		The player removing the punishment
	 * @param name
	 * 		The punished users name
	 * @param type
	 * 		The type of punishment
	 */
	public static void removePunishment(Player player, String name, PunishmentType type) {
		/*
			1. send removal attempt to master server
			2. masster server sends attempt to all worlds
			3. if the worlds can find a player by that name they punish them
			4. on punish, we send back the success to the master server
			5. the success is then delivered back to the world it came from
		 */
		
		// writing the removal request
		MasterCommunication.write(new PunishmentRemovalRequestPacketOut(new Punishment(player.getDetails().getUsername(), name, type, 0)));
		
		// the message for a request
		String message = String.format("[<col=%s>Attempting to remove punishment '%s' from %s</col>.]", BLUE, type.name().toLowerCase(), name);
		player.getTransmitter().sendMessage(message);
		System.out.println(message);
	}
	
	/**
	 * Gets the message for the punishment
	 *
	 * @param punishment
	 * 		The punishment
	 * @param add
	 * 		If the punishment was added or removed
	 * @param success
	 * 		if the punishment was successful or failed
	 */
	public static String getMessage(Punishment punishment, boolean add, boolean success) {
		return "[" + (success ? "Successfully" : "Failed to") + " " + (add ? "add" + (!success ? "" : "ed") + "" : "remove" + (!success ? "" : "d")) + " punishment '" + punishment.getType().name() + "' to '" + punishment.getPunished() + "'.]";
	}
	
	/**
	 * Handles the addition of a punishment
	 *
	 * @param punishment
	 * 		The punishment to ad
	 * @return {@code True} If we could add a punishment, meaning there was a player in the world by that name.
	 */
	public static boolean addPunishment(Punishment punishment) {
		String punished = punishment.getPunished();
		Optional<Player> optional = World.getPlayerByUsername(punished);
		if (!optional.isPresent()) {
			return false;
		}
		Player player = optional.get();
		// make sure the player is renderable before we do this incase of saving issues
		if (!player.isRenderable() && !player.getSession().isInLobby()) {
			return false;
		}
		// as long as the punishment as added to the list successfully [ban/mute]
		return punishment.getType().add(player, punishment);
	}
	
	/**
	 * Handles the deletion of a punishment
	 *
	 * @param punishment
	 * 		The punishment to delete
	 */
	public static boolean deletePunishment(Punishment punishment) {
		PunishmentType type = punishment.getType();
		String punished = punishment.getPunished();
		if (type == MUTE) {
			Optional<Player> optional = World.getPlayerByUsername(punished);
			if (!optional.isPresent()) {
				return false;
			}
			Player player = optional.get();
			// make sure the player is renderable before we do this incase of saving issues
			if (!player.isRenderable() && !player.getSession().isInLobby()) {
				return false;
			}
			// as long as the punishment was removed
			return type.remove(player, punishment);
		} else if (type == BAN) {
			Optional<Player> optional = World.getPlayerByUsername(punished);
			// that player is online so its not possible they are banned
			if (optional.isPresent()) {
				return false;
			}
			// that player didn't exist
			if (!PlayerRepository.exists(punished)) {
				return false;
			}
			var player = PlayerRepository.load(punished);
			// unable to load file
			if (player == null) {
				return false;
			}
			// as long as the punishment was removed
			boolean remove = type.remove(player, punishment);
			return remove;
		} else if (type == ADDRESS_BAN || type == ADDRESS_MUTE) {
			// TODO: address punishments
		}
		return false;
	}
	
}
