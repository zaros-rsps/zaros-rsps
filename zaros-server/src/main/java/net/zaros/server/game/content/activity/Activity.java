package net.zaros.server.game.content.activity;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.TeleportType;
import net.zaros.server.game.node.Node;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.rs.ButtonOption;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/5/2017
 */
public class Activity {

	/**
	 * The parameters of the activity
	 */
	@Getter
	@Setter
	protected transient Object[] parameters;

	/**
	 * The player doing the activity
	 */
	@Getter
	@Setter
	protected transient Player player;

	public Activity(Object... parameters) {
		this.parameters = parameters;
	}

	/**
	 * Handles the start of the activity
	 */
	public void start() {

	}

	/**
	 * Handles the change of location
	 */
	public void updateLocation() {

	}

	/**
	 * Each game tick in which the activity is set for a player, this method is
	 * called
	 */
	public void tick() {

	}

	/**
	 * Handles the end of an activity. This method call cancels the player's current
	 * activity
	 */
	public void end() {
		player.getManager().getActivities().end();
	}

	public boolean handleButton(int interfaceId, int buttonId, int slotId, int itemId, ButtonOption option) {
		return false;
	}

	/**
	 * Checks if this activity handles the interaction with a node
	 *
	 * @param node
	 *               The node
	 * @param option
	 *               The option
	 */
	public boolean handleNodeInteraction(Node node, InteractionOption option) {
		if (node.isPlayer()) {
			return handlePlayerOption(node.toPlayer(), option);
		} else if (node.isNPC()) {
			return handleNPCOption(node.toNPC(), option);
		} else if (node.isGameObject()) {
			return handleObject(node.toGameObject(), option);
		} else if (node.isItem()) {
			return handleItem(node.toItem(), option);
		}
		return false;
	}

	/**
	 * Checks if this teleporation type is allowed
	 *
	 * @param type
	 *             The type
	 */
	public boolean teleportationAllowed(TeleportType type) {
		return true;
	}

	/**
	 * If the activity handles the player option
	 *
	 * @param target
	 *               The target
	 * @param option
	 *               The option
	 */
	protected boolean handlePlayerOption(Player target, InteractionOption option) {
		return false;
	}

	/**
	 * If the activity handles the npc option
	 *
	 * @param npc
	 *               The npc
	 * @param option
	 *               The option
	 */
	protected boolean handleNPCOption(NPC npc, InteractionOption option) {
		return false;
	}

	/**
	 * Handles the object interaction
	 *
	 * @param object
	 *               The object
	 * @param option
	 *               The option clicked
	 */
	public boolean handleObject(GameObject object, InteractionOption option) {
		return false;
	}

	/**
	 * Handles the item interaction
	 *
	 * @param item
	 *               The item
	 * @param option
	 *               The option
	 */
	private boolean handleItem(Item item, InteractionOption option) {
		return false;
	}

	/**
	 * If the activity can move
	 *
	 * @param x
	 *            The x
	 * @param y
	 *            The y
	 * @param dir
	 *            The direction
	 */
	public boolean canMove(int x, int y, int dir) {
		return true;
	}

	/**
	 * Checks that combat can continue with the target
	 *
	 * @param target
	 *               The target
	 */
	public boolean combatAcceptable(Entity target) {
		return true;
	}

	/**
	 * Checks if the activity saves on logout.
	 */
	public boolean savesOnLogout() {
		return false;
	}

	/**
	 * Handles the pre-death event for the specified {@link Entity}.
	 *
	 * @param entity
	 *               the entity to handle for.
	 */
	public boolean handlePreDeath(Entity entity) {
		return false;
	}

	/**
	 * Handles the post-death event for the specified {@link Entity}.
	 *
	 * @param entity
	 *               the entity to handle for.
	 */
	public boolean handlePostDeath(Entity entity) {
		return false;
	}

	/**
	 * Handles an inventory item interaction event.
	 * 
	 * @param item
	 *               the item that we interacted with
	 * @param slot
	 *               the slot which the item is at.
	 * @param option
	 *               the option that was used for the interaction.
	 * @return <code>true</code> if the event was handled otherwise
	 *         <code>false</code>.
	 */
	public boolean handleInventory(Item item, int slot, InteractionOption option) {
		return false;
	}

	/**
	 * Handles a modal interface closing event.
	 * 
	 * @param interfaceId
	 *                    the interface id.
	 * @return <code>true</code> if the event was handled otherwise
	 *         <code>false</code>.
	 */
	public boolean handleModalClosing(int interfaceId) {
		return false;
	}

	/**
	 * Handles the player logout event.
	 * 
	 * @return <code>true</code> if the event was handled otherwise
	 *         <code>false</code>.
	 */
	public boolean handlePreDisconnect() {
		return false;
	}

	/**
	 * Handles the logout post process.
	 */
	public void handlePostDisconnect() {
		// NOOP
	}

}