package net.zaros.server.game.content.zskillsystem.impl;

import net.zaros.server.game.content.zskillsystem.AbstractSkill;
import net.zaros.server.game.content.zskillsystem.properties.status.SkillStatus;
import net.zaros.server.game.content.zskillsystem.properties.type.SkillType;

/**
 * @author Jacob Rhiel
 */
public abstract class ArtisanSkill extends AbstractSkill {

    public ArtisanSkill(boolean enabled, int skillId, SkillStatus status) {
        super(enabled, skillId, status, SkillType.ARTISAN);
    }

}
