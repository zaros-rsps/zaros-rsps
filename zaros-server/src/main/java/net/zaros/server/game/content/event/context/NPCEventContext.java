package net.zaros.server.game.content.event.context;

import lombok.Getter;
import net.zaros.server.game.content.event.EventContext;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/1/2017
 */
public class NPCEventContext implements EventContext {
	
	/**
	 * The npc interacting with
	 */
	@Getter
	private final NPC npc;
	
	/**
	 * The option we selected
	 */
	@Getter
	private final InteractionOption option;

	/**
	 * The option string
	 */
	@Getter
	private final int optionIndex;
	
	public NPCEventContext(NPC npc, InteractionOption option, int optionIndex) {
		this.npc = npc;
		this.option = option;
		this.optionIndex = optionIndex;
	}
}
