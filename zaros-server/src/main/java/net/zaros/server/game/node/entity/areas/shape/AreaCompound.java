package net.zaros.server.game.node.entity.areas.shape;

/**
 * Represents an area shape that is a compound of other shapes.
 * 
 * @author Walied K. Yassen
 */
public final class AreaCompound implements AreaShape {

	/**
	 * The compound shapes.
	 */
	private final AreaShape[] shapes;

	/**
	 * Constructs a new {@link AreaCompound} type object instance.
	 * 
	 * @param shapes
	 *               the compound shapes.
	 */
	public AreaCompound(AreaShape[] shapes) {
		this.shapes = shapes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.node.entity.areas.shape.AreaShape#intersects(int,
	 * int)
	 */
	@Override
	public boolean intersects(int x, int y) {
		for (var shape : shapes) {
			if (shape.intersects(x, y)) {
				return true;
			}
		}
		return false;
	}
}
