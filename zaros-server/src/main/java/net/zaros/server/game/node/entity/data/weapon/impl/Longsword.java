package net.zaros.server.game.node.entity.data.weapon.impl;

import net.zaros.server.game.node.entity.data.weapon.AttackType;
import net.zaros.server.game.node.entity.data.weapon.WeaponInterface;
import net.zaros.server.game.node.entity.data.weapon.WeaponSpecial;
import net.zaros.server.utility.rs.Hit;

import java.util.List;

/**
 * The interface for longswords.
 *
 * @author Gabriel || Wolfsdarker
 */
public enum Longsword implements WeaponInterface {

    /**
     * The regular longsword weapon interface.
     */
    REGULAR(),

    /**
     * The dragon longsword weapon interface.
     */
    DRAGON() {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    },

    /**
     * The vesta longsword weapon interface.
     */
    VESTA() {
        @Override
        public WeaponSpecial specialAttack() {
            //TODO: Special
            return null;
        }
    };

    /**
     * The attack types.
     */
    private static final List<AttackType> attackTypes =
            List.of(AttackType.LONGSWORD_STAB, AttackType.LONGSWORD_LUNGE, AttackType.LONGSWORD_SLASH, AttackType.LONGSWORD_BLOCK);

    @Override
    public Hit.HitSplat attackType() {
        return Hit.HitSplat.MELEE_DAMAGE;
    }

    @Override
    public List<AttackType> attackStyles() {
        return attackTypes;
    }

    @Override
    public WeaponSpecial specialAttack() {
        return null;
    }
}
