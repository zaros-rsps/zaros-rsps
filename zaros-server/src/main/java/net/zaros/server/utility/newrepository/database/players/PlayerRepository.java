package net.zaros.server.utility.newrepository.database.players;

import net.zaros.server.Configuration;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.newrepository.Repository;
import net.zaros.server.utility.newrepository.RepositoryDirectory;
import net.zaros.server.utility.newrepository.loader.GsonLoader;
import net.zaros.server.utility.newrepository.loader.RepositoryLoader;

/**
 * Represents the player repository.
 * 
 * @author Walied K. Yassen
 */
public final class PlayerRepository {

	/**
	 * The players database base directory.
	 */
	private static final RepositoryDirectory DIRECTORY = Repository.getDatabaseDirectory().getDirectory(Configuration.Repository.PLAYERS, true);

	/**
	 * The players format loader.
	 */
	private static final RepositoryLoader<Player> LOADER = new GsonLoader<>(Player.class);

	/**
	 * Loads the {@link Player} object with the specified {@code username}.
	 * 
	 * @param username
	 *                 the username of the player that we want to load.
	 * @return the loaded {@link Player} object.
	 */
	public static Player load(String username) {
		var file = DIRECTORY.getFile(username + ".json");
		return LOADER.load(file);
	}

	/**
	 * Stores the specified {@link Player} object under the file with the specified
	 * {@code username}.
	 * 
	 * @param username
	 *                 the username which will be the file name that we are storing
	 *                 in.
	 * @param player
	 *                 the player object to store.
	 */
	public static void store(String username, Player player) {
		var file = DIRECTORY.getFile(username + ".json");
		LOADER.store(file, player);
	}

	/**
	 * Stores the specified {@code data} in the player file, this action will
	 * overwrite the existing data in that file or will create a new file if it was
	 * Inexistent.
	 * 
	 * @param username
	 *                 the username which will be the file name that we are storing
	 *                 in.
	 * @param player
	 *                 the data to store.
	 */
	public static void store(String username, byte[] data) {
		var file = DIRECTORY.getFile(username + ".json");
		file.writeAllBytes(data);
	}

	/**
	 * Serialises the specified {@link Player} object into a byte array.
	 * 
	 * @param player
	 *               the player object to serialise.
	 * @return the serialised data of the player.
	 */
	public static byte[] serialise(Player player) {
		return LOADER.serialise(player);
	}

	/**
	 * Checks whether or not the specified {@code username} has a player file
	 * associated with it or not.
	 * 
	 * @param username
	 *                 the username to check.
	 * @return <code>true</code> if the player exists otherwise <code>false</code>.
	 */
	public static boolean exists(String username) {
		return DIRECTORY.getFile(username + ".json").exists();
	}

	private PlayerRepository() {
		// NOOP
	}

}
