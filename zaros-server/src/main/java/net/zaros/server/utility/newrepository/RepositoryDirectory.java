package net.zaros.server.utility.newrepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Represents a single repository directory.
 * 
 * @author Walied K. Yassen
 */
@Slf4j
public final class RepositoryDirectory {

	/**
	 * The directory path.
	 */
	@Getter
	private final Path path;

	/**
	 * Constructs a new {@link RepositoryDirectory} type object instance.
	 * 
	 * @param path
	 *             the path which leads to our directory in the local file system.
	 */
	public RepositoryDirectory(String path) {
		this(Paths.get(path), false);
	}

	/**
	 * Constructs a new {@link RepositoryDirectory} type object instance.
	 * 
	 * @param path
	 *             the path which leads to our directory in the local file system.
	 */
	public RepositoryDirectory(Path path) {
		this(path, false);
	}

	/**
	 * Constructs a new {@link RepositoryDirectory} type object instance.
	 * 
	 * @param path
	 *               the path which leads to our directory in the local file system.
	 * @param create
	 *               whether or not to create the directory if it was not
	 *               inexistent.
	 */
	public RepositoryDirectory(String path, boolean create) {
		this(Paths.get(path), create);
	}

	/**
	 * Constructs a new {@link RepositoryDirectory} type object instance.
	 * 
	 * @param path
	 *               the path which leads to our directory in the local file system.
	 * @param create
	 *               whether or not to create the directory if it was not
	 *               inexistent.
	 */
	public RepositoryDirectory(Path path, boolean create) {
		this.path = path;
		if (create && !exists()) {
			create();
		}
	}

	/**
	 * Creates the directory in the local file system.
	 */
	public void create() {
		try {
			Files.createDirectories(path);
		} catch (IOException e) {
			log.error("Error while creating a repository directory: {}", path, e);
		}
	}

	/**
	 * Checks whether or not if this directory exists.
	 * 
	 * @return <code>true</code> if it does otherwise <code>false</code>.
	 */
	public boolean exists() {
		return Files.exists(path);
	}

	/**
	 * @param name
	 * @return
	 */
	public RepositoryDirectory getDirectory(String name) {
		return new RepositoryDirectory(path.resolve(name));
	}

	/**
	 * @param name
	 * @param create
	 * @return
	 */
	public RepositoryDirectory getDirectory(String name, boolean create) {
		return new RepositoryDirectory(path.resolve(name), create);
	}

	/**
	 * @param name
	 * @return
	 */
	public RepositoryFile getFile(String name) {
		return new RepositoryFile(path.resolve(name));
	}

	/**
	 * @param name
	 * @param create
	 * @return
	 */
	public RepositoryFile getFile(String name, boolean create) {
		return new RepositoryFile(path.resolve(name), create);
	}

}
