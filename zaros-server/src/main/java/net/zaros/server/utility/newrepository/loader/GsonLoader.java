package net.zaros.server.utility.newrepository.loader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.RequiredArgsConstructor;
import net.zaros.server.utility.newrepository.RepositoryFile;

/**
 * Represents a repository file JSON format loader.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class GsonLoader<T> implements RepositoryLoader<T> {

	/**
	 * The {@link Gson} object that we will use to load.
	 */
	private final Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

	/**
	 * The type of the entity we are trying to load.
	 */
	private final Class<T> type;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.newrepository.loader.RepositoryLoader#load(net.zaros
	 * .server.utility.newrepository.RepositoryFile)
	 */
	@Override
	public T load(RepositoryFile file) {
		return gson.fromJson(file.reader(), type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.newrepository.loader.RepositoryLoader#store(net.
	 * zaros.server.utility.newrepository.RepositoryFile, java.lang.Object)
	 */
	@Override
	public void store(RepositoryFile file, T entity) {
		if (file.exists()) {
			file.delete();
		}
		file.writeAllBytes(serialise(entity));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.newrepository.loader.RepositoryLoader#serialise(java
	 * .lang.Object)
	 */
	@Override
	public byte[] serialise(T entity) {
		return gson.toJson(entity).getBytes();
	}
}
