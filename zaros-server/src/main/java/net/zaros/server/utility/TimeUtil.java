package net.zaros.server.utility;

/**
 * Holds all the utilities that are related to time.
 * 
 * @author Walied K. Yassen
 */
public final class TimeUtil {

	/**
	 * Converts the specified time from {@link millisecond}s unit to {@code hour}s
	 * unit.
	 * 
	 * @param millis
	 *               the time in milliseconds which we want to convert into hours.
	 * @return the equivalent time in hours.
	 */
	public static long toHours(long millis) {
		return millis / 1000 / 60;
	}

	private TimeUtil() {
		// NOOP
	}
}
