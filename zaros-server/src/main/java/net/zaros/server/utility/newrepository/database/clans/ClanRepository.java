package net.zaros.server.utility.newrepository.database.clans;

import java.util.HashMap;
import java.util.Map;

import net.zaros.server.Configuration;
import net.zaros.server.game.clan.Clan;
import net.zaros.server.utility.newrepository.Repository;
import net.zaros.server.utility.newrepository.RepositoryDirectory;
import net.zaros.server.utility.newrepository.RepositoryFile;
import net.zaros.server.utility.newrepository.database.index.IndexTables;
import net.zaros.server.utility.newrepository.loader.GsonLoader;
import net.zaros.server.utility.newrepository.loader.RepositoryLoader;

/**
 * Represents the clan local disk repository.
 * 
 * @author Walied K. Yassen
 */
public final class ClanRepository {

	/**
	 * The clans database base directory.
	 */
	private static final RepositoryDirectory DIRECTORY = Repository.getDatabaseDirectory().getDirectory(Configuration.Repository.CLANS, true);

	/**
	 * The clans repository format loader.
	 */
	private static final RepositoryLoader<Clan> LOADER = new GsonLoader<>(Clan.class);

	/**
	 * The current loaded clans.
	 */
	public static final Map<Long, Clan> clans = new HashMap<Long, Clan>();

	/**
	 * Loads the {@link Clan} object with the specified clan {@code id} from the
	 * local disk.
	 * 
	 * @param id
	 *           the clan id.
	 * @return the created {@link Clan} object or null if anything occurred during
	 *         the loading process.
	 */
	public static Clan load(long id) {
		var clan = clans.get(id);
		if (clan == null) {
			var file = getFile(id);
			if (!file.exists()) {
				return null;
			}
			clan = LOADER.load(file);
			clan.initialise();
			clans.put(id, clan);
		}
		return clan;
	}

	/**
	 * Stores the specified {@link Clan} in our local disk repository.
	 * 
	 * @param clan
	 *             the clan to store.
	 */
	public static void store(Clan clan) {
		var file = getFile(clan.getId());
		LOADER.store(file, clan);
	}

	/**
	 * Creates a new {@link Clan} with the specified {@code name}.
	 * 
	 * @param name
	 *             the initial clan name.
	 * @return the created {@link Clan} object.
	 * @throws IllegalArgumentException
	 *                                  if the clan name is already taken by another
	 *                                  clan.
	 */
	public static Clan create(String name) {
		if (exists(name)) {
			throw new IllegalArgumentException("The specified clan name is already taken by another clan.");
		}
		// create the clan object with the given name.
		var clan = new Clan(name);
		// initialise the clan.
		clan.initialise();
		// store the clan in our local disk.
		store(clan);
		// cache the clan.
		clans.put(clan.getId(), clan);
		// register the clan name in the index-table.
		IndexTables.getClan().add(clan.getId(), clan.getName());
		return clan;
	}

	/**
	 * Deletes the clan file which has the specified clan {@code id}.
	 * 
	 * @param id
	 *           the id of the clan that we want to delete it's file.
	 */
	public static void delete(long id) {
		var file = getFile(id);
		file.delete();
		clans.remove(id);
	}

	/**
	 * Checks whether or not the specified clan {@code id} exists in our repository
	 * or not.
	 * 
	 * @param name
	 *             the clan name to check.
	 * @return <code>true</code> if it is does otherwise <code>false</code>.
	 */
	public static boolean exists(long id) {
		var file = getFile(id);
		return file.exists();
	}

	/**
	 * Checks whether or not the specified clan {@code name} exists in our
	 * repository or not.
	 * 
	 * @param name
	 *             the clan name to check.
	 * @return <code>true</code> if it is does otherwise <code>false</code>.
	 */
	public static boolean exists(String name) {
		return IndexTables.getClan().containsValue(name);
	}

	/**
	 * Gets the {@link RepositoryFile} for the specific clan.
	 * 
	 * @param id
	 *           the clan id which we want to get it's file.
	 * @return the {@link RepositoryFile} of the clan.
	 */
	private static RepositoryFile getFile(long id) {
		return DIRECTORY.getFile(id + ".json");
	}

	private ClanRepository() {
		// NOOP
	}
}
