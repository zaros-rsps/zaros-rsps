package net.zaros.server.utility.newrepository.items;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.utility.database.DatabaseManager;

/**
 * Represents the server sided only items details repository.
 * 
 * @author Walied K. Yassen
 */
@Slf4j
@SuppressWarnings("unused")
public final class ItemRepository {

	/**
	 * The default {@link ItemDetails} object if none was present.
	 */
	private static final ItemDetails DEFAULT_DETAILS = new ItemDetails(0.0d, false, false, "", "", new int[18]);

	/**
	 * The bonuses columns in the SQL table.
	 */
	private static final String[] BONUSES_COLUMNS = { "attack_stab", "attack_slash", "attack_crush", "attack_magic", "attack_range", "defense_stab", "defense_slash", "defense_crush", "defense_magic", "defense_range", "summoning", "absorb_melee", "absorb_Magic", "absorb_range", "strength", "ranged_strength", "prayer", "magic_damage" };

	/**
	 * The currently loaded item details.
	 */
	private static final Map<Integer, ItemDetails> CACHED = new HashMap<Integer, ItemDetails>();

	/**
	 * The table creation SQL command syntax.
	 */
	private static final String CREATE_SQL;

	/**
	 * The row updating SQL command syntax.
	 */
	private static final String UPDATE_SQL;

	/**
	 * Initialises the SQL commands syntax.
	 */
	static {
		{
			var builder = new StringBuilder();
			builder.append("CREATE TABLE IF NOT EXISTS  items (`id` INTEGER NOT NULL UNIQUE, `weight` REAL, `bankable` BOOLEAN DEFAULT 1, `tradeable` BOOLEAN DEFAULT 1, `examine` TEXT, `destroy` TEXT");
			for (var column : BONUSES_COLUMNS) {
				builder.append(", `").append(column).append('`').append(" INTEGER");
			}
			builder.append(");");
			CREATE_SQL = builder.toString();
		}
		{
			var builder = new StringBuilder();
			builder.append("INSERT OR REPLACE INTO items (`id`, 'weight', `bankable`, `tradeable`, `destroy`, `examine`");
			for (var column : BONUSES_COLUMNS) {
				builder.append(", `").append(column).append('`');
			}
			builder.append(") VALUES (?,?,?,?,?,?");
			for (var element : BONUSES_COLUMNS) {
				builder.append(",?");
			}
			builder.append(");");
			UPDATE_SQL = builder.toString();
		}
	}

	/**
	 * Initialises the repository table.
	 */
	public static void initialise() {
		try {

			DatabaseManager.getConfigsDatabase().executeUpdate(CREATE_SQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Looks up for the {@link ItemDetails} object with the specified {@code id} in
	 * the cached details map. If it was not present, then we attempt to load the
	 * details from the repository using the {@link #load(int)} method, then the
	 * loaded object will be cached for later use.
	 * 
	 * @param id
	 *           the item id which we want to get it's associated details object.
	 * @return the {@link ItemDetails} object.
	 */
	public static ItemDetails lookup(int id) {
		var details = CACHED.get(id);
		if (details == null) {
			CACHED.put(id, details = load(id));
		}
		return details;
	}

	/**
	 * Loads the {@link ItemDetails} object with the specified {@code id} from the
	 * database.
	 * 
	 * @param id
	 *           the id of the item that we want to get it's {@link ItemDetails}
	 *           object.
	 * @return the {@link ItemDetails} object
	 */
	public static ItemDetails load(int id) {
		try (var statement = DatabaseManager.getConfigsDatabase().prepareStatement("SELECT * from items WHERE id = ?")) {
			statement.setInt(1, id);
			var result = statement.executeQuery();
			if (!result.next()) {
				return DEFAULT_DETAILS;
			}
			var weight = result.getDouble("weight");
			var bankable = result.getBoolean("bankable");
			var tradeable = result.getBoolean("tradeable");
			var destroy = result.getString("destroy");
			var examine = result.getString("examine");
			var bonuses = new int[18];
			for (int index = 0; index < bonuses.length; index++) {
				bonuses[index] = result.getInt(BONUSES_COLUMNS[index]);
			}
			return new ItemDetails(weight, bankable, tradeable, destroy, examine, bonuses);
		} catch (SQLException e) {
			log.error("Error while trying to load item details from the database: {}", id, e);
			return DEFAULT_DETAILS;
		}
	}

	/**
	 * Stores the specified {@link ItemDetails} object into the repository.
	 * 
	 * @param id
	 *                the item id which we will store the details under.
	 * @param details
	 *                the details object which we want to store.
	 */
	public static void store(int id, ItemDetails details) {
		try (var statement = DatabaseManager.getConfigsDatabase().prepareStatement(UPDATE_SQL)) {
			var column = 1;
			statement.setInt(column++, id);
			statement.setDouble(column++, details.getWeight());
			statement.setBoolean(column++, details.isBankable());
			statement.setBoolean(column++, details.isTradeable());
			statement.setString(column++, details.getDestroy());
			statement.setString(column++, details.getExamine());
			for (var bonus : details.getBonuses() == null ? DEFAULT_DETAILS.getBonuses() : details.getBonuses()) {
				statement.setInt(column++, bonus);
			}
			statement.executeUpdate();
		} catch (SQLException e) {
			log.error("Error while trying to save the item details into the database", e);
		}
	}

	/**
	 * Saves the current set of data to the database.
	 */
	private static void save() {
		try (var statement = DatabaseManager.getConfigsDatabase().prepareStatement(UPDATE_SQL)) {
			for (var id : CACHED.keySet()) {
				var details = CACHED.get(id);
				if (details == null) {
					continue;
				}
				int column = 1;
				statement.setInt(column++, id);
				statement.setDouble(column++, details.getWeight());
				statement.setBoolean(column++, details.isBankable());
				statement.setBoolean(column++, details.isTradeable());
				statement.setString(column++, details.getDestroy());
				statement.setString(column++, details.getExamine());
				for (int bonus : details.getBonuses() == null ? new int[18] : details.getBonuses()) {
					statement.setInt(column++, bonus);
				}
				statement.addBatch();
			}
		} catch (SQLException e) {
			log.error("Error while trying to save the item details into the database", e);
		}
	}

	/**
	 * Clears the repository cache.
	 */
	public static void clear() {
		CACHED.clear();
	}

	private ItemRepository() {
		// noop
	}
}
