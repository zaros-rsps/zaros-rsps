package net.zaros.server.utility;

/**
 * The map of all attribute keys. Keys here can be used in either the temporary
 * attribute map or the saved attribute map.
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/21/2017
 */
public enum AttributeKey {

	// MISC ATTRIBUTES
	COST_VALUE,

	FOOD_DELAY,

	FIND_TARGET_DELAY,

	LAST_HIT_BY_ENTITY,

	DESTROY_INTERFACE_TYPE,

	ATTACKED_BY_TIME,

	LAST_DIALOGUE_MESSAGE,

	SKILL_MENU,

	GOD_CHARGED,

	TELEBLOCKED_UNTIL,

	MIASMIC_EFFECT,

	MIASMIC_IMMUNITY,

	// UPDATING ATTRIBUTES
	MAP_REGION_CHANGED,

	TELEPORT_LOCATION,

	TELEPORTED,

	FORCE_NEXT_MAP_LOAD,

	// saved vars
	LAST_RESTORE_TIME,

	FILTERING_PROFANITY,

	DUAL_MOUSE_BUTTONS,

	CHAT_EFFECTS,

	ACCEPTING_AID,

	FORCE_MULTI_AREA,

	LAST_LONGIN_STAMP,

	LAST_UNCOLLAPSED_TELEPORT,

	LAST_SELECTED_TELEPORT,

	// game bar status

	FILTER,
	PUBLIC,
	PRIVATE,
	FRIENDS,
	CLAN,
	TRADE,
	ASSIST,

	/**
	 * The current minigame status we are in..
	 */
	MINIGAME_STATUS,

	/**
	 * Whether we have talked to Fiara in Fist of Guthix or not.
	 */
	FOG_FIARA,

	/**
	 * The current Fist of Guthix game charges.
	 */
	FOG_CHARGES,

	/**
	 * The current Fist of Guthix pair.
	 */
	FOG_PAIR,

	/**
	 * The current ban lift time of the Fist of Guthix minigame if it was present.
	 */
	FOG_BAN,

	/**
	 * Whether or not the current player has forfeit the current session.
	 */
	FOG_FORFEIT,

	/**
	 * The Fist of Guthix house entering processEffects number.
	 */
	FOG_HOUSE,

	/**
	 * The Fist of Guthix house barrier entering time.
	 */
	FOG_BARRIER,

	/**
	 * The current combat target.
	 */
	TARGET,

	/**
	 * The last combat target.
	 */
	LAST_TARGET,

	/**
	 * The current combat attacker.
	 */
	ATTACKER,

	/**
	 * The instant of the last attack.
	 */
	LAST_ATTACK_INSTANT,

	/**
	 * The instant of the last attack taken.
	 */
	LAST_HIT_TAKEN_INSTANT,

	/**
	 * The delay for the next attack.
	 */
	ATTACK_DELAY,

	/**
	 * The primary damage mask.
	 */
	PRIMARY_DAMAGE_MASK,

	/**
	 * The secondary damage mask.
	 */
	SECONDARY_DAMAGE_MASK,

	/**
	 * The combat script's key.
	 */
	COMBAT_SCRIPT,

	/**
	 * The key for the weapon interface.
	 */
	WEAPON,

	/**
	 * The key for the fight style.
	 */
	FIGHT_TYPE,

	/**
	 * The key for auto retaliating in combat.
	 */
	AUTO_RETALIATE,

	/**
	 * The key for the current spell.
	 */
	CURRENT_SPELL,

	/**
	 * The key for the previous spell.
	 */
	PREVIOUS_SPELL,

	/**
	 * The key for the autocast spell.
	 */
	AUTOCAST_SPELL,

	/**
	 * The keu for ammunition data.
	 */
	AMMUNITION_DATA,

	/**
	 * The key for the ranged weapon data.
	 */
	RANGED_WEAPON_DATA,

	/**
	 * The key for the special weapon data.
	 */
	SPECIAL_WEAPON,

	/**
	 * The last instant for the dragon throwaxe special.
	 */
	LAST_DRAGON_THROWAXE_SPEC_INSTANT,

	/**
	 * The key for the veracs effect.
	 */
	VERACS_EFFECT,

	/**
	 * The key for the guthans effect.
	 */
	GUTHANS_EFFECT,

	/**
	 * The key for the bolt modifier.
	 */
	BOLT_MODIFIER,

	/**
	 * The key for the barrows coffin brother.
	 */
	BARROWS_COFFIN_BROTHER,

	/**
	 * The key for veracs killed.
	 */
	KILLED_VERACS,

	/**
	 * The key for dharok killed.
	 */
	KILLED_DHAROK,

	/**
	 * The key for ahrim killed.
	 */
	KILLED_AHRIM,

	/**
	 * The key for guthan killed.
	 */
	KILLED_GUTHAN,

	/**
	 * The key for karil killed.
	 */
	KILLED_KARIL,

	/**
	 * The key for torag killed.
	 */
	KILLED_TORAG,

	/**
	 * The key for last instant of prayer drain.
	 */
	LAST_BARROWS_PRAYER_DRAIN_INSTANT,

	/**
	 * The key to set spawned for.
	 */
	SPAWNED_FOR,

	/**
	 * The key for the chat mode.
	 */
	CHAT_MODE,

	/**
	 * Indicates that the player whose holding this attribute is part of founding a
	 * clan.
	 */
	CLAN_IS_FOUNDER,

	/**
	 * Holds the clan founders list.
	 */
	CLAN_FOUNDERS,
}
