package net.zaros.server.utility.newrepository;

import java.nio.file.Paths;

import net.zaros.server.Configuration;
import net.zaros.server.utility.newrepository.items.ItemRepository;
import net.zaros.server.utility.newrepository.map.xteas.XteasRepository;

/**
 * Represents our local disk repository manager.
 * 
 * @author Walied K. Yassen
 */
public final class Repository {

	/**
	 * The repository base directory.
	 */
	private static final RepositoryDirectory DIRECTORY = new RepositoryDirectory(Paths.get(Configuration.Repository.BASE), true);

	/**
	 * The map repository directory.
	 */
	private static final RepositoryDirectory MAP_DIRECTORY = DIRECTORY.getDirectory(Configuration.Repository.MAP, true);

	/**
	 * The map repository directory.
	 */
	private static final RepositoryDirectory CACHE_DIRECTORY = DIRECTORY.getDirectory(Configuration.Repository.CACHE, true);

	/**
	 * The database repository directory.
	 */
	private static final RepositoryDirectory DATABASE_DIRECTORY = DIRECTORY.getDirectory(Configuration.Repository.DATABASE, true);

	/**
	 * The plugin repository directory.
	 */
	private static final RepositoryDirectory PLUGINS_DIRECTORY = DIRECTORY.getDirectory(Configuration.Repository.PLUGINS, true);

	/**
	 * Initialises the repository.
	 */
	public static void initialise() {
		XteasRepository.initialise();
		ItemRepository.initialise();
	}

	/**
	 * @param index
	 * @return
	 */
	public static RepositoryDirectory getDirectory(String name) {
		return DIRECTORY.getDirectory(name);
	}

	/**
	 * @param index
	 * @return
	 */
	public static RepositoryDirectory getDirectory(String name, boolean create) {
		return DIRECTORY.getDirectory(name, create);
	}

	/**
	 * Gets the repository directory.
	 * 
	 * @return the repository directory.
	 */
	public static RepositoryDirectory getDirectory() {
		return DIRECTORY;
	}

	/**
	 * Gets the map repository directory.
	 * 
	 * @return the map repository directory.
	 */
	public static RepositoryDirectory getMapDirectory() {
		return MAP_DIRECTORY;
	}

	/**
	 * Gets the cache repository directory.
	 * 
	 * @return the cache repository directory.
	 */
	public static RepositoryDirectory getCacheDirectory() {
		return CACHE_DIRECTORY;
	}

	/**
	 * Gets the database repository directory.
	 * 
	 * @return the database repository directory.
	 */
	public static RepositoryDirectory getDatabaseDirectory() {
		return DATABASE_DIRECTORY;
	}

	/**
	 * Gets the plugin repository directory.
	 * 
	 * @return the plugin repository directory.
	 */
	public static RepositoryDirectory getPluginsDirectory() {
		return PLUGINS_DIRECTORY;
	}

	private Repository() {
		// NOOP
	}
}
