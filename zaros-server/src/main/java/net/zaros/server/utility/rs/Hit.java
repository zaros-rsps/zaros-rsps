package net.zaros.server.utility.rs;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.node.entity.Entity;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Represents a damage to hit.
 *
 * @author Emperor
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/21/2017
 */
public class Hit {
	
	/**
	 * The attributes a hit can have, used for storing data about where the hit came from.
	 */
	@Getter
	private final ConcurrentHashMap<HitAttributes, Object> attributes;
	
	/**
	 * The entity dealing the damage.
	 */
	@Getter
	protected final Entity source;
	
	/**
	 * The damage hitsplat.
	 */
	@Getter
	@Setter
	private HitSplat splat;
	
	/**
	 * The amount of damage to be dealt
	 */
	@Getter
	private int damage;
	
	/**
	 * The max hit
	 */
	private double maxHit = -1;
	
	/**
	 * The amount of soaked damage.
	 */
	@Getter
	@Setter
	private int soaked;
	
	/**
	 * The delay on the hit
	 */
	@Getter
	@Setter
	private int delay;
	
	public Hit(Entity source, int damage) {
		this(source, damage, HitSplat.REGULAR_DAMAGE, 1);
	}
	
	public Hit(Entity source, int damage, HitSplat splat) {
		this(source, damage, splat, 1);
	}

	public Hit(Entity source, int damage, HitSplat splat, int delay) {
		this.source = source;
		this.damage = damage;
		this.splat = splat;
		this.soaked = 0;
		this.delay = delay;
		this.attributes = new ConcurrentHashMap<>();
	}

	/**
	 * Sets the delay of the hit
	 */
	public int getClientTicks() {
		return 600 * delay / 20;
	}

	/**
	 * Decreases the hit's delay by 1.
	 */
	public void decreaseDelay() {
		delay -= 1;
	}
	
	/**
	 * Sets the max hit
	 *
	 * @param maxHit
	 * 		The max hit
	 */
	public Hit setMaxHit(double maxHit) {
		this.maxHit = maxHit;
		return this;
	}
	
	/**
	 * Checks if the hit is critical, based on the max hit and the hit landed.
	 */
	public boolean isCritical() {
		if (maxHit == -1 || damage == 0) {
			return false;
		}
		// we can only set combat splats to critical
		if (!splat.isDefaultCombatSplat()) {
			return false;
		}
		double criticalMinimum = maxHit * 0.90;
		return damage >= criticalMinimum;
	}
	
	/**
	 * Gets an attribute
	 *
	 * @param key
	 * 		The key of the attribute
	 * @param defaultValue
	 * 		The default value to return
	 * @param <K>
	 * 		The attribute tyle
	 */
	@SuppressWarnings("unchecked")
	public <K> K getAttribute(HitAttributes key, K defaultValue) {
		K value = (K) attributes.get(key);
		if (value == null) {
			return defaultValue;
		}
		return value;
	}
	
	@Override
	public String toString() {
		return "Hit{" + "source=" + source + ", splat=" + splat + ", damage=" + damage + ", maxHit=" + maxHit + '}';
	}
	
	/**
	 * Represents the damage types.
	 *
	 * @author Emperor
	 */
	public enum HitSplat {
		
		MISSED(8),
		REGULAR_DAMAGE(3),
		DRAGONFIRE(3),
		MELEE_DAMAGE(0),
		RANGE_DAMAGE(1),
		MAGIC_DAMAGE(2),
		MAGICAL_MELEE_DAMAGE(2),
		MAGICAL_RANGED_DAMAGE(2),
		REFLECTED_DAMAGE(4),
		ABSORB_DAMAGE(5),
		POISON_DAMAGE(6),
		DESEASE_DAMAGE(7),
		HEALED_DAMAGE(9),
		CANNON_DAMAGE(13);
		
		@Getter
		@Setter
		private int mark;
		
		HitSplat(int mark) {
			this.setMark(mark);
		}
		
		public boolean isDefaultCombatSplat() {
			return this == MELEE_DAMAGE || this == RANGE_DAMAGE || this == MAGIC_DAMAGE;
		}
	}
	
	/**
	 * Sets the damage
	 *
	 * @param damage
	 * 		The damage
	 */
	public void setDamage(int damage) {
		this.damage = damage;
		if (damage <= 0) {
			setSplat(HitSplat.MISSED);
		}
	}
	
	/**
	 * The possible attributes of the hit
	 */
	public enum HitAttributes {
		HIT_SOUND,
		WEAPON_USED,
		SPECIAL_ATTACK_USED,
		FORCE_LEECH,

		/**
		 * The key for the veracs effect.
		 */
		VERACS_EFFECT,

		/**
		 * The key for the guthans effect.
		 */
		GUTHANS_EFFECT,

		/**
		 * The key for the bolt modifier.
		 */
		BOLT_MODIFIER,
	}
}
