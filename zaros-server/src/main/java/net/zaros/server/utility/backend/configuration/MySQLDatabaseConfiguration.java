package net.zaros.server.utility.backend.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * A database configuration for MySQL Database servers
 *
 * @author Nikki
 */
@AllArgsConstructor
public class MySQLDatabaseConfiguration {
	
	/**
	 * The database server host
	 */
	@Getter
	@Setter
	private String host;
	
	/**
	 * The database server port
	 */
	@Getter
	@Setter
	private int port;
	
	/**
	 * The database on the server
	 */
	@Getter
	@Setter
	private String database;
	
	/**
	 * The username of the server
	 */
	@Getter
	@Setter
	private String username;
	
	/**
	 * The password of the server
	 */
	@Getter
	@Setter
	private String password;

}