package net.zaros.server.utility.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import lombok.Getter;

/**
 * Represents an SQLite {@link Database} implmentation.
 * 
 * @author Walied K. Yassen
 */
public final class SQLiteDatabase implements Database {

	/**
	 * The database data source.
	 */
	@Getter
	private final HikariDataSource source;

	/**
	 * Constructs a new {@link SQLiteDatabase} type object instance.
	 * 
	 * @param poolName
	 *                 the database pool name.
	 * @param path
	 *                 the database local file path.
	 */
	public SQLiteDatabase(String poolName, String path) {
		// create the data source configurations.
		var config = new HikariConfig();
		config.setPoolName(poolName);
		config.setDriverClassName("org.sqlite.JDBC");
		config.setJdbcUrl("jdbc:sqlite:" + path);
		config.setConnectionTestQuery("SELECT 1");
		config.setInitializationFailFast(false);
		config.setMaxLifetime(60000);
		config.setIdleTimeout(30000);
		config.setMaximumPoolSize(25);
		config.setAutoCommit(false);
		config.addDataSourceProperty("prepStmtCacheSize", 250);
		config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
		config.addDataSourceProperty("cachePrepStmts", true);
		config.addDataSourceProperty("useServerPrepStmts", true);
		config.addDataSourceProperty("useLocalSessionState", true);
		config.addDataSourceProperty("rewriteBatchedStatements", true);
		config.addDataSourceProperty("cacheResultSetMetadata", true);
		config.addDataSourceProperty("cacheServerConfiguration", true);
		config.addDataSourceProperty("maintainTimeStats", true);
		// create the data source object.
		source = new HikariDataSource(config);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.database.Database#executeQuery(java.lang.String)
	 */
	@Override
	public ResultSet executeQuery(String query) throws SQLException {
		var statement = createStatement();
		var result = statement.executeQuery(query);
		statement.getConnection().close();
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.database.Database#executeUpdate(java.lang.String)
	 */
	@Override
	public int executeUpdate(String query) throws SQLException {
		var statement = createStatement();
		var result = statement.executeUpdate(query);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.database.Database#prepareStatement(java.lang.String,
	 * int)
	 */
	@Override
	public PreparedStatement prepareStatement(String sql, int... columns) throws SQLException {
		var connection = getConnection();
		return connection.prepareStatement(sql, columns);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.database.Database#prepareStatement(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public PreparedStatement prepareStatement(String sql, String... columns) throws SQLException {
		var connection = getConnection();
		return connection.prepareStatement(sql, columns);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.database.Database#prepareStatement(java.lang.String)
	 */
	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		var connection = getConnection();
		return connection.prepareStatement(sql);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.utility.database.Database#createStatement()
	 */
	@Override
	public Statement createStatement() throws SQLException {
		var connection = getConnection();
		return connection.createStatement();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.utility.database.Database#getConnection()
	 */
	@Override
	public Connection getConnection() throws SQLException {
		return source.getConnection();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.utility.database.Database#finish(java.sql.Statement)
	 */
	@Override
	public void finish(Statement statement) throws SQLException {
		finish(statement.getConnection());
		statement.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.utility.database.Database#finish(java.sql.Connection)
	 */
	@Override
	public void finish(Connection connection) throws SQLException {
		connection.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		try {
			try {
				if (!source.isClosed()) {
					source.close();
				}
			} catch (Throwable e) {
				// NOOP
			}
		} finally {
			super.finalize();
		}
	}
}
