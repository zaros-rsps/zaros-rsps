package net.zaros.server.utility.database;

import lombok.Getter;
import net.zaros.server.Configuration;
import net.zaros.server.utility.newrepository.Repository;

/**
 * Represents our database manager.
 * 
 * @author Walied K. Yassen
 */
public final class DatabaseManager {

	/**
	 * Represents the configurations database.
	 */
	@Getter
	private static Database configsDatabase;

	/**
	 * Initialises the database manager.
	 */
	public static void initialise() {
		configsDatabase = new SQLiteDatabase("Configurations", Repository.getDatabaseDirectory().getFile(Configuration.Repository.CONFIGS_DATABASE).getAbsolutePath());
	}

	private DatabaseManager() {
		// NOOP
	}
}
