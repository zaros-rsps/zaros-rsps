package net.zaros.server.utility.newrepository.database.index.impl;

import org.apache.commons.lang3.tuple.Pair;

import net.zaros.cache.util.buffer.Buffer;
import net.zaros.server.utility.newrepository.database.index.IndexTable;
import net.zaros.server.utility.newrepository.database.index.IndexTables;

/**
 * Represents the clans index table. Used to look-up the name of a clan by it's
 * clan uid or hash.
 * 
 * @author Walied K. Yassen
 */
public final class ClanIndexTable extends IndexTable<Long, String> {

	/**
	 * Constructs a new {@link ClanIndexTable} type object instance.
	 */
	public ClanIndexTable() {
		super(IndexTables.getDirectory().getFile("clans.index"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.repository.index.IndexTable#decode(net.zaros.cache.
	 * util.buffer.Buffer)
	 */
	@Override
	protected Pair<Long, String> decode(Buffer buffer) {
		return Pair.of(buffer.readLong(), buffer.readString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.repository.index.IndexTable#encode(net.zaros.cache.
	 * util.buffer.Buffer, java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void encode(Buffer buffer, Long key, String value) {
		buffer.writeLong(key);
		buffer.writeString(value);
	}
}
