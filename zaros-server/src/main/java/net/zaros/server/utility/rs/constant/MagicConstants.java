package net.zaros.server.utility.rs.constant;

import lombok.Getter;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/23/2017
 */
public interface MagicConstants extends SkillConstants {

	/**
	 * The ids of runes
	 */
	@SuppressWarnings("unused")
	int AIR_RUNE = 556, WATER_RUNE = 555, EARTH_RUNE = 557, FIRE_RUNE = 554, MIND_RUNE = 558, BODY_RUNE = 559, NATURE_RUNE = 561, CHAOS_RUNE = 562, DEATH_RUNE = 560, BLOOD_RUNE = 565, SOUL_RUNE = 566, ASTRAL_RUNE = 9075, LAW_RUNE = 563, STEAM_RUNE = 4694, MIST_RUNE = 4695, DUST_RUNE = 4696, SMOKE_RUNE = 4697, MUD_RUNE = 4698, LAVA_RUNE = 4699, ARMADYL_RUNE = 21773, COSMIC_RUNE = 564;

	/**
	 * Checks whether or not the specified item {@link id} is an elemental rune item
	 * id.
	 * 
	 * @param id
	 *           the item id to check.
	 * @return <code>true</code> if it is otherwise <code>false</code>.
	 */
	public static boolean isElementalRune(int id) {
		switch (id) {
		case AIR_RUNE:
		case FIRE_RUNE:
		case EARTH_RUNE:
		case WATER_RUNE:
			return true;
		default:
			return false;
		}
	}

	/**
	 * Checks whether or not the specified item {@link id} is a catalytic rune item
	 * id.
	 * 
	 * @param id
	 *           the item id to check.
	 * @return <code>true</code> if it is otherwise <code>false</code>.
	 */
	public static boolean isCatalyticRune(int id) {
		switch (id) {
		case MIND_RUNE:
		case CHAOS_RUNE:
		case DEATH_RUNE:
		case BODY_RUNE:
		case SOUL_RUNE:
		case COSMIC_RUNE:
		case ASTRAL_RUNE:
		case NATURE_RUNE:
		case LAW_RUNE:
		case ARMADYL_RUNE:
		case BLOOD_RUNE:
			return true;
		default:
			return false;
		}
	}

	enum MagicBook {
		REGULAR(192),
		ANCIENTS(193),
		LUNARS(430);

		/**
		 * The id of the interface
		 */
		@Getter
		private final int interfaceId;

		MagicBook(int interfaceId) {
			this.interfaceId = interfaceId;
		}
	}
}
