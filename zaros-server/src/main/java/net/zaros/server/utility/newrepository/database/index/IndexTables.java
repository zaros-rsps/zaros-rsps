package net.zaros.server.utility.newrepository.database.index;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import lombok.Getter;
import net.zaros.server.Configuration;
import net.zaros.server.utility.newrepository.Repository;
import net.zaros.server.utility.newrepository.RepositoryDirectory;
import net.zaros.server.utility.newrepository.database.index.impl.ClanIndexTable;
import net.zaros.server.utility.newrepository.database.index.impl.UniqueIndexTable;

/**
 * Represents the index tables manager.
 * 
 * @author Walied K. Yassen
 */
public final class IndexTables {

	/**
	 * The index tables base directory.
	 */
	private static final RepositoryDirectory DIRECTORY = Repository.getDatabaseDirectory().getDirectory(Configuration.Repository.INDEX, true);

	/**
	 * The clans index table.
	 */
	@Getter
	private static ClanIndexTable clan;

	/**
	 * The unique index table.
	 */
	@Getter
	private static UniqueIndexTable unique;

	/**
	 * Initialises the index tables.
	 * 
	 * @param service
	 *                the scheduler service, used for scheduling the saving task.
	 */
	public static void initialise(ScheduledExecutorService service) {
		clan = new ClanIndexTable();
		unique = new UniqueIndexTable();
		service.scheduleAtFixedRate(IndexTables::save, 3, 3, TimeUnit.MINUTES);
	}

	/**
	 * Saves the index tables.
	 */
	private static void save() {
		unique.save();
		clan.save();
	}

	/**
	 * Gets the index tables database base directory.
	 * 
	 * @return the index tables database base directory.
	 */
	public static RepositoryDirectory getDirectory() {
		return DIRECTORY;
	}
}
