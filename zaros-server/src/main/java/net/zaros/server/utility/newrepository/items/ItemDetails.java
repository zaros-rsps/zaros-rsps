package net.zaros.server.utility.newrepository.items;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * @author Walied K. Yassen
 */
@Builder
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class ItemDetails {

	/**
	 * The item weight.
	 */
	@Getter
	private final double weight;

	/**
	 * Whether or not the item is bankable.
	 */
	@Getter
	private final boolean bankable;

	/**
	 * Whether or not the item is tradeable.
	 */
	@Getter
	private final boolean tradeable;

	/**
	 * The item destroy message.
	 */
	@Getter
	private final String destroy;

	/**
	 * The item examine message.
	 */
	@Getter
	private final String examine;

	/**
	 * The item bonuses.
	 */
	@Getter
	private final int[] bonuses;

}
