package net.zaros.server.utility.newrepository;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Walied K. Yassen
 */
@Slf4j
public final class RepositoryFile {

	/**
	 * The create options set.
	 */
	private static final StandardOpenOption[] CREATE = new StandardOpenOption[] { StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE };

	/**
	 * The append create options set.
	 */
	private static final StandardOpenOption[] APPEND_CREATE = new StandardOpenOption[] { StandardOpenOption.APPEND, StandardOpenOption.CREATE };

	/**
	 * The path which leads to our repository file.
	 */
	@Getter
	private final Path path;

	/**
	 * Constructs a new {@link RepositoryFile} type object instance.
	 * 
	 * @param path
	 *             the path which leads to our file in the local file system.
	 */
	public RepositoryFile(String path) {
		this(Paths.get(path), false);
	}

	/**
	 * Constructs a new {@link RepositoryFile} type object instance.
	 * 
	 * @param path
	 *             the path which leads to our file in the local file system.
	 */
	public RepositoryFile(Path path) {
		this(path, false);
	}

	/**
	 * Constructs a new {@link RepositoryFile} type object instance.
	 * 
	 * @param path
	 *               the path which leads to our file in the local file system.
	 * @param create
	 *               whether or not to create the file if it was not inexistent.
	 */
	public RepositoryFile(String path, boolean create) {
		this(Paths.get(path), create);
	}

	/**
	 * Constructs a new {@link RepositoryFile} type object instance.
	 * 
	 * @param path
	 *               the path which leads to our repository file.
	 * @param create
	 *               whether or not to create the file if it was inexistent.
	 */
	public RepositoryFile(Path path, boolean create) {
		this.path = path;

	}

	/**
	 * Attempts to read all of the local file bytes.
	 * 
	 * @return the read data as a {@code byte[]} object.
	 */
	public byte[] readAllBytes() {
		try {
			return Files.readAllBytes(path);
		} catch (IOException e) {
			log.error("Error while reading repository file bytes: {}", path, e);
			return new byte[0];
		}
	}

	/**
	 * Writes the specified {@code bytes} into the file in the local file system.
	 * 
	 * @param bytes
	 *              the bytes to write.
	 */
	public void writeAllBytes(byte[] bytes) {
		writeAllBytes(bytes, false);
	}

	/**
	 * Writes the specified {@code bytes} into the file in the local file system.
	 * 
	 * @param bytes
	 *               the bytes to write.
	 * @param append
	 *               whether to append the data or just overwrite the existing data
	 *               (<code>true</code> for appending).
	 */
	public void writeAllBytes(byte[] bytes, boolean append) {
		/*if (exists() && !append) {
			// delete the file if it does not exist, to prevent the overwriting.
			delete();
		}*/
		try {
			Files.write(path, bytes, append ? APPEND_CREATE : CREATE);
		} catch (IOException e) {
			log.error("Error while writing repository file bytes: {}", path, e);
		}
	}

	/**
	 * Creates a new {@link BufferedReader} for this file.
	 * 
	 * @return the created {@link BufferedReader} object.
	 */
	public BufferedReader reader() {
		try {
			return Files.newBufferedReader(path);
		} catch (IOException e) {
			log.error("Error while creating a file reader: {}", path, e);
			return null;
		}
	}

	/**
	 * Creates a new {@link BufferedWriter} for this file.
	 * 
	 * @param options
	 *                the file open options.
	 * @return the created {@link BufferedWriter} object.
	 */
	public BufferedWriter writer(StandardOpenOption... options) {
		try {
			return Files.newBufferedWriter(path, options);
		} catch (IOException e) {
			log.error("Error while creating a file writer: {}", path, e);
			return null;
		}
	}

	/**
	 * Creates the file in the local file system.
	 */
	public void create() {
		try {
			Files.createFile(path);
		} catch (IOException e) {
			log.error("Error while creating a repository file: {}", path, e);
		}
	}

	/**
	 * Deletes this file if it exists from the local file system.
	 */
	public void delete() {
		try {
			Files.deleteIfExists(path);
		} catch (IOException e) {
			log.error("Error while deleting a repository file: {}", path, e);
		}
	}

	/**
	 * Checks whether or not if this file exists.
	 * 
	 * @return <code>true</code> if it does otherwise <code>false</code>.
	 */
	public boolean exists() {
		return Files.exists(path);
	}

	/**
	 * Gets the absolute path of this file.
	 * 
	 * @return the absolute path.
	 */
	public String getAbsolutePath() {
		return path.toFile().getAbsolutePath();
	}
}
