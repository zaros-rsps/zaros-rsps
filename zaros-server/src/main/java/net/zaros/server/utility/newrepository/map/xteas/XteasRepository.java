package net.zaros.server.utility.newrepository.map.xteas;

import java.util.Map;
import java.util.TreeMap;

import lombok.RequiredArgsConstructor;
import net.zaros.server.Configuration;
import net.zaros.server.utility.newrepository.Repository;
import net.zaros.server.utility.newrepository.RepositoryDirectory;
import net.zaros.server.utility.newrepository.RepositoryFile;
import net.zaros.server.utility.newrepository.loader.GsonLoader;
import net.zaros.server.utility.newrepository.loader.RepositoryLoader;

/**
 * Represents the map XTEAs repository.
 * 
 * @author Walied K. Yassen
 */
public final class XteasRepository {

	/**
	 * The XTEAS repository directory.
	 */
	private static final RepositoryDirectory DIRECTORY = Repository.getMapDirectory().getDirectory(Configuration.Repository.XTEAS, true);

	/**
	 * The XTEAs repository format loader.
	 */
	private static final RepositoryLoader<Xtea[]> LOADER = new GsonLoader<>(Xtea[].class);

	/**
	 * The currently loaded XTEAs map.
	 */
	private static final Map<Integer, int[]> XTEAS = new TreeMap<Integer, int[]>();

	/**
	 * The default XTEAs key set.
	 */
	private static final int[] DEFAULT_KEYS = new int[4];

	/**
	 * Initialises the XTEAs repository.
	 */
	public static void initialise() {
		var file = DIRECTORY.getFile("xteas.json");
		if (!file.exists()) {
			save(file);
		} else {
			load(file);
		}
	}

	/**
	 * Loads all of the XTEAs from the specified {@link RepositoryFile file}.
	 * 
	 * @param file
	 *             the file to load from.
	 */
	public static void load(RepositoryFile file) {
		var xteas = LOADER.load(file);
		for (var xtea : xteas) {
			XTEAS.put(xtea.region, xtea.keys);
		}
	}

	/**
	 * Stores all of the currently loaded XTEAs in the specified
	 * {@link RepositoryFile file}.
	 * 
	 * @param file
	 *             the file to store in.
	 */
	public static void save(RepositoryFile file) {
		var xteas = XTEAS.entrySet().stream().map(entry -> new Xtea(entry.getKey(), entry.getValue())).toArray(Xtea[]::new);
		LOADER.store(file, xteas);
	}

	/**
	 * Looks-up the XTEAs key set for the specified {@code region} id.
	 * 
	 * @param region
	 *               the region id which we want it's XTEAs key set.
	 * @return the XTEAs key set of that region.
	 */
	public static int[] lookup(int region) {
		return XTEAS.getOrDefault(region, DEFAULT_KEYS);
	}

	/**
	 * A redundant class used to infer the types for the {@link GsonLoader}.
	 * 
	 * @author Walied K. Yassen
	 */
	@RequiredArgsConstructor
	private static final class Xtea {

		/**
		 * The region id which the keys are for.
		 */
		public final int region;
		/**
		 * The XTEA keys.
		 */
		public final int[] keys;
	}

	private XteasRepository() {
		// NOOP
	}
}
