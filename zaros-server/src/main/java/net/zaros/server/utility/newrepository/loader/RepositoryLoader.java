package net.zaros.server.utility.newrepository.loader;

import net.zaros.server.utility.newrepository.RepositoryFile;

/**
 * Represents a specific repository format loader, such as JSON, or Binary or
 * any other format.
 * 
 * @author Walied K. Yassen
 */
public interface RepositoryLoader<T> {

	/**
	 * Loads the entity object from the specified repository {@code file}.
	 * 
	 * @param file
	 *             the repository file to load the entity from.
	 * @return the loaded entity object.
	 */
	T load(RepositoryFile file);

	/**
	 * Stores the specified {@code entity} in the specified {@code file} using this
	 * format loader.
	 * 
	 * @param file
	 *               the file which to store the entity in.
	 * @param entity
	 *               the entity which we want to store.
	 */
	void store(RepositoryFile file, T entity);

	/**
	 * Serialises the specified {@code entity} into a byte array.
	 * 
	 * @param entity
	 *               the entity to serialise.
	 * @return the serialised entity dat as a {@code byte} array.
	 */
	byte[] serialise(T entity);
}
