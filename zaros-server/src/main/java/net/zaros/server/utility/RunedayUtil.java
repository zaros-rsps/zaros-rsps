package net.zaros.server.utility;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

/**
 * Contains all of the the RuneDay utilities.
 * 
 * @author Walied K. Yassen
 */
public final class RunedayUtil {

	/**
	 * The day which the runeday system was introduced, also known as the day that
	 * membership was introduced in.
	 */
	private static final LocalDateTime RUNEDAY_START = LocalDateTime.of(2002, 2, 27, 0, 0);

	public static void main(String[] args) {
		System.out.println(RUNEDAY_START.toEpochSecond(ZoneOffset.ofHours(0)) / 60);
	}
	/**
	 * The RuneDay time zone.
	 */
	private static final ZoneId TIMEZONE = ZoneId.of("Europe/London");

	/**
	 * Gets the current time in RuneDays unit.
	 * 
	 * @return the current time in RuneDays.
	 */
	public static int getCurrentRuneDay() {
		return toRuneDay(System.currentTimeMillis());
	}

	/**
	 * Converts the specified time from {@code millisecond} unit to {@code RuneDay}
	 * unit.
	 * 
	 * @param millis
	 *               the milliseconds of the time that we want in RuneDay instead.
	 * @return the RuneDay of the given time.
	 */
	public static int toRuneDay(long millis) {
		return (int) ChronoUnit.DAYS.between(RUNEDAY_START, LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), TIMEZONE));
	}

	/**
	 * Converts the specified {@code RuneDays} to milliseconds time.
	 * 
	 * @param runeDays
	 *                 the RuneDays to convert to milliseconds.
	 * @return the milliseconds of the provided RuneDays.
	 */
	public static long toMillis(int runeDays) {
		return RUNEDAY_START.plusDays(runeDays)
				.atZone(TIMEZONE)
				.toInstant()
				.toEpochMilli();
	}

	private RunedayUtil() {
		// NOOP
	}
}
