package net.zaros.server.utility.rs.constant;

import net.zaros.cache.type.objtype.ItemDefinition;
import net.zaros.cache.type.objtype.ItemDefinitionParser;
import net.zaros.server.game.node.item.Item;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/21/2017
 */
public interface EquipConstants {

	/**
	 * The slot in the equipment container
	 */
	byte SLOT_HAT = 0, SLOT_CAPE = 1, SLOT_AMULET = 2, SLOT_WEAPON = 3, SLOT_CHEST = 4, SLOT_SHIELD = 5, SLOT_LEGS = 7, SLOT_HANDS = 9, SLOT_FEET = 10, SLOT_RING = 12, SLOT_ARROWS = 13, SLOT_AURA = 14;
	
	/**
	 * Checks if an item's definitions will result in being full body.
	 *
	 * @param def
	 * 		The definitions
	 */
	static boolean isFullBody(ItemDefinition def) {
		return def.getEquipType() == 6;
	}

	static boolean hideArms(ItemDefinition def) {
		String name = def.getName().toLowerCase();
		return name.contains("d'hide") || name.equalsIgnoreCase("leather body");
	}
	
	/**
	 * Checks if an item's definitions will result in being full hat.
	 *
	 * @param def
	 * 		The definitions
	 */
	static boolean isFullHat(ItemDefinition def) {
		return def.getEquipType() == 8;
	}
	
	/**
	 * Checks if an item's definitions will result in being full mask.
	 *
	 * @param def
	 * 		The definitions
	 */
	static boolean isFullMask(ItemDefinition def) {
		String name = def.getName().toLowerCase();
		return !isFullHat(def)
				|| name.contains("horns") || name.contains("hat") || name.contains("afro") || name.contains("cowl")
				|| name.contains("tattoo") || name.contains("headdress") || name.contains("hood")
				|| name.contains("mask") && !name.contains("h'ween")
				|| name.contains("helm") && !name.contains("full");
	}
	
	static int getItemSlot(int itemId) {
		return ItemDefinitionParser.forId(itemId).getEquipSlot();
	}
	
	static boolean isTwoHanded(Item item) {
		return item.getDefinitions().getEquipType() == 5;
	}
}
