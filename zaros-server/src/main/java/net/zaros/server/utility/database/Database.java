package net.zaros.server.utility.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Walied K. Yassen
 */
public interface Database {

	/**
	 * @param query
	 * @return
	 * @throws SQLException
	 */
	ResultSet executeQuery(String query) throws SQLException;

	/**
	 * @param query
	 * @return
	 * @throws SQLException
	 */
	int executeUpdate(String query) throws SQLException;

	/**
	 * @param sql
	 * @param columns
	 * @return
	 * @throws SQLException
	 */
	PreparedStatement prepareStatement(String sql, int... columns) throws SQLException;

	/**
	 * @param sql
	 * @param columns
	 * @return
	 * @throws SQLException
	 */
	PreparedStatement prepareStatement(String sql, String... columns) throws SQLException;

	/**
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	PreparedStatement prepareStatement(String sql) throws SQLException;

	/**
	 * Creates an empty {@link Statement} object.
	 * 
	 * @return the created {@link Statement} object.
	 * @throws SQLException
	 *                      if anything occurs during the statement object creation.
	 */
	Statement createStatement() throws SQLException;

	/**
	 * Gets a free and ready to use {@link Connection} object from the database
	 * connections pool.
	 * 
	 * @return the {@link Connection} object.
	 * @throws SQLException
	 *                      if anything occurs during retrieving or creating the
	 *                      connection object.
	 */
	Connection getConnection() throws SQLException;

	/**
	 * @param statement
	 */
	void finish(Statement statement) throws SQLException;

	/**
	 * @param connection
	 */
	void finish(Connection connection) throws SQLException;
}