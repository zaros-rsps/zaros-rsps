package net.zaros.server.utility.newrepository.database.index;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import net.zaros.cache.util.buffer.Buffer;
import net.zaros.cache.util.buffer.DynamicBuffer;
import net.zaros.cache.util.buffer.FixedBuffer;
import net.zaros.server.utility.newrepository.RepositoryFile;

/**
 * Represents an index table. An index table (to this definition) is a table
 * which takes {@code K} as an input and gives and {@code V} as an output. Those
 * both variables are stored within a file on the local disk which we call the
 * index table.
 * 
 * @author Walied K. Yassen
 */
public abstract class IndexTable<K, V> {

	/**
	 * The entries list that is currently within the table.
	 */
	protected final Map<K, V> entries = new HashMap<K, V>();

	/**
	 * The repository file of the index table.
	 */
	private final RepositoryFile file;

	/**
	 * Whether the content of this table has been changed or not.
	 */
	private boolean changed;

	/**
	 * Constructs a new {@link IndexTable} type object instance.
	 * 
	 * @param file
	 *             the path which leads to the index-table file on the local disk.
	 */
	public IndexTable(RepositoryFile file) {
		this.file = file;
		initialise();
	}

	/**
	 * Initialises the index table.
	 */
	private void initialise() {
		if (file.exists()) {
			load();
		} else {
			changed = true;
			save();
		}
	}

	/**
	 * Loads the index table content from the local disk content.
	 */
	public void load() {
		// check whether the file exists or not.
		if (!file.exists()) {
			return;
		}
		// wrap the data into a buffer object.
		var buffer = new FixedBuffer(file.readAllBytes());
		// read how many entries do we have within the table.
		var count = buffer.readInt();
		// keep looping until we have zero elements to read.
		while (count-- > 0) {
			// decode the next pair.
			var pair = decode(buffer);
			// add the result key and value to the map.
			entries.put(pair.getKey(), pair.getValue());
		}
	}

	/**
	 * Decodes the next index-table entry ({@code key} and {@code value}) from the
	 * specified {@link Buffer buffer}.
	 * 
	 * @param buffer
	 *               the buffer to decode from.
	 * @return the decoded key and value wraped in a {@link Pair} object.
	 */
	protected abstract Pair<K, V> decode(Buffer buffer);

	/**
	 * Saves the current state and entries of the index table to the local disk.
	 */
	public void save() {
		if (!changed) {
			return;
		}
		// delete the file if it exists.
		file.delete();
		// create the buffer object.
		var buffer = new DynamicBuffer(16);
		// write the entries count.
		buffer.writeInt(entries.size());
		// loop through all the elements and encodes them.
		for (var key : entries.keySet()) {
			encode(buffer, key, entries.get(key));
		}
		// convert the buffer into a byte array.
		var data = buffer.getDataTrimmed();
		// write the converted data into the local disk.
		file.writeAllBytes(data);
	}

	/**
	 * Encodes the specified index-table entry ({@code key} and {@code value}) into
	 * given the {@code buffer}.
	 * 
	 * @param buffer
	 *               the buffer encode into.
	 * @param key
	 *               the key of the entry to encode.
	 * @param value
	 *               the value of the entry to encode.
	 */
	protected abstract void encode(Buffer buffer, K key, V value);

	/**
	 * Looks-up for the an index-table entry with the specified {@code key}.
	 * 
	 * @param key
	 *            the index-table entry key.
	 * @return the index-table entry value object.
	 */
	public V lookup(K key) {
		return entries.get(key);
	}

	/**
	 * Looks-up for the an index-table entry with the specified {@code key}.
	 * 
	 * @param key
	 *                     the index-table entry key.
	 * @param defaultValue
	 *                     the default value to return when the key it is not
	 *                     present.
	 * @return the index-table entry value object.
	 */
	public V lookup(K key, V defaultValue) {
		return entries.getOrDefault(key, defaultValue);
	}

	/**
	 * Adds a new index-table entry to this table.
	 * 
	 * @param key
	 *              the entry key.
	 * @param value
	 *              the entry value.
	 */
	public void add(K key, V value) {
		entries.put(key, value);
		changed = true;
	}

	/**
	 * Removes the index-table entry by it's specified {@code key}.
	 * 
	 * @param key
	 *            the key for the entry to remove.
	 */
	public void remove(K key) {
		entries.remove(key);
		changed = true;
	}

	/**
	 * Checks whether or not this index table contains the specified {@code key}.
	 * 
	 * @param key
	 *            the key to check if it exists within this table.
	 * @return <code>true</code> if it does otherwise <code>false</code>.
	 */
	public boolean containsKey(K key) {
		return entries.containsKey(key);
	}

	/**
	 * Checks whether or not this index table contains the specified {@code value}.
	 * 
	 * @param value
	 *              the value to check if it exists within this table.
	 * @return <code>true</code> if it does otherwise <code>false</code>.
	 */
	public boolean containsValue(V value) {
		return entries.containsValue(value);
	}

	/**
	 * Gets the index-table entries count.
	 * 
	 * @return the entries count within this table.
	 */
	public int size() {
		return entries.size();
	}
}
