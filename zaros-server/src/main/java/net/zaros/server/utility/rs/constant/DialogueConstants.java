package net.zaros.server.utility.rs.constant;

/**
 * A class for storing facial expressions.
 *
 * @author Thomas Le Godais
 */
public interface DialogueConstants {

	/**
	 * The default option
	 */
	String DEFAULT_OPTION = "Select an Option";

	public static final int NORMAL = 9827;
	public static final int QUESTIONS = 9827;
	public static final int MAD = 9789;
	public static final int MOCK = 9878;
	public static final int LAUGHING = 9851;
	public static final int WORRIED = 9775;
	public static final int HAPPY = 9850;
	public static final int CONFUSED = 9830;
	public static final int DRUNK = 9835;
	public static final int ANGERY = 9790;
	public static final int SAD = 9775;
	public static final int SCARED = 9780;
	public static final int E9785 = 9785;
	public static final int E9811 = 9811;
	public static final int E8381 = 8381;
	public static final int ERM_THINKING = 9812;
	public static final int E9836 = 9836;
	public static final int E9814 = 9814;
	public static final int E9827 = 9827;
	public static final int E9761 = 9761;
	public static final int E9828 = 9828;
	public static final int E9807 = 9807;
	public static final int E9808 = 9808;
	public static final int E8478 = 8478;
	public static final int E9809 = 9809;
	public static final int E9810 = 9810;
	public static final int E9848 = 9848;
	public static final int E9849 = 9849;
	public static final int E9829 = 9829;
	public static final int E9812 = 9812;
	public static final int E9840 = 9840;
	public static final int E9843 = 9843;
	public static final int E9844 = 9844;
	public static final int E9847 = 9847;
	public static final int E9759 = 9759;
	public static final int E9786 = 9786;
}