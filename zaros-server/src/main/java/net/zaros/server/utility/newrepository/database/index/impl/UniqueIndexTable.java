package net.zaros.server.utility.newrepository.database.index.impl;

import org.apache.commons.lang3.tuple.Pair;

import net.zaros.cache.util.buffer.Buffer;
import net.zaros.server.utility.newrepository.database.index.IndexTable;
import net.zaros.server.utility.newrepository.database.index.IndexTables;
import net.zaros.server.utility.newrepository.database.index.impl.UniqueIndexTable.UniqueDomain;

/**
 * @author Walied K. Yassen
 */
public final class UniqueIndexTable extends IndexTable<UniqueDomain, Long> {

	/**
	 * Constructs a new {@link UniqueIndexTable} type object instance.
	 */
	public UniqueIndexTable() {
		super(IndexTables.getDirectory().getFile("unique.index"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.repository.index.IndexTable#decode(net.zaros.cache.
	 * util.buffer.Buffer)
	 */
	@Override
	protected Pair<UniqueDomain, Long> decode(Buffer buffer) {
		// readString in-case the order was changed,
		return Pair.of(UniqueDomain.valueOf(buffer.readString()), buffer.readLong());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.utility.repository.index.IndexTable#encode(net.zaros.cache.
	 * util.buffer.Buffer, java.lang.Object, java.lang.Object)
	 */
	@Override
	protected void encode(Buffer buffer, UniqueDomain key, Long value) {
		buffer.writeString(key.name());
		buffer.writeLong(value);
	}

	/**
	 * Generates a new unique id.
	 * 
	 * @param type
	 *             the unique id domain.
	 * @return the generated unique id.
	 */
	public long generateId(UniqueDomain type) {
		var value = lookup(type, 1L);
		add(type, value + 1);
		return value;
	}

	/**
	 * Represents a unique index type.
	 * 
	 * @author Walied K. Yassen
	 */
	public static enum UniqueDomain {
		/**
		 * The player UID type.
		 */
		PLAYER,

		/**
		 * The clan UID type.
		 */
		CLAN,
	}
}
