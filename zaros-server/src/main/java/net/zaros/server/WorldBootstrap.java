package net.zaros.server;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.game.world.World;

/**
 * Represents the main class for bootstrapping our World Server or alternatively
 * known as Game Server.
 * 
 * @author Walied K. Yassen
 */
@Slf4j
public final class WorldBootstrap {

	/**
	 * The main method executed from the JVM
	 *
	 * @param args
	 *             Program arguments
	 */
	public static void main(String[] args) {
		try {
			World.initialise(args);
		} catch (Exception e) {
			log.error("Unexpected error on initialization", e);
			System.exit(0);
		}
	}

	/**
	 * A private constructor which prevents the {@code WorldBootstrap} class type to
	 * be instanced anywhere outside this class.
	 * 
	 * @throws IllegalAccessException
	 *                                if this constructor was accessed in a illegal
	 *                                away (reflection).
	 */
	private WorldBootstrap() throws IllegalAccessException {
		throw new IllegalAccessException(getClass().getSimpleName() + " is not allowed to be instantiate.");
	}
}
