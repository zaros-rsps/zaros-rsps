package net.zaros.server.core.newtask.scheduler.tick;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.zaros.server.core.newtask.base.AbstractTask;

/**
 * Represents a tick based task, it executes from the world thread each tick.
 * 
 * @author Walied K. Yassen
 */
@Slf4j
public abstract class TickTask extends AbstractTask implements Comparable<TickTask> {

	/**
	 * The current tick number, it starts at 0 and increments at the end of each
	 * execution.
	 */
	protected int tick;

	/**
	 * The period time in ticks between each execution after the initial execution.
	 */
	@Getter
	@Setter
	private int period;

	/**
	 * The next execution time in ticks..
	 */
	@Getter
	@Setter
	private int nextExecution;

	/**
	 * Whether or not this task is asyncrhonous.
	 */
	@Getter
	@Setter
	private boolean asynchronous;

	/**
	 * The owner task future.
	 */
	@Getter
	@Setter
	private TickFuture future;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			execute();
			tick++;
		} catch (Throwable e) {
			log.error("An error occurred during the task execution", e);
			future.cancel();
		}
	}

	/**
	 * Executes a single tick round.
	 */
	protected abstract void execute();

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.newtask.Task#cancel()
	 */
	@Override
	public void cancel() {
		future.cancel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(TickTask o) {
		return nextExecution - o.nextExecution;
	}

	/**
	 * Checks whether or not this task is still alive.
	 * 
	 * @return <code>true</code> if it is otherwise <code>false</code>.
	 */
	public boolean isAlive() {
		return nextExecution != -1;
	}

	/**
	 * Checks whether or not this task is dead.
	 * 
	 * @return <code>true</code> if it is otherwise <code>false</code>.
	 */
	public boolean isDead() {
		return nextExecution == -1;
	}
}
