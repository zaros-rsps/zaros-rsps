package net.zaros.server.core.event.player;

import net.zaros.server.game.node.entity.player.Player;

/**
 * Represents an {@link Event} that occurs when the player has just logged in.
 * 
 * @author Walied K. Yassen
 */
public final class PlayerLoginEventPost extends PlayerEvent {

	/**
	 * Construct a new {@link PlayerLoginEventPost} type object instance.
	 * 
	 * @param player
	 *               the event source player.
	 */
	public PlayerLoginEventPost(Player player) {
		super(player);
	}
}
