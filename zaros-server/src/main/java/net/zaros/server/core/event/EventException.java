package net.zaros.server.core.event;

/**
 * The <code>EventException</code> class is the base exception class for all of
 * the events exceptions.
 * 
 * @author Walied K. Yassen
 */
public class EventException extends RuntimeException {

	/**
	 * The serialisation key of the {@link EventException} class type.
	 */
	private static final long serialVersionUID = -3396369510988534666L;

	/**
	 * Constructs a new {@link EventException} object instance.
	 */
	public EventException() {
		super();
	}

	/**
	 * Constructs a new {@link EventException} object instance.
	 *
	 * @param message
	 *                    the detailed message.
	 * @param cause
	 *                    the exception cause.
	 */
	public EventException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new {@link EventException} object instance.
	 *
	 * @param message
	 *                    the detailed message of this exception.
	 */
	public EventException(String message) {
		super(message);
	}

	/**
	 * Constructs a new {@link EventException} object instance.
	 *
	 * @param cause
	 *                  the exception cause.
	 */
	public EventException(Throwable cause) {
		super(cause);
	}
}
