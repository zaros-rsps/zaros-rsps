package net.zaros.server.core.event.player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.zaros.server.core.event.CancellableEvent;
import net.zaros.server.core.event.Event;
import net.zaros.server.game.node.entity.player.Player;

/**
 * Represents a player-sourced {@link Event}, which is basically an
 * {@link Event} that occurred because of a specific player.
 * 
 * @author Walied K. Yassen
 */
@AllArgsConstructor
public abstract class PlayerEvent extends CancellableEvent {

	/**
	 * The player which caused this event to happen.
	 */
	@Getter
	protected final Player player;
}
