package net.zaros.server.core.system;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/22/2017
 */
@Slf4j
public class SystemFinalization extends Thread {

	@Override
	public void run() {
		int count = 0;
		try {
			for (Player player : World.getPlayers()) {
				if (player == null) {
					continue;
				}
				player.save();
				count++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("System shutdown has been executed and saved " + count + " players.");
	}
}
