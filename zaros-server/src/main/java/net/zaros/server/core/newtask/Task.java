package net.zaros.server.core.newtask;

/**
 * Represents the base interface for any task in our task system.
 * 
 * @author Walied K. Yassen
 */
public interface Task extends Runnable {

	/**
	 * Cancels this task. The procedure varies depending on the implementation.
	 */
	void cancel();

	/**
	 * Gets the task id.
	 * 
	 * @return the task id.
	 */
	int getId();
}
