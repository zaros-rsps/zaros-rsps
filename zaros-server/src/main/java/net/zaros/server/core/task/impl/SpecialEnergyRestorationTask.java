package net.zaros.server.core.task.impl;

import net.zaros.server.core.SequencialUpdate;
import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/10/2017
 */
public class SpecialEnergyRestorationTask extends ScheduledTask{
	
	public SpecialEnergyRestorationTask() {
		super(50, -1);
	}
	
	@Override
	public void run() {
		for (Player player : SequencialUpdate.getRenderablePlayers()) {
			player.getCombatDefinitions().reduceSpecial(-10);
		}
	}
}
