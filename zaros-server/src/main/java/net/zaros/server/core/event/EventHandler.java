package net.zaros.server.core.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.Method;
import java.util.concurrent.Executor;

/**
 * Represents a single event handler, an event handler is an object which
 * represents a method that is annotated with a {@link EventListener}
 * annotation. This object stores more information about the annotated methods,
 * such as the {@link #handle unreflected} object. The object is only used for
 * invocation and should not be used for any other stuff.
 *
 * @author Walied K. Yassen
 */
public class EventHandler {

	/**
	 * The {@link Logger} instance of the {@link EventHandler} class type.
	 */
	private static final Logger logger = LoggerFactory.getLogger(EventHandler.class);

	/**
	 * The event handler identifier.
	 */
	private final EventIdentifier<?> identifier;

	/**
	 * The reflected event method.
	 */
	private final Method method;

	/**
	 * The unreflected event method handle.
	 */
	private final MethodHandle handle;

	/**
	 * The handler priority.
	 */
	private final EventPriority priority;

	/**
	 * Should be asynchronously executed?
	 */
	private final boolean asynchronous;

	/**
	 * Tells whether the handler should respect the event cancellation or not.
	 */
	private final boolean respectCancellation;

	/**
	 * Constructs a new {@link EventHandler} object instance.
	 *
	 * @param identifier
	 *                                the event handler identifier.
	 * @param method
	 *                                the reflected event method.
	 * @param handle
	 *                                the unreflected event method.
	 * @param priority
	 *                                the event priority.
	 * @param asynchronous
	 *                                should the event run asynchronously?
	 * @param respectCancellation
	 *                                should the event respect the cancellation
	 *                                state?
	 */
	public EventHandler(EventIdentifier<?> identifier, Method method, MethodHandle handle, EventPriority priority, boolean asynchronous, boolean respectCancellation) {
		this.identifier = identifier;
		this.method = method;
		this.handle = handle;
		this.priority = priority;
		this.asynchronous = asynchronous;
		this.respectCancellation = respectCancellation;
	}

	/**
	 * Handles the invocation of this event handler.
	 *
	 * @param executor
	 *                     the asynchronous events task executor. Used to execute
	 *                     the asynchronous events.
	 * @param event
	 *                     the event object which the invocation is happening for.
	 */
	public void handle(Executor executor, Event event) {
		if (event.isCancelled() && respectCancellation) {
			return;
		}
		if (asynchronous) {
			executor.execute(() -> invoke(event));
		} else {
			invoke(event);
		}
	}

	/**
	 * Invokes this event listener.
	 *
	 * @param event
	 *                  the event object which the invocation is happening for.
	 */
	private void invoke(Event event) {
		try {
			handle.invoke(event);
		} catch (Throwable e) {
			logger.error("Error while invoking EventHandler handle", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (asynchronous ? 1231 : 1237);
		result = prime * result + (handle == null ? 0 : handle.hashCode());
		result = prime * result + (identifier == null ? 0 : identifier.hashCode());
		result = prime * result + (method == null ? 0 : method.hashCode());
		result = prime * result + (priority == null ? 0 : priority.hashCode());
		result = prime * result + (respectCancellation ? 1231 : 1237);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EventHandler other = (EventHandler) obj;
		if (asynchronous != other.asynchronous) {
			return false;
		}
		if (handle == null) {
			if (other.handle != null) {
				return false;
			}
		} else if (!handle.equals(other.handle)) {
			return false;
		}
		if (identifier == null) {
			if (other.identifier != null) {
				return false;
			}
		} else if (!identifier.equals(other.identifier)) {
			return false;
		}
		if (method == null) {
			if (other.method != null) {
				return false;
			}
		} else if (!method.equals(other.method)) {
			return false;
		}
		if (priority != other.priority) {
			return false;
		}
		if (respectCancellation != other.respectCancellation) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the handler identifier. Used to distinguish which handlers belong to
	 * specific set of handlers.
	 *
	 * @return the handler identifier if it was present otherwie {@code null}.
	 */
	public EventIdentifier<?> getIdentifier() {
		return identifier;
	}

	/**
	 * Gets the handler event reflected method.
	 *
	 * @return the handler event reflected method.
	 */
	public Method getMethod() {
		return method;
	}

	/**
	 * Gets the handler event unreflected method.
	 *
	 * @return the handler event unreflected method.
	 */
	public MethodHandle getHandle() {
		return handle;
	}

	/**
	 * Gets the event priority. The event priority controls what handlers should be
	 * invoked first.
	 *
	 * @return the event {@link EventPriority priority}.
	 */
	public EventPriority getPriority() {
		return priority;
	}

	/**
	 * Checks whether if the handler should executed the event asynchronously or
	 * not.
	 *
	 * @return <code>true</code> if it should otherwise <code>false</code>.
	 */
	public boolean isAsynchronous() {
		return asynchronous;
	}

	/**
	 * Checks whether if the handler should respect the event cancellation state or
	 * not.
	 *
	 * @return <code>true</code> if it should otherwise <code>false</code>.
	 */
	public boolean isRespectCancellation() {
		return respectCancellation;
	}

}
