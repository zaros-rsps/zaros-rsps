package net.zaros.server.core.newtask.future;

/**
 * Represents a task completion listener.
 * 
 * @author Walied K. Yassen
 */
public interface TaskFutureListener {

	default void onComplete() {

	}

	default void onFailure() {

	}
}
