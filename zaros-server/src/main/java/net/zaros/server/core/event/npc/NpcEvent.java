package net.zaros.server.core.event.npc;

import net.zaros.server.core.event.CancellableEvent;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;

/**
 * Represents a {@link CancellableEvent} that has occurred due to interacting
 * with an NPC.
 * 
 * @author Walied K. Yassen
 */
public final class NpcEvent extends CancellableEvent {

	/**
	 * The player who interacted with the NPC.
	 */
	private final Player player;

	/**
	 * The NPC which was interacted with.
	 */
	private final NPC npc;

	/**
	 * The NPC option that was used in the interaction.
	 */
	private final NpcOption option;

	/**
	 * Construct a new {@link NpcEvent} type object instance.
	 * 
	 * @param player
	 * @param npc
	 * @param option
	 */
	public NpcEvent(Player player, NPC npc, NpcOption option) {
		this.player = player;
		this.npc = npc;
		this.option = option;
	}

	/**
	 * Gets the {@link Player} who interacted with the {@link NPC}.
	 * 
	 * @return the {@link Player} object.
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Gets the {@link NPC} that was interacted with.
	 * 
	 * @return the {@link NPC} object.
	 */
	public NPC getNpc() {
		return npc;
	}

	/**
	 * Gets the {@link NpcOption}.
	 * 
	 * @return the {@link NpcOption}.
	 */
	public NpcOption getOption() {
		return option;
	}
}
