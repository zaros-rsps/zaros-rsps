package net.zaros.server.core.event;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * An event bucket is basically a list of {@link EventHandler} ordered by
 * priority and optimised for performance.
 * </p>
 * The {@link EventBucket} type is responsible for registering and unregistering
 * the {@link EventHandler} objects and batching them for performance.
 * </p>
 * Batching is basically a process that turns all the complicated collections
 * into one array for faster iterations and accessing.
 *
 * @author Walied K. Yassen
 */
public final class EventBucket {

	/**
	 * An empty batched array of {@link EventHandler} to save performance and space.
	 */
	private static final EventHandler[] EMPTY_BUCKET = new EventHandler[0];

	/**
	 * The handlers map containing all the {@link EventHandler} objects by
	 * {@link EventPriority}.
	 */
	private final Map<EventPriority, List<EventHandler>> handlers;

	/**
	 * The up-to-date batched array of {@link EventHandler} to save performance and
	 * space.
	 */
	private EventHandler[] batched;

	/**
	 * The current amount of {@link EventHandler} objects within this bucket.
	 */
	private int size;

	/**
	 * Constructs a new {@link EventBucket} object instance.
	 */
	protected EventBucket() {
		handlers = new EnumMap<>(EventPriority.class);
		batched = EMPTY_BUCKET;
	}

	/**
	 * Batches all the {@link EventHandler} within this bucket into one single
	 * {@link EventHandler[]} array object.
	 * </p>
	 * The batched array elements are ordered from the ones having the
	 * {@linkplain EventPriority#HIGHEST highest} priority to the ones having the
	 * {@linkplain EventPriority#LOWEST lowest} priority (Where the highest priority
	 * starts at the index <code>0</code> growing up to the lowest).
	 */
	public void batch() {
		// check whether the batched handlers array needs recreation or not.
		if (batched == null || size != batched.length) {
			batched = size == 0 ? EMPTY_BUCKET : new EventHandler[size];
		}
		// the current cursor, indicates our position within the batched array.
		int cursor = 0;
		// iterate throughout each priority and batch it's handlers.
		for (EventPriority priority : EventPriority.values()) {
			// find the handlers set for current priority.
			List<EventHandler> handlers = this.handlers.get(priority);
			// check if the handlers exist for the current priority or not.
			if (handlers == null) {
				continue;
			}
			// add each handler into our batched array.
			for (EventHandler handler : handlers) {
				batched[cursor++] = handler;
			}
		}

	}

	/**
	 * Registers the specified {@link EventHandler} to this {@link EventBucket}. By
	 * registering it is basically adding the specified handler to our unbatched
	 * handlers map. and later is batched along with other handlers upon requesting.
	 * 
	 * @param handler
	 *                    the event handler to register.
	 */
	public void register(EventHandler handler) {
		List<EventHandler> handlers = this.handlers.get(handler.getPriority());
		if (handlers == null) {
			this.handlers.put(handler.getPriority(), handlers = new ArrayList<>());
		}
		if (handlers.contains(handler)) {
			return;
		}
		if (handlers.add(handler)) {
			size++;
			if (batched != null) {
				batched = null;
			}
		}
	}

	/**
	 * Unregisters the specified {@link EventHandler} from this {@link EventBucket}.
	 * By unregistering it is basically removing the specified handler from our
	 * unbatched handlers map. and later is removed from the batched along with
	 * other handlers upon requesting.
	 * 
	 * @param handler
	 *                    the event handler to unregister.
	 */
	public void unregister(EventHandler handler) {
		List<EventHandler> handlers = this.handlers.get(handler.getPriority());
		if (handlers == null) {
			return;
		}
		if (!handlers.contains(handler)) {
			return;
		}
		System.out.println(handler);
		if (handlers.remove(handler)) {
			size--;
			if (batched != null) {
				batched = null;
			}
		}
	}

	/**
	 * Clears this bucket from handlers. Any {@link EventHandler} within this bucket
	 * will be removed and the batched array will be reset.
	 */
	public void clear() {
		handlers.clear();
		batched = EMPTY_BUCKET;
		size = 0;
	}

	/**
	 * Gets the batched {@link EventHandler} array object. If there was none present
	 * then we will batch the current handlers list using the {@link #batch()}
	 * method.
	 *
	 * @return the batched {@link EventHandler} array object
	 */
	public EventHandler[] getBatched() {
		if (batched == null) {
			batch();
		}
		return batched;
	}

	/**
	 * Gets the bucket size. The size is determined by how many {@link EventHandler}
	 * objects that are currently within this bucket.
	 *
	 * @return the current bucket size.
	 */
	public int size() {
		return size;
	}
}
