package net.zaros.server.core.task.impl;

import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.link.prayer.Prayer;
import net.zaros.server.game.world.World;
import net.zaros.server.utility.rs.constant.SkillConstants;
import net.zaros.server.utility.tool.RandomFunction;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/28/2017
 */
public class SkillRestorationTask extends ScheduledTask {
	
	public SkillRestorationTask() {
		super(100, -1);
	}
	
	@Override
	public void run() {
		for (Player player : World.getPlayers()) {
			if (player == null || !player.isRenderable()) {
				continue;
			}
			int restoreCount = player.getPrayers().prayerOn(Prayer.RAPID_RESTORE) ? 2 : 1;
			if (player.getTemporaryAttribute("resting", false)) {
				restoreCount += 1;
			}
			boolean berserkUsage = player.getPrayers().prayerOn(Prayer.BERSERKER);
			for (int skill = 0; skill < 25; skill++) {
				if (skill == SkillConstants.SUMMONING) {
					continue;
				}
				for (int time = 0; time < restoreCount; time++) {
					int currentLevel = player.getSkills().getLevel(skill);
					int normalLevel = player.getSkills().getLevelForXp(skill);
					if (currentLevel > normalLevel) {
						if ((skill == SkillConstants.ATTACK || skill == SkillConstants.STRENGTH || skill == SkillConstants.DEFENCE || skill == SkillConstants.RANGE || skill == SkillConstants.MAGIC) && berserkUsage && RandomFunction.getRandom(100) <= 15) {
							continue;
						}
						player.getSkills().setLevel(skill, currentLevel - 1);
					} else if (currentLevel < normalLevel) {
						player.getSkills().setLevel(skill, currentLevel + 1);
					} else {
						break;
					}
				}
			}
		}

		for (NPC npc : World.getNpcs()) {
			if (npc == null || !npc.isRenderable()) {
				continue;
			}
			int restoreCount = npc.getPrayers().prayerOn(Prayer.RAPID_RESTORE) ? 2 : 1;
			if (npc.getTemporaryAttribute("resting", false)) {
				restoreCount += 1;
			}
			boolean berserkUsage = npc.getPrayers().prayerOn(Prayer.BERSERKER);
			for (int skill = 0; skill < 7; skill++) {
				for (int time = 0; time < restoreCount; time++) {
					int currentLevel = npc.getSkills().getLevel(skill);
					int normalLevel = npc.getSkills().getLevelForXp(skill);
					if (currentLevel > normalLevel) {
						if ((skill == SkillConstants.ATTACK || skill == SkillConstants.STRENGTH
								|| skill == SkillConstants.DEFENCE
								|| skill == SkillConstants.RANGE
								|| skill == SkillConstants.MAGIC)
								&& berserkUsage && RandomFunction.getRandom(100) <= 15) {
							continue;
						}
						npc.getSkills().setLevel(skill, currentLevel - 1);
					} else if (currentLevel < normalLevel) {
						npc.getSkills().setLevel(skill, currentLevel + 1);
					} else {
						break;
					}
				}
			}
		}
	}
}
