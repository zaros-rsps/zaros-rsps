package net.zaros.server.core.service;

import java.util.concurrent.ExecutorService;

/**
 * Represents the main interface for every service in our system.
 * 
 * @author Walied K. Yassen
 */
public interface Service<T> {

	/**
	 * Starts the service.
	 */
	void start(ExecutorService executor);

	/**
	 * Serves the specified {@code entry}.
	 * 
	 * @param entry
	 *              the entry to serve.
	 */
	void serve(T entry);

	/**
	 * Shuts the service down.
	 */
	void shutdown();
}
