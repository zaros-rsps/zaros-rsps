/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.core.event.player;

import net.zaros.server.game.node.entity.player.Player;

/**
 * Represents an {@link Event} that occurs right before the player is being
 * processed and logged in onto the world.
 * 
 * @author Walied K. Yassen
 */
public final class PlayerLoginEventPre extends PlayerEvent {

	/**
	 * Construct a new {@link PlayerLoginEventPre} type object instance.
	 * 
	 * @param player
	 *               the event source player.
	 */
	public PlayerLoginEventPre(Player player) {
		super(player);
	}
}
