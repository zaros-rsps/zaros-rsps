package net.zaros.server.core.task.impl;

import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.world.World;

/**
 * Represents the minigame manager pulsing task.
 * 
 * @author Walied K. Yassen
 */
public final class MinigamePulseTask extends ScheduledTask {

	/**
	 * Construct a new {@link MinigamePulseTask} type object instance.
	 */
	public MinigamePulseTask() {
		super(1, -1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.core.task.ScheduledTask#run()
	 */
	@Override
	public void run() {
		World.getMinigameManager().process();
	}
}
