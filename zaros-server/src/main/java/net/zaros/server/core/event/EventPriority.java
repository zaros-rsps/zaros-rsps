package net.zaros.server.core.event;

/**
 * An event priority is what controls which order each event is executed by. The
 * highest the priority is the earlier it gets executed.
 * 
 * @author Walied K. Yassen
 */
public enum EventPriority {

	/**
	 * The highest priority allowed in our system, events with this priority will
	 * have the invocation order of the-first.
	 */
	HIGHEST,

	/**
	 * The second highest priority allowed in our system, events with this priority
	 * will have the invocation order of the-after-first.
	 */
	HIGHER,

	/**
	 * The third highest priority allowed in our system, events with this priority
	 * will have the invocation order of before-normal.
	 */
	HIGH,

	/**
	 * The normal and the default priority, events with this priority will have the
	 * invocation order same as the declaration order.
	 */
	NORMAL,

	/**
	 * The third lowest priority possible in our system, events with this priority
	 * will have the invocation order of after-normal.
	 */
	LOW,

	/**
	 * The second lowest priority possible in our system, events with this priority
	 * will have the invocation order of before-the-last.
	 */
	LOWER,

	/**
	 * The lowest priority possible in our system, events with this priority will
	 * have the invocation order of the-last.
	 */
	LOWEST,
}
