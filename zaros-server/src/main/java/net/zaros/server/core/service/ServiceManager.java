package net.zaros.server.core.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import lombok.Getter;
import net.zaros.server.core.service.impl.PlayerInitialisationService;
import net.zaros.server.core.service.impl.PlayerLoginService;

/**
 * Represents the server logic services manager.
 * 
 * @author Walied K. Yassen
 */
public final class ServiceManager {

	/**
	 * The services executor service.
	 */
	private static ExecutorService executor;

	/**
	 * The player initialisation service.
	 */
	@Getter
	private static PlayerInitialisationService playerInitialisationService;

	/**
	 * The player login service.
	 */
	@Getter
	private static PlayerLoginService playerLoginService;

	/**
	 * Initialises the services manager.
	 */
	public static void initialise() {
		// initialise the executor.
		executor = Executors.newCachedThreadPool(new ThreadFactoryBuilder().setNameFormat("service-thread-%d").setDaemon(false).build());
		// initialise the services.
		playerInitialisationService = new PlayerInitialisationService();
		playerLoginService = new PlayerLoginService();
		// start the services.
		start();
	}

	/**
	 * Starts the services manager.
	 */
	public static void start() {
		playerInitialisationService.start(executor);
		playerLoginService.start(executor);
	}

	/**
	 * Shuts the services manager down.
	 */
	public static void shutdown() {
		try {
			if (playerInitialisationService != null) {
				playerInitialisationService.shutdown();
			}
		} finally {
			if (executor != null) {
				executor.shutdownNow();
				executor = null;
			}
		}
	}

	private ServiceManager() {
		// NOOP
	}
}
