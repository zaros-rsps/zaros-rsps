package net.zaros.server.core.event;

/**
 * The main and base class for all of the events in our system.
 * 
 * @author Walied K. Yassen
 */
public abstract class Event {

	/**
	 * Checks whether this event was cancelled or not.
	 *
	 * @return <code>true</code> if the event was cancelled otherwise
	 *         <code>false</code>.
	 * @see CancellableEvent
	 */
	public boolean isCancelled() {
		return false;
	}
}
