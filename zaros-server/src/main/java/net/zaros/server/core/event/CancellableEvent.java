package net.zaros.server.core.event;

/**
 * A base {@link Event} implementation that supports cancelling the event.
 *
 * @author Walied K. Yassen
 */
public abstract class CancellableEvent extends Event {

	/**
	 * Represents or not the event is currently canceled.
	 */
	private volatile boolean cancelled;

	/**
	 * Marks the event as cancelled. When the event is marked as cancelled, the
	 * expected behaviour is to ignore the event in the executor pipeline.
	 * 
	 * @see EventManager#post(Event)
	 */
	public final void cancel() {
		if (cancelled) {
			return;
		}
		cancelled = true;
	}

	/**
	 * Marks the event as uncancelled. When the event is marked as uncancelled, the
	 * expected behaviour is to respect the event in the executor pipeline.
	 *
	 * @see EventManager#post(Event)
	 */
	public final void uncancel() {
		if (!cancelled) {
			return;
		}
		cancelled = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.praesul.core.event.Event#isCancelled()
	 */
	@Override
	public boolean isCancelled() {
		return cancelled;
	}
}
