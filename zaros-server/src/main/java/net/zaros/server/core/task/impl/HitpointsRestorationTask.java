package net.zaros.server.core.task.impl;

import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/28/2017
 */
public class HitpointsRestorationTask extends ScheduledTask {
	
	public HitpointsRestorationTask() {
		super(10, -1);
	}
	
	@Override
	public void run() {
		for (Player player : World.getPlayers()) {
			if (player == null || player.isDead() || !player.isRenderable()) {
				continue;
			}
			player.restoreHitPoints();
		}
		for (NPC npc : World.getNpcs()) {
			if (npc == null || npc.isDead() || !npc.isRenderable()) {
				continue;
			}
			npc.restoreHitPoints();
		}
	}
}
