package net.zaros.server.core.event.loc;

/**
 * Represents a location interaction option.
 * 
 * @author Walied K. Yassen
 */
public enum LocOption {
	FIRST,
	SECOND,
	THIRD,
	FOURTH,
	FIFTH,
	EXAMINE,
}
