package net.zaros.server.core.newtask.base;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.core.newtask.Task;

/**
 * Represents an abstract implementation of the {@link Task} interface, this has
 * the common methods and fields for the implementations of the {@link Task}
 * interface.
 * 
 * @author Walied K. Yassen
 */
public abstract class AbstractTask implements Task {

	/**
	 * The task id in the scheduler.
	 */
	@Getter
	@Setter
	private int id = -1;

	/**
	 * Initialises this {@link AbstractTask} object.
	 * 
	 * @param id
	 *           the task id in the scheduler.
	 */
	public void initialise(int id) {
		if (this.id != -1) {
			throw new IllegalStateException("The task has been already initialised before.");
		}
		this.id = id;
	}
}
