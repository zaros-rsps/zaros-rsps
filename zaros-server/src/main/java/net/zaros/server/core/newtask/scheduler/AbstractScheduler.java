package net.zaros.server.core.newtask.scheduler;

import java.util.concurrent.atomic.AtomicInteger;

import net.zaros.server.core.newtask.Task;
import net.zaros.server.core.newtask.future.TaskFuture;

/**
 * Represents an abstract implementation of the {@link Scheduler} interface.
 * 
 * @author Walied K. Yassen
 */
public abstract class AbstractScheduler<T extends Task, F extends TaskFuture<T>> implements Scheduler<T, F> {

	/**
	 * The task id(s) generator.
	 */
	private final AtomicInteger ids = new AtomicInteger(1);

	/**
	 * Generates a new task id.
	 * 
	 * @return the generated task id.
	 */
	protected int generateId() {
		return ids.getAndIncrement();
	}
}
