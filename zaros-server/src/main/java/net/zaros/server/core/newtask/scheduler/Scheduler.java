package net.zaros.server.core.newtask.scheduler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import net.zaros.server.core.newtask.Task;
import net.zaros.server.core.newtask.future.TaskFuture;

/**
 * Represents a task scheduler. It is responsible for running, executing, and ,
 * cancelling, or scheduling tasks.
 * 
 * @author Walied K. Yassen
 */
public interface Scheduler<T extends Task, F extends TaskFuture<T>> {

	/**
	 * @param task
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	default F run(Runnable task) {
		return run(createTask(task));
	}

	/**
	 * @param task
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	F run(T task);

	/**
	 * @param task
	 * @param delay
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	default F runDelayed(Runnable task, int delay) {
		return runDelayed(createTask(task), delay);
	}

	/**
	 * @param task
	 * @param delay
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	F runDelayed(T task, int delay);

	/**
	 * @param task
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	default F runAsync(Runnable task) {
		return runAsync(createTask(task));
	}

	/**
	 * @param task
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	F runAsync(T task);

	/**
	 * @param task
	 * @param delay
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	default F runAsyncDelayed(Runnable task, int delay) {
		return runAsyncDelayed(createTask(task), delay);
	}

	/**
	 * @param task
	 * @param delay
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	F runAsyncDelayed(T task, int delay);

	/**
	 * @param task
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	default F schedule(Runnable task) {
		return schedule(createTask(task));
	}

	/**
	 * @param task
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	F schedule(T task);

	/**
	 * @param task
	 * @param delay
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	default F scheduleDelayed(Runnable task, int delay) {
		return scheduleDelayed(task, delay);
	}

	/**
	 * @param task
	 * @param delay
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	F scheduleDelayed(T task, int delay);

	/**
	 * @param task
	 * @param delay
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	default F scheduleDelayed(Runnable task, int delay, int period) {
		return scheduleDelayed(createTask(task), delay, period);
	}

	/**
	 * @param task
	 * @param delay
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	F scheduleDelayed(T task, int delay, int period);

	/**
	 * @param task
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	default F scheduleAsync(Runnable task) {
		return scheduleAsync(createTask(task));
	}

	/**
	 * @param task
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	F scheduleAsync(T task);

	/**
	 * @param task
	 * @param delay
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	default F scheduleAsyncDelayed(Runnable task, int delay) {
		return scheduleAsyncDelayed(createTask(task), delay);
	}

	/**
	 * Schedules an asynchronous delayed task. An asynchronous delayed task is a
	 * regular task that executes asynchronously and after a fixed time delay.
	 * </p>
	 * A scheduled task is a task is executed repeatedly until is called to stop. It
	 * has a specific fixed time duration between each executions, and a fixed time
	 * before the initial execution.
	 * 
	 * @param task
	 *              the task object, which will have it's {@link Runnable#run()}
	 *              invoked for execution.
	 * @param delay
	 *              the delay time, which is the time that a task waits before
	 *              performing the initial execution.
	 * @return the future of the task as {@link TaskFuture} object.
	 * @see Scheduler#scheduleAsyncDelayed(Task, int, int)
	 */
	F scheduleAsyncDelayed(T task, int delay);

	/**
	 * @param task
	 * @param delay
	 * @return the future of the task as {@link TaskFuture} object.
	 * @see Scheduler#scheduleAsyncDelayed(Task, int, int)
	 */
	default F scheduleAsyncDelayed(Runnable task, int delay, int period) {
		return scheduleAsyncDelayed(createTask(task), delay, period);
	}

	/**
	 * Schedules an asynchronous delayed task. An asynchronous delayed task is a
	 * regular task that executes asynchronously and after a fixed time delay.
	 * </p>
	 * A scheduled task is a task is executed repeatedly until is called to stop. It
	 * has a specific fixed time duration between each executions, and a fixed time
	 * before the initial execution.
	 * 
	 * @param task
	 *               the task object, which will have it's {@link Runnable#run()}
	 *               invoked for execution.
	 * @param delay
	 *               the delay time, which is the time that a task waits before
	 *               performing the initial execution.
	 * @param period
	 *               the period time, which is the time that a task waits before the
	 *               next execution after the initial execution.
	 * @return the future of the task as {@link TaskFuture} object.
	 */
	F scheduleAsyncDelayed(T task, int delay, int period);

	/**
	 * Cancels the specified {@code task}.
	 * </p>
	 * When the task is cancelled, it will no longer be executed or recognised by
	 * this scheduler object.
	 * 
	 * @param task
	 *             the task to cancel.
	 * @see #cancel(int)
	 */
	default void cancel(T task) {
		cancel(task.getId());
	}

	/**
	 * Cancels the task with the specified {@code id}.
	 * </p>
	 * When the task is cancelled, it will no longer be executed or recognised by
	 * this scheduler object.
	 * 
	 * @param id
	 *           the task id to cancel.
	 */
	void cancel(int id);

	/**
	 * Cancels all the tasks that are within this scheduler. This includes both of
	 * the running and the pending task sets.
	 * 
	 * @see #cancel(Task)
	 */
	void cancelAll();

	/**
	 * Creates a new {@link TaskRunnable} object with the specified {@link Runnable}
	 * instance.
	 * </p>
	 * The created {@link TaskRunnable} object will have not be initialised or
	 * attached to a {@link TaskRunnable} object.
	 * 
	 * @param runnable
	 *                 the task task object for the execution.
	 * @return the created {@link TaskRunnable} object.
	 */
	T createTask(Runnable runnable);

	/**
	 * Gets an {@link Iterable} object of both the pending and the running tasks.
	 * </p>
	 * The returned {@link Collection} is not modifiable, if you try to modify it,
	 * an exception will be thrown.
	 * 
	 * @return the {@link Iterable} object.
	 */
	default Collection<T> getAllTasks() {
		var collection = new ArrayList<T>(size());
		collection.addAll(getRunningTasks());
		collection.addAll(getPendingTasks());
		return Collections.unmodifiableCollection(collection);
	}

	/**
	 * Gets an {@link Iterable} instance of the running tasks.
	 * </p>
	 * The returned {@link Collection} is not modifiable, if you try to modify it,
	 * an exception will be thrown.
	 * 
	 * @return the {@link Iterable} instance.
	 */
	Collection<T> getRunningTasks();

	/**
	 * Gets an {@link Iterable} instance of the pending tasks.
	 * </p>
	 * The returned {@link Collection} is not modifiable, if you try to modify it,
	 * an exception will be thrown.
	 * 
	 * @return the {@link Iterable} instance.
	 */
	Collection<T> getPendingTasks();

	/**
	 * Gets the amount of tasks that are either running or pending in the scheduler.
	 * 
	 * @return the amount of tasks.
	 */
	int size();
}
