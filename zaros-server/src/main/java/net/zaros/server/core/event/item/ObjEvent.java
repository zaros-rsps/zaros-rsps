package net.zaros.server.core.event.item;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.zaros.server.core.event.CancellableEvent;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;

/**
 * Represents the object interaction event.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class ObjEvent extends CancellableEvent {

	/**
	 * The player who done the interaction.
	 */
	@Getter
	private final Player player;

	/**
	 * The item we interacted with.
	 */
	@Getter
	private final Item item;

	/**
	 * The slot which the item is at.
	 */
	@Getter
	private final int slot;

	/**
	 * The option that was used for the interaction.
	 */
	@Getter
	private final ObjOption option;
}
