package net.zaros.server.core.event;

import java.lang.annotation.Annotation;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import net.zaros.server.core.event.item.ObjEvent;
import net.zaros.server.core.event.item.ObjListener;
import net.zaros.server.core.event.loc.LocEvent;
import net.zaros.server.core.event.loc.LocListener;
import net.zaros.server.core.event.npc.NpcEvent;
import net.zaros.server.core.event.npc.NpcListener;
import net.zaros.server.game.plugin.java.Plugin;

/**
 * An {@code EventManager} object is responsible for managing and handling
 * events posting or listeners registration.
 * 
 * @author Walied K. Yassen
 */
public final class EventManager {

	/**
	 * The {@link Logger} instance of the {@link EventManager} class type.
	 */
	private static final Logger logger = LoggerFactory.getLogger(EventManager.class);

	/**
	 * The general {@linkplain Event} {@linkplain EventBucket bucket} map.
	 */
	private final Map<Class<? extends Event>, EventBucket> buckets = new HashMap<>();

	/**
	 * The {@linkplain ObjEvent} {@linkplain EventBucket bucket} map.
	 */
	private final Map<Integer, EventBucket> objBuckets = new HashMap<Integer, EventBucket>();

	/**
	 * The {@linkplain NpcEvent} {@linkplain EventBucket bucket} map.
	 */
	private final Map<Integer, EventBucket> npcBuckets = new HashMap<Integer, EventBucket>();

	/**
	 * The {@linkplain LocEvent} {@linkplain EventBucket bucket} map.
	 */
	private final Map<Integer, EventBucket> locBuckets = new HashMap<Integer, EventBucket>();

	/**
	 * The method handles look-up.
	 */
	private final MethodHandles.Lookup handlesLookup = MethodHandles.lookup();

	/**
	 * The asynchronous events task executor.
	 */
	private final Executor executor;

	/**
	 * Constructs a new {@link EventManager} object instance.
	 */
	public EventManager() {
		this(Executors.newCachedThreadPool(new ThreadFactoryBuilder().setNameFormat("EventExecutor-%d").build()));
	}

	/**
	 * Constructs a new {@link EventManager} object instance.
	 *
	 * @param executor
	 *                 the asynchronous events task executor.
	 */
	public EventManager(Executor executor) {
		this.executor = executor;
	}

	/**
	 * Posts the specified {@link ObjEvent}. When the specified {@link ObjEvent} is
	 * posted, an {@link EventBucket} is grabbed based on what item does the
	 * {@link ObjEvent#getItem()} provide, and then the event gets handled using the
	 * handlers provided in {@link EventBucket#getBatched()}.
	 * </p>
	 * The return value might not be accurate all the time as some events have the
	 * {@link NpcListener#asynchronous()} property, therefore the state of
	 * cancellation might be altered at a later time than the returning time.
	 * 
	 * @param event
	 *              the {@link ObjEvent} to be posted.
	 * @return <true>true</code> if the event was cancelled otherwise
	 *         <code>false</code>.
	 * @see #post(Event)
	 */
	public boolean post(ObjEvent event) {
		return post(event, objBuckets.get(event.getItem().getId()));
	}

	/**
	 * Posts the specified {@link NpcEvent}. When the specified {@link NpcEvent} is
	 * posted, an {@link EventBucket} is grabbed based on what NPC does the
	 * {@link NpcEvent#getNpc()} provide, and then the event gets handled using the
	 * handlers provided in {@link EventBucket#getBatched()}.
	 * </p>
	 * The return value might not be accurate all the time as some events have the
	 * {@link NpcListener#asynchronous()} property, therefore the state of
	 * cancellation might be altered at a later time than the returning time.
	 * 
	 * @param event
	 *              the {@link NpcEvent} to be posted.
	 * @return <true>true</code> if the event was cancelled otherwise
	 *         <code>false</code>.
	 * @see #post(Event)
	 */
	public boolean post(NpcEvent event) {
		return post(event, npcBuckets.get(event.getNpc().getId()));
	}

	/**
	 * Posts the specified {@link LocEvent}. When the specified {@link LocEvent} is
	 * posted, an {@link EventBucket} is grabbed based on what object does the
	 * {@link LocEvent#getObject()} provide, and then the event gets handled using
	 * the handlers provided in {@link EventBucket#getBatched()}.
	 * </p>
	 * The return value might not be accurate all the time as some events have the
	 * {@link LocListener#asynchronous()} property, therefore the state of
	 * cancellation might be altered at a later time than the returning time.
	 * 
	 * @param event
	 *              the {@link LocEvent} to be posted.
	 * @return <true>true</code> if the event was cancelled otherwise
	 *         <code>false</code>.
	 * @see #post(Event)
	 */
	public boolean post(LocEvent event) {
		return post(event, locBuckets.get(event.getObject().getId()));
	}

	/**
	 * Posts the specified {@link Event}. When the specified {@link Event} is
	 * posted, an {@link EventBucket} is grabbed based on {@link Event} class type,
	 * and then the event gets handled using the handlers provided in
	 * {@link EventBucket#getBatched()}.
	 * </p>
	 * The return value might not be accurate all the time as some events have the
	 * {@link EventListener#asynchronous()} property, therefore the state of
	 * cancellation might be altered at a later time than the returning time.
	 * 
	 * @param event
	 *              the {@link Event} to be posted.
	 * @return <true>true</code> if the event was cancelled otherwise
	 *         <code>false</code>.
	 * @see #post(Event)
	 */
	public boolean post(Event event) {
		return post(event, buckets.get(event.getClass()));
	}

	/**
	 * Posts the specified {@link Event} to all of the {@link EventHandler} objects
	 * that are found in the specified {@link EventBucket}.
	 * </p>
	 * The return value might not be accurate all the time as some events have the
	 * {@link EventListener#asynchronous()} property, therefore the state of
	 * cancellation might be altered at a later time than the returning time.
	 * 
	 * @param event
	 *               the event which will be handled and executed.
	 * @param bucket
	 *               the bucket which we will be grabbing the handlers from.
	 * @return <true>true</code> if the event was cancelled otherwise
	 *         <code>false</code>.
	 */
	private boolean post(Event event, EventBucket bucket) {
		// fail-fast: check whether there is a bucket for our event type or not.
		if (bucket == null) {
			return false;
		}
		// gets the batched EventHandlers array.
		EventHandler[] handlers = bucket.getBatched();
		// iterate through each handler and handle the event.
		for (EventHandler handler : handlers) {
			handler.handle(executor, event);
		}
		// return whether the event is cancelled or not.
		return event.isCancelled();
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it takes them and
	 * registers them in their corresponding {@link EventBucket}.
	 *
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to register.
	 * @see #register(EventIdentifier, Object)
	 */
	public void register(Object eventsOwner) {
		register((EventIdentifier<?>) null, eventsOwner);
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it takes them and
	 * registers them in their corresponding {@link EventBucket}.
	 * </p>
	 * The registered methods will have {@link Plugin} identifier, which can be used
	 * later to unregister the methods upon plugin disabling.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to register.
	 * @see #register(EventIdentifier, Object)
	 * @see Plugin#getEventsKey()
	 */
	public void register(Plugin plugin, Object eventsOwner) {
		register(plugin.getEventsKey(), eventsOwner);
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it takes them and
	 * registers them in their corresponding {@link EventBucket}.
	 * </p>
	 * The registered methods will have the provided {@link EventIdentifier
	 * identifier}, which can be used later to unregister the methods upon plugin
	 * disabling.
	 * </p>
	 * This method will register all kind of events, whether it is an
	 * {@link NpcListener}, {@link LocListener}, or an {@link EventListener}.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to register.
	 * @see #register(EventIdentifier, Object)
	 */
	public void register(EventIdentifier<?> identifier, Object eventsOwner) {
		registerGeneralEvents(identifier, eventsOwner);
		registerNpcEvents(identifier, eventsOwner);
		registerLocEvents(identifier, eventsOwner);
		registerObjEvents(identifier, eventsOwner);
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it takes them and
	 * registers them in their corresponding {@link EventBucket}.
	 * </p>
	 * The registered methods will have the provided {@link EventIdentifier
	 * identifier}, which can be used later to unregister the methods upon plugin
	 * disabling.
	 * </p>
	 * This method will only register one kind of events which is
	 * {@link EventListener}.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to register.
	 * @see #register(EventIdentifier, Object)
	 */
	private void registerGeneralEvents(EventIdentifier<?> identifier, Object eventsOwner) {
		// grab the event methods from the given owner.
		Method[] methods = getEventMethods(eventsOwner);
		// iterate throughout each event method.
		for (Method method : methods) {
			// we use try-catch block so we don't others top the methods from
			// being registered.
			try {
				// grab the listener annotation.
				EventListener listener = method.getAnnotation(EventListener.class);
				// grab the event's class type.
				Class<? extends Event> eventClass = method.getParameterTypes()[0].asSubclass(Event.class);
				// check whether the method is accessible or not.
				if (!method.isAccessible()) {
					method.setAccessible(true);
				}
				// create the method handle.
				MethodHandle handle = handlesLookup.unreflect(method).bindTo(eventsOwner);
				// create the events handler.
				EventHandler eventHandler = new EventHandler(identifier, method, handle, listener.priority(), listener.asynchronous(), listener.respectCancellation());
				// grab the event's bucket.
				EventBucket bucket = buckets.get(eventClass);
				// create the bucket if it was not present.
				if (bucket == null) {
					buckets.put(eventClass, bucket = new EventBucket());
				}
				// register the handler in the bucket.
				bucket.register(eventHandler);
			} catch (Throwable e) {
				// throw an exception to the logger if anything has occurred.
				logger.error("An error occurred while registering general events, owner: {}", eventsOwner, e);
			}
		}
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it takes them and
	 * registers them in their corresponding {@link EventBucket}.
	 * </p>
	 * The registered methods will have the provided {@link EventIdentifier
	 * identifier}, which can be used later to unregister the methods upon plugin
	 * disabling.
	 * </p>
	 * This method will only register one kind of events which is
	 * {@link NpcListener}.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to register.
	 * @see #register(EventIdentifier, Object)
	 */
	private void registerNpcEvents(EventIdentifier<?> identifier, Object eventsOwner) {
		// grab the event methods from the given owner.
		Method[] methods = getNpcMethods(eventsOwner);
		// iterate throughout each event method.
		for (Method method : methods) {
			// encapsulate within an try-catch block, so we don't stop the methods from
			// being registered.
			try {
				// grab the listener annotation.
				NpcListener listener = method.getAnnotation(NpcListener.class);
				// check whether the method is accessible or not.
				if (!method.isAccessible()) {
					method.setAccessible(true);
				}
				// create the method handle.
				MethodHandle handle = handlesLookup.unreflect(method).bindTo(eventsOwner);
				// create the events handler.
				EventHandler eventHandler = new EventHandler(identifier, method, handle, listener.priority(), listener.asynchronous(), listener.respectCancellation());
				for (int npcid : listener.ids()) {
					// grab the event's bucket.
					EventBucket bucket = npcBuckets.get(npcid);
					// create the bucket if it was not present.
					if (bucket == null) {
						npcBuckets.put(npcid, bucket = new EventBucket());
					}
					// register the handler in the bucket.
					bucket.register(eventHandler);
				}
			} catch (Throwable e) {
				// throw an exception to the logger if anything has occurred.
				logger.error("An error occurred while registering npc events, owner: {}", eventsOwner, e);
			}
		}
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it takes them and
	 * registers them in their corresponding {@link EventBucket}.
	 * </p>
	 * The registered methods will have the provided {@link EventIdentifier
	 * identifier}, which can be used later to unregister the methods upon plugin
	 * disabling.
	 * </p>
	 * This method will only register one kind of events which is
	 * {@link LocListener}.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to register.
	 * @see #register(EventIdentifier, Object)
	 */
	private void registerLocEvents(EventIdentifier<?> identifier, Object eventsOwner) {
		// grab the event methods from the given owner.
		Method[] methods = getLocMethods(eventsOwner);
		// iterate throughout each event method.
		for (Method method : methods) {
			// encapsulate within an try-catch block, so we don't stop the methods from
			// being registered.
			try {
				// grab the listener annotation.
				LocListener listener = method.getAnnotation(LocListener.class);
				// check whether the method is accessible or not.
				if (!method.isAccessible()) {
					method.setAccessible(true);
				}
				// create the method handle.
				MethodHandle handle = handlesLookup.unreflect(method).bindTo(eventsOwner);
				// create the events handler.
				EventHandler eventHandler = new EventHandler(identifier, method, handle, listener.priority(), listener.asynchronous(), listener.respectCancellation());
				for (int locid : listener.ids()) {
					// grab the event's bucket.
					EventBucket bucket = locBuckets.get(locid);
					// create the bucket if it was not present.
					if (bucket == null) {
						locBuckets.put(locid, bucket = new EventBucket());
					}
					// register the handler in the bucket.
					bucket.register(eventHandler);
				}
			} catch (Throwable e) {
				// throw an exception to the logger if anything has occurred.
				logger.error("An error occurred while registering location events, owner: {}", eventsOwner, e);
			}
		}
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it takes them and
	 * registers them in their corresponding {@link EventBucket}.
	 * </p>
	 * The registered methods will have the provided {@link EventIdentifier
	 * identifier}, which can be used later to unregister the methods upon plugin
	 * disabling.
	 * </p>
	 * This method will only register one kind of events which is
	 * {@link ObjListener}.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to register.
	 * @see #register(EventIdentifier, Object)
	 */
	private void registerObjEvents(EventIdentifier<?> identifier, Object eventsOwner) {
		// grab the event methods from the given owner.
		Method[] methods = getObjMethods(eventsOwner);
		// iterate throughout each event method.
		for (Method method : methods) {
			// encapsulate within an try-catch block, so we don't stop the methods from
			// being registered.
			try {
				// grab the listener annotation.
				ObjListener listener = method.getAnnotation(ObjListener.class);
				// check whether the method is accessible or not.
				if (!method.isAccessible()) {
					method.setAccessible(true);
				}
				// create the method handle.
				MethodHandle handle = handlesLookup.unreflect(method).bindTo(eventsOwner);
				// create the events handler.
				EventHandler eventHandler = new EventHandler(identifier, method, handle, listener.priority(), listener.asynchronous(), listener.respectCancellation());
				for (int locid : listener.ids()) {
					// grab the event's bucket.
					EventBucket bucket = objBuckets.get(locid);
					// create the bucket if it was not present.
					if (bucket == null) {
						objBuckets.put(locid, bucket = new EventBucket());
					}
					// register the handler in the bucket.
					bucket.register(eventHandler);
				}
			} catch (Throwable e) {
				// throw an exception to the logger if anything has occurred.
				logger.error("An error occurred while registering location events, owner: {}", eventsOwner, e);
			}
		}
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it unregisters them
	 * from their corresponding {@link EventBucket}.
	 * </p>
	 * This method will unregister all kind of events, whether it is an
	 * {@link NpcListener}, {@link LocListener}, or an {@link EventListener}.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to unregister.
	 * @see #register(EventIdentifier, Object)
	 */
	public void unregister(Object eventsOwner) {
		unregisterGeneralEvents(eventsOwner);
		unregisterNpcEvents(eventsOwner);
		unregisterLocEvents(eventsOwner);
		unregisterObjEvents(eventsOwner);
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it unregisters them
	 * from their corresponding {@link EventBucket}.
	 * </p>
	 * This method will only unregister one kind of events which is
	 * {@link EventListener}.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to unregister.
	 * @see #register(EventIdentifier, Object)
	 */
	private void unregisterGeneralEvents(Object eventsOwner) {
		// grab the event methods from the given owner.
		Method[] methods = getEventMethods(eventsOwner);
		// iterate throughout each event method.
		for (Method method : methods) {
			// grab the event's class type.
			Class<? extends Event> eventClass = method.getParameterTypes()[0].asSubclass(Event.class);
			// grab the event's bucket.
			EventBucket bucket = buckets.get(eventClass);
			// create the bucket if it was not present.
			if (bucket == null) {
				// most likely not to happen to a single method, but oh well.
				continue;
			}
			for (EventHandler handler : bucket.getBatched()) {
				if (method.equals(handler.getMethod())) {
					bucket.unregister(handler);
					break;
				}
			}
		}
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it unregisters them
	 * from their corresponding {@link EventBucket}.
	 * </p>
	 * This method will only unregister one kind of events which is
	 * {@link NpcListener}.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to unregister.
	 * @see #register(EventIdentifier, Object)
	 */
	private void unregisterObjEvents(Object eventsOwner) {
		// grab the event methods from the given owner.
		Method[] methods = getObjMethods(eventsOwner);
		// iterate throughout each event method.
		for (Method method : methods) {
			// grab the listener annotation.
			ObjListener listener = method.getAnnotation(ObjListener.class);
			// grab the event's bucket.
			for (int npcid : listener.ids()) {
				// grab the event bucket associated with the NPC.
				EventBucket bucket = objBuckets.get(npcid);
				// check whether the bucket is present or not.
				if (bucket == null) {
					continue;
				}
				// iterate through all of the events and unregister the one that matches.
				for (EventHandler handler : bucket.getBatched()) {
					if (method.equals(handler.getMethod())) {
						bucket.unregister(handler);
						break;
					}
				}
			}
		}
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it unregisters them
	 * from their corresponding {@link EventBucket}.
	 * </p>
	 * This method will only unregister one kind of events which is
	 * {@link NpcListener}.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to unregister.
	 * @see #register(EventIdentifier, Object)
	 */
	private void unregisterNpcEvents(Object eventsOwner) {
		// grab the event methods from the given owner.
		Method[] methods = getNpcMethods(eventsOwner);
		// iterate throughout each event method.
		for (Method method : methods) {
			// grab the listener annotation.
			NpcListener listener = method.getAnnotation(NpcListener.class);
			// grab the event's bucket.
			for (int npcid : listener.ids()) {
				// grab the event bucket associated with the NPC.
				EventBucket bucket = npcBuckets.get(npcid);
				// check whether the bucket is present or not.
				if (bucket == null) {
					continue;
				}
				// iterate through all of the events and unregister the one that matches.
				for (EventHandler handler : bucket.getBatched()) {
					if (method.equals(handler.getMethod())) {
						bucket.unregister(handler);
						break;
					}
				}
			}
		}
	}

	/**
	 * Looks for all the methods that are within the events owner object and filter
	 * which ones can are valid event listener methods. Then it unregisters them
	 * from their corresponding {@link EventBucket}.
	 * </p>
	 * This method will only unregister one kind of events which is
	 * {@link LocListener}.
	 * 
	 * @param eventsOwner
	 *                    the events owner object, which contains all the listener
	 *                    methods that we are going to unregister.
	 * @see #register(EventIdentifier, Object)
	 */
	private void unregisterLocEvents(Object eventsOwner) {
		// grab the event methods from the given owner.
		Method[] methods = getLocMethods(eventsOwner);
		// iterate throughout each event method.
		for (Method method : methods) {
			// grab the listener annotation.
			LocListener listener = method.getAnnotation(LocListener.class);
			// grab the event's bucket.
			for (int locid : listener.ids()) {
				// grab the event bucket associated with the NPC.
				EventBucket bucket = locBuckets.get(locid);
				// check whether the bucket is present or not.
				if (bucket == null) {
					continue;
				}
				// iterate through all of the events and unregister the one that matches.
				for (EventHandler handler : bucket.getBatched()) {
					if (method.equals(handler.getMethod())) {
						bucket.unregister(handler);
						break;
					}
				}
			}
		}
	}

	/**
	 * Unregister the {@link EventHandler}s which were registered by the specified
	 * {@linkplain Plugin plugin}.
	 *
	 * @param plugin
	 *               the plugin which we will unregister all of its' events.
	 */
	public void unregister(Plugin plugin) {
		unregister(plugin.getEventsKey());
	}

	/**
	 * Unregister the {@link EventHandler}s which has the same identifier as the
	 * specified one.
	 *
	 * @param eventsIdentifier
	 *                         the events identifier which events that matches it,
	 *                         will be unregistered.
	 */
	public void unregister(EventIdentifier<?> eventsIdentifier) {
		unregister(buckets.values(), eventsIdentifier);
		unregister(objBuckets.values(), eventsIdentifier);
		unregister(npcBuckets.values(), eventsIdentifier);
		unregister(locBuckets.values(), eventsIdentifier);
	}

	// -----------------------------------------------------------------------------

	/**
	 * @param buckets
	 * @param eventsIdentifier
	 */
	private void unregister(Collection<EventBucket> buckets, EventIdentifier<?> eventsIdentifier) {
		// iterate through all the handler buckets.
		for (EventBucket bucket : buckets) {
			// iterate through each handler and remove the ones we want.
			for (EventHandler handler : bucket.getBatched()) {
				// check whether the handler identifier matches our identifier or not.
				if (handler.getIdentifier() != null && handler.getIdentifier().equals(eventsIdentifier)) {
					// unregister the handler.
					bucket.unregister(handler);
				}
			}
		}
	}

	/**
	 * Unregister all of the registered handlers from this manager.
	 */
	public void unregisterAll() {
		buckets.values().forEach(EventBucket::clear);
		objBuckets.values().forEach(EventBucket::clear);
		npcBuckets.values().forEach(EventBucket::clear);
		locBuckets.values().forEach(EventBucket::clear);
	}

	/**
	 * Gets the {@link Method}s list that are valid to be registered as listeners
	 * from the specified events owner.
	 *
	 * @param eventsOwner
	 *                    the events owner which contains all the methods to be
	 *                    picked from.
	 * @return the {@link Method}s list.
	 */
	private final Method[] getEventMethods(Object eventsOwner) {
		return lookupMethods(eventsOwner, EventListener.class, Event.class);
	}

	/**
	 * Gets the {@link Method}s list that are valid to be registered as NPC
	 * listeners from the specified events owner.
	 *
	 * @param eventsOwner
	 *                    the events owner which contains all the methods to be
	 *                    picked from.
	 * @return the {@link Method}s list.
	 */
	private final Method[] getObjMethods(Object eventsOwner) {
		return lookupMethods(eventsOwner, ObjListener.class, ObjEvent.class);
	}

	/**
	 * Gets the {@link Method}s list that are valid to be registered as NPC
	 * listeners from the specified events owner.
	 *
	 * @param eventsOwner
	 *                    the events owner which contains all the methods to be
	 *                    picked from.
	 * @return the {@link Method}s list.
	 */
	private final Method[] getNpcMethods(Object eventsOwner) {
		return lookupMethods(eventsOwner, NpcListener.class, NpcEvent.class);
	}

	/**
	 * Gets the {@link Method}s list that are valid to be registered as NPC
	 * listeners from the specified events owner.
	 *
	 * @param eventsOwner
	 *                    the events owner which contains all the methods to be
	 *                    picked from.
	 * @return the {@link Method}s list.
	 */
	private final Method[] getLocMethods(Object eventsOwner) {
		return lookupMethods(eventsOwner, LocListener.class, LocEvent.class);
	}

	private static Method[] lookupMethods(Object eventsOwner, Class<? extends Annotation> annotationType, Class<? extends Event> eventType) {
		// a list of the methods which passed the filter to be returned later.
		List<Method> methods = new ArrayList<>();
		// iterate throughout all the methods and filter which ones we want.
		for (Method method : eventsOwner.getClass().getDeclaredMethods()) {
			// ignore if the method is either an abstract or a static method.
			if (Modifier.isStatic(method.getModifiers()) || Modifier.isAbstract(method.getModifiers())) {
				continue;
			}
			// ignore if there was no EventListener annotation present.
			if (!method.isAnnotationPresent(annotationType)) {
				continue;
			}
			// throw an error if the method signature is invalid.
			if (method.getParameterTypes().length != 1 || !eventType.isAssignableFrom(method.getParameterTypes()[0])) {
				throw new EventException("The method signature must only contain one paramter of type " + eventType.getSimpleName() + ", method: " + method);
			}
			// add the method to the valid methods list.
			methods.add(method);
		}
		// return the methods list as an array.
		return methods.toArray(new Method[methods.size()]);
	}
}
