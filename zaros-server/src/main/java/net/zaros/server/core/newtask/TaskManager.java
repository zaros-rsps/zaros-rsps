package net.zaros.server.core.newtask;

import lombok.Getter;
import net.zaros.server.core.newtask.scheduler.tick.TickScheduler;

/**
 * Represents the task manager for our server.
 * 
 * @author Walied K. Yassen
 */
public final class TaskManager {

	/**
	 * The tick scheduler.
	 */
	@Getter
	private static final TickScheduler tickScheduler = new TickScheduler();

	private TaskManager() {
		// NOOP
	}
}
