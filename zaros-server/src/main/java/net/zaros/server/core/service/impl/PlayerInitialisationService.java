package net.zaros.server.core.service.impl;

import net.zaros.server.core.service.QueuedService;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Walied K. Yassen
 */
public final class PlayerInitialisationService extends QueuedService<Player> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.service.QueuedService#serve(java.lang.Object)
	 */
	@Override
	public void serve(Player entry) {
		try {
			Thread.sleep(50L);
		} catch (InterruptedException e) {
			// NOOP
		}
		entry.initialise();
	}
}
