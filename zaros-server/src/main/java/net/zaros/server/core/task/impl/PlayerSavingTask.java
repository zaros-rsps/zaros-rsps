package net.zaros.server.core.task.impl;

import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/3/2017
 */
public class PlayerSavingTask extends ScheduledTask {
	
	public PlayerSavingTask() {
		super(1000, -1);
	}
	
	@Override
	public void run() {
		for (Player player : World.getPlayers()) {
			if (player == null) {
				continue;
			}
			player.save();
		}
	}
}
