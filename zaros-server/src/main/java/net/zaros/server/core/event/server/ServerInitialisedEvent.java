package net.zaros.server.core.event.server;

import net.zaros.server.core.event.Event;

/**
 * Represents an {@link Event} that occurs when the server has just finished
 * initialising and bootstrapping.
 * 
 * @author Walied K. Yassen
 */
public final class ServerInitialisedEvent extends Event {
	// NOOP
}
