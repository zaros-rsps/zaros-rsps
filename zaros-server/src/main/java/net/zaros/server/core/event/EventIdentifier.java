package net.zaros.server.core.event;

import com.google.common.base.Preconditions;

/**
 * An event identifier is an object that can be solely used to identify events
 * registration source.
 * 
 * @author Walied K. Yassen
 */
public final class EventIdentifier<T> {

	/**
	 * The events identifier object.
	 */
	private final T identifier;

	/**
	 * Constructs a new {@link EventIdentifier} object instance.
	 *
	 * @param identifier
	 *                       the events identifier object.
	 */
	public EventIdentifier(T identifier) {
		this.identifier = Preconditions.checkNotNull(identifier, "The identifier cannot be null");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (identifier == null ? 0 : identifier.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EventIdentifier<?> other = (EventIdentifier<?>) obj;
		if (identifier == null) {
			if (other.identifier != null) {
				return false;
			}
		} else if (!identifier.equals(other.identifier)) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the events identifier object.
	 *
	 * @return the events identifier object.
	 */
	public T getIdentifier() {
		return identifier;
	}
}
