package net.zaros.server.core.newtask.future;

import net.zaros.server.core.newtask.Task;

/**
 * Represents a task future, a task future is what controls the task future,
 * such as cancelling the task, or getting the state of the execution and
 * whatsoever.
 * 
 * @author Walied K. Yassen
 */
public interface TaskFuture<T extends Task> {

	/**
	 * Cancels the task which this future is for. When the task is cancelled, it
	 * will be no longer queued for execution and will not be recognised by the
	 * implementation scheduler.
	 */
	void cancel();

	/**
	 * Adds the specified {@link TaskFutureListener} to this future.
	 * 
	 * @param listener
	 *                 the task future listener to add.
	 */
	void addListener(TaskFutureListener listener);

	/**
	 * Removes the specified {@link TaskFutureListener} from this future.
	 * 
	 * @param listener
	 *                 the listener to remove.
	 */
	void removeListener(TaskFutureListener listener);

	/**
	 * Checks whether or not the task of this future has finished execution yet or
	 * not.
	 * 
	 * @return <code>true</code> if it did otherwise <code>false</code>.
	 */
	boolean isDone();
}
