package net.zaros.server.core.event.npc;

/**
 * Represents an NPC (Non-Player-Character) context menu option.
 * 
 * @author Walied K. Yassen
 */
public enum NpcOption {
	/**
	 * The first NPC option.
	 */
	OPTION1,

	/**
	 * The second NPC option.
	 */
	OPTION2,

	/**
	 * The third NPC option.
	 */
	OPTION3,

	/**
	 * The fourth NPC option.
	 */
	OPTION4,

	/**
	 * The fifth NPC option.
	 */
	OPTION5,
}
