package net.zaros.server.core.newtask.scheduler.tick;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.zaros.server.core.newtask.future.TaskFuture;
import net.zaros.server.core.newtask.future.TaskFutureListener;

/**
 * Represents a {@link TaskFuture} for the {@link TickTask} type.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class TickFuture implements TaskFuture<TickTask> {

	/**
	 * The owner scheduler.
	 */
	@Getter
	private final TickScheduler scheduler;

	/**
	 * the tick future task.
	 */
	@Getter
	private final TickTask task;

	/**
	 * The future listeners.
	 */
	private List<TaskFutureListener> listeners;

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.newtask.TaskFuture#cancel()
	 */
	@Override
	public void cancel() {
		if (task.isDead()) {
			throw new IllegalStateException("Cannot cancel a dead task.");
		}
		scheduler.cancel(task);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.future.TaskFuture#addListener(net.zaros.server.
	 * core.newtask.future.TaskFutureListener)
	 */
	@Override
	public void addListener(TaskFutureListener listener) {
		if (listeners == null) {
			listeners = new ArrayList<TaskFutureListener>();
		}
		listeners.add(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.future.TaskFuture#removeListener(net.zaros.
	 * server.core.newtask.future.TaskFutureListener)
	 */
	@Override
	public void removeListener(TaskFutureListener listener) {
		if (listeners == null) {
			return;
		}
		listeners.remove(listener);
		if (listeners.size() < 1) {
			listeners = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.newtask.TaskFuture#isDone()
	 */
	@Override
	public boolean isDone() {
		return task.isDead();
	}
}
