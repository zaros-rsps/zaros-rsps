package net.zaros.server.core.event.loc;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.zaros.server.core.event.CancellableEvent;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.object.GameObject;

/**
 * Represents the location interaction event.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class LocEvent extends CancellableEvent {

	/**
	 * The player who done the interaction.
	 */
	@Getter
	private final Player player;

	/**
	 * The game object which we interacted with.
	 */
	@Getter
	private final GameObject object;

	/**
	 * The item option that was used in the interaction.
	 */
	@Getter
	private final LocOption option;
}
