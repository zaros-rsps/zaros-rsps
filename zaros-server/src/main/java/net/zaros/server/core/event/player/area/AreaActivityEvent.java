package net.zaros.server.core.event.player.area;

import lombok.Getter;
import net.zaros.server.core.event.player.PlayerEvent;
import net.zaros.server.core.event.player.PlayerLoginEventPre;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;

/**
 * Represents an {@link Event} that gets fired when the player is being checked
 * for area activity change.
 * 
 * @author Walied K. Yassen
 */
public class AreaActivityEvent extends PlayerEvent {

	/**
	 * The new area {@link Location} object.
	 */
	@Getter
	private final Location location;

	/**
	 * Construct a new {@link PlayerLoginEventPre} type object instance.
	 * 
	 * @param player   the event cause player.
	 * @param location the new area location.
	 */
	public AreaActivityEvent(Player player, Location location) {
		super(player);
		this.location = location;
	}

}
