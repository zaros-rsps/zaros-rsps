package net.zaros.server.core.service;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;

import lombok.Getter;

/**
 * Represents a queued service. By queued we mean the entries are put into a
 * {@link Deque} instance, and will be taken by the insertion order to be
 * processed on a separate thread.
 * 
 * @author Walied K. Yassen
 */
public abstract class QueuedService<T> implements Service<T>, Runnable {

	/**
	 * The currently pending entries.
	 */
	private final Queue<T> entries;

	/**
	 * Whether the service is currently running or not.
	 */
	@Getter
	private volatile boolean running;

	/**
	 * Constructs a new {@link QueuedService} type object instance.
	 */
	public QueuedService() {
		this(new LinkedList<T>());
	}

	/**
	 * Constructs a new {@link QueuedService} type object instance.
	 * 
	 * @param entries
	 *                the entries queue.
	 */
	public QueuedService(Queue<T> entries) {
		this.entries = entries;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.service.Service#start(java.util.concurrent.
	 * ExecutorService)
	 */
	@Override
	public void start(ExecutorService executor) {
		if (running) {
			throw new IllegalStateException("The service has alreayd been started..");
		}
		running = true;
		executor.execute(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while (running) {
			T entry = dequeue();
			try {
				if (entry != null) {
					serve(entry);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.service.Service#serve(java.lang.Object)
	 */
	@Override
	public abstract void serve(T entry);

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.service.Service#shutdown()
	 */
	@Override
	public void shutdown() {
		if (!running) {
			throw new IllegalStateException("The service is not currently running..");
		}
		running = false;
	}

	/**
	 * Enqueues a new entry to this service.
	 * 
	 * @param entry
	 *              the entry to enqueue.
	 */
	public void enqueue(T entry) {
		synchronized (entries) {
			boolean idle = entries.isEmpty();
			entries.add(entry);
			if (idle) {
				entries.notifyAll();
			}
		}
	}

	/**
	 * Dequeues the next entry from the queue.
	 * 
	 * @return the dequeued entry.
	 */
	public T dequeue() {
		T entry;
		synchronized (entries) {
			while ((entry = entries.poll()) == null) {
				try {
					entries.wait();
				} catch (InterruptedException e) {
					// NOOP
				}
			}
		}
		return entry;
	}
}
