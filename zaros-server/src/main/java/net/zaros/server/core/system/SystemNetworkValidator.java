package net.zaros.server.core.system;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.network.NetworkSession;
import net.zaros.server.network.world.WorldSession;

/**
 * This class handles the validation of all connected sessions to our network.
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/17/2017
 */
@Slf4j
public class SystemNetworkValidator implements Runnable {
	
	@Override
	public void run() {
		try {
			for (Player player : World.getPlayers()) {
				if (player == null) {
					continue;
				}
				checkSessionLegibility(player.getSession());
			}
			for (NetworkSession session : NetworkSession.getAllSessions()) {
				if (!(session instanceof WorldSession)) {
					continue;
				}
				checkSessionLegibility((WorldSession) session);
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	
	/**
	 * Check to see that a session is legible to exist still
	 *
	 * @param session
	 * 		The session
	 */
	private static void checkSessionLegibility(WorldSession session) {
		Channel channel = session.getChannel();
		boolean disconnect = false;
		// verify the channel is good
		if (!channel.isOpen() || !channel.isWritable() || !channel.isRegistered() || !channel.isActive()) {
			log.info("Invalid channel connected:\t" + channel);
			disconnect = true;
		}
		// if they haven't set a player and were existent for a while
		if (session.getPlayer() == null && session.getElapsedCreationTime() >= 5_000) {
			log.info("Session without player attached for too long:\t" + session);
			disconnect = true;
		}
		if (disconnect) {
			log.info("Disconnected session " + session);
			channel.disconnect();
		}
	}
}
