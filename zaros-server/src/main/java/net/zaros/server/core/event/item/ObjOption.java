package net.zaros.server.core.event.item;

/**
 * Represents an object interaction option.
 * 
 * @author Walied K. Yassen
 */
public enum ObjOption {
	FIRST,
	SECOND,
	THIRD,
	FOURTH,
	FIFTH,
	EXAMINE,
}
