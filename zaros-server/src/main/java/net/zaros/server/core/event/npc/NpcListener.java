package net.zaros.server.core.event.npc;

import net.zaros.server.core.event.Event;
import net.zaros.server.core.event.EventException;
import net.zaros.server.core.event.EventManager;
import net.zaros.server.core.event.EventPriority;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation which can be used to annotate a method to tell whether the
 * method is an {@link Event} listener method or not. When a method is annotated
 * with this annotation, it makes it visible for the {@link EventManager}
 * methods such as {@link EventManager#register(Object)} and
 * {@link EventManager#unregister(Object)} methods.
 * <p>
 * Methods which are annotated with this annotation must always meet the same
 * signature rules which are:
 * <ul>
 * <li>Must have a return type of <code>void</code>.</li>
 * <li>Must have only one parameter and the parameter type must be
 * {@link NpcEvent}.</li>
 * <li>Must be a dynamic method, static methods aren't allowed.</li>
 * </ul>
 * <br>
 * Any violation to these rules will result in an {@link EventException} being
 * thrown upon using the {@link EventManager#register(Object)} method.
 *
 * @author Walied K. Yassen
 * @since 0.1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface NpcListener {

	/**
	 * Gets the target NPC id which we will bind this listener for.
	 * 
	 * @return the NPC id.
	 */
	int[] ids();

	/**
	 * Gets the listener priority, the priority controls what the invocation order
	 * of a listener is, the highest the priority is, the more important to the
	 * invoker it becomes, the earlier executed it gets.
	 *
	 * @return the {@link EventPriority} of this listener.
	 * @see EventPriority
	 */
	EventPriority priority() default EventPriority.NORMAL;

	/**
	 * Tells whether this listener should run asynchronously or synchronously.
	 *
	 * @return <code>true</code> if the listener should run asynchronously or
	 *         <code>false</code> if the listener should run synchronously.
	 */
	boolean asynchronous() default false;

	/**
	 * Tells whether this listener should respect the {@link Event#isCancelled()}
	 * rule or not. When the listener is respecting the cancellation rule and the
	 * event is cancelled, then the listener will not be invoked, otherwise it will
	 * just be ignored.
	 *
	 * @return <code>true</code> if the listener respects the
	 *         {@link Event#isCancelled()} rule otherwise <code>false</code>.
	 * @see Event#isCancelled()
	 */
	boolean respectCancellation() default true;
}
