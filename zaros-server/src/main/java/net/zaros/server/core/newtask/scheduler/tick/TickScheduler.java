package net.zaros.server.core.newtask.scheduler.tick;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import net.zaros.server.core.newtask.scheduler.AbstractScheduler;

/**
 * Represents the scheduler that uses a world tick as the base time unit.
 * 
 * @author Walied K. Yassen
 */
public final class TickScheduler extends AbstractScheduler<TickTask, TickFuture> {

	/**
	 * The default delay if there was none provided.
	 */
	private static final int DEFAULT_DELAY = 1;

	/**
	 * The default period if there was none provided.
	 */
	private static final int DEFAULT_PERIOD = 1;

	/***
	 * The active tasks map.
	 */
	private final Map<Integer, TickTask> activeTasks = new ConcurrentHashMap<Integer, TickTask>();

	/**
	 * The asynchronous tick scheduler.
	 */
	private final Executor executor = Executors.newCachedThreadPool(new ThreadFactoryBuilder().setNameFormat("tick-scheduler-#%d").setDaemon(true).build());

	/**
	 * The pending tasks list.
	 */
	private final Queue<TickTask> running = new PriorityQueue<TickTask>(1024);

	/**
	 * The pending tasks list.
	 */
	private final Queue<TickTask> pending = new PriorityQueue<TickTask>(1024);

	/**
	 * The current world tick.
	 */
	private int currentTick;

	/**
	 * The main heart beat method of this scheduler. It is responsible for
	 * processing the active tasks and pull the pending tasks.
	 */
	public void tick() {
		// keep polling until the queue is over.
		while (isReadyNext()) {
			running.add(pending.poll());
		}
		var continuousTasks = new ArrayList<TickTask>();
		while (!running.isEmpty()) {
			var task = running.poll();
			if (task.isAsynchronous()) {
				executor.execute(task);
			} else {
				task.run();
			}
			var period = task.getPeriod();
			if (period > 0) {
				task.setNextExecution(currentTick + period);
				continuousTasks.add(task);
			}
		}
		// add all the continuous tasks to the pending list again.
		pending.addAll(continuousTasks);
		// increment the ticks count.
		currentTick++;
	}

	/**
	 * Checks whether or not the next task is ready to be executed or not.
	 * 
	 * @return <code>true</code> if it is otherwise <code>false</code>.
	 */
	private boolean isReadyNext() {
		var task = pending.peek();
		if (task == null) {
			return false;
		}
		return task.getNextExecution() <= currentTick;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#run(java.lang.Runnable)
	 */
	@Override
	public TickFuture run(Runnable task) {
		return run(createTask(task));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#run(net.zaros.server.core.
	 * newtask.Task)
	 */
	@Override
	public TickFuture run(TickTask task) {
		return runDelayed(task, DEFAULT_DELAY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#runDelayed(net.zaros.server
	 * .core.newtask.Task, int)
	 */
	@Override
	public TickFuture runDelayed(TickTask task, int delay) {
		return addTask(task, delay, -1, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#runAsync(net.zaros.server.
	 * core.newtask.Task)
	 */
	@Override
	public TickFuture runAsync(TickTask task) {
		return runAsyncDelayed(task, DEFAULT_DELAY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#runAsyncDelayed(net.zaros.
	 * server.core.newtask.Task, int)
	 */
	@Override
	public TickFuture runAsyncDelayed(TickTask task, int delay) {
		return addTask(task, delay, task.getPeriod(), true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#schedule(net.zaros.server.
	 * core.newtask.Task)
	 */
	@Override
	public TickFuture schedule(TickTask task) {
		return scheduleDelayed(task, DEFAULT_DELAY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#scheduleDelayed(net.zaros.
	 * server.core.newtask.Task, int)
	 */
	@Override
	public TickFuture scheduleDelayed(TickTask task, int delay) {
		return scheduleDelayed(task, delay, DEFAULT_PERIOD);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#scheduleDelayed(net.zaros.
	 * server.core.newtask.Task, int, int)
	 */
	@Override
	public TickFuture scheduleDelayed(TickTask task, int delay, int period) {
		return addTask(task, delay, period, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#scheduleAsync(net.zaros.
	 * server.core.newtask.Task)
	 */
	@Override
	public TickFuture scheduleAsync(TickTask task) {
		return scheduleAsyncDelayed(task, DEFAULT_DELAY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#scheduleAsyncDelayed(net.
	 * zaros.server.core.newtask.Task, int)
	 */
	@Override
	public TickFuture scheduleAsyncDelayed(TickTask task, int delay) {
		return scheduleAsyncDelayed(task, delay, DEFAULT_PERIOD);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.core.newtask.scheduler.Scheduler#scheduleAsyncDelayed(net.
	 * zaros.server.core.newtask.Task, int, int)
	 */
	@Override
	public TickFuture scheduleAsyncDelayed(TickTask task, int delay, int period) {
		return addTask(task, delay, period, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.newtask.scheduler.Scheduler#cancel(int)
	 */
	@Override
	public void cancel(int id) {
		// TODO: Implement a proper cancellation. Insert a tick at the top of the queue
		// (Integer.MIN_VALUE) with cancel request.
		var task = activeTasks.get(id);
		if (task == null) {
			return;
		}
		activeTasks.remove(id);
		pending.remove(task);
		running.remove(task);
		task.setId(-1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.newtask.scheduler.Scheduler#cancelAll()
	 */
	@Override
	public void cancelAll() {
		pending.forEach(this::cancel);
		running.forEach(this::cancel);
	}

	/**
	 * Adds a new {@link TickTask} to the pending tasks list to be executed or
	 * scheduled later on.
	 * 
	 * @param task
	 *                     the task object to add to the pending list.
	 * @param delay
	 *                     the task initial execution delay time in ticks.
	 * @param period
	 *                     the task execution delay time in ticks.
	 * @param asynchronous
	 *                     whether the task should be executed on the world thread
	 *                     (synchronous), or in a different thread (asynchronous).
	 * @return the created {@link TickFuture} object.
	 */
	private TickFuture addTask(TickTask task, int delay, int period, boolean asynchronous) {
		var taskId = generateId();
		task.initialise(taskId);
		task.setNextExecution(currentTick + delay);
		task.setPeriod(period);
		task.setAsynchronous(asynchronous);
		pending.add(task);
		return createFuture(task);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.newtask.scheduler.Scheduler#createTask(java.lang.
	 * Runnable)
	 */
	@Override
	public TickTask createTask(Runnable runnable) {
		return new TickTask() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see net.zaros.server.core.newtask.scheduler.tick.TickTask#execute()
			 */
			@Override
			protected void execute() {
				runnable.run();
			}
		};
	}

	/**
	 * Creates a new {@link TickFuture} object for the specified {@link TickTask
	 * task}.
	 * 
	 * @param task
	 *             the task to create the future for.
	 * @return the created {@link TickFuture} object.
	 */
	private TickFuture createFuture(TickTask task) {
		var future = new TickFuture(this, task);
		task.setFuture(future);
		return future;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.newtask.scheduler.Scheduler#getRunningTasks()
	 */
	@Override
	public Collection<TickTask> getRunningTasks() {
		return Collections.unmodifiableCollection(activeTasks.values());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.newtask.scheduler.Scheduler#getPendingTasks()
	 */
	@Override
	public Collection<TickTask> getPendingTasks() {
		return Collections.unmodifiableCollection(pending);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.core.newtask.scheduler.Scheduler#size()
	 */
	@Override
	public int size() {
		return activeTasks.size() + pending.size();
	}
}
