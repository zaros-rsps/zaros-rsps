package net.zaros.server.core;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import lombok.Getter;
import net.zaros.server.core.newtask.TaskManager;
import net.zaros.server.core.system.SystemManager;
import net.zaros.server.game.node.InitializingNodeList;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.utility.backend.SequentialService;

/**
 * The sequence for an update in the world. This occurs on every tick.
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/21/2017
 */
public final class SequencialUpdate implements SequentialService {
	
	/**
	 * The players that are renderable
	 */
	@Getter
	private static final InitializingNodeList<Player> renderablePlayers = new InitializingNodeList<>();
	
	/**
	 * Starts the sequence
	 */
	@Override
	public void start() {
		try {
			SystemManager.getScheduler().pulse();
			TaskManager.getTickScheduler().tick();
			for (Player player : getRenderablePlayers()) {
				player.tick();
				player.getUpdateMasks().prepare(player);
			}
			for (NPC npc : World.getNpcs()) {
				if (npc == null || !npc.isRenderable()) {
					continue;
				}
				npc.tick();
				npc.getUpdateMasks().prepare(npc);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Executes the updating part of the sequence
	 */
	@Override
	public void execute() {
		// the countdown latch, for sync'd decrement
		final CountDownLatch latch = new CountDownLatch(getRenderablePlayers().size());
		// loop thru the renderable
		for (Player player : getRenderablePlayers()) {
			EngineWorkingSet.submitEngineWork(() -> {
				try {
					player.sendUpdating();
					latch.countDown();
				} catch (Throwable e) {
					e.printStackTrace();
				}
			});
		}
		try {
			latch.await(1000L, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Finishes the sequence
	 */
	@Override
	public void end() {
		try {
			for (Player player : getRenderablePlayers()) {
				player.getUpdateMasks().finish();
				player.getRenderInformation().updateInformation();
				player.getHitMap().getHitList().clear();
				player.getSession().flushPackets();
			}
			for (NPC npc : World.getNpcs()) {
				if (npc == null || !npc.isRenderable()) {
					continue;
				}
				npc.getUpdateMasks().finish();
				npc.getHitMap().getHitList().clear();
			}
			renderablePlayers.sync();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
}