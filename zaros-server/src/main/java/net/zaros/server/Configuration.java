/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.electronwill.nightconfig.core.Config;
import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.file.FileConfig;

import net.zaros.server.game.node.Location;

/**
 * Represents the configuration manager in our system.
 * 
 * @author Walied K. Yassen
 */
public final class Configuration {

	/**
	 * The {@link Path} object that represents our configurations file location in
	 * the local file system.
	 */
	private static final Path PATH = Paths.get("configurations.toml");

	/**
	 * The configurations file.
	 */
	private static FileConfig file;

	/**
	 * Contains general information about our server, commonly used information with
	 * no categories.
	 * 
	 * @author Walied K. Yassen
	 */
	public interface General {
		/**
		 * The current server name.
		 */
		String NAME = get("general.name");
	}

	/**
	 * Contains all the information needed to read or write from our repository.
	 * 
	 * @author Walied K. Yassen
	 */
	public interface Repository {
		/**
		 * The base directory name.
		 */
		String BASE = get("repository.base");
		/**
		 * The cache directory name.
		 */
		String CACHE = get("repository.cache");
		/**
		 * The plugin(s) directory name.
		 */
		String PLUGINS = get("repository.plugins");
		/**
		 * The database directory name.
		 */
		String DATABASE = get("repository.database");
		/**
		 * The index directory name.
		 */
		String INDEX = get("repository.index");
		/**
		 * The players directory name.
		 */
		String PLAYERS = get("repository.players");
		/**
		 * The clans directory name.
		 */
		String CLANS = get("repository.clans");
		/**
		 * The discord database name.
		 */
		String DISCORD_DATABASE = get("repository.discord");
		/**
		 * The configurations database name.
		 */
		String CONFIGS_DATABASE = get("repository.configs");
		/**
		 * The map directory name.
		 */
		String MAP = get("repository.map");
		/**
		 * The map xteas directory name.
		 */
		String XTEAS = get("repository.xteas");
	}

	/**
	 * Contains all the information and settings needed to bootstrap the network and
	 * communicate with the client.
	 * 
	 * @author Walied K. Yassen
	 */
	public interface Network {
		/**
		 * Whether or not we should enable the ISAAC encryption to packets.
		 */
		boolean ENABLE_ISAAC = get("network.isaac");
		/**
		 * The timeout time in milliseconds.
		 */
		int TIMEOUT_TIME = get("network.timeout");
	}

	/**
	 * Contains all the settings and information required to set-up and fully
	 * process a world.
	 * 
	 * @author Walied K. Yassen
	 */
	public interface World {
		/**
		 * The maximum amount of players allowed per world.
		 */
		int LIMIT_PLAYERS = get("world.limit_players");
		/**
		 * The maximum amount of npcs allowed per world/
		 */
		int LIMIT_NPCS = get("world.limit_npcs");
		/**
		 * The player spawn location.
		 */
		Location SPAWN_LOCATION = loc(get("world.spawn_location"));
		/**
		 * The player respawn location.
		 */
		Location DEATH_LOCATION = loc(get("world.death_location"));
	}

	/**
	 * Contains all the settings that controls our game world economy.
	 * 
	 * @author Walied K. Yassen
	 */
	public interface Economy {
		/**
		 * The combat stats experience multiplier. This value is multiplied by the
		 * source experience to produce the final experience.
		 */
		int COMBAT_EXPERIENCE_RATE = get("economy.combat_experience_rate");
		/**
		 * The prayer stat experience multiplier. This value is multiplied by the source
		 * experience to produce the final experience.
		 */
		int PRAYER_EXPERIENCE_RATE = get("economy.prayer_experience_rate");;
		/**
		 * The skilling stats experience multiplier. This value is multiplied by the
		 * source experience to produce the final experience.
		 */
		int SKILL_EXPERIENCE_RATE = get("economy.skill_experience_rate");;
	}

	/**
	 * Contains all the settings that are related to our Discord Bot system.
	 * 
	 * @author Walied K. Yassen
	 */
	public interface Discord {

		/**
		 * The bot user name.
		 */
		String NAME = get("discord.name");
		/**
		 * The bot access token.
		 */
		String TOKEN = get("discord.token");
	}

	/**
	 * Gets the configuration value by the specified configuration {@code path}.
	 * 
	 * @param path
	 *             the configuration path which leads to our value.
	 * @return the configuration value.
	 */
	private static <T> T get(String path) {
		ensureInitialised();
		return file.get(path);
	}

	/**
	 * Ensures that the {@link FileConfig} which this class depends on is currently
	 * loaded.
	 */
	private static void ensureInitialised() {
		if (file == null) {
			file = CommentedFileConfig.of(PATH);
			file.load();
		}
	}

	private static Location loc(Config config) {
		int level = config.getInt("level");
		int x = config.getInt("x");
		int y = config.getInt("y");
		return new Location(x, y, level);
	}
}
