package net.zaros.server;

import net.zaros.cache.Cache;
import net.zaros.server.core.system.SystemManager;
import net.zaros.server.game.world.World;
import net.zaros.server.network.lobby.LobbyNetwork;
import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.web.sql.SQLRepository;
import net.zaros.server.utility.backend.UnexpectedArgsException;
import net.zaros.server.utility.newrepository.Repository;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/19/2017
 */
public class LSBootstrap {

	public static void main(String[] args) {
		try {
			SystemManager.setDefaults(args);//BootstrapType.LOBBY);
		} catch (UnexpectedArgsException e) {
			UnexpectedArgsException.push();
			System.exit(1);
			return;
		}
		try {
			Cache.initialise(Repository.getCacheDirectory());
			// starts the communication to the master server
			MasterCommunication.start();
			// stores the sql configuration data
			SQLRepository.storeConfiguration();
			// loads all lobby packets
			LobbyNetwork.PACKET_REPOSITORY.storeAll();
			// runs the procedure
			World.initialise(args);
			// binds the network port
			LobbyNetwork.bind();
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
