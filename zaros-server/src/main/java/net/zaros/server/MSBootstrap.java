package net.zaros.server;

import net.zaros.cache.Cache;
import net.zaros.server.core.system.SystemManager;
import net.zaros.server.network.master.server.engine.MSEngineFactory;
import net.zaros.server.network.master.server.network.MSNetworkSystem;
import net.zaros.server.network.web.sql.SQLRepository;
import net.zaros.server.utility.newrepository.Repository;
import net.zaros.server.utility.newrepository.database.index.IndexTables;

/**
 * The lobby server bootstrap, used to start the master server on its own.
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/10/2017
 */
public class MSBootstrap {

	/**
	 * The network system
	 */
	private static final MSNetworkSystem NETWORK_SYSTEM = new MSNetworkSystem();

	/**
	 * The main method that starts the master server
	 *
	 * @param args
	 *             The jvm arguments
	 */
	public static void main(String[] args) {
		try {
			Cache.initialise(Repository.getCacheDirectory());
			SystemManager.setDefaults(args);// BootstrapType.MANAGEMENT);
			SQLRepository.storeConfiguration();
			MSEngineFactory.startUp();
			IndexTables.initialise(MSEngineFactory.getScheduler());
			NETWORK_SYSTEM.bind();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

}