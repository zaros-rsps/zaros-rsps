package net.zaros.server.network.master.server.network.packet.in;

import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.engine.MSEngineFactory;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.network.master.utility.LoginRequest;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/12/2017
 */
@Readable(packetIds = { PacketConstants.LOGIN_REQUEST_PACKET_ID })
public class LoginRequestPacketIn implements ReadablePacket<MSSession> {
	
	@Override
	public void read(MSSession session, IncomingPacket packet) {
		int worldId = packet.readUnsignedShort();
		boolean lobby = packet.readByte() == 1;
		String username = packet.readString();
		String password = packet.readString();
		String uuid = packet.readString();
		
		// adds a login request
		MSEngineFactory.addLoginRequest(new LoginRequest(worldId, lobby, username, password, session, uuid));
	}
}
