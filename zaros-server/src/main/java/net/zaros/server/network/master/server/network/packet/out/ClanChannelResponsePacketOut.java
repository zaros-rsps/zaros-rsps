package net.zaros.server.network.master.server.network.packet.out;

import net.zaros.server.network.master.network.packet.writeable.WritablePacket;
import net.zaros.server.network.master.server.world.MSClans.ClanChannelResponse;

/**
 * @author Walied K. Yassen
 */
public class ClanChannelResponsePacketOut extends WritablePacket {

	/**
	 * The response code.
	 */
	private final ClanChannelResponse response;

	/**
	 * The requester user id.
	 */
	private final long userId;

	/**
	 * The channel clan id.
	 */
	private final long clanId;

	/**
	 * The clan channel block.
	 */
	private final byte[] attachment;

	/**
	 * Constructs a new {@link ClanChannelResponsePacketOut} type object instance.
	 * 
	 * @param response
	 *                 the response code.
	 * @param userId
	 *                 the requester user id.
	 * @param clanId
	 *                 the channel clan id.
	 */
	public ClanChannelResponsePacketOut(ClanChannelResponse response, long userId, long clanId) {
		this(response, userId, clanId, null);
	}

	/**
	 * Constructs a new {@link ClanChannelResponsePacketOut} type object instance.
	 * 
	 * @param response
	 *                   the response code.
	 * @param userId
	 *                   the requester user id.
	 * @param clanId
	 *                   the channel clan id.
	 * @param attachment
	 *                   the channel data block.
	 */
	public ClanChannelResponsePacketOut(ClanChannelResponse response, long userId, long clanId, byte[] attachment) {
		super(CLAN_CHANNEL_RESPONSE_PACKET_ID);
		this.response = response;
		this.userId = userId;
		this.clanId = clanId;
		this.attachment = attachment;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.network.master.network.packet.writeable.WritablePacket#
	 * create()
	 */
	@Override
	public WritablePacket create() {
		writeByte(response.ordinal());
		writeLong(userId);
		writeLong(clanId);
		if (attachment == null) {
			writeShort(0);
		} else {
			writeShort(attachment.length);
			write(attachment);
		}
		return this;
	}
}
