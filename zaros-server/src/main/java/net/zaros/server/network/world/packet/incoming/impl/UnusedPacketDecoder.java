package net.zaros.server.network.world.packet.incoming.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/8/2017
 */
public class UnusedPacketDecoder implements IncomingPacketDecoder {

	@Override
	public int[] bindings() {
		return arguments(52, 20);
	}

	@Override
	public void read(Player player, Packet packet) {

	}
}
