package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.Skills;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/21/2017
 */
public final class SkillPacketBuilder implements OutgoingPacketBuilder {
	
	/**
	 * The skill slot id.
	 */
	private final int slot;
	
	public SkillPacketBuilder(int slot) {
		this.slot = slot;
	}
	
	@Override
	public Packet build(Player player) {
		final PacketBuilder bldr = new PacketBuilder(8);
		final Skills skills = player.getSkills();
		
		bldr.writeByteC(skills.getLevel(slot));
		bldr.writeByte(slot);
		bldr.writeInt2((int) skills.getExperience(slot));
		return bldr.toPacket();
	}
}
