package net.zaros.server.network.world.packet.incoming.impl;

import net.zaros.server.game.content.action.interaction.PlayerFollowAction;
import net.zaros.server.game.content.combat.CombatManager;
import net.zaros.server.game.content.combat.data.magic.SpellSelection;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.content.combat.script.CombatScriptSelection;
import net.zaros.server.game.content.event.EventRepository;
import net.zaros.server.game.content.event.context.NodeReachEventContext;
import net.zaros.server.game.content.event.impl.NodeReachEvent;
import net.zaros.server.game.content.skills.magic.Spell;
import net.zaros.server.game.node.entity.link.interaction.TradeInteraction;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.InteractionOption;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/20/2017
 */
public class PlayerInteractionPacketDecoder implements IncomingPacketDecoder {
	
	/**
	 * The attack player opcode
	 */
	private static final byte ATTACK_PLAYER = 43;
	
	/**
	 * The follow player opcode
	 */
	private static final byte FOLLOW_PLAYER = 44;
	
	/**
	 * The trade player opcode
	 */
	private static final byte PLAYER_TRADE_REQUEST = 90;
	
	/**
	 * The interface on player packet
	 */
	private static final byte PLAYER_INTERFACE_USAGE = 65;
	
	/**
	 * The request accept opcode
	 */
	private static final byte PLAYER_REQUEST_ACCEPT = 83;
	
	@Override
	public int[] bindings() {
		return arguments(ATTACK_PLAYER, FOLLOW_PLAYER, PLAYER_TRADE_REQUEST, PLAYER_INTERFACE_USAGE, PLAYER_REQUEST_ACCEPT);
	}
	
	@Override
	public void read(Player player, Packet packet) {
		switch (packet.getOpcode()) {
			case PLAYER_INTERFACE_USAGE:
				readPlayerInterfaceUsage(player, packet);
				break;
			case PLAYER_REQUEST_ACCEPT:
				readPlayerRequestAcceptPacket(player, packet);
				break;
			default:
				readPlayerOptionPacket(player, packet);
				break;
		}
	}
	
	/**
	 * Reads the packet received when an interface is used on a player
	 *
	 * @param player
	 * 		The player
	 * @param packet
	 * 		The packet
	 */
	private void readPlayerInterfaceUsage(Player player, Packet packet) {
		int index = packet.readLEShort();
		boolean running = packet.readByte() == 1;
		int id = packet.readShortA();
		int interfaceHash = packet.readLEInt();
		int interfaceId = interfaceHash >> 16;
		int componentId = interfaceHash & 0xFFFF;
		int slot = packet.readLEShortA();
		if (index < 0 || index > 2048) {
			return;
		}
		Player p2 = World.getPlayers().get(index);
		if (p2 == null) {
			return;
		}
		switch (interfaceId) {
			case 192: // regular
			case 193: // ancients

				Spell spell = SpellSelection.getSpell(player, componentId);

				if (spell != null) {
					player.faceEntity(p2);
					player.putTemporaryAttribute(AttributeKey.CURRENT_SPELL, spell);
					CombatManager.startCombat(player, p2);

					CombatScript script = CombatScriptSelection.get(player);

					if (CombatManager.isWithinDistance(player, p2, script, false)) {
						player.getMovement().resetWalkSteps();
					}

					return;
				}
				break;
		}
	}
	
	/**
	 * Reads the packet that is sent when a player option is clicked
	 *
	 * @param player
	 * 		The player
	 * @param packet
	 * 		The packet
	 */
	private void readPlayerOptionPacket(Player player, Packet packet) {
		int index = packet.readShort();
		boolean running = packet.readByte() == 1;
		if (index > 2047 || index < 1) {
			return;
		}
		Player p2 = World.getPlayers().get(index);
		if (p2 == null) {
			return;
		}
		switch (packet.getOpcode()) {
			case ATTACK_PLAYER:
				decodePlayerAttack(player, p2);
				break;
			case FOLLOW_PLAYER:
				decodePlayerFollow(player, p2);
				break;
			case PLAYER_TRADE_REQUEST:
				handleTradeRequest(player, p2);
				break;
		}
	}
	
	/**
	 * Reads the player request accept packet
	 *
	 * @param player
	 * 		The player
	 * @param packet
	 * 		The packet
	 */
	private void readPlayerRequestAcceptPacket(Player player, Packet packet) {
		int index = packet.readShort();
		int type = packet.readByte();
		if (index < 1 || index > 2047) {
			return;
		}
		Player p2 = World.getPlayers().get(index);
		if (p2 == null) {
			return;
		}
		switch(type) {
			// trade request type
			case 0:
				handleTradeRequest(player, p2);
				break;
		}
	}
	
	/**
	 * Decodes the player attack packet
	 *
	 * @param player
	 * 		The player
	 * @param p2
	 * 		The other player we're attacking
	 */
	private void decodePlayerAttack(Player player, Player p2) {
		// stop everything
		player.stop(true, true, true, false);
		// face the player
		player.faceEntity(p2);
		if (!player.getVariables().isInFightArea()) {
			return;
		}
		if (!player.getVariables().isInFightArea() || !p2.getVariables().isInFightArea()) {
			player.getTransmitter().sendMessage("You can only attack players in a player-vs-player area.");
			return;
		}
		if (!p2.getArea().isMulti() || !player.getArea().isMulti()) {
			if (player.getAttackedBy() != p2 && player.getAttackedByDelay() > System.currentTimeMillis()) {
				player.getTransmitter().sendMessage("You are already in combat.");
				return;
			}
			if (p2.getAttackedBy() != player && p2.getAttackedByDelay() > System.currentTimeMillis()) {
				if (p2.getAttackedBy() instanceof NPC) {
					p2.setAttackedBy(player);
				} else {
					player.getTransmitter().sendMessage("That player is already in combat.");
					return;
				}
			}
		}
		
		if (player.getManager().getActivities().handlesNodeInteraction(p2, InteractionOption.ATTACK_OPTION)) {
			return;
		}
		CombatManager.startCombat(player, p2);
	}
	
	/**
	 * Decodes the player follow packet
	 *
	 * @param player
	 * 		The player
	 * @param other
	 * 		The other player we're following
	 */
	private void decodePlayerFollow(Player player, Player other) {
		player.stop(true, true, true, false);
		player.getManager().getActions().startAction(new PlayerFollowAction(other));
	}
	
	/**
	 * Decodes the player request packet
	 *
	 * @param player
	 * 		The player
	 * @param other
	 * 		The other player we're requesting
	 */
	private void handleTradeRequest(Player player, Player other) {
		player.stop(true, true, true, false);
		int distance = player.getLocation().getDistance(other.getLocation());
		if (distance == 0 || distance > 1) {
			EventRepository.executeEvent(player, NodeReachEvent.class, new NodeReachEventContext(other, () -> {
				if (!other.isRenderable()) {
					return;
				}
				TradeInteraction.handleTradeRequesting(player, other);
			}));
		} else {
			TradeInteraction.handleTradeRequesting(player, other);
		}
	}
	
}
