package net.zaros.server.network.master.server.network.packet.in;

import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.network.master.server.world.MSClans;

/**
 * @author Walied K. Yassen
 */
@Readable(packetIds = { PacketConstants.CLAN_CHANNEL_MESSAGE_REQUEST_PACKET_ID })
public final class ClanChannelMessageRequestPacketIn implements ReadablePacket<MSSession> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.master.network.packet.readable.ReadablePacket#read(
	 * net.zaros.server.network.master.network.MasterSession,
	 * net.zaros.server.network.master.network.packet.IncomingPacket)
	 */
	@Override
	public void read(MSSession session, IncomingPacket packet) {
		var userId = packet.readLong();
		var clanId = packet.readLong();
		var displayName = packet.readString();
		var modLevel = packet.readUnsignedByte();
		var message = packet.readString();
		MSClans.handleMessageRequest(userId, clanId, displayName, modLevel, message);
	}
}
