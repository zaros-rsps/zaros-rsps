package net.zaros.server.network.master.client.packet.out;

import net.zaros.server.game.GameConstants;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;
import net.zaros.server.utility.backend.SecureOperations;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/12/2017
 */
public class PlayerFilePacketOut extends WritablePacket {
	
	/**
	 * The name of the file
	 */
	private final String username;
	
	/**
	 * The contents of the file
	 */
	private final byte[] serialised;
	
	public PlayerFilePacketOut(String username, byte[] serialised) {
		super(PLAYER_FILE_UPDATE_PACKET_ID);
		this.username = username;
		this.serialised = SecureOperations.getCompressedEncrypted(serialised, GameConstants.FILE_ENCRYPTION_KEY);
	}
	
	@Override
	public WritablePacket create() {
		writeString(username);
		writeInt(serialised.length);
		for (byte content : serialised) {
			writeByte(content);
		}
		return this;
	}
	
}