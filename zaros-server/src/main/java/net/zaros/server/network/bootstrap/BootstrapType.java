package net.zaros.server.network.bootstrap;

import lombok.Getter;

/**
 * @author Jacob Rhiel
 */
public enum BootstrapType {

    MANAGEMENT(true, 0, false),

    LOBBY(true, 10, false),

    MAIN_WORLD(true, 1, false),

    PVP_WORLD(true, 2, false),

    ;

    @Getter
    private final boolean debug, webIntegrated;

    @Getter
    private final int world;

    BootstrapType(boolean debug, int world, boolean webIntegrated) {
        this.debug = debug;
        this.world = world;
        this.webIntegrated = webIntegrated;
    }

}
