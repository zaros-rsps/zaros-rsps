package net.zaros.server.network.master.client;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/11/2017
 */
public class MCFlags {
	
	/**
	 * The id of the world this client is for
	 */
	public static int worldId;
	
}
