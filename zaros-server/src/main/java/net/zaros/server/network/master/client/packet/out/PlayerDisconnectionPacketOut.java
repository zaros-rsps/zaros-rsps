package net.zaros.server.network.master.client.packet.out;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * @author Walied K. Yassen
 */
public class PlayerDisconnectionPacketOut extends WritablePacket {

	/**
	 * The name of the player who disconnected
	 */
	private final Player player;

	/**
	 * The id of the world we're on
	 */
	private final int worldId;

	public PlayerDisconnectionPacketOut(Player player, int worldId) {
		super(PLAYER_DISCONNECTION_PACKET_ID);
		this.player = player;
		this.worldId = worldId;

	}

	@Override
	public WritablePacket create() {
		writeLong(player.getUserId());
		writeLong(player.getCurrentClanId());
		writeShort(worldId);
		return this;
	}
}
