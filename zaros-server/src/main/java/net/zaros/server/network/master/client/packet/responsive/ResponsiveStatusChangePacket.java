package net.zaros.server.network.master.client.packet.responsive;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.network.master.client.packet.ResponsiveGamePacket;
import net.zaros.server.utility.rs.constant.GameBarStatus;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/15/2017
 */
public class ResponsiveStatusChangePacket extends ResponsiveGamePacket {

	/**
	 * The name of the user who updated their status
	 */
	private final String name;

	/**
	 * The status that the user updated to
	 */
	private final byte status;

	/**
	 * The id of the world that the user who updated their status was in
	 */
	private final int worldId;

	/**
	 * If the user who updated their status was online
	 */
	private final boolean online;

	/**
	 * If the user who updated their status was in the lobby
	 */
	private final boolean lobby;

	public ResponsiveStatusChangePacket(String name, byte status, int worldId, boolean online, boolean lobby) {
		this.name = name;
		this.status = status;
		this.worldId = worldId;
		this.online = online;
		this.lobby = lobby;
	}

	@Override
	public void read() {
		if (status >= GameBarStatus.values().length) {
			return;
		}

		GameBarStatus barStatus = GameBarStatus.values()[status];

		for (Player other : World.getPlayers()) {
			update(other, barStatus);
		}
	}

	private void update(Player player, GameBarStatus barStatus) {

		if (player == null) {
			return;
		}
		// we skip the players that aren't renderable if they aren't in lobby
		// lobby players are always unrenderable
		if (!player.isRenderable() && !player.getSession().isInLobby()) {
			return;
		}
		// the player never sees themselves on their own list
		if (name.equals(player.getDetails().getUsername())) {
			return;
		}
		// we don't have that person on our friends list so we don't care their status
		// was changed
		if (!player.getManager().getContacts().hasFriend(name)) {
			return;
		}
		/*// we do not show if they are on ignore list
		if (player.getManager().getContacts().getIgnoreList().contains(player.getDetails().getUsername())) {
			player.getManager().getContacts().updateFriendStatus(name, worldId, lobby, false, true);
			// continue because none of the following applies
			return;
		}*/
		if (barStatus == GameBarStatus.ON) {
			player.getManager().getContacts().updateFriendStatus(name, worldId, lobby, online, true);
		} else if (barStatus == GameBarStatus.FRIENDS) {
			// if player has them on friends list
			if (player.getManager().getContacts().getFriendList().contains(player.getDetails().getUsername())) {
				// show the player as online
				player.getManager().getContacts().updateFriendStatus(name, worldId, lobby, online, true);
			} else {
				// if they don't, we show them as offline
				player.getManager().getContacts().updateFriendStatus(name, worldId, lobby, false, true);
			}
		} else if (barStatus == GameBarStatus.OFF) {
			player.getManager().getContacts().updateFriendStatus(name, worldId, false, false, true);
		}
	}

}
