package net.zaros.server.network.master.server.network.packet.in;

import static net.zaros.server.network.master.network.packet.PacketConstants.VERIFICATION_ATTEMPT_PACKET_ID;

import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.network.master.server.network.packet.out.SuccessfulVerificationOut;
import net.zaros.server.network.master.server.world.MSRepository;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/11/2017
 */
@Readable(packetIds = { VERIFICATION_ATTEMPT_PACKET_ID })
public class VerificationPacketIn implements ReadablePacket<MSSession> {
	
	@Override
	public void read(MSSession session, IncomingPacket packet) {
		if (session.isVerified()) {
			System.out.println("Session attempted duplicate verification: " + session);
			return;
		}
		
		var worldId = packet.readUnsignedShort();
		var key = packet.readString();
		
		// the key we expected for this world
		var expectedKey = VERIFICATION_KEY;
		
		// keys didn't match
		if (!expectedKey.equals(key)) {
			System.out.println("Keys were not equal, we received " + key + " for world " + worldId);
			return;
		}
		
		// the world created
		var world = MSRepository.createNewWorld(worldId);
		// if the world was created successfully
		boolean created = world != null;
		
		// we let the session know they have been verified
		session.setVerified(true);
		session.sync(world);
		session.write(new SuccessfulVerificationOut(created));
	}
}
