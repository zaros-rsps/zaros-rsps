package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/29/2017
 */
public class MinimapFlagResetBuilder implements OutgoingPacketBuilder {
	
	/**
	 * The map x type
	 */
	private final int mapX;
	
	/**
	 * The map y type
	 */
	private final int mapY;
	
	public MinimapFlagResetBuilder(int mapX, int mapY) {
		this.mapX = mapX;
		this.mapY = mapY;
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(16);
		bldr.writeByteC(mapX);
		bldr.writeByteC(mapY);
		return bldr.toPacket();
	}
}
