package net.zaros.server.network.world.packet.incoming.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/8/2017
 */
public class WorldMapDecoder implements IncomingPacketDecoder {

	@Override
	public int[] bindings() {
		return arguments(63);
	}

	@Override
	public void read(Player player, Packet packet) {
		/*
		 * int coordinateHash = packet.readInt2(); int x = coordinateHash >> 14; int y =
		 * coordinateHash & 0x3fff; int plane = coordinateHash >> 28;
		 */
	}
}
