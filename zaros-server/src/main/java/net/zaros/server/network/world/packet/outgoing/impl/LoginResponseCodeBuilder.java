package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;
import net.zaros.server.utility.backend.ReturnCode;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/18/2017
 */
public final class LoginResponseCodeBuilder implements OutgoingPacketBuilder {
	
	/**
	 * The value of the login response
	 */
	private final int value;
	
	public LoginResponseCodeBuilder(int value) {
		this.value = value;
	}
	
	public LoginResponseCodeBuilder(ReturnCode returnCode) {
		this.value = returnCode.getValue();
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder builder = new PacketBuilder();
		builder.writeByte(value);
		return builder.toPacket();
	}
}
