package net.zaros.server.network.master.client.packet.in;

import net.zaros.server.game.world.punishment.Punishment;
import net.zaros.server.game.world.punishment.PunishmentType;
import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.client.packet.responsive.ResponsivePunishmentSuccessPacket;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/17/2017
 */
@Readable(packetIds = { PacketConstants.PUNISHMENT_ADDITION_SUCCESS_DELIVERY_PACKET_ID, PacketConstants.PUNISHMENT_REMOVAL_SUCCESS_DELIVERY_PACKET })
public class PunishmentStateSuccessDeliveryPacketIn implements ReadablePacket<MCSession> {
	
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		var worldId = packet.readUnsignedShort();
		var message = packet.readString();
		var punisher = packet.readString();
		var punished = packet.readString();
		var type = (byte) packet.readByte();
		var time = packet.readLong();
		
		if (type < 0 || type >= PunishmentType.values().length) {
			System.out.println("Invalid punishment type received.");
			return;
		}
		
		// the instance of the punishment
		Punishment punishment = new Punishment(punisher, punished, PunishmentType.values()[type], time);
		
		// read it now
		MasterCommunication.read(new ResponsivePunishmentSuccessPacket(worldId, punishment, message));
	}
}
