package net.zaros.server.network.master.client.packet.in;

import java.util.Optional;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.world.packet.outgoing.impl.PrivateMessageSendBuilder;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/15/2017
 */
@Readable(packetIds = { PacketConstants.PRIVATE_MESSAGE_DELIVERY_PACKET_ID })
public class PrivateMessageDeliveredPacketIn implements ReadablePacket<MCSession> {
	
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		var sourceName = packet.readString();
		var deliveryName = packet.readString();
		var message = packet.readString();
		
		Optional<Player> optional = World.getPlayerByUsername(sourceName);
		if (!optional.isPresent()) {
			System.out.println("Unable to find player by name " + sourceName);
			return;
		}
		Player player = optional.get();
		player.getTransmitter().send(new PrivateMessageSendBuilder(Misc.formatPlayerNameForDisplay(deliveryName), Misc.formatTextToSentence(message)).build(player));
	}
}
