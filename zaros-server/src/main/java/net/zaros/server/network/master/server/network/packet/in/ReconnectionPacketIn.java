package net.zaros.server.network.master.server.network.packet.in;

import static net.zaros.server.network.master.network.packet.PacketConstants.RECONNECTION_PACKET_ID;

import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.network.master.server.world.MSClans;
import net.zaros.server.network.master.server.world.MSRepository;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/3/2017
 */
@Readable(packetIds = { RECONNECTION_PACKET_ID })
public class ReconnectionPacketIn implements ReadablePacket<MSSession> {

	@Override
	public void read(MSSession session, IncomingPacket packet) {
		var worldId = packet.readUnsignedShort();
		var size = packet.readInt();
		for (var index = 0; index < size; index++) {
			var userId = packet.readLong();
			var clanId = packet.readLong();
			var username = packet.readString();
			var uid = packet.readString();
			MSClans.updatePlayerWorld(clanId, userId, worldId);
			MSRepository.getWorld(worldId).ifPresent(world -> world.addPlayer(username, uid, userId));
		}
	}
}
