package net.zaros.server.network.web.http;

import okhttp3.Request;
import okhttp3.ResponseBody;

import java.io.IOException;

/**
 * @author Jacob Rhiel
 */
public class HttpRequest {

    public ResponseBody call(HttpClient client) throws IOException {
        Request request = new Request.Builder().url("").build();
        return client.newCall(request).execute().body();
    }

    public void constructRequest() {

    }


}


