package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.Packet.PacketType;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;
import net.zaros.server.utility.tool.BufferUtils;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/13/2017
 */
public class PrivateMessageReceiveBuilder implements OutgoingPacketBuilder {
	
	/**
	 * The player name.
	 */
	private final String name;
	
	/**
	 * The message to receive.
	 */
	private final String message;
	
	/**
	 * The player rights.
	 */
	private final int rights;
	
	public PrivateMessageReceiveBuilder(String name, String message, int rights) {
		this.name = name;
		this.message = message;
		this.rights = rights;
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(118, PacketType.VAR_BYTE);
		
		byte[] encryptedData = new byte[message.length() + 1];
		encryptedData[0] = (byte) message.length();
		BufferUtils.huffmanCompress(message, encryptedData, 1);
		
		bldr.writeByte(1);
		bldr.writeString(name); // display name
		bldr.writeString(name); // user name
		for (int i = 0; i < 5; i++) {
			bldr.writeByte(Misc.getRandom(255));
		}
		bldr.writeByte(rights);
		bldr.writeBytes(encryptedData);
		
		return bldr.toPacket();
	}
}
