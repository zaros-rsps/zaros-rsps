package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;
import net.zaros.server.utility.rs.constant.GameBarStatus;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/9/2017
 */
public class GameStatusBuilder implements OutgoingPacketBuilder {
	
	/**
	 * The flag for the public bar
	 */
	private final byte publicFlag;
	
	/**
	 * The flag for the trade bar
	 */
	private final byte tradeFlag;
	
	public GameStatusBuilder(GameBarStatus publicBar, GameBarStatus tradeBar) {
		this.publicFlag = publicBar.getValue();
		this.tradeFlag = tradeBar.getValue();
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(58);
		bldr.writeByteA(publicFlag);
		bldr.writeByteA(tradeFlag);
		return bldr.toPacket();
	}
}
