package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;
import net.zaros.server.utility.rs.constant.GameBarStatus;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/12/2017
 */
public class PrivateStatusBuilder implements OutgoingPacketBuilder {
	
	/**
	 * The value to send
	 */
	private final int value;
	
	public PrivateStatusBuilder(int value) {
		this.value = value;
	}
	
	public PrivateStatusBuilder(GameBarStatus status) {
		this.value = status.getValue();
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(104);
		bldr.writeByte(value);
		return bldr.toPacket();
	}
}
