package net.zaros.server.network.master.server.network.packet.out;

import net.zaros.server.game.world.punishment.Punishment;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/17/2017
 */
public class PunishmentAdditionSuccessDeliveryPacketOut extends WritablePacket {
	
	/**
	 * The id of the world that successfully applied the punishment
	 */
	private final int worldId;
	
	/**
	 * The punishment
	 */
	private final Punishment punishment;
	
	/**
	 * The message of succession
	 */
	private final String message;
	
	public PunishmentAdditionSuccessDeliveryPacketOut(int worldId, Punishment punishment, String message) {
		super(PUNISHMENT_ADDITION_SUCCESS_DELIVERY_PACKET_ID);
		this.worldId = worldId;
		this.punishment = punishment;
		this.message = message;
	}
	
	@Override
	public WritablePacket create() {
		writeShort(worldId);
		writeString(message);
		writeString(punishment.getPunisher());
		writeString(punishment.getPunished());
		writeByte((byte) punishment.getType().ordinal());
		writeLong(punishment.getTime());
		return this;
	}
}
