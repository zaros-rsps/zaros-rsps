package net.zaros.server.network.master.client.packet.responsive;

import java.util.Optional;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.game.world.punishment.Punishment;
import net.zaros.server.network.master.client.packet.ResponsiveGamePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/17/2017
 */
public class ResponsivePunishmentSuccessPacket extends ResponsiveGamePacket {
	
	/**
	 * The id of the player who was punished
	 */
	private final int worldId;
	
	/**
	 * The punishment
	 */
	private final Punishment punishment;
	
	/**
	 * The message
	 */
	private final String message;
	
	public ResponsivePunishmentSuccessPacket(int worldId, Punishment punishment, String message) {
		this.worldId = worldId;
		this.punishment = punishment;
		this.message = message;
	}
	
	@Override
	public void read() {
		Optional<Player> optional = World.getPlayerByUsername(punishment.getPunisher());
		if (!optional.isPresent()) {
			return;
		}
		Player player = optional.get();
		player.getTransmitter().sendMessage(message);
	}
}
