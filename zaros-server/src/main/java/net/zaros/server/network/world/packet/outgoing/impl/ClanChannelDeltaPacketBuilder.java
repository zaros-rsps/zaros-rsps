package net.zaros.server.network.world.packet.outgoing.impl;

import lombok.RequiredArgsConstructor;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.Packet.PacketType;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * Represents the client clan channel delta update block packet builder.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class ClanChannelDeltaPacketBuilder implements OutgoingPacketBuilder {

	/**
	 * The channel delta update block.
	 */
	private final byte[] block;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder#build(
	 * net.zaros.server.game.node.entity.player.Player)
	 */
	@Override
	public Packet build(Player player) {
		var builder = new PacketBuilder(84, PacketType.VAR_SHORT);
		builder.writeByte(1);
		builder.writeBytes(block);
		return builder.toPacket();
	}
}
