package net.zaros.server.network.master.server.world;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import net.zaros.server.network.master.MasterConstants;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Walied K. Yassen
 * @author Tyluur
 */
@Slf4j
public final class MSRepository implements MasterConstants {

	/**
	 * The array of worlds that we hold
	 */
	@Singular
	private static final Map<Integer, MSWorld> WORLDS = new HashMap<Integer, MSWorld>();

	/**
	 * Creates a new world
	 *
	 * @param worldId
	 *                The id of the world
	 */
	public static MSWorld createNewWorld(int worldId) {
		// we've already made this world.
		if (WORLDS.containsKey(worldId)) {
			log.info("Attempted to create a new world when it was already made: " + worldId);
			return null;
		}
		// creates a new world
		var world = new MSWorld(worldId);
		// saves the index of the world
		WORLDS.put(worldId, world);
		log.info("World " + worldId + " was just registered & verified.");
		return world;
	}

	/**
	 * Checks if a player is online on any of the worlds
	 *
	 * @param username
	 *                   The username of the player
	 * @param checkLobby
	 *                   If we should check the lobby players
	 */
	public static boolean isOnline(String username, boolean checkLobby) {
		boolean online = false;

		// we found them in the lobby! don't check worlds unnecessarily
		if (online) {
			return true;
		}

		// loop through all the worlds
		for (var world : WORLDS.values()) {
			if (world.isLobby() && !checkLobby) {
				continue;
			}
			// found the player is online, loop doesn't need to continue.
			if (world.isOnline(username)) {
				online = true;
				break;
			}
		}
		return online;
	}

	/**
	 * Gets a world by ids id
	 *
	 * @param worldId
	 *                The world id
	 */
	public static Optional<MSWorld> getWorld(int worldId) {
		var world = WORLDS.get(worldId);
		if (world == null) {
			return Optional.empty();
		}
		return Optional.of(world);
	}

	/**
	 * Unregisters the world
	 *
	 * @param world
	 *              The world
	 */
	public static void unregister(MSWorld world) {
		WORLDS.remove(world.getId());
		world.unregister();
		System.out.println("World " + world.getId() + " was just unregistered.");
	}

	/**
	 * If the player exists in a world, we return the master session that that
	 * player is in
	 *
	 * @param username
	 *                 The name of the player
	 */
	public static Optional<MSSession> getSessionByUsername(String username) {
		var formattedName = Misc.formatPlayerNameForProtocol(username);
		// loop through all the worlds
		for (var world : WORLDS.values()) {
			if (world == null) {
				continue;
			}
			// there is a player by that name in this world so this is the right world
			if (world.playerRegistered(formattedName)) {
				return Optional.of(world.getSession());
			}
		}
		return Optional.empty();
	}

	/**
	 * Gets a player by their username from any of the worlds
	 *
	 * @param username
	 *                 The username of the player
	 */
	public static Optional<MSPlayer> getPlayer(String username) {
		for (var world : WORLDS.values()) {
			if (world == null) {
				continue;
			}
			var optional = world.getPlayerByName(username);
			if (!optional.isPresent()) {
				continue;
			}
			return optional;
		}
		return Optional.empty();
	}

	/**
	 * Gets a player by their id from any of the worlds
	 *
	 * @param username
	 *                 The id of the player
	 */
	public static Optional<MSPlayer> getPlayerById(long userId) {
		for (var world : WORLDS.values()) {
			if (world == null) {
				continue;
			}
			var optional = world.getPlayerById(userId);
			if (!optional.isPresent()) {
				continue;
			}
			return optional;
		}
		return Optional.empty();
	}

	/**
	 * Gets the player details in an object array. [0] = username, [1] =
	 * online/offline, [2] = world id
	 *
	 * @param username
	 *                 The name of the player
	 */
	public static Object[] getPlayerDetails(String username) {
		var optional = getPlayer(username);
		return optional.map(player -> new Object[] { username, true, player.getWorldId() }).orElseGet(() -> new Object[] { username, false, (int) 0 });
	}

	/**
	 * Gets the amount of worlds that are active
	 */
	public static int getWorldCount() {
		int count = 0;
		for (var world : WORLDS.values()) {
			if (world == null || world.isLobby()) {
				continue;
			}
			count++;
		}
		return count;
	}

	public static Map<Integer, MSWorld> getWorlds() {
		return WORLDS;
	}
}