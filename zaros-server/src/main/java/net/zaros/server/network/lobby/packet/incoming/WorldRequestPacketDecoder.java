package net.zaros.server.network.lobby.packet.incoming;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.lobby.packet.outgoing.WorldListBuilder;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/18/2017
 */
public class WorldRequestPacketDecoder implements IncomingPacketDecoder {
	
	@Override
	public int[] bindings() {
		return Misc.arguments(33);
	}
	
	@Override
	public void read(Player player, Packet packet) {
		player.getTransmitter().send(new WorldListBuilder(true, true).build(player));
	}
}
