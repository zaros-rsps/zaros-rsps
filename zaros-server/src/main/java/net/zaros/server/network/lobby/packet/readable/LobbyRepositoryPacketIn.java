package net.zaros.server.network.lobby.packet.readable;

import net.zaros.server.game.world.list.WorldList;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/25/2017
 */
@Readable(packetIds = { PacketConstants.REPOSITORY_UPDATE_PACKET_ID })
public class LobbyRepositoryPacketIn implements ReadablePacket<MCSession> {
	
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		int id = packet.readUnsignedShort();
		int size = packet.readInt();
		
		// updates the size of the world
		WorldList.updateSize(id, size);
	}
}
