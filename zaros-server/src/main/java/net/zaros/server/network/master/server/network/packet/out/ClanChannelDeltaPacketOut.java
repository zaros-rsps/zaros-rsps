package net.zaros.server.network.master.server.network.packet.out;

import java.util.Set;

import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * Represents the clan channel delta update block outgoing packet.
 * 
 * @author Walied K. Yassen
 */
public final class ClanChannelDeltaPacketOut extends WritablePacket {

	/**
	 * The current user ids.
	 */
	private final Set<Long> userIds;

	/**
	 * The delta update block.
	 */
	private final byte[] updateBlock;

	/**
	 * Constructs a new {@link ClanChannelDeltaPacketOut} type object instance.
	 * 
	 * @param userIds
	 *                    the user ids which we sending the update for.
	 * @param updateBlock
	 *                    the delta update block.
	 */
	public ClanChannelDeltaPacketOut(Set<Long> userIds, byte[] updateBlock) {
		super(PacketConstants.CLAN_CHANNEL_DELTA_BLOCK_PACKET_ID);
		this.userIds = userIds;
		this.updateBlock = updateBlock;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.network.master.network.packet.writeable.WritablePacket#
	 * create()
	 */
	@Override
	public WritablePacket create() {
		writeShort(userIds.size());
		for (var id : userIds) {
			writeLong(id);
		}
		writeInt(updateBlock.length);
		write(updateBlock);
		return this;
	}
}
