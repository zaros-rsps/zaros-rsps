package net.zaros.server.network.master.client.packet.in;

import net.zaros.server.game.world.World;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;

/**
 * @author Walied K. Yassen
 */
@Readable(packetIds = { PacketConstants.CLAN_SETTINGS_BLOCK_PACKET_ID })
public final class ClanSettingsBlockPacketIn implements ReadablePacket<MCSession> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.master.network.packet.readable.ReadablePacket#read(
	 * net.zaros.server.network.master.network.MasterSession,
	 * net.zaros.server.network.master.network.packet.IncomingPacket)
	 */
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		var userId = packet.readLong();
		// read the settings block.
		var block = new byte[packet.readInt()];
		packet.read(block);
		// grab the world player.
		var player = World.getPlayerById(userId);
		if (player.isPresent()) {
			player.get().getTransmitter().sendClanSettingsFull(block);
		}

	}

}
