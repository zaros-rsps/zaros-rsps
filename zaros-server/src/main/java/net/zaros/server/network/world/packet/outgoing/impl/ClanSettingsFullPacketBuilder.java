package net.zaros.server.network.world.packet.outgoing.impl;

import lombok.RequiredArgsConstructor;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.Packet.PacketType;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * Represents the clan settings block packet builder.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class ClanSettingsFullPacketBuilder implements OutgoingPacketBuilder {

	/**
	 * Whether it is for affined or unaffined
	 */
	private final boolean affined;

	/**
	 * The clan settings full update block.
	 */
	private final byte[] block;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder#build(
	 * net.zaros.server.game.node.entity.player.Player)
	 */
	@Override
	public Packet build(Player player) {
		var builder = new PacketBuilder(99, PacketType.VAR_SHORT);
		builder.writeByte(affined ? 1 : 0);
		builder.writeBytes(block);
		return builder.toPacket();
	}

}
