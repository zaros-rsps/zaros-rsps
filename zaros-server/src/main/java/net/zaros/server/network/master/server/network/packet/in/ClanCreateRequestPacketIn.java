package net.zaros.server.network.master.server.network.packet.in;

import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.network.master.server.world.MSClans;

/**
 * Represents the clan creation receive packet.
 * 
 * @author Walied K. Yassen
 */
@Readable(packetIds = { PacketConstants.CLAN_CREATE_REQUEST_PACKET_ID })
public final class ClanCreateRequestPacketIn implements ReadablePacket<MSSession> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.master.network.packet.readable.ReadablePacket#read(
	 * net.zaros.server.network.master.network.MasterSession,
	 * net.zaros.server.network.master.network.packet.IncomingPacket)
	 */
	@Override
	public void read(MSSession session, IncomingPacket packet) {
		var userid = packet.readLong();
		var userName = packet.readString();
		var clanName = packet.readString();
		MSClans.handleCreateRequest(userid, userName, clanName);
	}
}
