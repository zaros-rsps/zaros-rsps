package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

public class InterfaceModelBuilder implements OutgoingPacketBuilder {

    /**
     * The interface id.
     */
    private final int interfaceId;

    /**
     * The child id.
     */
    private final int componentId;

    /**
     * If we should hide the child.
     */
    private final int model;

    /**
     * Constructs a new {@code InterfaceDisplayModificationBuilder} {@code Object}
     *
     * @param interfaceId
     * 		The id of the interface
     * @param componentId
     * 		The child id of the interface
     * @param model
     * 		If we should hide the child
     */
    public InterfaceModelBuilder(int interfaceId, int componentId, int model) {
        this.interfaceId = interfaceId;
        this.componentId = componentId;
        this.model = model;
    }

    @Override
    public Packet build(Player player) {
        PacketBuilder bldr = new PacketBuilder(102);
        bldr.writeInt(interfaceId << 16 | componentId);
        bldr.writeShortA(model);
        return bldr.toPacket();
    }
}

