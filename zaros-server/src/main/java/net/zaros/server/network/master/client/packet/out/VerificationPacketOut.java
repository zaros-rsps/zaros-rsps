package net.zaros.server.network.master.client.packet.out;

import net.zaros.server.network.master.client.MCFlags;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/11/2017
 */
public class VerificationPacketOut extends WritablePacket {
	
	public VerificationPacketOut() {
		super(VERIFICATION_ATTEMPT_PACKET_ID);
	}
	
	@Override
	public WritablePacket create() {
		final int worldId = MCFlags.worldId;
		writeShort(worldId);
		writeString(VERIFICATION_KEY);
		return this;
	}
}
