/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.network.world.packet.outgoing.impl;

import lombok.Value;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * Represents the synth sound play packet builder.
 * 
 * @author Walied K. Yassen
 */
@Value
public class SynthSoundBuilder implements OutgoingPacketBuilder {

	/**
	 * The synth sound id.
	 */
	private final int soundId;

	/**
	 * The sound repeat count.
	 */
	private final int repeat;

	/**
	 * The sound play delay.
	 */
	private final int delay;

	/**
	 * The sound volume.
	 */
	private final int volume;

	/**
	 * The sound seek.
	 */
	private final int seek;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.network.world.packet.outgoing.OutgoingPacketBuilder#build(org.
	 * redrune.game.node.entity.player.Player)
	 */
	@Override
	public Packet build(Player player) {
		PacketBuilder builder = new PacketBuilder(72);
		builder.writeShort(soundId);
		builder.writeByte(repeat);
		builder.writeShort(delay);
		builder.writeByte(volume);
		builder.writeShort(seek);
		return builder.toPacket();
	}

}
