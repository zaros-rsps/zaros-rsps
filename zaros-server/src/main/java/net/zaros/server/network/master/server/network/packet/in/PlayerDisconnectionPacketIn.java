package net.zaros.server.network.master.server.network.packet.in;

import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.network.master.server.network.packet.out.LobbyRepositoryPacketOut;
import net.zaros.server.network.master.server.world.MSClans;
import net.zaros.server.network.master.server.world.MSRepository;
import net.zaros.server.network.master.server.world.MSWorld;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/12/2017
 */
@Readable(packetIds = { PacketConstants.PLAYER_DISCONNECTION_PACKET_ID })
public class PlayerDisconnectionPacketIn implements ReadablePacket<MSSession> {

	@Override
	public void read(MSSession session, IncomingPacket packet) {
		var userId = packet.readLong();
		var clanId = packet.readLong();
		var worldId = packet.readUnsignedShort();
		MSClans.updatePlayerWorld(clanId, userId, 0);
		MSRepository.getWorld(worldId).ifPresent(world -> {
			world.removePlayer(userId);

			// sends the repository update as long as the world isn't a lobby
			if (!world.isLobby()) {
				MSRepository.getWorlds().values().stream().filter(MSWorld::isLobby).forEach(lobbyWorld -> {
					lobbyWorld.getSession().write(new LobbyRepositoryPacketOut(world));
				});
			}
		});
	}
}
