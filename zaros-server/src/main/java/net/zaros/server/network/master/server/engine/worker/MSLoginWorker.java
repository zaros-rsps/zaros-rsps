package net.zaros.server.network.master.server.engine.worker;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.game.GameFlags;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.punishment.PunishmentType;
import net.zaros.server.network.master.server.engine.MSEngineWorker;
import net.zaros.server.network.master.server.network.packet.out.AccountCreationResponsePacketOut;
import net.zaros.server.network.master.server.network.packet.out.LoginResponsePacketOut;
import net.zaros.server.network.master.server.world.MSRepository;
import net.zaros.server.network.master.utility.LoginRequest;
import net.zaros.server.network.web.http.HTTPFunctions;
import net.zaros.server.network.web.sql.SQLFunctions;
import net.zaros.server.network.web.sql.impl.WebLoginDetail;
import net.zaros.server.utility.backend.CreationResponse;
import net.zaros.server.utility.backend.ReturnCode;
import net.zaros.server.utility.newrepository.database.players.PlayerRepository;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/12/2017
 */
@Slf4j
public final class MSLoginWorker extends MSEngineWorker {

	/**
	 * The queue of login requests
	 */
	private final Queue<LoginRequest> loginRequests = new LinkedBlockingQueue<>();

	@Override
	public void schedule(ScheduledExecutorService service) {
		service.scheduleWithFixedDelay(this, 0, 100, TimeUnit.MILLISECONDS);
		log.info("Scheduled the login worker!");
	}

	@Override
	public void run() {
		try {
			LoginRequest request;
			while ((request = loginRequests.poll()) != null) {
				if (request.isCreation()) {
					handleCreationRequest(request);
					continue;
				}
				var worldId = request.getWorldId();
				var lobby = request.isLobby();
				var username = request.getUsername();
				var password = request.getPassword();
				var session = request.getSession();
				var uid = request.getUuid();

				// if there is a user online with that name already
				boolean online;

				// the response code
				var returnCode = ReturnCode.SUCCESSFUL;

				if (lobby) {
					online = MSRepository.isOnline(username, true);
				} else {
					online = MSRepository.isOnline(username, false);
				}

				// if the player is online we change the return code
				if (online) {
					session.write(new LoginResponsePacketOut(uid, ReturnCode.ALREADY_ONLINE.getValue(), "empty".getBytes(), username, lobby, 0));
					return;
				}

				// we must have the player file before this stage
				if (!PlayerRepository.exists(username)) {
					session.write(new LoginResponsePacketOut(uid, ReturnCode.INVALID_ACCOUNT_REQUESTED.getValue(), "empty".getBytes(), username, lobby, 0));
					return;
				}

				// the id of the row that the player's data is in
				int rowId = 0;

				// as long as we're web integrated we will check the credentials with the
				// database
				if (GameFlags.webIntegrated) {
					var loginDetail = SQLFunctions.getLoginDetail(username, password);
					switch (loginDetail.getResponse()) {
					case NON_EXISTENT_USERNAME:
						returnCode = ReturnCode.INVALID_ACCOUNT_REQUESTED;
						break;
					case WRONG_CREDENTIALS:
						returnCode = ReturnCode.INVALID_CREDENTIALS;
						break;
					case SQL_ERROR:
						returnCode = ReturnCode.ERROR_LOADING_PROFILE;
						break;
					case CORRECT:
						returnCode = ReturnCode.SUCCESSFUL;
						break;
					}
					rowId = loginDetail.getRowId();
				}

				// the player , so we can load variables
				var player = PlayerRepository.load(username);

				if (player == null) {
					returnCode = ReturnCode.ERROR_LOADING_PROFILE;
				} else if (player.getVariables().hasPunishment(PunishmentType.BAN)) {
					returnCode = ReturnCode.ACCOUNT_DISABLED;
				}

				var serialised = "empty".getBytes();
				if (returnCode == ReturnCode.SUCCESSFUL) {
					serialised = PlayerRepository.serialise(player);
				}
				// the byte value
				var responseCode = returnCode.getValue();
				// send the response now
				session.write(new LoginResponsePacketOut(uid, responseCode, serialised, username, lobby, rowId));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Handles the creation request of an account
	 *
	 * @param request
	 *                The request
	 */
	private void handleCreationRequest(LoginRequest request) {
		// the username of the request
		final String username = request.getUsername();

		// the response
		CreationResponse response = CreationResponse.SUCCESSFUL;

		// there's already a file in the server by that name, we won't be making an
		// account
		if (PlayerRepository.exists(username)) {
			request.getSession().write(new AccountCreationResponsePacketOut(request.getUuid(), CreationResponse.ALREADY_TAKEN.getValue()));
			return;
		}

		// as long as integration is on
		if (GameFlags.webIntegrated) {
			WebLoginDetail detail = SQLFunctions.getLoginDetail(request.getUsername(), request.getPassword());
			switch (detail.getResponse()) {
			case NON_EXISTENT_USERNAME:
				if (!HTTPFunctions.registerUser(request.getUsername(), request.getPassword())) {
					log.warn("Unable to register user from request " + request);
					response = CreationResponse.BUSY_SERVER;
				} else {
					response = CreationResponse.SUCCESSFUL;
				}
				break;
			case WRONG_CREDENTIALS:
				response = CreationResponse.ALREADY_TAKEN;
				break;
			case SQL_ERROR:
				response = CreationResponse.BUSY_SERVER;
				break;
			}
		}

		// if we could make the account successfully, we will create an account in the
		// file server
		if (response == CreationResponse.SUCCESSFUL) {
			Player player = new Player(username);
			player.setCreationData();
			PlayerRepository.store(username, player);
		}
		request.getSession().write(new AccountCreationResponsePacketOut(request.getUuid(), response.getValue()));
	}

	/**
	 * Adds a request to the queue
	 *
	 * @param request
	 *                The request to add
	 */
	public void addRequest(LoginRequest request) {
		try {
			loginRequests.add(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
