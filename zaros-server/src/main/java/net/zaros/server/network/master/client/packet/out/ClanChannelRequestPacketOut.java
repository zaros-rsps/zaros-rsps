package net.zaros.server.network.master.client.packet.out;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * Represents the clan channel join/leave request packet.
 * 
 * @author Walied K. Yassen
 */
public final class ClanChannelRequestPacketOut extends WritablePacket {

	/**
	 * The requester player.
	 */
	private final Player player;

	/**
	 * the requested clan id.
	 */
	private final long clanId;

	/**
	 * Whether it is a join or a leave request
	 */
	private final boolean leave;

	/**
	 * Constructs a new {@link ClanChannelRequestPacketOut} type object instance.
	 * 
	 * @param player
	 *               the requester player.
	 * @param clanId
	 *               the requested clan id.
	 * @param leave
	 *               whether it is a join or a leave request.
	 */
	public ClanChannelRequestPacketOut(Player player, long clanId, boolean leave) {
		super(CLAN_CHANNEL_REQUEST_PACKET_ID);
		this.player = player;
		this.clanId = clanId;
		this.leave = leave;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.network.master.network.packet.writeable.WritablePacket#
	 * create()
	 */
	@Override
	public WritablePacket create() {
		writeLong(player.getUserId());
		writeLong(clanId);
		writeByte(leave ? 1 : 0);
		return this;
	}
}
