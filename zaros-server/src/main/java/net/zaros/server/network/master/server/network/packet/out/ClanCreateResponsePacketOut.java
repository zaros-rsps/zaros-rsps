package net.zaros.server.network.master.server.network.packet.out;

import net.zaros.server.network.master.network.packet.writeable.WritablePacket;
import net.zaros.server.network.master.server.world.MSClans.ClanCreateResponse;

/**
 * Represents the clan create response packet.
 * 
 * @author Walied K. Yassen
 */
public final class ClanCreateResponsePacketOut extends WritablePacket {

	/**
	 * The clan response.
	 */
	private final ClanCreateResponse response;

	/**
	 * The clan user id.
	 */
	private final long userId;

	/**
	 * The created clan id.
	 */
	private final long clanId;

	/**
	 * Constructs a new {@link ClanCreateResponsePacketOut} type object instance.
	 * 
	 * @param response
	 *                 the clan create response type.
	 * @param userId
	 *                 the requester user id.
	 */
	public ClanCreateResponsePacketOut(ClanCreateResponse response, long userId) {
		this(response, userId, -1);
	}

	/**
	 * Constructs a new {@link ClanCreateResponsePacketOut} type object instance.
	 * 
	 * @param response
	 *                 the clan create response type.
	 * @param userId
	 *                 the requester user id.
	 * @param clanId
	 *                 the created clan id.
	 */
	public ClanCreateResponsePacketOut(ClanCreateResponse response, long userId, long clanId) {
		super(CLAN_CREATE_RESPONSE_PACKET_ID);
		this.response = response;
		this.userId = userId;
		this.clanId = clanId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.network.master.network.packet.writeable.WritablePacket#
	 * create()
	 */
	@Override
	public WritablePacket create() {
		writeLong(userId);
		writeByte((byte) response.ordinal());
		if (response == ClanCreateResponse.CLAN_CREATED) {
			writeLong(clanId);
		}
		return this;
	}
}
