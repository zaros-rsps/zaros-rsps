package net.zaros.server.network.master.client.packet.in;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.network.master.client.MCFlags;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/11/2017
 */
@Readable(packetIds = { PacketConstants.SUCCESSFUL_VERIFICATION_PACKET_ID })
@Slf4j
public class SuccessfulVerificationIn implements ReadablePacket<MCSession> {

	@Override
	public void read(MCSession session, IncomingPacket packet) {
		var successful = packet.readByte();
		String message = packet.readString();
		if (successful == 1) {
			log.info(message.replaceAll("#", "" + MCFlags.worldId));
			session.setVerified(true);
		} else {
			log.warn("Unable to successfully verify our client. [" + successful + "]");
		}
	}
}
