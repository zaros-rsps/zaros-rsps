package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.Packet.PacketType;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/31/2017
 */
public class InterfaceStringBuilder implements OutgoingPacketBuilder {
	
	/**
	 * The id of the interface to change text on
	 */
	private final int interfaceId;
	
	/**
	 * The component id of the interface to change text on
	 */
	private final int componentId;
	
	/**
	 * The text we will store
	 */
	private final String text;
	
	public InterfaceStringBuilder(int interfaceId, int componentId, String text) {
		this.interfaceId = interfaceId;
		this.componentId = componentId;
		this.text = text;
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(69, PacketType.VAR_SHORT);
		bldr.writeInt(interfaceId << 16 | componentId);
		bldr.writeString(text);
		return bldr.toPacket();
	}
}
