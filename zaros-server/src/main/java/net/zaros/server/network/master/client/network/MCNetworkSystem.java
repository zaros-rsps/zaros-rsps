package net.zaros.server.network.master.client.network;

import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.Optional;

import com.google.common.base.Preconditions;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.zaros.server.network.master.MasterConstants;
import net.zaros.server.network.master.client.MCFlags;
import net.zaros.server.network.master.client.network.MCNetworkStatus.StatusChangeEvent;
import net.zaros.server.network.master.client.network.channel.MCChannelFuture;
import net.zaros.server.network.master.client.network.channel.MCChannelInitializer;
import net.zaros.server.network.master.client.packet.out.VerificationPacketOut;
import net.zaros.server.network.master.network.packet.OutgoingPacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/11/2017
 */
@Slf4j
public class MCNetworkSystem implements MasterConstants {

	/**
	 * The bootstrap
	 */
	@Getter
	private final Bootstrap bootstrap = new Bootstrap();

	/**
	 * The id of the world for this system
	 */
	@Getter
	private final int worldId;

	/**
	 * The status of the system
	 */
	@Getter
	private MCNetworkStatus status = MCNetworkStatus.NOT_CONNECTED;

	/**
	 * The session
	 */
	@Getter
	@Setter
	private MCSession session;

	/**
	 * Constructs a new network system
	 *
	 * @param worldId
	 *                The id of the world
	 */
	public MCNetworkSystem(int worldId) {
		this.worldId = MCFlags.worldId = worldId;
	}

	/**
	 * Connects the system to the server
	 */
	public void connect() {
		// the address to connect to
		final InetSocketAddress address = new InetSocketAddress(IP, PORT_ID);

		// builds the bootstrap
		bootstrap.group(new NioEventLoopGroup(1));
		bootstrap.channel(NioSocketChannel.class);
		bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
		bootstrap.handler(new MCChannelInitializer(this));

		// connects right after the bootstrap is built
		bootstrap.remoteAddress(address).connect().addListener(new MCChannelFuture(this, address));
	}

	/**
	 * Sets the status
	 *
	 * @param status
	 *               The status to set to
	 */
	public void setStatus(MCNetworkStatus status) {
		// makes sure we don't set a null status
		Preconditions.checkArgument(status != null, "The status is not allowed to be null.");
		// are the statuses different
		boolean different = !Objects.equals(status, this.status);

		// the previous status
		final MCNetworkStatus oldStatus = this.status;

		// we change the status
		this.status = status;

		// statuses didn't change so we dont need to fire an update
		if (!different) {
			return;
		}

		status.setSystem(this);

		// executes the leave event
		Optional.ofNullable(oldStatus.leaveEvent()).ifPresent(StatusChangeEvent::execute);

		// executes the set event
		Optional.ofNullable(status.setEvent()).ifPresent(StatusChangeEvent::execute);
	}

	/**
	 * Writes the packet
	 *
	 * @param packet
	 *               The packet
	 */
	public boolean write(OutgoingPacket packet) {
		if (session == null || !session.isConnected()) {
			log.warn("Unable to write a packet, session: {}", session);
			return false;
		}
		session.write(packet);
		return true;
	}

	/**
	 * Attempts to verify the session
	 */
	public void attemptVerification() {
		if (session.isVerified()) {
			return;
		}
		write(new VerificationPacketOut());
	}
}