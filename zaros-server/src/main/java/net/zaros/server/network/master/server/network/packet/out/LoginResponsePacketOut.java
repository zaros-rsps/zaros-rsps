package net.zaros.server.network.master.server.network.packet.out;

import net.zaros.server.game.GameConstants;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;
import net.zaros.server.utility.backend.SecureOperations;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/12/2017
 */
public class LoginResponsePacketOut extends WritablePacket {
	
	/**
	 * The uid of the session
	 */
	private final String uid;
	
	/**
	 * The response code
	 */
	private final byte responseCode;
	
	/**
	 * The username of the login request
	 */
	private final String username;
	
	/**
	 * If the response is to the lobby
	 */
	private final boolean lobby;
	
	/**
	 * The data of the file to transmit over the network
	 */
	private final byte[] data;
	
	/**
	 * The id of the row that the player's sql data is in
	 */
	private final int rowId;
	
	public LoginResponsePacketOut(String uid, byte responseCode, byte[] serialised, String username, boolean lobby, int rowId) {
		super(LOGIN_RESPONSE_PACKET_ID);
		this.uid = uid;
		this.responseCode = responseCode;
		this.username = username;
		this.lobby = lobby;
		this.rowId = rowId;
		data = SecureOperations.getCompressedEncrypted(serialised, GameConstants.FILE_ENCRYPTION_KEY);
	}
	
	@Override
	public WritablePacket create() {
		writeString(uid);
		writeByte(responseCode);
		writeByte((byte) (lobby ? 1 : 0));
		writeString(username);
		writeInt(rowId);
		writeInt(data.length);
		for (byte datum : data) {
			writeByte(datum);
		}
		return this;
	}
}
