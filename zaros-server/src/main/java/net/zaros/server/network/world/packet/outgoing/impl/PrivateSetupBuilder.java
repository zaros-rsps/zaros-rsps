package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;
import net.zaros.server.utility.rs.constant.GameBarStatus;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/9/2017
 */
public class PrivateSetupBuilder implements OutgoingPacketBuilder {
	
	/**
	 * The flag to send
	 */
	private final byte flag;
	
	public PrivateSetupBuilder(GameBarStatus bar) {
		this.flag = bar.getValue();
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(104);
		bldr.writeByte(flag);
		return bldr.toPacket();
	}
}
