package net.zaros.server.network.master.client.packet.out;

import net.zaros.server.game.GameFlags;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * Represents the player login complete packet.
 * 
 * @author Walied K. Yassen
 */
public final class PlayerLoginCompletePacketOut extends WritablePacket {

	/**
	 * The logged in player.
	 */
	private final Player player;

	/**
	 * Constructs a new {@link PlayerLoginCompletePacketOut} type object instance.
	 * 
	 * @param player
	 *               the logged in player.
	 */
	public PlayerLoginCompletePacketOut(Player player) {
		super(PacketConstants.LOGIN_COMPLETE_PACKET_ID);
		this.player = player;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.network.master.network.packet.writeable.WritablePacket#
	 * create()
	 */
	@Override
	public WritablePacket create() {
		writeShort(GameFlags.worldId);
		writeLong(player.getUserId());
		writeLong(player.getCurrentClanId());
		writeString(player.getDetails().getUsername());
		writeString(player.getSession().getUid());
		return this;
	}
}
