package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/26/2017
 */
public class SystemUpdateBuilder implements OutgoingPacketBuilder {
	
	/**
	 * Updating time processEffects.
	 */
	private final int time;
	
	public SystemUpdateBuilder(int time) {
		this.time = time;
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(10);
		bldr.writeShort(125);
		bldr.writeShort((int) (time * 1.6));
		return bldr.toPacket();
	}
}
