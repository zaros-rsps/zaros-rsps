package net.zaros.server.network.world.packet.incoming.impl;

import net.zaros.server.game.content.event.EventRepository;
import net.zaros.server.game.content.event.context.CommandEventContext;
import net.zaros.server.game.content.event.impl.CommandEvent;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/19/2017
 */
public class CommandHandlerPacketDecoder implements IncomingPacketDecoder {
	
	@Override
	public int[] bindings() {
		return Misc.arguments(12);
	}
	
	@Override
	public void read(Player player, Packet packet) {
		if (packet.getBuffer().readableBytes() < 1) {
			return;
		}
		packet.readUnsignedByte();
		packet.readUnsignedByte();
		String command = packet.readRS2String();
		String[] args = command.toLowerCase().split(" ");
		EventRepository.executeEvent(player, CommandEvent.class, new CommandEventContext(args, true));
	}
}
