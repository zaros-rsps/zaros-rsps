package net.zaros.server.network.master.client.packet.in;

import java.util.HashSet;

import net.zaros.server.game.world.World;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;

/**
 * Represents the clan channel delta incoming packet.
 * 
 * @author Walied K. Yassen
 */
@Readable(packetIds = { PacketConstants.CLAN_CHANNEL_DELTA_BLOCK_PACKET_ID })
public final class ClanChannelDeltaPacketIn implements ReadablePacket<MCSession> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.master.network.packet.readable.ReadablePacket#read(
	 * net.zaros.server.network.master.network.MasterSession,
	 * net.zaros.server.network.master.network.packet.IncomingPacket)
	 */
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		var ids = new HashSet<Long>();
		var count = packet.readUnsignedShort();
		while (count-- > 0) {
			ids.add(packet.readLong());
		}
		var block = new byte[packet.readInt()];
		packet.read(block);
		var players = World.getPlayersByIds(ids);
		for (var player : players) {
			player.getTransmitter().sendClanChannelDelta(block);
		}
	}
}
