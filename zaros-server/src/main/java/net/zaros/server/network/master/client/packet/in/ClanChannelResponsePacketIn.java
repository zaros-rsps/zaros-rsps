package net.zaros.server.network.master.client.packet.in;

import net.zaros.server.game.world.World;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.world.MSClans.ClanChannelResponse;

/**
 * Represents the clan channel response packet handler.
 * 
 * @author Walied K. Yassen
 */
@Readable(packetIds = { PacketConstants.CLAN_CHANNEL_RESPONSE_PACKET_ID })
public final class ClanChannelResponsePacketIn implements ReadablePacket<MCSession> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.master.network.packet.readable.ReadablePacket#read(
	 * net.zaros.server.network.master.network.MasterSession,
	 * net.zaros.server.network.master.network.packet.IncomingPacket)
	 */
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		// read the response.
		var response = ClanChannelResponse.values()[packet.readByte()];
		// read the identifiers.
		var userId = packet.readLong();
		@SuppressWarnings("unused")
		var clanId = packet.readLong();
		// read the attachment block.
		var attachmentSize = packet.readShort();
		var attachment = new byte[attachmentSize];
		packet.read(attachment);
		// grab the world player.
		var player = World.getPlayerById(userId);
		if (response == ClanChannelResponse.CHANNEL_JOINED || response == ClanChannelResponse.CHANNEL_LEFT) {
			if (player.isPresent()) {
				player.get().getTransmitter().sendClanChannelFull(attachment);
			}
		}
	}
}
