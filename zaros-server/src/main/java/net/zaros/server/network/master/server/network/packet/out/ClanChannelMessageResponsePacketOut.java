package net.zaros.server.network.master.server.network.packet.out;

import java.util.Set;

import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;
import net.zaros.server.network.master.server.world.MSClans.ClanChannelMessageResponse;

/**
 * Represents the clan channel message response outgoing packet builder.
 * 
 * @author Walied K. Yassen
 */
public final class ClanChannelMessageResponsePacketOut extends WritablePacket {

	/**
	 * The clan chanel message response type.
	 */
	private final ClanChannelMessageResponse response;

	/**
	 * The user id(s) which are going to receive the message.
	 */
	private final Set<Long> userIds;

	/**
	 * The sender player display name.
	 */
	private final String displayName;

	/**
	 * The sender player moderator level.
	 */
	private final int modLevel;

	/**
	 * The message text content.
	 */
	private final String message;

	/**
	 * Constructs a new {@link ClanChannelMessageResponsePacketOut} type object
	 * instance.
	 * 
	 * @param response
	 *                    the clan channel message response type.
	 * @param userIds
	 *                    the user id(s) which should receive this message.
	 * @param displayName
	 *                    the sender player display name .
	 * @param modLevel
	 *                    the sender player moderator level.
	 * @param message
	 *                    the message text content.
	 */
	public ClanChannelMessageResponsePacketOut(ClanChannelMessageResponse response, Set<Long> userIds, String displayName, int modLevel, String message) {
		super(PacketConstants.CLAN_CHANNEL_MESSAGE_RESPONSE_PACKET_ID);
		this.response = response;
		this.userIds = userIds;
		this.displayName = displayName;
		this.modLevel = modLevel;
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.network.master.network.packet.writeable.WritablePacket#
	 * create()
	 */
	@Override
	public WritablePacket create() {
		writeByte(response.ordinal());
		if (response == ClanChannelMessageResponse.SUCCESSFUL) {
			writeShort(userIds.size());
			for (var userId : userIds) {
				writeLong(userId);
			}
			writeString(displayName);
			writeByte(modLevel);
			writeString(message);
		}
		return this;
	}
}
