package net.zaros.server.network.master.client.packet.in;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.game.clan.manager.ClanManager;
import net.zaros.server.game.world.World;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.world.MSClans.ClanCreateResponse;

/**
 * Represents the clan creation response handler packet.
 * 
 * @author Walied K. Yassen
 */
@Slf4j
@Readable(packetIds = { PacketConstants.CLAN_CREATE_RESPONSE_PACKET_ID })
public final class ClanCreateResponsePacketIn implements ReadablePacket<MCSession> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.master.network.packet.readable.ReadablePacket#read(
	 * net.zaros.server.network.master.network.MasterSession,
	 * net.zaros.server.network.master.network.packet.IncomingPacket)
	 */
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		var userId = packet.readLong();
		var response = ClanCreateResponse.values()[packet.readByte()];
		var player = World.getPlayerById(userId).get();
		if (response == ClanCreateResponse.CLAN_CREATED) {
			var clanId = packet.readLong();
			player.setCurrentClanId(clanId);
			player.setJoinedClanChannel(true);
			ClanManager.requestChannelJoin(player, clanId);
		} else {
			log.warn("Unhandled ClanResponse in {}, response: {}", getClass().getSimpleName(), response);
		}
	}
}
