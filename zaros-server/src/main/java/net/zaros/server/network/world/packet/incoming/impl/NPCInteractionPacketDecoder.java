package net.zaros.server.network.world.packet.incoming.impl;

import net.zaros.server.game.GameFlags;
import net.zaros.server.game.content.combat.CombatManager;
import net.zaros.server.game.content.combat.data.magic.SpellSelection;
import net.zaros.server.game.content.combat.script.CombatScript;
import net.zaros.server.game.content.combat.script.CombatScriptSelection;
import net.zaros.server.game.content.event.EventRepository;
import net.zaros.server.game.content.event.context.NPCEventContext;
import net.zaros.server.game.content.event.context.NodeReachEventContext;
import net.zaros.server.game.content.event.impl.NPCEvent;
import net.zaros.server.game.content.event.impl.NodeReachEvent;
import net.zaros.server.game.content.skills.magic.Spell;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.repository.npc.spawn.NPCSpawnRepository;
import net.zaros.server.utility.rs.InteractionOption;

import static net.zaros.server.utility.rs.InteractionOption.*;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/31/2017
 */
public class NPCInteractionPacketDecoder implements IncomingPacketDecoder {

	/**
	 * The packet id for option
	 */
	private static final int FIRST_NPC_OPTION = 29, SECOND_NPC_OPTION = 10, THIRD_NPC_OPTION = 69, FOURTH_NPC_OPTION = 61, ATTACK_NPC_OPTION = 70, LAST_NPC_OPTION = 27, NPC_INTERFACE_USAGE = 57;

	@Override
	public int[] bindings() {
		return arguments(FIRST_NPC_OPTION, SECOND_NPC_OPTION, THIRD_NPC_OPTION, FOURTH_NPC_OPTION, ATTACK_NPC_OPTION, LAST_NPC_OPTION, NPC_INTERFACE_USAGE);
	}

	@Override
	public void read(Player player, Packet packet) {
		switch (packet.getOpcode()) {
		case NPC_INTERFACE_USAGE:
			readInterfaceUsagePacket(player, packet);
			break;
		default:
			readNPCOptionPacket(player, packet);
			break;
		}
	}

	/**
	 * Reads the packet that is sent when using an interface on an npc
	 *
	 * @param player
	 *               The player
	 * @param packet
	 *               The packet
	 */
	@SuppressWarnings("unused")
	private void readInterfaceUsagePacket(Player player, Packet packet) {
		boolean running = packet.readByteS() == 1;
		int slot = packet.readLEShort();
		int interfaceHash = packet.readInt1();
		int id = packet.readLEShort();
		int index = packet.readLEShort();

		int interfaceId = interfaceHash >> 16;
		int componentId = interfaceHash & 0xFFFF;

		if (index < 0 || index > Short.MAX_VALUE) {
			return;
		}
		NPC npc = World.getNpcs().get(index);
		if (npc == null || npc.isDead() || !npc.isRenderable() || !player.getMapRegionsIds().contains(npc.getRegion().getRegionId())) {
			return;
		}

		switch (interfaceId) {
			case 192: // regular
			case 193: // ancients

				Spell spell = SpellSelection.getSpell(player, componentId);

				if (spell != null) {
					player.faceEntity(npc);
					player.putTemporaryAttribute(AttributeKey.CURRENT_SPELL, spell);
					CombatManager.startCombat(player, npc);

					CombatScript script = CombatScriptSelection.get(player);

					if (CombatManager.isWithinDistance(player, npc, script, false)) {
						player.getMovement().resetWalkSteps();
					}

					return;
				}
				break;
			default:
				System.out.println("Interface on npc [" + interfaceId + "," + componentId + "] unhandled");
				break;
		}
	}

	/**
	 * Reads the packet that is sent when clicking an option on an npc
	 *
	 * @param player
	 *               The player
	 * @param packet
	 *               The packet
	 */
	private void readNPCOptionPacket(Player player, Packet packet) {
		int index = packet.readLEShortA();
		boolean forceRun = packet.readByteC() == 1;

		if (index < 0) {
			System.out.println("Invalid npc index found: " + index);
			return;
		}

		NPC npc = World.getNpcs().get(index);

		// for some reason this npc was found, we still shouldn't interact with it...
		if (npc == null || !npc.isRenderable()) {
			return;
		}
		InteractionOption option = getOptionByOpcode(packet.getOpcode());
		if (option == null) {
			System.out.println("Unable to identify interaction option for opcode " + packet.getOpcode());
			return;
		}

		int optionIndex = getOption(packet.getOpcode());

		// different options handled differently
		switch (option) {
		case ATTACK_OPTION:
			// stop everything
			player.stop(true, true, true, false);
			// face the player
			player.faceEntity(npc);
			// make sure we aren't at multi

			if (!npc.getCombatDefinitions().isForceMultiAttacked()) {
				if (!npc.getArea().isMulti() || !player.getArea().isMulti()) {
					if (player.getAttackedBy() != npc && player.getAttackedByDelay() > System.currentTimeMillis()) {
						player.getTransmitter().sendMessage("You are already in combat.");
						return;
					}
					if (npc.getAttackedBy() != player && npc.getAttackedByDelay() > System.currentTimeMillis()) {
						player.getTransmitter().sendMessage("This npc is already in combat.");
						return;
					}
				}
			}
			// make sure we can fight in the activity
			if (player.getManager().getActivities().handlesNodeInteraction(npc, InteractionOption.ATTACK_OPTION)) {
				return;
			}
			CombatManager.startCombat(player, npc);

			CombatScript script = CombatScriptSelection.get(player);

			if (CombatManager.isWithinDistance(player, npc, script, false)) {
				player.getMovement().resetWalkSteps();
			}

			break;
		case EXAMINE:
			if (player.getTemporaryAttribute("remove_npc_spawns", false)) {
				if (NPCSpawnRepository.removeNPC(npc)) {
					npc.deregister();
				} else {
					player.getTransmitter().sendMessage("Couldn't remove this npc!");
				}
			} else {
				if (GameFlags.debugMode) {
					player.getTransmitter().sendMessage(npc.toString(), true);
					System.out.println(npc.toString());
				}
			}
			break;
		default:
			player.getMovement().reset(forceRun);
			EventRepository.executeEvent(player, NodeReachEvent.class, new NodeReachEventContext(npc, () -> {
				// executing the npc interaction event on arrival
				EventRepository.executeEvent(player, NPCEvent.class, new NPCEventContext(npc, option, optionIndex));
			}));
			break;
		}
	}

	/**
	 * Gets the {@link InteractionOption} {@code Object} for the option clicked on
	 * the {@code NPC}
	 *
	 * @param opcode
	 *               The option's opcode
	 */
	private InteractionOption getOptionByOpcode(int opcode) {
		switch (opcode) {
		case ATTACK_NPC_OPTION:
			return ATTACK_OPTION;
		case FIRST_NPC_OPTION:
			return FIRST_OPTION;
		case SECOND_NPC_OPTION:
			return SECOND_OPTION;
		case THIRD_NPC_OPTION:
			return THIRD_OPTION;
		case FOURTH_NPC_OPTION:
			return FOURTH_OPTION;
		case LAST_NPC_OPTION:
			return EXAMINE;
		default:
			return null;
		}
	}

	private int getOption(int opcode) {
		switch (opcode) {
		case ATTACK_NPC_OPTION:
			return 0;
		case FIRST_NPC_OPTION:
			return 1;
		case SECOND_NPC_OPTION:
			return 2;
		case THIRD_NPC_OPTION:
			return 3;
		case FOURTH_NPC_OPTION:
			return 4;
		case LAST_NPC_OPTION:
			return 5;
		default:
			return -1;
		}
	}
}
