package net.zaros.server.network.master.server.network.packet.in;

import java.util.Optional;

import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.network.master.server.network.packet.out.FriendDetailsCompletePacketOut;
import net.zaros.server.network.master.server.world.MSRepository;
import net.zaros.server.network.master.server.world.MSWorld;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/15/2017
 */
@Readable(packetIds = { PacketConstants.FRIEND_DETAILS_REQUEST_PACKET_ID })
public class FriendDetailsRequestPacketIn implements ReadablePacket<MSSession> {

	@Override
	public void read(MSSession session, IncomingPacket packet) {
		var owner = packet.readString();
		var ownerWorldId = packet.readUnsignedShort();
		var requestedName = packet.readString();
		var details = MSRepository.getPlayerDetails(requestedName);
		var online = (boolean) details[1];
		var requestedWorldId = (int) details[2];
		Optional<MSWorld> optional = MSRepository.getWorld(ownerWorldId);
		if (optional.isPresent()) {
			optional.get().getSession().write(new FriendDetailsCompletePacketOut(owner, requestedName, requestedWorldId, online, requestedWorldId == 0 ? false : MSRepository.getWorld(requestedWorldId).get().isLobby()));
		} else {
			System.out.println("Unable to find world by " + ownerWorldId);
		}
	}
}
