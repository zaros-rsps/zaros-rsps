package net.zaros.server.network.master;

import io.netty.util.AttributeKey;
import net.zaros.server.network.NetworkConstants;
import net.zaros.server.network.master.network.MasterSession;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/10/2017
 */
public interface MasterConstants {
	
	/**
	 * The ip of the master server to connect to
	 */
	String IP = "127.0.0.1";
	
	/**
	 * The id of the port we're listening on
	 */
	int PORT_ID = NetworkConstants.MASTER_SERVER_PORT_ID;
	
	/**
	 * The message you find when you connect successfully and were verified
	 */
	String WELCOME_MESSAGE = "Welcome to the Master Server, world #! Your connection has been verified successfully!";
	
	/**
	 * The verification key.
	 */
	String VERIFICATION_KEY = ":srpU6Yv2D/Nyb8%";
	
	/**
	 * The attribute that contains the key for a session.
	 */
	AttributeKey<MasterSession> SESSION_KEY = AttributeKey.valueOf("session.key");
}
