/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.network.world.packet.outgoing.impl;

import lombok.Value;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * Represents a MIDI jingle packet builder.
 * 
 * @author Walied K. Yassen
 */
@Value
public final class MidiJinglePacketBuilder implements OutgoingPacketBuilder {

	/**
	 * The jingle song id.
	 */
	private final int jingleId;

	/**
	 * The jingle volume.
	 */
	private final int volume;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.network.world.packet.outgoing.OutgoingPacketBuilder#build(org.
	 * redrune.game.node.entity.player.Player)
	 */
	@Override
	public Packet build(Player player) {
		PacketBuilder builder = new PacketBuilder(39);
		builder.writeMedium(0);
		builder.writeLEShort(jingleId);
		builder.writeByte(volume);
		player.getTransmitter().send(builder.toPacket());
		return null;
	}

}
