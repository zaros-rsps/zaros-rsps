package net.zaros.server.network.master.server.network.packet.out;

import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * Represents the clan settings block outgoing packet.
 * 
 * @author Walied K. Yassen
 */
public final class ClanSettingsBlockPacketOut extends WritablePacket {

	/**
	 * The user id which we are sending the block to.
	 */
	private final long userId;

	/**
	 * The clan settings block.
	 */
	private final byte[] block;

	/**
	 * Constructs a new {@link ClanSettingsBlockPacketOut} type object instance.
	 * 
	 * @param userId
	 *               the user id which we are sending the block to.
	 * @param block
	 *               the clan settings block.
	 */
	public ClanSettingsBlockPacketOut(long userId, byte[] block) {
		super(PacketConstants.CLAN_SETTINGS_BLOCK_PACKET_ID);
		this.userId = userId;
		this.block = block;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.network.master.network.packet.writeable.WritablePacket#
	 * create()
	 */
	@Override
	public WritablePacket create() {
		writeLong(userId);
		if (block != null) {
			writeInt(block.length);
			write(block);
		} else {
			writeInt(0);
		}
		return this;
	}
}
