package net.zaros.server.network.world.packet.incoming.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/27/2017
 */
public class InterfaceClosedPacketDecoder implements IncomingPacketDecoder {
	
	@Override
	public int[] bindings() {
		return Misc.arguments(64);
	}
	
	@Override
	public void read(Player player, Packet packet) {
		player.getManager().getInterfaces().closeAllInterfaces();
	}
}
