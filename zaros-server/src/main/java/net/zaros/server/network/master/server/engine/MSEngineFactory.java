package net.zaros.server.network.master.server.engine;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import lombok.Getter;
import net.zaros.server.core.system.SystemManager;
import net.zaros.server.network.master.server.engine.worker.MSLoginWorker;
import net.zaros.server.network.master.utility.LoginRequest;
import net.zaros.server.utility.backend.RS2ThreadFactory;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/12/2017
 */
public class MSEngineFactory {
	
	/**
	 * The scheduled executor service
	 */
	@Getter
	private static final ScheduledExecutorService scheduler = (ScheduledExecutorService) Services.SCHEDULED.getService();
	
	/**
	 * The login worker
	 */
	private static MSLoginWorker loginWorker;
	
	/**
	 * Loads all engine factory workers
	 */
	public static void startUp() {
		(loginWorker = new MSLoginWorker()).schedule(scheduler);
	}
	
	/**
	 * Adds a request to the worker's queue
	 *
	 * @param request
	 * 		The request to add
	 */
	public static void addLoginRequest(LoginRequest request) {
		loginWorker.addRequest(request);
	}
	
	/**
	 * The enum of services we may have
	 */
	private enum Services {
		
		SCHEDULED {
			@Override
			ExecutorService getService() {
				return SystemManager.PROCESSOR_COUNT >= 6 ? Executors.newScheduledThreadPool(SystemManager.PROCESSOR_COUNT >= 12 ? 4 : 2, new RS2ThreadFactory("Scheduled-Worker")) : Executors.newSingleThreadScheduledExecutor(new RS2ThreadFactory("Scheduled-Worker"));
			}
		};
		
		abstract ExecutorService getService();
	}
	
}
