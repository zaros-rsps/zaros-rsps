package net.zaros.server.network.world.packet.incoming.impl;

import net.zaros.cache.Cache;
import net.zaros.cache.util.Utils;
import net.zaros.server.game.content.event.EventRepository;
import net.zaros.server.game.content.event.context.item.ItemOnItemContext;
import net.zaros.server.game.content.event.context.magic.MagicOnItemContext;
import net.zaros.server.game.content.event.impl.item.ItemOnItemEvent;
import net.zaros.server.game.content.event.impl.magic.MagicOnItemEvent;
import net.zaros.server.game.module.ModuleRepository;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;
import net.zaros.server.utility.rs.ButtonOption;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/22/2017
 */
public class InterfaceClickPacketDecoder implements IncomingPacketDecoder {

	@Override
	public int[] bindings() {
		return Misc.arguments(85, 7, 66, 11, 48, 17, 84, 40, 25, 8, 54, 26);
	}

	@Override
	public void read(Player player, Packet packet) {
		try {
			switch (packet.getOpcode()) {
			case 8:
				decodeDialoguePacket(player, packet);
				break;
			case 26:
				decodeItemOnItemPacket(player, packet);
				break;
			default:
				int clickData = packet.readLEInt();
				int interfaceId = clickData & 0xFFF;
				int componentId = clickData >> 16;
				int itemId = packet.readShortA();
				int slotId = packet.readShortA();
				if (player.getDetails().isStaff()) {
					StringBuilder bldr = new StringBuilder("[interfaceId=" + interfaceId + ", componentId=" + componentId + "");
					bldr.append(itemId == -1 ? "" : ", itemId=" + itemId + "");
					bldr.append(slotId == -1 ? "" : ", slotId=" + slotId + "");
					bldr.append(", packetId=").append(packet.getOpcode()).append("]");
					player.getTransmitter().sendMessage(bldr.toString(), false);
				}
				if (itemId == 65535) {
					itemId = -1;
				}
				if (slotId == 65535) {
					slotId = -1;
				}
				if (interfaceId > Utils.getInterfaceDefinitionsSize(Cache.getStore())) {
					System.out.println("Unable to handle interface post-decoding! (" + interfaceId + ", " + componentId + ") [packetId=" + packet.getOpcode() + "]");
					return;
				}
				if (!player.getManager().getInterfaces().hasOpenInterface(interfaceId)) {
					System.out.println("Interface " + interfaceId + ", [" + componentId + "] was not existent in the player's mapping of opened interface.");
					return;
				}

				if (player.getManager().getActivities().handleButton(interfaceId, componentId, slotId, itemId, getOption(packet.getOpcode()))) {
					return;
				}
				/*
				 * if (SkillProducing.handleSkillButtons(player, interfaceId, componentId)) {
				 * return; }
				 */
				if (ModuleRepository.handle(player, interfaceId, componentId, itemId, slotId, packet.getOpcode())) {
					return;
				}
				break;
			}
		} catch (Exception e) {
			System.out.println("Error reading packet: " + packet.getOpcode());
			e.printStackTrace();
		}
	}

	/**
	 * Decodes an incoming dialogue
	 *
	 * @param player
	 *               The player
	 * @param packet
	 *               The packet
	 */
	private void decodeDialoguePacket(Player player, Packet packet) {
		int interfaceHash = packet.readInt2();
		packet.readLEShortA();
		int interfaceId = interfaceHash >> 16;
		int componentId = interfaceHash & 0xFF;
		if (interfaceId > Utils.getInterfaceDefinitionsSize(Cache.getStore())) {
			System.out.println("Unable to handle interface post-decoding! (" + interfaceId + ", " + componentId + ")");
			return;
		}
		if (!player.getManager().getInterfaces().hasOpenInterface(interfaceId)) {
			System.out.println("Interface " + interfaceId + ", [" + componentId + "] was not existent in the player's mapping of opened interface.");
			return;
		}
		if (ModuleRepository.handle(player, interfaceId, componentId, -1, -1, packet.getOpcode())) {
			return;
		}
		player.getManager().getDialogues().handleOption(interfaceId, componentId);
	}

	/**
	 * Decodes the packet that is sent when two items are used together
	 *
	 * @param player
	 *               The player
	 * @param packet
	 *               The packet
	 */
	private void decodeItemOnItemPacket(Player player, Packet packet) {
		int usedWithId = packet.readLEShortA();
		int usedWithSlot = packet.readLEShortA();
		int usedSlot = packet.readLEShortA();
		int interface_id = packet.readLEInt() >> 16;
		int interface_child = packet.readLEInt() & 0xfff;
		int usedId = packet.readShortA();

		System.out.println(usedWithId + ", " + usedWithSlot + ", " + usedSlot + ", " + usedId + ", " + interface_id + ", " + interface_child);

		if (usedSlot == 65535) {
			EventRepository.executeEvent(player, MagicOnItemEvent.class, new MagicOnItemContext(interface_id, interface_child, usedWithSlot));
			return;
		}

		Item itemUsed = player.getInventory().getItems().get(usedSlot);
		if (itemUsed == null) {
			return;
		}
		Item usedWith = player.getInventory().getItems().get(usedWithSlot);
		if (usedWith == null) {
			return;
		}
		if (usedId != itemUsed.getId() || usedWithId != usedWith.getId()) {
			System.out.println("Error in parsing item on item...");
			return;
		}

		EventRepository.executeEvent(player, ItemOnItemEvent.class, new ItemOnItemContext(usedSlot, usedWithSlot));
	}

	private ButtonOption getOption(int packetId) {
		switch (packetId) {
		case 85:
			return ButtonOption.FIRST;
		case 7:
			return ButtonOption.SECOND;
		case 66:
			return ButtonOption.THIRD;
		case 11:
			return ButtonOption.FOURTH;
		case 48:
			return ButtonOption.FIFTH;
		case 17:
			return ButtonOption.SIXTH;
		case 84:
			return ButtonOption.SEVENTH;
		case 40:
			return ButtonOption.EIGHTH;
		case 25:
			return ButtonOption.NINTH;
		case 54:
			return ButtonOption.TENTH;
		default:
			return null;
		}
	}
}
