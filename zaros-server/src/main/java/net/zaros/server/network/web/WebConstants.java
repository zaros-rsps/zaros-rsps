package net.zaros.server.network.web;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/15/2017
 */
public final class WebConstants {
	
	/**
	 * The url of the website
	 */
	public static final String WEBSITE_URL = "http://zaros.rs";
	
}
