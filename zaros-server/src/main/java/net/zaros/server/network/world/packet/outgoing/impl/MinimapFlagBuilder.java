package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/7/2017
 */
public class MinimapFlagBuilder implements OutgoingPacketBuilder {
	
	/**
	 * The x to write the flag on
	 */
	private final int flagX;
	
	/**
	 * The y to write the flag on
	 */
	private final int flagY;
	
	public MinimapFlagBuilder(int flagX, int flagY) {
		this.flagX = flagX;
		this.flagY = flagY;
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(16);
		bldr.writeByteC(flagX).writeByteC(flagY);
		return bldr.toPacket();
	}
}
