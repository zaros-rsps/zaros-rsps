/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.network.world.packet.incoming.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;

/**
 * Represents the map ready packet decoder.
 * 
 * @author Walied K. Yassen
 */
public final class MapReadyPacketDecoder implements IncomingPacketDecoder {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.network.world.packet.incoming.IncomingPacketDecoder#bindings()
	 */
	@Override
	public int[] bindings() {
		return new int[] { 0 };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.network.world.packet.incoming.IncomingPacketDecoder#read(org.
	 * redrune.game.node.entity.player.Player,
	 * org.redrune.network.world.packet.Packet)
	 */
	@Override
	public void read(Player player, Packet packet) {
		player.getTransmitter().sendMessage("Build Complete!");
	}

}
