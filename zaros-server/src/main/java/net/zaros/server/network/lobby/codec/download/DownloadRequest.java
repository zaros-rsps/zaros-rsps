package net.zaros.server.network.lobby.codec.download;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import net.zaros.cache.Cache;
import net.zaros.cache.util.BufferUtils;
import net.zaros.server.network.world.packet.PacketBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/19/2017
 */
public class DownloadRequest {

	/**
	 * The index id of the download request
	 */
	@Getter
	private final int indexId;

	/**
	 * The archive the request is in
	 */
	@Getter
	private final int archiveId;

	/**
	 * The priority of the request
	 */
	@Getter
	private final int priority;

	public DownloadRequest(int indexId, int archiveId, int priority) {
		this.indexId = indexId;
		this.archiveId = archiveId;
		this.priority = priority;
	}

	/**
	 * Sends the request to the channel
	 *
	 * @param ctx
	 *            The channel handler context
	 */
	public void push(ChannelHandlerContext ctx) {
		try {
			PacketBuilder builder = new PacketBuilder();
			builder.writeBytes(generateFile(indexId, archiveId, priority));
			ctx.writeAndFlush(builder.getBuffer());
		} catch (Exception e) {
			System.err.println("Error while serving request: " + this);
			e.printStackTrace();
		}
	}

	public static ByteBuf generateFile(int indexId, int archiveId, int opcode) {
		byte[] cacheFile = getFile(indexId, archiveId);
		if (cacheFile == null) {
			return Unpooled.buffer();
		}
		int compression = cacheFile[0] & 0xFF;
		int length = BufferUtils.readInt(1, cacheFile);
		int attributes = compression;
		boolean priority = opcode == 1;
		if (!priority) {
			attributes |= 0x80;
		}
		ByteBuf outBuffer = Unpooled.buffer();
		outBuffer.writeByte((byte) indexId);
		outBuffer.writeInt(archiveId);
		outBuffer.writeByte((byte) attributes);
		outBuffer.writeInt(length);
		int realLength = compression != 0 ? length + 4 : length;
		for (int offset = 5; offset < realLength + 5; offset++) {
			if (outBuffer.writerIndex() % 512 == 0) {
				outBuffer.writeByte((byte) 255);
			}
			outBuffer.writeByte(cacheFile[offset]);
		}
		return outBuffer;
	}

	public static byte[] getFile(int cache, int id) {
		if (cache == 255 && id == 255) {
			return Cache.getVersionTable();
		}
		if (cache == 255) {
			return Cache.getStore().getIndex255().getArchiveData(id);
		}
		return Cache.getStore().getIndexes()[cache].getMainFile().getArchiveData(id);
	}

	@Override
	public String toString() {
		return "DownloadRequest{" + "indexId=" + indexId + ", archiveId=" + archiveId + ", priority=" + priority + '}';
	}
}
