package net.zaros.server.network.master.server.network.packet.in;

import java.util.Optional;

import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.network.master.server.network.packet.out.LobbyRepositoryPacketOut;
import net.zaros.server.network.master.server.world.MSClans;
import net.zaros.server.network.master.server.world.MSRepository;
import net.zaros.server.network.master.server.world.MSWorld;

/**
 * @author Walied K. Yassen
 */
@Readable(packetIds = { PacketConstants.LOGIN_COMPLETE_PACKET_ID })
public final class PlayerLoginCompletePacketIn implements ReadablePacket<MSSession> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.master.network.packet.readable.ReadablePacket#read(
	 * net.zaros.server.network.master.network.MasterSession,
	 * net.zaros.server.network.master.network.packet.IncomingPacket)
	 */
	@Override
	public void read(MSSession session, IncomingPacket packet) {
		var worldId = packet.readUnsignedShort();
		var userId = packet.readLong();
		var clanId = packet.readLong();
		var username = packet.readString();
		var uid = packet.readString();
		// if we had a successful login, we can then add the player to the world
		// as long as they are connecting to a world, not the lobby.
		// add the player and update the lobby if we can
		Optional<MSWorld> optional = MSRepository.getWorld(worldId);
		optional.ifPresent(world -> {
			MSClans.updatePlayerWorld(clanId, userId, worldId);
			world.addPlayer(username, uid, userId);
			// sends the repository update as long as the world isn't the lobby world
			if (!world.isLobby()) {
				MSRepository.getWorlds().values().stream().filter(MSWorld::isLobby).forEach((lobbyWorld) -> {
					lobbyWorld.getSession().write(new LobbyRepositoryPacketOut(world));
				});
			}
		});
	}

}
