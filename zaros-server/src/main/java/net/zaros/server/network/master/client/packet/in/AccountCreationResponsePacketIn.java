package net.zaros.server.network.master.client.packet.in;

import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.client.packet.responsive.ResponsiveCreationPacket;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/16/2017
 */
@Readable(packetIds = { PacketConstants.ACCOUNT_CREATION_RESPONSE_PACKET_ID })
public class AccountCreationResponsePacketIn implements ReadablePacket<MCSession> {
	
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		String uid = packet.readString();
		byte responseId = (byte) packet.readByte();
		
		MasterCommunication.read(new ResponsiveCreationPacket(uid, responseId));
	}
}
