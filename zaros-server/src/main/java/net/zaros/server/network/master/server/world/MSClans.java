package net.zaros.server.network.master.server.world;

import java.util.Optional;

import net.zaros.server.game.clan.Clan;
import net.zaros.server.game.clan.ClanRank;
import net.zaros.server.network.master.server.network.packet.out.ClanChannelResponsePacketOut;
import net.zaros.server.network.master.server.network.packet.out.ClanCreateResponsePacketOut;
import net.zaros.server.network.master.server.network.packet.out.ClanSettingsBlockPacketOut;
import net.zaros.server.utility.newrepository.database.clans.ClanRepository;

/**
 * Represents the master server clan manager.
 * 
 * @author Walied K. Yassen
 */
public final class MSClans {

	/**
	 * Handles a clan creation packet.
	 * 
	 * @param userId
	 *                 the requester user id.
	 * @param userName
	 *                 the requester user username.
	 * @param clanName
	 *                 the desired clan name.
	 */
	public static void handleCreateRequest(long userId, String userName, String clanName) {
		Optional<MSPlayer> optional = MSRepository.getPlayerById(userId);
		if (!optional.isPresent()) {
			return;
		}
		var player = optional.get();
		var world = MSRepository.getWorld(player.getWorldId()).get();
		if (ClanRepository.exists(clanName)) {
			world.getSession().write(new ClanCreateResponsePacketOut(ClanCreateResponse.CLAN_EXISTS, userId));
			return;
		}
		var clan = ClanRepository.create(clanName);
		clan.getSettings().addMember(userId, userName, ClanRank.OWNER);
		clan.updateMember(userId, player.getWorldId());
		clan.getSettings().postUpdate(Clan.memberFilter(userId));
		world.getSession().write(new ClanCreateResponsePacketOut(ClanCreateResponse.CLAN_CREATED, userId, clan.getId()));
		handleInitialiseRequest(userId, clan.getId());
	}

	/**
	 * Handles clan initialisation request.
	 * 
	 * @param userId
	 *               the requester player user id.
	 * @param clanId
	 *               the desired clan id.
	 */
	public static void handleInitialiseRequest(long userId, long clanId) {
		var player = MSRepository.getPlayerById(userId);
		var world = player.isPresent() ? MSRepository.getWorld(player.get().getWorldId()).get() : null;
		var clan = ClanRepository.load(clanId);
		if (clan == null) {
			return;
		}
		var member = clan.getSettings().getMemberById(userId);
		if (member == null) {
			return;
		}
		world.getSession().write(new ClanSettingsBlockPacketOut(userId, clan.getSettings().getUpdateBlock().getBytes()));
	}

	/**
	 * Handles a clan channel join or leave request.
	 * </p>
	 * If the requester user is not currently online in the network, we will not
	 * send any response back to the source world.
	 * 
	 * @param userId
	 *               the requester user id.
	 * @param clanId
	 *               the requester clan id.
	 * @param leave
	 *               whether it is a leave or join request (<code>true</code> if
	 *               leaving otherwise <code>false</code>).
	 */
	public static void handleChannelRequest(long userId, long clanId, boolean leave) {
		var player = MSRepository.getPlayerById(userId);
		var world = player.isPresent() ? MSRepository.getWorld(player.get().getWorldId()).get() : null;
		var clan = ClanRepository.load(clanId);
		if (clan == null) {
			if (world != null) {
				world.getSession().write(new ClanChannelResponsePacketOut(ClanChannelResponse.CHANNEL_NOT_FOUND, userId, clanId));
			}
			return;
		}
		var member = clan.getSettings().getMemberById(userId);
		if (member == null) {
			return;
		}
		if (leave) {
			clan.getChannel().removeMember(member.toChannelMember());
			clan.getChannel().postUpdate(Clan.memberFilter(member.getUserId()));
			if (world != null) {
				world.getSession().write(new ClanChannelResponsePacketOut(ClanChannelResponse.CHANNEL_LEFT, userId, clanId, null));
			}
		} else {
			clan.getChannel().addMember(member.toChannelMember());
			clan.getChannel().postUpdate(Clan.memberFilter(member.getUserId()));
			if (world != null) {
				world.getSession().write(new ClanChannelResponsePacketOut(ClanChannelResponse.CHANNEL_JOINED, userId, clanId, clan.getChannel().getUpdateBlock().getBytes()));
			}
		}
	}

	/**
	 * Hnaldes a clan channel message request.
	 * 
	 * @param userId
	 *                    the message sender player user id.
	 * @param clanId
	 *                    the clan which the message was sent in.
	 * @param displayName
	 *                    the message sender player display name.
	 * @param modLevel
	 *                    the message sender player moderator level.
	 * @param message
	 *                    the message text content.
	 */
	public static void handleMessageRequest(long userId, long clanId, String displayName, int modLevel, String message) {
		var player = MSRepository.getPlayerById(userId);
		if (!player.isPresent()) {
			System.out.println("Inexistant");
			return;
		}
		var clan = ClanRepository.load(clanId);
		if (clan == null) {
			System.out.println("Not present");
			return;
		}
		clan.getChannel().sendMessage(displayName, modLevel, message);
	}

	/**
	 * Updates the specified member current world id.
	 * 
	 * @param clanId
	 *                the id of the clan which the member belongs to.
	 * @param userId
	 *                the id of the user that represents the member.
	 * @param worldId
	 *                the current world id that the user is in.
	 */
	public static void updatePlayerWorld(long clanId, long userId, int worldId) {
		var clan = ClanRepository.load(clanId);
		if (clan == null) {
			return;
		}
		clan.updateMember(userId, worldId);
	}

	/**
	 * Represents the clan creation response.
	 * 
	 * @author Walied K. Yassen
	 */
	public static enum ClanCreateResponse {
		/**
		 * Indicates that the clan name is already taken by another clan.
		 */
		CLAN_EXISTS,

		/**
		 * Indicates that the clan has been created successfully.
		 */
		CLAN_CREATED,
	}

	/**
	 * Represents the clan chat channel join response.
	 * 
	 * @author Walied K. Yassen
	 */
	public static enum ClanChannelResponse {
		/**
		 * Indicates that the channel we are trying to join is currently full.
		 */
		CHANNEL_FULL,

		/**
		 * Indicates that we have successfully joined the clan chat channel.
		 */
		CHANNEL_JOINED,

		/**
		 * Indicates that we have successfully joined the clan chat channel.
		 */
		CHANNEL_LEFT,

		/**
		 * Indicates that we could not find the channel the user desired to join.
		 */
		CHANNEL_NOT_FOUND,
	}

	/**
	 * Represents the clan channel message request response.
	 * 
	 * @author Walied K. Yassen
	 */
	public static enum ClanChannelMessageResponse {
		/**
		 * The message was processed successfully.
		 */
		SUCCESSFUL,
	}

	private MSClans() {
		// NOOP
	}

}
