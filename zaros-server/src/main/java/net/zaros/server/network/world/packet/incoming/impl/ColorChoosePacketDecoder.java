package net.zaros.server.network.world.packet.incoming.impl;

import net.zaros.server.game.content.event.EventRepository;
import net.zaros.server.game.content.event.context.ColorChooseEventContext;
import net.zaros.server.game.content.event.impl.ColorChooseEvent;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;
import net.zaros.server.utility.tool.Misc;

public class ColorChoosePacketDecoder implements IncomingPacketDecoder {

    @Override
    public int[] bindings() {
        return Misc.arguments(73);
    }

    @Override
    public void read(Player player, Packet packet) {
        if (packet.getBuffer().readableBytes() < 1) {
            return;
        }
        int color = packet.readShort();
        EventRepository.executeEvent(player, ColorChooseEvent.class, new ColorChooseEventContext(color));
    }
}
