package net.zaros.server.network.world.packet.outgoing.impl;

import lombok.RequiredArgsConstructor;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.Packet.PacketType;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;
import net.zaros.server.utility.tool.BufferUtils;
import net.zaros.server.utility.tool.Misc;

/**
 * Represents a clan channel normal message outgoing packet builder.
 * 
 * @author Walied K. Yassen
 */
@RequiredArgsConstructor
public final class ClanChannelMessagePacketBuilder implements OutgoingPacketBuilder {

	/**
	 * Whether it is the affined channel or not.
	 */
	private final boolean affined;

	/**
	 * The message sender player's display name.
	 */
	private final String displayName;

	/**
	 * The message text content.
	 */
	private final String message;

	/**
	 * The sender player moderator level.
	 */
	private final int modLevel;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder#build(
	 * net.zaros.server.game.node.entity.player.Player)
	 */
	@Override
	public Packet build(Player player) {
		var builder = new PacketBuilder(45, PacketType.VAR_BYTE);
		builder.writeByte(affined ? 1 : 0);
		builder.writeString(displayName);
		builder.writeShort(Misc.random(0, 65535));
		builder.writeMedium(Misc.random(0, 65535));
		builder.writeByte(modLevel);
		var buffer = new byte[256];
		buffer[0] = (byte) message.length();
		var encoded_count = (byte) (1 + BufferUtils.huffmanCompress(message, buffer, 1));
		builder.writeBytes(buffer, 0, encoded_count);
		return builder.toPacket();
	}
}
