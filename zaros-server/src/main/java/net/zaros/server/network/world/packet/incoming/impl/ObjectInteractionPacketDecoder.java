package net.zaros.server.network.world.packet.incoming.impl;

import net.zaros.server.core.event.loc.LocEvent;
import net.zaros.server.core.event.loc.LocOption;
import net.zaros.server.game.GameFlags;
import net.zaros.server.game.content.event.EventRepository;
import net.zaros.server.game.content.event.context.NodeReachEventContext;
import net.zaros.server.game.content.event.context.ObjectEventContext;
import net.zaros.server.game.content.event.impl.NodeReachEvent;
import net.zaros.server.game.content.event.impl.ObjectEvent;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.game.world.World;
import net.zaros.server.game.world.region.RegionDeletion;
import net.zaros.server.game.world.region.RegionManager;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;
import net.zaros.server.utility.rs.InteractionOption;
import net.zaros.server.utility.tool.Misc;

import java.util.Optional;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/29/2017
 */
public class ObjectInteractionPacketDecoder implements IncomingPacketDecoder {

	/**
	 * The opcode for the interface on object packet
	 */
	public static final int INTERFACE_ON_OBJECT_OPCODE = 42;

	@Override
	public int[] bindings() {
		return Misc.arguments(1, 39, 86, 58, 38, 75, INTERFACE_ON_OBJECT_OPCODE);
	}

	@Override
	public void read(Player player, Packet packet) {
		if (packet.getOpcode() == INTERFACE_ON_OBJECT_OPCODE) {
			handleInterfaceOnObjectPacket(player, packet);
		} else {
			int y = packet.readShortA();
			int x = packet.readLEShortA();
			int id = packet.readInt();
			boolean forceRun = packet.readByte() == 1;
			int packetId = packet.getOpcode();
			int regionId = Location.getRegionId(x, y);

			Optional<GameObject> optional = RegionManager.getRegion(regionId).findAnyGameObject(id, x, y,
					player.getLocation().getPlane(), -1);
			if (!optional.isPresent()) {
				return;
			}
			GameObject object = optional.get();
			InteractionOption option = getOption(packetId);
			if (option == null) {
				throw new IllegalStateException("Unexpected packet id " + packetId + ", could not find option.");
			}
			switch (option) {
			// TODO object examines
			case EXAMINE:
				if (World.getEventManager().post(new LocEvent(player, object, getLocOption(packetId)))) {
					return;
				}
				if (player.getTemporaryAttribute("remove_spawns", false)) {
					player.getRegion().removeObject(object);
					RegionDeletion.dumpObject(object);
				} else {
					if (GameFlags.debugMode) {
						player.getTransmitter()
								.sendMessage("Examining: [" + object.getDefinitions().getName() + ": " + id + ", [" + x
										+ ", " + y + ", " + regionId + "], " + object.getType() + ", "
										+ object.getRotation() + ", " + object.getDefinitions().getConfigId() + ", "
										+ object.getDefinitions().getConfigFileId(), true);
					}
				}
				break;
			default:
				player.getMovement().reset(forceRun);
				EventRepository.executeEvent(player, NodeReachEvent.class, new NodeReachEventContext(object, () -> {
					// executing the object interaction event
					if (!World.getEventManager().post(new LocEvent(player, object, getLocOption(packetId)))) {
						EventRepository.executeEvent(player, ObjectEvent.class, new ObjectEventContext(object, option));
					}
				}));
				break;
			}
		}
	}

	/**
	 * Handles the interact on object packet interaction
	 *
	 * @param player The player
	 * @param packet The packet
	 */
	private void handleInterfaceOnObjectPacket(Player player, Packet packet) {
		final int y = packet.readLEShortA();
		final int x = packet.readShort();
		final int itemSlot = packet.readByte();
		packet.readByte(); // unknown
		final int fromInterface = packet.readLEShort();
		packet.readShort(); // unknown
		packet.readShort(); // item id
		int runningFlag = packet.readByte();
		final int objectId = packet.readInt();
		final int regionId = Location.getRegionId(x, y);
		Optional<GameObject> optional = RegionManager.getRegion(regionId).findAnyGameObject(objectId, x, y,
				player.getLocation().getPlane(), -1);
		if (!optional.isPresent()) {
			return;
		}
		GameObject object = optional.get();
		boolean forceRun = runningFlag > 0;

		if (fromInterface == 679) {
			Item item = player.getInventory().getItems().get(itemSlot);
			if (item == null) {
				return;
			}
			player.getMovement().reset(forceRun);
			EventRepository.executeEvent(player, NodeReachEvent.class, new NodeReachEventContext(object, () -> {
				// executing the object interaction event on arrival
				EventRepository.executeEvent(player, ObjectEvent.class,
						new ObjectEventContext(object, InteractionOption.ITEM_ON_OBJECT, item));
			}));
		}
	}

	/**
	 * Gets the option that the player clicked by the packet id
	 *
	 * @param packetId The packet id
	 */
	private LocOption getLocOption(int packetId) {
		switch (packetId) {
		case 1:
			return LocOption.FIRST;
		case 39:
			return LocOption.SECOND;
		case 86:
			return LocOption.THIRD;
		case 58:
			return LocOption.FOURTH;
		case 38:
			return LocOption.FIFTH;
		case 75:
			return LocOption.EXAMINE;
		default:
			return null;
		}
	}

	/**
	 * Gets the option that the player clicked by the packet id
	 *
	 * @param packetId The packet id
	 */
	private InteractionOption getOption(int packetId) {
		switch (packetId) {
		case 1:
			return InteractionOption.FIRST_OPTION;
		case 39:
			return InteractionOption.SECOND_OPTION;
		case 86:
			return InteractionOption.THIRD_OPTION;
		case 58:
			return InteractionOption.FOURTH_OPTION;
		case 38:
			return InteractionOption.FIFTH_OPTION;
		case 75:
			return InteractionOption.EXAMINE;
		default:
			return null;
		}
	}
}
