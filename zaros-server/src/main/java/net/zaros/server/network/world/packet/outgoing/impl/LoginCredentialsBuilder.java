package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/19/2017
 */
public final class LoginCredentialsBuilder implements OutgoingPacketBuilder {
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder();
		bldr.writeByte(13 + 1); //length
		bldr.writeByte(player.getDetails().getDominantRight().getClientRight());
		bldr.writeByte(0);
		bldr.writeByte(0);
		bldr.writeByte(0);
		bldr.writeByte(1);
		bldr.writeByte(0);
		bldr.writeShort(player.getIndex());
		bldr.writeByte(1);
		bldr.writeMedium(0);
		bldr.writeByte(1); // members
		bldr.writeString("");
		return bldr.toPacket();
	}
}
