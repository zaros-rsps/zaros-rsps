package net.zaros.server.network.master.client.packet.out;

import java.util.Collection;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * @author Walied K. Yassen
 * @author Tyluur
 */
public class ReconnectionPacketOut extends WritablePacket {

	/**
	 * The id of the world this packet is for
	 */
	private final int worldId;

	/**
	 * The list of players
	 */
	private final Collection<Player> players;

	/**
	 * Constructs a new outgoing packet
	 */
	public ReconnectionPacketOut(int worldId, Collection<Player> players) {
		super(RECONNECTION_PACKET_ID);
		this.worldId = worldId;
		this.players = players;
	}

	@Override
	public WritablePacket create() {
		writeShort(worldId);
		writeInt(players.size());
		for (var player : players) {
			writeLong(player.getUserId());
			writeLong(player.getCurrentClanId());
			writeString(player.getDetails().getUsername());
			writeString(player.getSession().getUid());
		}
		return this;
	}
}
