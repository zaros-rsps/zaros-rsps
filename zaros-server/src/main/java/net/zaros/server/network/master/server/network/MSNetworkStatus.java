package net.zaros.server.network.master.server.network;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.zaros.server.network.master.MasterConstants;

/**
 * The possible states that the network can hold
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/11/2017
 */
@Slf4j
public enum MSNetworkStatus {

	// the default state
	NOT_CONNECTED,

	// the connected state
	CONNECTED(() -> log("Successfully bound the server to port " + MasterConstants.PORT_ID)),

	// the state that the connection is no longer valid
	CONNECTION_DROPPED(() -> log("The lobby server was just dropped."));

	/**
	 * The event that is fired when the state is changed
	 */
	@Getter
	private final StatusChangeEvent event;

	/**
	 * Constructs a status with no event
	 */
	MSNetworkStatus() {
		event = null;
	}

	/**
	 * Constructs a status with an event
	 *
	 * @param event
	 *              The event
	 */
	MSNetworkStatus(StatusChangeEvent event) {
		this.event = event;
	}

	/**
	 * The event that is executed when the status is changed
	 */
	@FunctionalInterface
	public interface StatusChangeEvent {

		/**
		 * Executes the event
		 */
		void execute();
	}

	// Workaround method.
	private static void log(String text) {
		log.info(text);
	}
}
