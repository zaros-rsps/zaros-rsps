package net.zaros.server.network.master.client.packet.in;

import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.client.packet.responsive.ResponsiveFriendDetailsPacket;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/15/2017
 */
@Readable(packetIds = { PacketConstants.FRIEND_DETAILS_COMPLETE_PACKET_ID})
public class FriendDetailsCompletePacketIn implements ReadablePacket<MCSession> {
	
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		String owner = packet.readString();
		String requestedName = packet.readString();
		int requestedWorldId = packet.readUnsignedShort();
		boolean online = packet.readByte() == 1;
		boolean lobby = packet.readByte() == 1;
		
		MasterCommunication.read(new ResponsiveFriendDetailsPacket(owner, requestedName, requestedWorldId, online, lobby));
	}
}
