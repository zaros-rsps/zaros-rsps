/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * @author Walied K. Yassen
 */
public final class MinimapTypeBuilder implements OutgoingPacketBuilder {

	/**
	 * The minimap type.
	 */
	private final int type;

	/**
	 * Construct a new {@link MinimapTypeBuilder} type object instance.
	 * 
	 * @param type
	 *             the minimap type.
	 */
	public MinimapTypeBuilder(int type) {
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.network.world.packet.outgoing.OutgoingPacketBuilder#build(org.
	 * redrune.game.node.entity.player.Player)
	 */
	@Override
	public Packet build(Player player) {
		PacketBuilder builder = new PacketBuilder(140);
		builder.writeByte(type);
		return builder.toPacket();
	}
}
