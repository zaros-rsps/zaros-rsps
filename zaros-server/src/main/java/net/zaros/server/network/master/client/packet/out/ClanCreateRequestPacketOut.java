package net.zaros.server.network.master.client.packet.out;

import lombok.Getter;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * @author Walied K. Yassen
 */
public final class ClanCreateRequestPacketOut extends WritablePacket {

	/**
	 * The requester player.
	 */
	@Getter
	private final Player player;

	/**
	 * The requested clan name.
	 */
	@Getter
	private final String name;

	/**
	 * Constructs a new {@link ClanCreateRequestPacketOut} type object instance.
	 * 
	 * @param player
	 *               the requester player.
	 * @param name
	 *               the requested clan name.
	 */
	public ClanCreateRequestPacketOut(Player player, String name) {
		super(CLAN_CREATE_REQUEST_PACKET_ID);
		this.player = player;
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.network.master.network.packet.writeable.WritablePacket#
	 * create()
	 */
	@Override
	public WritablePacket create() {
		writeLong(player.getUserId());
		writeString(player.getDetails().getDisplayName());
		writeString(name);
		return this;
	}
}
