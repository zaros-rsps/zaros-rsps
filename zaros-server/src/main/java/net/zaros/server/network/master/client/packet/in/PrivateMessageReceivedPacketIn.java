package net.zaros.server.network.master.client.packet.in;

import java.util.Optional;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.world.packet.outgoing.impl.PrivateMessageReceiveBuilder;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/15/2017
 */
@Readable(packetIds = { PacketConstants.PRIVATE_MESSAGE_RECEIVED_PACKET_ID })
public class PrivateMessageReceivedPacketIn implements ReadablePacket<MCSession> {
	
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		var fromName = packet.readString();
		var deliveryName = packet.readString();
		var fromClientRights = (byte) packet.readByte();
		var message = packet.readString();
		
		Optional<Player> optional = World.getPlayerByUsername(deliveryName);
		if (!optional.isPresent()) {
			System.out.println("unable to find player in the world with username " + deliveryName);
			return;
		}
		Player player = optional.get();
		player.getTransmitter().send(new PrivateMessageReceiveBuilder(Misc.formatPlayerNameForDisplay(fromName), Misc.formatTextToSentence(message), fromClientRights).build(player));
	}
}
