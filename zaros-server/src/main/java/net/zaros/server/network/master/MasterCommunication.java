package net.zaros.server.network.master;

import net.zaros.server.game.GameFlags;
import net.zaros.server.network.lobby.packet.readable.LobbyRepositoryPacketIn;
import net.zaros.server.network.master.client.network.MCNetworkSystem;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.client.packet.ResponsiveGamePacket;
import net.zaros.server.network.master.network.packet.OutgoingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/12/2017
 */
public class MasterCommunication implements PacketConstants {
	
	/**
	 * The instance of the network system
	 */
	private static final MCNetworkSystem SYSTEM = new MCNetworkSystem(GameFlags.worldId);
	
	/**
	 * Starts the communication
	 */
	public static void start() {
		SYSTEM.connect();
		if (GameFlags.isLobbyWorld()) {
			MCSession.getReadableRepository().include(new LobbyRepositoryPacketIn());
		}
	}
	
	/**
	 * Writes an outgoing packet to the master server
	 *
	 * @param packet
	 * 		The packet to write
	 */
	public static void write(OutgoingPacket packet) {
		try {
			SYSTEM.write(packet);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	
	/**
	 * Reads packets that the client receives back
	 *
	 * @param packet
	 * 		The responsive game packet that is read
	 */
	public static void read(ResponsiveGamePacket packet) {
		try {
			packet.read();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	
	/**
	 * Checks that we are connected to the master server
	 */
	public static boolean isConnected() {
		if (SYSTEM.getSession() == null) {
			return false;
		}
		// if we had a session we need to make sure its still connected
		return SYSTEM.getSession().isConnected();
	}
	
}