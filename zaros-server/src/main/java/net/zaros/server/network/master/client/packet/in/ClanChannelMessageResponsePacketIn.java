package net.zaros.server.network.master.client.packet.in;

import java.util.HashSet;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.game.world.World;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.world.MSClans.ClanChannelMessageResponse;

/**
 * Represents the clan channel message response incoming packet decoder.
 * 
 * @author Walied K. Yassen
 */
@Readable(packetIds = { PacketConstants.CLAN_CHANNEL_MESSAGE_RESPONSE_PACKET_ID })
@Slf4j
public final class ClanChannelMessageResponsePacketIn implements ReadablePacket<MCSession> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.master.network.packet.readable.ReadablePacket#read(
	 * net.zaros.server.network.master.network.MasterSession,
	 * net.zaros.server.network.master.network.packet.IncomingPacket)
	 */
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		var response = ClanChannelMessageResponse.values()[packet.readUnsignedByte()];
		if (response == ClanChannelMessageResponse.SUCCESSFUL) {
			var count = packet.readUnsignedShort();
			var userIds = new HashSet<Long>();
			while (count-- > 0) {
				userIds.add(packet.readLong());
			}
			var displayName = packet.readString();
			var modLevel = packet.readUnsignedByte();
			var message = packet.readString();
			var players = World.getPlayersByIds(userIds);
			players.forEach(player -> {
				player.getTransmitter().sendClanMessage(displayName, message, modLevel);
			});
		} else {
			log.warn("Unhanded {}: {} in {}", ClanChannelMessageResponse.class.getSimpleName(), response, getClass().getSimpleName());
		}
	}

}
