package net.zaros.server.network.master.client.packet.in;

import java.io.UnsupportedEncodingException;
import java.util.Optional;

import net.zaros.server.game.GameConstants;
import net.zaros.server.network.NetworkSession;
import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.client.packet.responsive.ResponsiveLoginPacket;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.world.packet.outgoing.impl.LoginResponseCodeBuilder;
import net.zaros.server.utility.backend.ReturnCode;
import net.zaros.server.utility.backend.SecureOperations;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/12/2017
 */
@Readable(packetIds = { PacketConstants.LOGIN_RESPONSE_PACKET_ID })
public class LoginResponsePacketIn implements ReadablePacket<MCSession> {
	
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		// decode first
		var uuid = packet.readString();
		var responseCode = (byte) packet.readByte();
		var lobby = (byte) packet.readByte() == 1;
		var username = packet.readString();
		var rowId = packet.readInt();
		var dataLength = packet.readInt();
		var data = new byte[dataLength];
		for (int i = 0; i < dataLength; i++) {
			data[i] = (byte) packet.readByte();
		}
		
		String fileText;
		try {
			// convert to text now
			fileText = new String(SecureOperations.getDecryptedDecompressed(data, GameConstants.FILE_ENCRYPTION_KEY), "UTF-8");
			// handle the reading now
			MasterCommunication.read(new ResponsiveLoginPacket(username, fileText, uuid, lobby, responseCode, rowId));
		} catch (UnsupportedEncodingException e) {
			sendSessionResponse(uuid, ReturnCode.ERROR_LOADING_PROFILE.getValue());
			e.printStackTrace();
		}
	}
	
	/**
	 * Sends a response to the session
	 *
	 * @param uuid
	 * 		The uuid of the session
	 * @param responseCode
	 * 		The response code id
	 */
	private void sendSessionResponse(String uuid, int responseCode) {
		Optional<NetworkSession> optional = NetworkSession.findByUid(uuid);
		if (!optional.isPresent()) {
			System.err.println("Unable to find session by id " + uuid);
			return;
		}
		NetworkSession session = optional.get();
		// sends the response code
		session.write(new LoginResponseCodeBuilder(responseCode).build(null));
	}
}
