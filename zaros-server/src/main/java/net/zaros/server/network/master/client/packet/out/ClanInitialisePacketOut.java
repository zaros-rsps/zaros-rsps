package net.zaros.server.network.master.client.packet.out;

import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * Reprsents the clan initialise outgoing packet.
 * 
 * @author Walied K. Yassen
 */
public final class ClanInitialisePacketOut extends WritablePacket {

	/**
	 * The player user id.
	 */
	private final long userId;

	/**
	 * The current clan id.
	 */
	private final long clanId;

	/**
	 * Whether to join the clan chat channel or not.
	 */
	private final boolean joinChannel;

	/**
	 * Constructs a new {@link ClanInitialisePacketOut} type object instance.
	 * 
	 * @param userId
	 *                    the player user id.
	 * @param clanId
	 *                    the current clan id.
	 * @param joinChannel
	 *                    whether to join the channel or not.
	 */
	public ClanInitialisePacketOut(long userId, long clanId, boolean joinChannel) {
		super(PacketConstants.CLAN_INITIALISE_PACKET_ID);
		this.userId = userId;
		this.clanId = clanId;
		this.joinChannel = joinChannel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.network.master.network.packet.writeable.WritablePacket#
	 * create()
	 */
	@Override
	public WritablePacket create() {
		writeLong(userId);
		writeLong(clanId);
		writeByte(joinChannel ? 1 : 0);
		return this;
	}

}
