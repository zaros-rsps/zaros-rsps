package net.zaros.server.network.master.server.network.packet.in;

import net.zaros.server.game.GameConstants;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.utility.backend.SecureOperations;
import net.zaros.server.utility.newrepository.database.players.PlayerRepository;

/**
 * @author Walied K. Yassen
 */
@Readable(packetIds = { PacketConstants.PLAYER_FILE_UPDATE_PACKET_ID })
public class PlayerFilePacketIn implements ReadablePacket<MSSession> {

	@Override
	public void read(MSSession session, IncomingPacket packet) {
		var username = packet.readString();
		var size = packet.readInt();
		var data = new byte[size];
		packet.read(data);
		data = SecureOperations.getDecryptedDecompressed(data, GameConstants.FILE_ENCRYPTION_KEY);
		PlayerRepository.store(username, data);
	}
}
