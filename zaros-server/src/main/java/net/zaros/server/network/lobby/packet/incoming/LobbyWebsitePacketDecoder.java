package net.zaros.server.network.lobby.packet.incoming;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;
import net.zaros.server.network.world.packet.outgoing.impl.MessageBuilder;

import static net.zaros.server.game.GameConstants.*;
import static net.zaros.server.network.world.packet.outgoing.impl.MessageBuilder.URL_MESSAGE_IDENTIFIER;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/17/2017
 */
public class LobbyWebsitePacketDecoder implements IncomingPacketDecoder {
	
	@Override
	public int[] bindings() {
		return arguments(62);
	}
	
	@Override
	public void read(Player player, Packet packet) {
		int id = packet.readByte();
		String name = packet.readRS2String();
		String url = packet.readRS2String();
		int unknown = packet.readByte();
		
		String destinationUrl = null;
		switch(id) {
			case 101:
				destinationUrl = EMAIL_MODIFICATION_URL;
				break;
			case 116:
				destinationUrl = INBOX_URL;
				break;
			case 100:
				destinationUrl = DONATION_URL;
				break;
		}
		
		if (destinationUrl != null) {
			player.getTransmitter().send(new MessageBuilder(URL_MESSAGE_IDENTIFIER, destinationUrl).build(player));
		}
	}
}
