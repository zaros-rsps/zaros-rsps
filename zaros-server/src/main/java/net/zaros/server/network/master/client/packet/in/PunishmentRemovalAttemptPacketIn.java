package net.zaros.server.network.master.client.packet.in;

import net.zaros.server.game.world.punishment.Punishment;
import net.zaros.server.game.world.punishment.PunishmentType;
import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.client.packet.responsive.ResponsivePunishmentPacket;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/17/2017
 */
@Readable(packetIds = { PacketConstants.PUNISHMENT_REMOVAL_ATTEMPT_PACKET_ID})
public class PunishmentRemovalAttemptPacketIn implements ReadablePacket<MCSession> {
	
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		var punisher = packet.readString();
		var punished = packet.readString();
		var type = (byte) packet.readByte();
		var time = packet.readLong();
		
		if (type < 0 || type >= PunishmentType.values().length) {
			System.out.println("Invalid punishment type received.");
			return;
		}
		
		// the punishment instance
		Punishment punishment = new Punishment(punisher, punished, PunishmentType.values()[type], time);
		
		// read the punishment now
		MasterCommunication.read(new ResponsivePunishmentPacket(punishment));
	}
}
