package net.zaros.server.network.lobby.packet.outgoing;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.list.WorldList;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.Packet.PacketType;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/18/2017
 */
public final class WorldListBuilder implements OutgoingPacketBuilder {
	
	private final boolean worldConfiguration;
	
	private final boolean worldStatus;
	
	public WorldListBuilder(boolean worldConfiguration, boolean worldStatus) {
		this.worldConfiguration = worldConfiguration;
		this.worldStatus = worldStatus;
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(23, PacketType.VAR_SHORT);
		bldr.writeByte(1);
		bldr.writeByte(worldConfiguration ? 2 : 0);
		bldr.writeByte(worldStatus ? 1 : 0);
		if (worldConfiguration) {
			WorldList.populateConfiguration(bldr);
		}
		if (worldStatus) {
			WorldList.populateStatus(bldr);
		}
		return bldr.toPacket();
	}
}
