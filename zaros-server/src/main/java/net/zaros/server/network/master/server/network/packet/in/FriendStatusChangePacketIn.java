package net.zaros.server.network.master.server.network.packet.in;

import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.network.master.server.network.packet.out.FriendStatusDeliveryPacketOut;
import net.zaros.server.network.master.server.world.MSRepository;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/15/2017
 */
@Readable(packetIds = { PacketConstants.FRIEND_STATUS_CHANGE_RECEIVE_PACKET_ID })
public class FriendStatusChangePacketIn implements ReadablePacket<MSSession> {
	
	@Override
	public void read(MSSession session, IncomingPacket packet) {
		String name = packet.readString();
		byte status = (byte) packet.readByte();
		int worldId = packet.readUnsignedShort();
		boolean online = packet.readByte() == 1;
		boolean lobby = packet.readByte() == 1;
		
		// sending the update to all worlds available
		for (var world : MSRepository.getWorlds().values()) {
			if (world != null) {
				world.getSession().write(new FriendStatusDeliveryPacketOut(name, status, worldId, online, lobby));
			}
		}
	}
}
