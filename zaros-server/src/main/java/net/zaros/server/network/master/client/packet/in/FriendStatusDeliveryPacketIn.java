package net.zaros.server.network.master.client.packet.in;

import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.master.client.network.MCSession;
import net.zaros.server.network.master.client.packet.responsive.ResponsiveStatusChangePacket;
import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/15/2017
 */
@Readable(packetIds = { PacketConstants.FRIEND_STATUS_CHANGE_DELIVER_PACKET_ID })
public class FriendStatusDeliveryPacketIn implements ReadablePacket<MCSession> {
	
	@Override
	public void read(MCSession session, IncomingPacket packet) {
		var name = packet.readString();
		var status = (byte) packet.readByte();
		var worldId = packet.readUnsignedShort();
		var online = packet.readByte() == 1;
		var lobby = packet.readByte() == 1;
		MasterCommunication.read(new ResponsiveStatusChangePacket(name, status, worldId, online, lobby));
	}
}
