package net.zaros.server.network.master.client.packet.out;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * Represents a clan channel message outgoing packet.
 * 
 * @author Walied K. Yassen
 */
public final class ClanChannelMessageRequestPacketOut extends WritablePacket {

	/**
	 * The message sender player.
	 */
	private final Player player;

	/**
	 * The message text content.
	 */
	private final String text;

	/**
	 * Constructs a new {@link ClanChannelMessageRequestPacketOut} type object instance.
	 * 
	 * @param player
	 *               the message sender player.
	 * @param text
	 *               the message text content.
	 */
	public ClanChannelMessageRequestPacketOut(Player player, String text) {
		super(PacketConstants.CLAN_CHANNEL_MESSAGE_REQUEST_PACKET_ID);
		this.player = player;
		this.text = text;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.network.master.network.packet.writeable.WritablePacket#
	 * create()
	 */
	@Override
	public WritablePacket create() {
		writeLong(player.getUserId());
		writeLong(player.getCurrentClanId());
		writeString(player.getDetails().getDisplayName());
		writeByte(player.getDetails().getDominantRight().getClientRight());
		writeString(text);
		return this;
	}

}
