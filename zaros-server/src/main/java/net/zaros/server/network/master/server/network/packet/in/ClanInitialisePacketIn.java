package net.zaros.server.network.master.server.network.packet.in;

import net.zaros.server.network.master.network.packet.IncomingPacket;
import net.zaros.server.network.master.network.packet.PacketConstants;
import net.zaros.server.network.master.network.packet.readable.Readable;
import net.zaros.server.network.master.network.packet.readable.ReadablePacket;
import net.zaros.server.network.master.server.network.MSSession;
import net.zaros.server.network.master.server.world.MSClans;

/**
 * Represents the clan system initialise packet handler.
 * 
 * @author Walied K. Yassen
 */
@Readable(packetIds = { PacketConstants.CLAN_INITIALISE_PACKET_ID })
public final class ClanInitialisePacketIn implements ReadablePacket<MSSession> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.network.master.network.packet.readable.ReadablePacket#read(
	 * net.zaros.server.network.master.network.MasterSession,
	 * net.zaros.server.network.master.network.packet.IncomingPacket)
	 */
	@Override
	public void read(MSSession session, IncomingPacket packet) {
		var userId = packet.readLong();
		var currentClanId = packet.readLong();
		var joinChannel = packet.readUnsignedByte() == 1;
		if (currentClanId != 0) {
			if (joinChannel) {
				MSClans.handleChannelRequest(userId, currentClanId, false);
			}
			// the order this way prevents rebuilding.
			MSClans.handleInitialiseRequest(userId, currentClanId);
		}
	}
}
