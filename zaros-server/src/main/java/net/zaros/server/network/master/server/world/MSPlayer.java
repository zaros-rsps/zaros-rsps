package net.zaros.server.network.master.server.world;

import lombok.Data;
import lombok.Getter;
import net.zaros.server.network.master.MasterConstants;

/**
 * @author Walied K. Yassen
 */
@Data
public final class MSPlayer implements MasterConstants {

	/**
	 * The name of the player in the lobby server
	 */
	@Getter
	private final String username;

	/**
	 * The id of the world the player is on
	 */
	@Getter
	private final int worldId;

	/**
	 * The uid of the player
	 */
	@Getter
	private final String uid;

	/**
	 * The user id of the player.
	 */
	@Getter
	private final long userId;
}
