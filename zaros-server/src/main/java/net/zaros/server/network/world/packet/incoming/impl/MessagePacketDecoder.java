package net.zaros.server.network.world.packet.incoming.impl;

import lombok.extern.slf4j.Slf4j;
import net.zaros.server.game.content.event.EventRepository;
import net.zaros.server.game.content.event.context.CommandEventContext;
import net.zaros.server.game.content.event.impl.CommandEvent;
import net.zaros.server.game.module.interaction.rsinterface.GameframeInteractionModule;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.world.World;
import net.zaros.server.game.world.punishment.PunishmentType;
import net.zaros.server.network.master.MasterCommunication;
import net.zaros.server.network.master.client.packet.out.ClanChannelMessageRequestPacketOut;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.incoming.IncomingPacketDecoder;
import net.zaros.server.network.world.packet.outgoing.impl.PublicChatBuilder;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.constant.GameBarStatus;
import net.zaros.server.utility.tool.BufferUtils;
import net.zaros.server.utility.tool.Misc;

/**
 * @author Tyluur
 * @author Walied K. Yassen
 */
@Slf4j
public class MessagePacketDecoder implements IncomingPacketDecoder {

	/**
	 * The public chat message opcode.
	 */
	private static final int CHAT_SETMODE = 18;
	/**
	 * The public chat message opcode.
	 */
	private static final int MESSAGE_PUBLIC = 19;

	/**
	 * The private chat message opcode
	 */
	public static final int MESSAGE_PRIVATE = 13;

	/**
	 * The opcode of the game bar settings flag
	 */
	public static final int SET_CHATFILTERSETTINGS = 78;

	@Override
	public int[] bindings() {
		return arguments(CHAT_SETMODE, MESSAGE_PUBLIC, MESSAGE_PRIVATE, SET_CHATFILTERSETTINGS);
	}

	@Override
	public void read(Player player, Packet packet) {
		switch (packet.getOpcode()) {
		case CHAT_SETMODE:
			player.putTemporaryAttribute(AttributeKey.CHAT_MODE, packet.readUnsignedByte());
			break;
		case MESSAGE_PUBLIC:
			readMessagePacket(player, packet);
			break;
		case MESSAGE_PRIVATE:
			readPrivateMessagePacket(player, packet);
			break;
		case SET_CHATFILTERSETTINGS:
			readGameBarPacket(player, packet);
			break;
		}
	}

	/**
	 * Reads the packet for a public chat message
	 *
	 * @param player
	 *               The player
	 * @param packet
	 *               The packet
	 */
	private void readMessagePacket(Player player, Packet packet) {
		int effects = packet.readUnsignedShort();
		int length = packet.readUnsignedByte();
		String text = Misc.formatTextToSentence(BufferUtils.decompressHuffman(packet, length));
		if (text.startsWith("::")) {
			EventRepository.executeEvent(player, CommandEvent.class, new CommandEventContext(text.replaceFirst("::", "").split(" "), false));
			return;
		}
		if (player.getVariables().hasPunishment(PunishmentType.MUTE) || player.getVariables().hasPunishment(PunishmentType.ADDRESS_MUTE)) {
			player.getTransmitter().sendUnrepeatingMessages("You are muted.");
			return;
		}
		var chatMode = player.removeTemporaryAttribute(AttributeKey.CHAT_MODE, 0);
		if (chatMode == 0) {
			// local chat.
			for (var p : World.getPlayers()) {
				if (p == null || p.getLocation().getRegionId() != player.getLocation().getRegionId()) {
					continue;
				}
				p.getSession().write(new PublicChatBuilder(player.getIndex(), player.getDetails().getDominantRight().getClientRight(), text, effects).build(p));
			}
		} else if (chatMode == 2) {
			// main clan channel.
			if (!player.hasClan()) {
				return;
			}
			MasterCommunication.write(new ClanChannelMessageRequestPacketOut(player, text));
		} else {
			log.warn("Unhandled chat mode:" + chatMode);
		}
	}

	/**
	 * Decodes the packet saying that we should send a private message
	 *
	 * @param player
	 *               The player
	 * @param packet
	 *               The packet
	 */
	private void readPrivateMessagePacket(Player player, Packet packet) {
		String name = Misc.formatPlayerNameForProtocol(packet.readRS2String());
		byte length = packet.readByte();
		String message = BufferUtils.decompressHuffman(packet, length);

		if (player.getVariables().hasPunishment(PunishmentType.MUTE) || player.getVariables().hasPunishment(PunishmentType.ADDRESS_MUTE)) {
			player.getTransmitter().sendUnrepeatingMessages("You are muted.");
			return;
		}

		player.getManager().getContacts().sendPrivateMessage(name, message);
	}

	/**
	 * Reads the packet for the game bar settings.
	 *
	 * @param player
	 *               The player
	 * @param packet
	 *               The packet
	 */
	public void readGameBarPacket(Player player, Packet packet) {
		byte publicFlag = packet.readByte();
		byte privateFlag = packet.readByte();
		byte friendsFlag = packet.readByte();

		if (publicFlag < 0 || publicFlag > 3) {
			publicFlag = 0;
		}
		if (privateFlag < 0 || privateFlag > 2) {
			privateFlag = 0;
		}
		if (friendsFlag < 0 || friendsFlag > 2) {
			friendsFlag = 0;
		}

		final GameBarStatus publicStatus = GameBarStatus.byValue(publicFlag).orElse(GameBarStatus.ON);
		final GameBarStatus privateStatus = GameBarStatus.byValue(privateFlag).orElse(GameBarStatus.ON);
		final GameBarStatus friendsStatus = GameBarStatus.byValue(friendsFlag).orElse(GameBarStatus.ON);

		GameframeInteractionModule.updateGameBar(player, AttributeKey.PUBLIC, publicStatus);
		GameframeInteractionModule.updateGameBar(player, AttributeKey.PRIVATE, privateStatus);
		GameframeInteractionModule.updateGameBar(player, AttributeKey.FRIENDS, friendsStatus);

		player.getManager().getContacts().sendMyStatusChange(!privateStatus.equals(GameBarStatus.OFF));
	}
}
