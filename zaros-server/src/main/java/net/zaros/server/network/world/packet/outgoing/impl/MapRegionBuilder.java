package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.Packet.PacketType;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.newrepository.map.xteas.XteasRepository;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 5/19/2017
 */
public final class MapRegionBuilder implements OutgoingPacketBuilder {
	
	/**
	 * If the packet is being sent from a login request
	 */
	private final boolean onLogin;
	
	public MapRegionBuilder(boolean onLogin) {
		this.onLogin = onLogin;
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(19, PacketType.VAR_SHORT);
		Location pos = player.getLocation();
		if (onLogin) {
			player.getRenderInformation().enterWorld(bldr);
		}
		int regionX = pos.getRegionX();
		int regionY = pos.getRegionY();
		bldr.writeByteC(player.getTemporaryAttribute(AttributeKey.FORCE_NEXT_MAP_LOAD, false) ? 1 : 0); //Force refresh? 1 : 0
		bldr.writeLEShort(regionY);
		bldr.writeLEShortA(regionX);
		bldr.writeByteS(0); //Scene graph size index.
		for (int regionId : player.getMapRegionsIds()) {
			int[] keys = XteasRepository.lookup(regionId);
			if (keys == null) {
				keys = new int[4];
			}
			for (int i = 0; i < 4; i++) {
				bldr.writeInt(keys[i]);
			}
		}
		return bldr.toPacket();
	}
	
}
