/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.server.network.world.packet.outgoing.impl;

import lombok.AllArgsConstructor;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * @author Walied K. Yassen
 */
@AllArgsConstructor
public class CameraLookAtBuilder implements OutgoingPacketBuilder {

	/**
	 * The local x position to look at.
	 */
	private final int localx;

	/**
	 * The local y position to look at.
	 */
	private final int localy;

	/**
	 * The local height position to look at.
	 */
	private final int height;

	/**
	 * The initial speed.
	 */
	private final int speed;

	/**
	 * The initial velocity.
	 */
	private final int velocity;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.network.world.packet.outgoing.OutgoingPacketBuilder#build(org.
	 * redrune.game.node.entity.player.Player)
	 */
	@Override
	public Packet build(Player player) {
		PacketBuilder builder = new PacketBuilder(26);
		builder.writeByte(localy);
		builder.writeByte(localx);
		builder.writeShortA(height);
		builder.writeByte(speed);
		builder.writeByteS(velocity);
		return builder.toPacket();
	}
}
