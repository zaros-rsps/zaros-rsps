package net.zaros.server.network.master.client.packet.out;

import net.zaros.server.network.master.network.packet.writeable.WritablePacket;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/15/2017
 */
public class FriendDetailsRequestPacketOut extends WritablePacket {
	
	/**
	 * The name of the user who requested the friend's details
	 */
	private final String owner;
	
	/**
	 * The id of the world that the user who requested the details was on
	 */
	private final int requestWorldId;
	
	/**
	 * The username of the user who requested the friend's details
	 */
	private final String requestedName;
	
	public FriendDetailsRequestPacketOut(String owner, int requestWorldId, String requestedName) {
		super(FRIEND_DETAILS_REQUEST_PACKET_ID);
		this.owner = owner;
		this.requestWorldId = requestWorldId;
		this.requestedName = requestedName;
	}
	
	@Override
	public WritablePacket create() {
		writeString(owner);
		writeShort(requestWorldId);
		writeString(requestedName);
		return this;
	}
}
