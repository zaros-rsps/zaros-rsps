package net.zaros.server.network.master.server.world;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.network.master.server.network.MSSession;

/**
 * Holds information of a certain game world.
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @author Emperor
 * @since 7/12/2017
 */
public class MSWorld {

	/**
	 * The world id.
	 */
	@Getter
	private final int id;

	/**
	 * The list of players in the world
	 */
	@Getter
	private final Set<MSPlayer> players = new LinkedHashSet<>();

	/**
	 * The session
	 */
	@Getter
	@Setter
	private MSSession session;

	public MSWorld(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "MSWorld{" + "id=" + id + ", players=" + players + '}';
	}

	/**
	 * Checks if a player is on the world
	 *
	 * @param username
	 *                 The name of the player
	 */
	public boolean isOnline(String username) {
		for (MSPlayer worldPlayerName : players) {
			if (worldPlayerName.getUsername().equals(username)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Adds a player to the list of players
	 *
	 * @param username
	 *                 The name of the player
	 * @param uid
	 *                 The unique identification of the player
	 */
	public boolean addPlayer(String username, String uid, long userId) {
		return players.add(new MSPlayer(username, id, uid, userId));
	}

	/**
	 * Removes the player with the specified {@code userId} from the players list.
	 *
	 * @param userId
	 *               the player user id.
	 * @return <code>true</code> if the player was removed otherwise
	 *         <code>false</code>.
	 */
	public boolean removePlayer(long userId) {
		return players.removeIf(player -> player.getUserId() == userId);
	}

	/**
	 * Checks if a player is registered in this world
	 *
	 * @param username
	 *                 The name of the player
	 */
	public boolean playerRegistered(String username) {
		for (MSPlayer worldPlayer : players) {
			if (worldPlayer.getUsername().equals(username)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Finds a player in the world and constructs an optional for that player by
	 * their name
	 *
	 * @param username
	 *                 The name of the player in the world
	 */
	public Optional<MSPlayer> getPlayerByName(String username) {
		for (MSPlayer worldPlayer : players) {
			if (Objects.equals(worldPlayer.getUsername(), username)) {
				return Optional.of(worldPlayer);
			}
		}
		return Optional.empty();
	}

	/**
	 * Finds a player in the world and constructs an optional for that player by
	 * their id
	 *
	 * @param userId
	 *               The id of the player in the world
	 */
	public Optional<MSPlayer> getPlayerById(long userId) {
		for (MSPlayer player : players) {
			if (player.getUserId() == userId) {
				return Optional.of(player);
			}
		}
		return Optional.empty();
	}

	/**
	 * Finds an optional instance by the uid
	 *
	 * @param uid
	 *            The uid of the player
	 */
	public Optional<MSPlayer> getPlayerByUid(String uid) {
		for (MSPlayer worldPlayer : players) {
			if (Objects.equals(worldPlayer.getUid(), uid)) {
				return Optional.of(worldPlayer);
			}
		}
		return Optional.empty();
	}

	/**
	 * Removes the world
	 */
	void unregister() {
		players.clear();
	}

	/**
	 * If the world is a lobby
	 */
	public boolean isLobby() {
		return id > 1099;
	}
}
