package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/10/2017
 */
public class LogoutBuilder implements OutgoingPacketBuilder {
	
	/**
	 * If we are logging out to the lobby
	 */
	private final boolean lobby;
	
	/**
	 * Constructs a new logout
	 *
	 * @param lobby
	 * 		If we are logging out to the lobby
	 */
	public LogoutBuilder(boolean lobby) {
		this.lobby = lobby;
	}
	
	@Override
	public Packet build(Player player) {
		return new PacketBuilder(lobby ? 59 : 126).toPacket();
	}
}
