package net.zaros.server.network.world.packet.outgoing.impl;

import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.network.world.packet.Packet;
import net.zaros.server.network.world.packet.Packet.PacketType;
import net.zaros.server.network.world.packet.PacketBuilder;
import net.zaros.server.network.world.packet.outgoing.OutgoingPacketBuilder;
import net.zaros.server.utility.tool.BufferUtils;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 6/13/2017
 */
public class PrivateMessageSendBuilder implements OutgoingPacketBuilder {
	
	/**
	 * The player's name.
	 */
	private final String name;
	
	/**
	 * The message.
	 */
	private final String message;
	
	public PrivateMessageSendBuilder(String name, String message) {
		this.name = name;
		this.message = message;
	}
	
	@Override
	public Packet build(Player player) {
		PacketBuilder bldr = new PacketBuilder(127, PacketType.VAR_BYTE);
		byte[] encryptedData = new byte[message.length() + 1];
		BufferUtils.huffmanCompress(message, encryptedData, 0);
		bldr.writeString(name);
		bldr.writeByte((byte) message.length());
		bldr.writeBytes(encryptedData);
		return bldr.toPacket();
	}
}
