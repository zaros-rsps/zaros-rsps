package net.zaros.cache;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.CRC32;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.zaros.cache.store.Index;
import net.zaros.cache.store.Store;
import net.zaros.cache.type.ConfigLoader;
import net.zaros.cache.util.whirlpool.Whirlpool;
import net.zaros.server.utility.newrepository.RepositoryDirectory;

/**
 * Represents the server cache manager.
 * 
 * @author Walied K. Yassen
 */
@Slf4j
public final class Cache {

	/**
	 * The backing library main class for our cache system.
	 */
	private static Store store;

	/**
	 * The checksum file bytes.
	 */
	@Getter
	private static byte[] versionTable;

	/**
	 * Initialises the cache system.
	 * 
	 * @param directory
	 *                  the cache main directory.
	 * @throws IOException
	 *                     if anything occurs during the cache system
	 *                     initialisation.
	 */
	public static void initialise(RepositoryDirectory directory) {
		try {
			var absolutePath = directory.getPath().toFile().getAbsolutePath() + File.separator;
			ConfigLoader.store = store = new Store(absolutePath);
			generateChecksumFile();
		} catch (IOException e) {
			log.error("Error while starting the cache system", e);
			System.exit(1);
		}
	}

	/**
	 * Generates the checksum file.
	 */
	private static void generateChecksumFile() {
		int count = store.getIndexes().length;
		int size = store.getIndexes().length * 74 + 3;
		CRC32 crc = new CRC32();
		ByteBuffer mainFileBuffer = ByteBuffer.allocate(size);
		mainFileBuffer.put((byte) 0).putInt(size - 5).put((byte) count);
		for (int i = 0; i < count; i++) {
			byte[] file = store.getIndex255().getArchiveData(i);
			if (file == null) {
				mainFileBuffer.putInt(0).putInt(0).put(new byte[64]);
				continue;
			}
			crc.update(file);
			Index index = store.getIndexes()[i];
			mainFileBuffer.putInt((int) crc.getValue()).putInt(index.getTable().getRevision()).put(Whirlpool.getHash(file, 0, file.length));
			crc.reset();
		}
		int bufferPosition = mainFileBuffer.position();
		mainFileBuffer.rewind();
		byte[] mainFileData = new byte[bufferPosition];
		mainFileBuffer.get(mainFileData).rewind().position(bufferPosition);
		mainFileBuffer.put((byte) 10).put(Whirlpool.getHash(mainFileData, 5, mainFileData.length - 5));
		versionTable = mainFileBuffer.array();
	}

	/**
	 * Gets the backing library main class object.
	 * 
	 * @return the backing library main class object.
	 */
	public static Store getStore() {
		return store;
	}

	private Cache() {
		// NOOP
	}
}
