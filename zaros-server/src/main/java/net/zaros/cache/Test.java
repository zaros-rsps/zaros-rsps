package net.zaros.cache;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * @author Walied K. Yassen
 */
public class Test {

	public static void main(String[] args) {
		var path = "test";
		var name = "Test";
		// create the data source configurations.
		var config = new HikariConfig();
		config.setPoolName(name);
		config.setDriverClassName("org.h2.Driver");
		config.setJdbcUrl("jdbc:h2:G:\\GitHub\\zaros-rsps\\zaros-server\\" + path);
		config.setConnectionTestQuery("SELECT 1");
		config.setInitializationFailFast(false);
		config.setMaxLifetime(60000);
		config.setIdleTimeout(30000);
		config.setMaximumPoolSize(25);
		config.setAutoCommit(true);
		config.addDataSourceProperty("prepStmtCacheSize", 250);
		config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
		config.addDataSourceProperty("cachePrepStmts", true);
		config.addDataSourceProperty("useServerPrepStmts", true);
		config.addDataSourceProperty("useLocalSessionState", true);
		config.addDataSourceProperty("rewriteBatchedStatements", true);
		config.addDataSourceProperty("cacheResultSetMetadata", true);
		config.addDataSourceProperty("cacheServerConfiguration", true);
		config.addDataSourceProperty("maintainTimeStats", true);
		// create the data source object.
		var source = new HikariDataSource(config);
	}
}
