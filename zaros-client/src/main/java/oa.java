
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.lang.reflect.Method;

import net.zaros.client.NodeDeque;
import net.zaros.client.Model;
import net.zaros.client.Class186;
import net.zaros.client.Class241;
import net.zaros.client.Class296_Sub35;
import net.zaros.client.Class296_Sub35_Sub1;
import net.zaros.client.Class33;
import net.zaros.client.Class338;
import net.zaros.client.Class338_Sub8_Sub2;
import net.zaros.client.Class355_Sub1;
import net.zaros.client.Class366_Sub4;
import net.zaros.client.Class373;
import net.zaros.client.Class376;
import net.zaros.client.Class390;
import net.zaros.client.Sprite;
import net.zaros.client.Class55;
import net.zaros.client.Class92;
import net.zaros.client.Exception_Sub1;
import net.zaros.client.HashTable;
import net.zaros.client.Interface11;
import net.zaros.client.Interface13;
import net.zaros.client.Interface19;
import net.zaros.client.Interface3;
import net.zaros.client.MaterialRaw;
import net.zaros.client.Mesh;
import net.zaros.client.aa;
import net.zaros.client.d;
import net.zaros.client.ha;
import net.zaros.client.s;
import net.zaros.client.za;

public class oa extends ha implements Interface3 {
	private NodeDeque aClass155_3455;
	public long nativeid = 0L;
	private ya aYa3456;
	private boolean aBoolean3457 = false;
	private static int[] anIntArray3458 = new int[Math.max(Math.max(104, 20), 24573)];
	public static int[] anIntArray3459;
	private static int[] anIntArray3460 = anIntArray3458;
	private static int[] anIntArray3461;
	private static byte[] aByteArray3462 = new byte[8191];
	private static int[] anIntArray3463 = new int[8191];
	private static float[] aFloatArray3464;
	public static int[] anIntArray3465;
	private static float[] aFloatArray3466;
	private static int[] anIntArray3467;
	private Class373 aClass373_3468;
	private int anInt3469;
	private int anInt3470;
	private static short[] aShortArray3471;
	private HashTable aClass263_3472;
	private Class373 aClass373_3473;
	private p aP3474;
	private boolean aBoolean3475;
	int anInt3476;
	private a[] anAArray3477;

	public native int I();

	public native void ZA(int i, float f, float f_0_, float f_1_, float f_2_, float f_3_);

	public boolean r() {
		return true;
	}

	public void a(Class390 class390) {
		if (class390.aClass84_3282.method820(0) != 0) {
			a(class390, false);
			C().method139(this, anIntArray3467, anIntArray3461, anIntArray3463, aShortArray3471,
					class390.aClass84_3282.method820(0));
		}
	}

	public native void HA(int i, int i_4_, int i_5_, int i_6_, int[] is);

	public void a(int i, Class296_Sub35[] class296_sub35s) {
		int i_7_ = 0;
		for (int i_8_ = 0; i_8_ < i; i_8_++) {
			anIntArray3460[i_7_++] = class296_sub35s[i_8_].method2749(true);
			anIntArray3460[i_7_++] = class296_sub35s[i_8_].method2751(-28925);
			anIntArray3460[i_7_++] = class296_sub35s[i_8_].method2750(-4444);
			anIntArray3460[i_7_++] = class296_sub35s[i_8_].method2748(103);
			aFloatArray3466[i_8_] = class296_sub35s[i_8_].method2745((byte) 100);
			anIntArray3460[i_7_++] = class296_sub35s[i_8_].method2746(-24996);
		}
		N(i, anIntArray3460, aFloatArray3466);
	}

	private native void ma(long l);

	public int d(int i, int i_9_) {
		return i | i_9_;
	}

	public native void A(int i, aa var_aa, int i_10_, int i_11_);

	public Class33 s() {
		return new Class33(0, "SSE", 1, "CPU", 0L);
	}

	public native int i();

	public boolean j() {
		return true;
	}

	public native int[] na(int i, int i_12_, int i_13_, int i_14_);

	public native void P(int i, int i_15_, int i_16_, int i_17_, int i_18_);

	public void e(int i, int i_19_, int i_20_, int i_21_, int i_22_, int i_23_) {
		wa(i, i_19_, i_20_, i_21_, i_22_, i_23_);
	}

	public int q() {
		return 4;
	}

	private native void N(int i, int[] is, float[] fs);

	private boolean WA(short i) {
		synchronized (aD1299) {
			if (!aD1299.is_ready(i))
				return false;
			MaterialRaw class170 = aD1299.method14(i, -9412);
			if (class170 == null)
				return false;
			int[] is;
			if (class170.anInt1788 != 2)
				is = aD1299.get_transparent_pixels(128, 128, true, (byte) 121, 0.7F, i);
			else
				is = aD1299.method17(0.7F, 128, true, (byte) -104, 128, i);
			CA(i, is, class170.aShort1775, class170.anInt1788, class170.aByte1774, class170.aByte1784,
					class170.anInt1794, class170.small_sized, class170.aByte1773, class170.aByte1793, class170.speed_u,
					class170.speed_v, class170.aBoolean1792, class170.aBoolean1786, class170.aBoolean1785,
					class170.aBoolean1779, class170.aBoolean1780, class170.aByte1795, class170.aBoolean1790,
					class170.aBoolean1789, class170.anInt1782);
		}
		return true;
	}

	private void a(Class390 class390, boolean bool) {
		int i = 0;
		int i_24_ = 0;
		int i_25_ = 0;
		int i_26_ = 0;
		int i_27_ = 0;
		for (Class338_Sub8_Sub2 class338_sub8_sub2 = (Class338_Sub8_Sub2) class390.aClass84_3282.method818(
				106); class338_sub8_sub2 != null; class338_sub8_sub2 = ((Class338_Sub8_Sub2) class390.aClass84_3282
						.method816(-127))) {
			anIntArray3467[i++] = class338_sub8_sub2.anInt6580;
			anIntArray3467[i++] = class338_sub8_sub2.anInt6589;
			anIntArray3467[i++] = class338_sub8_sub2.anInt6581;
			anIntArray3461[i_24_++] = class338_sub8_sub2.anInt6583;
			aShortArray3471[i_26_++] = (short) class338_sub8_sub2.anInt6582;
			anIntArray3463[i_25_++] = class338_sub8_sub2.anInt6579;
			if (bool)
				aByteArray3462[i_27_++] = class338_sub8_sub2.aByte6586;
		}
	}

	private native void va(za var_za);

	public boolean d() {
		return false;
	}

	public void i(int i) {
		anAArray3477[i].method153();
	}

	public native void H(int i, int i_28_, int i_29_, int[] is);

	public void a(int i, int i_30_, int i_31_, int i_32_, int i_33_, int i_34_, int i_35_, int i_36_, int i_37_,
			int i_38_, int i_39_, int i_40_, int i_41_) {
		/* empty */
	}

	public native int JA(int i, int i_42_, int i_43_, int i_44_, int i_45_, int i_46_);

	public native void ya();

	public void a(int i, int i_47_, int i_48_, int i_49_, int i_50_, int i_51_, int i_52_) {
		C().method147(this, i, i_47_, i_48_, i_49_, i_50_, i_51_, i_52_);
	}

	private native void FA();

	public Sprite a(int[] is, int i, int i_53_, int i_54_, int i_55_, boolean bool) {
		return new j(this, is, i, i_53_, i_54_, i_55_, false);
	}

	public void a(float f, float f_56_, float f_57_) {
		/* empty */
	}

	public native int E();

	public Class55 a(Class92 class92, Class186[] class186s, boolean bool) {
		int[] is = new int[class186s.length];
		int[] is_58_ = new int[class186s.length];
		boolean bool_59_ = false;
		for (int i = 0; i < class186s.length; i++) {
			is[i] = class186s[i].anInt1899;
			is_58_[i] = class186s[i].anInt1904;
			if (class186s[i].aByteArray1905 != null)
				bool_59_ = true;
		}
		if (bool) {
			if (bool_59_)
				throw new IllegalArgumentException("Cannot specify alpha with non-mono font unless someone writes it");
			return new h(this, aYa3456, class92, class186s, null);
		}
		if (bool_59_)
			throw new IllegalArgumentException("Cannot specify alpha with non-mono font unless someone writes it");
		return new n(this, aYa3456, class92, class186s, null);
	}

	public void t() {
		if (!aBoolean3457) {
			anAArray3477 = null;
			aP3474 = null;
			aYa3456 = null;
			aClass373_3468 = null;
			aClass263_3472.clear();
			for (ya var_ya = (ya) aClass155_3455.removeFirst((byte) 114); var_ya != null; var_ya = (ya) aClass155_3455
					.removeNext(1001))
				var_ya.ga();
			aClass155_3455.method1581(327680);
			FA();
			if (aBoolean3475) {
				Class355_Sub1.method3702((byte) 78, true, false);
				aBoolean3475 = false;
			}
			g();
			Class376.method3957(false);
			aBoolean3457 = true;
		}
	}

	public Class296_Sub35 a(int i, int i_60_, int i_61_, int i_62_, int i_63_, float f) {
		return new Class296_Sub35_Sub1(i, i_60_, i_61_, i_62_, i_63_, f);
	}

	public boolean a() {
		return true;
	}

	public native void w(boolean bool);

	private Object OA() {
		return new ba(this);
	}

	public void c() {
		/* empty */
	}

	public native void da(int i, int i_64_, int i_65_, int[] is);

	public boolean p() {
		return true;
	}

	public void a(int i) {
		throw new IllegalStateException();
	}

	public void h() {
		/* empty */
	}

	public Interface19 b(int i, int i_66_) {
		return a(i, i_66_, false);
	}

	public za b(int i) {
		ya var_ya = new ya(this, i);
		aClass155_3455.addLast((byte) 98, var_ya);
		return var_ya;
	}

	private native void AA(short i, short i_67_, int i_68_, byte i_69_, byte i_70_, int i_71_, boolean bool, byte i_72_,
			byte i_73_, byte i_74_, byte i_75_, boolean bool_76_, boolean bool_77_, boolean bool_78_, boolean bool_79_,
			boolean bool_80_, byte i_81_, boolean bool_82_, boolean bool_83_, int i_84_);

	public void c(int i, int i_85_) throws Exception_Sub1 {
		if (aP3474 == null)
			throw new IllegalStateException("off");
		aP3474.method2768(i, i_85_);
	}

	public native void KA(int i, int i_86_, int i_87_, int i_88_);

	public native void EA(int i, int i_89_, int i_90_, int i_91_);

	public void a(Interface13 interface13) {
		wa var_wa = (wa) interface13;
		n(var_wa.aJ3652.nativeid, var_wa.aXa3651.nativeid);
	}

	public void A() {
		/* empty */
	}

	public Model a(Mesh class132, int i, int i_92_, int i_93_, int i_94_) {
		return new i(this, aYa3456, class132, i, i_92_, i_93_, i_94_);
	}

	public native void ra(int i, int i_95_, int i_96_, int i_97_);

	private native void d(int i);

	public void a(Rectangle[] rectangles, int i, int i_98_, int i_99_) throws Exception_Sub1 {
		if (aP3474 == null)
			throw new IllegalStateException("off");
		aP3474.method2772(rectangles, i, i_98_, i_99_);
	}

	public native void aa(int i, int i_100_, int i_101_, int i_102_, int i_103_, int i_104_);

	public native void T(int i, int i_105_, int i_106_, int i_107_);

	public oa(Canvas canvas, d var_d, int i, int i_108_) {
		super(var_d);
		aClass155_3455 = new NodeDeque();
		anInt3470 = 4096;
		anInt3469 = 4096;
		aClass263_3472 = new HashTable(4);
		aBoolean3475 = false;
		do {
			try {
				if (!Class366_Sub4.method3779("sw3d", (byte) -17))
					throw new RuntimeException("");
				Class376.method3956(-108);
				MA(aD1299, 0, 0);
				Class338.method3437(true, false, false);
				aBoolean3475 = true;
				aClass373_3468 = new ja();
				a(new ja());
				f(1);
				i(0);
				if (canvas == null)
					break;
				b(canvas, i, i_108_);
				b(canvas);
			} catch (Throwable throwable) {
				this.method1091((byte) -113);
				throw new RuntimeException();
			}
			break;
		} while (false);
	}

	private native void wa(int i, int i_109_, int i_110_, int i_111_, int i_112_, int i_113_);

	public boolean z() {
		return false;
	}

	public Sprite a(int i, int i_114_, int i_115_, int i_116_, boolean bool) {
		return new j(this, i, i_114_, i_115_, i_116_, !bool);
	}

	public native void f(int i, int i_117_);

	public native void X(int i);

	public void y() {
		b(aP3474.aCanvas3478);
	}

	public void a(za var_za) {
		aYa3456 = (ya) var_za;
		va(var_za);
	}

	protected synchronized void finalize() {
		this.method1091((byte) -81);
		if (nativeid != 0L)
			Class376.method3955((byte) 123, this);
	}

	public native int M();

	public native void pa();

	public boolean u() {
		return true;
	}

	public Class373 m() {
		return new ja();
	}

	public boolean k() {
		return false;
	}

	public native void DA(int i, int i_118_, int i_119_, int i_120_);

	public void a(Canvas canvas, int i, int i_121_) {
		p var_p = (p) aClass263_3472.get((long) canvas.hashCode());
		var_p.method2771(canvas, i, i_121_);
		if (canvas != null && canvas == aP3474.aCanvas3478)
			b(canvas);
	}

	public Class373 f() {
		return aClass373_3468;
	}

	public void a(int[] is) {
		Dimension dimension = aP3474.aCanvas3478.getSize();
		is[0] = dimension.width;
		is[1] = dimension.height;
	}

	private native void Z(int i, int i_122_, int i_123_, int i_124_, int i_125_, int i_126_, aa var_aa, int i_127_,
			int i_128_);

	public native void K(int[] is);

	public Class241 c(int i, int i_129_, int i_130_, int i_131_, int i_132_, int i_133_) {
		return null;
	}

	public boolean n() {
		return false;
	}

	public a C() {
		for (int i = 0; i < anInt3476; i++) {
			if (anAArray3477[i].aRunnable3450 == Thread.currentThread())
				return anAArray3477[i];
		}
		return null;
	}

	public void b(int i, int i_134_, int i_135_, int i_136_, int i_137_, int i_138_) {
		U(i, i_134_, i_135_, i_137_, i_138_);
		U(i, i_134_ + i_136_ - 1, i_135_, i_137_, i_138_);
		P(i, i_134_ + 1, i_136_ - 1, i_137_, i_138_);
		P(i + i_135_ - 1, i_134_ + 1, i_136_ - 1, i_137_, i_138_);
	}

	private boolean c(short i) {
		synchronized (this) {
			MaterialRaw class170 = aD1299.method14(i, -9412);
			if (class170 == null)
				return false;
			AA(i, class170.aShort1775, class170.anInt1788, class170.aByte1774, class170.aByte1784, class170.anInt1794,
					class170.small_sized, class170.aByte1773, class170.aByte1793, class170.speed_u, class170.speed_v,
					class170.aBoolean1792, class170.aBoolean1786, class170.aBoolean1785, class170.aBoolean1779,
					class170.aBoolean1780, class170.aByte1795, class170.aBoolean1790, class170.aBoolean1789,
					class170.anInt1782);
		}
		return true;
	}

	public Class373 o() {
		return aClass373_3473;
	}

	public native void za(int i, int i_139_, int i_140_, int i_141_, int i_142_);

	public void g(int i) {
		Class376.method3954(-94);
		d(i);
		for (ya var_ya = (ya) aClass155_3455.removeFirst((byte) 120); var_ya != null; var_ya = (ya) aClass155_3455
				.removeNext(1001))
			var_ya.r();
	}

	public native void Q(int i, int i_143_, int i_144_, int i_145_, int i_146_, int i_147_, byte[] is, int i_148_,
			int i_149_);

	public aa a(int i, int i_150_, int[] is, int[] is_151_) {
		return new na(this, aYa3456, i, i_150_, is, is_151_);
	}

	public boolean x() {
		return true;
	}

	public void a(int i, int i_152_, int i_153_, int i_154_, int i_155_, int i_156_, aa var_aa, int i_157_, int i_158_,
			int i_159_, int i_160_, int i_161_) {
		/* empty */
	}

	public boolean l() {
		return true;
	}

	public void e() {
		/* empty */
	}

	public void a(Class390 class390, int i) {
		a(class390, false);
		C().method139(this, anIntArray3467, anIntArray3461, anIntArray3463, aShortArray3471,
				class390.aClass84_3282.method820(0));
	}

	public void a(boolean bool) {
		/* empty */
	}

	public void b(Canvas canvas) {
		if (canvas != null) {
			p var_p = (p) aClass263_3472.get((long) canvas.hashCode());
			aP3474 = var_p;
			t(var_p);
		} else {
			aP3474 = null;
			t(null);
		}
	}

	private native void CA(short i, int[] is, short i_162_, int i_163_, byte i_164_, byte i_165_, int i_166_,
			boolean bool, byte i_167_, byte i_168_, byte i_169_, byte i_170_, boolean bool_171_, boolean bool_172_,
			boolean bool_173_, boolean bool_174_, boolean bool_175_, byte i_176_, boolean bool_177_, boolean bool_178_,
			int i_179_);

	public void a(Class373 class373) {
		aClass373_3473 = class373;
		ma(((ja) class373).nativeid);
	}

	public native void C(boolean bool);

	public native void xa(float f);

	public boolean B() {
		return true;
	}

	public Class241 a(Class241 class241, Class241 class241_180_, float f, Class241 class241_181_) {
		return null;
	}

	private native void t(p var_p);

	public native int r(int i, int i_182_, int i_183_, int i_184_, int i_185_, int i_186_, int i_187_);

	public void h(int i) {
		anInt3469 = anInt3470 = i;
		if (anInt3476 > 1)
			throw new IllegalStateException("No MT");
		f(anInt3476);
		i(0);
	}

	public void c(int i) {
		anAArray3477[i].method142();
	}

	public void a(int i, int i_188_, int i_189_, int i_190_) {
		/* empty */
	}

	public void f(int i) {
		anInt3476 = i;
		anAArray3477 = new a[anInt3476];
		for (int i_191_ = 0; i_191_ < anInt3476; i_191_++)
			anAArray3477[i_191_] = new a(this, anInt3469, anInt3470);
	}

	public void b(boolean bool) {
		/* empty */
	}

	public Interface11 a(int i, int i_192_) {
		return new xa(i, i_192_);
	}

	public void a(int i, int i_193_, int i_194_, int i_195_, int i_196_, int i_197_, int i_198_, int i_199_,
			int i_200_) {
		/* empty */
	}

	public Interface13 a(Interface19 interface19, Interface11 interface11) {
		return new wa(this, (j) interface19, (xa) interface11);
	}

	public int e(int i, int i_201_) {
		int i_202_ = i & 0xfffff;
		int i_203_ = i_201_ & 0xfffff;
		return i_202_ & i_203_ ^ i_203_;
	}

	public native int XA();

	public void w() {
		/* empty */
	}

	public Sprite a(int i, int i_204_, boolean bool) {
		return new j(this, i, i_204_);
	}

	private native void n(long l, long l_205_);

	public native void U(int i, int i_206_, int i_207_, int i_208_, int i_209_);

	public native void la();

	public void b(Canvas canvas, int i, int i_210_) {
		p var_p = (p) aClass263_3472.get((long) canvas.hashCode());
		if (var_p == null) {
			canvas.setIgnoreRepaint(true);
			var_p = new p(this, canvas, i, i_210_);
			aClass263_3472.put((long) canvas.hashCode(), var_p);
		} else if (var_p.anInt3479 != i || var_p.anInt3481 != i_210_)
			var_p.method2771(canvas, i, i_210_);
	}

	private native void MA(d var_d, int i, int i_211_);

	public void a(int i, int i_212_, int i_213_, int i_214_, int i_215_, int i_216_, aa var_aa, int i_217_,
			int i_218_) {
		Z(i, i_212_, i_213_, i_214_, i_215_, i_216_, var_aa, i_217_, i_218_);
	}

	public boolean b() {
		return false;
	}

	public void a(Canvas canvas) {
		if (aP3474.aCanvas3478 == canvas)
			b((Canvas) null);
		p var_p = (p) aClass263_3472.get((long) canvas.hashCode());
		if (var_p != null) {
			var_p.unlink();
			var_p.method2769();
		}
	}

	public void a(Class241 class241) {
		/* empty */
	}

	public native int[] Y();

	public native void L(int i, int i_219_, int i_220_);

	public boolean v() {
		return true;
	}

	private void g() {
		System.gc();
		System.runFinalization();
		Class376.method3954(-123);
	}

	public native void F(int i, int i_221_);

	public native void b(int i, int i_222_, int i_223_, int i_224_, double d);

	public Sprite a(Class186 class186, boolean bool) {
		j var_j = new j(this, class186.anIntArray1900, class186.aByteArray1901, class186.aByteArray1905, 0,
				class186.anInt1899, class186.anInt1899, class186.anInt1904);
		var_j.method4097(class186.anInt1903, class186.anInt1906, class186.anInt1907, class186.anInt1902);
		return var_j;
	}

	public native void GA(int i);

	public s a(int i, int i_225_, int[][] is, int[][] is_226_, int i_227_, int i_228_, int i_229_) {
		return new t(this, aYa3456, i, i_225_, is, is_226_, i_227_, i_228_, i_229_);
	}

	static {
		anIntArray3459 = new int[6];
		aFloatArray3464 = new float[20];
		aFloatArray3466 = aFloatArray3464;
		anIntArray3461 = new int[8191];
		anIntArray3467 = anIntArray3458;
		anIntArray3465 = anIntArray3458;
		aShortArray3471 = new short[8191];
	}
}
