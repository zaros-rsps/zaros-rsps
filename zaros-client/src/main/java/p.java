

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Rectangle;

import net.zaros.client.Class376;
import net.zaros.client.Class72;
import net.zaros.client.Interface3;
import net.zaros.client.Node;

public class p extends Node implements Interface3 {
	public Canvas aCanvas3478;
	public int anInt3479;
	private static long aLong3480;
	public int anInt3481;
	private static boolean aBoolean3482 = false;
	public long nativeid;

	private final native void oa(Canvas canvas, int i, int i_0_);

	private final native void sa(oa var_oa, Canvas canvas, int i, int i_1_);

	protected final void finalize() {
		if (nativeid != 0L)
			Class376.method3955((byte) 123, this);
	}

	final void method2768(int i, int i_2_) {
		try {
			synchronized (aCanvas3478.getTreeLock()) {
				Dimension dimension = aCanvas3478.getSize();
				H(i, i_2_, dimension.width, dimension.height);
				aBoolean3482 = false;
			}
		} catch (Exception exception) {
			method2770(exception);
		}
	}

	public final native void w(boolean bool);

	final void method2769() {
		w(true);
		nativeid = 0L;
		aCanvas3478 = null;
	}

	private final void method2770(Exception exception) {
		if (!aBoolean3482) {
			aLong3480 = Class72.method771(-112);
			aBoolean3482 = true;
		} else if (Class72.method771(-126) - aLong3480 < 30000L)
			aCanvas3478.repaint();
		else
			throw new RuntimeException(exception.getMessage());
	}

	final void method2771(Canvas canvas, int i, int i_3_) {
		anInt3479 = i;
		anInt3481 = i_3_;
		oa(canvas, i, i_3_);
	}

	p(oa var_oa, Canvas canvas, int i, int i_4_) {
		aCanvas3478 = canvas;
		anInt3479 = i;
		anInt3481 = i_4_;
		sa(var_oa, aCanvas3478, i, i_4_);
	}

	private final native void H(int i, int i_5_, int i_6_, int i_7_);

	private final native void K(int i, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_);

	final void method2772(Rectangle[] rectangles, int i, int i_13_, int i_14_) {
		try {
			synchronized (aCanvas3478.getTreeLock()) {
				for (int i_15_ = 0; i_15_ < i; i_15_++) {
					Rectangle rectangle = rectangles[i_15_];
					if (rectangle.width > 0 && rectangle.height > 0)
						K(rectangle.x, rectangle.y, rectangle.width, rectangle.height, i_13_, i_14_);
				}
				aBoolean3482 = false;
			}
		} catch (Exception exception) {
			method2770(exception);
		}
	}
}
