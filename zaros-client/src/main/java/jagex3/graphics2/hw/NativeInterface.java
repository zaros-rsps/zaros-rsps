/* NativeInterface - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagex3.graphics2.hw;

public class NativeInterface {
	long peer;

	public final native void copyPositions(int[] is, int[] is_0_, int[] is_1_, short[] is_2_, int i, int i_3_, int i_4_, long l);

	public final native void release();

	public final native void setAmbient(float f);

	public final native void setSunColour(float f, float f_5_, float f_6_, float f_7_, float f_8_);

	private final native void init(int i, int i_9_);

	public NativeInterface(int i, int i_10_) {
		init(i, i_10_);
	}

	public final native void copyColours(short[] is, byte[] is_11_, short[] is_12_, int i, short[] is_13_, int i_14_, int i_15_, int i_16_, long l);

	public final native void copyTexCoords(float[] fs, float[] fs_17_, int i, int i_18_, int i_19_, long l);

	public final native void copyNormals(short[] is, short[] is_20_, short[] is_21_, byte[] is_22_, float f, float f_23_, int i, int i_24_, int i_25_, long l);

	public final native void initTextureMetrics(int i, byte i_26_, byte i_27_);

	public final native void copyLighting(short[] is, byte[] is_28_, short[] is_29_, short[] is_30_, short[] is_31_, short[] is_32_, byte[] is_33_, int i, int i_34_, short[] is_35_, int i_36_, int i_37_, int i_38_, long l);

	public final native void setSunDirection(float f, float f_39_, float f_40_);
}
