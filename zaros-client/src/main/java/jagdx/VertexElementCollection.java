/* VertexElementCollection - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;
import jaclib.peer.sa;
import jaclib.peer.vba;

public class VertexElementCollection extends sa {
	private final native void init();

	public VertexElementCollection(vba var_vba) {
		super(var_vba);
		init();
	}

	public final native void finish();

	public final native void addElement(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_);

	public final native void reset();
}
