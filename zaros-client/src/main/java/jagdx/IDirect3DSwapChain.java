/* IDirect3DSwapChain - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;
import jaclib.peer.IUnknown;
import jaclib.peer.vba;

public class IDirect3DSwapChain extends IUnknown {
	private vba b;

	private final native int _GetBackBuffer(int i, int i_0_, IDirect3DSurface idirect3dsurface);

	public final IDirect3DSurface a(int i, int i_1_) {
		IDirect3DSurface idirect3dsurface = new IDirect3DSurface(b);
		int i_2_ = _GetBackBuffer(i, i_1_, idirect3dsurface);
		if (aj.a(i_2_, (byte) -91))
			throw new tn(String.valueOf(i_2_));
		return idirect3dsurface;
	}

	IDirect3DSwapChain(vba var_vba) {
		super(var_vba);
		b = var_vba;
	}

	public final native int Present(int i);
}
