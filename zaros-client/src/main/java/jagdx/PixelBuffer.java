/* PixelBuffer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;
import jaclib.memory.Buffer;
import jaclib.peer.sa;
import jaclib.peer.vba;

public class PixelBuffer extends sa implements Buffer {
	private final native void putub(byte[] is, int i, int i_0_, int i_1_);

	public final void a(int[] is, int i, int i_2_, int i_3_) {
		if (is == null | i < 0 | i_3_ + i > is.length | i_2_ < 0 || getSize() < i_2_ + i_3_ * 4)
			throw new tn();
		geti(is, i, i_2_, i_3_);
	}

	public final native int getSlicePitch();

	private final native void getub(byte[] is, int i, int i_4_, int i_5_);

	private final native void puti(int[] is, int i, int i_6_, int i_7_);

	public final long getAddress() {
		return 0L;
	}

	public PixelBuffer(vba var_vba) {
		super(var_vba);
		init();
	}

	public final native int getSize();

	public final void a(byte[] is, int i, int i_8_, int i_9_) {
		if (is.length < i + i_9_ | (i < 0 | is == null) | i_8_ < 0 || i_8_ + i_9_ > getSize())
			throw new tn();
		putub(is, i, i_8_, i_9_);
	}

	private final native void init();

	public final void b(int[] is, int i, int i_10_, int i_11_) {
		if (i + i_11_ > is.length | (i < 0 | is == null) | i_10_ < 0 || i_11_ * 4 + i_10_ > getSize())
			throw new tn();
		puti(is, i, i_10_, i_11_);
	}

	private final native void geti(int[] is, int i, int i_12_, int i_13_);

	public final native int getRowPitch();
}
