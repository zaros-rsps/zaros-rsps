/* IDirect3DVertexBuffer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;
import jaclib.memory.Source;
import jaclib.peer.IUnknown;
import jaclib.peer.vba;

public class IDirect3DVertexBuffer extends IUnknown {
	int b;

	public final native int Lock(int i, int i_0_, int i_1_, GeometryBuffer geometrybuffer);

	public final native int Unlock();

	IDirect3DVertexBuffer(vba var_vba) {
		super(var_vba);
	}

	public final boolean a(Source source, int i, int i_2_, int i_3_, int i_4_) {
		if (source == null || i < 0 || i_3_ > i + source.getSize())
			throw new tn("");
		if (i_2_ < 0 || b + i_2_ < i_3_)
			throw new tn("");
		return _Update((long) i + source.getAddress(), i_2_, i_3_, i_4_);
	}

	protected final long a() {
		b = 0;
		return super.a();
	}

	private final native boolean _Update(long l, int i, int i_5_, int i_6_);
}
