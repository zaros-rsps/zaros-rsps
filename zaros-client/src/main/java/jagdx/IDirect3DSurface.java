/* IDirect3DSurface - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;
import jaclib.peer.IUnknown;
import jaclib.peer.vba;

public class IDirect3DSurface extends IUnknown {
	public final native boolean UnlockRect();

	public final native int LockRect(int i, int i_0_, int i_1_, int i_2_, int i_3_, PixelBuffer pixelbuffer);

	IDirect3DSurface(vba var_vba) {
		super(var_vba);
	}
}
