/* IDirect3DEventQuery - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;
import jaclib.peer.IUnknown;
import jaclib.peer.vba;

public class IDirect3DEventQuery extends IUnknown {
	public final native int Issue();

	IDirect3DEventQuery(vba var_vba) {
		super(var_vba);
	}

	public final native int IsSignaled();
}
