/* D3DPRESENT_PARAMETERS - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;
import java.awt.Canvas;

public class D3DPRESENT_PARAMETERS {
	public int PresentationInterval = 0;
	public int BackBufferFormat = 0;
	public int SwapEffect;
	public int BackBufferCount;
	public int MultiSampleType = 0;
	public Canvas DeviceWindow;
	public int MultiSampleQuality;
	public boolean EnableAutoDepthStencil;
	public int Flags;
	public int BackBufferWidth;
	public int FullScreen_RefreshRateInHz;
	public int AutoDepthStencilFormat;
	public int BackBufferHeight;
	public boolean Windowed;

	public D3DPRESENT_PARAMETERS(Canvas canvas) {
		SwapEffect = 1;
		MultiSampleQuality = 0;
		BackBufferCount = 0;
		BackBufferWidth = 0;
		AutoDepthStencilFormat = 0;
		BackBufferHeight = 0;
		DeviceWindow = canvas;
	}
}
