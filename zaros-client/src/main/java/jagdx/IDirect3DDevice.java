/* IDirect3DDevice - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;
import jaclib.peer.IUnknown;
import jaclib.peer.vba;

public final class IDirect3DDevice extends IUnknown {
	private static float[] c = new float[4];
	private vba b;

	private final native int _CreateOffscreenPlainSurface(int i, int i_0_, int i_1_, int i_2_, IDirect3DSurface idirect3dsurface);

	private final native int _CreateRenderTarget(int i, int i_3_, int i_4_, int i_5_, int i_6_, boolean bool, IDirect3DSurface idirect3dsurface);

	private final native int _GetSwapChain(int i, IDirect3DSwapChain idirect3dswapchain);

	IDirect3DDevice(vba var_vba) {
		super(var_vba);
		b = var_vba;
	}

	public final IDirect3DSurface a(int i, int i_7_, int i_8_, int i_9_, int i_10_, boolean bool) {
		IDirect3DSurface idirect3dsurface = new IDirect3DSurface(b);
		int i_11_ = _CreateRenderTarget(i, i_7_, i_8_, i_9_, i_10_, bool, idirect3dsurface);
		if (aj.a(i_11_, (byte) -91))
			throw new tn(String.valueOf(i_11_));
		return idirect3dsurface;
	}

	public final native int SetViewport(int i, int i_12_, int i_13_, int i_14_, float f, float f_15_);

	public final native int DrawIndexedPrimitive(int i, int i_16_, int i_17_, int i_18_, int i_19_, int i_20_);

	public final native boolean LightEnable(int i, boolean bool);

	public final int a(int i, float[] fs) {
		return SetVertexShaderConstantF(i, fs, fs.length / 4);
	}

	public final native int SetVertexDeclaration(IDirect3DVertexDeclaration idirect3dvertexdeclaration);

	private final native int SetRenderStatef(int i, float f);

	public final native int BeginScene();

	public final int b(int i, float f, float f_21_, float f_22_, float f_23_) {
		c[1] = f_21_;
		c[0] = f;
		c[2] = f_22_;
		c[3] = f_23_;
		return SetVertexShaderConstantF(i, c, 1);
	}

	public final IDirect3DVertexShader a(byte[] is) {
		if (is == null)
			return null;
		IDirect3DVertexShader idirect3dvertexshader = new IDirect3DVertexShader(b);
		int i = _CreateVertexShader(is, idirect3dvertexshader);
		if (aj.a(i, (byte) -91))
			throw new tn(String.valueOf(i));
		return idirect3dvertexshader;
	}

	public final native int SetSamplerState(int i, int i_24_, int i_25_);

	public final IDirect3DEventQuery c() {
		IDirect3DEventQuery idirect3deventquery = new IDirect3DEventQuery(b);
		if (aj.a(_CreateEventQuery(idirect3deventquery), (int) 56))
			return (idirect3deventquery.a((byte) -125) ? idirect3deventquery : null);
		return null;
	}

	public final native int _CreatePixelShader(byte[] is, IDirect3DPixelShader idirect3dpixelshader);

	public final IDirect3DSurface b() {
		IDirect3DSurface idirect3dsurface = new IDirect3DSurface(b);
		int i = _GetDepthStencilSurface(idirect3dsurface);
		if (aj.a(i, (byte) -91))
			throw new tn(String.valueOf(i));
		return idirect3dsurface;
	}

	public final native int SetPixelShader(IDirect3DPixelShader idirect3dpixelshader);

	private final native int _GetBackBuffer(int i, int i_26_, int i_27_, IDirect3DSurface idirect3dsurface);

	public final native int SetVertexShaderConstantF(int i, float[] fs, int i_28_);

	public final native int SetVertexShader(IDirect3DVertexShader idirect3dvertexshader);

	public final native int SetTexture(int i, IDirect3DBaseTexture idirect3dbasetexture);

	public final int a(int i, float f, float f_29_, float f_30_, float f_31_) {
		c[2] = f_30_;
		c[3] = f_31_;
		c[1] = f_29_;
		c[0] = f;
		return SetPixelShaderConstantF(i, c, 1);
	}

	public final native int _CreateVertexShader(byte[] is, IDirect3DVertexShader idirect3dvertexshader);

	public final native int _CreateVolumeTexture(int i, int i_32_, int i_33_, int i_34_, int i_35_, int i_36_, int i_37_, IDirect3DVolumeTexture idirect3dvolumetexture);

	public final native int SetLight(int i, D3DLIGHT d3dlight);

	private final native int _GetRenderTarget(int i, IDirect3DSurface idirect3dsurface);

	private final native int _GetDepthStencilSurface(IDirect3DSurface idirect3dsurface);

	public final native int SetScissorRect(int i, int i_38_, int i_39_, int i_40_);

	public final native int Clear(int i, int i_41_, float f, int i_42_);

	public final int a(int i, float f) {
		return SetRenderStatef(i, f);
	}

	public final IDirect3DCubeTexture a(int i, int i_43_, int i_44_, int i_45_, int i_46_) {
		IDirect3DCubeTexture idirect3dcubetexture = new IDirect3DCubeTexture(b);
		int i_47_ = _CreateCubeTexture(i, i_43_, i_44_, i_45_, i_46_, idirect3dcubetexture);
		if (aj.a(i_47_, (byte) -91))
			throw new tn(String.valueOf(i_47_));
		return idirect3dcubetexture;
	}

	public final native int SetStreamSource(int i, IDirect3DVertexBuffer idirect3dvertexbuffer, int i_48_, int i_49_);

	public final int a(int i, boolean bool) {
		return SetRenderStateb(i, bool);
	}

	public final IDirect3DVertexDeclaration a(VertexElementCollection vertexelementcollection, IDirect3DVertexDeclaration idirect3dvertexdeclaration) {
		if (idirect3dvertexdeclaration == null)
			idirect3dvertexdeclaration = new IDirect3DVertexDeclaration(b);
		else
			idirect3dvertexdeclaration.b((byte) 83);
		int i = _CreateVertexDeclaration(vertexelementcollection, idirect3dvertexdeclaration);
		if (aj.a(i, (byte) -91))
			throw new tn(String.valueOf(i));
		return idirect3dvertexdeclaration;
	}

	public final native int StretchRect(IDirect3DSurface idirect3dsurface, int i, int i_50_, int i_51_, int i_52_, IDirect3DSurface idirect3dsurface_53_, int i_54_, int i_55_, int i_56_, int i_57_, int i_58_);

	public final native int SetTextureStageState(int i, int i_59_, int i_60_);

	public final IDirect3DTexture a(int i, int i_61_, int i_62_, int i_63_, int i_64_, int i_65_) {
		IDirect3DTexture idirect3dtexture = new IDirect3DTexture(b);
		int i_66_ = _CreateTexture(i, i_61_, i_62_, i_63_, i_64_, i_65_, idirect3dtexture);
		if (aj.a(i_66_, (byte) -91))
			throw new tn(String.valueOf(i_66_));
		return idirect3dtexture;
	}

	public final native int Reset(D3DPRESENT_PARAMETERS d3dpresent_parameters);

	public final native int DrawPrimitive(int i, int i_67_, int i_68_);

	public final IDirect3DIndexBuffer a(int i, int i_69_, int i_70_, int i_71_, IDirect3DIndexBuffer idirect3dindexbuffer) {
		if (idirect3dindexbuffer == null)
			idirect3dindexbuffer = new IDirect3DIndexBuffer(b);
		else
			idirect3dindexbuffer.b((byte) 83);
		int i_72_ = _CreateIndexBuffer(i, i_69_, i_70_, i_71_, idirect3dindexbuffer);
		if (aj.a(i_72_, (byte) -91))
			throw new tn(String.valueOf(i_72_));
		return idirect3dindexbuffer;
	}

	private final native int SetRenderStateb(int i, boolean bool);

	public final native int TestCooperativeLevel();

	public final native int _CreateTexture(int i, int i_73_, int i_74_, int i_75_, int i_76_, int i_77_, IDirect3DTexture idirect3dtexture);

	public final IDirect3DVertexBuffer a(int i, int i_78_, int i_79_, int i_80_, IDirect3DVertexBuffer idirect3dvertexbuffer) {
		if (idirect3dvertexbuffer == null)
			idirect3dvertexbuffer = new IDirect3DVertexBuffer(b);
		else
			idirect3dvertexbuffer.b((byte) 83);
		int i_81_ = _CreateVertexBuffer(i, i_78_, i_79_, i_80_, idirect3dvertexbuffer);
		if (aj.a(i_81_, (byte) -91))
			throw new tn(String.valueOf(i_81_));
		idirect3dvertexbuffer.b = i;
		return idirect3dvertexbuffer;
	}

	public final native int SetRenderState(int i, int i_82_);

	private final native int _CreateIndexBuffer(int i, int i_83_, int i_84_, int i_85_, IDirect3DIndexBuffer idirect3dindexbuffer);

	public final IDirect3DSurface a(int i) {
		IDirect3DSurface idirect3dsurface = new IDirect3DSurface(b);
		int i_86_ = _GetRenderTarget(i, idirect3dsurface);
		if (aj.a(i_86_, (byte) -91))
			throw new tn(String.valueOf(i_86_));
		return idirect3dsurface;
	}

	public final IDirect3DSwapChain b(int i) {
		IDirect3DSwapChain idirect3dswapchain = new IDirect3DSwapChain(b);
		int i_87_ = _GetSwapChain(i, idirect3dswapchain);
		if (aj.a(i_87_, (byte) -91))
			throw new tn(String.valueOf(i_87_));
		return idirect3dswapchain;
	}

	public final native int SetTransform(int i, float[] fs);

	private final native int _CreateVertexDeclaration(VertexElementCollection vertexelementcollection, IDirect3DVertexDeclaration idirect3dvertexdeclaration);

	public final native int _CreateCubeTexture(int i, int i_88_, int i_89_, int i_90_, int i_91_, IDirect3DCubeTexture idirect3dcubetexture);

	public final IDirect3DVolumeTexture a(int i, int i_92_, int i_93_, int i_94_, int i_95_, int i_96_, int i_97_) {
		IDirect3DVolumeTexture idirect3dvolumetexture = new IDirect3DVolumeTexture(b);
		int i_98_ = _CreateVolumeTexture(i, i_92_, i_93_, i_94_, i_95_, i_96_, i_97_, idirect3dvolumetexture);
		if (aj.a(i_98_, (byte) -91))
			throw new tn(String.valueOf(i_98_));
		return idirect3dvolumetexture;
	}

	public final native int SetPixelShaderConstantF(int i, float[] fs, int i_99_);

	private final native int _CreateVertexBuffer(int i, int i_100_, int i_101_, int i_102_, IDirect3DVertexBuffer idirect3dvertexbuffer);

	public final native int SetIndices(IDirect3DIndexBuffer idirect3dindexbuffer);

	public final native int EndScene();

	public final native int SetFVF(int i);

	private final native int _CreateDepthStencilSurface(int i, int i_103_, int i_104_, int i_105_, int i_106_, boolean bool, IDirect3DSurface idirect3dsurface);

	private final native int _CreateEventQuery(IDirect3DEventQuery idirect3deventquery);

	public final IDirect3DPixelShader b(byte[] is) {
		if (is == null)
			return null;
		IDirect3DPixelShader idirect3dpixelshader = new IDirect3DPixelShader(b);
		int i = _CreatePixelShader(is, idirect3dpixelshader);
		if (aj.a(i, (byte) -91))
			throw new tn(String.valueOf(i));
		return idirect3dpixelshader;
	}
}
