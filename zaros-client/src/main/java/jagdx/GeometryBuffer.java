/* GeometryBuffer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;
import jaclib.memory.Buffer;
import jaclib.peer.sa;
import jaclib.peer.vba;

public class GeometryBuffer extends sa implements Buffer {
	public final void a(byte[] is, int i, int i_0_, int i_1_) {
		if (i_0_ < 0 | (is == null | i < 0 | is.length < i_1_ + i) || i_0_ + i_1_ > getSize())
			throw new tn();
		putub(is, i, i_0_, i_1_);
	}

	private final native void init();

	public final native long getAddress();

	private final native void putub(byte[] is, int i, int i_2_, int i_3_);

	public GeometryBuffer(vba var_vba) {
		super(var_vba);
		init();
	}

	private final native void getub(byte[] is, int i, int i_4_, int i_5_);

	public final native int getSize();
}
