/* IDirect3D - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagdx;
import java.awt.Canvas;

import jaclib.peer.IUnknown;
import jaclib.peer.vba;

public class IDirect3D extends IUnknown {
	private vba b;

	public final native int CheckDeviceFormat(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_);

	public final int a(int i, D3DDISPLAYMODE d3ddisplaymode) {
		return _GetAdapterDisplayMode(i, d3ddisplaymode);
	}

	public final native int CheckDeviceMultiSampleType(int i, int i_5_, int i_6_, boolean bool, int i_7_);

	private final native int _GetDeviceCaps(int i, int i_8_, D3DCAPS d3dcaps);

	public final IDirect3DDevice a(int i, int i_9_, Canvas canvas, int i_10_, D3DPRESENT_PARAMETERS d3dpresent_parameters) {
		IDirect3DDevice idirect3ddevice = new IDirect3DDevice(b);
		int i_11_ = _CreateDevice(i, i_9_, canvas, i_10_, d3dpresent_parameters, idirect3ddevice);
		if (aj.a(i_11_, (byte) -91))
			throw new tn(String.valueOf(i_11_));
		return idirect3ddevice;
	}

	public final native int CheckDeviceType(int i, int i_12_, int i_13_, int i_14_, boolean bool);

	public final D3DADAPTER_IDENTIFIER b(int i, int i_15_) {
		D3DADAPTER_IDENTIFIER d3dadapter_identifier = new D3DADAPTER_IDENTIFIER();
		int i_16_ = _GetAdapterIdentifier(i, i_15_, d3dadapter_identifier);
		if (aj.a(i_16_, (byte) -91))
			throw new tn(String.valueOf(i_16_));
		return d3dadapter_identifier;
	}

	private static final native int _Direct3DCreate(int i, IDirect3D idirect3d);

	private final native int _GetAdapterDisplayMode(int i, D3DDISPLAYMODE d3ddisplaymode);

	private final native int _GetAdapterIdentifier(int i, int i_17_, D3DADAPTER_IDENTIFIER d3dadapter_identifier);

	public final native int CheckDepthStencilMatch(int i, int i_18_, int i_19_, int i_20_, int i_21_);

	private IDirect3D(vba var_vba) {
		super(var_vba);
		b = var_vba;
	}

	public static final IDirect3D a(int i, vba var_vba) {
		IDirect3D idirect3d = new IDirect3D(var_vba);
		int i_22_ = _Direct3DCreate(i, idirect3d);
		if (aj.a(i_22_, (byte) -91))
			throw new tn(String.valueOf(i_22_));
		return idirect3d;
	}

	private final native int _CreateDevice(int i, int i_23_, Canvas canvas, int i_24_, D3DPRESENT_PARAMETERS d3dpresent_parameters, IDirect3DDevice idirect3ddevice);

	public final D3DCAPS a(int i, int i_25_) {
		D3DCAPS d3dcaps = new D3DCAPS();
		int i_26_ = _GetDeviceCaps(i, i_25_, d3dcaps);
		if (aj.a(i_26_, (byte) -91))
			throw new tn(String.valueOf(i_26_));
		return d3dcaps;
	}
}
