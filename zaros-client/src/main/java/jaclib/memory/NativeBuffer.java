/* NativeBuffer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.memory;

public class NativeBuffer implements Buffer, Source {
	private int b = -1;
	private long a;

	public void a(byte[] is, int i, int i_0_, int i_1_) {
		if (b < i_0_ + i_1_ | (i_0_ < 0 | (is.length < i + i_1_ | (i < 0 | (a == 0L | is == null)))))
			throw new RuntimeException();
		put(a, is, i, i_0_, i_1_);
	}

	private final native void put(long l, byte[] is, int i, int i_2_, int i_3_);

	protected final void a(long l, int i) {
		a = l;
		b = i;
	}

	private final native void get(long l, byte[] is, int i, int i_4_, int i_5_);

	public final int getSize() {
		return b;
	}

	public final long getAddress() {
		return a;
	}
}
