/* Stream - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.memory;

public final class Stream {
	private int c;
	private int b;
	private byte[] e;
	private Buffer d;
	private int a;

	public final void a(Buffer buffer) {
		a(buffer, 0, buffer.getSize());
	}

	public final void c(int i) {
		if (e.length <= b + 3)
			b();
		e[b++] = (byte) (i >> 16);
		e[b++] = (byte) (i >> 8);
		e[b++] = (byte) i;
		e[b++] = (byte) (i >> 24);
	}

	public final void a(float f) {
		if (b + 3 >= e.length)
			b();
		int i = floatToRawIntBits(f);
		e[b++] = (byte) (i >> 24);
		e[b++] = (byte) (i >> 16);
		e[b++] = (byte) (i >> 8);
		e[b++] = (byte) i;
	}

	public final void b(int i) {
		if (b + 3 >= e.length)
			b();
		e[b++] = (byte) i;
		e[b++] = (byte) (i >> 8);
		e[b++] = (byte) (i >> 16);
		e[b++] = (byte) (i >> 24);
	}

	public final void a(int i, int i_0_, int i_1_, int i_2_) {
		if (e.length <= b + 3)
			b();
		e[b++] = (byte) i;
		e[b++] = (byte) i_0_;
		e[b++] = (byte) i_1_;
		e[b++] = (byte) i_2_;
	}

	public Stream(Buffer buffer, int i, int i_3_) {
		this(buffer.getSize() < 4096 ? buffer.getSize() : 4096);
		a(buffer, i, i_3_);
	}

	public Stream(Buffer buffer) {
		this(buffer, 0, buffer.getSize());
	}

	public final void b(float f) {
		if (e.length <= b + 3)
			b();
		int i = floatToRawIntBits(f);
		e[b++] = (byte) i;
		e[b++] = (byte) (i >> 8);
		e[b++] = (byte) (i >> 16);
		e[b++] = (byte) (i >> 24);
	}

	public final void e(int i) {
		if (b >= e.length)
			b();
		e[b++] = (byte) i;
	}

	public final int c() {
		return b + c;
	}

	public final void b(int i, int i_4_, int i_5_, int i_6_) {
		if (b + 3 >= e.length)
			b();
		e[b++] = (byte) i_5_;
		e[b++] = (byte) i_4_;
		e[b++] = (byte) i;
		e[b++] = (byte) i_6_;
	}

	public final void a(int i) {
		if (e.length <= b + 1)
			b();
		e[b++] = (byte) i;
		e[b++] = (byte) (i >> 8);
	}

	public final void b() {
		if (b > 0) {
			if (a < c + b)
				throw new RuntimeException();
			d.a(e, 0, c, b);
			c += b;
			b = 0;
		}
	}

	public Stream() {
		this(4096);
	}

	public static final native int floatToRawIntBits(float f);

	private final void a(Buffer buffer, int i, int i_7_) {
		b();
		d = buffer;
		c = i;
		a = i_7_ + i;
		if (a > buffer.getSize())
			throw new RuntimeException();
	}

	public final void f(int i) {
		b();
		c = i;
	}

	public static final boolean a() {
		if (getLSB(-65536) != -1)
			return false;
		return true;
	}

	private static final native byte getLSB(int i);

	public final void d(int i) {
		if (b + 1 >= e.length)
			b();
		e[b++] = (byte) (i >> 8);
		e[b++] = (byte) i;
	}

	private Stream(int i) {
		e = new byte[i];
	}
}
