/* NativeHeap - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.memory.heap;

public final class NativeHeap {
	private boolean a;
	long peer;
	private int b;

	public final NativeHeapBuffer a(int i, boolean bool) {
		if (!a)
			throw new IllegalStateException();
		return new NativeHeapBuffer(this, allocateBuffer(i, bool), i);
	}

	final synchronized native long getBufferAddress(int i);

	public final synchronized void a() {
		if (a)
			deallocateHeap();
		a = false;
	}

	public NativeHeap(int i) {
		b = i;
		allocateHeap(b);
		a = true;
	}

	final synchronized native void deallocateBuffer(int i);

	final synchronized native void get(int i, byte[] is, int i_0_, int i_1_, int i_2_);

	private final native void allocateHeap(int i);

	final synchronized boolean b() {
		return a;
	}

	final synchronized native void put(int i, byte[] is, int i_3_, int i_4_, int i_5_);

	private final native void deallocateHeap();

	protected final synchronized void finalize() throws Throwable {
		super.finalize();
		a();
	}

	final synchronized native int allocateBuffer(int i, boolean bool);
}
