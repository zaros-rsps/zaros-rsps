/* NativeHeapBuffer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.memory.heap;
import jaclib.memory.Buffer;
import jaclib.memory.Source;

public final class NativeHeapBuffer implements Buffer, Source {
	private boolean a = true;
	private int d;
	public int b;
	private NativeHeap c;

	private final synchronized boolean a() {
		if (!c.b() || !a)
			return false;
		return true;
	}

	public final int getSize() {
		return b;
	}

	public final long getAddress() {
		return c.getBufferAddress(d);
	}

	public final synchronized void a(byte[] is, int i, int i_0_, int i_1_) {
		if (!a() | is == null | i < 0 | is.length < i + i_1_ | i_0_ < 0 | b < i_0_ + i_1_)
			throw new RuntimeException();
		c.put(d, is, i, i_0_, i_1_);
	}

	protected final synchronized void finalize() throws Throwable {
		super.finalize();
		b();
	}

	NativeHeapBuffer(NativeHeap nativeheap, int i, int i_2_) {
		b = i_2_;
		c = nativeheap;
		d = i;
	}

	private final synchronized void b() {
		if (a())
			c.deallocateBuffer(d);
		a = false;
	}
}
