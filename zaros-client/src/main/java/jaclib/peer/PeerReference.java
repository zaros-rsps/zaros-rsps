/* PeerReference - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.peer;
import java.lang.ref.WeakReference;

abstract class PeerReference extends WeakReference {
	private long peer;
	PeerReference b;
	PeerReference a;

	final boolean a(boolean bool) {
		if (bool != true)
			setPeer(77L);
		return 0L != peer;
	}

	final long a(int i) {
		if (i != 0)
			a(-102);
		long l;
		if (0L != peer) {
			l = releasePeer(peer);
			peer = 0L;
		} else
			l = 0L;
		return l;
	}

	final void setPeer(long l) {
		a(0);
		peer = l;
	}

	protected abstract long releasePeer(long l);

	PeerReference(Peer peer, vba var_vba) {
		super(peer, var_vba.c);
		var_vba.b(58, this);
	}
}
