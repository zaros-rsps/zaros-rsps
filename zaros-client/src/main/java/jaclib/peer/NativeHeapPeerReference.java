/* NativeHeapPeerReference - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.peer;

class NativeHeapPeerReference extends PeerReference {
	NativeHeapPeerReference(sa var_sa, vba var_vba) {
		super((Peer) var_sa, var_vba);
	}

	protected final native long releasePeer(long l);
}
