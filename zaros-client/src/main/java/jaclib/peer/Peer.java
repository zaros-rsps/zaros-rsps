/* Peer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.peer;

abstract class Peer {
	protected PeerReference reference;

	protected Peer() {
		/* empty */
	}

	public final boolean a(byte i) {
		return reference.a(true);
	}

	protected long a() {
		return reference.a(0);
	}

	private static final native void init(Class var_class);

	static {
		init(PeerReference.class);
	}
}
