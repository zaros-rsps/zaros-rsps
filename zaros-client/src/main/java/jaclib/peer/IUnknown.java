/* IUnknown - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.peer;

public class IUnknown extends Peer {
	protected IUnknown(vba var_vba) {
		reference = new IUnknownReference(this, var_vba);
	}

	public final long b(byte i) {
		if (i != 83)
			return -104L;
		return super.a();
	}

	public final native long AddRef();
}
