package net.zaros.client;

/* Class296_Sub45 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class296_Sub45 extends Node {
	int anInt4950;
	Class296_Sub19 aClass296_Sub19_4951;
	volatile boolean aBoolean4952 = true;
	Class296_Sub45 aClass296_Sub45_4953;

	abstract int method2929();

	abstract Class296_Sub45 method2930();

	final void method2931(int[] is, int i, int i_0_) {
		if (aBoolean4952)
			method2934(is, i, i_0_);
		else
			method2933(i_0_);
	}

	public Class296_Sub45() {
		/* empty */
	}

	abstract Class296_Sub45 method2932();

	abstract void method2933(int i);

	abstract void method2934(int[] is, int i, int i_1_);

	int method2935() {
		return 255;
	}
}
