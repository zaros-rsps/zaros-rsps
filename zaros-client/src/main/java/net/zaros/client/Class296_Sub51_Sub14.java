package net.zaros.client;

/* Class296_Sub51_Sub14 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub14 extends TextureOperation {
	private boolean aBoolean6421;
	private boolean aBoolean6422 = true;
	static int anInt6423 = -1;

	static final Class322 method3114(boolean bool, ha var_ha, int i, boolean bool_0_, int i_1_) {
		if (i_1_ == -1)
			return null;
		if (Class338_Sub3_Sub1_Sub5.anIntArray6843 != null) {
			for (int i_2_ = 0; Class338_Sub3_Sub1_Sub5.anIntArray6843.length > i_2_; i_2_++) {
				if (Class338_Sub3_Sub1_Sub5.anIntArray6843[i_2_] == i_1_)
					return Class353.aClass322Array3049[i_2_];
			}
		}
		Class322 class322 = ((Class322) Class110.aClass113_1143.get((long) (i_1_ << 1 | (bool ? 1 : 0))));
		if (class322 != null) {
			if (bool_0_ && class322.aClass92_2828 == null) {
				Class92 class92 = Class259.method2240(Class368_Sub23.aClass138_5571, (byte) 10, i_1_);
				if (class92 == null)
					return null;
				class322.aClass92_2828 = class92;
			}
			return class322;
		}
		Class186[] class186s = Class186.method1868(Class296_Sub22.aClass138_4733, i_1_);
		if (class186s == null)
			return null;
		if (i != 9057)
			return null;
		Class92 class92 = Class259.method2240(Class368_Sub23.aClass138_5571, (byte) 10, i_1_);
		if (class92 == null)
			return null;
		if (!bool_0_)
			class322 = new Class322(var_ha.a(class92, class186s, bool));
		else
			class322 = new Class322(var_ha.a(class92, class186s, bool), class92);
		Class110.aClass113_1143.put(class322, (long) (i_1_ << 1 | (bool ? 1 : 0)));
		return class322;
	}

	final int[] get_monochrome_output(int i, int i_3_) {
		int[] is = aClass318_5035.method3335(i_3_, (byte) 28);
		if (i != 0)
			anInt6423 = -48;
		if (aClass318_5035.aBoolean2819) {
			int[] is_4_ = this.method3064(0, 0, (!aBoolean6421 ? i_3_ : -i_3_ + Class67.anInt753));
			if (aBoolean6422) {
				for (int i_5_ = 0; i_5_ < Class41_Sub10.anInt3769; i_5_++)
					is[i_5_] = is_4_[-i_5_ + Class41_Sub25.anInt3803];
			} else
				ArrayTools.removeElements(is_4_, 0, is, 0, Class41_Sub10.anInt3769);
		}
		return is;
	}

	public Class296_Sub51_Sub14() {
		super(1, false);
		aBoolean6421 = true;
	}

	static final boolean method3115(byte i, int i_6_, int i_7_, int i_8_, int i_9_) {
		if (!Class103.aBoolean1086 || !Class12.aBoolean133)
			return false;
		if (Class296_Sub39_Sub20.anInt6255 < 100)
			return false;
		if (!Class50.method614((byte) -110, i_9_, i_8_, i_7_))
			return false;
		if (i > -114)
			method3116(true, -96);
		int i_10_ = i_9_ << Class313.anInt2779;
		int i_11_ = i_8_ << Class313.anInt2779;
		if (Class236.method2132(i_11_, Js5TextureLoader.anInt3440, Class360_Sub2.aSArray5304[i_7_].method3355(i_8_, (byte) -107, i_9_), i_6_, i_10_, 0, Js5TextureLoader.anInt3440)) {
			Class264.anInt2473++;
			return true;
		}
		return false;
	}

	static final String method3116(boolean bool, int i) {
		Class296_Sub14 class296_sub14 = ((Class296_Sub14) Class296_Sub51_Sub11.aClass263_6399.get((long) i));
		if (bool != true)
			method3117(null, -119, false, -114, 83, -9, true);
		if (class296_sub14 != null) {
			Class296_Sub15_Sub4 class296_sub15_sub4 = class296_sub14.aClass219_Sub1_4667.method2050(4095);
			if (class296_sub15_sub4 != null) {
				double d = class296_sub14.aClass219_Sub1_4667.method2048(false);
				if (d >= (double) class296_sub15_sub4.method2545(true) && (double) class296_sub15_sub4.method2546(0) >= d)
					return class296_sub15_sub4.method2547((byte) -21);
			}
		}
		return null;
	}

	final void method3071(int i, Packet class296_sub17, int i_12_) {
		if (i >= -84)
			aBoolean6422 = false;
		int i_13_ = i_12_;
		while_118_ : do {
			do {
				if (i_13_ != 0) {
					if (i_13_ != 1) {
						if (i_13_ == 2)
							break;
						break while_118_;
					}
				} else {
					aBoolean6422 = class296_sub17.g1() == 1;
					return;
				}
				aBoolean6421 = class296_sub17.g1() == 1;
				return;
			} while (false);
			monochromatic = class296_sub17.g1() == 1;
			break;
		} while (false);
	}

	final int[][] get_colour_output(int i, int i_14_) {
		int[][] is = aClass86_5034.method823((byte) 77, i);
		if (aClass86_5034.aBoolean939) {
			int[][] is_15_ = this.method3075((byte) 121, 0, aBoolean6421 ? -i + Class67.anInt753 : i);
			int[] is_16_ = is_15_[0];
			int[] is_17_ = is_15_[1];
			int[] is_18_ = is_15_[2];
			int[] is_19_ = is[0];
			int[] is_20_ = is[1];
			int[] is_21_ = is[2];
			if (!aBoolean6422) {
				for (int i_22_ = 0; i_22_ < Class41_Sub10.anInt3769; i_22_++) {
					is_19_[i_22_] = is_16_[i_22_];
					is_20_[i_22_] = is_17_[i_22_];
					is_21_[i_22_] = is_18_[i_22_];
				}
			} else {
				for (int i_23_ = 0; i_23_ < Class41_Sub10.anInt3769; i_23_++) {
					is_19_[i_23_] = is_16_[Class41_Sub25.anInt3803 - i_23_];
					is_20_[i_23_] = is_17_[-i_23_ + Class41_Sub25.anInt3803];
					is_21_[i_23_] = is_18_[-i_23_ + Class41_Sub25.anInt3803];
				}
			}
		}
		if (i_14_ != 17621)
			return null;
		return is;
	}

	static final void method3117(Js5 class138, int i, boolean bool, int i_24_, int i_25_, int i_26_, boolean bool_27_) {
		Class249.aClass296_Sub45_Sub4_2357 = null;
		TextureOperation.aClass138_5040 = class138;
		Class296_Sub50.aBoolean5025 = bool_27_;
		Class296_Sub51_Sub7.anInt6371 = i_25_;
		Class296_Sub30.anInt4816 = i;
		if (bool)
			method3116(false, -29);
		Class368_Sub1.anInt5423 = i_26_;
		ReferenceWrapper.anInt6128 = 1;
		Class392.anInt3488 = i_24_;
	}

	static final Class296_Sub1 method3118(int i, int i_28_, byte i_29_, int i_30_) {
		Class296_Sub1 class296_sub1 = null;
		if (i_28_ == 0)
			class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 116, Class368_Sub15.aClass311_5517);
		if (i_28_ == 1)
			class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 106, Class239.aClass311_2253);
		class296_sub1.out.writeLEShort(Class41_Sub26.worldBaseY + i_30_);
		class296_sub1.out.writeByteC(!Class2.aClass166_66.method1637(82, 43) ? 0 : 1);
		class296_sub1.out.writeLEShortA(Class206.worldBaseX + i);
		Class205.foundY = i_30_;
		Class377.aBoolean3189 = false;
		if (i_29_ > -44)
			anInt6423 = 63;
		Class210_Sub1.foundX = i;
		EquipmentData.method3304((byte) 76);
		return class296_sub1;
	}

	static final void method3119(int i, int i_31_, int i_32_, int i_33_, int i_34_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_33_ << 32 | (long) i_32_, 18);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.anInt6145 = i_31_;
		class296_sub39_sub5.intParam = i_34_;
		if (i != 1392471456)
			method3114(true, null, 52, false, -8);
	}
}
