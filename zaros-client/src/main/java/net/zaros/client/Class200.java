package net.zaros.client;

/* Class200 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class200 {
	private int anInt2008;
	private Class388 aClass388_2009;
	static Class200 aClass200_2010 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2011 = new Class200(Class388.aClass388_3274);
	private Interface21 anInterface21_2012;
	static Class200 aClass200_2013 = new Class200(Class388.aClass388_3274);
	static Class200 aClass200_2014 = new Class200(Class388.aClass388_3274);
	static Class200 aClass200_2015 = new Class200(Class388.aClass388_3274);
	static Class200 aClass200_2016 = new Class200(Class388.aClass388_3274);
	static Class200 aClass200_2017 = new Class200(Class388.aClass388_3274);
	static Class200 aClass200_2018 = new Class200(Class388.aClass388_3274);
	static Class200 aClass200_2019 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2020 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2021 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2022 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2023 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2024 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2025 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2026 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2027 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2028 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2029 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2030 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2031 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2032 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2033 = new Class200(Class388.aClass388_3272);
	static Class200 aClass200_2034 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2035 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2036 = new Class200(Class388.aClass388_3271);
	static Class200 aClass200_2037 = new Class200(Class388.aClass388_3273);

	static final Class200[] method1948(byte i) {
		if (i >= -72)
			method1954(-36);
		return (new Class200[]{aClass200_2010, aClass200_2011, aClass200_2013, aClass200_2014, aClass200_2015, aClass200_2016, aClass200_2017, aClass200_2018, aClass200_2019, aClass200_2020, aClass200_2021, aClass200_2022, aClass200_2023, aClass200_2024, aClass200_2025, aClass200_2026, aClass200_2027, aClass200_2028, aClass200_2029, aClass200_2030, aClass200_2031, aClass200_2032, aClass200_2033, aClass200_2034, aClass200_2035, aClass200_2036, aClass200_2037});
	}

	final void method1949(byte i, int i_0_) {
		anInt2008 = i_0_;
		if (i <= 36)
			aClass200_2020 = null;
	}

	final int method1950(int i) {
		if (i != 0)
			aClass200_2026 = null;
		return anInt2008;
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	final void method1951(int i, Interface21 interface21) {
		if (interface21.method78(20598) != aClass388_2009)
			throw new IllegalArgumentException();
		if (i != 4977)
			aClass200_2017 = null;
		anInterface21_2012 = interface21;
	}

	static final void method1952(int i, int i_1_, int i_2_) {
		if (ReferenceWrapper.anInt6128 != 0) {
			if (i_1_ >= 0)
				Class41_Sub13.anIntArray3772[i_1_] = i_2_;
			else {
				for (int i_3_ = 0; i_3_ < 16; i_3_++)
					Class41_Sub13.anIntArray3772[i_3_] = i_2_;
			}
		}
		int i_4_ = 41 % ((2 - i) / 47);
		Class235.aClass296_Sub45_Sub4_2229.method3022(i_2_, 0, i_1_);
	}

	private Class200(Class388 class388) {
		aClass388_2009 = class388;
		anInt2008 = 1;
	}

	final Interface21 method1953(int i) {
		if (i != 0)
			method1953(-42);
		return anInterface21_2012;
	}

	public static void method1954(int i) {
		aClass200_2017 = null;
		aClass200_2024 = null;
		aClass200_2029 = null;
		aClass200_2026 = null;
		aClass200_2023 = null;
		aClass200_2021 = null;
		aClass200_2033 = null;
		aClass200_2019 = null;
		aClass200_2020 = null;
		aClass200_2015 = null;
		aClass200_2037 = null;
		aClass200_2016 = null;
		aClass200_2014 = null;
		aClass200_2018 = null;
		if (i != 15330)
			aClass200_2013 = null;
		aClass200_2034 = null;
		aClass200_2013 = null;
		aClass200_2010 = null;
		aClass200_2011 = null;
		aClass200_2028 = null;
		aClass200_2036 = null;
		aClass200_2022 = null;
		aClass200_2032 = null;
		aClass200_2025 = null;
		aClass200_2031 = null;
		aClass200_2035 = null;
		aClass200_2030 = null;
		aClass200_2027 = null;
	}
}
