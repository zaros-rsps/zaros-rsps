package net.zaros.client;

/* Class368_Sub20 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub20 extends Class368 {
	private int anInt5543;
	private int anInt5544;
	static IncomingPacket aClass231_5545 = new IncomingPacket(124, 7);
	private int[] anIntArray5546;
	static IncomingPacket aClass231_5547 = new IncomingPacket(125, 0);
	private int anInt5548;
	static IncomingPacket aClass231_5549 = new IncomingPacket(89, 8);
	static Class217 aClass217_5550;

	@Override
	final void method3807(byte i) {
		Mobile class338_sub3_sub1_sub3 = Class379.aClass302Array3624[anInt5544].method3262(0);
		int i_0_ = 1 % ((52 - i) / 52);
		if (anInt5543 != 0) {
			Class16_Sub2.method243(new int[]{anInt5548}, new int[1], new int[]{anInt5543}, class338_sub3_sub1_sub3, true);
		} else {
			Class160.method1619(0, 0, false, anIntArray5546, class338_sub3_sub1_sub3);
		}
	}

	static final void method3857(boolean bool, byte i) {
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 111, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5001);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 56, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 58, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5018);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 39, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5029);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 45, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 49, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub1_5008);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 72, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub29_5002);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 42, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub11_5023);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 44, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub9_4999);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 38, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub16_5019);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 71, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 47, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991);
		if (i >= 104) {
			Class343_Sub1.aClass296_Sub50_5282.method3060(0, 90, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub7_5004);
			Class343_Sub1.aClass296_Sub50_5282.method3060(0, 119, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013);
			Class343_Sub1.aClass296_Sub50_5282.method3060(0, 48, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5020);
			Class343_Sub1.aClass296_Sub50_5282.method3060(0, 99, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5015);
			Class343_Sub1.aClass296_Sub50_5282.method3060(0, 110, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub8_5027);
			Class343_Sub1.aClass296_Sub50_5282.method3060(0, 62, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987);
			Class343_Sub1.aClass296_Sub50_5282.method3060(0, 117, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub27_5010);
			Class343_Sub1.aClass296_Sub50_5282.method3060(0, 59, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011);
			Class296_Sub45_Sub2.method2980(true);
			Class343_Sub1.aClass296_Sub50_5282.method3060(2, 42, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub10_5009);
			Class343_Sub1.aClass296_Sub50_5282.method3060(1, 54, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub15_4990);
			Class368_Sub4.method3817(102);
			Class368_Sub22.method3866(11);
			Class380.aBoolean3210 = true;
		}
	}

	public static void method3858(byte i) {
		aClass231_5547 = null;
		aClass231_5549 = null;
		aClass231_5545 = null;
		if (i > 63) {
			aClass217_5550 = null;
		}
	}

	static final void method3859(boolean bool, byte i, int i_1_, int i_2_) {
		if (Class296_Sub51_Sub11.aClass263_6399.get(i_2_) == null) {
			if (AnimationsLoader.aBoolean2763) {
				Class296_Sub14 class296_sub14 = new Class296_Sub14(i_2_, new Class219_Sub1(4096, Class77.aClass138_879, i_2_), i_1_, bool);
				class296_sub14.aClass219_Sub1_4667.method2059(Class92.aStringArray993[Class394.langID], (byte) 76);
				Class296_Sub51_Sub11.aClass263_6399.put(i_2_, class296_sub14);
			} else {
				Class338_Sub3_Sub3_Sub2.method3564(-1, i_2_, bool);
			}
		}
		if (i > -59) {
			aClass231_5547 = null;
		}
	}

	static final void method3860(byte i, int i_3_, String string, String string_4_) {
		Class220.anInt2150 = i_3_;
		Class187.anInt1910 = 2;
		Class377.loginConnection = Class296_Sub45_Sub2.aClass204_6277;
		if (i >= -38) {
			quicksort(-92, 113, false, null, null);
		}
		Class41_Sub27.method508(false, string_4_, string, false, (byte) -109);
	}

	Class368_Sub20(Packet class296_sub17) {
		super(class296_sub17);
		anInt5544 = class296_sub17.g2();
		anIntArray5546 = new int[4];
		anInt5548 = class296_sub17.gSmart2or4s();
		Class291.method2407(anIntArray5546, 0, anIntArray5546.length, anInt5548);
		anInt5543 = class296_sub17.g4();
	}

	public static void quicksort(int i, int i_5_, boolean bool, String[] strings, int[] is) {
		if (i < i_5_) {
			int i_6_ = (i + i_5_) / 2;
			int i_7_ = i;
			String string = strings[i_6_];
			strings[i_6_] = strings[i_5_];
			strings[i_5_] = string;
			int i_8_ = is[i_6_];
			is[i_6_] = is[i_5_];
			is[i_5_] = i_8_;
			for (int i_9_ = i; i_9_ < i_5_; i_9_++) {
				if (string == null || strings[i_9_] != null && (i_9_ & 0x1) > strings[i_9_].compareTo(string)) {
					String string_10_ = strings[i_9_];
					strings[i_9_] = strings[i_7_];
					strings[i_7_] = string_10_;
					int i_11_ = is[i_9_];
					is[i_9_] = is[i_7_];
					is[i_7_++] = i_11_;
				}
			}
			strings[i_5_] = strings[i_7_];
			strings[i_7_] = string;
			is[i_5_] = is[i_7_];
			is[i_7_] = i_8_;
			quicksort(i, i_7_ - 1, bool, strings, is);
			quicksort(i_7_ + 1, i_5_, bool, strings, is);
		}
		if (bool) {
			method3858((byte) -125);
		}
	}

	static final void method3862(ObjectDefinition class70, int i, NPC class338_sub3_sub1_sub3_sub2, boolean bool, int i_12_, Player class338_sub3_sub1_sub3_sub1, int i_13_, int i_14_) {
		Class296_Sub36 class296_sub36 = new Class296_Sub36();
		class296_sub36.anInt4861 = i;
		class296_sub36.anInt4880 = i_13_ << 9;
		class296_sub36.anInt4879 = i_14_ << 9;
		if (class70 == null) {
			if (class338_sub3_sub1_sub3_sub2 == null) {
				if (class338_sub3_sub1_sub3_sub1 != null) {
					class296_sub36.aClass338_Sub3_Sub1_Sub3_Sub1_4864 = class338_sub3_sub1_sub3_sub1;
					class296_sub36.anInt4859 = class338_sub3_sub1_sub3_sub1.getSize() + i_13_ << 9;
					class296_sub36.anInt4869 = i_14_ + class338_sub3_sub1_sub3_sub1.getSize() << 9;
					class296_sub36.anInt4851 = Class41_Sub5.method410(class338_sub3_sub1_sub3_sub1, 11736);
					class296_sub36.anInt4863 = class338_sub3_sub1_sub3_sub1.anInt6881 << 9;
					class296_sub36.aBoolean4850 = class338_sub3_sub1_sub3_sub1.hasDisplayName;
					class296_sub36.anInt4872 = 256;
					class296_sub36.anInt4857 = 256;
					class296_sub36.anInt4856 = class338_sub3_sub1_sub3_sub1.anInt6887;
					class296_sub36.anInt4876 = 0;
					ha_Sub1_Sub1.aClass263_5823.put(class338_sub3_sub1_sub3_sub1.index, class296_sub36);
				}
			} else {
				class296_sub36.aClass338_Sub3_Sub1_Sub3_Sub2_4878 = class338_sub3_sub1_sub3_sub2;
				NPCDefinition class147 = class338_sub3_sub1_sub3_sub2.definition;
				if (class147.configData != null) {
					class296_sub36.aBoolean4874 = true;
					class147 = class147.getNPCDefinitionByConfigID(Class16_Sub3_Sub1.configsRegister);
				}
				if (class147 != null) {
					class296_sub36.anInt4859 = i_13_ + class147.size << 9;
					class296_sub36.anInt4869 = class147.size + i_14_ << 9;
					class296_sub36.anInt4851 = Class137.method1422(class338_sub3_sub1_sub3_sub2, (byte) -101);
					class296_sub36.anInt4857 = class147.anInt1502;
					class296_sub36.anInt4863 = class147.anInt1504 << 9;
					class296_sub36.aBoolean4850 = class147.aBoolean1481;
					class296_sub36.anInt4856 = class147.anInt1456;
					class296_sub36.anInt4872 = class147.anInt1463;
					class296_sub36.anInt4876 = class147.anInt1464 << 9;
				}
				Class296_Sub15_Sub4.aClass155_6031.addLast((byte) -35, class296_sub36);
			}
		} else {
			class296_sub36.aClass70_4877 = class70;
			int i_15_ = class70.sizeX;
			int i_16_ = class70.sizeY;
			if (i_12_ == 1 || i_12_ == 3) {
				i_15_ = class70.sizeY;
				i_16_ = class70.sizeX;
			}
			class296_sub36.anInt4858 = class70.anInt833;
			class296_sub36.anInt4851 = class70.anInt811;
			class296_sub36.anInt4859 = i_15_ + i_13_ << 9;
			class296_sub36.anInt4869 = i_14_ + i_16_ << 9;
			class296_sub36.aBoolean4850 = class70.aBoolean810;
			class296_sub36.anInt4857 = class70.anInt825;
			class296_sub36.anInt4856 = class70.anInt815;
			class296_sub36.anInt4876 = class70.anInt813 << 9;
			class296_sub36.anInt4863 = class70.anInt761 << 9;
			class296_sub36.anIntArray4853 = class70.anIntArray766;
			class296_sub36.aBoolean4855 = class70.aBoolean781;
			class296_sub36.anInt4854 = class70.anInt768;
			class296_sub36.anInt4872 = class70.anInt808;
			if (class70.transforms != null) {
				class296_sub36.aBoolean4874 = true;
				class296_sub36.method2763(0);
			}
			if (class296_sub36.anIntArray4853 != null) {
				class296_sub36.anInt4867 = (int) (Math.random() * (-class296_sub36.anInt4858 + class296_sub36.anInt4854)) + class296_sub36.anInt4858;
			}
			Class126.aClass155_1289.addLast((byte) -41, class296_sub36);
		}
		if (bool) {
			aClass231_5549 = null;
		}
	}
}
