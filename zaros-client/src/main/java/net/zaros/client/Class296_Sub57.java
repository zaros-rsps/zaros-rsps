package net.zaros.client;

/* Class296_Sub57 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub57 extends Node {
	int anInt5082;
	int anInt5083;

	static final aa_Sub1 method3219(int[] is, int i, int i_0_, int[] is_1_, ha_Sub3 var_ha_Sub3, int i_2_) {
		byte[] is_3_ = new byte[i_2_ * i];
		for (int i_4_ = 0; i_2_ > i_4_; i_4_++) {
			int i_5_ = is_1_[i_4_] + i_4_ * i;
			for (int i_6_ = 0; i_6_ < is[i_4_]; i_6_++)
				is_3_[i_5_++] = (byte) -1;
		}
		if (i_0_ > -100)
			return null;
		return new aa_Sub1(var_ha_Sub3, i, i_2_, is_3_);
	}

	Class296_Sub57(int i, int i_7_) {
		anInt5083 = i;
		anInt5082 = i_7_;
	}
}
