package net.zaros.client;
/* Class157 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;
import java.net.Socket;

abstract class Class157 {
	String aString1593;
	int anInt1594;
	static int anInt1595 = -1;
	static Class209 aClass209_1596 = new Class209(14);
	static GraphicsLoader graphicsLoader;
	static int anInt1598 = 0;

	abstract Socket method1586(int i) throws IOException;

	static final Class92 method1587(int i, ha var_ha, int i_0_) {
		if (i > -19)
			anInt1595 = -58;
		Class322 class322 = Class296_Sub51_Sub14.method3114(true, var_ha, 9057, true, i_0_);
		if (class322 == null)
			return null;
		return class322.aClass92_2828;
	}

	public static void method1588(boolean bool) {
		graphicsLoader = null;
		if (bool != true)
			method1589(29, 49, 100);
		aClass209_1596 = null;
	}

	public Class157() {
		/* empty */
	}

	static final boolean method1589(int i, int i_1_, int i_2_) {
		if (i_1_ != -12205)
			return true;
		return (i & 0x220) == 544 | (i & 0x18) != 0;
	}

	final Socket method1590(boolean bool) throws IOException {
		if (bool)
			return null;
		return new Socket(aString1593, anInt1594);
	}
}
