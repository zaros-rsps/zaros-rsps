package net.zaros.client;
/* Class120 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class TranslatableString {
	private String[] translations;
	public static TranslatableString aClass120_1197 = new TranslatableString("This is the developer console. To close, press the `, \u00b2 or \u00a7 keys on your keyboard.", "Das ist die Entwicklerkonsole. Zum Schlie\u00dfen, die Tasten `, \u00b2 or \u00a7 dr\u00fccken.", "Ceci est la console de d\u00e9veloppement. Pour la fermer, appuyez sur les touches `, \u00b2 ou \u00a7.", "Este \u00e9 o painel de controle do desenvolvedor. Para fechar, pressione `, \u00b2 ou \u00a7.");
	public static TranslatableString aClass120_1198 = new TranslatableString("There was an error executing the command.", "Es gab einen Fehler beim Ausf\u00fchren des Befehls.", "Une erreur s'est produite lors de l'ex\u00e9cution de la commande.", "Houve um erro quando o comando foi executado.");
	public static TranslatableString aClass120_1199 = new TranslatableString("Unknown developer command: ", "Unbekannter Befehl: ", "Commande inconnue : ", "Comando desconhecido: ");
	public static TranslatableString aClass120_1200 = new TranslatableString("Cancel", "Abbrechen", "Annuler", "Cancelar");
	public static TranslatableString aClass120_1201;
	public static TranslatableString aClass120_1202;
	public static TranslatableString aClass120_1203;
	public static TranslatableString take;
	public static TranslatableString drop;
	public static TranslatableString aClass120_1206;
	public static TranslatableString aClass120_1207;
	public static TranslatableString aClass120_1208;
	public static TranslatableString aClass120_1209;
	public static TranslatableString aClass120_1210;
	public static TranslatableString aClass120_1211;
	public static TranslatableString aClass120_1212;
	public static TranslatableString aClass120_1213;
	public static TranslatableString aClass120_1214;
	public static TranslatableString aClass120_1215;
	public static TranslatableString aClass120_1216;
	public static TranslatableString cantFind;
	public static TranslatableString aClass120_1218;
	public static TranslatableString aClass120_1219;
	public static TranslatableString aClass120_1220;
	public static TranslatableString aClass120_1221;
	public static TranslatableString aClass120_1222;
	public static TranslatableString aClass120_1223;
	public static TranslatableString aClass120_1224;
	public static TranslatableString aClass120_1225;
	public static TranslatableString aClass120_1226;
	public static TranslatableString aClass120_1227;
	public static TranslatableString aClass120_1228;
	public static TranslatableString aClass120_1229;
	public static TranslatableString aClass120_1230;
	public static TranslatableString aClass120_1231;
	public static TranslatableString aClass120_1232;
	public static TranslatableString aClass120_1233;
	public static TranslatableString aClass120_1234;
	public static TranslatableString aClass120_1235;
	public static TranslatableString aClass120_1236;
	public static TranslatableString aClass120_1237;
	public static TranslatableString aClass120_1238;
	public static TranslatableString aClass120_1239;
	public static TranslatableString aClass120_1240;
	public static TranslatableString aClass120_1241;
	public static TranslatableString aClass120_1242;
	public static TranslatableString aClass120_1243;
	public static TranslatableString aClass120_1244;
	public static TranslatableString aClass120_1245;
	public static TranslatableString aClass120_1246;
	public static TranslatableString aClass120_1247;
	public static TranslatableString aClass120_1248;
	public static TranslatableString aClass120_1249;
	public static TranslatableString aClass120_1250;
	public static TranslatableString aClass120_1251;
	public static TranslatableString aClass120_1252;
	public static TranslatableString aClass120_1253;
	public static TranslatableString aClass120_1254;
	public static TranslatableString aClass120_1255;
	public static TranslatableString aClass120_1256;
	public static TranslatableString aClass120_1257;
	public static TranslatableString aClass120_1258;
	public static TranslatableString aClass120_1259;
	public static Class209 aClass209_1260;
	public static boolean aBoolean1261;
	public static OutgoingPacket aClass311_1262;
	public static boolean aBoolean1263;
	public static int camPosY;

	public String getTranslation(int i_3_) {
		return translations[i_3_];
	}

	@Override
	public final String toString() {
		throw new IllegalStateException();
	}

	public static void method1030(byte i) {
		aClass120_1255 = null;
		aClass120_1209 = null;
		aClass120_1225 = null;
		aClass120_1197 = null;
		aClass120_1210 = null;
		aClass120_1230 = null;
		aClass120_1258 = null;
		aClass311_1262 = null;
		aClass120_1219 = null;
		aClass120_1256 = null;
		aClass120_1202 = null;
		aClass120_1212 = null;
		aClass120_1218 = null;
		aClass120_1211 = null;
		aClass120_1249 = null;
		aClass120_1200 = null;
		aClass120_1236 = null;
		aClass120_1203 = null;
		aClass120_1246 = null;
		aClass120_1201 = null;
		aClass120_1206 = null;
		aClass120_1224 = null;
		aClass120_1229 = null;
		aClass120_1208 = null;
		aClass120_1248 = null;
		aClass120_1257 = null;
		aClass120_1222 = null;
		aClass120_1207 = null;
		cantFind = null;
		aClass120_1215 = null;
		aClass120_1245 = null;
		aClass120_1243 = null;
		aClass120_1234 = null;
		aClass120_1238 = null;
		aClass120_1250 = null;
		aClass120_1199 = null;
		aClass120_1235 = null;
		aClass120_1223 = null;
		aClass120_1216 = null;
		aClass120_1240 = null;
		aClass120_1237 = null;
		aClass120_1228 = null;
		int i_4_ = -26 % ((i + 12) / 47);
		aClass120_1247 = null;
		aClass209_1260 = null;
		aClass120_1241 = null;
		aClass120_1233 = null;
		aClass120_1254 = null;
		aClass120_1221 = null;
		aClass120_1253 = null;
		aClass120_1227 = null;
		aClass120_1251 = null;
		aClass120_1252 = null;
		aClass120_1239 = null;
		aClass120_1259 = null;
		drop = null;
		aClass120_1214 = null;
		aClass120_1232 = null;
		aClass120_1231 = null;
		aClass120_1198 = null;
		take = null;
		aClass120_1244 = null;
		aClass120_1226 = null;
		aClass120_1213 = null;
		aClass120_1242 = null;
		aClass120_1220 = null;
	}

	public static final void method1031(int i, byte i_5_) {
		IOException_Sub1.method131((byte) -45);
		if (i_5_ < -1) {
			int i_6_ = ConfigurationsLoader.configsLoader.getDefinition(i).anInt677;
			if (i_6_ != 0) {
				int i_7_ = Class16_Sub3_Sub1.configsRegister.configurationsRegister[i];
				if (i_6_ == 6) {
					Class177.anInt1845 = i_7_;
				}
				if (i_6_ == 5) {
					Class296_Sub9_Sub1.anInt5982 = i_7_;
				}
			}
		}
	}

	private TranslatableString(String string, String string_8_, String string_9_, String string_10_) {
		translations = new String[] { string, string_8_, string_9_, string_10_ };
	}

	public static final int[][] method1032(float f, int i, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_, boolean bool, int i_16_) {
		int[][] is = new int[i_13_][i_16_];
		Class296_Sub51_Sub25 class296_sub51_sub25 = new Class296_Sub51_Sub25();
		class296_sub51_sub25.anInt6472 = i_11_;
		class296_sub51_sub25.anInt6467 = i_14_;
		class296_sub51_sub25.anInt6471 = (int) (f * 4096.0F);
		if (i != 4) {
			method1031(-49, (byte) -78);
		}
		class296_sub51_sub25.anInt6470 = i_12_;
		class296_sub51_sub25.aBoolean6469 = bool;
		class296_sub51_sub25.method3076((byte) 51);
		AdvancedMemoryCache.method998(i_16_, -1, i_13_);
		for (int i_17_ = 0; i_13_ > i_17_; i_17_++) {
			class296_sub51_sub25.method3147(i_17_, (byte) 80, is[i_17_]);
		}
		return is;
	}

	static {
		new TranslatableString("#Player", "#Spieler", "#Joueur", "#Jogador");
		aClass120_1201 = new TranslatableString("Members object", "Gegenstand f\u00fcr Mitglieder", "Objet d'abonn\u00e9s", "Objeto para membros");
		new TranslatableString("Login to a members' server to use this object.", "Du musst auf einer Mitglieder-Welt sein, um diesen Gegenstand zu benutzen.", "Connectez-vous \u00e0 un serveur d'abonn\u00e9s pour utiliser cet objet.", "Acesse um servidor para membros para usar este objeto.");
		new TranslatableString("Swap this note at any bank for the equivalent item.", "Dieses Zertifikat kann in einer Bank entsprechend eingetauscht werden.", "\u00c9changez ce re\u00e7u contre l'objet correspondant dans la banque de votre choix.", "V\u00e1 a qualquer banco para trocar esta nota pelo objeto equivalente.");
		aClass120_1202 = new TranslatableString("Discard", "Ablegen", "Jeter", "Descartar");
		aClass120_1203 = new TranslatableString("Discard", "Ablegen", "Jeter", "Descartar");
		take = new TranslatableString("Take", "Nehmen", "Prendre", "Pegar");
		drop = new TranslatableString("Drop", "Fallen lassen", "Poser", "Largar");
		new TranslatableString("Ok", "Okay", "OK", "Ok");
		new TranslatableString("Select", "Ausw\u00e4hlen", "S\u00e9lectionner", "Selecionar");
		aClass120_1206 = new TranslatableString("Continue", "Weiter", "Continuer", "Continuar");
		new TranslatableString("Invalid player name.", "Unzul\u00e4ssiger Charaktername!", "Nom de joueur incorrect.", "Nome de jogador inv\u00e1lido.");
		new TranslatableString("You can't report yourself!", "Du kannst dich nicht selbst melden!", "Vous ne pouvez pas vous signaler vous-m\u00eame !", "Voc\u00ea n\u00e3o pode denunciar a si pr\u00f3prio!");
		new TranslatableString("You already sent an abuse report under 60 secs ago! Do not abuse this system!", "Du hast bereits vor weniger als 60 Sekunden einen Regelversto\u00df gemeldet!", "Vous avez d\u00e9j\u00e0 signal\u00e9 un abus il y a moins d'une minute ! N'abusez pas du syst\u00e8me !", "Voc\u00ea j\u00e1 enviou uma den\u00fancia de abuso h\u00e1 menos de um minuto. N\u00e3o abuse deste sistema!");
		new TranslatableString(null, "Dieses System darf nicht missbraucht werden!", null, null);
		new TranslatableString("You cannot report that person for Staff Impersonation, they are Jagex Staff.", "Diese Person ist ein Jagex-Mitarbeiter!", "Cette personne est un membre du personnel de Jagex, vous ne pouvez pas la signaler pour abus d'identit\u00e9.", "Voc\u00ea n\u00e3o pode denunciar essa pessoa por tentar se passar por membro da equipe Jagex, pois ela faz parte da equipe.");
		new TranslatableString("You can spot a Jagex moderator by the gold crown next to their name.", "Jagex-Mitarbeiter haben eine goldene Krone neben ihrem Namen.", "Vous pouvez reconna\u00eetre les mod\u00e9rateurs Jagex \u00e0 la couronne dor\u00e9e en regard de leur nom.", "Os moderadores da Jagex s\u00e3o identificados por uma coroa dourada pr\u00f3xima ao \u007fnome.");
		new TranslatableString("You can report that person under a different rule.", "Diese Person kann bez\u00fcglich einer anderen Regel gemeldet werden.", "Vous pouvez signaler cette personne pour une autre infraction aux r\u00e8gles.", "Voc\u00ea n\u00e3o pode denunciar essa pessoa de acordo com uma regra diferente.");
		new TranslatableString("Thank-you, your abuse report has been received.", "Vielen Dank, deine Meldung ist bei uns eingegangen.", "Merci, nous avons bien re\u00e7u votre rapport d'abus.", "Obrigado. Sua den\u00fancia de abuso foi recebida.");
		new TranslatableString("Unable to send abuse report - system busy.", "Meldung konnte nicht gesendet werden - Systeme \u00fcberlastet", "Impossible de signaler un abus - Erreur syst\u00e8me", "Sistema ocupado. N\u00e3o foi poss\u00edvel enviar sua den\u00fancia de abuso.");
		new TranslatableString("Invalid name", "Unzul\u00e4ssiger Name!", "Nom incorrect", "Nome inv\u00e1lido");
		new TranslatableString("To use this item please login to a members' server.", "Du musst auf einer Mitglieder-Welt sein, um diesen Gegenstand zu benutzen.", "Veuillez vous connecter \u00e0 un serveur d'abonn\u00e9s pour utiliser cet objet.", "Acesse um servidor para membros para usar este objeto.");
		new TranslatableString("To interact with this please login to a members' server.", "Logg dich auf einer Mitglieder-Welt ein, um damit zu interagieren.", "Veuillez vous connecter \u00e0 un serveur d'abonn\u00e9s pour cette interaction.", "Para interagir, acesse um servidor para membros.");
		new TranslatableString("Nothing interesting happens.", "Nichts Interessantes passiert.", "Il ne se passe rien d'int\u00e9ressant.", "Nada de interessante acontece.");
		new TranslatableString("You can't reach that.", "Da kommst du nicht hin.", "Vous ne pouvez pas l'atteindre.", "Voc\u00ea n\u00e3o consegue alcan\u00e7ar isso.");
		new TranslatableString("Invalid teleport!", "Unzul\u00e4ssiger Teleport!", "T\u00e9l\u00e9portation non valide !", "Teleporte inv\u00e1lido!");
		new TranslatableString("To go here you must login to a members' server.", "Du musst auf einer Mitglieder-Welt sein, um dort hinzukommen.", "Vous devez vous connecter \u00e0 un serveur d'abonn\u00e9s pour aller \u00e0 cet endroit.", "Para entrar aqui, acesse um servidor para membros.");
		new TranslatableString("Unable to add friend - system busy.", "Der Freund konnte nicht hinzugef\u00fcgt werden, das System ist derzeit ausgelastet.", "Impossible d'ajouter un ami - syst\u00e8me occup\u00e9.", "N\u00e3o foi poss\u00edvel adicionar o amigo. O sistema est\u00e1 ocupado.");
		new TranslatableString("Unable to add friend - unknown player.", "Spieler konnte nicht hinzugef\u00fcgt werden - Spieler unbekannt.", "Impossible d'ajouter l'ami - joueur inconnu.", "N\u00e3o foi poss\u00edvel adicionar um amigo - jogador desconhecido.");
		new TranslatableString("Unable to add name - system busy.", "Der Name konnte nicht hinzugef\u00fcgt werden, das System ist derzeit ausgelastet.", "Impossible d'ajouter un nom - syst\u00e8me occup\u00e9.", "N\u00e3o foi poss\u00edvel adicionar o nome. O sistema est\u00e1 ocupado.");
		new TranslatableString("Unable to add name - unknown player.", "Name konnte nicht hinzugef\u00fcgt werden - Spieler unbekannt.", "Impossible d'ajouter le nom - joueur inconnu.", "N\u00e3o foi poss\u00edvel adicionar um nome - jogador desconhecido.");
		aClass120_1207 = new TranslatableString("Your friends list is full (200 names maximum)", "Deine Freunde-Liste ist voll, du hast das Maximum von 200 erreicht.", "Votre liste d'amis est pleine (200 noms maximum).", "Sua lista de amigos est\u00e1 cheia. O limite \u00e9 200.");
		new TranslatableString("Unable to delete friend - system busy.", "Der Freund konnte nicht entfernt werden, das System ist derzeit ausgelastet.", "Impossible de supprimer un ami - syst\u00e8me occup\u00e9.", "N\u00e3o foi poss\u00edvel excluir o amigo. O sistema est\u00e1 ocupado.");
		new TranslatableString("Unable to delete name - system busy.", "Name konnte nicht gel\u00f6scht werden - Systemfehler.", "Impossible d'effacer le nom - syst\u00e8me occup\u00e9.", "N\u00e3o foi poss\u00edvel deletar o nome - sistema ocupado.");
		new TranslatableString("Unable to send message - system busy.", "Deine Nachricht konnte nicht verschickt werden, das System ist derzeit ausgelastet.", "Impossible d'envoyer un message - syst\u00e8me occup\u00e9.", "N\u00e3o foi poss\u00edvel enviar a mensagem. O sistema est\u00e1 ocupado.");
		new TranslatableString("Unable to send message - player unavailable.", "Deine Nachricht konnte nicht verschickt werden,", "Impossible d'envoyer un message - joueur indisponible.", "N\u00e3o foi poss\u00edvel enviar a mensagem. O jogador n\u00e3o est\u00e1 dispon\u00edvel.");
		new TranslatableString(null, "der Spieler ist momentan nicht verf\u00fcgbar.", null, null);
		new TranslatableString("Unable to send message - player not on your friends list.", "Nachricht kann nicht geschickt werden,", "Impossible d'envoyer un message - joueur non inclus dans votre liste d'amis.", "N\u00e3o foi poss\u00edvel enviar a mensagem. O jogador n\u00e3o est\u00e1 na sua lista de amigos.");
		new TranslatableString(null, "Spieler nicht auf deiner Freunde-Liste.", null, null);
		new TranslatableString("You appear to be telling someone your password - please don't!", "Willst du jemandem dein Passwort verraten? Das darfst du nicht! Falls das", "Il semble que vous r\u00e9v\u00e9liez votre mot de passe \u00e0 quelqu'un - ne faites jamais \u00e7a !", "Parece que voc\u00ea est\u00e1 revelando sua senha a algu\u00e9m. N\u00e3o fa\u00e7a isso!");
		new TranslatableString("If you are not, please change your password to something more obscure!", "nicht der Fall ist, \u00e4ndere dein Passwort zu einem ungew\u00f6hnlicheren Begriff!", "Si ce n'est pas le cas, remplacez votre mot de passe par une formule moins \u00e9vidente !", "Caso n\u00e3o esteja, altere sua senha para algo mais obscuro!");
		new TranslatableString("Unable to send message - set your display name first by logging into the game.", "Nachricht konnte nicht gesendet werden.  Bitte richte erst deinen Charakternamen ein, ", "Impossible d'envoyer le message - enregistrez un nom de personnage en vous connectant au jeu.", "N\u00e3o \u00e9 poss\u00edvel enviar a mensagem. Defina um nome de personagem antes, fazendo login no jogo.");
		new TranslatableString(null, "indem du dich ins Spiel einloggst.", null, null);
		new TranslatableString("For that rule you can only report players who have spoken or traded recently.", "Mit dieser Option k\u00f6nnen nur Spieler gemeldet werden,", "Cette r\u00e8gle n'est invocable que pour les discussions ou \u00e9changes r\u00e9cents.", "Para essa regra, voc\u00ea s\u00f3 pode denunciar jogadores com quem tenha falado ou negociado recentemente.");
		new TranslatableString(null, "die k\u00fcrzlich gesprochen oder gehandelt haben.", null, null);
		new TranslatableString("That player is offline, or has privacy mode enabled.", "Dieser Spieler ist offline oder hat den Privatsph\u00e4ren-Modus aktiviert.", "Ce joueur est d\u00e9connect\u00e9 ou en mode priv\u00e9.", "O jogador est\u00e1 offline ou est\u00e1 com o modo de privacidade ativado.");
		new TranslatableString("You cannot send a quick chat message to a player on this world at this time.", "Einem Spieler auf dieser Welt k\u00f6nnen derzeit keine Direktchat-Nachrichten", "Impossible d'envoyer un message rapide \u00e0 un joueur de ce serveur \u00e0 l'heure actuelle.", "Voc\u00ea n\u00e3o pode enviar uma mensagem de papo r\u00e1pido para um jogador neste mundo neste momento.");
		new TranslatableString(null, "geschickt werden.", null, null);
		new TranslatableString("This player is on a quick chat world and cannot receive your message.", "Der Spieler kann auf einer Direktchat-Welt keine Nachrichten empfangen.", "Ce joueur est sur un serveur \u00e0 messagerie rapide et ne peut pas recevoir votre message.", "Este jogador n\u00e3o pode receber sua mensagem porque est\u00e1 em um mundo de papo r\u00e1pido.");
		new TranslatableString("Chat disabled", "Deaktiviert", "Messagerie d\u00e9sactiv\u00e9e", "Bate-papo desativado");
		new TranslatableString("friends_chat", "friends_chat", "friends_chat", "friends_chat");
		new TranslatableString("You are not currently in a friends chat channel.", "Du befindest dich derzeit nicht in einem Freundes-Chatraum.", "Vous ne faites pas partie d'un canal de discussion.", "No momento, voc\u00ea n\u00e3o est\u00e1 no bate-papo entre amigos.");
		new TranslatableString("You are not allowed to talk in this friends chat channel.", "Du darfst in diesem Freundes-Chatraum nicht reden.", "Vous n'\u00eates pas autoris\u00e9 \u00e0 parler dans ce canal de discussion.", "Voc\u00ea pode falar neste bate-papo entre amigos.");
		new TranslatableString("Error sending message to friends chat - please try again later!", "Fehler beim Versenden der Nachricht - bitte versuch es sp\u00e4ter erneut.", "Erreur lors de l'envoi de ce message \u2013 veuillez r\u00e9essayer ult\u00e9rieurement !", "Erro ao enviar mensagem para bate-papo entre amigos - favor tentar novamente mais tarde!");
		new TranslatableString("Please wait until you are logged out of your previous channel.", "Bitte warte, bis du den vorherigen Chatraum verlassen hast.", "Veuillez attendre d'\u00eatre d\u00e9connect\u00e9(e) de votre canal pr\u00e9c\u00e9dent.", "Aguarde at\u00e9 se desconectar do canal anterior.");
		new TranslatableString("You are not currently in a channel.", "Du befindest dich derzeit nicht in einem Chatraum.", "Vous n'\u00eates dans aucun canal \u00e0 l'heure actuelle.", "No momento voc\u00ea n\u00e3o est\u00e1 em um canal.");
		new TranslatableString("Attempting to join channel...", "Chatraum wird betreten...", "Tentative de connexion au canal...", "Tentando acessar canal...");
		new TranslatableString("Sending request to leave channel...", "Chatraum wird verlassen...", "Envoi de la demande de sortie du canal...", "Enviando solicita\u00e7\u00e3o para deixar o canal...");
		new TranslatableString("Already attempting to join a channel - please wait...", "Du versuchst bereits, einem Chatraum beizutreten - bitte warte.", "Tentative de connexion au canal d\u00e9j\u00e0 en cours - veuillez patienter...", "J\u00e1 h\u00e1 uma tentativa de entrar em um canal. Aguarde...");
		new TranslatableString("Leave request already in progress - please wait...", "Du versuchst bereits, einen Chatraum zu verlassen - bitte warte.", "Demande de sortie d\u00e9j\u00e0 effectu\u00e9e - veuillez patienter...", "Solicita\u00e7\u00e3o de sa\u00edda j\u00e1 em andamento. Aguarde...");
		new TranslatableString("Invalid channel name entered!", "Ung\u00fcltiger Chatraum-Name angegeben.", "Nom de canal incorrect !", "Nome de canal inv\u00e1lido!");
		new TranslatableString("Unable to join friends chat at this time - please try again later!", "Freundes-Chatraum kann nicht betreten werden - bitte versuch es sp\u00e4ter erneut.", "Vous ne pouvez pas rejoindre ce canal de discussion pour le moment - veuillez r\u00e9essayer ult\u00e9rieurement !", "N\u00e3o foi poss\u00edvel participar do bate-papo entre amigos - favor tentar novamente mais tarde!");
		new TranslatableString("Now talking in friends chat channel ", "Freundes-Chatraum: ", "Vous participez actuellement au canal de discussion : ", "Falando no momento no bate-papo entre amigos: ");
		new TranslatableString("Now talking in friends chat channel of player: ", "Freundes-Chat dieses Spielers beigetreten: ", "Vous participez actuellement au canal de discussion du joueur : ", "Falando no momento no bate-papo entre amigos do jogador: ");
		new TranslatableString("To talk, start each line of chat with the / symbol.", "Leite eine Zeile mit / ein, um hier zu chatten.", "Pour parler, ins\u00e9rez le symbole / au d\u00e9but de chaque ligne.", "Para falar, comece cada linha de conversa com o s\u00edmbolo /.");
		new TranslatableString("Error joining friends chat channel - please try again later!", "Fehler beim Betreten des Freundes-Chatraums - bitte versuch es sp\u00e4ter erneut.", "Erreur lors de la connexion au canal de discussion - veuillez r\u00e9essayer ult\u00e9rieurement !", "Erro ao participar do bate-papo entre-amigos - favor tentar novamente mais tarde!");
		new TranslatableString("You are temporarily blocked from joining channels - please try again later!", "Du darfst derzeit keine Chatr\u00e4ume betreten - bitte versuch es sp\u00e4ter.", "Vous \u00eates temporairement exclu des canaux - veuillez r\u00e9essayer ult\u00e9rieurement.", "Voc\u00ea est\u00e1 temporariamente impedido de entrar em canais. Tente de novo depois!");
		new TranslatableString("The channel you tried to join does not exist.", "Der von dir gew\u00fcnschte Chatraum existiert nicht.", "Le canal que vous essayez de rejoindre n'existe pas.", "O canal que voc\u00ea tentou acessar n\u00e3o existe.");
		new TranslatableString("The channel you tried to join is currently full.", "Der von dir gew\u00fcnschte Chatraum ist derzeit \u00fcberf\u00fcllt.", "Le canal que vous essayez de rejoindre est plein.", "O canal que voc\u00ea tentou acessar est\u00e1 cheio no momento.");
		new TranslatableString("You do not have a high enough rank to join this friends chat channel.", "Dein Rang reicht nicht aus, um diesen Freundes-Chatraum zu betreten.", "Votre rang n'est pas assez \u00e9lev\u00e9 pour rejoindre ce canal de discussion.", "Voc\u00ea n\u00e3o tem uma classifica\u00e7\u00e3o alta o suficiente para participar deste bate-papo entre amigos.");
		new TranslatableString("You are temporarily banned from this friends chat channel.", "Du wurdest tempor\u00e4r von diesem Freundes-Chatraum gesperrt.", "Vous avez \u00e9t\u00e9 exclu temporairement de ce canal de discussion.", "Voc\u00ea foi temporariamente banido deste bate-papo entre amigos.");
		new TranslatableString("You are not allowed to join this user's friends chat channel.", "Du darfst den Freundes-Chatraum dieses Benutzers nicht betreten.", "Vous n'\u00eates pas autoris\u00e9 \u00e0 rejoindre le canal de discussion de cet utilisateur.", "Voc\u00ea n\u00e3o pode participar do bate-papo entre amigos deste usu\u00e1rio.");
		new TranslatableString(" joined the channel.", " hat den Chatraum betreten.", " a rejoint le canal.", " entrou no canal.");
		new TranslatableString(" left the channel.", " hat den Chatraum verlassen.", " a quitt\u00e9 le canal.", " deixou o canal.");
		new TranslatableString(" was kicked from the channel.", " wurde aus dem Chatraum rausgeworfen.", " a \u00e9t\u00e9 expuls\u00e9 du canal.", " foi expulso do canal.");
		new TranslatableString("You have been kicked from the channel.", "Du wurdest aus dem Chatraum rausgeworfen.", "Vous avez \u00e9t\u00e9 expuls\u00e9 du canal.", "Voc\u00ea foi expulso do canal.");
		new TranslatableString("You have been removed from this channel.", "Du wurdest aus dem Chatraum entfernt.", "Vous avez \u00e9t\u00e9 supprim\u00e9 de ce canal.", "Voc\u00ea foi retirado desse canal.");
		new TranslatableString("You have left the channel.", "Du hast den Chatraum verlassen.", "Vous avez quitt\u00e9 le canal.", "Voc\u00ea saiu do canal.");
		new TranslatableString("Your friends chat channel has now been enabled!", "Dein Freundes-Chat ist jetzt eingeschaltet.", "Votre canal de discussion est maintenant activ\u00e9 !", "O seu bate-papo entre amigos foi ativado!");
		new TranslatableString("Join your channel by clicking 'Join Chat' and typing: ", "Klick auf 'Betreten' und gib ein: ", "Pour rejoindre votre canal, cliquez sur \u00ab Participer \u00bb et entrez : ", "Para entrar no seu canal, clique em \"Acessar bate-papo\" e digite: ");
		new TranslatableString("Your friends chat channel has now been disabled!", "Dein Freundes-Chat ist jetzt ausgeschaltet.", "Votre canal de discussion est maintenant d\u00e9sactiv\u00e9 !", "O seu bate-papo entre amigos foi desativado!");
		new TranslatableString("You do not have permission to kick users in this channel.", "Du darfst keine Benutzer aus diesem Chatraum rauswerfen.", "Vous n'\u00eates pas autoris\u00e9 \u00e0 expulser des utilisateurs de ce canal.", "Voc\u00ea n\u00e3o tem permiss\u00e3o para expulsar usu\u00e1rios neste canal.");
		new TranslatableString("You do not have permission to kick this user.", "Du darfst diesen Benutzer nicht rauswerfen.", "Vous n'\u00eates pas autoris\u00e9 \u00e0 expulser cet utilisateur.", "Voc\u00ea n\u00e3o tem permiss\u00e3o para expulsar este usu\u00e1rio.");
		new TranslatableString("That user is not in this channel.", "Dieser Benutzer befindet sich nicht in diesem Chatraum.", "Cet utilisateur n'est pas dans ce canal.", "Esse usu\u00e1rio n\u00e3o est\u00e1 no canal.");
		new TranslatableString("Your request to kick/ban this user was successful.", "Der Rauswurf/die Sperrung war erfolgreich.", "Votre demande d'exclusion de ce joueur a \u00e9t\u00e9 accept\u00e9e.", "Seu pedido para expulsar/suspender este jogador foi bem sucedido.");
		new TranslatableString("Your request to refresh this user's temporary ban was successful.", "Die Verl\u00e4ngerung der tempor\u00e4ren Sperrung dieses Spielers war erfolgreich.", "Le renouvellement d'exclusion temporaire de ce joueur a \u00e9t\u00e9 accept\u00e9.", "Seu pedido para reiniciar a suspens\u00e3o tempor\u00e1ria deste jogador foi bem sucedido.");
		new TranslatableString("You have been temporarily muted due to breaking a rule.", "Aufgrund eines Regelversto\u00dfes wurdest du vor\u00fcbergehend stumm geschaltet.", "La messagerie instantan\u00e9e a \u00e9t\u00e9 temporairement bloqu\u00e9e suite \u00e0 une infraction.", "Voc\u00ea foi temporariamente vetado por ter violado uma regra.");
		new TranslatableString("This mute will remain for a further ", "Diese Stummschaltung gilt f\u00fcr weitere ", "Votre acc\u00e8s restera bloqu\u00e9 encore ", "Este veto permanecer\u00e1 por mais ");
		new TranslatableString(" days.", " Tage.", " jours.", " dias.");
		new TranslatableString("You will be un-muted within 24 hours.", "Du wirst innerhalb der n\u00e4chsten 24 Stunden wieder sprechen k\u00f6nnen.", "Vous aurez \u00e0 nouveau acc\u00e8s \u00e0 la messagerie instantan\u00e9e dans 24 heures.", "O veto ser\u00e1 retirado dentro de 24 horas.");
		new TranslatableString("To prevent further mutes please read the rules.", "Um eine erneute Stummschaltung zu verhindern, lies bitte die Regeln.", "Pour \u00e9viter un nouveau blocage, lisez le r\u00e8glement.", "Para evitar outros vetos, leia as regras.");
		new TranslatableString("You have been permanently muted due to breaking a rule.", "Du wurdest permanent stumm geschaltet, da du gegen eine Regel versto\u00dfen hast.", "L'acc\u00e8s \u00e0 la messagerie instantan\u00e9e vous a d\u00e9finitivement \u00e9t\u00e9 retir\u00e9 suite \u00e0 une infraction.", "Voc\u00ea foi permanentemente vetado por ter violado uma regra.");
		aClass120_1208 = new TranslatableString("Loading - please wait.", "Ladevorgang - bitte warte.", "Chargement en cours. Veuillez patienter.", "Carregando. Aguarde.");
		aClass120_1209 = new TranslatableString("Profiling...", "Profiling...", "Profilage...", "Profiling...");
		aClass120_1210 = new TranslatableString("Connection lost.", "Verbindung abgebrochen.", "Connexion perdue.", "Conex\u00e3o perdida.");
		aClass120_1211 = new TranslatableString("Please wait - attempting to reestablish.", "Bitte warte - es wird versucht, die Verbindung wiederherzustellen.", "Veuillez patienter - tentative de r\u00e9tablissement.", "Tentando reestabelecer conex\u00e3o. Aguarde.");
		aClass120_1212 = new TranslatableString("Checking for updates - ", "Suche nach Updates - ", "V\u00e9rification des mises \u00e0 jour - ", "Verificando atualiza\u00e7\u00f5es - ");
		aClass120_1213 = new TranslatableString("Fetching Updates - ", "Lade Update - ", "Chargement des MAJ - ", "Carregando atualiza\u00e7\u00f5es - ");
		new TranslatableString("Loading config - ", "Lade Konfiguration - ", "Chargement des fichiers config - ", "Carregando config - ");
		new TranslatableString("Loaded config", "Konfig geladen.", "Fichiers config charg\u00e9s", "Config carregada");
		new TranslatableString("Loading sprites - ", "Lade Sprites - ", "Chargement des sprites - ", "Carregando sprites - ");
		new TranslatableString("Loaded sprites", "Sprites geladen.", "Sprites charg\u00e9s", "Sprites carregados");
		new TranslatableString("Loading wordpack - ", "Lade Wordpack - ", "Chargement du module texte - ", "Carregando pacote de palavras - ");
		new TranslatableString("Loaded wordpack", "Wordpack geladen.", "Module texte charg\u00e9", "Pacote de palavras carregado");
		new TranslatableString("Loading interfaces - ", "Lade Benutzeroberfl\u00e4che - ", "Chargement des interfaces - ", "Carregando interfaces - ");
		new TranslatableString("Loaded interfaces", "Benutzeroberfl\u00e4che geladen.", "Interfaces charg\u00e9es", "Interfaces carregadas");
		new TranslatableString("Loading interface scripts - ", "Lade Interface-Skripte - ", "Chargement des interfaces - ", "Carregando interfaces - ");
		new TranslatableString("Loaded interface scripts", "Interface-Skripte geladen", "Interfaces charg\u00e9es", "Interfaces carregadas");
		new TranslatableString("Loading additional fonts - ", "Lade Zusatzschriftarten - ", "Chargement de polices secondaires - ", "Carregando fontes principais - ");
		new TranslatableString("Loaded additional fonts", "Zusatzschriftarten geladen", "Polices secondaires charg\u00e9es", "Fontes principais carregadas");
		new TranslatableString("Loading world map - ", "Lade Weltkarte - ", "Chargement de la mappemonde - ", "Carregando mapa-m\u00fandi - ");
		new TranslatableString("Loaded world map", "Weltkarte geladen", "Mappemonde charg\u00e9e", "Mapa-m\u00fandi carregado");
		new TranslatableString("Loading world list data", "Lade Liste der Welten", "Chargement de la liste des serveurs", "Carregando dados da lista de mundos");
		new TranslatableString("Loaded world list data", "Liste der Welten geladen", "Liste des serveurs charg\u00e9e", "Dados da lista de mundos carregados");
		new TranslatableString("Loaded client variable data", "Client-Variablen geladen", "Variables du client charg\u00e9es", "As vari\u00e1veis do sistema foram carregadas");
		aClass120_1214 = new TranslatableString("Loading...", "Lade...", "Chargement en cours...", "Carregando...");
		new TranslatableString("Please close the interface you have open before using 'Report Abuse'.", "Bitte schlie\u00df die momentan ge\u00f6ffnete Benutzeroberfl\u00e4che,", "Fermez l'interface que vous avez ouverte avant d'utiliser le bouton \u00ab Signaler un abus \u00bb.", "Feche a interface aberta antes de usar o recurso \"Denunciar abuso\".");
		new TranslatableString(null, "bevor du die Option 'Regelversto\u00df melden' benutzt.", null, null);
		new TranslatableString("System update in: ", "System-Update in: ", "Mise \u00e0 jour syst\u00e8me dans : ", "Atualiza\u00e7\u00e3o do sistema em: ");
		aClass120_1215 = new TranslatableString(" has logged in.", " loggt sich ein.", " s'est connect\u00e9.", " entrou no jogo.");
		aClass120_1216 = new TranslatableString(" has logged out.", " loggt sich aus.", " s'est d\u00e9connect\u00e9.", " saiu do jogo.");
		cantFind = new TranslatableString("Unable to find ", "Spieler kann nicht gefunden werden: ", "Impossible de trouver ", "N\u00e3o \u00e9 poss\u00edvel encontrar ");
		new TranslatableString("Use", "Benutzen", "Utiliser", "Usar");
		aClass120_1218 = new TranslatableString("Examine", "Untersuchen", "Examiner", "Examinar");
		aClass120_1219 = new TranslatableString("Attack", "Angreifen", "Attaquer", "Atacar");
		aClass120_1220 = new TranslatableString("Choose Option", "W\u00e4hl eine Option", "Choisir une option", "Selecionar op\u00e7\u00e3o");
		aClass120_1221 = new TranslatableString(" more options", " weitere Optionen", " autres options", " mais op\u00e7\u00f5es");
		aClass120_1222 = new TranslatableString("Walk here", "Hierhin gehen", "Atteindre", "Caminhar para c\u00e1");
		aClass120_1223 = new TranslatableString("Face here", "Hierhin drehen", "Regarder dans cette direction", "Virar para c\u00e1");
		aClass120_1224 = new TranslatableString("level: ", "Stufe: ", "niveau ", "n\u00edvel: ");
		aClass120_1225 = new TranslatableString("skill: ", "Fertigkeit: ", "comp\u00e9tence ", "habilidade: ");
		aClass120_1226 = new TranslatableString("rating: ", "Kampfstufe: ", "classement ", "qualifica\u00e7\u00e3o: ");
		aClass120_1227 = new TranslatableString("Please wait...", "Bitte warte...", "Veuillez attendre", "Aguarde...");
		new TranslatableString("Close", "Bitte schlie\u00df die momentan ge\u00f6ffnete Benutzeroberfl\u00e4che,", "Fermez l'interface que vous avez ouverte avant d'utiliser le bouton \u00ab Signaler un abus \u00bb.", "Feche a interface aberta antes de usar o recurso \"Denunciar abuso\".");
		aClass120_1228 = new TranslatableString(" ", ": ", " ", " ");
		aClass120_1229 = new TranslatableString("M", "M", "M", "M");
		aClass120_1230 = new TranslatableString("M", "M", "M", "M");
		aClass120_1231 = new TranslatableString("K", "T", "K", "K");
		aClass120_1232 = new TranslatableString("K", "T", "K", "K");
		new TranslatableString("From", "Von:", "De", "De");
		aClass120_1233 = new TranslatableString("Self", "Mich", "Moi", "Eu");
		aClass120_1234 = new TranslatableString(" is already on your friends list.", " steht bereits auf deiner Freunde-Liste!", " est d\u00e9j\u00e0 dans votre liste d'amis.", " j\u00e1 est\u00e1 na sua lista de amigos.");
		aClass120_1235 = new TranslatableString("Your ignore list is full. Max of 100 users.", "Deine Ignorieren-Liste ist voll, du kannst nur 100 Spieler darauf eintragen.", "Votre liste noire est pleine (100 noms maximum).", "Sua lista de ignorados est\u00e1 cheia. O limite \u00e9 100 usu\u00e1rios.");
		aClass120_1236 = new TranslatableString(" is already on your ignore list.", " steht bereits auf deiner Ignorieren-Liste!", " est d\u00e9j\u00e0 dans votre liste noire.", " j\u00e1 est\u00e1 na sua lista de ignorados.");
		aClass120_1237 = new TranslatableString("You can't add yourself to your own friends list.", "Du kannst dich nicht auf deine eigene Freunde-Liste setzen!", "Vous ne pouvez pas ajouter votre nom \u00e0 votre liste d'amis.", "Voc\u00ea n\u00e3o pode adicionar a si pr\u00f3prio \u00e0 sua lista de amigos.");
		aClass120_1238 = new TranslatableString("You can't add yourself to your own ignore list.", "Du kannst dich nicht auf deine eigene Ignorieren-Liste setzen!", "Vous ne pouvez pas ajouter votre nom \u00e0 votre liste noire.", "Voc\u00ea n\u00e3o pode adicionar a si pr\u00f3prio \u00e0 sua lista de ignorados.");
		new TranslatableString("Changes will take effect on your friends chat in the next 60 seconds.", "Die \u00c4nderungen am Freundes-Chat werden innerhalb von 60 Sekunden \u00fcbernommen.", "Les modifications seront apport\u00e9s \u00e0 votre canal de discussion dans les 60 prochaines secondes.", "Mundan\u00e7as v\u00e3o ocorrer em seu bate-papo entre amigos nos pr\u00f3ximos 60 segundos.");
		aClass120_1239 = new TranslatableString("Please remove ", "Bitte entferne ", "Veuillez commencer par supprimer ", "Remova ");
		aClass120_1240 = new TranslatableString(" from your ignore list first.", " zuerst von deiner Ignorieren-Liste!", " de votre liste noire.", " da sua lista de ignorados primeiro.");
		aClass120_1241 = new TranslatableString("Please remove ", "Bitte entferne ", "Veuillez commencer par supprimer ", "Remova ");
		aClass120_1242 = new TranslatableString(" from your friends list first.", " zuerst von deiner Freunde-Liste!", " de votre liste d'amis.", " da sua lista de amigos primeiro.");
		aClass120_1243 = new TranslatableString("yellow:", "gelb:", "jaune:", "amarelo:");
		aClass120_1244 = new TranslatableString("red:", "rot:", "rouge:", "vermelho:");
		aClass120_1245 = new TranslatableString("green:", "gr\u00fcn:", "vert:", "verde:");
		aClass120_1246 = new TranslatableString("cyan:", "blaugr\u00fcn:", "cyan:", "cyan:");
		aClass120_1247 = new TranslatableString("purple:", "lila:", "violet:", "roxo:");
		aClass120_1248 = new TranslatableString("white:", "weiss:", "blanc:", "branco:");
		aClass120_1249 = new TranslatableString("flash1:", "blinken1:", "clignotant1:", "flash1:");
		aClass120_1250 = new TranslatableString("flash2:", "blinken2:", "clignotant2:", "flash2:");
		aClass120_1251 = new TranslatableString("flash3:", "blinken3:", "clignotant3:", "brilho3:");
		aClass120_1252 = new TranslatableString("glow1:", "leuchten1:", "brillant1:", "brilho1:");
		aClass120_1253 = new TranslatableString("glow2:", "leuchten2:", "brillant2:", "brilho2:");
		aClass120_1254 = new TranslatableString("glow3:", "leuchten3:", "brillant3:", "brilho3:");
		aClass120_1255 = new TranslatableString("wave:", "welle:", "ondulation:", "onda:");
		aClass120_1256 = new TranslatableString("wave2:", "welle2:", "ondulation2:", "onda2:");
		aClass120_1257 = new TranslatableString("shake:", "sch\u00fctteln:", "tremblement:", "tremor:");
		aClass120_1258 = new TranslatableString("scroll:", "scrollen:", "d\u00e9roulement:", "rolagem:");
		aClass120_1259 = new TranslatableString("slide:", "gleiten:", "glissement:", "deslizamento:");
		new TranslatableString("Friend", "Freund", "Ami", "Amigo");
		aClass209_1260 = new Class209(10);
		aBoolean1261 = false;
		aClass311_1262 = new OutgoingPacket(52, 4);
		aBoolean1263 = false;
	}
}
