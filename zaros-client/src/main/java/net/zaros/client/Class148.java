package net.zaros.client;

/* Class148 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class148 {
	private static int anInt1512;
	private static int anInt1513;
	private static int anInt1514;
	private static int anInt1515;
	private static int anInt1516;
	private static int[] anIntArray1517;
	private static int anInt1518;
	private static int anInt1519;

	private static final void method1505() {
		anInt1514 = 0;
	}

	private static final boolean method1506(int i) {
		int i_0_ = anInt1516;
		int i_1_ = anInt1512;
		int i_2_ = anInt1519;
		int i_3_;
		for (/**/; i_1_ >= i_0_; i_1_ = i_3_) {
			anInt1519 = ++i_2_;
			if (i_2_ >= i)
				return false;
			i_3_ = anInt1515;
			for (/**/; i_0_ < anInt1514; i_0_ += 4) {
				int i_4_ = anIntArray1517[i_0_ + 1];
				if (i_2_ < i_4_)
					break;
				int i_5_ = anIntArray1517[i_0_];
				int i_6_ = anIntArray1517[i_0_ + 2];
				int i_7_ = anIntArray1517[i_0_ + 3];
				int i_8_ = (i_6_ - i_5_ << 16) / (i_7_ - i_4_);
				int i_9_ = (i_5_ << 16) + 32768;
				anIntArray1517[i_0_] = i_9_;
				anIntArray1517[i_0_ + 2] = i_8_;
			}
			for (int i_10_ = i_3_; i_10_ < i_0_; i_10_ += 4) {
				int i_11_ = anIntArray1517[i_10_ + 3];
				if (i_2_ >= i_11_) {
					anIntArray1517[i_10_] = anIntArray1517[i_3_];
					anIntArray1517[i_10_ + 1] = anIntArray1517[i_3_ + 1];
					anIntArray1517[i_10_ + 2] = anIntArray1517[i_3_ + 2];
					anIntArray1517[i_10_ + 3] = anIntArray1517[i_3_ + 3];
					i_3_ += 4;
				}
			}
			if (i_3_ == anInt1514) {
				anInt1514 = 0;
				return false;
			}
			method1508(i_3_, i_0_);
			anInt1515 = i_3_;
			anInt1516 = i_0_;
		}
		anInt1513 = anIntArray1517[i_1_] >> 16;
		anInt1518 = anIntArray1517[i_1_ + 4] >> 16;
		anIntArray1517[i_1_] += anIntArray1517[i_1_ + 2];
		anIntArray1517[i_1_ + 4] += anIntArray1517[i_1_ + 6];
		i_1_ += 8;
		anInt1512 = i_1_;
		return true;
	}

	private static final void method1507(int i, int i_12_) {
		if (i_12_ > i + 4) {
			int i_13_ = i;
			int i_14_ = anIntArray1517[i_13_];
			int i_15_ = anIntArray1517[i_13_ + 1];
			int i_16_ = anIntArray1517[i_13_ + 2];
			int i_17_ = anIntArray1517[i_13_ + 3];
			for (int i_18_ = i + 4; i_18_ < i_12_; i_18_ += 4) {
				int i_19_ = anIntArray1517[i_18_ + 1];
				if (i_19_ < i_15_) {
					anIntArray1517[i_13_] = anIntArray1517[i_18_];
					anIntArray1517[i_13_ + 1] = i_19_;
					anIntArray1517[i_13_ + 2] = anIntArray1517[i_18_ + 2];
					anIntArray1517[i_13_ + 3] = anIntArray1517[i_18_ + 3];
					i_13_ += 4;
					anIntArray1517[i_18_] = anIntArray1517[i_13_];
					anIntArray1517[i_18_ + 1] = anIntArray1517[i_13_ + 1];
					anIntArray1517[i_18_ + 2] = anIntArray1517[i_13_ + 2];
					anIntArray1517[i_18_ + 3] = anIntArray1517[i_13_ + 3];
				}
			}
			anIntArray1517[i_13_] = i_14_;
			anIntArray1517[i_13_ + 1] = i_15_;
			anIntArray1517[i_13_ + 2] = i_16_;
			anIntArray1517[i_13_ + 3] = i_17_;
			method1507(i, i_13_);
			method1507(i_13_ + 4, i_12_);
		}
	}

	private static final void method1508(int i, int i_20_) {
		for (/**/; i_20_ >= i + 8; i_20_ -= 4) {
			boolean bool = true;
			for (int i_21_ = i + 4; i_21_ < i_20_; i_21_ += 4) {
				int i_22_ = anIntArray1517[i_21_ - 4];
				int i_23_ = anIntArray1517[i_21_];
				if (i_22_ > i_23_) {
					bool = false;
					anIntArray1517[i_21_ - 4] = i_23_;
					anIntArray1517[i_21_] = i_22_;
					i_22_ = anIntArray1517[i_21_ - 2];
					anIntArray1517[i_21_ - 2] = anIntArray1517[i_21_ + 2];
					anIntArray1517[i_21_ + 2] = i_22_;
					i_22_ = anIntArray1517[i_21_ - 1];
					anIntArray1517[i_21_ - 1] = anIntArray1517[i_21_ + 3];
					anIntArray1517[i_21_ + 3] = i_22_;
				}
			}
			if (bool)
				break;
		}
	}

	private static final void method1509(ha var_ha, int[] is, int i, int i_24_, int i_25_, int[] is_26_, int[] is_27_) {
		int[] is_28_ = new int[4];
		var_ha.K(is_28_);
		if (is_26_ != null && is_28_[3] - is_28_[1] != is_26_.length)
			throw new IllegalStateException();
		method1505();
		method1513(is, i, i_24_);
		method1511(is_28_[1]);
		while (method1506(is_28_[3])) {
			int i_29_ = anInt1513;
			int i_30_ = anInt1518;
			int i_31_ = anInt1519;
			if (is_26_ != null) {
				int i_32_ = i_31_ - is_28_[1];
				if (i_29_ < is_26_[i_32_] + is_28_[0])
					i_29_ = is_26_[i_32_] + is_28_[0];
				if (i_30_ > is_26_[i_32_] + is_27_[i_32_] + is_28_[0])
					i_30_ = is_26_[i_32_] + is_27_[i_32_] + is_28_[0];
				if (i_30_ - i_29_ <= 0)
					continue;
			}
			var_ha.U(i_29_, i_31_, i_30_ - i_29_, i_25_, 1);
		}
	}

	static final void method1510(ha var_ha, int[] is, int i) {
		method1509(var_ha, is, 0, is.length, i, null, null);
	}

	private static final void method1511(int i) {
		if (anInt1514 < 0) {
			anInt1515 = anInt1516 = anInt1512 = 0;
			anInt1519 = 2147483646;
		} else {
			method1507(0, anInt1514);
			int i_33_ = anIntArray1517[1];
			if (i_33_ < i)
				i_33_ = i;
			int i_34_ = 0;
			int i_35_;
			for (i_35_ = 0; i_35_ < anInt1514; i_35_ += 4) {
				int i_36_ = anIntArray1517[i_35_ + 1];
				if (i_33_ < i_36_)
					break;
				int i_37_ = anIntArray1517[i_35_];
				int i_38_ = anIntArray1517[i_35_ + 2];
				int i_39_ = anIntArray1517[i_35_ + 3];
				int i_40_ = (i_38_ - i_37_ << 16) / (i_39_ - i_36_);
				int i_41_ = (i_37_ << 16) + 32768;
				anIntArray1517[i_35_] = i_41_ + (i_33_ - i_36_) * i_40_;
				anIntArray1517[i_35_ + 2] = i_40_;
			}
			anInt1515 = i_34_;
			anInt1516 = i_35_;
			anInt1512 = i_35_;
			anInt1519 = i_33_ - 1;
		}
	}

	public static void method1512() {
		anIntArray1517 = null;
	}

	private static final void method1513(int[] is, int i, int i_42_) {
		int i_43_ = anInt1514 + (i_42_ << 1);
		if (anIntArray1517 == null || anIntArray1517.length < i_43_) {
			int[] is_44_ = new int[i_43_];
			for (int i_45_ = 0; i_45_ < anInt1514; i_45_++)
				is_44_[i_45_] = anIntArray1517[i_45_];
			anIntArray1517 = is_44_;
		}
		i_42_ += i;
		int i_46_ = i_42_ - 2;
		for (int i_47_ = i; i_47_ < i_42_; i_47_ += 2) {
			int i_48_ = is[i_46_ + 1];
			int i_49_ = is[i_47_ + 1];
			if (i_48_ < i_49_) {
				anIntArray1517[anInt1514++] = is[i_46_];
				anIntArray1517[anInt1514++] = i_48_;
				anIntArray1517[anInt1514++] = is[i_47_];
				anIntArray1517[anInt1514++] = i_49_;
			} else if (i_49_ < i_48_) {
				anIntArray1517[anInt1514++] = is[i_47_];
				anIntArray1517[anInt1514++] = i_49_;
				anIntArray1517[anInt1514++] = is[i_46_];
				anIntArray1517[anInt1514++] = i_48_;
			}
			i_46_ = i_47_;
		}
	}

	static final void method1514(ha var_ha, int[] is, int i, int[] is_50_, int[] is_51_) {
		method1509(var_ha, is, 0, is.length, i, is_50_, is_51_);
	}
}
