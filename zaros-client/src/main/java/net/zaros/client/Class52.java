package net.zaros.client;

/* Class52 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class52 {
	static int anInt634;
	static boolean[][] aBooleanArrayArray635 = {{true, true, true, true, true, true, true, true, true, true, true, true, true}, {true, true, true, false, false, false, true, true, false, false, false, false, true}, {true, false, false, false, false, true, true, true, false, false, false, false, false}, {false, false, true, true, true, true, false, false, false, false, false, false, false}, {true, true, true, true, true, true, false, false, false, false, false, false, false}, {true, true, true, false, false, true, true, true, false, false, false, false, false}, {true, true, false, false, false, true, true, true, false, false, false, false, true}, {true, true, false, false, false, false, false, true, false, false, false, false, false}, {false, true, true, true, true, true, true, true, false, false, false, false, false}, {true, false, false, false, true, true, true, true, true, true, false, false, false}, {true, true, true, true, true, false, false, false, true, true, false, false, false}, {true, true, true, false, false, false, false, false, false, false, true, true, false}, new boolean[13], {true, true, true, true, true, true, true, true, true, true, true, true, true}, new boolean[13]};
	static s[] aSArray636;
	static float aFloat637;
	private Js5 aClass138_638;
	private AdvancedMemoryCache aClass113_639 = new AdvancedMemoryCache(64);
	private Js5 aClass138_640;
	static OutgoingPacket aClass311_641 = new OutgoingPacket(87, 0);
	static int anInt642 = -1;

	final Class296_Sub39_Sub12 method639(int i, byte i_0_) {
		Class296_Sub39_Sub12 class296_sub39_sub12 = (Class296_Sub39_Sub12) aClass113_639.get((long) i);
		if (class296_sub39_sub12 != null)
			return class296_sub39_sub12;
		byte[] is;
		if (i < 32768)
			is = aClass138_640.getFile(0, i);
		else
			is = aClass138_638.getFile(0, i & 0x7fff);
		class296_sub39_sub12 = new Class296_Sub39_Sub12();
		if (i_0_ > -59)
			return null;
		if (is != null)
			class296_sub39_sub12.method2845(1, new Packet(is));
		if (i >= 32768)
			class296_sub39_sub12.method2843(-108);
		aClass113_639.put(class296_sub39_sub12, (long) i);
		return class296_sub39_sub12;
	}

	public static void method640(int i) {
		aSArray636 = null;
		aBooleanArrayArray635 = null;
		aClass311_641 = null;
		if (i < 89)
			anInt642 = -38;
	}

	Class52(int i, Js5 class138, Js5 class138_1_) {
		aClass138_640 = class138;
		aClass138_638 = class138_1_;
		if (aClass138_640 != null)
			aClass138_640.getLastFileId(0);
		if (aClass138_638 != null)
			aClass138_638.getLastFileId(0);
	}
}
