package net.zaros.client;

/* Class283 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class283 {
	private long[] aLongArray2608;
	private long[] aLongArray2609 = new long[8];
	private long[] aLongArray2610;
	private int anInt2611;
	private byte[] aByteArray2612 = new byte[32];
	private long[] aLongArray2613;
	private int anInt2614;
	private long[] aLongArray2616;
	private byte[] aByteArray2617;

	private final void method2356(boolean bool) {
		int i = 0;
		int i_0_ = 0;
		while (i < 8) {
			aLongArray2609[i] = (Class235.method2129(Class41.method379((long) aByteArray2617[i_0_ + 7], 255L), (Class235.method2129(Class41.method379(255L << 8, (long) aByteArray2617[i_0_ + 6] << 8), (Class235.method2129((Class235.method2129((Class235.method2129((Class235.method2129((Class235.method2129((long) aByteArray2617[i_0_] << 56, Class41.method379((long) (aByteArray2617[i_0_ + 1]), 255L) << 48)), Class41.method379((long) (aByteArray2617[i_0_ + 2]) << 40, 255L << 40))), Class41.method379((long) aByteArray2617[i_0_ + 3], 255L) << 32)), (Class41.method379(255L, (long) aByteArray2617[i_0_ + 4]) << 24))), (Class41.method379(255L, (long) aByteArray2617[i_0_ + 5]) << 16)))))));
			i++;
			i_0_ += 8;
		}
		if (bool)
			method2360((byte) 116, -112L, null);
		for (int i_1_ = 0; i_1_ < 8; i_1_++)
			aLongArray2610[i_1_] = Class235.method2129(aLongArray2609[i_1_], (aLongArray2608[i_1_] = aLongArray2616[i_1_]));
		for (int i_2_ = 1; i_2_ <= 10; i_2_++) {
			for (int i_3_ = 0; i_3_ < 8; i_3_++) {
				aLongArray2613[i_3_] = 0L;
				int i_4_ = 0;
				int i_5_ = 56;
				for (/**/; i_4_ < 8; i_4_++) {
					aLongArray2613[i_3_] = (Class235.method2129(aLongArray2613[i_3_], (Class296_Sub51_Sub27.aLongArrayArray6485[i_4_][((255 & ((int) ((aLongArray2608[(-i_4_ + i_3_ & 7)]) >>> i_5_))))])));
					i_5_ -= 8;
				}
			}
			for (int i_6_ = 0; i_6_ < 8; i_6_++)
				aLongArray2608[i_6_] = aLongArray2613[i_6_];
			aLongArray2608[0] = Class235.method2129(aLongArray2608[0], (Class296_Sub51_Sub27.aLongArray6484[i_2_]));
			for (int i_7_ = 0; i_7_ < 8; i_7_++) {
				aLongArray2613[i_7_] = aLongArray2608[i_7_];
				int i_8_ = 0;
				int i_9_ = 56;
				while (i_8_ < 8) {
					aLongArray2613[i_7_] = (Class235.method2129(aLongArray2613[i_7_], (Class296_Sub51_Sub27.aLongArrayArray6485[i_8_][((((int) ((aLongArray2610[(7 & i_7_ - i_8_)]) >>> i_9_)) & 255))])));
					i_8_++;
					i_9_ -= 8;
				}
			}
			for (int i_10_ = 0; i_10_ < 8; i_10_++)
				aLongArray2610[i_10_] = aLongArray2613[i_10_];
		}
		for (int i_11_ = 0; i_11_ < 8; i_11_++)
			aLongArray2616[i_11_] = Class235.method2129(aLongArray2616[i_11_], Class235.method2129((aLongArray2609[i_11_]), (aLongArray2610[i_11_])));
	}

	final void method2357(int i, byte[] is, int i_12_) {
		aByteArray2617[anInt2614] = (byte) Class48.bitOR(aByteArray2617[anInt2614], 128 >>> (anInt2611 & 7));
		anInt2614++;
		if (anInt2614 > 32) {
			while (anInt2614 < 64)
				aByteArray2617[anInt2614++] = (byte) 0;
			method2356(false);
			anInt2614 = 0;
		}
		if (i == 18993) {
			while (anInt2614 < 32)
				aByteArray2617[anInt2614++] = (byte) 0;
			ArrayTools.removeElement(aByteArray2612, 0, aByteArray2617, 32, 32);
			method2356(false);
			int i_13_ = 0;
			int i_14_ = i_12_;
			while (i_13_ < 8) {
				long l = aLongArray2616[i_13_];
				is[i_14_] = (byte) (int) (l >>> 56);
				is[i_14_ + 1] = (byte) (int) (l >>> 48);
				is[i_14_ + 2] = (byte) (int) (l >>> 40);
				is[i_14_ + 3] = (byte) (int) (l >>> 32);
				is[i_14_ + 4] = (byte) (int) (l >>> 24);
				is[i_14_ + 5] = (byte) (int) (l >>> 16);
				is[i_14_ + 6] = (byte) (int) (l >>> 8);
				is[i_14_ + 7] = (byte) (int) l;
				i_13_++;
				i_14_ += 8;
			}
		}
	}

	static final void method2358(int i, int i_15_, int i_16_, int i_17_, int i_18_) {
		float f = (float) Class106.anInt1114 / (float) Class106.anInt1116;
		int i_19_ = i;
		int i_20_ = i_17_;
		if (!(f < 1.0F))
			i_19_ = (int) ((float) i_17_ / f);
		else
			i_20_ = (int) (f * (float) i);
		i_15_ -= (i_17_ - i_20_) / 2;
		i_18_ -= (-i_19_ + i) / 2;
		BITConfigDefinition.anInt2604 = -1;
		Class296_Sub51_Sub5.anInt6365 = -1;
		Class69.anInt3688 = i_18_ * Class106.anInt1116 / i_19_;
		Class219_Sub1.anInt4569 = Class106.anInt1114 - Class106.anInt1114 * i_15_ / i_20_;
		if (i_16_ != 0)
			method2358(7, -112, -41, -13, -63);
		Class296_Sub51_Sub25.method3146((byte) -10);
	}

	final void method2359(boolean bool) {
		for (int i = 0; i < 32; i++)
			aByteArray2612[i] = (byte) 0;
		anInt2611 = anInt2614 = 0;
		aByteArray2617[0] = (byte) 0;
		if (!bool) {
			for (int i = 0; i < 8; i++)
				aLongArray2616[i] = 0L;
		}
	}

	final void method2360(byte i, long l, byte[] is) {
		int i_21_ = 0;
		int i_22_ = -((int) l & 0x7) + 8 & 0x7;
		int i_23_ = anInt2611 & 0x7;
		long l_24_ = l;
		int i_25_ = -70 / ((i + 62) / 53);
		int i_26_ = 31;
		int i_27_ = 0;
		for (/**/; i_26_ >= 0; i_26_--) {
			i_27_ += ((int) l_24_ & 0xff) + (aByteArray2612[i_26_] & 0xff);
			aByteArray2612[i_26_] = (byte) i_27_;
			l_24_ >>>= 8;
			i_27_ >>>= 8;
		}
		while (l > 8L) {
			int i_28_ = (is[i_21_] << i_22_ & 0xff | (is[i_21_ + 1] & 0xff) >>> -i_22_ + 8);
			if (i_28_ < 0 || i_28_ >= 256)
				throw new RuntimeException("LOGIC ERROR");
			aByteArray2617[anInt2614] = (byte) Class48.bitOR(aByteArray2617[anInt2614], i_28_ >>> i_23_);
			anInt2614++;
			anInt2611 += 8 - i_23_;
			if (anInt2611 == 512) {
				method2356(false);
				anInt2611 = anInt2614 = 0;
			}
			aByteArray2617[anInt2614] = (byte) (255 & i_28_ << -i_23_ + 8);
			i_21_++;
			l -= 8L;
			anInt2611 += i_23_;
		}
		int i_29_;
		if (l > 0L) {
			i_29_ = is[i_21_] << i_22_ & 0xff;
			aByteArray2617[anInt2614] = (byte) Class48.bitOR(aByteArray2617[anInt2614], i_29_ >>> i_23_);
		} else
			i_29_ = 0;
		if (l + (long) i_23_ < 8L)
			anInt2611 += l;
		else {
			l -= (long) (-i_23_ + 8);
			anInt2611 += 8 - i_23_;
			anInt2614++;
			if (anInt2611 == 512) {
				method2356(false);
				anInt2611 = anInt2614 = 0;
			}
			aByteArray2617[anInt2614] = (byte) (i_29_ << -i_23_ + 8 & 255);
			anInt2611 += (int) l;
		}
	}

	public Class283() {
		anInt2611 = 0;
		anInt2614 = 0;
		aLongArray2616 = new long[8];
		aLongArray2613 = new long[8];
		aByteArray2617 = new byte[64];
		aLongArray2608 = new long[8];
		aLongArray2610 = new long[8];
	}
}
