package net.zaros.client;

/* Class288 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class288 {
	private int anInt2646;
	static int anInt2647;
	private int anInt2648;
	static IncomingPacket aClass231_2649;
	static float aFloat2650;
	private int[][] anIntArrayArray2651;
	static int anInt2652 = 100;
	static Class161 aClass161_2653;
	static Class186 aClass186_2654;

	final int method2391(int i, int i_0_) {
		if (i_0_ != -2)
			method2394(null, (byte) -25);
		if (anIntArrayArray2651 != null)
			i = (int) ((long) anInt2646 * (long) i / (long) anInt2648);
		return i;
	}

	final int method2392(int i, int i_1_) {
		if (i != 21284)
			method2391(75, 118);
		if (anIntArrayArray2651 != null)
			i_1_ = 6 + (int) ((long) anInt2646 * (long) i_1_ / (long) anInt2648);
		return i_1_;
	}

	final short[] method2393(byte i, short[] is) {
		if (i < 47)
			method2397((byte) -116, null);
		if (anIntArrayArray2651 != null) {
			int i_2_ = ((int) ((long) anInt2646 * (long) is.length / (long) anInt2648) + 14);
			int[] is_3_ = new int[i_2_];
			int i_4_ = 0;
			int i_5_ = 0;
			for (int i_6_ = 0; is.length > i_6_; i_6_++) {
				int i_7_ = is[i_6_];
				int[] is_8_ = anIntArrayArray2651[i_5_];
				for (int i_9_ = 0; i_9_ < 14; i_9_++)
					is_3_[i_4_ + i_9_] += i_7_ * is_8_[i_9_] >> 2;
				i_5_ += anInt2646;
				int i_10_ = i_5_ / anInt2648;
				i_4_ += i_10_;
				i_5_ -= i_10_ * anInt2648;
			}
			is = new short[i_2_];
			for (int i_11_ = 0; i_11_ < i_2_; i_11_++) {
				int i_12_ = is_3_[i_11_] + 8192 >> 14;
				if (i_12_ < -32768)
					is[i_11_] = (short) -32768;
				else if (i_12_ <= 32767)
					is[i_11_] = (short) i_12_;
				else
					is[i_11_] = (short) 32767;
			}
		}
		return is;
	}

	final byte[] method2394(byte[] is, byte i) {
		if (anIntArrayArray2651 != null) {
			int i_13_ = ((int) ((long) anInt2646 * (long) is.length / (long) anInt2648) + 14);
			int[] is_14_ = new int[i_13_];
			int i_15_ = 0;
			int i_16_ = 0;
			for (int i_17_ = 0; i_17_ < is.length; i_17_++) {
				int i_18_ = is[i_17_];
				int[] is_19_ = anIntArrayArray2651[i_16_];
				for (int i_20_ = 0; i_20_ < 14; i_20_++)
					is_14_[i_20_ + i_15_] += i_18_ * is_19_[i_20_];
				i_16_ += anInt2646;
				int i_21_ = i_16_ / anInt2648;
				i_16_ -= anInt2648 * i_21_;
				i_15_ += i_21_;
			}
			is = new byte[i_13_];
			for (int i_22_ = 0; i_13_ > i_22_; i_22_++) {
				int i_23_ = is_14_[i_22_] + 32768 >> 16;
				if (i_23_ < -128)
					is[i_22_] = (byte) -128;
				else if (i_23_ > 127)
					is[i_22_] = (byte) 127;
				else
					is[i_22_] = (byte) i_23_;
			}
		}
		if (i < 90)
			method2393((byte) -22, null);
		return is;
	}

	public static void method2395(byte i) {
		aClass186_2654 = null;
		aClass161_2653 = null;
		aClass231_2649 = null;
		if (i != -124)
			aClass231_2649 = null;
	}

	static final void method2396(int i, int i_24_) {
		if (i_24_ <= 97)
			method2396(-5, 21);
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i, 17);
		class296_sub39_sub5.insertIntoQueue();
	}

	static final void method2397(byte i, byte[] is) {
		if (i <= 64)
			aClass161_2653 = null;
		Packet class296_sub17 = new Packet(is);
		for (;;) {
			int i_25_ = class296_sub17.g1();
			if (i_25_ == 0)
				break;
			if (i_25_ != 1) {
				if (i_25_ != 4) {
					if (i_25_ == 5) {
						int i_26_ = class296_sub17.g1();
						ByteStream.anIntArray6043 = new int[i_26_];
						for (int i_27_ = 0; i_26_ > i_27_; i_27_++) {
							ByteStream.anIntArray6043[i_27_] = class296_sub17.g2();
							if (ByteStream.anIntArray6043[i_27_] == 65535)
								ByteStream.anIntArray6043[i_27_] = -1;
						}
					}
				} else {
					int i_28_ = class296_sub17.g1();
					ObjectDefinition.anIntArray771 = new int[i_28_];
					for (int i_29_ = 0; i_29_ < i_28_; i_29_++) {
						ObjectDefinition.anIntArray771[i_29_] = class296_sub17.g2();
						if (ObjectDefinition.anIntArray771[i_29_] == 65535)
							ObjectDefinition.anIntArray771[i_29_] = -1;
					}
				}
			} else {
				int[] is_30_ = Class353.anIntArray3048 = new int[6];
				is_30_[0] = class296_sub17.g2();
				is_30_[1] = class296_sub17.g2();
				is_30_[2] = class296_sub17.g2();
				is_30_[3] = class296_sub17.g2();
				is_30_[4] = class296_sub17.g2();
				is_30_[5] = class296_sub17.g2();
			}
		}
	}

	Class288(int i, int i_31_) {
		if (i != i_31_) {
			int i_32_ = Class122_Sub3.method1049(i, false, i_31_);
			i /= i_32_;
			i_31_ /= i_32_;
			anIntArrayArray2651 = new int[i][14];
			anInt2648 = i;
			anInt2646 = i_31_;
			for (int i_33_ = 0; i_33_ < i; i_33_++) {
				int[] is = anIntArrayArray2651[i_33_];
				double d = (double) i_33_ / (double) i + 6.0;
				int i_34_ = (int) Math.floor(d - 7.0 + 1.0);
				if (i_34_ < 0)
					i_34_ = 0;
				int i_35_ = (int) Math.ceil(d + 7.0);
				if (i_35_ > 14)
					i_35_ = 14;
				double d_36_ = (double) i_31_ / (double) i;
				for (/**/; i_34_ < i_35_; i_34_++) {
					double d_37_ = ((double) i_34_ - d) * 3.141592653589793;
					double d_38_ = d_36_;
					if (d_37_ < -1.0E-4 || d_37_ > 1.0E-4)
						d_38_ *= Math.sin(d_37_) / d_37_;
					d_38_ *= (Math.cos(((double) i_34_ - d) * 0.2243994752564138) * 0.46) + 0.54;
					is[i_34_] = (int) Math.floor(d_38_ * 65536.0 + 0.5);
				}
			}
		}
	}

	static {
		aClass231_2649 = new IncomingPacket(5, -2);
		aClass161_2653 = new Class161(6, 0, 4, 2);
	}
}
