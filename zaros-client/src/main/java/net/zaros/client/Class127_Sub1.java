package net.zaros.client;

/* Class127_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class127_Sub1 extends Class127 {
	static SubInPacket aClass260_4282 = new SubInPacket(7, 4);
	Class211[] aClass211Array4283;
	static NodeDeque aClass155_4284 = new NodeDeque();
	static int[] anIntArray4285;
	static int anInt4287 = 0;

	public static void method1358(byte i) {
		aClass260_4282 = null;
		anIntArray4285 = null;
		if (i <= 32)
			method1359(30);
		aClass155_4284 = null;
	}

	static final void method1359(int i) {
		for (int i_0_ = i; i_0_ < Applet_Sub1.anInt7; i_0_++) {
			int i_1_ = (Class296_Sub28.method2683((byte) 89, Class404.anInt3385 + i_0_, Applet_Sub1.anInt7) * Class317.anInt3705);
			for (int i_2_ = 0; i_2_ < Class317.anInt3705; i_2_++) {
				int i_3_ = (Class296_Sub28.method2683((byte) 109, i_2_ + Class292.anInt2665, Class317.anInt3705) + i_1_);
				if (BillboardRaw.anIntArray1080[i_3_] == Class41_Sub20.anInt3795)
					IOException_Sub1.anInterface13Array38[i_3_].method52(0, 0, Class290.anInt2656, Class395.anInt3317, i_2_ * Class290.anInt2656, Class395.anInt3317 * i_0_, true, true);
			}
		}
	}

	static final void method1360(int i) {
		StaticMethods.method2475((byte) 12);
		Class368.aClass241_3131 = null;
		s_Sub3.aClass262_5164 = null;
		ClipData.aClass190ArrayArray184 = null;
		Class368_Sub7.aClass262_5461 = null;
		Class105_Sub2.aClass241_5692 = null;
		Class296_Sub51_Sub23.aClass241_6458 = null;
		if (i != 25390)
			PlayerUpdate.invisiblePlayersCount = 103;
		Class368_Sub16.aHa5527 = null;
	}

	Class127_Sub1(Class211[] class211s) {
		aClass211Array4283 = class211s;
	}

	static {
	}
}
