package net.zaros.client;
/* Class188 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Date;

final class Class188 {
	static int[] anIntArray1924;
	r aR1925;
	Model aClass178_1926;
	static int anInt1927;

	static final void method1889(long l, int i) {
		Class296_Sub38.aCalendar4889.setTime(new Date(l));
		if (i != 26998)
			method1890((byte) 79);
	}

	public static void method1890(byte i) {
		if (i < -72)
			anIntArray1924 = null;
	}

	public Class188() {
		/* empty */
	}

	static final void method1891(int i) {
		FileOnDisk class58 = null;
		try {
			class58 = Class398.method4113(i ^ 0xeda, "2");
			Packet class296_sub17 = new Packet(ObjectDefinition.anInt809 * 6 + 3);
			class296_sub17.p1(1);
			class296_sub17.p2(ObjectDefinition.anInt809);
			for (int i_0_ = 0; Class269.globalIntVars.length > i_0_; i_0_++) {
				if (Class14.aBooleanArray153[i_0_]) {
					class296_sub17.p2(i_0_);
					class296_sub17.p4(Class269.globalIntVars[i_0_]);
				}
			}
			class58.write(class296_sub17.pos, class296_sub17.data, 0, true);
		} catch (Exception exception) {
			/* empty */
		}
		if (i == 3746) {
			try {
				if (class58 != null)
					class58.close(0);
			} catch (Exception exception) {
				/* empty */
			}
			Class262.aLong2453 = Class72.method771(-119);
			Class32_Sub1.aBoolean5651 = false;
		}
	}
}
