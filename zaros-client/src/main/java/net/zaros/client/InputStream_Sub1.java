package net.zaros.client;
/* InputStream_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.InputStream;

final class InputStream_Sub1 extends InputStream {
	static Class218[] aClass218Array34;

	static final ModeWhere findMode(int i, byte i_0_) {
		ModeWhere[] modes = Packet.getModeWheres(26);
		if (i_0_ != 84)
			method128(-93);
		for (int i_1_ = 0; modes.length > i_1_; i_1_++) {
			ModeWhere class248 = modes[i_1_];
			if (i == class248.modeID)
				return class248;
		}
		return null;
	}

	public final int read() {
		Class106_Sub1.method942(30000L, 0);
		return -1;
	}

	static final void method127(int i, int i_2_) {
		if (Class366_Sub6.anInt5392 == 7 && (Class296_Sub39_Sub12.loginState == i && Class397_Sub3.anInt5799 == 0)) {
			Class220.anInt2150 = i_2_;
			Class41_Sub8.method422(1, 9);
		}
	}

	public static void method128(int i) {
		if (i < -106)
			aClass218Array34 = null;
	}

	static final String method129(int i, boolean bool, int i_3_) {
		if (!bool || i < 0)
			return Integer.toString(i);
		if (i_3_ != -1)
			method129(-73, true, 11);
		return Class220.method2069(10, bool, i, 54);
	}
}
