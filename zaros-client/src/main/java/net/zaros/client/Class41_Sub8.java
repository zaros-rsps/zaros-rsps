package net.zaros.client;

/* Class41_Sub8 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub8 extends Class41 {
	static int anInt3763 = 0;
	static Class326[] aClass326Array3764;
	static int[] anIntArray3765;

	public static void method421(byte i) {
		if (i != 60)
			anIntArray3765 = null;
		aClass326Array3764 = null;
		anIntArray3765 = null;
	}

	static final void method422(int i, int i_0_) {
		if (Class366_Sub6.anInt5392 != i_0_) {
			if (i_0_ == 14 || i_0_ == 15)
				ObjectDefinitionLoader.method366((byte) 36);
			if (i_0_ != 14 && Class306.aClass154_2744 != null) {
				Class306.aClass154_2744.method1561(i + 124);
				Class306.aClass154_2744 = null;
			}
			if (i_0_ == 3)
				Class274.method2315((Class99.anInt1064 != NPCDefinition.aClass268_1478.anInt2490), (byte) 113);
			if (i_0_ == 7)
				Class224.method2079((NPCDefinition.aClass268_1478.anInt2493 != Class99.anInt1064), i ^ ~0x4ca5);
			if (i_0_ == 5 || i_0_ == 13)
				Class338.method3436((byte) -92);
			else if (i_0_ == 6 || i_0_ == 9 && Class366_Sub6.anInt5392 != 10)
				ObjectDefinitionLoader.method366((byte) 36);
			if (Class56.method666(Class366_Sub6.anInt5392, i ^ 0x64e1)) {
				Class296_Sub39_Sub1.fs2.discardUnpacked_ = 2;
				Class296_Sub30.aClass138_4822.discardUnpacked_ = 2;
				Class326.fs16.discardUnpacked_ = 2;
				Class199.aClass138_2005.discardUnpacked_ = 2;
				Class63.aClass138_726.discardUnpacked_ = 2;
				Class196.fs20.discardUnpacked_ = 2;
				Class365.gfxFS.discardUnpacked_ = 2;
			}
			if (Class56.method666(i_0_, 25824)) {
				Class117.anInt1187 = 0;
				StaticMethods.anInt5969 = 0;
				Class240.anInt2264 = 1;
				OutputStream_Sub1.anInt40 = 0;
				CS2Stack.anInt2252 = 1;
				Class254.method2212(i ^ 0x6f2c, true);
				Class296_Sub39_Sub1.fs2.discardUnpacked_ = 1;
				Class296_Sub30.aClass138_4822.discardUnpacked_ = 1;
				Class326.fs16.discardUnpacked_ = 1;
				Class199.aClass138_2005.discardUnpacked_ = 1;
				Class63.aClass138_726.discardUnpacked_ = 1;
				Class196.fs20.discardUnpacked_ = 1;
				Class365.gfxFS.discardUnpacked_ = 1;
			}
			if (i_0_ == 12 || i_0_ == 3)
				Class44_Sub1.method572((byte) 37);
			boolean bool = (i_0_ == 2 || Class355_Sub1.method3701(true, i_0_) || BITConfigDefinition.method2353(-8, i_0_));
			boolean bool_1_ = (Class366_Sub6.anInt5392 == 2 || Class355_Sub1.method3701(true, Class366_Sub6.anInt5392) || BITConfigDefinition.method2353(-8, Class366_Sub6.anInt5392));
			if (!bool_1_ == bool) {
				if (!bool) {
					StaticMethods.method2461(2, 0);
					Class42.aClass285_395.method2378(-120, true);
				} else {
					Class30.anInt318 = Class122_Sub1.anInt5657;
					if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5021.method489(123) == 0)
						StaticMethods.method2461(2, 0);
					else {
						Class296_Sub51_Sub14.method3117(Class42_Sub4.fs6, 2, false, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5021.method489(121), Class122_Sub1.anInt5657, 0, false);
						Class296_Sub34.method2734(255);
					}
					Class42.aClass285_395.method2378(-80, false);
				}
			}
			if (i != 1)
				aClass326Array3764 = null;
			if (Class56.method666(i_0_, 25824) || i_0_ == 14 || i_0_ == 15)
				Class41_Sub13.aHa3774.w();
			Class366_Sub6.anInt5392 = i_0_;
		}
	}

	final int method380(int i, byte i_2_) {
		if (aClass296_Sub50_392.method3055(true).method327(false) < 96)
			return 3;
		if (i_2_ != 41)
			ItemArrayMethods.getItemsNode(false, 50);
		return 1;
	}

	static final void method423(int i) {
		if (i != 0)
			method423(-64);
		if (BITConfigDefinition.method2353(-8, Class366_Sub6.anInt5392))
			StringNode.logout(i ^ 0x3, false);
		else {
			Class306.aClass154_2744 = Class296_Sub45_Sub2.aClass204_6277.aClass154_2045;
			Class296_Sub45_Sub2.aClass204_6277.aClass154_2045 = null;
			method422(i ^ 0x1, 14);
		}
	}

	Class41_Sub8(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
		Class103.method897(1, anInt389);
	}

	final void method381(int i, byte i_3_) {
		if (i_3_ != -110)
			method421((byte) -118);
		anInt389 = i;
		Class103.method897(1, anInt389);
	}

	final boolean method424(int i) {
		if (aClass296_Sub50_392.method3055(true).method327(false) < 96)
			return false;
		if (i != -25952)
			aClass326Array3764 = null;
		return true;
	}

	Class41_Sub8(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
		Class103.method897(1, anInt389);
	}

	final int method426(int i) {
		if (i <= 114)
			method424(-73);
		return anInt389;
	}

	final void method386(int i) {
		if (aClass296_Sub50_392.method3055(true).method327(false) < 96)
			anInt389 = 0;
		if (i == 2) {
			if (anInt389 < 0 || anInt389 > 2)
				anInt389 = method383((byte) 110);
		}
	}

	final int method383(byte i) {
		if (aClass296_Sub50_392.method3055(true).method327(false) < 96)
			return 0;
		if (i != 110)
			return -47;
		return 2;
	}
}
