package net.zaros.client;
/* Class83 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class83 {
	static boolean aBoolean914 = false;
	private Queuable aClass296_Sub39_915;
	private Queue aClass145_916;
	static Class294 aClass294_917 = new Class294(9, 2);
	static int anInt918 = 1337;
	static Class209 aClass209_919;
	static int worldFlags = 0;

	final Queuable method810(byte i) {
		Queuable class296_sub39 = aClass296_Sub39_915;
		if (class296_sub39 == aClass145_916.head) {
			aClass296_Sub39_915 = null;
			return null;
		}
		if (i <= 93)
			worldFlags = -34;
		aClass296_Sub39_915 = class296_sub39.queue_next;
		return class296_sub39;
	}

	public static void method811(int i) {
		aClass294_917 = null;
		if (i == 0)
			aClass209_919 = null;
	}

	final Queuable method812(int i) {
		if (i != 0)
			return null;
		Queuable class296_sub39 = aClass145_916.head.queue_next;
		if (aClass145_916.head == class296_sub39) {
			aClass296_Sub39_915 = null;
			return null;
		}
		aClass296_Sub39_915 = class296_sub39.queue_next;
		return class296_sub39;
	}

	public Class83() {
		/* empty */
	}

	Class83(Queue class145) {
		aClass145_916 = class145;
	}

	static {
		aClass209_919 = new Class209(61);
	}
}
