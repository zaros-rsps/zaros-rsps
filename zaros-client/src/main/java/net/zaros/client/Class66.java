package net.zaros.client;

/* Class66 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class66 {
	private int anInt734;
	private int anInt735;
	private int[] anIntArray736;
	private int anInt737 = Class296_Sub18.method2641(16);
	private int anInt738;
	private int anInt739;
	private int anInt740;

	final void method710(float[] fs, int i, boolean bool) {
		for (int i_0_ = 0; i_0_ < i; i_0_++)
			fs[i_0_] = 0.0F;
		if (!bool) {
			int i_1_ = Class296_Sub18.aClass346Array4683[anInt738].anInt3020;
			int i_2_ = anInt735 - anInt734;
			int i_3_ = i_2_ / anInt740;
			int[] is = new int[i_3_];
			for (int i_4_ = 0; i_4_ < 8; i_4_++) {
				int i_5_ = 0;
				while (i_5_ < i_3_) {
					if (i_4_ == 0) {
						int i_6_ = Class296_Sub18.aClass346Array4683[anInt738].method3667();
						for (int i_7_ = i_1_ - 1; i_7_ >= 0; i_7_--) {
							if (i_5_ + i_7_ < i_3_)
								is[i_5_ + i_7_] = i_6_ % anInt739;
							i_6_ /= anInt739;
						}
					}
					for (int i_8_ = 0; i_8_ < i_1_; i_8_++) {
						int i_9_ = is[i_5_];
						int i_10_ = anIntArray736[i_9_ * 8 + i_4_];
						if (i_10_ >= 0) {
							int i_11_ = anInt734 + i_5_ * anInt740;
							Class346 class346 = Class296_Sub18.aClass346Array4683[i_10_];
							if (anInt737 == 0) {
								int i_12_ = anInt740 / class346.anInt3020;
								for (int i_13_ = 0; i_13_ < i_12_; i_13_++) {
									float[] fs_14_ = class346.method3665();
									for (int i_15_ = 0; i_15_ < class346.anInt3020; i_15_++)
										fs[i_11_ + i_13_ + i_15_ * i_12_] += fs_14_[i_15_];
								}
							} else {
								int i_16_ = 0;
								while (i_16_ < anInt740) {
									float[] fs_17_ = class346.method3665();
									for (int i_18_ = 0; i_18_ < class346.anInt3020; i_18_++) {
										fs[i_11_ + i_16_] += fs_17_[i_18_];
										i_16_++;
									}
								}
							}
						}
						if (++i_5_ >= i_3_)
							break;
					}
				}
			}
		}
	}

	Class66() {
		anInt734 = Class296_Sub18.method2641(24);
		anInt735 = Class296_Sub18.method2641(24);
		anInt740 = Class296_Sub18.method2641(24) + 1;
		anInt739 = Class296_Sub18.method2641(6) + 1;
		anInt738 = Class296_Sub18.method2641(8);
		int[] is = new int[anInt739];
		for (int i = 0; i < anInt739; i++) {
			int i_19_ = 0;
			int i_20_ = Class296_Sub18.method2641(3);
			boolean bool = Class296_Sub18.method2649() != 0;
			if (bool)
				i_19_ = Class296_Sub18.method2641(5);
			is[i] = i_19_ << 3 | i_20_;
		}
		anIntArray736 = new int[anInt739 * 8];
		for (int i = 0; i < anInt739 * 8; i++)
			anIntArray736[i] = ((is[i >> 3] & 1 << (i & 0x7)) != 0 ? Class296_Sub18.method2641(8) : -1);
	}
}
