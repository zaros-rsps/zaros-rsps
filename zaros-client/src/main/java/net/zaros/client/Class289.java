package net.zaros.client;

/* Class289 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class289 {
	static final void method2398(int i, int i_0_, int i_1_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i, 13);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.intParam = i_1_;
		if (i_0_ != -16526)
			method2399(111, 15, 92);
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	public Class289() {
		/* empty */
	}

	static final boolean method2399(int i, int i_2_, int i_3_) {
		if (i_3_ != -27448)
			return true;
		if (!((i_2_ & 0x70000) != 0 | Class175.method1701((byte) -9, i_2_, i)) && !Class296_Sub51_Sub26.method3150(i_2_, (byte) 105, i))
			return false;
		return true;
	}
}
