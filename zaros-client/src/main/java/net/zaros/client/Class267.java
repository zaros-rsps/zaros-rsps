package net.zaros.client;
/* Class267 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.DisplayMode;
import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.lang.reflect.Field;

public final class Class267 {
	private DisplayMode aDisplayMode2483;
	private GraphicsDevice aGraphicsDevice2484;

	public final void method2291(Frame frame, int i, int i_0_, int i_1_, int i_2_) {
		aDisplayMode2483 = aGraphicsDevice2484.getDisplayMode();
		if (aDisplayMode2483 == null)
			throw new NullPointerException();
		frame.setUndecorated(true);
		frame.enableInputMethods(false);
		method2293(frame, false);
		if (i_2_ == 0) {
			int i_3_ = aDisplayMode2483.getRefreshRate();
			DisplayMode[] displaymodes = aGraphicsDevice2484.getDisplayModes();
			boolean bool = false;
			for (int i_4_ = 0; displaymodes.length > i_4_; i_4_++) {
				if (i == displaymodes[i_4_].getWidth() && displaymodes[i_4_].getHeight() == i_0_ && displaymodes[i_4_].getBitDepth() == i_1_) {
					int i_5_ = displaymodes[i_4_].getRefreshRate();
					if (!bool || Math.abs(-i_3_ + i_5_) < Math.abs(i_2_ - i_3_)) {
						bool = true;
						i_2_ = i_5_;
					}
				}
			}
			if (!bool)
				i_2_ = i_3_;
		}
		aGraphicsDevice2484.setDisplayMode(new DisplayMode(i, i_0_, i_1_, i_2_));
	}

	public final void method2292() {
		if (aDisplayMode2483 != null) {
			aGraphicsDevice2484.setDisplayMode(aDisplayMode2483);
			if (!aGraphicsDevice2484.getDisplayMode().equals(aDisplayMode2483))
				throw new RuntimeException("Did not return to correct resolution!");
			aDisplayMode2483 = null;
		}
		method2293(null, false);
	}

	private final void method2293(Frame frame, boolean bool) {
		boolean bool_6_ = bool;
		try {
			Field field = Class.forName("sun.awt.Win32GraphicsDevice").getDeclaredField("valid");
			field.setAccessible(true);
			boolean bool_7_ = ((Boolean) field.get(aGraphicsDevice2484)).booleanValue();
			if (bool_7_) {
				field.set(aGraphicsDevice2484, Boolean.FALSE);
				bool_6_ = true;
			}
		} catch (Throwable throwable) {
			/* empty */
		}
		try {
			aGraphicsDevice2484.setFullScreenWindow(frame);
		} finally {
			if (bool_6_) {
				try {
					Field field = Class.forName("sun.awt.Win32GraphicsDevice").getDeclaredField("valid");
					field.set(aGraphicsDevice2484, Boolean.TRUE);
				} catch (Throwable throwable) {
					/* empty */
				}
			}
		}
	}

	public Class267() throws Exception {
		GraphicsEnvironment graphicsenvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		aGraphicsDevice2484 = graphicsenvironment.getDefaultScreenDevice();
		if (!aGraphicsDevice2484.isFullScreenSupported()) {
			GraphicsDevice[] graphicsdevices = graphicsenvironment.getScreenDevices();
			GraphicsDevice[] graphicsdevices_8_ = graphicsdevices;
			for (int i = 0; graphicsdevices_8_.length > i; i++) {
				GraphicsDevice graphicsdevice = graphicsdevices_8_[i];
				if (graphicsdevice != null && graphicsdevice.isFullScreenSupported()) {
					aGraphicsDevice2484 = graphicsdevice;
					return;
				}
			}
			throw new Exception();
		}
	}

	public final int[] method2294() {
		DisplayMode[] displaymodes = aGraphicsDevice2484.getDisplayModes();
		int[] is = new int[displaymodes.length << 2];
		for (int i = 0; displaymodes.length > i; i++) {
			is[i << 2] = displaymodes[i].getWidth();
			is[(i << 2) + 1] = displaymodes[i].getHeight();
			is[(i << 2) + 2] = displaymodes[i].getBitDepth();
			is[(i << 2) + 3] = displaymodes[i].getRefreshRate();
		}
		return is;
	}
}
