package net.zaros.client;

/* Class305 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class305 {
	private static Class14 aClass14_2738 = new Class14();

	static final int method3273(byte[] is, int i, byte[] is_0_, int i_1_, int i_2_) {
		synchronized (aClass14_2738) {
			aClass14_2738.aByteArray156 = is_0_;
			aClass14_2738.anInt149 = i_2_;
			aClass14_2738.aByteArray174 = is;
			aClass14_2738.anInt157 = 0;
			aClass14_2738.anInt141 = i;
			aClass14_2738.anInt172 = 0;
			aClass14_2738.anInt161 = 0;
			aClass14_2738.anInt158 = 0;
			aClass14_2738.anInt148 = 0;
			method3277(aClass14_2738);
			i -= aClass14_2738.anInt141;
			aClass14_2738.aByteArray156 = null;
			aClass14_2738.aByteArray174 = null;
			return i;
		}
	}

	private static final void method3274(int[] is, int[] is_3_, int[] is_4_, byte[] is_5_, int i, int i_6_, int i_7_) {
		int i_8_ = 0;
		for (int i_9_ = i; i_9_ <= i_6_; i_9_++) {
			for (int i_10_ = 0; i_10_ < i_7_; i_10_++) {
				if (is_5_[i_10_] == i_9_) {
					is_4_[i_8_] = i_10_;
					i_8_++;
				}
			}
		}
		for (int i_11_ = 0; i_11_ < 23; i_11_++)
			is_3_[i_11_] = 0;
		for (int i_12_ = 0; i_12_ < i_7_; i_12_++)
			is_3_[is_5_[i_12_] + 1]++;
		for (int i_13_ = 1; i_13_ < 23; i_13_++)
			is_3_[i_13_] += is_3_[i_13_ - 1];
		for (int i_14_ = 0; i_14_ < 23; i_14_++)
			is[i_14_] = 0;
		int i_15_ = 0;
		for (int i_16_ = i; i_16_ <= i_6_; i_16_++) {
			i_15_ += is_3_[i_16_ + 1] - is_3_[i_16_];
			is[i_16_] = i_15_ - 1;
			i_15_ <<= 1;
		}
		for (int i_17_ = i + 1; i_17_ <= i_6_; i_17_++)
			is_3_[i_17_] = (is[i_17_ - 1] + 1 << 1) - is_3_[i_17_];
	}

	private static final byte method3275(Class14 class14) {
		return (byte) method3281(1, class14);
	}

	private static final void method3276(Class14 class14) {
		class14.anInt169 = 0;
		for (int i = 0; i < 256; i++) {
			if (class14.aBooleanArray152[i]) {
				class14.aByteArray139[class14.anInt169] = (byte) i;
				class14.anInt169++;
			}
		}
	}

	private static final void method3277(Class14 class14) {
		boolean bool = false;
		boolean bool_18_ = false;
		boolean bool_19_ = false;
		boolean bool_20_ = false;
		boolean bool_21_ = false;
		boolean bool_22_ = false;
		boolean bool_23_ = false;
		boolean bool_24_ = false;
		boolean bool_25_ = false;
		boolean bool_26_ = false;
		boolean bool_27_ = false;
		boolean bool_28_ = false;
		boolean bool_29_ = false;
		boolean bool_30_ = false;
		boolean bool_31_ = false;
		boolean bool_32_ = false;
		boolean bool_33_ = false;
		boolean bool_34_ = false;
		int i = 0;
		int[] is = null;
		int[] is_35_ = null;
		int[] is_36_ = null;
		class14.anInt151 = 1;
		if (ConfigurationsLoader.anIntArray88 == null)
			ConfigurationsLoader.anIntArray88 = new int[class14.anInt151 * 100000];
		boolean bool_37_ = true;
		while (bool_37_) {
			byte i_38_ = method3279(class14);
			if (i_38_ == 23)
				break;
			i_38_ = method3279(class14);
			i_38_ = method3279(class14);
			i_38_ = method3279(class14);
			i_38_ = method3279(class14);
			i_38_ = method3279(class14);
			i_38_ = method3279(class14);
			i_38_ = method3279(class14);
			i_38_ = method3279(class14);
			i_38_ = method3279(class14);
			i_38_ = method3275(class14);
			class14.anInt146 = 0;
			int i_39_ = method3279(class14);
			class14.anInt146 = class14.anInt146 << 8 | i_39_ & 0xff;
			i_39_ = method3279(class14);
			class14.anInt146 = class14.anInt146 << 8 | i_39_ & 0xff;
			i_39_ = method3279(class14);
			class14.anInt146 = class14.anInt146 << 8 | i_39_ & 0xff;
			for (int i_40_ = 0; i_40_ < 16; i_40_++) {
				i_38_ = method3275(class14);
				if (i_38_ == 1)
					class14.aBooleanArray143[i_40_] = true;
				else
					class14.aBooleanArray143[i_40_] = false;
			}
			for (int i_41_ = 0; i_41_ < 256; i_41_++)
				class14.aBooleanArray152[i_41_] = false;
			for (int i_42_ = 0; i_42_ < 16; i_42_++) {
				if (class14.aBooleanArray143[i_42_]) {
					for (int i_43_ = 0; i_43_ < 16; i_43_++) {
						i_38_ = method3275(class14);
						if (i_38_ == 1)
							class14.aBooleanArray152[i_42_ * 16 + i_43_] = true;
					}
				}
			}
			method3276(class14);
			int i_44_ = class14.anInt169 + 2;
			int i_45_ = method3281(3, class14);
			int i_46_ = method3281(15, class14);
			for (int i_47_ = 0; i_47_ < i_46_; i_47_++) {
				int i_48_ = 0;
				for (;;) {
					i_38_ = method3275(class14);
					if (i_38_ == 0)
						break;
					i_48_++;
				}
				class14.aByteArray154[i_47_] = (byte) i_48_;
			}
			byte[] is_49_ = new byte[6];
			for (byte i_50_ = 0; i_50_ < i_45_; i_50_++)
				is_49_[i_50_] = i_50_;
			for (int i_51_ = 0; i_51_ < i_46_; i_51_++) {
				byte i_52_ = class14.aByteArray154[i_51_];
				byte i_53_ = is_49_[i_52_];
				for (/**/; i_52_ > 0; i_52_--)
					is_49_[i_52_] = is_49_[i_52_ - 1];
				is_49_[0] = i_53_;
				class14.aByteArray159[i_51_] = i_53_;
			}
			for (int i_54_ = 0; i_54_ < i_45_; i_54_++) {
				int i_55_ = method3281(5, class14);
				for (int i_56_ = 0; i_56_ < i_44_; i_56_++) {
					for (;;) {
						i_38_ = method3275(class14);
						if (i_38_ == 0)
							break;
						i_38_ = method3275(class14);
						if (i_38_ == 0)
							i_55_++;
						else
							i_55_--;
					}
					class14.aByteArrayArray164[i_54_][i_56_] = (byte) i_55_;
				}
			}
			for (int i_57_ = 0; i_57_ < i_45_; i_57_++) {
				int i_58_ = 32;
				byte i_59_ = 0;
				for (int i_60_ = 0; i_60_ < i_44_; i_60_++) {
					if (class14.aByteArrayArray164[i_57_][i_60_] > i_59_)
						i_59_ = class14.aByteArrayArray164[i_57_][i_60_];
					if (class14.aByteArrayArray164[i_57_][i_60_] < i_58_)
						i_58_ = class14.aByteArrayArray164[i_57_][i_60_];
				}
				method3274(class14.anIntArrayArray165[i_57_], class14.anIntArrayArray145[i_57_], class14.anIntArrayArray142[i_57_], class14.aByteArrayArray164[i_57_], i_58_, i_59_, i_44_);
				class14.anIntArray150[i_57_] = i_58_;
			}
			int i_61_ = class14.anInt169 + 1;
			int i_62_ = -1;
			int i_63_ = 0;
			for (int i_64_ = 0; i_64_ <= 255; i_64_++)
				class14.anIntArray140[i_64_] = 0;
			int i_65_ = 4095;
			for (int i_66_ = 15; i_66_ >= 0; i_66_--) {
				for (int i_67_ = 15; i_67_ >= 0; i_67_--) {
					class14.aByteArray167[i_65_] = (byte) (i_66_ * 16 + i_67_);
					i_65_--;
				}
				class14.anIntArray163[i_66_] = i_65_ + 1;
			}
			int i_68_ = 0;
			if (i_63_ == 0) {
				i_62_++;
				i_63_ = 50;
				byte i_69_ = class14.aByteArray159[i_62_];
				i = class14.anIntArray150[i_69_];
				is = class14.anIntArrayArray165[i_69_];
				is_36_ = class14.anIntArrayArray142[i_69_];
				is_35_ = class14.anIntArrayArray145[i_69_];
			}
			i_63_--;
			int i_70_ = i;
			int i_71_;
			int i_72_;
			for (i_72_ = method3281(i_70_, class14); i_72_ > is[i_70_]; i_72_ = i_72_ << 1 | i_71_) {
				i_70_++;
				i_71_ = method3275(class14);
			}
			int i_73_ = is_36_[i_72_ - is_35_[i_70_]];
			while (i_73_ != i_61_) {
				if (i_73_ == 0 || i_73_ == 1) {
					int i_74_ = -1;
					int i_75_ = 1;
					do {
						if (i_73_ == 0)
							i_74_ += i_75_;
						else if (i_73_ == 1)
							i_74_ += i_75_ * 2;
						i_75_ *= 2;
						if (i_63_ == 0) {
							i_62_++;
							i_63_ = 50;
							byte i_76_ = class14.aByteArray159[i_62_];
							i = class14.anIntArray150[i_76_];
							is = class14.anIntArrayArray165[i_76_];
							is_36_ = class14.anIntArrayArray142[i_76_];
							is_35_ = class14.anIntArrayArray145[i_76_];
						}
						i_63_--;
						i_70_ = i;
						for (i_72_ = method3281(i_70_, class14); i_72_ > is[i_70_]; i_72_ = i_72_ << 1 | i_71_) {
							i_70_++;
							i_71_ = method3275(class14);
						}
						i_73_ = is_36_[i_72_ - is_35_[i_70_]];
					} while (i_73_ == 0 || i_73_ == 1);
					i_74_++;
					i_39_ = (class14.aByteArray139[(class14.aByteArray167[class14.anIntArray163[0]] & 0xff)]);
					class14.anIntArray140[i_39_ & 0xff] += i_74_;
					for (/**/; i_74_ > 0; i_74_--) {
						ConfigurationsLoader.anIntArray88[i_68_] = i_39_ & 0xff;
						i_68_++;
					}
				} else {
					int i_77_ = i_73_ - 1;
					if (i_77_ < 16) {
						int i_78_ = class14.anIntArray163[0];
						i_38_ = class14.aByteArray167[i_78_ + i_77_];
						for (/**/; i_77_ > 3; i_77_ -= 4) {
							int i_79_ = i_78_ + i_77_;
							class14.aByteArray167[i_79_] = class14.aByteArray167[i_79_ - 1];
							class14.aByteArray167[i_79_ - 1] = class14.aByteArray167[i_79_ - 2];
							class14.aByteArray167[i_79_ - 2] = class14.aByteArray167[i_79_ - 3];
							class14.aByteArray167[i_79_ - 3] = class14.aByteArray167[i_79_ - 4];
						}
						for (/**/; i_77_ > 0; i_77_--)
							class14.aByteArray167[i_78_ + i_77_] = class14.aByteArray167[i_78_ + i_77_ - 1];
						class14.aByteArray167[i_78_] = i_38_;
					} else {
						int i_80_ = i_77_ / 16;
						int i_81_ = i_77_ % 16;
						int i_82_ = class14.anIntArray163[i_80_] + i_81_;
						i_38_ = class14.aByteArray167[i_82_];
						for (/**/; i_82_ > class14.anIntArray163[i_80_]; i_82_--)
							class14.aByteArray167[i_82_] = class14.aByteArray167[i_82_ - 1];
						class14.anIntArray163[i_80_]++;
						for (/**/; i_80_ > 0; i_80_--) {
							class14.anIntArray163[i_80_]--;
							class14.aByteArray167[class14.anIntArray163[i_80_]] = (class14.aByteArray167[(class14.anIntArray163[i_80_ - 1] + 16 - 1)]);
						}
						class14.anIntArray163[0]--;
						class14.aByteArray167[class14.anIntArray163[0]] = i_38_;
						if (class14.anIntArray163[0] == 0) {
							int i_83_ = 4095;
							for (int i_84_ = 15; i_84_ >= 0; i_84_--) {
								for (int i_85_ = 15; i_85_ >= 0; i_85_--) {
									class14.aByteArray167[i_83_] = (class14.aByteArray167[(class14.anIntArray163[i_84_] + i_85_)]);
									i_83_--;
								}
								class14.anIntArray163[i_84_] = i_83_ + 1;
							}
						}
					}
					class14.anIntArray140[(class14.aByteArray139[i_38_ & 0xff] & 0xff)]++;
					ConfigurationsLoader.anIntArray88[i_68_] = class14.aByteArray139[i_38_ & 0xff] & 0xff;
					i_68_++;
					if (i_63_ == 0) {
						i_62_++;
						i_63_ = 50;
						byte i_86_ = class14.aByteArray159[i_62_];
						i = class14.anIntArray150[i_86_];
						is = class14.anIntArrayArray165[i_86_];
						is_36_ = class14.anIntArrayArray142[i_86_];
						is_35_ = class14.anIntArrayArray145[i_86_];
					}
					i_63_--;
					i_70_ = i;
					for (i_72_ = method3281(i_70_, class14); i_72_ > is[i_70_]; i_72_ = i_72_ << 1 | i_71_) {
						i_70_++;
						i_71_ = method3275(class14);
					}
					i_73_ = is_36_[i_72_ - is_35_[i_70_]];
				}
			}
			class14.anInt160 = 0;
			class14.aByte171 = (byte) 0;
			class14.anIntArray147[0] = 0;
			for (int i_87_ = 1; i_87_ <= 256; i_87_++)
				class14.anIntArray147[i_87_] = class14.anIntArray140[i_87_ - 1];
			for (int i_88_ = 1; i_88_ <= 256; i_88_++)
				class14.anIntArray147[i_88_] += class14.anIntArray147[i_88_ - 1];
			for (int i_89_ = 0; i_89_ < i_68_; i_89_++) {
				i_39_ = (byte) (ConfigurationsLoader.anIntArray88[i_89_] & 0xff);
				ConfigurationsLoader.anIntArray88[class14.anIntArray147[i_39_ & 0xff]] |= i_89_ << 8;
				class14.anIntArray147[i_39_ & 0xff]++;
			}
			class14.anInt166 = ConfigurationsLoader.anIntArray88[class14.anInt146] >> 8;
			class14.anInt168 = 0;
			class14.anInt166 = ConfigurationsLoader.anIntArray88[class14.anInt166];
			class14.anInt162 = (byte) (class14.anInt166 & 0xff);
			class14.anInt166 >>= 8;
			class14.anInt168++;
			class14.anInt144 = i_68_;
			method3278(class14);
			if (class14.anInt168 == class14.anInt144 + 1 && class14.anInt160 == 0)
				bool_37_ = true;
			else
				bool_37_ = false;
		}
	}

	private static final void method3278(Class14 class14) {
		byte i = class14.aByte171;
		int i_90_ = class14.anInt160;
		int i_91_ = class14.anInt168;
		int i_92_ = class14.anInt162;
		int[] is = ConfigurationsLoader.anIntArray88;
		int i_93_ = class14.anInt166;
		byte[] is_94_ = class14.aByteArray174;
		int i_95_ = class14.anInt157;
		int i_96_ = class14.anInt141;
		int i_97_ = i_96_;
		int i_98_ = class14.anInt144 + 1;
		while_248_ : for (;;) {
			if (i_90_ > 0) {
				for (;;) {
					if (i_96_ == 0)
						break while_248_;
					if (i_90_ == 1)
						break;
					is_94_[i_95_] = i;
					i_90_--;
					i_95_++;
					i_96_--;
				}
				if (i_96_ == 0) {
					i_90_ = 1;
					break;
				}
				is_94_[i_95_] = i;
				i_95_++;
				i_96_--;
			}
			for (;;) {
				if (i_91_ == i_98_) {
					i_90_ = 0;
					break while_248_;
				}
				i = (byte) i_92_;
				i_93_ = is[i_93_];
				int i_99_ = (byte) i_93_;
				i_93_ >>= 8;
				i_91_++;
				if (i_99_ != i_92_) {
					i_92_ = i_99_;
					if (i_96_ == 0) {
						i_90_ = 1;
						break while_248_;
					}
					is_94_[i_95_] = i;
					i_95_++;
					i_96_--;
				} else {
					if (i_91_ != i_98_)
						break;
					if (i_96_ == 0) {
						i_90_ = 1;
						break while_248_;
					}
					is_94_[i_95_] = i;
					i_95_++;
					i_96_--;
				}
			}
			i_90_ = 2;
			i_93_ = is[i_93_];
			int i_100_ = (byte) i_93_;
			i_93_ >>= 8;
			if (++i_91_ != i_98_) {
				if (i_100_ != i_92_)
					i_92_ = i_100_;
				else {
					i_90_ = 3;
					i_93_ = is[i_93_];
					i_100_ = (byte) i_93_;
					i_93_ >>= 8;
					if (++i_91_ != i_98_) {
						if (i_100_ != i_92_)
							i_92_ = i_100_;
						else {
							i_93_ = is[i_93_];
							i_100_ = (byte) i_93_;
							i_93_ >>= 8;
							i_91_++;
							i_90_ = (i_100_ & 0xff) + 4;
							i_93_ = is[i_93_];
							i_92_ = (byte) i_93_;
							i_93_ >>= 8;
							i_91_++;
						}
					}
				}
			}
		}
		int i_101_ = class14.anInt148;
		class14.anInt148 += i_97_ - i_96_;
		class14.aByte171 = i;
		class14.anInt160 = i_90_;
		class14.anInt168 = i_91_;
		class14.anInt162 = i_92_;
		ConfigurationsLoader.anIntArray88 = is;
		class14.anInt166 = i_93_;
		class14.aByteArray174 = is_94_;
		class14.anInt157 = i_95_;
		class14.anInt141 = i_96_;
	}

	private static final byte method3279(Class14 class14) {
		return (byte) method3281(8, class14);
	}

	public static void method3280() {
		aClass14_2738 = null;
	}

	private static final int method3281(int i, Class14 class14) {
		for (;;) {
			if (class14.anInt172 >= i) {
				int i_102_ = class14.anInt161 >> class14.anInt172 - i & (1 << i) - 1;
				class14.anInt172 -= i;
				return i_102_;
			}
			class14.anInt161 = (class14.anInt161 << 8 | class14.aByteArray156[class14.anInt149] & 0xff);
			class14.anInt172 += 8;
			class14.anInt149++;
			class14.anInt158++;
		}
	}
}
