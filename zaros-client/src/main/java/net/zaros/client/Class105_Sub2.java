package net.zaros.client;
import jaclib.memory.Buffer;

final class Class105_Sub2 extends Class105 implements Interface15_Impl1 {
	static Class241 aClass241_5692;
	static Class161 aClass161_5693 = new Class161(2, 4, 4, 0);
	private Class67 aClass67_5694;
	static boolean aBoolean5695 = true;

	public final int method61(byte i) {
		if (i < 115)
			aBoolean5695 = false;
		return super.method61((byte) 121);
	}

	static final void method913(int i, int i_0_, byte i_1_) {
		if (i_1_ < -88) {
			GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_0_, 14);
			class296_sub39_sub5.insertIntoQueue_2();
			class296_sub39_sub5.intParam = i;
		}
	}

	public static void method914(byte i) {
		aClass241_5692 = null;
		aClass161_5693 = null;
		if (i <= 25)
			aClass241_5692 = null;
	}

	public final void method32(int i, int i_2_) {
		super.method32(i * aClass67_5694.anInt741, i_2_);
		if (i_2_ != -21709)
			method915(43);
	}

	public final boolean method33(int i) {
		if (i != 32185)
			aBoolean5695 = true;
		return super.method909(-10218, aHa_Sub1_Sub1_3657.aMapBuffer5840);
	}

	public final Buffer method30(byte i, boolean bool) {
		if (i >= -17)
			method33(62);
		return super.method907(aHa_Sub1_Sub1_3657.aMapBuffer5840, (byte) -16, bool);
	}

	static final void method915(int i) {
		for (Class296_Sub39_Sub1 class296_sub39_sub1 = ((Class296_Sub39_Sub1) Class152.aClass145_1558.getFront()); class296_sub39_sub1 != null; class296_sub39_sub1 = ((Class296_Sub39_Sub1) Class152.aClass145_1558.getNext())) {
			if (class296_sub39_sub1.anInt6117 > 1) {
				class296_sub39_sub1.anInt6117 = 0;
				ReferenceTable.aClass113_986.put(class296_sub39_sub1, (((Class296_Sub39_Sub9) (class296_sub39_sub1.aClass145_6119.head.queue_next)).aLong6161));
				class296_sub39_sub1.aClass145_6119.clear();
			}
		}
		Class239.anInt2254 = i;
		Class230.anInt2210 = 0;
		HardReferenceWrapper.aClass155_6698.method1581(327680);
		FileOnDisk.aClass263_670.clear();
		Class152.aClass145_1558.clear();
		Class296_Sub11.method2498((byte) 110, (Class296_Sub39_Sub1.aClass296_Sub39_Sub9_6125));
	}

	public final void method31(byte i) {
		super.method31(i);
	}

	Class105_Sub2(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Class67 class67, boolean bool) {
		super(var_ha_Sub1_Sub1, 34963, bool);
		aClass67_5694 = class67;
	}

	public final Class67 method34(int i) {
		if (i != 16839)
			aBoolean5695 = true;
		return aClass67_5694;
	}
}
