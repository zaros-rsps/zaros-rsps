package net.zaros.client;

/* Class296_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class ItemsNode extends Node {
	static int anInt4593 = 16777215;
	static IncomingPacket aClass231_4594 = new IncomingPacket(80, 6);
	int[] itemIDS;
	int[] itemAmounts = new int[1];

	private final long method2437(int[] is, int i, boolean bool, boolean bool_0_, int[] is_1_) {
		long[] ls = TextureOperation.someHashTable;
		long l = -1L;
		l = l >>> 8 ^ ls[(int) (((long) (i >> 8) ^ l) & 0xffL)];
		l = l >>> 8 ^ ls[(int) ((l ^ (long) i) & 0xffL)];
		for (int i_2_ = 0; i_2_ < is_1_.length; i_2_++) {
			l = l >>> 8 ^ ls[(int) ((l ^ (long) (is_1_[i_2_] >> 24)) & 0xffL)];
			l = ls[(int) (((long) (is_1_[i_2_] >> 16) ^ l) & 0xffL)] ^ l >>> 8;
			l = l >>> 8 ^ ls[(int) ((l ^ (long) (is_1_[i_2_] >> 8)) & 0xffL)];
			l = l >>> 8 ^ ls[(int) (((long) is_1_[i_2_] ^ l) & 0xffL)];
		}
		if (is != null) {
			for (int i_3_ = 0; i_3_ < 5; i_3_++)
				l = ls[(int) (((long) is[i_3_] ^ l) & 0xffL)] ^ l >>> 8;
		}
		if (bool)
			itemAmounts = null;
		l = ls[(int) (((long) (!bool_0_ ? 0 : 1) ^ l) & 0xffL)] ^ l >>> 8;
		return l;
	}

	static final int method2438(boolean bool, int i) {
		if (i != 0)
			anInt4593 = -112;
		int i_4_ = ConfigsRegister.anInt3674;
		while_0_ : do {
			do {
				if (i_4_ != 0) {
					if (i_4_ != 1) {
						if (i_4_ == 2)
							break;
						break while_0_;
					}
				} else {
					if (bool)
						return 0;
					return Class34.anInt337;
				}
				return Class34.anInt337;
			} while (false);
			return 0;
		} while (false);
		return 0;
	}

	public static void method2439(byte i) {
		aClass231_4594 = null;
		if (i != 119)
			aClass231_4594 = null;
	}

	final Model method2440(boolean bool, ha var_ha, int i, PlayerEquipment class402, int i_5_, int i_6_, Animator class44) {
		Model class178 = null;
		int i_7_ = i;
		Class280 class280 = null;
		if (i_6_ != -1)
			class280 = Class41_Sub10.aClass62_3768.method700(i_6_, 0);
		int[] is = itemIDS;
		if (class280 != null && class280.anIntArray2559 != null) {
			is = new int[class280.anIntArray2559.length];
			for (int i_8_ = 0; i_8_ < class280.anIntArray2559.length; i_8_++) {
				int i_9_ = class280.anIntArray2559[i_8_];
				if (i_9_ < 0 || itemIDS.length <= i_9_)
					is[i_8_] = -1;
				else
					is[i_8_] = itemIDS[i_9_];
			}
		}
		if (class44 != null)
			i_7_ |= class44.method568(0);
		long l = method2437(class402 == null ? null : class402.body_colours, i_6_, false, bool, is);
		if (Class249.aClass113_2355 != null)
			class178 = (Model) Class249.aClass113_2355.get(l);
		if (class178 == null || var_ha.e(class178.ua(), i_7_) != 0) {
			if (class178 != null)
				i_7_ = var_ha.d(i_7_, class178.ua());
			int i_10_ = i_7_;
			boolean bool_11_ = false;
			for (int i_12_ = 0; is.length > i_12_; i_12_++) {
				if (is[i_12_] != -1 && !Class296_Sub39_Sub1.itemDefinitionLoader.list(is[i_12_]).checkCustomize(bool, null))
					bool_11_ = true;
			}
			if (bool_11_)
				return null;
			Mesh[] class132s = new Mesh[is.length];
			for (int i_13_ = 0; is.length > i_13_; i_13_++) {
				if (is[i_13_] != -1)
					class132s[i_13_] = Class296_Sub39_Sub1.itemDefinitionLoader.list(is[i_13_]).getEquipMesh(bool, null);
			}
			if (class280 != null && class280.anIntArrayArray2558 != null) {
				for (int i_14_ = 0; class280.anIntArrayArray2558.length > i_14_; i_14_++) {
					if (class280.anIntArrayArray2558[i_14_] != null && class132s[i_14_] != null) {
						int i_15_ = class280.anIntArrayArray2558[i_14_][0];
						int i_16_ = class280.anIntArrayArray2558[i_14_][1];
						int i_17_ = class280.anIntArrayArray2558[i_14_][2];
						int i_18_ = class280.anIntArrayArray2558[i_14_][3];
						int i_19_ = class280.anIntArrayArray2558[i_14_][4];
						int i_20_ = class280.anIntArrayArray2558[i_14_][5];
						if (i_18_ != 0 || i_19_ != 0 || i_20_ != 0)
							class132s[i_14_].method1392(i_19_, i_20_, i_18_, (byte) 84);
						if (i_15_ != 0 || i_16_ != 0 || i_17_ != 0)
							class132s[i_14_].translate(i_15_, i_17_, i_16_);
					}
				}
			}
			Mesh class132 = new Mesh(class132s, class132s.length);
			if (class402 != null)
				i_10_ |= 0x4000;
			class178 = var_ha.a(class132, i_10_, Class318.anInt2817, 64, 850);
			if (class402 != null) {
				for (int i_21_ = 0; i_21_ < 10; i_21_++) {
					for (int i_22_ = 0; (CS2Call.aShortArrayArray4971[i_21_].length > i_22_); i_22_++) {
						if ((Class42_Sub4.aShortArrayArrayArray3846[i_21_][i_22_]).length > class402.body_colours[i_21_])
							class178.ia((CS2Call.aShortArrayArray4971[i_21_][i_22_]), (Class42_Sub4.aShortArrayArrayArray3846[i_21_][i_22_][class402.body_colours[i_21_]]));
					}
				}
			}
			if (Class249.aClass113_2355 != null) {
				class178.s(i_7_);
				Class249.aClass113_2355.put(class178, l);
			}
		}
		if (class44 == null)
			return class178;
		Model class178_23_ = class178.method1728((byte) 1, i_7_, true);
		class44.method556(class178_23_, 0, (byte) -63);
		int i_24_ = -61 / ((i_5_ - 75) / 48);
		return class178_23_;
	}

	static final boolean method2441(int i, int i_25_) {
		if (i != -26121)
			return true;
		if (i_25_ < 12 || i_25_ > 17)
			return false;
		return true;
	}

	public ItemsNode() {
		itemIDS = new int[]{-1};
	}
}
