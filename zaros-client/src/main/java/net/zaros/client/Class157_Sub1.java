package net.zaros.client;
/* Class157_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

public final class Class157_Sub1 extends Class157 {
	private ProxySelector aProxySelector4295 = ProxySelector.getDefault();
	static Class aClass4296;
	static Class aClass4297;

	private final Socket method1591(Proxy proxy, int i) throws IOException {
		if (i != -8410)
			aProxySelector4295 = null;
		if (proxy.type() == Proxy.Type.DIRECT)
			return method1590(false);
		java.net.SocketAddress socketaddress = proxy.address();
		if (!(socketaddress instanceof InetSocketAddress))
			return null;
		InetSocketAddress inetsocketaddress = (InetSocketAddress) socketaddress;
		if (proxy.type() != Proxy.Type.HTTP) {
			if (proxy.type() == Proxy.Type.SOCKS) {
				Socket socket = new Socket(proxy);
				socket.connect(new InetSocketAddress(aString1593, anInt1594));
				return socket;
			}
		} else {
			String string = null;
			try {
				Class var_class = (Class.forName("sun.net.www.protocol.http.AuthenticationInfo"));
				Method method = (var_class.getDeclaredMethod("getProxyAuth", (new Class[]{(aClass4296 == null ? aClass4296 = method1593("java.lang.String") : aClass4296), Integer.TYPE})));
				method.setAccessible(true);
				Object object = method.invoke(null, (new Object[]{inetsocketaddress.getHostName(), new Integer(inetsocketaddress.getPort())}));
				if (object != null) {
					Method method_0_ = (var_class.getDeclaredMethod("supportsPreemptiveAuthorization", new Class[0]));
					method_0_.setAccessible(true);
					if (((Boolean) method_0_.invoke(object, new Object[0])).booleanValue()) {
						Method method_1_ = var_class.getDeclaredMethod("getHeaderName", new Class[0]);
						method_1_.setAccessible(true);
						Method method_2_ = (var_class.getDeclaredMethod("getHeaderValue", (new Class[]{(aClass4297 == null ? aClass4297 = method1593("java.net.URL") : aClass4297), (aClass4296 == null ? (aClass4296 = method1593("java.lang.String")) : aClass4296)})));
						method_2_.setAccessible(true);
						String string_3_ = (String) method_1_.invoke(object, new Object[0]);
						String string_4_ = ((String) method_2_.invoke(object, (new Object[]{new URL("https://" + aString1593 + "/"), "https"})));
						string = string_3_ + ": " + string_4_;
					}
				}
			} catch (Exception exception) {
				/* empty */
			}
			return method1592(inetsocketaddress.getHostName(), inetsocketaddress.getPort(), string);
		}
		return null;
	}

	private final Socket method1592(String string, int i, String string_5_) throws IOException {
		Socket socket = new Socket(string, i);
		socket.setSoTimeout(10000);
		OutputStream outputstream = socket.getOutputStream();
		if (string_5_ != null)
			outputstream.write(("CONNECT " + aString1593 + ":" + anInt1594 + " HTTP/1.0\n" + string_5_ + "\n\n").getBytes(Charset.forName("ISO-8859-1")));
		else
			outputstream.write(("CONNECT " + aString1593 + ":" + anInt1594 + " HTTP/1.0\n\n").getBytes(Charset.forName("ISO-8859-1")));
		outputstream.flush();
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		String string_6_ = bufferedreader.readLine();
		if (string_6_ != null) {
			if (string_6_.startsWith("HTTP/1.0 200") || string_6_.startsWith("HTTP/1.1 200"))
				return socket;
			if (string_6_.startsWith("HTTP/1.0 407") || string_6_.startsWith("HTTP/1.1 407")) {
				int i_7_ = 0;
				string_6_ = bufferedreader.readLine();
				String string_8_ = "proxy-authenticate: ";
				for (/**/; string_6_ != null && i_7_ < 50; string_6_ = bufferedreader.readLine()) {
					if (string_6_.toLowerCase().startsWith(string_8_)) {
						string_6_ = string_6_.substring(string_8_.length()).trim();
						int i_9_ = string_6_.indexOf(' ');
						if (i_9_ != -1)
							string_6_ = string_6_.substring(0, i_9_);
						throw new IOException_Sub1(string_6_);
					}
					i_7_++;
				}
				throw new IOException_Sub1("");
			}
		}
		outputstream.close();
		bufferedreader.close();
		socket.close();
		return null;
	}

	final Socket method1586(int i) throws IOException {
		boolean bool = (Boolean.parseBoolean(System.getProperty("java.net.useSystemProxies")));
		if (!bool)
			System.setProperty("java.net.useSystemProxies", "true");
		boolean bool_10_ = anInt1594 == 443;
		List list;
		List list_11_;
		try {
			list = aProxySelector4295.select(new URI((!bool_10_ ? "http" : "https") + "://" + aString1593));
			list_11_ = aProxySelector4295.select(new URI((!bool_10_ ? "https" : "http") + "://" + aString1593));
		} catch (URISyntaxException urisyntaxexception) {
			return method1590(false);
		}
		list.addAll(list_11_);
		Object[] objects = list.toArray();
		if (i != -1)
			return null;
		IOException_Sub1 ioexception_sub1 = null;
		Object[] objects_12_ = objects;
		for (int i_13_ = 0; objects_12_.length > i_13_; i_13_++) {
			Object object = objects_12_[i_13_];
			Proxy proxy = (Proxy) object;
			Socket socket;
			try {
				Socket socket_14_ = method1591(proxy, -8410);
				if (socket_14_ == null)
					continue;
				socket = socket_14_;
			} catch (IOException_Sub1 ioexception_sub1_15_) {
				ioexception_sub1 = ioexception_sub1_15_;
				continue;
			} catch (IOException ioexception) {
				continue;
			}
			return socket;
		}
		if (ioexception_sub1 != null)
			throw ioexception_sub1;
		return method1590(false);
	}

	static Class method1593(String string) {
		Class var_class;
		try {
			var_class = Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
		return var_class;
	}
}
