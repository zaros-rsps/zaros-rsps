package net.zaros.client;

/* Class338_Sub3_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class338_Sub3_Sub1 extends Class338_Sub3 {
	boolean aBoolean6557;
	short aShort6558;
	short aShort6559;
	short aShort6560;
	private static char[] aCharArray6561 = new char[64];
	byte aByte6562;
	static int anInt6563;
	short aShort6564;

	static final void method3476(byte[] is, byte[] is_0_, int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_) {
		if (i_6_ <= -91) {
			int i_7_ = -(i_5_ >> 2);
			i_5_ = -(i_5_ & 0x3);
			for (int i_8_ = -i_1_; i_8_ < 0; i_8_++) {
				for (int i_9_ = i_7_; i_9_ < 0; i_9_++) {
					is[i++] -= is_0_[i_3_++];
					is[i++] -= is_0_[i_3_++];
					is[i++] -= is_0_[i_3_++];
					is[i++] -= is_0_[i_3_++];
				}
				for (int i_10_ = i_5_; i_10_ < 0; i_10_++)
					is[i++] -= is_0_[i_3_++];
				i_3_ += i_2_;
				i += i_4_;
			}
		}
	}

	void method3477(int i) {
		if (i != -1)
			method3477(94);
	}

	final boolean method3465(int i) {
		int i_11_ = aShort6560;
		if (i != 0)
			method3476(null, null, -53, -107, -65, 100, 43, -48, 9);
		for (/**/; i_11_ <= aShort6559; i_11_++) {
			for (int i_12_ = aShort6564; i_12_ <= aShort6558; i_12_++) {
				int i_13_ = (i_11_ - Class296_Sub45_Sub2.anInt6288 + Class379_Sub2.anInt5684);
				if (i_13_ >= 0 && i_13_ < (Class296_Sub15_Sub2.aBooleanArrayArray6006).length) {
					int i_14_ = (-Class296_Sub39_Sub20_Sub2.anInt6728 + i_12_ + Class379_Sub2.anInt5684);
					if (i_14_ >= 0 && i_14_ < (Class296_Sub15_Sub2.aBooleanArrayArray6006).length && (Class296_Sub15_Sub2.aBooleanArrayArray6006[i_13_][i_14_]))
						return true;
				}
			}
		}
		return false;
	}

	final boolean method3470(int i, ha var_ha) {
		if (i != -30488)
			return false;
		return Class69_Sub4.method743(this.method3461(var_ha, i + 30612), aShort6559, true, aShort6558, aByte5203, aShort6560, aShort6564);
	}

	public static void method3478(byte i) {
		if (i < -101)
			aCharArray6561 = null;
	}

	final int method3463(int i, Class296_Sub35[] class296_sub35s) {
		int i_15_ = 0;
		while_92_ : for (int i_16_ = aShort6560; aShort6559 >= i_16_; i_16_++) {
			for (int i_17_ = aShort6564; aShort6558 >= i_17_; i_17_++) {
				long l = Class49.aLongArrayArrayArray462[z][i_16_][i_17_];
				long l_18_ = 0L;
				while_91_ : while (l_18_ <= 48L) {
					int i_19_ = (int) (l >>> (int) l_18_ & 0xffffL);
					if (i_19_ <= 0)
						break;
					Class93 class93 = Class302.aClass93Array2721[i_19_ - 1];
					for (int i_20_ = 0; i_15_ > i_20_; i_20_++) {
						if (class93.aClass296_Sub35_999 == class296_sub35s[i_20_]) {
							l_18_ += 16L;
							continue while_91_;
						}
					}
					class296_sub35s[i_15_++] = class93.aClass296_Sub35_999;
					if (i_15_ == 4)
						break while_92_;
					l_18_ += 16L;
				}
			}
		}
		for (int i_21_ = i_15_; i_21_ < 4; i_21_++)
			class296_sub35s[i_21_] = null;
		if (i != (aByte6562 ^ 0xffffffff)) {
			int i_22_ = -Class296_Sub45_Sub2.anInt6288 + aShort6560;
			int i_23_ = -Class296_Sub39_Sub20_Sub2.anInt6728 + aShort6564;
			int i_24_;
			int i_25_;
			short i_26_;
			short i_27_;
			if (aByte6562 == 1) {
				if (i_22_ < i_23_) {
					i_26_ = aShort6560;
					i_25_ = aShort6564 - 1;
					i_27_ = aShort6564;
					i_24_ = aShort6560 + 1;
				} else {
					i_27_ = aShort6564;
					i_25_ = aShort6564 + 1;
					i_24_ = aShort6560 - 1;
					i_26_ = aShort6560;
				}
			} else if (-i_22_ >= i_23_) {
				i_24_ = aShort6560 + 1;
				i_25_ = aShort6564 + 1;
				i_26_ = aShort6560;
				i_27_ = aShort6564;
			} else {
				i_25_ = aShort6564 - 1;
				i_24_ = aShort6560 - 1;
				i_27_ = aShort6564;
				i_26_ = aShort6560;
			}
			int i_28_ = 0;
			while_94_ : for (/**/; i_28_ < i_15_; i_28_++) {
				long l = Class49.aLongArrayArrayArray462[z][i_26_][i_25_];
				while (l != 0L) {
					Class93 class93 = (Class302.aClass93Array2721[(int) ((l & 0xffffL) + -1L)]);
					l >>>= 16;
					if (class296_sub35s[i_28_] == class93.aClass296_Sub35_999)
						continue while_94_;
				}
				l = Class49.aLongArrayArrayArray462[z][i_24_][i_27_];
				while (l != 0L) {
					Class93 class93 = (Class302.aClass93Array2721[(int) ((l & 0xffffL) - 1L)]);
					l >>>= 16;
					if (class296_sub35s[i_28_] == class93.aClass296_Sub35_999)
						continue while_94_;
				}
				for (int i_29_ = i_28_; i_29_ < i_15_ - 1; i_29_++)
					class296_sub35s[i_29_] = class296_sub35s[i_29_ + 1];
				i_15_--;
			}
		}
		return i_15_;
	}

	Class338_Sub3_Sub1(int i, int i_30_, int i_31_, int i_32_, int i_33_, int i_34_, int i_35_, int i_36_, int i_37_, boolean bool, byte i_38_) {
		aByte6562 = i_38_;
		aShort6559 = (short) i_35_;
		aShort6558 = (short) i_37_;
		z = (byte) i;
		aBoolean6557 = bool;
		aByte5203 = (byte) i_30_;
		tileY = i_33_;
		anInt5213 = i_32_;
		tileX = i_31_;
		aShort6560 = (short) i_34_;
		aShort6564 = (short) i_36_;
	}

	static {
		for (int i = 0; i < 26; i++)
			aCharArray6561[i] = (char) (i + 65);
		for (int i = 26; i < 52; i++)
			aCharArray6561[i] = (char) (i + 97 - 26);
		for (int i = 52; i < 62; i++)
			aCharArray6561[i] = (char) (i + 48 - 52);
		aCharArray6561[62] = '+';
		aCharArray6561[63] = '/';
		anInt6563 = 0;
	}
}
