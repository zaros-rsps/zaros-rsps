package net.zaros.client;
/* Class367 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;

final class Class367 {
	static int npcsCount;
	static byte[][] aByteArrayArray3124;
	static int anInt3125 = -1;
	static int anInt3126;
	static SubInPacket aClass260_3127;

	static final void method3800(int i) {
		if (Class2.anIntArray56 == null || Class259.anIntArray2419 == null) {
			Class259.anIntArray2419 = new int[256];
			Class2.anIntArray56 = new int[256];
			for (int i_0_ = 0; i_0_ < 256; i_0_++) {
				double d = (double) i_0_ / 255.0 * 6.283185307179586;
				Class2.anIntArray56[i_0_] = (int) (Math.sin(d) * 4096.0);
				Class259.anIntArray2419[i_0_] = (int) (Math.cos(d) * 4096.0);
			}
		}
		if (i != 4096)
			anInt3125 = 27;
	}

	static final int method3801(byte i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		int i_6_ = -95 % ((i - 89) / 35);
		if (Class360_Sub2.aSArray5304 == null)
			return 0;
		if (i_5_ < 3) {
			int i_7_ = i_3_ >> 9;
			int i_8_ = i_4_ >> 9;
			if (i_1_ < 0 || i_2_ < 0 || Class198.currentMapSizeX - 1 < i_1_ || Class296_Sub38.currentMapSizeY - 1 < i_2_)
				return 0;
			if (i_7_ < 1 || i_8_ < 1 || i_7_ > Class198.currentMapSizeX - 1 || i_8_ > Class296_Sub38.currentMapSizeY - 1)
				return 0;
			boolean bool = (((Class41_Sub18.aByteArrayArrayArray3786[1][i_3_ >> 9][i_4_ >> 9]) & 0x2) != 0);
			if ((i_3_ & 0x1ff) == 0) {
				boolean bool_9_ = (((Class41_Sub18.aByteArrayArrayArray3786[1][i_7_ - 1][i_4_ >> 9]) & 0x2) != 0);
				boolean bool_10_ = (((Class41_Sub18.aByteArrayArrayArray3786[1][i_7_][i_4_ >> 9]) & 0x2) != 0);
				if (!bool_9_ != !bool_10_)
					bool = ((Class41_Sub18.aByteArrayArrayArray3786[1][i_1_][i_2_]) & 0x2) != 0;
			}
			if ((i_4_ & 0x1ff) == 0) {
				boolean bool_11_ = (((Class41_Sub18.aByteArrayArrayArray3786[1][i_3_ >> 9][i_8_ - 1]) & 0x2) != 0);
				boolean bool_12_ = (((Class41_Sub18.aByteArrayArrayArray3786[1][i_3_ >> 9][i_8_]) & 0x2) != 0);
				if (bool_11_ != bool_12_)
					bool = ((Class41_Sub18.aByteArrayArrayArray3786[1][i_1_][i_2_]) & 0x2) != 0;
			}
			if (bool)
				i_5_++;
		}
		return Class360_Sub2.aSArray5304[i_5_].method3349(0, i_4_, i_3_);
	}

	static final void method3802(int i) {
		if (Animator.aFrame435 == null) {
			int i_13_ = Class241_Sub2_Sub2.anInt5908;
			if (i != -1)
				method3801((byte) 127, -114, 91, 125, -123, -108);
			int i_14_ = CS2Stack.anInt2251;
			int i_15_ = -i_13_ + (StaticMethods.anInt1838 - Class241.anInt2301);
			int i_16_ = -Class384.anInt3254 + Class152.anInt1568 - i_14_;
			do {
				if (i_13_ > 0 || i_15_ > 0 || i_14_ > 0 || i_16_ > 0) {
					try {
						java.awt.Container container;
						if (Class340.aFrame3707 != null)
							container = Class340.aFrame3707;
						else if (CS2Script.anApplet6140 == null)
							container = Class55.anApplet_Sub1_656;
						else
							container = CS2Script.anApplet6140;
						int i_17_ = 0;
						int i_18_ = 0;
						if (container == Class340.aFrame3707) {
							Insets insets = Class340.aFrame3707.getInsets();
							i_17_ = insets.left;
							i_18_ = insets.top;
						}
						Graphics graphics = container.getGraphics();
						graphics.setColor(Color.black);
						if (i_13_ > 0)
							graphics.fillRect(i_17_, i_18_, i_13_, Class152.anInt1568);
						if (i_14_ > 0)
							graphics.fillRect(i_17_, i_18_, StaticMethods.anInt1838, i_14_);
						if (i_15_ > 0)
							graphics.fillRect(-i_15_ + (i_17_ + StaticMethods.anInt1838), i_18_, i_15_, Class152.anInt1568);
						if (i_16_ <= 0)
							break;
						graphics.fillRect(i_17_, -i_16_ + (i_18_ + Class152.anInt1568), StaticMethods.anInt1838, i_16_);
					} catch (Exception exception) {
						/* empty */
					}
					break;
				}
			} while (false);
		}
	}

	static final boolean method3803(int i, int i_19_, int i_20_) {
		if (i_19_ >= -55)
			return false;
		if ((i & 0x100) == 0)
			return false;
		return true;
	}

	public static void method3804(int i) {
		if (i == -30918) {
			aClass260_3127 = null;
			aByteArrayArray3124 = null;
		}
	}

	static {
		npcsCount = 0;
		aClass260_3127 = new SubInPacket(0, 3);
	}
}
