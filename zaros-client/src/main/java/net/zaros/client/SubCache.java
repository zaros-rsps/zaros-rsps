package net.zaros.client;

/* Class301 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class SubCache {
	private Queuable aClass296_Sub39_2705 = new Queuable();
	static OutgoingPacket aClass311_2706 = new OutgoingPacket(80, -1);
	static boolean aBoolean2707 = true;
	static SubInPacket aClass260_2708;
	static boolean aBoolean2709 = true;
	private int maxCapacity;
	private int capacity;
	private HashTable table;
	static Class210_Sub1[] aClass210_Sub1Array2713;
	private Queue references = new Queue();

	public static void method3251(int i) {
		aClass210_Sub1Array2713 = null;
		aClass260_2708 = null;
		aClass311_2706 = null;
		if (i >= -127)
			method3258(null, 92);
	}

	final void remove(long key) {
		Queuable n = (Queuable) table.get(key);
		if (n != null) {
			n.unlink();
			n.queue_unlink();
			capacity++;
		}
	}

	static final void method3253() {
		for (;;) {
			boolean bool = true;
			for (int i = 0; i < StaticMethods.aClass142Array5951.length; i++) {
				if (!StaticMethods.aClass142Array5951[i].method1477()) {
					synchronized (StaticMethods.aClass142Array5951[i]) {
						StaticMethods.aClass142Array5951[i].notify();
					}
					bool = false;
				} else
					aa.aLongArray47[i] = StaticMethods.aClass142Array5951[i].method1478();
			}
			if (bool)
				break;
			try {
				Class106_Sub1.method942(1L, 0);
			} catch (Exception exception) {
				/* empty */
			}
		}
		StaticMethods.aClass142Array5951[StaticMethods.aClass142Array5951.length - 1].method1475();
		Class122.method1037(1);
		for (;;) {
			boolean bool = true;
			for (int i = 0; i < StaticMethods.aClass142Array5951.length - 1; i++) {
				if (!StaticMethods.aClass142Array5951[i].method1477()) {
					synchronized (StaticMethods.aClass142Array5951[i]) {
						StaticMethods.aClass142Array5951[i].notify();
					}
					bool = false;
				}
			}
			if (bool)
				break;
			try {
				Class106_Sub1.method942(1L, 0);
			} catch (Exception exception) {
				/* empty */
			}
		}
		for (int i = 1; i < StaticMethods.aClass142Array5951.length - 2; i++)
			StaticMethods.aClass142Array5951[i].method1475();
		Class122.method1037(2);
		while (!StaticMethods.aClass142Array5951[0].method1477()) {
			synchronized (StaticMethods.aClass142Array5951[0]) {
				StaticMethods.aClass142Array5951[0].notify();
			}
			try {
				Class106_Sub1.method942(1L, 0);
			} catch (Exception exception) {
				/* empty */
			}
		}
		StaticMethods.aClass142Array5951[0].method1475();
	}

	final void put(long l, Queuable v) {
		if (capacity != 0)
			capacity--;
		else {
			Queuable n = references.remove();
			n.unlink();
			n.queue_unlink();
			if (aClass296_Sub39_2705 == n) {
				n = references.remove();
				n.unlink();
				n.queue_unlink();
			}
		}
		table.put(l, v);
		references.insert(v, -2);
	}

	final void clear() {
		references.clear();
		table.clear();
		aClass296_Sub39_2705 = new Queuable();
		capacity = maxCapacity;
	}

	static final void method3256() {
		CS2Stack.aClass264_2249 = CS2Stack.aClass264_2243;
	}

	final Queuable get(long l) {
		Queuable n = (Queuable) table.get(l);
		if (n != null)
			references.insert(n, -2);
		return n;
	}

	static final void method3258(Class338_Sub2 class338_sub2, int i) {
		class338_sub2.aClass338_Sub3_5193 = null;
		if (i != 5362)
			method3253();
		int i_4_ = class338_sub2.aClass338_Sub5Array5194.length;
		for (int i_5_ = 0; i_5_ < i_4_; i_5_++)
			class338_sub2.aClass338_Sub5Array5194[i_5_].aBoolean5224 = false;
		synchronized (Class368.aClass404Array3129) {
			if (Class368.aClass404Array3129.length > i_4_ && Class69_Sub1_Sub1.anIntArray6686[i_4_] < 200) {
				Class368.aClass404Array3129[i_4_].method4158(class338_sub2, 1);
				Class69_Sub1_Sub1.anIntArray6686[i_4_]++;
			}
		}
	}

	SubCache(int i) {
		maxCapacity = i;
		capacity = i;
		int size;
		for (size = 1; i > size + size; size += size) {
			/* empty */
		}
		table = new HashTable(size);
	}

	static {
		aClass260_2708 = new SubInPacket(13, -1);
	}
}
