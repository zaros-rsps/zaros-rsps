package net.zaros.client;

/* Class338_Sub3_Sub1_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Mobile extends Class338_Sub3_Sub1 {
	boolean aBoolean6764;
	int anInt6765;
	int anInt6766;
	int anInt6767;
	int anInt6768;
	int anInt6769;
	int anInt6770;
	private byte aByte6771 = 0;
	static int[] bitMasks = {0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535, 131071, 262143, 524287, 1048575, 2097151, 4194303, 8388607, 16777215, 33554431, 67108863, 134217727, 268435455, 536870911, 1073741823, 2147483647, -1};
	int[] soakTypeQueue;
	boolean aBoolean6774;
	int[] hpBarRatioQueue;
	static byte[][] aByteArrayArray6776;
	Animator aClass44_6777;
	int[] hitTypeQueue;
	int anInt6779;
	int anInt6780;
	int anInt6781;
	private int size;
	boolean aBoolean6783;
	int[] soakDamageQueue;
	int anInt6785;
	static int anInt6786;
	static int anInt6787;
	int interactingWithIndex;
	int[] anIntArray6789;
	int anInt6790;
	int[] hitDamageQueue;
	int[] hitDelayQueue;
	static String[] aStringArray6793;
	int[] anIntArray6794;
	int anInt6795;
	Class79 aClass79_6796;
	int index;
	int anInt6798;
	int anInt6799;
	int anInt6800;
	int anInt6801;
	Animator aClass44_6802;
	int anInt6803;
	Class5 aClass5_6804;
	int anInt6805;
	byte aByte6806;
	int anInt6807;
	byte aByte6808;
	int anInt6809;
	int anInt6810;
	int anInt6811;
	int lastUpdate;
	int anInt6813;
	Class44_Sub1_Sub1[] aClass44_Sub1_Sub1Array6814;
	int anInt6815;
	int anInt6816;
	Class212[] aClass212Array6817;
	byte aByte6818;
	int anInt6819;
	int anInt6820;
	int anInt6821;
	byte aByte6822;
	int[] anIntArray6823;
	private Class5 aClass5_6824;
	private Class5 aClass5_6825;
	int[] waypointQueueX;
	int[] wayPointQueueY;
	Model[] aClass178Array6828;
	int anInt6829;
	byte[] walkingTypes;
	Class338_Sub1 aClass338_Sub1_6831;
	int currentWayPoint;
	int anInt6833;
	boolean aBoolean6834;
	boolean aBoolean6835;
	int anInt6836;

	protected final void finalize() {
		if (aClass338_Sub1_6831 != null)
			aClass338_Sub1_6831.method3439();
	}

	abstract boolean method3494(byte i);

	static final void method3495(int i, int i_0_, int i_1_) {
		if (Class106.aFloat1103 > Class106.aFloat1100) {
			Class106.aFloat1100 += (double) Class106.aFloat1100 / 30.0;
			if (Class106.aFloat1103 < Class106.aFloat1100)
				Class106.aFloat1100 = Class106.aFloat1103;
			Class296_Sub51_Sub25.method3146((byte) -10);
			Class106.anInt1099 = (int) Class106.aFloat1100 >> 1;
			Class106.aByteArrayArrayArray1102 = Class84.method814(Class106.anInt1099, (byte) -54);
		} else if (Class106.aFloat1100 > Class106.aFloat1103) {
			Class106.aFloat1100 -= (double) Class106.aFloat1100 / 30.0;
			if (Class106.aFloat1103 > Class106.aFloat1100)
				Class106.aFloat1100 = Class106.aFloat1103;
			Class296_Sub51_Sub25.method3146((byte) -10);
			Class106.anInt1099 = (int) Class106.aFloat1100 >> 1;
			Class106.aByteArrayArrayArray1102 = Class84.method814(Class106.anInt1099, (byte) -54);
		}
		if ((Class296_Sub51_Sub5.anInt6365 ^ 0xffffffff) != i && BITConfigDefinition.anInt2604 != -1) {
			int i_2_ = Class296_Sub51_Sub5.anInt6365 - Class69.anInt3688;
			if (i_2_ < 2 || i_2_ > 2)
				i_2_ /= 8;
			int i_3_ = BITConfigDefinition.anInt2604 - Class219_Sub1.anInt4569;
			if (i_3_ < 2 || i_3_ > 2)
				i_3_ /= 8;
			Class69.anInt3688 = i_2_ + Class69.anInt3688;
			if (i_2_ == 0 && i_3_ == 0) {
				BITConfigDefinition.anInt2604 = -1;
				Class296_Sub51_Sub5.anInt6365 = -1;
			}
			Class219_Sub1.anInt4569 += i_3_;
			Class296_Sub51_Sub25.method3146((byte) -10);
		}
		if (aa_Sub2.anInt3728 <= 0) {
			Class325.anInt2868 = -1;
			Class41_Sub26.anInt3807 = -1;
		} else {
			Class139.anInt1421--;
			if (Class139.anInt1421 == 0) {
				aa_Sub2.anInt3728--;
				Class139.anInt1421 = 100;
			}
		}
		if (StringNode.aBoolean4622 && Class366_Sub8.aClass155_5409 != null) {
			for (Class296_Sub38 class296_sub38 = ((Class296_Sub38) Class366_Sub8.aClass155_5409.removeFirst((byte) 126)); class296_sub38 != null; class296_sub38 = (Class296_Sub38) Class366_Sub8.aClass155_5409.removeNext(1001)) {
				Class18 class18 = Class106.aClass245_1094.method2179(11702, (class296_sub38.aClass296_Sub53_4892.anInt5046));
				if (class296_sub38.method2773(i_0_, i_1_, -1728)) {
					if (class18.aStringArray224 != null) {
						if (class18.aStringArray224[4] != null)
							StaticMethods.method1008(-124, false, class18.anInt217, -1, -1, class18.aStringArray224[4], 0, (long) (class296_sub38.aClass296_Sub53_4892.anInt5046), 1001, true, (long) (class296_sub38.aClass296_Sub53_4892.anInt5046), class18.aString212, false);
						if (class18.aStringArray224[3] != null)
							StaticMethods.method1008(-15, false, class18.anInt217, -1, -1, class18.aStringArray224[3], 0, (long) (class296_sub38.aClass296_Sub53_4892.anInt5046), 1011, true, (long) (class296_sub38.aClass296_Sub53_4892.anInt5046), class18.aString212, false);
						if (class18.aStringArray224[2] != null)
							StaticMethods.method1008(i + 88, false, class18.anInt217, -1, -1, class18.aStringArray224[2], 0, (long) (class296_sub38.aClass296_Sub53_4892.anInt5046), 1007, true, (long) (class296_sub38.aClass296_Sub53_4892.anInt5046), class18.aString212, false);
						if (class18.aStringArray224[1] != null)
							StaticMethods.method1008(47, false, class18.anInt217, -1, -1, class18.aStringArray224[1], 0, (long) (class296_sub38.aClass296_Sub53_4892.anInt5046), 1010, true, (long) (class296_sub38.aClass296_Sub53_4892.anInt5046), class18.aString212, false);
						if (class18.aStringArray224[0] != null)
							StaticMethods.method1008(-94, false, class18.anInt217, -1, -1, class18.aStringArray224[0], 0, (long) (class296_sub38.aClass296_Sub53_4892.anInt5046), 1008, true, (long) (class296_sub38.aClass296_Sub53_4892.anInt5046), class18.aString212, false);
					}
					if (!class296_sub38.aClass296_Sub53_4892.aBoolean5044) {
						class296_sub38.aClass296_Sub53_4892.aBoolean5044 = true;
						CS2Executor.method1520((Class338_Sub3_Sub2_Sub1.aClass81_6849), (class296_sub38.aClass296_Sub53_4892.anInt5046), class18.anInt217);
					}
					if (class296_sub38.aClass296_Sub53_4892.aBoolean5044)
						CS2Executor.method1520(LookupTable.aClass81_51, (class296_sub38.aClass296_Sub53_4892.anInt5046), class18.anInt217);
				} else if (class296_sub38.aClass296_Sub53_4892.aBoolean5044) {
					class296_sub38.aClass296_Sub53_4892.aBoolean5044 = false;
					CS2Executor.method1520(Class384.aClass81_3253, (class296_sub38.aClass296_Sub53_4892.anInt5046), class18.anInt217);
				}
			}
		}
	}

	static final void method3496(int i, int i_4_, int i_5_) {
		if (i_4_ != 12279)
			bitMasks = null;
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_5_, 17);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.intParam = i;
	}

	abstract Class79 method3497(int i);

	final void method3498(byte i) {
		currentWayPoint = 0;
		if (i <= 110)
			aClass212Array6817 = null;
		anInt6829 = 0;
	}

	final void method3499(int i) {
		Class280 class280 = method3516(false);
		if ((class280.anInt2575 != 0 || anInt6815 != 0)) {
			aClass5_6804.method177((byte) -68);
			int i_7_ = i - aClass5_6804.anInt77 & 0x3fff;
			if (i_7_ > 8192)
				anInt6816 = i_7_ + aClass5_6804.anInt77 - 16384;
			else
				anInt6816 = i_7_ + aClass5_6804.anInt77;
		}
	}

	final void method3500(boolean bool, Model[] class178s, boolean bool_8_, ha var_ha, Class373 class373) {
		if (bool_8_ == true) {
			if (!bool) {
				int i = 0;
				int i_9_ = 0;
				int i_10_ = 0;
				int i_11_ = 0;
				int i_12_ = -1;
				int i_13_ = -1;
				EmissiveTriangle[][] class89s = new EmissiveTriangle[class178s.length][];
				EffectiveVertex[][] class232s = new EffectiveVertex[class178s.length][];
				for (int i_14_ = 0; i_14_ < class178s.length; i_14_++) {
					if (class178s[i_14_] != null) {
						class178s[i_14_].method1718(class373);
						class89s[i_14_] = class178s[i_14_].method1735();
						class232s[i_14_] = class178s[i_14_].method1729();
						if (class232s[i_14_] != null) {
							i_11_++;
							i_10_ += class232s[i_14_].length;
							i_13_ = i_14_;
						}
						if (class89s[i_14_] != null) {
							i_12_ = i_14_;
							i_9_++;
							i += class89s[i_14_].length;
						}
					}
				}
				if ((aClass338_Sub1_6831 == null || aClass338_Sub1_6831.aBoolean5177) && (i_9_ > 0 || i_11_ > 0))
					aClass338_Sub1_6831 = Class338_Sub1.method3442(Class29.anInt307, true);
				if (aClass338_Sub1_6831 != null) {
					Object object = null;
					EmissiveTriangle[] class89s_15_;
					if (i_9_ == 1)
						class89s_15_ = class89s[i_12_];
					else {
						class89s_15_ = new EmissiveTriangle[i];
						int i_16_ = 0;
						for (int i_17_ = 0; class178s.length > i_17_; i_17_++) {
							if (class89s[i_17_] != null) {
								ArrayTools.removeElement(class89s[i_17_], 0, class89s_15_, i_16_, class89s[i_17_].length);
								i_16_ += class89s[i_17_].length;
							}
						}
					}
					Object object_18_ = null;
					EffectiveVertex[] class232s_19_;
					if (i_11_ == 1)
						class232s_19_ = class232s[i_13_];
					else {
						class232s_19_ = new EffectiveVertex[i_10_];
						int i_20_ = 0;
						for (int i_21_ = 0; class178s.length > i_21_; i_21_++) {
							if (class232s[i_21_] != null) {
								ArrayTools.removeElement(class232s[i_21_], 0, class232s_19_, i_20_, class232s[i_21_].length);
								i_20_ += class232s[i_21_].length;
							}
						}
					}
					aClass338_Sub1_6831.method3452(var_ha, (long) Class29.anInt307, class89s_15_, class232s_19_, false);
					aBoolean6835 = true;
				}
			} else if (aClass338_Sub1_6831 != null)
				aClass338_Sub1_6831.method3441((long) Class29.anInt307);
			if (aClass338_Sub1_6831 != null)
				aClass338_Sub1_6831.method3450(z, aShort6560, aShort6559, aShort6564, aShort6558);
		}
	}

	final void method3501(int i) {
		if (i != 16383)
			method3505(80);
		if (aClass79_6796 != null && aClass79_6796.aString886 != null) {
			aClass79_6796.anInt882--;
			if (aClass79_6796.anInt882 == 0)
				aClass79_6796.aString886 = null;
		}
	}

	static final int method3502(int i, int i_22_) {
		if (i == 6406)
			return 1;
		if (i == 6409)
			return 1;
		if (i != 32841) {
			if (i == 6410)
				return 2;
			if (i == 6407)
				return 3;
			if (i == 6408)
				return 4;
		} else
			return 1;
		if (i_22_ != -32842)
			method3502(127, -8);
		throw new IllegalArgumentException("");
	}

	final boolean method3459(int i) {
		if (i != 0)
			return false;
		return false;
	}

	final int method3462(byte i) {
		if (i != 28)
			method3504(null, -37, 57, 1, 67, null, 94);
		return anInt6800;
	}

	final void method3503(boolean bool, Model class178) {
		if (bool)
			method3469(24);
		int i = aClass5_6824.anInt77;
		int i_23_ = aClass5_6825.anInt77;
		if (i != 0 || i_23_ != 0) {
			int i_24_ = class178.fa() / 2;
			class178.H(0, -i_24_, 0);
			class178.VA(i & 0x3fff);
			class178.FA(i_23_ & 0x3fff);
			class178.H(0, i_24_, 0);
		}
	}

	final void method3504(ha var_ha, int i, int i_25_, int i_26_, int i_27_, Class280 class280, int i_28_) {
		if (i_27_ != 28149)
			aClass178Array6828 = null;
		for (int i_29_ = 0; aClass212Array6817.length > i_29_; i_29_++) {
			byte i_30_ = 0;
			if (i_29_ != 0) {
				if (i_29_ == 1)
					i_30_ = (byte) 5;
				else if (i_29_ != 2) {
					if (i_29_ == 3)
						i_30_ = (byte) 7;
				} else
					i_30_ = (byte) 1;
			} else
				i_30_ = (byte) 2;
			Class212 class212 = aClass212Array6817[i_29_];
			if (class212.anInt2105 != -1 && !class212.aClass44_2108.method567(i_27_ ^ 0x6df4)) {
				Graphic class23 = Class157.graphicsLoader.getGraphic(class212.anInt2105);
				boolean bool = class23.aByte260 == 3 && (i != 0 || i_26_ != 0);
				int i_31_ = i_25_;
				if (bool)
					i_31_ |= 0x7;
				else {
					if (class212.anInt2104 != 0)
						i_31_ |= 0x5;
					if (class212.anInt2106 != 0)
						i_31_ |= 0x2;
					if (class212.anInt2107 >= 0)
						i_31_ |= 0x7;
				}
				Model class178 = (aClass178Array6828[i_29_ + 1] = class23.method291(var_ha, (byte) -83, i_30_, class212.aClass44_2108, i_31_));
				if (class178 != null) {
					if (class212.anInt2107 < 0 || class280.anIntArrayArray2558 == null || (class280.anIntArrayArray2558[class212.anInt2107] == null)) {
						if (class212.anInt2104 != 0)
							class178.a(class212.anInt2104 * 2048);
					} else {
						int i_32_ = 0;
						int i_33_ = 0;
						int i_34_ = 0;
						if (class280.anIntArrayArray2558 != null && (class280.anIntArrayArray2558[class212.anInt2107]) != null) {
							i_33_ += (class280.anIntArrayArray2558[class212.anInt2107][1]);
							i_32_ += (class280.anIntArrayArray2558[class212.anInt2107][0]);
							i_34_ += (class280.anIntArrayArray2558[class212.anInt2107][2]);
						}
						if (class280.anIntArrayArray2561 != null && (class280.anIntArrayArray2561[class212.anInt2107]) != null) {
							i_33_ += (class280.anIntArrayArray2561[class212.anInt2107][1]);
							i_34_ += (class280.anIntArrayArray2561[class212.anInt2107][2]);
							i_32_ += (class280.anIntArrayArray2561[class212.anInt2107][0]);
						}
						if (i_34_ != 0 || i_32_ != 0) {
							int i_35_ = i_28_;
							if (anIntArray6823 != null && anIntArray6823[class212.anInt2107] != -1)
								i_35_ = anIntArray6823[class212.anInt2107];
							int i_36_ = (i_35_ + class212.anInt2104 * 2048 - i_28_ & 0x3fff);
							if (i_36_ != 0)
								class178.a(i_36_);
							int i_37_ = Class296_Sub4.anIntArray4598[i_36_];
							int i_38_ = Class296_Sub4.anIntArray4618[i_36_];
							int i_39_ = i_38_ * i_32_ + i_34_ * i_37_ >> 14;
							i_34_ = -(i_32_ * i_37_) + i_34_ * i_38_ >> 14;
							i_32_ = i_39_;
						}
						class178.H(i_32_, i_33_, i_34_);
					}
					if (class212.anInt2106 != 0)
						class178.H(0, -class212.anInt2106 << 2, 0);
					if (bool) {
						if (anInt6801 != 0)
							class178.FA(anInt6801);
						if (anInt6769 != 0)
							class178.VA(anInt6769);
						if (anInt6795 != 0)
							class178.H(0, anInt6795, 0);
					}
				}
			} else
				aClass178Array6828[i_29_ + 1] = null;
		}
		anInt6786++;
	}

	abstract int method3505(int i);

	public Mobile() {
		this(10);
	}

	final int method3466(byte i) {
		if (i <= 77)
			method3513((byte) -99);
		if (anInt6790 == -32768)
			return 0;
		return anInt6790;
	}

	final void applyHit(int hitType, int soakType, int damage, int soakDamage, int delay, int hpBarRatio, int i_45_) {
			boolean bool = true;
			boolean bool_47_ = true;
			for (int i_48_ = 0; NPCDefinition.aClass268_1478.anInt2491 > i_48_; i_48_++) {
				if (hitDelayQueue[i_48_] > i_45_)
					bool = false;
				else
					bool_47_ = false;
			}
			int queueIndex = -1;
			int i_50_ = -1;
			int i_51_ = 0;
			if (hitType >= 0) {
				Class330 class330 = ParamType.aClass164_3248.method1634(hitType, 105);
				i_50_ = class330.anInt2925;
				i_51_ = class330.anInt2919;
			}
			if (bool_47_) {
				if (i_50_ == -1)
					return;
				queueIndex = 0;
				int i_52_ = 0;
				if (i_50_ == 0)
					i_52_ = hitDelayQueue[0];
				else if (i_50_ == 1)
					i_52_ = hitDamageQueue[0];
				for (int i_53_ = 1; NPCDefinition.aClass268_1478.anInt2491 > i_53_; i_53_++) {
					if (i_50_ != 0) {
						if (i_50_ == 1 && hitDamageQueue[i_53_] < i_52_) {
							queueIndex = i_53_;
							i_52_ = hitDamageQueue[i_53_];
						}
					} else if (i_52_ > hitDelayQueue[i_53_]) {
						i_52_ = hitDelayQueue[i_53_];
						queueIndex = i_53_;
					}
				}
				if (i_50_ == 1 && i_52_ >= damage)
					return;
			} else {
				if (bool)
					aByte6771 = (byte) 0;
				for (int i_54_ = 0; NPCDefinition.aClass268_1478.anInt2491 > i_54_; i_54_++) {
					int i_55_ = aByte6771;
					aByte6771 = (byte) ((aByte6771 + 1) % NPCDefinition.aClass268_1478.anInt2491);
					if (hitDelayQueue[i_55_] <= i_45_) {
						queueIndex = i_55_;
						break;
					}
				}
			}
			if (queueIndex >= 0) {
				hitTypeQueue[queueIndex] = hitType;
				hitDamageQueue[queueIndex] = damage;
				soakTypeQueue[queueIndex] = soakType;
				soakDamageQueue[queueIndex] = soakDamage;
				hitDelayQueue[queueIndex] = i_45_ - (-i_51_ - delay);
				hpBarRatioQueue[queueIndex] = hpBarRatio;
			}
	}

	final void method3507(int i, String string, int i_56_, int i_57_, int i_58_) {
		if (aClass79_6796 == null)
			aClass79_6796 = new Class79();
		if (i_56_ > -86)
			method3502(105, 88);
		aClass79_6796.anInt882 = aClass79_6796.anInt883 = i_58_;
		aClass79_6796.aString886 = string;
		aClass79_6796.anInt884 = i_57_;
		aClass79_6796.anInt887 = i;
	}

	final boolean method3469(int i) {
		if (i < 82)
			anInt6779 = -89;
		return aBoolean6834;
	}

	final int method3508(byte i) {
		Class280 class280 = method3516(false);
		if (i != 65)
			getRenderEmote();
		int i_59_ = aClass5_6804.anInt77;
		boolean bool;
		if (class280.anInt2575 == 0)
			bool = aClass5_6804.method179((byte) 73, anInt6815, anInt6816, anInt6815);
		else
			bool = aClass5_6804.method179((byte) 96, class280.anInt2579, anInt6816, class280.anInt2575);
		int i_60_ = aClass5_6804.anInt77 - i_59_;
		if (i_60_ == 0) {
			anInt6810 = 0;
			aClass5_6804.method178(2301, anInt6816);
		} else
			anInt6810++;
		if (bool) {
			if (class280.anInt2595 != 0) {
				if (i_60_ > 0)
					aClass5_6824.method179((byte) -66, class280.anInt2576, class280.anInt2567, class280.anInt2595);
				else
					aClass5_6824.method179((byte) -89, class280.anInt2576, -class280.anInt2567, class280.anInt2595);
			}
			if (class280.anInt2593 != 0)
				aClass5_6825.method179((byte) 92, class280.anInt2588, class280.anInt2587, class280.anInt2593);
		} else {
			if (class280.anInt2595 != 0)
				aClass5_6824.method179((byte) -94, class280.anInt2576, 0, class280.anInt2595);
			else
				aClass5_6824.method178(2301, 0);
			if (class280.anInt2593 == 0)
				aClass5_6825.method178(i ^ 0x8bc, 0);
			else
				aClass5_6825.method179((byte) 71, class280.anInt2588, 0, class280.anInt2593);
		}
		return i_60_;
	}

	public static void method3509(int i) {
		int i_61_ = 85 % ((i - 24) / 38);
		aByteArrayArray6776 = null;
		bitMasks = null;
		aStringArray6793 = null;
	}

	final void setSize(int size) {
		this.size = size;
	}

	Mobile(int i) {
		super(0, 0, 0, 0, 0, 0, 0, 0, 0, false, (byte) 0);
		anInt6766 = 0;
		aBoolean6764 = false;
		anInt6770 = 0;
		size = 1;
		interactingWithIndex = -1;
		hpBarRatioQueue = new int[NPCDefinition.aClass268_1478.anInt2491];
		anIntArray6789 = null;
		hitTypeQueue = new int[NPCDefinition.aClass268_1478.anInt2491];
		soakDamageQueue = new int[NPCDefinition.aClass268_1478.anInt2491];
		hitDelayQueue = new int[NPCDefinition.aClass268_1478.anInt2491];
		anInt6767 = 0;
		aBoolean6774 = true;
		anInt6779 = -1000;
		anInt6798 = -1;
		anInt6799 = -1000;
		anInt6800 = 0;
		hitDamageQueue = new int[NPCDefinition.aClass268_1478.anInt2491];
		anInt6790 = -32768;
		aBoolean6783 = false;
		soakTypeQueue = new int[NPCDefinition.aClass268_1478.anInt2491];
		aClass44_6777 = new Class44_Sub1(this, false);
		aClass44_6802 = new Class44_Sub1(this, false);
		aByte6806 = (byte) 0;
		lastUpdate = 0;
		anInt6815 = 256;
		anInt6820 = -1;
		anInt6803 = -1;
		anInt6810 = 0;
		aClass5_6804 = new Class5();
		aClass5_6824 = new Class5();
		aClass5_6825 = new Class5();
		anInt6829 = 0;
		aBoolean6835 = false;
		anInt6833 = 0;
		currentWayPoint = 0;
		aBoolean6834 = false;
		anInt6836 = 0;
		waypointQueueX = new int[i];
		aClass212Array6817 = new Class212[4];
		walkingTypes = new byte[i];
		wayPointQueueY = new int[i];
		aClass178Array6828 = new Model[5];
		for (int i_64_ = 0; i_64_ < 4; i_64_++)
			aClass212Array6817[i_64_] = new Class212(this);
		aClass44_Sub1_Sub1Array6814 = (new Class44_Sub1_Sub1[Class338_Sub3_Sub3_Sub1.equipmentData.colourData.length]);
	}

	int getSize() {
		return size;
	}

	final void method3512(byte i, int[] is, int[] is_65_) {
		if (anIntArray6794 == null && is != null)
			anIntArray6794 = new int[(Class338_Sub3_Sub3_Sub1.equipmentData.colourData).length];
		else if (is == null) {
			anIntArray6794 = null;
			return;
		}
		for (int i_66_ = 0; anIntArray6794.length > i_66_; i_66_++)
			anIntArray6794[i_66_] = -1;
		if (i > -118)
			getRenderEmote();
		for (int i_67_ = 0; is.length > i_67_; i_67_++) {
			int i_68_ = is_65_[i_67_];
			int i_69_ = 0;
			while (i_69_ < anIntArray6794.length) {
				if ((i_68_ & 0x1) != 0)
					anIntArray6794[i_69_] = is[i_67_];
				i_69_++;
				i_68_ >>= 1;
			}
		}
	}

	int method3513(byte i) {
		Class280 class280 = method3516(false);
		if (i > -25)
			playGraphic(-100, 5, -73, -16, false, 120, 87);
		int i_70_;
		if (class280.anInt2578 != -1)
			i_70_ = class280.anInt2578;
		else if (anInt6790 != -32768)
			i_70_ = -anInt6790;
		else
			i_70_ = 200;
		Class247 class247 = (Class338_Sub2.aClass247ArrayArrayArray5195[z][tileX >> Class313.anInt2779][tileY >> Class313.anInt2779]);
		if (class247 != null && class247.aClass338_Sub3_Sub5_2346 != null)
			return i_70_ + class247.aClass338_Sub3_Sub5_2346.aShort6573;
		return i_70_;
	}

	final void method3477(int i) {
		int i_71_ = (i + size << 8) + 240;
		aShort6559 = (short) (tileX + i_71_ >> 9);
		aShort6558 = (short) (i_71_ + tileY >> 9);
		aShort6564 = (short) (-i_71_ + tileY >> 9);
		aShort6560 = (short) (-i_71_ + tileX >> 9);
	}

	abstract int getRenderEmote();

	final boolean method3515(int i, int i_72_, int i_73_) {
		if (anIntArray6823 == null) {
			if (i_72_ == -1)
				return true;
			anIntArray6823 = new int[(Class338_Sub3_Sub3_Sub1.equipmentData.colourData).length];
			for (int i_74_ = 0; i_74_ < (Class338_Sub3_Sub3_Sub1.equipmentData.colourData).length; i_74_++)
				anIntArray6823[i_74_] = -1;
		}
		Class280 class280 = method3516(false);
		int i_75_ = 256;
		if (class280.anIntArray2573 != null && class280.anIntArray2573[i_73_] > 0)
			i_75_ = class280.anIntArray2573[i_73_];
		if (i > -40)
			waypointQueueX = null;
		if (i_72_ == -1) {
			if (anIntArray6823[i_73_] == -1)
				return true;
			int i_76_ = aClass5_6804.method175((byte) -81);
			int i_77_ = anIntArray6823[i_73_];
			int i_78_ = -i_77_ + i_76_;
			if (-i_75_ <= i_78_ && i_75_ >= i_78_) {
				anIntArray6823[i_73_] = -1;
				for (int i_79_ = 0; ((Class338_Sub3_Sub3_Sub1.equipmentData.colourData).length > i_79_); i_79_++) {
					if (anIntArray6823[i_79_] != -1)
						return true;
				}
				anIntArray6823 = null;
				return true;
			}
			if (i_78_ > 0 && i_78_ <= 8192 || i_78_ <= -8192)
				anIntArray6823[i_73_] = i_75_ + i_77_ & 16383;
			else
				anIntArray6823[i_73_] = i_77_ - i_75_ & 16383;
			return false;
		}
		if (anIntArray6823[i_73_] == -1)
			anIntArray6823[i_73_] = aClass5_6804.method175((byte) -81);
		int i_80_ = anIntArray6823[i_73_];
		int i_81_ = -i_80_ + i_72_;
		if (-i_75_ <= i_81_ && i_81_ <= i_75_) {
			anIntArray6823[i_73_] = i_72_;
			return true;
		}
		if (i_81_ > 0 && i_81_ <= 8192 || i_81_ <= -8192)
			anIntArray6823[i_73_] = i_80_ + i_75_ & 16383;
		else
			anIntArray6823[i_73_] = -i_75_ + i_80_ & 16383;
		return false;
	}

	final Class280 method3516(boolean bool) {
		if (bool)
			method3495(102, 126, -113);
		int i = getRenderEmote();
		if (i != -1)
			return Class41_Sub10.aClass62_3768.method700(i, 0);
		return Class122_Sub1.aClass280_5658;
	}

	final void method3517(boolean bool, boolean bool_82_, int i) {
		Class280 class280 = method3516(!bool_82_);
		if (bool || class280.anInt2575 != 0 || anInt6815 != 0) {
			if (bool_82_ != true)
				anInt6803 = 81;
			anInt6816 = i & 0x3fff;
			aClass5_6804.method178(2301, anInt6816);
		}
	}

	final void method3518(int i, boolean bool, int i_83_, int i_84_, int i_85_, int i_86_) {
		int i_87_ = aShort6559 + aShort6560 >> 1;
		int i_88_ = aShort6564 + aShort6558 >> 1;
		int i_89_ = Class296_Sub4.anIntArray4598[i_86_];
		int i_90_ = Class296_Sub4.anIntArray4618[i_86_];
		int i_91_ = -i_85_ / 2;
		int i_92_ = -i_83_ / 2;
		int i_93_ = i_91_ * i_90_ + i_92_ * i_89_ >> 14;
		int i_94_ = i_92_ * i_90_ - i_89_ * i_91_ >> 14;
		int i_95_ = Class367.method3801((byte) 127, i_87_, i_88_, tileX + i_93_, tileY + i_94_, z);
		int i_96_ = i_85_ / 2;
		int i_97_ = -i_83_ / 2;
		int i_98_ = i_97_ * i_89_ + i_90_ * i_96_ >> 14;
		int i_99_ = i_90_ * i_97_ - i_89_ * i_96_ >> 14;
		int i_100_ = Class367.method3801((byte) 127, i_87_, i_88_, i_98_ + tileX, tileY + i_99_, z);
		int i_101_ = -i_85_ / 2;
		int i_102_ = i_83_ / 2;
		int i_103_ = i_89_ * i_102_ + i_101_ * i_90_ >> 14;
		int i_104_ = i_102_ * i_90_ - i_89_ * i_101_ >> 14;
		int i_105_ = Class367.method3801((byte) -113, i_87_, i_88_, tileX + i_103_, tileY + i_104_, z);
		int i_106_ = i_85_ / 2;
		int i_107_ = i_83_ / 2;
		int i_108_ = i_106_ * i_90_ + i_89_ * i_107_ >> 14;
		int i_109_ = i_90_ * i_107_ - i_106_ * i_89_ >> 14;
		if (bool == true) {
			int i_110_ = Class367.method3801((byte) -24, i_87_, i_88_, i_108_ + tileX, i_109_ + tileY, z);
			int i_111_ = i_95_ >= i_100_ ? i_100_ : i_95_;
			int i_112_ = i_110_ > i_105_ ? i_105_ : i_110_;
			int i_113_ = i_100_ >= i_110_ ? i_110_ : i_100_;
			int i_114_ = i_105_ > i_95_ ? i_95_ : i_105_;
			anInt6801 = (int) (Math.atan2((double) (-i_112_ + i_111_), (double) i_83_) * 2607.5945876176133) & 0x3fff;
			anInt6769 = (int) (Math.atan2((double) (i_114_ - i_113_), (double) i_85_) * 2607.5945876176133) & 0x3fff;
			if (anInt6801 != 0 && i_84_ != 0) {
				int i_115_ = -i_84_ + 16384;
				if (anInt6801 > 8192) {
					if (i_115_ > anInt6801)
						anInt6801 = i_115_;
				} else if (i_84_ < anInt6801)
					anInt6801 = i_84_;
			}
			anInt6795 = i_95_ + i_110_;
			if (anInt6769 != 0 && i != 0) {
				int i_116_ = -i + 16384;
				if (anInt6769 > 8192) {
					if (i_116_ > anInt6769)
						anInt6769 = i_116_;
				} else if (anInt6769 > i)
					anInt6769 = i;
			}
			if (anInt6795 > i_100_ + i_105_)
				anInt6795 = i_100_ + i_105_;
			anInt6795 = (anInt6795 >> 1) - anInt5213;
		}
	}

	final void playGraphic(int i, int i_117_, int i_118_, int i_119_, boolean bool, int i_120_, int i_121_) {
		Class212 class212 = aClass212Array6817[i_117_];
		int i_122_ = class212.anInt2105;
		if (i_121_ != -1 && i_122_ != -1) {
			if (i_122_ == i_121_) {
				Graphic class23 = Class157.graphicsLoader.getGraphic(i_121_);
				if (class23.aBoolean267 && class23.anInt264 != -1) {
					Animation class43 = Class296_Sub51_Sub13.animationsLoader.getAnimation(class23.anInt264, (byte) -124);
					int i_123_ = class43.anInt410;
					if (i_123_ != 0) {
						if (i_123_ == 2) {
							class212.aClass44_2108.method554((byte) 109);
							return;
						}
					} else
						return;
				}
			} else {
				Graphic class23 = Class157.graphicsLoader.getGraphic(i_121_);
				Graphic class23_124_ = Class157.graphicsLoader.getGraphic(i_122_);
				if (class23.anInt264 != -1 && class23_124_.anInt264 != -1) {
					Animation class43 = Class296_Sub51_Sub13.animationsLoader.getAnimation(class23.anInt264, (byte) -124);
					Animation class43_125_ = Class296_Sub51_Sub13.animationsLoader.getAnimation(class23_124_.anInt264, (byte) -124);
					if (class43_125_.anInt408 > class43.anInt408)
						return;
				}
			}
		}
		int i_126_ = 0;
		if (i_121_ != -1 && !(Class157.graphicsLoader.getGraphic(i_121_).aBoolean267))
			i_126_ = 2;
		class212.anInt2104 = i_118_;
		class212.anInt2107 = i_119_;
		class212.anInt2105 = i_121_;
		class212.anInt2106 = i >> 16;
		if (i_121_ != -1 && bool)
			i_126_ = 1;
		class212.aClass44_2108.method548(i & i_120_, i_126_, (i_121_ != -1 ? (Class157.graphicsLoader.getGraphic(i_121_).anInt264) : -1), false, 21847);
	}
}
