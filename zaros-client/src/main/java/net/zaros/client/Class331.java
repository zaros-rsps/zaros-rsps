package net.zaros.client;

import net.zaros.client.configs.objtype.ObjTypeList;

/* Class331 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class331 {
	int anInt2933;
	int anInt2934;
	int anInt2935 = 128;
	int anInt2936 = 128;
	int anInt2937;
	int anInt2938;

	final Class331 method3406(int i) {
		int i_0_ = -33 % ((-42 - i) / 63);
		return new Class331(anInt2938, anInt2935, anInt2936, anInt2934, anInt2937, anInt2933);
	}

	static final void method3407(int i, aa var_aa, byte i_1_, int i_2_, int i_3_, int i_4_, InterfaceComponent class51, String string, int i_5_, int i_6_, Class92 class92, Class55 class55) {
		int i_7_;
		if (Class361.anInt3103 != 4)
			i_7_ = ((int) Class41_Sub26.aFloat3806 + StaticMethods.anInt6075) & 0x3fff;
		else
			i_7_ = (int) Class41_Sub26.aFloat3806 & 0x3fff;
		int i_8_ = Math.max(class51.anInt578 / 2, class51.anInt623 / 2) + 10;
		int i_9_ = i_3_ * i_3_ + i_2_ * i_2_;
		if (i_9_ <= i_8_ * i_8_) {
			int i_10_ = Class296_Sub4.anIntArray4598[i_7_];
			int i_11_ = Class296_Sub4.anIntArray4618[i_7_];
			if (Class361.anInt3103 != 4) {
				i_10_ = i_10_ * 256 / (ObjTypeList.anInt1320 + 256);
				i_11_ = i_11_ * 256 / (ObjTypeList.anInt1320 + 256);
			}
			int i_12_ = i_3_ * i_10_ + i_11_ * i_2_ >> 14;
			int i_13_ = -(i_10_ * i_2_) + i_3_ * i_11_ >> 14;
			int i_14_ = class92.method845((byte) 102, null, string, 100);
			int i_15_ = class92.method850(string, 100, 0, -23919, null);
			i_12_ -= i_14_ / 2;
			if (i_1_ >= -48)
				method3412(null, false);
			if (i_12_ >= -class51.anInt578 && i_12_ <= class51.anInt578 && -class51.anInt623 <= i_13_ && i_13_ <= class51.anInt623)
				class55.a((byte) -101, null, 0, var_aa, 0, i_14_, -i_15_ + class51.anInt623 / 2 + (i_4_ + (-i_13_ - i_5_)), i_12_ - (-i - class51.anInt578 / 2), string, 1, i_6_, 50, i, i_4_, null, 0);
		}
	}

	final void method3408(Class331 class331_16_, int i) {
		anInt2934 = class331_16_.anInt2934;
		anInt2937 = class331_16_.anInt2937;
		anInt2933 = class331_16_.anInt2933;
		anInt2935 = class331_16_.anInt2935;
		if (i != 16383)
			method3411(null, -64);
		anInt2938 = class331_16_.anInt2938;
		anInt2936 = class331_16_.anInt2936;
	}

	static final void method3409(int i, aa var_aa, InterfaceComponent class51, Sprite class397, int i_17_, int i_18_, int i_19_, boolean bool) {
		if (class397 != null) {
			int i_20_;
			if (Class361.anInt3103 != 4)
				i_20_ = ((int) Class41_Sub26.aFloat3806 + StaticMethods.anInt6075) & 0x3fff;
			else
				i_20_ = (int) Class41_Sub26.aFloat3806 & 0x3fff;
			int i_21_ = Math.max(class51.anInt578 / 2, class51.anInt623 / 2) + 10;
			int i_22_ = i_17_ * i_17_ + i_18_ * i_18_;
			if (i_22_ <= i_21_ * i_21_) {
				int i_23_ = Class296_Sub4.anIntArray4598[i_20_];
				int i_24_ = Class296_Sub4.anIntArray4618[i_20_];
				if (!bool) {
					if (Class361.anInt3103 != 4) {
						i_24_ = i_24_ * 256 / (ObjTypeList.anInt1320 + 256);
						i_23_ = i_23_ * 256 / (ObjTypeList.anInt1320 + 256);
					}
					int i_25_ = i_18_ * i_23_ + i_17_ * i_24_ >> 14;
					int i_26_ = i_24_ * i_18_ - i_17_ * i_23_ >> 14;
					class397.method4093((i_19_ - (-(class51.anInt578 / 2) - i_25_) - class397.method4099() / 2), (-i_26_ + i + class51.anInt623 / 2 - class397.method4088() / 2), var_aa, i_19_, i);
				}
			}
		}
	}

	static final Class379_Sub3 method3410(int i, Packet class296_sub17) {
		int i_27_ = -47 / ((i + 4) / 37);
		Class379 class379 = Class296_Sub39_Sub1.method2784(class296_sub17, 0);
		int i_28_ = class296_sub17.g4();
		int i_29_ = class296_sub17.g4();
		int i_30_ = class296_sub17.g2();
		return new Class379_Sub3(class379.aClass252_3616, class379.aClass357_3621, class379.anInt3623, class379.anInt3615, class379.anInt3613, class379.anInt3626, class379.anInt3619, class379.anInt3620, class379.anInt3617, i_28_, i_29_, i_30_);
	}

	static final int method3411(String string, int i) {
		if (i != 2)
			return -78;
		return string.length() + 2;
	}

	static final short[] method3412(short[] is, boolean bool) {
		if (is == null)
			return null;
		short[] is_31_ = new short[is.length];
		if (bool != true)
			method3410(42, null);
		Class291.copy(is, 0, is_31_, 0, is.length);
		return is_31_;
	}

	Class331(int i) {
		anInt2938 = i;
	}

	private Class331(int i, int i_32_, int i_33_, int i_34_, int i_35_, int i_36_) {
		anInt2935 = i_32_;
		anInt2937 = i_35_;
		anInt2936 = i_33_;
		anInt2933 = i_36_;
		anInt2938 = i;
		anInt2934 = i_34_;
	}
}
