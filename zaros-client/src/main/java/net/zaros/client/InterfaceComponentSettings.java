package net.zaros.client;
/* Class296_Sub31 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class InterfaceComponentSettings extends Node {
	static OutgoingPacket aClass311_4823 = new OutgoingPacket(13, -1);
	int originalHash;
	static int[] anIntArray4825;
	static int anInt4826 = 0;
	int settingsHash;

	final boolean method2702(boolean bool) {
		if (bool != true)
			return false;
		if ((settingsHash >> 21 & 0x1) == 0)
			return false;
		return true;
	}

	public static void method2703(int i) {
		anIntArray4825 = null;
		if (i != 21666)
			aClass311_4823 = null;
		aClass311_4823 = null;
	}

	static final void method2704(String string, boolean bool, String string_0_) {
		if (string.length() <= 320 && Class338_Sub3_Sub1_Sub1.method3481(-38)) {
			Class296_Sub45_Sub2.aClass204_6276.method1966(320);
			if (bool != true)
				aClass311_4823 = null;
			Class181.method1817(false);
			Class379.aString3625 = string_0_;
			Class286.aString2643 = string;
			Class41_Sub8.method422(1, 5);
		}
	}

	final int method2705(int i) {
		if (i != 7)
			method2702(true);
		return settingsHash >> 18 & 0x7;
	}

	final boolean method2706(byte i) {
		if (i >= -84)
			return true;
		if ((settingsHash & 0x61ed28) >> 22 == 0)
			return false;
		return true;
	}

	final int method2707(int i) {
		if (i >= -45)
			settingsHash = -113;
		return Class296_Sub51_Sub23.method3140((byte) 75, settingsHash);
	}

	final boolean method2708(int i) {
		if (i != 5)
			return true;
		if ((settingsHash & 0x1) == 0)
			return false;
		return true;
	}

	InterfaceComponentSettings(int i, int i_1_) {
		settingsHash = i;
		originalHash = i_1_;
	}

	final boolean method2709(int i, byte i_2_) {
		if (i_2_ != 107)
			return true;
		if ((settingsHash >> i + 1 & 0x1) == 0)
			return false;
		return true;
	}
}
