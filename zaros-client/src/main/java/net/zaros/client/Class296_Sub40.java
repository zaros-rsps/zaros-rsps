package net.zaros.client;

/* Class296_Sub40 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub40 extends Node {
	String aString4907;
	int anInt4908;
	static IncomingPacket aClass231_4909 = new IncomingPacket(85, 0);
	static int[][] anIntArrayArray4910;
	static boolean somethingWithIgnore0 = false;
	static int currentMapLoadType;
	static int anInt4913;
	static int anInt4914;

	public static void method2911(int i) {
		if (i != 4)
			anIntArrayArray4910 = null;
		aClass231_4909 = null;
		anIntArrayArray4910 = null;
	}

	static final boolean method2912(int i, int i_0_, int i_1_, int i_2_, int i_3_) {
		if ((Class41_Sub18.aByteArrayArrayArray3786[0][i_2_][i_0_] & 0x2) != 0)
			return true;
		if (i != 23266)
			method2912(36, -10, 119, 121, -35);
		if ((Class41_Sub18.aByteArrayArrayArray3786[i_3_][i_2_][i_0_] & 0x10) != 0)
			return false;
		if (Class338_Sub8_Sub1.method3606(i_0_, i_3_, i_2_, (byte) 125) == i_1_)
			return true;
		return false;
	}

	static final char method2913(char c, boolean bool) {
		try {
			if (bool)
				anInt4913 = 110;
			char c_4_ = c;
			while_162_ : do {
				while_161_ : do {
					while_160_ : do {
						while_159_ : do {
							while_158_ : do {
								while_157_ : do {
									while_156_ : do {
										while_155_ : do {
											while_154_ : do {
												do {
													if (c_4_ != ' ' && c_4_ != '\u00a0' && c_4_ != '_' && c_4_ != '-') {
														if (c_4_ != '[' && c_4_ != ']' && c_4_ != '#') {
															if ((c_4_ != '\u00e0') && (c_4_ != '\u00e1') && (c_4_ != '\u00e2') && (c_4_ != '\u00e4') && (c_4_ != '\u00e3') && (c_4_ != '\u00c0') && (c_4_ != '\u00c1') && (c_4_ != '\u00c2') && (c_4_ != '\u00c4') && (c_4_ != '\u00c3')) {
																if ((c_4_ != '\u00e8') && (c_4_ != '\u00e9') && (c_4_ != '\u00ea') && (c_4_ != '\u00eb') && (c_4_ != '\u00c8') && (c_4_ != '\u00c9') && (c_4_ != '\u00ca') && (c_4_ != '\u00cb')) {
																	if ((c_4_ != '\u00ed') && (c_4_ != '\u00ee') && (c_4_ != '\u00ef') && (c_4_ != '\u00cd') && (c_4_ != '\u00ce') && (c_4_ != '\u00cf')) {
																		if ((c_4_ != '\u00f2') && c_4_ != '\u00f3' && c_4_ != '\u00f4' && c_4_ != '\u00f6' && c_4_ != '\u00f5' && c_4_ != '\u00d2' && c_4_ != '\u00d3' && c_4_ != '\u00d4' && c_4_ != '\u00d6' && c_4_ != '\u00d5') {
																			if (c_4_ != '\u00f9' && c_4_ != '\u00fa' && c_4_ != '\u00fb' && c_4_ != '\u00fc' && c_4_ != '\u00d9' && c_4_ != '\u00da' && c_4_ != '\u00db' && c_4_ != '\u00dc') {
																				if (c_4_ != '\u00e7' && c_4_ != '\u00c7') {
																					if (c_4_ != '\u00ff' && c_4_ != '\u0178') {
																						if (c_4_ != '\u00f1' && c_4_ != '\u00d1') {
																							if (c_4_ == '\u00df')
																								break while_161_;
																							break while_162_;
																						}
																					} else
																						break while_159_;
																					break while_160_;
																				}
																			} else
																				break while_157_;
																			break while_158_;
																		}
																	} else
																		break while_155_;
																	break while_156_;
																}
															} else
																break;
															break while_154_;
														}
													} else
														return '_';
													return c;
												} while (false);
												return 'a';
											} while (false);
											return 'e';
										} while (false);
										return 'i';
									} while (false);
									return 'o';
								} while (false);
								return 'u';
							} while (false);
							return 'c';
						} while (false);
						return 'y';
					} while (false);
					return 'n';
				} while (false);
				return 'b';
			} while (false);
			return Character.toLowerCase(c);
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "pfa.B(" + c + ',' + bool + ')');
		}
	}

	public Class296_Sub40() {
		/* empty */
	}

	Class296_Sub40(String string, int i) {
		aString4907 = string;
		anInt4908 = i;
	}

	static {
		anIntArrayArray4910 = new int[][]{{0, 2}, {0, 2}, {0, 0, 2}, {2, 0, 0}, {0, 2, 0}, {0, 0, 2}, {0, 5, 1, 4}, {0, 4, 4, 4}, {4, 4, 4, 0}, {6, 6, 6, 2, 2, 2}, {2, 2, 2, 6, 6, 6}, {0, 11, 6, 6, 6, 4}, {0, 2}, {0, 4, 4, 4}, {0, 4, 4, 4}};
		currentMapLoadType = 0;
		anInt4913 = 5000;
		anInt4914 = 0;
	}
}
