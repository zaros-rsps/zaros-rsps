package net.zaros.client;
import jaggl.OpenGL;

final class Class341 {
	static boolean aBoolean2977;
	static Interface1[] anInterface1Array2978 = new Interface1[128];
	private int anInt2979;

	final void method3622(int i) {
		if (i >= -117)
			anInterface1Array2978 = null;
		OpenGL.glEndList();
	}

	static final boolean method3623(int i, int i_0_, int i_1_, int i_2_) {
		Class30.aClass373_317.method3903(i_0_, i, i_2_, Class210_Sub1.anIntArray4539);
		int i_3_ = Class210_Sub1.anIntArray4539[2];
		if (i_3_ < 50)
			return false;
		Class210_Sub1.anIntArray4539[2] = i_3_;
		Class210_Sub1.anIntArray4539[1] = (Class166_Sub1.anInt4302 + Class369.anInt3136 * Class210_Sub1.anIntArray4539[1] / i_3_);
		Class210_Sub1.anIntArray4539[0] = Class128.anInt1315 + (Class217.anInt2118 * Class210_Sub1.anIntArray4539[i_1_] / i_3_);
		return true;
	}

	static final String method3624(int i, String[] strings, int i_4_, boolean bool) {
		if (i_4_ == 0)
			return "";
		if (i_4_ == 1) {
			String string = strings[i];
			if (string == null)
				return "null";
			return string.toString();
		}
		if (bool)
			method3624(34, null, -9, false);
		int i_5_ = i_4_ + i;
		int i_6_ = 0;
		for (int i_7_ = i; i_5_ > i_7_; i_7_++) {
			String string = strings[i_7_];
			if (string == null)
				i_6_ += 4;
			else
				i_6_ += string.length();
		}
		StringBuffer stringbuffer = new StringBuffer(i_6_);
		for (int i_8_ = i; i_8_ < i_5_; i_8_++) {
			String string = strings[i_8_];
			if (string != null)
				stringbuffer.append(string);
			else
				stringbuffer.append("null");
		}
		return stringbuffer.toString();
	}

	final void method3625(byte i, char c) {
		try {
			int i_9_ = -123 / ((i + 54) / 46);
			OpenGL.glCallList(anInt2979 + c);
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "th.C(" + i + ',' + c + ')');
		}
	}

	public static void method3626(byte i) {
		anInterface1Array2978 = null;
		if (i <= 28)
			method3624(-50, null, -102, false);
	}

	final void method3627(byte i, int i_10_) {
		OpenGL.glNewList(anInt2979 + i_10_, 4864);
		if (i >= -89)
			aBoolean2977 = false;
	}

	Class341(ha_Sub3 var_ha_Sub3, int i) {
		anInt2979 = OpenGL.glGenLists(i);
	}

	static {
		aBoolean2977 = false;
	}
}
