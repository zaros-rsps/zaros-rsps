package net.zaros.client;

/* Class113 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class AdvancedMemoryCache {
	private HashTable cache;
	private int maxCapacity;
	static AdvancedMemoryCache aClass113_1164 = new AdvancedMemoryCache(128, 4);
	private int capacity;
	static int anInt1166;
	static int anInt1167 = 2;
	static IncomingPacket CLAN_CHANNEL_DELTA = new IncomingPacket(84, -2);
	static int anInt1169;
	private Queue references = new Queue();
	static int anInt1171 = -1;
	static int anInt1172 = -1;

	public Object getFirst() {
		ReferenceWrapper r = (ReferenceWrapper) cache.getFirst(true);
		while (r != null) {
			Object object = r.getReference(127);
			if (object != null) {
				return object;
			}
			ReferenceWrapper crw = r;
			r = (ReferenceWrapper) cache.getNext(0);
			crw.unlink();
			crw.queue_unlink();
			capacity += crw.usedSlots;
		}
		return null;
	}

	public Object getNext() {
		ReferenceWrapper rw = (ReferenceWrapper) cache.getNext(0);
		while (rw != null) {
			Object object = rw.getReference(117);
			if (object == null) {
				ReferenceWrapper crw = rw;
				rw = (ReferenceWrapper) cache.getNext(0);
				crw.unlink();
				crw.queue_unlink();
				capacity += crw.usedSlots;
			} else {
				return object;
			}
		}
		return null;
	}

	public void clean(int i_2_) {
		if (Class296_Sub51_Sub21.softReferenceFactory != null) {
			for (ReferenceWrapper rw = (ReferenceWrapper) references.getFront(); rw != null; rw = (ReferenceWrapper) references.getNext()) {
				if (rw.isSoft(true)) {
					if (rw.getReference(108) == null) {
						rw.unlink();
						rw.queue_unlink();
						capacity += rw.usedSlots;
					}
				} else if (i_2_ < ++rw.queue_uid) {
					ReferenceWrapper srfw = Class296_Sub51_Sub21.softReferenceFactory.wrapReference(rw, 4);
					cache.put(rw.uid, srfw);
					Class296_Sub15_Sub3.insertNode(srfw, 825, rw);
					rw.unlink();
					rw.queue_unlink();
				}
			}
		}
	}

	static public boolean method984(int i, boolean bool) {
		if (i >= -99) {
			anInt1169 = -51;
		}
		boolean bool_4_ = Class41_Sub13.aHa3774.b();
		if (bool == !bool_4_) {
			if (!bool) {
				Class41_Sub13.aHa3774.h();
			} else if (!Class41_Sub13.aHa3774.k()) {
				bool = false;
			}
			if (bool == !bool_4_) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(!bool ? 0 : 1, 82, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub27_5010);
				Class368_Sub4.method3820(1);
				return true;
			}
			return false;
		}
		return true;
	}

	static public void method986(int i, int i_64_, int i_65_, int i_66_, int i_67_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask(i, i_66_);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.anInt6149 = i_67_;
		class296_sub39_sub5.anInt6145 = i_65_;
		class296_sub39_sub5.intParam = i_64_;
	}

	static public void method987(int i) {
		Class254.method2212(28461, false);
		if (r_Sub2.anInt6714 >= 0 && r_Sub2.anInt6714 != 0) {
			Class33.method348(false, false, r_Sub2.anInt6714);
			r_Sub2.anInt6714 = -1;
		}
		if (i != 1) {
			method986(-109, 51, -85, 11, -11);
		}
	}

	public void clear() {
		references.clear();
		cache.clear();
		capacity = maxCapacity;
	}

	public static void method989(byte i) {
		if (i <= -81) {
			aClass113_1164 = null;
			CLAN_CHANNEL_DELTA = null;
		}
	}

	public int getHardReferenceCount() {
		int i = 0;
		for (ReferenceWrapper rw = (ReferenceWrapper) references.getFront(); rw != null; rw = (ReferenceWrapper) references.getNext()) {
			if (!rw.isSoft(true)) {
				i++;
			}
		}
		return i;
	}

	public void putInternal(int i, int i_68_, long key, Object obj) {
		if (i_68_ > maxCapacity) {
			throw new IllegalStateException("s>cs");
		}
		remove(key);
		capacity -= i_68_;
		while (capacity < 0) {
			ReferenceWrapper rw = (ReferenceWrapper) references.remove();
			remove(rw);
		}
		HardReferenceWrapper hrw = new HardReferenceWrapper(obj, i_68_);
		cache.put(key, hrw);
		references.insert(hrw, -2);
		hrw.queue_uid = i;
	}

	private void remove(ReferenceWrapper rw) {
		if (rw != null) {
			rw.unlink();
			rw.queue_unlink();
			capacity += rw.usedSlots;
		}
	}

	public void remove(long key) {
		ReferenceWrapper r = (ReferenceWrapper) cache.get(key);
		remove(r);
	}

	public void clearSoftReferences() {
		for (ReferenceWrapper rw = (ReferenceWrapper) references.getFront(); rw != null; rw = (ReferenceWrapper) references.getNext()) {
			if (rw.isSoft(true)) {
				rw.unlink();
				rw.queue_unlink();
				capacity += rw.usedSlots;
			}
		}
	}

	public int getCapacity() {
		return capacity;
	}

	public AdvancedMemoryCache(int i) {
		this(i, i);
	}

	public int getMaxCapacity() {
		return maxCapacity;
	}

	public void put(Object obj, long key) {
		putInternal(0, 1, key, obj);
	}

	static public void method998(int i, int i_70_, int i_71_) {
		if (Class41_Sub10.anInt3769 != i) {
			Class33.anIntArray334 = new int[i];
			for (int i_72_ = 0; i > i_72_; i_72_++) {
				Class33.anIntArray334[i_72_] = (i_72_ << 12) / i;
			}
			Class41_Sub25.anInt3803 = i - 1;
			StaticMethods.anInt5924 = i * 32;
			Class41_Sub10.anInt3769 = i;
		}
		if (i_71_ != Class296_Sub35_Sub2.anInt6114) {
			if (i_71_ != Class41_Sub10.anInt3769) {
				Class294.anIntArray2686 = new int[i_71_];
				for (int i_73_ = 0; i_71_ > i_73_; i_73_++) {
					Class294.anIntArray2686[i_73_] = (i_73_ << 12) / i_71_;
				}
			} else {
				Class294.anIntArray2686 = Class33.anIntArray334;
			}
			Class296_Sub35_Sub2.anInt6114 = i_71_;
			Class67.anInt753 = i_71_ - 1;
		}
		if (i_70_ != -1) {
			CLAN_CHANNEL_DELTA = null;
		}
	}

	public Object get(long key) {
		ReferenceWrapper r = (ReferenceWrapper) cache.get(key);
		if (r == null) {
			return null;
		}
		Object object = r.getReference(127);
		if (object == null) {
			r.unlink();
			r.queue_unlink();
			capacity += r.usedSlots;
			return null;
		}
		if (!r.isSoft(true)) {
			references.insert(r, -2);
			r.queue_uid = 0L;
		} else {
			HardReferenceWrapper hr = new HardReferenceWrapper(object, r.usedSlots);
			cache.put(r.uid, hr);
			references.insert(hr, -2);
			hr.queue_uid = 0L;
			r.unlink();
			r.queue_unlink();
		}
		return object;
	}

	AdvancedMemoryCache(int count, int i_74_) {
		capacity = count;
		maxCapacity = count;
		int size;
		for (size = 1; size + size < count; size += size) {
			if (i_74_ <= size) {
				break;
			}
		}
		cache = new HashTable(size);
	}
}
