package net.zaros.client;
/* d - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public interface d {
	public int[] get_transparent_pixels(int i, int i_0_, boolean bool, byte i_1_, float f, int i_2_);

	public int method13(byte i);

	public MaterialRaw method14(int i, int i_3_);

	public boolean is_ready(int materialid);

	public float[] method16(int i, float f, boolean bool, int i_5_, int i_6_, int i_7_);

	public int[] method17(float f, int i, boolean bool, byte i_8_, int i_9_, int i_10_);
}
