package net.zaros.client;

import net.zaros.client.configs.objtype.ObjType;

/* Class338_Sub3_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub3_Sub2_Sub1 extends Class338_Sub3_Sub2 {
	int anInt6846;
	int anInt6847;
	int anInt6848;
	static Class81 aClass81_6849 = new Class81("", 15);
	int anInt6850;
	private int anInt6851;
	private boolean aBoolean6852 = false;
	int anInt6853;
	int anInt6854;
	int anInt6855;

	final boolean method3475(int i, int i_0_, ha var_ha, int i_1_) {
		Class373 class373 = var_ha.f();
		class373.method3902(tileX, anInt5213 - 10, tileY);
		ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(anInt6848);
		Model class178 = class158.getModel(anInt6850, var_ha, null, 131072, (byte) 4, null);
		if (class178 != null && (Class296_Sub39_Sub10.aBoolean6177 ? class178.method1731(i_1_, i, class373, true, class158.pickShiftSize, ModeWhat.anInt1192) : class178.method1732(i_1_, i, class373, true, class158.pickShiftSize)))
			return true;
		if (anInt6854 != -1) {
			ObjType class158_2_ = Class296_Sub39_Sub1.itemDefinitionLoader.list(anInt6854);
			class178 = class158_2_.getModel(anInt6847, var_ha, null, 131072, (byte) 4, null);
			if (class178 != null && (!Class296_Sub39_Sub10.aBoolean6177 ? class178.method1732(i_1_, i, class373, true, class158_2_.pickShiftSize) : class178.method1731(i_1_, i, class373, true, class158_2_.pickShiftSize, ModeWhat.anInt1192)))
				return true;
		}
		if (i_0_ > -48)
			return true;
		if (anInt6853 != -1) {
			ObjType class158_3_ = Class296_Sub39_Sub1.itemDefinitionLoader.list(anInt6853);
			class178 = class158_3_.getModel(anInt6846, var_ha, null, 131072, (byte) 4, null);
			if (class178 != null && (!Class296_Sub39_Sub10.aBoolean6177 ? class178.method1732(i_1_, i, class373, true, class158_3_.pickShiftSize) : class178.method1731(i_1_, i, class373, true, class158_3_.pickShiftSize, ModeWhat.anInt1192)))
				return true;
		}
		return false;
	}

	final boolean method3469(int i) {
		if (i <= 82)
			anInt6846 = -99;
		return aBoolean6852;
	}

	final int method3462(byte i) {
		if (i != 28)
			method3459(77);
		return anInt6851;
	}

	public static void method3554(int i) {
		if (i > -101)
			method3555(-20, -98, -71, -74, -45, (byte) -108);
		aClass81_6849 = null;
	}

	final int method3458(int i) {
		ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(anInt6848);
		int i_4_ = class158.pickShiftSize;
		if (anInt6854 != -1) {
			ObjType class158_5_ = Class296_Sub39_Sub1.itemDefinitionLoader.list(anInt6854);
			if (class158_5_.pickShiftSize > i_4_)
				i_4_ = class158_5_.pickShiftSize;
		}
		if (anInt6853 != -1) {
			ObjType class158_6_ = Class296_Sub39_Sub1.itemDefinitionLoader.list(anInt6853);
			if (i_4_ < class158_6_.pickShiftSize)
				i_4_ = class158_6_.pickShiftSize;
		}
		return i_4_;
	}

	static final void method3555(int i, int i_7_, int i_8_, int i_9_, int i_10_, byte i_11_) {
		if (RuntimeException_Sub1.anInt3391 >= i_7_ && i >= EmissiveTriangle.anInt952) {
			boolean bool;
			if (ConfigurationDefinition.anInt676 > i_9_) {
				bool = false;
				i_9_ = ConfigurationDefinition.anInt676;
			} else if (i_9_ <= Class288.anInt2652)
				bool = true;
			else {
				bool = false;
				i_9_ = Class288.anInt2652;
			}
			boolean bool_12_;
			if (ConfigurationDefinition.anInt676 <= i_10_) {
				if (Class288.anInt2652 >= i_10_)
					bool_12_ = true;
				else {
					i_10_ = Class288.anInt2652;
					bool_12_ = false;
				}
			} else {
				i_10_ = ConfigurationDefinition.anInt676;
				bool_12_ = false;
			}
			if (EmissiveTriangle.anInt952 <= i_7_)
				Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_7_++]), i_10_, (byte) -86, i_8_, i_9_);
			else
				i_7_ = EmissiveTriangle.anInt952;
			if (i > RuntimeException_Sub1.anInt3391)
				i = RuntimeException_Sub1.anInt3391;
			else
				Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i--]), i_10_, (byte) 122, i_8_, i_9_);
			if (!bool || !bool_12_) {
				if (!bool) {
					if (bool_12_) {
						for (int i_13_ = i_7_; i >= i_13_; i_13_++)
							Class296_Sub51_Sub37.anIntArrayArray6536[i_13_][i_10_] = i_8_;
					}
				} else {
					for (int i_14_ = i_7_; i_14_ <= i; i_14_++)
						Class296_Sub51_Sub37.anIntArrayArray6536[i_14_][i_9_] = i_8_;
				}
			} else {
				for (int i_15_ = i_7_; i >= i_15_; i_15_++) {
					int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_15_];
					is[i_9_] = is[i_10_] = i_8_;
				}
			}
		}
		if (i_11_ != 113)
			method3556((byte) -97, 72, -11);
	}

	final boolean method3459(int i) {
		if (i != 0)
			method3473(null, (byte) -94);
		return false;
	}

	static final boolean method3556(byte i, int i_16_, int i_17_) {
		if (i <= 74)
			method3556((byte) -35, -33, -76);
		return (Class366_Sub1.method3771(i_16_, -1, i_17_) & Class41_Sub26.method498(i_16_, i_17_, (byte) -74));
	}

	final Class96 method3461(ha var_ha, int i) {
		int i_18_ = -126 % ((79 - i) / 44);
		return null;
	}

	final void method3460(int i, ha var_ha) {
		int i_19_ = 123 / ((-41 - i) / 62);
	}

	final Class338_Sub2 method3473(ha var_ha, byte i) {
		Class234 class234 = Class296_Sub15.method2516(z, tileX >> Class313.anInt2779, tileY >> Class313.anInt2779);
		Class338_Sub3_Sub5 class338_sub3_sub5 = Class296_Sub15_Sub3.method2540(z, tileX >> Class313.anInt2779, tileY >> Class313.anInt2779);
		int i_20_ = 0;
		if (class234 != null && class234.aClass338_Sub3_Sub1_2226.aBoolean6557)
			i_20_ = class234.aClass338_Sub3_Sub1_2226.method3466((byte) 109);
		if (class338_sub3_sub5 != null && class338_sub3_sub5.aShort6573 > -i_20_)
			i_20_ = -class338_sub3_sub5.aShort6573;
		if (anInt6855 != i_20_) {
			anInt5213 -= anInt6855;
			anInt6855 = i_20_;
			anInt5213 += i_20_;
		}
		if (i > -84)
			anInt6851 = 81;
		Class373 class373 = var_ha.f();
		class373.method3910();
		if (anInt6855 == 0) {
			boolean bool = false;
			boolean bool_21_ = false;
			boolean bool_22_ = false;
			s var_s = Class360_Sub2.aSArray5304[aByte5203];
			int i_23_ = anInt6851 << 1;
			int i_24_ = i_23_;
			int i_25_ = -i_23_ / 2;
			int i_26_ = -i_24_ / 2;
			int i_27_ = var_s.method3349(0, i_26_ + tileY, tileX + i_25_);
			int i_28_ = i_23_ / 2;
			int i_29_ = -i_24_ / 2;
			int i_30_ = var_s.method3349(0, tileY + i_29_, i_28_ + tileX);
			int i_31_ = -i_23_ / 2;
			int i_32_ = i_24_ / 2;
			int i_33_ = var_s.method3349(0, tileY + i_32_, i_31_ + tileX);
			int i_34_ = i_23_ / 2;
			int i_35_ = i_24_ / 2;
			int i_36_ = var_s.method3349(0, i_35_ + tileY, i_34_ + tileX);
			int i_37_ = i_27_ < i_30_ ? i_27_ : i_30_;
			int i_38_ = i_36_ <= i_33_ ? i_36_ : i_33_;
			int i_39_ = i_36_ > i_30_ ? i_30_ : i_36_;
			if (i_24_ != 0) {
				int i_40_ = ((int) (Math.atan2((double) (i_37_ - i_38_), (double) i_24_) * 2607.5945876176133) & 0x3fff);
				if (i_40_ != 0)
					class373.method3914(i_40_);
			}
			int i_41_ = i_27_ < i_33_ ? i_27_ : i_33_;
			if (i_23_ != 0) {
				int i_42_ = ((int) (Math.atan2((double) (-i_39_ + i_41_), (double) i_23_) * 2607.5945876176133) & 0x3fff);
				if (i_42_ != 0)
					class373.method3900(-i_42_);
			}
			int i_43_ = i_27_ + i_36_;
			if (i_30_ + i_33_ < i_43_)
				i_43_ = i_30_ + i_33_;
			i_43_ = -anInt5213 + (i_43_ >> 1);
			if (i_43_ != 0)
				class373.method3904(0, i_43_, 0);
		}
		class373.method3904(tileX, anInt5213 - 10, tileY);
		Class338_Sub2 class338_sub2 = Class144.method1481(3, true, true);
		aBoolean6852 = false;
		anInt6851 = 0;
		if (anInt6853 != -1) {
			Model class178 = (Class296_Sub39_Sub1.itemDefinitionLoader.list(anInt6853).getModel(anInt6846, var_ha, null, 2048, (byte) 4, null));
			if (class178 != null) {
				if (Class296_Sub39_Sub10.aBoolean6177)
					class178.method1719(class373, (class338_sub2.aClass338_Sub5Array5194[2]), ModeWhat.anInt1192, 0);
				else
					class178.method1715(class373, (class338_sub2.aClass338_Sub5Array5194[2]), 0);
				aBoolean6852 |= class178.F();
				anInt6851 = class178.ma();
			}
		}
		if (anInt6854 != -1) {
			Model class178 = (Class296_Sub39_Sub1.itemDefinitionLoader.list(anInt6854).getModel(anInt6847, var_ha, null, 2048, (byte) 4, null));
			if (class178 != null) {
				if (Class296_Sub39_Sub10.aBoolean6177)
					class178.method1719(class373, (class338_sub2.aClass338_Sub5Array5194[1]), ModeWhat.anInt1192, 0);
				else
					class178.method1715(class373, (class338_sub2.aClass338_Sub5Array5194[1]), 0);
				aBoolean6852 |= class178.F();
				if (class178.ma() > anInt6851)
					anInt6851 = class178.ma();
			}
		}
		Model class178 = Class296_Sub39_Sub1.itemDefinitionLoader.list(anInt6848).getModel(anInt6850, var_ha, null, 2048, (byte) 4, null);
		if (class178 != null) {
			if (!Class296_Sub39_Sub10.aBoolean6177)
				class178.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
			else
				class178.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
			aBoolean6852 |= class178.F();
			if (class178.ma() > anInt6851)
				anInt6851 = class178.ma();
		}
		return class338_sub2;
	}

	Class338_Sub3_Sub2_Sub1(int i, int i_44_, int i_45_, int i_46_, int i_47_) {
		super(i, i_44_, i_45_, i_46_, i_47_);
		anInt6851 = 0;
		anInt6854 = -1;
		anInt6853 = -1;
		anInt6855 = 0;
	}

	final int method3466(byte i) {
		if (i < 77)
			return -77;
		return -10;
	}
}
