package net.zaros.client;

/* Class368_Sub22 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub22 extends Class368 {
	private int anInt5558;
	static int anInt5559;
	static OutgoingPacket aClass311_5560 = new OutgoingPacket(11, 8);
	private long aLong5561;

	final void method3807(byte i) {
		int i_0_ = 56 / ((i - 52) / 52);
		IntegerNode class296_sub16 = ((IntegerNode) Class352.aClass263_3043.get(aLong5561));
		if (class296_sub16 != null)
			class296_sub16.value = anInt5558;
		else
			Class352.aClass263_3043.put(aLong5561, new IntegerNode(anInt5558));
	}

	static final void method3866(int i) {
		Class361.aBoolean3096 = true;
		if (i != 11)
			method3867(36);
	}

	Class368_Sub22(Packet class296_sub17, boolean bool) {
		super(class296_sub17);
		int i = class296_sub17.g2();
		if (bool)
			aLong5561 = (long) i | 0x100000000L;
		else
			aLong5561 = (long) i;
		anInt5558 = class296_sub17.g4();
	}

	public static void method3867(int i) {
		if (i != 22067)
			anInt5559 = 100;
		aClass311_5560 = null;
	}
}
