package net.zaros.client;

/* Class106 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

class Class106 {
	static IConfigsRegister anInterface17_1090;
	static ObjectDefinitionLoader aClass38_1091;
	private static Class15 aClass15_1092;
	static Js5 aClass138_1093;
	static Class245 aClass245_1094;
	private static Class117 aClass117_1095;
	static Class401 aClass401_1096;
	static Class296_Sub39_Sub14 aClass296_Sub39_Sub14_1097;
	private static HashTable aClass263_1098 = new HashTable(16);
	static int anInt1099;
	static float aFloat1100;
	static NodeDeque aClass155_1101;
	static byte[][][] aByteArrayArrayArray1102;
	static float aFloat1103;
	static int anInt1104 = (int) (Math.random() * 17.0) - 8;
	private static byte[] aByteArray1105;
	static int anInt1106 = (int) (Math.random() * 11.0) - 5;
	static Class259 aClass259_1107;
	private static short[] aShortArray1108;
	private static int[] anIntArray1109;
	private static Class404[][][] aClass404ArrayArrayArray1110;
	static int anInt1111;
	static int anInt1112;
	private static short[] aShortArray1113;
	static int anInt1114;
	private static byte[] aByteArray1115;
	static int anInt1116;
	static int anInt1117;
	private static byte[] aByteArray1118;
	private static short[] aShortArray1119;
	static int anInt1120;
	static int anInt1121;
	private static byte[] aByteArray1122;
	static int anInt1123;
	private static byte[] aByteArray1124;
	private static HashTable aClass263_1125;
	static int anInt1126;
	private static byte[] aByteArray1127;
	static int anInt1128;
	static int anInt1129;
	static int anInt1130;

	static final void method916() {
		aByteArray1118 = new byte[anInt1116 * anInt1114];
		aByteArray1122 = new byte[anInt1116 * anInt1114];
		aByteArray1124 = new byte[anInt1116 * anInt1114];
		aShortArray1113 = new short[anInt1116 * anInt1114];
		aByteArray1115 = new byte[anInt1116 * anInt1114];
		aClass263_1125 = new HashTable(1024);
		aClass404ArrayArrayArray1110 = new Class404[3][anInt1116 >> 6][anInt1114 >> 6];
		anIntArray1109 = new int[aClass15_1092.anInt175 + 1];
	}

	static final void method917() {
		int[] is = new int[3];
		for (int i = 0; i < aClass259_1107.anInt2417; i++) {
			boolean bool = (aClass296_Sub39_Sub14_1097.method2877(aClass259_1107.anIntArray2418[i] & 0x3fff, is, aClass259_1107.anIntArray2418[i] >> 14 & 0x3fff, (byte) -57, aClass259_1107.anIntArray2418[i] >> 28 & 0x3));
			if (bool) {
				Class296_Sub53 class296_sub53 = new Class296_Sub53(aClass259_1107.anIntArray2420[i]);
				class296_sub53.anInt5047 = is[1] - anInt1111;
				class296_sub53.anInt5042 = is[2] - anInt1117;
				aClass155_1101.addLast((byte) -78, class296_sub53);
			}
		}
	}

	static final void method918(int i) {
		aClass296_Sub39_Sub14_1097 = (Class296_Sub39_Sub14) aClass263_1098.get((long) i);
	}

	private static final void method919(byte[] is, byte[] is_0_, short[] is_1_, int i, int i_2_) {
		int[] is_3_ = new int[anInt1114];
		int[] is_4_ = new int[anInt1114];
		int[] is_5_ = new int[anInt1114];
		int[] is_6_ = new int[anInt1114];
		int[] is_7_ = new int[anInt1114];
		for (int i_8_ = -5; i_8_ < anInt1116; i_8_++) {
			int i_9_ = i_8_ + 5;
			int i_10_ = i_8_ - 5;
			for (int i_11_ = 0; i_11_ < anInt1114; i_11_++) {
				if (i_9_ < anInt1116) {
					int i_12_ = is[i_9_ + i_11_ * anInt1116] & 0xff;
					if (i_12_ > 0) {
						Class236 class236 = aClass117_1095.method1020(true, i_12_ - 1);
						is_3_[i_11_] += class236.anInt2234;
						is_4_[i_11_] += class236.anInt2236;
						is_5_[i_11_] += class236.anInt2237;
						is_6_[i_11_] += class236.anInt2238;
						is_7_[i_11_]++;
					}
				}
				if (i_10_ >= 0) {
					int i_13_ = is[i_10_ + i_11_ * anInt1116] & 0xff;
					if (i_13_ > 0) {
						Class236 class236 = aClass117_1095.method1020(true, i_13_ - 1);
						is_3_[i_11_] -= class236.anInt2234;
						is_4_[i_11_] -= class236.anInt2236;
						is_5_[i_11_] -= class236.anInt2237;
						is_6_[i_11_] -= class236.anInt2238;
						is_7_[i_11_]--;
					}
				}
			}
			if (i_8_ >= 0) {
				int i_14_ = 0;
				int i_15_ = 0;
				int i_16_ = 0;
				int i_17_ = 0;
				int i_18_ = 0;
				for (int i_19_ = -5; i_19_ < anInt1114; i_19_++) {
					int i_20_ = i_19_ + 5;
					if (i_20_ < anInt1114) {
						i_14_ += is_3_[i_20_];
						i_15_ += is_4_[i_20_];
						i_16_ += is_5_[i_20_];
						i_17_ += is_6_[i_20_];
						i_18_ += is_7_[i_20_];
					}
					int i_21_ = i_19_ - 5;
					if (i_21_ >= 0) {
						i_14_ -= is_3_[i_21_];
						i_15_ -= is_4_[i_21_];
						i_16_ -= is_5_[i_21_];
						i_17_ -= is_6_[i_21_];
						i_18_ -= is_7_[i_21_];
					}
					if (i_19_ >= 0 && i_18_ > 0) {
						if ((is[i_8_ + i_19_ * anInt1116] & 0xff) == 0) {
							int i_22_ = i_8_ + i_19_ * anInt1116;
							is_0_[i_22_] = (byte) 0;
							is_1_[i_22_] = (short) 0;
						} else {
							int i_23_ = (i_17_ == 0 ? 0 : (Class296_Sub51_Sub17.method3123(i_16_ / i_18_, i_14_ * 256 / i_17_, i_15_ / i_18_, -1)));
							int i_24_ = (i_23_ & 0x7f) + i_2_;
							if (i_24_ < 0)
								i_24_ = 0;
							else if (i_24_ > 127)
								i_24_ = 127;
							int i_25_ = ((i_23_ + i & 0xfc00) + (i_23_ & 0x380) + i_24_);
							int i_26_ = i_8_ + i_19_ * anInt1116;
							int i_27_ = (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1.method3576(Class61.method694(96, i_25_, 127), (byte) -55)) & 0xffff]);
							is_0_[i_26_] = (byte) (i_27_ >> 16 & 0xff);
							is_1_[i_26_] = (short) (i_27_ & 0xffff);
						}
					}
				}
			}
		}
	}

	private static final NodeDeque method920(ha var_ha, int i, int i_28_, int i_29_, int i_30_) {
		for (Class296_Sub53 class296_sub53 = (Class296_Sub53) aClass155_1101.removeFirst((byte) 126); class296_sub53 != null; class296_sub53 = (Class296_Sub53) aClass155_1101.removeNext(1001))
			method921(var_ha, class296_sub53, i, i_28_, i_29_, i_30_);
		return aClass155_1101;
	}

	private static final void method921(ha var_ha, Class296_Sub53 class296_sub53, int i, int i_31_, int i_32_, int i_33_) {
		class296_sub53.anInt5041 = anInt1130 + (i_32_ + i * (class296_sub53.anInt5047 - anInt1128) >> 16);
		class296_sub53.anInt5043 = anInt1129 - (i_33_ + i_31_ * (class296_sub53.anInt5042 - anInt1120) >> 16);
	}

	static final void method922(Js5 class138, Class15 class15, Class117 class117, ObjectDefinitionLoader class38, Class245 class245, Class401 class401, IConfigsRegister interface17) {
		aClass138_1093 = class138;
		aClass15_1092 = class15;
		aClass117_1095 = class117;
		aClass38_1091 = class38;
		aClass245_1094 = class245;
		aClass401_1096 = class401;
		anInterface17_1090 = interface17;
		aClass263_1098.clear();
		int i = aClass138_1093.getFileIndex("details");
		int[] is = aClass138_1093.getChildIndicies(i);
		if (is != null) {
			for (int i_34_ = 0; i_34_ < is.length; i_34_++) {
				Class296_Sub39_Sub14 class296_sub39_sub14 = Class354.method3691(i, (byte) -105, is[i_34_], aClass138_1093);
				aClass263_1098.put((long) (class296_sub39_sub14.anInt6214), class296_sub39_sub14);
			}
		}
		Class338.method3437(false, false, true);
	}

	static final Class296_Sub39_Sub14 method923(int i) {
		return ((Class296_Sub39_Sub14) aClass263_1098.get((long) i));
	}

	private static final int method924(d var_d, int i, int i_35_, int i_36_) {
		Class61 class61 = aClass15_1092.method232(true, i);
		if (class61 == null)
			return 0;
		int i_37_ = class61.anInt709;
		if (i_37_ >= 0 && var_d.method14(i_37_, -9412).aBoolean1792)
			i_37_ = -1;
		int i_38_;
		if (class61.anInt707 >= 0) {
			int i_39_ = class61.anInt707;
			int i_40_ = (i_39_ & 0x7f) + i_36_;
			if (i_40_ < 0)
				i_40_ = 0;
			else if (i_40_ > 127)
				i_40_ = 127;
			int i_41_ = (i_39_ + i_35_ & 0xfc00) + (i_39_ & 0x380) + i_40_;
			i_38_ = (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1.method3576(Class296_Sub15.method2522(i_41_, 96, -72), (byte) -127)) & 0xffff]) | ~0xffffff;
		} else if (i_37_ >= 0)
			i_38_ = (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1.method3576(Class296_Sub15.method2522((var_d.method14(i_37_, -9412).aShort1775), 96, -71), (byte) -56)) & 0xffff]) | ~0xffffff;
		else if (class61.anInt702 == -1)
			i_38_ = 0;
		else {
			int i_42_ = class61.anInt702;
			int i_43_ = (i_42_ & 0x7f) + i_36_;
			if (i_43_ < 0)
				i_43_ = 0;
			else if (i_43_ > 127)
				i_43_ = 127;
			int i_44_ = (i_42_ + i_35_ & 0xfc00) + (i_42_ & 0x380) + i_43_;
			i_38_ = (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1.method3576(Class296_Sub15.method2522(i_44_, 96, -90), (byte) -117)) & 0xffff]) | ~0xffffff;
		}
		return i_38_;
	}

	public static void method925() {
		aClass15_1092 = null;
		aClass117_1095 = null;
		aClass38_1091 = null;
		aClass245_1094 = null;
		aClass401_1096 = null;
		anInterface17_1090 = null;
		aClass296_Sub39_Sub14_1097 = null;
		aClass138_1093 = null;
		aClass263_1098 = null;
		aByteArrayArrayArray1102 = null;
		aShortArray1108 = null;
		aByteArray1105 = null;
		aClass259_1107 = null;
		aClass155_1101 = null;
		anIntArray1109 = null;
		aByteArray1118 = null;
		aByteArray1127 = null;
		aShortArray1119 = null;
		aByteArray1122 = null;
		aByteArray1124 = null;
		aShortArray1113 = null;
		aByteArray1115 = null;
		aClass263_1125 = null;
		aClass404ArrayArrayArray1110 = null;
	}

	static final void method926(ha var_ha, Class296_Sub53 class296_sub53, Class18 class18) {
		if (class18.anIntArray211 != null) {
			int[] is = new int[class18.anIntArray211.length];
			for (int i = 0; i < is.length / 2; i++) {
				int i_45_ = class18.anIntArray211[i * 2] + class296_sub53.anInt5047;
				int i_46_ = (class18.anIntArray211[i * 2 + 1] + class296_sub53.anInt5042);
				is[i * 2] = anInt1130 + ((anInt1123 - anInt1130) * (i_45_ - anInt1128) / (anInt1112 - anInt1128));
				is[i * 2 + 1] = anInt1129 - ((anInt1129 - anInt1126) * (i_46_ - anInt1120) / (anInt1121 - anInt1120));
			}
			Class148.method1510(var_ha, is, class18.anInt213);
			if (class18.anInt196 > 0) {
				for (int i = 0; i < is.length / 2 - 1; i++) {
					int i_47_ = is[i * 2];
					int i_48_ = is[i * 2 + 1];
					int i_49_ = is[(i + 1) * 2];
					int i_50_ = is[(i + 1) * 2 + 1];
					if (i_49_ < i_47_) {
						int i_51_ = i_47_;
						int i_52_ = i_48_;
						i_47_ = i_49_;
						i_48_ = i_50_;
						i_49_ = i_51_;
						i_50_ = i_52_;
					} else if (i_49_ == i_47_ && i_50_ < i_48_) {
						int i_53_ = i_48_;
						i_48_ = i_50_;
						i_50_ = i_53_;
					}
					var_ha.a(i_47_, i_48_, i_49_, i_50_, (class18.anIntArray229[class18.aByteArray194[i] & 0xff]), 1, class18.anInt196, class18.anInt226, class18.anInt227);
				}
				int i = is[is.length - 2];
				int i_54_ = is[is.length - 1];
				int i_55_ = is[0];
				int i_56_ = is[1];
				if (i_55_ < i) {
					int i_57_ = i;
					int i_58_ = i_54_;
					i = i_55_;
					i_54_ = i_56_;
					i_55_ = i_57_;
					i_56_ = i_58_;
				} else if (i_55_ == i && i_56_ < i_54_) {
					int i_59_ = i_54_;
					i_54_ = i_56_;
					i_56_ = i_59_;
				}
				var_ha.a(i, i_54_, i_55_, i_56_, (class18.anIntArray229[(class18.aByteArray194[class18.aByteArray194.length - 1]) & 0xff]), 1, class18.anInt196, class18.anInt226, class18.anInt227);
			} else {
				for (int i = 0; i < is.length / 2 - 1; i++)
					var_ha.method1094(is[(i + 1) * 2 + 1], is[i * 2 + 1], is[(i + 1) * 2], is[i * 2], 123, (class18.anIntArray229[class18.aByteArray194[i] & 0xff]));
				var_ha.method1094(is[1], is[is.length - 1], is[0], is[is.length - 2], 124, (class18.anIntArray229[((class18.aByteArray194[class18.aByteArray194.length - 1]) & 0xff)]));
			}
		}
	}

	private static final void method927(ha var_ha, int i, int i_60_, int i_61_, int i_62_, short[] is, byte[] is_63_) {
		if (is != null) {
			for (int i_64_ = 0; i_64_ < is.length; i_64_++) {
				ObjectDefinition class70 = aClass38_1091.getObjectDefinition(is[i_64_] & 0xffff);
				int i_65_ = class70.anInt765;
				if (i_65_ != -1) {
					Class28 class28 = aClass401_1096.method4143(i_65_, 24180);
					Sprite class397 = class28.method315(var_ha, -22801, (class70.aBoolean806 ? class70.aBoolean812 : false), (class70.aBoolean793 ? is_63_[i_64_] >> 6 & 0x3 : 0));
					if (class397 != null) {
						int i_66_ = i_61_ * class397.method4099() >> 2;
						int i_67_ = i_62_ * class397.method4088() >> 2;
						if (class28.aBoolean298) {
							int i_68_ = class70.sizeX;
							int i_69_ = class70.sizeY;
							if ((is_63_[i_64_] >> 6 & 0x1) == 1) {
								int i_70_ = i_68_;
								i_68_ = i_69_;
								i_69_ = i_70_;
							}
							i_66_ = i_68_ * i_61_;
							i_67_ = i_69_ * i_62_;
						}
						if (i_66_ != 0 && i_67_ != 0) {
							if (class28.anInt303 != 0)
								class397.method4083(i, i_60_ - i_67_ + i_62_, i_66_, i_67_, 0, (class28.anInt303 | ~0xffffff), 1);
							else
								class397.method4080(i, i_60_ - i_67_ + i_62_, i_66_, i_67_);
						}
					}
				}
			}
		}
	}

	static final void method928(int i, int i_71_, int i_72_, int i_73_, int i_74_, int i_75_, int i_76_, int i_77_) {
		anInt1128 = i - anInt1111;
		anInt1121 = i_71_ - anInt1117;
		anInt1112 = i_72_ - anInt1111;
		anInt1120 = i_73_ - anInt1117;
		anInt1130 = i_74_;
		anInt1126 = i_75_;
		anInt1123 = i_76_;
		anInt1129 = i_77_;
	}

	static final NodeDeque method929(ha var_ha) {
		int i = anInt1112 - anInt1128;
		int i_78_ = anInt1121 - anInt1120;
		int i_79_ = (anInt1123 - anInt1130 << 16) / i;
		int i_80_ = (anInt1129 - anInt1126 << 16) / i_78_;
		return method920(var_ha, i_79_, i_80_, 0, 0);
	}

	static final void method930(ha var_ha) {
		int i = anInt1112 - anInt1128;
		int i_81_ = anInt1121 - anInt1120;
		int i_82_ = (anInt1123 - anInt1130 << 16) / i;
		int i_83_ = (anInt1129 - anInt1126 << 16) / i_81_;
		method934(var_ha, i_82_, i_83_, 0, 0);
	}

	static final void method931(d var_d, int i, int i_84_) {
		for (int i_85_ = 0; i_85_ < aClass15_1092.anInt175; i_85_++)
			anIntArray1109[i_85_ + 1] = method924(var_d, i_85_, i, i_84_);
	}

	static final Queue method932(int i, int i_86_) {
		Queue class145 = new Queue();
		for (Class296_Sub39_Sub14 class296_sub39_sub14 = (Class296_Sub39_Sub14) aClass263_1098.getFirst(true); class296_sub39_sub14 != null; class296_sub39_sub14 = (Class296_Sub39_Sub14) aClass263_1098.getNext(0)) {
			if (class296_sub39_sub14.aBoolean6210 && class296_sub39_sub14.method2879(i_86_, i, -4))
				class145.insert(class296_sub39_sub14, -2);
		}
		return class145;
	}

	static final void method933(ha var_ha, int i, int i_87_) {
		Packet class296_sub17 = new Packet(aClass138_1093.getFile_("area", aClass296_Sub39_Sub14_1097.aString6216));
		int i_88_ = class296_sub17.g1();
		int[] is = new int[i_88_];
		for (int i_89_ = 0; i_89_ < i_88_; i_89_++)
			is[i_89_] = class296_sub17.g1();
		int i_90_ = class296_sub17.g1();
		int[] is_91_ = new int[i_90_];
		for (int i_92_ = 0; i_92_ < i_90_; i_92_++)
			is_91_[i_92_] = class296_sub17.g1();
		while (class296_sub17.pos < class296_sub17.data.length) {
			if (class296_sub17.g1() == 0) {
				int i_93_ = class296_sub17.g1();
				int i_94_ = class296_sub17.g1();
				for (int i_95_ = 0; i_95_ < 64; i_95_++) {
					for (int i_96_ = 0; i_96_ < 64; i_96_++) {
						int i_97_ = i_93_ * 64 + i_95_ - anInt1111;
						int i_98_ = i_94_ * 64 + i_96_ - anInt1117;
						method937(var_ha, class296_sub17, i_93_, i_94_, i_97_, i_98_, is, is_91_);
					}
				}
			} else {
				int i_99_ = class296_sub17.g1();
				int i_100_ = class296_sub17.g1();
				int i_101_ = class296_sub17.g1();
				int i_102_ = class296_sub17.g1();
				for (int i_103_ = 0; i_103_ < 8; i_103_++) {
					for (int i_104_ = 0; i_104_ < 8; i_104_++) {
						int i_105_ = i_99_ * 64 + i_101_ * 8 + i_103_ - anInt1111;
						int i_106_ = i_100_ * 64 + i_102_ * 8 + i_104_ - anInt1117;
						method937(var_ha, class296_sub17, i_99_, i_100_, i_105_, i_106_, is, is_91_);
					}
				}
			}
		}
		Object object = null;
		aByteArray1127 = new byte[anInt1116 * anInt1114];
		aShortArray1119 = new short[anInt1116 * anInt1114];
		for (int i_107_ = 0; i_107_ < 3; i_107_++) {
			byte[] is_108_ = new byte[anInt1116 * anInt1114];
			for (int i_109_ = 0; i_109_ < aClass404ArrayArrayArray1110[i_107_].length; i_109_++) {
				for (int i_110_ = 0; i_110_ < aClass404ArrayArrayArray1110[i_107_][0].length; i_110_++) {
					Class404 class404 = aClass404ArrayArrayArray1110[i_107_][i_109_][i_110_];
					if (class404 != null) {
						for (Class338_Sub4 class338_sub4 = ((Class338_Sub4) class404.method4160((byte) 96)); class338_sub4 != null; class338_sub4 = (Class338_Sub4) class404.method4163(-24917))
							is_108_[(i_109_ * 64 + class338_sub4.aByte5217 + ((i_110_ * 64 + class338_sub4.aByte5220) * anInt1116))] = (byte) class338_sub4.anInt5219;
					}
				}
			}
			method919(is_108_, aByteArray1127, aShortArray1119, i, i_87_);
			for (int i_111_ = 0; i_111_ < aClass404ArrayArrayArray1110[i_107_].length; i_111_++) {
				for (int i_112_ = 0; i_112_ < aClass404ArrayArrayArray1110[i_107_][0].length; i_112_++) {
					Class404 class404 = aClass404ArrayArrayArray1110[i_107_][i_111_][i_112_];
					if (class404 != null) {
						for (Class338_Sub4 class338_sub4 = ((Class338_Sub4) class404.method4160((byte) 97)); class338_sub4 != null; class338_sub4 = ((Class338_Sub4) class404.method4163(-24917))) {
							int i_113_ = (i_111_ * 64 + class338_sub4.aByte5217 + ((i_112_ * 64 + class338_sub4.aByte5220) * anInt1116));
							class338_sub4.anInt5219 = ((aByteArray1127[i_113_] & 0xff) << 16 | aShortArray1119[i_113_] & 0xffff);
							if (class338_sub4.anInt5219 != 0)
								class338_sub4.anInt5219 |= ~0xffffff;
						}
					}
				}
			}
		}
		method919(aByteArray1118, aByteArray1127, aShortArray1119, i, i_87_);
		aByteArray1118 = null;
		method939();
	}

	private static final void method934(ha var_ha, int i, int i_114_, int i_115_, int i_116_) {
		int i_117_ = anInt1112 - anInt1128;
		int i_118_ = anInt1121 - anInt1120;
		if (anInt1112 < anInt1116)
			i_117_++;
		if (anInt1121 < anInt1114)
			i_118_++;
		for (int i_119_ = 0; i_119_ < i_117_; i_119_++) {
			int i_120_ = (i_115_ + i * i_119_ >> 16) + anInt1130;
			int i_121_ = (i_115_ + i * (i_119_ + 1) >> 16) + anInt1130;
			int i_122_ = i_121_ - i_120_;
			if (i_122_ > 0) {
				int i_123_ = anInt1128 + i_119_;
				if (i_123_ < 0 || i_123_ >= anInt1116) {
					for (int i_124_ = 0; i_124_ < i_118_; i_124_++) {
						int i_125_ = (anInt1129 - (i_116_ + i_114_ * (i_124_ + 1) >> 16));
						int i_126_ = anInt1129 - (i_116_ + i_114_ * i_124_ >> 16);
						int i_127_ = i_126_ - i_125_;
						int i_128_;
						if (aClass296_Sub39_Sub14_1097.anInt6211 != -1)
							i_128_ = (aClass296_Sub39_Sub14_1097.anInt6211 | ~0xffffff);
						else if ((i_119_ + anInt1128 & 0x4) != (i_124_ + anInt1121 & 0x4))
							i_128_ = -11840664;
						else
							i_128_ = anIntArray1109[aClass15_1092.anInt179 + 1];
						if (i_128_ == 0)
							i_128_ = -16777216;
						var_ha.aa(i_120_, i_125_, i_122_, i_127_, i_128_, 0);
					}
				} else {
					for (int i_129_ = 0; i_129_ < i_118_; i_129_++) {
						int i_130_ = (anInt1129 - (i_116_ + i_114_ * (i_129_ + 1) >> 16));
						int i_131_ = anInt1129 - (i_116_ + i_114_ * i_129_ >> 16);
						int i_132_ = i_131_ - i_130_;
						if (i_132_ > 0) {
							int i_133_ = i_129_ + anInt1120;
							int i_134_ = i_123_ + i_133_ * anInt1116;
							int i_135_ = 0;
							int i_136_ = 0;
							int i_137_ = 0;
							if (i_133_ >= 0 && i_133_ < anInt1114) {
								i_135_ = ((aByteArray1127[i_134_] & 0xff) << 16 | aShortArray1119[i_134_] & 0xffff);
								if (i_135_ != 0)
									i_135_ |= ~0xffffff;
								i_136_ = aByteArray1122[i_134_] & 0xff;
								i_137_ = aShortArray1113[i_134_] & 0xffff;
							}
							if (i_135_ == 0 && i_136_ == 0 && i_137_ == 0) {
								if (aClass296_Sub39_Sub14_1097.anInt6211 != -1)
									i_135_ = (aClass296_Sub39_Sub14_1097.anInt6211 | ~0xffffff);
								else if ((i_119_ + anInt1128 & 0x4) != (i_129_ + anInt1121 & 0x4))
									i_135_ = -11840664;
								else
									i_135_ = (anIntArray1109[aClass15_1092.anInt179 + 1]);
								if (i_135_ == 0)
									i_135_ = -16777216;
								var_ha.aa(i_120_, i_130_, i_122_, i_132_, i_135_, 0);
							} else if (i_137_ > 0) {
								if (i_137_ == 65535) {
									Class296_Sub12 class296_sub12 = ((Class296_Sub12) (aClass263_1125.get((long) (i_123_ << 16 | i_133_))));
									if (class296_sub12 != null)
										method935(var_ha, i_120_, i_130_, i_122_, i_132_, i_135_, i_136_, aByteArray1124[i_134_], (class296_sub12.aShortArray4651), (class296_sub12.aByteArray4652), true);
								} else {
									aShortArray1108[0] = (short) (i_137_ - 1);
									aByteArray1105[0] = aByteArray1115[i_134_];
									method935(var_ha, i_120_, i_130_, i_122_, i_132_, i_135_, i_136_, aByteArray1124[i_134_], aShortArray1108, aByteArray1105, true);
								}
							} else
								method935(var_ha, i_120_, i_130_, i_122_, i_132_, i_135_, i_136_, aByteArray1124[i_134_], null, null, true);
						}
					}
				}
			}
		}
		for (int i_138_ = -16; i_138_ < i_117_ + 16; i_138_++) {
			int i_139_ = (i_115_ + i * i_138_ >> 16) + anInt1130;
			int i_140_ = (i_115_ + i * (i_138_ + 1) >> 16) + anInt1130;
			int i_141_ = i_140_ - i_139_;
			if (i_141_ > 0) {
				int i_142_ = i_138_ + anInt1128;
				if (i_142_ >= 0 && i_142_ < anInt1116) {
					for (int i_143_ = -16; i_143_ < i_118_ + 16; i_143_++) {
						int i_144_ = (anInt1129 - (i_116_ + i_114_ * (i_143_ + 1) >> 16));
						int i_145_ = anInt1129 - (i_116_ + i_114_ * i_143_ >> 16);
						int i_146_ = i_145_ - i_144_;
						if (i_146_ > 0) {
							int i_147_ = i_143_ + anInt1120;
							if (i_147_ >= 0 && i_147_ < anInt1114) {
								int i_148_ = ((aShortArray1113[i_142_ + i_147_ * anInt1116]) & 0xffff);
								if (i_148_ > 0) {
									if (i_148_ == 65535) {
										Class296_Sub12 class296_sub12 = ((Class296_Sub12) (aClass263_1125.get((long) (i_142_ << 16 | i_147_))));
										if (class296_sub12 != null)
											method927(var_ha, i_139_, i_144_, i_141_, i_146_, (class296_sub12.aShortArray4651), (class296_sub12.aByteArray4652));
									} else {
										aShortArray1108[0] = (short) (i_148_ - 1);
										aByteArray1105[0] = (aByteArray1115[i_142_ + i_147_ * anInt1116]);
										method927(var_ha, i_139_, i_144_, i_141_, i_146_, aShortArray1108, aByteArray1105);
									}
								} else
									method927(var_ha, i_139_, i_144_, i_141_, i_146_, null, null);
							}
						}
					}
				}
			}
		}
		int i_149_ = anInt1128 >> 6;
		int i_150_ = anInt1120 >> 6;
		if (i_149_ < 0)
			i_149_ = 0;
		if (i_150_ < 0)
			i_150_ = 0;
		int i_151_ = anInt1112 >> 6;
		int i_152_ = anInt1121 >> 6;
		if (i_151_ >= aClass404ArrayArrayArray1110[0].length)
			i_151_ = aClass404ArrayArrayArray1110[0].length - 1;
		if (i_152_ >= aClass404ArrayArrayArray1110[0][0].length)
			i_152_ = aClass404ArrayArrayArray1110[0][0].length - 1;
		for (int i_153_ = 0; i_153_ < 3; i_153_++) {
			for (int i_154_ = i_149_; i_154_ <= i_151_; i_154_++) {
				for (int i_155_ = i_150_; i_155_ <= i_152_; i_155_++) {
					Class404 class404 = aClass404ArrayArrayArray1110[i_153_][i_154_][i_155_];
					if (class404 != null) {
						int i_156_ = (i_154_ + (anInt1111 >> 6)) * 64;
						int i_157_ = (i_155_ + (anInt1117 >> 6)) * 64;
						for (Class338_Sub4 class338_sub4 = ((Class338_Sub4) class404.method4160((byte) 110)); class338_sub4 != null; class338_sub4 = ((Class338_Sub4) class404.method4163(-24917))) {
							int i_158_ = (i_156_ + class338_sub4.aByte5217 - anInt1111 - anInt1128);
							int i_159_ = (i_157_ + class338_sub4.aByte5220 - anInt1117 - anInt1120);
							int i_160_ = (i_115_ + i * i_158_ >> 16) + anInt1130;
							int i_161_ = ((i_115_ + i * (i_158_ + 1) >> 16) + anInt1130);
							int i_162_ = (anInt1129 - (i_116_ + i_114_ * (i_159_ + 1) >> 16));
							int i_163_ = anInt1129 - (i_116_ + i_114_ * i_159_ >> 16);
							method935(var_ha, i_160_, i_162_, i_161_ - i_160_, i_163_ - i_162_, class338_sub4.anInt5219, class338_sub4.aByte5218 & 0xff, class338_sub4.aByte5214, class338_sub4.aShortArray5215, class338_sub4.aByteArray5216, false);
						}
					}
				}
			}
			for (int i_164_ = i_149_; i_164_ <= i_151_; i_164_++) {
				for (int i_165_ = i_150_; i_165_ <= i_152_; i_165_++) {
					Class404 class404 = aClass404ArrayArrayArray1110[i_153_][i_164_][i_165_];
					if (class404 != null) {
						int i_166_ = (i_164_ + (anInt1111 >> 6)) * 64;
						int i_167_ = (i_165_ + (anInt1117 >> 6)) * 64;
						for (Class338_Sub4 class338_sub4 = ((Class338_Sub4) class404.method4160((byte) 85)); class338_sub4 != null; class338_sub4 = ((Class338_Sub4) class404.method4163(-24917))) {
							int i_168_ = (i_166_ + class338_sub4.aByte5217 - anInt1111 - anInt1128);
							int i_169_ = (i_167_ + class338_sub4.aByte5220 - anInt1117 - anInt1120);
							int i_170_ = (i_115_ + i * i_168_ >> 16) + anInt1130;
							int i_171_ = ((i_115_ + i * (i_168_ + 1) >> 16) + anInt1130);
							int i_172_ = (anInt1129 - (i_116_ + i_114_ * (i_169_ + 1) >> 16));
							int i_173_ = anInt1129 - (i_116_ + i_114_ * i_169_ >> 16);
							method927(var_ha, i_170_, i_172_, i_171_ - i_170_, i_173_ - i_172_, class338_sub4.aShortArray5215, class338_sub4.aByteArray5216);
						}
					}
				}
			}
		}
	}

	private static final void method935(ha var_ha, int i, int i_174_, int i_175_, int i_176_, int i_177_, int i_178_, int i_179_, short[] is, byte[] is_180_, boolean bool) {
		if (bool || i_177_ != 0 || i_178_ > 0) {
			if (i_178_ == 0)
				var_ha.aa(i, i_174_, i_175_, i_176_, i_177_, 0);
			else {
				int i_181_ = i_179_ & 0x3f;
				if (i_181_ == 0 || i_175_ <= 1 || i_176_ <= 1) {
					int i_182_ = anIntArray1109[i_178_];
					if (bool || i_182_ != 0)
						var_ha.aa(i, i_174_, i_175_, i_176_, i_182_, 0);
				} else {
					int i_183_ = bool ? 0 : 1;
					Class324.method3378(i, i_183_, i_179_ >> 6 & 0x3, aByteArrayArrayArray1102, anInt1099, i_176_, i_174_, i_181_, 122, anIntArray1109[i_178_], i_175_, var_ha, i_177_);
				}
			}
		}
		if (is != null) {
			int i_184_;
			if (i_175_ == 1)
				i_184_ = i;
			else
				i_184_ = i + i_175_ - 1;
			int i_185_;
			if (i_176_ == 1)
				i_185_ = i_174_;
			else
				i_185_ = i_174_ + i_176_ - 1;
			for (int i_186_ = 0; i_186_ < is.length; i_186_++) {
				int i_187_ = is_180_[i_186_] & 0x3f;
				if (i_187_ == 0 || i_187_ == 2 || i_187_ == 3 || i_187_ == 9) {
					ObjectDefinition class70 = aClass38_1091.getObjectDefinition(is[i_186_] & 0xffff);
					if (class70.anInt765 == -1) {
						int i_188_ = -3355444;
						if (class70.interactonType == 1)
							i_188_ = -3407872;
						int i_189_ = is_180_[i_186_] >> 6 & 0x3;
						if (i_187_ == 0) {
							if (i_189_ == 0)
								var_ha.P(i, i_174_, i_176_, i_188_, 0);
							else if (i_189_ == 1)
								var_ha.U(i, i_174_, i_175_, i_188_, 0);
							else if (i_189_ == 2)
								var_ha.P(i_184_, i_174_, i_176_, i_188_, 0);
							else
								var_ha.U(i, i_185_, i_175_, i_188_, 0);
						} else if (i_187_ == 2) {
							if (i_189_ == 0) {
								var_ha.P(i, i_174_, i_176_, -1, 0);
								var_ha.U(i, i_174_, i_175_, i_188_, 0);
							} else if (i_189_ == 1) {
								var_ha.P(i_184_, i_174_, i_176_, -1, 0);
								var_ha.U(i, i_174_, i_175_, i_188_, 0);
							} else if (i_189_ == 2) {
								var_ha.P(i_184_, i_174_, i_176_, -1, 0);
								var_ha.U(i, i_185_, i_175_, i_188_, 0);
							} else {
								var_ha.P(i, i_174_, i_176_, -1, 0);
								var_ha.U(i, i_185_, i_175_, i_188_, 0);
							}
						} else if (i_187_ == 3) {
							if (i_189_ == 0)
								var_ha.U(i, i_174_, 1, i_188_, 0);
							else if (i_189_ == 1)
								var_ha.U(i_184_, i_174_, 1, i_188_, 0);
							else if (i_189_ == 2)
								var_ha.U(i_184_, i_185_, 1, i_188_, 0);
							else
								var_ha.U(i, i_185_, 1, i_188_, 0);
						} else if (i_187_ == 9) {
							if (i_189_ == 0 || i_189_ == 2) {
								for (int i_190_ = 0; i_190_ < i_176_; i_190_++)
									var_ha.U(i + i_190_, i_185_ - i_190_, 1, i_188_, 0);
							} else {
								for (int i_191_ = 0; i_191_ < i_176_; i_191_++)
									var_ha.U(i + i_191_, i_174_ + i_191_, 1, i_188_, 0);
							}
						}
					}
				}
			}
		}
	}

	static final Class296_Sub39_Sub14 method936(int i, int i_192_) {
		for (Class296_Sub39_Sub14 class296_sub39_sub14 = (Class296_Sub39_Sub14) aClass263_1098.getFirst(true); class296_sub39_sub14 != null; class296_sub39_sub14 = (Class296_Sub39_Sub14) aClass263_1098.getNext(0)) {
			if (class296_sub39_sub14.aBoolean6210 && class296_sub39_sub14.method2879(i_192_, i, -4))
				return class296_sub39_sub14;
		}
		return null;
	}

	private static final void method937(ha var_ha, Packet class296_sub17, int i, int i_193_, int i_194_, int i_195_, int[] is, int[] is_196_) {
		int i_197_ = class296_sub17.g1();
		if ((i_197_ & 0x1) == 0) {
			boolean bool = (i_197_ & 0x2) == 0;
			int i_198_ = i_197_ >> 2 & 0x3f;
			if (i_198_ != 62) {
				if (i_198_ == 63)
					i_198_ = class296_sub17.g1();
				else if (bool)
					i_198_ = is[i_198_];
				else
					i_198_ = is_196_[i_198_];
				if (bool) {
					aByteArray1118[i_194_ + i_195_ * anInt1116] = (byte) i_198_;
					aByteArray1122[i_194_ + i_195_ * anInt1116] = (byte) 0;
				} else {
					aByteArray1122[i_194_ + i_195_ * anInt1116] = (byte) i_198_;
					aByteArray1124[i_194_ + i_195_ * anInt1116] = (byte) 0;
					aByteArray1118[i_194_ + i_195_ * anInt1116] = class296_sub17.g1b();
				}
			}
		} else {
			int i_199_ = (i_197_ >> 1 & 0x3) + 1;
			boolean bool = (i_197_ & 0x8) != 0;
			boolean bool_200_ = (i_197_ & 0x10) != 0;
			for (int i_201_ = 0; i_201_ < i_199_; i_201_++) {
				int i_202_ = class296_sub17.g1();
				int i_203_ = 0;
				int i_204_ = 0;
				if (bool) {
					i_203_ = class296_sub17.g1();
					i_204_ = class296_sub17.g1();
				}
				int i_205_ = 0;
				if (bool_200_)
					i_205_ = class296_sub17.g1();
				if (i_201_ == 0) {
					aByteArray1118[i_194_ + i_195_ * anInt1116] = (byte) i_202_;
					aByteArray1122[i_194_ + i_195_ * anInt1116] = (byte) i_203_;
					aByteArray1124[i_194_ + i_195_ * anInt1116] = (byte) i_204_;
					if (i_205_ == 1) {
						aShortArray1113[i_194_ + i_195_ * anInt1116] = (short) (class296_sub17.g2() + 1);
						aByteArray1115[i_194_ + i_195_ * anInt1116] = class296_sub17.g1b();
					} else if (i_205_ > 1) {
						aShortArray1113[i_194_ + i_195_ * anInt1116] = (short) -1;
						short[] is_206_ = new short[i_205_];
						byte[] is_207_ = new byte[i_205_];
						for (int i_208_ = 0; i_208_ < i_205_; i_208_++) {
							is_206_[i_208_] = (short) class296_sub17.g2();
							is_207_[i_208_] = class296_sub17.g1b();
						}
						aClass263_1125.put((long) (i_194_ << 16 | i_195_), new Class296_Sub12(is_206_, is_207_));
					}
				} else {
					short[] is_209_ = null;
					byte[] is_210_ = null;
					if (i_205_ > 0) {
						is_209_ = new short[i_205_];
						is_210_ = new byte[i_205_];
						for (int i_211_ = 0; i_211_ < i_205_; i_211_++) {
							is_209_[i_211_] = (short) class296_sub17.g2();
							is_210_[i_211_] = class296_sub17.g1b();
						}
					}
					if ((aClass404ArrayArrayArray1110[i_201_ - 1][i - (anInt1111 >> 6)][i_193_ - (anInt1117 >> 6)]) == null)
						aClass404ArrayArrayArray1110[i_201_ - 1][i - (anInt1111 >> 6)][i_193_ - (anInt1117 >> 6)] = new Class404();
					Class338_Sub4 class338_sub4 = new Class338_Sub4(i_194_ & 0x3f, i_195_ & 0x3f, i_202_, i_203_, i_204_, is_209_, is_210_);
					aClass404ArrayArrayArray1110[i_201_ - 1][i - (anInt1111 >> 6)][i_193_ - (anInt1117 >> 6)].method4158(class338_sub4, 1);
				}
			}
		}
	}

	static final void method938() {
		aByteArray1118 = null;
		aByteArray1127 = null;
		aShortArray1119 = null;
		aByteArray1122 = null;
		aByteArray1124 = null;
		aShortArray1113 = null;
		aByteArray1115 = null;
		aClass263_1125 = null;
		aClass404ArrayArrayArray1110 = null;
		anIntArray1109 = null;
	}

	private static final void method939() {
		for (int i = 0; i < anInt1116; i++) {
			for (int i_212_ = 0; i_212_ < anInt1114; i_212_++) {
				int i_213_ = aShortArray1113[i + i_212_ * anInt1116] & 0xffff;
				if (i_213_ != 0) {
					if (i_213_ == 65535) {
						Class296_Sub12 class296_sub12 = ((Class296_Sub12) aClass263_1125.get((long) (i << 16 | i_212_)));
						if (class296_sub12 != null) {
							for (int i_214_ = 0; (i_214_ < class296_sub12.aShortArray4651.length); i_214_++) {
								ObjectDefinition class70 = (aClass38_1091.getObjectDefinition((class296_sub12.aShortArray4651[i_214_] & 0xffff)));
								int i_215_ = class70.anInt759;
								if (class70.transforms != null) {
									class70 = class70.method757(anInterface17_1090, false);
									if (class70 != null)
										i_215_ = class70.anInt759;
								}
								if (i_215_ != -1) {
									Class296_Sub53 class296_sub53 = new Class296_Sub53(i_215_);
									class296_sub53.anInt5047 = i;
									class296_sub53.anInt5042 = i_212_;
									aClass155_1101.addLast((byte) -93, class296_sub53);
								}
							}
						}
					} else {
						ObjectDefinition class70 = aClass38_1091.getObjectDefinition(i_213_ - 1);
						int i_216_ = class70.anInt759;
						if (class70.transforms != null) {
							class70 = class70.method757(anInterface17_1090, false);
							if (class70 != null)
								i_216_ = class70.anInt759;
						}
						if (i_216_ != -1) {
							Class296_Sub53 class296_sub53 = new Class296_Sub53(i_216_);
							class296_sub53.anInt5047 = i;
							class296_sub53.anInt5042 = i_212_;
							aClass155_1101.addLast((byte) 112, class296_sub53);
						}
					}
				}
			}
		}
		for (int i = 0; i < 3; i++) {
			for (int i_217_ = 0; i_217_ < aClass404ArrayArrayArray1110[0].length; i_217_++) {
				for (int i_218_ = 0; i_218_ < aClass404ArrayArrayArray1110[0][0].length; i_218_++) {
					Class404 class404 = aClass404ArrayArrayArray1110[i][i_217_][i_218_];
					if (class404 != null) {
						for (Class338_Sub4 class338_sub4 = ((Class338_Sub4) class404.method4160((byte) -96)); class338_sub4 != null; class338_sub4 = ((Class338_Sub4) class404.method4163(-24917))) {
							if (class338_sub4.aShortArray5215 != null) {
								for (int i_219_ = 0; (i_219_ < class338_sub4.aShortArray5215.length); i_219_++) {
									ObjectDefinition class70 = (aClass38_1091.getObjectDefinition((class338_sub4.aShortArray5215[i_219_]) & 0xffff));
									int i_220_ = class70.anInt759;
									if (class70.transforms != null) {
										class70 = (class70.method757(anInterface17_1090, false));
										if (class70 != null)
											i_220_ = class70.anInt759;
									}
									if (i_220_ != -1) {
										Class296_Sub53 class296_sub53 = new Class296_Sub53(i_220_);
										class296_sub53.anInt5047 = ((i_217_ + (anInt1111 >> 6)) * 64 + class338_sub4.aByte5217 - anInt1111);
										class296_sub53.anInt5042 = ((i_218_ + (anInt1117 >> 6)) * 64 + class338_sub4.aByte5220 - anInt1117);
										aClass155_1101.addLast((byte) 115, class296_sub53);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	static {
		aByteArray1105 = new byte[1];
		aShortArray1108 = new short[1];
		aClass155_1101 = new NodeDeque();
	}
}
