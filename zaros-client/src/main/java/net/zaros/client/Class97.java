package net.zaros.client;

import net.zaros.client.configs.objtype.ObjTypeList;

/* Class97 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class97 {
	static int anInt1050;
	static byte aByte1051;

	static final boolean method872(int i, String string) {
		if (string == null) {
			return false;
		}
		for (int i_0_ = i; Class285.anInt2625 > i_0_; i_0_++) {
			if (string.equalsIgnoreCase(Js5TextureLoader.aStringArray3443[i_0_])) {
				return true;
			}
		}
		if (string.equalsIgnoreCase(Class296_Sub51_Sub11.localPlayer.displayName)) {
			return true;
		}
		return false;
	}

	static final void method873(int i, int i_1_, int i_2_, Class296_Sub39_Sub9 class296_sub39_sub9) {
		if (class296_sub39_sub9 != null && class296_sub39_sub9 != HardReferenceWrapper.aClass155_6698.aClass296_1586) {
			int dstX = class296_sub39_sub9.anInt6172;
			int dstY = class296_sub39_sub9.anInt6164;
			int i_5_ = 90 % ((30 - i_2_) / 55);
			int opcode = class296_sub39_sub9.anInt6165;
			int clickType = (int) class296_sub39_sub9.aLong6162;
			if (opcode >= 2000) {
				opcode -= 2000;
			}
			long hash = class296_sub39_sub9.aLong6162;
			/*if (Loader.isShifting && dstY == 44498944) {//TODO DROP_ITEM
				opcode = 23;
				clickType = 0;
				System.out.println(i + ", " + i_1_ + ", " + i_2_);
				InterfaceComponent clickedComponent = Class103.method894(0, dstY, dstX);
				ItemDefinition itemDefinition = Class296_Sub39_Sub1.itemDefinitionLoader.getItemDefinition(clickedComponent.clickedItem);
				String[] previousActions = itemDefinition.actions;
				itemDefinition.actions = new String[] {  };
				if (clickedComponent != null) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 127, Loader.DROP_ITEM_PACKET);
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -119, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				opcode = -1;//restart client
				clickType = -1;//agai
			}*/
			if (opcode == 53) {
				InterfaceComponent class51 = Class103.method894(0, dstY, dstX);
				if (class51 != null) {
					Class285.method2368(0);
					InterfaceComponentSettings class296_sub31 = GameClient.method115(class51);
					Class3.method172(class296_sub31.originalHash, class296_sub31.method2707(-118), class51, 22635);
					Class228.aString2200 = Class368_Sub19.method3856(class51, 119);
					ConfigsRegister.aString3672 = class51.componentName + "<col=ffffff>";
					if (Class228.aString2200 == null) {
						Class228.aString2200 = "Null";
					}
				}
			} else {
				OutgoingPacket class311 = null;
				if (opcode != 2) {
					if (opcode == 15) {
						class311 = Class296_Sub27.aClass311_4779;
					} else if (opcode != 58) {
						if (opcode != 11) {
							if (opcode != 13) {
								if (opcode == 1003) {
									class311 = Class41_Sub28.aClass311_3812;
								}
							} else {
								class311 = Class366_Sub2.aClass311_5367;
							}
						} else {
							class311 = Class44_Sub1_Sub1.aClass311_5810;
						}
					} else {
						class311 = Class296_Sub32.aClass311_4836;
					}
				} else {
					class311 = Class151.aClass311_1554;
				}
				if (class311 != null) {
					Class183.anInt1880 = 2;
					Class100.anInt1068 = i;
					Class41_Sub14.anInt3775 = 0;
					Class401.anInt3366 = i_1_;
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 95, class311);
					class296_sub1.out.p2(dstY + Class41_Sub26.worldBaseY);
					class296_sub1.out.p2(clickType);
					class296_sub1.out.p2(Class206.worldBaseX + dstX);
					class296_sub1.out.p1(!Class2.aClass166_66.method1637(82, 61) ? 0 : 1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					Class241.findPathToItem(dstY, dstX);
				}
				if (opcode == 48) {
					Class41_Sub14.anInt3775 = 0;
					Class401.anInt3366 = i_1_;
					Class100.anInt1068 = i;
					Class183.anInt1880 = 2;
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 116, Class296_Sub34_Sub2.aClass311_6104);
					class296_sub1.out.writeLEShortA(dstY + Class41_Sub26.worldBaseY);
					class296_sub1.out.p2(Class206.worldBaseX + dstX);
					class296_sub1.out.writeLEShort(Class180.anInt1857);
					class296_sub1.out.writeInt_v2(Class366_Sub4.anInt5375);
					class296_sub1.out.p2(Class69.anInt3689);
					class296_sub1.out.method2603(!Class2.aClass166_66.method1637(82, 97) ? 0 : 1);
					class296_sub1.out.p4((int) (hash >>> 32) & 0x7fffffff);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					Class352.findPathToObject(hash, dstY, dstX);
				}
				OutgoingPacket packet = null;
				if (opcode != 47) {
					if (opcode == 19) {
						packet = Class302.aClass311_2720;
					} else if (opcode != 21) {
						if (opcode == 3) {
							packet = Class225.aClass311_2178;
						} else if (opcode == 16) {
							packet = Class243.aClass311_2315;
						} else if (opcode != 50) {
							if (opcode != 46) {
								if (opcode != 8) {
									if (opcode == 25) {
										packet = Class347.aClass311_3028;
									} else if (opcode == 6) {
										packet = Class205_Sub2.aClass311_5628;
									}
								} else {
									packet = OutgoingPacket.aClass311_3635;
								}
							} else {
								packet = Class382.aClass311_3240;
							}
						} else {
							packet = Node.aClass311_2696;
						}
					} else {
						packet = Class177.aClass311_1846;
					}
				} else {
					packet = StaticMethods.aClass311_6071;
				}
				if (packet != null) {
					Player target = PlayerUpdate.visiblePlayers[clickType];
					if (target != null) {
						Class183.anInt1880 = 2;
						Class296_Sub51_Sub16.anInt6427++;
						Class41_Sub14.anInt3775 = 0;
						Class401.anInt3366 = i_1_;
						Class100.anInt1068 = i;
						Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 89, packet);
						class296_sub1.out.p2(clickType);
						class296_sub1.out.p1(!Class2.aClass166_66.method1637(82, 81) ? 0 : 1);
						Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
						Class405.findPathStandart(-2, target.wayPointQueueY[0], target.waypointQueueX[0], target.getSize(), target.getSize(), 0, 0, true);
					}
				}
				if (opcode == 1008 || opcode == 1010 || opcode == 1007 || opcode == 1011 || opcode == 1001) {
					Class31.method335(dstX, -10322, opcode, clickType);
				}
				if (opcode == 30 && Player.aClass51_6872 == null) {
					Class296_Sub51_Sub1.method3080(dstY, dstX, -128);
					Player.aClass51_6872 = Class103.method894(0, dstY, dstX);
					Class332.method3416(Player.aClass51_6872, (byte) 88);
				}
				if (opcode == 5) {
					Player target = PlayerUpdate.visiblePlayers[clickType];
					if (target != null) {
						Class401.anInt3366 = i_1_;
						Class183.anInt1880 = 2;
						Class123_Sub1_Sub2.anInt5816++;
						Class100.anInt1068 = i;
						Class41_Sub14.anInt3775 = 0;
						Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 94, Class296_Sub39_Sub7.aClass311_6157);
						class296_sub1.out.writeLEShort(clickType);
						class296_sub1.out.p1(!Class2.aClass166_66.method1637(82, 75) ? 0 : 1);
						class296_sub1.out.writeShortA(Class69.anInt3689);
						class296_sub1.out.writeLEInt(Class366_Sub4.anInt5375);
						class296_sub1.out.writeLEShortA(Class180.anInt1857);
						Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
						Class405.findPathStandart(-2, target.wayPointQueueY[0], target.waypointQueueX[0], target.getSize(), target.getSize(), 0, 0, true);
					}
				}
				OutgoingPacket class311_9_ = null;
				if (opcode == 45) {
					class311_9_ = ha_Sub3.aClass311_4118;
				} else if (opcode == 22) {
					class311_9_ = Class33.aClass311_335;
				} else if (opcode == 4) {
					class311_9_ = Class191.aClass311_1947;
				} else if (opcode == 57) {
					class311_9_ = Class241_Sub1.aClass311_4574;
				} else if (opcode == 1012) {
					class311_9_ = Class365.aClass311_3116;
				} else if (opcode == 1009) {
					class311_9_ = Class296_Sub39_Sub15.aClass311_6224;
				}
				if (class311_9_ != null) {
					Class100.anInt1068 = i;
					Class401.anInt3366 = i_1_;
					Class183.anInt1880 = 2;
					Class41_Sub14.anInt3775 = 0;
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 116, class311_9_);
					class296_sub1.out.writeShortA(dstY + Class41_Sub26.worldBaseY);
					class296_sub1.out.writeLEShortA(Class206.worldBaseX + dstX);
					class296_sub1.out.p4((int) (hash >>> 32) & 0x7fffffff);
					class296_sub1.out.p1(!Class2.aClass166_66.method1637(82, 90) ? 0 : 1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					Class352.findPathToObject(hash, dstY, dstX);
				}
				OutgoingPacket class311_10_ = null;
				if (opcode == 51) {
					class311_10_ = Class241_Sub1.aClass311_4575;
				} else if (opcode != 9) {
					if (opcode == 17) {
						class311_10_ = Class78.aClass311_3427;
					} else if (opcode != 12) {
						if (opcode == 60) {
							class311_10_ = BITConfigsLoader.aClass311_2206;
						} else if (opcode == 1004) {
							class311_10_ = aa.aClass311_48;
						}
					} else {
						class311_10_ = Class296_Sub30.aClass311_4818;
					}
				} else {
					class311_10_ = Class360_Sub6.aClass311_5330;
				}
				if (class311_10_ != null) {
					NPCNode target = (NPCNode) Class41_Sub18.localNpcs.get(clickType);
					if (target != null) {
						Class41_Sub14.anInt3775 = 0;
						Class183.anInt1880 = 2;
						NPC class338_sub3_sub1_sub3_sub2 = target.value;
						Class100.anInt1068 = i;
						Class401.anInt3366 = i_1_;
						Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 123, class311_10_);
						class296_sub1.out.writeLEShortA(clickType);
						class296_sub1.out.writeByteC(Class2.aClass166_66.method1637(82, 118) ? 1 : 0);
						Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
						Class405.findPathStandart(-2, class338_sub3_sub1_sub3_sub2.wayPointQueueY[0], class338_sub3_sub1_sub3_sub2.waypointQueueX[0], class338_sub3_sub1_sub3_sub2.getSize(), class338_sub3_sub1_sub3_sub2.getSize(), 0, 0, true);
					}
				}

				if (opcode == 23 || opcode == 1006) {
					Class296_Sub39_Sub18.interfaceClicked(clickType, dstY, class296_sub39_sub9.aString6166, dstX);
				}
				if (opcode == 18) {
					if (Class338_Sub3_Sub5.rights <= 0 || !Class2.aClass166_66.method1637(82, 44) || !Class2.aClass166_66.method1637(81, 118)) {
						Class183.anInt1880 = 1;
						Class401.anInt3366 = i_1_;
						Class100.anInt1068 = i;
						Class41_Sub14.anInt3775 = 0;
						Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 108, Class296_Sub39_Sub20_Sub1.aClass311_6723);
						class296_sub1.out.writeLEShort(dstY + Class41_Sub26.worldBaseY);
						class296_sub1.out.writeLEShort(dstX + Class206.worldBaseX);
						Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					} else {
						Class360_Sub3.method3741(Class41_Sub26.worldBaseY + dstY, Class296_Sub51_Sub11.localPlayer.z, (byte) 106, Class206.worldBaseX + dstX);
					}
				}
				if (opcode == 44) {
					if (Class338_Sub3_Sub5.rights <= 0 || !Class2.aClass166_66.method1637(82, 63) || !Class2.aClass166_66.method1637(81, 127)) {
						Class296_Sub1 class296_sub1 = Class296_Sub51_Sub14.method3118(dstX, clickType, (byte) -73, dstY);
						if (clickType != 1) {
							Class41_Sub14.anInt3775 = 0;
							Class100.anInt1068 = i;
							Class401.anInt3366 = i_1_;
							Class183.anInt1880 = 1;
						} else {
							class296_sub1.out.p1(-1);
							class296_sub1.out.p1(-1);
							class296_sub1.out.p2((int) Class41_Sub26.aFloat3806);
							class296_sub1.out.p1(57);
							class296_sub1.out.p1(StaticMethods.anInt6075);
							class296_sub1.out.p1(ObjTypeList.anInt1320);
							class296_sub1.out.p1(89);
							class296_sub1.out.p2(Class296_Sub51_Sub11.localPlayer.tileX);
							class296_sub1.out.p2(Class296_Sub51_Sub11.localPlayer.tileY);
							class296_sub1.out.p1(63);
						}
						Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
						Class405.findPathStandart(-4, dstY, dstX, 1, 1, 0, 0, true);
					} else {
						Class360_Sub3.method3741(dstY + Class41_Sub26.worldBaseY, Class296_Sub51_Sub11.localPlayer.z, (byte) 126, dstX + Class206.worldBaseX);
					}
				}
				if (opcode == 59) {
					NPCNode class296_sub7 = (NPCNode) Class41_Sub18.localNpcs.get(clickType);
					if (class296_sub7 != null) {
						Class41_Sub14.anInt3775 = 0;
						Class100.anInt1068 = i;
						NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
						Class401.anInt3366 = i_1_;
						Class183.anInt1880 = 2;
						Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 118, StaticMethods.aClass311_6080);
						class296_sub1.out.method2603(!Class2.aClass166_66.method1637(82, 51) ? 0 : 1);
						class296_sub1.out.writeLEShort(Class69.anInt3689);
						class296_sub1.out.method2570(Class366_Sub4.anInt5375);
						class296_sub1.out.writeLEShort(Class180.anInt1857);
						class296_sub1.out.writeLEShort(clickType);
						Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
						Class405.findPathStandart(-2, class338_sub3_sub1_sub3_sub2.wayPointQueueY[0], class338_sub3_sub1_sub3_sub2.waypointQueueX[0], class338_sub3_sub1_sub3_sub2.getSize(), class338_sub3_sub1_sub3_sub2.getSize(), 0, 0, true);
					}
				}
				if (opcode == 49) {
					Class100.anInt1068 = i;
					Class401.anInt3366 = i_1_;
					Class183.anInt1880 = 1;
					Class41_Sub14.anInt3775 = 0;
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 104, Class373_Sub1.aClass311_5578);
					class296_sub1.out.writeLEShort(dstY + Class41_Sub26.worldBaseY);
					class296_sub1.out.writeLEShort(Class69.anInt3689);
					class296_sub1.out.writeLEShortA(Class180.anInt1857);
					class296_sub1.out.p2(dstX + Class206.worldBaseX);
					class296_sub1.out.writeInt_v2(Class366_Sub4.anInt5375);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					Class405.findPathStandart(-4, dstY, dstX, 1, 1, 0, 0, true);
				}
				if (opcode == 52) {
					Class100.anInt1068 = i;
					Class401.anInt3366 = i_1_;
					Class183.anInt1880 = 2;
					Class41_Sub14.anInt3775 = 0;
					Class123_Sub1_Sub2.anInt5816++;
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 102, Class296_Sub39_Sub7.aClass311_6157);
					class296_sub1.out.writeLEShort(Class296_Sub51_Sub11.localPlayer.index);
					class296_sub1.out.p1(Class2.aClass166_66.method1637(82, 56) ? 1 : 0);
					class296_sub1.out.writeShortA(Class69.anInt3689);
					class296_sub1.out.writeLEInt(Class366_Sub4.anInt5375);
					class296_sub1.out.writeLEShortA(Class180.anInt1857);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (opcode == 10) {
					Class183.anInt1880 = 2;
					Class401.anInt3366 = i_1_;
					Class100.anInt1068 = i;
					Class41_Sub14.anInt3775 = 0;
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 108, Class254.aClass311_3598);
					class296_sub1.out.writeLEShortA(Class180.anInt1857);
					class296_sub1.out.p4(Class366_Sub4.anInt5375);
					class296_sub1.out.p2(Class69.anInt3689);
					class296_sub1.out.writeLEShortA(Class206.worldBaseX + dstX);
					class296_sub1.out.writeLEShortA(Class41_Sub26.worldBaseY + dstY);
					class296_sub1.out.p1(Class2.aClass166_66.method1637(82, 50) ? 1 : 0);
					class296_sub1.out.writeLEShortA(clickType);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					Class241.findPathToItem(dstY, dstX);
				}
				if (opcode == 20) {
					InterfaceComponent class51 = Class103.method894(0, dstY, dstX);
					if (class51 != null) {
						Class338_Sub3_Sub1_Sub5.method3548((byte) 85, class51);
					}
				}
				if (Class127.aBoolean1304) {
					Class285.method2368(0);
				}
				if (StaticMethods.aClass51_5944 != null && Class246.anInt2335 == 0) {
					Class332.method3416(StaticMethods.aClass51_5944, (byte) 125);
				}
			}
		}
	}

	static final void method874(byte i) {
		if (i != -100) {
			method872(21, null);
		}
		Class391.anInt3299 = Class338_Sub10.anInt5270 = Class156.anInt3594 = Class27.anInt295 = 0;
	}
}
