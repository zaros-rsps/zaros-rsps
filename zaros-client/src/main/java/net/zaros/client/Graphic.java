package net.zaros.client;
/* Class23 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Graphic {
	private int anInt255 = 0;
	private int anInt256 = 0;
	private int anInt257;
	private int anInt258;
	private short[] aShortArray259;
	byte aByte260 = 0;
	static int anInt261 = 52;
	int gfxID;
	GraphicsLoader loader;
	int anInt264 = -1;
	private int anInt265;
	private short[] aShortArray266;
	boolean aBoolean267;
	private short[] aShortArray268;
	private int anInt269;
	static int lookatY;
	private short[] aShortArray271;
	private int anInt272;

	final Model method291(ha var_ha, byte i, byte i_0_, Animator class44, int i_1_) {
		int i_2_ = 118 % ((i - 25) / 33);
		return method295(0, 113, null, class44, 0, i_1_, i_0_, var_ha, null, 0, false);
	}

	static final void method292(byte i) {
		Class36.anInt356 = (int) (Class198.currentMapSizeX * 34.46);
		Class389.anInt3281 = 200;
		Class36.anInt356 <<= 2;
		int i_3_ = 74 % ((23 - i) / 52);
		if (Class41_Sub13.aHa3774.x()) {
			Class36.anInt356 += 512;
		}
		Class41.method384(false, false);
	}

	final void init(Packet buff, boolean newGfx) {
		if (newGfx) {
			buff.pos += 3;
		}
		for (;;) {
			int i_5_ = buff.g1();
			if (i_5_ == 0) {
				break;
			}
			parseOpcode(i_5_, buff, newGfx);
		}
	}

	private final void parseOpcode(int opcode, Packet buffer, boolean newGfx) {
		if (opcode == 44 || opcode == 45) {
			buffer.g2();
		} else if (opcode == 1) {
			anInt269 = newGfx ? buffer.gSmart2or4s() : buffer.g2();
		} else if (opcode == 2) {
			anInt264 = newGfx ? buffer.gSmart2or4s() : buffer.g2();
		} else if (opcode == 4) {
			anInt257 = buffer.g2();
		} else if (opcode == 5) {
			anInt272 = buffer.g2();
		} else if (opcode != 6) {
			if (opcode != 7) {
				if (opcode == 8) {
					anInt255 = buffer.g1();
				} else if (opcode == 9) {
					aByte260 = (byte) 3;
					anInt265 = 8224;
				} else if (opcode != 10) {
					if (opcode != 11) {
						if (opcode != 12) {
							if (opcode == 13) {
								aByte260 = (byte) 5;
							} else if (opcode == 14) {
								aByte260 = (byte) 2;
								anInt265 = buffer.g1() * 256;
							} else if (opcode != 15) {
								if (opcode == 16) {
									aByte260 = (byte) 3;
									anInt265 = buffer.g4();
								} else if (opcode != 40) {
									if (opcode == 41) {
										int i_7_ = buffer.g1();
										aShortArray268 = new short[i_7_];
										aShortArray266 = new short[i_7_];
										for (int i_8_ = 0; i_8_ < i_7_; i_8_++) {
											aShortArray266[i_8_] = (short) buffer.g2();
											aShortArray268[i_8_] = (short) buffer.g2();
										}
									}
								} else {
									int i_9_ = buffer.g1();
									aShortArray259 = new short[i_9_];
									aShortArray271 = new short[i_9_];
									for (int i_10_ = 0; i_10_ < i_9_; i_10_++) {
										aShortArray271[i_10_] = (short) buffer.g2();
										aShortArray259[i_10_] = (short) buffer.g2();
									}
								}
							} else {
								aByte260 = (byte) 3;
								anInt265 = buffer.g2();
							}
						} else {
							aByte260 = (byte) 4;
						}
					} else {
						aByte260 = (byte) 1;
					}
				} else {
					aBoolean267 = true;
				}
			} else {
				anInt256 = buffer.g1();
			}
		} else {
			anInt258 = buffer.g2();
		}
	}

	final Model method295(int i, int i_11_, s var_s, Animator class44, int i_12_, int i_13_, byte i_14_, ha var_ha, s var_s_15_, int i_16_, boolean bool) {
		int i_17_ = i_13_;
		bool = bool & aByte260 != 0;
		if (class44 != null) {
			i_17_ |= class44.method568(0);
		}
		if (bool) {
			i_17_ = i_17_ | (aByte260 == 3 ? 7 : 2);
		}
		if (anInt272 != 128) {
			i_17_ |= 0x2;
		}
		if (anInt257 != 128 || anInt258 != 0) {
			i_17_ |= 0x5;
		}
		Model class178;
		synchronized (loader.modelcache) {
			class178 = (Model) loader.modelcache.get(gfxID |= var_ha.anInt1295 << 29);
			int i_18_ = 16 % ((i_11_ - 75) / 37);
		}
		if (class178 == null || var_ha.e(class178.ua(), i_17_) != 0) {
			if (class178 != null) {
				i_17_ = var_ha.d(i_17_, class178.ua());
			}
			int i_19_ = i_17_;
			if (aShortArray271 != null) {
				i_19_ |= 0x4000;
			}
			if (aShortArray266 != null) {
				i_19_ |= 0x8000;
			}
			Mesh class132 = Class296_Sub51_Sub1.fromJs5(loader.modelCache, anInt269, 0);
			if (class132 == null) {
				return null;
			}
			if (class132.version_number < 13) {
				class132.scale(2);
			}
			class178 = var_ha.a(class132, i_19_, loader.anInt2482, anInt256 + 64, anInt255 + 850);
			if (aShortArray271 != null) {
				for (int i_20_ = 0; i_20_ < aShortArray271.length; i_20_++) {
					class178.ia(aShortArray271[i_20_], aShortArray259[i_20_]);
				}
			}
			if (aShortArray266 != null) {
				for (int i_21_ = 0; aShortArray266.length > i_21_; i_21_++) {
					class178.aa(aShortArray266[i_21_], aShortArray268[i_21_]);
				}
			}
			class178.s(i_17_);
			synchronized (loader.modelcache) {
				loader.modelcache.put(class178, gfxID |= var_ha.anInt1295 << 29);
			}
		}
		Model class178_22_ = class178.method1728(i_14_, i_17_, true);
		if (class44 != null) {
			class44.method556(class178_22_, 0, (byte) -63);
		}
		if (anInt257 != 128 || anInt272 != 128) {
			class178_22_.O(anInt257, anInt272, anInt257);
		}
		if (anInt258 != 0) {
			if (anInt258 == 90) {
				class178_22_.a(4096);
			}
			if (anInt258 == 180) {
				class178_22_.a(8192);
			}
			if (anInt258 == 270) {
				class178_22_.a(12288);
			}
		}
		if (bool) {
			class178_22_.p(aByte260, anInt265, var_s, var_s_15_, i, i_12_, i_16_);
		}
		class178_22_.s(i_13_);
		return class178_22_;
	}

	public Graphic() {
		anInt258 = 0;
		aBoolean267 = false;
		anInt265 = -1;
		anInt257 = 128;
		anInt272 = 128;
	}
}
