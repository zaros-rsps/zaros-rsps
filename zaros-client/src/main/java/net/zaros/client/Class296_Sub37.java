package net.zaros.client;

/* Class296_Sub37 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub37 extends Node {
	int anInt4881;
	int anInt4882;
	static Class81 aClass81_4883 = new Class81("", 21);
	static IncomingPacket MIDI_JINGLE;
	static boolean fromBilling = false;

	static final void method2766(byte i, int i_0_, int i_1_) {
		Class190 class190 = ClipData.aClass190ArrayArray184[i_0_][i_1_];
		if (class190 != null) {
			Class42_Sub4.anInt3849 = class190.anInt1944;
			HashTable.anInt2466 = class190.anInt1940;
			Class296_Sub38.anInt4900 = class190.anInt1937;
		}
		if (i <= -101)
			Class384.method4018((byte) -119);
	}

	public static void method2767(int i) {
		aClass81_4883 = null;
		MIDI_JINGLE = null;
		if (i <= 121)
			method2766((byte) -32, 61, -109);
	}

	Class296_Sub37(int i, int i_2_) {
		anInt4881 = i;
		anInt4882 = i_2_;
	}

	static {
		MIDI_JINGLE = new IncomingPacket(39, 6);
	}
}
