package net.zaros.client;

/* Class296_Sub51_Sub16 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub16 extends TextureOperation {
	static int anInt6427;

	public Class296_Sub51_Sub16() {
		super(3, false);
	}

	static final Class210_Sub1 method3121(boolean bool) {
		if (bool != true)
			return null;
		Class69_Sub1_Sub1.anInt6689 = 0;
		return Class338_Sub8_Sub1.method3607((byte) 126);
	}

	final int[][] get_colour_output(int i, int i_0_) {
		int[][] is = aClass86_5034.method823((byte) 78, i);
		if (i_0_ != 17621)
			return null;
		if (aClass86_5034.aBoolean939) {
			int[] is_1_ = this.method3064(2, i_0_ ^ 0x44d5, i);
			int[][] is_2_ = this.method3075((byte) -65, 0, i);
			int[][] is_3_ = this.method3075((byte) -75, 1, i);
			int[] is_4_ = is[0];
			int[] is_5_ = is[1];
			int[] is_6_ = is[2];
			int[] is_7_ = is_2_[0];
			int[] is_8_ = is_2_[1];
			int[] is_9_ = is_2_[2];
			int[] is_10_ = is_3_[0];
			int[] is_11_ = is_3_[1];
			int[] is_12_ = is_3_[2];
			for (int i_13_ = 0; Class41_Sub10.anInt3769 > i_13_; i_13_++) {
				int i_14_ = is_1_[i_13_];
				if (i_14_ != 4096) {
					if (i_14_ != 0) {
						int i_15_ = -i_14_ + 4096;
						is_4_[i_13_] = (i_14_ * is_7_[i_13_] + i_15_ * is_10_[i_13_] >> 12);
						is_5_[i_13_] = (i_14_ * is_8_[i_13_] + i_15_ * is_11_[i_13_] >> 12);
						is_6_[i_13_] = (is_12_[i_13_] * i_15_ + i_14_ * is_9_[i_13_] >> 12);
					} else {
						is_4_[i_13_] = is_10_[i_13_];
						is_5_[i_13_] = is_11_[i_13_];
						is_6_[i_13_] = is_12_[i_13_];
					}
				} else {
					is_4_[i_13_] = is_7_[i_13_];
					is_5_[i_13_] = is_8_[i_13_];
					is_6_[i_13_] = is_9_[i_13_];
				}
			}
		}
		return is;
	}

	final int[] get_monochrome_output(int i, int i_16_) {
		int[] is = aClass318_5035.method3335(i_16_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int[] is_17_ = this.method3064(0, i, i_16_);
			int[] is_18_ = this.method3064(1, 0, i_16_);
			int[] is_19_ = this.method3064(2, 0, i_16_);
			for (int i_20_ = 0; i_20_ < Class41_Sub10.anInt3769; i_20_++) {
				int i_21_ = is_19_[i_20_];
				if (i_21_ != 4096) {
					if (i_21_ == 0)
						is[i_20_] = is_18_[i_20_];
					else
						is[i_20_] = ((4096 - i_21_) * is_18_[i_20_] + i_21_ * is_17_[i_20_]) >> 12;
				} else
					is[i_20_] = is_17_[i_20_];
			}
		}
		if (i != 0)
			return null;
		return is;
	}

	final void method3071(int i, Packet class296_sub17, int i_22_) {
		if (i_22_ == 0)
			monochromatic = class296_sub17.g1() == 1;
		if (i >= -84)
			method3121(false);
	}
}
