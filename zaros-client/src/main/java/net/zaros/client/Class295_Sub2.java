package net.zaros.client;

import jaclib.memory.Buffer;

final class Class295_Sub2 extends Class295 implements Interface5 {
	private int anInt3497;
	static int anInt3498 = -2;

	static final void method2425(String string, int i, byte i_0_) {
		int i_1_ = PlayerUpdate.visiblePlayersCount;
		int[] is = PlayerUpdate.visiblePlayersIndexes;
		boolean bool = false;
		if (i_0_ >= -73) {
			anInt3498 = -35;
		}
		for (int i_2_ = 0; i_2_ < i_1_; i_2_++) {
			Player class338_sub3_sub1_sub3_sub1 = PlayerUpdate.visiblePlayers[is[i_2_]];
			if (class338_sub3_sub1_sub3_sub1 != null && Class296_Sub51_Sub11.localPlayer != class338_sub3_sub1_sub3_sub1 && class338_sub3_sub1_sub3_sub1.displayName != null && class338_sub3_sub1_sub3_sub1.displayName.equalsIgnoreCase(string)) {
				OutgoingPacket class311 = null;
				if (i == 1) {
					class311 = StaticMethods.aClass311_6071;
				} else if (i != 4) {
					if (i != 5) {
						if (i == 6) {
							class311 = Node.aClass311_2696;
						} else if (i == 7) {
							class311 = Class382.aClass311_3240;
						} else if (i == 9) {
							class311 = Class347.aClass311_3028;
						}
					} else {
						class311 = Class243.aClass311_2315;
					}
				} else {
					class311 = Class225.aClass311_2178;
				}
				if (class311 != null) {
					Class296_Sub51_Sub16.anInt6427++;
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 119, class311);
					class296_sub1.out.p2(is[i_2_]);
					class296_sub1.out.p1(0);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				bool = true;
				break;
			}
		}
		if (!bool) {
			Class34.method351(4, true, TranslatableString.cantFind.getTranslation(Class394.langID) + string);
		}
	}

	@Override
	public final int method24(int i) {
		if (i >= -123) {
			return -91;
		}
		return anInt3497;
	}

	@Override
	public final void method25(byte[] is, byte i, int i_3_, int i_4_) {
		int i_5_ = -92 % ((i - 19) / 50);
		method2422(is, i_4_);
		anInt3497 = i_3_;
	}

	static final void method2426(int i, int i_6_, int i_7_, int i_8_) {
		int i_9_ = i + Class206.worldBaseX;
		int i_10_ = i_7_ + Class41_Sub26.worldBaseY;
		if (Class338_Sub2.aClass247ArrayArrayArray5195 != null && i >= 0 && i_7_ >= 0 && Class198.currentMapSizeX > i && i_7_ < Class296_Sub38.currentMapSizeY && (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026.method496(i_6_ + 124) != 0 || i_8_ == Class296_Sub51_Sub11.localPlayer.z)) {
			long l = i_9_ | i_8_ << 28 | i_10_ << 14;
			Class296_Sub56 class296_sub56 = (Class296_Sub56) Class296_Sub11.aClass263_4647.get(l);
			if (class296_sub56 == null) {
				Class360.method3728(i_8_, i, i_7_);
			} else {
				Class296_Sub2 class296_sub2 = (Class296_Sub2) class296_sub56.aClass155_5080.removeFirst((byte) 125);
				if (class296_sub2 == null) {
					Class360.method3728(i_8_, i, i_7_);
				} else {
					Class338_Sub3_Sub2_Sub1 class338_sub3_sub2_sub1 = (Class338_Sub3_Sub2_Sub1) Class360.method3728(i_8_, i, i_7_);
					if (class338_sub3_sub2_sub1 == null) {
						class338_sub3_sub2_sub1 = new Class338_Sub3_Sub2_Sub1(i << 9, Class360_Sub2.aSArray5304[i_8_].method3355(i_7_, (byte) -121, i), i_7_ << 9, i_8_, i_8_);
					} else {
						class338_sub3_sub2_sub1.anInt6854 = class338_sub3_sub2_sub1.anInt6853 = -1;
					}
					class338_sub3_sub2_sub1.anInt6848 = class296_sub2.anInt4591;
					class338_sub3_sub2_sub1.anInt6850 = class296_sub2.anInt4592;
					for (;;) {
						Class296_Sub2 class296_sub2_11_ = (Class296_Sub2) class296_sub56.aClass155_5080.removeNext(1001);
						if (class296_sub2_11_ == null) {
							break;
						}
						if (class296_sub2_11_.anInt4591 != class338_sub3_sub2_sub1.anInt6848) {
							class338_sub3_sub2_sub1.anInt6847 = class296_sub2_11_.anInt4592;
							class338_sub3_sub2_sub1.anInt6854 = class296_sub2_11_.anInt4591;
							for (;;) {
								Class296_Sub2 class296_sub2_12_ = (Class296_Sub2) class296_sub56.aClass155_5080.removeNext(1001);
								if (class296_sub2_12_ == null) {
									break;
								}
								if (class338_sub3_sub2_sub1.anInt6848 != class296_sub2_12_.anInt4591 && class296_sub2_12_.anInt4591 != class338_sub3_sub2_sub1.anInt6854) {
									class338_sub3_sub2_sub1.anInt6853 = class296_sub2_12_.anInt4591;
									class338_sub3_sub2_sub1.anInt6846 = class296_sub2_12_.anInt4592;
								}
							}
							break;
						}
					}
					int i_13_ = aa_Sub1.method155(-1537652855, i_8_, (i << 9) + 256, (i_7_ << 9) + 256);
					class338_sub3_sub2_sub1.tileY = i_7_ << 9;
					class338_sub3_sub2_sub1.aByte5203 = (byte) i_8_;
					class338_sub3_sub2_sub1.z = (byte) i_8_;
					class338_sub3_sub2_sub1.anInt6855 = i_6_;
					class338_sub3_sub2_sub1.tileX = i << 9;
					class338_sub3_sub2_sub1.anInt5213 = i_13_;
					if (r_Sub2.method2871(i_7_, i, (byte) -47)) {
						class338_sub3_sub2_sub1.aByte5203++;
					}
					Class306.method3283(i_8_, i, i_7_, i_13_, class338_sub3_sub2_sub1);
				}
			}
		}
	}

	@Override
	public final long method27(int i) {
		if (i <= 126) {
			method27(-9);
		}
		return aBuffer2692.getAddress();
	}

	@Override
	public final int method26(int i) {
		if (i != 19442) {
			anInt3498 = 70;
		}
		return 0;
	}

	Class295_Sub2(ha_Sub3 var_ha_Sub3, int i, Buffer buffer) {
		super(var_ha_Sub3, buffer);
		anInt3497 = i;
	}

	Class295_Sub2(ha_Sub3 var_ha_Sub3, int i, byte[] is, int i_14_) {
		super(var_ha_Sub3, is, i_14_);
		anInt3497 = i;
	}

	static final void method2427(int i) {
		Class106_Sub1.method943(false, 1);
		OutputStream_Sub1.anInt40 = 0;
		boolean ready = true;
		for (int i_15_ = 0; i_15_ < Class296_Sub51_Sub31.aByteArrayArray6509.length; i_15_++) {
			if (Class56.anIntArray659[i_15_] != -1 && Class296_Sub51_Sub31.aByteArrayArray6509[i_15_] == null) {
				Class296_Sub51_Sub31.aByteArrayArray6509[i_15_] = Class324.fs5.getFile(Class56.anIntArray659[i_15_], 0);
				if (Class296_Sub51_Sub31.aByteArrayArray6509[i_15_] == null) {
					ready = false;
					OutputStream_Sub1.anInt40++;
				}
			}
			if (Class4.anIntArray70[i_15_] != -1 && Class107_Sub1.aByteArrayArray5667[i_15_] == null) {
				Class107_Sub1.aByteArrayArray5667[i_15_] = Class324.fs5.getFile(Class4.anIntArray70[i_15_], 0, Class355_Sub2.xteaKeys[i_15_]);
				if (Class107_Sub1.aByteArrayArray5667[i_15_] == null) {
					OutputStream_Sub1.anInt40++;
					ready = false;
				}
			}
			if (Class188.anIntArray1924[i_15_] != -1 && StaticMethods.aByteArrayArray3167[i_15_] == null) {
				StaticMethods.aByteArrayArray3167[i_15_] = Class324.fs5.getFile(Class188.anIntArray1924[i_15_], 0);
				if (StaticMethods.aByteArrayArray3167[i_15_] == null) {
					ready = false;
					OutputStream_Sub1.anInt40++;
				}
			}
			if (StaticMethods.anIntArray1844[i_15_] != -1 && Class166.aByteArrayArray1691[i_15_] == null) {
				Class166.aByteArrayArray1691[i_15_] = Class324.fs5.getFile(StaticMethods.anIntArray1844[i_15_], 0);
				if (Class166.aByteArrayArray1691[i_15_] == null) {
					ready = false;
					OutputStream_Sub1.anInt40++;
				}
			}
			if (Class243.anIntArray2317 != null && ParticleEmitterRaw.aByteArrayArray1772[i_15_] == null && Class243.anIntArray2317[i_15_] != -1) {
				ParticleEmitterRaw.aByteArrayArray1772[i_15_] = Class324.fs5.getFile(Class243.anIntArray2317[i_15_], 0, Class355_Sub2.xteaKeys[i_15_]);
				if (ParticleEmitterRaw.aByteArrayArray1772[i_15_] == null) {
					ready = false;
					OutputStream_Sub1.anInt40++;
				}
			}
		}
		if (Class296_Sub51_Sub19.aClass259_6439 == null) {
			if (Class373.aClass296_Sub39_Sub14_3177 != null && Class205_Sub2.aClass138_5631.hasFile(Class373.aClass296_Sub39_Sub14_3177.aString6216 + "_staticelements", true)) {
				if (Class205_Sub2.aClass138_5631.hasFileBuffer_(Class373.aClass296_Sub39_Sub14_3177.aString6216 + "_staticelements")) {
					Class296_Sub51_Sub19.aClass259_6439 = Class272.method2312(Class172.member, Class205_Sub2.aClass138_5631, Class373.aClass296_Sub39_Sub14_3177.aString6216 + "_staticelements", (byte) 91);
				} else {
					OutputStream_Sub1.anInt40++;
					ready = false;
				}
			} else {
				Class296_Sub51_Sub19.aClass259_6439 = new Class259(0);
			}
		}
		if (!ready) {
			StaticMethods.anInt5969 = 1;
		} else {
			ready = true;
			Class117.anInt1187 = 0;
			for (int i_16_ = 0; Class296_Sub51_Sub31.aByteArrayArray6509.length > i_16_; i_16_++) {
				byte[] is = Class107_Sub1.aByteArrayArray5667[i_16_];
				if (is != null) {
					int i_17_ = (Class296_Sub43.anIntArray4940[i_16_] >> 8) * 64 - Class206.worldBaseX;
					int i_18_ = -Class41_Sub26.worldBaseY + (Class296_Sub43.anIntArray4940[i_16_] & 0xff) * 64;
					if (Class338_Sub2.mapLoadType != 0) {
						i_17_ = 10;
						i_18_ = 10;
					}
					ready &= Class296_Sub35_Sub1.method2753(Class296_Sub38.currentMapSizeY, -1, i_18_, is, i_17_, Class198.currentMapSizeX);
				}
				is = Class166.aByteArrayArray1691[i_16_];
				if (is != null) {
					int i_19_ = (Class296_Sub43.anIntArray4940[i_16_] >> 8) * 64 - Class206.worldBaseX;
					int i_20_ = -Class41_Sub26.worldBaseY + (Class296_Sub43.anIntArray4940[i_16_] & 0xff) * 64;
					if (Class338_Sub2.mapLoadType != 0) {
						i_19_ = 10;
						i_20_ = 10;
					}
					ready &= Class296_Sub35_Sub1.method2753(Class296_Sub38.currentMapSizeY, -1, i_20_, is, i_19_, Class198.currentMapSizeX);
				}
			}
			if (!ready) {
				StaticMethods.anInt5969 = 2;
			} else {
				if (StaticMethods.anInt5969 != 0) {
					ModeWhat.method1021(Class123_Sub1_Sub1.aClass92_5814, 8493, TranslatableString.aClass120_1208.getTranslation(Class394.langID) + "<br>(100%)", Class41_Sub13.aHa3774, true, Class205_Sub1.aClass55_5642);
				}
				Class108.method948(-2060);
				TextureOperation.method3069(-116);
				Class225.method2084((byte) -22);
				boolean bool_21_ = false;
				if (Class41_Sub13.aHa3774.l() && Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013.method393(127) == 2) {
					for (int i_22_ = 0; i_22_ < Class296_Sub51_Sub31.aByteArrayArray6509.length; i_22_++) {
						if (Class166.aByteArrayArray1691[i_22_] != null || StaticMethods.aByteArrayArray3167[i_22_] != null) {
							bool_21_ = true;
							break;
						}
					}
				}
				int i_23_;
				if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub1_5008.method389(115) != 1) {
					i_23_ = ReferenceTable.anIntArray984[Class152.mapSizeType];
				} else {
					i_23_ = Class107_Sub1.anIntArray5669[Class152.mapSizeType];
				}
				if (Class41_Sub13.aHa3774.x()) {
					i_23_++;
				}
				Class53.method642(Class41_Sub13.aHa3774, Class338_Sub3_Sub5_Sub2.anInt6667, 9, 4, Class198.currentMapSizeX, Class296_Sub38.currentMapSizeY, i_23_, bool_21_, Class41_Sub13.aHa3774.q() > 0);
				Class319.method3338(Class319.anInt3650);
				if (Class319.anInt3650 != 0) {
					Class360_Sub11.method3754(Class41_Sub2.aClass55_3742);
				} else {
					Class360_Sub11.method3754(null);
				}
				if (i >= -124) {
					anInt3498 = -80;
				}
				for (int i_24_ = 0; i_24_ < 4; i_24_++) {
					BITConfigDefinition.mapClips[i_24_].setup();
				}
				Class252.method2202((byte) -35);
				Class296_Sub34.method2733(false, 3);
				Class211.method2017((byte) 59);
				Class41_Sub3.aBoolean3744 = false;
				Class108.method948(-2060);
				System.gc();
				Class106_Sub1.method943(true, 1);
				StaticMethods.method2729(-1325);
				Class360_Sub6.anInt5331 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method465(119);
				TranslatableString.aBoolean1263 = FileWorker.anInt3004 >= 96;
				Class41_Sub2.aBoolean3740 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013.method393(120) == 2;
				Class259.aBoolean2416 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub7_5004.method420(121) == 1;
				Class41_Sub5.anInt3751 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026.method496(126) != 1 ? NPCDefinitionLoader.anInt1401 : -1;
				Class296_Sub51_Sub2.aBoolean6338 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub29_5002.method515(119) == 1;
				ByteStream.aBoolean6044 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991.method497(121) == 1;
				StaticMethods.aClass181_Sub1_5960 = new Class181_Sub1(4, Class198.currentMapSizeX, Class296_Sub38.currentMapSizeY, false);
				if (Class338_Sub2.mapLoadType != 0) {
					Class296_Sub49.method3046(Class296_Sub51_Sub31.aByteArrayArray6509, StaticMethods.aClass181_Sub1_5960, 69);
				} else {
					Class308.method3290(StaticMethods.aClass181_Sub1_5960, -27493, Class296_Sub51_Sub31.aByteArrayArray6509);
				}
				Class296_Sub37.method2766((byte) -106, Class198.currentMapSizeX >> 4, Class296_Sub38.currentMapSizeY >> 4);
				Class296_Sub20.method2655(-121);
				if (bool_21_) {
					Class69_Sub1.method732(true);
					Class67.aClass181_Sub1_755 = new Class181_Sub1(1, Class198.currentMapSizeX, Class296_Sub38.currentMapSizeY, true);
					if (Class338_Sub2.mapLoadType != 0) {
						Class296_Sub49.method3046(StaticMethods.aByteArrayArray3167, Class67.aClass181_Sub1_755, 120);
						Class106_Sub1.method943(true, 1);
					} else {
						Class308.method3290(Class67.aClass181_Sub1_755, -27493, StaticMethods.aByteArrayArray3167);
						Class106_Sub1.method943(true, 1);
					}
					Class67.aClass181_Sub1_755.method1824(0, StaticMethods.aClass181_Sub1_5960.anIntArrayArrayArray1866[0], (byte) -117);
					Class67.aClass181_Sub1_755.method1830(null, null, Class41_Sub13.aHa3774, -30558);
					Class69_Sub1.method732(false);
				}
				StaticMethods.aClass181_Sub1_5960.method1830(BITConfigDefinition.mapClips, bool_21_ ? Class67.aClass181_Sub1_755.anIntArrayArrayArray1866 : null, Class41_Sub13.aHa3774, -30558);
				if (Class338_Sub2.mapLoadType == 0) {
					Class106_Sub1.method943(true, 1);
					Class44_Sub1.method574(Class107_Sub1.aByteArrayArray5667, StaticMethods.aClass181_Sub1_5960, (byte) -127);
					if (ParticleEmitterRaw.aByteArrayArray1772 != null) {
						Class36.method357(-99);
					}
				} else {
					Class106_Sub1.method943(true, 1);
					Class296_Sub39_Sub12.method2847(Class107_Sub1.aByteArrayArray5667, StaticMethods.aClass181_Sub1_5960, -24660);
				}
				TextureOperation.method3069(-113);
				if (FileWorker.anInt3004 < 96) {
					Class384.method4016((byte) 107);
				}
				Class106_Sub1.method943(true, 1);
				StaticMethods.aClass181_Sub1_5960.method1820(Class41_Sub13.aHa3774, false, bool_21_ ? Class52.aSArray636[0] : null, null);
				StaticMethods.aClass181_Sub1_5960.method1831(false, Class41_Sub13.aHa3774, -1);
				Class106_Sub1.method943(true, 1);
				if (bool_21_) {
					Class69_Sub1.method732(true);
					Class106_Sub1.method943(true, 1);
					if (Class338_Sub2.mapLoadType != 0) {
						Class296_Sub39_Sub12.method2847(Class166.aByteArrayArray1691, Class67.aClass181_Sub1_755, -24660);
					} else {
						Class44_Sub1.method574(Class166.aByteArrayArray1691, Class67.aClass181_Sub1_755, (byte) -127);
					}
					TextureOperation.method3069(-128);
					Class106_Sub1.method943(true, 1);
					Class67.aClass181_Sub1_755.method1820(Class41_Sub13.aHa3774, false, null, Class244.aSArray2320[0]);
					Class67.aClass181_Sub1_755.method1831(true, Class41_Sub13.aHa3774, -1);
					Class106_Sub1.method943(true, 1);
					Class69_Sub1.method732(false);
				}
				Class391.method4040(-126);
				int i_25_ = StaticMethods.aClass181_Sub1_5960.anInt4514;
				if (i_25_ > FileWorker.anInt3005) {
					i_25_ = FileWorker.anInt3005;
				}
				if (FileWorker.anInt3005 - 1 > i_25_) {
					i_25_ = FileWorker.anInt3005 - 1;
				}
				if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026.method496(121) == 0) {
					Class123_Sub2.method1069(i_25_);
				} else {
					Class123_Sub2.method1069(0);
				}
				for (int i_26_ = 0; i_26_ < 4; i_26_++) {
					for (int i_27_ = 0; Class198.currentMapSizeX > i_27_; i_27_++) {
						for (int i_28_ = 0; i_28_ < Class296_Sub38.currentMapSizeY; i_28_++) {
							method2426(i_27_, 0, i_28_, i_26_);
						}
					}
				}
				Class296_Sub51_Sub21.method3133(-54);
				Class108.method948(-2060);
				Class296_Sub39_Sub17.method2894(1);
				TextureOperation.method3069(-127);
				Class103.method893(-2);
				if (Class340.aFrame3707 != null && Class296_Sub45_Sub2.aClass204_6277.aClass154_2045 != null && Class366_Sub6.anInt5392 == 12) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 118, Class116.aClass311_3677);
					class296_sub1.out.p4(1057001181);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (Class338_Sub2.mapLoadType == 0) {
					int i_29_ = (Class41_Sub6.anInt3760 - (Class198.currentMapSizeX >> 4)) / 8;
					int i_30_ = (Class41_Sub6.anInt3760 + (Class198.currentMapSizeX >> 4)) / 8;
					int i_31_ = (-(Class296_Sub38.currentMapSizeY >> 4) + Class97.anInt1050) / 8;
					int i_32_ = ((Class296_Sub38.currentMapSizeY >> 4) + Class97.anInt1050) / 8;
					for (int i_33_ = i_29_ - 1; i_30_ + 1 >= i_33_; i_33_++) {
						for (int i_34_ = i_31_ - 1; i_34_ <= i_32_ + 1; i_34_++) {
							if (i_29_ > i_33_ || i_33_ > i_30_ || i_34_ < i_31_ || i_34_ > i_32_) {
								Class324.fs5.requestFile("m" + i_33_ + "_" + i_34_, 6637);
								Class324.fs5.requestFile("l" + i_33_ + "_" + i_34_, 6637);
							}
						}
					}
				}
				if (Class366_Sub6.anInt5392 != 4) {
					if (Class366_Sub6.anInt5392 != 8) {
						if (Class366_Sub6.anInt5392 == 10) {
							Class41_Sub8.method422(1, 9);
						} else {
							Class41_Sub8.method422(1, 11);
							if (Class296_Sub45_Sub2.aClass204_6277.aClass154_2045 != null) {
								Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 85, za.aClass311_5084);
								Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
							}
						}
					} else {
						Class41_Sub8.method422(1, 7);
					}
				} else {
					Class41_Sub8.method422(1, 3);
				}
				Class296_Sub51_Sub35.method3187((byte) -75);
				Class108.method948(-2060);
				Class131.method1382(97);
				Class41.aBoolean388 = true;
				if (Class324.aBoolean2861) {
					Class41_Sub18.writeConsoleMessage("Took: " + (Class72.method771(-109) + -Class403.aLong3380) + "ms");
					Class324.aBoolean2861 = false;
				}
			}
		}
	}
}
