package net.zaros.client;
/* Class154_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;
import java.net.Socket;

final class Class154_Sub1 extends Class154 {
	static int[] anIntArray4288;
	private Class29 aClass29_4289;
	private Class2 aClass2_4290;
	static int anInt4291 = 0;
	static Class92 aClass92_4292;
	static int anInt4293;
	private Socket aSocket4294;

	final boolean method1556(boolean bool, int i) throws IOException {
		if (bool != true)
			method1555(-55);
		return aClass2_4290.method166(i, -112);
	}

	final int method1559(byte i, int i_0_, byte[] is, int i_1_) throws IOException {
		int i_2_ = -48 / ((i - 58) / 51);
		return aClass2_4290.method167(is, i_1_, i_0_, true);
	}

	final void method1555(int i) {
		aClass2_4290.method169((byte) -89);
		aClass29_4289.method319((byte) 83);
		if (i != -10035)
			method1555(61);
	}

	final void method1561(int i) {
		try {
			aSocket4294.close();
			if (i < 121)
				aClass29_4289 = null;
		} catch (IOException ioexception) {
			/* empty */
		}
		aClass2_4290.method170(false);
		aClass29_4289.method321(-9359);
	}

	static final void method1563(int i) {
		synchronized (Class258.aClass113_2415) {
			Class258.aClass113_2415.clearSoftReferences();
			if (i != 1000000)
				method1564((byte) 57);
		}
		synchronized (ParticleEmitterRaw.aClass113_1725) {
			ParticleEmitterRaw.aClass113_1725.clearSoftReferences();
		}
	}

	public static void method1564(byte i) {
		aClass92_4292 = null;
		int i_3_ = 33 % ((-53 - i) / 45);
		anIntArray4288 = null;
	}

	static final Js5 createFS(int i_4_, int id, boolean bool) {
		Js5DiskStore class168 = null;
		if (Class321.dataFile != null)
			class168 = new Js5DiskStore(id, Class321.dataFile, Class392.aClass255Array3491[id], 1000000);
		Class296_Sub51_Sub2.cacheFileWorkers[id] = Class296_Sub39_Sub17.aClass111_6241.method974(id, (byte) -120, (Class368_Sub23.aClass168_5574), class168);
		Class296_Sub51_Sub2.cacheFileWorkers[id].method3647((byte) -87);
		return new Js5(Class296_Sub51_Sub2.cacheFileWorkers[id], bool, i_4_);
	}

	static final void setGlobalIntVar(int varValue, int varID) {
		GameLoopTask task = Class296_Sub39_Sub6.makeGameLoopTask((long) varID, 1);
		task.insertIntoQueue_2();
		task.intParam = varValue;
	}

	final void method1558(int i, byte[] is, int i_8_, int i_9_) throws IOException {
		aClass29_4289.method320(i, i_8_ + 7978, i_9_, is);
		if (i_8_ != -7979)
			method1555(-98);
	}

	Class154_Sub1(Socket socket, int i) throws IOException {
		aSocket4294 = socket;
		aSocket4294.setSoTimeout(30000);
		aSocket4294.setTcpNoDelay(true);
		aClass2_4290 = new Class2(aSocket4294.getInputStream(), i);
		aClass29_4289 = new Class29(aSocket4294.getOutputStream(), i);
	}

	protected final void finalize() {
		method1561(127);
	}

	static {
		anIntArray4288 = new int[]{1, 2, 4, 8};
		anInt4293 = 328;
	}
}
