package net.zaros.client;

/* Class296_Sub17_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class ByteStream extends Packet {
	private ISAACCipher cipher;
	static int anInt6040;
	private int bitPosition;
	static int anInt6042 = 13156520;
	static int[] anIntArray6043;
	static boolean aBoolean6044;

	final void endBITStream() {
		pos = (bitPosition + 7) / 8;
	}

	final int readBITS(int count) {
		int bufferPosition = bitPosition >> 3;
		int i_2_ = 8 - (bitPosition & 0x7);
		bitPosition += count;
		int i_3_ = 0;
		for (/**/; count > i_2_; i_2_ = 8) {
			i_3_ += (data[bufferPosition++] & Mobile.bitMasks[i_2_]) << count - i_2_;
			count -= i_2_;
		}
		if (count != i_2_) {
			i_3_ += data[bufferPosition] >> i_2_ - count & Mobile.bitMasks[count];
		} else {
			i_3_ += Mobile.bitMasks[i_2_] & data[bufferPosition];
		}
		return i_3_;
	}

	final boolean nextOpcodeBig() {
		int op = data[pos] - cipher.peek() & 0xff;
		if (op < 128) {
			return false;
		}
		return true;
	}

	final void readEncryptedData(byte[] buffer, int offset, int length) {
		for (int i = 0; i < length; i++) {
			buffer[offset + i] = (byte) (data[pos++] - cipher.next());
		}
	}

	ByteStream(int i) {
		super(i);
	}

	final int bitOffset(int numBits) {
		return numBits * 8 - bitPosition;
	}

	public static void method2626(byte i) {
		if (i >= -17) {
			method2631(106);
		}
		anIntArray6043 = null;
	}

	final int readOpcode() {
		int opcode = data[pos++] - cipher.next() & 0xff;
		if (opcode >= 128) {
			opcode = (data[pos++] - cipher.next() & 0xff) + (opcode - 128 << 8);
		}
		return opcode;
	}

	final void writeOpcode(int opc) {
		data[pos++] = (byte) (cipher.next() + opc);
	}

	final void prepareBITStream() {
		bitPosition = pos * 8;
	}

	final void initCipher(int[] seeds) {
		this.cipher = new ISAACCipher(seeds);
	}

	static final int method2631(int i) {
		int i_12_ = Class253.aClass326_2389.method3386((byte) -44);
		int i_13_ = 63 % ((i - 39) / 47);
		if (Class41_Sub8.aClass326Array3764.length - 1 > i_12_) {
			Class253.aClass326_2389 = Class41_Sub8.aClass326Array3764[i_12_ + 1];
		}
		return 100;
	}

	static final int method2632(int i, int i_14_, int i_15_) {
		if (i != 25051) {
			method2632(-110, 42, -119);
		}
		i_14_ = (i_15_ & 0x7f) * i_14_ >> 7;
		if (i_14_ < 2) {
			i_14_ = 2;
		} else if (i_14_ > 126) {
			i_14_ = 126;
		}
		return i_14_ + (i_15_ & 0xff80);
	}

	final void setCipher(ISAACCipher cipher) {
		this.cipher = cipher;
	}

	static {
		anInt6040 = -1;
		aBoolean6044 = false;
	}
}
