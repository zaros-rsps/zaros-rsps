package net.zaros.client;


/* Class296_Sub39_Sub18 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub18 extends Queuable {
	Class338_Sub3_Sub1_Sub2 aClass338_Sub3_Sub1_Sub2_6246;
	static int anInt6247;

	static final void method2896(int i, int i_0_, int i_1_, int i_2_, byte i_3_, byte[] is) {
		if (i_1_ < i) {
			i_2_ = i - i_1_ >> 2;
			i_0_ += i_1_;
			while (--i_2_ >= 0) {
				is[i_0_++] = (byte) 1;
				is[i_0_++] = (byte) 1;
				is[i_0_++] = (byte) 1;
				is[i_0_++] = (byte) 1;
			}
			i_2_ = -i_1_ + i & 0x3;
			while (--i_2_ >= 0)
				is[i_0_++] = (byte) 1;
			if (i_3_ != -85)
				anInt6247 = 116;
		}
	}

	static final int method2897(byte i, int i_4_) {
		if (i != 61)
			anInt6247 = -106;
		if (i_4_ == 6407 || i_4_ == 34843 || i_4_ == 34837)
			return 6407;
		if (i_4_ == 6408 || i_4_ == 34842 || i_4_ == 34836)
			return 6408;
		if (i_4_ == 6406 || i_4_ == 34844)
			return 6406;
		if (i_4_ == 6409 || i_4_ == 34846)
			return 6409;
		if (i_4_ == 6410 || i_4_ == 34847)
			return 6410;
		if (i_4_ == 6402)
			return 6402;
		throw new IllegalArgumentException("");
	}

	static final void interfaceClicked(int walkType, int dstY, String string, int dstX) {
		InterfaceComponent clickedComponent = Class103.method894(0, dstY, dstX);
		if (clickedComponent != null) {
			if (clickedComponent.anObjectArray585 != null) {
				CS2Call callArgs = new CS2Call();
				callArgs.callArgs = clickedComponent.anObjectArray585;
				callArgs.anInt4965 = walkType;
				callArgs.callerInterface = clickedComponent;
				callArgs.aString4968 = string;
				CS2Executor.runCS2(callArgs);
			}
			if (Class366_Sub6.anInt5392 == 11 && GameClient.method115(clickedComponent).method2709(walkType - 1, (byte) 107)) {
				if (walkType == 1) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 120, Class202.aClass311_2041);
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -117, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (walkType == 2) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 84, Class368_Sub23.aClass311_5565);
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -101, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (walkType == 3) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 110, (Class296_Sub39_Sub13.aClass311_6204));
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -118, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (walkType == 4) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 121, Class368_Sub22.aClass311_5560);
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -127, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (walkType == 5) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 108, OutgoingPacket.aClass311_292);
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -107, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (walkType == 6) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 99, HashTable.aClass311_2461);
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -106, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (walkType == 7) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 91, Class391.aClass311_3296);
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -117, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (walkType == 8) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 127, Class41.aClass311_391);
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -119, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (walkType == 9) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 102, Class328.aClass311_2906);
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -107, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (walkType == 10) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 104, Class320.aClass311_2820);
					Class296_Sub51_Sub35.method3186(dstX, dstY, (byte) -110, clickedComponent.clickedItem, class296_sub1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
			}
		}
	}

	static final int method2899(int i, int i_8_) {
		int i_9_ = (i * i >> 12) * i >> 12;
		int i_10_ = i * 6 - 61440;
		if (i_8_ != -1184463124)
			anInt6247 = 80;
		int i_11_ = (i * i_10_ >> 12) + 40960;
		return i_9_ * i_11_ >> 12;
	}

	Class296_Sub39_Sub18(Class338_Sub3_Sub1_Sub2 class338_sub3_sub1_sub2) {
		aClass338_Sub3_Sub1_Sub2_6246 = class338_sub3_sub1_sub2;
	}
}
