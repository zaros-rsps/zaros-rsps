package net.zaros.client;
import jaclib.memory.Buffer;
import jaclib.memory.Source;

import jagdx.IDirect3DVertexBuffer;
import jagdx.aj;

final class Class133 implements Interface15_Impl2 {
	private int anInt5703;
	private boolean aBoolean5704 = false;
	private byte aByte5705;
	private int anInt5706;
	private boolean aBoolean5707;
	IDirect3DVertexBuffer anIDirect3DVertexBuffer5708;
	private ha_Sub1_Sub2 aHa_Sub1_Sub2_5709;

	public final void method31(byte i) {
		if (anIDirect3DVertexBuffer5708 != null) {
			anIDirect3DVertexBuffer5708.b((byte) 83);
			anIDirect3DVertexBuffer5708 = null;
		}
		anInt5706 = 0;
		if (i == 82)
			anInt5703 = 0;
	}

	public final boolean method48(boolean bool, Source source, int i, int i_0_) {
		if (bool)
			return true;
		if (method46(i_0_, -101, i))
			return anIDirect3DVertexBuffer5708.a(source, 0, 0, anInt5706, !aBoolean5707 ? 0 : 8192);
		return false;
	}

	public final boolean method49(int i) {
		if (aBoolean5704 && aj.a(anIDirect3DVertexBuffer5708.Unlock(), (int) 27)) {
			aBoolean5704 = false;
			return true;
		}
		if (i != 2968)
			method61((byte) 124);
		return false;
	}

	public final boolean method46(int i, int i_1_, int i_2_) {
		anInt5706 = i;
		int i_3_ = -23 % ((-24 - i_1_) / 40);
		aByte5705 = (byte) i_2_;
		if (anInt5703 < anInt5706) {
			int i_4_ = 8;
			int i_5_;
			if (!aBoolean5707)
				i_5_ = 1;
			else {
				i_4_ |= 0x200;
				i_5_ = 0;
			}
			if (anIDirect3DVertexBuffer5708 != null)
				anIDirect3DVertexBuffer5708.b((byte) 83);
			anIDirect3DVertexBuffer5708 = (aHa_Sub1_Sub2_5709.anIDirect3DDevice5865.a(anInt5706, i_4_, 0, i_5_, anIDirect3DVertexBuffer5708));
			anInt5703 = anInt5706;
		}
		return anIDirect3DVertexBuffer5708 != null;
	}

	final int method1400(byte i) {
		if (i < 122)
			return 40;
		return aByte5705;
	}

	public final Buffer method47(boolean bool, int i) {
		if (anIDirect3DVertexBuffer5708 == null)
			return null;
		bool &= aBoolean5707;
		if (i != -8102)
			aBoolean5704 = false;
		if (!aBoolean5704 && aj.a(anIDirect3DVertexBuffer5708.Lock(0, anInt5703, bool ? 8192 : 0, (aHa_Sub1_Sub2_5709.aGeometryBuffer5863)), (int) 97)) {
			aBoolean5704 = true;
			return aHa_Sub1_Sub2_5709.aGeometryBuffer5863;
		}
		return null;
	}

	public final int method61(byte i) {
		if (i < 115)
			method49(-34);
		return anInt5706;
	}

	Class133(ha_Sub1_Sub2 var_ha_Sub1_Sub2, boolean bool) {
		aBoolean5707 = bool;
		aHa_Sub1_Sub2_5709 = var_ha_Sub1_Sub2;
	}
}
