package net.zaros.client;

/* Class174 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class174 {
	private int anInt1815;
	private int anInt1816;
	private int anInt1817;
	private int anInt1818;
	private int anInt1819;
	private Sprite aClass397_1820;
	int anInt1821;
	private int anInt1822;
	private int anInt1823;
	private static Sprite aClass397_1824;
	private int anInt1825;
	private int anInt1826;
	private boolean aBoolean1827;
	private int anInt1828;
	private int anInt1829;
	private int anInt1830;
	private static int[] anIntArray1831 = new int[4];
	private int anInt1832;
	private static Sprite aClass397_1833;
	private static Model aClass178_1834;
	private int anInt1835;

	final void method1691(ha var_ha, int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		if (aClass397_1820 != null) {
			int[] is = new int[3];
			int i_9_ = -(anInt1817 - i_5_ << 16);
			int i_10_ = anInt1825 - i_6_ << 15;
			int i_11_ = -(anInt1835 - i_7_ << 16);
			Class373 class373 = var_ha.o();
			class373.method3903(0, 0, 0, is);
			i_9_ += is[0];
			i_10_ += is[1];
			i_11_ += is[2];
			var_ha.H(i_9_, i_10_, i_11_, is);
			if (is[2] >= 0) {
				int i_12_ = is[0] - anInt1829 / 2;
				int i_13_ = is[1] - anInt1829 / 2;
				if (i_13_ < i_2_ && i_13_ + anInt1829 > 0 && i_12_ < i_1_ && i_12_ + anInt1829 > 0)
					aClass397_1820.method4083(i_12_, i_13_, anInt1829, anInt1829, 0, i_8_ << 24 | 0xffffff, 1);
			}
		}
	}

	private final boolean method1692(ha var_ha, Class174 class174_14_) {
		if (aClass397_1820 == null) {
			if (anInt1822 == 0) {
				if (Class366_Sub8.aD5412.is_ready(anInt1816)) {
					int[] is = Class366_Sub8.aD5412.method17(0.7F, anInt1828, false, (byte) -104, anInt1828, anInt1816);
					aClass397_1820 = var_ha.method1096(112, 0, anInt1828, anInt1828, anInt1828, is);
				}
			} else if (anInt1822 == 2)
				method1694(var_ha, class174_14_);
			else if (anInt1822 == 1)
				method1695(var_ha, class174_14_);
		}
		if (aClass397_1820 == null)
			return false;
		return true;
	}

	private static final void method1693(ha var_ha) {
		if (aClass178_1834 == null) {
			Mesh class132 = new Mesh(580, 1104, 1);
			class132.method1386((short) 1024, true, (short) 1024, (short) 1024, (byte) 0, (byte) 0, (byte) 0, (short) 0, (short) 0, (short) 32767);
			class132.method1399(128, 0, 0, 1);
			class132.method1399(-128, 0, 0, 1);
			for (int i = 0; i <= 24; i++) {
				int i_15_ = i * 8192 / 24;
				int i_16_ = Class296_Sub4.anIntArray4598[i_15_];
				int i_17_ = Class296_Sub4.anIntArray4618[i_15_];
				for (int i_18_ = 1; i_18_ < 24; i_18_++) {
					int i_19_ = i_18_ * 8192 / 24;
					int i_20_ = Class296_Sub4.anIntArray4618[i_19_] >> 7;
					int i_21_ = Class296_Sub4.anIntArray4598[i_19_] * i_16_ >> 21;
					int i_22_ = Class296_Sub4.anIntArray4598[i_19_] * i_17_ >> 21;
					class132.method1399(i_20_, -i_21_, i_22_, 1);
				}
				if (i > 0) {
					int i_23_ = i * 23 + 2;
					int i_24_ = i_23_ - 23;
					class132.method1389(0, i_23_, -70, (short) 0, i_24_, (byte) 0, (byte) 0, (short) 127, (byte) 0);
					for (int i_25_ = 1; i_25_ < 23; i_25_++) {
						int i_26_ = i_24_ + 1;
						int i_27_ = i_23_ + 1;
						class132.method1389(i_24_, i_23_, -91, (short) 0, i_26_, (byte) 0, (byte) 0, (short) 127, (byte) 0);
						class132.method1389(i_26_, i_23_, -112, (short) 0, i_27_, (byte) 0, (byte) 0, (short) 127, (byte) 0);
						i_24_ = i_26_;
						i_23_ = i_27_;
					}
					class132.method1389(i_23_, 1, -75, (short) 0, i_24_, (byte) 0, (byte) 0, (short) 127, (byte) 0);
				}
			}
			class132.highest_face_index = class132.num_vertices;
			class132.faces_label = null;
			class132.vertices_label = null;
			class132.faces_priority = null;
			aClass178_1834 = var_ha.a(class132, 51200, 33, 64, 768);
		}
	}

	private final void method1694(ha var_ha, Class174 class174_28_) {
		Mesh class132 = Class296_Sub51_Sub1.fromJs5(Class239.aClass138_2255, anInt1816, 0);
		if (class132 != null) {
			var_ha.K(anIntArray1831);
			var_ha.KA(0, 0, anInt1828, anInt1828);
			var_ha.ya();
			var_ha.aa(0, 0, anInt1828, anInt1828, 0, 0);
			int i = 0;
			int i_29_ = 0;
			int i_30_ = 256;
			if (class174_28_ != null) {
				if (class174_28_.aBoolean1827) {
					i = -class174_28_.anInt1817;
					i_29_ = -class174_28_.anInt1825;
					i_30_ = -class174_28_.anInt1835;
				} else {
					i = anInt1817 - class174_28_.anInt1817;
					i_29_ = anInt1825 - class174_28_.anInt1825;
					i_30_ = anInt1835 - class174_28_.anInt1835;
				}
			}
			if (anInt1815 != 0) {
				int i_31_ = -anInt1815 & 0x3fff;
				int i_32_ = Class296_Sub4.anIntArray4598[i_31_];
				int i_33_ = Class296_Sub4.anIntArray4618[i_31_];
				int i_34_ = i_29_ * i_33_ - i_30_ * i_32_ >> 14;
				i_30_ = i_29_ * i_32_ + i_30_ * i_33_ >> 14;
				i_29_ = i_34_;
			}
			if (anInt1819 != 0) {
				int i_35_ = -anInt1819 & 0x3fff;
				int i_36_ = Class296_Sub4.anIntArray4598[i_35_];
				int i_37_ = Class296_Sub4.anIntArray4618[i_35_];
				int i_38_ = i_30_ * i_36_ + i * i_37_ >> 14;
				i_30_ = i_30_ * i_37_ - i * i_36_ >> 14;
				i = i_38_;
			}
			var_ha.xa(1.0F);
			var_ha.ZA(anInt1826, 1.0F, 1.0F, (float) i, (float) i_29_, (float) i_30_);
			class132.method1392(anInt1818 & 0x3fff, anInt1823 & 0x3fff, anInt1830 & 0x3fff, (byte) 84);
			Model class178 = var_ha.a(class132, 2048, 0, 64, 768);
			int i_39_ = class178.RA() - class178.V();
			int i_40_ = class178.EA() - class178.fa();
			int i_41_ = i_39_ > i_40_ ? i_39_ : i_40_;
			int i_42_ = anInt1828 * 1024 / i_41_;
			int[] is = var_ha.Y();
			var_ha.DA(anInt1828 / 2, anInt1828 / 2, i_42_, i_42_);
			var_ha.a(var_ha.m());
			Class373 class373 = var_ha.f();
			class373.method3902(0, 0, var_ha.i() - class178.HA());
			class178.method1719(class373, null, var_ha.i(), 1);
			aClass397_1820 = var_ha.a(0, 0, anInt1828, anInt1828, true);
			aClass397_1820.method4084(0, 0, 3);
			var_ha.DA(is[0], is[1], is[2], is[3]);
			var_ha.KA(anIntArray1831[0], anIntArray1831[1], anIntArray1831[2], anIntArray1831[3]);
		}
	}

	private final void method1695(ha var_ha, Class174 class174_43_) {
		method1693(var_ha);
		method1700(var_ha);
		var_ha.K(anIntArray1831);
		var_ha.KA(0, 0, anInt1828, anInt1828);
		var_ha.ya();
		var_ha.aa(0, 0, anInt1828, anInt1828, anInt1826 | ~0xffffff, 0);
		int i = 0;
		int i_44_ = 0;
		int i_45_ = 256;
		if (class174_43_ != null) {
			if (class174_43_.aBoolean1827) {
				i = -class174_43_.anInt1817;
				i_44_ = -class174_43_.anInt1825;
				i_45_ = -class174_43_.anInt1835;
			} else {
				i = class174_43_.anInt1817 - anInt1817;
				i_44_ = class174_43_.anInt1825 - anInt1825;
				i_45_ = class174_43_.anInt1835 - anInt1835;
			}
		}
		if (anInt1815 != 0) {
			int i_46_ = Class296_Sub4.anIntArray4598[anInt1815];
			int i_47_ = Class296_Sub4.anIntArray4618[anInt1815];
			int i_48_ = i_44_ * i_47_ - i_45_ * i_46_ >> 14;
			i_45_ = i_44_ * i_46_ + i_45_ * i_47_ >> 14;
			i_44_ = i_48_;
		}
		if (anInt1819 != 0) {
			int i_49_ = Class296_Sub4.anIntArray4598[anInt1819];
			int i_50_ = Class296_Sub4.anIntArray4618[anInt1819];
			int i_51_ = i_45_ * i_49_ + i * i_50_ >> 14;
			i_45_ = i_45_ * i_50_ - i * i_49_ >> 14;
			i = i_51_;
		}
		Model class178 = aClass178_1834.method1728((byte) 0, 51200, true);
		class178.aa((short) 0, (short) anInt1816);
		var_ha.xa(1.0F);
		var_ha.ZA(16777215, 1.0F, 1.0F, (float) i, (float) i_44_, (float) i_45_);
		int i_52_ = anInt1828 * 1024 / (class178.RA() - class178.V());
		if (anInt1826 != 0)
			i_52_ = i_52_ * 13 / 16;
		int[] is = var_ha.Y();
		var_ha.DA(anInt1828 / 2, anInt1828 / 2, i_52_, i_52_);
		var_ha.a(var_ha.m());
		Class373 class373 = var_ha.m();
		class373.method3902(0, 0, var_ha.i() - class178.HA());
		class178.method1719(class373, null, 1024, 1);
		int i_53_ = anInt1828 * 13 / 16;
		int i_54_ = (anInt1828 - i_53_) / 2;
		aClass397_1833.method4083(i_54_, i_54_, i_53_, i_53_, 0, anInt1826 | ~0xffffff, 1);
		aClass397_1820 = var_ha.a(0, 0, anInt1828, anInt1828, true);
		var_ha.ya();
		var_ha.aa(0, 0, anInt1828, anInt1828, 0, 0);
		aClass397_1824.method4083(0, 0, anInt1828, anInt1828, 1, 0, 0);
		aClass397_1820.method4084(0, 0, 3);
		var_ha.DA(is[0], is[1], is[2], is[3]);
		var_ha.KA(anIntArray1831[0], anIntArray1831[1], anIntArray1831[2], anIntArray1831[3]);
	}

	public static void method1696() {
		aClass178_1834 = null;
		aClass397_1833 = null;
		aClass397_1824 = null;
		anIntArray1831 = null;
	}

	final boolean method1697(int i, int i_55_, int i_56_, int i_57_) {
		int i_58_;
		int i_59_;
		int i_60_;
		if (!aBoolean1827) {
			i_58_ = anInt1817 - i;
			i_59_ = anInt1825 - i_55_;
			i_60_ = anInt1835 - i_56_;
			anInt1821 = (int) Math.sqrt((double) (i_58_ * i_58_ + i_59_ * i_59_ + i_60_ * i_60_));
			if (anInt1821 == 0)
				anInt1821 = 1;
			i_58_ = (i_58_ << 8) / anInt1821;
			i_59_ = (i_59_ << 8) / anInt1821;
			i_60_ = (i_60_ << 8) / anInt1821;
		} else {
			anInt1821 = 1073741823;
			i_58_ = anInt1817;
			i_59_ = anInt1825;
			i_60_ = anInt1835;
		}
		int i_61_ = (int) (Math.sqrt((double) (i_58_ * i_58_ + i_59_ * i_59_ + i_60_ * i_60_)) * 256.0);
		if (i_61_ > 128) {
			i_58_ = (i_58_ << 16) / i_61_;
			i_59_ = (i_59_ << 16) / i_61_;
			i_60_ = (i_60_ << 16) / i_61_;
			anInt1829 = anInt1832 * i_57_ / (aBoolean1827 ? 1024 : anInt1821);
		} else
			anInt1829 = 0;
		if (anInt1829 < 8) {
			aClass397_1820 = null;
			return false;
		}
		int i_62_ = Class8.get_next_high_pow2(anInt1829);
		if (i_62_ > i_57_)
			i_62_ = Statics.method629(false, i_57_);
		if (i_62_ > 512)
			i_62_ = 512;
		if (i_62_ != anInt1828)
			anInt1828 = i_62_;
		anInt1815 = (int) (Math.asin((double) ((float) i_59_ / 256.0F)) * 2607.5945876176133) & 0x3fff;
		anInt1819 = (int) (Math.atan2((double) i_58_, (double) -i_60_) * 2607.5945876176133) & 0x3fff;
		aClass397_1820 = null;
		return true;
	}

	final boolean method1698(ha var_ha, Class174 class174_63_) {
		if (aClass397_1820 == null && !method1692(var_ha, class174_63_))
			return false;
		return true;
	}

	static final void method1699() {
		aClass178_1834 = null;
		aClass397_1824 = null;
	}

	private static final void method1700(ha var_ha) {
		if (aClass397_1824 == null) {
			int[] is = new int[16384];
			int[] is_64_ = new int[16384];
			for (int i = 0; i < 64; i++) {
				int i_65_ = 64 - i;
				i_65_ *= i_65_;
				int i_66_ = 128 - i - 1;
				int i_67_ = i * 128;
				int i_68_ = i_66_ * 128;
				for (int i_69_ = 0; i_69_ < 64; i_69_++) {
					int i_70_ = 64 - i_69_;
					i_70_ *= i_70_;
					int i_71_ = 128 - i_69_ - 1;
					int i_72_ = 256 - (i_70_ + i_65_ << 8) / 4096;
					i_72_ = i_72_ * 16 * 192 / 1536;
					if (i_72_ < 0)
						i_72_ = 0;
					else if (i_72_ > 255)
						i_72_ = 255;
					int i_73_ = i_72_ / 2;
					is_64_[i_67_ + i_69_] = is_64_[i_67_ + i_71_] = is_64_[i_68_ + i_69_] = is_64_[i_68_ + i_71_] = i_72_ << 16 | ~0xffffff;
					is[i_67_ + i_69_] = is[i_67_ + i_71_] = is[i_68_ + i_69_] = is[i_68_ + i_71_] = 127 - i_73_ << 24 | 0xffffff;
				}
			}
			aClass397_1824 = var_ha.method1096(114, 0, 128, 128, 128, is_64_);
			aClass397_1833 = var_ha.method1096(119, 0, 128, 128, 128, is);
		}
	}

	Class174(int i, int i_74_, int i_75_, int i_76_, int i_77_, int i_78_, int i_79_, boolean bool, int i_80_, int i_81_, int i_82_) {
		anInt1817 = i_75_;
		anInt1825 = i_76_;
		anInt1835 = i_77_;
		aBoolean1827 = bool;
		anInt1816 = i_74_;
		anInt1826 = i_79_;
		anInt1832 = i_78_;
		anInt1822 = i;
		anInt1830 = i_80_;
		anInt1818 = i_81_;
		anInt1823 = i_82_;
	}
}
