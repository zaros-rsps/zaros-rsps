package net.zaros.client;

/* Class396 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class396 {
	static IncomingPacket aClass231_3318 = new IncomingPacket(115, 6);
	static long aLong3319;
	static IncomingPacket aClass231_3320 = new IncomingPacket(17, -1);

	static final void method4072(byte i, int i_0_, int i_1_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_0_, 7);
		class296_sub39_sub5.insertIntoQueue_2();
		if (i != 15)
			aClass231_3318 = null;
		class296_sub39_sub5.intParam = i_1_;
	}

	static final void method4073(int i, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		if (i_6_ != 4966)
			method4072((byte) 117, -126, -64);
		Class225[] class225s = Class338_Sub2.aClass225Array5200;
		for (int i_8_ = 0; i_8_ < class225s.length; i_8_++) {
			Class225 class225 = class225s[i_8_];
			if (class225 != null && class225.iconType == 2) {
				Class296_Sub44.method2925(i_3_, (byte) -44, class225.anInt2177 * 2, class225.anInt2184, class225.anInt2183, i_5_ >> 1, i_2_ >> 1, class225.anInt2176, i_4_);
				if (StaticMethods.anIntArray4629[0] > -1 && Class29.anInt307 % 20 < 10) {
					Sprite class397 = Class151.aClass397Array1555[class225.modelId];
					int i_9_ = i - 12 + StaticMethods.anIntArray4629[0];
					int i_10_ = i_7_ - 28 + StaticMethods.anIntArray4629[1];
					class397.method4096(i_9_, i_10_);
					Class368_Sub8.method3838(false, i_9_, i_10_, class397.method4088() + i_10_, i_9_ + class397.method4099());
				}
			}
		}
	}

	public static void method4074(int i) {
		aClass231_3320 = null;
		if (i != 17)
			method4074(-110);
		aClass231_3318 = null;
	}
}
