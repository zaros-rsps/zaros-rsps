package net.zaros.client;

/* Class296_Sub51_Sub15 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub15 extends TextureOperation {
	private int anInt6424 = 4096;
	static String[] aStringArray6425;
	static Class54 aClass54_6426;

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		if (i_0_ == 0)
			anInt6424 = class296_sub17.g2();
		if (i > -84)
			method3120((byte) -74);
	}

	final int[] get_monochrome_output(int i, int i_1_) {
		if (i != 0)
			return null;
		int[] is = aClass318_5035.method3335(i_1_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int[] is_2_ = this.method3064(0, 0, Class67.anInt753 & i_1_ - 1);
			int[] is_3_ = this.method3064(0, 0, i_1_);
			int[] is_4_ = this.method3064(0, 0, Class67.anInt753 & i_1_ + 1);
			for (int i_5_ = 0; i_5_ < Class41_Sub10.anInt3769; i_5_++) {
				int i_6_ = anInt6424 * (-is_2_[i_5_] + is_4_[i_5_]);
				int i_7_ = (anInt6424 * (is_3_[i_5_ + 1 & Class41_Sub25.anInt3803] - is_3_[i_5_ - 1 & Class41_Sub25.anInt3803]));
				int i_8_ = i_7_ >> 12;
				int i_9_ = i_6_ >> 12;
				int i_10_ = i_8_ * i_8_ >> 12;
				int i_11_ = i_9_ * i_9_ >> 12;
				int i_12_ = (int) (Math.sqrt((double) ((float) (i_10_ + 4096 + i_11_) / 4096.0F)) * 4096.0);
				int i_13_ = i_12_ != 0 ? 16777216 / i_12_ : 0;
				is[i_5_] = -i_13_ + 4096;
			}
		}
		return is;
	}

	public static void method3120(byte i) {
		aStringArray6425 = null;
		aClass54_6426 = null;
		if (i >= -71)
			aClass54_6426 = null;
	}

	public Class296_Sub51_Sub15() {
		super(1, true);
	}
}
