package net.zaros.client;

/* Class338_Sub3_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub3_Sub1_Sub1 extends Class338_Sub3_Sub1 implements Interface14 {
	private Class96 aClass96_6629;
	private boolean aBoolean6630;
	private byte aByte6631;
	private byte aByte6632;
	private boolean aBoolean6633;
	Model aClass178_6634;
	private boolean aBoolean6635;
	private boolean aBoolean6636;
	private r aR6637;
	private int locid;

	private final Model method3479(int i, ha var_ha, int i_0_) {
		if (aClass178_6634 != null && var_ha.e(aClass178_6634.ua(), i_0_) == 0) {
			return aClass178_6634;
		}
		int i_1_ = 108 % ((6 - i) / 58);
		Class188 class188 = method3483(var_ha, false, (byte) -80, i_0_);
		if (class188 != null) {
			return class188.aClass178_1926;
		}
		return null;
	}

	@Override
	final void method3460(int i, ha var_ha) {
		int i_2_ = 119 / ((i + 41) / 62);
	}

	static final int animChildID(int animID) {
		return animID & 0x7f;
	}

	@Override
	final boolean method3459(int i) {
		if (aClass178_6634 != null) {
			if (aClass178_6634.r()) {
				return false;
			}
			return true;
		}
		if (i != 0) {
			method3484((byte) 50, null);
		}
		return true;
	}

	static final boolean method3481(int i) {
		int i_4_ = 92 % ((37 - i) / 56);
		if (Class366_Sub6.anInt5392 != 3) {
			return false;
		}
		if (Class296_Sub39_Sub12.loginState != 0 || Class397_Sub3.anInt5799 != 0) {
			return false;
		}
		return true;
	}

	@Override
	public final void method60(byte i, ha var_ha) {
		int i_5_ = 40 % ((i + 23) / 35);
		Object object = null;
		r var_r;
		if (aR6637 != null || !aBoolean6636) {
			var_r = aR6637;
			aR6637 = null;
		} else {
			Class188 class188 = method3483(var_ha, true, (byte) -84, 262144);
			var_r = class188 == null ? null : class188.aR1925;
		}
		if (var_r != null) {
			Class296_Sub45_Sub4.method3017(var_r, aByte5203, tileX, tileY, null);
		}
	}

	@Override
	final void method3472(byte i) {
		aBoolean6630 = false;
		if (aClass178_6634 != null) {
			aClass178_6634.s(aClass178_6634.ua() & ~0x10000);
		}
		int i_6_ = 94 % ((i + 56) / 38);
	}

	final int method3482(int i) {
		if (i != 15) {
			method3479(32, null, 62);
		}
		if (aClass178_6634 != null) {
			return aClass178_6634.na() / 4;
		}
		return 15;
	}

	@Override
	public final void method56(ha var_ha, byte i) {
		if (i != -117) {
			method3483(null, false, (byte) -19, 124);
		}
		Object object = null;
		r var_r;
		if (aR6637 != null || !aBoolean6636) {
			var_r = aR6637;
			aR6637 = null;
		} else {
			Class188 class188 = method3483(var_ha, true, (byte) -123, 262144);
			var_r = class188 == null ? null : class188.aR1925;
		}
		if (var_r != null) {
			Class296_Sub39_Sub20_Sub2.method2910(var_r, aByte5203, tileX, tileY, null);
		}
	}

	private final Class188 method3483(ha var_ha, boolean bool, byte i, int i_7_) {
		if (i >= -5) {
			return null;
		}
		ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(locid);
		s var_s;
		s var_s_8_;
		if (aBoolean6635) {
			var_s_8_ = Class244.aSArray2320[0];
			var_s = Class52.aSArray636[aByte5203];
		} else {
			var_s = Class244.aSArray2320[aByte5203];
			if (aByte5203 < 3) {
				var_s_8_ = Class244.aSArray2320[aByte5203 + 1];
			} else {
				var_s_8_ = null;
			}
		}
		return class70.method750(var_s, var_s_8_, var_ha, bool, -954198435, tileY, tileX, i_7_, null, aByte6632 == 11 ? (int) (aByte6631 + 4) : aByte6631, anInt5213, aByte6632 != 11 ? aByte6632 : 10);
	}

	@Override
	public final boolean method55(byte i) {
		if (i != -57) {
			method3472((byte) 117);
		}
		return aBoolean6636;
	}

	@Override
	final boolean method3469(int i) {
		if (i < 82) {
			return true;
		}
		if (aClass178_6634 != null) {
			return aClass178_6634.F();
		}
		return false;
	}

	@Override
	public final int method54(int i) {
		if (i != -11077) {
			aR6637 = null;
		}
		return aByte6632;
	}

	@Override
	public final int method57(byte i) {
		return locid;
	}

	@Override
	final int method3466(byte i) {
		if (i < 77) {
			aByte6632 = (byte) -51;
		}
		if (aClass178_6634 != null) {
			return aClass178_6634.fa();
		}
		return 0;
	}

	@Override
	public final int method59(int i) {
		if (i < 16) {
			method3460(101, null);
		}
		return aByte6631;
	}

	@Override
	public final void method58(int i) {
		if (i != -19727) {
			aBoolean6633 = false;
		}
		if (aClass178_6634 != null) {
			aClass178_6634.method1722();
		}
	}

	@Override
	final boolean method3475(int i, int i_9_, ha var_ha, int i_10_) {
		Model class178 = method3479(-92, var_ha, 131072);
		if (i_9_ > -48) {
			aBoolean6636 = false;
		}
		if (class178 != null) {
			Class373 class373 = var_ha.f();
			class373.method3902(tileX, anInt5213, tileY);
			if (!Class296_Sub39_Sub10.aBoolean6177) {
				return class178.method1732(i_10_, i, class373, false, 0);
			}
			return class178.method1731(i_10_, i, class373, false, 0, ModeWhat.anInt1192);
		}
		return false;
	}

	@Override
	final boolean method3468(int i) {
		if (i > -29) {
			method55((byte) -59);
		}
		return aBoolean6630;
	}

	static final String method3484(byte i, int[] is) {
		StringBuffer stringbuffer = new StringBuffer();
		int i_11_ = OutgoingPacket.anInt2766;
		if (i <= 35) {
			method3484((byte) -79, null);
		}
		for (int i_12_ = 0; i_12_ < is.length; i_12_++) {
			Class251 class251 = Class296_Sub51_Sub38.aClass405_6542.method4170(is[i_12_], 112);
			if (class251.anInt2378 != -1) {
				Sprite class397 = (Sprite) Class366_Sub6.aClass113_5390.get(class251.anInt2378);
				if (class397 == null) {
					Class186 class186 = Class186.method1876(Class205_Sub2.fs8, class251.anInt2378, 0);
					if (class186 != null) {
						class397 = Class41_Sub13.aHa3774.a(class186, true);
						Class366_Sub6.aClass113_5390.put(class397, class251.anInt2378);
					}
				}
				if (class397 != null) {
					Class44_Sub1_Sub1.aClass397Array5811[i_11_] = class397;
					stringbuffer.append(" <img=").append(i_11_).append(">");
					i_11_++;
				}
			}
		}
		return stringbuffer.toString();
	}

	Class338_Sub3_Sub1_Sub1(ha var_ha, ObjectDefinition class70, int i, int i_13_, int i_14_, int i_15_, int i_16_, boolean bool, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_, int i_22_, boolean bool_23_) {
		super(i, i_13_, i_14_, i_15_, i_16_, i_17_, i_18_, i_19_, i_20_, class70.anInt804 == 1, Class296_Sub51_Sub18.method3126(i_21_, (byte) 17, i_22_));
		aBoolean6630 = bool_23_;
		aBoolean6633 = class70.interactonType != 0 && !bool;
		aByte6632 = (byte) i_21_;
		aBoolean6635 = bool;
		aByte5203 = (byte) i_13_;
		locid = class70.ID;
		aByte6631 = (byte) i_22_;
		aBoolean6636 = var_ha.B() && class70.aBoolean779 && !aBoolean6635 && Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method465(117) != 0;
		int i_24_ = 2048;
		if (aBoolean6630) {
			i_24_ |= 0x10000;
		}
		Class188 class188 = method3483(var_ha, aBoolean6636, (byte) -47, i_24_);
		if (class188 != null) {
			aClass178_6634 = class188.aClass178_1926;
			aR6637 = class188.aR1925;
			if (aBoolean6630) {
				aClass178_6634 = aClass178_6634.method1728((byte) 0, i_24_, false);
			}
		}
	}

	@Override
	final Class338_Sub2 method3473(ha var_ha, byte i) {
		if (aClass178_6634 == null) {
			return null;
		}
		if (i > -84) {
			return null;
		}
		Class373 class373 = var_ha.f();
		class373.method3902(tileX, anInt5213, tileY);
		Class338_Sub2 class338_sub2 = Class144.method1481(1, true, aBoolean6633);
		if (Class296_Sub39_Sub10.aBoolean6177) {
			aClass178_6634.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
		} else {
			aClass178_6634.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
		}
		return class338_sub2;
	}

	@Override
	final int method3462(byte i) {
		if (i != 28) {
			aByte6632 = (byte) 30;
		}
		if (aClass178_6634 == null) {
			return 0;
		}
		return aClass178_6634.ma();
	}

	static final void method3485() {
		if (Class399.aClass247ArrayArrayArray3355 != null) {
			for (int i = 0; i < Class399.aClass247ArrayArrayArray3355.length; i++) {
				for (int i_25_ = 0; i_25_ < Class228.anInt2201; i_25_++) {
					for (int i_26_ = 0; i_26_ < Class368_Sub12.anInt5488; i_26_++) {
						if (Class399.aClass247ArrayArrayArray3355[i][i_25_][i_26_] != null) {
							Class399.aClass247ArrayArrayArray3355[i][i_25_][i_26_].method2189(false);
						}
						Class399.aClass247ArrayArrayArray3355[i][i_25_][i_26_] = null;
					}
				}
			}
		}
		Class399.aClass247ArrayArrayArray3355 = null;
		Class244.aSArray2320 = null;
		if (Class351.aClass247ArrayArrayArray3041 != null) {
			for (int i = 0; i < Class351.aClass247ArrayArrayArray3041.length; i++) {
				for (int i_27_ = 0; i_27_ < Class228.anInt2201; i_27_++) {
					for (int i_28_ = 0; i_28_ < Class368_Sub12.anInt5488; i_28_++) {
						if (Class351.aClass247ArrayArrayArray3041[i][i_27_][i_28_] != null) {
							Class351.aClass247ArrayArrayArray3041[i][i_27_][i_28_].method2189(false);
						}
						Class351.aClass247ArrayArrayArray3041[i][i_27_][i_28_] = null;
					}
				}
			}
		}
		Class351.aClass247ArrayArrayArray3041 = null;
		Class52.aSArray636 = null;
		Class338_Sub2.aClass247ArrayArrayArray5195 = null;
		Class360_Sub2.aSArray5304 = null;
		Class296_Sub15_Sub2.aBooleanArrayArray6006 = null;
		Class370.aBooleanArrayArray3140 = null;
		Class285.anIntArray2624 = null;
		Class121.aBooleanArrayArrayArray1268 = null;
		Class241_Sub2_Sub2.aBooleanArrayArrayArray5907 = null;
		Class154.method1557((byte) -65);
		if (s.aClass338_Sub3_Sub1Array2833 != null) {
			for (int i = 0; i < Class127.anInt1303; i++) {
				s.aClass338_Sub3_Sub1Array2833[i] = null;
			}
			Class127.anInt1303 = 0;
		}
		Class368_Sub10.aClass338_Sub3Array5481 = null;
		Class368_Sub16.aClass338_Sub3Array5525 = null;
		Class338_Sub3_Sub3.aClass338_Sub3Array6568 = null;
		if (Class296_Sub51_Sub37.aClass338_Sub3Array6531 != null) {
			for (int i = 0; i < Class296_Sub51_Sub37.aClass338_Sub3Array6531.length; i++) {
				Class296_Sub51_Sub37.aClass338_Sub3Array6531[i] = null;
			}
			Class338_Sub8_Sub2.anInt6584 = 0;
		}
		if (Model.aClass338_Sub3Array1850 != null) {
			for (int i = 0; i < Model.aClass338_Sub3Array1850.length; i++) {
				Model.aClass338_Sub3Array1850[i] = null;
			}
			CS2Call.anInt4963 = 0;
		}
		if (Class302.aClass93Array2721 != null) {
			for (int i = 0; i < Class368_Sub4.anInt5440; i++) {
				Class302.aClass93Array2721[i] = null;
			}
			for (int i = 0; i < Class368_Sub9.anInt5477; i++) {
				for (int i_29_ = 0; i_29_ < Class228.anInt2201; i_29_++) {
					for (int i_30_ = 0; i_30_ < Class368_Sub12.anInt5488; i_30_++) {
						Class49.aLongArrayArrayArray462[i][i_29_][i_30_] = 0L;
					}
				}
			}
			Class368_Sub4.anInt5440 = 0;
		}
		Class261.method2247(-10899);
		CS2Stack.aClass264_2249 = CS2Stack.aClass264_2243;
		CS2Stack.aClass264_2249.method2277((byte) 125);
		Mobile.aByteArrayArray6776 = null;
		Class296_Sub51_Sub32.anIntArrayArray6514 = null;
		StaticMethods.aShortArrayArray5919 = null;
		if (StaticMethods.aClass142Array5951 != null) {
			Class306.method3285();
			Class16_Sub2.aHa3733.f(1);
			Class16_Sub2.aHa3733.i(0);
		}
		if (Class127.aClass299Array1301 != null) {
			Class127.aClass299Array1301 = null;
		}
		Class16_Sub2.aHa3733 = null;
	}

	@Override
	final void method3467(int i, int i_31_, Class338_Sub3 class338_sub3, boolean bool, int i_32_, int i_33_, ha var_ha) {
		int i_34_ = 63 % ((20 - i_33_) / 48);
		do {
			if (!(class338_sub3 instanceof Class338_Sub3_Sub4_Sub2)) {
				if (class338_sub3 instanceof Class338_Sub3_Sub1_Sub1) {
					Class338_Sub3_Sub1_Sub1 class338_sub3_sub1_sub1_35_ = (Class338_Sub3_Sub1_Sub1) class338_sub3;
					if (aClass178_6634 != null && class338_sub3_sub1_sub1_35_.aClass178_6634 != null) {
						aClass178_6634.method1736(class338_sub3_sub1_sub1_35_.aClass178_6634, i_31_, i_32_, i, bool);
					}
				}
			} else {
				Class338_Sub3_Sub4_Sub2 class338_sub3_sub4_sub2 = (Class338_Sub3_Sub4_Sub2) class338_sub3;
				if (aClass178_6634 == null || class338_sub3_sub4_sub2.aClass178_6674 == null) {
					break;
				}
				aClass178_6634.method1736(class338_sub3_sub4_sub2.aClass178_6674, i_31_, i_32_, i, bool);
			}
			break;
		} while (false);
	}

	@Override
	final Class96 method3461(ha var_ha, int i) {
		int i_36_ = 68 / ((79 - i) / 44);
		if (aClass96_6629 == null) {
			aClass96_6629 = Class41_Sub21.method478(anInt5213, method3479(72, var_ha, 0), tileX, tileY, (byte) -67);
		}
		return aClass96_6629;
	}
}
