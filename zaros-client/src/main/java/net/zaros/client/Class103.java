package net.zaros.client;

/* Class103 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class103 {
	static boolean aBoolean1086 = true;
	static IncomingPacket aClass231_1087 = new IncomingPacket(70, -2);

	static final void method893(int i) {
		Class368_Sub16.aHa5527.L(ha_Sub3.anInt4115, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub1_5008.method389(116) != 1 ? -1 : Class296_Sub28.anInt4799 + 256 << 2), 0);
		if (i != -2)
			aClass231_1087 = null;
	}

	static final InterfaceComponent method894(int i, int i_0_, int i_1_) {
		InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(i_0_);
		if (i == (i_1_ ^ 0xffffffff))
			return class51;
		if (class51 == null || class51.aClass51Array555 == null || class51.aClass51Array555.length <= i_1_)
			return null;
		return class51.aClass51Array555[i_1_];
	}

	public static void method895(int i) {
		if (i == 0)
			aClass231_1087 = null;
	}

	static final void method896(int i, ha var_ha) {
		if (i != 0)
			method896(-56, null);
		if (SeekableFile.aBoolean2397)
			Class205_Sub4.method1985(var_ha, (byte) -121);
		else
			Class262.method2262(var_ha, (byte) -87);
	}

	static final void method897(int i, int i_2_) {
		if (i != 1)
			aClass231_1087 = null;
		if (i_2_ < 0 || i_2_ > 2)
			i_2_ = 0;
		AdvancedMemoryCache.anInt1167 = i_2_;
		Class380.aClass338_Sub1Array3201 = (new Class338_Sub1[Class316.anIntArray2801[AdvancedMemoryCache.anInt1167] + 1]);
		Class379_Sub2_Sub1.anInt6606 = 0;
		Model.anInt1848 = 0;
	}

	static final boolean method898(int i) {
		if (ReferenceWrapper.anInt6128 != 0)
			return true;
		if (i >= -39)
			return true;
		return Class235.aClass296_Sub45_Sub4_2229.method3009(-81);
	}
}
