package net.zaros.client.launch;

import java.applet.Applet;
import java.awt.EventQueue;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;

import net.zaros.client.Applet_Sub1;
import net.zaros.client.Constants;
import net.zaros.client.GameClient;
import net.zaros.client.ui.GameFrame;
import net.zaros.client.ui.skin.ZarosLookAndFeel;

/**
 * @author Walied K. Yassen
 */
public final class Bootstrap extends Applet {

	/**
	 * The serialisation key of the {@link Bootstrap} type.
	 */
	private static final long serialVersionUID = -794016480477518369L;

	/**
	 * The applet parameters list.
	 */
	private final Map<String, String> parameters = new HashMap<>();

	/**
	 * The game client instance.
	 */
	private final GameClient client;

	/**
	 * The client frame instance.
	 */
	private final GameFrame frame;

	/**
	 * Constructs a new {@link Bootstrap} type object instance.
	 */
	private Bootstrap() {
		client = new GameClient();
		frame = new GameFrame(this, client);
	}

	/**
	 * Initialises the GameClient.
	 */
	public void initialise() {
		initialise_params();
		initialise_frame();
		initialise_client();
	}

	/**
	 * Initialises the applet parameters.
	 */
	private void initialise_params() {
		parameters.put("java_arguments", "-Xmx102m -Dsun.java2d.noddraw=true");
		parameters.put("colourid", "0");
		parameters.put("worldid", "1");
		parameters.put("lobbyid", "1");
		parameters.put("lobbyaddress", Constants.HOST_ADDRESS);
		parameters.put("demoid", "0");
		parameters.put("demoaddress", "");
		parameters.put("modewhere", "0");
		parameters.put("modewhat", "0");
		parameters.put("lang", "0");
		parameters.put("objecttag", "0");
		parameters.put("js", "1");
		parameters.put("game", "0");
		parameters.put("affid", "0");
		parameters.put("advert", "1");
		parameters.put("settings", "wwGlrZHF5gJcZl7tf7KSRh0MZLhiU0gI0xDX6DwZ-Qk");
		parameters.put("country", "0");
		parameters.put("haveie6", "0");
		parameters.put("havefirefox", "1");
		parameters.put("cookieprefix", "");
		parameters.put("cookiehost", Constants.HOST_ADDRESS);
		parameters.put("cachesubdirid", "0");
		parameters.put("crashurl", "");
		parameters.put("unsignedurl", "");
		parameters.put("sitesettings_member", "1");
		parameters.put("frombilling", "false");
		parameters.put("sskey", "");
		parameters.put("force64mb", "false");
		parameters.put("worldflags", "8");
	}

	/**
	 * Initialises the frame.
	 */
	private void initialise_frame() {
		// NOOP
	}

	/**
	 * Initialises the client.
	 */
	private void initialise_client() {
		Applet_Sub1.provideLoaderApplet(this);
		client.init();
	}

	/**
	 * Bootstraps the GameClient.
	 */
	public void boot() {
		frame.start();
		client.start();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.applet.Applet#getParameter(java.lang.String)
	 */
	@Override
	public String getParameter(String name) {
		if (parameters.containsKey(name)) {
			return parameters.get(name);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.applet.Applet#getDocumentBase()
	 */
	@Override
	public URL getDocumentBase() {
		return getCodeBase();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.applet.Applet#getCodeBase()
	 */
	@Override
	public URL getCodeBase() {
		try {
			return new URL("http://" + Constants.HOST_ADDRESS);
		} catch (Throwable e) {
			return super.getCodeBase();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				JFrame.setDefaultLookAndFeelDecorated(true);
				JDialog.setDefaultLookAndFeelDecorated(true);
				UIManager.setLookAndFeel(new ZarosLookAndFeel());
				Bootstrap bootstrap = new Bootstrap();
				bootstrap.initialise();
				bootstrap.boot();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		});
	}
}
