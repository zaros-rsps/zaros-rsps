package net.zaros.client;

/* Class338_Sub3_Sub3_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub3_Sub3_Sub2 extends Class338_Sub3_Sub3 implements Interface14 {
	private boolean aBoolean6624;
	private Class96 aClass96_6625;
	Class380 aClass380_6626;
	private boolean aBoolean6627 = true;
	static long aLong6628;

	static final void method3564(int i, int i_0_, boolean bool) {
		if (i != -1)
			method3564(66, 8, false);
		if (bool) {
			Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 84, Class241_Sub1.aClass311_4576);
			class296_sub1.out.p2(i_0_);
			Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
		} else
			CS2Executor.method1520(Class328.aClass81_2911, i_0_, -1);
	}

	final boolean method3459(int i) {
		if (i != 0)
			method58(-42);
		return false;
	}

	final Class96 method3461(ha var_ha, int i) {
		int i_1_ = -65 % ((i - 79) / 44);
		return aClass96_6625;
	}

	final Class338_Sub2 method3473(ha var_ha, byte i) {
		Model class178 = aClass380_6626.method3975(true, false, var_ha, 2048, (byte) 39);
		if (class178 == null)
			return null;
		Class373 class373 = var_ha.f();
		class373.method3902(aShort6567 + tileX, anInt5213, tileY + aShort6569);
		Class338_Sub2 class338_sub2 = Class144.method1481(1, true, aBoolean6624);
		int i_2_ = tileX >> 9;
		int i_3_ = tileY >> 9;
		aClass380_6626.method3977(class373, true, class178, i_2_, false, var_ha, i_3_, i_2_, i_3_);
		if (Class296_Sub39_Sub10.aBoolean6177)
			class178.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
		else
			class178.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
		if (aClass380_6626.aClass338_Sub1_3202 != null) {
			Class390 class390 = aClass380_6626.aClass338_Sub1_3202.method3444();
			if (Class296_Sub39_Sub10.aBoolean6177)
				var_ha.a(class390, ModeWhat.anInt1192);
			else
				var_ha.a(class390);
		}
		aBoolean6627 = class178.F() || aClass380_6626.aClass338_Sub1_3202 != null;
		if (i > -84)
			return null;
		if (aClass96_6625 == null)
			aClass96_6625 = Class41_Sub21.method478(anInt5213, class178, tileX, tileY, (byte) -111);
		else
			Billboard.method4025(class178, 124, tileY, anInt5213, aClass96_6625, tileX);
		return class338_sub2;
	}

	public final int method59(int i) {
		if (i <= 16)
			method3564(-52, 58, true);
		return aClass380_6626.anInt3200;
	}

	Class338_Sub3_Sub3_Sub2(ha var_ha, ObjectDefinition class70, int i, int i_4_, int i_5_, int i_6_, int i_7_, boolean bool, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_) {
		super(i_5_, i_6_, i_7_, i, i_4_, i_8_, i_9_);
		aClass380_6626 = new Class380(var_ha, class70, i_10_, i_11_, z, i_4_, this, bool, i_12_);
		aBoolean6624 = class70.interactonType != 0 && !bool;
	}

	public final void method58(int i) {
		if (i != -19727) {
			/* empty */
		}
	}

	public final int method54(int i) {
		if (i != -11077)
			return -123;
		return aClass380_6626.anInt3203;
	}

	public final void method56(ha var_ha, byte i) {
		if (i != -117)
			method58(-124);
		aClass380_6626.method3976(262144, var_ha);
	}

	final void method3460(int i, ha var_ha) {
		int i_13_ = 86 % ((i + 41) / 62);
		Model class178 = aClass380_6626.method3975(true, false, var_ha, 262144, (byte) 39);
		if (class178 != null) {
			int i_14_ = tileX >> 9;
			int i_15_ = tileY >> 9;
			Class373 class373 = var_ha.f();
			class373.method3902(tileX, anInt5213, tileY);
			aClass380_6626.method3977(class373, false, class178, i_14_, false, var_ha, i_15_, i_14_, i_15_);
		}
	}

	final int method3466(byte i) {
		if (i <= 77)
			return -9;
		return aClass380_6626.method3980(true);
	}

	final void method3565(Class375 class375, byte i) {
		if (i >= -75)
			aBoolean6627 = true;
		aClass380_6626.method3984(-5581, class375);
	}

	public final boolean method55(byte i) {
		if (i != -57)
			return true;
		return aClass380_6626.method3988(i + 61);
	}

	final int method3462(byte i) {
		if (i != 28)
			aClass380_6626 = null;
		return aClass380_6626.method3982(14356);
	}

	public final int method57(byte i) {
		if (i <= 83)
			method59(-13);
		return aClass380_6626.anInt3216;
	}

	final boolean method3469(int i) {
		if (i <= 82)
			method3466((byte) -43);
		return aBoolean6627;
	}

	final boolean method3475(int i, int i_16_, ha var_ha, int i_17_) {
		Model class178 = aClass380_6626.method3975(false, false, var_ha, 131072, (byte) 39);
		if (class178 == null)
			return false;
		if (i_16_ >= -48)
			return true;
		Class373 class373 = var_ha.f();
		class373.method3902(tileX + aShort6567, anInt5213, tileY + aShort6569);
		if (Class296_Sub39_Sub10.aBoolean6177)
			return class178.method1731(i_17_, i, class373, false, 0, ModeWhat.anInt1192);
		return class178.method1732(i_17_, i, class373, false, 0);
	}

	public final void method60(byte i, ha var_ha) {
		aClass380_6626.method3987(var_ha, (byte) -53);
		int i_18_ = -33 % ((i + 23) / 35);
	}
}
