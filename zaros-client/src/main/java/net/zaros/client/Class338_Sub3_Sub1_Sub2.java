package net.zaros.client;
/* Class338_Sub3_Sub1_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;

final class Class338_Sub3_Sub1_Sub2 extends Class338_Sub3_Sub1 {
	private int anInt6735;
	int anInt6736;
	private double aDouble6737;
	int anInt6738;
	private double aDouble6739;
	private int anInt6740 = 0;
	static Interface1[] anInterface1Array6741 = new Interface1[75];
	private int anInt6742;
	private int anInt6743;
	private boolean aBoolean6744;
	private int anInt6745;
	private double aDouble6746;
	private double aDouble6747;
	private int anInt6748;
	private boolean aBoolean6749;
	private int anInt6750;
	int anInt6751;
	private int anInt6752;
	private int anInt6753 = 0;
	private int anInt6754;
	private double aDouble6755;
	private double aDouble6756;
	private Class338_Sub1 aClass338_Sub1_6757;
	private double aDouble6758;
	static int anInt6759;
	private Animator aClass44_6760;
	private double aDouble6761;
	int anInt6762;
	private boolean aBoolean6763 = false;

	final void method3467(int i, int i_0_, Class338_Sub3 class338_sub3, boolean bool, int i_1_, int i_2_, ha var_ha) {
		int i_3_ = -117 % ((i_2_ - 20) / 48);
		throw new IllegalStateException();
	}

	final void method3486(int i) {
		if (i == 1643253353) {
			if (aClass338_Sub1_6757 != null)
				aClass338_Sub1_6757.method3439();
		}
	}

	final void method3487(int i) {
		if (!aBoolean6749) {
			if (anInt6742 != 0) {
				Mobile class338_sub3_sub1_sub3 = null;
				if (za_Sub1.anInt6554 != 3) {
					if (anInt6742 >= 0) {
						int i_4_ = anInt6742 - 1;
						NPCNode class296_sub7 = ((NPCNode) Class41_Sub18.localNpcs.get((long) i_4_));
						if (class296_sub7 != null)
							class338_sub3_sub1_sub3 = (class296_sub7.value);
					} else {
						int i_5_ = -anInt6742 - 1;
						if (Class362.myPlayerIndex == i_5_)
							class338_sub3_sub1_sub3 = (Class296_Sub51_Sub11.localPlayer);
						else
							class338_sub3_sub1_sub3 = (PlayerUpdate.visiblePlayers[i_5_]);
					}
				} else
					class338_sub3_sub1_sub3 = Class379.aClass302Array3624[anInt6742 - 1].method3262(0);
				if (class338_sub3_sub1_sub3 != null) {
					tileY = class338_sub3_sub1_sub3.tileY;
					tileX = class338_sub3_sub1_sub3.tileX;
					anInt5213 = (aa_Sub1.method155(-1537652855, z, class338_sub3_sub1_sub3.tileX, class338_sub3_sub1_sub3.tileY) - anInt6752);
					if (anInt6748 >= 0) {
						Class280 class280 = class338_sub3_sub1_sub3.method3516(false);
						int i_6_ = 0;
						int i_7_ = 0;
						if (class280.anIntArrayArray2558 != null && (class280.anIntArrayArray2558[anInt6748] != null)) {
							i_6_ += class280.anIntArrayArray2558[anInt6748][0];
							i_7_ += class280.anIntArrayArray2558[anInt6748][2];
						}
						if (class280.anIntArrayArray2561 != null && (class280.anIntArrayArray2561[anInt6748] != null)) {
							i_7_ += class280.anIntArrayArray2561[anInt6748][2];
							i_6_ += class280.anIntArrayArray2561[anInt6748][0];
						}
						if (i_6_ != 0 || i_7_ != 0) {
							int i_8_ = class338_sub3_sub1_sub3.aClass5_6804.method175((byte) -81);
							int i_9_ = i_8_;
							if (class338_sub3_sub1_sub3.anIntArray6823 != null && (class338_sub3_sub1_sub3.anIntArray6823[anInt6748]) != -1)
								i_9_ = (class338_sub3_sub1_sub3.anIntArray6823[anInt6748]);
							int i_10_ = i_9_ - i_8_ & 0x3fff;
							int i_11_ = Class296_Sub4.anIntArray4598[i_10_];
							int i_12_ = Class296_Sub4.anIntArray4618[i_10_];
							int i_13_ = i_11_ * i_7_ + i_6_ * i_12_ >> 14;
							i_7_ = -(i_11_ * i_6_) + i_7_ * i_12_ >> 14;
							i_6_ = i_13_;
							tileY += i_7_;
							tileX += i_6_;
						}
					}
				}
			}
			if (i != 5937)
				method3486(-7);
		}
	}

	private final Model method3488(int i, int i_14_, ha var_ha) {
		if (i <= 55)
			aDouble6737 = 1.5631459873114963;
		Graphic class23 = Class157.graphicsLoader.getGraphic(anInt6745);
		return class23.method291(var_ha, (byte) -93, (byte) 2, aClass44_6760, i_14_);
	}

	final void method3489(boolean bool, int i) {
		aBoolean6749 = true;
		aDouble6755 += aDouble6747 * (double) i;
		aDouble6746 += aDouble6737 * (double) i;
		if (aBoolean6744)
			aDouble6758 = (double) (aa_Sub1.method155(-1537652855, z, (int) aDouble6746, (int) aDouble6755) - anInt6752);
		else if (anInt6735 != -1) {
			aDouble6758 += ((double) i * ((double) i * (aDouble6761 * 0.5)) + aDouble6756 * (double) i);
			aDouble6756 += (double) i * aDouble6761;
		} else
			aDouble6758 += (double) i * aDouble6756;
		anInt6754 = (int) (Math.atan2(aDouble6737, aDouble6747) * 2607.5945876176133) + 8192 & 0x3fff;
		anInt6743 = (int) (Math.atan2(aDouble6756, aDouble6739) * 2607.5945876176133) & 0x3fff;
		if (aClass44_6760.method559(1, (byte) 123) && aClass44_6760.method546(-126))
			aClass44_6760.method547(14899);
		if (bool)
			aDouble6747 = 0.013304813803443352;
	}

	final boolean method3469(int i) {
		if (i < 82)
			return false;
		return aBoolean6763;
	}

	final void method3477(int i) {
		aShort6560 = aShort6559 = (short) (int) (aDouble6746 / 512.0);
		aShort6564 = aShort6558 = (short) (int) (aDouble6755 / 512.0);
		if (i != -1)
			finalize();
	}

	final boolean method3459(int i) {
		if (i != 0)
			return false;
		return false;
	}

	protected final void finalize() {
		if (aClass338_Sub1_6757 != null)
			aClass338_Sub1_6757.method3439();
	}

	final boolean method3475(int i, int i_15_, ha var_ha, int i_16_) {
		if (i_15_ >= -48)
			return true;
		return false;
	}

	Class338_Sub3_Sub1_Sub2(int i, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_, int i_27_, int i_28_, boolean bool, int i_29_) {
		super(i_17_, i_18_, i_19_, aa_Sub1.method155(-1537652855, i_17_, i_19_, i_20_) - i_21_, i_20_, i_19_ >> 9, i_19_ >> 9, i_20_ >> 9, i_20_ >> 9, false, (byte) 0);
		aBoolean6749 = false;
		aBoolean6749 = false;
		anInt6735 = i_24_;
		anInt6762 = i_28_;
		anInt6738 = i_23_;
		anInt6745 = i;
		anInt6750 = i_25_;
		anInt6751 = i_22_;
		anInt6736 = i_27_;
		anInt6752 = i_21_;
		anInt6742 = i_26_;
		aBoolean6744 = bool;
		anInt6748 = i_29_;
		int i_30_ = Class157.graphicsLoader.getGraphic(anInt6745).anInt264;
		aClass44_6760 = new Class44_Sub1(this, false);
		aClass44_6760.method549((byte) 115, i_30_);
	}

	final Class96 method3461(ha var_ha, int i) {
		int i_31_ = -47 % ((i - 79) / 44);
		return null;
	}

	final Class338_Sub2 method3473(ha var_ha, byte i) {
		Model class178 = method3488(108, 2048, var_ha);
		if (class178 == null)
			return null;
		Class373 class373 = var_ha.f();
		class373.method3909(anInt6743);
		class373.method3917(anInt6754);
		class373.method3904((int) aDouble6746, (int) aDouble6758, (int) aDouble6755);
		if (i > -84)
			method3492(null, -105, null, -43);
		method3490(var_ha, class373, class178, 7574);
		Class338_Sub2 class338_sub2 = Class144.method1481(1, true, false);
		if (Class296_Sub39_Sub10.aBoolean6177)
			class178.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
		else
			class178.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
		if (aClass338_Sub1_6757 != null) {
			Class390 class390 = aClass338_Sub1_6757.method3444();
			if (!Class296_Sub39_Sub10.aBoolean6177)
				var_ha.a(class390);
			else
				var_ha.a(class390, ModeWhat.anInt1192);
		}
		aBoolean6763 = class178.F();
		anInt6753 = class178.fa();
		anInt6740 = class178.ma();
		return class338_sub2;
	}

	final int method3462(byte i) {
		if (i != 28)
			method3469(-37);
		return anInt6740;
	}

	final void method3460(int i, ha var_ha) {
		int i_32_ = 41 / ((i + 41) / 62);
		Model class178 = method3488(91, 0, var_ha);
		if (class178 != null) {
			Class373 class373 = var_ha.f();
			class373.method3909(anInt6743);
			class373.method3917(anInt6754);
			class373.method3904((int) aDouble6746, (int) aDouble6758, (int) aDouble6755);
			anInt6753 = class178.fa();
			anInt6740 = class178.ma();
			method3490(var_ha, class373, class178, 7574);
		}
	}

	private final void method3490(ha var_ha, Class373 class373, Model class178, int i) {
		if (i != 7574)
			anInt6762 = 14;
		class178.method1718(class373);
		EmissiveTriangle[] class89s = class178.method1735();
		EffectiveVertex[] class232s = class178.method1729();
		if ((aClass338_Sub1_6757 == null || aClass338_Sub1_6757.aBoolean5177) && (class89s != null || class232s != null))
			aClass338_Sub1_6757 = Class338_Sub1.method3442(Class29.anInt307, true);
		if (aClass338_Sub1_6757 != null) {
			aClass338_Sub1_6757.method3452(var_ha, (long) Class29.anInt307, class89s, class232s, false);
			aClass338_Sub1_6757.method3450(z, aShort6560, aShort6559, aShort6564, aShort6558);
		}
	}

	final void method3491(int i, int i_33_, int i_34_, int i_35_, int i_36_) {
		if (!aBoolean6749) {
			double d = (double) (-tileX + i);
			double d_37_ = (double) (-tileY + i_35_);
			double d_38_ = Math.sqrt(d * d + d_37_ * d_37_);
			aDouble6746 = (double) tileX + d * (double) anInt6750 / d_38_;
			aDouble6755 = (double) anInt6750 * d_37_ / d_38_ + (double) tileY;
			if (!aBoolean6744)
				aDouble6758 = (double) anInt5213;
			else
				aDouble6758 = (double) (aa_Sub1.method155(i_36_ + 388788411, z, (int) aDouble6746, (int) aDouble6755) - anInt6752);
		}
		if (i_36_ != -1926441266)
			aDouble6761 = 0.4789837743869849;
		double d = (double) (anInt6738 + 1 - i_34_);
		aDouble6737 = ((double) i - aDouble6746) / d;
		aDouble6747 = ((double) i_35_ - aDouble6755) / d;
		aDouble6739 = Math.sqrt(aDouble6737 * aDouble6737 + aDouble6747 * aDouble6747);
		if (anInt6735 == -1)
			aDouble6756 = ((double) i_33_ - aDouble6758) / d;
		else {
			if (!aBoolean6749)
				aDouble6756 = -aDouble6739 * Math.tan((double) anInt6735 * 0.02454369);
			aDouble6761 = (-(d * aDouble6756) + ((double) i_33_ - aDouble6758)) * 2.0 / (d * d);
		}
	}

	final boolean method3468(int i) {
		if (i > -29)
			anInt6740 = -12;
		return false;
	}

	static final ha method3492(Canvas canvas, int i, d var_d, int i_39_) {
		int i_40_ = 106 / ((45 - i) / 55);
		return new ha_Sub3(canvas, var_d, i_39_);
	}

	public static void method3493(boolean bool) {
		anInterface1Array6741 = null;
		if (bool)
			method3492(null, 126, null, -69);
	}

	final void method3472(byte i) {
		int i_41_ = -122 / ((-56 - i) / 38);
		throw new IllegalStateException();
	}

	final int method3466(byte i) {
		if (i < 77)
			anInt6742 = 9;
		return anInt6753;
	}
}
