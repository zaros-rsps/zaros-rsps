package net.zaros.client;

/* Class296_Sub36 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub36 extends Node {
	boolean aBoolean4850;
	int anInt4851;
	Class296_Sub18 aClass296_Sub18_4852;
	int[] anIntArray4853;
	int anInt4854;
	boolean aBoolean4855;
	int anInt4856;
	int anInt4857;
	int anInt4858;
	int anInt4859;
	Class296_Sub45_Sub1 aClass296_Sub45_Sub1_4860;
	int anInt4861;
	Class296_Sub19_Sub1 aClass296_Sub19_Sub1_4862;
	int anInt4863;
	Player aClass338_Sub3_Sub1_Sub3_Sub1_4864;
	static float aFloat4865 = 0.0F;
	Class296_Sub18 aClass296_Sub18_4866;
	int anInt4867;
	int anInt4868 = 0;
	int anInt4869;
	static int[][][] anIntArrayArrayArray4870;
	Class296_Sub45_Sub1 aClass296_Sub45_Sub1_4871;
	int anInt4872;
	static int[][] someSecondaryXteaBuffer;
	boolean aBoolean4874;
	Class296_Sub19_Sub1 aClass296_Sub19_Sub1_4875;
	int anInt4876;
	ObjectDefinition aClass70_4877;
	NPC aClass338_Sub3_Sub1_Sub3_Sub2_4878;
	int anInt4879;
	int anInt4880;

	public static void method2761(int i) {
		if (i < -15) {
			someSecondaryXteaBuffer = null;
			anIntArrayArrayArray4870 = null;
		}
	}

	static final void method2762(int i, byte i_0_, int i_1_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_1_, 6);
		class296_sub39_sub5.insertIntoQueue_2();
		if (i_0_ < 124)
			method2764(-120L, (byte) -93, null);
		class296_sub39_sub5.intParam = i;
	}

	final void method2763(int i) {
		if (i != 0)
			anInt4863 = 57;
		int i_2_ = anInt4851;
		boolean bool = aBoolean4850;
		if (aClass70_4877 != null) {
			ObjectDefinition class70 = aClass70_4877.method757((za_Sub1.anInt6554 != 3 ? (IConfigsRegister) (Class16_Sub3_Sub1.configsRegister) : Class205_Sub1.anInterface17_5640), false);
			if (class70 == null) {
				anInt4856 = 0;
				aBoolean4850 = false;
				aBoolean4855 = false;
				anIntArray4853 = null;
				anInt4854 = 0;
				anInt4857 = 256;
				anInt4858 = 0;
				anInt4851 = -1;
				anInt4863 = 0;
				anInt4876 = 0;
				anInt4872 = 256;
			} else {
				anInt4851 = class70.anInt811;
				anInt4872 = class70.anInt808;
				anInt4863 = class70.anInt761 << 9;
				anIntArray4853 = class70.anIntArray766;
				anInt4854 = class70.anInt768;
				aBoolean4850 = class70.aBoolean810;
				anInt4858 = class70.anInt833;
				anInt4856 = class70.anInt815;
				aBoolean4855 = class70.aBoolean781;
				anInt4857 = class70.anInt825;
			}
		} else if (aClass338_Sub3_Sub1_Sub3_Sub2_4878 != null) {
			int i_3_ = Class137.method1422(aClass338_Sub3_Sub1_Sub3_Sub2_4878, (byte) 67);
			if (i_2_ != i_3_) {
				anInt4851 = i_3_;
				NPCDefinition class147 = aClass338_Sub3_Sub1_Sub3_Sub2_4878.definition;
				if (class147.configData != null)
					class147 = class147.getNPCDefinitionByConfigID(Class16_Sub3_Sub1.configsRegister);
				if (class147 == null) {
					anInt4872 = 256;
					anInt4856 = anInt4863 = anInt4876 = 0;
					anInt4857 = 256;
					aBoolean4850 = (aClass338_Sub3_Sub1_Sub3_Sub2_4878.definition.aBoolean1481);
				} else {
					anInt4876 = class147.anInt1464 << 9;
					anInt4872 = class147.anInt1463;
					anInt4863 = class147.anInt1504 << 9;
					anInt4857 = class147.anInt1502;
					anInt4856 = class147.anInt1456;
					aBoolean4850 = class147.aBoolean1481;
				}
			}
		} else if (aClass338_Sub3_Sub1_Sub3_Sub1_4864 != null) {
			anInt4851 = Class41_Sub5.method410(aClass338_Sub3_Sub1_Sub3_Sub1_4864, 11736);
			anInt4872 = 256;
			anInt4863 = aClass338_Sub3_Sub1_Sub3_Sub1_4864.anInt6881 << 9;
			anInt4857 = 256;
			anInt4876 = 0;
			anInt4856 = aClass338_Sub3_Sub1_Sub3_Sub1_4864.anInt6887;
			aBoolean4850 = aClass338_Sub3_Sub1_Sub3_Sub1_4864.hasDisplayName;
		}
		do {
			if (i_2_ != anInt4851 || bool == !aBoolean4850) {
				if (aClass296_Sub45_Sub1_4871 == null)
					break;
				Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(aClass296_Sub45_Sub1_4871);
				aClass296_Sub18_4866 = null;
				aClass296_Sub45_Sub1_4871 = null;
				aClass296_Sub19_Sub1_4862 = null;
			}
			break;
		} while (false);
	}

	static final void method2764(long l, byte i, ha var_ha) {
		Class236.anInt2235 = 0;
		if (i != -56)
			someSecondaryXteaBuffer = null;
		Class79.anInt888 = Class246.anInt2334;
		Class196.anInt1991 = 0;
		Class246.anInt2334 = 0;
		long l_4_ = Class72.method771(i ^ 0x46);
		for (Class338_Sub1 class338_sub1 = ((Class338_Sub1) Class368_Sub19.aClass404_5542.method4160((byte) 116)); class338_sub1 != null; class338_sub1 = (Class338_Sub1) Class368_Sub19.aClass404_5542.method4163(-24917)) {
			if (class338_sub1.method3448(var_ha, l))
				Class236.anInt2235++;
		}
		if (Class100.aBoolean1070 && l % 100L == 0L) {
			System.out.println("Particle system count: " + Class368_Sub19.aClass404_5542.method4159((byte) -40) + ", running: " + Class236.anInt2235);
			System.out.println("Emitters: " + Class196.anInt1991 + " Particles: " + Class246.anInt2334 + ". Time taken: " + (Class72.method771(-119) + -l_4_) + "ms");
		}
	}

	static final void method2765(int i, Node class296, Node class296_5_) {
		if (i == 0) {
			if (class296.prev != null)
				class296.unlink();
			class296.next = class296_5_;
			class296.prev = class296_5_.prev;
			class296.prev.next = class296;
			class296.next.prev = class296;
		}
	}

	public Class296_Sub36() {
		/* empty */
	}
}
