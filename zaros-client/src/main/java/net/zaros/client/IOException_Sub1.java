package net.zaros.client;
/* IOException_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;

final class IOException_Sub1 extends IOException {
	static int anInt36;
	static float[] aFloatArray37 = new float[2];
	static Interface13[] anInterface13Array38;

	static final void method131(byte i) {
		for (Class296_Sub36 class296_sub36 = ((Class296_Sub36) Class126.aClass155_1289.removeFirst((byte) 125)); class296_sub36 != null; class296_sub36 = (Class296_Sub36) Class126.aClass155_1289.removeNext(1001)) {
			if (class296_sub36.aBoolean4874)
				class296_sub36.method2763(0);
		}
		for (Class296_Sub36 class296_sub36 = ((Class296_Sub36) Class296_Sub15_Sub4.aClass155_6031.removeFirst((byte) 123)); class296_sub36 != null; class296_sub36 = ((Class296_Sub36) Class296_Sub15_Sub4.aClass155_6031.removeNext(1001))) {
			if (class296_sub36.aBoolean4874)
				class296_sub36.method2763(0);
		}
		if (i > -28)
			method132(88);
	}

	IOException_Sub1(String string) {
		super(string);
	}

	public static void method132(int i) {
		if (i <= 67)
			aFloatArray37 = null;
		anInterface13Array38 = null;
		aFloatArray37 = null;
	}

	static final boolean method133(MidiDecoder class296_sub47, byte i) {
		if (i >= -33)
			method131((byte) 57);
		if (class296_sub47 == null)
			return true;
		za.aClass25_5085 = null;
		if (Class368_Sub21.aClass296_Sub47_5555 != class296_sub47) {
			Class368_Sub21.aClass296_Sub47_5555 = class296_sub47;
			Class34.anInt336 = 0;
		}
		ReferenceWrapper.anInt6128 = 0;
		SubInPacket.aClass296_Sub47_2421 = null;
		Class249.aClass296_Sub45_Sub4_2357 = null;
		TextureOperation.aClass138_5040 = null;
		if (Class34.anInt336 == 0) {
			Class296_Sub45_Sub3.aClass25_6295 = new Class25(Class88.aClass138_944, Class375.aClass138_3182);
			Class235.aClass296_Sub45_Sub4_2229.method3005(false);
			Class34.anInt336 = 1;
		}
		if (Class34.anInt336 == 1) {
			if (!Class235.aClass296_Sub45_Sub4_2229.method3008(Class286.aClass138_2639, class296_sub47, Class296_Sub45_Sub3.aClass25_6295, false, 22050))
				return false;
			Class368_Sub21.aClass296_Sub47_5555 = null;
			Class34.anInt336 = 0;
			Class296_Sub45_Sub3.aClass25_6295 = null;
		}
		return true;
	}
}
