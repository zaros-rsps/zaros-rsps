package net.zaros.client;

/* Class382 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class382 {
	static IncomingPacket MIDI_SONG = new IncomingPacket(51, 4);
	static OutgoingPacket aClass311_3240;
	static boolean aBoolean3241 = false;

	static final boolean method4006(int i, int i_0_, int i_1_) {
		if (i_0_ != -20268)
			return false;
		if ((i & 0x37) == 0)
			return false;
		return true;
	}

	static final Model method4007(ha var_ha, int i, int i_2_, int i_3_, int i_4_, int i_5_, byte i_6_, Animator class44, Model class178, int i_7_, int i_8_, int i_9_, int i_10_) {
		if (class178 == null)
			return null;
		int i_11_ = 2055;
		if (class44 != null) {
			i_11_ |= class44.method568(0);
			i_11_ &= ~0x200;
		}
		long l = ((long) ((i_10_ << 16) + i_4_ + (i_7_ << 24)) - (-((long) i_9_ << 32) - ((long) i_3_ << 48)));
		Model class178_12_;
		synchronized (Class280.aClass113_2555) {
			class178_12_ = (Model) Class280.aClass113_2555.get(l);
		}
		if (class178_12_ == null || var_ha.e(class178_12_.ua(), i_11_) != 0) {
			if (class178_12_ != null)
				i_11_ = var_ha.d(i_11_, class178_12_.ua());
			int i_13_;
			if (i_4_ == 1)
				i_13_ = 9;
			else if (i_4_ != 2) {
				if (i_4_ != 3) {
					if (i_4_ == 4)
						i_13_ = 18;
					else
						i_13_ = 21;
				} else
					i_13_ = 15;
			} else
				i_13_ = 12;
			int i_14_ = 3;
			int[] is = {64, 96, 128};
			Mesh class132 = new Mesh(1 + i_13_ * i_14_, -i_13_ + i_13_ * (i_14_ * 2), 0);
			int i_15_ = class132.method1399(0, 0, 0, 1);
			int[][] is_16_ = new int[i_14_][i_13_];
			for (int i_17_ = 0; i_17_ < i_14_; i_17_++) {
				int i_18_ = is[i_17_];
				int i_19_ = is[i_17_];
				for (int i_20_ = 0; i_20_ < i_13_; i_20_++) {
					int i_21_ = (i_20_ << 14) / i_13_;
					int i_22_ = Class296_Sub4.anIntArray4598[i_21_] * i_18_ >> 14;
					int i_23_ = i_19_ * Class296_Sub4.anIntArray4618[i_21_] >> 14;
					is_16_[i_17_][i_20_] = class132.method1399(0, i_23_, i_22_, 1);
				}
			}
			for (int i_24_ = 0; i_14_ > i_24_; i_24_++) {
				int i_25_ = (i_24_ * 256 + 128) / i_14_;
				int i_26_ = -i_25_ + 256;
				byte i_27_ = (byte) (i_7_ * i_25_ + i_10_ * i_26_ >> 8);
				short i_28_ = (short) (((i_25_ * (i_3_ & 0xfc00) + i_26_ * (i_9_ & 0xfc00) & 0xfc0000) + (((i_25_ * (i_3_ & 0x380) + (i_9_ & 0x380) * i_26_) & 0x38000) + (((i_9_ & 0x7f) * i_26_ + (i_3_ & 0x7f) * i_25_) & 0x7f00))) >> 8);
				for (int i_29_ = 0; i_13_ > i_29_; i_29_++) {
					if (i_24_ != 0) {
						class132.method1389(is_16_[i_24_ - 1][i_29_], is_16_[i_24_][(i_29_ + 1) % i_13_], -80, (short) -1, (is_16_[i_24_ - 1][(i_29_ + 1) % i_13_]), i_27_, (byte) -1, i_28_, (byte) 1);
						class132.method1389(is_16_[i_24_ - 1][i_29_], is_16_[i_24_][i_29_], -99, (short) -1, is_16_[i_24_][(i_29_ + 1) % i_13_], i_27_, (byte) -1, i_28_, (byte) 1);
					} else
						class132.method1389(i_15_, is_16_[0][i_29_], -86, (short) -1, is_16_[0][(i_29_ + 1) % i_13_], i_27_, (byte) -1, i_28_, (byte) 1);
				}
			}
			class178_12_ = var_ha.a(class132, i_11_, Class99.anInt1062, 64, 768);
			synchronized (Class280.aClass113_2555) {
				Class280.aClass113_2555.put(class178_12_, l);
			}
		}
		int i_30_ = class178.V();
		int i_31_ = class178.RA();
		int i_32_ = class178.HA();
		if (i_6_ < 93)
			method4009(73);
		int i_33_ = class178.G();
		if (class44 != null) {
			class178_12_ = class178_12_.method1728((byte) 3, i_11_, true);
			class178_12_.O(i_31_ - i_30_ >> 1, 128, -i_32_ + i_33_ >> 1);
			class178_12_.H(i_30_ + i_31_ >> 1, 0, i_32_ + i_33_ >> 1);
			class44.method561((byte) 107, class178_12_);
		} else {
			class178_12_ = class178_12_.method1728((byte) 3, i_11_, true);
			class178_12_.O(i_31_ - i_30_ >> 1, 128, i_33_ - i_32_ >> 1);
			class178_12_.H(i_30_ + i_31_ >> 1, 0, i_33_ + i_32_ >> 1);
		}
		if (i_5_ != 0)
			class178_12_.FA(i_5_);
		if (i_2_ != 0)
			class178_12_.VA(i_2_);
		if (i != 0)
			class178_12_.H(0, i, 0);
		return class178_12_;
	}

	static final Class338_Sub7 method4008(byte i) {
		if (i <= 17)
			method4009(-70);
		Class338_Sub7 class338_sub7 = (Class338_Sub7) Class366_Sub5.aClass404_5386.method4162(255);
		if (class338_sub7 != null) {
			Class241.anInt2300--;
			return class338_sub7;
		}
		return new Class338_Sub7();
	}

	public static void method4009(int i) {
		aClass311_3240 = null;
		if (i != 11800)
			method4007(null, -124, 45, -124, 127, -120, (byte) 104, null, null, -24, -101, 27, 63);
		MIDI_SONG = null;
	}

	static {
		aClass311_3240 = new OutgoingPacket(72, 3);
	}
}
