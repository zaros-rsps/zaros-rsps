package net.zaros.client;
import jaggl.OpenGL;

final class Class207 {
	private ha_Sub3 aHa_Sub3_2072;
	private int anInt2073;
	private int anInt2074;
	private int anInt2075;
	byte[] aByteArray2076;
	private s_Sub2 aS_Sub2_2077;
	private int anInt2078;
	static float aFloat2079;
	int anInt2080;
	private Class50[][] aClass50ArrayArray2081;

	static final void method1993(int i) {
		Class246.aClient2332.method87(-49);
		Class377.loginConnection.method1968(i + 1);
		Class377.loginConnection.incomingPacket1 = null;
		Class377.loginConnection.incomingPacket3 = null;
		Class377.loginConnection.incomingPacket2 = null;
		Class379_Sub2_Sub1.anInt6607 = 0;
		Class377.loginConnection.in_stream.pos = 0;
		if (i != -1)
			method1993(-84);
		Class377.loginConnection.anInt2062 = 0;
		Class254.method2210(0);
		Class338_Sub8.aClass94Array5257 = null;
		Class368_Sub2.unaffinedClanSettings = null;
		Class78.anInt3431 = 0;
		Class296_Sub51_Sub2.affinedClanSettings = null;
		Exception_Sub1.aString45 = null;
		Class271.anInt2512 = 0;
		Class285.anInt2625 = 0;
	}

	final void method1994(r var_r, int i, int i_0_, int i_1_) {
		r_Sub1 var_r_Sub1 = (r_Sub1) var_r;
		i_1_ += var_r_Sub1.anInt6702 + 1;
		i += var_r_Sub1.anInt6704 + 1;
		if (i_0_ > -123)
			aS_Sub2_2077 = null;
		int i_2_ = i + i_1_ * anInt2080;
		int i_3_ = 0;
		int i_4_ = var_r_Sub1.anInt6705;
		int i_5_ = var_r_Sub1.anInt6703;
		int i_6_ = -i_5_ + anInt2080;
		if (i_1_ <= 0) {
			int i_7_ = -i_1_ + 1;
			i_4_ -= i_7_;
			i_3_ += i_5_ * i_7_;
			i_1_ = 1;
			i_2_ += anInt2080 * i_7_;
		}
		int i_8_ = 0;
		if (anInt2073 <= i_1_ + i_4_) {
			int i_9_ = -anInt2073 + (i_4_ + i_1_) + 1;
			i_4_ -= i_9_;
		}
		if (i <= 0) {
			int i_10_ = -i + 1;
			i = 1;
			i_8_ += i_10_;
			i_5_ -= i_10_;
			i_2_ += i_10_;
			i_3_ += i_10_;
			i_6_ += i_10_;
		}
		if (i_5_ + i >= anInt2080) {
			int i_11_ = -anInt2080 + (i + i_5_ + 1);
			i_6_ += i_11_;
			i_8_ += i_11_;
			i_5_ -= i_11_;
		}
		if (i_5_ > 0 && i_4_ > 0) {
			Class327.method3391(i_2_, i_8_, i_3_, i_5_, aByteArray2076, i_6_, i_4_, var_r_Sub1.aByteArray6706, 21245);
			method1998(i_1_, i, (byte) 16, i_4_, i_5_);
		}
	}

	static final Class196 method1995(int i, byte i_12_) {
		if (i_12_ != 15)
			method1995(-56, (byte) -92);
		Class196 class196 = new Class196(i, false);
		return class196;
	}

	final void method1996(int i, int i_13_, boolean[][] bools, boolean bool, int i_14_, int i_15_) {
		aHa_Sub3_2072.method1278(1553921480, false);
		aHa_Sub3_2072.method1285(110, false);
		aHa_Sub3_2072.method1340(-2, (byte) 65);
		aHa_Sub3_2072.method1272((byte) -107, 1);
		aHa_Sub3_2072.method1336((byte) 14, 1);
		float f = 1.0F / (float) (aHa_Sub3_2072.anInt4140 * 128);
		if (bool) {
			for (int i_16_ = 0; anInt2074 > i_16_; i_16_++) {
				int i_17_ = i_16_ << anInt2075;
				int i_18_ = i_16_ + 1 << anInt2075;
				for (int i_19_ = 0; i_19_ < anInt2078; i_19_++) {
					int i_20_ = i_19_ << anInt2075;
					int i_21_ = i_19_ + 1 << anInt2075;
					while_122_ : for (int i_22_ = i_20_; i_21_ > i_22_; i_22_++) {
						if (-i_14_ <= -i + i_22_ && i_22_ - i <= i_14_) {
							for (int i_23_ = i_17_; i_18_ > i_23_; i_23_++) {
								if (i_23_ - i_15_ >= -i_14_ && i_14_ >= i_23_ - i_15_ && (bools[i_22_ - (i - i_14_)][i_23_ - i_15_ + i_14_])) {
									OpenGL.glMatrixMode(5890);
									OpenGL.glLoadIdentity();
									OpenGL.glScalef(f, f, 1.0F);
									OpenGL.glTranslatef((float) -i_19_ / f, (float) -i_16_ / f, 1.0F);
									OpenGL.glMatrixMode(5888);
									aClass50ArrayArray2081[i_19_][i_16_].method616(false);
									break while_122_;
								}
							}
						}
					}
				}
			}
		} else {
			for (int i_24_ = 0; i_24_ < anInt2074; i_24_++) {
				int i_25_ = i_24_ << anInt2075;
				int i_26_ = i_24_ + 1 << anInt2075;
				for (int i_27_ = 0; anInt2078 > i_27_; i_27_++) {
					int i_28_ = 0;
					int i_29_ = i_27_ << anInt2075;
					int i_30_ = i_27_ + 1 << anInt2075;
					Class296_Sub17_Sub2 class296_sub17_sub2 = aHa_Sub3_2072.aClass296_Sub17_Sub2_4194;
					class296_sub17_sub2.pos = 0;
					for (int i_31_ = i_25_; i_31_ < i_26_; i_31_++) {
						if (-i_14_ <= -i_15_ + i_31_ && i_14_ >= -i_15_ + i_31_) {
							int i_32_ = aS_Sub2_2077.anInt2832 * i_31_ + i_29_;
							for (int i_33_ = i_29_; i_30_ > i_33_; i_33_++) {
								if (-i + i_33_ >= -i_14_ && i_14_ >= -i + i_33_ && (bools[i_33_ - (i - i_14_)][i_14_ + (-i_15_ + i_31_)])) {
									short[] is = (aS_Sub2_2077.aShortArrayArray5111[i_32_]);
									if (is != null) {
										if (!aHa_Sub3_2072.aBoolean4184) {
											for (int i_34_ = 0; is.length > i_34_; i_34_++) {
												class296_sub17_sub2.method2590(is[i_34_] & 0xffff);
												i_28_++;
											}
										} else {
											for (int i_35_ = 0; i_35_ < is.length; i_35_++) {
												class296_sub17_sub2.p2(is[i_35_] & 0xffff);
												i_28_++;
											}
										}
									}
								}
								i_32_++;
							}
						}
					}
					if (i_28_ > 0) {
						OpenGL.glMatrixMode(5890);
						OpenGL.glLoadIdentity();
						OpenGL.glScalef(f, f, 1.0F);
						OpenGL.glTranslatef((float) -i_27_ / f, (float) -i_24_ / f, 1.0F);
						OpenGL.glMatrixMode(5888);
						aClass50ArrayArray2081[i_27_][i_24_].method612(class296_sub17_sub2.data, i_13_ - 5891, i_28_, 5123);
					}
				}
			}
		}
		OpenGL.glMatrixMode(i_13_);
		OpenGL.glLoadIdentity();
		OpenGL.glMatrixMode(5888);
	}

	final void method1997(int i, int i_36_, r var_r, int i_37_) {
		r_Sub1 var_r_Sub1 = (r_Sub1) var_r;
		i += var_r_Sub1.anInt6704 + 1;
		i_37_ += var_r_Sub1.anInt6702 + 1;
		if (i_36_ > 65) {
			int i_38_ = i + anInt2080 * i_37_;
			int i_39_ = 0;
			int i_40_ = var_r_Sub1.anInt6705;
			int i_41_ = var_r_Sub1.anInt6703;
			int i_42_ = -i_41_ + anInt2080;
			if (i_37_ <= 0) {
				int i_43_ = 1 - i_37_;
				i_38_ += i_43_ * anInt2080;
				i_40_ -= i_43_;
				i_39_ += i_41_ * i_43_;
				i_37_ = 1;
			}
			int i_44_ = 0;
			if (i_37_ + i_40_ >= anInt2073) {
				int i_45_ = -anInt2073 + (i_40_ + i_37_ + 1);
				i_40_ -= i_45_;
			}
			if (i <= 0) {
				int i_46_ = -i + 1;
				i_44_ += i_46_;
				i_41_ -= i_46_;
				i_42_ += i_46_;
				i_38_ += i_46_;
				i = 1;
				i_39_ += i_46_;
			}
			if (anInt2080 <= i + i_41_) {
				int i_47_ = i + (i_41_ + 1) - anInt2080;
				i_44_ += i_47_;
				i_41_ -= i_47_;
				i_42_ += i_47_;
			}
			if (i_41_ > 0 && i_40_ > 0) {
				Class380.method3979(i_44_, i_41_, i_40_, var_r_Sub1.aByteArray6706, -39, i_39_, i_42_, i_38_, aByteArray2076);
				method1998(i_37_, i, (byte) 16, i_40_, i_41_);
			}
		}
	}

	private final void method1998(int i, int i_48_, byte i_49_, int i_50_, int i_51_) {
		if (aClass50ArrayArray2081 != null) {
			int i_52_ = i_48_ - 1 >> 7;
			int i_53_ = i_51_ - 1 - 1 + i_48_ >> 7;
			int i_54_ = i - 1 >> 7;
			int i_55_ = i_50_ - 1 + i - 1 >> 7;
			if (i_49_ == 16) {
				for (int i_56_ = i_52_; i_53_ >= i_56_; i_56_++) {
					Class50[] class50s = aClass50ArrayArray2081[i_56_];
					for (int i_57_ = i_54_; i_55_ >= i_57_; i_57_++)
						class50s[i_57_].aBoolean473 = true;
				}
			}
		}
	}

	final void method1999(int i) {
		aClass50ArrayArray2081 = new Class50[anInt2078][anInt2074];
		int i_58_ = 0;
		if (i != 3418)
			method1994(null, 111, -104, 108);
		for (/**/; anInt2074 > i_58_; i_58_++) {
			for (int i_59_ = 0; anInt2078 > i_59_; i_59_++)
				aClass50ArrayArray2081[i_59_][i_58_] = new Class50(aHa_Sub3_2072, this, aS_Sub2_2077, i_59_, i_58_, anInt2075, i_59_ * 128 + 1, i_58_ * 128 + 1);
		}
	}

	final boolean method2000(int i, int i_60_, r var_r, int i_61_) {
		r_Sub1 var_r_Sub1 = (r_Sub1) var_r;
		i_61_ += var_r_Sub1.anInt6704 + 1;
		i_60_ += var_r_Sub1.anInt6702 + 1;
		int i_62_ = i_60_ * anInt2080 + i_61_;
		int i_63_ = var_r_Sub1.anInt6705;
		int i_64_ = var_r_Sub1.anInt6703;
		int i_65_ = anInt2080 - i_64_;
		if (i_60_ <= 0) {
			int i_66_ = -i_60_ + 1;
			i_63_ -= i_66_;
			i_60_ = 1;
			i_62_ += i_66_ * anInt2080;
		}
		if (anInt2073 <= i_63_ + i_60_) {
			int i_67_ = -anInt2073 + (i_60_ + i_63_ + 1);
			i_63_ -= i_67_;
		}
		if (i_61_ <= 0) {
			int i_68_ = -i_61_ + 1;
			i_61_ = 1;
			i_65_ += i_68_;
			i_62_ += i_68_;
			i_64_ -= i_68_;
		}
		if (anInt2080 <= i_61_ + i_64_) {
			int i_69_ = i_61_ + (i_64_ - anInt2080) + 1;
			i_65_ += i_69_;
			i_64_ -= i_69_;
		}
		if (i >= -72)
			return false;
		if (i_64_ <= 0 || i_63_ <= 0)
			return false;
		int i_70_ = 8;
		i_65_ += anInt2080 * (i_70_ - 1);
		return Class380.method3985(i_64_, i_62_, i_65_, i_70_, i_63_, (byte) 41, aByteArray2076);
	}

	Class207(ha_Sub3 var_ha_Sub3, s_Sub2 var_s_Sub2) {
		aHa_Sub3_2072 = var_ha_Sub3;
		aS_Sub2_2077 = var_s_Sub2;
		anInt2080 = 2 + (aS_Sub2_2077.anInt2832 * aS_Sub2_2077.anInt2836 >> aHa_Sub3_2072.anInt4139);
		anInt2073 = (aS_Sub2_2077.anInt2834 * aS_Sub2_2077.anInt2836 >> aHa_Sub3_2072.anInt4139) + 2;
		aByteArray2076 = new byte[anInt2080 * anInt2073];
		anInt2075 = aHa_Sub3_2072.anInt4139 + 7 - aS_Sub2_2077.anInt2835;
		anInt2078 = aS_Sub2_2077.anInt2832 >> anInt2075;
		anInt2074 = aS_Sub2_2077.anInt2834 >> anInt2075;
	}
}
