package net.zaros.client;

/* Class106_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class106_Sub1 extends Class106 {
	static int[] anIntArray3902 = new int[1000];
	static int anInt3903 = -1;

	public static void method940(int i) {
		anIntArray3902 = null;
		if (i != -1)
			anInt3903 = -17;
	}

	static final int method941(String string, int i, int i_0_, String string_1_) {
		int i_2_ = string_1_.length();
		int i_3_ = string.length();
		int i_4_ = 0;
		int i_5_ = 0;
		char c = '\0';
		char c_6_ = '\0';
		while_138_ : do {
			char c_7_;
			char c_8_;
			for (;;) {
				if (-c + i_4_ >= i_2_ && i_3_ <= i_5_ - c_6_)
					break while_138_;
				if (i_4_ - c >= i_2_)
					return -1;
				if (i_3_ <= i_5_ - c_6_)
					return 1;
				if (c == 0)
					c_7_ = string_1_.charAt(i_4_++);
				else {
					c_7_ = c;
					boolean bool = false;
				}
				if (c_6_ == 0)
					c_8_ = string.charAt(i_5_++);
				else {
					c_8_ = c_6_;
					boolean bool = false;
				}
				c = Class296_Sub35_Sub3.method2760((byte) 116, c_7_);
				c_6_ = Class296_Sub35_Sub3.method2760((byte) 73, c_8_);
				c_7_ = Class69.method724(c_7_, i, 338);
				c_8_ = Class69.method724(c_8_, i, 338);
				if (c_7_ != c_8_ && (Character.toUpperCase(c_7_) != Character.toUpperCase(c_8_))) {
					c_7_ = Character.toLowerCase(c_7_);
					c_8_ = Character.toLowerCase(c_8_);
					if (c_8_ != c_7_)
						break;
				}
			}
			return (Class41_Sub14.method451(c_7_, 13, i) - Class41_Sub14.method451(c_8_, 22, i));
		} while (false);
		int i_9_ = Math.min(i_2_, i_3_);
		for (int i_10_ = 0; i_10_ < i_9_; i_10_++) {
			if (i != 2)
				i_4_ = i_5_ = i_10_;
			else {
				i_4_ = -i_10_ - 1 + i_2_;
				i_5_ = i_3_ - i_10_ - 1;
			}
			char c_11_ = string_1_.charAt(i_4_);
			char c_12_ = string.charAt(i_5_);
			if (c_12_ != c_11_ && (Character.toUpperCase(c_11_) != Character.toUpperCase(c_12_))) {
				c_11_ = Character.toLowerCase(c_11_);
				c_12_ = Character.toLowerCase(c_12_);
				if (c_11_ != c_12_)
					return (Class41_Sub14.method451(c_11_, 41, i) - Class41_Sub14.method451(c_12_, 41, i));
			}
		}
		int i_13_ = -i_3_ + i_2_;
		if (i_13_ != 0)
			return i_13_;
		int i_14_ = -122 / ((53 - i_0_) / 58);
		for (int i_15_ = 0; i_15_ < i_9_; i_15_++) {
			char c_16_ = string_1_.charAt(i_15_);
			char c_17_ = string.charAt(i_15_);
			if (c_17_ != c_16_)
				return (Class41_Sub14.method451(c_16_, 46, i) - Class41_Sub14.method451(c_17_, 23, i));
		}
		return 0;
	}

	static final void method942(long l, int i) {
		if (l > (long) i) {
			if (l % 10L != 0L)
				Class137.method1426(l, 1406);
			else {
				Class137.method1426(l - 1L, 1406);
				Class137.method1426(1L, 1406);
			}
		}
	}

	static final void method943(boolean bool, int i) {
		Class108.method948(-2060);
		if (Class130.method1376(Class366_Sub6.anInt5392, -14723) && i == 1) {
			Connection[] class204s = Class296_Sub45_Sub2.aClass204Array6278;
			for (int i_18_ = 0; i_18_ < class204s.length; i_18_++) {
				Connection class204 = class204s[i_18_];
				class204.anInt2060++;
				if (class204.anInt2060 < 50 && !bool)
					return;
				class204.anInt2060 = 0;
				if (!class204.aBoolean2065 && class204.aClass154_2045 != null) {
					Class219.anInt2127++;
					Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 123, Class44_Sub2.aClass311_3860);
					class204.sendPacket(class296_sub1, (byte) 119);
					try {
						class204.method1963(true);
					} catch (java.io.IOException ioexception) {
						class204.aBoolean2065 = true;
					}
				}
			}
			Class108.method948(-2060);
		}
	}
}
