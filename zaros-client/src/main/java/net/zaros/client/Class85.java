package net.zaros.client;
/* Class85 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class85 {
	static Class350 aClass350_927;
	static String[] aStringArray928 = new String[100];
	static Sprite[] aClass397Array929;

	public static void method821(byte i) {
		aClass397Array929 = null;
		aClass350_927 = null;
		aStringArray928 = null;
		int i_0_ = -113 / ((49 - i) / 44);
	}

	static final String method822(byte i, String string) {
		StringBuffer stringbuffer = new StringBuffer();
		int i_1_ = -110 % ((i - 9) / 32);
		int i_2_ = string.length();
		for (int i_3_ = 0; i_3_ < i_2_; i_3_++) {
			char c = string.charAt(i_3_);
			if (c == 37 && i_3_ + 2 < i_2_) {
				c = string.charAt(i_3_ + 1);
				boolean bool = false;
				int i_4_;
				if (c >= 48 && c <= 57)
					i_4_ = c - 48;
				else if (c < 97 || c > 102) {
					if (c < 65 || c > 70) {
						stringbuffer.append('%');
						continue;
					}
					i_4_ = 10 + c - 65;
				} else
					i_4_ = c + 10 - 97;
				i_4_ *= 16;
				int i_5_ = string.charAt(i_3_ + 2);
				if (i_5_ >= 48 && i_5_ <= 57)
					i_4_ += i_5_ - 48;
				else if (i_5_ >= 97 && i_5_ <= 102)
					i_4_ += 10 + i_5_ - 97;
				else {
					if (i_5_ < 65 || i_5_ > 70) {
						stringbuffer.append('%');
						continue;
					}
					i_4_ += i_5_ - 55;
				}
				i_3_ += 2;
				if (i_4_ != 0 && Class277.method2324((byte) i_4_, (byte) -111))
					stringbuffer.append(Mesh.method1387((byte) -38, (byte) i_4_));
			} else if (c == 43)
				stringbuffer.append(' ');
			else
				stringbuffer.append(c);
		}
		return stringbuffer.toString();
	}
}
