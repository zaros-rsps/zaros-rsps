package net.zaros.client;

/* Class296_Sub39_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class ReferenceWrapper extends Queuable {
	int usedSlots;
	static IncomingPacket aClass231_6127 = new IncomingPacket(52, -1);
	static int anInt6128;
	static int[][] anIntArrayArray6129 = {{0, 1, 2, 3}, {1, -1, -1, 0}, {-1, 2, -1, 0}, {-1, 0, -1, 2}, {0, 1, -1, 2}, {1, 2, -1, 0}, {-1, 4, -1, 1}, {-1, 3, 4, -1}, {-1, 0, 2, -1}, {-1, -1, 2, 0}, {0, 2, 5, 3}, {0, -1, 6, -1}, {0, 1, 2, 3}};

	abstract boolean isSoft(boolean bool);

	public static void method2788(byte i) {
		if (i != -60)
			anInt6128 = 41;
		aClass231_6127 = null;
		anIntArrayArray6129 = null;
	}

	static final boolean method2789(int i, int i_0_, int i_1_) {
		if (i != -1)
			method2789(54, -128, 9);
		if ((i_1_ & 0x800) == 0 || (i_0_ & 0x37) == 0)
			return false;
		return true;
	}

	static final void method2790(boolean bool, byte[][][] is, int i, byte i_2_, int i_3_, int i_4_, boolean bool_5_) {
		int i_6_ = bool ? 1 : 0;
		Class338_Sub8_Sub2.anInt6584 = 0;
		CS2Call.anInt4963 = 0;
		Class86.anInt936++;
		if ((i_4_ & 0x2) == 0) {
			for (Class338_Sub3 class338_sub3 = Class368_Sub10.aClass338_Sub3Array5481[i_6_]; class338_sub3 != null; class338_sub3 = class338_sub3.aClass338_Sub3_5202) {
				if (!EquipmentData.method3303(class338_sub3, bool, is, i, i_2_)) {
					Class227.method2092(class338_sub3);
					if (class338_sub3.anInt5208 != -1)
						Class296_Sub51_Sub37.aClass338_Sub3Array6531[Class338_Sub8_Sub2.anInt6584++] = class338_sub3;
				}
			}
		}
		if ((i_4_ & 0x1) == 0) {
			for (Class338_Sub3 class338_sub3 = Class368_Sub16.aClass338_Sub3Array5525[i_6_]; class338_sub3 != null; class338_sub3 = class338_sub3.aClass338_Sub3_5202) {
				if (!EquipmentData.method3303(class338_sub3, bool, is, i, i_2_)) {
					Class227.method2092(class338_sub3);
					if (class338_sub3.anInt5208 != -1)
						Model.aClass338_Sub3Array1850[CS2Call.anInt4963++] = class338_sub3;
				}
			}
			for (Class338_Sub3 class338_sub3 = Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_6_]; class338_sub3 != null; class338_sub3 = class338_sub3.aClass338_Sub3_5202) {
				if (!EquipmentData.method3303(class338_sub3, bool, is, i, i_2_)) {
					if (class338_sub3.method3469(84)) {
						Class227.method2092(class338_sub3);
						if (class338_sub3.anInt5208 != -1)
							Model.aClass338_Sub3Array1850[CS2Call.anInt4963++] = class338_sub3;
					} else {
						Class227.method2092(class338_sub3);
						if (class338_sub3.anInt5208 != -1)
							Class296_Sub51_Sub37.aClass338_Sub3Array6531[Class338_Sub8_Sub2.anInt6584++] = class338_sub3;
					}
				}
			}
			if (!bool) {
				for (int i_7_ = 0; i_7_ < Class127.anInt1303; i_7_++) {
					if (!EquipmentData.method3303((s.aClass338_Sub3_Sub1Array2833[i_7_]), bool, is, i, i_2_)) {
						Class227.method2092(s.aClass338_Sub3_Sub1Array2833[i_7_]);
						if (s.aClass338_Sub3_Sub1Array2833[i_7_].anInt5208 != -1) {
							if (s.aClass338_Sub3_Sub1Array2833[i_7_].method3469(126))
								Model.aClass338_Sub3Array1850[CS2Call.anInt4963++] = s.aClass338_Sub3_Sub1Array2833[i_7_];
							else
								Class296_Sub51_Sub37.aClass338_Sub3Array6531[Class338_Sub8_Sub2.anInt6584++] = s.aClass338_Sub3_Sub1Array2833[i_7_];
						}
					}
				}
			}
		}
		if (Class338_Sub8_Sub2.anInt6584 > 0) {
			Class266.method2290(Class296_Sub51_Sub37.aClass338_Sub3Array6531, 0, Class338_Sub8_Sub2.anInt6584 - 1);
			for (int i_8_ = 0; i_8_ < Class338_Sub8_Sub2.anInt6584; i_8_++)
				Class347.method3671((Class296_Sub51_Sub37.aClass338_Sub3Array6531[i_8_]), true, bool_5_);
		}
		if (Class368_Sub2.aBoolean5427)
			Class16_Sub2.aHa3733.a(0, null);
		if ((i_4_ & 0x2) == 0) {
			for (int i_9_ = Class360_Sub5.anInt5328; i_9_ < Class368_Sub9.anInt5477; i_9_++) {
				if (i_9_ >= i && is != null) {
					int i_10_ = Class296_Sub15_Sub2.aBooleanArrayArray6006.length;
					if ((Class296_Sub39_Sub4.anInt5744 + Class296_Sub15_Sub2.aBooleanArrayArray6006.length) > Class228.anInt2201)
						i_10_ -= (Class296_Sub39_Sub4.anInt5744 + (Class296_Sub15_Sub2.aBooleanArrayArray6006).length - Class228.anInt2201);
					int i_11_ = Class296_Sub15_Sub2.aBooleanArrayArray6006[0].length;
					if (AdvancedMemoryCache.anInt1166 + (Class296_Sub15_Sub2.aBooleanArrayArray6006[0]).length > Class368_Sub12.anInt5488)
						i_11_ -= (AdvancedMemoryCache.anInt1166 + (Class296_Sub15_Sub2.aBooleanArrayArray6006[0]).length - Class368_Sub12.anInt5488);
					boolean[][] bools = Class370.aBooleanArrayArray3140;
					if (Class208.aBoolean2089) {
						if (Class338_Sub3_Sub2.aBoolean6566)
							bools = Class121.aBooleanArrayArrayArray1268[i_9_];
						for (int i_12_ = Class56.anInt662; i_12_ < i_10_; i_12_++) {
							int i_13_ = (i_12_ + Class296_Sub39_Sub4.anInt5744 - Class56.anInt662);
							for (int i_14_ = Class69_Sub3.anInt5729; i_14_ < i_11_; i_14_++) {
								bools[i_12_][i_14_] = false;
								if (Class296_Sub15_Sub2.aBooleanArrayArray6006[i_12_][i_14_]) {
									int i_15_ = (i_14_ + AdvancedMemoryCache.anInt1166 - Class69_Sub3.anInt5729);
									for (int i_16_ = i_9_; i_16_ >= 0; i_16_--) {
										if ((Class338_Sub2.aClass247ArrayArrayArray5195[i_16_][i_13_][i_15_]) != null && (Class338_Sub2.aClass247ArrayArrayArray5195[i_16_][i_13_][i_15_].aByte2349) == i_9_) {
											if (i_16_ >= i && (is[i_16_][i_13_][i_15_]) == i_2_ || (Class50.method614((byte) -114, i_13_, i_15_, i_9_)))
												bools[i_12_][i_14_] = false;
											else
												bools[i_12_][i_14_] = true;
											break;
										}
									}
								}
							}
						}
					}
					if (Class338_Sub3_Sub2.aBoolean6566) {
						if (i_3_ >= 0)
							Class360_Sub2.aSArray5304[i_9_].method3348(0, 0, 0, null, false, i_3_, i_4_);
						else
							Class360_Sub2.aSArray5304[i_9_].method3354(0, 0, 0, null, false, i_4_);
						for (int i_17_ = 0; i_17_ < Class377.anInt3190; i_17_++)
							Class127.aClass299Array1301[i_17_].method3244(new Class338_Sub10(i_9_ + 1), (byte) 80);
					} else if (i_3_ >= 0)
						Class360_Sub2.aSArray5304[i_9_].method3348(Class296_Sub45_Sub2.anInt6288, Class296_Sub39_Sub20_Sub2.anInt6728, Class379_Sub2.anInt5684, Class370.aBooleanArrayArray3140, false, i_3_, i_4_);
					else
						Class360_Sub2.aSArray5304[i_9_].method3354(Class296_Sub45_Sub2.anInt6288, Class296_Sub39_Sub20_Sub2.anInt6728, Class379_Sub2.anInt5684, Class370.aBooleanArrayArray3140, false, i_4_);
				} else {
					int i_18_ = Class296_Sub15_Sub2.aBooleanArrayArray6006.length;
					if ((Class296_Sub39_Sub4.anInt5744 + Class296_Sub15_Sub2.aBooleanArrayArray6006.length) > Class228.anInt2201)
						i_18_ -= (Class296_Sub39_Sub4.anInt5744 + (Class296_Sub15_Sub2.aBooleanArrayArray6006).length - Class228.anInt2201);
					int i_19_ = Class296_Sub15_Sub2.aBooleanArrayArray6006[0].length;
					if (AdvancedMemoryCache.anInt1166 + (Class296_Sub15_Sub2.aBooleanArrayArray6006[0]).length > Class368_Sub12.anInt5488)
						i_19_ -= (AdvancedMemoryCache.anInt1166 + (Class296_Sub15_Sub2.aBooleanArrayArray6006[0]).length - Class368_Sub12.anInt5488);
					boolean[][] bools = Class370.aBooleanArrayArray3140;
					if (Class208.aBoolean2089) {
						if (Class338_Sub3_Sub2.aBoolean6566)
							bools = Class121.aBooleanArrayArrayArray1268[i_9_];
						for (int i_20_ = Class56.anInt662; i_20_ < i_18_; i_20_++) {
							int i_21_ = (i_20_ + Class296_Sub39_Sub4.anInt5744 - Class56.anInt662);
							for (int i_22_ = Class69_Sub3.anInt5729; i_22_ < i_19_; i_22_++) {
								if ((Class296_Sub15_Sub2.aBooleanArrayArray6006[i_20_][i_22_]) && !Class50.method614((byte) -123, i_21_, (i_22_ + AdvancedMemoryCache.anInt1166 - (Class69_Sub3.anInt5729)), i_9_))
									bools[i_20_][i_22_] = true;
								else
									bools[i_20_][i_22_] = false;
							}
						}
					}
					if (Class338_Sub3_Sub2.aBoolean6566) {
						if (i_3_ >= 0)
							Class360_Sub2.aSArray5304[i_9_].method3348(0, 0, 0, null, false, i_3_, i_4_);
						else
							Class360_Sub2.aSArray5304[i_9_].method3354(0, 0, 0, null, false, i_4_);
						for (int i_23_ = 0; i_23_ < Class377.anInt3190; i_23_++)
							Class127.aClass299Array1301[i_23_].method3244(new Class338_Sub10(i_9_ + 1), (byte) 80);
					} else if (i_3_ >= 0)
						Class360_Sub2.aSArray5304[i_9_].method3348(Class296_Sub45_Sub2.anInt6288, Class296_Sub39_Sub20_Sub2.anInt6728, Class379_Sub2.anInt5684, Class370.aBooleanArrayArray3140, true, i_3_, i_4_);
					else
						Class360_Sub2.aSArray5304[i_9_].method3354(Class296_Sub45_Sub2.anInt6288, Class296_Sub39_Sub20_Sub2.anInt6728, Class379_Sub2.anInt5684, Class370.aBooleanArrayArray3140, true, i_4_);
				}
			}
		}
		if (CS2Call.anInt4963 > 0) {
			Class296_Sub51_Sub25.method3145(Model.aClass338_Sub3Array1850, 0, CS2Call.anInt4963 - 1);
			for (int i_24_ = 0; i_24_ < CS2Call.anInt4963; i_24_++)
				Class347.method3671(Model.aClass338_Sub3Array1850[i_24_], true, bool_5_);
		}
	}

	abstract Object getReference(int i);

	ReferenceWrapper(int i) {
		usedSlots = i;
	}

	static {
		anInt6128 = 0;
	}
}
