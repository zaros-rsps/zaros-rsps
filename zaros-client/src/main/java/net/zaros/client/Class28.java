package net.zaros.client;

/* Class28 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class28 {
	boolean aBoolean298 = false;
	static int anInt299 = -1;
	int anInt300;
	Class401 aClass401_301;
	static boolean under = false;
	int anInt303;

	final Sprite method315(ha var_ha, int i, boolean bool, int i_0_) {
		long l = (long) (anInt300 | i_0_ << 16 | (bool ? 262144 : 0) | var_ha.anInt1295 << 19);
		Sprite class397 = (Sprite) aClass401_301.aClass113_3367.get(l);
		if (class397 != null)
			return class397;
		if (!aClass401_301.aClass138_3364.hasEntryBuffer(anInt300))
			return null;
		if (i != -22801)
			aClass401_301 = null;
		Class186 class186 = Class186.method1876(aClass401_301.aClass138_3364, anInt300, 0);
		if (class186 != null) {
			class186.anInt1902 = class186.anInt1903 = class186.anInt1907 = class186.anInt1906 = 0;
			if (bool)
				class186.method1875();
			for (int i_1_ = 0; i_1_ < i_0_; i_1_++)
				class186.method1873();
		}
		class397 = var_ha.a(class186, true);
		if (class397 != null)
			aClass401_301.aClass113_3367.put(class397, l);
		return class397;
	}

	final boolean method316(byte i) {
		if (i != -21)
			return false;
		return aClass401_301.aClass138_3364.hasEntryBuffer(anInt300);
	}

	final void method317(Packet class296_sub17, int i) {
		if (i == -28563) {
			for (;;) {
				int i_2_ = class296_sub17.g1();
				if (i_2_ == 0)
					break;
				method318(class296_sub17, 97, i_2_);
			}
		}
	}

	private final void method318(Packet class296_sub17, int i, int i_3_) {
		if (i_3_ == 1)
			anInt300 = class296_sub17.g2();
		else if (i_3_ == 2)
			anInt303 = class296_sub17.readUnsignedMedInt();
		else if (i_3_ == 3)
			aBoolean298 = true;
		else if (i_3_ == 4)
			anInt300 = -1;
		if (i < 56) {
			/* empty */
		}
	}

	public Class28() {
		/* empty */
	}
}
