package net.zaros.client;
/* Class58 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import net.zaros.client.configs.objtype.ObjTypeList;

final class FileOnDisk {
	private RandomAccessFile raf;
	static HashTable aClass263_670 = new HashTable(16);
	private long aLong671;
	private long offset;
	private File f;
	static float aFloat674;
	static OutgoingPacket aClass311_675 = new OutgoingPacket(93, -1);

	protected final void finalize() throws Throwable {
		if (raf != null) {
			System.out.println("Warning! fileondisk " + f + " not closed correctly using close(). Auto-closing instead. ");
			close(0);
		}
	}

	final File getFile(boolean bool) {
		if (bool != true)
			aFloat674 = -0.21998422F;
		return f;
	}

	static final Class296_Sub50 method674(byte i) {
		FileOnDisk class58 = null;
		Class296_Sub50 class296_sub50 = new Class296_Sub50(Class296_Sub50.game, 0);
		try {
			Class278 class278 = Class252.aClass398_2383.method4117("", (byte) 110, true);
			while (class278.anInt2540 == 0)
				Class106_Sub1.method942(1L, 0);
			if (class278.anInt2540 == 1) {
				class58 = (FileOnDisk) class278.anObject2539;
				byte[] is = new byte[(int) class58.getLength(-2)];
				int i_0_;
				for (int i_1_ = 0; i_1_ < is.length; i_1_ += i_0_) {
					i_0_ = class58.read((byte) 9, i_1_, is, is.length - i_1_);
					if (i_0_ == -1)
						throw new IOException("EOF");
				}
				class296_sub50 = new Class296_Sub50(new Packet(is), Class296_Sub50.game, 0);
			}
			if (i != 2)
				method676(-117, 107, -78, -113, -28, 45, -81, -98, -37);
		} catch (Exception exception) {
			/* empty */
		}
		try {
			if (class58 != null)
				class58.close(0);
		} catch (Exception exception) {
			/* empty */
		}
		return class296_sub50;
	}

	final void seek(long l, byte i) throws IOException {
		if (i != 85)
			method676(-7, 45, -104, 74, -11, 74, 66, 34, 19);
		raf.seek(l);
		offset = l;
	}

	static final void method676(int i, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_) {
		if (!InterfaceComponent.loadInterface(i_3_)) {
			if (i_5_ != -1)
				Class360_Sub9.aBooleanArray5345[i_5_] = true;
			else {
				for (int i_10_ = 0; i_10_ < 100; i_10_++)
					Class360_Sub9.aBooleanArray5345[i_10_] = true;
			}
		} else {
			int i_11_ = 0;
			int i_12_ = 0;
			int i_13_ = 0;
			int i_14_ = 0;
			int i_15_ = 0;
			if (Class368_Sub5_Sub2.aBoolean6597) {
				i_14_ = Class365.anInt3120;
				i_13_ = Class368_Sub23.anInt5570;
				i_15_ = Class309.anInt2755;
				i_12_ = Class322.anInt2827;
				i_11_ = Class296_Sub45_Sub3.anInt6294;
				Class309.anInt2755 = 1;
			}
			if (Class338_Sub10.aClass51ArrayArray5272[i_3_] == null)
				Class93.method860(i_2_, i_8_, i_5_ < 0, Class192.openedInterfaceComponents[i_3_], i, i_4_, i_9_, i_5_, false, i_7_, -1);
			else
				Class93.method860(i_2_, i_8_, i_5_ < 0, Class338_Sub10.aClass51ArrayArray5272[i_3_], i, i_4_, i_9_, i_5_, false, i_7_, -1);
			if (i_6_ < 99)
				aClass311_675 = null;
			if (Class368_Sub5_Sub2.aBoolean6597) {
				if (i_5_ >= 0 && Class309.anInt2755 == 2)
					Class368_Sub8.method3838(false, Class296_Sub45_Sub3.anInt6294, Class322.anInt2827, Class365.anInt3120, Class368_Sub23.anInt5570);
				Class368_Sub23.anInt5570 = i_13_;
				Class309.anInt2755 = i_15_;
				Class365.anInt3120 = i_14_;
				Class322.anInt2827 = i_12_;
				Class296_Sub45_Sub3.anInt6294 = i_11_;
			}
		}
	}

	final long getLength(int i) throws IOException {
		if (i != -2)
			aLong671 = 82L;
		return raf.length();
	}

	static final void setGlobalString(String string, int configID) {
		GameLoopTask task = Class296_Sub39_Sub6.makeGameLoopTask((long) configID, 2);
		task.insertIntoQueue_2();
		task.stringParam = string;
	}

	static final void method679(int i, long l, int i_17_, int i_18_, aa var_aa, InterfaceComponent class51, int i_19_, int i_20_, int i_21_) {
		int i_22_ = i_21_ * i_21_ + i_17_ * i_17_;
		if (l >= (long) i_22_) {
			int i_23_ = Math.min(class51.anInt578 / 2, class51.anInt623 / 2);
			int i_24_ = -127 / ((i_18_ + 77) / 44);
			if (i_23_ * i_23_ < i_22_) {
				i_23_ -= 10;
				int i_25_;
				if (Class361.anInt3103 != 4)
					i_25_ = ((int) Class41_Sub26.aFloat3806 + StaticMethods.anInt6075) & 0x3fff;
				else
					i_25_ = (int) Class41_Sub26.aFloat3806 & 0x3fff;
				int i_26_ = Class296_Sub4.anIntArray4598[i_25_];
				int i_27_ = Class296_Sub4.anIntArray4618[i_25_];
				if (Class361.anInt3103 != 4) {
					i_26_ = i_26_ * 256 / (ObjTypeList.anInt1320 + 256);
					i_27_ = i_27_ * 256 / (ObjTypeList.anInt1320 + 256);
				}
				int i_28_ = i_21_ * i_27_ + i_26_ * i_17_ >> 14;
				int i_29_ = -(i_26_ * i_21_) + i_27_ * i_17_ >> 14;
				double d = Math.atan2((double) i_28_, (double) i_29_);
				int i_30_ = (int) (Math.sin(d) * (double) i_23_);
				int i_31_ = (int) ((double) i_23_ * Math.cos(d));
				GraphicsLoader.aClass397Array2478[i].method4081((float) i_30_ + ((float) class51.anInt578 / 2.0F + (float) i_20_), (float) -i_31_ + ((float) i_19_ + (float) class51.anInt623 / 2.0F), 4096, (int) (-d / 6.283185307179586 * 65535.0));
			} else
				Class331.method3409(i_19_, var_aa, class51, Class100.aClass397Array1069[i], i_21_, i_17_, i_20_, false);
		}
	}

	final void close(int i) throws IOException {
		if (raf != null) {
			raf.close();
			raf = null;
		}
		if (i != 0) {
			/* empty */
		}
	}

	final int read(byte i, int i_32_, byte[] is, int i_33_) throws IOException {
		int i_34_ = raf.read(is, i_32_, i_33_);
		if (i_34_ > 0)
			offset += (long) i_34_;
		if (i != 9)
			f = null;
		return i_34_;
	}

	public static void method682(int i) {
		aClass263_670 = null;
		if (i != -2)
			method676(109, -39, -101, 55, 85, -103, 61, 7, -3);
		aClass311_675 = null;
	}

	FileOnDisk(File file, String string, long l) throws IOException {
		if (l == -1L)
			l = 9223372036854775807L;
		if (file.length() > l)
			file.delete();
		raf = new RandomAccessFile(file, string);
		offset = 0L;
		f = file;
		aLong671 = l;
		int i = raf.read();
		if (i != -1 && !string.equals("r")) {
			raf.seek(0L);
			raf.write(i);
		}
		raf.seek(0L);
	}

	final void write(int i, byte[] is, int i_35_, boolean bool) throws IOException {
		if (aLong671 < offset + (long) i) {
			raf.seek(aLong671);
			raf.write(1);
			throw new EOFException();
		}
		raf.write(is, i_35_, i);
		offset += (long) i;
		if (bool != true)
			aFloat674 = 1.8106354F;
	}
}
