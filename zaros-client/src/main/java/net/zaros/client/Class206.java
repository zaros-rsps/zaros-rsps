package net.zaros.client;

/* Class206 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class206 {
	static int anInt2067;
	static int anInt2068;
	static int worldBaseX;
	static InterfaceComponent[] aClass51Array2070;
	static boolean aBoolean2071 = false;

	public static void method1988(int i) {
		if (i == -1)
			aClass51Array2070 = null;
	}

	static final void method1989(int i, int i_0_, int i_1_, int i_2_, byte i_3_) {
		Class296_Sub36 class296_sub36 = (Class296_Sub36) Class126.aClass155_1289.removeFirst((byte) 123);
		if (i_3_ != -10)
			method1988(-66);
		for (/**/; class296_sub36 != null; class296_sub36 = (Class296_Sub36) Class126.aClass155_1289.removeNext(i_3_ ^ ~0x3e0))
			StaticMethods.method2460(i_2_, i, i_0_, 4096, class296_sub36, i_1_);
		for (Class296_Sub36 class296_sub36_4_ = ((Class296_Sub36) Class296_Sub15_Sub4.aClass155_6031.removeFirst((byte) 116)); class296_sub36_4_ != null; class296_sub36_4_ = ((Class296_Sub36) Class296_Sub15_Sub4.aClass155_6031.removeNext(1001))) {
			int i_5_ = 1;
			Class280 class280 = class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878.method3516(false);
			int i_6_ = class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878.aClass44_6777.method557((byte) -109);
			if (i_6_ != -1 && !(class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878.aBoolean6783)) {
				if (class280.anInt2554 != i_6_ && i_6_ != class280.anInt2562 && i_6_ != class280.anInt2565 && i_6_ != class280.anInt2566) {
					if (class280.anInt2564 == i_6_ || i_6_ == class280.anInt2598 || class280.anInt2583 == i_6_ || i_6_ == class280.anInt2577)
						i_5_ = 3;
				} else
					i_5_ = 2;
			} else
				i_5_ = 0;
			if (i_5_ != class296_sub36_4_.anInt4868) {
				int i_7_ = (Class137.method1422(class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878, (byte) 87));
				NPCDefinition class147 = (class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878.definition);
				if (class147.configData != null)
					class147 = class147.getNPCDefinitionByConfigID(Class16_Sub3_Sub1.configsRegister);
				if (class147 != null && i_7_ != -1) {
					if (class296_sub36_4_.anInt4851 != i_7_ || (class147.aBoolean1481 == !class296_sub36_4_.aBoolean4850)) {
						boolean bool = false;
						if (class296_sub36_4_.aClass296_Sub45_Sub1_4871 != null) {
							class296_sub36_4_.anInt4856 -= 512;
							if (class296_sub36_4_.anInt4856 <= 0) {
								Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36_4_.aClass296_Sub45_Sub1_4871);
								bool = true;
								class296_sub36_4_.aClass296_Sub45_Sub1_4871 = null;
							}
						} else
							bool = true;
						if (bool) {
							class296_sub36_4_.aClass296_Sub18_4866 = null;
							class296_sub36_4_.anInt4856 = class147.anInt1456;
							class296_sub36_4_.aClass296_Sub19_Sub1_4862 = null;
							class296_sub36_4_.aBoolean4850 = class147.aBoolean1481;
							class296_sub36_4_.anInt4851 = i_7_;
							class296_sub36_4_.anInt4868 = i_5_;
						}
					} else {
						class296_sub36_4_.anInt4856 = class147.anInt1456;
						class296_sub36_4_.anInt4868 = i_5_;
					}
				} else {
					class296_sub36_4_.aBoolean4850 = false;
					class296_sub36_4_.anInt4868 = i_5_;
					class296_sub36_4_.anInt4851 = -1;
				}
			}
			class296_sub36_4_.anInt4880 = (class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878.tileX);
			class296_sub36_4_.anInt4859 = ((class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878.tileX) + (class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878.getSize() << 8));
			class296_sub36_4_.anInt4879 = (class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878.tileY);
			class296_sub36_4_.anInt4869 = ((class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878.tileY) + (class296_sub36_4_.aClass338_Sub3_Sub1_Sub3_Sub2_4878.getSize() << 8));
			StaticMethods.method2460(i_2_, i, i_0_, i_3_ + 4106, class296_sub36_4_, i_1_);
		}
		for (Class296_Sub36 class296_sub36_8_ = ((Class296_Sub36) ha_Sub1_Sub1.aClass263_5823.getFirst(true)); class296_sub36_8_ != null; class296_sub36_8_ = ((Class296_Sub36) ha_Sub1_Sub1.aClass263_5823.getNext(0))) {
			int i_9_ = 1;
			Class280 class280 = class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.method3516(false);
			int i_10_ = class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.aClass44_6777.method557((byte) -72);
			if (i_10_ != -1 && !(class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.aBoolean6783)) {
				if (class280.anInt2554 == i_10_ || i_10_ == class280.anInt2562 || class280.anInt2565 == i_10_ || i_10_ == class280.anInt2566)
					i_9_ = 2;
				else if (class280.anInt2564 == i_10_ || class280.anInt2598 == i_10_ || i_10_ == class280.anInt2583 || i_10_ == class280.anInt2577)
					i_9_ = 3;
			} else
				i_9_ = 0;
			if (i_9_ != class296_sub36_8_.anInt4868) {
				int i_11_ = (Class41_Sub5.method410(class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864, 11736));
				if (i_11_ == class296_sub36_8_.anInt4851 && !(class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.hasDisplayName) == !class296_sub36_8_.aBoolean4850) {
					class296_sub36_8_.anInt4856 = (class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.anInt6887);
					class296_sub36_8_.anInt4868 = i_9_;
				} else {
					boolean bool = false;
					if (class296_sub36_8_.aClass296_Sub45_Sub1_4871 == null)
						bool = true;
					else {
						class296_sub36_8_.anInt4856 -= 512;
						if (class296_sub36_8_.anInt4856 <= 0) {
							Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36_8_.aClass296_Sub45_Sub1_4871);
							class296_sub36_8_.aClass296_Sub45_Sub1_4871 = null;
							bool = true;
						}
					}
					if (bool) {
						class296_sub36_8_.anInt4856 = (class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.anInt6887);
						class296_sub36_8_.aClass296_Sub19_Sub1_4862 = null;
						class296_sub36_8_.aBoolean4850 = (class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.hasDisplayName);
						class296_sub36_8_.anInt4868 = i_9_;
						class296_sub36_8_.aClass296_Sub18_4866 = null;
						class296_sub36_8_.anInt4851 = i_11_;
					}
				}
			}
			class296_sub36_8_.anInt4880 = (class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.tileX);
			class296_sub36_8_.anInt4859 = ((class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.tileX) + (class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.getSize() << 8));
			class296_sub36_8_.anInt4879 = (class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.tileY);
			class296_sub36_8_.anInt4869 = ((class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.tileY) + (class296_sub36_8_.aClass338_Sub3_Sub1_Sub3_Sub1_4864.getSize() << 8));
			StaticMethods.method2460(i_2_, i, i_0_, 4096, class296_sub36_8_, i_1_);
		}
	}

	static final boolean method1990(int i, int i_12_, byte i_13_) {
		if (i_13_ != 89)
			method1992(-41, -82, -117, null);
		if ((i & 0x800) == 0)
			return false;
		return true;
	}

	static final void method1991(int i, byte i_14_, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_) {
		if (i_14_ <= 103)
			aClass51Array2070 = null;
		Class338_Sub3_Sub2.method3553((byte) -44, i_17_);
		int i_20_ = 0;
		int i_21_ = -i_15_ + i_17_;
		if (i_21_ < 0)
			i_21_ = 0;
		int i_22_ = i_17_;
		int i_23_ = -i_17_;
		int i_24_ = i_21_;
		int i_25_ = -i_21_;
		int i_26_ = -1;
		int i_27_ = -1;
		if (i >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i) {
			int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i];
			int i_28_ = ParticleEmitterRaw.method1668(-i_17_ + i_19_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 122);
			int i_29_ = ParticleEmitterRaw.method1668(i_17_ + i_19_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -10);
			int i_30_ = ParticleEmitterRaw.method1668(i_19_ - i_21_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 121);
			int i_31_ = ParticleEmitterRaw.method1668(i_19_ + i_21_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -19);
			Class296_Sub14.method2511(is, i_30_, (byte) -1, i_18_, i_28_);
			Class296_Sub14.method2511(is, i_31_, (byte) 8, i_16_, i_30_);
			Class296_Sub14.method2511(is, i_29_, (byte) -66, i_18_, i_31_);
		}
		while (i_22_ > i_20_) {
			i_26_ += 2;
			i_27_ += 2;
			i_25_ += i_27_;
			i_23_ += i_26_;
			if (i_25_ >= 0 && i_24_ >= 1) {
				i_24_--;
				i_25_ -= i_24_ << 1;
				Class358.anIntArray3087[i_24_] = i_20_;
			}
			i_20_++;
			if (i_23_ >= 0) {
				i_22_--;
				i_23_ -= i_22_ << 1;
				int i_32_ = i - i_22_;
				int i_33_ = i_22_ + i;
				if (EmissiveTriangle.anInt952 <= i_33_ && i_32_ <= RuntimeException_Sub1.anInt3391) {
					if (i_22_ < i_21_) {
						int i_34_ = Class358.anIntArray3087[i_22_];
						int i_35_ = ParticleEmitterRaw.method1668(i_20_ + i_19_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -40);
						int i_36_ = ParticleEmitterRaw.method1668(i_19_ - i_20_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 8);
						int i_37_ = ParticleEmitterRaw.method1668(i_34_ + i_19_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 123);
						int i_38_ = ParticleEmitterRaw.method1668(-i_34_ + i_19_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 123);
						if (RuntimeException_Sub1.anInt3391 >= i_33_) {
							int[] is = (Class296_Sub51_Sub37.anIntArrayArray6536[i_33_]);
							Class296_Sub14.method2511(is, i_38_, (byte) 127, i_18_, i_36_);
							Class296_Sub14.method2511(is, i_37_, (byte) -112, i_16_, i_38_);
							Class296_Sub14.method2511(is, i_35_, (byte) 117, i_18_, i_37_);
						}
						if (EmissiveTriangle.anInt952 <= i_32_) {
							int[] is = (Class296_Sub51_Sub37.anIntArrayArray6536[i_32_]);
							Class296_Sub14.method2511(is, i_38_, (byte) 119, i_18_, i_36_);
							Class296_Sub14.method2511(is, i_37_, (byte) 124, i_16_, i_38_);
							Class296_Sub14.method2511(is, i_35_, (byte) 124, i_18_, i_37_);
						}
					} else {
						int i_39_ = ParticleEmitterRaw.method1668(i_20_ + i_19_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 127);
						int i_40_ = ParticleEmitterRaw.method1668(i_19_ - i_20_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -72);
						if (i_33_ <= RuntimeException_Sub1.anInt3391)
							Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_33_]), i_39_, (byte) -25, i_18_, i_40_);
						if (i_32_ >= EmissiveTriangle.anInt952)
							Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_32_]), i_39_, (byte) -115, i_18_, i_40_);
					}
				}
			}
			int i_41_ = i - i_20_;
			int i_42_ = i + i_20_;
			if (EmissiveTriangle.anInt952 <= i_42_ && i_41_ <= RuntimeException_Sub1.anInt3391) {
				int i_43_ = i_19_ + i_22_;
				int i_44_ = -i_22_ + i_19_;
				if (ConfigurationDefinition.anInt676 <= i_43_ && Class288.anInt2652 >= i_44_) {
					i_43_ = ParticleEmitterRaw.method1668(i_43_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 123);
					i_44_ = ParticleEmitterRaw.method1668(i_44_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -85);
					if (i_21_ <= i_20_) {
						if (i_42_ <= RuntimeException_Sub1.anInt3391)
							Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_42_]), i_43_, (byte) 121, i_18_, i_44_);
						if (EmissiveTriangle.anInt952 <= i_41_)
							Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_41_]), i_43_, (byte) 125, i_18_, i_44_);
					} else {
						int i_45_ = (i_24_ < i_20_ ? Class358.anIntArray3087[i_20_] : i_24_);
						int i_46_ = ParticleEmitterRaw.method1668(i_45_ + i_19_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -77);
						int i_47_ = ParticleEmitterRaw.method1668(i_19_ - i_45_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 124);
						if (RuntimeException_Sub1.anInt3391 >= i_42_) {
							int[] is = (Class296_Sub51_Sub37.anIntArrayArray6536[i_42_]);
							Class296_Sub14.method2511(is, i_47_, (byte) -61, i_18_, i_44_);
							Class296_Sub14.method2511(is, i_46_, (byte) 123, i_16_, i_47_);
							Class296_Sub14.method2511(is, i_43_, (byte) 114, i_18_, i_46_);
						}
						if (EmissiveTriangle.anInt952 <= i_41_) {
							int[] is = (Class296_Sub51_Sub37.anIntArrayArray6536[i_41_]);
							Class296_Sub14.method2511(is, i_47_, (byte) 122, i_18_, i_44_);
							Class296_Sub14.method2511(is, i_46_, (byte) 112, i_16_, i_47_);
							Class296_Sub14.method2511(is, i_43_, (byte) -103, i_18_, i_46_);
						}
					}
				}
			}
		}
	}

	static final void method1992(int i, int i_48_, int i_49_, InterfaceComponent class51) {
		Class380.anInt3206 = i_49_;
		if (i_48_ != -26396)
			method1991(-96, (byte) 61, -69, 40, 75, -41, 104);
		Class28.anInt299 = i;
		Applet_Sub1.aClass51_12 = class51;
	}
}
