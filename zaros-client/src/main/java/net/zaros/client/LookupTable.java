package net.zaros.client;

/* Class1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class LookupTable {
	static Class81 aClass81_51;
	static int anInt52 = -1;
	static Js5 aClass138_53;
	static int anInt54;
	private int[] identTable;

	public static void method162(byte i) {
		aClass138_53 = null;
		aClass81_51 = null;
		if (i != 90) {
			aClass138_53 = null;
		}
	}

	static final String method163(int i) {
		if (i != 27006) {
			method162((byte) 45);
		}
		String string = "www";
		if (Class41_Sub29.modeWhere == ModeWhere.officeModeWhere) {
			string = "www-wtrc";
		} else if (Class41_Sub29.modeWhere != Class121.office2ModeWhere) {
			if (Class41_Sub29.modeWhere == Class3.office3ModeWhere) {
				string = "www-wtwip";
			}
		} else {
			string = "www-wtqa";
		}
		String string_1_ = "";
		if (Class347.settings != null) {
			string_1_ = "/p=" + Class347.settings;
		}
		return "http://" + string + "." + Class296_Sub50.game.aString338 + ".com/l=" + Class394.langID + "/a=" + Class209.affliateID + string_1_ + "/";
	}

	final int lookup(int i) {
		int i_3_ = (identTable.length >> 1) - 1;
		int i_4_ = i & i_3_;
		for (;;) {
			int i_5_ = identTable[i_4_ + 1 + i_4_];
			if (i_5_ == -1) {
				return -1;
			}
			if (i == identTable[i_4_ + i_4_]) {
				return i_5_;
			}
			i_4_ = i_3_ & i_4_ + 1;
		}
	}

	static final void method165(byte i) {
		Class377.loginConnection.method1968(0);
		Class377.loginConnection.incomingPacket2 = null;
		Class377.loginConnection.in_stream.pos = 0;
		Class377.loginConnection.incomingPacket = null;
		Class377.loginConnection.anInt2062 = 0;
		Class377.loginConnection.protSize = 0;
		Class379_Sub2_Sub1.anInt6607 = 0;
		Class377.loginConnection.incomingPacket3 = null;
		Class377.loginConnection.incomingPacket1 = null;
		Class88.method829((byte) -82);
		Model.method1739(90);
		if (i > 55) {
			for (int i_6_ = 0; i_6_ < 2048; i_6_++) {
				PlayerUpdate.visiblePlayers[i_6_] = null;
			}
			Class296_Sub51_Sub11.localPlayer = null;
			for (int i_7_ = 0; i_7_ < Class368_Sub23.npcNodeCount; i_7_++) {
				NPC class338_sub3_sub1_sub3_sub2 = Class241.npcNodes[i_7_].value;
				if (class338_sub3_sub1_sub3_sub2 != null) {
					class338_sub3_sub1_sub3_sub2.interactingWithIndex = -1;
				}
			}
			Class379_Sub2_Sub1.method3972(0);
			Class368_Sub11.anInt5486 = Class242.anInt2309 = -1;
			Class361.anInt3103 = 1;
			Class41_Sub8.method422(1, 11);
			for (int i_8_ = 0; i_8_ < 100; i_8_++) {
				Class360_Sub9.aBooleanArray5345[i_8_] = true;
			}
			Class300.method3248((byte) 19);
			GameClient.aClass296_Sub48_3717 = null;
			Class338_Sub3_Sub3_Sub2.aLong6628 = 0L;
		}
	}

	LookupTable(int[] is) {
		int i;
		for (i = 1; i <= (is.length >> 1) + is.length; i <<= 1) {
			/* empty */
		}
		identTable = new int[i + i];
		for (int i_9_ = 0; i_9_ < i + i; i_9_++) {
			identTable[i_9_] = -1;
		}
		for (int i_10_ = 0; i_10_ < is.length; i_10_++) {
			int i_11_;
			for (i_11_ = i - 1 & is[i_10_]; identTable[i_11_ + i_11_ + 1] != -1; i_11_ = i_11_ + 1 & i - 1) {
				/* empty */
			}
			identTable[i_11_ + i_11_] = is[i_10_];
			identTable[i_11_ + i_11_ + 1] = i_10_;
		}
	}

	static {
		aClass81_51 = new Class81("", 17);
	}
}
