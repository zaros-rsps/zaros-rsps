package net.zaros.client;
/* Class102 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jagdx.IDirect3DSurface;
import jagdx.IDirect3DSwapChain;

final class Class102 {
	private IDirect3DSurface anIDirect3DSurface1083;
	private IDirect3DSwapChain anIDirect3DSwapChain1084;
	private IDirect3DSurface anIDirect3DSurface1085;

	final boolean method889(int i) {
		if (i != 0)
			return false;
		return anIDirect3DSwapChain1084 != null;
	}

	final int method890(int i, int i_0_) {
		if (i_0_ != -15265)
			return -44;
		return anIDirect3DSwapChain1084.Present(i);
	}

	final void method891(IDirect3DSwapChain idirect3dswapchain, boolean bool, IDirect3DSurface idirect3dsurface) {
		anIDirect3DSwapChain1084 = idirect3dswapchain;
		anIDirect3DSurface1083 = idirect3dsurface;
		if (bool != true)
			anIDirect3DSurface1083 = null;
		anIDirect3DSurface1085 = anIDirect3DSwapChain1084.a(0, 0);
	}

	final void method892(int i) {
		if (i == 0) {
			if (anIDirect3DSurface1085 != null) {
				anIDirect3DSurface1085.b((byte) 83);
				anIDirect3DSurface1085 = null;
			}
			if (anIDirect3DSurface1083 != null) {
				anIDirect3DSurface1083.b((byte) 83);
				anIDirect3DSurface1083 = null;
			}
			if (anIDirect3DSwapChain1084 != null) {
				anIDirect3DSwapChain1084.b((byte) 83);
				anIDirect3DSwapChain1084 = null;
			}
		}
	}

	Class102(IDirect3DSwapChain idirect3dswapchain, IDirect3DSurface idirect3dsurface) {
		method891(idirect3dswapchain, true, idirect3dsurface);
	}
}
