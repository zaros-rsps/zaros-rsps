package net.zaros.client;

/* Class123_Sub1_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class123_Sub1_Sub2 extends Class123_Sub1 {
	private byte[] aByteArray5815;
	static int anInt5816;
	static int[] anIntArray5817;

	final byte[] method1064(int i, int i_0_, int i_1_, byte i_2_) {
		aByteArray5815 = new byte[i * i_0_ * (i_1_ * 2)];
		if (i_2_ != 17)
			return null;
		this.method1051(i_0_, i, -96, i_1_);
		return aByteArray5815;
	}

	final void method1058(byte i, byte i_3_, int i_4_) {
		int i_5_ = 26 / ((i + 24) / 42);
		int i_6_ = i_4_ * 2;
		int i_7_ = i_3_ & 0xff;
		aByteArray5815[i_6_++] = (byte) (i_7_ * 3 >> 5);
		aByteArray5815[i_6_] = (byte) (i_7_ * 3 >> 5);
	}

	public Class123_Sub1_Sub2() {
		super(8, 5, 8, 8, 2, 0.1F, 0.55F, 3.0F);
	}

	static final void method1065(int i, int i_8_, int i_9_, int i_10_, Mobile class338_sub3_sub1_sub3) {
		Class280 class280 = class338_sub3_sub1_sub3.method3516(false);
		Animator class44 = class338_sub3_sub1_sub3.aClass44_6777;
		if (i_8_ != 0)
			anIntArray5817 = null;
		int i_11_ = ((-class338_sub3_sub1_sub3.aClass5_6804.anInt77 + class338_sub3_sub1_sub3.anInt6816) & 0x3fff);
		if (i_10_ != -1) {
			if (class338_sub3_sub1_sub3.interactingWithIndex != -1 && (i_11_ >= 10240 || i_11_ <= 2048)) {
				int i_12_ = ((Class296_Sub51_Sub30.anIntArray6495[i] - class338_sub3_sub1_sub3.aClass5_6804.anInt77) & 0x3fff);
				if (i_10_ != 2 || class280.anInt2554 == -1) {
					if (i_10_ != 0 || class280.anInt2564 == -1) {
						if (i_12_ <= 2048 || i_12_ > 6144 || class280.anInt2586 == -1) {
							if (i_12_ < 10240 || i_12_ >= 14336 || class280.anInt2599 == -1) {
								if (i_12_ > 6144 && i_12_ < 10240 && class280.anInt2551 != -1)
									class44.method549((byte) 115, class280.anInt2551);
								else
									class44.method549((byte) 115, class280.anInt2597);
							} else
								class44.method549((byte) 115, class280.anInt2599);
						} else
							class44.method549((byte) 115, class280.anInt2586);
					} else if (i_12_ <= 2048 || i_12_ > 6144 || class280.anInt2583 == -1) {
						if (i_12_ >= 10240 && i_12_ < 14336 && class280.anInt2577 != -1)
							class44.method549((byte) 115, class280.anInt2577);
						else if (i_12_ > 6144 && i_12_ < 10240 && class280.anInt2598 != -1)
							class44.method549((byte) 115, class280.anInt2598);
						else
							class44.method549((byte) 115, class280.anInt2564);
					} else
						class44.method549((byte) 115, class280.anInt2583);
				} else if (i_12_ <= 2048 || i_12_ > 6144 || class280.anInt2565 == -1) {
					if (i_12_ >= 10240 && i_12_ < 14336 && class280.anInt2566 != -1)
						class44.method549((byte) 115, class280.anInt2566);
					else if (i_12_ <= 6144 || i_12_ >= 10240 || class280.anInt2562 == -1)
						class44.method549((byte) 115, class280.anInt2554);
					else
						class44.method549((byte) 115, class280.anInt2562);
				} else
					class44.method549((byte) 115, class280.anInt2565);
				class338_sub3_sub1_sub3.aBoolean6783 = false;
			} else if (i_11_ != 0 || class338_sub3_sub1_sub3.anInt6810 > 25) {
				if (i_10_ == 2 && class280.anInt2554 != -1) {
					if (i_9_ < 0 && class280.anInt2581 != -1)
						class44.method549((byte) 115, class280.anInt2581);
					else if (i_9_ > 0 && class280.anInt2582 != -1)
						class44.method549((byte) 115, class280.anInt2582);
					else
						class44.method549((byte) 115, class280.anInt2554);
				} else if (i_10_ == 0 && class280.anInt2564 != -1) {
					if (i_9_ < 0 && class280.anInt2560 != -1)
						class44.method549((byte) 115, class280.anInt2560);
					else if (i_9_ > 0 && class280.anInt2563 != -1)
						class44.method549((byte) 115, class280.anInt2563);
					else
						class44.method549((byte) 115, class280.anInt2564);
				} else if (i_9_ >= 0 || class280.anInt2596 == -1) {
					if (i_9_ <= 0 || class280.anInt2552 == -1)
						class44.method549((byte) 115, class280.anInt2597);
					else
						class44.method549((byte) 115, class280.anInt2552);
				} else
					class44.method549((byte) 115, class280.anInt2596);
				class338_sub3_sub1_sub3.aBoolean6783 = false;
			} else {
				if (i_10_ != 2 || class280.anInt2554 == -1) {
					if (i_10_ != 0 || class280.anInt2564 == -1)
						class44.method549((byte) 115, class280.anInt2597);
					else
						class44.method549((byte) 115, class280.anInt2564);
				} else
					class44.method549((byte) 115, class280.anInt2554);
				class338_sub3_sub1_sub3.aBoolean6783 = false;
			}
		} else if (i_11_ == 0 && class338_sub3_sub1_sub3.anInt6810 <= 25) {
			if (!class338_sub3_sub1_sub3.aBoolean6783 || !class280.method2342(i_8_ + 6660, class44.method557((byte) -63))) {
				class44.method549((byte) 115, class280.method2348((byte) 34));
				class338_sub3_sub1_sub3.aBoolean6783 = class44.method570((byte) 40);
			}
		} else if (i_9_ < 0 && class280.anInt2592 != -1) {
			class44.method549((byte) 115, class280.anInt2592);
			class338_sub3_sub1_sub3.aBoolean6783 = false;
		} else if (i_9_ > 0 && class280.anInt2550 != -1) {
			class44.method549((byte) 115, class280.anInt2550);
			class338_sub3_sub1_sub3.aBoolean6783 = false;
		} else if (!class338_sub3_sub1_sub3.aBoolean6783 || !class280.method2342(6660, class44.method557((byte) -128))) {
			class44.method549((byte) 115, class280.method2348((byte) 106));
			class338_sub3_sub1_sub3.aBoolean6783 = class338_sub3_sub1_sub3.aClass44_6777.method570((byte) 40);
		}
	}

	public static void method1066(boolean bool) {
		anIntArray5817 = null;
		if (bool)
			anIntArray5817 = null;
	}
}
