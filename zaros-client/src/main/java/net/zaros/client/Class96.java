package net.zaros.client;
/* Class96 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class96 {
	int anInt1038;
	private int anInt1039;
	int anInt1040;
	private int anInt1041;
	int anInt1042;
	private int anInt1043;
	private int anInt1044;
	int anInt1045;
	int anInt1046;
	int anInt1047;
	static Class246 aClass246_1048 = new Class246();
	static int[] anIntArray1049;

	final boolean method867(int i, int i_0_, int i_1_, byte i_2_) {
		if (i_1_ < anInt1042 || anInt1045 < i_1_)
			return false;
		if (i < anInt1040 || anInt1047 < i)
			return false;
		if (anInt1038 > i_0_ || anInt1046 < i_0_)
			return false;
		if (i_2_ >= -93)
			return false;
		int i_3_ = -anInt1044 + i_1_;
		int i_4_ = -anInt1039 + i_0_;
		if (i_3_ * i_3_ + i_4_ * i_4_ >= anInt1043)
			return false;
		return true;
	}

	static final void method868(byte i) {
		if (i != -35)
			anIntArray1049 = null;
		if (Class296_Sub51_Sub9.anInt6389 > 0) {
			int i_5_ = 0;
			for (int i_6_ = 0; i_6_ < Class296_Sub51_Sub15.aStringArray6425.length; i_6_++) {
				if (Class296_Sub51_Sub15.aStringArray6425[i_6_].indexOf("--> ") != -1 && ++i_5_ == Class296_Sub51_Sub9.anInt6389) {
					Class355_Sub2.aString3503 = (Class296_Sub51_Sub15.aStringArray6425[i_6_].substring(Class296_Sub51_Sub15.aStringArray6425[i_6_].indexOf(">") + 2));
					break;
				}
			}
		} else
			Class355_Sub2.aString3503 = "";
	}

	final void method869(int i, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_) {
		anInt1041 = i_16_;
		anInt1044 = i_10_;
		anInt1043 = i_12_ * i_12_;
		anInt1039 = i_15_;
		anInt1040 = anInt1041 + i_14_;
		anInt1046 = i_13_ + anInt1039;
		anInt1047 = i_8_ + anInt1041;
		anInt1038 = anInt1039 + i;
		int i_17_ = 19 / ((-15 - i_7_) / 38);
		anInt1042 = i_9_ + anInt1044;
		anInt1045 = i_11_ + anInt1044;
	}

	static final boolean method870(int i, byte i_18_) {
		if (i_18_ != 62)
			return true;
		if (i != 7 && i != 9)
			return false;
		return true;
	}

	public static void method871(int i) {
		anIntArray1049 = null;
		if (i == 0)
			aClass246_1048 = null;
	}

	Class96(int i, int i_19_, int i_20_, int i_21_, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_, int i_27_) {
		anInt1044 = i;
		anInt1041 = i_19_;
		anInt1043 = i_21_ * i_21_;
		anInt1039 = i_20_;
		anInt1040 = anInt1041 + i_24_;
		anInt1045 = anInt1044 + i_23_;
		anInt1042 = anInt1044 + i_22_;
		anInt1047 = anInt1041 + i_25_;
		anInt1046 = i_27_ + anInt1039;
		anInt1038 = i_26_ + anInt1039;
	}
}
