package net.zaros.client;
import jaggl.OpenGL;

abstract class Class205 implements Interface6 {
	private Class131 aClass131_3504 = ISAACCipher.aClass131_1892;
	static Sprite[] aClass397Array3505;
	private boolean aBoolean3506;
	int anInt3507;
	ha_Sub1_Sub1 aHa_Sub1_Sub1_3508;
	static int foundY = -1;
	private int anInt3510;
	private int anInt3511;
	static int anInt3512;
	Class67 aClass67_3513;
	Class202 aClass202_3514;

	final int method1971(int i) {
		if (Class67.aClass67_745 == aClass67_3513) {
			if (Class122_Sub1_Sub1.aClass202_6602 != aClass202_3514) {
				if (za_Sub2.aClass202_6555 == aClass202_3514)
					return 6408;
				if (Class13.aClass202_3516 == aClass202_3514)
					return 6406;
				if (StaticMethods.aClass202_6068 != aClass202_3514) {
					if (aClass202_3514 == Class125.aClass202_1285)
						return 6410;
					if (Class55.aClass202_654 == aClass202_3514)
						return 6145;
				} else
					return 6409;
			} else
				return 6407;
		} else if (Class67.aClass67_748 == aClass67_3513) {
			if (Class122_Sub1_Sub1.aClass202_6602 != aClass202_3514) {
				if (za_Sub2.aClass202_6555 != aClass202_3514) {
					if (aClass202_3514 != Class13.aClass202_3516) {
						if (StaticMethods.aClass202_6068 == aClass202_3514)
							return 34846;
						if (aClass202_3514 == Class125.aClass202_1285)
							return 34847;
						if (Class55.aClass202_654 == aClass202_3514)
							return 6145;
					} else
						return 34844;
				} else
					return 34842;
			} else
				return 34843;
		} else if (Class67.aClass67_749 == aClass67_3513) {
			if (Class122_Sub1_Sub1.aClass202_6602 != aClass202_3514) {
				if (za_Sub2.aClass202_6555 == aClass202_3514)
					return 34836;
				if (Class13.aClass202_3516 == aClass202_3514)
					return 34838;
				if (aClass202_3514 != StaticMethods.aClass202_6068) {
					if (Class125.aClass202_1285 != aClass202_3514) {
						if (Class55.aClass202_654 == aClass202_3514)
							return 6145;
					} else
						return 34841;
				} else
					return 34840;
			} else
				return 34837;
		}
		int i_0_ = 73 % ((i - 39) / 61);
		throw new IllegalStateException();
	}

	final void method1972(int i, int[] is, int i_1_, byte i_2_, int i_3_) {
		if (i_3_ > 0 && !Class30.method329(2844, i_3_))
			throw new IllegalArgumentException("");
		if (i_1_ > 0 && !Class30.method329(2844, i_1_))
			throw new IllegalArgumentException("");
		if (za_Sub2.aClass202_6555 != aClass202_3514)
			throw new IllegalArgumentException("");
		int i_4_ = -112 % ((5 - i_2_) / 58);
		int i_5_ = 0;
		int i_6_ = i_3_ >= i_1_ ? i_1_ : i_3_;
		int i_7_ = i_3_ >> 1;
		int i_8_ = i_1_ >> 1;
		int[] is_9_ = is;
		int[] is_10_ = new int[i_8_ * i_7_];
		for (;;) {
			OpenGL.glTexImage2Di(i, i_5_, method1971(-107), i_3_, i_1_, 0, 32993, aHa_Sub1_Sub1_3508.anInt5852, is_9_, 0);
			if (i_6_ <= 1)
				break;
			int i_11_ = 0;
			int i_12_ = 0;
			int i_13_ = i_3_ + i_12_;
			for (int i_14_ = 0; i_8_ > i_14_; i_14_++) {
				for (int i_15_ = 0; i_7_ > i_15_; i_15_++) {
					int i_16_ = is_9_[i_12_++];
					int i_17_ = is_9_[i_13_++];
					int i_18_ = is_9_[i_12_++];
					int i_19_ = (i_16_ & 0xff1dac) >> 16;
					int i_20_ = is_9_[i_13_++];
					int i_21_ = i_16_ >> 24 & 0xff;
					int i_22_ = i_16_ >> 8 & 0xff;
					int i_23_ = i_16_ & 0xff;
					i_21_ += i_18_ >> 24 & 0xff;
					i_23_ += i_18_ & 0xff;
					i_19_ += (i_18_ & 0xff3677) >> 16;
					i_22_ += i_18_ >> 8 & 0xff;
					i_22_ += (i_17_ & 0xff49) >> 8;
					i_19_ += (i_17_ & 0xffca84) >> 16;
					i_23_ += i_17_ & 0xff;
					i_21_ += i_17_ >> 24 & 0xff;
					i_23_ += i_20_ & 0xff;
					i_19_ += i_20_ >> 16 & 0xff;
					i_21_ += i_20_ >> 24 & 0xff;
					i_22_ += (i_20_ & 0xffe8) >> 8;
					is_10_[i_11_++] = (Class48.bitOR((((((i_21_ << 22) & -16777216) | (16711680 & (i_19_ << 14))) | (i_22_ << 6 & 65280))), (i_23_ & 1020) >> 2));
				}
				i_12_ += i_3_;
				i_13_ += i_3_;
			}
			int[] is_24_ = is_10_;
			is_10_ = is_9_;
			is_9_ = is_24_;
			i_3_ = i_7_;
			i_1_ = i_8_;
			i_5_++;
			i_7_ >>= 1;
			i_6_ >>= 1;
			i_8_ >>= 1;
		}
	}

	private final void method1973(int i) {
		aHa_Sub1_Sub1_3508.method1140(this, false);
		if (ISAACCipher.aClass131_1892 != aClass131_3504) {
			OpenGL.glTexParameteri(anInt3507, 10241, aBoolean3506 ? 9984 : 9728);
			OpenGL.glTexParameteri(anInt3507, 10240, 9728);
		} else {
			OpenGL.glTexParameteri(anInt3507, 10241, aBoolean3506 ? 9987 : 9729);
			OpenGL.glTexParameteri(anInt3507, 10240, 9729);
		}
		if (i != -1136834440)
			method1973(0);
	}

	public final void method28(int i) {
		int i_25_ = aHa_Sub1_Sub1_3508.method1114(false);
		int i_26_ = aHa_Sub1_Sub1_3508.anIntArray5849[i_25_];
		if (anInt3507 != i_26_) {
			if (i_26_ != 0) {
				OpenGL.glBindTexture(i_26_, 0);
				OpenGL.glDisable(i_26_);
			}
			OpenGL.glEnable(anInt3507);
			aHa_Sub1_Sub1_3508.anIntArray5849[i_25_] = anInt3507;
		}
		OpenGL.glBindTexture(anInt3507, anInt3511);
		if (i != -9994)
			foundY = -109;
	}

	public final void method29(Class131 class131, int i) {
		if (i != 16518)
			aClass131_3504 = null;
		if (aClass131_3504 != class131) {
			aClass131_3504 = class131;
			method1973(-1136834440);
		}
	}

	private final void method1974(byte i, int i_27_) {
		aHa_Sub1_Sub1_3508.anInt3937 -= i_27_;
		int i_28_ = 33 / ((i + 16) / 61);
		aHa_Sub1_Sub1_3508.anInt3937 += method1977(false);
	}

	protected final void finalize() throws Throwable {
		method1979(false);
		super.finalize();
	}

	final void method1975(int i, byte i_29_, float[] fs, int i_30_, int i_31_) {
		if (i_30_ > 0 && !Class30.method329(2844, i_30_))
			throw new IllegalArgumentException("");
		if (i_31_ > 0 && !Class30.method329(2844, i_31_))
			throw new IllegalArgumentException("");
		int i_32_ = -26 % ((70 - i_29_) / 41);
		int i_33_ = aClass202_3514.anInt2042;
		int i_34_ = 0;
		int i_35_ = i_31_ > i_30_ ? i_30_ : i_31_;
		int i_36_ = i_30_ >> 1;
		int i_37_ = i_31_ >> 1;
		float[] fs_38_ = fs;
		float[] fs_39_ = new float[i_36_ * i_37_ * i_33_];
		for (;;) {
			OpenGL.glTexImage2Df(i, i_34_, method1971(-122), i_30_, i_31_, 0, BITConfigDefinition.method2350(6409, aClass202_3514), 5126, fs_38_, 0);
			if (i_35_ <= 1)
				break;
			int i_40_ = i_33_ * i_30_;
			for (int i_41_ = 0; i_33_ > i_41_; i_41_++) {
				int i_42_ = i_41_;
				int i_43_ = i_41_;
				int i_44_ = i_40_ + i_43_;
				for (int i_45_ = 0; i_45_ < i_37_; i_45_++) {
					for (int i_46_ = 0; i_36_ > i_46_; i_46_++) {
						float f = fs_38_[i_43_];
						i_43_ += i_33_;
						f += fs_38_[i_43_];
						i_43_ += i_33_;
						f += fs_38_[i_44_];
						i_44_ += i_33_;
						f += fs_38_[i_44_];
						i_44_ += i_33_;
						fs_39_[i_42_] = f * 0.25F;
						i_42_ += i_33_;
					}
					i_43_ += i_40_;
					i_44_ += i_40_;
				}
			}
			float[] fs_47_ = fs_39_;
			fs_39_ = fs_38_;
			fs_38_ = fs_47_;
			i_30_ = i_36_;
			i_31_ = i_37_;
			i_36_ >>= 1;
			i_35_ >>= 1;
			i_37_ >>= 1;
			i_34_++;
		}
	}

	public static void method1976(int i) {
		int i_48_ = 87 % ((i - 13) / 53);
		aClass397Array3505 = null;
	}

	private final int method1977(boolean bool) {
		if (bool)
			foundY = 25;
		int i = aClass67_3513.anInt741 * (aClass202_3514.anInt2042 * anInt3510);
		if (aBoolean3506)
			return i * 4 / 3;
		return i;
	}

	final void method1978(byte[] is, int i, int i_49_, int i_50_, int i_51_) {
		if (i_51_ > 0 && !Class30.method329(i ^ 0x1f1d, i_51_))
			throw new IllegalArgumentException("");
		if (i_50_ > 0 && !Class30.method329(2844, i_50_))
			throw new IllegalArgumentException("");
		int i_52_ = aClass202_3514.anInt2042;
		int i_53_ = 0;
		int i_54_ = i_50_ > i_51_ ? i_51_ : i_50_;
		int i_55_ = i_51_ >> 1;
		if (i == 5121) {
			int i_56_ = i_50_ >> 1;
			byte[] is_57_ = is;
			byte[] is_58_ = new byte[i_56_ * (i_55_ * i_52_)];
			for (;;) {
				OpenGL.glTexImage2Dub(i_49_, i_53_, method1971(-59), i_51_, i_50_, 0, BITConfigDefinition.method2350(i ^ 0xd08, aClass202_3514), 5121, is_57_, 0);
				if (i_54_ <= 1)
					break;
				int i_59_ = i_52_ * i_51_;
				byte[] is_60_ = is_58_;
				for (int i_61_ = 0; i_61_ < i_52_; i_61_++) {
					int i_62_ = i_61_;
					int i_63_ = i_61_;
					int i_64_ = i_63_ + i_59_;
					for (int i_65_ = 0; i_65_ < i_56_; i_65_++) {
						for (int i_66_ = 0; i_66_ < i_55_; i_66_++) {
							int i_67_ = is_57_[i_63_];
							i_63_ += i_52_;
							i_67_ += is_57_[i_63_];
							i_67_ += is_57_[i_64_];
							i_63_ += i_52_;
							i_64_ += i_52_;
							i_67_ += is_57_[i_64_];
							i_64_ += i_52_;
							is_58_[i_62_] = (byte) (i_67_ >> 2);
							i_62_ += i_52_;
						}
						i_63_ += i_59_;
						i_64_ += i_59_;
					}
				}
				is_58_ = is_57_;
				is_57_ = is_60_;
				i_51_ = i_55_;
				i_50_ = i_56_;
				i_54_ >>= 1;
				i_55_ >>= 1;
				i_53_++;
				i_56_ >>= 1;
			}
		}
	}

	Class205(ha_Sub1_Sub1 var_ha_Sub1_Sub1, int i, Class202 class202, Class67 class67, int i_68_, boolean bool) {
		aClass202_3514 = class202;
		aHa_Sub1_Sub1_3508 = var_ha_Sub1_Sub1;
		anInt3510 = i_68_;
		aClass67_3513 = class67;
		aBoolean3506 = bool;
		anInt3507 = i;
		OpenGL.glGenTextures(1, Class296_Sub39_Sub1.anIntArray6121, 0);
		anInt3511 = Class296_Sub39_Sub1.anIntArray6121[0];
		method1973(-1136834440);
		method1974((byte) -101, 0);
	}

	private final void method1979(boolean bool) {
		if (!bool) {
			if (anInt3511 > 0) {
				aHa_Sub1_Sub1_3508.method1234(0, method1977(bool), anInt3511);
				anInt3511 = 0;
			}
		}
	}
}
