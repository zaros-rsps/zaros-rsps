package net.zaros.client;

/* Class16_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

class Class16_Sub1 extends Class16 {
	static IncomingPacket aClass231_3732 = new IncomingPacket(16, 2);

	static final boolean method235(byte i, Connection class204) {
		if (i >= -98)
			return true;
		try {
			return PacketParser.method3136(class204, -113008285);
		} catch (java.io.IOException ioexception) {
			if (Class366_Sub6.anInt5392 == 9) {
				class204.aClass154_2045 = null;
				return false;
			}
			Class41_Sub8.method423(0);
			return true;
		} catch (Exception exception) {
			String string = ("T2 - " + (class204.incomingPacket != null ? class204.incomingPacket.getOpcode() : -1) + "," + (class204.incomingPacket1 != null ? class204.incomingPacket1.getOpcode() : -1) + "," + (class204.incomingPacket3 != null ? class204.incomingPacket3.getOpcode() : -1) + " - " + class204.protSize + "," + ((Class296_Sub51_Sub11.localPlayer.waypointQueueX[0]) + Class206.worldBaseX) + "," + (Class41_Sub26.worldBaseY + (Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0])) + " - ");
			for (int i_0_ = 0; class204.protSize > i_0_; i_0_++) {
				if (i_0_ >= 50)
					break;
				string += (class204.in_stream.data[i_0_] + ",");
			}
			Class219_Sub1.method2062(string, (byte) -103, exception);
			StringNode.logout(3, false);
			return true;
		}
	}

	static final void method236(int i) {
		if (i < 3)
			aClass231_3732 = null;
		Class292.anInt2665 = 0;
		Class404.anInt3385 = 0;
		for (int i_1_ = 0; i_1_ < Applet_Sub1.anInt7; i_1_++) {
			int i_2_ = Class317.anInt3705 * i_1_;
			for (int i_3_ = 0; Class317.anInt3705 > i_3_; i_3_++) {
				int i_4_ = i_2_ + i_3_;
				IOException_Sub1.anInterface13Array38[i_4_].method53(Class290.anInt2656 * i_3_, i_1_ * Class395.anInt3317, Class290.anInt2656, Class395.anInt3317, 0, 0, true, true);
			}
		}
	}

	public static void method237(byte i) {
		aClass231_3732 = null;
		int i_5_ = 86 / ((i - 11) / 50);
	}

	static final String method238(int i, Packet class296_sub17, int i_6_) {
		try {
			if (i_6_ < 89)
				return null;
			int i_7_ = class296_sub17.readSmart();
			if (i < i_7_)
				i_7_ = i;
			byte[] is = new byte[i_7_];
			class296_sub17.pos += HashTable.aClass130_2463.method1379((class296_sub17.data), 0, i_7_, -27318, class296_sub17.pos, is);
			String string = Class360.method3722(0, is, i_7_, (byte) 75);
			return string;
		} catch (Exception exception) {
			return "Cabbage";
		}
	}
}
