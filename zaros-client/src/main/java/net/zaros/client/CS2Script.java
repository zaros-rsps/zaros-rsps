package net.zaros.client;
/* Class296_Sub39_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.applet.Applet;

final class CS2Script extends Queuable {
	int longStackSize;
	int stringLocalsCount;
	int longLocalsCount;
	String name;
	int intStackSize;
	Class81 aClass81_6135;
	int[] intPool;
	String[] stringPool;
	int intLocalsCount;
	int[] codeOpcodes;
	static Applet anApplet6140;
	HashTable[] switches;
	long[] longPool;
	int stringStackSize;
	static int anInt6144;

	public static void method2796(int i) {
		int i_0_ = -67 / ((i - 71) / 55);
		anApplet6140 = null;
	}

	static final boolean method2797(int i, int i_1_) {
		int i_2_ = 49 % ((i - 66) / 34);
		if (i_1_ != 2 && i_1_ != 3) {
			return false;
		}
		return true;
	}

	public CS2Script() {
		/* empty */
	}

	static final Sprite method2798(Js5 class138, int i, int i_3_) {
		Sprite class397 = (Sprite) AdvancedMemoryCache.aClass113_1164.get(i);
		if (i_3_ != -3) {
			return null;
		}
		if (class397 == null) {
			if (Class368_Sub2.aBoolean5431) {
				class397 = Class41_Sub13.aHa3774.a(Class186.method1872(class138, i), true);
			} else {
				class397 = Class205_Sub4.method1987(108, class138.get(i));
			}
			AdvancedMemoryCache.aClass113_1164.put(class397, i);
		}
		return class397;
	}
}
