package net.zaros.client;

/* Class296_Sub50 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.lang.reflect.Field;

final class Class296_Sub50 extends Node {
	Class41_Sub4 aClass41_Sub4_4987;
	Class41_Sub30 aClass41_Sub30_4988;
	Class41_Sub13 aClass41_Sub13_4989;
	Class41_Sub15 aClass41_Sub15_4990;
	Class41_Sub26 aClass41_Sub26_4991;
	Class41_Sub12 aClass41_Sub12_4992;
	Class41_Sub3 aClass41_Sub3_4993;
	Class41_Sub28 aClass41_Sub28_4994;
	Class41_Sub23 aClass41_Sub23_4995;
	Class41_Sub20 aClass41_Sub20_4996;
	Class41_Sub22 aClass41_Sub22_4997;
	Class41_Sub23 aClass41_Sub23_4998;
	Class41_Sub9 aClass41_Sub9_4999;
	Class41_Sub5 aClass41_Sub5_5000;
	Class41_Sub25 aClass41_Sub25_5001;
	Class41_Sub29 aClass41_Sub29_5002;
	Class41_Sub13 aClass41_Sub13_5003;
	Class41_Sub7 aClass41_Sub7_5004;
	Class41_Sub23 aClass41_Sub23_5005;
	Class41_Sub30 aClass41_Sub30_5006;
	private Class30 aClass30_5007;
	Class41_Sub1 aClass41_Sub1_5008;
	Class41_Sub10 aClass41_Sub10_5009;
	Class41_Sub27 aClass41_Sub27_5010;
	Class41_Sub19 aClass41_Sub19_5011;
	Class41_Sub18 aClass41_Sub18_5012;
	Class41_Sub2 aClass41_Sub2_5013;
	Class41_Sub6 aClass41_Sub6_5014;
	Class41_Sub21 aClass41_Sub21_5015;
	Class41_Sub24 aClass41_Sub24_5016;
	Class41_Sub23 aClass41_Sub23_5017;
	Class41_Sub14 aClass41_Sub14_5018;
	Class41_Sub16 aClass41_Sub16_5019;
	Class41_Sub21 aClass41_Sub21_5020;
	Class41_Sub23 aClass41_Sub23_5021;
	Class41_Sub17 aClass41_Sub17_5022;
	Class41_Sub11 aClass41_Sub11_5023;
	private GameType aClass35_5024;
	static boolean aBoolean5025;
	Class41_Sub25 aClass41_Sub25_5026;
	Class41_Sub8 aClass41_Sub8_5027;
	static Class357 aClass357_5028 = new Class357();
	Class41_Sub14 aClass41_Sub14_5029;
	static GameType game = null;

	final GameType method3050(int i) {
		if (i != 28520)
			method3054(7);
		return aClass35_5024;
	}

	private final void method3051(byte i) {
		try {
			Field[] fields = this.getClass().getDeclaredFields();
			Field[] fields_0_ = fields;
			for (int i_1_ = 0; i_1_ < fields_0_.length; i_1_++) {
				Field field = fields_0_[i_1_];
				if (Class41.class.isAssignableFrom(field.getType())) {
					Class41 class41 = (Class41) field.get(this);
					class41.method386(2);
				}
			}
			int i_2_ = -118 / ((i - 52) / 41);
		} catch (IllegalAccessException illegalaccessexception) {
			/* empty */
		}
	}

	private final void method3052(int i, Packet class296_sub17, byte i_3_) {
		aClass41_Sub6_5014 = new Class41_Sub6(class296_sub17.g1(), this);
		aClass41_Sub25_5001 = new Class41_Sub25(class296_sub17.g1(), this);
		aClass41_Sub14_5018 = new Class41_Sub14(class296_sub17.g1() + 1, this);
		aClass41_Sub22_4997 = new Class41_Sub22(class296_sub17.g1(), this);
		aClass41_Sub3_4993 = new Class41_Sub3(class296_sub17.g1(), this);
		aClass41_Sub11_5023 = new Class41_Sub11(class296_sub17.g1(), this);
		aClass41_Sub9_4999 = new Class41_Sub9(class296_sub17.g1(), this);
		class296_sub17.g1();
		aClass41_Sub16_5019 = new Class41_Sub16(class296_sub17.g1(), this);
		int i_4_ = class296_sub17.g1();
		int i_5_ = 0;
		if (i >= 17)
			i_5_ = class296_sub17.g1();
		aClass41_Sub18_5012 = new Class41_Sub18(i_4_ <= i_5_ ? i_5_ : i_4_, this);
		boolean bool = true;
		boolean bool_6_ = true;
		if (i >= 2) {
			bool = class296_sub17.g1() == 1;
			if (i >= 17)
				bool_6_ = class296_sub17.g1() == 1;
		} else {
			bool = class296_sub17.g1() == 1;
			class296_sub17.g1();
		}
		aClass41_Sub7_5004 = new Class41_Sub7(bool_6_ | bool ? 1 : 0, this);
		aClass41_Sub2_5013 = new Class41_Sub2(class296_sub17.g1(), this);
		aClass41_Sub1_5008 = new Class41_Sub1(class296_sub17.g1(), this);
		aClass41_Sub21_5020 = new Class41_Sub21(class296_sub17.g1(), this);
		aClass41_Sub24_5016 = new Class41_Sub24(class296_sub17.g1(), this);
		aClass41_Sub23_4995 = new Class41_Sub23(class296_sub17.g1(), this);
		if (i >= 20)
			aClass41_Sub23_4998 = new Class41_Sub23(class296_sub17.g1(), this);
		else
			aClass41_Sub23_4998 = new Class41_Sub23(aClass41_Sub23_4995.method489(121), this);
		aClass41_Sub23_5017 = new Class41_Sub23(class296_sub17.g1(), this);
		aClass41_Sub23_5005 = new Class41_Sub23(class296_sub17.g1(), this);
		if (i >= 21)
			aClass41_Sub23_5021 = new Class41_Sub23(class296_sub17.g1(), this);
		else
			aClass41_Sub23_5021 = new Class41_Sub23(aClass41_Sub23_5017.method489(118), this);
		if (i >= 1) {
			class296_sub17.g2();
			class296_sub17.g2();
		}
		if (i >= 3 && i < 6)
			class296_sub17.g1();
		if (i >= 4)
			aClass41_Sub8_5027 = new Class41_Sub8(class296_sub17.g1(), this);
		class296_sub17.g4();
		if (i >= 6)
			aClass41_Sub13_5003 = new Class41_Sub13(class296_sub17.g1(), this);
		if (i >= 7)
			aClass41_Sub5_5000 = new Class41_Sub5(class296_sub17.g1(), this);
		if (i >= 8)
			class296_sub17.g1();
		if (i >= 9)
			aClass41_Sub4_4987 = new Class41_Sub4(class296_sub17.g1(), this);
		if (i >= 10)
			aClass41_Sub27_5010 = new Class41_Sub27(class296_sub17.g1(), this);
		if (i >= 11)
			aClass41_Sub20_4996 = new Class41_Sub20(class296_sub17.g1(), this);
		if (i >= 12)
			aClass41_Sub11_5023 = new Class41_Sub11(class296_sub17.g1(), this);
		if (i >= 13)
			aClass41_Sub29_5002 = new Class41_Sub29(class296_sub17.g1(), this);
		if (i >= 14)
			aClass41_Sub30_4988 = new Class41_Sub30(class296_sub17.g1(), this);
		if (i >= 15)
			aClass41_Sub12_4992 = new Class41_Sub12(class296_sub17.g1(), this);
		if (i >= 16)
			aClass41_Sub26_4991 = new Class41_Sub26(class296_sub17.g1(), this);
		if (i >= 18)
			aClass41_Sub15_4990 = new Class41_Sub15(class296_sub17.g1(), this);
		if (i >= 19)
			aClass41_Sub10_5009 = new Class41_Sub10(class296_sub17.g1(), this);
		if (i >= 22)
			aClass41_Sub28_4994 = new Class41_Sub28(class296_sub17.g1(), this);
		if (i_3_ < 56)
			method3058(null, (byte) -32);
	}

	static final void method3053(byte i) {
		Class368_Sub5.gameLoopCache.clear();
		if (i != -56)
			method3059(-91);
		ObjectDefinitionLoader.gameTaskQueue.clear();
		aa_Sub1.aClass145_3719.clear();
	}

	final boolean method3054(int i) {
		if (i < 56)
			aClass41_Sub23_5017 = null;
		if (!aClass41_Sub30_5006.method527(-100) || aClass41_Sub30_5006.method521(120) != 0
				|| aClass30_5007.method327(false) >= 96)
			return false;
		return true;
	}

	final Class30 method3055(boolean bool) {
		if (bool != true)
			return null;
		return aClass30_5007;
	}

	private final void method3056(boolean bool, boolean bool_7_) {
		if (bool || aClass41_Sub21_5020 == null)
			aClass41_Sub21_5020 = new Class41_Sub21(this);
		if (bool || aClass41_Sub21_5015 == null)
			aClass41_Sub21_5015 = new Class41_Sub21(aClass41_Sub21_5020.method475(117), this);
		if (bool || aClass41_Sub27_5010 == null)
			aClass41_Sub27_5010 = new Class41_Sub27(this);
		if (bool || aClass41_Sub6_5014 == null)
			aClass41_Sub6_5014 = new Class41_Sub6(this);
		if (bool || aClass41_Sub4_4987 == null)
			aClass41_Sub4_4987 = new Class41_Sub4(this);
		if (bool || aClass41_Sub9_4999 == null)
			aClass41_Sub9_4999 = new Class41_Sub9(this);
		if (bool || aClass41_Sub1_5008 == null)
			aClass41_Sub1_5008 = new Class41_Sub1(this);
		if (bool || aClass41_Sub29_5002 == null)
			aClass41_Sub29_5002 = new Class41_Sub29(this);
		if (bool || aClass41_Sub22_4997 == null)
			aClass41_Sub22_4997 = new Class41_Sub22(this);
		if (bool || aClass41_Sub11_5023 == null)
			aClass41_Sub11_5023 = new Class41_Sub11(this);
		if (bool || aClass41_Sub7_5004 == null)
			aClass41_Sub7_5004 = new Class41_Sub7(this);
		if (bool || aClass41_Sub18_5012 == null)
			aClass41_Sub18_5012 = new Class41_Sub18(this);
		if (bool || aClass41_Sub17_5022 == null)
			aClass41_Sub17_5022 = new Class41_Sub17(this);
		if (bool || aClass41_Sub8_5027 == null)
			aClass41_Sub8_5027 = new Class41_Sub8(this);
		if (bool || aClass41_Sub14_5018 == null)
			aClass41_Sub14_5018 = new Class41_Sub14(this);
		if (bool || aClass41_Sub14_5029 == null)
			aClass41_Sub14_5029 = new Class41_Sub14(aClass41_Sub14_5018.method449(117), this);
		if (bool || aClass41_Sub10_5009 == null)
			aClass41_Sub10_5009 = new Class41_Sub10(this);
		if (bool || aClass41_Sub19_5011 == null)
			aClass41_Sub19_5011 = new Class41_Sub19(this);
		if (bool || aClass41_Sub16_5019 == null)
			aClass41_Sub16_5019 = new Class41_Sub16(this);
		if (bool || aClass41_Sub3_4993 == null)
			aClass41_Sub3_4993 = new Class41_Sub3(this);
		if (bool || aClass41_Sub26_4991 == null)
			aClass41_Sub26_4991 = new Class41_Sub26(this);
		if (bool || aClass41_Sub30_4988 == null)
			aClass41_Sub30_4988 = new Class41_Sub30(this);
		if (!bool_7_) {
			if (bool || aClass41_Sub30_5006 == null)
				aClass41_Sub30_5006 = new Class41_Sub30(aClass41_Sub30_4988.method521(122), this);
			if (bool || aClass41_Sub25_5001 == null)
				aClass41_Sub25_5001 = new Class41_Sub25(this);
			if (bool || aClass41_Sub25_5026 == null)
				aClass41_Sub25_5026 = new Class41_Sub25(aClass41_Sub25_5001.method496(123), this);
			if (bool || aClass41_Sub2_5013 == null)
				aClass41_Sub2_5013 = new Class41_Sub2(this);
			if (bool || aClass41_Sub13_5003 == null)
				aClass41_Sub13_5003 = new Class41_Sub13(this);
			if (bool || aClass41_Sub13_4989 == null)
				aClass41_Sub13_4989 = new Class41_Sub13(aClass41_Sub13_5003.method444(126), this);
			if (bool || aClass41_Sub20_4996 == null)
				aClass41_Sub20_4996 = new Class41_Sub20(this);
			if (bool || aClass41_Sub15_4990 == null)
				aClass41_Sub15_4990 = new Class41_Sub15(this);
			if (bool || aClass41_Sub12_4992 == null)
				aClass41_Sub12_4992 = new Class41_Sub12(this);
			if (bool || aClass41_Sub28_4994 == null)
				aClass41_Sub28_4994 = new Class41_Sub28(this);
			if (bool || aClass41_Sub5_5000 == null)
				aClass41_Sub5_5000 = new Class41_Sub5(this);
			if (bool || aClass41_Sub23_4995 == null)
				aClass41_Sub23_4995 = new Class41_Sub23(this);
			if (bool || aClass41_Sub23_5005 == null)
				aClass41_Sub23_5005 = new Class41_Sub23(this);
			if (bool || aClass41_Sub23_4998 == null)
				aClass41_Sub23_4998 = new Class41_Sub23(this);
			if (bool || aClass41_Sub23_5017 == null)
				aClass41_Sub23_5017 = new Class41_Sub23(this);
			if (bool || aClass41_Sub23_5021 == null)
				aClass41_Sub23_5021 = new Class41_Sub23(this);
			if (bool || aClass41_Sub24_5016 == null)
				aClass41_Sub24_5016 = new Class41_Sub24(this);
		}
	}

	final Packet method3057(int i) {
		Packet class296_sub17 = new Packet(Class215.method2026(71));
		class296_sub17.p1(i);
		class296_sub17.p1(aClass41_Sub21_5020.method475(119));
		class296_sub17.p1(aClass41_Sub27_5010.method505(i ^ 0x6c));
		class296_sub17.p1(aClass41_Sub6_5014.method416(123));
		class296_sub17.p1(aClass41_Sub4_4987.method402(i ^ 0x61));
		class296_sub17.p1(aClass41_Sub9_4999.method430(i + 92));
		class296_sub17.p1(aClass41_Sub1_5008.method389(124));
		class296_sub17.p1(aClass41_Sub29_5002.method515(123));
		class296_sub17.p1(aClass41_Sub22_4997.method484(124));
		class296_sub17.p1(aClass41_Sub11_5023.method437(126));
		class296_sub17.p1(aClass41_Sub7_5004.method420(125));
		class296_sub17.p1(aClass41_Sub18_5012.method465(123));
		class296_sub17.p1(aClass41_Sub17_5022.method456(121));
		class296_sub17.p1(aClass41_Sub8_5027.method426(118));
		class296_sub17.p1(aClass41_Sub14_5018.method449(i + 91));
		class296_sub17.p1(aClass41_Sub10_5009.method435(120));
		class296_sub17.p1(aClass41_Sub19_5011.method468(117));
		class296_sub17.p1(aClass41_Sub16_5019.method453(122));
		class296_sub17.p1(aClass41_Sub3_4993.method400(123));
		class296_sub17.p1(aClass41_Sub26_4991.method497(116));
		class296_sub17.p1(aClass41_Sub30_4988.method521(116));
		class296_sub17.p1(aClass41_Sub25_5001.method496(i + 95));
		class296_sub17.p1(aClass41_Sub2_5013.method393(115));
		class296_sub17.p1(aClass41_Sub13_5003.method444(122));
		class296_sub17.p1(aClass41_Sub20_4996.method470(124));
		class296_sub17.p1(aClass41_Sub15_4990.method452(123));
		class296_sub17.p1(aClass41_Sub12_4992.method440(i + 92));
		class296_sub17.p1(aClass41_Sub28_4994.method511(124));
		class296_sub17.p1(aClass41_Sub5_5000.method414(125));
		class296_sub17.p1(aClass41_Sub23_4995.method489(120));
		class296_sub17.p1(aClass41_Sub23_5005.method489(118));
		class296_sub17.p1(aClass41_Sub23_4998.method489(116));
		class296_sub17.p1(aClass41_Sub23_5017.method489(120));
		class296_sub17.p1(aClass41_Sub23_5021.method489(123));
		class296_sub17.p1(aClass41_Sub24_5016.method494(i + 98));
		return class296_sub17;
	}

	private final void method3058(Packet class296_sub17, byte i) {
		if (i <= 117)
			method3059(119);
		if (class296_sub17 == null || class296_sub17.data == null)
			method3056(true, false);
		else {
			int i_8_ = class296_sub17.g1();
			if (i_8_ < 23) {
				try {
					method3052(i_8_, class296_sub17, (byte) 103);
				} catch (Exception exception) {
					method3056(true, false);
				}
				method3056(false, false);
			} else if (i_8_ > 25)
				method3056(true, false);
			else {
				aClass41_Sub21_5020 = new Class41_Sub21(class296_sub17.g1(), this);
				aClass41_Sub21_5015 = new Class41_Sub21(aClass41_Sub21_5020.method475(123), this);
				aClass41_Sub27_5010 = new Class41_Sub27(class296_sub17.g1(), this);
				aClass41_Sub6_5014 = new Class41_Sub6(class296_sub17.g1(), this);
				aClass41_Sub4_4987 = new Class41_Sub4(class296_sub17.g1(), this);
				aClass41_Sub9_4999 = new Class41_Sub9(class296_sub17.g1(), this);
				aClass41_Sub1_5008 = new Class41_Sub1(class296_sub17.g1(), this);
				aClass41_Sub29_5002 = new Class41_Sub29(class296_sub17.g1(), this);
				aClass41_Sub22_4997 = new Class41_Sub22(class296_sub17.g1(), this);
				aClass41_Sub11_5023 = new Class41_Sub11(class296_sub17.g1(), this);
				aClass41_Sub7_5004 = new Class41_Sub7(class296_sub17.g1(), this);
				aClass41_Sub18_5012 = new Class41_Sub18(class296_sub17.g1(), this);
				if (i_8_ >= 24)
					aClass41_Sub17_5022 = new Class41_Sub17(class296_sub17.g1(), this);
				aClass41_Sub8_5027 = new Class41_Sub8(class296_sub17.g1(), this);
				aClass41_Sub14_5018 = new Class41_Sub14(class296_sub17.g1(), this);
				aClass41_Sub14_5029 = new Class41_Sub14(aClass41_Sub14_5018.method449(127), this);
				aClass41_Sub10_5009 = new Class41_Sub10(class296_sub17.g1(), this);
				if (i_8_ >= 25)
					aClass41_Sub19_5011 = new Class41_Sub19(class296_sub17.g1(), this);
				aClass41_Sub16_5019 = new Class41_Sub16(class296_sub17.g1(), this);
				aClass41_Sub3_4993 = new Class41_Sub3(class296_sub17.g1(), this);
				aClass41_Sub26_4991 = new Class41_Sub26(class296_sub17.g1(), this);
				aClass41_Sub30_4988 = new Class41_Sub30(class296_sub17.g1(), this);
				aClass41_Sub30_5006 = new Class41_Sub30(aClass41_Sub30_4988.method521(120), this);
				aClass41_Sub25_5001 = new Class41_Sub25(class296_sub17.g1(), this);
				aClass41_Sub25_5026 = new Class41_Sub25(aClass41_Sub25_5001.method496(117), this);
				aClass41_Sub2_5013 = new Class41_Sub2(class296_sub17.g1(), this);
				aClass41_Sub13_5003 = new Class41_Sub13(class296_sub17.g1(), this);
				aClass41_Sub13_4989 = new Class41_Sub13(aClass41_Sub13_5003.method444(119), this);
				aClass41_Sub20_4996 = new Class41_Sub20(class296_sub17.g1(), this);
				aClass41_Sub15_4990 = new Class41_Sub15(class296_sub17.g1(), this);
				aClass41_Sub12_4992 = new Class41_Sub12(class296_sub17.g1(), this);
				aClass41_Sub28_4994 = new Class41_Sub28(class296_sub17.g1(), this);
				aClass41_Sub5_5000 = new Class41_Sub5(class296_sub17.g1(), this);
				aClass41_Sub23_4995 = new Class41_Sub23(class296_sub17.g1(), this);
				aClass41_Sub23_5005 = new Class41_Sub23(class296_sub17.g1(), this);
				aClass41_Sub23_4998 = new Class41_Sub23(class296_sub17.g1(), this);
				aClass41_Sub23_5017 = new Class41_Sub23(class296_sub17.g1(), this);
				aClass41_Sub23_5021 = new Class41_Sub23(class296_sub17.g1(), this);
				aClass41_Sub24_5016 = new Class41_Sub24(class296_sub17.g1(), this);
				method3056(false, false);
			}
		}
		method3051((byte) 104);
	}

	public static void method3059(int i) {
		if (i >= 51) {
			game = null;
			aClass357_5028 = null;
		}
	}

	final void method3060(int i, int i_9_, Class41 class41) {
		if (i_9_ > 37) {
			class41.method385(i, false);
			method3051((byte) 108);
		}
	}

	Class296_Sub50(GameType class35, int i) {
		aClass35_5024 = class35;
		aClass30_5007 = new Class30(Class252.aClass398_2383.aBoolean3332, FileWorker.anInt3004,
				Class296_Sub30.anInt4820, (Class398.aString3327.toLowerCase().indexOf("arm") != -1));
		aClass41_Sub30_5006 = new Class41_Sub30(i, this);
		method3056(true, false);
	}

	Class296_Sub50(Packet class296_sub17, GameType class35, int i) {
		aClass35_5024 = class35;
		aClass30_5007 = new Class30(Class252.aClass398_2383.aBoolean3332, FileWorker.anInt3004,
				Class296_Sub30.anInt4820, Class398.aString3327.indexOf("arm") != -1);
		aClass41_Sub30_5006 = new Class41_Sub30(i, this);
		method3058(class296_sub17, (byte) 118);
	}
}
