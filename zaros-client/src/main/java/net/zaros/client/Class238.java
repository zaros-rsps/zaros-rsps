package net.zaros.client;

/* Class238 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class238 implements Interface11 {
	float[] aFloatArray3627;
	static String[] aStringArray3628;
	static Class186 aClass186_3629;
	static double aDouble3630;
	int anInt3631;
	int anInt3632;
	static long aLong3633 = 0L;

	static final void method2139(int i, int i_0_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_0_, i);
		class296_sub39_sub5.insertIntoQueue();
	}

	Class238(int i, int i_1_) {
		aFloatArray3627 = new float[i * i_1_];
		anInt3632 = i_1_;
		anInt3631 = i;
	}

	public static void method2140(boolean bool) {
		aClass186_3629 = null;
		aStringArray3628 = null;
		if (bool != true)
			method2140(true);
	}

	static {
		aStringArray3628 = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	}
}
