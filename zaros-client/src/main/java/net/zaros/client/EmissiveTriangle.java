package net.zaros.client;

import net.zaros.client.logic.clans.channel.ClanChannel;

public class EmissiveTriangle {
	private int anInt947;
	public int anInt948;
	public int anInt949;
	public int anInt950;
	public int anInt951;
	public static int anInt952 = 0;
	public int anInt953;
	public int anInt954;
	public byte aByte955;
	public EmissiveTriangle aClass89_956;
	public int anInt957;
	public static int anInt958;
	public static ClanChannel affinedClanChannel;
	public int anInt960;
	public int anInt961;
	public static volatile long aLong962 = 0L;
	public int anInt963;
	public int anInt964;
	public int anInt965;

	public  ParticleEmitterRaw method831(int i) {
		return InvisiblePlayer.method1932(-16734, anInt947);
	}

	public  EmissiveTriangle method832(int i, int i_0_, int i_1_, int i_2_) {
		if (i_1_ != 13007) {
			anInt960 = 5;
		}
		return new EmissiveTriangle(anInt947, i_0_, i_2_, i, aByte955);
	}

	public static void method833(int i) {
		affinedClanChannel = null;
		if (i != 0) {
			anInt958 = -73;
		}
	}

	public EmissiveTriangle(int i, int i_3_, int i_4_, int i_5_, byte i_6_) {
		anInt964 = i_5_;
		anInt947 = i;
		anInt954 = i_3_;
		anInt953 = i_4_;
		aByte955 = i_6_;
	}

	static {
		anInt958 = -1;
	}
}
