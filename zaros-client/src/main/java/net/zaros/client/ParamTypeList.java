package net.zaros.client;

public final class ParamTypeList {
	private AdvancedMemoryCache cachedDefinitions = new AdvancedMemoryCache(64);
	static Class256 aClass256_1694;
	private Js5 defsfs;

	public static void method1652(boolean bool) {
		aClass256_1694 = null;
		if (bool) {
			method1657((byte) -99, 26, -16);
		}
	}

	static final void method1653(byte i) {
		if (i != 116) {
			aClass256_1694 = null;
		}
		for (int i_0_ = 0; i_0_ < 5; i_0_++) {
			Class296_Sub51_Sub13.aBooleanArray6414[i_0_] = false;
		}
		Class368.anInt3130 = Class124.camPosZ;
		Class361.anInt3103 = 5;
		StaticMethods.anInt5949 = TranslatableString.camPosY;
		ClotchesLoader.anInt279 = -1;
		ClotchesLoader.anInt273 = Class29.anInt307;
		Class319.anInt3646 = Class296_Sub17_Sub2.camRotX;
		Class368_Sub5.anInt5454 = Class44_Sub1.camRotY;
		Class42_Sub3.anInt3844 = -1;
		Class366_Sub2.lookatVelocity = 0;
		Class322.anInt2826 = Class219.camPosX;
		StaticMethods.lookatSpeed = 0;
		Class368_Sub11.anInt5486 = Class242.anInt2309 = -1;
	}

	static final String method1654(String string, int i) {
		if (Class5.aString81.startsWith("win")) {
			return string + ".dll";
		}
		if (!Class5.aString81.startsWith("linux")) {
			if (Class5.aString81.startsWith("mac")) {
				return "lib" + string + ".dylib";
			}
		} else {
			return "lib" + string + ".so";
		}
		if (i < 110) {
			method1654(null, 83);
		}
		return null;
	}

	final void method1655(byte i) {
		synchronized (cachedDefinitions) {
			if (i != -100) {
				method1659(36);
			}
			cachedDefinitions.clearSoftReferences();
		}
	}

	public ParamType list(int id) {
		ParamType def;
		synchronized (cachedDefinitions) {
			def = (ParamType) cachedDefinitions.get(id);
		}
		if (def != null) {
			return def;
		}
		byte[] data;
		synchronized (defsfs) {
			data = defsfs.getFile(11, id);
		}
		def = new ParamType();
		if (data != null) {
			def.init(new Packet(data), true);
		}
		synchronized (cachedDefinitions) {
			cachedDefinitions.put(def, id);
		}
		return def;
	}

	static final boolean method1657(byte i, int i_2_, int i_3_) {
		int i_4_ = -23 % ((i - 48) / 61);
		if ((i_3_ & 0x10000) == 0) {
			return false;
		}
		return true;
	}

	final void method1658(byte i, int i_5_) {
		synchronized (cachedDefinitions) {
			cachedDefinitions.clean(i_5_);
		}
		if (i <= 113) {
			aClass256_1694 = null;
		}
	}

	final void method1659(int i) {
		synchronized (cachedDefinitions) {
			if (i != 16421) {
				/* empty */
			} else {
				cachedDefinitions.clear();
			}
		}
	}

	ParamTypeList(GameType class35, int i, Js5 class138) {
		defsfs = class138;
		if (defsfs != null) {
			defsfs.getLastFileId(11);
		}
	}
}
