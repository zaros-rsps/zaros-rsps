package net.zaros.client;

/* Class248 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class ModeWhere {
	static short aShort2350 = 256;
	static boolean aBoolean2351 = false;
	static int anInt2352;
	int modeID;
	static ModeWhere officeModeWhere = new ModeWhere("WTRC", "office", "_rc", 1);
	static boolean aBoolean2354;

	static final int method2191(int i) {
		int i_0_ = -1;
		for (int i_1_ = 0; i_1_ < Class247.anInt2336 - 1; i_1_++) {
			if (i < (Class379_Sub2_Sub1.anIntArray6605[i_1_] + Class377.anIntArray3192[i_1_])) {
				i_0_ = i_1_;
				break;
			}
		}
		if (i_0_ == -1)
			i_0_ = Class247.anInt2336 - 1;
		return i_0_;
	}

	static final boolean method2192(int i) {
		try {
			if (ReferenceWrapper.anInt6128 == 2) {
				if (SubInPacket.aClass296_Sub47_2421 == null) {
					SubInPacket.aClass296_Sub47_2421 = MidiDecoder.method3042((TextureOperation.aClass138_5040), (Class296_Sub51_Sub7.anInt6371), Class368_Sub1.anInt5423);
					if (SubInPacket.aClass296_Sub47_2421 == null)
						return false;
				}
				if (za.aClass25_5085 == null)
					za.aClass25_5085 = new Class25(Class88.aClass138_944, Class375.aClass138_3182);
				Class296_Sub45_Sub4 class296_sub45_sub4 = Class235.aClass296_Sub45_Sub4_2229;
				if (Class249.aClass296_Sub45_Sub4_2357 != null)
					class296_sub45_sub4 = Class249.aClass296_Sub45_Sub4_2357;
				if (class296_sub45_sub4.method3008(Class286.aClass138_2639, (SubInPacket.aClass296_Sub47_2421), za.aClass25_5085, false, 22050)) {
					Class235.aClass296_Sub45_Sub4_2229 = class296_sub45_sub4;
					Class235.aClass296_Sub45_Sub4_2229.method3013((byte) -76);
					if (Class287.anInt2644 > 0) {
						ReferenceWrapper.anInt6128 = 3;
						Class235.aClass296_Sub45_Sub4_2229.method3019((Class392.anInt3488 < Class287.anInt2644 ? Class392.anInt3488 : Class287.anInt2644), i ^ 0x6c69);
						for (int i_2_ = 0; Class41_Sub13.anIntArray3772.length > i_2_; i_2_++) {
							Class235.aClass296_Sub45_Sub4_2229.method3022(Class41_Sub13.anIntArray3772[i_2_], 0, i_2_);
							Class41_Sub13.anIntArray3772[i_2_] = 255;
						}
					} else {
						ReferenceWrapper.anInt6128 = 0;
						Class235.aClass296_Sub45_Sub4_2229.method3019(Class392.anInt3488, 96);
						for (int i_3_ = 0; Class41_Sub13.anIntArray3772.length > i_3_; i_3_++) {
							Class235.aClass296_Sub45_Sub4_2229.method3022(Class41_Sub13.anIntArray3772[i_3_], 0, i_3_);
							Class41_Sub13.anIntArray3772[i_3_] = 255;
						}
					}
					if (Class249.aClass296_Sub45_Sub4_2357 == null) {
						if (Class205_Sub4.aLong5635 > 0L)
							Class235.aClass296_Sub45_Sub4_2229.method3000(SubInPacket.aClass296_Sub47_2421, -84, Class296_Sub50.aBoolean5025, Class205_Sub4.aLong5635, true);
						else
							Class235.aClass296_Sub45_Sub4_2229.method3014(SubInPacket.aClass296_Sub47_2421, Class296_Sub50.aBoolean5025, true);
					}
					if (Class296_Sub39_Sub9.aClass381_6163 != null)
						Class296_Sub39_Sub9.aClass381_6163.method3996(107, Class235.aClass296_Sub45_Sub4_2229);
					Class249.aClass296_Sub45_Sub4_2357 = null;
					za.aClass25_5085 = null;
					SubInPacket.aClass296_Sub47_2421 = null;
					TextureOperation.aClass138_5040 = null;
					Class205_Sub4.aLong5635 = 0L;
					return true;
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			Class235.aClass296_Sub45_Sub4_2229.method3001(-532329720);
			Class249.aClass296_Sub45_Sub4_2357 = null;
			SubInPacket.aClass296_Sub47_2421 = null;
			ReferenceWrapper.anInt6128 = 0;
			za.aClass25_5085 = null;
			TextureOperation.aClass138_5040 = null;
		}
		if (i != 27651)
			method2191(-9);
		return false;
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	ModeWhere(String string, String string_4_, String string_5_, int i) {
		modeID = i;
	}
}
