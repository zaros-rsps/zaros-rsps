package net.zaros.client;

/* Class32 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

class Class32 implements Interface9 {
	static int[] pathBufferX = new int[50];
	private Js5 aClass138_3540;
	Sprite aClass397_3541;
	Class107 aClass107_3542;

	static final void method340(int i) {
		if (i != 19055)
			pathBufferX = null;
		Class296_Sub39_Sub13.aClass113_6205.clear();
		Class338_Sub3_Sub1_Sub4.aClass113_6641.clear();
		Class153.aClass113_1576.clear();
		Class41_Sub20.aClass113_3794.clear();
	}

	public final void method42(byte i) {
		if (i != 88)
			aClass107_3542 = null;
		aClass397_3541 = CS2Script.method2798(aClass138_3540, aClass107_3542.anInt3576, -3);
	}

	public final boolean method44(byte i) {
		if (i < 91)
			method341(80);
		return aClass138_3540.hasEntryBuffer(aClass107_3542.anInt3576);
	}

	public static void method341(int i) {
		pathBufferX = null;
		if (i != 0)
			method343(-38, 84, null);
	}

	static final void method342(int i, int i_0_, int i_1_, byte i_2_, int i_3_, int i_4_) {
		int i_5_ = -i_4_ + i_0_;
		int i_6_ = -i_3_ + i;
		if (i_6_ != 0) {
			if (i_5_ == 0) {
				Class241_Sub2_Sub2.method2162(i_4_, i, i_3_, (byte) -118, i_1_);
				return;
			}
		} else {
			if (i_5_ != 0)
				Class140.method1467(i_0_, i_1_, i_3_, false, i_4_);
			return;
		}
		if (i_6_ < 0)
			i_6_ = -i_6_;
		if (i_5_ < 0)
			i_5_ = -i_5_;
		boolean bool = i_6_ < i_5_;
		if (bool) {
			int i_7_ = i_3_;
			i_3_ = i_4_;
			int i_8_ = i;
			i_4_ = i_7_;
			i = i_0_;
			i_0_ = i_8_;
		}
		if (i_3_ > i) {
			int i_9_ = i_3_;
			int i_10_ = i_4_;
			i_3_ = i;
			i = i_9_;
			i_4_ = i_0_;
			i_0_ = i_10_;
		}
		int i_11_ = i_4_;
		int i_12_ = -i_3_ + i;
		int i_13_ = i_0_ - i_4_;
		int i_14_ = -(i_12_ >> 1);
		int i_15_ = i_0_ <= i_4_ ? -1 : 1;
		if (i_13_ < 0)
			i_13_ = -i_13_;
		if (!bool) {
			for (int i_16_ = i_3_; i >= i_16_; i_16_++) {
				Class296_Sub51_Sub37.anIntArrayArray6536[i_11_][i_16_] = i_1_;
				i_14_ += i_13_;
				if (i_14_ > 0) {
					i_14_ -= i_12_;
					i_11_ += i_15_;
				}
			}
		} else {
			for (int i_17_ = i_3_; i >= i_17_; i_17_++) {
				i_14_ += i_13_;
				Class296_Sub51_Sub37.anIntArrayArray6536[i_17_][i_11_] = i_1_;
				if (i_14_ > 0) {
					i_14_ -= i_12_;
					i_11_ += i_15_;
				}
			}
		}
		if (i_2_ != -119)
			pathBufferX = null;
	}

	public void method43(boolean bool, int i) {
		int i_18_ = -83 / ((i + 32) / 52);
		if (bool) {
			int i_19_ = (aClass107_3542.aClass252_3575.method2204(aClass397_3541.method4099(), 23236, (Class368_Sub7.anInt5463)) + aClass107_3542.anInt3570);
			int i_20_ = ((aClass107_3542.aClass357_3571.method3711(Class296_Sub15_Sub1.anInt5996, aClass397_3541.method4088(), (byte) 109)) + aClass107_3542.anInt3577);
			aClass397_3541.method4096(i_19_, i_20_);
		}
	}

	static final int method343(int i, int i_21_, String string) {
		if (i != -1)
			pathBufferX = null;
		return Class18.method277(i ^ 0x3a, string, i_21_, true);
	}

	static final boolean method344(byte i) {
		if (NodeDeque.js) {
			try {
				if (((Boolean) Class297.method3231("showingVideoAd", CS2Script.anApplet6140, false)).booleanValue())
					return false;
				return true;
			} catch (Throwable throwable) {
				/* empty */
			}
		}
		int i_22_ = 6 % ((19 - i) / 50);
		return true;
	}

	Class32(Js5 class138, Class107 class107) {
		aClass107_3542 = class107;
		aClass138_3540 = class138;
	}
}
