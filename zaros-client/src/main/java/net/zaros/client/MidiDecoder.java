package net.zaros.client;

/* Class296_Sub47 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class MidiDecoder extends Node {
	byte[] buffer;
	HashTable aClass263_4973;

	public void method3040() {
		if (aClass263_4973 == null) {
			aClass263_4973 = new HashTable(16);
			int[] is = new int[16];
			int[] is_0_ = new int[16];
			is[9] = is_0_[9] = 128;
			MidiEvent class109 = new MidiEvent(buffer);
			int i = class109.getTracksCount();
			for (int i_1_ = 0; i_1_ < i; i_1_++) {
				class109.loadTrackBuffer(i_1_);
				class109.readTickDuration(i_1_);
				class109.saveTrackBuffer(i_1_);
			}
			while_255_: for (;;) {
				int i_2_ = class109.getFastestTrack();
				int i_3_ = class109.tick_durations[i_2_];
				while (class109.tick_durations[i_2_] == i_3_) {
					class109.loadTrackBuffer(i_2_);
					int i_4_ = class109.method965(i_2_);
					if (i_4_ == 1) {
						class109.discardTrackBuffer();
						class109.saveTrackBuffer(i_2_);
						if (!class109.isFinished()) {
							break;
						}
						break while_255_;
					}
					int i_5_ = i_4_ & 0xf0;
					if (i_5_ == 176) {
						int i_6_ = i_4_ & 0xf;
						int i_7_ = i_4_ >> 8 & 0x7f;
						int i_8_ = i_4_ >> 16 & 0x7f;
						if (i_7_ == 0) {
							is[i_6_] = (is[i_6_] & ~0x1fc000) + (i_8_ << 14);
						}
						if (i_7_ == 32) {
							is[i_6_] = (is[i_6_] & ~0x3f80) + (i_8_ << 7);
						}
					}
					if (i_5_ == 192) {
						int i_9_ = i_4_ & 0xf;
						int i_10_ = i_4_ >> 8 & 0x7f;
						is_0_[i_9_] = is[i_9_] + i_10_;
					}
					if (i_5_ == 144) {
						int i_11_ = i_4_ & 0xf;
						int i_12_ = i_4_ >> 8 & 0x7f;
						int i_13_ = i_4_ >> 16 & 0x7f;
						if (i_13_ > 0) {
							int i_14_ = is_0_[i_11_];
							Class296_Sub25 class296_sub25 = (Class296_Sub25) aClass263_4973.get(i_14_);
							if (class296_sub25 == null) {
								class296_sub25 = new Class296_Sub25(new byte[128]);
								aClass263_4973.put(i_14_, class296_sub25);
							}
							class296_sub25.aByteArray4763[i_12_] = (byte) 1;
						}
					}
					class109.readTickDuration(i_2_);
					class109.saveTrackBuffer(i_2_);
				}
			}
		}
	}

	final void method3041() {
		aClass263_4973 = null;
	}

	static final MidiDecoder method3042(Js5 class138, int i, int i_15_) {
		byte[] is = class138.getFile(i, i_15_);
		if (is == null) {
			return null;
		}
		return new MidiDecoder(new Packet(is));
	}

	private MidiDecoder(Packet packet) {
		packet.pos = packet.data.length - 3;
		int num_tracks = packet.g1();
		int timeFormat = packet.g2();
		int size = 14 + num_tracks * 10;
		packet.pos = 0;
		int tempo_size = 0;
		int control_change_size = 0;
		int notes_on_size = 0;
		int notes_off_size = 0;
		int pitch_bend_size = 0;
		int channel_pressure_size = 0;
		int poly_pressure_size = 0;
		int program_change_size = 0;
		track_loop: for (int track = 0; track < num_tracks; track++) {
			int jag_command = -1;
			for (;;) {
				int lsb = packet.g1();
				if (lsb != jag_command) {
					size++;
				}
				jag_command = lsb & 0xf;
				if (lsb == 7) {
					continue track_loop;
				}
				if (lsb == 23) {
					tempo_size++;
				} else if (jag_command == 0) {
					notes_on_size++;
				} else if (jag_command == 1) {
					notes_off_size++;
				} else if (jag_command == 2) {
					control_change_size++;
				} else if (jag_command == 3) {
					pitch_bend_size++;
				} else if (jag_command == 4) {
					channel_pressure_size++;
				} else if (jag_command == 5) {
					poly_pressure_size++;
				} else {
					if (jag_command != 6) {
						break;
					}
					program_change_size++;
				}
			}
			throw new RuntimeException();
		}
		size += tempo_size * 5;
		size += (notes_on_size + notes_off_size + control_change_size + pitch_bend_size + poly_pressure_size) * 2;
		size += channel_pressure_size + program_change_size;
		int i_13_ = packet.pos;
		int i_14_ = num_tracks + tempo_size + control_change_size + notes_on_size + notes_off_size + pitch_bend_size + channel_pressure_size + poly_pressure_size + program_change_size;
		for (int i_31_ = 0; i_31_ < i_14_; i_31_++) {
			packet.gvarint();
		}
		size += packet.pos - i_13_;
		int programs_offset = packet.pos;
		int modulation_wheel_size = 0;
		int modulation_wheel_lsb_size = 0;
		int main_volume_size = 0;
		int main_volume_lsb_size = 0;
		int pan_size = 0;
		int pan_lsb_size = 0;
		int nr_param_msb_size = 0;
		int nr_param_lsb_size = 0;
		int r_param_msb_size = 0;
		int r_param_lsb_size = 0;
		int channel_commands_size = 0;
		int other_size = 0;
		int control = 0;
		for (int i_30_ = 0; i_30_ < control_change_size; i_30_++) {
			control = control + packet.g1() & 0x7f;
			if (control == 0 || control == 32) {
				program_change_size++;
			} else if (control == 1) {
				modulation_wheel_size++;
			} else if (control == 33) {
				modulation_wheel_lsb_size++;
			} else if (control == 7) {
				main_volume_size++;
			} else if (control == 39) {
				main_volume_lsb_size++;
			} else if (control == 10) {
				pan_size++;
			} else if (control == 42) {
				pan_lsb_size++;
			} else if (control == 99) {
				nr_param_msb_size++;
			} else if (control == 98) {
				nr_param_lsb_size++;
			} else if (control == 101) {
				r_param_msb_size++;
			} else if (control == 100) {
				r_param_lsb_size++;
			} else if (control == 64 || control == 65 || control == 120 || control == 121 || control == 123) {
				channel_commands_size++;
			} else {
				other_size++;
			}
		}
		int data_pos = 0;
		int channel_commands_offset = packet.pos;
		packet.pos += channel_commands_size;
		int poly_pressure_offset = packet.pos;
		packet.pos += poly_pressure_size;
		int channel_pressure_offset = packet.pos;
		packet.pos += channel_pressure_size;
		int pitch_bend_lsb_offset = packet.pos;
		packet.pos += pitch_bend_size;
		int modulation_wheel_offset = packet.pos;
		packet.pos += modulation_wheel_size;
		int main_volume_offset = packet.pos;
		packet.pos += main_volume_size;
		int pan_offset = packet.pos;
		packet.pos += pan_size;
		int notes_value_offset = packet.pos;
		packet.pos += notes_on_size + notes_off_size + poly_pressure_size;
		int notes_on_velocity_offset = packet.pos;
		packet.pos += notes_on_size;
		int other_offset = packet.pos;
		packet.pos += other_size;
		int notes_off_velocity_offset = packet.pos;
		packet.pos += notes_off_size;
		int modulation_wheel_lsb_offset = packet.pos;
		packet.pos += modulation_wheel_lsb_size;
		int main_volume_lsb_offset = packet.pos;
		packet.pos += main_volume_lsb_size;
		int pan_lsb_offset = packet.pos;
		packet.pos += pan_lsb_size;
		int bank_select_offset = packet.pos;
		packet.pos += program_change_size;
		int pitch_bend_msb_offset = packet.pos;
		packet.pos += pitch_bend_size;
		int nr_param_msb_offset = packet.pos;
		packet.pos += nr_param_msb_size;
		int nr_param_lsb_offset = packet.pos;
		packet.pos += nr_param_lsb_size;
		int r_param_msb_offset = packet.pos;
		packet.pos += r_param_msb_size;
		int r_param_lsb_offset = packet.pos;
		packet.pos += r_param_lsb_size;
		int temp_offset = packet.pos;
		packet.pos += tempo_size;
		buffer = new byte[size];
		Packet out = new Packet(buffer);
		out.p4(1297377380);
		out.p4(6);
		out.p2(num_tracks > 1 ? 1 : 0);
		out.p2(num_tracks);
		out.p2(timeFormat);
		packet.pos = i_13_;
		int lsb = 0;
		int note = 0;
		int start_velocity = 0;
		int end_velocity = 0;
		int pitch = 0;
		int val6 = 0;
		int pressure = 0;
		int[] controls = new int[128];
		control = 0;
		for (int i_77_ = 0; i_77_ < num_tracks; i_77_++) {
			out.p4(1297379947);
			out.pos += 4;
			int bytesWrittenOffset = out.pos;
			int runningStatus = -1;
			messages_loop: do {
				for (;;) {
					int deltaTick = packet.gvarint();
					out.pvarint(deltaTick);
					int status = packet.data[data_pos++] & 0xff;
					boolean statusChanged = status != runningStatus;
					runningStatus = status & 0xf;
					if (status == 7) {
						if (statusChanged) {
							out.p1(255);
						}
						out.p1(47);
						out.p1(0);
						break messages_loop;
					}
					if (status == 23) {
						if (statusChanged) {
							out.p1(255);
						}
						out.p1(81);
						out.p1(3);
						out.p1(packet.data[temp_offset++]);
						out.p1(packet.data[temp_offset++]);
						out.p1(packet.data[temp_offset++]);
					} else {
						lsb ^= status >> 4;
						if (runningStatus == 0) {
							if (statusChanged) {
								out.p1(lsb + 144);
							}
							note += packet.data[notes_value_offset++];
							start_velocity += packet.data[notes_on_velocity_offset++];
							out.p1(note & 0x7f);
							out.p1(start_velocity & 0x7f);
						} else if (runningStatus == 1) {
							if (statusChanged) {
								out.p1(lsb + 128);
							}
							note += packet.data[notes_value_offset++];
							end_velocity += packet.data[notes_off_velocity_offset++];
							out.p1(note & 0x7f);
							out.p1(end_velocity & 0x7f);
						} else if (runningStatus == 2) {
							if (statusChanged) {
								out.p1(lsb + 176);
							}
							control = control + packet.data[programs_offset++] & 0x7f;
							out.p1(control);
							int value;
							if (control == 0 || control == 32) {
								value = packet.data[bank_select_offset++];
							} else if (control == 1) {
								value = packet.data[modulation_wheel_offset++];
							} else if (control == 33) {
								value = packet.data[modulation_wheel_lsb_offset++];
							} else if (control == 7) {
								value = packet.data[main_volume_offset++];
							} else if (control == 39) {
								value = packet.data[main_volume_lsb_offset++];
							} else if (control == 10) {
								value = packet.data[pan_offset++];
							} else if (control == 42) {
								value = packet.data[pan_lsb_offset++];
							} else if (control == 99) {
								value = packet.data[nr_param_msb_offset++];
							} else if (control == 98) {
								value = packet.data[nr_param_lsb_offset++];
							} else if (control == 101) {
								value = packet.data[r_param_msb_offset++];
							} else if (control == 100) {
								value = packet.data[r_param_lsb_offset++];
							} else if (control == 64 || control == 65 || control == 120 || control == 121 || control == 123) {
								value = packet.data[channel_commands_offset++];
							} else {
								value = packet.data[other_offset++];
							}
							value += controls[control];
							controls[control] = value;
							out.p1(value & 0x7f);
						} else if (runningStatus == 3) {
							if (statusChanged) {
								out.p1(lsb + 224);
							}
							pitch += packet.data[pitch_bend_msb_offset++];
							pitch += packet.data[pitch_bend_lsb_offset++] << 7;
							out.p1(pitch & 0x7f);
							out.p1(pitch >> 7 & 0x7f);
						} else if (runningStatus == 4) {
							if (statusChanged) {
								out.p1(lsb + 208);
							}
							val6 += packet.data[channel_pressure_offset++];
							out.p1(val6 & 0x7f);
						} else if (runningStatus == 5) {
							if (statusChanged) {
								out.p1(lsb + 160);
							}
							note += packet.data[notes_value_offset++];
							pressure += packet.data[poly_pressure_offset++];
							out.p1(note & 0x7f);
							out.p1(pressure & 0x7f);
						} else {
							if (runningStatus != 6) {
								break;
							}
							if (statusChanged) {
								out.p1(lsb + 192);
							}
							out.p1(packet.data[bank_select_offset++]);
						}
					}
				}
				throw new RuntimeException();
			} while (false);
			out.psize4(out.pos - bytesWrittenOffset);
		}
	}
}
