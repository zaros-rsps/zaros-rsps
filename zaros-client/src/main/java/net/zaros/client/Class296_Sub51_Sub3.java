package net.zaros.client;

/* Class296_Sub51_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.lang.reflect.Method;
import java.util.Random;

import jaggl.OpenGL;

final class Class296_Sub51_Sub3 extends TextureOperation {
	static int anInt6343;
	private int anInt6344;
	private byte[] aByteArray6345 = new byte[512];
	private int anInt6346;
	static int anInt6347 = -1;
	private int anInt6348;
	private int anInt6349;
	private short[] aShortArray6350;
	private int anInt6351;
	private int anInt6352;

	public Class296_Sub51_Sub3() {
		super(0, true);
		anInt6344 = 1;
		anInt6346 = 5;
		anInt6349 = 5;
		anInt6348 = 2;
		aShortArray6350 = new short[512];
		anInt6351 = 0;
		anInt6352 = 2048;
	}

	final int[] get_monochrome_output(int i, int i_0_) {
		int[] is = aClass318_5035.method3335(i_0_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int i_1_ = 2048 + anInt6349 * Class294.anIntArray2686[i_0_];
			int i_2_ = i_1_ >> 12;
			int i_3_ = i_2_ + 1;
			int i_4_ = 0;
			while_58_: for (/**/; Class41_Sub10.anInt3769 > i_4_; i_4_++) {
				Class52.anInt634 = Class107.anInt3572 = Class268.anInt2489 = Class252.anInt2385 = 2147483647;
				int i_5_ = 2048 + Class33.anIntArray334[i_4_] * anInt6346;
				int i_6_ = i_5_ >> 12;
				int i_7_ = i_6_ + 1;
				for (int i_8_ = i_2_ - 1; i_8_ <= i_3_; i_8_++) {
					int i_9_ = (aByteArray6345[(i_8_ >= anInt6349 ? -anInt6349 + i_8_ : i_8_) & 0xff] & 0xff);
					for (int i_10_ = i_6_ - 1; i_10_ <= i_7_; i_10_++) {
						int i_11_ = ((aByteArray6345[i_9_ + (i_10_ >= anInt6346 ? -anInt6346 + i_10_ : i_10_) & 0xff]
								& 0xff) * 2);
						int i_12_ = i_5_ - aShortArray6350[i_11_++] - (i_10_ << 12);
						int i_13_ = i_1_ - (i_8_ << 12) - aShortArray6350[i_11_];
						int i_14_ = anInt6344;
						int i_15_;
						while_54_: do {
							while_53_: do {
								while_52_: do {
									while_51_: do {
										do {
											if (i_14_ != 1) {
												if (i_14_ != 3) {
													if (i_14_ != 4) {
														if (i_14_ != 5) {
															if (i_14_ == 2)
																break while_52_;
															break while_53_;
														}
													} else
														break;
													break while_51_;
												}
											} else {
												i_15_ = (i_13_ * i_13_ + i_12_ * i_12_) >> 12;
												break while_54_;
											}
											i_12_ = i_12_ < 0 ? -i_12_ : i_12_;
											i_13_ = i_13_ < 0 ? -i_13_ : i_13_;
											i_15_ = (i_12_ > i_13_ ? i_12_ : i_13_);
											break while_54_;
										} while (false);
										i_12_ = (int) ((Math
												.sqrt((double) ((float) ((i_12_ >= 0) ? i_12_ : -i_12_) / 4096.0F)))
												* 4096.0);
										i_13_ = (int) ((Math
												.sqrt((double) ((float) ((i_13_ >= 0) ? i_13_ : -i_13_) / 4096.0F)))
												* 4096.0);
										i_15_ = i_12_ + i_13_;
										i_15_ = i_15_ * i_15_ >> 12;
										break while_54_;
									} while (false);
									i_12_ *= i_12_;
									i_13_ *= i_13_;
									i_15_ = (int) ((Math
											.sqrt(Math.sqrt((double) ((float) (i_12_ + i_13_) / 1.6777216E7F))))
											* 4096.0);
									break while_54_;
								} while (false);
								i_15_ = ((i_13_ >= 0 ? i_13_ : -i_13_) + (i_12_ >= 0 ? i_12_ : -i_12_));
								break while_54_;
							} while (false);
							i_15_ = (int) ((Math
									.sqrt((double) ((float) (i_13_ * i_13_ + i_12_ * i_12_) / 1.6777216E7F))) * 4096.0);
						} while (false);
						if (Class52.anInt634 <= i_15_) {
							if (i_15_ < Class107.anInt3572) {
								Class252.anInt2385 = Class268.anInt2489;
								Class268.anInt2489 = Class107.anInt3572;
								Class107.anInt3572 = i_15_;
							} else if (i_15_ < Class268.anInt2489) {
								Class252.anInt2385 = Class268.anInt2489;
								Class268.anInt2489 = i_15_;
							} else if (i_15_ < Class252.anInt2385)
								Class252.anInt2385 = i_15_;
						} else {
							Class252.anInt2385 = Class268.anInt2489;
							Class268.anInt2489 = Class107.anInt3572;
							Class107.anInt3572 = Class52.anInt634;
							Class52.anInt634 = i_15_;
						}
					}
				}
				int i_16_ = anInt6348;
				while_56_: do {
					while_55_: do {
						do {
							if (i_16_ != 0) {
								if (i_16_ != 1) {
									if (i_16_ != 3) {
										if (i_16_ != 4) {
											if (i_16_ == 2)
												break while_56_;
											continue while_58_;
										}
									} else
										break;
									break while_55_;
								}
							} else {
								is[i_4_] = Class52.anInt634;
								continue while_58_;
							}
							is[i_4_] = Class107.anInt3572;
							continue while_58_;
						} while (false);
						is[i_4_] = Class268.anInt2489;
						continue while_58_;
					} while (false);
					is[i_4_] = Class252.anInt2385;
					continue while_58_;
				} while (false);
				is[i_4_] = Class107.anInt3572 - Class52.anInt634;
			}
		}
		if (i != 0)
			method3071(-71, null, 84);
		return is;
	}

	final void method3071(int i, Packet class296_sub17, int i_17_) {
		if (i >= -84)
			anInt6351 = -125;
		int i_18_ = i_17_;
		while_63_: do {
			while_62_: do {
				while_61_: do {
					while_60_: do {
						while_59_: do {
							do {
								if (i_18_ != 0) {
									if (i_18_ != 1) {
										if (i_18_ != 2) {
											if (i_18_ != 3) {
												if (i_18_ != 4) {
													if (i_18_ != 5) {
														if (i_18_ == 6)
															break while_62_;
														break while_63_;
													}
												} else
													break while_60_;
												break while_61_;
											}
										} else
											break;
										break while_59_;
									}
								} else {
									anInt6346 = anInt6349 = class296_sub17.g1();
									return;
								}
								anInt6351 = class296_sub17.g1();
								return;
							} while (false);
							anInt6352 = class296_sub17.g2();
							return;
						} while (false);
						anInt6348 = class296_sub17.g1();
						return;
					} while (false);
					anInt6344 = class296_sub17.g1();
					return;
				} while (false);
				anInt6346 = class296_sub17.g1();
				return;
			} while (false);
			anInt6349 = class296_sub17.g1();
			break;
		} while (false);
	}

	private final void method3082(int i) {
		if (i >= -122)
			method3076((byte) 33);
		Random random = new Random((long) anInt6351);
		aShortArray6350 = new short[512];
		if (anInt6352 > 0) {
			for (int i_19_ = 0; i_19_ < 512; i_19_++)
				aShortArray6350[i_19_] = (short) s_Sub3.method3373(anInt6352, random, 6445);
		}
	}

	final void method3076(byte i) {
		aByteArray6345 = GameClient.method117(true, anInt6351);
		method3082(-128);
		int i_20_ = 65 / ((i + 58) / 40);
	}

	static final void method3083(byte i) {
		Class296_Sub30.anInt4820 = Runtime.getRuntime().availableProcessors();
	}

	static final Class77 method3084(int i, ha_Sub3 var_ha_Sub3, String string, int i_22_) {
		long l = OpenGL.glCreateShaderObjectARB(i_22_);
		OpenGL.glShaderSourceARB(l, string);
		OpenGL.glCompileShaderARB(l);
		OpenGL.glGetObjectParameterivARB(l, 35713, r_Sub2.anIntArray6707, 0);
		if (r_Sub2.anIntArray6707[0] == 0) {
			if (r_Sub2.anIntArray6707[0] == 0)
				System.out.println("Shader compile failed:");
			OpenGL.glGetObjectParameterivARB(l, 35716, r_Sub2.anIntArray6707, 1);
			if (r_Sub2.anIntArray6707[1] > 1) {
				byte[] is = new byte[r_Sub2.anIntArray6707[1]];
				OpenGL.glGetInfoLogARB(l, r_Sub2.anIntArray6707[1], r_Sub2.anIntArray6707, 0, is, 0);
				System.out.println(new String(is));
			}
			if (r_Sub2.anIntArray6707[0] == 0) {
				OpenGL.glDeleteObjectARB(l);
				return null;
			}
		}
		if (i != 19399)
			anInt6347 = -62;
		return new Class77(var_ha_Sub3, l, i_22_);
	}
}
