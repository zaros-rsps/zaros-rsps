package net.zaros.client;

/* Class53 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class53 {
	static int lookatX;

	static final void method641(int i) {
		if (Class338_Sub6.anInterface2Array5237 != null) {
			Interface2[] interface2s = Class338_Sub6.anInterface2Array5237;
			for (int i_0_ = 0; interface2s.length > i_0_; i_0_++) {
				Interface2 interface2 = interface2s[i_0_];
				interface2.method10((byte) -97);
			}
		}
		if (i != -91)
			method642(null, -93, -39, 55, -51, 102, 7, true, false);
	}

	static final void method642(ha var_ha, int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, boolean bool, boolean bool_6_) {
		Class16_Sub2.aHa3733 = var_ha;
		Class377.anInt3190 = i;
		Class338_Sub3_Sub2.aBoolean6566 = Class377.anInt3190 > 1 && Class16_Sub2.aHa3733.r();
		Class313.anInt2779 = i_1_;
		Js5TextureLoader.anInt3440 = 1 << Class313.anInt2779;
		Class304.anInt2737 = Js5TextureLoader.anInt3440 >> 1;
		Math.sqrt((double) (Class304.anInt2737 * Class304.anInt2737 + Class304.anInt2737 * Class304.anInt2737));
		Class368_Sub9.anInt5477 = i_2_;
		Class228.anInt2201 = i_3_;
		Class368_Sub12.anInt5488 = i_4_;
		Class379_Sub2.anInt5684 = i_5_;
		BITConfigsLoader.aClass327_2205 = Class41_Sub27.method510((byte) 4);
		Class296_Sub39_Sub12.method2844(0);
		Class399.aClass247ArrayArrayArray3355 = new Class247[i_2_][Class228.anInt2201][Class368_Sub12.anInt5488];
		Class244.aSArray2320 = new s[i_2_];
		if (bool) {
			Class296_Sub51_Sub32.anIntArrayArray6514 = new int[Class228.anInt2201][Class368_Sub12.anInt5488];
			Mobile.aByteArrayArray6776 = new byte[Class228.anInt2201][Class368_Sub12.anInt5488];
			StaticMethods.aShortArrayArray5919 = new short[Class228.anInt2201][Class368_Sub12.anInt5488];
			Class351.aClass247ArrayArrayArray3041 = (new Class247[1][Class228.anInt2201][Class368_Sub12.anInt5488]);
			Class52.aSArray636 = new s[1];
		} else {
			Class296_Sub51_Sub32.anIntArrayArray6514 = null;
			Mobile.aByteArrayArray6776 = null;
			StaticMethods.aShortArrayArray5919 = null;
			Class351.aClass247ArrayArrayArray3041 = null;
			Class52.aSArray636 = null;
		}
		if (bool_6_) {
			Class49.aLongArrayArrayArray462 = new long[i_2_][i_3_][i_4_];
			Class302.aClass93Array2721 = new Class93[65535];
			Class296_Sub17_Sub2.aBooleanArray6047 = new boolean[65535];
			Class368_Sub4.anInt5440 = 0;
		} else {
			Class49.aLongArrayArrayArray462 = null;
			Class302.aClass93Array2721 = null;
			Class296_Sub17_Sub2.aBooleanArray6047 = null;
			Class368_Sub4.anInt5440 = 0;
		}
		Class69_Sub1.method732(false);
		Class368_Sub10.aClass338_Sub3Array5481 = new Class338_Sub3[2];
		Class368_Sub16.aClass338_Sub3Array5525 = new Class338_Sub3[2];
		Class338_Sub3_Sub3.aClass338_Sub3Array6568 = new Class338_Sub3[2];
		Class296_Sub51_Sub37.aClass338_Sub3Array6531 = new Class338_Sub3[10000];
		Class338_Sub8_Sub2.anInt6584 = 0;
		Model.aClass338_Sub3Array1850 = new Class338_Sub3[5000];
		CS2Call.anInt4963 = 0;
		s.aClass338_Sub3_Sub1Array2833 = new Class338_Sub3_Sub1[5000];
		Class127.anInt1303 = 0;
		Class296_Sub15_Sub2.aBooleanArrayArray6006 = (new boolean[Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 + 1][Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 + 1]);
		Class370.aBooleanArrayArray3140 = (new boolean[Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 + 2][Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 + 2]);
		Class285.anIntArray2624 = new int[Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 + 2];
		CS2Stack.aClass264_2249 = CS2Stack.aClass264_2243;
		if (Class338_Sub3_Sub2.aBoolean6566) {
			Class121.aBooleanArrayArrayArray1268 = (new boolean[i_2_][Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 + 1][Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 + 1]);
			Class241_Sub2_Sub2.aBooleanArrayArrayArray5907 = new boolean[i_2_][][];
			if (StaticMethods.aClass142Array5951 != null)
				Class306.method3285();
			StaticMethods.aClass142Array5951 = new Class142[Class377.anInt3190];
			Class16_Sub2.aHa3733.f(StaticMethods.aClass142Array5951.length + 1);
			Class16_Sub2.aHa3733.i(0);
			for (int i_7_ = 0; i_7_ < StaticMethods.aClass142Array5951.length; i_7_++) {
				StaticMethods.aClass142Array5951[i_7_] = new Class142(i_7_ + 1, Class16_Sub2.aHa3733);
				new Thread(StaticMethods.aClass142Array5951[i_7_], "wr" + i_7_).start();
			}
			int i_8_;
			if (Class377.anInt3190 == 2) {
				i_8_ = 4;
				Class247.anInt2336 = 2;
			} else if (Class377.anInt3190 == 3) {
				i_8_ = 6;
				Class247.anInt2336 = 3;
			} else {
				i_8_ = 8;
				Class247.anInt2336 = 4;
			}
			Class127.aClass299Array1301 = new Class299[i_8_];
			for (int i_9_ = 0; i_9_ < i_8_; i_9_++)
				Class127.aClass299Array1301[i_9_] = new Class299(s_Sub3.aStringArrayArray5163[Class377.anInt3190 - 2][i_9_]);
		} else
			Class247.anInt2336 = 1;
		Class379_Sub2_Sub1.anIntArray6605 = new int[Class247.anInt2336 - 1];
		Class377.anIntArray3192 = new int[Class247.anInt2336 - 1];
	}
}
