package net.zaros.client;

/* Class169 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Color;

public class ParticleEmitterRaw {
	int anInt1702;
	int anInt1703;
	int[] anIntArray1704;
	int anInt1705;
	int anInt1706;
	private int anInt1707;
	private int anInt1708;
	int anInt1709;
	boolean aBoolean1710;
	int anInt1711;
	int anInt1712;
	boolean aBoolean1713 = true;
	int anInt1714;
	boolean aBoolean1715;
	int anInt1716;
	private int anInt1717;
	int anInt1718;
	int anInt1719;
	int anInt1720;
	int anInt1721;
	int anInt1722;
	int anInt1723;
	int anInt1724;
	static AdvancedMemoryCache aClass113_1725 = new AdvancedMemoryCache(5);
	int anInt1726;
	boolean aBoolean1727;
	int anInt1728;
	boolean aBoolean1729;
	int anInt1730;
	int anInt1731;
	int anInt1732;
	private int anInt1733;
	int anInt1734;
	int[] anIntArray1735;
	short aShort1736;
	private int anInt1737;
	int anInt1738;
	private int anInt1739;
	short aShort1740;
	int anInt1741;
	int anInt1742;
	int anInt1743;
	int anInt1744;
	int anInt1745;
	private int anInt1746;
	int anInt1747;
	int anInt1748;
	short aShort1749;
	short aShort1750;
	boolean aBoolean1751;
	int anInt1752;
	int anInt1753;
	private int anInt1754;
	int anInt1755;
	static Color[] aColorArray1756 = { new Color(9179409), new Color(3289650), new Color(3289650), new Color(3289650) };
	int anInt1757;
	int anInt1758;
	boolean aBoolean1759;
	int anInt1760;
	boolean aBoolean1761;
	private int anInt1762;
	int anInt1763;
	private int anInt1764;
	int anInt1765;
	int[] anIntArray1766;
	boolean aBoolean1767;
	int anInt1768;
	int[] anIntArray1769;
	static NodeDeque aClass155_1770;
	static int anInt1771 = -1;
	static byte[][] aByteArrayArray1772;

	static final int method1663(int i, byte[] is, int i_0_, String string) {
		int i_1_ = i_0_;
		int i_2_ = string.length();
		if (i != -1) {
			return -116;
		}
		for (int i_3_ = 0; i_2_ > i_3_; i_3_ += 4) {
			int i_4_ = Class386.method4029(string.charAt(i_3_), -1);
			int i_5_ = i_2_ > i_3_ + 1 ? Class386.method4029(string.charAt(i_3_ + 1), i) : -1;
			int i_6_ = i_3_ + 2 >= i_2_ ? -1 : Class386.method4029(string.charAt(i_3_ + 2), -1);
			int i_7_ = i_2_ > i_3_ + 3 ? Class386.method4029(string.charAt(i_3_ + 3), -1) : -1;
			is[i_0_++] = (byte) Class48.bitOR(i_5_ >>> 4, i_4_ << 2);
			if (i_6_ == -1) {
				break;
			}
			is[i_0_++] = (byte) Class48.bitOR(i_5_ << 4 & 240, i_6_ >>> 2);
			if (i_7_ == -1) {
				break;
			}
			is[i_0_++] = (byte) Class48.bitOR(i_7_, i_6_ << 6 & 192);
		}
		return i_0_ - i_1_;
	}

	private final void method1664(int i, int i_8_, Packet buffer) {
		if (i_8_ == 1) {
			aShort1740 = (short) buffer.g2();
			aShort1749 = (short) buffer.g2();
			aShort1750 = (short) buffer.g2();
			aShort1736 = (short) buffer.g2();
			int i_9_ = 3;
			aShort1749 <<= i_9_;
			aShort1736 <<= i_9_;
			aShort1740 <<= i_9_;
			aShort1750 <<= i_9_;
		} else if (i_8_ == 2) {
			buffer.g1();
		} else if (i_8_ == 3) {
			anInt1723 = buffer.g4();
			anInt1721 = buffer.g4();
		} else if (i_8_ == 4) {
			anInt1702 = buffer.g1();
			anInt1731 = buffer.g1b();
		} else if (i_8_ == 5) {
			anInt1728 = anInt1757 = buffer.g2() << 12 << 2;
		} else if (i_8_ == 6) {
			anInt1707 = buffer.g4();
			anInt1737 = buffer.g4();
		} else if (i_8_ != 7) {
			if (i_8_ != 8) {
				if (i_8_ != 9) {
					if (i_8_ == 10) {
						int i_10_ = buffer.g1();
						anIntArray1769 = new int[i_10_];
						for (int i_11_ = 0; i_11_ < i_10_; i_11_++) {
							anIntArray1769[i_11_] = buffer.g2();
						}
					} else if (i_8_ == 12) {
						anInt1705 = buffer.g1b();
					} else if (i_8_ == 13) {
						anInt1718 = buffer.g1b();
					} else if (i_8_ == 14) {
						anInt1738 = buffer.g2();
					} else if (i_8_ != 15) {
						if (i_8_ != 16) {
							if (i_8_ != 17) {
								if (i_8_ == 18) {
									anInt1742 = buffer.g4();
								} else if (i_8_ != 19) {
									if (i_8_ == 20) {
										anInt1746 = buffer.g1();
									} else if (i_8_ != 21) {
										if (i_8_ == 22) {
											anInt1720 = buffer.g4();
										} else if (i_8_ != 23) {
											if (i_8_ != 24) {
												if (i_8_ != 25) {
													if (i_8_ != 26) {
														if (i_8_ != 27) {
															if (i_8_ != 28) {
																if (i_8_ == 29) {
																	buffer.g2b();
																} else if (i_8_ != 30) {
																	if (i_8_ != 31) {
																		if (i_8_ != 32) {
																			if (i_8_ != 33) {
																				if (i_8_ == 34) {
																					aBoolean1729 = false;
																				}
																			} else {
																				aBoolean1727 = true;
																			}
																		} else {
																			aBoolean1713 = false;
																		}
																	} else {
																		anInt1728 = buffer.g2() << 12 << 2;
																		anInt1757 = buffer.g2() << 12 << 2;
																	}
																} else {
																	aBoolean1759 = true;
																}
															} else {
																anInt1733 = buffer.g1();
															}
														} else {
															anInt1758 = buffer.g2() << 12 << 2;
														}
													} else {
														aBoolean1761 = false;
													}
												} else {
													int i_12_ = buffer.g1();
													anIntArray1766 = new int[i_12_];
													for (int i_13_ = 0; i_13_ < i_12_; i_13_++) {
														anIntArray1766[i_13_] = buffer.g2();
													}
												}
											} else {
												aBoolean1715 = false;
											}
										} else {
											anInt1708 = buffer.g1();
										}
									} else {
										anInt1764 = buffer.g1();
									}
								} else {
									anInt1744 = buffer.g1();
								}
							} else {
								anInt1765 = buffer.g2();
							}
						} else {
							aBoolean1751 = buffer.g1() == 1;
							anInt1709 = buffer.g2();
							anInt1724 = buffer.g2();
							aBoolean1710 = buffer.g1() == 1;
						}
					} else {
						anInt1719 = buffer.g2();
					}
				} else {
					int i_14_ = buffer.g1();
					anIntArray1704 = new int[i_14_];
					for (int i_15_ = 0; i_14_ > i_15_; i_15_++) {
						anIntArray1704[i_15_] = buffer.g2();
					}
				}
			} else {
				anInt1752 = buffer.g2();
				anInt1716 = buffer.g2();
			}
		} else {
			anInt1743 = buffer.g2();
			anInt1768 = buffer.g2();
		}

		if (i != 4) {
			anInt1737 = 62;
		}
	}

	public static void method1665(int i) {
		aByteArrayArray1772 = null;
		aColorArray1756 = null;
		aClass155_1770 = null;
		aClass113_1725 = null;
		int i_16_ = -95 / ((i + 76) / 32);
	}

	public final void method1666(Packet buffer) {
		for (;;) {
			int i_17_ = buffer.g1();
			if (i_17_ == 0) {
				break;
			}
			method1664(4, i_17_, buffer);
		}
	}

	final void method1667(int i) {
		if (anInt1705 > -2 || anInt1718 > -2) {
			aBoolean1767 = true;
		}
		anInt1760 = anInt1707 >> 16 & 0xff;
		if (i != 24) {
			aByteArrayArray1772 = null;
		}
		anInt1754 = (anInt1737 & 0xffc19b) >> 16;
		anInt1763 = anInt1754 - anInt1760;
		anInt1762 = anInt1737 >> 8 & 0xff;
		anInt1732 = anInt1707 >> 8 & 0xff;
		anInt1753 = -anInt1732 + anInt1762;
		anInt1755 = anInt1707 & 0xff;
		anInt1739 = anInt1737 & 0xff;
		anInt1726 = -anInt1755 + anInt1739;
		anInt1717 = anInt1737 >> 24 & 0xff;
		anInt1722 = anInt1707 >> 24 & 0xff;
		anInt1712 = -anInt1722 + anInt1717;
		if (anInt1720 != -1) {
			anInt1734 = anInt1768 * anInt1708 / 100;
			if (anInt1734 == 0) {
				anInt1734 = 1;
			}
			anInt1748 = (anInt1720 + -anInt1723 - (-anInt1723 + anInt1721) / 2) / anInt1734;
		}
		if (anInt1742 != 0) {
			anInt1730 = anInt1764 * anInt1768 / 100;
			anInt1741 = anInt1768 * anInt1746 / 100;
			if (anInt1741 == 0) {
				anInt1741 = 1;
			}
			if (anInt1730 == 0) {
				anInt1730 = 1;
			}
			anInt1745 = (-(anInt1763 / 2) - anInt1760 + ((anInt1742 & 0xffb60d) >> 16) << 8) / anInt1741;
			anInt1703 = ((anInt1742 >> 8 & 0xff) - (anInt1732 + anInt1753 / 2) << 8) / anInt1741;
			anInt1714 = ((anInt1742 & 0xff) - (anInt1755 + anInt1726 / 2) << 8) / anInt1741;
			ParticleEmitterRaw class169_18_ = this;
			class169_18_.anInt1703 = class169_18_.anInt1703 - (anInt1703 > 0 ? 4 : 4);
			ParticleEmitterRaw class169_19_ = this;
			class169_19_.anInt1714 = class169_19_.anInt1714 - (anInt1714 > 0 ? 4 : 4);
			ParticleEmitterRaw class169_20_ = this;
			class169_20_.anInt1745 = class169_20_.anInt1745 + (anInt1745 <= 0 ? 4 : -4);
			anInt1711 = (-anInt1722 - (anInt1712 / 2 - (anInt1742 >> 24 & 0xff)) << 8) / anInt1730;
			ParticleEmitterRaw class169_21_ = this;
			class169_21_.anInt1711 = class169_21_.anInt1711 - (anInt1711 > 0 ? 4 : 4);
		}
		if (anInt1758 != -1) {
			anInt1706 = anInt1733 * anInt1768 / 100;
			if (anInt1706 == 0) {
				anInt1706 = 1;
			}
			anInt1747 = (-((-anInt1728 + anInt1757) / 2) - (anInt1728 - anInt1758)) / anInt1706;
		}
	}

	static final int method1668(int i, int i_22_, int i_23_, byte i_24_) {
		int i_25_ = -81 / ((i_24_ - 71) / 49);
		if (i < i_23_) {
			return i_23_;
		}
		if (i <= i_22_) {
			return i;
		}
		return i_22_;
	}

	public ParticleEmitterRaw() {
		anInt1708 = 100;
		anInt1719 = -1;
		anInt1720 = -1;
		anInt1702 = 0;
		aBoolean1715 = true;
		anInt1733 = 100;
		aBoolean1727 = false;
		anInt1724 = -1;
		anInt1738 = 0;
		aBoolean1751 = true;
		anInt1746 = 100;
		anInt1705 = -2;
		aBoolean1759 = false;
		aBoolean1710 = true;
		anInt1718 = -2;
		anInt1765 = -1;
		anInt1758 = -1;
		aBoolean1767 = false;
		anInt1764 = 100;
		anInt1744 = 0;
		anInt1709 = -1;
		aBoolean1729 = true;
		aBoolean1761 = true;
	}

	static {
		aClass155_1770 = new NodeDeque();
	}
}
