package net.zaros.client;

/* Class164 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class164 {
	AdvancedMemoryCache aClass113_1680 = new AdvancedMemoryCache(20);
	static int anInt1681;
	static Class209 aClass209_1682 = new Class209(11);
	private Js5 aClass138_1683;
	Js5 aClass138_1684;
	private AdvancedMemoryCache aClass113_1685 = new AdvancedMemoryCache(64);
	static IncomingPacket aClass231_1686 = new IncomingPacket(69, -2);
	static boolean aBoolean1687 = true;

	final void method1630(boolean bool) {
		synchronized (aClass113_1685) {
			aClass113_1685.clear();
		}
		synchronized (aClass113_1680) {
			aClass113_1680.clear();
			if (bool != true)
				method1631(-121);
		}
	}

	public static void method1631(int i) {
		aClass209_1682 = null;
		if (i != -2)
			anInt1681 = -84;
		aClass231_1686 = null;
	}

	final void method1632(int i) {
		synchronized (aClass113_1685) {
			aClass113_1685.clearSoftReferences();
		}
		synchronized (aClass113_1680) {
			if (i != 50560) {
				/* empty */
			} else
				aClass113_1680.clearSoftReferences();
		}
	}

	static final boolean method1633(int i, int i_0_, int i_1_) {
		if (i != 46)
			method1633(47, -10, -43);
		if ((i_0_ & 0xc580) == 0)
			return false;
		return true;
	}

	final Class330 method1634(int i, int i_2_) {
		int i_3_ = 99 / ((i_2_ - 42) / 55);
		Class330 class330;
		synchronized (aClass113_1685) {
			class330 = (Class330) aClass113_1685.get((long) i);
		}
		if (class330 != null)
			return class330;
		byte[] is;
		synchronized (aClass138_1683) {
			is = aClass138_1683.getFile(46, i);
		}
		class330 = new Class330();
		class330.aClass164_2923 = this;
		if (is != null)
			class330.method3400(120, new Packet(is));
		synchronized (aClass113_1685) {
			aClass113_1685.put(class330, (long) i);
		}
		return class330;
	}

	final void method1635(byte i, int i_4_) {
		synchronized (aClass113_1685) {
			aClass113_1685.clean(i_4_);
		}
		if (i >= 96) {
			synchronized (aClass113_1680) {
				aClass113_1680.clean(i_4_);
			}
		}
	}

	Class164(GameType class35, int i, Js5 class138, Js5 class138_5_) {
		aClass138_1684 = class138_5_;
		aClass138_1683 = class138;
		aClass138_1683.getLastFileId(46);
	}
}
