package net.zaros.client;
/* Class296_Sub39_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class GameLoopTask extends Queuable {
	int anInt6145;
	static Js5 aClass138_6146;
	int intParam;
	String stringParam;
	int anInt6149;

	final long method2804() {
		return queue_uid & 0x7fffffffffffffffL;
	}

	final void insertIntoQueue_2() {
		queue_uid |= ~0x7fffffffffffffffL;
		if (method2804() == 0L)
			aa_Sub1.aClass145_3719.insert(this, -2);
	}

	final int getType() {
		return (int) (uid >>> 56 & 0xffL);
	}

	final long bigUID() {
		return uid & 0xffffffffffffffL;
	}

	final void insertIntoQueue() {
		queue_uid = (Class72.method771(-121) + 500L | queue_uid & ~0x7fffffffffffffffL);
		ObjectDefinitionLoader.gameTaskQueue.insert(this, -2);
	}

	public static void method2809(byte i) {
		aClass138_6146 = null;
		int i_0_ = -69 / ((31 - i) / 43);
	}

	GameLoopTask(int i, long l) {
		uid = (long) i << 56 | l;
	}
}
