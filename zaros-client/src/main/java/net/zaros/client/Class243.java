package net.zaros.client;

/* Class243 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class243 {
	static OutgoingPacket aClass311_2315;
	static int anInt2316 = 0;
	static int[] anIntArray2317;

	static final void method2172(int i, Mobile class338_sub3_sub1_sub3) {
		if (!(class338_sub3_sub1_sub3 instanceof NPC)) {
			if (class338_sub3_sub1_sub3 instanceof Player) {
				Player class338_sub3_sub1_sub3_sub1 = (Player) class338_sub3_sub1_sub3;
				Class368_Sub6.method3832((class338_sub3_sub1_sub3_sub1.z != (Class296_Sub51_Sub11.localPlayer.z)), 2009, class338_sub3_sub1_sub3_sub1);
			}
		} else {
			NPC class338_sub3_sub1_sub3_sub2 = (NPC) class338_sub3_sub1_sub3;
			if (class338_sub3_sub1_sub3_sub2.definition != null)
				Class172.method1681((class338_sub3_sub1_sub3_sub2.z != (Class296_Sub51_Sub11.localPlayer.z)), class338_sub3_sub1_sub3_sub2, -15204);
		}
		if (i != -26844) {
			/* empty */
		}
	}

	public static void method2173(byte i) {
		if (i != 106)
			anIntArray2317 = null;
		aClass311_2315 = null;
		anIntArray2317 = null;
	}

	static {
		aClass311_2315 = new OutgoingPacket(47, 3);
	}
}
