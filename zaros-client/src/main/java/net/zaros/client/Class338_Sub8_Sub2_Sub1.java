package net.zaros.client;

/* Class338_Sub8_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub8_Sub2_Sub1 extends Class338_Sub8_Sub2 {
	private short aShort6856;
	private int anInt6857;
	private short aShort6858;
	private short aShort6859;
	private short aShort6860;
	private short aShort6861;
	private short aShort6862;
	Class338_Sub6 aClass338_Sub6_6863;
	private int anInt6864;

	final void method3610() {
		aClass338_Sub6_6863.aClass338_Sub1_5235.aClass338_Sub8_Sub2_Sub1Array5179[aShort6860] = null;
		Class296_Sub51_Sub36.aClass338_Sub8_Sub2_Sub1Array6528[(ISAACCipher.anInt1895)] = this;
		ISAACCipher.anInt1895 = ISAACCipher.anInt1895 + 1 & 0x3ff;
		this.method3438(false);
		this.method3600((byte) -103);
	}

	final void method3611(long l, int i) {
		aShort6856 -= i;
		if (aShort6856 <= 0)
			method3610();
		else {
			int i_0_ = anInt6580 >> 12;
			int i_1_ = anInt6589 >> 12;
			int i_2_ = anInt6581 >> 12;
			Class338_Sub1 class338_sub1 = aClass338_Sub6_6863.aClass338_Sub1_5235;
			ParticleEmitterRaw class169 = aClass338_Sub6_6863.aClass169_5228;
			if (class169.anInt1742 != 0) {
				if (aShort6862 - aShort6856 <= class169.anInt1741) {
					int i_3_ = ((anInt6583 >> 8 & 0xff00) + (anInt6864 >> 16 & 0xff) + class169.anInt1745 * i);
					int i_4_ = ((anInt6583 & 0xff00) + (anInt6864 >> 8 & 0xff) + class169.anInt1703 * i);
					int i_5_ = ((anInt6583 << 8 & 0xff00) + (anInt6864 & 0xff) + class169.anInt1714 * i);
					if (i_3_ < 0)
						i_3_ = 0;
					else if (i_3_ > 65535)
						i_3_ = 65535;
					if (i_4_ < 0)
						i_4_ = 0;
					else if (i_4_ > 65535)
						i_4_ = 65535;
					if (i_5_ < 0)
						i_5_ = 0;
					else if (i_5_ > 65535)
						i_5_ = 65535;
					anInt6583 &= ~0xffffff;
					anInt6583 |= (((i_3_ & 0xff00) << 8) + (i_4_ & 0xff00) + ((i_5_ & 0xff00) >> 8));
					anInt6864 &= ~0xffffff;
					anInt6864 |= (((i_3_ & 0xff) << 16) + ((i_4_ & 0xff) << 8) + (i_5_ & 0xff));
				}
				if (aShort6862 - aShort6856 <= class169.anInt1730) {
					int i_6_ = ((anInt6583 >> 16 & 0xff00) + (anInt6864 >> 24 & 0xff) + class169.anInt1711 * i);
					if (i_6_ < 0)
						i_6_ = 0;
					else if (i_6_ > 65535)
						i_6_ = 65535;
					anInt6583 &= 0xffffff;
					anInt6583 |= (i_6_ & 0xff00) << 16;
					anInt6864 &= 0xffffff;
					anInt6864 |= (i_6_ & 0xff) << 24;
				}
			}
			if (class169.anInt1720 != -1 && aShort6862 - aShort6856 <= class169.anInt1734)
				anInt6857 += class169.anInt1748 * i;
			if (class169.anInt1758 != -1 && aShort6862 - aShort6856 <= class169.anInt1706)
				anInt6579 += class169.anInt1747 * i;
			double d = (double) aShort6859;
			double d_7_ = (double) aShort6858;
			double d_8_ = (double) aShort6861;
			boolean bool = false;
			if (class169.anInt1702 == 1) {
				int i_9_ = i_0_ - aClass338_Sub6_6863.aClass337_5232.anInt2964;
				int i_10_ = i_1_ - aClass338_Sub6_6863.aClass337_5232.anInt2962;
				int i_11_ = i_2_ - aClass338_Sub6_6863.aClass337_5232.anInt2969;
				int i_12_ = ((int) Math.sqrt((double) (i_9_ * i_9_ + i_10_ * i_10_ + i_11_ * i_11_)) >> 2);
				long l_13_ = (long) (class169.anInt1731 * i_12_ * i);
				anInt6857 -= (long) anInt6857 * l_13_ >> 18;
			} else if (class169.anInt1702 == 2) {
				int i_14_ = i_0_ - aClass338_Sub6_6863.aClass337_5232.anInt2964;
				int i_15_ = i_1_ - aClass338_Sub6_6863.aClass337_5232.anInt2962;
				int i_16_ = i_2_ - aClass338_Sub6_6863.aClass337_5232.anInt2969;
				int i_17_ = i_14_ * i_14_ + i_15_ * i_15_ + i_16_ * i_16_;
				long l_18_ = (long) (class169.anInt1731 * i_17_ * i);
				anInt6857 -= (long) anInt6857 * l_18_ >> 28;
			}
			if (class169.anIntArray1704 != null) {
				Node class296 = class338_sub1.aClass155_5185.aClass296_1586;
				for (Node class296_19_ = class296.next; class296_19_ != class296; class296_19_ = class296_19_.next) {
					Class296_Sub39_Sub17 class296_sub39_sub17 = (Class296_Sub39_Sub17) class296_19_;
					Class95 class95 = class296_sub39_sub17.aClass95_6245;
					if (class95.anInt1030 != 1) {
						boolean bool_20_ = false;
						for (int i_21_ = 0; i_21_ < class169.anIntArray1704.length; i_21_++) {
							if (class169.anIntArray1704[i_21_] == class95.anInt1027) {
								bool_20_ = true;
								break;
							}
						}
						if (bool_20_) {
							double d_22_ = (double) (i_0_ - class296_sub39_sub17.anInt6235);
							double d_23_ = (double) (i_1_ - class296_sub39_sub17.anInt6243);
							double d_24_ = (double) (i_2_ - class296_sub39_sub17.anInt6242);
							double d_25_ = (d_22_ * d_22_ + d_23_ * d_23_ + d_24_ * d_24_);
							if (!(d_25_ > (double) class95.aLong1029)) {
								double d_26_ = Math.sqrt(d_25_);
								if (d_26_ == 0.0)
									d_26_ = 1.0;
								double d_27_ = ((d_22_ * (double) (class296_sub39_sub17.anInt6236) + d_23_ * (double) class95.anInt1026 + (d_24_ * (double) (class296_sub39_sub17.anInt6239))) * 65535.0 / ((double) class95.anInt1033 * d_26_));
								if (!(d_27_ < (double) class95.anInt1020)) {
									double d_28_ = 0.0;
									if (class95.anInt1035 == 1)
										d_28_ = (d_26_ / 16.0 * (double) class95.anInt1024);
									else if (class95.anInt1035 == 2)
										d_28_ = (d_26_ / 16.0 * (d_26_ / 16.0) * (double) class95.anInt1024);
									if (class95.anInt1022 == 0) {
										if (class95.anInt1034 == 0) {
											d += ((double) (class296_sub39_sub17.anInt6236) - d_28_) * (double) i;
											d_7_ += ((double) class95.anInt1026 - d_28_) * (double) i;
											d_8_ += ((double) (class296_sub39_sub17.anInt6239) - d_28_) * (double) i;
											bool = true;
										} else {
											anInt6580 += ((double) (class296_sub39_sub17.anInt6236) - d_28_) * (double) i;
											anInt6589 += ((double) class95.anInt1026 - d_28_) * (double) i;
											anInt6581 += ((double) (class296_sub39_sub17.anInt6239) - d_28_) * (double) i;
										}
									} else {
										double d_29_ = (d_22_ / d_26_ * (double) class95.anInt1033);
										double d_30_ = (d_23_ / d_26_ * (double) class95.anInt1033);
										double d_31_ = (d_24_ / d_26_ * (double) class95.anInt1033);
										if (class95.anInt1034 == 0) {
											d += d_29_ * (double) i;
											d_7_ += d_30_ * (double) i;
											d_8_ += d_31_ * (double) i;
											bool = true;
										} else {
											anInt6580 += d_29_ * (double) i;
											anInt6589 += d_30_ * (double) i;
											anInt6581 += d_31_ * (double) i;
										}
									}
								}
							}
						}
					}
				}
			}
			if (class169.anIntArray1766 != null) {
				for (int i_32_ = 0; i_32_ < class169.anIntArray1766.length; i_32_++) {
					Class296_Sub39_Sub17 class296_sub39_sub17 = ((Class296_Sub39_Sub17) (Class241_Sub1.aClass400_4577.method4137(46, (long) class169.anIntArray1766[i_32_])));
					while (class296_sub39_sub17 != null) {
						Class95 class95 = class296_sub39_sub17.aClass95_6245;
						double d_33_ = (double) (i_0_ - class296_sub39_sub17.anInt6235);
						double d_34_ = (double) (i_1_ - class296_sub39_sub17.anInt6243);
						double d_35_ = (double) (i_2_ - class296_sub39_sub17.anInt6242);
						double d_36_ = d_33_ * d_33_ + d_34_ * d_34_ + d_35_ * d_35_;
						if (d_36_ > (double) class95.aLong1029)
							class296_sub39_sub17 = (Class296_Sub39_Sub17) Class241_Sub1.aClass400_4577.method4135(true);
						else {
							double d_37_ = Math.sqrt(d_36_);
							if (d_37_ == 0.0)
								d_37_ = 1.0;
							double d_38_ = (((d_33_ * (double) class296_sub39_sub17.anInt6236) + d_34_ * (double) class95.anInt1026 + d_35_ * (double) (class296_sub39_sub17.anInt6239)) * 65535.0 / ((double) class95.anInt1033 * d_37_));
							if (d_38_ < (double) class95.anInt1020)
								class296_sub39_sub17 = ((Class296_Sub39_Sub17) Class241_Sub1.aClass400_4577.method4135(true));
							else {
								double d_39_ = 0.0;
								if (class95.anInt1035 == 1)
									d_39_ = (d_37_ / 16.0 * (double) class95.anInt1024);
								else if (class95.anInt1035 == 2)
									d_39_ = (d_37_ / 16.0 * (d_37_ / 16.0) * (double) class95.anInt1024);
								if (class95.anInt1022 == 0) {
									if (class95.anInt1034 == 0) {
										d += ((double) (class296_sub39_sub17.anInt6236) - d_39_) * (double) i;
										d_7_ += ((double) class95.anInt1026 - d_39_) * (double) i;
										d_8_ += ((double) (class296_sub39_sub17.anInt6239) - d_39_) * (double) i;
										bool = true;
									} else {
										anInt6580 += ((double) (class296_sub39_sub17.anInt6236) - d_39_) * (double) i;
										anInt6589 += ((double) class95.anInt1026 - d_39_) * (double) i;
										anInt6581 += ((double) (class296_sub39_sub17.anInt6239) - d_39_) * (double) i;
									}
								} else {
									double d_40_ = (d_33_ / d_37_ * (double) class95.anInt1033);
									double d_41_ = (d_34_ / d_37_ * (double) class95.anInt1033);
									double d_42_ = (d_35_ / d_37_ * (double) class95.anInt1033);
									if (class95.anInt1034 == 0) {
										d += d_40_ * (double) i;
										d_7_ += d_41_ * (double) i;
										d_8_ += d_42_ * (double) i;
										bool = true;
									} else {
										anInt6580 += d_40_ * (double) i;
										anInt6589 += d_41_ * (double) i;
										anInt6581 += d_42_ * (double) i;
									}
								}
								class296_sub39_sub17 = ((Class296_Sub39_Sub17) Class241_Sub1.aClass400_4577.method4135(true));
							}
						}
					}
				}
			}
			if (class169.anIntArray1769 != null) {
				if (class169.anIntArray1735 == null) {
					class169.anIntArray1735 = new int[class169.anIntArray1769.length];
					for (int i_43_ = 0; i_43_ < class169.anIntArray1769.length; i_43_++) {
						Class355.method3697(35044, class169.anIntArray1769[i_43_]);
						class169.anIntArray1735[i_43_] = (((IntegerNode) (Class41_Sub24.aClass263_3802.get((long) class169.anIntArray1769[i_43_]))).value);
					}
				}
				for (int i_44_ = 0; i_44_ < class169.anIntArray1735.length; i_44_++) {
					Class95 class95 = (Class144.aClass95Array1435[class169.anIntArray1735[i_44_]]);
					if (class95.anInt1034 == 0) {
						d += (double) (class95.anInt1028 * i);
						d_7_ += (double) (class95.anInt1026 * i);
						d_8_ += (double) (class95.anInt1032 * i);
						bool = true;
					} else {
						anInt6580 += class95.anInt1028 * i;
						anInt6589 += class95.anInt1026 * i;
						anInt6581 += class95.anInt1032 * i;
					}
				}
			}
			if (bool) {
				while (d > 32767.0 || d_7_ > 32767.0 || d_8_ > 32767.0 || d < -32767.0 || d_7_ < -32767.0 || d_8_ < -32767.0) {
					d /= 2.0;
					d_7_ /= 2.0;
					d_8_ /= 2.0;
					anInt6857 <<= 1;
				}
				aShort6859 = (short) (int) d;
				aShort6858 = (short) (int) d_7_;
				aShort6861 = (short) (int) d_8_;
			}
			anInt6580 += (((long) aShort6859 * (long) (anInt6857 << 2) >> 23) * (long) i);
			anInt6589 += (((long) aShort6858 * (long) (anInt6857 << 2) >> 23) * (long) i);
			anInt6581 += (((long) aShort6861 * (long) (anInt6857 << 2) >> 23) * (long) i);
		}
	}

	final void method3612(ha var_ha, long l) {
		int i = anInt6580 >> Class313.anInt2779 + 12;
		int i_45_ = anInt6581 >> Class313.anInt2779 + 12;
		int i_46_ = anInt6589 >> 12;
		if (i_46_ > 0 || i_46_ < -262144 || i < 0 || i >= Class228.anInt2201 || i_45_ < 0 || i_45_ >= Class368_Sub12.anInt5488)
			method3610();
		else {
			Class338_Sub1 class338_sub1 = aClass338_Sub6_6863.aClass338_Sub1_5235;
			ParticleEmitterRaw class169 = aClass338_Sub6_6863.aClass169_5228;
			s[] var_ses = Class360_Sub2.aSArray5304;
			int i_47_ = class338_sub1.anInt5187;
			Class247 class247 = (Class338_Sub2.aClass247ArrayArrayArray5195[class338_sub1.anInt5187][i][i_45_]);
			if (class247 != null)
				i_47_ = class247.aByte2349;
			int i_48_ = var_ses[i_47_].method3355(i_45_, (byte) -128, i);
			int i_49_;
			if (i_47_ < Class368_Sub9.anInt5477 - 1)
				i_49_ = var_ses[i_47_ + 1].method3355(i_45_, (byte) -112, i);
			else
				i_49_ = i_48_ - (8 << Class313.anInt2779);
			if (class169.aBoolean1767) {
				if (class169.anInt1705 == -1 && i_46_ > i_48_) {
					method3610();
					return;
				}
				if (class169.anInt1705 >= 0 && i_46_ > var_ses[class169.anInt1705].method3355(i_45_, (byte) -118, i)) {
					method3610();
					return;
				}
				if (class169.anInt1718 == -1 && i_46_ < i_49_) {
					method3610();
					return;
				}
				if (class169.anInt1718 >= 0 && i_46_ < var_ses[class169.anInt1718 + 1].method3355(i_45_, (byte) -108, i)) {
					method3610();
					return;
				}
			}
			int i_50_;
			for (i_50_ = Class368_Sub9.anInt5477 - 1; (i_50_ > 0 && i_46_ > var_ses[i_50_].method3355(i_45_, (byte) -110, i)); i_50_--) {
				/* empty */
			}
			if (class169.aBoolean1729 && i_50_ == 0 && i_46_ > var_ses[0].method3355(i_45_, (byte) -125, i))
				method3610();
			else if (i_50_ == Class368_Sub9.anInt5477 - 1 && (var_ses[i_50_].method3355(i_45_, (byte) -125, i) - i_46_) > 8 << Class313.anInt2779)
				method3610();
			else {
				class247 = (Class338_Sub2.aClass247ArrayArrayArray5195[i_50_][i][i_45_]);
				if (class247 == null) {
					if (i_50_ == 0 || (Class338_Sub2.aClass247ArrayArrayArray5195[0][i][i_45_]) == null)
						class247 = Class338_Sub2.aClass247ArrayArrayArray5195[0][i][i_45_] = new Class247(0);
					boolean bool = ((Class338_Sub2.aClass247ArrayArrayArray5195[0][i][i_45_].aClass247_2338) != null);
					if (i_50_ == 3 && bool) {
						method3610();
						return;
					}
					for (int i_51_ = 1; i_51_ <= i_50_; i_51_++) {
						if ((Class338_Sub2.aClass247ArrayArrayArray5195[i_51_][i][i_45_]) == null) {
							class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i_51_][i][i_45_] = new Class247(i_51_);
							if (bool)
								class247.aByte2349++;
						}
					}
				}
				if (class169.aBoolean1727) {
					int i_52_ = anInt6580 >> 12;
					int i_53_ = anInt6581 >> 12;
					if (class247.aClass338_Sub3_Sub4_2348 != null) {
						Class96 class96 = class247.aClass338_Sub3_Sub4_2348.method3461(var_ha, 127);
						if (class96 != null && class96.method867(i_46_, i_53_, i_52_, (byte) -95)) {
							method3610();
							return;
						}
					}
					if (class247.aClass338_Sub3_Sub4_2343 != null) {
						Class96 class96 = class247.aClass338_Sub3_Sub4_2343.method3461(var_ha, -29);
						if (class96 != null && class96.method867(i_46_, i_53_, i_52_, (byte) -104)) {
							method3610();
							return;
						}
					}
					if (class247.aClass338_Sub3_Sub5_2346 != null) {
						Class96 class96 = class247.aClass338_Sub3_Sub5_2346.method3461(var_ha, 12);
						if (class96 != null && class96.method867(i_46_, i_53_, i_52_, (byte) -106)) {
							method3610();
							return;
						}
					}
					for (Class234 class234 = class247.aClass234_2341; class234 != null; class234 = class234.aClass234_2224) {
						Class96 class96 = class234.aClass338_Sub3_Sub1_2226.method3461(var_ha, -36);
						if (class96 != null && class96.method867(i_46_, i_53_, i_52_, (byte) -97)) {
							method3610();
							return;
						}
					}
				}
				class338_sub1.aClass390_5188.aClass84_3282.method815(24150, this);
			}
		}
	}

	final void method3613(Class338_Sub6 class338_sub6, int i, int i_54_, int i_55_, int i_56_, int i_57_, int i_58_, int i_59_, int i_60_, int i_61_, int i_62_, int i_63_, boolean bool, boolean bool_64_) {
		aClass338_Sub6_6863 = class338_sub6;
		anInt6580 = i << 12;
		anInt6589 = i_54_ << 12;
		anInt6581 = i_55_ << 12;
		anInt6583 = i_61_;
		aShort6862 = aShort6856 = (short) i_60_;
		anInt6579 = i_62_;
		anInt6582 = i_63_;
		aBoolean6578 = bool_64_;
		aShort6859 = (short) i_56_;
		aShort6858 = (short) i_57_;
		aShort6861 = (short) i_58_;
		anInt6857 = i_59_;
		aByte6586 = aClass338_Sub6_6863.aClass89_5233.aByte955;
		method3614();
	}

	private final void method3614() {
		int i = aClass338_Sub6_6863.aClass338_Sub1_5235.anInt5183;
		if ((aClass338_Sub6_6863.aClass338_Sub1_5235.aClass338_Sub8_Sub2_Sub1Array5179[i]) != null)
			aClass338_Sub6_6863.aClass338_Sub1_5235.aClass338_Sub8_Sub2_Sub1Array5179[i].method3610();
		aClass338_Sub6_6863.aClass338_Sub1_5235.aClass338_Sub8_Sub2_Sub1Array5179[i] = this;
		aShort6860 = (short) aClass338_Sub6_6863.aClass338_Sub1_5235.anInt5183;
		aClass338_Sub6_6863.aClass338_Sub1_5235.anInt5183 = i + 1 & 0x1fff;
		aClass338_Sub6_6863.aClass404_5229.method4158(this, 1);
	}

	Class338_Sub8_Sub2_Sub1(Class338_Sub6 class338_sub6, int i, int i_65_, int i_66_, int i_67_, int i_68_, int i_69_, int i_70_, int i_71_, int i_72_, int i_73_, int i_74_, boolean bool, boolean bool_75_) {
		aClass338_Sub6_6863 = class338_sub6;
		anInt6580 = i << 12;
		anInt6589 = i_65_ << 12;
		anInt6581 = i_66_ << 12;
		anInt6583 = i_72_;
		aShort6862 = aShort6856 = (short) i_71_;
		anInt6579 = i_73_;
		anInt6582 = i_74_;
		aBoolean6578 = bool_75_;
		aShort6859 = (short) i_67_;
		aShort6858 = (short) i_68_;
		aShort6861 = (short) i_69_;
		anInt6857 = i_70_;
		aByte6586 = aClass338_Sub6_6863.aClass89_5233.aByte955;
		method3614();
	}
}
