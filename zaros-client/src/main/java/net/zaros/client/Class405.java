package net.zaros.client;

/* Class405 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class405 {
	private Js5 aClass138_3387;
	private AdvancedMemoryCache aClass113_3388 = new AdvancedMemoryCache(64);
	static Class298 aClass298_3389 = new Class298();

	final Class251 method4170(int i, int i_0_) {
		Class251 class251;
		synchronized (aClass113_3388) {
			class251 = (Class251) aClass113_3388.get((long) i);
		}
		if (class251 != null)
			return class251;
		byte[] is;
		synchronized (aClass138_3387) {
			is = aClass138_3387.getFile(35, i);
		}
		class251 = new Class251();
		if (i_0_ < 106)
			return null;
		if (is != null)
			class251.method2199(-117, new Packet(is));
		class251.method2200((byte) 112);
		synchronized (aClass113_3388) {
			aClass113_3388.put(class251, (long) i);
		}
		return class251;
	}

	final void method4171(int i, int i_1_) {
		synchronized (aClass113_3388) {
			aClass113_3388.clean(i);
		}
		if (i_1_ != 0)
			method4171(30, 4);
	}

	final void method4172(byte i) {
		synchronized (aClass113_3388) {
			if (i >= -75)
				aClass298_3389 = null;
			aClass113_3388.clear();
		}
	}

	static final boolean findPathStandart(int clipType, int toMapX, int toMapY, int sizeX, int sizeY, int targetFace, int unknown, boolean findAlternatePath) {
		int fromMapX = (Class296_Sub51_Sub11.localPlayer.waypointQueueX[0]);
		int fromMapY = (Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0]);
		if (fromMapX < 0 || fromMapX >= Class198.currentMapSizeX || fromMapY < 0 || fromMapY >= Class296_Sub38.currentMapSizeY)
			return false;
		if (toMapY < 0 || Class198.currentMapSizeX <= toMapY || toMapX < 0 || toMapX >= Class296_Sub38.currentMapSizeY)
			return false;
		int numSteps = RouteFinder.findRoute((BITConfigDefinition.mapClips[(Class296_Sub51_Sub11.localPlayer.z)]), clipType, Class296_Sub51_Sub11.localPlayer.getSize(), fromMapX, fromMapY, toMapY, toMapX, sizeX, sizeY, targetFace, Class32.pathBufferX, Class124.pathBufferY, findAlternatePath, unknown);
		if (numSteps < 1)
			return false;
		Class210_Sub1.foundX = Class32.pathBufferX[numSteps - 1];
		Class205.foundY = Class124.pathBufferY[numSteps - 1];
		Class377.aBoolean3189 = false;
		EquipmentData.method3304((byte) 76);
		return true;
	}

	public static void method4174(byte i) {
		aClass298_3389 = null;
		if (i <= 67)
			aClass298_3389 = null;
	}

	final void method4175(byte i) {
		synchronized (aClass113_3388) {
			if (i != -12) {
				/* empty */
			} else
				aClass113_3388.clearSoftReferences();
		}
	}

	Class405(GameType class35, int i, Js5 class138) {
		aClass138_3387 = class138;
		if (aClass138_3387 != null)
			aClass138_3387.getLastFileId(35);
	}
}
