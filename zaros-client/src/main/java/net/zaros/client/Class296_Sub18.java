package net.zaros.client;

/* Class296_Sub18 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub18 extends Node {
	private int anInt4680;
	private static int anInt4681;
	private static int anInt4682;
	static Class346[] aClass346Array4683;
	private static int[] anIntArray4684;
	private boolean aBoolean4685;
	private static int anInt4686;
	private static float[] aFloatArray4687;
	private static float[] aFloatArray4688;
	private int anInt4689;
	private static Class74[] aClass74Array4690;
	private int anInt4691;
	private static Class66[] aClass66Array4692;
	private int anInt4693;
	private static boolean[] aBooleanArray4694;
	private boolean aBoolean4695;
	private byte[][] aByteArrayArray4696;
	private static float[] aFloatArray4697;
	private static byte[] aByteArray4698;
	private float[] aFloatArray4699;
	private static boolean aBoolean4700 = false;
	private static int[] anIntArray4701;
	private static int anInt4702;
	private static float[] aFloatArray4703;
	private int anInt4704;
	private int anInt4705;
	private static Class222[] aClass222Array4706;
	private static float[] aFloatArray4707;
	private byte[] aByteArray4708;
	private static float[] aFloatArray4709;
	private static int[] anIntArray4710;
	private int anInt4711;
	private static float[] aFloatArray4712;
	private int anInt4713;

	static final Class296_Sub18 method2640(Js5 class138, int i) {
		if (!method2643(class138)) {
			class138.hasEntryBuffer(i);
			return null;
		}
		byte[] is = class138.get(i);
		if (is == null)
			return null;
		return new Class296_Sub18(is);
	}

	static final int method2641(int i) {
		int i_0_ = 0;
		int i_1_ = 0;
		int i_2_;
		for (/**/; i >= 8 - anInt4686; i -= i_2_) {
			i_2_ = 8 - anInt4686;
			int i_3_ = (1 << i_2_) - 1;
			i_0_ += (aByteArray4698[anInt4682] >> anInt4686 & i_3_) << i_1_;
			anInt4686 = 0;
			anInt4682++;
			i_1_ += i_2_;
		}
		if (i > 0) {
			i_2_ = (1 << i) - 1;
			i_0_ += (aByteArray4698[anInt4682] >> anInt4686 & i_2_) << i_1_;
			anInt4686 += i;
		}
		return i_0_;
	}

	private static final void method2642(byte[] is) {
		method2648(is, 0);
		anInt4702 = 1 << method2641(4);
		anInt4681 = 1 << method2641(4);
		aFloatArray4709 = new float[anInt4681];
		for (int i = 0; i < 2; i++) {
			int i_4_ = i != 0 ? anInt4681 : anInt4702;
			int i_5_ = i_4_ >> 1;
			int i_6_ = i_4_ >> 2;
			int i_7_ = i_4_ >> 3;
			float[] fs = new float[i_5_];
			for (int i_8_ = 0; i_8_ < i_6_; i_8_++) {
				fs[i_8_ * 2] = (float) Math.cos((double) (i_8_ * 4) * 3.141592653589793 / (double) i_4_);
				fs[i_8_ * 2 + 1] = -(float) Math.sin((double) (i_8_ * 4) * 3.141592653589793 / (double) i_4_);
			}
			float[] fs_9_ = new float[i_5_];
			for (int i_10_ = 0; i_10_ < i_6_; i_10_++) {
				fs_9_[i_10_ * 2] = (float) Math.cos((double) (i_10_ * 2 + 1) * 3.141592653589793 / (double) (i_4_ * 2));
				fs_9_[i_10_ * 2 + 1] = (float) Math.sin((double) (i_10_ * 2 + 1) * 3.141592653589793 / (double) (i_4_ * 2));
			}
			float[] fs_11_ = new float[i_6_];
			for (int i_12_ = 0; i_12_ < i_7_; i_12_++) {
				fs_11_[i_12_ * 2] = (float) Math.cos((double) (i_12_ * 4 + 2) * 3.141592653589793 / (double) i_4_);
				fs_11_[i_12_ * 2 + 1] = -(float) Math.sin((double) (i_12_ * 4 + 2) * 3.141592653589793 / (double) i_4_);
			}
			int[] is_13_ = new int[i_7_];
			int i_14_ = Class296_Sub29_Sub2.method2694((byte) 118, i_7_ - 1);
			for (int i_15_ = 0; i_15_ < i_7_; i_15_++)
				is_13_[i_15_] = Class274.method2316((byte) 79, i_15_, i_14_);
			if (i != 0) {
				aFloatArray4712 = fs;
				aFloatArray4688 = fs_9_;
				aFloatArray4687 = fs_11_;
				anIntArray4710 = is_13_;
			} else {
				aFloatArray4697 = fs;
				aFloatArray4703 = fs_9_;
				aFloatArray4707 = fs_11_;
				anIntArray4701 = is_13_;
			}
		}
		int i = method2641(8) + 1;
		aClass346Array4683 = new Class346[i];
		for (int i_16_ = 0; i_16_ < i; i_16_++)
			aClass346Array4683[i_16_] = new Class346();
		int i_17_ = method2641(6) + 1;
		for (int i_18_ = 0; i_18_ < i_17_; i_18_++)
			method2641(16);
		int i_19_ = method2641(6) + 1;
		aClass74Array4690 = new Class74[i_19_];
		for (int i_20_ = 0; i_20_ < i_19_; i_20_++)
			aClass74Array4690[i_20_] = new Class74();
		int i_21_ = method2641(6) + 1;
		aClass66Array4692 = new Class66[i_21_];
		for (int i_22_ = 0; i_22_ < i_21_; i_22_++)
			aClass66Array4692[i_22_] = new Class66();
		int i_23_ = method2641(6) + 1;
		aClass222Array4706 = new Class222[i_23_];
		for (int i_24_ = 0; i_24_ < i_23_; i_24_++)
			aClass222Array4706[i_24_] = new Class222();
		int i_25_ = method2641(6) + 1;
		aBooleanArray4694 = new boolean[i_25_];
		anIntArray4684 = new int[i_25_];
		for (int i_26_ = 0; i_26_ < i_25_; i_26_++) {
			aBooleanArray4694[i_26_] = method2649() != 0;
			method2641(16);
			method2641(16);
			anIntArray4684[i_26_] = method2641(8);
		}
		aBoolean4700 = true;
	}

	private static final boolean method2643(Js5 class138) {
		if (!aBoolean4700) {
			byte[] is = class138.getFile(0, 0);
			if (is == null)
				return false;
			method2642(is);
		}
		return true;
	}

	final Class296_Sub19_Sub1 method2644(int[] is) {
		if (is != null && is[0] <= 0)
			return null;
		if (aByteArray4708 == null) {
			anInt4691 = 0;
			aFloatArray4699 = new float[anInt4681];
			aByteArray4708 = new byte[anInt4705];
			anInt4711 = 0;
			anInt4713 = 0;
		}
		for (/**/; anInt4713 < aByteArrayArray4696.length; anInt4713++) {
			if (is != null && is[0] <= 0)
				return null;
			float[] fs = method2645(anInt4713);
			if (fs != null) {
				int i = anInt4711;
				int i_27_ = fs.length;
				if (i_27_ > anInt4705 - i)
					i_27_ = anInt4705 - i;
				for (int i_28_ = 0; i_28_ < i_27_; i_28_++) {
					int i_29_ = (int) (fs[i_28_] * 128.0F + 128.0F);
					if ((i_29_ & ~0xff) != 0)
						i_29_ = (i_29_ ^ 0xffffffff) >> 31;
					aByteArray4708[i++] = (byte) (i_29_ - 128);
				}
				if (is != null)
					is[0] -= i - anInt4711;
				anInt4711 = i;
			}
		}
		aFloatArray4699 = null;
		byte[] is_30_ = aByteArray4708;
		aByteArray4708 = null;
		return new Class296_Sub19_Sub1(anInt4680, is_30_, anInt4689, anInt4693, aBoolean4695);
	}

	private final float[] method2645(int i) {
		method2648(aByteArrayArray4696[i], 0);
		method2649();
		int i_31_ = method2641(Class296_Sub29_Sub2.method2694((byte) -8, (anIntArray4684.length - 1)));
		boolean bool = aBooleanArray4694[i_31_];
		int i_32_ = bool ? anInt4681 : anInt4702;
		boolean bool_33_ = false;
		boolean bool_34_ = false;
		if (bool) {
			bool_33_ = method2649() != 0;
			bool_34_ = method2649() != 0;
		}
		int i_35_ = i_32_ >> 1;
		int i_36_;
		int i_37_;
		int i_38_;
		if (bool && !bool_33_) {
			i_36_ = (i_32_ >> 2) - (anInt4702 >> 2);
			i_37_ = (i_32_ >> 2) + (anInt4702 >> 2);
			i_38_ = anInt4702 >> 1;
		} else {
			i_36_ = 0;
			i_37_ = i_35_;
			i_38_ = i_32_ >> 1;
		}
		int i_39_;
		int i_40_;
		int i_41_;
		if (bool && !bool_34_) {
			i_39_ = i_32_ - (i_32_ >> 2) - (anInt4702 >> 2);
			i_40_ = i_32_ - (i_32_ >> 2) + (anInt4702 >> 2);
			i_41_ = anInt4702 >> 1;
		} else {
			i_39_ = i_35_;
			i_40_ = i_32_;
			i_41_ = i_32_ >> 1;
		}
		Class222 class222 = aClass222Array4706[anIntArray4684[i_31_]];
		int i_42_ = class222.anInt2161;
		int i_43_ = class222.anIntArray2160[i_42_];
		boolean bool_44_ = !aClass74Array4690[i_43_].method773();
		boolean bool_45_ = bool_44_;
		for (int i_46_ = 0; i_46_ < class222.anInt2158; i_46_++) {
			Class66 class66 = aClass66Array4692[class222.anIntArray2159[i_46_]];
			float[] fs = aFloatArray4709;
			class66.method710(fs, i_32_ >> 1, bool_45_);
		}
		if (!bool_44_) {
			int i_47_ = class222.anInt2161;
			int i_48_ = class222.anIntArray2160[i_47_];
			aClass74Array4690[i_48_].method774(aFloatArray4709, i_32_ >> 1);
		}
		if (bool_44_) {
			for (int i_49_ = i_32_ >> 1; i_49_ < i_32_; i_49_++)
				aFloatArray4709[i_49_] = 0.0F;
		} else {
			int i_50_ = i_32_ >> 1;
			int i_51_ = i_32_ >> 2;
			int i_52_ = i_32_ >> 3;
			float[] fs = aFloatArray4709;
			for (int i_53_ = 0; i_53_ < i_50_; i_53_++)
				fs[i_53_] *= 0.5F;
			for (int i_54_ = i_50_; i_54_ < i_32_; i_54_++)
				fs[i_54_] = -fs[i_32_ - i_54_ - 1];
			float[] fs_55_ = bool ? aFloatArray4712 : aFloatArray4697;
			float[] fs_56_ = bool ? aFloatArray4688 : aFloatArray4703;
			float[] fs_57_ = bool ? aFloatArray4687 : aFloatArray4707;
			int[] is = bool ? anIntArray4710 : anIntArray4701;
			for (int i_58_ = 0; i_58_ < i_51_; i_58_++) {
				float f = fs[i_58_ * 4] - fs[i_32_ - i_58_ * 4 - 1];
				float f_59_ = fs[i_58_ * 4 + 2] - fs[i_32_ - i_58_ * 4 - 3];
				float f_60_ = fs_55_[i_58_ * 2];
				float f_61_ = fs_55_[i_58_ * 2 + 1];
				fs[i_32_ - i_58_ * 4 - 1] = f * f_60_ - f_59_ * f_61_;
				fs[i_32_ - i_58_ * 4 - 3] = f * f_61_ + f_59_ * f_60_;
			}
			for (int i_62_ = 0; i_62_ < i_52_; i_62_++) {
				float f = fs[i_50_ + 3 + i_62_ * 4];
				float f_63_ = fs[i_50_ + 1 + i_62_ * 4];
				float f_64_ = fs[i_62_ * 4 + 3];
				float f_65_ = fs[i_62_ * 4 + 1];
				fs[i_50_ + 3 + i_62_ * 4] = f + f_64_;
				fs[i_50_ + 1 + i_62_ * 4] = f_63_ + f_65_;
				float f_66_ = fs_55_[i_50_ - 4 - i_62_ * 4];
				float f_67_ = fs_55_[i_50_ - 3 - i_62_ * 4];
				fs[i_62_ * 4 + 3] = (f - f_64_) * f_66_ - (f_63_ - f_65_) * f_67_;
				fs[i_62_ * 4 + 1] = (f_63_ - f_65_) * f_66_ + (f - f_64_) * f_67_;
			}
			int i_68_ = Class296_Sub29_Sub2.method2694((byte) 116, i_32_ - 1);
			for (int i_69_ = 0; i_69_ < i_68_ - 3; i_69_++) {
				int i_70_ = i_32_ >> i_69_ + 2;
				int i_71_ = 8 << i_69_;
				for (int i_72_ = 0; i_72_ < 2 << i_69_; i_72_++) {
					int i_73_ = i_32_ - i_70_ * 2 * i_72_;
					int i_74_ = i_32_ - i_70_ * (i_72_ * 2 + 1);
					for (int i_75_ = 0; i_75_ < i_32_ >> i_69_ + 4; i_75_++) {
						int i_76_ = i_75_ * 4;
						float f = fs[i_73_ - 1 - i_76_];
						float f_77_ = fs[i_73_ - 3 - i_76_];
						float f_78_ = fs[i_74_ - 1 - i_76_];
						float f_79_ = fs[i_74_ - 3 - i_76_];
						fs[i_73_ - 1 - i_76_] = f + f_78_;
						fs[i_73_ - 3 - i_76_] = f_77_ + f_79_;
						float f_80_ = fs_55_[i_75_ * i_71_];
						float f_81_ = fs_55_[i_75_ * i_71_ + 1];
						fs[i_74_ - 1 - i_76_] = (f - f_78_) * f_80_ - (f_77_ - f_79_) * f_81_;
						fs[i_74_ - 3 - i_76_] = (f_77_ - f_79_) * f_80_ + (f - f_78_) * f_81_;
					}
				}
			}
			for (int i_82_ = 1; i_82_ < i_52_ - 1; i_82_++) {
				int i_83_ = is[i_82_];
				if (i_82_ < i_83_) {
					int i_84_ = i_82_ * 8;
					int i_85_ = i_83_ * 8;
					float f = fs[i_84_ + 1];
					fs[i_84_ + 1] = fs[i_85_ + 1];
					fs[i_85_ + 1] = f;
					f = fs[i_84_ + 3];
					fs[i_84_ + 3] = fs[i_85_ + 3];
					fs[i_85_ + 3] = f;
					f = fs[i_84_ + 5];
					fs[i_84_ + 5] = fs[i_85_ + 5];
					fs[i_85_ + 5] = f;
					f = fs[i_84_ + 7];
					fs[i_84_ + 7] = fs[i_85_ + 7];
					fs[i_85_ + 7] = f;
				}
			}
			for (int i_86_ = 0; i_86_ < i_50_; i_86_++)
				fs[i_86_] = fs[i_86_ * 2 + 1];
			for (int i_87_ = 0; i_87_ < i_52_; i_87_++) {
				fs[i_32_ - 1 - i_87_ * 2] = fs[i_87_ * 4];
				fs[i_32_ - 2 - i_87_ * 2] = fs[i_87_ * 4 + 1];
				fs[i_32_ - i_51_ - 1 - i_87_ * 2] = fs[i_87_ * 4 + 2];
				fs[i_32_ - i_51_ - 2 - i_87_ * 2] = fs[i_87_ * 4 + 3];
			}
			for (int i_88_ = 0; i_88_ < i_52_; i_88_++) {
				float f = fs_57_[i_88_ * 2];
				float f_89_ = fs_57_[i_88_ * 2 + 1];
				float f_90_ = fs[i_50_ + i_88_ * 2];
				float f_91_ = fs[i_50_ + i_88_ * 2 + 1];
				float f_92_ = fs[i_32_ - 2 - i_88_ * 2];
				float f_93_ = fs[i_32_ - 1 - i_88_ * 2];
				float f_94_ = f_89_ * (f_90_ - f_92_) + f * (f_91_ + f_93_);
				fs[i_50_ + i_88_ * 2] = (f_90_ + f_92_ + f_94_) * 0.5F;
				fs[i_32_ - 2 - i_88_ * 2] = (f_90_ + f_92_ - f_94_) * 0.5F;
				f_94_ = f_89_ * (f_91_ + f_93_) - f * (f_90_ - f_92_);
				fs[i_50_ + i_88_ * 2 + 1] = (f_91_ - f_93_ + f_94_) * 0.5F;
				fs[i_32_ - 1 - i_88_ * 2] = (-f_91_ + f_93_ + f_94_) * 0.5F;
			}
			for (int i_95_ = 0; i_95_ < i_51_; i_95_++) {
				fs[i_95_] = (fs[i_95_ * 2 + i_50_] * fs_56_[i_95_ * 2] + fs[i_95_ * 2 + 1 + i_50_] * fs_56_[i_95_ * 2 + 1]);
				fs[i_50_ - 1 - i_95_] = (fs[i_95_ * 2 + i_50_] * fs_56_[i_95_ * 2 + 1] - fs[i_95_ * 2 + 1 + i_50_] * fs_56_[i_95_ * 2]);
			}
			for (int i_96_ = 0; i_96_ < i_51_; i_96_++)
				fs[i_32_ - i_51_ + i_96_] = -fs[i_96_];
			for (int i_97_ = 0; i_97_ < i_51_; i_97_++)
				fs[i_97_] = fs[i_51_ + i_97_];
			for (int i_98_ = 0; i_98_ < i_51_; i_98_++)
				fs[i_51_ + i_98_] = -fs[i_51_ - i_98_ - 1];
			for (int i_99_ = 0; i_99_ < i_51_; i_99_++)
				fs[i_50_ + i_99_] = fs[i_32_ - i_99_ - 1];
			for (int i_100_ = i_36_; i_100_ < i_37_; i_100_++) {
				float f = (float) Math.sin(((double) (i_100_ - i_36_) + 0.5) / (double) i_38_ * 0.5 * 3.141592653589793);
				aFloatArray4709[i_100_] *= (float) Math.sin((double) f * 1.5707963267948966 * (double) f);
			}
			for (int i_101_ = i_39_; i_101_ < i_40_; i_101_++) {
				float f = (float) Math.sin((((double) (i_101_ - i_39_) + 0.5) / (double) i_41_ * 0.5 * 3.141592653589793) + 1.5707963267948966);
				aFloatArray4709[i_101_] *= (float) Math.sin((double) f * 1.5707963267948966 * (double) f);
			}
		}
		float[] fs = null;
		if (anInt4691 > 0) {
			int i_102_ = anInt4691 + i_32_ >> 2;
			fs = new float[i_102_];
			if (!aBoolean4685) {
				for (int i_103_ = 0; i_103_ < anInt4704; i_103_++) {
					int i_104_ = (anInt4691 >> 1) + i_103_;
					fs[i_103_] += aFloatArray4699[i_104_];
				}
			}
			if (!bool_44_) {
				for (int i_105_ = i_36_; i_105_ < i_32_ >> 1; i_105_++) {
					int i_106_ = fs.length - (i_32_ >> 1) + i_105_;
					fs[i_106_] += aFloatArray4709[i_105_];
				}
			}
		}
		float[] fs_107_ = aFloatArray4699;
		aFloatArray4699 = aFloatArray4709;
		aFloatArray4709 = fs_107_;
		anInt4691 = i_32_;
		anInt4704 = i_40_ - (i_32_ >> 1);
		aBoolean4685 = bool_44_;
		return fs;
	}

	static final float method2646(int i) {
		int i_108_ = i & 0x1fffff;
		int i_109_ = i & ~0x7fffffff;
		int i_110_ = (i & 0x7fe00000) >> 21;
		if (i_109_ != 0)
			i_108_ = -i_108_;
		return (float) ((double) i_108_ * Math.pow(2.0, (double) (i_110_ - 788)));
	}

	static final Class296_Sub18 method2647(Js5 class138, int i, int i_111_) {
		if (!method2643(class138)) {
			class138.fileExists(i, i_111_);
			return null;
		}
		byte[] is = class138.getFile(i, i_111_);
		if (is == null)
			return null;
		return new Class296_Sub18(is);
	}

	private static final void method2648(byte[] is, int i) {
		aByteArray4698 = is;
		anInt4682 = i;
		anInt4686 = 0;
	}

	static final int method2649() {
		int i = aByteArray4698[anInt4682] >> anInt4686 & 0x1;
		anInt4686++;
		anInt4682 += anInt4686 >> 3;
		anInt4686 &= 0x7;
		return i;
	}

	private Class296_Sub18(byte[] is) {
		method2650(is);
	}

	private final void method2650(byte[] is) {
		Packet class296_sub17 = new Packet(is);
		anInt4680 = class296_sub17.g4();
		anInt4705 = class296_sub17.g4();
		anInt4689 = class296_sub17.g4();
		anInt4693 = class296_sub17.g4();
		if (anInt4693 < 0) {
			anInt4693 = anInt4693 ^ 0xffffffff;
			aBoolean4695 = true;
		}
		int i = class296_sub17.g4();
		aByteArrayArray4696 = new byte[i][];
		for (int i_112_ = 0; i_112_ < i; i_112_++) {
			int i_113_ = 0;
			int i_114_;
			do {
				i_114_ = class296_sub17.g1();
				i_113_ += i_114_;
			} while (i_114_ >= 255);
			byte[] is_115_ = new byte[i_113_];
			class296_sub17.readBytes(is_115_, 0, i_113_);
			aByteArrayArray4696[i_112_] = is_115_;
		}
	}

	public static void method2651() {
		aByteArray4698 = null;
		aClass346Array4683 = null;
		aClass74Array4690 = null;
		aClass66Array4692 = null;
		aClass222Array4706 = null;
		aBooleanArray4694 = null;
		anIntArray4684 = null;
		aFloatArray4709 = null;
		aFloatArray4697 = null;
		aFloatArray4703 = null;
		aFloatArray4707 = null;
		aFloatArray4712 = null;
		aFloatArray4688 = null;
		aFloatArray4687 = null;
		anIntArray4701 = null;
		anIntArray4710 = null;
	}
}
