package net.zaros.client;
import jaggl.OpenGL;

class Class69_Sub1 extends Class69 {
	private int anInt5710 = -1;
	int anInt5711;
	static int anInt5712;
	static int anInt5713 = 0;
	private int anInt5714 = -1;
	int anInt5715;

	final void method728(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, byte i_5_) {
		int i_6_ = -i_2_ + (-i_1_ + aHa_Sub3_3681.anInt4113);
		aHa_Sub3_3681.method1316(this, (byte) -107);
		int i_7_ = -119 / ((i_5_ - 5) / 57);
		OpenGL.glCopyTexSubImage2D(anInt3682, 0, i, -i_4_ - i_2_ + anInt5715, i_3_, i_6_, i_0_, i_2_);
		OpenGL.glFlush();
	}

	final void method729(int i, int i_8_, int i_9_, int i_10_) {
		OpenGL.glFramebufferTexture2DEXT(i, i_8_, anInt3682, anInt3680, i_9_);
		anInt5710 = i;
		anInt5714 = i_8_;
		if (i_10_ < 44)
			method730(64, -100, 13, -123, false, null, -42, -100, -72);
	}

	final void method730(int i, int i_11_, int i_12_, int i_13_, boolean bool, int[] is, int i_14_, int i_15_, int i_16_) {
		if (i_14_ == 0)
			i_14_ = i_13_;
		if (bool) {
			int[] is_17_ = new int[i_13_ * i_15_];
			for (int i_18_ = 0; i_18_ < i_15_; i_18_++) {
				int i_19_ = i_13_ * i_18_;
				int i_20_ = i_14_ * (i_15_ - 1 - i_18_) + i_16_;
				for (int i_21_ = 0; i_21_ < i_13_; i_21_++)
					is_17_[i_19_++] = is[i_20_++];
			}
			is = is_17_;
		}
		aHa_Sub3_3681.method1316(this, (byte) -119);
		if (i_13_ != i_14_)
			OpenGL.glPixelStorei(3314, i_14_);
		OpenGL.glTexSubImage2Di(anInt3682, i, i_12_, anInt5715 - i_11_ - i_15_, i_13_, i_15_, 32993, aHa_Sub3_3681.anInt4231, is, i_16_);
		if (i_14_ != i_13_)
			OpenGL.glPixelStorei(3314, 0);
	}

	Class69_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_22_, int i_23_, int i_24_, boolean bool, float[] fs, int i_25_) {
		super(var_ha_Sub3, i, i_22_, i_23_ * i_24_, bool);
		anInt5715 = i_24_;
		anInt5711 = i_23_;
		aHa_Sub3_3681.method1316(this, (byte) -117);
		if (bool && anInt3682 != 34037) {
			Class92.method849(i_24_, i_23_, i, i_22_, -1, i_25_, fs);
			this.method718(true, (byte) 24);
		} else {
			OpenGL.glTexImage2Df(anInt3682, 0, anInt3685, anInt5711, anInt5715, 0, i_25_, 5126, fs, 0);
			this.method718(false, (byte) 24);
		}
		this.method723(95, true);
	}

	Class69_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_26_, int i_27_, int i_28_, boolean bool, int[] is, int i_29_, int i_30_, boolean bool_31_) {
		super(var_ha_Sub3, i, i_26_, i_28_ * i_27_, bool);
		anInt5715 = i_28_;
		anInt5711 = i_27_;
		if (bool_31_) {
			int[] is_32_ = new int[is.length];
			for (int i_33_ = 0; i_33_ < i_28_; i_33_++) {
				int i_34_ = i_33_ * i_27_;
				int i_35_ = (-i_33_ + i_28_ - 1) * i_27_;
				for (int i_36_ = 0; i_27_ > i_36_; i_36_++)
					is_32_[i_34_++] = is[i_35_++];
			}
			is = is_32_;
		}
		aHa_Sub3_3681.method1316(this, (byte) -109);
		if (anInt3682 != 34037 && bool && i_29_ == 0 && i_30_ == 0) {
			Class404.method4165(anInt5711, aHa_Sub3_3681.anInt4231, anInt5715, 32993, 0, anInt3682, is, anInt3685);
			this.method718(true, (byte) 24);
		} else {
			OpenGL.glPixelStorei(3314, i_29_);
			OpenGL.glTexImage2Di(anInt3682, 0, anInt3685, anInt5711, anInt5715, 0, 32993, aHa_Sub3_3681.anInt4231, is, i_30_ * 4);
			OpenGL.glPixelStorei(3314, 0);
			this.method718(false, (byte) 24);
		}
		this.method723(101, true);
	}

	Class69_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_37_, int i_38_, int i_39_) {
		super(var_ha_Sub3, i, i_37_, i_39_ * i_38_, false);
		anInt5715 = i_39_;
		anInt5711 = i_38_;
		aHa_Sub3_3681.method1316(this, (byte) -123);
		OpenGL.glTexImage2Dub(anInt3682, 0, anInt3685, i_38_, i_39_, 0, Class296_Sub39_Sub18.method2897((byte) 61, anInt3685), 5121, null, 0);
		this.method723(103, true);
	}

	Class69_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_40_, int i_41_, int i_42_, int i_43_) {
		super(var_ha_Sub3, i, 6407, i_42_ * i_43_, false);
		anInt5711 = i_42_;
		anInt5715 = i_43_;
		int i_44_ = aHa_Sub3_3681.anInt4113 - i_43_ - i_41_;
		aHa_Sub3_3681.method1316(this, (byte) -126);
		OpenGL.glCopyTexImage2D(anInt3682, 0, anInt3685, i_40_, i_44_, i_42_, i_43_, 0);
		this.method723(89, true);
	}

	final void method731(byte i, int i_45_, int i_46_, int i_47_, int i_48_, byte[] is, boolean bool, int i_49_, int i_50_, int i_51_) {
		if (i_49_ == 0)
			i_49_ = i_47_;
		if (bool) {
			int i_52_ = Mobile.method3502(i_50_, i ^ 0x8054);
			int i_53_ = i_52_ * i_47_;
			int i_54_ = i_49_ * i_52_;
			byte[] is_55_ = new byte[i_48_ * i_53_];
			for (int i_56_ = 0; i_56_ < i_48_; i_56_++) {
				int i_57_ = i_56_ * i_53_;
				int i_58_ = (i_48_ - i_56_ - 1) * i_54_ + i_46_;
				for (int i_59_ = 0; i_53_ > i_59_; i_59_++)
					is_55_[i_57_++] = is[i_58_++];
			}
			is = is_55_;
		}
		aHa_Sub3_3681.method1316(this, (byte) -126);
		OpenGL.glPixelStorei(3317, 1);
		if (i_49_ != i_47_)
			OpenGL.glPixelStorei(3314, i_49_);
		OpenGL.glTexSubImage2Dub(anInt3682, 0, i_45_, i_51_, i_47_, i_48_, i_50_, 5121, is, i_46_);
		if (i == -30) {
			if (i_49_ != i_47_)
				OpenGL.glPixelStorei(3314, 0);
			OpenGL.glPixelStorei(3317, 4);
		}
	}

	static final void method732(boolean bool) {
		if (bool) {
			Class338_Sub2.aClass247ArrayArrayArray5195 = Class351.aClass247ArrayArrayArray3041;
			Class360_Sub2.aSArray5304 = Class52.aSArray636;
		} else {
			Class338_Sub2.aClass247ArrayArrayArray5195 = Class399.aClass247ArrayArrayArray3355;
			Class360_Sub2.aSArray5304 = Class244.aSArray2320;
		}
		Class368_Sub9.anInt5477 = Class338_Sub2.aClass247ArrayArrayArray5195.length;
	}

	final void method733(boolean bool, boolean bool_60_, boolean bool_61_) {
		if (anInt3682 == 3553) {
			aHa_Sub3_3681.method1316(this, (byte) -118);
			OpenGL.glTexParameteri(anInt3682, 10242, !bool_60_ ? 33071 : 10497);
			OpenGL.glTexParameteri(anInt3682, 10243, bool ? 10497 : 33071);
		}
		if (bool_61_ != true)
			method731((byte) 78, 0, -126, 31, -75, null, false, 19, -3, 79);
	}

	Class69_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_62_, int i_63_, int i_64_, boolean bool, byte[] is, int i_65_, boolean bool_66_) {
		super(var_ha_Sub3, i, i_62_, i_63_ * i_64_, bool);
		anInt5715 = i_64_;
		anInt5711 = i_63_;
		if (bool_66_) {
			byte[] is_67_ = new byte[is.length];
			for (int i_68_ = 0; i_68_ < i_64_; i_68_++) {
				int i_69_ = i_63_ * i_68_;
				int i_70_ = i_63_ * (i_64_ + (-i_68_ - 1));
				for (int i_71_ = 0; i_71_ < i_63_; i_71_++)
					is_67_[i_69_++] = is[i_70_++];
			}
			is = is_67_;
		}
		aHa_Sub3_3681.method1316(this, (byte) -106);
		OpenGL.glPixelStorei(3317, 1);
		if (!bool || anInt3682 == 34037) {
			OpenGL.glTexImage2Dub(anInt3682, 0, anInt3685, anInt5711, anInt5715, 0, i_65_, 5121, is, 0);
			this.method718(false, (byte) 24);
		} else {
			StaticMethods.method2777(i_64_, i, 1, i_63_, i_65_, i_62_, is);
			this.method718(true, (byte) 24);
		}
		OpenGL.glPixelStorei(3317, 4);
		this.method723(96, true);
	}

	public final void method73(boolean bool) {
		OpenGL.glFramebufferTexture2DEXT(anInt5710, anInt5714, anInt3682, 0, 0);
		anInt5714 = -1;
		if (bool == true)
			anInt5710 = -1;
	}
}
