package net.zaros.client;

import jaclib.memory.Buffer;
import jaclib.memory.Stream;

final class Class55_Sub2 extends Class55 {
	private Interface6_Impl1 anInterface6_Impl1_3870;
	private ha_Sub1 aHa_Sub1_3871;
	private Interface15_Impl2 anInterface15_Impl2_3872;
	private boolean aBoolean3873;

	public void a(char c, int i, int i_0_, int i_1_, boolean bool, aa var_aa, int i_2_, int i_3_) {
		/* empty */
	}

	Class55_Sub2(ha_Sub1 var_ha_Sub1, Class92 class92, Class186[] class186s, boolean bool) {
		super(var_ha_Sub1, class92);
		aHa_Sub1_3871 = var_ha_Sub1;
		int i = 0;
		for (int i_4_ = 0; i_4_ < 256; i_4_++) {
			Class186 class186 = class186s[i_4_];
			if (class186.anInt1904 > i)
				i = class186.anInt1904;
			if (class186.anInt1899 > i)
				i = class186.anInt1899;
		}
		int i_5_ = i * 16;
		if (bool) {
			byte[] is = new byte[i_5_ * i_5_];
			for (int i_6_ = 0; i_6_ < 256; i_6_++) {
				Class186 class186 = class186s[i_6_];
				int i_7_ = class186.anInt1904;
				int i_8_ = class186.anInt1899;
				int i_9_ = i_6_ % 16 * i;
				int i_10_ = i_6_ / 16 * i;
				int i_11_ = i_10_ * i_5_ + i_9_;
				int i_12_ = 0;
				if (class186.aByteArray1905 == null) {
					byte[] is_13_ = class186.aByteArray1901;
					for (int i_14_ = 0; i_14_ < i_7_; i_14_++) {
						for (int i_15_ = 0; i_15_ < i_8_; i_15_++)
							is[i_11_++] = (byte) (is_13_[i_12_++] == 0 ? 0 : -1);
						i_11_ += i_5_ - i_8_;
					}
				} else {
					byte[] is_16_ = class186.aByteArray1905;
					for (int i_17_ = 0; i_17_ < i_7_; i_17_++) {
						for (int i_18_ = 0; i_18_ < i_8_; i_18_++)
							is[i_11_++] = is_16_[i_12_++];
						i_11_ += i_5_ - i_8_;
					}
				}
			}
			if (!var_ha_Sub1.method1200(Class67.aClass67_745, Class13.aClass202_3516, (byte) 53)) {
				int[] is_19_ = new int[is.length];
				for (int i_20_ = 0; i_20_ < is.length; i_20_++)
					is_19_[i_20_] = is[i_20_] << 24;
				anInterface6_Impl1_3870 = var_ha_Sub1.method1179(i_5_, i_5_, false, is_19_, -461);
			} else
				anInterface6_Impl1_3870 = var_ha_Sub1.method1180(false, 7, Class13.aClass202_3516, is, i_5_, i_5_);
			aBoolean3873 = true;
		} else {
			int[] is = new int[i_5_ * i_5_];
			for (int i_21_ = 0; i_21_ < 256; i_21_++) {
				Class186 class186 = class186s[i_21_];
				int[] is_22_ = class186.anIntArray1900;
				byte[] is_23_ = class186.aByteArray1905;
				byte[] is_24_ = class186.aByteArray1901;
				int i_25_ = class186.anInt1904;
				int i_26_ = class186.anInt1899;
				int i_27_ = i_21_ % 16 * i;
				int i_28_ = i_21_ / 16 * i;
				int i_29_ = i_28_ * i_5_ + i_27_;
				int i_30_ = 0;
				if (is_23_ != null) {
					for (int i_31_ = 0; i_31_ < i_25_; i_31_++) {
						for (int i_32_ = 0; i_32_ < i_26_; i_32_++) {
							is[i_29_++] = (is_23_[i_30_] << 24 | is_22_[is_24_[i_30_] & 0xff]);
							i_30_++;
						}
						i_29_ += i_5_ - i_26_;
					}
				} else {
					for (int i_33_ = 0; i_33_ < i_25_; i_33_++) {
						for (int i_34_ = 0; i_34_ < i_26_; i_34_++) {
							int i_35_;
							if ((i_35_ = is_24_[i_30_++]) != 0)
								is[i_29_++] = is_22_[i_35_ & 0xff] | ~0xffffff;
							else
								i_29_++;
						}
						i_29_ += i_5_ - i_26_;
					}
				}
			}
			anInterface6_Impl1_3870 = var_ha_Sub1.method1179(i_5_, i_5_, false, is, -461);
			aBoolean3873 = false;
		}
		anInterface6_Impl1_3870.method29(aa_Sub2.aClass131_3724, 16518);
		anInterface15_Impl2_3872 = var_ha_Sub1.method1205(false, -79);
		anInterface15_Impl2_3872.method46(20480, 72, 20);
		for (int i_36_ = 0; i_36_ < 4; i_36_++) {
			Buffer buffer = anInterface15_Impl2_3872.method47(true, -8102);
			if (buffer != null) {
				Stream stream = aHa_Sub1_3871.method1156(buffer, -118);
				float f = (anInterface6_Impl1_3870.method69(-87, (float) i_5_) / (float) i_5_);
				float f_37_ = (anInterface6_Impl1_3870.method71((float) i_5_, (byte) -91) / (float) i_5_);
				for (int i_38_ = 0; i_38_ < 256; i_38_++) {
					Class186 class186 = class186s[i_38_];
					int i_39_ = class186.anInt1904;
					int i_40_ = class186.anInt1899;
					int i_41_ = class186.anInt1906;
					int i_42_ = class186.anInt1903;
					float f_43_ = (float) (i_38_ % 16 * i);
					float f_44_ = (float) (i_38_ / 16 * i);
					float f_45_ = f_43_ * f;
					float f_46_ = f_44_ * f_37_;
					float f_47_ = (f_43_ + (float) i_40_) * f;
					float f_48_ = (f_44_ + (float) i_39_) * f_37_;
					if (Stream.a()) {
						stream.a((float) i_42_);
						stream.a((float) i_41_);
						stream.a(0.0F);
						stream.a(f_45_);
						stream.a(f_46_);
						stream.a((float) i_42_);
						stream.a((float) (i_41_ + i_39_));
						stream.a(0.0F);
						stream.a(f_45_);
						stream.a(f_48_);
						stream.a((float) (i_42_ + i_40_));
						stream.a((float) (i_41_ + i_39_));
						stream.a(0.0F);
						stream.a(f_47_);
						stream.a(f_48_);
						stream.a((float) (i_42_ + i_40_));
						stream.a((float) i_41_);
						stream.a(0.0F);
						stream.a(f_47_);
						stream.a(f_46_);
					} else {
						stream.b((float) i_42_);
						stream.b((float) i_41_);
						stream.b(0.0F);
						stream.b(f_45_);
						stream.b(f_46_);
						stream.b((float) i_42_);
						stream.b((float) (i_41_ + i_39_));
						stream.b(0.0F);
						stream.b(f_45_);
						stream.b(f_48_);
						stream.b((float) (i_42_ + i_40_));
						stream.b((float) (i_41_ + i_39_));
						stream.b(0.0F);
						stream.b(f_47_);
						stream.b(f_48_);
						stream.b((float) (i_42_ + i_40_));
						stream.b((float) i_41_);
						stream.b(0.0F);
						stream.b(f_47_);
						stream.b(f_46_);
					}
				}
				stream.b();
				if (anInterface15_Impl2_3872.method49(2968))
					break;
			}
		}
	}

	public void fa(char c, int i, int i_49_, int i_50_, boolean bool) {
		aHa_Sub1_3871.method1167((byte) -96);
		aHa_Sub1_3871.method1140(anInterface6_Impl1_3870, false);
		if (aBoolean3873 || bool) {
			aHa_Sub1_3871.method1177(Class41_Sub4.aClass125_3745, 9815, Js5.aClass125_1411);
			aHa_Sub1_3871.method1158((byte) -104, 0, Class151.aClass287_1553);
			aHa_Sub1_3871.method1139(0, i_50_);
		} else
			aHa_Sub1_3871.method1177(Class41_Sub4.aClass125_3745, 9815, Class41_Sub4.aClass125_3745);
		Class373_Sub2 class373_sub2 = aHa_Sub1_3871.method1215(-88);
		class373_sub2.method3902(i, i_49_, 0);
		aHa_Sub1_3871.method1202(0);
		aHa_Sub1_3871.method1178(anInterface15_Impl2_3872, (byte) -120, 0);
		aHa_Sub1_3871.method1209(aHa_Sub1_3871.aClass127_4059, (byte) -99);
		aHa_Sub1_3871.method1185(0, c * '\004', aa_Sub2.aClass289_3730, 2);
		if (aBoolean3873 || bool)
			aHa_Sub1_3871.method1158((byte) -127, 0, Class199.aClass287_2007);
	}
}
