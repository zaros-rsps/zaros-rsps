package net.zaros.client;

/* Class279 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class279 {
	private AdvancedMemoryCache aClass113_2546 = new AdvancedMemoryCache(16);
	static int anInt2547;
	private Js5 aClass138_2548;
	static int anInt2549;

	private final Class71 method2337(int i, int i_0_) {
		Class71 class71;
		synchronized (aClass113_2546) {
			class71 = (Class71) aClass113_2546.get((long) i);
		}
		if (class71 != null)
			return class71;
		byte[] is;
		synchronized (aClass138_2548) {
			is = aClass138_2548.getFile(29, i);
		}
		class71 = new Class71();
		if (is != null)
			class71.method770(new Packet(is), (byte) -93);
		if (i_0_ != 0)
			method2339((byte) 110);
		synchronized (aClass113_2546) {
			aClass113_2546.put(class71, (long) i);
		}
		return class71;
	}

	final void method2338(int i, boolean bool) {
		synchronized (aClass113_2546) {
			aClass113_2546.clean(i);
		}
		if (bool)
			method2341((byte) -103);
	}

	final void method2339(byte i) {
		synchronized (aClass113_2546) {
			aClass113_2546.clear();
		}
		if (i != -111) {
			/* empty */
		}
	}

	final Class262 method2340(int i, boolean bool, Class182 class182, int i_1_, int i_2_, int i_3_) {
		if (bool != true)
			return null;
		Class174[] class174s = null;
		Class71 class71 = method2337(i_3_, 0);
		if (class71.anIntArray843 != null) {
			class174s = new Class174[class71.anIntArray843.length];
			for (int i_4_ = 0; i_4_ < class174s.length; i_4_++) {
				Class187 class187 = class182.method1850(class71.anIntArray843[i_4_], false);
				class174s[i_4_] = new Class174(class187.anInt1914, class187.anInt1917, class187.anInt1911, class187.anInt1913, class187.anInt1916, class187.anInt1921, class187.anInt1909, class187.aBoolean1908, class187.anInt1915, class187.anInt1918, class187.anInt1919);
			}
		}
		return new Class262(class71.anInt842, class174s, class71.anInt839, i, i_1_, i_2_, class71.anInt840, class71.anInt841);
	}

	final void method2341(byte i) {
		synchronized (aClass113_2546) {
			aClass113_2546.clearSoftReferences();
			if (i != -107)
				method2338(69, false);
		}
	}

	Class279(GameType class35, int i, Js5 class138) {
		aClass138_2548 = class138;
		aClass138_2548.getLastFileId(29);
	}
}
