package net.zaros.client;

/* Class296_Sub56 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub56 extends Node {
	NodeDeque aClass155_5080 = new NodeDeque();
	static int[] anIntArray5081 = new int[8];

	public static void method3216(int i) {
		if (i != -2776)
			anIntArray5081 = null;
		anIntArray5081 = null;
	}

	static final boolean method3217(int i, int i_0_, int i_1_) {
		if (!Class318.aBoolean2814)
			return false;
		int i_2_ = i_1_ >> 16;
		int i_3_ = i_1_ & 0xffff;
		if (Class192.openedInterfaceComponents[i_2_] == null || Class192.openedInterfaceComponents[i_2_][i_3_] == null)
			return false;
		if (i_0_ != -1007)
			return false;
		InterfaceComponent class51 = Class192.openedInterfaceComponents[i_2_][i_3_];
		if (i != -1 || class51.type != 0) {
			for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeFirst((byte) 125); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeNext(1001)) {
				if (class296_sub39_sub9.anInt6172 == i && class296_sub39_sub9.anInt6164 == class51.uid && (class296_sub39_sub9.anInt6165 == 20 || class296_sub39_sub9.anInt6165 == 1006 || class296_sub39_sub9.anInt6165 == 53 || class296_sub39_sub9.anInt6165 == 23 || class296_sub39_sub9.anInt6165 == 30))
					return true;
			}
		} else {
			for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeFirst((byte) 121); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeNext(1001)) {
				if (class296_sub39_sub9.anInt6165 == 20 || class296_sub39_sub9.anInt6165 == 1006 || class296_sub39_sub9.anInt6165 == 53 || class296_sub39_sub9.anInt6165 == 23 || class296_sub39_sub9.anInt6165 == 30) {
					for (InterfaceComponent class51_4_ = InterfaceComponent.getInterfaceComponent((class296_sub39_sub9.anInt6164)); class51_4_ != null; class51_4_ = Class181_Sub1.method1834(class51_4_, -2)) {
						if (class51_4_.uid == class51.uid)
							return true;
					}
				}
			}
		}
		return false;
	}

	static final boolean method3218(int i, int i_5_) {
		if (i != 8800)
			method3216(-36);
		if (i_5_ < 4 || i_5_ > 8)
			return false;
		return true;
	}

	public Class296_Sub56() {
		/* empty */
	}
}
