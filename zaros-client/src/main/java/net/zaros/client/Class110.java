package net.zaros.client;

/* Class110 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class110 {
	static Class294 aClass294_1140;
	static boolean[][] aBooleanArrayArray1142 = {new boolean[13], {false, false, true, true, true, true, true, false, false, false, false, false, true}, {true, true, true, true, true, true, false, false, false, false, false, false, false}, {true, true, true, false, false, true, true, true, false, false, false, false, false}, {true, false, false, false, false, true, true, true, false, false, false, false, false}, {false, false, true, true, true, true, false, false, false, false, false, false, false}, {false, true, true, true, true, true, false, false, false, false, false, false, true}, {false, true, true, true, true, true, true, true, false, false, false, false, true}, {true, true, false, false, false, false, false, true, false, false, false, false, false}, {true, true, true, true, true, false, false, false, true, true, false, false, false}, {true, false, false, false, true, true, true, true, true, true, false, false, false}, {true, false, true, true, true, true, true, true, false, false, true, true, false}, {true, true, true, true, true, true, true, true, true, true, true, true, true}, new boolean[13], {true, true, true, true, true, true, true, true, true, true, true, true, true}};
	static AdvancedMemoryCache aClass113_1143;

	public static void method967(int i) {
		aClass113_1143 = null;
		aBooleanArrayArray1142 = null;
		aClass294_1140 = null;
		if (i == 20524)
			RouteFinder.distances = null;
	}

	static final void method968(boolean bool) {
		if (bool)
			method969(-76, 10, -99, -97, -46, -43);
		Class292.method2411(-32048);
		Class368_Sub1.method3810(-48001, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub24_5016.method494(118) == 1, 2, 22050);
		Class274.aClass381_2524 = Class338_Sub3_Sub5_Sub1.method3585(Class230.aCanvas2209, 0, 22050, -14959, Class252.aClass398_2383);
		Class123_Sub2.method1071(Class211.method2016(-24345, null), true, !bool);
		Class325.aClass381_2869 = Class338_Sub3_Sub5_Sub1.method3585(Class230.aCanvas2209, 1, 2048, -14959, Class252.aClass398_2383);
		Class325.aClass381_2869.method3996(117, Class16_Sub3.aClass296_Sub45_Sub5_3734);
	}

	static final void method969(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		if (i_1_ != 9404)
			method969(-63, 107, 99, -114, 45, -85);
		Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i++]), i_4_, (byte) -85, i_0_, i_2_);
		Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_3_--]), i_4_, (byte) 118, i_0_, i_2_);
		for (int i_5_ = i; i_5_ <= i_3_; i_5_++) {
			int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_5_];
			is[i_2_] = is[i_4_] = i_0_;
		}
	}

	static final Class357[] method970(int i) {
		if (i != 0)
			aClass113_1143 = null;
		return (new Class357[]{Class296_Sub50.aClass357_5028, r_Sub2.aClass357_6717, Class368_Sub18.aClass357_5534});
	}

	static final void method971(String string, int i, boolean bool) {
		if (string != null) {
			if (Class317.ignoresSize >= 100)
				Class34.method351(4, true, TranslatableString.aClass120_1235.getTranslation(Class394.langID));
			else {
				String string_6_ = Class296_Sub49.parseName(string);
				if (string_6_ != null) {
					for (int i_7_ = 0; Class317.ignoresSize > i_7_; i_7_++) {
						String string_8_ = Class296_Sub49.parseName((Class362.ignoreNames1[i_7_]));
						if (string_8_ != null && string_8_.equals(string_6_)) {
							Class34.method351(4, true, string + (TranslatableString.aClass120_1236.getTranslation(Class394.langID)));
							return;
						}
						if (Class296_Sub39_Sub11.ignoreNames3[i_7_] != null) {
							String string_9_ = (Class296_Sub49.parseName((Class296_Sub39_Sub11.ignoreNames3[i_7_])));
							if (string_9_ != null && string_9_.equals(string_6_)) {
								Class34.method351(4, true, (string + (TranslatableString.aClass120_1236.getTranslation(Class394.langID))));
								return;
							}
						}
					}
					for (int i_10_ = i; Class285.anInt2625 > i_10_; i_10_++) {
						String string_11_ = Class296_Sub49.parseName((Js5TextureLoader.aStringArray3443[i_10_]));
						if (string_11_ != null && string_11_.equals(string_6_)) {
							Class34.method351(4, true, ((TranslatableString.aClass120_1241.getTranslation(Class394.langID)) + string + (TranslatableString.aClass120_1242.getTranslation(Class394.langID))));
							return;
						}
						if (Class338_Sub9.aStringArray5268[i_10_] != null) {
							String string_12_ = Class296_Sub49.parseName((Class338_Sub9.aStringArray5268[i_10_]));
							if (string_12_ != null && string_12_.equals(string_6_)) {
								Class34.method351(4, true, ((TranslatableString.aClass120_1241.getTranslation(Class394.langID)) + string + (TranslatableString.aClass120_1242.getTranslation(Class394.langID))));
								return;
							}
						}
					}
					if (Class296_Sub49.parseName((Class296_Sub51_Sub11.localPlayer.displayName)).equals(string_6_))
						Class34.method351(4, true, (TranslatableString.aClass120_1238.getTranslation(Class394.langID)));
					else {
						Connection class204 = Class296_Sub51_Sub13.method3111(true);
						Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 122, Class317.aClass311_3699);
						class296_sub1.out.p1(Class117.method1015((byte) -91, string) + 1);
						class296_sub1.out.writeString(string);
						class296_sub1.out.p1(bool ? 1 : 0);
						class204.sendPacket(class296_sub1, (byte) 119);
					}
				}
			}
		}
	}

	static {
		aClass294_1140 = new Class294(6, 1);
		aClass113_1143 = new AdvancedMemoryCache(20);
	}
}
