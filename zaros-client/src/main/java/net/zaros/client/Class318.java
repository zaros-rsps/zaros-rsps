package net.zaros.client;

/* Class318 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class318 {
	private int anInt2806;
	private int[][] anIntArrayArray2807;
	static int anInt2808 = 0;
	static boolean aBoolean2809;
	static float aFloat2810;
	private NodeDeque aClass155_2811;
	static int removedNpcsCount = 0;
	private int anInt2813 = -1;
	static boolean aBoolean2814;
	private int anInt2815 = 0;
	private int anInt2816;
	static int anInt2817;
	private Class296_Sub37[] aClass296_Sub37Array2818;
	boolean aBoolean2819;

	final void method3334(int i) {
		for (int i_0_ = 0; i_0_ < anInt2816; i_0_++)
			anIntArrayArray2807[i_0_] = null;
		if (i < 81)
			aFloat2810 = -0.70390826F;
		aClass296_Sub37Array2818 = null;
		anIntArrayArray2807 = null;
		aClass155_2811.method1581(327680);
		aClass155_2811 = null;
	}

	final int[] method3335(int i, byte i_1_) {
		if (i_1_ != 28)
			method3334(-30);
		if (anInt2806 != anInt2816) {
			if (anInt2816 != 1) {
				Class296_Sub37 class296_sub37 = aClass296_Sub37Array2818[i];
				if (class296_sub37 != null)
					aBoolean2819 = false;
				else {
					aBoolean2819 = true;
					if (anInt2815 < anInt2816) {
						class296_sub37 = new Class296_Sub37(i, anInt2815);
						anInt2815++;
					} else {
						Class296_Sub37 class296_sub37_2_ = ((Class296_Sub37) aClass155_2811.method1569(i_1_ ^ 0x62));
						class296_sub37 = new Class296_Sub37(i, class296_sub37_2_.anInt4882);
						aClass296_Sub37Array2818[class296_sub37_2_.anInt4881] = null;
						class296_sub37_2_.unlink();
					}
					aClass296_Sub37Array2818[i] = class296_sub37;
				}
				aClass155_2811.method1570(true, class296_sub37);
				return anIntArrayArray2807[class296_sub37.anInt4882];
			}
			aBoolean2819 = anInt2813 != i;
			anInt2813 = i;
			return anIntArrayArray2807[0];
		}
		aBoolean2819 = aClass296_Sub37Array2818[i] == null;
		aClass296_Sub37Array2818[i] = Class61.aClass296_Sub37_704;
		return anIntArrayArray2807[i];
	}

	static final void method3336(byte i) {
		Class296_Sub35_Sub2.anInt6113 = (Class304.aClass92_2729.anInt992 + Class304.aClass92_2729.anInt989 + 2);
		Class41_Sub15.anInt3779 = (Class123_Sub1_Sub1.aClass92_5814.anInt992 + 2 + Class123_Sub1_Sub1.aClass92_5814.anInt989);
		Class296_Sub51_Sub15.aStringArray6425 = new String[500];
		int i_3_ = 0;
		if (i != -73)
			method3336((byte) 30);
		for (/**/; Class296_Sub51_Sub15.aStringArray6425.length > i_3_; i_3_++)
			Class296_Sub51_Sub15.aStringArray6425[i_3_] = "";
		Class41_Sub18.writeConsoleMessage((TranslatableString.aClass120_1197.getTranslation(Class394.langID)));
	}

	final int[][] method3337(int i) {
		if (anInt2806 != anInt2816)
			throw new RuntimeException("Can only retrieve a full image cache");
		int i_4_ = 0;
		if (i < 96)
			method3337(125);
		for (/**/; i_4_ < anInt2816; i_4_++)
			aClass296_Sub37Array2818[i_4_] = Class61.aClass296_Sub37_704;
		return anIntArrayArray2807;
	}

	Class318(int i, int i_5_, int i_6_) {
		aClass155_2811 = new NodeDeque();
		aBoolean2819 = false;
		anInt2806 = i_5_;
		anInt2816 = i;
		aClass296_Sub37Array2818 = new Class296_Sub37[anInt2806];
		anIntArrayArray2807 = new int[anInt2816][i_6_];
	}

	static {
		aFloat2810 = 0.25F;
		aBoolean2814 = false;
	}
}
