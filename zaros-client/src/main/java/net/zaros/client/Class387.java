package net.zaros.client;
/* Class387 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Image;

final class Class387 implements Interface21 {
	static int anInt3711 = -1;
	private Js5 aClass138_3712;
	static Image anImage3713;
	private String aString3714;

	public final int method79(int i) {
		if (aClass138_3712.hasFileBuffer_(aString3714))
			return 100;
		if (i != 20667)
			return -123;
		return aClass138_3712.getFileCompletion_(aString3714);
	}

	static final int method4034(boolean bool) {
		if (bool != true)
			method4035(80);
		if (ConfigsRegister.anInt3674 == 1)
			return Class34.anInt337;
		return 0;
	}

	public static void method4035(int i) {
		if (i != 1)
			anInt3711 = 39;
		anImage3713 = null;
	}

	public final Class388 method78(int i) {
		if (i != 20598)
			return null;
		return Class388.aClass388_3273;
	}

	Class387(Js5 class138, String string) {
		aClass138_3712 = class138;
		aString3714 = string;
	}
}
