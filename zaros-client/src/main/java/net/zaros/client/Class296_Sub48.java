package net.zaros.client;
import jaggl.OpenGL;

final class Class296_Sub48 extends Node {
	volatile String aString4974;
	static int[] anIntArray4975 = new int[1];
	volatile int anInt4976 = -1;

	public static void method3043(boolean bool) {
		anIntArray4975 = null;
		if (bool)
			method3043(false);
	}

	static final void method3044(Queuable class296_sub39, Queuable class296_sub39_0_, byte i) {
		if (i != -69)
			anIntArray4975 = null;
		if (class296_sub39.queue_previous != null)
			class296_sub39.queue_unlink();
		class296_sub39.queue_previous = class296_sub39_0_.queue_previous;
		class296_sub39.queue_next = class296_sub39_0_;
		class296_sub39.queue_previous.queue_next = class296_sub39;
		class296_sub39.queue_next.queue_previous = class296_sub39;
	}

	static final Class369 method3045(int i, byte[] is, ha_Sub1_Sub1 var_ha_Sub1_Sub1, int i_1_) {
		if (is == null)
			return null;
		int i_2_ = OpenGL.glGenProgramARB();
		OpenGL.glBindProgramARB(i_1_, i_2_);
		OpenGL.glProgramRawARB(i_1_, 34933, is);
		OpenGL.glGetIntegerv(34379, Class123.anIntArray1278, 0);
		if (Class123.anIntArray1278[i] != -1) {
			OpenGL.glBindProgramARB(i_1_, 0);
			return null;
		}
		OpenGL.glBindProgramARB(i_1_, 0);
		return new Class369(var_ha_Sub1_Sub1, i_1_, i_2_);
	}

	Class296_Sub48(String string) {
		aString4974 = string;
	}
}
