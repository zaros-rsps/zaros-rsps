package net.zaros.client;

/* Class296_Sub51_Sub28 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub28 extends TextureOperation {
	private int anInt6486 = 3072;
	static int anInt6487 = 0;
	static Class303[] aClass303Array6488;
	static int anInt6489 = 0;
	private int anInt6490 = 1024;
	private int anInt6491 = 2048;

	public Class296_Sub51_Sub28() {
		super(1, false);
	}

	final int[] get_monochrome_output(int i, int i_0_) {
		if (i != 0)
			return null;
		int[] is = aClass318_5035.method3335(i_0_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int[] is_1_ = this.method3064(0, i, i_0_);
			for (int i_2_ = 0; i_2_ < Class41_Sub10.anInt3769; i_2_++)
				is[i_2_] = (anInt6491 * is_1_[i_2_] >> 12) + anInt6490;
		}
		return is;
	}

	final void method3076(byte i) {
		anInt6491 = anInt6486 - anInt6490;
		int i_3_ = -111 % ((-58 - i) / 40);
	}

	final void method3071(int i, Packet class296_sub17, int i_4_) {
		int i_5_ = i_4_;
		while_151_ : do {
			do {
				if (i_5_ != 0) {
					if (i_5_ != 1) {
						if (i_5_ == 2)
							break;
						break while_151_;
					}
				} else {
					anInt6490 = class296_sub17.g2();
					break while_151_;
				}
				anInt6486 = class296_sub17.g2();
				break while_151_;
			} while (false);
			monochromatic = class296_sub17.g1() == 1;
		} while (false);
		if (i > -84)
			method3076((byte) 40);
	}

	public static void method3163(int i) {
		if (i != 416591148)
			method3163(37);
		aClass303Array6488 = null;
	}

	final int[][] get_colour_output(int i, int i_6_) {
		if (i_6_ != 17621)
			return null;
		int[][] is = aClass86_5034.method823((byte) 119, i);
		if (aClass86_5034.aBoolean939) {
			int[][] is_7_ = this.method3075((byte) -39, 0, i);
			int[] is_8_ = is_7_[0];
			int[] is_9_ = is_7_[1];
			int[] is_10_ = is_7_[2];
			int[] is_11_ = is[0];
			int[] is_12_ = is[1];
			int[] is_13_ = is[2];
			for (int i_14_ = 0; i_14_ < Class41_Sub10.anInt3769; i_14_++) {
				is_11_[i_14_] = anInt6490 + (anInt6491 * is_8_[i_14_] >> 12);
				is_12_[i_14_] = (anInt6491 * is_9_[i_14_] >> 12) + anInt6490;
				is_13_[i_14_] = (anInt6491 * is_10_[i_14_] >> 12) + anInt6490;
			}
		}
		return is;
	}
}
