package net.zaros.client;

/* Class41_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub2 extends Class41 {
	static boolean aBoolean3740 = false;
	static int anInt3741;
	static Class55 aClass55_3742;

	final int method393(int i) {
		if (i < 114)
			method399(43);
		return anInt389;
	}

	final boolean method394(int i) {
		if (i != -25952)
			return false;
		if (aClass296_Sub50_392.method3054(60))
			return false;
		return true;
	}

	final void method386(int i) {
		if (i != 2)
			method397(-57);
		if (aClass296_Sub50_392.method3054(i + 60))
			anInt389 = 0;
		if (anInt389 < 0 && anInt389 > 2)
			anInt389 = method383((byte) 110);
	}

	final int method383(byte i) {
		if (i != 110)
			aBoolean3740 = false;
		return 1;
	}

	final void method381(int i, byte i_0_) {
		anInt389 = i;
		if (i_0_ != -110) {
			/* empty */
		}
	}

	Class41_Sub2(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	Class41_Sub2(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	static final void writeEmailVerification(String string, byte i) {
		if (i > -87)
			aClass55_3742 = null;
		Class296_Sub1 class296_sub1 = Class296_Sub51_Sub30.method3169(115);
		class296_sub1.out.p1(Class292.aClass269_2676.protocolId); // 28
		class296_sub1.out.p2(0);
		int i_4_ = class296_sub1.out.pos;
		class296_sub1.out.p2(666);
		//int[] is = Class139.writeIsaacKeys(class296_sub1, -126);
		int i_5_ = class296_sub1.out.pos;
		class296_sub1.out.writeString(string);
		class296_sub1.out.p1(Class394.langID);
		class296_sub1.out.pos += 7;
		//class296_sub1.out.encryptXteas(is, i_5_, class296_sub1.out.position);
		class296_sub1.out.endBytePacket(class296_sub1.out.pos - i_4_);
		Class296_Sub45_Sub2.aClass204_6276.sendPacket(class296_sub1, (byte) 119);
		Class41_Sub28.anInt3817 = 0;
		Class162.anInt3555 = 0;
		Class368_Sub4.anInt5441 = -3;
		Class397_Sub3.anInt5799 = 1;
	}

	public static void method397(int i) {
		if (i == 9991)
			aClass55_3742 = null;
	}

	final int method380(int i, byte i_6_) {
		if (i_6_ != 41)
			return 28;
		if (aClass296_Sub50_392.method3054(i_6_ ^ 0x6a))
			return 3;
		if (i == 0 || aClass296_Sub50_392.aClass41_Sub29_5002.method515(124) == 1)
			return 1;
		return 2;
	}

	static final Class156 method398(byte i, Packet class296_sub17) {
		if (i >= -98)
			method398((byte) -5, null);
		int i_7_ = class296_sub17.g4();
		return new Class156(i_7_);
	}

	static final void method399(int i) {
		Class338_Sub3_Sub1_Sub1.method3485();
		for (int i_8_ = 0; i_8_ < 4; i_8_++)
			BITConfigDefinition.mapClips[i_8_].setup();
		Class296_Sub45_Sub4.method3018(-112);
		TextureOperation.method3069(-115);
		Class225.method2084((byte) -22);
		if (i >= -6)
			method399(-100);
		System.gc();
		Class41_Sub13.aHa3774.ya();
	}
}
