package net.zaros.client;
/* Class217_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jagex3.jagmisc.jagmisc;

final class Class217_Sub2 extends Class217 {
	private long aLong4550;
	private long aLong4551 = 0L;
	private int anInt4552;
	private int anInt4553;
	private long[] aLongArray4554;
	private long aLong4555;

	final long method2032(byte i) {
		aLong4551 += method2038((byte) 73);
		if (aLong4551 < aLong4550)
			return (aLong4550 - aLong4551) / 1000000L;
		int i_0_ = -50 / ((i + 54) / 35);
		return 0L;
	}

	final long method2036(boolean bool) {
		if (bool != true)
			method2034(true, 84L);
		return aLong4551;
	}

	final int method2034(boolean bool, long l) {
		if (bool)
			anInt4552 = 110;
		if (aLong4551 >= aLong4550) {
			int i = 0;
			do {
				i++;
				aLong4550 += l;
			} while (i < 10 && aLong4551 > aLong4550);
			if (aLong4550 < aLong4551)
				aLong4550 = aLong4551;
			return i;
		}
		aLong4555 += aLong4550 - aLong4551;
		aLong4551 += -aLong4551 + aLong4550;
		aLong4550 += l;
		return 1;
	}

	final void method2033(boolean bool) {
		aLong4555 = 0L;
		if (aLong4550 > aLong4551)
			aLong4551 += -aLong4551 + aLong4550;
		if (bool)
			method2036(true);
	}

	private final long method2038(byte i) {
		long l = jagmisc.nanoTime();
		long l_1_ = l + -aLong4555;
		aLong4555 = l;
		if (i != 73)
			aLongArray4554 = null;
		if (l_1_ > -5000000000L && l_1_ < 5000000000L) {
			aLongArray4554[anInt4553] = l_1_;
			anInt4553 = (anInt4553 + 1) % 10;
			if (anInt4552 < 1)
				anInt4552++;
		}
		long l_2_ = 0L;
		for (int i_3_ = 1; i_3_ <= anInt4552; i_3_++)
			l_2_ += aLongArray4554[(-i_3_ + 10 + anInt4553) % 10];
		return l_2_ / (long) anInt4552;
	}

	Class217_Sub2() {
		aLong4550 = 0L;
		anInt4552 = 1;
		aLongArray4554 = new long[10];
		anInt4553 = 0;
		aLong4555 = 0L;
		aLong4550 = aLong4551 = jagmisc.nanoTime();
		if (aLong4551 == 0L)
			throw new RuntimeException();
	}
}
