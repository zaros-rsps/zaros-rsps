package net.zaros.client;
/* Class91 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jaggl.OpenGL;

final class Class91 {
	private Class389 aClass389_987;
	private ha_Sub3 aHa_Sub3_988;

	final boolean method840(Class69_Sub2 class69_sub2, Class69_Sub2 class69_sub2_0_, int i, float f) {
		if (!method841(1))
			return false;
		Class392 class392 = aHa_Sub3_988.aClass392_4260;
		Class296_Sub39_Sub4 class296_sub39_sub4 = new Class296_Sub39_Sub4(aHa_Sub3_988, 6408, class69_sub2.anInt5718, class69_sub2.anInt5717);
		aHa_Sub3_988.method1351(class392, (byte) -27);
		boolean bool = false;
		class392.method4046((byte) 124, 0, class296_sub39_sub4);
		if (i >= -47)
			method840(null, null, -107, 0.58755344F);
		if (class392.method4053((byte) 118)) {
			OpenGL.glPushMatrix();
			OpenGL.glLoadIdentity();
			OpenGL.glMatrixMode(5889);
			OpenGL.glPushMatrix();
			OpenGL.glLoadIdentity();
			OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
			OpenGL.glPushAttrib(2048);
			OpenGL.glViewport(0, 0, class69_sub2.anInt5718, class69_sub2.anInt5717);
			OpenGL.glUseProgramObjectARB(aClass389_987.aLong3280);
			OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB((aClass389_987.aLong3280), "heightMap"), 0);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB((aClass389_987.aLong3280), "rcpRelief"), 1.0F / f);
			OpenGL.glUniform2fARB(OpenGL.glGetUniformLocationARB((aClass389_987.aLong3280), "sampleSize"), 1.0F / (float) class69_sub2_0_.anInt5718, 1.0F / (float) class69_sub2_0_.anInt5717);
			for (int i_1_ = 0; i_1_ < class69_sub2.anInt5721; i_1_++) {
				float f_2_ = (float) i_1_ / (float) class69_sub2.anInt5721;
				aHa_Sub3_988.method1316(class69_sub2_0_, (byte) -104);
				OpenGL.glBegin(7);
				OpenGL.glTexCoord3f(0.0F, 0.0F, f_2_);
				OpenGL.glVertex2f(0.0F, 0.0F);
				OpenGL.glTexCoord3f(1.0F, 0.0F, f_2_);
				OpenGL.glVertex2f(1.0F, 0.0F);
				OpenGL.glTexCoord3f(1.0F, 1.0F, f_2_);
				OpenGL.glVertex2f(1.0F, 1.0F);
				OpenGL.glTexCoord3f(0.0F, 1.0F, f_2_);
				OpenGL.glVertex2f(0.0F, 1.0F);
				OpenGL.glEnd();
				class69_sub2.method738(0, 0, i_1_, class69_sub2.anInt5718, false, 0, class69_sub2.anInt5717, 0);
			}
			OpenGL.glUseProgramObjectARB(0L);
			OpenGL.glPopAttrib();
			OpenGL.glPopMatrix();
			OpenGL.glMatrixMode(5888);
			OpenGL.glPopMatrix();
			bool = true;
		}
		class392.method4047((byte) -101, 0);
		aHa_Sub3_988.method1280(class392, -1);
		return bool;
	}

	final boolean method841(int i) {
		if (i != 1)
			method841(99);
		if (aHa_Sub3_988.aBoolean4190 && aHa_Sub3_988.aBoolean4174 && aClass389_987 == null) {
			Class77 class77 = (Class296_Sub51_Sub3.method3084(19399, aHa_Sub3_988, "uniform float rcpRelief;\nuniform vec2 sampleSize;\nuniform sampler3D heightMap;\nvoid main() {\nfloat dx = texture3D(heightMap, vec3(-sampleSize.x, 0.0, 0.0)+gl_TexCoord[0].xyz).r - texture3D(heightMap, vec3(sampleSize.x, 0.0, 0.0)+gl_TexCoord[0].xyz).r;\nfloat dy = texture3D(heightMap, vec3(0.0, -sampleSize.y, 0.0)+gl_TexCoord[0].xyz).r - texture3D(heightMap, vec3(0.0, sampleSize.y, 0.0)+gl_TexCoord[0].xyz).r;\ngl_FragColor = vec4(0.5+normalize(vec3(dx, dy, rcpRelief))*0.5, texture3D(heightMap, gl_TexCoord[0].xyz).r);\n}\n", 35632));
			if (class77 != null)
				aClass389_987 = Class296_Sub51_Sub19.method3127(1, (new Class77[]{class77}), aHa_Sub3_988);
		}
		if (aClass389_987 == null)
			return false;
		return true;
	}

	Class91(ha_Sub3 var_ha_Sub3) {
		aHa_Sub3_988 = var_ha_Sub3;
	}
}
