package net.zaros.client;
/* Class237 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class CS2Stack {
	int[] intLocals;
	static Class264 aClass264_2243;
	int scriptID = -1;
	String[] stringLocals;
	static int anInt2246;
	CS2Script script;
	long[] longLocals;
	static Class264 aClass264_2249;
	static double aDouble2250;
	static int anInt2251;
	static int anInt2252;

	public static void method2136(byte i) {
		aClass264_2243 = null;
		aClass264_2249 = null;
		if (i != -27)
			aClass264_2249 = null;
	}

	static final void method2137(int i) {
		if (i < 24)
			anInt2251 = 48;
		if (Class366_Sub6.anInt5392 != 3) {
			if (Class366_Sub6.anInt5392 != 7) {
				if (Class366_Sub6.anInt5392 != 9) {
					if (Class366_Sub6.anInt5392 == 11)
						Class41_Sub8.method422(1, 12);
				} else
					Class41_Sub8.method422(1, 10);
			} else
				Class41_Sub8.method422(1, 8);
		} else
			Class41_Sub8.method422(1, 4);
	}

	static final SubInPacket[] method2138(int i) {
		if (i != 4)
			method2137(-93);
		return (new SubInPacket[]{Class367.aClass260_3127, Class296_Sub45_Sub2.aClass260_6275, r_Sub2.aClass260_6710, Class42_Sub1.aClass260_3832, Class41_Sub17.aClass260_3782, Class296_Sub24.aClass260_4753, Class335.aClass260_2953, Class127_Sub1.aClass260_4282, Class22.aClass260_252, Class338_Sub8.aClass260_5255, Class41_Sub6.aClass260_3758, Class56.aClass260_665, Class75.aClass260_869, SubCache.aClass260_2708, Class178_Sub2.aClass260_4436});
	}

	static {
		aClass264_2249 = aClass264_2243 = new Class264(false);
		anInt2251 = 0;
		anInt2252 = 1;
	}
}
