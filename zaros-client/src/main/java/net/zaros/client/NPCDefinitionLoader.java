package net.zaros.client;

/* Class136 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class NPCDefinitionLoader {
	GameType game;
	private Js5 definitionsCache;
	Js5 aClass138_1397;
	private AdvancedMemoryCache cachedDefinitions = new AdvancedMemoryCache(64);
	private int languageID;
	boolean aBoolean1400;
	static int anInt1401 = 0;
	static byte aByte1402 = -6;
	AdvancedMemoryCache aClass113_1403 = new AdvancedMemoryCache(50);
	AdvancedMemoryCache aClass113_1404 = new AdvancedMemoryCache(5);
	private String[] defaultNPCOptions;
	int anInt1406;

	final NPCDefinition getDefinition(int npcID) {
		NPCDefinition def;
		synchronized (cachedDefinitions) {
			def = (NPCDefinition) cachedDefinitions.get(npcID);
		}
		if (def != null) {
			return def;
		}
		byte[] data;
		synchronized (definitionsCache) {
			data = definitionsCache.getFile(Class296_Sub51_Sub27.npcFileID(npcID), Class287.npcChildID(npcID));
		}
		def = new NPCDefinition();
		def.npcID = npcID;
		def.loader = this;
		def.options = defaultNPCOptions.clone();
		if (data != null) {
			boolean newNpc = data[0] == 'N' && data[1] == 'E' && data[2] == 'W';
			def.init(new Packet(data), newNpc);
		}
		def.method1500(-6061);
		DefinitionManipulator.manipulateNPC(npcID, def);
		synchronized (cachedDefinitions) {
			cachedDefinitions.put(def, npcID);
		}
		return def;
	}

	final void method1414(byte i) {
		synchronized (cachedDefinitions) {
			cachedDefinitions.clear();
			if (i >= -90) {
				languageID = -15;
			}
		}
		synchronized (aClass113_1403) {
			aClass113_1403.clear();
		}
		synchronized (aClass113_1404) {
			aClass113_1404.clear();
		}
	}

	final void method1415(int i, int i_1_) {
		synchronized (cachedDefinitions) {
			cachedDefinitions.clean(i_1_);
			if (i != 64) {
				anInt1406 = 9;
			}
		}
		synchronized (aClass113_1403) {
			aClass113_1403.clean(i_1_);
		}
		synchronized (aClass113_1404) {
			aClass113_1404.clean(i_1_);
		}
	}

	final void method1416(int i, boolean bool) {
		if (bool == !aBoolean1400) {
			aBoolean1400 = bool;
			if (i > -51) {
				aClass138_1397 = null;
			}
			method1414((byte) -95);
		}
	}

	final void method1417(int i, int i_2_) {
		anInt1406 = i;
		synchronized (aClass113_1403) {
			aClass113_1403.clear();
		}
		synchronized (aClass113_1404) {
			aClass113_1404.clear();
		}
		if (i_2_ != -27707) {
			aByte1402 = (byte) -26;
		}
	}

	final void method1418(byte i) {
		synchronized (cachedDefinitions) {
			cachedDefinitions.clearSoftReferences();
		}
		if (i != -100) {
			method1416(-89, false);
		}
		synchronized (aClass113_1403) {
			aClass113_1403.clearSoftReferences();
		}
		synchronized (aClass113_1404) {
			aClass113_1404.clearSoftReferences();
		}
	}

	static final void method1419(int i, s var_s) {
		Class360_Sub2.aSArray5304[i] = var_s;
	}

	static final Model method1420(int i, ha var_ha, byte i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		if (i_3_ != 122) {
			anInt1401 = -65;
		}
		long l = i_6_;
		Model class178 = (Model) World.aClass113_1160.get(l);
		int i_8_ = 2055;
		if (class178 == null) {
			Mesh class132 = Class296_Sub51_Sub1.fromJs5(Class184.fs7, i_6_, 0);
			if (class132 == null) {
				return null;
			}
			if (class132.version_number < 13) {
				class132.scale(2);
			}
			class178 = var_ha.a(class132, i_8_, Class146.anInt1442, 64, 768);
			World.aClass113_1160.put(class178, l);
		}
		class178 = class178.method1728((byte) 6, i_8_, true);
		if (i_7_ != 0) {
			class178.a(i_7_);
		}
		if (i_5_ != 0) {
			class178.FA(i_5_);
		}
		if (i_4_ != 0) {
			class178.VA(i_4_);
		}
		if (i != 0) {
			class178.H(0, i, 0);
		}
		return class178;
	}

	final void method1421(int i) {
		synchronized (aClass113_1403) {
			aClass113_1403.clear();
		}
		synchronized (aClass113_1404) {
			if (i != 12409) {
				defaultNPCOptions = null;
			}
			aClass113_1404.clear();
		}
	}

	NPCDefinitionLoader(GameType class35, int i, boolean bool, Js5 class138, Js5 class138_9_) {
		aClass138_1397 = class138_9_;
		aBoolean1400 = bool;
		languageID = i;
		definitionsCache = class138;
		game = class35;
		if (definitionsCache != null) {
			int i_10_ = definitionsCache.getLastGroupId() - 1;
			definitionsCache.getLastFileId(i_10_);
		}
		if (game == Class363.runescape) {
			defaultNPCOptions = new String[] { null, null, null, null, null, TranslatableString.aClass120_1218.getTranslation(languageID) };
		} else {
			defaultNPCOptions = new String[] { null, null, null, null, null, null };
		}
	}
}
