package net.zaros.client;

public abstract class Class373 {
	static Class296_Sub39_Sub14 aClass296_Sub39_Sub14_3177;
	static boolean aBoolean3178 = false;

	public abstract void method3900(int i);

	public abstract void method3901(int i, int i_0_, int i_1_, int[] is);

	public abstract void method3902(int i, int i_2_, int i_3_);

	public abstract void method3903(int i, int i_4_, int i_5_, int[] is);

	public abstract void method3904(int i, int i_6_, int i_7_);

	public abstract void method3905(int i, int i_8_, int i_9_, int[] is);

	public abstract void method3906(int i);

	public abstract void method3907(int i, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_);

	public abstract void method3908(int[] is);

	public abstract void method3909(int i);

	public abstract void method3910();

	public abstract void method3911(int i);

	public static void method3912(boolean bool) {
		if (!bool)
			aClass296_Sub39_Sub14_3177 = null;
	}

	public static final void method3913(int i, String string) {
		if (string != null && i > 58) {
			String string_15_ = Class296_Sub49.parseName(string);
			if (string_15_ != null) {
				for (int i_16_ = 0; Class285.anInt2625 > i_16_; i_16_++) {
					String string_17_ = Js5TextureLoader.aStringArray3443[i_16_];
					String string_18_ = Class296_Sub49.parseName(string_17_);
					if (Class351.method3684(string_17_, string, string_15_, string_18_)) {
						Class285.anInt2625--;
						for (int i_19_ = i_16_; Class285.anInt2625 > i_19_; i_19_++) {
							Js5TextureLoader.aStringArray3443[i_19_] = Js5TextureLoader.aStringArray3443[i_19_ + 1];
							Class338_Sub9.aStringArray5268[i_19_] = Class338_Sub9.aStringArray5268[i_19_ + 1];
							Class95.anIntArray1021[i_19_] = Class95.anIntArray1021[i_19_ + 1];
							StaticMethods.aStringArray5940[i_19_] = (StaticMethods.aStringArray5940[i_19_
									+ 1]);
							Class338_Sub3_Sub4_Sub1.anIntArray6653[i_19_] = (Class338_Sub3_Sub4_Sub1.anIntArray6653[i_19_
									+ 1]);
							StaticMethods.aBooleanArray5945[i_19_] = (StaticMethods.aBooleanArray5945[i_19_
									+ 1]);
							Class296_Sub34_Sub2.aBooleanArray6101[i_19_] = (Class296_Sub34_Sub2.aBooleanArray6101[i_19_
									+ 1]);
						}
						Class156.anInt3595 = Class338_Sub3_Sub3_Sub1.anInt6616;
						Connection class204 = Class296_Sub51_Sub13.method3111(true);
						Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 98,
								Class363.aClass311_3108);
						class296_sub1.out.p1(Class117.method1015((byte) -117, string));
						class296_sub1.out.writeString(string);
						class204.sendPacket(class296_sub1, (byte) 119);
						break;
					}
				}
			}
		}
	}

	public abstract void method3914(int i);

	public Class373() {
		/* empty */
	}

	public abstract void method3915(Class373 class373_20_);

	public abstract Class373 method3916();

	public abstract void method3917(int i);
}
