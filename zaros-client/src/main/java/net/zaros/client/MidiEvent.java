package net.zaros.client;
/* Class109 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class MidiEvent {
	private Packet buffer;
	private static byte[] aByteArray1132 = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	private int[] anIntArray1133;
	private int[] commands;
	private int[] current_positions;
	private long aLong1136;
	int[] tick_durations;
	int anInt1138;
	private int anInt1139;

	final void readTickDuration(int i) {
		int i_0_ = buffer.gvarint();
		tick_durations[i] += i_0_;
	}

	final boolean method952() {
		if (buffer.data == null) {
			return false;
		}
		return true;
	}

	final boolean isFinished() {
		int i = current_positions.length;
		for (int i_1_ = 0; i_1_ < i; i_1_++) {
			if (current_positions[i_1_] >= 0) {
				return false;
			}
		}
		return true;
	}

	final void method954(byte[] is) {
		buffer.data = is;
		buffer.pos = 10;
		int i = buffer.g2();
		anInt1138 = buffer.g2();
		anInt1139 = 500000;
		anIntArray1133 = new int[i];
		int i_2_ = 0;
		while (i_2_ < i) {
			int i_3_ = buffer.g4();
			int i_4_ = buffer.g4();
			if (i_3_ == 1297379947) {
				anIntArray1133[i_2_] = buffer.pos;
				i_2_++;
			}
			buffer.pos += i_4_;
		}
		aLong1136 = 0L;
		current_positions = new int[i];
		for (int i_5_ = 0; i_5_ < i; i_5_++) {
			current_positions[i_5_] = anIntArray1133[i_5_];
		}
		tick_durations = new int[i];
		commands = new int[i];
	}

	public static void method955() {
		aByteArray1132 = null;
	}

	final void saveTrackBuffer(int i) {
		current_positions[i] = buffer.pos;
	}

	private final int getStatusSize(int track, int status) {
		if (status == 255) {
			int i_7_ = buffer.g1();
			int i_8_ = buffer.gvarint();
			if (i_7_ == 47) {
				buffer.pos += i_8_;
				return 1;
			}
			if (i_7_ == 81) {
				int i_9_ = buffer.readUnsignedMedInt();
				i_8_ -= 3;
				int i_10_ = tick_durations[track];
				aLong1136 += (long) i_10_ * (long) (anInt1139 - i_9_);
				anInt1139 = i_9_;
				buffer.pos += i_8_;
				return 2;
			}
			buffer.pos += i_8_;
			return 3;
		}
		byte i_11_ = aByteArray1132[status - 128];
		int i_12_ = status;
		if (i_11_ >= 1) {
			i_12_ |= buffer.g1() << 8;
		}
		if (i_11_ >= 2) {
			i_12_ |= buffer.g1() << 16;
		}
		return i_12_;
	}

	final int getTracksCount() {
		return current_positions.length;
	}

	final void discardTrackBuffer() {
		buffer.pos = -1;
	}

	final void method960(long l) {
		aLong1136 = l;
		int i = current_positions.length;
		for (int i_13_ = 0; i_13_ < i; i_13_++) {
			tick_durations[i_13_] = 0;
			commands[i_13_] = 0;
			buffer.pos = anIntArray1133[i_13_];
			readTickDuration(i_13_);
			current_positions[i_13_] = buffer.pos;
		}
	}

	final void loadTrackBuffer(int i) {
		buffer.pos = current_positions[i];
	}

	final long method962(int i) {
		return aLong1136 + (long) i * (long) anInt1139;
	}

	final int getFastestTrack() {
		int i = current_positions.length;
		int i_14_ = -1;
		int i_15_ = 2147483647;
		for (int i_16_ = 0; i_16_ < i; i_16_++) {
			if (current_positions[i_16_] >= 0 && tick_durations[i_16_] < i_15_) {
				i_14_ = i_16_;
				i_15_ = tick_durations[i_16_];
			}
		}
		return i_14_;
	}

	final void method964() {
		buffer.data = null;
		anIntArray1133 = null;
		current_positions = null;
		tick_durations = null;
		commands = null;
	}

	public MidiEvent() {
		buffer = new Packet(null);
	}

	final int method965(int i) {
		int i_17_ = method966(i);
		return i_17_;
	}

	private final int method966(int track) {
		int status = buffer.data[buffer.pos];
		if (status < 0) {
			status &= 0xff;
			commands[track] = status;
			buffer.pos++;
		} else {
			status = commands[track];
		}
		if (status == 240 || status == 247) {
			int size = buffer.gvarint();
			if (status == 247 && size > 0) {
				int command = buffer.data[buffer.pos] & 0xff;
				if (command >= 241 && command <= 243 || command == 246 || command == 248 || command >= 250 && command <= 252 || command == 254) {
					buffer.pos++;
					commands[track] = command;
					return getStatusSize(track, command);
				}
			}
			buffer.pos += size;
			return 0;
		}
		return getStatusSize(track, status);
	}

	MidiEvent(byte[] is) {
		buffer = new Packet(null);
		method954(is);
	}
}
