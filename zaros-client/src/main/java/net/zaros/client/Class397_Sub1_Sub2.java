package net.zaros.client;

/* Class397_Sub1_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class397_Sub1_Sub2 extends Class397_Sub1 {
	private byte[] aByteArray6691;
	private int[] anIntArray6692;

	public void method4094(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		if (i_1_ > 0 && i_2_ > 0) {
			int i_7_ = 0;
			int i_8_ = 0;
			int i_9_ = aHa_Sub2_5768.anInt4084;
			int i_10_ = anInt5772 + anInt5758 + anInt5757;
			int i_11_ = anInt5761 + anInt5756 + anInt5750;
			int i_12_ = (i_10_ << 16) / i_1_;
			int i_13_ = (i_11_ << 16) / i_2_;
			if (anInt5772 > 0) {
				int i_14_ = ((anInt5772 << 16) + i_12_ - 1) / i_12_;
				i += i_14_;
				i_7_ += i_14_ * i_12_ - (anInt5772 << 16);
			}
			if (anInt5761 > 0) {
				int i_15_ = ((anInt5761 << 16) + i_13_ - 1) / i_13_;
				i_0_ += i_15_;
				i_8_ += i_15_ * i_13_ - (anInt5761 << 16);
			}
			if (anInt5758 < i_10_)
				i_1_ = ((anInt5758 << 16) - i_7_ + i_12_ - 1) / i_12_;
			if (anInt5756 < i_11_)
				i_2_ = ((anInt5756 << 16) - i_8_ + i_13_ - 1) / i_13_;
			int i_16_ = i + i_0_ * i_9_;
			int i_17_ = i_9_ - i_1_;
			if (i_0_ + i_2_ > aHa_Sub2_5768.anInt4078)
				i_2_ -= i_0_ + i_2_ - aHa_Sub2_5768.anInt4078;
			if (i_0_ < aHa_Sub2_5768.anInt4085) {
				int i_18_ = aHa_Sub2_5768.anInt4085 - i_0_;
				i_2_ -= i_18_;
				i_16_ += i_18_ * i_9_;
				i_8_ += i_13_ * i_18_;
			}
			if (i + i_1_ > aHa_Sub2_5768.anInt4104) {
				int i_19_ = i + i_1_ - aHa_Sub2_5768.anInt4104;
				i_1_ -= i_19_;
				i_17_ += i_19_;
			}
			if (i < aHa_Sub2_5768.anInt4079) {
				int i_20_ = aHa_Sub2_5768.anInt4079 - i;
				i_1_ -= i_20_;
				i_16_ += i_20_;
				i_7_ += i_12_ * i_20_;
				i_17_ += i_20_;
			}
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_5_ == 0) {
				if (i_3_ == 1) {
					int i_21_ = i_7_;
					for (int i_22_ = -i_2_; i_22_ < 0; i_22_++) {
						int i_23_ = (i_8_ >> 16) * anInt5758;
						for (int i_24_ = -i_1_; i_24_ < 0; i_24_++) {
							is[i_16_++] = (anIntArray6692[(aByteArray6691[(i_7_ >> 16) + i_23_] & 0xff)]);
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_21_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_3_ == 0) {
					int i_25_ = (i_4_ & 0xff0000) >> 16;
					int i_26_ = (i_4_ & 0xff00) >> 8;
					int i_27_ = i_4_ & 0xff;
					int i_28_ = i_7_;
					for (int i_29_ = -i_2_; i_29_ < 0; i_29_++) {
						int i_30_ = (i_8_ >> 16) * anInt5758;
						for (int i_31_ = -i_1_; i_31_ < 0; i_31_++) {
							int i_32_ = (anIntArray6692[(aByteArray6691[(i_7_ >> 16) + i_30_] & 0xff)]);
							int i_33_ = (i_32_ & 0xff0000) * i_25_ & ~0xffffff;
							int i_34_ = (i_32_ & 0xff00) * i_26_ & 0xff0000;
							int i_35_ = (i_32_ & 0xff) * i_27_ & 0xff00;
							is[i_16_++] = (i_33_ | i_34_ | i_35_) >>> 8;
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_28_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_3_ == 3) {
					int i_36_ = i_7_;
					for (int i_37_ = -i_2_; i_37_ < 0; i_37_++) {
						int i_38_ = (i_8_ >> 16) * anInt5758;
						for (int i_39_ = -i_1_; i_39_ < 0; i_39_++) {
							byte i_40_ = aByteArray6691[(i_7_ >> 16) + i_38_];
							int i_41_ = i_40_ > 0 ? anIntArray6692[i_40_] : 0;
							int i_42_ = i_41_ + i_4_;
							int i_43_ = (i_41_ & 0xff00ff) + (i_4_ & 0xff00ff);
							int i_44_ = ((i_43_ & 0x1000100) + (i_42_ - i_43_ & 0x10000));
							is[i_16_++] = i_42_ - i_44_ | i_44_ - (i_44_ >>> 8);
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_36_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_3_ == 2) {
					int i_45_ = i_4_ >>> 24;
					int i_46_ = 256 - i_45_;
					int i_47_ = (i_4_ & 0xff00ff) * i_46_ & ~0xff00ff;
					int i_48_ = (i_4_ & 0xff00) * i_46_ & 0xff0000;
					i_4_ = (i_47_ | i_48_) >>> 8;
					int i_49_ = i_7_;
					for (int i_50_ = -i_2_; i_50_ < 0; i_50_++) {
						int i_51_ = (i_8_ >> 16) * anInt5758;
						for (int i_52_ = -i_1_; i_52_ < 0; i_52_++) {
							int i_53_ = (anIntArray6692[(aByteArray6691[(i_7_ >> 16) + i_51_] & 0xff)]);
							i_47_ = (i_53_ & 0xff00ff) * i_45_ & ~0xff00ff;
							i_48_ = (i_53_ & 0xff00) * i_45_ & 0xff0000;
							is[i_16_++] = ((i_47_ | i_48_) >>> 8) + i_4_;
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_49_;
						i_16_ += i_17_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_5_ == 1) {
				if (i_3_ == 1) {
					int i_54_ = i_7_;
					for (int i_55_ = -i_2_; i_55_ < 0; i_55_++) {
						int i_56_ = (i_8_ >> 16) * anInt5758;
						for (int i_57_ = -i_1_; i_57_ < 0; i_57_++) {
							int i_58_ = aByteArray6691[(i_7_ >> 16) + i_56_];
							if (i_58_ != 0)
								is[i_16_++] = anIntArray6692[i_58_ & 0xff];
							else
								i_16_++;
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_54_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_3_ == 0) {
					int i_59_ = i_7_;
					if ((i_4_ & 0xffffff) == 16777215) {
						int i_60_ = i_4_ >>> 24;
						int i_61_ = 256 - i_60_;
						for (int i_62_ = -i_2_; i_62_ < 0; i_62_++) {
							int i_63_ = (i_8_ >> 16) * anInt5758;
							for (int i_64_ = -i_1_; i_64_ < 0; i_64_++) {
								int i_65_ = aByteArray6691[(i_7_ >> 16) + i_63_];
								if (i_65_ != 0) {
									int i_66_ = anIntArray6692[i_65_ & 0xff];
									int i_67_ = is[i_16_];
									is[i_16_++] = ((((i_66_ & 0xff00ff) * i_60_ + (i_67_ & 0xff00ff) * i_61_)
											& ~0xff00ff)
											+ (((i_66_ & 0xff00) * i_60_ + (i_67_ & 0xff00) * i_61_) & 0xff0000)) >> 8;
								} else
									i_16_++;
								i_7_ += i_12_;
							}
							i_8_ += i_13_;
							i_7_ = i_59_;
							i_16_ += i_17_;
						}
					} else {
						int i_68_ = (i_4_ & 0xff0000) >> 16;
						int i_69_ = (i_4_ & 0xff00) >> 8;
						int i_70_ = i_4_ & 0xff;
						int i_71_ = i_4_ >>> 24;
						int i_72_ = 256 - i_71_;
						for (int i_73_ = -i_2_; i_73_ < 0; i_73_++) {
							int i_74_ = (i_8_ >> 16) * anInt5758;
							for (int i_75_ = -i_1_; i_75_ < 0; i_75_++) {
								int i_76_ = aByteArray6691[(i_7_ >> 16) + i_74_];
								if (i_76_ != 0) {
									int i_77_ = anIntArray6692[i_76_ & 0xff];
									if (i_71_ != 255) {
										int i_78_ = ((i_77_ & 0xff0000) * i_68_ & ~0xffffff);
										int i_79_ = ((i_77_ & 0xff00) * i_69_ & 0xff0000);
										int i_80_ = (i_77_ & 0xff) * i_70_ & 0xff00;
										i_77_ = (i_78_ | i_79_ | i_80_) >>> 8;
										int i_81_ = is[i_16_];
										is[i_16_++] = ((((i_77_ & 0xff00ff) * i_71_ + (i_81_ & 0xff00ff) * i_72_)
												& ~0xff00ff)
												+ (((i_77_ & 0xff00) * i_71_ + (i_81_ & 0xff00) * i_72_)
														& 0xff0000)) >> 8;
									} else {
										int i_82_ = ((i_77_ & 0xff0000) * i_68_ & ~0xffffff);
										int i_83_ = ((i_77_ & 0xff00) * i_69_ & 0xff0000);
										int i_84_ = (i_77_ & 0xff) * i_70_ & 0xff00;
										is[i_16_++] = (i_82_ | i_83_ | i_84_) >>> 8;
									}
								} else
									i_16_++;
								i_7_ += i_12_;
							}
							i_8_ += i_13_;
							i_7_ = i_59_;
							i_16_ += i_17_;
						}
						return;
					}
					return;
				}
				if (i_3_ == 3) {
					int i_85_ = i_7_;
					int i_86_ = i_4_ >>> 24;
					int i_87_ = 256 - i_86_;
					for (int i_88_ = -i_2_; i_88_ < 0; i_88_++) {
						int i_89_ = (i_8_ >> 16) * anInt5758;
						for (int i_90_ = -i_1_; i_90_ < 0; i_90_++) {
							byte i_91_ = aByteArray6691[(i_7_ >> 16) + i_89_];
							int i_92_ = i_91_ > 0 ? anIntArray6692[i_91_] : 0;
							int i_93_ = i_92_ + i_4_;
							int i_94_ = (i_92_ & 0xff00ff) + (i_4_ & 0xff00ff);
							int i_95_ = ((i_94_ & 0x1000100) + (i_93_ - i_94_ & 0x10000));
							i_95_ = i_93_ - i_95_ | i_95_ - (i_95_ >>> 8);
							if (i_92_ == 0 && i_86_ != 255) {
								i_92_ = i_95_;
								i_95_ = is[i_16_];
								i_95_ = ((((i_92_ & 0xff00ff) * i_86_ + (i_95_ & 0xff00ff) * i_87_) & ~0xff00ff)
										+ (((i_92_ & 0xff00) * i_86_ + (i_95_ & 0xff00) * i_87_) & 0xff0000)) >> 8;
							}
							is[i_16_++] = i_95_;
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_85_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_3_ == 2) {
					int i_96_ = i_4_ >>> 24;
					int i_97_ = 256 - i_96_;
					int i_98_ = (i_4_ & 0xff00ff) * i_97_ & ~0xff00ff;
					int i_99_ = (i_4_ & 0xff00) * i_97_ & 0xff0000;
					i_4_ = (i_98_ | i_99_) >>> 8;
					int i_100_ = i_7_;
					for (int i_101_ = -i_2_; i_101_ < 0; i_101_++) {
						int i_102_ = (i_8_ >> 16) * anInt5758;
						for (int i_103_ = -i_1_; i_103_ < 0; i_103_++) {
							int i_104_ = aByteArray6691[(i_7_ >> 16) + i_102_];
							if (i_104_ != 0) {
								int i_105_ = anIntArray6692[i_104_ & 0xff];
								i_98_ = (i_105_ & 0xff00ff) * i_96_ & ~0xff00ff;
								i_99_ = (i_105_ & 0xff00) * i_96_ & 0xff0000;
								is[i_16_++] = ((i_98_ | i_99_) >>> 8) + i_4_;
							} else
								i_16_++;
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_100_;
						i_16_ += i_17_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_5_ == 2) {
				if (i_3_ == 1) {
					int i_106_ = i_7_;
					for (int i_107_ = -i_2_; i_107_ < 0; i_107_++) {
						int i_108_ = (i_8_ >> 16) * anInt5758;
						for (int i_109_ = -i_1_; i_109_ < 0; i_109_++) {
							int i_110_ = aByteArray6691[(i_7_ >> 16) + i_108_];
							if (i_110_ != 0) {
								int i_111_ = anIntArray6692[i_110_ & 0xff];
								int i_112_ = is[i_16_];
								int i_113_ = i_111_ + i_112_;
								int i_114_ = ((i_111_ & 0xff00ff) + (i_112_ & 0xff00ff));
								i_112_ = (i_114_ & 0x1000100) + (i_113_ - i_114_ & 0x10000);
								is[i_16_++] = i_113_ - i_112_ | i_112_ - (i_112_ >>> 8);
							} else
								i_16_++;
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_106_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_3_ == 0) {
					int i_115_ = i_7_;
					int i_116_ = (i_4_ & 0xff0000) >> 16;
					int i_117_ = (i_4_ & 0xff00) >> 8;
					int i_118_ = i_4_ & 0xff;
					for (int i_119_ = -i_2_; i_119_ < 0; i_119_++) {
						int i_120_ = (i_8_ >> 16) * anInt5758;
						for (int i_121_ = -i_1_; i_121_ < 0; i_121_++) {
							int i_122_ = aByteArray6691[(i_7_ >> 16) + i_120_];
							if (i_122_ != 0) {
								int i_123_ = anIntArray6692[i_122_ & 0xff];
								int i_124_ = (i_123_ & 0xff0000) * i_116_ & ~0xffffff;
								int i_125_ = (i_123_ & 0xff00) * i_117_ & 0xff0000;
								int i_126_ = (i_123_ & 0xff) * i_118_ & 0xff00;
								i_123_ = (i_124_ | i_125_ | i_126_) >>> 8;
								int i_127_ = is[i_16_];
								int i_128_ = i_123_ + i_127_;
								int i_129_ = ((i_123_ & 0xff00ff) + (i_127_ & 0xff00ff));
								i_127_ = (i_129_ & 0x1000100) + (i_128_ - i_129_ & 0x10000);
								is[i_16_++] = i_128_ - i_127_ | i_127_ - (i_127_ >>> 8);
							} else
								i_16_++;
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_115_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_3_ == 3) {
					int i_130_ = i_7_;
					for (int i_131_ = -i_2_; i_131_ < 0; i_131_++) {
						int i_132_ = (i_8_ >> 16) * anInt5758;
						for (int i_133_ = -i_1_; i_133_ < 0; i_133_++) {
							byte i_134_ = aByteArray6691[(i_7_ >> 16) + i_132_];
							int i_135_ = i_134_ > 0 ? anIntArray6692[i_134_] : 0;
							int i_136_ = i_135_ + i_4_;
							int i_137_ = (i_135_ & 0xff00ff) + (i_4_ & 0xff00ff);
							int i_138_ = ((i_137_ & 0x1000100) + (i_136_ - i_137_ & 0x10000));
							i_135_ = i_136_ - i_138_ | i_138_ - (i_138_ >>> 8);
							i_138_ = is[i_16_];
							i_136_ = i_135_ + i_138_;
							i_137_ = (i_135_ & 0xff00ff) + (i_138_ & 0xff00ff);
							i_138_ = (i_137_ & 0x1000100) + (i_136_ - i_137_ & 0x10000);
							is[i_16_++] = i_136_ - i_138_ | i_138_ - (i_138_ >>> 8);
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_130_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_3_ == 2) {
					int i_139_ = i_4_ >>> 24;
					int i_140_ = 256 - i_139_;
					int i_141_ = (i_4_ & 0xff00ff) * i_140_ & ~0xff00ff;
					int i_142_ = (i_4_ & 0xff00) * i_140_ & 0xff0000;
					i_4_ = (i_141_ | i_142_) >>> 8;
					int i_143_ = i_7_;
					for (int i_144_ = -i_2_; i_144_ < 0; i_144_++) {
						int i_145_ = (i_8_ >> 16) * anInt5758;
						for (int i_146_ = -i_1_; i_146_ < 0; i_146_++) {
							int i_147_ = aByteArray6691[(i_7_ >> 16) + i_145_];
							if (i_147_ != 0) {
								int i_148_ = anIntArray6692[i_147_ & 0xff];
								i_141_ = (i_148_ & 0xff00ff) * i_139_ & ~0xff00ff;
								i_142_ = (i_148_ & 0xff00) * i_139_ & 0xff0000;
								i_148_ = ((i_141_ | i_142_) >>> 8) + i_4_;
								int i_149_ = is[i_16_];
								int i_150_ = i_148_ + i_149_;
								int i_151_ = ((i_148_ & 0xff00ff) + (i_149_ & 0xff00ff));
								i_149_ = (i_151_ & 0x1000100) + (i_150_ - i_151_ & 0x10000);
								is[i_16_++] = i_150_ - i_149_ | i_149_ - (i_149_ >>> 8);
							} else
								i_16_++;
							i_7_ += i_12_;
						}
						i_8_ += i_13_;
						i_7_ = i_143_;
						i_16_ += i_17_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	public void method4093(int i, int i_152_, aa var_aa, int i_153_, int i_154_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		i += anInt5772;
		i_152_ += anInt5761;
		int i_155_ = 0;
		int i_156_ = aHa_Sub2_5768.anInt4084;
		int i_157_ = anInt5758;
		int i_158_ = anInt5756;
		int i_159_ = i_156_ - i_157_;
		int i_160_ = 0;
		int i_161_ = i + i_152_ * i_156_;
		if (i_152_ < aHa_Sub2_5768.anInt4085) {
			int i_162_ = aHa_Sub2_5768.anInt4085 - i_152_;
			i_158_ -= i_162_;
			i_152_ = aHa_Sub2_5768.anInt4085;
			i_155_ += i_162_ * i_157_;
			i_161_ += i_162_ * i_156_;
		}
		if (i_152_ + i_158_ > aHa_Sub2_5768.anInt4078)
			i_158_ -= i_152_ + i_158_ - aHa_Sub2_5768.anInt4078;
		if (i < aHa_Sub2_5768.anInt4079) {
			int i_163_ = aHa_Sub2_5768.anInt4079 - i;
			i_157_ -= i_163_;
			i = aHa_Sub2_5768.anInt4079;
			i_155_ += i_163_;
			i_161_ += i_163_;
			i_160_ += i_163_;
			i_159_ += i_163_;
		}
		if (i + i_157_ > aHa_Sub2_5768.anInt4104) {
			int i_164_ = i + i_157_ - aHa_Sub2_5768.anInt4104;
			i_157_ -= i_164_;
			i_160_ += i_164_;
			i_159_ += i_164_;
		}
		if (i_157_ > 0 && i_158_ > 0) {
			aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
			int[] is = var_aa_Sub2.anIntArray3726;
			int[] is_165_ = var_aa_Sub2.anIntArray3729;
			int[] is_166_ = aHa_Sub2_5768.anIntArray4108;
			int i_167_ = i_152_;
			if (i_154_ > i_167_) {
				i_167_ = i_154_;
				i_161_ += (i_154_ - i_152_) * i_156_;
				i_155_ += (i_154_ - i_152_) * anInt5758;
			}
			int i_168_ = (i_154_ + is.length < i_152_ + i_158_ ? i_154_ + is.length : i_152_ + i_158_);
			for (int i_169_ = i_167_; i_169_ < i_168_; i_169_++) {
				int i_170_ = is[i_169_ - i_154_] + i_153_;
				int i_171_ = is_165_[i_169_ - i_154_];
				int i_172_ = i_157_;
				if (i > i_170_) {
					int i_173_ = i - i_170_;
					if (i_173_ >= i_171_) {
						i_155_ += i_157_ + i_160_;
						i_161_ += i_157_ + i_159_;
						continue;
					}
					i_171_ -= i_173_;
				} else {
					int i_174_ = i_170_ - i;
					if (i_174_ >= i_157_) {
						i_155_ += i_157_ + i_160_;
						i_161_ += i_157_ + i_159_;
						continue;
					}
					i_155_ += i_174_;
					i_172_ -= i_174_;
					i_161_ += i_174_;
				}
				int i_175_ = 0;
				if (i_172_ < i_171_)
					i_171_ = i_172_;
				else
					i_175_ = i_172_ - i_171_;
				for (int i_176_ = -i_171_; i_176_ < 0; i_176_++) {
					int i_177_ = aByteArray6691[i_155_++];
					if (i_177_ != 0)
						is_166_[i_161_++] = anIntArray6692[i_177_ & 0xff];
					else
						i_161_++;
				}
				i_155_ += i_175_ + i_160_;
				i_161_ += i_175_ + i_159_;
			}
		}
	}

	public void method4101(int i, int i_178_) {
		int[] is = aHa_Sub2_5768.anIntArray4108;
		if (Class397_Sub1.anInt5766 == 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_179_ = Class397_Sub1.anInt5771;
				while (i_179_ < 0) {
					int i_180_ = Class397_Sub1.anInt5759;
					int i_181_ = Class397_Sub1.anInt5765;
					int i_182_ = Class397_Sub1.anInt5775;
					int i_183_ = Class397_Sub1.anInt5770;
					if (i_181_ >= 0 && i_182_ >= 0 && i_181_ - (anInt5758 << 12) < 0
							&& i_182_ - (anInt5756 << 12) < 0) {
						for (/**/; i_183_ < 0; i_183_++) {
							int i_184_ = (i_182_ >> 12) * anInt5758 + (i_181_ >> 12);
							int i_185_ = i_180_++;
							int[] is_186_ = is;
							int i_187_ = i;
							int i_188_ = i_178_;
							if (i_188_ == 0) {
								if (i_187_ == 1)
									is_186_[i_185_] = (anIntArray6692[aByteArray6691[i_184_] & 0xff]);
								else if (i_187_ == 0) {
									int i_189_ = (anIntArray6692[aByteArray6691[i_184_] & 0xff]);
									int i_190_ = (((i_189_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_191_ = (((i_189_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_192_ = (((i_189_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_186_[i_185_] = (i_190_ | i_191_ | i_192_) >>> 8;
								} else if (i_187_ == 3) {
									int i_193_ = (anIntArray6692[aByteArray6691[i_184_] & 0xff]);
									int i_194_ = Class397_Sub1.anInt5767;
									int i_195_ = i_193_ + i_194_;
									int i_196_ = ((i_193_ & 0xff00ff) + (i_194_ & 0xff00ff));
									int i_197_ = ((i_196_ & 0x1000100) + (i_195_ - i_196_ & 0x10000));
									is_186_[i_185_] = i_195_ - i_197_ | i_197_ - (i_197_ >>> 8);
								} else if (i_187_ == 2) {
									int i_198_ = (anIntArray6692[aByteArray6691[i_184_] & 0xff]);
									int i_199_ = (((i_198_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_200_ = (((i_198_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_186_[i_185_] = (((i_199_ | i_200_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_188_ == 1) {
								if (i_187_ == 1) {
									int i_201_ = aByteArray6691[i_184_];
									if (i_201_ != 0)
										is_186_[i_185_] = anIntArray6692[i_201_ & 0xff];
								} else if (i_187_ == 0) {
									int i_202_ = aByteArray6691[i_184_];
									if (i_202_ != 0) {
										int i_203_ = anIntArray6692[i_202_ & 0xff];
										if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
											int i_204_ = (Class397_Sub1.anInt5767 >>> 24);
											int i_205_ = 256 - i_204_;
											int i_206_ = is_186_[i_185_];
											is_186_[i_185_] = (((((i_203_ & 0xff00ff) * i_204_)
													+ ((i_206_ & 0xff00ff) * i_205_)) & ~0xff00ff)
													+ ((((i_203_ & 0xff00) * i_204_) + ((i_206_ & 0xff00) * i_205_))
															& 0xff0000)) >> 8;
										} else if (Class397_Sub1.anInt5755 != 255) {
											int i_207_ = (((i_203_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_208_ = (((i_203_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_209_ = (((i_203_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											i_203_ = (i_207_ | i_208_ | i_209_) >>> 8;
											int i_210_ = is_186_[i_185_];
											is_186_[i_185_] = (((((i_203_ & 0xff00ff) * (Class397_Sub1.anInt5755))
													+ ((i_210_ & 0xff00ff) * (Class397_Sub1.anInt5749))) & ~0xff00ff)
													+ ((((i_203_ & 0xff00) * (Class397_Sub1.anInt5755))
															+ ((i_210_ & 0xff00) * (Class397_Sub1.anInt5749)))
															& 0xff0000)) >> 8;
										} else {
											int i_211_ = (((i_203_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_212_ = (((i_203_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_213_ = (((i_203_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											is_186_[i_185_] = (i_211_ | i_212_ | i_213_) >>> 8;
										}
									}
								} else if (i_187_ == 3) {
									byte i_214_ = aByteArray6691[i_184_];
									int i_215_ = (i_214_ > 0 ? anIntArray6692[i_214_] : 0);
									int i_216_ = Class397_Sub1.anInt5767;
									int i_217_ = i_215_ + i_216_;
									int i_218_ = ((i_215_ & 0xff00ff) + (i_216_ & 0xff00ff));
									int i_219_ = ((i_218_ & 0x1000100) + (i_217_ - i_218_ & 0x10000));
									i_219_ = i_217_ - i_219_ | i_219_ - (i_219_ >>> 8);
									if (i_215_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_215_ = i_219_;
										i_219_ = is_186_[i_185_];
										i_219_ = (((((i_215_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_219_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_215_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_219_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_186_[i_185_] = i_219_;
								} else if (i_187_ == 2) {
									int i_220_ = aByteArray6691[i_184_];
									if (i_220_ != 0) {
										int i_221_ = anIntArray6692[i_220_ & 0xff];
										int i_222_ = (((i_221_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_223_ = (((i_221_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										is_186_[i_185_++] = (((i_222_ | i_223_) >>> 8) + Class397_Sub1.anInt5773);
									}
								} else
									throw new IllegalArgumentException();
							} else if (i_188_ == 2) {
								if (i_187_ == 1) {
									int i_224_ = aByteArray6691[i_184_];
									if (i_224_ != 0) {
										int i_225_ = anIntArray6692[i_224_ & 0xff];
										int i_226_ = is_186_[i_185_];
										int i_227_ = i_225_ + i_226_;
										int i_228_ = ((i_225_ & 0xff00ff) + (i_226_ & 0xff00ff));
										i_226_ = ((i_228_ & 0x1000100) + (i_227_ - i_228_ & 0x10000));
										is_186_[i_185_] = (i_227_ - i_226_ | i_226_ - (i_226_ >>> 8));
									}
								} else if (i_187_ == 0) {
									int i_229_ = aByteArray6691[i_184_];
									if (i_229_ != 0) {
										int i_230_ = anIntArray6692[i_229_ & 0xff];
										int i_231_ = (((i_230_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_232_ = (((i_230_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_233_ = (((i_230_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_230_ = (i_231_ | i_232_ | i_233_) >>> 8;
										int i_234_ = is_186_[i_185_];
										int i_235_ = i_230_ + i_234_;
										int i_236_ = ((i_230_ & 0xff00ff) + (i_234_ & 0xff00ff));
										i_234_ = ((i_236_ & 0x1000100) + (i_235_ - i_236_ & 0x10000));
										is_186_[i_185_] = (i_235_ - i_234_ | i_234_ - (i_234_ >>> 8));
									}
								} else if (i_187_ == 3) {
									byte i_237_ = aByteArray6691[i_184_];
									int i_238_ = (i_237_ > 0 ? anIntArray6692[i_237_] : 0);
									int i_239_ = Class397_Sub1.anInt5767;
									int i_240_ = i_238_ + i_239_;
									int i_241_ = ((i_238_ & 0xff00ff) + (i_239_ & 0xff00ff));
									int i_242_ = ((i_241_ & 0x1000100) + (i_240_ - i_241_ & 0x10000));
									i_242_ = i_240_ - i_242_ | i_242_ - (i_242_ >>> 8);
									if (i_238_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_238_ = i_242_;
										i_242_ = is_186_[i_185_];
										i_242_ = (((((i_238_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_242_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_238_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_242_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_186_[i_185_] = i_242_;
								} else if (i_187_ == 2) {
									int i_243_ = aByteArray6691[i_184_];
									if (i_243_ != 0) {
										int i_244_ = anIntArray6692[i_243_ & 0xff];
										int i_245_ = (((i_244_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_246_ = (((i_244_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										i_244_ = (((i_245_ | i_246_) >>> 8) + Class397_Sub1.anInt5773);
										int i_247_ = is_186_[i_185_];
										int i_248_ = i_244_ + i_247_;
										int i_249_ = ((i_244_ & 0xff00ff) + (i_247_ & 0xff00ff));
										i_247_ = ((i_249_ & 0x1000100) + (i_248_ - i_249_ & 0x10000));
										is_186_[i_185_] = (i_248_ - i_247_ | i_247_ - (i_247_ >>> 8));
									}
								}
							} else
								throw new IllegalArgumentException();
						}
					}
					i_179_++;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_250_ = Class397_Sub1.anInt5771;
				while (i_250_ < 0) {
					int i_251_ = Class397_Sub1.anInt5759;
					int i_252_ = Class397_Sub1.anInt5765;
					int i_253_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_254_ = Class397_Sub1.anInt5770;
					if (i_252_ >= 0 && i_252_ - (anInt5758 << 12) < 0) {
						int i_255_;
						if ((i_255_ = i_253_ - (anInt5756 << 12)) >= 0) {
							i_255_ = ((Class397_Sub1.anInt5777 - i_255_) / Class397_Sub1.anInt5777);
							i_254_ += i_255_;
							i_253_ += Class397_Sub1.anInt5777 * i_255_;
							i_251_ += i_255_;
						}
						if ((i_255_ = ((i_253_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_254_)
							i_254_ = i_255_;
						for (/**/; i_254_ < 0; i_254_++) {
							int i_256_ = (i_253_ >> 12) * anInt5758 + (i_252_ >> 12);
							int i_257_ = i_251_++;
							int[] is_258_ = is;
							int i_259_ = i;
							int i_260_ = i_178_;
							if (i_260_ == 0) {
								if (i_259_ == 1)
									is_258_[i_257_] = (anIntArray6692[aByteArray6691[i_256_] & 0xff]);
								else if (i_259_ == 0) {
									int i_261_ = (anIntArray6692[aByteArray6691[i_256_] & 0xff]);
									int i_262_ = (((i_261_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_263_ = (((i_261_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_264_ = (((i_261_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_258_[i_257_] = (i_262_ | i_263_ | i_264_) >>> 8;
								} else if (i_259_ == 3) {
									int i_265_ = (anIntArray6692[aByteArray6691[i_256_] & 0xff]);
									int i_266_ = Class397_Sub1.anInt5767;
									int i_267_ = i_265_ + i_266_;
									int i_268_ = ((i_265_ & 0xff00ff) + (i_266_ & 0xff00ff));
									int i_269_ = ((i_268_ & 0x1000100) + (i_267_ - i_268_ & 0x10000));
									is_258_[i_257_] = i_267_ - i_269_ | i_269_ - (i_269_ >>> 8);
								} else if (i_259_ == 2) {
									int i_270_ = (anIntArray6692[aByteArray6691[i_256_] & 0xff]);
									int i_271_ = (((i_270_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_272_ = (((i_270_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_258_[i_257_] = (((i_271_ | i_272_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_260_ == 1) {
								if (i_259_ == 1) {
									int i_273_ = aByteArray6691[i_256_];
									if (i_273_ != 0)
										is_258_[i_257_] = anIntArray6692[i_273_ & 0xff];
								} else if (i_259_ == 0) {
									int i_274_ = aByteArray6691[i_256_];
									if (i_274_ != 0) {
										int i_275_ = anIntArray6692[i_274_ & 0xff];
										if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
											int i_276_ = (Class397_Sub1.anInt5767 >>> 24);
											int i_277_ = 256 - i_276_;
											int i_278_ = is_258_[i_257_];
											is_258_[i_257_] = (((((i_275_ & 0xff00ff) * i_276_)
													+ ((i_278_ & 0xff00ff) * i_277_)) & ~0xff00ff)
													+ ((((i_275_ & 0xff00) * i_276_) + ((i_278_ & 0xff00) * i_277_))
															& 0xff0000)) >> 8;
										} else if (Class397_Sub1.anInt5755 != 255) {
											int i_279_ = (((i_275_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_280_ = (((i_275_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_281_ = (((i_275_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											i_275_ = (i_279_ | i_280_ | i_281_) >>> 8;
											int i_282_ = is_258_[i_257_];
											is_258_[i_257_] = (((((i_275_ & 0xff00ff) * (Class397_Sub1.anInt5755))
													+ ((i_282_ & 0xff00ff) * (Class397_Sub1.anInt5749))) & ~0xff00ff)
													+ ((((i_275_ & 0xff00) * (Class397_Sub1.anInt5755))
															+ ((i_282_ & 0xff00) * (Class397_Sub1.anInt5749)))
															& 0xff0000)) >> 8;
										} else {
											int i_283_ = (((i_275_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_284_ = (((i_275_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_285_ = (((i_275_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											is_258_[i_257_] = (i_283_ | i_284_ | i_285_) >>> 8;
										}
									}
								} else if (i_259_ == 3) {
									byte i_286_ = aByteArray6691[i_256_];
									int i_287_ = (i_286_ > 0 ? anIntArray6692[i_286_] : 0);
									int i_288_ = Class397_Sub1.anInt5767;
									int i_289_ = i_287_ + i_288_;
									int i_290_ = ((i_287_ & 0xff00ff) + (i_288_ & 0xff00ff));
									int i_291_ = ((i_290_ & 0x1000100) + (i_289_ - i_290_ & 0x10000));
									i_291_ = i_289_ - i_291_ | i_291_ - (i_291_ >>> 8);
									if (i_287_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_287_ = i_291_;
										i_291_ = is_258_[i_257_];
										i_291_ = (((((i_287_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_291_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_287_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_291_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_258_[i_257_] = i_291_;
								} else if (i_259_ == 2) {
									int i_292_ = aByteArray6691[i_256_];
									if (i_292_ != 0) {
										int i_293_ = anIntArray6692[i_292_ & 0xff];
										int i_294_ = (((i_293_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_295_ = (((i_293_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										is_258_[i_257_++] = (((i_294_ | i_295_) >>> 8) + Class397_Sub1.anInt5773);
									}
								} else
									throw new IllegalArgumentException();
							} else if (i_260_ == 2) {
								if (i_259_ == 1) {
									int i_296_ = aByteArray6691[i_256_];
									if (i_296_ != 0) {
										int i_297_ = anIntArray6692[i_296_ & 0xff];
										int i_298_ = is_258_[i_257_];
										int i_299_ = i_297_ + i_298_;
										int i_300_ = ((i_297_ & 0xff00ff) + (i_298_ & 0xff00ff));
										i_298_ = ((i_300_ & 0x1000100) + (i_299_ - i_300_ & 0x10000));
										is_258_[i_257_] = (i_299_ - i_298_ | i_298_ - (i_298_ >>> 8));
									}
								} else if (i_259_ == 0) {
									int i_301_ = aByteArray6691[i_256_];
									if (i_301_ != 0) {
										int i_302_ = anIntArray6692[i_301_ & 0xff];
										int i_303_ = (((i_302_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_304_ = (((i_302_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_305_ = (((i_302_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_302_ = (i_303_ | i_304_ | i_305_) >>> 8;
										int i_306_ = is_258_[i_257_];
										int i_307_ = i_302_ + i_306_;
										int i_308_ = ((i_302_ & 0xff00ff) + (i_306_ & 0xff00ff));
										i_306_ = ((i_308_ & 0x1000100) + (i_307_ - i_308_ & 0x10000));
										is_258_[i_257_] = (i_307_ - i_306_ | i_306_ - (i_306_ >>> 8));
									}
								} else if (i_259_ == 3) {
									byte i_309_ = aByteArray6691[i_256_];
									int i_310_ = (i_309_ > 0 ? anIntArray6692[i_309_] : 0);
									int i_311_ = Class397_Sub1.anInt5767;
									int i_312_ = i_310_ + i_311_;
									int i_313_ = ((i_310_ & 0xff00ff) + (i_311_ & 0xff00ff));
									int i_314_ = ((i_313_ & 0x1000100) + (i_312_ - i_313_ & 0x10000));
									i_314_ = i_312_ - i_314_ | i_314_ - (i_314_ >>> 8);
									if (i_310_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_310_ = i_314_;
										i_314_ = is_258_[i_257_];
										i_314_ = (((((i_310_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_314_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_310_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_314_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_258_[i_257_] = i_314_;
								} else if (i_259_ == 2) {
									int i_315_ = aByteArray6691[i_256_];
									if (i_315_ != 0) {
										int i_316_ = anIntArray6692[i_315_ & 0xff];
										int i_317_ = (((i_316_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_318_ = (((i_316_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										i_316_ = (((i_317_ | i_318_) >>> 8) + Class397_Sub1.anInt5773);
										int i_319_ = is_258_[i_257_];
										int i_320_ = i_316_ + i_319_;
										int i_321_ = ((i_316_ & 0xff00ff) + (i_319_ & 0xff00ff));
										i_319_ = ((i_321_ & 0x1000100) + (i_320_ - i_321_ & 0x10000));
										is_258_[i_257_] = (i_320_ - i_319_ | i_319_ - (i_319_ >>> 8));
									}
								}
							} else
								throw new IllegalArgumentException();
							i_253_ += Class397_Sub1.anInt5777;
						}
					}
					i_250_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_322_ = Class397_Sub1.anInt5771;
				while (i_322_ < 0) {
					int i_323_ = Class397_Sub1.anInt5759;
					int i_324_ = Class397_Sub1.anInt5765;
					int i_325_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_326_ = Class397_Sub1.anInt5770;
					if (i_324_ >= 0 && i_324_ - (anInt5758 << 12) < 0) {
						if (i_325_ < 0) {
							int i_327_ = ((Class397_Sub1.anInt5777 - 1 - i_325_) / Class397_Sub1.anInt5777);
							i_326_ += i_327_;
							i_325_ += Class397_Sub1.anInt5777 * i_327_;
							i_323_ += i_327_;
						}
						int i_328_;
						if ((i_328_ = ((i_325_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
								/ Class397_Sub1.anInt5777)) > i_326_)
							i_326_ = i_328_;
						for (/**/; i_326_ < 0; i_326_++) {
							int i_329_ = (i_325_ >> 12) * anInt5758 + (i_324_ >> 12);
							int i_330_ = i_323_++;
							int[] is_331_ = is;
							int i_332_ = i;
							int i_333_ = i_178_;
							if (i_333_ == 0) {
								if (i_332_ == 1)
									is_331_[i_330_] = (anIntArray6692[aByteArray6691[i_329_] & 0xff]);
								else if (i_332_ == 0) {
									int i_334_ = (anIntArray6692[aByteArray6691[i_329_] & 0xff]);
									int i_335_ = (((i_334_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_336_ = (((i_334_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_337_ = (((i_334_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_331_[i_330_] = (i_335_ | i_336_ | i_337_) >>> 8;
								} else if (i_332_ == 3) {
									int i_338_ = (anIntArray6692[aByteArray6691[i_329_] & 0xff]);
									int i_339_ = Class397_Sub1.anInt5767;
									int i_340_ = i_338_ + i_339_;
									int i_341_ = ((i_338_ & 0xff00ff) + (i_339_ & 0xff00ff));
									int i_342_ = ((i_341_ & 0x1000100) + (i_340_ - i_341_ & 0x10000));
									is_331_[i_330_] = i_340_ - i_342_ | i_342_ - (i_342_ >>> 8);
								} else if (i_332_ == 2) {
									int i_343_ = (anIntArray6692[aByteArray6691[i_329_] & 0xff]);
									int i_344_ = (((i_343_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_345_ = (((i_343_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_331_[i_330_] = (((i_344_ | i_345_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_333_ == 1) {
								if (i_332_ == 1) {
									int i_346_ = aByteArray6691[i_329_];
									if (i_346_ != 0)
										is_331_[i_330_] = anIntArray6692[i_346_ & 0xff];
								} else if (i_332_ == 0) {
									int i_347_ = aByteArray6691[i_329_];
									if (i_347_ != 0) {
										int i_348_ = anIntArray6692[i_347_ & 0xff];
										if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
											int i_349_ = (Class397_Sub1.anInt5767 >>> 24);
											int i_350_ = 256 - i_349_;
											int i_351_ = is_331_[i_330_];
											is_331_[i_330_] = (((((i_348_ & 0xff00ff) * i_349_)
													+ ((i_351_ & 0xff00ff) * i_350_)) & ~0xff00ff)
													+ ((((i_348_ & 0xff00) * i_349_) + ((i_351_ & 0xff00) * i_350_))
															& 0xff0000)) >> 8;
										} else if (Class397_Sub1.anInt5755 != 255) {
											int i_352_ = (((i_348_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_353_ = (((i_348_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_354_ = (((i_348_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											i_348_ = (i_352_ | i_353_ | i_354_) >>> 8;
											int i_355_ = is_331_[i_330_];
											is_331_[i_330_] = (((((i_348_ & 0xff00ff) * (Class397_Sub1.anInt5755))
													+ ((i_355_ & 0xff00ff) * (Class397_Sub1.anInt5749))) & ~0xff00ff)
													+ ((((i_348_ & 0xff00) * (Class397_Sub1.anInt5755))
															+ ((i_355_ & 0xff00) * (Class397_Sub1.anInt5749)))
															& 0xff0000)) >> 8;
										} else {
											int i_356_ = (((i_348_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_357_ = (((i_348_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_358_ = (((i_348_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											is_331_[i_330_] = (i_356_ | i_357_ | i_358_) >>> 8;
										}
									}
								} else if (i_332_ == 3) {
									byte i_359_ = aByteArray6691[i_329_];
									int i_360_ = (i_359_ > 0 ? anIntArray6692[i_359_] : 0);
									int i_361_ = Class397_Sub1.anInt5767;
									int i_362_ = i_360_ + i_361_;
									int i_363_ = ((i_360_ & 0xff00ff) + (i_361_ & 0xff00ff));
									int i_364_ = ((i_363_ & 0x1000100) + (i_362_ - i_363_ & 0x10000));
									i_364_ = i_362_ - i_364_ | i_364_ - (i_364_ >>> 8);
									if (i_360_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_360_ = i_364_;
										i_364_ = is_331_[i_330_];
										i_364_ = (((((i_360_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_364_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_360_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_364_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_331_[i_330_] = i_364_;
								} else if (i_332_ == 2) {
									int i_365_ = aByteArray6691[i_329_];
									if (i_365_ != 0) {
										int i_366_ = anIntArray6692[i_365_ & 0xff];
										int i_367_ = (((i_366_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_368_ = (((i_366_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										is_331_[i_330_++] = (((i_367_ | i_368_) >>> 8) + Class397_Sub1.anInt5773);
									}
								} else
									throw new IllegalArgumentException();
							} else if (i_333_ == 2) {
								if (i_332_ == 1) {
									int i_369_ = aByteArray6691[i_329_];
									if (i_369_ != 0) {
										int i_370_ = anIntArray6692[i_369_ & 0xff];
										int i_371_ = is_331_[i_330_];
										int i_372_ = i_370_ + i_371_;
										int i_373_ = ((i_370_ & 0xff00ff) + (i_371_ & 0xff00ff));
										i_371_ = ((i_373_ & 0x1000100) + (i_372_ - i_373_ & 0x10000));
										is_331_[i_330_] = (i_372_ - i_371_ | i_371_ - (i_371_ >>> 8));
									}
								} else if (i_332_ == 0) {
									int i_374_ = aByteArray6691[i_329_];
									if (i_374_ != 0) {
										int i_375_ = anIntArray6692[i_374_ & 0xff];
										int i_376_ = (((i_375_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_377_ = (((i_375_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_378_ = (((i_375_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_375_ = (i_376_ | i_377_ | i_378_) >>> 8;
										int i_379_ = is_331_[i_330_];
										int i_380_ = i_375_ + i_379_;
										int i_381_ = ((i_375_ & 0xff00ff) + (i_379_ & 0xff00ff));
										i_379_ = ((i_381_ & 0x1000100) + (i_380_ - i_381_ & 0x10000));
										is_331_[i_330_] = (i_380_ - i_379_ | i_379_ - (i_379_ >>> 8));
									}
								} else if (i_332_ == 3) {
									byte i_382_ = aByteArray6691[i_329_];
									int i_383_ = (i_382_ > 0 ? anIntArray6692[i_382_] : 0);
									int i_384_ = Class397_Sub1.anInt5767;
									int i_385_ = i_383_ + i_384_;
									int i_386_ = ((i_383_ & 0xff00ff) + (i_384_ & 0xff00ff));
									int i_387_ = ((i_386_ & 0x1000100) + (i_385_ - i_386_ & 0x10000));
									i_387_ = i_385_ - i_387_ | i_387_ - (i_387_ >>> 8);
									if (i_383_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_383_ = i_387_;
										i_387_ = is_331_[i_330_];
										i_387_ = (((((i_383_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_387_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_383_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_387_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_331_[i_330_] = i_387_;
								} else if (i_332_ == 2) {
									int i_388_ = aByteArray6691[i_329_];
									if (i_388_ != 0) {
										int i_389_ = anIntArray6692[i_388_ & 0xff];
										int i_390_ = (((i_389_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_391_ = (((i_389_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										i_389_ = (((i_390_ | i_391_) >>> 8) + Class397_Sub1.anInt5773);
										int i_392_ = is_331_[i_330_];
										int i_393_ = i_389_ + i_392_;
										int i_394_ = ((i_389_ & 0xff00ff) + (i_392_ & 0xff00ff));
										i_392_ = ((i_394_ & 0x1000100) + (i_393_ - i_394_ & 0x10000));
										is_331_[i_330_] = (i_393_ - i_392_ | i_392_ - (i_392_ >>> 8));
									}
								}
							} else
								throw new IllegalArgumentException();
							i_325_ += Class397_Sub1.anInt5777;
						}
					}
					i_322_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5766 < 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_395_ = Class397_Sub1.anInt5771;
				while (i_395_ < 0) {
					int i_396_ = Class397_Sub1.anInt5759;
					int i_397_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_398_ = Class397_Sub1.anInt5775;
					int i_399_ = Class397_Sub1.anInt5770;
					if (i_398_ >= 0 && i_398_ - (anInt5756 << 12) < 0) {
						int i_400_;
						if ((i_400_ = i_397_ - (anInt5758 << 12)) >= 0) {
							i_400_ = ((Class397_Sub1.anInt5766 - i_400_) / Class397_Sub1.anInt5766);
							i_399_ += i_400_;
							i_397_ += Class397_Sub1.anInt5766 * i_400_;
							i_396_ += i_400_;
						}
						if ((i_400_ = ((i_397_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_399_)
							i_399_ = i_400_;
						for (/**/; i_399_ < 0; i_399_++) {
							int i_401_ = (i_398_ >> 12) * anInt5758 + (i_397_ >> 12);
							int i_402_ = i_396_++;
							int[] is_403_ = is;
							int i_404_ = i;
							int i_405_ = i_178_;
							if (i_405_ == 0) {
								if (i_404_ == 1)
									is_403_[i_402_] = (anIntArray6692[aByteArray6691[i_401_] & 0xff]);
								else if (i_404_ == 0) {
									int i_406_ = (anIntArray6692[aByteArray6691[i_401_] & 0xff]);
									int i_407_ = (((i_406_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_408_ = (((i_406_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_409_ = (((i_406_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_403_[i_402_] = (i_407_ | i_408_ | i_409_) >>> 8;
								} else if (i_404_ == 3) {
									int i_410_ = (anIntArray6692[aByteArray6691[i_401_] & 0xff]);
									int i_411_ = Class397_Sub1.anInt5767;
									int i_412_ = i_410_ + i_411_;
									int i_413_ = ((i_410_ & 0xff00ff) + (i_411_ & 0xff00ff));
									int i_414_ = ((i_413_ & 0x1000100) + (i_412_ - i_413_ & 0x10000));
									is_403_[i_402_] = i_412_ - i_414_ | i_414_ - (i_414_ >>> 8);
								} else if (i_404_ == 2) {
									int i_415_ = (anIntArray6692[aByteArray6691[i_401_] & 0xff]);
									int i_416_ = (((i_415_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_417_ = (((i_415_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_403_[i_402_] = (((i_416_ | i_417_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_405_ == 1) {
								if (i_404_ == 1) {
									int i_418_ = aByteArray6691[i_401_];
									if (i_418_ != 0)
										is_403_[i_402_] = anIntArray6692[i_418_ & 0xff];
								} else if (i_404_ == 0) {
									int i_419_ = aByteArray6691[i_401_];
									if (i_419_ != 0) {
										int i_420_ = anIntArray6692[i_419_ & 0xff];
										if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
											int i_421_ = (Class397_Sub1.anInt5767 >>> 24);
											int i_422_ = 256 - i_421_;
											int i_423_ = is_403_[i_402_];
											is_403_[i_402_] = (((((i_420_ & 0xff00ff) * i_421_)
													+ ((i_423_ & 0xff00ff) * i_422_)) & ~0xff00ff)
													+ ((((i_420_ & 0xff00) * i_421_) + ((i_423_ & 0xff00) * i_422_))
															& 0xff0000)) >> 8;
										} else if (Class397_Sub1.anInt5755 != 255) {
											int i_424_ = (((i_420_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_425_ = (((i_420_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_426_ = (((i_420_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											i_420_ = (i_424_ | i_425_ | i_426_) >>> 8;
											int i_427_ = is_403_[i_402_];
											is_403_[i_402_] = (((((i_420_ & 0xff00ff) * (Class397_Sub1.anInt5755))
													+ ((i_427_ & 0xff00ff) * (Class397_Sub1.anInt5749))) & ~0xff00ff)
													+ ((((i_420_ & 0xff00) * (Class397_Sub1.anInt5755))
															+ ((i_427_ & 0xff00) * (Class397_Sub1.anInt5749)))
															& 0xff0000)) >> 8;
										} else {
											int i_428_ = (((i_420_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_429_ = (((i_420_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_430_ = (((i_420_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											is_403_[i_402_] = (i_428_ | i_429_ | i_430_) >>> 8;
										}
									}
								} else if (i_404_ == 3) {
									byte i_431_ = aByteArray6691[i_401_];
									int i_432_ = (i_431_ > 0 ? anIntArray6692[i_431_] : 0);
									int i_433_ = Class397_Sub1.anInt5767;
									int i_434_ = i_432_ + i_433_;
									int i_435_ = ((i_432_ & 0xff00ff) + (i_433_ & 0xff00ff));
									int i_436_ = ((i_435_ & 0x1000100) + (i_434_ - i_435_ & 0x10000));
									i_436_ = i_434_ - i_436_ | i_436_ - (i_436_ >>> 8);
									if (i_432_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_432_ = i_436_;
										i_436_ = is_403_[i_402_];
										i_436_ = (((((i_432_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_436_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_432_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_436_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_403_[i_402_] = i_436_;
								} else if (i_404_ == 2) {
									int i_437_ = aByteArray6691[i_401_];
									if (i_437_ != 0) {
										int i_438_ = anIntArray6692[i_437_ & 0xff];
										int i_439_ = (((i_438_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_440_ = (((i_438_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										is_403_[i_402_++] = (((i_439_ | i_440_) >>> 8) + Class397_Sub1.anInt5773);
									}
								} else
									throw new IllegalArgumentException();
							} else if (i_405_ == 2) {
								if (i_404_ == 1) {
									int i_441_ = aByteArray6691[i_401_];
									if (i_441_ != 0) {
										int i_442_ = anIntArray6692[i_441_ & 0xff];
										int i_443_ = is_403_[i_402_];
										int i_444_ = i_442_ + i_443_;
										int i_445_ = ((i_442_ & 0xff00ff) + (i_443_ & 0xff00ff));
										i_443_ = ((i_445_ & 0x1000100) + (i_444_ - i_445_ & 0x10000));
										is_403_[i_402_] = (i_444_ - i_443_ | i_443_ - (i_443_ >>> 8));
									}
								} else if (i_404_ == 0) {
									int i_446_ = aByteArray6691[i_401_];
									if (i_446_ != 0) {
										int i_447_ = anIntArray6692[i_446_ & 0xff];
										int i_448_ = (((i_447_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_449_ = (((i_447_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_450_ = (((i_447_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_447_ = (i_448_ | i_449_ | i_450_) >>> 8;
										int i_451_ = is_403_[i_402_];
										int i_452_ = i_447_ + i_451_;
										int i_453_ = ((i_447_ & 0xff00ff) + (i_451_ & 0xff00ff));
										i_451_ = ((i_453_ & 0x1000100) + (i_452_ - i_453_ & 0x10000));
										is_403_[i_402_] = (i_452_ - i_451_ | i_451_ - (i_451_ >>> 8));
									}
								} else if (i_404_ == 3) {
									byte i_454_ = aByteArray6691[i_401_];
									int i_455_ = (i_454_ > 0 ? anIntArray6692[i_454_] : 0);
									int i_456_ = Class397_Sub1.anInt5767;
									int i_457_ = i_455_ + i_456_;
									int i_458_ = ((i_455_ & 0xff00ff) + (i_456_ & 0xff00ff));
									int i_459_ = ((i_458_ & 0x1000100) + (i_457_ - i_458_ & 0x10000));
									i_459_ = i_457_ - i_459_ | i_459_ - (i_459_ >>> 8);
									if (i_455_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_455_ = i_459_;
										i_459_ = is_403_[i_402_];
										i_459_ = (((((i_455_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_459_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_455_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_459_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_403_[i_402_] = i_459_;
								} else if (i_404_ == 2) {
									int i_460_ = aByteArray6691[i_401_];
									if (i_460_ != 0) {
										int i_461_ = anIntArray6692[i_460_ & 0xff];
										int i_462_ = (((i_461_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_463_ = (((i_461_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										i_461_ = (((i_462_ | i_463_) >>> 8) + Class397_Sub1.anInt5773);
										int i_464_ = is_403_[i_402_];
										int i_465_ = i_461_ + i_464_;
										int i_466_ = ((i_461_ & 0xff00ff) + (i_464_ & 0xff00ff));
										i_464_ = ((i_466_ & 0x1000100) + (i_465_ - i_466_ & 0x10000));
										is_403_[i_402_] = (i_465_ - i_464_ | i_464_ - (i_464_ >>> 8));
									}
								}
							} else
								throw new IllegalArgumentException();
							i_397_ += Class397_Sub1.anInt5766;
						}
					}
					i_395_++;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_467_ = Class397_Sub1.anInt5771;
				while (i_467_ < 0) {
					int i_468_ = Class397_Sub1.anInt5759;
					int i_469_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_470_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_471_ = Class397_Sub1.anInt5770;
					int i_472_;
					if ((i_472_ = i_469_ - (anInt5758 << 12)) >= 0) {
						i_472_ = ((Class397_Sub1.anInt5766 - i_472_) / Class397_Sub1.anInt5766);
						i_471_ += i_472_;
						i_469_ += Class397_Sub1.anInt5766 * i_472_;
						i_470_ += Class397_Sub1.anInt5777 * i_472_;
						i_468_ += i_472_;
					}
					if ((i_472_ = ((i_469_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_471_)
						i_471_ = i_472_;
					if ((i_472_ = i_470_ - (anInt5756 << 12)) >= 0) {
						i_472_ = ((Class397_Sub1.anInt5777 - i_472_) / Class397_Sub1.anInt5777);
						i_471_ += i_472_;
						i_469_ += Class397_Sub1.anInt5766 * i_472_;
						i_470_ += Class397_Sub1.anInt5777 * i_472_;
						i_468_ += i_472_;
					}
					if ((i_472_ = ((i_470_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_471_)
						i_471_ = i_472_;
					for (/**/; i_471_ < 0; i_471_++) {
						int i_473_ = (i_470_ >> 12) * anInt5758 + (i_469_ >> 12);
						int i_474_ = i_468_++;
						int[] is_475_ = is;
						int i_476_ = i;
						int i_477_ = i_178_;
						if (i_477_ == 0) {
							if (i_476_ == 1)
								is_475_[i_474_] = (anIntArray6692[aByteArray6691[i_473_] & 0xff]);
							else if (i_476_ == 0) {
								int i_478_ = (anIntArray6692[aByteArray6691[i_473_] & 0xff]);
								int i_479_ = (((i_478_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_480_ = (((i_478_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_481_ = (((i_478_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_475_[i_474_] = (i_479_ | i_480_ | i_481_) >>> 8;
							} else if (i_476_ == 3) {
								int i_482_ = (anIntArray6692[aByteArray6691[i_473_] & 0xff]);
								int i_483_ = Class397_Sub1.anInt5767;
								int i_484_ = i_482_ + i_483_;
								int i_485_ = ((i_482_ & 0xff00ff) + (i_483_ & 0xff00ff));
								int i_486_ = ((i_485_ & 0x1000100) + (i_484_ - i_485_ & 0x10000));
								is_475_[i_474_] = i_484_ - i_486_ | i_486_ - (i_486_ >>> 8);
							} else if (i_476_ == 2) {
								int i_487_ = (anIntArray6692[aByteArray6691[i_473_] & 0xff]);
								int i_488_ = (((i_487_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_489_ = (((i_487_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_475_[i_474_] = (((i_488_ | i_489_) >>> 8) + Class397_Sub1.anInt5773);
							} else
								throw new IllegalArgumentException();
						} else if (i_477_ == 1) {
							if (i_476_ == 1) {
								int i_490_ = aByteArray6691[i_473_];
								if (i_490_ != 0)
									is_475_[i_474_] = anIntArray6692[i_490_ & 0xff];
							} else if (i_476_ == 0) {
								int i_491_ = aByteArray6691[i_473_];
								if (i_491_ != 0) {
									int i_492_ = anIntArray6692[i_491_ & 0xff];
									if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
										int i_493_ = Class397_Sub1.anInt5767 >>> 24;
										int i_494_ = 256 - i_493_;
										int i_495_ = is_475_[i_474_];
										is_475_[i_474_] = ((((i_492_ & 0xff00ff) * i_493_
												+ ((i_495_ & 0xff00ff) * i_494_)) & ~0xff00ff)
												+ (((i_492_ & 0xff00) * i_493_ + ((i_495_ & 0xff00) * i_494_))
														& 0xff0000)) >> 8;
									} else if (Class397_Sub1.anInt5755 != 255) {
										int i_496_ = (((i_492_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_497_ = (((i_492_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_498_ = (((i_492_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_492_ = (i_496_ | i_497_ | i_498_) >>> 8;
										int i_499_ = is_475_[i_474_];
										is_475_[i_474_] = (((((i_492_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_499_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_492_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_499_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									} else {
										int i_500_ = (((i_492_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_501_ = (((i_492_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_502_ = (((i_492_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										is_475_[i_474_] = (i_500_ | i_501_ | i_502_) >>> 8;
									}
								}
							} else if (i_476_ == 3) {
								byte i_503_ = aByteArray6691[i_473_];
								int i_504_ = i_503_ > 0 ? anIntArray6692[i_503_] : 0;
								int i_505_ = Class397_Sub1.anInt5767;
								int i_506_ = i_504_ + i_505_;
								int i_507_ = ((i_504_ & 0xff00ff) + (i_505_ & 0xff00ff));
								int i_508_ = ((i_507_ & 0x1000100) + (i_506_ - i_507_ & 0x10000));
								i_508_ = i_506_ - i_508_ | i_508_ - (i_508_ >>> 8);
								if (i_504_ == 0 && Class397_Sub1.anInt5755 != 255) {
									i_504_ = i_508_;
									i_508_ = is_475_[i_474_];
									i_508_ = (((((i_504_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_508_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_504_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_508_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								}
								is_475_[i_474_] = i_508_;
							} else if (i_476_ == 2) {
								int i_509_ = aByteArray6691[i_473_];
								if (i_509_ != 0) {
									int i_510_ = anIntArray6692[i_509_ & 0xff];
									int i_511_ = (((i_510_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_512_ = (((i_510_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_475_[i_474_++] = (((i_511_ | i_512_) >>> 8) + Class397_Sub1.anInt5773);
								}
							} else
								throw new IllegalArgumentException();
						} else if (i_477_ == 2) {
							if (i_476_ == 1) {
								int i_513_ = aByteArray6691[i_473_];
								if (i_513_ != 0) {
									int i_514_ = anIntArray6692[i_513_ & 0xff];
									int i_515_ = is_475_[i_474_];
									int i_516_ = i_514_ + i_515_;
									int i_517_ = ((i_514_ & 0xff00ff) + (i_515_ & 0xff00ff));
									i_515_ = ((i_517_ & 0x1000100) + (i_516_ - i_517_ & 0x10000));
									is_475_[i_474_] = i_516_ - i_515_ | i_515_ - (i_515_ >>> 8);
								}
							} else if (i_476_ == 0) {
								int i_518_ = aByteArray6691[i_473_];
								if (i_518_ != 0) {
									int i_519_ = anIntArray6692[i_518_ & 0xff];
									int i_520_ = (((i_519_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_521_ = (((i_519_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_522_ = (((i_519_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_519_ = (i_520_ | i_521_ | i_522_) >>> 8;
									int i_523_ = is_475_[i_474_];
									int i_524_ = i_519_ + i_523_;
									int i_525_ = ((i_519_ & 0xff00ff) + (i_523_ & 0xff00ff));
									i_523_ = ((i_525_ & 0x1000100) + (i_524_ - i_525_ & 0x10000));
									is_475_[i_474_] = i_524_ - i_523_ | i_523_ - (i_523_ >>> 8);
								}
							} else if (i_476_ == 3) {
								byte i_526_ = aByteArray6691[i_473_];
								int i_527_ = i_526_ > 0 ? anIntArray6692[i_526_] : 0;
								int i_528_ = Class397_Sub1.anInt5767;
								int i_529_ = i_527_ + i_528_;
								int i_530_ = ((i_527_ & 0xff00ff) + (i_528_ & 0xff00ff));
								int i_531_ = ((i_530_ & 0x1000100) + (i_529_ - i_530_ & 0x10000));
								i_531_ = i_529_ - i_531_ | i_531_ - (i_531_ >>> 8);
								if (i_527_ == 0 && Class397_Sub1.anInt5755 != 255) {
									i_527_ = i_531_;
									i_531_ = is_475_[i_474_];
									i_531_ = (((((i_527_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_531_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_527_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_531_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								}
								is_475_[i_474_] = i_531_;
							} else if (i_476_ == 2) {
								int i_532_ = aByteArray6691[i_473_];
								if (i_532_ != 0) {
									int i_533_ = anIntArray6692[i_532_ & 0xff];
									int i_534_ = (((i_533_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_535_ = (((i_533_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_533_ = (((i_534_ | i_535_) >>> 8) + Class397_Sub1.anInt5773);
									int i_536_ = is_475_[i_474_];
									int i_537_ = i_533_ + i_536_;
									int i_538_ = ((i_533_ & 0xff00ff) + (i_536_ & 0xff00ff));
									i_536_ = ((i_538_ & 0x1000100) + (i_537_ - i_538_ & 0x10000));
									is_475_[i_474_] = i_537_ - i_536_ | i_536_ - (i_536_ >>> 8);
								}
							}
						} else
							throw new IllegalArgumentException();
						i_469_ += Class397_Sub1.anInt5766;
						i_470_ += Class397_Sub1.anInt5777;
					}
					i_467_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_539_ = Class397_Sub1.anInt5771;
				while (i_539_ < 0) {
					int i_540_ = Class397_Sub1.anInt5759;
					int i_541_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_542_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_543_ = Class397_Sub1.anInt5770;
					int i_544_;
					if ((i_544_ = i_541_ - (anInt5758 << 12)) >= 0) {
						i_544_ = ((Class397_Sub1.anInt5766 - i_544_) / Class397_Sub1.anInt5766);
						i_543_ += i_544_;
						i_541_ += Class397_Sub1.anInt5766 * i_544_;
						i_542_ += Class397_Sub1.anInt5777 * i_544_;
						i_540_ += i_544_;
					}
					if ((i_544_ = ((i_541_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_543_)
						i_543_ = i_544_;
					if (i_542_ < 0) {
						i_544_ = ((Class397_Sub1.anInt5777 - 1 - i_542_) / Class397_Sub1.anInt5777);
						i_543_ += i_544_;
						i_541_ += Class397_Sub1.anInt5766 * i_544_;
						i_542_ += Class397_Sub1.anInt5777 * i_544_;
						i_540_ += i_544_;
					}
					if ((i_544_ = ((i_542_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
							/ Class397_Sub1.anInt5777)) > i_543_)
						i_543_ = i_544_;
					for (/**/; i_543_ < 0; i_543_++) {
						int i_545_ = (i_542_ >> 12) * anInt5758 + (i_541_ >> 12);
						int i_546_ = i_540_++;
						int[] is_547_ = is;
						int i_548_ = i;
						int i_549_ = i_178_;
						if (i_549_ == 0) {
							if (i_548_ == 1)
								is_547_[i_546_] = (anIntArray6692[aByteArray6691[i_545_] & 0xff]);
							else if (i_548_ == 0) {
								int i_550_ = (anIntArray6692[aByteArray6691[i_545_] & 0xff]);
								int i_551_ = (((i_550_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_552_ = (((i_550_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_553_ = (((i_550_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_547_[i_546_] = (i_551_ | i_552_ | i_553_) >>> 8;
							} else if (i_548_ == 3) {
								int i_554_ = (anIntArray6692[aByteArray6691[i_545_] & 0xff]);
								int i_555_ = Class397_Sub1.anInt5767;
								int i_556_ = i_554_ + i_555_;
								int i_557_ = ((i_554_ & 0xff00ff) + (i_555_ & 0xff00ff));
								int i_558_ = ((i_557_ & 0x1000100) + (i_556_ - i_557_ & 0x10000));
								is_547_[i_546_] = i_556_ - i_558_ | i_558_ - (i_558_ >>> 8);
							} else if (i_548_ == 2) {
								int i_559_ = (anIntArray6692[aByteArray6691[i_545_] & 0xff]);
								int i_560_ = (((i_559_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_561_ = (((i_559_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_547_[i_546_] = (((i_560_ | i_561_) >>> 8) + Class397_Sub1.anInt5773);
							} else
								throw new IllegalArgumentException();
						} else if (i_549_ == 1) {
							if (i_548_ == 1) {
								int i_562_ = aByteArray6691[i_545_];
								if (i_562_ != 0)
									is_547_[i_546_] = anIntArray6692[i_562_ & 0xff];
							} else if (i_548_ == 0) {
								int i_563_ = aByteArray6691[i_545_];
								if (i_563_ != 0) {
									int i_564_ = anIntArray6692[i_563_ & 0xff];
									if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
										int i_565_ = Class397_Sub1.anInt5767 >>> 24;
										int i_566_ = 256 - i_565_;
										int i_567_ = is_547_[i_546_];
										is_547_[i_546_] = ((((i_564_ & 0xff00ff) * i_565_
												+ ((i_567_ & 0xff00ff) * i_566_)) & ~0xff00ff)
												+ (((i_564_ & 0xff00) * i_565_ + ((i_567_ & 0xff00) * i_566_))
														& 0xff0000)) >> 8;
									} else if (Class397_Sub1.anInt5755 != 255) {
										int i_568_ = (((i_564_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_569_ = (((i_564_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_570_ = (((i_564_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_564_ = (i_568_ | i_569_ | i_570_) >>> 8;
										int i_571_ = is_547_[i_546_];
										is_547_[i_546_] = (((((i_564_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_571_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_564_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_571_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									} else {
										int i_572_ = (((i_564_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_573_ = (((i_564_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_574_ = (((i_564_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										is_547_[i_546_] = (i_572_ | i_573_ | i_574_) >>> 8;
									}
								}
							} else if (i_548_ == 3) {
								byte i_575_ = aByteArray6691[i_545_];
								int i_576_ = i_575_ > 0 ? anIntArray6692[i_575_] : 0;
								int i_577_ = Class397_Sub1.anInt5767;
								int i_578_ = i_576_ + i_577_;
								int i_579_ = ((i_576_ & 0xff00ff) + (i_577_ & 0xff00ff));
								int i_580_ = ((i_579_ & 0x1000100) + (i_578_ - i_579_ & 0x10000));
								i_580_ = i_578_ - i_580_ | i_580_ - (i_580_ >>> 8);
								if (i_576_ == 0 && Class397_Sub1.anInt5755 != 255) {
									i_576_ = i_580_;
									i_580_ = is_547_[i_546_];
									i_580_ = (((((i_576_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_580_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_576_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_580_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								}
								is_547_[i_546_] = i_580_;
							} else if (i_548_ == 2) {
								int i_581_ = aByteArray6691[i_545_];
								if (i_581_ != 0) {
									int i_582_ = anIntArray6692[i_581_ & 0xff];
									int i_583_ = (((i_582_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_584_ = (((i_582_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_547_[i_546_++] = (((i_583_ | i_584_) >>> 8) + Class397_Sub1.anInt5773);
								}
							} else
								throw new IllegalArgumentException();
						} else if (i_549_ == 2) {
							if (i_548_ == 1) {
								int i_585_ = aByteArray6691[i_545_];
								if (i_585_ != 0) {
									int i_586_ = anIntArray6692[i_585_ & 0xff];
									int i_587_ = is_547_[i_546_];
									int i_588_ = i_586_ + i_587_;
									int i_589_ = ((i_586_ & 0xff00ff) + (i_587_ & 0xff00ff));
									i_587_ = ((i_589_ & 0x1000100) + (i_588_ - i_589_ & 0x10000));
									is_547_[i_546_] = i_588_ - i_587_ | i_587_ - (i_587_ >>> 8);
								}
							} else if (i_548_ == 0) {
								int i_590_ = aByteArray6691[i_545_];
								if (i_590_ != 0) {
									int i_591_ = anIntArray6692[i_590_ & 0xff];
									int i_592_ = (((i_591_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_593_ = (((i_591_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_594_ = (((i_591_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_591_ = (i_592_ | i_593_ | i_594_) >>> 8;
									int i_595_ = is_547_[i_546_];
									int i_596_ = i_591_ + i_595_;
									int i_597_ = ((i_591_ & 0xff00ff) + (i_595_ & 0xff00ff));
									i_595_ = ((i_597_ & 0x1000100) + (i_596_ - i_597_ & 0x10000));
									is_547_[i_546_] = i_596_ - i_595_ | i_595_ - (i_595_ >>> 8);
								}
							} else if (i_548_ == 3) {
								byte i_598_ = aByteArray6691[i_545_];
								int i_599_ = i_598_ > 0 ? anIntArray6692[i_598_] : 0;
								int i_600_ = Class397_Sub1.anInt5767;
								int i_601_ = i_599_ + i_600_;
								int i_602_ = ((i_599_ & 0xff00ff) + (i_600_ & 0xff00ff));
								int i_603_ = ((i_602_ & 0x1000100) + (i_601_ - i_602_ & 0x10000));
								i_603_ = i_601_ - i_603_ | i_603_ - (i_603_ >>> 8);
								if (i_599_ == 0 && Class397_Sub1.anInt5755 != 255) {
									i_599_ = i_603_;
									i_603_ = is_547_[i_546_];
									i_603_ = (((((i_599_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_603_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_599_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_603_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								}
								is_547_[i_546_] = i_603_;
							} else if (i_548_ == 2) {
								int i_604_ = aByteArray6691[i_545_];
								if (i_604_ != 0) {
									int i_605_ = anIntArray6692[i_604_ & 0xff];
									int i_606_ = (((i_605_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_607_ = (((i_605_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_605_ = (((i_606_ | i_607_) >>> 8) + Class397_Sub1.anInt5773);
									int i_608_ = is_547_[i_546_];
									int i_609_ = i_605_ + i_608_;
									int i_610_ = ((i_605_ & 0xff00ff) + (i_608_ & 0xff00ff));
									i_608_ = ((i_610_ & 0x1000100) + (i_609_ - i_610_ & 0x10000));
									is_547_[i_546_] = i_609_ - i_608_ | i_608_ - (i_608_ >>> 8);
								}
							}
						} else
							throw new IllegalArgumentException();
						i_541_ += Class397_Sub1.anInt5766;
						i_542_ += Class397_Sub1.anInt5777;
					}
					i_539_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5777 == 0) {
			int i_611_ = Class397_Sub1.anInt5771;
			while (i_611_ < 0) {
				int i_612_ = Class397_Sub1.anInt5759;
				int i_613_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
				int i_614_ = Class397_Sub1.anInt5775;
				int i_615_ = Class397_Sub1.anInt5770;
				if (i_614_ >= 0 && i_614_ - (anInt5756 << 12) < 0) {
					if (i_613_ < 0) {
						int i_616_ = ((Class397_Sub1.anInt5766 - 1 - i_613_) / Class397_Sub1.anInt5766);
						i_615_ += i_616_;
						i_613_ += Class397_Sub1.anInt5766 * i_616_;
						i_612_ += i_616_;
					}
					int i_617_;
					if ((i_617_ = ((i_613_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
							/ Class397_Sub1.anInt5766)) > i_615_)
						i_615_ = i_617_;
					for (/**/; i_615_ < 0; i_615_++) {
						int i_618_ = (i_614_ >> 12) * anInt5758 + (i_613_ >> 12);
						int i_619_ = i_612_++;
						int[] is_620_ = is;
						int i_621_ = i;
						int i_622_ = i_178_;
						if (i_622_ == 0) {
							if (i_621_ == 1)
								is_620_[i_619_] = (anIntArray6692[aByteArray6691[i_618_] & 0xff]);
							else if (i_621_ == 0) {
								int i_623_ = (anIntArray6692[aByteArray6691[i_618_] & 0xff]);
								int i_624_ = (((i_623_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_625_ = (((i_623_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_626_ = (((i_623_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_620_[i_619_] = (i_624_ | i_625_ | i_626_) >>> 8;
							} else if (i_621_ == 3) {
								int i_627_ = (anIntArray6692[aByteArray6691[i_618_] & 0xff]);
								int i_628_ = Class397_Sub1.anInt5767;
								int i_629_ = i_627_ + i_628_;
								int i_630_ = ((i_627_ & 0xff00ff) + (i_628_ & 0xff00ff));
								int i_631_ = ((i_630_ & 0x1000100) + (i_629_ - i_630_ & 0x10000));
								is_620_[i_619_] = i_629_ - i_631_ | i_631_ - (i_631_ >>> 8);
							} else if (i_621_ == 2) {
								int i_632_ = (anIntArray6692[aByteArray6691[i_618_] & 0xff]);
								int i_633_ = (((i_632_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_634_ = (((i_632_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_620_[i_619_] = (((i_633_ | i_634_) >>> 8) + Class397_Sub1.anInt5773);
							} else
								throw new IllegalArgumentException();
						} else if (i_622_ == 1) {
							if (i_621_ == 1) {
								int i_635_ = aByteArray6691[i_618_];
								if (i_635_ != 0)
									is_620_[i_619_] = anIntArray6692[i_635_ & 0xff];
							} else if (i_621_ == 0) {
								int i_636_ = aByteArray6691[i_618_];
								if (i_636_ != 0) {
									int i_637_ = anIntArray6692[i_636_ & 0xff];
									if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
										int i_638_ = Class397_Sub1.anInt5767 >>> 24;
										int i_639_ = 256 - i_638_;
										int i_640_ = is_620_[i_619_];
										is_620_[i_619_] = ((((i_637_ & 0xff00ff) * i_638_
												+ ((i_640_ & 0xff00ff) * i_639_)) & ~0xff00ff)
												+ (((i_637_ & 0xff00) * i_638_ + ((i_640_ & 0xff00) * i_639_))
														& 0xff0000)) >> 8;
									} else if (Class397_Sub1.anInt5755 != 255) {
										int i_641_ = (((i_637_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_642_ = (((i_637_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_643_ = (((i_637_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_637_ = (i_641_ | i_642_ | i_643_) >>> 8;
										int i_644_ = is_620_[i_619_];
										is_620_[i_619_] = (((((i_637_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_644_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_637_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_644_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									} else {
										int i_645_ = (((i_637_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_646_ = (((i_637_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_647_ = (((i_637_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										is_620_[i_619_] = (i_645_ | i_646_ | i_647_) >>> 8;
									}
								}
							} else if (i_621_ == 3) {
								byte i_648_ = aByteArray6691[i_618_];
								int i_649_ = i_648_ > 0 ? anIntArray6692[i_648_] : 0;
								int i_650_ = Class397_Sub1.anInt5767;
								int i_651_ = i_649_ + i_650_;
								int i_652_ = ((i_649_ & 0xff00ff) + (i_650_ & 0xff00ff));
								int i_653_ = ((i_652_ & 0x1000100) + (i_651_ - i_652_ & 0x10000));
								i_653_ = i_651_ - i_653_ | i_653_ - (i_653_ >>> 8);
								if (i_649_ == 0 && Class397_Sub1.anInt5755 != 255) {
									i_649_ = i_653_;
									i_653_ = is_620_[i_619_];
									i_653_ = (((((i_649_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_653_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_649_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_653_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								}
								is_620_[i_619_] = i_653_;
							} else if (i_621_ == 2) {
								int i_654_ = aByteArray6691[i_618_];
								if (i_654_ != 0) {
									int i_655_ = anIntArray6692[i_654_ & 0xff];
									int i_656_ = (((i_655_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_657_ = (((i_655_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_620_[i_619_++] = (((i_656_ | i_657_) >>> 8) + Class397_Sub1.anInt5773);
								}
							} else
								throw new IllegalArgumentException();
						} else if (i_622_ == 2) {
							if (i_621_ == 1) {
								int i_658_ = aByteArray6691[i_618_];
								if (i_658_ != 0) {
									int i_659_ = anIntArray6692[i_658_ & 0xff];
									int i_660_ = is_620_[i_619_];
									int i_661_ = i_659_ + i_660_;
									int i_662_ = ((i_659_ & 0xff00ff) + (i_660_ & 0xff00ff));
									i_660_ = ((i_662_ & 0x1000100) + (i_661_ - i_662_ & 0x10000));
									is_620_[i_619_] = i_661_ - i_660_ | i_660_ - (i_660_ >>> 8);
								}
							} else if (i_621_ == 0) {
								int i_663_ = aByteArray6691[i_618_];
								if (i_663_ != 0) {
									int i_664_ = anIntArray6692[i_663_ & 0xff];
									int i_665_ = (((i_664_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_666_ = (((i_664_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_667_ = (((i_664_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_664_ = (i_665_ | i_666_ | i_667_) >>> 8;
									int i_668_ = is_620_[i_619_];
									int i_669_ = i_664_ + i_668_;
									int i_670_ = ((i_664_ & 0xff00ff) + (i_668_ & 0xff00ff));
									i_668_ = ((i_670_ & 0x1000100) + (i_669_ - i_670_ & 0x10000));
									is_620_[i_619_] = i_669_ - i_668_ | i_668_ - (i_668_ >>> 8);
								}
							} else if (i_621_ == 3) {
								byte i_671_ = aByteArray6691[i_618_];
								int i_672_ = i_671_ > 0 ? anIntArray6692[i_671_] : 0;
								int i_673_ = Class397_Sub1.anInt5767;
								int i_674_ = i_672_ + i_673_;
								int i_675_ = ((i_672_ & 0xff00ff) + (i_673_ & 0xff00ff));
								int i_676_ = ((i_675_ & 0x1000100) + (i_674_ - i_675_ & 0x10000));
								i_676_ = i_674_ - i_676_ | i_676_ - (i_676_ >>> 8);
								if (i_672_ == 0 && Class397_Sub1.anInt5755 != 255) {
									i_672_ = i_676_;
									i_676_ = is_620_[i_619_];
									i_676_ = (((((i_672_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_676_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_672_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_676_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								}
								is_620_[i_619_] = i_676_;
							} else if (i_621_ == 2) {
								int i_677_ = aByteArray6691[i_618_];
								if (i_677_ != 0) {
									int i_678_ = anIntArray6692[i_677_ & 0xff];
									int i_679_ = (((i_678_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_680_ = (((i_678_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_678_ = (((i_679_ | i_680_) >>> 8) + Class397_Sub1.anInt5773);
									int i_681_ = is_620_[i_619_];
									int i_682_ = i_678_ + i_681_;
									int i_683_ = ((i_678_ & 0xff00ff) + (i_681_ & 0xff00ff));
									i_681_ = ((i_683_ & 0x1000100) + (i_682_ - i_683_ & 0x10000));
									is_620_[i_619_] = i_682_ - i_681_ | i_681_ - (i_681_ >>> 8);
								}
							}
						} else
							throw new IllegalArgumentException();
						i_613_ += Class397_Sub1.anInt5766;
					}
				}
				i_611_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else if (Class397_Sub1.anInt5777 < 0) {
			for (int i_684_ = Class397_Sub1.anInt5771; i_684_ < 0; i_684_++) {
				int i_685_ = Class397_Sub1.anInt5759;
				int i_686_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
				int i_687_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
				int i_688_ = Class397_Sub1.anInt5770;
				if (i_686_ < 0) {
					int i_689_ = ((Class397_Sub1.anInt5766 - 1 - i_686_) / Class397_Sub1.anInt5766);
					i_688_ += i_689_;
					i_686_ += Class397_Sub1.anInt5766 * i_689_;
					i_687_ += Class397_Sub1.anInt5777 * i_689_;
					i_685_ += i_689_;
				}
				int i_690_;
				if ((i_690_ = (i_686_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
						/ Class397_Sub1.anInt5766) > i_688_)
					i_688_ = i_690_;
				if ((i_690_ = i_687_ - (anInt5756 << 12)) >= 0) {
					i_690_ = ((Class397_Sub1.anInt5777 - i_690_) / Class397_Sub1.anInt5777);
					i_688_ += i_690_;
					i_686_ += Class397_Sub1.anInt5766 * i_690_;
					i_687_ += Class397_Sub1.anInt5777 * i_690_;
					i_685_ += i_690_;
				}
				if ((i_690_ = ((i_687_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_688_)
					i_688_ = i_690_;
				for (/**/; i_688_ < 0; i_688_++) {
					int i_691_ = (i_687_ >> 12) * anInt5758 + (i_686_ >> 12);
					int i_692_ = i_685_++;
					int[] is_693_ = is;
					int i_694_ = i;
					int i_695_ = i_178_;
					if (i_695_ == 0) {
						if (i_694_ == 1)
							is_693_[i_692_] = (anIntArray6692[aByteArray6691[i_691_] & 0xff]);
						else if (i_694_ == 0) {
							int i_696_ = (anIntArray6692[aByteArray6691[i_691_] & 0xff]);
							int i_697_ = (((i_696_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
							int i_698_ = ((i_696_ & 0xff00) * Class397_Sub1.anInt5753 & 0xff0000);
							int i_699_ = ((i_696_ & 0xff) * Class397_Sub1.anInt5752 & 0xff00);
							is_693_[i_692_] = (i_697_ | i_698_ | i_699_) >>> 8;
						} else if (i_694_ == 3) {
							int i_700_ = (anIntArray6692[aByteArray6691[i_691_] & 0xff]);
							int i_701_ = Class397_Sub1.anInt5767;
							int i_702_ = i_700_ + i_701_;
							int i_703_ = (i_700_ & 0xff00ff) + (i_701_ & 0xff00ff);
							int i_704_ = ((i_703_ & 0x1000100) + (i_702_ - i_703_ & 0x10000));
							is_693_[i_692_] = i_702_ - i_704_ | i_704_ - (i_704_ >>> 8);
						} else if (i_694_ == 2) {
							int i_705_ = (anIntArray6692[aByteArray6691[i_691_] & 0xff]);
							int i_706_ = (((i_705_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
							int i_707_ = ((i_705_ & 0xff00) * Class397_Sub1.anInt5755 & 0xff0000);
							is_693_[i_692_] = (((i_706_ | i_707_) >>> 8) + Class397_Sub1.anInt5773);
						} else
							throw new IllegalArgumentException();
					} else if (i_695_ == 1) {
						if (i_694_ == 1) {
							int i_708_ = aByteArray6691[i_691_];
							if (i_708_ != 0)
								is_693_[i_692_] = anIntArray6692[i_708_ & 0xff];
						} else if (i_694_ == 0) {
							int i_709_ = aByteArray6691[i_691_];
							if (i_709_ != 0) {
								int i_710_ = anIntArray6692[i_709_ & 0xff];
								if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
									int i_711_ = Class397_Sub1.anInt5767 >>> 24;
									int i_712_ = 256 - i_711_;
									int i_713_ = is_693_[i_692_];
									is_693_[i_692_] = ((((i_710_ & 0xff00ff) * i_711_ + (i_713_ & 0xff00ff) * i_712_)
											& ~0xff00ff)
											+ (((i_710_ & 0xff00) * i_711_ + (i_713_ & 0xff00) * i_712_)
													& 0xff0000)) >> 8;
								} else if (Class397_Sub1.anInt5755 != 255) {
									int i_714_ = (((i_710_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_715_ = (((i_710_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_716_ = (((i_710_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_710_ = (i_714_ | i_715_ | i_716_) >>> 8;
									int i_717_ = is_693_[i_692_];
									is_693_[i_692_] = (((((i_710_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_717_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_710_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_717_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								} else {
									int i_718_ = (((i_710_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_719_ = (((i_710_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_720_ = (((i_710_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_693_[i_692_] = (i_718_ | i_719_ | i_720_) >>> 8;
								}
							}
						} else if (i_694_ == 3) {
							byte i_721_ = aByteArray6691[i_691_];
							int i_722_ = i_721_ > 0 ? anIntArray6692[i_721_] : 0;
							int i_723_ = Class397_Sub1.anInt5767;
							int i_724_ = i_722_ + i_723_;
							int i_725_ = (i_722_ & 0xff00ff) + (i_723_ & 0xff00ff);
							int i_726_ = ((i_725_ & 0x1000100) + (i_724_ - i_725_ & 0x10000));
							i_726_ = i_724_ - i_726_ | i_726_ - (i_726_ >>> 8);
							if (i_722_ == 0 && Class397_Sub1.anInt5755 != 255) {
								i_722_ = i_726_;
								i_726_ = is_693_[i_692_];
								i_726_ = (((((i_722_ & 0xff00ff) * Class397_Sub1.anInt5755)
										+ ((i_726_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
										+ ((((i_722_ & 0xff00) * Class397_Sub1.anInt5755)
												+ ((i_726_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
							}
							is_693_[i_692_] = i_726_;
						} else if (i_694_ == 2) {
							int i_727_ = aByteArray6691[i_691_];
							if (i_727_ != 0) {
								int i_728_ = anIntArray6692[i_727_ & 0xff];
								int i_729_ = (((i_728_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_730_ = (((i_728_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_693_[i_692_++] = (((i_729_ | i_730_) >>> 8) + Class397_Sub1.anInt5773);
							}
						} else
							throw new IllegalArgumentException();
					} else if (i_695_ == 2) {
						if (i_694_ == 1) {
							int i_731_ = aByteArray6691[i_691_];
							if (i_731_ != 0) {
								int i_732_ = anIntArray6692[i_731_ & 0xff];
								int i_733_ = is_693_[i_692_];
								int i_734_ = i_732_ + i_733_;
								int i_735_ = ((i_732_ & 0xff00ff) + (i_733_ & 0xff00ff));
								i_733_ = (i_735_ & 0x1000100) + (i_734_ - i_735_ & 0x10000);
								is_693_[i_692_] = i_734_ - i_733_ | i_733_ - (i_733_ >>> 8);
							}
						} else if (i_694_ == 0) {
							int i_736_ = aByteArray6691[i_691_];
							if (i_736_ != 0) {
								int i_737_ = anIntArray6692[i_736_ & 0xff];
								int i_738_ = (((i_737_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_739_ = (((i_737_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_740_ = (((i_737_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								i_737_ = (i_738_ | i_739_ | i_740_) >>> 8;
								int i_741_ = is_693_[i_692_];
								int i_742_ = i_737_ + i_741_;
								int i_743_ = ((i_737_ & 0xff00ff) + (i_741_ & 0xff00ff));
								i_741_ = (i_743_ & 0x1000100) + (i_742_ - i_743_ & 0x10000);
								is_693_[i_692_] = i_742_ - i_741_ | i_741_ - (i_741_ >>> 8);
							}
						} else if (i_694_ == 3) {
							byte i_744_ = aByteArray6691[i_691_];
							int i_745_ = i_744_ > 0 ? anIntArray6692[i_744_] : 0;
							int i_746_ = Class397_Sub1.anInt5767;
							int i_747_ = i_745_ + i_746_;
							int i_748_ = (i_745_ & 0xff00ff) + (i_746_ & 0xff00ff);
							int i_749_ = ((i_748_ & 0x1000100) + (i_747_ - i_748_ & 0x10000));
							i_749_ = i_747_ - i_749_ | i_749_ - (i_749_ >>> 8);
							if (i_745_ == 0 && Class397_Sub1.anInt5755 != 255) {
								i_745_ = i_749_;
								i_749_ = is_693_[i_692_];
								i_749_ = (((((i_745_ & 0xff00ff) * Class397_Sub1.anInt5755)
										+ ((i_749_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
										+ ((((i_745_ & 0xff00) * Class397_Sub1.anInt5755)
												+ ((i_749_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
							}
							is_693_[i_692_] = i_749_;
						} else if (i_694_ == 2) {
							int i_750_ = aByteArray6691[i_691_];
							if (i_750_ != 0) {
								int i_751_ = anIntArray6692[i_750_ & 0xff];
								int i_752_ = (((i_751_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_753_ = (((i_751_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								i_751_ = (((i_752_ | i_753_) >>> 8) + Class397_Sub1.anInt5773);
								int i_754_ = is_693_[i_692_];
								int i_755_ = i_751_ + i_754_;
								int i_756_ = ((i_751_ & 0xff00ff) + (i_754_ & 0xff00ff));
								i_754_ = (i_756_ & 0x1000100) + (i_755_ - i_756_ & 0x10000);
								is_693_[i_692_] = i_755_ - i_754_ | i_754_ - (i_754_ >>> 8);
							}
						}
					} else
						throw new IllegalArgumentException();
					i_686_ += Class397_Sub1.anInt5766;
					i_687_ += Class397_Sub1.anInt5777;
				}
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else {
			for (int i_757_ = Class397_Sub1.anInt5771; i_757_ < 0; i_757_++) {
				int i_758_ = Class397_Sub1.anInt5759;
				int i_759_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
				int i_760_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
				int i_761_ = Class397_Sub1.anInt5770;
				if (i_759_ < 0) {
					int i_762_ = ((Class397_Sub1.anInt5766 - 1 - i_759_) / Class397_Sub1.anInt5766);
					i_761_ += i_762_;
					i_759_ += Class397_Sub1.anInt5766 * i_762_;
					i_760_ += Class397_Sub1.anInt5777 * i_762_;
					i_758_ += i_762_;
				}
				int i_763_;
				if ((i_763_ = (i_759_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
						/ Class397_Sub1.anInt5766) > i_761_)
					i_761_ = i_763_;
				if (i_760_ < 0) {
					i_763_ = ((Class397_Sub1.anInt5777 - 1 - i_760_) / Class397_Sub1.anInt5777);
					i_761_ += i_763_;
					i_759_ += Class397_Sub1.anInt5766 * i_763_;
					i_760_ += Class397_Sub1.anInt5777 * i_763_;
					i_758_ += i_763_;
				}
				if ((i_763_ = (i_760_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
						/ Class397_Sub1.anInt5777) > i_761_)
					i_761_ = i_763_;
				for (/**/; i_761_ < 0; i_761_++) {
					int i_764_ = (i_760_ >> 12) * anInt5758 + (i_759_ >> 12);
					int i_765_ = i_758_++;
					int[] is_766_ = is;
					int i_767_ = i;
					int i_768_ = i_178_;
					if (i_768_ == 0) {
						if (i_767_ == 1)
							is_766_[i_765_] = (anIntArray6692[aByteArray6691[i_764_] & 0xff]);
						else if (i_767_ == 0) {
							int i_769_ = (anIntArray6692[aByteArray6691[i_764_] & 0xff]);
							int i_770_ = (((i_769_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
							int i_771_ = ((i_769_ & 0xff00) * Class397_Sub1.anInt5753 & 0xff0000);
							int i_772_ = ((i_769_ & 0xff) * Class397_Sub1.anInt5752 & 0xff00);
							is_766_[i_765_] = (i_770_ | i_771_ | i_772_) >>> 8;
						} else if (i_767_ == 3) {
							int i_773_ = (anIntArray6692[aByteArray6691[i_764_] & 0xff]);
							int i_774_ = Class397_Sub1.anInt5767;
							int i_775_ = i_773_ + i_774_;
							int i_776_ = (i_773_ & 0xff00ff) + (i_774_ & 0xff00ff);
							int i_777_ = ((i_776_ & 0x1000100) + (i_775_ - i_776_ & 0x10000));
							is_766_[i_765_] = i_775_ - i_777_ | i_777_ - (i_777_ >>> 8);
						} else if (i_767_ == 2) {
							int i_778_ = (anIntArray6692[aByteArray6691[i_764_] & 0xff]);
							int i_779_ = (((i_778_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
							int i_780_ = ((i_778_ & 0xff00) * Class397_Sub1.anInt5755 & 0xff0000);
							is_766_[i_765_] = (((i_779_ | i_780_) >>> 8) + Class397_Sub1.anInt5773);
						} else
							throw new IllegalArgumentException();
					} else if (i_768_ == 1) {
						if (i_767_ == 1) {
							int i_781_ = aByteArray6691[i_764_];
							if (i_781_ != 0)
								is_766_[i_765_] = anIntArray6692[i_781_ & 0xff];
						} else if (i_767_ == 0) {
							int i_782_ = aByteArray6691[i_764_];
							if (i_782_ != 0) {
								int i_783_ = anIntArray6692[i_782_ & 0xff];
								if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
									int i_784_ = Class397_Sub1.anInt5767 >>> 24;
									int i_785_ = 256 - i_784_;
									int i_786_ = is_766_[i_765_];
									is_766_[i_765_] = ((((i_783_ & 0xff00ff) * i_784_ + (i_786_ & 0xff00ff) * i_785_)
											& ~0xff00ff)
											+ (((i_783_ & 0xff00) * i_784_ + (i_786_ & 0xff00) * i_785_)
													& 0xff0000)) >> 8;
								} else if (Class397_Sub1.anInt5755 != 255) {
									int i_787_ = (((i_783_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_788_ = (((i_783_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_789_ = (((i_783_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_783_ = (i_787_ | i_788_ | i_789_) >>> 8;
									int i_790_ = is_766_[i_765_];
									is_766_[i_765_] = (((((i_783_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_790_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_783_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_790_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								} else {
									int i_791_ = (((i_783_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_792_ = (((i_783_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_793_ = (((i_783_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_766_[i_765_] = (i_791_ | i_792_ | i_793_) >>> 8;
								}
							}
						} else if (i_767_ == 3) {
							byte i_794_ = aByteArray6691[i_764_];
							int i_795_ = i_794_ > 0 ? anIntArray6692[i_794_] : 0;
							int i_796_ = Class397_Sub1.anInt5767;
							int i_797_ = i_795_ + i_796_;
							int i_798_ = (i_795_ & 0xff00ff) + (i_796_ & 0xff00ff);
							int i_799_ = ((i_798_ & 0x1000100) + (i_797_ - i_798_ & 0x10000));
							i_799_ = i_797_ - i_799_ | i_799_ - (i_799_ >>> 8);
							if (i_795_ == 0 && Class397_Sub1.anInt5755 != 255) {
								i_795_ = i_799_;
								i_799_ = is_766_[i_765_];
								i_799_ = (((((i_795_ & 0xff00ff) * Class397_Sub1.anInt5755)
										+ ((i_799_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
										+ ((((i_795_ & 0xff00) * Class397_Sub1.anInt5755)
												+ ((i_799_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
							}
							is_766_[i_765_] = i_799_;
						} else if (i_767_ == 2) {
							int i_800_ = aByteArray6691[i_764_];
							if (i_800_ != 0) {
								int i_801_ = anIntArray6692[i_800_ & 0xff];
								int i_802_ = (((i_801_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_803_ = (((i_801_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_766_[i_765_++] = (((i_802_ | i_803_) >>> 8) + Class397_Sub1.anInt5773);
							}
						} else
							throw new IllegalArgumentException();
					} else if (i_768_ == 2) {
						if (i_767_ == 1) {
							int i_804_ = aByteArray6691[i_764_];
							if (i_804_ != 0) {
								int i_805_ = anIntArray6692[i_804_ & 0xff];
								int i_806_ = is_766_[i_765_];
								int i_807_ = i_805_ + i_806_;
								int i_808_ = ((i_805_ & 0xff00ff) + (i_806_ & 0xff00ff));
								i_806_ = (i_808_ & 0x1000100) + (i_807_ - i_808_ & 0x10000);
								is_766_[i_765_] = i_807_ - i_806_ | i_806_ - (i_806_ >>> 8);
							}
						} else if (i_767_ == 0) {
							int i_809_ = aByteArray6691[i_764_];
							if (i_809_ != 0) {
								int i_810_ = anIntArray6692[i_809_ & 0xff];
								int i_811_ = (((i_810_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_812_ = (((i_810_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_813_ = (((i_810_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								i_810_ = (i_811_ | i_812_ | i_813_) >>> 8;
								int i_814_ = is_766_[i_765_];
								int i_815_ = i_810_ + i_814_;
								int i_816_ = ((i_810_ & 0xff00ff) + (i_814_ & 0xff00ff));
								i_814_ = (i_816_ & 0x1000100) + (i_815_ - i_816_ & 0x10000);
								is_766_[i_765_] = i_815_ - i_814_ | i_814_ - (i_814_ >>> 8);
							}
						} else if (i_767_ == 3) {
							byte i_817_ = aByteArray6691[i_764_];
							int i_818_ = i_817_ > 0 ? anIntArray6692[i_817_] : 0;
							int i_819_ = Class397_Sub1.anInt5767;
							int i_820_ = i_818_ + i_819_;
							int i_821_ = (i_818_ & 0xff00ff) + (i_819_ & 0xff00ff);
							int i_822_ = ((i_821_ & 0x1000100) + (i_820_ - i_821_ & 0x10000));
							i_822_ = i_820_ - i_822_ | i_822_ - (i_822_ >>> 8);
							if (i_818_ == 0 && Class397_Sub1.anInt5755 != 255) {
								i_818_ = i_822_;
								i_822_ = is_766_[i_765_];
								i_822_ = (((((i_818_ & 0xff00ff) * Class397_Sub1.anInt5755)
										+ ((i_822_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
										+ ((((i_818_ & 0xff00) * Class397_Sub1.anInt5755)
												+ ((i_822_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
							}
							is_766_[i_765_] = i_822_;
						} else if (i_767_ == 2) {
							int i_823_ = aByteArray6691[i_764_];
							if (i_823_ != 0) {
								int i_824_ = anIntArray6692[i_823_ & 0xff];
								int i_825_ = (((i_824_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_826_ = (((i_824_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								i_824_ = (((i_825_ | i_826_) >>> 8) + Class397_Sub1.anInt5773);
								int i_827_ = is_766_[i_765_];
								int i_828_ = i_824_ + i_827_;
								int i_829_ = ((i_824_ & 0xff00ff) + (i_827_ & 0xff00ff));
								i_827_ = (i_829_ & 0x1000100) + (i_828_ - i_829_ & 0x10000);
								is_766_[i_765_] = i_828_ - i_827_ | i_827_ - (i_827_ >>> 8);
							}
						}
					} else
						throw new IllegalArgumentException();
					i_759_ += Class397_Sub1.anInt5766;
					i_760_ += Class397_Sub1.anInt5777;
				}
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		}
	}

	public void method4104(int i, int i_830_, int i_831_, int i_832_, int i_833_, int i_834_, int i_835_, int i_836_,
			int i_837_) {
		if (i_832_ > 0 && i_833_ > 0) {
			int i_838_ = 0;
			int i_839_ = 0;
			int i_840_ = anInt5772 + anInt5758 + anInt5757;
			int i_841_ = anInt5761 + anInt5756 + anInt5750;
			int i_842_ = (i_840_ << 16) / i_832_;
			int i_843_ = (i_841_ << 16) / i_833_;
			if (anInt5772 > 0) {
				int i_844_ = ((anInt5772 << 16) + i_842_ - 1) / i_842_;
				i += i_844_;
				i_838_ += i_844_ * i_842_ - (anInt5772 << 16);
			}
			if (anInt5761 > 0) {
				int i_845_ = ((anInt5761 << 16) + i_843_ - 1) / i_843_;
				i_830_ += i_845_;
				i_839_ += i_845_ * i_843_ - (anInt5761 << 16);
			}
			if (anInt5758 < i_840_)
				i_832_ = ((anInt5758 << 16) - i_838_ + i_842_ - 1) / i_842_;
			if (anInt5756 < i_841_)
				i_833_ = ((anInt5756 << 16) - i_839_ + i_843_ - 1) / i_843_;
			int i_846_ = i + i_830_ * aHa_Sub2_5768.anInt4084;
			int i_847_ = aHa_Sub2_5768.anInt4084 - i_832_;
			if (i_830_ + i_833_ > aHa_Sub2_5768.anInt4078)
				i_833_ -= i_830_ + i_833_ - aHa_Sub2_5768.anInt4078;
			if (i_830_ < aHa_Sub2_5768.anInt4085) {
				int i_848_ = aHa_Sub2_5768.anInt4085 - i_830_;
				i_833_ -= i_848_;
				i_846_ += i_848_ * aHa_Sub2_5768.anInt4084;
				i_839_ += i_843_ * i_848_;
			}
			if (i + i_832_ > aHa_Sub2_5768.anInt4104) {
				int i_849_ = i + i_832_ - aHa_Sub2_5768.anInt4104;
				i_832_ -= i_849_;
				i_847_ += i_849_;
			}
			if (i < aHa_Sub2_5768.anInt4079) {
				int i_850_ = aHa_Sub2_5768.anInt4079 - i;
				i_832_ -= i_850_;
				i_846_ += i_850_;
				i_838_ += i_842_ * i_850_;
				i_847_ += i_850_;
			}
			float[] fs = aHa_Sub2_5768.aFloatArray4074;
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_836_ == 0) {
				if (i_834_ == 1) {
					int i_851_ = i_838_;
					for (int i_852_ = -i_833_; i_852_ < 0; i_852_++) {
						int i_853_ = (i_839_ >> 16) * anInt5758;
						for (int i_854_ = -i_832_; i_854_ < 0; i_854_++) {
							if ((float) i_831_ < fs[i_846_]) {
								is[i_846_] = (anIntArray6692[(aByteArray6691[(i_838_ >> 16) + i_853_]) & 0xff]);
								fs[i_846_] = (float) i_831_;
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_851_;
						i_846_ += i_847_;
					}
					return;
				}
				if (i_834_ == 0) {
					int i_855_ = (i_835_ & 0xff0000) >> 16;
					int i_856_ = (i_835_ & 0xff00) >> 8;
					int i_857_ = i_835_ & 0xff;
					int i_858_ = i_838_;
					for (int i_859_ = -i_833_; i_859_ < 0; i_859_++) {
						int i_860_ = (i_839_ >> 16) * anInt5758;
						for (int i_861_ = -i_832_; i_861_ < 0; i_861_++) {
							if ((float) i_831_ < fs[i_846_]) {
								int i_862_ = (anIntArray6692[(aByteArray6691[(i_838_ >> 16) + i_860_]) & 0xff]);
								int i_863_ = (i_862_ & 0xff0000) * i_855_ & ~0xffffff;
								int i_864_ = (i_862_ & 0xff00) * i_856_ & 0xff0000;
								int i_865_ = (i_862_ & 0xff) * i_857_ & 0xff00;
								is[i_846_] = (i_863_ | i_864_ | i_865_) >>> 8;
								fs[i_846_] = (float) i_831_;
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_858_;
						i_846_ += i_847_;
					}
					return;
				}
				if (i_834_ == 3) {
					int i_866_ = i_838_;
					for (int i_867_ = -i_833_; i_867_ < 0; i_867_++) {
						int i_868_ = (i_839_ >> 16) * anInt5758;
						for (int i_869_ = -i_832_; i_869_ < 0; i_869_++) {
							if ((float) i_831_ < fs[i_846_]) {
								byte i_870_ = aByteArray6691[(i_838_ >> 16) + i_868_];
								int i_871_ = i_870_ > 0 ? anIntArray6692[i_870_] : 0;
								int i_872_ = i_871_ + i_835_;
								int i_873_ = ((i_871_ & 0xff00ff) + (i_835_ & 0xff00ff));
								int i_874_ = ((i_873_ & 0x1000100) + (i_872_ - i_873_ & 0x10000));
								is[i_846_] = i_872_ - i_874_ | i_874_ - (i_874_ >>> 8);
								fs[i_846_] = (float) i_831_;
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_866_;
						i_846_ += i_847_;
					}
					return;
				}
				if (i_834_ == 2) {
					int i_875_ = i_835_ >>> 24;
					int i_876_ = 256 - i_875_;
					int i_877_ = (i_835_ & 0xff00ff) * i_876_ & ~0xff00ff;
					int i_878_ = (i_835_ & 0xff00) * i_876_ & 0xff0000;
					i_835_ = (i_877_ | i_878_) >>> 8;
					int i_879_ = i_838_;
					for (int i_880_ = -i_833_; i_880_ < 0; i_880_++) {
						int i_881_ = (i_839_ >> 16) * anInt5758;
						for (int i_882_ = -i_832_; i_882_ < 0; i_882_++) {
							if ((float) i_831_ < fs[i_846_]) {
								int i_883_ = (anIntArray6692[(aByteArray6691[(i_838_ >> 16) + i_881_]) & 0xff]);
								i_877_ = (i_883_ & 0xff00ff) * i_875_ & ~0xff00ff;
								i_878_ = (i_883_ & 0xff00) * i_875_ & 0xff0000;
								is[i_846_] = ((i_877_ | i_878_) >>> 8) + i_835_;
								fs[i_846_] = (float) i_831_;
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_879_;
						i_846_ += i_847_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_836_ == 1) {
				if (i_834_ == 1) {
					int i_884_ = i_838_;
					for (int i_885_ = -i_833_; i_885_ < 0; i_885_++) {
						int i_886_ = (i_839_ >> 16) * anInt5758;
						for (int i_887_ = -i_832_; i_887_ < 0; i_887_++) {
							if ((float) i_831_ < fs[i_846_]) {
								int i_888_ = aByteArray6691[(i_838_ >> 16) + i_886_];
								if (i_888_ != 0) {
									is[i_846_] = anIntArray6692[i_888_ & 0xff];
									fs[i_846_] = (float) i_831_;
								}
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_884_;
						i_846_ += i_847_;
					}
					return;
				}
				if (i_834_ == 0) {
					int i_889_ = i_838_;
					if ((i_835_ & 0xffffff) == 16777215) {
						int i_890_ = i_835_ >>> 24;
						int i_891_ = 256 - i_890_;
						for (int i_892_ = -i_833_; i_892_ < 0; i_892_++) {
							int i_893_ = (i_839_ >> 16) * anInt5758;
							for (int i_894_ = -i_832_; i_894_ < 0; i_894_++) {
								if ((float) i_831_ < fs[i_846_]) {
									int i_895_ = (aByteArray6691[(i_838_ >> 16) + i_893_]);
									if (i_895_ != 0) {
										int i_896_ = anIntArray6692[i_895_ & 0xff];
										int i_897_ = is[i_846_];
										is[i_846_] = ((((i_896_ & 0xff00ff) * i_890_ + ((i_897_ & 0xff00ff) * i_891_))
												& ~0xff00ff)
												+ (((i_896_ & 0xff00) * i_890_ + ((i_897_ & 0xff00) * i_891_))
														& 0xff0000)) >> 8;
										fs[i_846_] = (float) i_831_;
									}
								}
								i_838_ += i_842_;
								i_846_++;
							}
							i_839_ += i_843_;
							i_838_ = i_889_;
							i_846_ += i_847_;
						}
					} else {
						int i_898_ = (i_835_ & 0xff0000) >> 16;
						int i_899_ = (i_835_ & 0xff00) >> 8;
						int i_900_ = i_835_ & 0xff;
						int i_901_ = i_835_ >>> 24;
						int i_902_ = 256 - i_901_;
						for (int i_903_ = -i_833_; i_903_ < 0; i_903_++) {
							int i_904_ = (i_839_ >> 16) * anInt5758;
							for (int i_905_ = -i_832_; i_905_ < 0; i_905_++) {
								if ((float) i_831_ < fs[i_846_]) {
									int i_906_ = (aByteArray6691[(i_838_ >> 16) + i_904_]);
									if (i_906_ != 0) {
										int i_907_ = anIntArray6692[i_906_ & 0xff];
										if (i_901_ != 255) {
											int i_908_ = ((i_907_ & 0xff0000) * i_898_ & ~0xffffff);
											int i_909_ = ((i_907_ & 0xff00) * i_899_ & 0xff0000);
											int i_910_ = ((i_907_ & 0xff) * i_900_ & 0xff00);
											i_907_ = (i_908_ | i_909_ | i_910_) >>> 8;
											int i_911_ = is[i_846_];
											is[i_846_] = (((((i_907_ & 0xff00ff) * i_901_)
													+ ((i_911_ & 0xff00ff) * i_902_)) & ~0xff00ff)
													+ ((((i_907_ & 0xff00) * i_901_) + ((i_911_ & 0xff00) * i_902_))
															& 0xff0000)) >> 8;
											fs[i_846_] = (float) i_831_;
										} else {
											int i_912_ = ((i_907_ & 0xff0000) * i_898_ & ~0xffffff);
											int i_913_ = ((i_907_ & 0xff00) * i_899_ & 0xff0000);
											int i_914_ = ((i_907_ & 0xff) * i_900_ & 0xff00);
											is[i_846_] = (i_912_ | i_913_ | i_914_) >>> 8;
											fs[i_846_] = (float) i_831_;
										}
									}
								}
								i_838_ += i_842_;
								i_846_++;
							}
							i_839_ += i_843_;
							i_838_ = i_889_;
							i_846_ += i_847_;
						}
						return;
					}
					return;
				}
				if (i_834_ == 3) {
					int i_915_ = i_838_;
					int i_916_ = i_835_ >>> 24;
					int i_917_ = 256 - i_916_;
					for (int i_918_ = -i_833_; i_918_ < 0; i_918_++) {
						int i_919_ = (i_839_ >> 16) * anInt5758;
						for (int i_920_ = -i_832_; i_920_ < 0; i_920_++) {
							if ((float) i_831_ < fs[i_846_]) {
								byte i_921_ = aByteArray6691[(i_838_ >> 16) + i_919_];
								int i_922_ = i_921_ > 0 ? anIntArray6692[i_921_] : 0;
								int i_923_ = i_922_ + i_835_;
								int i_924_ = ((i_922_ & 0xff00ff) + (i_835_ & 0xff00ff));
								int i_925_ = ((i_924_ & 0x1000100) + (i_923_ - i_924_ & 0x10000));
								i_925_ = i_923_ - i_925_ | i_925_ - (i_925_ >>> 8);
								if (i_922_ == 0 && i_916_ != 255) {
									i_922_ = i_925_;
									i_925_ = is[i_846_];
									i_925_ = ((((i_922_ & 0xff00ff) * i_916_ + (i_925_ & 0xff00ff) * i_917_)
											& ~0xff00ff)
											+ (((i_922_ & 0xff00) * i_916_ + (i_925_ & 0xff00) * i_917_)
													& 0xff0000)) >> 8;
								}
								is[i_846_] = i_925_;
								fs[i_846_] = (float) i_831_;
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_915_;
						i_846_ += i_847_;
					}
					return;
				}
				if (i_834_ == 2) {
					int i_926_ = i_835_ >>> 24;
					int i_927_ = 256 - i_926_;
					int i_928_ = (i_835_ & 0xff00ff) * i_927_ & ~0xff00ff;
					int i_929_ = (i_835_ & 0xff00) * i_927_ & 0xff0000;
					i_835_ = (i_928_ | i_929_) >>> 8;
					int i_930_ = i_838_;
					for (int i_931_ = -i_833_; i_931_ < 0; i_931_++) {
						int i_932_ = (i_839_ >> 16) * anInt5758;
						for (int i_933_ = -i_832_; i_933_ < 0; i_933_++) {
							if ((float) i_831_ < fs[i_846_]) {
								int i_934_ = aByteArray6691[(i_838_ >> 16) + i_932_];
								if (i_934_ != 0) {
									int i_935_ = anIntArray6692[i_934_ & 0xff];
									i_928_ = ((i_935_ & 0xff00ff) * i_926_ & ~0xff00ff);
									i_929_ = ((i_935_ & 0xff00) * i_926_ & 0xff0000);
									is[i_846_] = ((i_928_ | i_929_) >>> 8) + i_835_;
									fs[i_846_] = (float) i_831_;
								}
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_930_;
						i_846_ += i_847_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_836_ == 2) {
				if (i_834_ == 1) {
					int i_936_ = i_838_;
					for (int i_937_ = -i_833_; i_937_ < 0; i_937_++) {
						int i_938_ = (i_839_ >> 16) * anInt5758;
						for (int i_939_ = -i_832_; i_939_ < 0; i_939_++) {
							if ((float) i_831_ < fs[i_846_]) {
								int i_940_ = aByteArray6691[(i_838_ >> 16) + i_938_];
								if (i_940_ != 0) {
									int i_941_ = anIntArray6692[i_940_ & 0xff];
									int i_942_ = is[i_846_];
									int i_943_ = i_941_ + i_942_;
									int i_944_ = ((i_941_ & 0xff00ff) + (i_942_ & 0xff00ff));
									i_942_ = ((i_944_ & 0x1000100) + (i_943_ - i_944_ & 0x10000));
									is[i_846_] = i_943_ - i_942_ | i_942_ - (i_942_ >>> 8);
									fs[i_846_] = (float) i_831_;
								}
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_936_;
						i_846_ += i_847_;
					}
					return;
				}
				if (i_834_ == 0) {
					int i_945_ = i_838_;
					int i_946_ = (i_835_ & 0xff0000) >> 16;
					int i_947_ = (i_835_ & 0xff00) >> 8;
					int i_948_ = i_835_ & 0xff;
					for (int i_949_ = -i_833_; i_949_ < 0; i_949_++) {
						int i_950_ = (i_839_ >> 16) * anInt5758;
						for (int i_951_ = -i_832_; i_951_ < 0; i_951_++) {
							if ((float) i_831_ < fs[i_846_]) {
								int i_952_ = aByteArray6691[(i_838_ >> 16) + i_950_];
								if (i_952_ != 0) {
									int i_953_ = anIntArray6692[i_952_ & 0xff];
									int i_954_ = ((i_953_ & 0xff0000) * i_946_ & ~0xffffff);
									int i_955_ = ((i_953_ & 0xff00) * i_947_ & 0xff0000);
									int i_956_ = (i_953_ & 0xff) * i_948_ & 0xff00;
									i_953_ = (i_954_ | i_955_ | i_956_) >>> 8;
									int i_957_ = is[i_846_];
									int i_958_ = i_953_ + i_957_;
									int i_959_ = ((i_953_ & 0xff00ff) + (i_957_ & 0xff00ff));
									i_957_ = ((i_959_ & 0x1000100) + (i_958_ - i_959_ & 0x10000));
									is[i_846_] = i_958_ - i_957_ | i_957_ - (i_957_ >>> 8);
									fs[i_846_] = (float) i_831_;
								}
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_945_;
						i_846_ += i_847_;
					}
					return;
				}
				if (i_834_ == 3) {
					int i_960_ = i_838_;
					for (int i_961_ = -i_833_; i_961_ < 0; i_961_++) {
						int i_962_ = (i_839_ >> 16) * anInt5758;
						for (int i_963_ = -i_832_; i_963_ < 0; i_963_++) {
							if ((float) i_831_ < fs[i_846_]) {
								byte i_964_ = aByteArray6691[(i_838_ >> 16) + i_962_];
								int i_965_ = i_964_ > 0 ? anIntArray6692[i_964_] : 0;
								int i_966_ = i_965_ + i_835_;
								int i_967_ = ((i_965_ & 0xff00ff) + (i_835_ & 0xff00ff));
								int i_968_ = ((i_967_ & 0x1000100) + (i_966_ - i_967_ & 0x10000));
								i_965_ = i_966_ - i_968_ | i_968_ - (i_968_ >>> 8);
								i_968_ = is[i_846_];
								i_966_ = i_965_ + i_968_;
								i_967_ = (i_965_ & 0xff00ff) + (i_968_ & 0xff00ff);
								i_968_ = (i_967_ & 0x1000100) + (i_966_ - i_967_ & 0x10000);
								is[i_846_] = i_966_ - i_968_ | i_968_ - (i_968_ >>> 8);
								fs[i_846_] = (float) i_831_;
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_960_;
						i_846_ += i_847_;
					}
					return;
				}
				if (i_834_ == 2) {
					int i_969_ = i_835_ >>> 24;
					int i_970_ = 256 - i_969_;
					int i_971_ = (i_835_ & 0xff00ff) * i_970_ & ~0xff00ff;
					int i_972_ = (i_835_ & 0xff00) * i_970_ & 0xff0000;
					i_835_ = (i_971_ | i_972_) >>> 8;
					int i_973_ = i_838_;
					for (int i_974_ = -i_833_; i_974_ < 0; i_974_++) {
						int i_975_ = (i_839_ >> 16) * anInt5758;
						for (int i_976_ = -i_832_; i_976_ < 0; i_976_++) {
							if ((float) i_831_ < fs[i_846_]) {
								int i_977_ = aByteArray6691[(i_838_ >> 16) + i_975_];
								if (i_977_ != 0) {
									int i_978_ = anIntArray6692[i_977_ & 0xff];
									i_971_ = ((i_978_ & 0xff00ff) * i_969_ & ~0xff00ff);
									i_972_ = ((i_978_ & 0xff00) * i_969_ & 0xff0000);
									i_978_ = ((i_971_ | i_972_) >>> 8) + i_835_;
									int i_979_ = is[i_846_];
									int i_980_ = i_978_ + i_979_;
									int i_981_ = ((i_978_ & 0xff00ff) + (i_979_ & 0xff00ff));
									i_979_ = ((i_981_ & 0x1000100) + (i_980_ - i_981_ & 0x10000));
									is[i_846_] = i_980_ - i_979_ | i_979_ - (i_979_ >>> 8);
									fs[i_846_] = (float) i_831_;
								}
							}
							i_838_ += i_842_;
							i_846_++;
						}
						i_839_ += i_843_;
						i_838_ = i_973_;
						i_846_ += i_847_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	public void method4090(int i, int i_982_, int i_983_, int i_984_, int i_985_, int i_986_) {
		throw new IllegalStateException();
	}

	public void method4103(int i, int i_987_, int i_988_, int i_989_, int i_990_, int i_991_, int i_992_, int i_993_,
			int i_994_) {
		if (i_989_ > 0 && i_990_ > 0) {
			int i_995_ = 0;
			int i_996_ = 0;
			int i_997_ = anInt5772 + anInt5758 + anInt5757;
			int i_998_ = anInt5761 + anInt5756 + anInt5750;
			int i_999_ = (i_997_ << 16) / i_989_;
			int i_1000_ = (i_998_ << 16) / i_990_;
			if (anInt5772 > 0) {
				int i_1001_ = ((anInt5772 << 16) + i_999_ - 1) / i_999_;
				i += i_1001_;
				i_995_ += i_1001_ * i_999_ - (anInt5772 << 16);
			}
			if (anInt5761 > 0) {
				int i_1002_ = ((anInt5761 << 16) + i_1000_ - 1) / i_1000_;
				i_987_ += i_1002_;
				i_996_ += i_1002_ * i_1000_ - (anInt5761 << 16);
			}
			if (anInt5758 < i_997_)
				i_989_ = ((anInt5758 << 16) - i_995_ + i_999_ - 1) / i_999_;
			if (anInt5756 < i_998_)
				i_990_ = ((anInt5756 << 16) - i_996_ + i_1000_ - 1) / i_1000_;
			int i_1003_ = i + i_987_ * aHa_Sub2_5768.anInt4084;
			int i_1004_ = aHa_Sub2_5768.anInt4084 - i_989_;
			if (i_987_ + i_990_ > aHa_Sub2_5768.anInt4078)
				i_990_ -= i_987_ + i_990_ - aHa_Sub2_5768.anInt4078;
			if (i_987_ < aHa_Sub2_5768.anInt4085) {
				int i_1005_ = aHa_Sub2_5768.anInt4085 - i_987_;
				i_990_ -= i_1005_;
				i_1003_ += i_1005_ * aHa_Sub2_5768.anInt4084;
				i_996_ += i_1000_ * i_1005_;
			}
			if (i + i_989_ > aHa_Sub2_5768.anInt4104) {
				int i_1006_ = i + i_989_ - aHa_Sub2_5768.anInt4104;
				i_989_ -= i_1006_;
				i_1004_ += i_1006_;
			}
			if (i < aHa_Sub2_5768.anInt4079) {
				int i_1007_ = aHa_Sub2_5768.anInt4079 - i;
				i_989_ -= i_1007_;
				i_1003_ += i_1007_;
				i_995_ += i_999_ * i_1007_;
				i_1004_ += i_1007_;
			}
			float[] fs = aHa_Sub2_5768.aFloatArray4074;
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_993_ == 0) {
				if (i_991_ == 1) {
					int i_1008_ = i_995_;
					for (int i_1009_ = -i_990_; i_1009_ < 0; i_1009_++) {
						int i_1010_ = (i_996_ >> 16) * anInt5758;
						for (int i_1011_ = -i_989_; i_1011_ < 0; i_1011_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								is[i_1003_] = (anIntArray6692[(aByteArray6691[(i_995_ >> 16) + i_1010_]) & 0xff]);
								fs[i_1003_] = (float) i_988_;
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1008_;
						i_1003_ += i_1004_;
					}
					return;
				}
				if (i_991_ == 0) {
					int i_1012_ = (i_992_ & 0xff0000) >> 16;
					int i_1013_ = (i_992_ & 0xff00) >> 8;
					int i_1014_ = i_992_ & 0xff;
					int i_1015_ = i_995_;
					for (int i_1016_ = -i_990_; i_1016_ < 0; i_1016_++) {
						int i_1017_ = (i_996_ >> 16) * anInt5758;
						for (int i_1018_ = -i_989_; i_1018_ < 0; i_1018_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								int i_1019_ = (anIntArray6692[(aByteArray6691[(i_995_ >> 16) + i_1017_]) & 0xff]);
								int i_1020_ = ((i_1019_ & 0xff0000) * i_1012_ & ~0xffffff);
								int i_1021_ = (i_1019_ & 0xff00) * i_1013_ & 0xff0000;
								int i_1022_ = (i_1019_ & 0xff) * i_1014_ & 0xff00;
								is[i_1003_] = (i_1020_ | i_1021_ | i_1022_) >>> 8;
								fs[i_1003_] = (float) i_988_;
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1015_;
						i_1003_ += i_1004_;
					}
					return;
				}
				if (i_991_ == 3) {
					int i_1023_ = i_995_;
					for (int i_1024_ = -i_990_; i_1024_ < 0; i_1024_++) {
						int i_1025_ = (i_996_ >> 16) * anInt5758;
						for (int i_1026_ = -i_989_; i_1026_ < 0; i_1026_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								byte i_1027_ = aByteArray6691[(i_995_ >> 16) + i_1025_];
								int i_1028_ = (i_1027_ > 0 ? anIntArray6692[i_1027_] : 0);
								int i_1029_ = i_1028_ + i_992_;
								int i_1030_ = ((i_1028_ & 0xff00ff) + (i_992_ & 0xff00ff));
								int i_1031_ = ((i_1030_ & 0x1000100) + (i_1029_ - i_1030_ & 0x10000));
								is[i_1003_] = i_1029_ - i_1031_ | i_1031_ - (i_1031_ >>> 8);
								fs[i_1003_] = (float) i_988_;
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1023_;
						i_1003_ += i_1004_;
					}
					return;
				}
				if (i_991_ == 2) {
					int i_1032_ = i_992_ >>> 24;
					int i_1033_ = 256 - i_1032_;
					int i_1034_ = (i_992_ & 0xff00ff) * i_1033_ & ~0xff00ff;
					int i_1035_ = (i_992_ & 0xff00) * i_1033_ & 0xff0000;
					i_992_ = (i_1034_ | i_1035_) >>> 8;
					int i_1036_ = i_995_;
					for (int i_1037_ = -i_990_; i_1037_ < 0; i_1037_++) {
						int i_1038_ = (i_996_ >> 16) * anInt5758;
						for (int i_1039_ = -i_989_; i_1039_ < 0; i_1039_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								int i_1040_ = (anIntArray6692[(aByteArray6691[(i_995_ >> 16) + i_1038_]) & 0xff]);
								i_1034_ = ((i_1040_ & 0xff00ff) * i_1032_ & ~0xff00ff);
								i_1035_ = (i_1040_ & 0xff00) * i_1032_ & 0xff0000;
								is[i_1003_] = ((i_1034_ | i_1035_) >>> 8) + i_992_;
								fs[i_1003_] = (float) i_988_;
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1036_;
						i_1003_ += i_1004_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_993_ == 1) {
				if (i_991_ == 1) {
					int i_1041_ = i_995_;
					for (int i_1042_ = -i_990_; i_1042_ < 0; i_1042_++) {
						int i_1043_ = (i_996_ >> 16) * anInt5758;
						for (int i_1044_ = -i_989_; i_1044_ < 0; i_1044_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								int i_1045_ = aByteArray6691[(i_995_ >> 16) + i_1043_];
								if (i_1045_ != 0) {
									is[i_1003_] = anIntArray6692[i_1045_ & 0xff];
									fs[i_1003_] = (float) i_988_;
								}
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1041_;
						i_1003_ += i_1004_;
					}
					return;
				}
				if (i_991_ == 0) {
					int i_1046_ = i_995_;
					if ((i_992_ & 0xffffff) == 16777215) {
						int i_1047_ = i_992_ >>> 24;
						int i_1048_ = 256 - i_1047_;
						for (int i_1049_ = -i_990_; i_1049_ < 0; i_1049_++) {
							int i_1050_ = (i_996_ >> 16) * anInt5758;
							for (int i_1051_ = -i_989_; i_1051_ < 0; i_1051_++) {
								if ((float) i_988_ < fs[i_1003_]) {
									int i_1052_ = (aByteArray6691[(i_995_ >> 16) + i_1050_]);
									if (i_1052_ != 0) {
										int i_1053_ = anIntArray6692[i_1052_ & 0xff];
										int i_1054_ = is[i_1003_];
										is[i_1003_] = ((((i_1053_ & 0xff00ff) * i_1047_
												+ ((i_1054_ & 0xff00ff) * i_1048_)) & ~0xff00ff)
												+ (((i_1053_ & 0xff00) * i_1047_ + ((i_1054_ & 0xff00) * i_1048_))
														& 0xff0000)) >> 8;
										fs[i_1003_] = (float) i_988_;
									}
								}
								i_995_ += i_999_;
								i_1003_++;
							}
							i_996_ += i_1000_;
							i_995_ = i_1046_;
							i_1003_ += i_1004_;
						}
					} else {
						int i_1055_ = (i_992_ & 0xff0000) >> 16;
						int i_1056_ = (i_992_ & 0xff00) >> 8;
						int i_1057_ = i_992_ & 0xff;
						int i_1058_ = i_992_ >>> 24;
						int i_1059_ = 256 - i_1058_;
						for (int i_1060_ = -i_990_; i_1060_ < 0; i_1060_++) {
							int i_1061_ = (i_996_ >> 16) * anInt5758;
							for (int i_1062_ = -i_989_; i_1062_ < 0; i_1062_++) {
								if ((float) i_988_ < fs[i_1003_]) {
									int i_1063_ = (aByteArray6691[(i_995_ >> 16) + i_1061_]);
									if (i_1063_ != 0) {
										int i_1064_ = anIntArray6692[i_1063_ & 0xff];
										if (i_1058_ != 255) {
											int i_1065_ = (((i_1064_ & 0xff0000) * i_1055_) & ~0xffffff);
											int i_1066_ = ((i_1064_ & 0xff00) * i_1056_ & 0xff0000);
											int i_1067_ = ((i_1064_ & 0xff) * i_1057_ & 0xff00);
											i_1064_ = (i_1065_ | i_1066_ | i_1067_) >>> 8;
											int i_1068_ = is[i_1003_];
											is[i_1003_] = (((((i_1064_ & 0xff00ff) * i_1058_)
													+ ((i_1068_ & 0xff00ff) * i_1059_)) & ~0xff00ff)
													+ ((((i_1064_ & 0xff00) * i_1058_) + ((i_1068_ & 0xff00) * i_1059_))
															& 0xff0000)) >> 8;
											fs[i_1003_] = (float) i_988_;
										} else {
											int i_1069_ = (((i_1064_ & 0xff0000) * i_1055_) & ~0xffffff);
											int i_1070_ = ((i_1064_ & 0xff00) * i_1056_ & 0xff0000);
											int i_1071_ = ((i_1064_ & 0xff) * i_1057_ & 0xff00);
											is[i_1003_] = (i_1069_ | i_1070_ | i_1071_) >>> 8;
											fs[i_1003_] = (float) i_988_;
										}
									}
								}
								i_995_ += i_999_;
								i_1003_++;
							}
							i_996_ += i_1000_;
							i_995_ = i_1046_;
							i_1003_ += i_1004_;
						}
						return;
					}
					return;
				}
				if (i_991_ == 3) {
					int i_1072_ = i_995_;
					int i_1073_ = i_992_ >>> 24;
					int i_1074_ = 256 - i_1073_;
					for (int i_1075_ = -i_990_; i_1075_ < 0; i_1075_++) {
						int i_1076_ = (i_996_ >> 16) * anInt5758;
						for (int i_1077_ = -i_989_; i_1077_ < 0; i_1077_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								byte i_1078_ = aByteArray6691[(i_995_ >> 16) + i_1076_];
								int i_1079_ = (i_1078_ > 0 ? anIntArray6692[i_1078_] : 0);
								int i_1080_ = i_1079_ + i_992_;
								int i_1081_ = ((i_1079_ & 0xff00ff) + (i_992_ & 0xff00ff));
								int i_1082_ = ((i_1081_ & 0x1000100) + (i_1080_ - i_1081_ & 0x10000));
								i_1082_ = i_1080_ - i_1082_ | i_1082_ - (i_1082_ >>> 8);
								if (i_1079_ == 0 && i_1073_ != 255) {
									i_1079_ = i_1082_;
									i_1082_ = is[i_1003_];
									i_1082_ = ((((i_1079_ & 0xff00ff) * i_1073_ + (i_1082_ & 0xff00ff) * i_1074_)
											& ~0xff00ff)
											+ (((i_1079_ & 0xff00) * i_1073_ + (i_1082_ & 0xff00) * i_1074_)
													& 0xff0000)) >> 8;
								}
								is[i_1003_] = i_1082_;
								fs[i_1003_] = (float) i_988_;
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1072_;
						i_1003_ += i_1004_;
					}
					return;
				}
				if (i_991_ == 2) {
					int i_1083_ = i_992_ >>> 24;
					int i_1084_ = 256 - i_1083_;
					int i_1085_ = (i_992_ & 0xff00ff) * i_1084_ & ~0xff00ff;
					int i_1086_ = (i_992_ & 0xff00) * i_1084_ & 0xff0000;
					i_992_ = (i_1085_ | i_1086_) >>> 8;
					int i_1087_ = i_995_;
					for (int i_1088_ = -i_990_; i_1088_ < 0; i_1088_++) {
						int i_1089_ = (i_996_ >> 16) * anInt5758;
						for (int i_1090_ = -i_989_; i_1090_ < 0; i_1090_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								int i_1091_ = aByteArray6691[(i_995_ >> 16) + i_1089_];
								if (i_1091_ != 0) {
									int i_1092_ = anIntArray6692[i_1091_ & 0xff];
									i_1085_ = ((i_1092_ & 0xff00ff) * i_1083_ & ~0xff00ff);
									i_1086_ = ((i_1092_ & 0xff00) * i_1083_ & 0xff0000);
									is[i_1003_] = ((i_1085_ | i_1086_) >>> 8) + i_992_;
									fs[i_1003_] = (float) i_988_;
								}
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1087_;
						i_1003_ += i_1004_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_993_ == 2) {
				if (i_991_ == 1) {
					int i_1093_ = i_995_;
					for (int i_1094_ = -i_990_; i_1094_ < 0; i_1094_++) {
						int i_1095_ = (i_996_ >> 16) * anInt5758;
						for (int i_1096_ = -i_989_; i_1096_ < 0; i_1096_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								int i_1097_ = aByteArray6691[(i_995_ >> 16) + i_1095_];
								if (i_1097_ != 0) {
									int i_1098_ = anIntArray6692[i_1097_ & 0xff];
									int i_1099_ = is[i_1003_];
									int i_1100_ = i_1098_ + i_1099_;
									int i_1101_ = ((i_1098_ & 0xff00ff) + (i_1099_ & 0xff00ff));
									i_1099_ = ((i_1101_ & 0x1000100) + (i_1100_ - i_1101_ & 0x10000));
									is[i_1003_] = (i_1100_ - i_1099_ | i_1099_ - (i_1099_ >>> 8));
									fs[i_1003_] = (float) i_988_;
								}
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1093_;
						i_1003_ += i_1004_;
					}
					return;
				}
				if (i_991_ == 0) {
					int i_1102_ = i_995_;
					int i_1103_ = (i_992_ & 0xff0000) >> 16;
					int i_1104_ = (i_992_ & 0xff00) >> 8;
					int i_1105_ = i_992_ & 0xff;
					for (int i_1106_ = -i_990_; i_1106_ < 0; i_1106_++) {
						int i_1107_ = (i_996_ >> 16) * anInt5758;
						for (int i_1108_ = -i_989_; i_1108_ < 0; i_1108_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								int i_1109_ = aByteArray6691[(i_995_ >> 16) + i_1107_];
								if (i_1109_ != 0) {
									int i_1110_ = anIntArray6692[i_1109_ & 0xff];
									int i_1111_ = ((i_1110_ & 0xff0000) * i_1103_ & ~0xffffff);
									int i_1112_ = ((i_1110_ & 0xff00) * i_1104_ & 0xff0000);
									int i_1113_ = (i_1110_ & 0xff) * i_1105_ & 0xff00;
									i_1110_ = (i_1111_ | i_1112_ | i_1113_) >>> 8;
									int i_1114_ = is[i_1003_];
									int i_1115_ = i_1110_ + i_1114_;
									int i_1116_ = ((i_1110_ & 0xff00ff) + (i_1114_ & 0xff00ff));
									i_1114_ = ((i_1116_ & 0x1000100) + (i_1115_ - i_1116_ & 0x10000));
									is[i_1003_] = (i_1115_ - i_1114_ | i_1114_ - (i_1114_ >>> 8));
									fs[i_1003_] = (float) i_988_;
								}
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1102_;
						i_1003_ += i_1004_;
					}
					return;
				}
				if (i_991_ == 3) {
					int i_1117_ = i_995_;
					for (int i_1118_ = -i_990_; i_1118_ < 0; i_1118_++) {
						int i_1119_ = (i_996_ >> 16) * anInt5758;
						for (int i_1120_ = -i_989_; i_1120_ < 0; i_1120_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								byte i_1121_ = aByteArray6691[(i_995_ >> 16) + i_1119_];
								int i_1122_ = (i_1121_ > 0 ? anIntArray6692[i_1121_] : 0);
								int i_1123_ = i_1122_ + i_992_;
								int i_1124_ = ((i_1122_ & 0xff00ff) + (i_992_ & 0xff00ff));
								int i_1125_ = ((i_1124_ & 0x1000100) + (i_1123_ - i_1124_ & 0x10000));
								i_1122_ = i_1123_ - i_1125_ | i_1125_ - (i_1125_ >>> 8);
								i_1125_ = is[i_1003_];
								i_1123_ = i_1122_ + i_1125_;
								i_1124_ = (i_1122_ & 0xff00ff) + (i_1125_ & 0xff00ff);
								i_1125_ = ((i_1124_ & 0x1000100) + (i_1123_ - i_1124_ & 0x10000));
								is[i_1003_] = i_1123_ - i_1125_ | i_1125_ - (i_1125_ >>> 8);
								fs[i_1003_] = (float) i_988_;
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1117_;
						i_1003_ += i_1004_;
					}
					return;
				}
				if (i_991_ == 2) {
					int i_1126_ = i_992_ >>> 24;
					int i_1127_ = 256 - i_1126_;
					int i_1128_ = (i_992_ & 0xff00ff) * i_1127_ & ~0xff00ff;
					int i_1129_ = (i_992_ & 0xff00) * i_1127_ & 0xff0000;
					i_992_ = (i_1128_ | i_1129_) >>> 8;
					int i_1130_ = i_995_;
					for (int i_1131_ = -i_990_; i_1131_ < 0; i_1131_++) {
						int i_1132_ = (i_996_ >> 16) * anInt5758;
						for (int i_1133_ = -i_989_; i_1133_ < 0; i_1133_++) {
							if ((float) i_988_ < fs[i_1003_]) {
								int i_1134_ = aByteArray6691[(i_995_ >> 16) + i_1132_];
								if (i_1134_ != 0) {
									int i_1135_ = anIntArray6692[i_1134_ & 0xff];
									i_1128_ = ((i_1135_ & 0xff00ff) * i_1126_ & ~0xff00ff);
									i_1129_ = ((i_1135_ & 0xff00) * i_1126_ & 0xff0000);
									i_1135_ = ((i_1128_ | i_1129_) >>> 8) + i_992_;
									int i_1136_ = is[i_1003_];
									int i_1137_ = i_1135_ + i_1136_;
									int i_1138_ = ((i_1135_ & 0xff00ff) + (i_1136_ & 0xff00ff));
									i_1136_ = ((i_1138_ & 0x1000100) + (i_1137_ - i_1138_ & 0x10000));
									is[i_1003_] = (i_1137_ - i_1136_ | i_1136_ - (i_1136_ >>> 8));
									fs[i_1003_] = (float) i_988_;
								}
							}
							i_995_ += i_999_;
							i_1003_++;
						}
						i_996_ += i_1000_;
						i_995_ = i_1130_;
						i_1003_ += i_1004_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	Class397_Sub1_Sub2(ha_Sub2 var_ha_Sub2, byte[] is, int[] is_1139_, int i, int i_1140_) {
		super(var_ha_Sub2, i, i_1140_);
		aByteArray6691 = is;
		anIntArray6692 = is_1139_;
	}

	public void method4084(int i, int i_1141_, int i_1142_) {
		throw new IllegalStateException();
	}

	public void method4102(int[] is, int[] is_1143_, int i, int i_1144_) {
		int[] is_1145_ = aHa_Sub2_5768.anIntArray4108;
		if (Class397_Sub1.anInt5766 == 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_1146_ = Class397_Sub1.anInt5771;
				while (i_1146_ < 0) {
					int i_1147_ = i_1146_ + i_1144_;
					if (i_1147_ >= 0) {
						if (i_1147_ >= is.length)
							break;
						int i_1148_ = Class397_Sub1.anInt5759;
						int i_1149_ = Class397_Sub1.anInt5765;
						int i_1150_ = Class397_Sub1.anInt5775;
						int i_1151_ = Class397_Sub1.anInt5770;
						if (i_1149_ >= 0 && i_1150_ >= 0 && i_1149_ - (anInt5758 << 12) < 0
								&& i_1150_ - (anInt5756 << 12) < 0) {
							int i_1152_ = is[i_1147_] - i;
							int i_1153_ = -is_1143_[i_1147_];
							int i_1154_ = (i_1152_ - (i_1148_ - Class397_Sub1.anInt5759));
							if (i_1154_ > 0) {
								i_1148_ += i_1154_;
								i_1151_ += i_1154_;
								i_1149_ += Class397_Sub1.anInt5766 * i_1154_;
								i_1150_ += Class397_Sub1.anInt5777 * i_1154_;
							} else
								i_1153_ -= i_1154_;
							if (i_1151_ < i_1153_)
								i_1151_ = i_1153_;
							for (/**/; i_1151_ < 0; i_1151_++) {
								int i_1155_ = (aByteArray6691[((i_1150_ >> 12) * anInt5758 + (i_1149_ >> 12))]);
								if (i_1155_ != 0)
									is_1145_[i_1148_++] = anIntArray6692[i_1155_ & 0xff];
								else
									i_1148_++;
							}
						}
					}
					i_1146_++;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_1156_ = Class397_Sub1.anInt5771;
				while (i_1156_ < 0) {
					int i_1157_ = i_1156_ + i_1144_;
					if (i_1157_ >= 0) {
						if (i_1157_ >= is.length)
							break;
						int i_1158_ = Class397_Sub1.anInt5759;
						int i_1159_ = Class397_Sub1.anInt5765;
						int i_1160_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1161_ = Class397_Sub1.anInt5770;
						if (i_1159_ >= 0 && i_1159_ - (anInt5758 << 12) < 0) {
							int i_1162_;
							if ((i_1162_ = i_1160_ - (anInt5756 << 12)) >= 0) {
								i_1162_ = ((Class397_Sub1.anInt5777 - i_1162_) / Class397_Sub1.anInt5777);
								i_1161_ += i_1162_;
								i_1160_ += Class397_Sub1.anInt5777 * i_1162_;
								i_1158_ += i_1162_;
							}
							if ((i_1162_ = ((i_1160_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_1161_)
								i_1161_ = i_1162_;
							int i_1163_ = is[i_1157_] - i;
							int i_1164_ = -is_1143_[i_1157_];
							int i_1165_ = (i_1163_ - (i_1158_ - Class397_Sub1.anInt5759));
							if (i_1165_ > 0) {
								i_1158_ += i_1165_;
								i_1161_ += i_1165_;
								i_1159_ += Class397_Sub1.anInt5766 * i_1165_;
								i_1160_ += Class397_Sub1.anInt5777 * i_1165_;
							} else
								i_1164_ -= i_1165_;
							if (i_1161_ < i_1164_)
								i_1161_ = i_1164_;
							for (/**/; i_1161_ < 0; i_1161_++) {
								int i_1166_ = (aByteArray6691[((i_1160_ >> 12) * anInt5758 + (i_1159_ >> 12))]);
								if (i_1166_ != 0)
									is_1145_[i_1158_++] = anIntArray6692[i_1166_ & 0xff];
								else
									i_1158_++;
								i_1160_ += Class397_Sub1.anInt5777;
							}
						}
					}
					i_1156_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_1167_ = Class397_Sub1.anInt5771;
				while (i_1167_ < 0) {
					int i_1168_ = i_1167_ + i_1144_;
					if (i_1168_ >= 0) {
						if (i_1168_ >= is.length)
							break;
						int i_1169_ = Class397_Sub1.anInt5759;
						int i_1170_ = Class397_Sub1.anInt5765;
						int i_1171_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1172_ = Class397_Sub1.anInt5770;
						if (i_1170_ >= 0 && i_1170_ - (anInt5758 << 12) < 0) {
							if (i_1171_ < 0) {
								int i_1173_ = ((Class397_Sub1.anInt5777 - 1 - i_1171_) / Class397_Sub1.anInt5777);
								i_1172_ += i_1173_;
								i_1171_ += Class397_Sub1.anInt5777 * i_1173_;
								i_1169_ += i_1173_;
							}
							int i_1174_;
							if ((i_1174_ = ((i_1171_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
									/ Class397_Sub1.anInt5777)) > i_1172_)
								i_1172_ = i_1174_;
							int i_1175_ = is[i_1168_] - i;
							int i_1176_ = -is_1143_[i_1168_];
							int i_1177_ = (i_1175_ - (i_1169_ - Class397_Sub1.anInt5759));
							if (i_1177_ > 0) {
								i_1169_ += i_1177_;
								i_1172_ += i_1177_;
								i_1170_ += Class397_Sub1.anInt5766 * i_1177_;
								i_1171_ += Class397_Sub1.anInt5777 * i_1177_;
							} else
								i_1176_ -= i_1177_;
							if (i_1172_ < i_1176_)
								i_1172_ = i_1176_;
							for (/**/; i_1172_ < 0; i_1172_++) {
								int i_1178_ = (aByteArray6691[((i_1171_ >> 12) * anInt5758 + (i_1170_ >> 12))]);
								if (i_1178_ != 0)
									is_1145_[i_1169_++] = anIntArray6692[i_1178_ & 0xff];
								else
									i_1169_++;
								i_1171_ += Class397_Sub1.anInt5777;
							}
						}
					}
					i_1167_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5766 < 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_1179_ = Class397_Sub1.anInt5771;
				while (i_1179_ < 0) {
					int i_1180_ = i_1179_ + i_1144_;
					if (i_1180_ >= 0) {
						if (i_1180_ >= is.length)
							break;
						int i_1181_ = Class397_Sub1.anInt5759;
						int i_1182_ = (Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764);
						int i_1183_ = Class397_Sub1.anInt5775;
						int i_1184_ = Class397_Sub1.anInt5770;
						if (i_1183_ >= 0 && i_1183_ - (anInt5756 << 12) < 0) {
							int i_1185_;
							if ((i_1185_ = i_1182_ - (anInt5758 << 12)) >= 0) {
								i_1185_ = ((Class397_Sub1.anInt5766 - i_1185_) / Class397_Sub1.anInt5766);
								i_1184_ += i_1185_;
								i_1182_ += Class397_Sub1.anInt5766 * i_1185_;
								i_1181_ += i_1185_;
							}
							if ((i_1185_ = ((i_1182_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_1184_)
								i_1184_ = i_1185_;
							int i_1186_ = is[i_1180_] - i;
							int i_1187_ = -is_1143_[i_1180_];
							int i_1188_ = (i_1186_ - (i_1181_ - Class397_Sub1.anInt5759));
							if (i_1188_ > 0) {
								i_1181_ += i_1188_;
								i_1184_ += i_1188_;
								i_1182_ += Class397_Sub1.anInt5766 * i_1188_;
								i_1183_ += Class397_Sub1.anInt5777 * i_1188_;
							} else
								i_1187_ -= i_1188_;
							if (i_1184_ < i_1187_)
								i_1184_ = i_1187_;
							for (/**/; i_1184_ < 0; i_1184_++) {
								int i_1189_ = (aByteArray6691[((i_1183_ >> 12) * anInt5758 + (i_1182_ >> 12))]);
								if (i_1189_ != 0)
									is_1145_[i_1181_++] = anIntArray6692[i_1189_ & 0xff];
								else
									i_1181_++;
								i_1182_ += Class397_Sub1.anInt5766;
							}
						}
					}
					i_1179_++;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_1190_ = Class397_Sub1.anInt5771;
				while (i_1190_ < 0) {
					int i_1191_ = i_1190_ + i_1144_;
					if (i_1191_ >= 0) {
						if (i_1191_ >= is.length)
							break;
						int i_1192_ = Class397_Sub1.anInt5759;
						int i_1193_ = (Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764);
						int i_1194_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1195_ = Class397_Sub1.anInt5770;
						int i_1196_;
						if ((i_1196_ = i_1193_ - (anInt5758 << 12)) >= 0) {
							i_1196_ = ((Class397_Sub1.anInt5766 - i_1196_) / Class397_Sub1.anInt5766);
							i_1195_ += i_1196_;
							i_1193_ += Class397_Sub1.anInt5766 * i_1196_;
							i_1194_ += Class397_Sub1.anInt5777 * i_1196_;
							i_1192_ += i_1196_;
						}
						if ((i_1196_ = ((i_1193_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_1195_)
							i_1195_ = i_1196_;
						if ((i_1196_ = i_1194_ - (anInt5756 << 12)) >= 0) {
							i_1196_ = ((Class397_Sub1.anInt5777 - i_1196_) / Class397_Sub1.anInt5777);
							i_1195_ += i_1196_;
							i_1193_ += Class397_Sub1.anInt5766 * i_1196_;
							i_1194_ += Class397_Sub1.anInt5777 * i_1196_;
							i_1192_ += i_1196_;
						}
						if ((i_1196_ = ((i_1194_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_1195_)
							i_1195_ = i_1196_;
						int i_1197_ = is[i_1191_] - i;
						int i_1198_ = -is_1143_[i_1191_];
						int i_1199_ = i_1197_ - (i_1192_ - Class397_Sub1.anInt5759);
						if (i_1199_ > 0) {
							i_1192_ += i_1199_;
							i_1195_ += i_1199_;
							i_1193_ += Class397_Sub1.anInt5766 * i_1199_;
							i_1194_ += Class397_Sub1.anInt5777 * i_1199_;
						} else
							i_1198_ -= i_1199_;
						if (i_1195_ < i_1198_)
							i_1195_ = i_1198_;
						for (/**/; i_1195_ < 0; i_1195_++) {
							int i_1200_ = (aByteArray6691[(i_1194_ >> 12) * anInt5758 + (i_1193_ >> 12)]);
							if (i_1200_ != 0)
								is_1145_[i_1192_++] = anIntArray6692[i_1200_ & 0xff];
							else
								i_1192_++;
							i_1193_ += Class397_Sub1.anInt5766;
							i_1194_ += Class397_Sub1.anInt5777;
						}
					}
					i_1190_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_1201_ = Class397_Sub1.anInt5771;
				while (i_1201_ < 0) {
					int i_1202_ = i_1201_ + i_1144_;
					if (i_1202_ >= 0) {
						if (i_1202_ >= is.length)
							break;
						int i_1203_ = Class397_Sub1.anInt5759;
						int i_1204_ = (Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764);
						int i_1205_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1206_ = Class397_Sub1.anInt5770;
						int i_1207_;
						if ((i_1207_ = i_1204_ - (anInt5758 << 12)) >= 0) {
							i_1207_ = ((Class397_Sub1.anInt5766 - i_1207_) / Class397_Sub1.anInt5766);
							i_1206_ += i_1207_;
							i_1204_ += Class397_Sub1.anInt5766 * i_1207_;
							i_1205_ += Class397_Sub1.anInt5777 * i_1207_;
							i_1203_ += i_1207_;
						}
						if ((i_1207_ = ((i_1204_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_1206_)
							i_1206_ = i_1207_;
						if (i_1205_ < 0) {
							i_1207_ = ((Class397_Sub1.anInt5777 - 1 - i_1205_) / Class397_Sub1.anInt5777);
							i_1206_ += i_1207_;
							i_1204_ += Class397_Sub1.anInt5766 * i_1207_;
							i_1205_ += Class397_Sub1.anInt5777 * i_1207_;
							i_1203_ += i_1207_;
						}
						if ((i_1207_ = ((i_1205_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
								/ Class397_Sub1.anInt5777)) > i_1206_)
							i_1206_ = i_1207_;
						int i_1208_ = is[i_1202_] - i;
						int i_1209_ = -is_1143_[i_1202_];
						int i_1210_ = i_1208_ - (i_1203_ - Class397_Sub1.anInt5759);
						if (i_1210_ > 0) {
							i_1203_ += i_1210_;
							i_1206_ += i_1210_;
							i_1204_ += Class397_Sub1.anInt5766 * i_1210_;
							i_1205_ += Class397_Sub1.anInt5777 * i_1210_;
						} else
							i_1209_ -= i_1210_;
						if (i_1206_ < i_1209_)
							i_1206_ = i_1209_;
						for (/**/; i_1206_ < 0; i_1206_++) {
							int i_1211_ = (aByteArray6691[(i_1205_ >> 12) * anInt5758 + (i_1204_ >> 12)]);
							if (i_1211_ != 0)
								is_1145_[i_1203_++] = anIntArray6692[i_1211_ & 0xff];
							else
								i_1203_++;
							i_1204_ += Class397_Sub1.anInt5766;
							i_1205_ += Class397_Sub1.anInt5777;
						}
					}
					i_1201_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5777 == 0) {
			int i_1212_ = Class397_Sub1.anInt5771;
			while (i_1212_ < 0) {
				int i_1213_ = i_1212_ + i_1144_;
				if (i_1213_ >= 0) {
					if (i_1213_ >= is.length)
						break;
					int i_1214_ = Class397_Sub1.anInt5759;
					int i_1215_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_1216_ = Class397_Sub1.anInt5775;
					int i_1217_ = Class397_Sub1.anInt5770;
					if (i_1216_ >= 0 && i_1216_ - (anInt5756 << 12) < 0) {
						if (i_1215_ < 0) {
							int i_1218_ = ((Class397_Sub1.anInt5766 - 1 - i_1215_) / Class397_Sub1.anInt5766);
							i_1217_ += i_1218_;
							i_1215_ += Class397_Sub1.anInt5766 * i_1218_;
							i_1214_ += i_1218_;
						}
						int i_1219_;
						if ((i_1219_ = ((i_1215_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
								/ Class397_Sub1.anInt5766)) > i_1217_)
							i_1217_ = i_1219_;
						int i_1220_ = is[i_1213_] - i;
						int i_1221_ = -is_1143_[i_1213_];
						int i_1222_ = i_1220_ - (i_1214_ - Class397_Sub1.anInt5759);
						if (i_1222_ > 0) {
							i_1214_ += i_1222_;
							i_1217_ += i_1222_;
							i_1215_ += Class397_Sub1.anInt5766 * i_1222_;
							i_1216_ += Class397_Sub1.anInt5777 * i_1222_;
						} else
							i_1221_ -= i_1222_;
						if (i_1217_ < i_1221_)
							i_1217_ = i_1221_;
						for (/**/; i_1217_ < 0; i_1217_++) {
							int i_1223_ = (aByteArray6691[(i_1216_ >> 12) * anInt5758 + (i_1215_ >> 12)]);
							if (i_1223_ != 0)
								is_1145_[i_1214_++] = anIntArray6692[i_1223_ & 0xff];
							else
								i_1214_++;
							i_1215_ += Class397_Sub1.anInt5766;
						}
					}
				}
				i_1212_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else if (Class397_Sub1.anInt5777 < 0) {
			int i_1224_ = Class397_Sub1.anInt5771;
			while (i_1224_ < 0) {
				int i_1225_ = i_1224_ + i_1144_;
				if (i_1225_ >= 0) {
					if (i_1225_ >= is.length)
						break;
					int i_1226_ = Class397_Sub1.anInt5759;
					int i_1227_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_1228_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_1229_ = Class397_Sub1.anInt5770;
					if (i_1227_ < 0) {
						int i_1230_ = ((Class397_Sub1.anInt5766 - 1 - i_1227_) / Class397_Sub1.anInt5766);
						i_1229_ += i_1230_;
						i_1227_ += Class397_Sub1.anInt5766 * i_1230_;
						i_1228_ += Class397_Sub1.anInt5777 * i_1230_;
						i_1226_ += i_1230_;
					}
					int i_1231_;
					if ((i_1231_ = ((i_1227_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
							/ Class397_Sub1.anInt5766)) > i_1229_)
						i_1229_ = i_1231_;
					if ((i_1231_ = i_1228_ - (anInt5756 << 12)) >= 0) {
						i_1231_ = ((Class397_Sub1.anInt5777 - i_1231_) / Class397_Sub1.anInt5777);
						i_1229_ += i_1231_;
						i_1227_ += Class397_Sub1.anInt5766 * i_1231_;
						i_1228_ += Class397_Sub1.anInt5777 * i_1231_;
						i_1226_ += i_1231_;
					}
					if ((i_1231_ = ((i_1228_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_1229_)
						i_1229_ = i_1231_;
					int i_1232_ = is[i_1225_] - i;
					int i_1233_ = -is_1143_[i_1225_];
					int i_1234_ = i_1232_ - (i_1226_ - Class397_Sub1.anInt5759);
					if (i_1234_ > 0) {
						i_1226_ += i_1234_;
						i_1229_ += i_1234_;
						i_1227_ += Class397_Sub1.anInt5766 * i_1234_;
						i_1228_ += Class397_Sub1.anInt5777 * i_1234_;
					} else
						i_1233_ -= i_1234_;
					if (i_1229_ < i_1233_)
						i_1229_ = i_1233_;
					for (/**/; i_1229_ < 0; i_1229_++) {
						int i_1235_ = (aByteArray6691[(i_1228_ >> 12) * anInt5758 + (i_1227_ >> 12)]);
						if (i_1235_ != 0)
							is_1145_[i_1226_++] = anIntArray6692[i_1235_ & 0xff];
						else
							i_1226_++;
						i_1227_ += Class397_Sub1.anInt5766;
						i_1228_ += Class397_Sub1.anInt5777;
					}
				}
				i_1224_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else {
			int i_1236_ = Class397_Sub1.anInt5771;
			while (i_1236_ < 0) {
				int i_1237_ = i_1236_ + i_1144_;
				if (i_1237_ >= 0) {
					if (i_1237_ >= is.length)
						break;
					int i_1238_ = Class397_Sub1.anInt5759;
					int i_1239_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_1240_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_1241_ = Class397_Sub1.anInt5770;
					if (i_1239_ < 0) {
						int i_1242_ = ((Class397_Sub1.anInt5766 - 1 - i_1239_) / Class397_Sub1.anInt5766);
						i_1241_ += i_1242_;
						i_1239_ += Class397_Sub1.anInt5766 * i_1242_;
						i_1240_ += Class397_Sub1.anInt5777 * i_1242_;
						i_1238_ += i_1242_;
					}
					int i_1243_;
					if ((i_1243_ = ((i_1239_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
							/ Class397_Sub1.anInt5766)) > i_1241_)
						i_1241_ = i_1243_;
					if (i_1240_ < 0) {
						i_1243_ = ((Class397_Sub1.anInt5777 - 1 - i_1240_) / Class397_Sub1.anInt5777);
						i_1241_ += i_1243_;
						i_1239_ += Class397_Sub1.anInt5766 * i_1243_;
						i_1240_ += Class397_Sub1.anInt5777 * i_1243_;
						i_1238_ += i_1243_;
					}
					if ((i_1243_ = ((i_1240_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
							/ Class397_Sub1.anInt5777)) > i_1241_)
						i_1241_ = i_1243_;
					int i_1244_ = is[i_1237_] - i;
					int i_1245_ = -is_1143_[i_1237_];
					int i_1246_ = i_1244_ - (i_1238_ - Class397_Sub1.anInt5759);
					if (i_1246_ > 0) {
						i_1238_ += i_1246_;
						i_1241_ += i_1246_;
						i_1239_ += Class397_Sub1.anInt5766 * i_1246_;
						i_1240_ += Class397_Sub1.anInt5777 * i_1246_;
					} else
						i_1245_ -= i_1246_;
					if (i_1241_ < i_1245_)
						i_1241_ = i_1245_;
					for (/**/; i_1241_ < 0; i_1241_++) {
						int i_1247_ = (aByteArray6691[(i_1240_ >> 12) * anInt5758 + (i_1239_ >> 12)]);
						if (i_1247_ != 0)
							is_1145_[i_1238_++] = anIntArray6692[i_1247_ & 0xff];
						else
							i_1238_++;
						i_1239_ += Class397_Sub1.anInt5766;
						i_1240_ += Class397_Sub1.anInt5777;
					}
				}
				i_1236_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		}
	}

	public void method4079(int i, int i_1248_, int i_1249_, int i_1250_, int i_1251_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		int i_1252_ = aHa_Sub2_5768.anInt4084;
		i += anInt5772;
		i_1248_ += anInt5761;
		int i_1253_ = i_1248_ * i_1252_ + i;
		int i_1254_ = 0;
		int i_1255_ = anInt5756;
		int i_1256_ = anInt5758;
		int i_1257_ = i_1252_ - i_1256_;
		int i_1258_ = 0;
		if (i_1248_ < aHa_Sub2_5768.anInt4085) {
			int i_1259_ = aHa_Sub2_5768.anInt4085 - i_1248_;
			i_1255_ -= i_1259_;
			i_1248_ = aHa_Sub2_5768.anInt4085;
			i_1254_ += i_1259_ * i_1256_;
			i_1253_ += i_1259_ * i_1252_;
		}
		if (i_1248_ + i_1255_ > aHa_Sub2_5768.anInt4078)
			i_1255_ -= i_1248_ + i_1255_ - aHa_Sub2_5768.anInt4078;
		if (i < aHa_Sub2_5768.anInt4079) {
			int i_1260_ = aHa_Sub2_5768.anInt4079 - i;
			i_1256_ -= i_1260_;
			i = aHa_Sub2_5768.anInt4079;
			i_1254_ += i_1260_;
			i_1253_ += i_1260_;
			i_1258_ += i_1260_;
			i_1257_ += i_1260_;
		}
		if (i + i_1256_ > aHa_Sub2_5768.anInt4104) {
			int i_1261_ = i + i_1256_ - aHa_Sub2_5768.anInt4104;
			i_1256_ -= i_1261_;
			i_1258_ += i_1261_;
			i_1257_ += i_1261_;
		}
		if (i_1256_ > 0 && i_1255_ > 0) {
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_1251_ == 0) {
				if (i_1249_ == 1) {
					for (int i_1262_ = -i_1255_; i_1262_ < 0; i_1262_++) {
						int i_1263_ = i_1253_ + i_1256_ - 3;
						while (i_1253_ < i_1263_) {
							is[i_1253_++] = (anIntArray6692[aByteArray6691[i_1254_++] & 0xff]);
							is[i_1253_++] = (anIntArray6692[aByteArray6691[i_1254_++] & 0xff]);
							is[i_1253_++] = (anIntArray6692[aByteArray6691[i_1254_++] & 0xff]);
							is[i_1253_++] = (anIntArray6692[aByteArray6691[i_1254_++] & 0xff]);
						}
						i_1263_ += 3;
						while (i_1253_ < i_1263_)
							is[i_1253_++] = (anIntArray6692[aByteArray6691[i_1254_++] & 0xff]);
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				if (i_1249_ == 0) {
					int i_1264_ = (i_1250_ & 0xff0000) >> 16;
					int i_1265_ = (i_1250_ & 0xff00) >> 8;
					int i_1266_ = i_1250_ & 0xff;
					for (int i_1267_ = -i_1255_; i_1267_ < 0; i_1267_++) {
						for (int i_1268_ = -i_1256_; i_1268_ < 0; i_1268_++) {
							int i_1269_ = (anIntArray6692[aByteArray6691[i_1254_++] & 0xff]);
							int i_1270_ = (i_1269_ & 0xff0000) * i_1264_ & ~0xffffff;
							int i_1271_ = (i_1269_ & 0xff00) * i_1265_ & 0xff0000;
							int i_1272_ = (i_1269_ & 0xff) * i_1266_ & 0xff00;
							is[i_1253_++] = (i_1270_ | i_1271_ | i_1272_) >>> 8;
						}
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				if (i_1249_ == 3) {
					for (int i_1273_ = -i_1255_; i_1273_ < 0; i_1273_++) {
						for (int i_1274_ = -i_1256_; i_1274_ < 0; i_1274_++) {
							int i_1275_ = (anIntArray6692[aByteArray6691[i_1254_++] & 0xff]);
							int i_1276_ = i_1275_ + i_1250_;
							int i_1277_ = (i_1275_ & 0xff00ff) + (i_1250_ & 0xff00ff);
							int i_1278_ = ((i_1277_ & 0x1000100) + (i_1276_ - i_1277_ & 0x10000));
							is[i_1253_++] = i_1276_ - i_1278_ | i_1278_ - (i_1278_ >>> 8);
						}
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				if (i_1249_ == 2) {
					int i_1279_ = i_1250_ >>> 24;
					int i_1280_ = 256 - i_1279_;
					int i_1281_ = (i_1250_ & 0xff00ff) * i_1280_ & ~0xff00ff;
					int i_1282_ = (i_1250_ & 0xff00) * i_1280_ & 0xff0000;
					i_1250_ = (i_1281_ | i_1282_) >>> 8;
					for (int i_1283_ = -i_1255_; i_1283_ < 0; i_1283_++) {
						for (int i_1284_ = -i_1256_; i_1284_ < 0; i_1284_++) {
							int i_1285_ = (anIntArray6692[aByteArray6691[i_1254_++] & 0xff]);
							i_1281_ = (i_1285_ & 0xff00ff) * i_1279_ & ~0xff00ff;
							i_1282_ = (i_1285_ & 0xff00) * i_1279_ & 0xff0000;
							is[i_1253_++] = ((i_1281_ | i_1282_) >>> 8) + i_1250_;
						}
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_1251_ == 1) {
				if (i_1249_ == 1) {
					for (int i_1286_ = -i_1255_; i_1286_ < 0; i_1286_++) {
						for (int i_1287_ = -i_1256_; i_1287_ < 0; i_1287_++) {
							int i_1288_ = aByteArray6691[i_1254_++];
							if (i_1288_ != 0) {
								int i_1289_ = (anIntArray6692[i_1288_ & 0xff] | ~0xffffff);
								int i_1290_ = 255;
								int i_1291_ = 0;
								int i_1292_ = is[i_1253_];
								is[i_1253_++] = (((((i_1289_ & 0xff00ff) * i_1290_ + (i_1292_ & 0xff00ff) * i_1291_)
										& ~0xff00ff) >> 8)
										+ (((((i_1289_ & ~0xff00ff) >>> 8) * i_1290_)
												+ (((i_1292_ & ~0xff00ff) >>> 8) * i_1291_)) & ~0xff00ff));
							} else
								i_1253_++;
						}
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				if (i_1249_ == 0) {
					if ((i_1250_ & 0xffffff) == 16777215) {
						int i_1293_ = i_1250_ >>> 24;
						int i_1294_ = 256 - i_1293_;
						for (int i_1295_ = -i_1255_; i_1295_ < 0; i_1295_++) {
							for (int i_1296_ = -i_1256_; i_1296_ < 0; i_1296_++) {
								int i_1297_ = aByteArray6691[i_1254_++];
								if (i_1297_ != 0) {
									int i_1298_ = anIntArray6692[i_1297_ & 0xff];
									int i_1299_ = is[i_1253_];
									is[i_1253_++] = ((((i_1298_ & 0xff00ff) * i_1293_ + (i_1299_ & 0xff00ff) * i_1294_)
											& ~0xff00ff)
											+ (((i_1298_ & 0xff00) * i_1293_ + (i_1299_ & 0xff00) * i_1294_)
													& 0xff0000)) >> 8;
								} else
									i_1253_++;
							}
							i_1253_ += i_1257_;
							i_1254_ += i_1258_;
						}
					} else {
						int i_1300_ = (i_1250_ & 0xff0000) >> 16;
						int i_1301_ = (i_1250_ & 0xff00) >> 8;
						int i_1302_ = i_1250_ & 0xff;
						int i_1303_ = i_1250_ >>> 24;
						int i_1304_ = 256 - i_1303_;
						for (int i_1305_ = -i_1255_; i_1305_ < 0; i_1305_++) {
							for (int i_1306_ = -i_1256_; i_1306_ < 0; i_1306_++) {
								int i_1307_ = aByteArray6691[i_1254_++];
								if (i_1307_ != 0) {
									int i_1308_ = anIntArray6692[i_1307_ & 0xff];
									if (i_1303_ != 255) {
										int i_1309_ = ((i_1308_ & 0xff0000) * i_1300_ & ~0xffffff);
										int i_1310_ = ((i_1308_ & 0xff00) * i_1301_ & 0xff0000);
										int i_1311_ = ((i_1308_ & 0xff) * i_1302_ & 0xff00);
										i_1308_ = (i_1309_ | i_1310_ | i_1311_) >>> 8;
										int i_1312_ = is[i_1253_];
										is[i_1253_++] = ((((i_1308_ & 0xff00ff) * i_1303_
												+ ((i_1312_ & 0xff00ff) * i_1304_)) & ~0xff00ff)
												+ (((i_1308_ & 0xff00) * i_1303_ + ((i_1312_ & 0xff00) * i_1304_))
														& 0xff0000)) >> 8;
									} else {
										int i_1313_ = ((i_1308_ & 0xff0000) * i_1300_ & ~0xffffff);
										int i_1314_ = ((i_1308_ & 0xff00) * i_1301_ & 0xff0000);
										int i_1315_ = ((i_1308_ & 0xff) * i_1302_ & 0xff00);
										is[i_1253_++] = (i_1313_ | i_1314_ | i_1315_) >>> 8;
									}
								} else
									i_1253_++;
							}
							i_1253_ += i_1257_;
							i_1254_ += i_1258_;
						}
						return;
					}
					return;
				}
				if (i_1249_ == 3) {
					int i_1316_ = i_1250_ >>> 24;
					int i_1317_ = 256 - i_1316_;
					for (int i_1318_ = -i_1255_; i_1318_ < 0; i_1318_++) {
						for (int i_1319_ = -i_1256_; i_1319_ < 0; i_1319_++) {
							byte i_1320_ = aByteArray6691[i_1254_++];
							int i_1321_ = i_1320_ > 0 ? anIntArray6692[i_1320_] : 0;
							int i_1322_ = i_1321_ + i_1250_;
							int i_1323_ = (i_1321_ & 0xff00ff) + (i_1250_ & 0xff00ff);
							int i_1324_ = ((i_1323_ & 0x1000100) + (i_1322_ - i_1323_ & 0x10000));
							i_1324_ = i_1322_ - i_1324_ | i_1324_ - (i_1324_ >>> 8);
							if (i_1321_ == 0 && i_1316_ != 255) {
								i_1321_ = i_1324_;
								i_1324_ = is[i_1253_];
								i_1324_ = ((((i_1321_ & 0xff00ff) * i_1316_ + (i_1324_ & 0xff00ff) * i_1317_)
										& ~0xff00ff)
										+ (((i_1321_ & 0xff00) * i_1316_ + (i_1324_ & 0xff00) * i_1317_)
												& 0xff0000)) >> 8;
							}
							is[i_1253_++] = i_1324_;
						}
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				if (i_1249_ == 2) {
					int i_1325_ = i_1250_ >>> 24;
					int i_1326_ = 256 - i_1325_;
					int i_1327_ = (i_1250_ & 0xff00ff) * i_1326_ & ~0xff00ff;
					int i_1328_ = (i_1250_ & 0xff00) * i_1326_ & 0xff0000;
					i_1250_ = (i_1327_ | i_1328_) >>> 8;
					for (int i_1329_ = -i_1255_; i_1329_ < 0; i_1329_++) {
						for (int i_1330_ = -i_1256_; i_1330_ < 0; i_1330_++) {
							int i_1331_ = aByteArray6691[i_1254_++];
							if (i_1331_ != 0) {
								int i_1332_ = anIntArray6692[i_1331_ & 0xff];
								i_1327_ = ((i_1332_ & 0xff00ff) * i_1325_ & ~0xff00ff);
								i_1328_ = (i_1332_ & 0xff00) * i_1325_ & 0xff0000;
								is[i_1253_++] = ((i_1327_ | i_1328_) >>> 8) + i_1250_;
							} else
								i_1253_++;
						}
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_1251_ == 2) {
				if (i_1249_ == 1) {
					for (int i_1333_ = -i_1255_; i_1333_ < 0; i_1333_++) {
						for (int i_1334_ = -i_1256_; i_1334_ < 0; i_1334_++) {
							int i_1335_ = aByteArray6691[i_1254_++];
							if (i_1335_ != 0) {
								int i_1336_ = anIntArray6692[i_1335_ & 0xff];
								int i_1337_ = is[i_1253_];
								int i_1338_ = i_1336_ + i_1337_;
								int i_1339_ = ((i_1336_ & 0xff00ff) + (i_1337_ & 0xff00ff));
								i_1337_ = ((i_1339_ & 0x1000100) + (i_1338_ - i_1339_ & 0x10000));
								is[i_1253_++] = i_1338_ - i_1337_ | i_1337_ - (i_1337_ >>> 8);
							} else
								i_1253_++;
						}
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				if (i_1249_ == 0) {
					int i_1340_ = (i_1250_ & 0xff0000) >> 16;
					int i_1341_ = (i_1250_ & 0xff00) >> 8;
					int i_1342_ = i_1250_ & 0xff;
					for (int i_1343_ = -i_1255_; i_1343_ < 0; i_1343_++) {
						for (int i_1344_ = -i_1256_; i_1344_ < 0; i_1344_++) {
							int i_1345_ = aByteArray6691[i_1254_++];
							if (i_1345_ != 0) {
								int i_1346_ = anIntArray6692[i_1345_ & 0xff];
								int i_1347_ = ((i_1346_ & 0xff0000) * i_1340_ & ~0xffffff);
								int i_1348_ = (i_1346_ & 0xff00) * i_1341_ & 0xff0000;
								int i_1349_ = (i_1346_ & 0xff) * i_1342_ & 0xff00;
								i_1346_ = (i_1347_ | i_1348_ | i_1349_) >>> 8;
								int i_1350_ = is[i_1253_];
								int i_1351_ = i_1346_ + i_1350_;
								int i_1352_ = ((i_1346_ & 0xff00ff) + (i_1350_ & 0xff00ff));
								i_1350_ = ((i_1352_ & 0x1000100) + (i_1351_ - i_1352_ & 0x10000));
								is[i_1253_++] = i_1351_ - i_1350_ | i_1350_ - (i_1350_ >>> 8);
							} else
								i_1253_++;
						}
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				if (i_1249_ == 3) {
					for (int i_1353_ = -i_1255_; i_1353_ < 0; i_1353_++) {
						for (int i_1354_ = -i_1256_; i_1354_ < 0; i_1354_++) {
							byte i_1355_ = aByteArray6691[i_1254_++];
							int i_1356_ = i_1355_ > 0 ? anIntArray6692[i_1355_] : 0;
							int i_1357_ = i_1356_ + i_1250_;
							int i_1358_ = (i_1356_ & 0xff00ff) + (i_1250_ & 0xff00ff);
							int i_1359_ = ((i_1358_ & 0x1000100) + (i_1357_ - i_1358_ & 0x10000));
							i_1356_ = i_1357_ - i_1359_ | i_1359_ - (i_1359_ >>> 8);
							i_1359_ = is[i_1253_];
							i_1357_ = i_1356_ + i_1359_;
							i_1358_ = (i_1356_ & 0xff00ff) + (i_1359_ & 0xff00ff);
							i_1359_ = (i_1358_ & 0x1000100) + (i_1357_ - i_1358_ & 0x10000);
							is[i_1253_++] = i_1357_ - i_1359_ | i_1359_ - (i_1359_ >>> 8);
						}
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				if (i_1249_ == 2) {
					int i_1360_ = i_1250_ >>> 24;
					int i_1361_ = 256 - i_1360_;
					int i_1362_ = (i_1250_ & 0xff00ff) * i_1361_ & ~0xff00ff;
					int i_1363_ = (i_1250_ & 0xff00) * i_1361_ & 0xff0000;
					i_1250_ = (i_1362_ | i_1363_) >>> 8;
					for (int i_1364_ = -i_1255_; i_1364_ < 0; i_1364_++) {
						for (int i_1365_ = -i_1256_; i_1365_ < 0; i_1365_++) {
							int i_1366_ = aByteArray6691[i_1254_++];
							if (i_1366_ != 0) {
								int i_1367_ = anIntArray6692[i_1366_ & 0xff];
								i_1362_ = ((i_1367_ & 0xff00ff) * i_1360_ & ~0xff00ff);
								i_1363_ = (i_1367_ & 0xff00) * i_1360_ & 0xff0000;
								i_1367_ = ((i_1362_ | i_1363_) >>> 8) + i_1250_;
								int i_1368_ = is[i_1253_];
								int i_1369_ = i_1367_ + i_1368_;
								int i_1370_ = ((i_1367_ & 0xff00ff) + (i_1368_ & 0xff00ff));
								i_1368_ = ((i_1370_ & 0x1000100) + (i_1369_ - i_1370_ & 0x10000));
								is[i_1253_++] = i_1369_ - i_1368_ | i_1368_ - (i_1368_ >>> 8);
							} else
								i_1253_++;
						}
						i_1253_ += i_1257_;
						i_1254_ += i_1258_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}
}
