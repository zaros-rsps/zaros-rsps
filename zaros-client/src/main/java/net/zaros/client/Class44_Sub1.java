package net.zaros.client;

/* Class44_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

class Class44_Sub1 extends Animator {
	static int cameraDestX;
	static Class209 aClass209_3856 = new Class209(40);
	private Class338_Sub3 aClass338_Sub3_3857;
	static int camRotY;

	static final void method572(byte i) {
		if (Class296_Sub34_Sub2.localModeWhere != Class41_Sub29.modeWhere) {
			try {
				int i_0_ = 106 % ((i - 79) / 34);
				Class297.method3231("tbrefresh", Class246.aClient2332, false);
			} catch (Throwable throwable) {
				/* empty */
			}
		}
	}

	public static void method573(int i) {
		if (i >= 4)
			aClass209_3856 = null;
	}

	static final void method574(byte[][] is, Class181_Sub1 class181_sub1, byte i) {
		int i_1_ = Class296_Sub51_Sub31.aByteArrayArray6509.length;
		if (i != -127)
			method575(null, true, 3, 39, true, 34, null);
		for (int i_2_ = 0; i_2_ < i_1_; i_2_++) {
			byte[] is_3_ = is[i_2_];
			if (is_3_ != null) {
				int i_4_ = (-Class206.worldBaseX + (Class296_Sub43.anIntArray4940[i_2_] >> 8) * 64);
				int i_5_ = (-Class41_Sub26.worldBaseY + (Class296_Sub43.anIntArray4940[i_2_] & 0xff) * 64);
				Class108.method948(i ^ 0x875);
				class181_sub1.method1840(is_3_, BITConfigDefinition.mapClips, i_5_, Class41_Sub13.aHa3774, 78, i_4_);
			}
		}
	}

	static final int method575(Class210_Sub1 class210_sub1, boolean bool, int i, int i_6_, boolean bool_7_, int i_8_, Class210_Sub1 class210_sub1_9_) {
		int i_10_ = Class270.method2307(class210_sub1, bool_7_, i, 1, class210_sub1_9_);
		if (i_6_ <= 25)
			camRotY = -36;
		if (i_10_ != 0) {
			if (!bool_7_)
				return i_10_;
			return -i_10_;
		}
		if (i_8_ == -1)
			return 0;
		int i_11_ = Class270.method2307(class210_sub1, bool, i_8_, 1, class210_sub1_9_);
		if (!bool)
			return i_11_;
		return -i_11_;
	}

	Class44_Sub1(Class338_Sub3 class338_sub3, boolean bool) {
		super(bool);
		aClass338_Sub3_3857 = class338_sub3;
	}

	final void method565(int i, Animation class43, byte i_12_) {
		Class296_Sub22.method2662(i_12_ ^ 0x5e, aClass338_Sub3_3857, class43, i);
		if (i_12_ != -95)
			method574(null, null, (byte) -21);
	}
}
