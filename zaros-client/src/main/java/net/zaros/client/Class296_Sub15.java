package net.zaros.client;
import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;

abstract class Class296_Sub15 extends Node {
	int anInt4670;
	static Js5 aClass138_4671;
	OggStreamState anOggStreamState4672;

	static final void method2513(MidiDecoder class296_sub47, int i, int i_0_) {
		Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5017.method489(121);
		if (i < 33)
			method2516(-108, 102, 48);
		if (class296_sub47 == null)
			Class208.method2003(-104);
		Class274.aClass381_2524.method3994(97);
		Class44_Sub1_Sub1.method576((byte) -119, class296_sub47);
	}

	final void method2514(OggPacket oggpacket, int i) {
		method2517(oggpacket, 16);
		if (i >= -28)
			aClass138_4671 = null;
		anInt4670++;
	}

	static final Class209 method2515(int i, int i_1_) {
		Class209[] class209s = Class41_Sub26.method499(-34);
		Class209[] class209s_2_ = class209s;
		for (int i_3_ = 0; i_3_ < class209s_2_.length; i_3_++) {
			Class209 class209 = class209s_2_[i_3_];
			if (i == class209.anInt2094)
				return class209;
		}
		if (i_1_ != 1188)
			method2520((byte) 116);
		return null;
	}

	static final Class234 method2516(int i, int i_4_, int i_5_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_4_][i_5_];
		if (class247 == null)
			return null;
		return class247.aClass234_2341;
	}

	abstract void method2517(OggPacket oggpacket, int i);

	static final long method2518(byte i, Interface14 interface14, int i_6_, int i_7_) {
		if (i < 51)
			aClass138_4671 = null;
		long l = 4194304L;
		long l_8_ = -9223372036854775808L;
		ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(interface14.method57((byte) 114));
		long l_9_ = (long) (interface14.method54(-11077) << 14 | (i_7_ | i_6_ << 7) | interface14.method59(42) << 20 | 0x40000000);
		if (class70.interactonType == 0)
			l_9_ |= l_8_;
		if (class70.anInt804 == 1)
			l_9_ |= l;
		l_9_ |= (long) interface14.method57((byte) 98) << 32;
		return l_9_;
	}

	abstract void method2519(int i);

	public static void method2520(byte i) {
		if (i >= -112)
			aClass138_4671 = null;
		aClass138_4671 = null;
	}

	static final int method2521(int i, Class296_Sub39_Sub1 class296_sub39_sub1) {
		if (i != -1)
			method2520((byte) 82);
		String string = TextureOperation.method3066(-4, class296_sub39_sub1);
		return Class304.aClass92_2729.method847(Class44_Sub1_Sub1.aClass397Array5811, string, 0);
	}

	static final int method2522(int i, int i_10_, int i_11_) {
		if (i == -2)
			return 12345678;
		if (i == -1) {
			if (i_10_ < 2)
				i_10_ = 2;
			else if (i_10_ > 126)
				i_10_ = 126;
			return i_10_;
		}
		i_10_ = (i & 0x7f) * i_10_ >> 7;
		if (i_10_ < 2)
			i_10_ = 2;
		else if (i_10_ > 126)
			i_10_ = 126;
		if (i_11_ > -34)
			aClass138_4671 = null;
		return i_10_ + (i & 0xff80);
	}

	static final void method2523(short[] is, String[] strings, int i, int i_12_, int i_13_) {
		if (i_13_ != 30082)
			aClass138_4671 = null;
		if (i_12_ < i) {
			int i_14_ = (i_12_ + i) / 2;
			int i_15_ = i_12_;
			String string = strings[i_14_];
			strings[i_14_] = strings[i];
			strings[i] = string;
			short i_16_ = is[i_14_];
			is[i_14_] = is[i];
			is[i] = i_16_;
			for (int i_17_ = i_12_; i_17_ < i; i_17_++) {
				if (string == null || (strings[i_17_] != null && (i_17_ & 0x1) > strings[i_17_].compareTo(string))) {
					String string_18_ = strings[i_17_];
					strings[i_17_] = strings[i_15_];
					strings[i_15_] = string_18_;
					short i_19_ = is[i_17_];
					is[i_17_] = is[i_15_];
					is[i_15_++] = i_19_;
				}
			}
			strings[i] = strings[i_15_];
			strings[i_15_] = string;
			is[i] = is[i_15_];
			is[i_15_] = i_16_;
			method2523(is, strings, i_15_ - 1, i_12_, 30082);
			method2523(is, strings, i, i_15_ + 1, 30082);
		}
	}

	Class296_Sub15(OggStreamState oggstreamstate) {
		anOggStreamState4672 = oggstreamstate;
	}
}
