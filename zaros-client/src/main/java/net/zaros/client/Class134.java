package net.zaros.client;
/* Class134 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.zip.Inflater;

final class Class134 {
	private Inflater anInflater1384;
	static HashTable aClass263_1385 = new HashTable(16);
	static Class256 aClass256_1386;
	static int anInt1387 = 0;
	static d aD1388;
	static Js5 fs35;

	final byte[] method1401(byte[] is, int i) {
		Packet class296_sub17 = new Packet(is);
		class296_sub17.pos = is.length - 4;
		int i_0_ = class296_sub17.method2574();
		if (i != 11147)
			return null;
		byte[] is_1_ = new byte[i_0_];
		class296_sub17.pos = 0;
		method1403(is_1_, -3651, class296_sub17);
		return is_1_;
	}

	static final boolean method1402(int i) {
		if (i <= 13)
			aClass263_1385 = null;
		if (!Class366_Sub4.method3779("jaclib", (byte) -17))
			return false;
		return Class366_Sub4.method3779("hw3d", (byte) -17);
	}

	public Class134() {
		this(-1, 1000000, 1000000);
	}

	final void method1403(byte[] is, int i, Packet class296_sub17) {
		if (class296_sub17.data[class296_sub17.pos] != 31 || (class296_sub17.data[class296_sub17.pos + 1] != -117))
			throw new RuntimeException("Invalid GZIP header!");
		if (anInflater1384 == null)
			anInflater1384 = new Inflater(true);
		try {
			if (i != -3651)
				aD1388 = null;
			anInflater1384.setInput(class296_sub17.data, class296_sub17.pos + 10, (class296_sub17.data.length - (class296_sub17.pos + 8) - 10));
			anInflater1384.inflate(is);
		} catch (Exception exception) {
			anInflater1384.reset();
			throw new RuntimeException("Invalid GZIP compressed data!");
		}
		anInflater1384.reset();
	}

	public static void method1404(byte i) {
		if (i >= -108)
			anInt1387 = -95;
		aClass256_1386 = null;
		fs35 = null;
		aClass263_1385 = null;
		aD1388 = null;
	}

	static final void method1405(int i, int i_2_, int i_3_, int i_4_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_2_][i_3_];
		if (class247 != null) {
			Class338_Sub3_Sub3 class338_sub3_sub3 = class247.aClass338_Sub3_Sub3_2342;
			Class338_Sub3_Sub3 class338_sub3_sub3_5_ = class247.aClass338_Sub3_Sub3_2337;
			if (class338_sub3_sub3 != null) {
				class338_sub3_sub3.aShort6567 = (short) (class338_sub3_sub3.aShort6567 * i_4_ / (16 << Class313.anInt2779 - 7));
				class338_sub3_sub3.aShort6569 = (short) (class338_sub3_sub3.aShort6569 * i_4_ / (16 << Class313.anInt2779 - 7));
			}
			if (class338_sub3_sub3_5_ != null) {
				class338_sub3_sub3_5_.aShort6567 = (short) (class338_sub3_sub3_5_.aShort6567 * i_4_ / (16 << Class313.anInt2779 - 7));
				class338_sub3_sub3_5_.aShort6569 = (short) (class338_sub3_sub3_5_.aShort6569 * i_4_ / (16 << Class313.anInt2779 - 7));
			}
		}
	}

	static final void method1406(Js5 class138, int[] is, byte i, Js5 class138_6_) {
		if (is != null)
			Class338_Sub3_Sub1_Sub5.anIntArray6843 = is;
		if (i != -7)
			method1406(null, null, (byte) -117, null);
		Class296_Sub22.aClass138_4733 = class138_6_;
		Class368_Sub23.aClass138_5571 = class138;
	}

	private Class134(int i, int i_10_, int i_11_) {
		/* empty */
	}
}
