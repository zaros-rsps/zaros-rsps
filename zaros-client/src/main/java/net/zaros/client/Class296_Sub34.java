package net.zaros.client;

/* Class296_Sub34 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class296_Sub34 extends Node {
	static int anInt4843 = 0;

	abstract int method2732(int i);

	static final void method2733(boolean bool, int i) {
		if (i != 3)
			method2734(-127);
		for (Class296_Sub36 class296_sub36 = ((Class296_Sub36) Class126.aClass155_1289.removeFirst((byte) 114)); class296_sub36 != null; class296_sub36 = (Class296_Sub36) Class126.aClass155_1289.removeNext(i ^ 0x3ea)) {
			if (class296_sub36.aClass296_Sub45_Sub1_4871 != null) {
				Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36.aClass296_Sub45_Sub1_4871);
				class296_sub36.aClass296_Sub45_Sub1_4871 = null;
			}
			if (class296_sub36.aClass296_Sub45_Sub1_4860 != null) {
				Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36.aClass296_Sub45_Sub1_4860);
				class296_sub36.aClass296_Sub45_Sub1_4860 = null;
			}
			class296_sub36.unlink();
		}
		if (bool) {
			for (Class296_Sub36 class296_sub36 = (Class296_Sub36) Class296_Sub15_Sub4.aClass155_6031.removeFirst((byte) 120); class296_sub36 != null; class296_sub36 = ((Class296_Sub36) Class296_Sub15_Sub4.aClass155_6031.removeNext(1001))) {
				if (class296_sub36.aClass296_Sub45_Sub1_4871 != null) {
					Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36.aClass296_Sub45_Sub1_4871);
					class296_sub36.aClass296_Sub45_Sub1_4871 = null;
				}
				class296_sub36.unlink();
			}
			for (Class296_Sub36 class296_sub36 = ((Class296_Sub36) ha_Sub1_Sub1.aClass263_5823.getFirst(true)); class296_sub36 != null; class296_sub36 = (Class296_Sub36) ha_Sub1_Sub1.aClass263_5823.getNext(0)) {
				if (class296_sub36.aClass296_Sub45_Sub1_4871 != null) {
					Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36.aClass296_Sub45_Sub1_4871);
					class296_sub36.aClass296_Sub45_Sub1_4871 = null;
				}
				class296_sub36.unlink();
			}
		}
	}

	static final void method2734(int i) {
		Class200.method1952(112, -1, i);
	}

	abstract int method2735(int i);

	public Class296_Sub34() {
		/* empty */
	}

	abstract int method2736(int i);

	abstract long method2737(int i);

	abstract int method2738(int i);
}
