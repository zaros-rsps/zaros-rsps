package net.zaros.client;
import jaggl.OpenGL;

final class Class296_Sub51_Sub19 extends TextureOperation {
	private int anInt6437 = 0;
	static volatile boolean aBoolean6438 = false;
	static Class259 aClass259_6439;
	private int anInt6440 = 4096;

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		if (i > -84)
			method3128((byte) -94);
		int i_1_ = i_0_;
		while_124_ : do {
			do {
				if (i_1_ != 0) {
					if (i_1_ != 1) {
						if (i_1_ == 2)
							break;
						break while_124_;
					}
				} else {
					anInt6437 = class296_sub17.g2();
					break while_124_;
				}
				anInt6440 = class296_sub17.g2();
				break while_124_;
			} while (false);
			monochromatic = class296_sub17.g1() == 1;
		} while (false);
	}

	static final Class389 method3127(int i, Class77[] class77s, ha_Sub3 var_ha_Sub3) {
		for (int i_2_ = 0; class77s.length > i_2_; i_2_++) {
			if (class77s[i_2_] == null || class77s[i_2_].aLong876 <= 0L)
				return null;
		}
		if (i != 1)
			aClass259_6439 = null;
		long l = OpenGL.glCreateProgramObjectARB();
		for (int i_3_ = 0; class77s.length > i_3_; i_3_++)
			OpenGL.glAttachObjectARB(l, class77s[i_3_].aLong876);
		OpenGL.glLinkProgramARB(l);
		OpenGL.glGetObjectParameterivARB(l, 35714, Class190.anIntArray1934, 0);
		if (Class190.anIntArray1934[0] == 0) {
			if (Class190.anIntArray1934[0] == 0)
				System.out.println("Shader linking failed:");
			OpenGL.glGetObjectParameterivARB(l, 35716, Class190.anIntArray1934, 1);
			if (Class190.anIntArray1934[1] > 1) {
				byte[] is = new byte[Class190.anIntArray1934[1]];
				OpenGL.glGetInfoLogARB(l, Class190.anIntArray1934[1], Class190.anIntArray1934, 0, is, 0);
				System.out.println(new String(is));
			}
			if (Class190.anIntArray1934[0] == 0) {
				for (int i_4_ = 0; i_4_ < class77s.length; i_4_++)
					OpenGL.glDetachObjectARB(l, class77s[i_4_].aLong876);
				OpenGL.glDeleteObjectARB(l);
				return null;
			}
		}
		return new Class389(var_ha_Sub3, l, class77s);
	}

	public static void method3128(byte i) {
		aClass259_6439 = null;
		if (i != 31)
			method3127(-76, null, null);
	}

	public Class296_Sub51_Sub19() {
		super(1, false);
	}

	final int[] get_monochrome_output(int i, int i_5_) {
		if (i != 0)
			return null;
		int[] is = aClass318_5035.method3335(i_5_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int[] is_6_ = this.method3064(0, 0, i_5_);
			for (int i_7_ = 0; Class41_Sub10.anInt3769 > i_7_; i_7_++) {
				int i_8_ = is_6_[i_7_];
				if (anInt6437 <= i_8_) {
					if (i_8_ > anInt6440)
						is[i_7_] = anInt6440;
					else
						is[i_7_] = i_8_;
				} else
					is[i_7_] = anInt6437;
			}
		}
		return is;
	}

	final int[][] get_colour_output(int i, int i_9_) {
		int[][] is = aClass86_5034.method823((byte) 110, i);
		if (i_9_ != 17621)
			anInt6440 = -128;
		if (aClass86_5034.aBoolean939) {
			int[][] is_10_ = this.method3075((byte) 122, 0, i);
			int[] is_11_ = is_10_[0];
			int[] is_12_ = is_10_[1];
			int[] is_13_ = is_10_[2];
			int[] is_14_ = is[0];
			int[] is_15_ = is[1];
			int[] is_16_ = is[2];
			for (int i_17_ = 0; i_17_ < Class41_Sub10.anInt3769; i_17_++) {
				int i_18_ = is_11_[i_17_];
				int i_19_ = is_12_[i_17_];
				int i_20_ = is_13_[i_17_];
				if (anInt6437 > i_18_)
					is_14_[i_17_] = anInt6437;
				else if (anInt6440 >= i_18_)
					is_14_[i_17_] = i_18_;
				else
					is_14_[i_17_] = anInt6440;
				if (i_19_ < anInt6437)
					is_15_[i_17_] = anInt6437;
				else if (i_19_ <= anInt6440)
					is_15_[i_17_] = i_19_;
				else
					is_15_[i_17_] = anInt6440;
				if (i_20_ < anInt6437)
					is_16_[i_17_] = anInt6437;
				else if (i_20_ <= anInt6440)
					is_16_[i_17_] = i_20_;
				else
					is_16_[i_17_] = anInt6440;
			}
		}
		return is;
	}
}
