package net.zaros.client;

/* Class317 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class317 implements Interface21 {
	static OutgoingPacket aClass311_3699 = new OutgoingPacket(68, -1);
	private String aString3700;
	private Js5 aClass138_3701;
	static IncomingPacket aClass231_3702;
	static int ignoresSize = 0;
	static float aFloat3704;
	static int anInt3705;

	@Override
	public final Class388 method78(int i) {
		if (i != 20598) {
			return null;
		}
		return Class388.aClass388_3272;
	}

	static final void parseSubpacket(SubInPacket subP, byte i) {
		ByteStream buff = Class296_Sub45_Sub2.aClass204_6277.in_stream;
		if (subP == Class367.aClass260_3127) {
			int i_0_ = buff.g2();
			int i_1_ = buff.g1();
			Class379.objectDefinitionLoader.getObjectDefinition(i_0_).method748(i_1_, i ^ 0xf);
		} else if (subP == Class75.aClass260_869) {
			int i_2_ = buff.readUnsignedByteC();
			int i_3_ = (i_2_ & 0x7) + ModeWhere.anInt2352;
			int i_4_ = Class41_Sub26.worldBaseY + i_3_;
			int i_5_ = Class10.anInt3537 + ((i_2_ & 0x7e) >> 4);
			int i_6_ = Class206.worldBaseX + i_5_;
			int i_7_ = buff.readUnsignedLEShortA();
			int i_8_ = buff.g2();
			boolean bool = i_5_ >= 0 && i_3_ >= 0 && i_5_ < Class198.currentMapSizeX && Class296_Sub38.currentMapSizeY > i_3_;
			if (bool || Class296_Sub39_Sub9.method2830((byte) 101, Class338_Sub2.mapLoadType)) {
				Class41_Sub29.method518(i_4_, (byte) 92, new Class296_Sub2(i_8_, i_7_), i_6_, Class368_Sub15.anInt5515);
				if (bool) {
					Class295_Sub2.method2426(i_5_, i - 15, i_3_, Class368_Sub15.anInt5515);
				}
			}
		} else if (r_Sub2.aClass260_6710 == subP) {
			int i_9_ = buff.readUnsignedShortA();
			int i_10_ = buff.readUnsignedByteS();
			int i_11_ = (i_10_ & 0x7) + ModeWhere.anInt2352;
			int i_12_ = Class41_Sub26.worldBaseY + i_11_;
			int i_13_ = Class10.anInt3537 + (i_10_ >> 4 & 0x7);
			int i_14_ = i_13_ + Class206.worldBaseX;
			Class296_Sub56 class296_sub56 = (Class296_Sub56) Class296_Sub11.aClass263_4647.get(i_14_ | Class368_Sub15.anInt5515 << 28 | i_12_ << 14);
			if (class296_sub56 != null) {
				for (Class296_Sub2 class296_sub2 = (Class296_Sub2) class296_sub56.aClass155_5080.removeFirst((byte) 122); class296_sub2 != null; class296_sub2 = (Class296_Sub2) class296_sub56.aClass155_5080.removeNext(1001)) {
					if (class296_sub2.anInt4591 == (i_9_ & 0x7fff)) {
						class296_sub2.unlink();
						break;
					}
				}
				if (class296_sub56.aClass155_5080.method1577((byte) -77)) {
					class296_sub56.unlink();
				}
				if (i_13_ >= 0 && i_11_ >= 0 && Class198.currentMapSizeX > i_13_ && i_11_ < Class296_Sub38.currentMapSizeY) {
					Class295_Sub2.method2426(i_13_, 0, i_11_, Class368_Sub15.anInt5515);
				}
			}
		} else if (subP == Class296_Sub24.aClass260_4753) {
			int i_15_ = buff.g1();
			int i_16_ = Class10.anInt3537 + ((i_15_ & 0x78) >> 4);
			int i_17_ = (i_15_ & 0x7) + ModeWhere.anInt2352;
			int i_18_ = buff.g2();
			if (i_18_ == 65535) {
				i_18_ = -1;
			}
			int i_19_ = buff.g1();
			int i_20_ = (i_19_ & 0xfe) >> 4;
			int i_21_ = i_19_ & 0x7;
			int i_22_ = buff.g1();
			int i_23_ = buff.g1();
			int i_24_ = buff.g2();
			if (i_16_ >= 0 && i_17_ >= 0 && i_16_ < Class198.currentMapSizeX && Class296_Sub38.currentMapSizeY > i_17_) {
				int i_25_ = i_20_ + 1;
				if (Class296_Sub51_Sub11.localPlayer.waypointQueueX[0] >= i_16_ - i_25_ && i_25_ + i_16_ >= Class296_Sub51_Sub11.localPlayer.waypointQueueX[0] && i_17_ - i_25_ <= Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0] && i_17_ + i_25_ >= Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0]) {
					Class368_Sub5_Sub1.method3827(i_24_, i_23_, (i_17_ << 8) + (i_16_ << 16) + (Class368_Sub15.anInt5515 << 24) + i_20_, i_18_, i_21_, i_22_, 23358);
				}
			}
		} else if (subP == Class22.aClass260_252) {
			int i_26_ = buff.g1();
			int i_27_ = (i_26_ >> 4 & 0x7) + Class10.anInt3537;
			int i_28_ = (i_26_ & 0x7) + ModeWhere.anInt2352;
			int i_29_ = buff.g2();
			if (i_29_ == 65535) {
				i_29_ = -1;
			}
			int i_30_ = buff.g1();
			int i_31_ = (i_30_ & 0xf3) >> 4;
			int i_32_ = i_30_ & 0x7;
			int i_33_ = buff.g1();
			int i_34_ = buff.g1();
			int i_35_ = buff.g2();
			if (i_27_ >= 0 && i_28_ >= 0 && i_27_ < Class198.currentMapSizeX && i_28_ < Class296_Sub38.currentMapSizeY) {
				int i_36_ = i_31_ + 1;
				if (i_27_ - i_36_ <= Class296_Sub51_Sub11.localPlayer.waypointQueueX[0] && i_36_ + i_27_ >= Class296_Sub51_Sub11.localPlayer.waypointQueueX[0] && i_28_ - i_36_ <= Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0] && i_28_ + i_36_ >= Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0]) {
					Billboard.method4022(i_35_, i ^ ~0xf, i_29_, i_32_, i_31_ + (i_28_ << 8) + (i_27_ << 16) + (Class368_Sub15.anInt5515 << 24), false, i_33_, i_34_);
				}
			}
		} else if (Class41_Sub6.aClass260_3758 == subP) {
			int i_37_ = buff.g1();
			int i_38_ = Class10.anInt3537 + ((i_37_ & 0x79) >> 4);
			int i_39_ = ModeWhere.anInt2352 + (i_37_ & 0x7);
			int i_40_ = buff.g2();
			if (i_40_ == 65535) {
				i_40_ = -1;
			}
			int i_41_ = buff.g1();
			int i_42_ = buff.g2();
			int i_43_ = buff.g1();
			if (i_38_ >= 0 && i_39_ >= 0 && i_38_ < Class198.currentMapSizeX && Class296_Sub38.currentMapSizeY > i_39_) {
				if (i_40_ != -1) {
					int i_44_ = i_38_ * 512 + 256;
					int i_45_ = i_39_ * 512 + 256;
					int i_46_ = Class368_Sub15.anInt5515;
					if (i_46_ < 3 && r_Sub2.method2871(i_39_, i_38_, (byte) -47)) {
						i_46_++;
					}
					Class338_Sub3_Sub1_Sub5 class338_sub3_sub1_sub5 = new Class338_Sub3_Sub1_Sub5(i_40_, i_42_, Class368_Sub15.anInt5515, i_46_, i_44_, -i_41_ + aa_Sub1.method155(i - 1537652870, Class368_Sub15.anInt5515, i_44_, i_45_), i_45_, i_38_, i_38_, i_39_, i_39_, i_43_, false);
					Class134.aClass263_1385.put(i_39_ | i_38_ << 16, new Class296_Sub39_Sub11(class338_sub3_sub1_sub5));
				} else {
					Class296_Sub39_Sub11 class296_sub39_sub11 = (Class296_Sub39_Sub11) Class134.aClass263_1385.get(i_38_ << 16 | i_39_);
					if (class296_sub39_sub11 != null) {
						class296_sub39_sub11.aClass338_Sub3_Sub1_Sub5_6190.method3545((byte) 94);
						class296_sub39_sub11.unlink();
					}
				}
			}
		} else if (i == 15) {
			if (subP == Class41_Sub17.aClass260_3782) {
				int i_47_ = buff.readUnsignedLEShort();
				int i_48_ = buff.readUnsignedShortA();
				int i_49_ = buff.g1();
				int i_50_ = (i_49_ & 0x7) + ModeWhere.anInt2352;
				int i_51_ = Class41_Sub26.worldBaseY + i_50_;
				int i_52_ = Class10.anInt3537 + (i_49_ >> 4 & 0x7);
				int i_53_ = i_52_ + Class206.worldBaseX;
				int i_54_ = buff.readUnsignedShortA();
				if (i_48_ != Class362.myPlayerIndex) {
					boolean bool = i_52_ >= 0 && i_50_ >= 0 && Class198.currentMapSizeX > i_52_ && Class296_Sub38.currentMapSizeY > i_50_;
					if (bool || Class296_Sub39_Sub9.method2830((byte) 124, Class338_Sub2.mapLoadType)) {
						Class41_Sub29.method518(i_51_, (byte) 92, new Class296_Sub2(i_54_, i_47_), i_53_, Class368_Sub15.anInt5515);
						if (bool) {
							Class295_Sub2.method2426(i_52_, 0, i_50_, Class368_Sub15.anInt5515);
						}
					}
				}
			} else if (subP == Class178_Sub2.aClass260_4436) {
				buff.g1();
				int i_55_ = buff.g1();
				int i_56_ = (i_55_ >> 4 & 0x7) + Class10.anInt3537;
				int i_57_ = ModeWhere.anInt2352 + (i_55_ & 0x7);
				int i_58_ = buff.g2();
				int i_59_ = buff.g1();
				int i_60_ = buff.readUnsignedMedInt();
				String string = buff.gstr();
				Class153.method1551(i_57_, i_58_, false, string, i_60_, i_56_, i_59_, Class368_Sub15.anInt5515);
			} else if (Class296_Sub45_Sub2.aClass260_6275 == subP) {
				int i_61_ = buff.g1();
				int i_62_ = ((i_61_ & 0x78) >> 4) + Class10.anInt3537;
				int i_63_ = ModeWhere.anInt2352 + (i_61_ & 0x7);
				int i_64_ = buff.readUnsignedByteC();
				int i_65_ = i_64_ >> 2;
				int i_66_ = i_64_ & 0x3;
				int i_67_ = Class304.anIntArray2731[i_65_];
				if (Class296_Sub39_Sub9.method2830((byte) 96, Class338_Sub2.mapLoadType) || i_62_ >= 0 && i_63_ >= 0 && i_62_ < Class198.currentMapSizeX && i_63_ < Class296_Sub38.currentMapSizeY) {
					Class268.method2302(i_67_, i_66_, 1024, Class368_Sub15.anInt5515, i_63_, i_62_, -1, i_65_);
				}
			} else if (subP == Class335.aClass260_2953) {
				int i_68_ = buff.g1();
				int i_69_ = (i_68_ & 0x7) + ModeWhere.anInt2352;
				int i_70_ = i_69_ + Class41_Sub26.worldBaseY;
				int i_71_ = (i_68_ >> 4 & 0x7) + Class10.anInt3537;
				int i_72_ = i_71_ + Class206.worldBaseX;
				int i_73_ = buff.g2();
				int i_74_ = buff.g2();
				int i_75_ = buff.g2();
				if (Class296_Sub11.aClass263_4647 != null) {
					Class296_Sub56 class296_sub56 = (Class296_Sub56) Class296_Sub11.aClass263_4647.get(i_72_ | i_70_ << 14 | Class368_Sub15.anInt5515 << 28);
					if (class296_sub56 != null) {
						for (Class296_Sub2 class296_sub2 = (Class296_Sub2) class296_sub56.aClass155_5080.removeFirst((byte) 127); class296_sub2 != null; class296_sub2 = (Class296_Sub2) class296_sub56.aClass155_5080.removeNext(1001)) {
							if ((i_73_ & 0x7fff) == class296_sub2.anInt4591 && class296_sub2.anInt4592 == i_74_) {
								class296_sub2.unlink();
								class296_sub2.anInt4592 = i_75_;
								Class41_Sub29.method518(i_70_, (byte) 92, class296_sub2, i_72_, Class368_Sub15.anInt5515);
								break;
							}
						}
						if (i_71_ >= 0 && i_69_ >= 0 && i_71_ < Class198.currentMapSizeX && i_69_ < Class296_Sub38.currentMapSizeY) {
							Class295_Sub2.method2426(i_71_, 0, i_69_, Class368_Sub15.anInt5515);
						}
					}
				}
			} else if (SubCache.aClass260_2708 == subP) {
				int i_76_ = buff.readUnsignedShortA();
				ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(i_76_);
				int i_77_ = buff.readUnsignedByteA();
				int i_78_ = i_77_ >> 2;
				int i_79_ = Class304.anIntArray2731[i_78_];
				int i_80_ = buff.readUnsignedByteA();
				int i_81_ = buff.readUnsignedByteA();
				int i_82_ = ((i_81_ & 0x78) >> 4) + Class10.anInt3537;
				int i_83_ = (i_81_ & 0x7) + ModeWhere.anInt2352;
				if (i_78_ == 11) {
					i_78_ = 10;
				}
				int i_84_ = 0;
				if (class70.shapes != null) {
					int i_85_ = -1;
					for (int i_86_ = 0; class70.shapes.length > i_86_; i_86_++) {
						if (class70.shapes[i_86_] == i_78_) {
							i_85_ = i_86_;
							break;
						}
					}
					i_84_ = class70.models[i_85_].length;
				}
				int i_87_ = 0;
				if (class70.modifiedColors != null) {
					i_87_ = class70.modifiedColors.length;
				}
				int i_88_ = 0;
				if (class70.aShortArray762 != null) {
					i_88_ = class70.aShortArray762.length;
				}
				if ((i_80_ & 0x1) != 1) {
					int[] is = null;
					if ((i_80_ & 0x2) == 2) {
						is = new int[i_84_];
						for (int i_89_ = 0; i_84_ > i_89_; i_89_++) {
							is[i_89_] = buff.g2();
						}
					}
					short[] is_90_ = null;
					if ((i_80_ & 0x4) == 4) {
						is_90_ = new short[i_87_];
						for (int i_91_ = 0; i_87_ > i_91_; i_91_++) {
							is_90_[i_91_] = (short) buff.g2();
						}
					}
					short[] is_92_ = null;
					if ((i_80_ & 0x8) == 8) {
						is_92_ = new short[i_88_];
						for (int i_93_ = 0; i_93_ < i_88_; i_93_++) {
							is_92_[i_93_] = (short) buff.g2();
						}
					}
					Class57.method672(i_83_, i_82_, Class368_Sub15.anInt5515, i_79_, new Class375(Class10.aLong3531++, is, is_90_, is_92_), (byte) -105);
				} else {
					Class57.method672(i_83_, i_82_, Class368_Sub15.anInt5515, i_79_, null, (byte) 122);
				}
			} else if (subP == Class42_Sub1.aClass260_3832) {
				int i_94_ = buff.g1();
				int i_95_ = Class10.anInt3537 * 2 + ((i_94_ & 0xf8) >> 4);
				int i_96_ = (i_94_ & 0xf) + ModeWhere.anInt2352 * 2;
				int i_97_ = buff.g1();
				boolean bool = (i_97_ & 0x1) != 0;
				boolean bool_98_ = (i_97_ & 0x2) != 0;
				int i_99_ = !bool_98_ ? -1 : i_97_ >> 2;
				int i_100_ = buff.g1b() + i_95_;
				int i_101_ = buff.g1b() + i_96_;
				int i_102_ = buff.g2b();
				int i_103_ = buff.g2b();
				int i_104_ = buff.g2();
				int i_105_ = buff.g1();
				if (!bool_98_) {
					i_105_ *= 4;
				} else {
					i_105_ = (byte) i_105_;
				}
				int i_106_ = buff.g1() * 4;
				int i_107_ = buff.g2();
				int i_108_ = buff.g2();
				int i_109_ = buff.g1();
				if (i_109_ == 255) {
					i_109_ = -1;
				}
				int i_110_ = buff.g2();
				if (i_95_ >= 0 && i_96_ >= 0 && i_95_ < Class198.currentMapSizeX * 2 && i_96_ < Class198.currentMapSizeX * 2 && i_100_ >= 0 && i_101_ >= 0 && Class296_Sub38.currentMapSizeY * 2 > i_100_ && i_101_ < Class296_Sub38.currentMapSizeY * 2 && i_104_ != 65535) {
					i_105_ <<= 2;
					i_95_ *= 256;
					i_96_ *= 256;
					i_101_ *= 256;
					i_100_ *= 256;
					i_110_ <<= 2;
					i_106_ <<= 2;
					if (i_102_ != 0 && i_99_ != -1) {
						Mobile class338_sub3_sub1_sub3 = null;
						if (i_102_ < 0) {
							int i_111_ = -i_102_ - 1;
							if (i_111_ != Class362.myPlayerIndex) {
								class338_sub3_sub1_sub3 = PlayerUpdate.visiblePlayers[i_111_];
							} else {
								class338_sub3_sub1_sub3 = Class296_Sub51_Sub11.localPlayer;
							}
						} else {
							int i_112_ = i_102_ - 1;
							NPCNode class296_sub7 = (NPCNode) Class41_Sub18.localNpcs.get(i_112_);
							if (class296_sub7 != null) {
								class338_sub3_sub1_sub3 = class296_sub7.value;
							}
						}
						if (class338_sub3_sub1_sub3 != null) {
							Class280 class280 = class338_sub3_sub1_sub3.method3516(false);
							if (class280.anIntArrayArray2558 != null && class280.anIntArrayArray2558[i_99_] != null) {
								i_105_ -= class280.anIntArrayArray2558[i_99_][1];
							}
							if (class280.anIntArrayArray2561 != null && class280.anIntArrayArray2561[i_99_] != null) {
								i_105_ -= class280.anIntArrayArray2561[i_99_][1];
							}
						}
					}
					Class338_Sub3_Sub1_Sub2 class338_sub3_sub1_sub2 = new Class338_Sub3_Sub1_Sub2(i_104_, Class368_Sub15.anInt5515, Class368_Sub15.anInt5515, i_95_, i_96_, i_105_, Class29.anInt307 + i_107_, Class29.anInt307 + i_108_, i_109_, i_110_, i_102_, i_103_, i_106_, bool, i_99_);
					class338_sub3_sub1_sub2.method3491(i_100_, -i_106_ + aa_Sub1.method155(-1537652855, Class368_Sub15.anInt5515, i_100_, i_101_), Class29.anInt307 + i_107_, i_101_, -1926441266);
					Class72.aClass155_844.addLast((byte) 99, new Class296_Sub39_Sub18(class338_sub3_sub1_sub2));
				}
			} else if (subP == Class338_Sub8.aClass260_5255) {
				int i_113_ = buff.readUnsignedByteA();
				int i_114_ = (i_113_ >> 4 & 0x7) + Class10.anInt3537;
				int i_115_ = ModeWhere.anInt2352 + (i_113_ & 0x7);
				int i_116_ = buff.g4();
				int i_117_ = buff.readUnsignedByteS();
				int i_118_ = i_117_ >> 2;
				int i_119_ = i_117_ & 0x3;
				int i_120_ = Class304.anIntArray2731[i_118_];
				if (Class296_Sub39_Sub9.method2830((byte) 86, Class338_Sub2.mapLoadType) || i_114_ >= 0 && i_115_ >= 0 && Class198.currentMapSizeX > i_114_ && Class296_Sub38.currentMapSizeY > i_115_) {
					Class268.method2302(i_120_, i_119_, 1024, Class368_Sub15.anInt5515, i_115_, i_114_, i_116_, i_118_);
				}
			} else if (subP == Class127_Sub1.aClass260_4282) {
				int i_121_ = buff.readUnsignedLEShort();
				if (i_121_ == 65535) {
					i_121_ = -1;
				}
				int i_122_ = buff.readUnsignedByteS();
				int i_123_ = (i_122_ >> 4 & 0x7) + Class10.anInt3537;
				int i_124_ = ModeWhere.anInt2352 + (i_122_ & 0x7);
				int i_125_ = buff.readUnsignedByteA();
				int i_126_ = i_125_ >> 2;
				int i_127_ = i_125_ & 0x3;
				int i_128_ = Class304.anIntArray2731[i_126_];
				Class314.method3320(i_126_, i_124_, -106, i_127_, i_128_, Class368_Sub15.anInt5515, i_123_, i_121_);
			} else if (subP == Class56.aClass260_665) {
				int settings = buff.g1();
				boolean bool = (settings & 0x80) != 0;
				int start_x = Class10.anInt3537 + ((settings & 0x3c) >> 3);
				int start_y = ModeWhere.anInt2352 + (settings & 0x7);
				int end_x = buff.g1b() + start_x;
				int end_y = start_y + buff.g1b();
				int lockon = buff.g2b();
				int spotanim = buff.g2();
				int start_height = buff.g1() * 4;
				int end_Height = buff.g1() * 4;
				int delay = buff.g2();
				int duration = buff.g2();
				int angle = buff.g1();
				int slope = buff.g2();
				if (angle == 255) {
					angle = -1;
				}
				if (start_x >= 0 && start_y >= 0 && start_x < Class198.currentMapSizeX && Class296_Sub38.currentMapSizeY > start_y && end_x >= 0 && end_y >= 0 && Class198.currentMapSizeX > end_x && Class296_Sub38.currentMapSizeY > end_y && spotanim != 65535) {
					start_height <<= 2;
					end_Height <<= 2;
					end_x = end_x * 512 + 256;
					start_y = start_y * 512 + 256;
					start_x = start_x * 512 + 256;
					slope <<= 2;
					end_y = end_y * 512 + 256;
					Class338_Sub3_Sub1_Sub2 class338_sub3_sub1_sub2 = new Class338_Sub3_Sub1_Sub2(spotanim, Class368_Sub15.anInt5515, Class368_Sub15.anInt5515, start_x, start_y, start_height, delay + Class29.anInt307, Class29.anInt307 + duration, angle, slope, 0, lockon, end_Height, bool, -1);
					class338_sub3_sub1_sub2.method3491(end_x, -end_Height + aa_Sub1.method155(-1537652855, Class368_Sub15.anInt5515, end_x, end_y), Class29.anInt307 + delay, end_y, -1926441266);
					Class72.aClass155_844.addLast((byte) 120, new Class296_Sub39_Sub18(class338_sub3_sub1_sub2));
				}
			} else {
				Class219_Sub1.method2062("T3 - " + subP, (byte) 77, null);
				StringNode.logout(3, false);
			}
		}
	}

	public static void method3332(int i) {
		aClass231_3702 = null;
		int i_142_ = 69 % ((33 - i) / 57);
		aClass311_3699 = null;
	}

	@Override
	public final int method79(int i) {
		if (aClass138_3701.method1431(aString3700)) {
			return 100;
		}
		if (i != 20667) {
			method3332(29);
		}
		return 0;
	}

	static final void playJingle(byte i, int i_143_, int i_144_, int i_145_) {
		i_143_ = i_143_ * Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5017.method489(125) >> 8;
		if (i_143_ != 0 && i_145_ != -1) {
			if (!Class124.aBoolean1282 && Class30.anInt318 != -1 && Class103.method898(-109) && !Class234.method2127(false)) {
				Class296_Sub20.aClass296_Sub45_Sub4_4719 = Class21.method289(-128);
				Class296_Sub45_Sub4 class296_sub45_sub4 = Class211.method2016(-24345, Class296_Sub20.aClass296_Sub45_Sub4_4719);
				Class123_Sub2.method1071(class296_sub45_sub4, true, true);
			}
			Class338_Sub8.method3602(0, Class296_Sub22.aClass138_4722, false, -126, i_145_, i_143_);
			Class200.method1952(64, -1, 255);
			Class124.aBoolean1282 = true;
		}
	}

	Class317(Js5 class138, String string) {
		aClass138_3701 = class138;
		aString3700 = string;
	}

	static {
		aClass231_3702 = new IncomingPacket(71, -1);
	}
}
