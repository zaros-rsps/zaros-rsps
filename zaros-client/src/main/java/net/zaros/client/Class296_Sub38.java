package net.zaros.client;
/* Class296_Sub38 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Calendar;
import java.util.TimeZone;

final class Class296_Sub38 extends Node {
	static String aString4886;
	int anInt4887 = -2147483648;
	int anInt4888 = 2147483647;
	static Calendar aCalendar4889;
	int anInt4890;
	static int currentMapSizeY = 104;
	Class296_Sub53 aClass296_Sub53_4892;
	int anInt4893 = -2147483648;
	int anInt4894;
	int anInt4895;
	int anInt4896;
	int anInt4897 = 2147483647;
	static Calendar aCalendar4898;
	static int anInt4899;
	static int anInt4900;

	final boolean method2773(int i, int i_0_, int i_1_) {
		if (i_1_ != -1728)
			method2773(-117, 88, 54);
		if (i >= anInt4897 && anInt4890 >= i && i_0_ >= anInt4896 && anInt4894 >= i_0_)
			return true;
		if (i >= anInt4888 && anInt4887 >= i && i_0_ >= anInt4895 && i_0_ <= anInt4893)
			return true;
		return false;
	}

	static final boolean method2774(byte i, int i_2_) {
		for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeFirst((byte) 119); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeNext(1001)) {
			if (Class50.method609(true, class296_sub39_sub9.anInt6165) && (long) i_2_ == class296_sub39_sub9.aLong6162)
				return true;
		}
		int i_3_ = 9 % ((2 - i) / 45);
		return false;
	}

	public static void method2775(byte i) {
		aString4886 = null;
		aCalendar4898 = null;
		int i_4_ = -2 % ((-51 - i) / 45);
		aCalendar4889 = null;
	}

	Class296_Sub38(Class296_Sub53 class296_sub53) {
		anInt4890 = -2147483648;
		anInt4894 = -2147483648;
		anInt4896 = 2147483647;
		anInt4895 = 2147483647;
		aClass296_Sub53_4892 = class296_sub53;
	}

	static {
		aCalendar4889 = Calendar.getInstance();
		aCalendar4898 = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		anInt4899 = 0;
		anInt4900 = -60;
	}
}
