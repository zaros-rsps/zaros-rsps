package net.zaros.client;
/* Class252 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;
import java.net.Socket;

final class Class252 {
	static double aDouble2381;
	static int anInt2382;
	static Class398 aClass398_2383;
	static IncomingPacket aClass231_2384;
	static int anInt2385;
	static int anInt2386 = -1;

	static final void method2202(byte i) {
		if (i != -35)
			method2206(91);
		for (int i_0_ = 0; i_0_ < Class41_Sub18.aByteArrayArrayArray3786.length; i_0_++) {
			for (int i_1_ = 0; i_1_ < Class41_Sub18.aByteArrayArrayArray3786[0].length; i_1_++) {
				for (int i_2_ = 0; (Class41_Sub18.aByteArrayArrayArray3786[0][0].length > i_2_); i_2_++)
					Class41_Sub18.aByteArrayArrayArray3786[i_0_][i_1_][i_2_] = (byte) 0;
			}
		}
	}

	public Class252() {
		/* empty */
	}

	static final void method2203(int i, int i_3_, int i_4_) {
		if (Class338_Sub3_Sub1_Sub1.method3481(-95)) {
			Class220.anInt2150 = i;
			int i_5_ = 109 % ((i_4_ - 47) / 59);
			if (Class296_Sub51_Sub14.anInt6423 != i_3_)
				Class384.aString3252 = "";
			Class296_Sub51_Sub14.anInt6423 = i_3_;
			Class41_Sub8.method422(1, 6);
		}
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	final int method2204(int i, int i_6_, int i_7_) {
		int i_8_ = Class241.anInt2301 <= i_7_ ? i_7_ : Class241.anInt2301;
		if (i_6_ != 23236)
			return 17;
		if (this == Class296_Sub24.aClass252_4760)
			return 0;
		if (this == Class218.aClass252_2124)
			return i_8_ - i;
		if (ReferenceTable.aClass252_975 == this)
			return (-i + i_8_) / 2;
		return 0;
	}

	public static void method2205(byte i) {
		aClass398_2383 = null;
		if (i >= -42)
			aDouble2381 = 0.04707312296075525;
		aClass231_2384 = null;
	}

	static final void method2206(int i) {
		if (Class397_Sub3.anInt5799 != 0) {
			try {
				if (++Class41_Sub28.anInt3817 > 2000) {
					Class296_Sub45_Sub2.aClass204_6276.method1966(320);
					if (Class162.anInt3555 >= 2) {
						Class368_Sub4.anInt5441 = -5;
						Class397_Sub3.anInt5799 = 0;
						return;
					}
					Class338_Sub3_Sub3_Sub1.lobbyWorld.method978((byte) 120);
					Class397_Sub3.anInt5799 = 1;
					Class41_Sub28.anInt3817 = 0;
					Class162.anInt3555++;
				}
				if (Class397_Sub3.anInt5799 == 1) {
					Class296_Sub45_Sub2.aClass204_6276.aClass278_2047 = Class338_Sub3_Sub3_Sub1.lobbyWorld.createSocket(43594, aClass398_2383);
					Class397_Sub3.anInt5799 = 2;
				}
				if (Class397_Sub3.anInt5799 == i) {
					if ((Class296_Sub45_Sub2.aClass204_6276.aClass278_2047.anInt2540) == 2)
						throw new IOException();
					if ((Class296_Sub45_Sub2.aClass204_6276.aClass278_2047.anInt2540) != 1)
						return;
					Class296_Sub45_Sub2.aClass204_6276.aClass154_2045 = Class41_Sub11.method436(15000, -1, (Socket) (Class296_Sub45_Sub2.aClass204_6276.aClass278_2047.anObject2539));
					Class296_Sub45_Sub2.aClass204_6276.aClass278_2047 = null;
					Class296_Sub45_Sub2.aClass204_6276.method1963(true);
					Class397_Sub3.anInt5799 = 4;
				}
				if (Class397_Sub3.anInt5799 == 4) {
					if (Class296_Sub45_Sub2.aClass204_6276.aClass154_2045.method1556(true, 1)) {
						Class296_Sub45_Sub2.aClass204_6276.aClass154_2045.method1559((byte) -118, 0, (Class296_Sub45_Sub2.aClass204_6276.in_stream.data), 1);
						int i_9_ = ((Class296_Sub45_Sub2.aClass204_6276.in_stream.data[0]) & 0xff);
						Class368_Sub4.anInt5441 = i_9_;
						Class397_Sub3.anInt5799 = 0;
						Class296_Sub45_Sub2.aClass204_6276.method1966(320);
					}
				}
			} catch (IOException ioexception) {
				Class296_Sub45_Sub2.aClass204_6276.method1966(i ^ 0x142);
				if (Class162.anInt3555 >= 2) {
					Class368_Sub4.anInt5441 = -4;
					Class397_Sub3.anInt5799 = 0;
				} else {
					Class338_Sub3_Sub3_Sub1.lobbyWorld.method978((byte) 125);
					Class162.anInt3555++;
					Class41_Sub28.anInt3817 = 0;
					Class397_Sub3.anInt5799 = 1;
				}
			}
		}
	}

	static {
		aDouble2381 = -1.0;
		aClass231_2384 = new IncomingPacket(50, -1);
	}
}
