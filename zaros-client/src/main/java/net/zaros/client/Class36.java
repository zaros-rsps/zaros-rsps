package net.zaros.client;
/* Class36 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

final class Class36 implements Runnable {
	private int anInt344 = 0;
	private OutputStream anOutputStream345;
	private InputStream anInputStream346;
	private int anInt347;
	private Class398 aClass398_348;
	private int anInt349;
	private byte[] aByteArray350;
	private boolean aBoolean351 = false;
	private boolean aBoolean352 = false;
	static int anInt353;
	private Socket aSocket354;
	private Class278 aClass278_355;
	static int anInt356;

	protected final void finalize() {
		method361(false);
	}

	final int method354(boolean bool) throws IOException {
		if (bool)
			method359(78);
		if (aBoolean351)
			return 0;
		return anInputStream346.available();
	}

	final void method355(int i, int i_0_, int i_1_, byte[] is) throws IOException {
		if (!aBoolean351) {
			while (i_1_ > 0) {
				int i_2_ = anInputStream346.read(is, i_0_, i_1_);
				if (i_2_ <= 0)
					throw new EOFException();
				i_1_ -= i_2_;
				i_0_ += i_2_;
			}
			if (i != 1163625227)
				aBoolean351 = false;
		}
	}

	public final void run() {
		try {
			for (;;) {
				int i;
				int i_3_;
				synchronized (this) {
					if (anInt344 == anInt347) {
						if (aBoolean351)
							break;
						try {
							this.wait();
						} catch (InterruptedException interruptedexception) {
							/* empty */
						}
					}
					if (anInt347 <= anInt344)
						i = -anInt347 + anInt344;
					else
						i = -anInt347 + anInt349;
					i_3_ = anInt347;
				}
				if (i > 0) {
					try {
						anOutputStream345.write(aByteArray350, i_3_, i);
					} catch (IOException ioexception) {
						aBoolean352 = true;
					}
					anInt347 = (i + anInt347) % anInt349;
					try {
						if (anInt344 == anInt347)
							anOutputStream345.flush();
					} catch (IOException ioexception) {
						aBoolean352 = true;
					}
				}
			}
			try {
				if (anInputStream346 != null)
					anInputStream346.close();
				if (anOutputStream345 != null)
					anOutputStream345.close();
				if (aSocket354 != null)
					aSocket354.close();
			} catch (IOException ioexception) {
				/* empty */
			}
			aByteArray350 = null;
		} catch (Exception exception) {
			Class219_Sub1.method2062(null, (byte) -128, exception);
		}
	}

	final void method356(byte i) throws IOException {
		if (i < 38)
			method359(-110);
		if (!aBoolean351) {
			if (aBoolean352) {
				aBoolean352 = false;
				throw new IOException();
			}
		}
	}

	static final void method357(int i) {
		int i_4_ = ParticleEmitterRaw.aByteArrayArray1772.length;
		int i_5_ = 8 / ((-28 - i) / 39);
		for (int i_6_ = 0; i_6_ < i_4_; i_6_++) {
			if (ParticleEmitterRaw.aByteArrayArray1772[i_6_] != null) {
				int i_7_ = -1;
				for (int i_8_ = 0; Animator.anInt427 > i_8_; i_8_++) {
					if (Class16_Sub1_Sub1.anIntArray5802[i_8_] == Class296_Sub43.anIntArray4940[i_6_]) {
						i_7_ = i_8_;
						break;
					}
				}
				if (i_7_ == -1) {
					Class16_Sub1_Sub1.anIntArray5802[Animator.anInt427] = Class296_Sub43.anIntArray4940[i_6_];
					i_7_ = Animator.anInt427++;
				}
				Packet class296_sub17 = new Packet(ParticleEmitterRaw.aByteArrayArray1772[i_6_]);
				int i_9_ = 0;
				while (class296_sub17.pos < ParticleEmitterRaw.aByteArrayArray1772[i_6_].length) {
					if (i_9_ >= 511 || Class367.npcsCount >= 1023)
						break;
					int i_10_ = i_9_++ << 6 | i_7_;
					int i_11_ = class296_sub17.g2();
					int i_12_ = i_11_ >> 14;
					int i_13_ = (i_11_ & 0x1fa2) >> 7;
					int i_14_ = i_11_ & 0x3f;
					int i_15_ = i_13_ + (-Class206.worldBaseX + (Class296_Sub43.anIntArray4940[i_6_] >> 8) * 64);
					int i_16_ = ((Class296_Sub43.anIntArray4940[i_6_] & 0xff) * 64 + (-Class41_Sub26.worldBaseY + i_14_));
					NPCDefinition class147 = (Class352.npcDefinitionLoader.getDefinition(class296_sub17.g2()));
					NPCNode class296_sub7 = (NPCNode) Class41_Sub18.localNpcs.get((long) i_10_);
					if (class296_sub7 == null && (class147.aByte1493 & 0x1) > 0 && i_12_ == NPCDefinitionLoader.anInt1401 && i_15_ >= 0 && class147.size + i_15_ < Class198.currentMapSizeX && i_16_ >= 0 && (i_16_ + class147.size < Class296_Sub38.currentMapSizeY)) {
						NPC class338_sub3_sub1_sub3_sub2 = new NPC();
						class338_sub3_sub1_sub3_sub2.index = i_10_;
						NPCNode class296_sub7_17_ = new NPCNode(class338_sub3_sub1_sub3_sub2);
						Class41_Sub18.localNpcs.put((long) i_10_, class296_sub7_17_);
						Class241.npcNodes[Class368_Sub23.npcNodeCount++] = class296_sub7_17_;
						ReferenceTable.npcIndexes[Class367.npcsCount++] = i_10_;
						class338_sub3_sub1_sub3_sub2.lastUpdate = Class29.anInt307;
						class338_sub3_sub1_sub3_sub2.setDefinition(class147);
						class338_sub3_sub1_sub3_sub2.setSize((class338_sub3_sub1_sub3_sub2.definition.size));
						class338_sub3_sub1_sub3_sub2.anInt6815 = (class338_sub3_sub1_sub3_sub2.definition.anInt1459) << 3;
						class338_sub3_sub1_sub3_sub2.method3517(true, true, (class338_sub3_sub1_sub3_sub2.definition.aByte1469) + 4 << 11 & 0x3a31);
						class338_sub3_sub1_sub3_sub2.updatePosition(i_16_, true, 65, class338_sub3_sub1_sub3_sub2.getSize(), i_12_, i_15_);
					}
				}
			}
		}
	}

	final void method358(int i, byte[] is, int i_18_, byte i_19_) throws IOException {
		if (!aBoolean351) {
			if (aBoolean352) {
				aBoolean352 = false;
				throw new IOException();
			}
			if (aByteArray350 == null)
				aByteArray350 = new byte[anInt349];
			synchronized (this) {
				if (i_19_ <= 115)
					method357(44);
				for (int i_20_ = 0; i_20_ < i_18_; i_20_++) {
					aByteArray350[anInt344] = is[i_20_ + i];
					anInt344 = (anInt344 + 1) % anInt349;
					if (anInt344 == (anInt347 + (anInt349 - 100)) % anInt349)
						throw new IOException();
				}
				if (aClass278_355 == null)
					aClass278_355 = aClass398_348.method4123(this, 3, -14396);
				this.notifyAll();
			}
		}
	}

	final void method359(int i) {
		if (!aBoolean351) {
			anInputStream346 = new InputStream_Sub2();
			if (i <= 34)
				method359(21);
			anOutputStream345 = new OutputStream_Sub2();
		}
	}

	Class36(Socket socket, Class398 class398, int i) throws IOException {
		anInt347 = 0;
		aSocket354 = socket;
		aClass398_348 = class398;
		aSocket354.setSoTimeout(30000);
		aSocket354.setTcpNoDelay(true);
		anInputStream346 = aSocket354.getInputStream();
		anOutputStream345 = aSocket354.getOutputStream();
		anInt349 = i;
	}

	final int method360(int i) throws IOException {
		if (i <= 12)
			return 81;
		if (aBoolean351)
			return 0;
		return anInputStream346.read();
	}

	final void method361(boolean bool) {
		if (!bool && !aBoolean351) {
			synchronized (this) {
				aBoolean351 = true;
				this.notifyAll();
			}
			if (aClass278_355 != null) {
				while (aClass278_355.anInt2540 == 0)
					Class106_Sub1.method942(1L, 0);
				if (aClass278_355.anInt2540 == 1) {
					try {
						((Thread) aClass278_355.anObject2539).join();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
			}
			aClass278_355 = null;
		}
	}
}
