package net.zaros.client;

/* Class370 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class370 {
	static boolean[][] aBooleanArrayArray3140;

	static final void method3872(boolean bool) {
		AdvancedMemoryCache.aClass113_1164.clear();
		if (bool != true)
			method3873(-11, 45);
	}

	static final synchronized byte[] method3873(int i, int i_0_) {
		if (i_0_ == i && Class149.anInt1523 > 0) {
			byte[] is = Class296_Sub1.aByteArrayArray4586[--Class149.anInt1523];
			Class296_Sub1.aByteArrayArray4586[Class149.anInt1523] = null;
			return is;
		}
		if (i_0_ == 5000 && Class203.anInt3564 > 0) {
			byte[] is = Class368_Sub10.aByteArrayArray5480[--Class203.anInt3564];
			Class368_Sub10.aByteArrayArray5480[Class203.anInt3564] = null;
			return is;
		}
		if (i_0_ == 30000 && Class41_Sub1.anInt3736 > 0) {
			byte[] is = (Class296_Sub39_Sub11.aByteArrayArray6187[--Class41_Sub1.anInt3736]);
			Class296_Sub39_Sub11.aByteArrayArray6187[Class41_Sub1.anInt3736] = null;
			return is;
		}
		if (aa_Sub1.aByteArrayArrayArray3721 != null) {
			for (int i_1_ = 0; i_1_ < Class76.anIntArray873.length; i_1_++) {
				if (Class76.anIntArray873[i_1_] == i_0_ && Class365.anIntArray3117[i_1_] > 0) {
					byte[] is = (aa_Sub1.aByteArrayArrayArray3721[i_1_][--Class365.anIntArray3117[i_1_]]);
					aa_Sub1.aByteArrayArrayArray3721[i_1_][Class365.anIntArray3117[i_1_]] = null;
					return is;
				}
			}
		}
		return new byte[i_0_];
	}

	static final synchronized void method3874(byte[] is, byte i) {
		if (is.length == 100 && Class149.anInt1523 < 1000)
			Class296_Sub1.aByteArrayArray4586[Class149.anInt1523++] = is;
		else {
			if (i != 125)
				method3875(125);
			if (is.length == 5000 && Class203.anInt3564 < 250)
				Class368_Sub10.aByteArrayArray5480[Class203.anInt3564++] = is;
			else if (is.length == 30000 && Class41_Sub1.anInt3736 < 50)
				Class296_Sub39_Sub11.aByteArrayArray6187[Class41_Sub1.anInt3736++] = is;
			else if (aa_Sub1.aByteArrayArrayArray3721 != null) {
				for (int i_2_ = 0; i_2_ < Class76.anIntArray873.length; i_2_++) {
					if (Class76.anIntArray873[i_2_] == is.length && (aa_Sub1.aByteArrayArrayArray3721[i_2_].length > Class365.anIntArray3117[i_2_])) {
						aa_Sub1.aByteArrayArrayArray3721[i_2_][Class365.anIntArray3117[i_2_]++] = is;
						break;
					}
				}
			}
		}
	}

	public static void method3875(int i) {
		aBooleanArrayArray3140 = null;
		if (i != -1)
			method3873(14, -86);
	}

	static final void method3876(int i, int i_3_, Animation class43) {
		if (Class296_Sub51_Sub1.anInt6335 < 50 && (class43 != null && class43.anIntArrayArray418 != null && i_3_ < class43.anIntArrayArray418.length && class43.anIntArrayArray418[i_3_] != null)) {
			int i_4_ = class43.anIntArrayArray418[i_3_][i];
			int i_5_ = i_4_ >> 8;
			if (class43.anIntArrayArray418[i_3_].length > 1) {
				int i_6_ = (int) (Math.random() * (double) (class43.anIntArrayArray418[i_3_]).length);
				if (i_6_ > 0)
					i_5_ = class43.anIntArrayArray418[i_3_][i_6_];
			}
			int i_7_ = i_4_ >> 5 & 0x7;
			int i_8_ = 256;
			if (class43.anIntArray415 != null && class43.anIntArray400 != null)
				i_8_ = Class220.method2072(class43.anIntArray400[i_3_], false, class43.anIntArray415[i_3_]);
			if (class43.aBoolean409)
				Class338_Sub3_Sub4.method3571(255, false, i_8_, 0, i_5_, 107, i_7_);
			else
				Class296_Sub51_Sub38.method3194(i - 106, i_5_, i_8_, 255, i_7_, 0);
		}
	}
}
