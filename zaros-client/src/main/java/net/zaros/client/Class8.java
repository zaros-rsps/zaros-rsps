package net.zaros.client;

/* Class8 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class8 {
	static IncomingPacket aClass231_94 = new IncomingPacket(136, 6);

	static final void method189(int i, int i_0_, int i_1_, int i_2_, ha var_ha, ObjectDefinition class70) {
		Class28 class28 = ConfigurationsLoader.aClass401_86.method4143(class70.anInt765, 24180);
		if (class28.anInt300 != -1) {
			if (class70.aBoolean793) {
				i_2_ += class70.anInt763;
				i_2_ &= 0x3;
			} else {
				i_2_ = 0;
			}
			Sprite class397 = class28.method315(var_ha, -22801, class70.aBoolean806, i_2_);
			if (class397 != null) {
				int i_3_ = class70.sizeX;
				int i_4_ = 20 / ((53 - i_0_) / 50);
				int i_5_ = class70.sizeY;
				if ((i_2_ & 0x1) == 1) {
					i_3_ = class70.sizeY;
					i_5_ = class70.sizeX;
				}
				int i_6_ = class397.method4099();
				int i_7_ = class397.method4088();
				if (class28.aBoolean298) {
					i_7_ = i_5_ * 4;
					i_6_ = i_3_ * 4;
				}
				if (class28.anInt303 != 0) {
					class397.method4083(i, i_1_ - (i_5_ - 1) * 4, i_6_, i_7_, 0, class28.anInt303 | ~0xffffff, 1);
				} else {
					class397.method4080(i, i_1_ + -(i_5_ * 4) + 4, i_6_, i_7_);
				}
			}
		}
	}

	public static void method190(int i) {
		aClass231_94 = null;
		if (i != -4) {
			aClass231_94 = null;
		}
	}

	public static int get_next_high_pow2(int base) {
		base = --base | base >>> 1;
		base |= base >>> 2;
		base |= base >>> 4;
		base |= base >>> 8;
		base |= base >>> 16;
		return base + 1;
	}

	static final int method192(byte i) {
		if (i != 101) {
			aClass231_94 = null;
		}
		if (Animator.aFrame435 != null) {
			return 3;
		}
		if (!Class236.aBoolean2231) {
			return 1;
		}
		return 2;
	}

	static final void method193(boolean bool, InterfaceComponent class51, int i, int i_9_) {
		if (class51.aByte626 != 0) {
			if (class51.aByte626 == 1) {
				class51.anInt614 = class51.locX + (i_9_ - class51.anInt578) / 2;
			} else if (class51.aByte626 != 2) {
				if (class51.aByte626 != 3) {
					if (class51.aByte626 != 4) {
						class51.anInt614 = -(class51.locX * i_9_ >> 14) + i_9_ - class51.anInt578;
					} else {
						class51.anInt614 = (i_9_ - class51.anInt578) / 2 + (i_9_ * class51.locX >> 14);
					}
				} else {
					class51.anInt614 = class51.locX * i_9_ >> 14;
				}
			} else {
				class51.anInt614 = -class51.anInt578 + i_9_ - class51.locX;
			}
		} else {
			class51.anInt614 = class51.locX;
		}
		if (bool) {
			aClass231_94 = null;
		}
		if (class51.aByte595 != 0) {
			if (class51.aByte595 == 1) {
				class51.anInt605 = (-class51.anInt623 + i) / 2 + class51.locY;
			} else if (class51.aByte595 != 2) {
				if (class51.aByte595 == 3) {
					class51.anInt605 = class51.locY * i >> 14;
				} else if (class51.aByte595 == 4) {
					class51.anInt605 = (i - class51.anInt623) / 2 + (i * class51.locY >> 14);
				} else {
					class51.anInt605 = -class51.anInt623 + i - (i * class51.locY >> 14);
				}
			} else {
				class51.anInt605 = -class51.anInt623 + i - class51.locY;
			}
		} else {
			class51.anInt605 = class51.locY;
		}
		do {
			if (BillboardRaw.aBoolean1071) {
				if (GameClient.method115(class51).settingsHash != 0 || class51.type == 0) {
					if (class51.anInt614 < 0) {
						class51.anInt614 = 0;
					} else if (class51.anInt614 + class51.anInt578 > i_9_) {
						class51.anInt614 = -class51.anInt578 + i_9_;
					}
					if (class51.anInt605 < 0) {
						class51.anInt605 = 0;
					} else {
						if (class51.anInt605 + class51.anInt623 <= i) {
							break;
						}
						class51.anInt605 = -class51.anInt623 + i;
					}
				}
				break;
			}
		} while (false);
	}
}
