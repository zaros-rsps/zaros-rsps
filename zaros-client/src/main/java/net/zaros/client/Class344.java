package net.zaros.client;

/* Class344 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class344 {
	static int anInt3007 = 0;
	static String aString3008;

	static final int method3653(boolean bool, int i, int i_0_, int i_1_) {
		if (i_0_ == i_1_)
			return i_0_;
		int i_2_ = -i + 128;
		int i_3_ = (i_1_ & 0x7f) * i + (i_0_ & 0x7f) * i_2_ >> 7;
		int i_4_ = (i_0_ & 0x380) * i_2_ + (i_1_ & 0x380) * i >> 7;
		if (bool)
			anInt3007 = -27;
		int i_5_ = (i_0_ & 0xfc00) * i_2_ + (i_1_ & 0xfc00) * i >> 7;
		return i_5_ & 0xfc00 | i_4_ & 0x380 | i_3_ & 0x7f;
	}

	public static void method3654(int i) {
		aString3008 = null;
		if (i != 896)
			aString3008 = null;
	}

	static final void method3655(int i, int i_6_, int i_7_, int i_8_, int i_9_) {
		if (i_7_ != 32734)
			method3655(122, -118, -87, 18, -110);
		int i_10_ = Class401.anInt3366;
		int i_11_ = Class100.anInt1068;
		if (Class368_Sub5_Sub2.aBoolean6597) {
			i_10_ += Class387.method4034(true);
			i_11_ += GraphicsLoader.method2286(true);
		}
		if (Class183.anInt1880 == 1) {
			Sprite class397 = (Class379_Sub1.aClass397Array5671[Class41_Sub14.anInt3775 / 100]);
			class397.method4096(i_10_ - 8, i_11_ - 8);
			Class368_Sub8.method3838(false, i_10_ - 8, i_11_ - 8, i_11_ + (-8 + class397.method4088()), i_10_ - 8 + class397.method4099());
		}
		if (Class183.anInt1880 == 2) {
			Sprite class397 = (Class379_Sub1.aClass397Array5671[Class41_Sub14.anInt3775 / 100 + 4]);
			class397.method4096(i_10_ - 8, i_11_ - 8);
			Class368_Sub8.method3838(false, i_10_ - 8, i_11_ - 8, i_11_ + class397.method4088() - 8, i_10_ - (8 - class397.method4099()));
		}
		Class151.method1541(-110);
	}
}
