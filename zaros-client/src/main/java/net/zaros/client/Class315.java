package net.zaros.client;

/* Class315 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class315 implements Runnable {
	private Queue aClass145_2788 = new Queue();
	static boolean aBoolean2789 = true;
	int anInt2790 = 0;
	private boolean aBoolean2791 = false;
	private Thread aThread2792;

	static final void method3322(int i) {
		for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeFirst((byte) 115); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeNext(1001)) {
			if (Class338.method3435(class296_sub39_sub9.anInt6165, true))
				AnimationsLoader.method3297(26036, class296_sub39_sub9);
		}
		if (i >= -88)
			method3322(113);
	}

	public final void run() {
		while (!aBoolean2791) {
			Class296_Sub39_Sub20_Sub2 class296_sub39_sub20_sub2;
			synchronized (aClass145_2788) {
				class296_sub39_sub20_sub2 = ((Class296_Sub39_Sub20_Sub2) aClass145_2788.remove());
				if (class296_sub39_sub20_sub2 != null)
					anInt2790--;
				else {
					try {
						aClass145_2788.wait();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
					continue;
				}
			}
			try {
				if (class296_sub39_sub20_sub2.anInt6731 != 2) {
					if (class296_sub39_sub20_sub2.anInt6731 == 3)
						class296_sub39_sub20_sub2.aByteArray6730 = (class296_sub39_sub20_sub2.aClass168_6732.load(-98, (int) class296_sub39_sub20_sub2.queue_uid));
				} else
					class296_sub39_sub20_sub2.aClass168_6732.writeFile(class296_sub39_sub20_sub2.aByteArray6730, class296_sub39_sub20_sub2.aByteArray6730.length, (int) class296_sub39_sub20_sub2.queue_uid, (byte) 117);
			} catch (Exception exception) {
				Class219_Sub1.method2062(null, (byte) -125, exception);
			}
			class296_sub39_sub20_sub2.aBoolean6256 = false;
		}
	}

	final void method3323(byte i) {
		aBoolean2791 = true;
		if (i != 117)
			method3323((byte) -120);
		synchronized (aClass145_2788) {
			aClass145_2788.notifyAll();
		}
		try {
			aThread2792.join();
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
		aThread2792 = null;
	}

	final Class296_Sub39_Sub20_Sub2 method3324(Js5DiskStore class168, int i, byte[] is, int i_0_) {
		Class296_Sub39_Sub20_Sub2 class296_sub39_sub20_sub2 = new Class296_Sub39_Sub20_Sub2();
		class296_sub39_sub20_sub2.aBoolean6253 = false;
		class296_sub39_sub20_sub2.queue_uid = (long) i;
		class296_sub39_sub20_sub2.aClass168_6732 = class168;
		class296_sub39_sub20_sub2.anInt6731 = 2;
		class296_sub39_sub20_sub2.aByteArray6730 = is;
		if (i_0_ <= 104)
			return null;
		method3329((byte) 6, class296_sub39_sub20_sub2);
		return class296_sub39_sub20_sub2;
	}

	static final boolean method3325(byte i, int i_1_, int i_2_) {
		int i_3_ = 17 / ((i + 67) / 43);
		if ((i_2_ & 0x84080) == 0)
			return false;
		return true;
	}

	final Class296_Sub39_Sub20_Sub2 method3326(Js5DiskStore class168, int i, int i_4_) {
		Class296_Sub39_Sub20_Sub2 class296_sub39_sub20_sub2 = new Class296_Sub39_Sub20_Sub2();
		class296_sub39_sub20_sub2.anInt6731 = 1;
		synchronized (aClass145_2788) {
			if (i != -3)
				aThread2792 = null;
			for (Class296_Sub39_Sub20_Sub2 class296_sub39_sub20_sub2_5_ = ((Class296_Sub39_Sub20_Sub2) aClass145_2788.getFront()); class296_sub39_sub20_sub2_5_ != null; class296_sub39_sub20_sub2_5_ = ((Class296_Sub39_Sub20_Sub2) aClass145_2788.getNext())) {
				if (class296_sub39_sub20_sub2_5_.queue_uid == (long) i_4_ && class168 == class296_sub39_sub20_sub2_5_.aClass168_6732 && class296_sub39_sub20_sub2_5_.anInt6731 == 2) {
					class296_sub39_sub20_sub2.aByteArray6730 = class296_sub39_sub20_sub2_5_.aByteArray6730;
					class296_sub39_sub20_sub2.aBoolean6256 = false;
					return class296_sub39_sub20_sub2;
				}
			}
		}
		class296_sub39_sub20_sub2.aByteArray6730 = class168.load(-68, i_4_);
		class296_sub39_sub20_sub2.aBoolean6253 = true;
		class296_sub39_sub20_sub2.aBoolean6256 = false;
		return class296_sub39_sub20_sub2;
	}

	final Class296_Sub39_Sub20_Sub2 method3327(Js5DiskStore class168, int i, int i_6_) {
		Class296_Sub39_Sub20_Sub2 class296_sub39_sub20_sub2 = new Class296_Sub39_Sub20_Sub2();
		class296_sub39_sub20_sub2.aClass168_6732 = class168;
		class296_sub39_sub20_sub2.queue_uid = (long) i;
		class296_sub39_sub20_sub2.anInt6731 = i_6_;
		class296_sub39_sub20_sub2.aBoolean6253 = false;
		method3329((byte) -41, class296_sub39_sub20_sub2);
		return class296_sub39_sub20_sub2;
	}

	static final Class254 method3328(byte i, Packet class296_sub17) {
		int i_7_ = class296_sub17.g2();
		if (i != 67)
			aBoolean2789 = false;
		return new Class254(i_7_);
	}

	private final void method3329(byte i, Class296_Sub39_Sub20_Sub2 class296_sub39_sub20_sub2) {
		synchronized (aClass145_2788) {
			aClass145_2788.insert(class296_sub39_sub20_sub2, -2);
			anInt2790++;
			aClass145_2788.notifyAll();
		}
		int i_8_ = 77 / ((i - 51) / 43);
	}

	Class315(Class398 class398) {
		Class278 class278 = class398.method4123(this, 5, -14396);
		while (class278.anInt2540 == 0)
			Class106_Sub1.method942(10L, 0);
		if (class278.anInt2540 == 2)
			throw new RuntimeException();
		aThread2792 = (Thread) class278.anObject2539;
	}
}
