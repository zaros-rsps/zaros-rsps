package net.zaros.client;

/* Class319 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class319 implements Interface13 {
	int anInt3643;
	int[] anIntArray3644;
	float[] aFloatArray3645;
	static int anInt3646;
	int anInt3647;
	private Class238 aClass238_3648;
	private ha_Sub2 aHa_Sub2_3649;
	static int anInt3650 = 0;

	public final void method52(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, boolean bool, boolean bool_5_) {
		StaticMethods.method1603(bool_5_ ? aFloatArray3645 : null, i_3_, true, i_1_, anInt3647, i_4_, i_2_, i, i_0_, aHa_Sub2_3649.aClass296_Sub29_4069.anIntArray4810, bool ? anIntArray3644 : null, aHa_Sub2_3649.aClass296_Sub29_4069.anInt4815, bool_5_ ? aHa_Sub2_3649.aFloatArray4099 : null);
	}

	static final void method3338(int i) {
		Class296_Sub13.anInt4654 = i;
	}

	public final void method53(int i, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, boolean bool, boolean bool_11_) {
		StaticMethods.method1603(!bool_11_ ? null : aHa_Sub2_3649.aFloatArray4099, i_9_, true, i_7_, aHa_Sub2_3649.aClass296_Sub29_4069.anInt4815, i_10_, i_8_, i, i_6_, anIntArray3644, bool ? (aHa_Sub2_3649.aClass296_Sub29_4069.anIntArray4810) : null, anInt3647, bool_11_ ? aFloatArray3645 : null);
	}

	Class319(ha_Sub2 var_ha_Sub2, Sprite class397, Class238 class238) {
		aHa_Sub2_3649 = var_ha_Sub2;
		if (!(class397 instanceof Class397_Sub1_Sub3)) {
			if (class397 instanceof Class397_Sub1_Sub1) {
				Class397_Sub1_Sub1 class397_sub1_sub1 = (Class397_Sub1_Sub1) class397;
				anInt3647 = class397_sub1_sub1.anInt5758;
				anInt3643 = class397_sub1_sub1.anInt5756;
				anIntArray3644 = class397_sub1_sub1.anIntArray6690;
			} else
				throw new RuntimeException();
		} else {
			Class397_Sub1_Sub3 class397_sub1_sub3 = (Class397_Sub1_Sub3) class397;
			anInt3647 = class397_sub1_sub3.anInt5758;
			anIntArray3644 = class397_sub1_sub3.anIntArray6693;
			anInt3643 = class397_sub1_sub3.anInt5756;
		}
		if (class238 != null) {
			aClass238_3648 = class238;
			if (anInt3647 != aClass238_3648.anInt3631 || anInt3643 != aClass238_3648.anInt3632)
				throw new RuntimeException();
			aFloatArray3645 = aClass238_3648.aFloatArray3627;
		}
	}
}
