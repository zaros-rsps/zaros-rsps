package net.zaros.client;
/* Class24 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class ClotchesLoader {
	static int anInt273;
	Js5 aClass138_274;
	private AdvancedMemoryCache cachedClotches = new AdvancedMemoryCache(64);
	static IncomingPacket aClass231_276;
	static int anInt277 = -1;
	private Js5 clothesFS;
	static int anInt279;

	final void clearSoftReferences() {
		synchronized (cachedClotches) {
			cachedClotches.clearSoftReferences();
		}
	}

	final void clearCachedClothes() {
		synchronized (cachedClotches) {
			cachedClotches.clear();
		}
	}

	final void method298(int i, byte i_0_) {
		synchronized (cachedClotches) {
			cachedClotches.clean(i);
			int i_1_ = 31 / ((i_0_ + 11) / 48);
		}
	}

	static final boolean method299(int i, int i_2_) {
		int i_3_ = 11 / ((i - 15) / 62);
		if (i_2_ != 49 && i_2_ != 48 && i_2_ != 59 && i_2_ != 10 && i_2_ != 5 && i_2_ != 52 && i_2_ != 20)
			return false;
		return true;
	}

	final ClothDefinition getClothDefinition(int clothID) {
		ClothDefinition definition;
		synchronized (cachedClotches) {
			definition = (ClothDefinition) cachedClotches.get((long) clothID);
		}
		if (definition != null)
			return definition;
		byte[] data;
		synchronized (clothesFS) {
			data = clothesFS.getFile(3, clothID);
		}
		definition = new ClothDefinition();
		definition.loader = this;
		if (data != null)
			definition.init(new Packet(data));
		synchronized (cachedClotches) {
			cachedClotches.put(definition, (long) clothID);
		}
		return definition;
	}

	public static void method301(int i) {
		int i_5_ = -93 % ((16 - i) / 63);
		aClass231_276 = null;
	}

	static final int method302(char c, byte i, String string) {
		try {
			int i_6_ = 0;
			int i_7_ = string.length();
			if (i < 31)
				return 115;
			for (int i_8_ = 0; i_7_ > i_8_; i_8_++) {
				if (string.charAt(i_8_) == c)
					i_6_++;
			}
			return i_6_;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, ("bea.F(" + c + ',' + i + ',' + (string != null ? "{...}" : "null") + ')'));
		}
	}

	static final int method303(int i, int i_9_, byte[] is, int i_10_) {
		if (i != 24443)
			anInt277 = 57;
		int i_11_ = -1;
		for (int i_12_ = i_10_; i_9_ > i_12_; i_12_++)
			i_11_ = (i_11_ >>> 8 ^ Class270.anIntArray2505[(i_11_ ^ is[i_12_]) & 0xff]);
		i_11_ ^= 0xffffffff;
		return i_11_;
	}

	ClotchesLoader(GameType class35, int i, Js5 class138, Js5 class138_13_) {
		aClass138_274 = class138_13_;
		clothesFS = class138;
		clothesFS.getLastFileId(3);
	}

	static {
		anInt273 = 0;
		aClass231_276 = new IncomingPacket(27, 16);
		anInt279 = -1;
	}
}
