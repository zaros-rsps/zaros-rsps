package net.zaros.client;

/* Class189_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.AWTException;
import java.awt.Component;
import java.awt.Robot;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.SwingUtilities;



/**
 * Class189_Sub2.java
 * 
 * @edited by Chryonic for RedRune <br>
 *         Changes: Middle Mouse Camera Movement, Zooming
 * @date Aug 10, 2017
 */
final class Class189_Sub2 extends Class189 implements MouseListener, MouseMotionListener, MouseWheelListener {
	private NodeDeque aClass155_4527 = new NodeDeque();
	private int anInt4528;
	private int anInt4529;
	private int anInt4530;
	private NodeDeque aClass155_4531 = new NodeDeque();
	private int anInt4532;
	private int anInt4533;
	private int anInt4534;
	private boolean aBoolean4535;
	private Component aComponent4536;
	public int mouseWheelX;
	public int mouseWheelY;
	Robot robot;
	private boolean first;
	private int lastX;
	private int lastY;
	private int firstY;
	private int firstX;

	private final void method1911(Component component, boolean bool) {
		method1913((byte) -39);
		aComponent4536 = component;
		if (!bool) {
			aComponent4536.addMouseListener(this);
			aComponent4536.addMouseMotionListener(this);
			aComponent4536.addMouseWheelListener(this);
		}
	}

	@Override
	public final synchronized void mouseDragged(MouseEvent mouseevent) {
		method1914(-104, mouseevent.getX(), mouseevent.getY());
		int x = mouseevent.getX();
		int y = mouseevent.getY();
		if (SwingUtilities.isMiddleMouseButton(mouseevent)) {
			Class257.method2236_static(CS2Script.anApplet6140, false);
			if (first) {
				try {
					robot = new Robot();
					robot.mouseMove(lastX, lastY);
					first = false;
				} catch (AWTException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			int accelX = this.mouseWheelX - x;
			int accelY = this.mouseWheelY - y;
			this.mouseWheelX = mouseevent.getX();
			this.mouseWheelY = mouseevent.getY();
			BillboardRaw.interfaceCounter();
			Class373_Sub1.method3925(-mouseWheelX, mouseWheelY, mouseWheelY, -30302);
			// Class296_Sub51_Sub32.method3178(false, accelX, true, mouseWheelY,
			// mouseWheelX, mouseWheelY, accelY);
			// GraphicDefinition.CAMERA_DIRECTION += accelX * 2;
			// Class3_Sub9.anInt2309 -= (accelY << 1);
			return;
		}
	}

	@Override
	public final synchronized void mouseEntered(MouseEvent mouseevent) {
		method1914(-116, mouseevent.getX(), mouseevent.getY());
	}

	@Override
	public final synchronized void mouseWheelMoved(MouseWheelEvent mousewheelevent) {
		int i = mousewheelevent.getX();
		int i_0_ = mousewheelevent.getY();
		int i_1_ = mousewheelevent.getWheelRotation();
		method1912(i_0_, 30135, i, i_1_, 6);
		mousewheelevent.consume();
	}

	@Override
	public final synchronized void mouseReleased(MouseEvent mouseevent) {
		if (mouseevent.getButton() == 2) {
			this.setLastX(mouseevent.getXOnScreen());
			this.setLastY(mouseevent.getYOnScreen());
			this.first = true;
			try {
				robot = new Robot();
				robot.mouseMove(firstX, firstY);
				first = false;
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// System.out.println(mouseevent.getButton());
		int i = method1915(2, mouseevent);
		if ((i & anInt4533) == 0) {
			i = anInt4533;
		}
		if ((i & 0x1) != 0) {
			method1912(mouseevent.getY(), 30135, mouseevent.getX(), mouseevent.getClickCount(), 3);
		}
		if ((i & 0x4) != 0) {
			method1912(mouseevent.getY(), 30135, mouseevent.getX(), mouseevent.getClickCount(), 5);
		}
		if ((i & 0x2) != 0) {
			method1912(mouseevent.getY(), 30135, mouseevent.getX(), mouseevent.getClickCount(), 4);
		}
		anInt4533 &= i ^ 0xffffffff;
		if (mouseevent.isPopupTrigger()) {
			mouseevent.consume();
		}
	}

	private void setLastY(int y) {
		this.lastY = y;

	}

	private void setLastX(int x) {
		this.lastX = x;

	}

	@Override
	public final synchronized void mouseClicked(MouseEvent mouseevent) {
		if (mouseevent.isPopupTrigger()) {
			mouseevent.consume();
		}
	}

	private final void method1912(int i, int i_2_, int i_3_, int i_4_, int i_5_) {
		if (i_2_ == 30135) {
			Class296_Sub34_Sub1 class296_sub34_sub1 = new Class296_Sub34_Sub1();
			class296_sub34_sub1.anInt6095 = i_5_;
			class296_sub34_sub1.anInt6092 = i;
			class296_sub34_sub1.anInt6094 = i_3_;
			class296_sub34_sub1.aLong6091 = Class72.method771(-119);
			class296_sub34_sub1.anInt6093 = i_4_;
			aClass155_4531.addLast((byte) -30, class296_sub34_sub1);
		}
	}

	@Override
	final synchronized void method1893(int i) {
		anInt4528 = anInt4534;
		anInt4529 = anInt4533;
		anInt4530 = anInt4532;
		NodeDeque class155 = aClass155_4527;
		if (i == 0) {
			aClass155_4527 = aClass155_4531;
			aClass155_4531 = class155;
			aClass155_4531.method1581(327680);
		}
	}

	@Override
	public final synchronized void mouseMoved(MouseEvent mouseevent) {
		method1914(-106, mouseevent.getX(), mouseevent.getY());
	}

	private final void method1913(byte i) {
		if (aComponent4536 != null) {
			aComponent4536.removeMouseWheelListener(this);
			aComponent4536.removeMouseMotionListener(this);
			aComponent4536.removeMouseListener(this);
			aClass155_4527 = null;
			if (i == -39) {
				anInt4528 = anInt4530 = anInt4529 = 0;
				aComponent4536 = null;
				aClass155_4531 = null;
				anInt4534 = anInt4532 = anInt4533 = 0;
			}
		}
	}

	@Override
	final Class296_Sub34 method1899(int i) {
		int i_6_ = -35 / ((i - 13) / 40);
		return (Class296_Sub34) aClass155_4527.method1573(1);
	}

	@Override
	final int method1897(int i) {
		if (i != 0) {
			method1895((byte) 41);
		}
		return anInt4530;
	}

	@Override
	public final synchronized void mouseExited(MouseEvent mouseevent) {
		method1914(-124, mouseevent.getX(), mouseevent.getY());
	}

	@Override
	final boolean method1901(byte i) {
		int i_7_ = -25 % ((-11 - i) / 55);
		return (anInt4529 & 0x1) != 0;
	}

	private final void method1914(int i, int i_8_, int i_9_) {
		anInt4532 = i_9_;
		anInt4534 = i_8_;
		if (i >= -97) {
			mouseEntered(null);
		}
		if (aBoolean4535) {
			method1912(i_9_, 30135, i_8_, 0, -1);
		}
	}

	@Override
	final boolean method1902(int i) {
		int i_10_ = -59 % ((i + 11) / 61);
		return (anInt4529 & 0x2) != 0;
	}

	@Override
	final void method1896(boolean bool) {
		if (bool) {
			method1892((byte) -68);
		}
		method1913((byte) -39);
	}

	@Override
	public final synchronized void mousePressed(MouseEvent mouseevent) {
		if (mouseevent.getButton() == 2) {
			this.setFirstX(mouseevent.getXOnScreen());
			this.setFirstY(mouseevent.getYOnScreen());
			// this.first = true;
			try {
				robot = new Robot();
				robot.mouseMove(lastX, lastY);
				first = false;
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int i = method1915(2, mouseevent);
		if (i == 1) {
			method1912(mouseevent.getY(), 30135, mouseevent.getX(), mouseevent.getClickCount(), 0);
		} else if (i == 4) {
			method1912(mouseevent.getY(), 30135, mouseevent.getX(), mouseevent.getClickCount(), 2);
		} else if (i == 2) {
			method1912(mouseevent.getY(), 30135, mouseevent.getX(), mouseevent.getClickCount(), 1);
		}
		anInt4533 |= i;
		if (mouseevent.isPopupTrigger()) {
			mouseevent.consume();
		}
	}

	private void setFirstY(int yOnScreen) {
		firstY = yOnScreen;

	}

	private void setFirstX(int xOnScreen) {
		firstX = xOnScreen;

	}

	@Override
	final int method1895(byte i) {
		if (i != -55) {
			mouseMoved(null);
		}
		return anInt4528;
	}

	@Override
	final boolean method1892(byte i) {
		if (i != -28) {
			mousePressed(null);
		}
		return (anInt4529 & 0x4) != 0;
	}

	private final int method1915(int i, MouseEvent mouseevent) {
		if (mouseevent.getButton() == 1) {
			if (mouseevent.isMetaDown()) {
				return 4;
			}
			return 1;
		}
		if (mouseevent.getButton() == 2) {
			return 2;
		}
		if (mouseevent.getButton() == 3) {
			return 4;
		}
		if (i != 2) {
			mouseEntered(null);
		}
		return 0;
	}

	Class189_Sub2(Component component, boolean bool) {
		method1911(component, false);
		aBoolean4535 = bool;
	}
}
