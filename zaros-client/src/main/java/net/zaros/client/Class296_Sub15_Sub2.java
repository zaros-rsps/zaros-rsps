package net.zaros.client;
import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;
import jagtheora.vorbis.DSPState;
import jagtheora.vorbis.VorbisBlock;
import jagtheora.vorbis.VorbisComment;
import jagtheora.vorbis.VorbisInfo;

final class Class296_Sub15_Sub2 extends Class296_Sub15 {
	private DSPState aDSPState6000;
	private int anInt6001;
	private VorbisComment aVorbisComment6002;
	private Class296_Sub45_Sub2 aClass296_Sub45_Sub2_6003;
	static int anInt6004 = 0;
	private VorbisBlock aVorbisBlock6005;
	static boolean[][] aBooleanArrayArray6006;
	private VorbisInfo aVorbisInfo6007 = new VorbisInfo();
	private Class288 aClass288_6008;
	private double aDouble6009;

	static final void method2528(int i, Js5 class138, int i_0_, boolean bool, long l, int i_1_, int i_2_) {
		AnimBase.method2670(l, 0, i_1_, i_2_, (byte) -79, i_0_, bool, class138);
		if (i != 2)
			anInt6004 = 7;
	}

	static final void method2529(int i, int i_3_) {
		Class139.anInt1421 = 100;
		Class41_Sub26.anInt3807 = i;
		Class325.anInt2868 = -1;
		if (i_3_ > -122)
			method2529(-12, -104);
		aa_Sub2.anInt3728 = 3;
	}

	Class296_Sub15_Sub2(OggStreamState oggstreamstate) {
		super(oggstreamstate);
		aVorbisComment6002 = new VorbisComment();
	}

	final void method2519(int i) {
		if (aVorbisBlock6005 != null)
			aVorbisBlock6005.b();
		if (aDSPState6000 != null)
			aDSPState6000.b();
		aVorbisComment6002.b();
		aVorbisInfo6007.b();
		if (i == 16) {
			if (aClass296_Sub45_Sub2_6003 != null)
				aClass296_Sub45_Sub2_6003.method2977(false);
		}
	}

	static final Class379_Sub1 method2530(Packet class296_sub17, int i) {
		Class379 class379 = Class296_Sub39_Sub1.method2784(class296_sub17, 0);
		int i_4_ = class296_sub17.g4();
		if (i != 2)
			method2529(11, -81);
		int i_5_ = class296_sub17.g4();
		return new Class379_Sub1(class379.aClass252_3616, class379.aClass357_3621, class379.anInt3623, class379.anInt3615, class379.anInt3613, class379.anInt3626, class379.anInt3619, class379.anInt3620, class379.anInt3617, i_4_, i_5_);
	}

	final int method2531(int i) {
		if (i != -2)
			aDSPState6000 = null;
		if (aClass296_Sub45_Sub2_6003 != null)
			return aClass296_Sub45_Sub2_6003.method2976(119);
		return 0;
	}

	final void method2517(OggPacket oggpacket, int i) {
		if (i != 16)
			aVorbisComment6002 = null;
		if (anInt4670 < 3) {
			int i_6_ = aVorbisInfo6007.headerIn(aVorbisComment6002, oggpacket);
			if (i_6_ < 0)
				throw new IllegalStateException(String.valueOf(i_6_));
			if (anInt4670 == 2) {
				if (aVorbisInfo6007.channels > 2 || aVorbisInfo6007.channels < 1)
					throw new RuntimeException(String.valueOf(aVorbisInfo6007.channels));
				aDSPState6000 = new DSPState(aVorbisInfo6007);
				aVorbisBlock6005 = new VorbisBlock(aDSPState6000);
				aClass288_6008 = new Class288(aVorbisInfo6007.rate, Class298.anInt2699);
				aClass296_Sub45_Sub2_6003 = new Class296_Sub45_Sub2(aVorbisInfo6007.channels);
			}
		} else {
			if (aVorbisBlock6005.synthesis(oggpacket) == 0)
				aDSPState6000.blockIn(aVorbisBlock6005);
			float[][] fs = aDSPState6000.pcmOut(aVorbisInfo6007.channels);
			aDouble6009 = aDSPState6000.granuleTime();
			if (aDouble6009 == -1.0)
				aDouble6009 = (double) ((float) anInt6001 / (float) aVorbisInfo6007.rate);
			aDSPState6000.read(fs[0].length);
			anInt6001 += fs[0].length;
			Class296_Sub39_Sub7 class296_sub39_sub7 = aClass296_Sub45_Sub2_6003.method2984(i + 1903774288, aDouble6009, fs[0].length);
			Class314.method3319(i ^ ~0x4f, fs, class296_sub39_sub7.aShortArrayArray6155);
			for (int i_7_ = 0; aVorbisInfo6007.channels > i_7_; i_7_++)
				class296_sub39_sub7.aShortArrayArray6155[i_7_] = aClass288_6008.method2393((byte) 73, (class296_sub39_sub7.aShortArrayArray6155[i_7_]));
			aClass296_Sub45_Sub2_6003.method2987((byte) -126, class296_sub39_sub7);
		}
	}

	final double method2532(byte i) {
		if (i != -32)
			return -0.9917770481274523;
		double d = aDouble6009;
		if (aClass296_Sub45_Sub2_6003 != null) {
			d = aClass296_Sub45_Sub2_6003.method2988((byte) 59);
			if (d < 0.0)
				d = aDouble6009;
		}
		return (double) -(256.0F / (float) Class298.anInt2699) + d;
	}

	final Class296_Sub45_Sub2 method2533(byte i) {
		if (i > -26)
			method2530(null, -95);
		return aClass296_Sub45_Sub2_6003;
	}

	public static void method2534(byte i) {
		if (i != -13)
			method2529(-92, -121);
		aBooleanArrayArray6006 = null;
	}
}
