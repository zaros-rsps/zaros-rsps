package net.zaros.client;

import jaggl.OpenGL;

public class Class366_Sub4 extends Class366 {
	static int anInt5375;
	private Class341 aClass341_5376;
	static Js5 aClass138_5377;
	private Class361 aClass361_5378;

	public static boolean method3779(String string, byte i) {
		return ObjectDefinitionLoader.method371(Class351.class, string, 65535);
	}

	void method3768(byte i, boolean bool) {
		aClass341_5376.method3625((byte) -118, '\0');
		if (aClass361_5378.aBoolean3102) {
			aHa_Sub3_3121.method1330(i ^ ~0x3c, 1);
			aHa_Sub3_3121.method1316(aClass361_5378.aClass69_Sub2_3098, (byte) -118);
			aHa_Sub3_3121.method1330(113, 0);
		}
		if (i != -88)
			method3764(false, 91, null);
	}

	boolean method3763(int i) {
		int i_0_ = -48 % ((73 - i) / 40);
		return true;
	}

	void method3764(boolean bool, int i, Class69 class69) {
		aHa_Sub3_3121.method1316(class69, (byte) -117);
		aHa_Sub3_3121.method1272((byte) -107, i);
		if (bool)
			method3780(true);
	}

	void method3770(byte i, boolean bool) {
		if (i != 33)
			aClass341_5376 = null;
	}

	Class366_Sub4(ha_Sub3 var_ha_Sub3, Class361 class361) {
		super(var_ha_Sub3);
		aClass361_5378 = class361;
		aClass341_5376 = new Class341(var_ha_Sub3, 2);
		aClass341_5376.method3627((byte) -114, 0);
		aHa_Sub3_3121.method1330(107, 1);
		if (aClass361_5378.aBoolean3102) {
			OpenGL.glTexGeni(8194, 9472, 9217);
			OpenGL.glEnable(3170);
		}
		OpenGL.glTexGeni(8192, 9472, 9216);
		OpenGL.glTexGeni(8193, 9472, 9216);
		OpenGL.glEnable(3168);
		OpenGL.glEnable(3169);
		aHa_Sub3_3121.method1330(115, 0);
		aClass341_5376.method3622(-125);
		aClass341_5376.method3627((byte) -90, 1);
		aHa_Sub3_3121.method1330(115, 1);
		if (aClass361_5378.aBoolean3102)
			OpenGL.glDisable(3170);
		OpenGL.glDisable(3168);
		OpenGL.glDisable(3169);
		aHa_Sub3_3121.method1330(123, 0);
		aClass341_5376.method3622(-123);
	}

	public static void method3780(boolean bool) {
		aClass138_5377 = null;
		if (bool != true)
			method3780(false);
	}

	void method3769(int i, byte i_1_, int i_2_) {
		float f = (float) ((i & 0x3) + 1) * -5.0E-4F;
		if (i_1_ == -81) {
			float f_3_ = (float) (((i & 0x1b) >> 3) + 1) * 5.0E-4F;
			float f_4_ = (i & 0x40) != 0 ? 9.765625E-4F : 4.8828125E-4F;
			aHa_Sub3_3121.method1330(i_1_ + 205, 1);
			boolean bool = (i & 0x80) != 0;
			if (!bool) {
				StaticMethods.aFloatArray6079[0] = 0.0F;
				StaticMethods.aFloatArray6079[2] = f_4_;
				StaticMethods.aFloatArray6079[3] = 0.0F;
				StaticMethods.aFloatArray6079[1] = 0.0F;
			} else {
				StaticMethods.aFloatArray6079[1] = 0.0F;
				StaticMethods.aFloatArray6079[0] = f_4_;
				StaticMethods.aFloatArray6079[3] = 0.0F;
				StaticMethods.aFloatArray6079[2] = 0.0F;
			}
			OpenGL.glTexGenfv(8192, 9474, StaticMethods.aFloatArray6079, 0);
			StaticMethods.aFloatArray6079[3] = f * (float) aHa_Sub3_3121.anInt4134 % 1.0F;
			StaticMethods.aFloatArray6079[1] = f_4_;
			StaticMethods.aFloatArray6079[2] = 0.0F;
			StaticMethods.aFloatArray6079[0] = 0.0F;
			OpenGL.glTexGenfv(8193, 9474, StaticMethods.aFloatArray6079, 0);
			if (aClass361_5378.aBoolean3102) {
				StaticMethods.aFloatArray6079[3] = (float) aHa_Sub3_3121.anInt4134 * f_3_ % 1.0F;
				StaticMethods.aFloatArray6079[2] = 0.0F;
				StaticMethods.aFloatArray6079[1] = 0.0F;
				StaticMethods.aFloatArray6079[0] = 0.0F;
				OpenGL.glTexGenfv(8194, 9473, StaticMethods.aFloatArray6079, 0);
			} else {
				int i_5_ = (int) (f_3_ * (float) aHa_Sub3_3121.anInt4134 * 16.0F);
				aHa_Sub3_3121.method1316((aClass361_5378.aClass69_Sub1Array3101[i_5_ % 16]), (byte) -106);
			}
			aHa_Sub3_3121.method1330(117, 0);
		}
	}

	public void method3766(int i) {
		aClass341_5376.method3625((byte) 50, '\001');
		aHa_Sub3_3121.method1330(110, 1);
		aHa_Sub3_3121.method1316(null, (byte) -111);
		if (i > 30)
			aHa_Sub3_3121.method1330(127, 0);
	}

}
