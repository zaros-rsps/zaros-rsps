package net.zaros.client;

/* Class122 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class122 implements Interface9 {
	Class379 aClass379_3543;
	private long aLong3544;
	private int anInt3545;
	Js5 aClass138_3546;
	private Js5 aClass138_3547;
	private Class55 aClass55_3548;
	static int anInt3549;
	static int anInt3550;
	public final void method43(boolean bool, int i) {
		int i_0_ = (aClass379_3543.aClass252_3616.method2204((aClass379_3543.anInt3613), 23236, (Class368_Sub7.anInt5463)) + aClass379_3543.anInt3623);
		int i_1_ = (aClass379_3543.aClass357_3621.method3711((Class296_Sub15_Sub1.anInt5996), (aClass379_3543.anInt3626), (byte) 121) + aClass379_3543.anInt3615);
		method1035(i_1_, bool, -24228, i_0_);
		method1036(bool, i_0_, i_1_, (byte) 80);
		String string = Class296_Sub51_Sub15.aClass54_6426.method651(true);
		int i_2_ = 82 % ((i + 32) / 52);
		if (Class72.method771(-112) - aLong3544 > 10000L)
			string += " (" + Class296_Sub51_Sub15.aClass54_6426.method645(116).method3386((byte) -44) + ")";
		aClass55_3548.a(15897, string, -1, aClass379_3543.anInt3617, i_0_ + aClass379_3543.anInt3613 / 2, (aClass379_3543.anInt3626 / 2 + i_1_ + aClass379_3543.anInt3619 + 4));
	}

	abstract void method1035(int i, boolean bool, int i_3_, int i_4_);

	abstract void method1036(boolean bool, int i, int i_5_, byte i_6_);

	public void method42(byte i) {
		Class92 class92 = Class259.method2240(aClass138_3547, (byte) 10, aClass379_3543.anInt3620);
		if (i != 88)
			aClass379_3543 = null;
		aClass55_3548 = Class41_Sub13.aHa3774.a(class92, Class186.method1868(aClass138_3546, (aClass379_3543.anInt3620)), true);
	}

	static final void method1037(int i) {
		if (i == 0) {
			if (Class377.anInt3190 == 2) {
				StaticMethods.aClass142Array5951[0].method1472(Class127.aClass299Array1301[0]);
				StaticMethods.aClass142Array5951[1].method1472(Class127.aClass299Array1301[1]);
			} else if (Class377.anInt3190 == 3) {
				StaticMethods.aClass142Array5951[0].method1472(Class127.aClass299Array1301[0]);
				StaticMethods.aClass142Array5951[1].method1472(Class127.aClass299Array1301[1]);
				StaticMethods.aClass142Array5951[2].method1472(Class127.aClass299Array1301[2]);
			} else {
				StaticMethods.aClass142Array5951[0].method1472(Class127.aClass299Array1301[0]);
				StaticMethods.aClass142Array5951[1].method1472(Class127.aClass299Array1301[1]);
				StaticMethods.aClass142Array5951[2].method1472(Class127.aClass299Array1301[2]);
				StaticMethods.aClass142Array5951[3].method1472(Class127.aClass299Array1301[3]);
			}
		} else if (i == 1) {
			if (Class377.anInt3190 == 2)
				StaticMethods.aClass142Array5951[0].method1472(Class127.aClass299Array1301[2]);
			else if (Class377.anInt3190 == 3) {
				StaticMethods.aClass142Array5951[0].method1472(Class127.aClass299Array1301[3]);
				StaticMethods.aClass142Array5951[1].method1472(Class127.aClass299Array1301[4]);
			} else {
				StaticMethods.aClass142Array5951[0].method1472(Class127.aClass299Array1301[4]);
				StaticMethods.aClass142Array5951[1].method1472(Class127.aClass299Array1301[5]);
				StaticMethods.aClass142Array5951[2].method1472(Class127.aClass299Array1301[6]);
			}
		} else if (i == 2) {
			if (Class377.anInt3190 == 2)
				StaticMethods.aClass142Array5951[0].method1472(Class127.aClass299Array1301[3]);
			else if (Class377.anInt3190 == 3)
				StaticMethods.aClass142Array5951[0].method1472(Class127.aClass299Array1301[5]);
			else
				StaticMethods.aClass142Array5951[0].method1472(Class127.aClass299Array1301[7]);
		}
	}

	static final void method1038(int i, Class338_Sub7 class338_sub7) {
		if (i != -16987)
			anInt3550 = 30;
		class338_sub7.aClass338_Sub3_Sub1_Sub3_5251 = null;
		if (Class241.anInt2300 < 20) {
			Class366_Sub5.aClass404_5386.method4158(class338_sub7, 1);
			Class241.anInt2300++;
		}
	}

	final int method1039(boolean bool) {
		if (bool != true)
			aClass55_3548 = null;
		int i = Class296_Sub51_Sub15.aClass54_6426.method654(!bool);
		int i_7_ = i * 100;
		if (anInt3545 == i && i != 0) {
			int i_8_ = Class296_Sub51_Sub15.aClass54_6426.method650(bool);
			if (i_8_ > i) {
				long l = (aLong3544 - Class296_Sub51_Sub15.aClass54_6426.method646(31244));
				if (l > 0L) {
					long l_9_ = l * 10000L / (long) i * (long) (-i + i_8_);
					long l_10_ = (Class72.method771(-120) + -aLong3544) * 10000L;
					if (l_10_ >= l_9_)
						i_7_ = i_8_ * 100;
					else
						i_7_ = (int) ((long) (i * 100) + ((long) (i_8_ - i) * 100L * l_10_ / l_9_));
				}
			}
		} else {
			anInt3545 = i;
			aLong3544 = Class72.method771(-108);
		}
		return i_7_;
	}

	Class122(Js5 class138, Js5 class138_11_, Class379 class379) {
		aClass379_3543 = class379;
		aClass138_3547 = class138_11_;
		aClass138_3546 = class138;
	}

	public boolean method44(byte i) {
		boolean bool = true;
		if (i < 91)
			aLong3544 = -48L;
		if (!aClass138_3546.hasEntryBuffer(aClass379_3543.anInt3620))
			bool = false;
		if (!aClass138_3547.hasEntryBuffer(aClass379_3543.anInt3620))
			bool = false;
		return bool;
	}

	static {
		anInt3550 = 0;
	}
}
