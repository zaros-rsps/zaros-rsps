package net.zaros.client;

/* Class296_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class296_Sub4 extends Node {
	public int anInt4597;
	public static int[] anIntArray4598 = new int[16384];
	Class242 aClass242_4599;
	int anInt4600;
	int anInt4601;
	int anInt4602;
	int anInt4603;
	int anInt4604;
	Class296_Sub19_Sub1 aClass296_Sub19_Sub1_4605;
	int anInt4606;
	Class296_Sub45_Sub1 aClass296_Sub45_Sub1_4607;
	int anInt4608;
	int anInt4609;
	Class296_Sub41 aClass296_Sub41_4610;
	int anInt4611;
	public int anInt4612;
	int anInt4613;
	public int anInt4614;
	public static int[] anIntArray4615;
	public int anInt4616;
	public int anInt4617;
	public static int[] anIntArray4618 = new int[16384];
	public int anInt4619;
	public int anInt4620;

	static final void method2442(int i, int i_0_) {
		if (i_0_ != 29464) {
			method2444(-26);
		}
		World.aClass113_1160.clean(i);
	}

	final void method2443(int i) {
		if (i > 40) {
			aClass296_Sub19_Sub1_4605 = null;
			aClass296_Sub41_4610 = null;
			aClass296_Sub45_Sub1_4607 = null;
			aClass242_4599 = null;
		}
	}

	public static void method2444(int i) {
		anIntArray4618 = null;
		anIntArray4598 = null;
		if (i == -18269) {
			anIntArray4615 = null;
		}
	}

	static {
		double d = 3.834951969714103E-4;
		for (int i = 0; i < 16384; i++) {
			anIntArray4598[i] = (int) (Math.sin(i * d) * 16384.0);
			anIntArray4618[i] = (int) (Math.cos(d * i) * 16384.0);
		}
	}
}
