package net.zaros.client;

/* Class296_Sub51_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class296_Sub51_Sub1 extends TextureOperation {
	static int anInt6335 = 0;
	private int anInt6336 = 6;
	static int[] mapSizes = { 104, 120, 136, 168 };

	@Override
	final int[][] get_colour_output(int i, int i_0_) {
		int[][] is = aClass86_5034.method823((byte) 48, i);
		while_28_: do {
			if (aClass86_5034.aBoolean939) {
				int[][] is_1_ = method3075((byte) 122, 0, i);
				int[][] is_2_ = method3075((byte) 110, 1, i);
				int[] is_3_ = is[0];
				int[] is_4_ = is[1];
				int[] is_5_ = is[2];
				int[] is_6_ = is_1_[0];
				int[] is_7_ = is_1_[1];
				int[] is_8_ = is_1_[2];
				int[] is_9_ = is_2_[0];
				int[] is_10_ = is_2_[1];
				int[] is_11_ = is_2_[2];
				int i_12_ = anInt6336;
				while_27_: do {
					while_26_: do {
						while_25_: do {
							while_24_: do {
								while_23_: do {
									while_22_: do {
										while_21_: do {
											while_20_: do {
												while_19_: do {
													do {
														if (i_12_ != 1) {
															if (i_12_ != 2) {
																if (i_12_ != 3) {
																	if (i_12_ != 4) {
																		if (i_12_ != 5) {
																			if (i_12_ != 6) {
																				if (i_12_ != 7) {
																					if (i_12_ != 8) {
																						if (i_12_ != 9) {
																							if (i_12_ != 10) {
																								if (i_12_ != 11) {
																									if (i_12_ != 12) {
																										break while_28_;
																									}
																								} else {
																									break while_26_;
																								}
																								break while_27_;
																							}
																						} else {
																							break while_24_;
																						}
																						break while_25_;
																					}
																				} else {
																					break while_22_;
																				}
																				break while_23_;
																			}
																		} else {
																			break while_20_;
																		}
																		break while_21_;
																	}
																} else {
																	break;
																}
																break while_19_;
															}
														} else {
															for (i_12_ = 0; Class41_Sub10.anInt3769 > i_12_; i_12_++) {
																is_3_[i_12_] = is_6_[i_12_] + is_9_[i_12_];
																is_4_[i_12_] = is_10_[i_12_] + is_7_[i_12_];
																is_5_[i_12_] = is_11_[i_12_] + is_8_[i_12_];
															}
															break while_28_;
														}
														for (i_12_ = 0; Class41_Sub10.anInt3769 > i_12_; i_12_++) {
															is_3_[i_12_] = -is_9_[i_12_] + is_6_[i_12_];
															is_4_[i_12_] = -is_10_[i_12_] + is_7_[i_12_];
															is_5_[i_12_] = is_8_[i_12_] - is_11_[i_12_];
														}
														break while_28_;
													} while (false);
													for (i_12_ = 0; i_12_ < Class41_Sub10.anInt3769; i_12_++) {
														is_3_[i_12_] = is_9_[i_12_] * is_6_[i_12_] >> 12;
														is_4_[i_12_] = is_7_[i_12_] * is_10_[i_12_] >> 12;
														is_5_[i_12_] = is_8_[i_12_] * is_11_[i_12_] >> 12;
													}
													break while_28_;
												} while (false);
												for (i_12_ = 0; Class41_Sub10.anInt3769 > i_12_; i_12_++) {
													int i_13_ = is_9_[i_12_];
													int i_14_ = is_10_[i_12_];
													int i_15_ = is_11_[i_12_];
													is_3_[i_12_] = i_13_ != 0 ? (is_6_[i_12_] << 12) / i_13_ : 4096;
													is_4_[i_12_] = i_14_ != 0 ? (is_7_[i_12_] << 12) / i_14_ : 4096;
													is_5_[i_12_] = i_15_ != 0 ? (is_8_[i_12_] << 12) / i_15_ : 4096;
												}
												break while_28_;
											} while (false);
											for (i_12_ = 0; Class41_Sub10.anInt3769 > i_12_; i_12_++) {
												is_3_[i_12_] = -((-is_9_[i_12_] + 4096) * (-is_6_[i_12_] + 4096) >> 12) + 4096;
												is_4_[i_12_] = -((4096 - is_10_[i_12_]) * (-is_7_[i_12_] + 4096) >> 12) + 4096;
												is_5_[i_12_] = 4096 - ((4096 - is_8_[i_12_]) * (4096 - is_11_[i_12_]) >> 12);
											}
											break while_28_;
										} while (false);
										for (i_12_ = 0; i_12_ < Class41_Sub10.anInt3769; i_12_++) {
											int i_16_ = is_9_[i_12_];
											int i_17_ = is_10_[i_12_];
											int i_18_ = is_11_[i_12_];
											is_3_[i_12_] = i_16_ < 2048 ? is_6_[i_12_] * i_16_ >> 11 : 4096 - ((-is_6_[i_12_] + 4096) * (-i_16_ + 4096) >> 11);
											is_4_[i_12_] = i_17_ < 2048 ? i_17_ * is_7_[i_12_] >> 11 : 4096 - ((-i_17_ + 4096) * (-is_7_[i_12_] + 4096) >> 11);
											is_5_[i_12_] = i_18_ >= 2048 ? 4096 - ((-is_8_[i_12_] + 4096) * (-i_18_ + 4096) >> 11) : i_18_ * is_8_[i_12_] >> 11;
										}
										break while_28_;
									} while (false);
									for (i_12_ = 0; i_12_ < Class41_Sub10.anInt3769; i_12_++) {
										int i_19_ = is_8_[i_12_];
										int i_20_ = is_7_[i_12_];
										int i_21_ = is_6_[i_12_];
										is_3_[i_12_] = i_21_ != 4096 ? (is_9_[i_12_] << 12) / (-i_21_ + 4096) : 4096;
										is_4_[i_12_] = i_20_ == 4096 ? 4096 : (is_10_[i_12_] << 12) / (-i_20_ + 4096);
										is_5_[i_12_] = i_19_ == 4096 ? 4096 : (is_11_[i_12_] << 12) / (-i_19_ + 4096);
									}
									break while_28_;
								} while (false);
								for (i_12_ = 0; i_12_ < Class41_Sub10.anInt3769; i_12_++) {
									int i_22_ = is_6_[i_12_];
									int i_23_ = is_8_[i_12_];
									int i_24_ = is_7_[i_12_];
									is_3_[i_12_] = i_22_ != 0 ? -((-is_9_[i_12_] + 4096 << 12) / i_22_) + 4096 : 0;
									is_4_[i_12_] = i_24_ != 0 ? 4096 - (-is_10_[i_12_] + 4096 << 12) / i_24_ : 0;
									is_5_[i_12_] = i_23_ != 0 ? -((4096 - is_11_[i_12_] << 12) / i_23_) + 4096 : 0;
								}
								break while_28_;
							} while (false);
							for (i_12_ = 0; Class41_Sub10.anInt3769 > i_12_; i_12_++) {
								int i_25_ = is_7_[i_12_];
								int i_26_ = is_9_[i_12_];
								int i_27_ = is_10_[i_12_];
								int i_28_ = is_8_[i_12_];
								int i_29_ = is_11_[i_12_];
								int i_30_ = is_6_[i_12_];
								is_3_[i_12_] = i_26_ > i_30_ ? i_30_ : i_26_;
								is_4_[i_12_] = i_25_ >= i_27_ ? i_27_ : i_25_;
								is_5_[i_12_] = i_28_ >= i_29_ ? i_29_ : i_28_;
							}
							break while_28_;
						} while (false);
						for (i_12_ = 0; Class41_Sub10.anInt3769 > i_12_; i_12_++) {
							int i_31_ = is_7_[i_12_];
							int i_32_ = is_10_[i_12_];
							int i_33_ = is_6_[i_12_];
							int i_34_ = is_8_[i_12_];
							int i_35_ = is_9_[i_12_];
							int i_36_ = is_11_[i_12_];
							is_3_[i_12_] = i_33_ <= i_35_ ? i_35_ : i_33_;
							is_4_[i_12_] = i_31_ <= i_32_ ? i_32_ : i_31_;
							is_5_[i_12_] = i_36_ >= i_34_ ? i_36_ : i_34_;
						}
						break while_28_;
					} while (false);
					for (i_12_ = 0; i_12_ < Class41_Sub10.anInt3769; i_12_++) {
						int i_37_ = is_8_[i_12_];
						int i_38_ = is_11_[i_12_];
						int i_39_ = is_7_[i_12_];
						int i_40_ = is_6_[i_12_];
						int i_41_ = is_10_[i_12_];
						int i_42_ = is_9_[i_12_];
						is_3_[i_12_] = i_42_ < i_40_ ? i_40_ - i_42_ : i_42_ - i_40_;
						is_4_[i_12_] = i_41_ >= i_39_ ? -i_39_ + i_41_ : i_39_ - i_41_;
						is_5_[i_12_] = i_38_ >= i_37_ ? -i_37_ + i_38_ : -i_38_ + i_37_;
					}
					break while_28_;
				} while (false);
				for (i_12_ = 0; i_12_ < Class41_Sub10.anInt3769; i_12_++) {
					int i_43_ = is_6_[i_12_];
					int i_44_ = is_8_[i_12_];
					int i_45_ = is_7_[i_12_];
					int i_46_ = is_11_[i_12_];
					int i_47_ = is_10_[i_12_];
					int i_48_ = is_9_[i_12_];
					is_3_[i_12_] = i_43_ + i_48_ - (i_43_ * i_48_ >> 11);
					is_4_[i_12_] = i_47_ + i_45_ - (i_47_ * i_45_ >> 11);
					is_5_[i_12_] = i_46_ + i_44_ - (i_44_ * i_46_ >> 11);
				}
			}
		} while (false);
		if (i_0_ != 17621) {
			return null;
		}
		return is;
	}

	public static void method3077(byte i) {
		mapSizes = null;
		if (i != -31) {
			fromJs5(null, 34, 123);
		}
	}

	public static Mesh fromJs5(Js5 class138, int i, int i_49_) {
		byte[] is = class138.getFile(i, i_49_);
		if (is == null) {
			return null;
		}
		return new Mesh(is);
	}

	public Class296_Sub51_Sub1() {
		super(2, false);
	}

	@Override
	final void method3071(int i, Packet class296_sub17, int i_50_) {
		if (i < -84) {
			int i_51_ = i_50_;
			do {
				if (i_51_ != 0) {
					if (i_51_ != 1) {
						break;
					}
				} else {
					anInt6336 = class296_sub17.g1();
					break;
				}
				monochromatic = class296_sub17.g1() == 1;
			} while (false);
		}
	}

	@Override
	final int[] get_monochrome_output(int i, int i_52_) {
		int[] is = aClass318_5035.method3335(i_52_, (byte) 28);
		while_38_: do {
			if (aClass318_5035.aBoolean2819) {
				int[] is_53_ = method3064(0, 0, i_52_);
				int[] is_54_ = method3064(1, i, i_52_);
				int i_55_ = anInt6336;
				while_37_: do {
					while_36_: do {
						while_35_: do {
							while_34_: do {
								while_33_: do {
									while_32_: do {
										while_31_: do {
											while_30_: do {
												while_29_: do {
													do {
														if (i_55_ != 1) {
															if (i_55_ != 2) {
																if (i_55_ != 3) {
																	if (i_55_ != 4) {
																		if (i_55_ != 5) {
																			if (i_55_ != 6) {
																				if (i_55_ != 7) {
																					if (i_55_ != 8) {
																						if (i_55_ != 9) {
																							if (i_55_ != 10) {
																								if (i_55_ != 11) {
																									if (i_55_ != 12) {
																										break while_38_;
																									}
																								} else {
																									break while_36_;
																								}
																								break while_37_;
																							}
																						} else {
																							break while_34_;
																						}
																						break while_35_;
																					}
																				} else {
																					break while_32_;
																				}
																				break while_33_;
																			}
																		} else {
																			break while_30_;
																		}
																		break while_31_;
																	}
																} else {
																	break;
																}
																break while_29_;
															}
														} else {
															for (i_55_ = 0; i_55_ < Class41_Sub10.anInt3769; i_55_++) {
																is[i_55_] = is_53_[i_55_] + is_54_[i_55_];
															}
															break while_38_;
														}
														for (i_55_ = 0; i_55_ < Class41_Sub10.anInt3769; i_55_++) {
															is[i_55_] = -is_54_[i_55_] + is_53_[i_55_];
														}
														break while_38_;
													} while (false);
													for (i_55_ = 0; i_55_ < Class41_Sub10.anInt3769; i_55_++) {
														is[i_55_] = is_54_[i_55_] * is_53_[i_55_] >> 12;
													}
													break while_38_;
												} while (false);
												for (i_55_ = 0; Class41_Sub10.anInt3769 > i_55_; i_55_++) {
													int i_56_ = is_54_[i_55_];
													is[i_55_] = i_56_ == 0 ? 4096 : (is_53_[i_55_] << 12) / i_56_;
												}
												break while_38_;
											} while (false);
											for (i_55_ = 0; i_55_ < Class41_Sub10.anInt3769; i_55_++) {
												is[i_55_] = -((-is_53_[i_55_] + 4096) * (-is_54_[i_55_] + 4096) >> 12) + 4096;
											}
											break while_38_;
										} while (false);
										for (i_55_ = 0; i_55_ < Class41_Sub10.anInt3769; i_55_++) {
											int i_57_ = is_54_[i_55_];
											is[i_55_] = i_57_ < 2048 ? i_57_ * is_53_[i_55_] >> 11 : -((-is_53_[i_55_] + 4096) * (-i_57_ + 4096) >> 11) + 4096;
										}
										break while_38_;
									} while (false);
									for (i_55_ = 0; Class41_Sub10.anInt3769 > i_55_; i_55_++) {
										int i_58_ = is_53_[i_55_];
										is[i_55_] = i_58_ != 4096 ? (is_54_[i_55_] << 12) / (-i_58_ + 4096) : 4096;
									}
									break while_38_;
								} while (false);
								for (i_55_ = 0; i_55_ < Class41_Sub10.anInt3769; i_55_++) {
									int i_59_ = is_53_[i_55_];
									is[i_55_] = i_59_ != 0 ? -((-is_54_[i_55_] + 4096 << 12) / i_59_) + 4096 : 0;
								}
								break while_38_;
							} while (false);
							for (i_55_ = 0; i_55_ < Class41_Sub10.anInt3769; i_55_++) {
								int i_60_ = is_54_[i_55_];
								int i_61_ = is_53_[i_55_];
								is[i_55_] = i_61_ < i_60_ ? i_61_ : i_60_;
							}
							break while_38_;
						} while (false);
						for (i_55_ = 0; i_55_ < Class41_Sub10.anInt3769; i_55_++) {
							int i_62_ = is_53_[i_55_];
							int i_63_ = is_54_[i_55_];
							is[i_55_] = i_62_ <= i_63_ ? i_63_ : i_62_;
						}
						break while_38_;
					} while (false);
					for (i_55_ = 0; Class41_Sub10.anInt3769 > i_55_; i_55_++) {
						int i_64_ = is_53_[i_55_];
						int i_65_ = is_54_[i_55_];
						is[i_55_] = i_65_ < i_64_ ? -i_65_ + i_64_ : i_65_ - i_64_;
					}
					break while_38_;
				} while (false);
				for (i_55_ = 0; i_55_ < Class41_Sub10.anInt3769; i_55_++) {
					int i_66_ = is_54_[i_55_];
					int i_67_ = is_53_[i_55_];
					is[i_55_] = i_66_ + i_67_ - (i_66_ * i_67_ >> 11);
				}
			}
		} while (false);
		if (i != 0) {
			return null;
		}
		return is;
	}

	static final void method3079(ObjectDefinition class70, int i, byte i_68_, int i_69_, int i_70_) {
		if (i_68_ != 13) {
			mapSizes = null;
		}
		for (Class296_Sub36 class296_sub36 = (Class296_Sub36) Class126.aClass155_1289.removeFirst((byte) 123); class296_sub36 != null; class296_sub36 = (Class296_Sub36) Class126.aClass155_1289.removeNext(i_68_ + 988)) {
			if (i_70_ == class296_sub36.anInt4861 && i_69_ << 9 == class296_sub36.anInt4880 && i << 9 == class296_sub36.anInt4879 && class70.ID == class296_sub36.aClass70_4877.ID) {
				if (class296_sub36.aClass296_Sub45_Sub1_4871 != null) {
					Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36.aClass296_Sub45_Sub1_4871);
					class296_sub36.aClass296_Sub45_Sub1_4871 = null;
				}
				if (class296_sub36.aClass296_Sub45_Sub1_4860 != null) {
					Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36.aClass296_Sub45_Sub1_4860);
					class296_sub36.aClass296_Sub45_Sub1_4860 = null;
				}
				class296_sub36.unlink();
				break;
			}
		}
	}

	static final void method3080(int i, int i_71_, int i_72_) {
		if (i_72_ >= -126) {
			anInt6335 = -84;
		}
		Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 111, GameClient.aClass311_3715);
		class296_sub1.out.writeInt_v2(i);
		class296_sub1.out.writeLEShortA(i_71_);
		Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
	}
}
