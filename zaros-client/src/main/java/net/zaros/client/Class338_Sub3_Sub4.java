package net.zaros.client;

/* Class338_Sub3_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class338_Sub3_Sub4 extends Class338_Sub3 {
	short aShort6570;

	final boolean method3465(int i) {
		if (i != 0)
			aShort6570 = (short) -54;
		return (Class296_Sub15_Sub2.aBooleanArrayArray6006[((tileX >> Class313.anInt2779) + (-Class296_Sub45_Sub2.anInt6288 + Class379_Sub2.anInt5684))][(Class379_Sub2.anInt5684 + ((tileY >> Class313.anInt2779) - Class296_Sub39_Sub20_Sub2.anInt6728))]);
	}

	static final int method3566(byte i) {
		if (i != 43)
			return 124;
		return (int) (1000000000L / Class296_Sub51_Sub32.aLong6513);
	}

	final int method3463(int i, Class296_Sub35[] class296_sub35s) {
		int i_0_ = tileX >> Class313.anInt2779;
		int i_1_ = tileY >> Class313.anInt2779;
		int i_2_ = 0;
		if (Class296_Sub45_Sub2.anInt6288 == i_0_)
			i_2_++;
		else if (i_0_ > Class296_Sub45_Sub2.anInt6288)
			i_2_ += 2;
		if (i_1_ != Class296_Sub39_Sub20_Sub2.anInt6728) {
			if (Class296_Sub39_Sub20_Sub2.anInt6728 > i_1_)
				i_2_ += 6;
		} else
			i_2_ += 3;
		int i_3_ = StaticMethods.anIntArray5967[i_2_];
		if ((aShort6570 & i_3_) != 0)
			return this.method3474(i_0_, class296_sub35s, i_1_, 4);
		if (aShort6570 == 1 && i_0_ > 0)
			return this.method3474(i_0_ - 1, class296_sub35s, i_1_, 4);
		if (aShort6570 == 4 && Class228.anInt2201 >= i_0_)
			return this.method3474(i_0_ + 1, class296_sub35s, i_1_, i + 5);
		if (aShort6570 == 8 && i_1_ > 0)
			return this.method3474(i_0_, class296_sub35s, i_1_ - 1, 4);
		if (aShort6570 == 2 && Class368_Sub12.anInt5488 >= i_1_)
			return this.method3474(i_0_, class296_sub35s, i_1_ + 1, 4);
		if (aShort6570 == 16 && i_0_ > 0 && i_1_ <= Class368_Sub12.anInt5488)
			return this.method3474(i_0_ - 1, class296_sub35s, i_1_ + 1, i + 5);
		if (aShort6570 == 32 && i_0_ <= Class228.anInt2201 && Class368_Sub12.anInt5488 >= i_1_)
			return this.method3474(i_0_ + 1, class296_sub35s, i_1_ + 1, 4);
		if (i != -1)
			return -120;
		if (aShort6570 == 128 && i_0_ > 0 && i_1_ > 0)
			return this.method3474(i_0_ - 1, class296_sub35s, i_1_ - 1, 4);
		if (aShort6570 == 64 && Class228.anInt2201 >= i_0_ && i_1_ > 0)
			return this.method3474(i_0_ + 1, class296_sub35s, i_1_ - 1, 4);
		throw new RuntimeException("");
	}

	static final void method3567(int i, int i_4_, int i_5_, Class375 class375, int i_6_, byte i_7_) {
		if (i_7_ == 80) {
			if (i_4_ >= 1 && i_5_ >= 1 && i_4_ <= Class198.currentMapSizeX - 2 && Class296_Sub38.currentMapSizeY - 2 >= i_5_) {
				if (Class338_Sub2.aClass247ArrayArrayArray5195 != null) {
					Interface14 interface14 = StaticMethods.aClass181_Sub1_5960.method1837(i, i_6_, -1, i_5_, i_4_);
					if (interface14 != null) {
						if (interface14 instanceof Class338_Sub3_Sub1_Sub4)
							((Class338_Sub3_Sub1_Sub4) interface14).method3544(0, class375);
						else if (!(interface14 instanceof Class338_Sub3_Sub5_Sub1)) {
							if (!(interface14 instanceof Class338_Sub3_Sub4_Sub1)) {
								if (interface14 instanceof Class338_Sub3_Sub3_Sub2)
									((Class338_Sub3_Sub3_Sub2) interface14).method3565(class375, (byte) -82);
							} else
								((Class338_Sub3_Sub4_Sub1) interface14).method3573(9418, class375);
						} else
							((Class338_Sub3_Sub5_Sub1) interface14).method3586(-125, class375);
					}
				}
			}
		}
	}

	static final void method3568(int i, boolean bool, int[] is, int i_8_, int[] is_9_) {
		if (i_8_ > i) {
			int i_10_ = (i_8_ + i) / 2;
			int i_11_ = i;
			int i_12_ = is_9_[i_10_];
			is_9_[i_10_] = is_9_[i_8_];
			is_9_[i_8_] = i_12_;
			int i_13_ = is[i_10_];
			is[i_10_] = is[i_8_];
			is[i_8_] = i_13_;
			int i_14_ = i_12_ != 2147483647 ? 1 : 0;
			for (int i_15_ = i; i_15_ < i_8_; i_15_++) {
				if (is_9_[i_15_] < (i_15_ & i_14_) + i_12_) {
					int i_16_ = is_9_[i_15_];
					is_9_[i_15_] = is_9_[i_11_];
					is_9_[i_11_] = i_16_;
					int i_17_ = is[i_15_];
					is[i_15_] = is[i_11_];
					is[i_11_++] = i_17_;
				}
			}
			is_9_[i_8_] = is_9_[i_11_];
			is_9_[i_11_] = i_12_;
			is[i_8_] = is[i_11_];
			is[i_11_] = i_13_;
			method3568(i, true, is, i_11_ - 1, is_9_);
			method3568(i_11_ + 1, true, is, i_8_, is_9_);
		}
		if (bool != true) {
			/* empty */
		}
	}

	static final boolean method3569(int i, int i_18_, byte i_19_) {
		if (i_19_ != -22)
			return true;
		return (Class244.method2178(i_18_, 0, i) & Class206.method1990(i, i_18_, (byte) 89));
	}

	Class338_Sub3_Sub4(int i, int i_20_, int i_21_, int i_22_, int i_23_, int i_24_) {
		aByte5203 = (byte) i_23_;
		tileX = i;
		anInt5213 = i_20_;
		aShort6570 = (short) i_24_;
		z = (byte) i_22_;
		tileY = i_21_;
	}

	final boolean method3470(int i, ha var_ha) {
		if (i != -30488)
			return true;
		return Class42_Sub3.method536(tileX >> Class313.anInt2779, i + 30564, tileY >> Class313.anInt2779, this, aByte5203);
	}

	static final void method3570(String string, byte i) {
		if (!string.equals("")) {
			if (i < 72)
				method3570(null, (byte) -115);
			Class122.anInt3549++;
			Connection class204 = Class296_Sub51_Sub13.method3111(true);
			Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 99, Class146.aClass311_1443);
			class296_sub1.out.p1(Class117.method1015((byte) -104, string));
			class296_sub1.out.writeString(string);
			class204.sendPacket(class296_sub1, (byte) 119);
		}
	}

	static final void method3571(int i, boolean bool, int i_25_, int i_26_, int i_27_, int i_28_, int i_29_) {
		if ((!bool ? Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4995.method489(127) : Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4998.method489(117)) != 0 && i_29_ != 0 && Class296_Sub51_Sub1.anInt6335 < 50 && i_27_ != -1)
			Class336.aClass391Array2956[Class296_Sub51_Sub1.anInt6335++] = new Class391(bool ? (byte) 3 : (byte) 2, i_27_, i_29_, i_26_, i, 0, i_25_, null);
		if (i_28_ <= 66)
			method3566((byte) -28);
	}
}
