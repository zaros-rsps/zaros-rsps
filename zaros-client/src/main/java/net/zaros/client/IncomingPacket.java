package net.zaros.client;
/* Class231 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class IncomingPacket {
	private int opcode;
	int length;
	static IncomingPacket CLANCHANNEL_FULL = new IncomingPacket(82, -2);
	static IncomingPacket CLANSETTINGS_DELTA = new IncomingPacket(32, -2);
	static IncomingPacket CLANSETTINGS_FULL = new IncomingPacket(99, -2);
	static IncomingPacket aClass231_5916 = new IncomingPacket(120, 6);
	public static IncomingPacket aClass231_1609;
	static IncomingPacket aClass231_4901;

	static {
		IncomingPacket.aClass231_4901 = new IncomingPacket(65, 3);
	}

	static {
		aClass231_1609 = new IncomingPacket(53, 0);
	}

	final int getOpcode() {
		return opcode;
	}

	static final Class296_Sub53 method2111(int i) {
		if (Class106.aClass155_1101 == null || Class405.aClass298_3389 == null)
			return null;
		Class405.aClass298_3389.method3233(Class106.aClass155_1101, 24459);
		if (i >= -29)
			method2111(39);
		Class296_Sub53 class296_sub53 = (Class296_Sub53) Class405.aClass298_3389.method3236((byte) 70);
		if (class296_sub53 == null)
			return null;
		Class18 class18 = Class106.aClass245_1094.method2179(11702, class296_sub53.anInt5046);
		if (class18 == null || !class18.aBoolean228 || !class18.method280(Class106.anInterface17_1090, true))
			return Class368_Sub4.method3819((byte) -78);
		return class296_sub53;
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	IncomingPacket(int i, int i_1_) {
		opcode = i;
		length = i_1_;
	}

	static final boolean method2112(int i, byte i_2_, int i_3_) {
		if (i_2_ > -6)
			method2111(-16);
		if (!(Class236.method2134(i, i_3_, false) | (i & 0x800) != 0) && !Class368_Sub8.method3836(i_3_, i, -1))
			return false;
		return true;
	}
}
