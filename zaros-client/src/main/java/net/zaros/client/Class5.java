package net.zaros.client;
/* Class5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.File;

final class Class5 {
	private int anInt75;
	static float[] aFloatArray76 = new float[16384];
	int anInt77;
	static float[] aFloatArray78 = new float[16384];
	static int cameraDestZ;
	static int anInt80;
	static String aString81;
	static String aString82;

	final int method175(byte i) {
		if (i != -81)
			return -35;
		return anInt77 & 0x3fff;
	}

	public static void method176(int i) {
		aString81 = null;
		aString82 = null;
		if (i != -1)
			aFloatArray76 = null;
		aFloatArray76 = null;
		aFloatArray78 = null;
	}

	final void method177(byte i) {
		if (i != -68)
			method179((byte) 89, -74, -37, 116);
		anInt77 &= 0x3fff;
	}

	final void method178(int i, int i_0_) {
		if (i != 2301)
			method179((byte) 24, 72, 63, 89);
		anInt75 = 0;
		anInt77 = i_0_;
	}

	final boolean method179(byte i, int i_1_, int i_2_, int i_3_) {
		int i_4_ = anInt75;
		if (anInt77 == i_2_ && anInt75 == 0)
			return false;
		boolean bool;
		if (anInt75 == 0) {
			if (i_2_ > anInt77 && anInt77 + i_3_ >= i_2_ || i_2_ < anInt77 && i_2_ >= -i_3_ + anInt77) {
				anInt77 = i_2_;
				return false;
			}
			bool = true;
		} else if (anInt75 > 0 && anInt77 < i_2_) {
			int i_5_ = anInt75 * anInt75 / (i_3_ * 2);
			int i_6_ = anInt77 + i_5_;
			if (i_2_ > i_6_ && i_6_ >= anInt77)
				bool = true;
			else
				bool = false;
		} else if (anInt75 >= 0 || i_2_ >= anInt77)
			bool = false;
		else {
			int i_7_ = anInt75 * anInt75 / (i_3_ * 2);
			int i_8_ = anInt77 - i_7_;
			if (i_8_ <= i_2_ || anInt77 < i_8_)
				bool = false;
			else
				bool = true;
		}
		if (!bool) {
			if (anInt75 <= 0) {
				anInt75 += i_3_;
				if (anInt75 > 0)
					anInt75 = 0;
			} else {
				anInt75 -= i_3_;
				if (anInt75 < 0)
					anInt75 = 0;
			}
		} else {
			if (i_2_ <= anInt77) {
				anInt75 -= i_3_;
				if (i_1_ != 0 && anInt75 < -i_1_)
					anInt75 = -i_1_;
			} else {
				anInt75 += i_3_;
				if (i_1_ != 0 && anInt75 > i_1_)
					anInt75 = i_1_;
			}
			if (anInt75 != i_4_) {
				int i_9_ = anInt75 * anInt75 / (i_3_ * 2);
				if (i_2_ > anInt77) {
					if (anInt77 + i_9_ > i_2_)
						anInt75 = i_4_;
				} else if (anInt77 > i_2_ && -i_9_ + anInt77 < i_2_)
					anInt75 = i_4_;
			}
		}
		anInt77 += anInt75 + i_4_ >> 1;
		int i_10_ = -125 % ((4 - i) / 63);
		return bool;
	}

	public Class5() {
		/* empty */
	}

	static {
		double d = 3.834951969714103E-4;
		for (int i = 0; i < 16384; i++) {
			aFloatArray76[i] = (float) Math.sin((double) i * d);
			aFloatArray78[i] = (float) Math.cos((double) i * d);
		}
		String string = "Unknown";
		try {
			string = System.getProperty("java.vendor").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		string.toLowerCase();
		string = "Unknown";
		try {
			string = System.getProperty("java.version").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		string.toLowerCase();
		string = "Unknown";
		try {
			string = System.getProperty("os.name").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		aString81 = string.toLowerCase();
		string = "Unknown";
		try {
			string = System.getProperty("os.arch").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		aString82 = string.toLowerCase();
		string = "Unknown";
		try {
			string = System.getProperty("os.version").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		string.toLowerCase();
		string = "~/";
		try {
			string = System.getProperty("user.home").toLowerCase();
		} catch (Exception exception) {
			/* empty */
		}
		new File(string);
	}
}
