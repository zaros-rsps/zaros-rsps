package net.zaros.client;
/* Class6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class ConfigurationsLoader {
	private Js5 fs;
	private AdvancedMemoryCache cachedConfigurations = new AdvancedMemoryCache(64);
	int configsCount;
	static ConfigurationsLoader configsLoader;
	static Class401 aClass401_86;
	static OutgoingPacket aClass311_87 = new OutgoingPacket(20, 4);
	static int[] anIntArray88;
	static Class258 aClass258_89 = new Class258();

	public static void method180(int i) {
		aClass258_89 = null;
		if (i >= -125)
			method180(-48);
		anIntArray88 = null;
		aClass401_86 = null;
		aClass311_87 = null;
	}

	final ConfigurationDefinition getDefinition(int configurationID) {
		ConfigurationDefinition definition;
		synchronized (cachedConfigurations) {
			definition = (ConfigurationDefinition) cachedConfigurations.get((long) configurationID);
		}
		if (definition != null)
			return definition;
		byte[] data;
		synchronized (fs) {
			data = fs.getFile(16, configurationID);
		}
		definition = new ConfigurationDefinition();
		if (data != null)
			definition.init(false, new Packet(data));
		synchronized (cachedConfigurations) {
			cachedConfigurations.put(definition, (long) configurationID);
		}
		return definition;
	}

	final void updateReferences(boolean bool, int i) {
		synchronized (cachedConfigurations) {
			if (bool != true)
				clearCachedConfigurations((byte) 54);
			cachedConfigurations.clean(i);
		}
	}

	final void clearCachedConfigurations(byte i) {
		synchronized (cachedConfigurations) {
			cachedConfigurations.clear();
		}
		if (i != -46)
			configsCount = 113;
	}

	ConfigurationsLoader(GameType class35, int i, Js5 class138) {
		fs = class138;
		if (fs != null)
			configsCount = fs.getLastFileId(16);
		else
			configsCount = 0;
	}

	static final int method184(float f, byte i, float f_1_, float f_2_) {
		float f_3_ = f_1_ < 0.0F ? -f_1_ : f_1_;
		float f_4_ = !(f < 0.0F) ? f : -f;
		if (i != 83)
			return 40;
		float f_5_ = f_2_ < 0.0F ? -f_2_ : f_2_;
		if (!(f_4_ > f_3_) || !(f_5_ < f_4_)) {
			if (!(f_5_ > f_3_) || !(f_4_ < f_5_)) {
				if (f_1_ > 0.0F)
					return 4;
				return 5;
			}
			if (f_2_ > 0.0F)
				return 2;
			return 3;
		}
		if (f > 0.0F)
			return 0;
		return 1;
	}

	final void clearSoftReferences(int i) {
		if (i != -30476)
			getDefinition(-58);
		synchronized (cachedConfigurations) {
			cachedConfigurations.clearSoftReferences();
		}
	}
}
