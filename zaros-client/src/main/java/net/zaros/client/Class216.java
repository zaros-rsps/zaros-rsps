package net.zaros.client;

/* Class216 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.lang.reflect.Method;

final class Class216 {
	static Class296_Sub39_Sub9 aClass296_Sub39_Sub9_2115;

	public static void method2029(byte i) {
		if (i == 54)
			aClass296_Sub39_Sub9_2115 = null;
	}

	static final synchronized void method2030(int i) {
		if (i != 10661)
			method2030(-22);
		if (Class350.anObject3040 == null) {
			try {
				Class var_class = java.lang.management.ManagementFactory.class;
				Method method = var_class.getDeclaredMethod("getPlatformMBeanServer", null);
				Object object = method.invoke(null, null);
				Method method_0_ = (var_class.getMethod("newPlatformMXBeanProxy",
						(new Class[] { (javax.management.MBeanServerConnection.class), String.class, Class.class })));
				Class350.anObject3040 = (method_0_.invoke(null,
						(new Object[] { object, "com.sun.management:type=HotSpotDiagnostic",
								(com.sun.management.HotSpotDiagnosticMXBean.class) })));
			} catch (Exception exception) {
				System.out.println("HeapDump setup error:");
				exception.printStackTrace();
			}
		}
	}

}
