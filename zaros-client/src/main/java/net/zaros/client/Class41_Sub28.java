package net.zaros.client;

/* Class41_Sub28 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jagex3.jagmisc.jagmisc;

import java.io.File;
import java.io.FileOutputStream;

final class Class41_Sub28 extends Class41 {
	static OutgoingPacket aClass311_3812;
	static boolean aBoolean3813 = false;
	static InterfaceComponent aClass51_3814 = null;
	static Class357 aClass357_3815;
	static int[] anIntArray3816;
	static int anInt3817 = 0;
	static OutgoingPacket aClass311_3818;

	final int method511(int i) {
		if (i < 114)
			aClass51_3814 = null;
		return anInt389;
	}

	Class41_Sub28(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final void method386(int i) {
		if (i != 2)
			aClass311_3812 = null;
	}

	static final void setMapSize(int type) {
		if (Class152.mapSizeType != type) {
			Class198.currentMapSizeX = Class296_Sub38.currentMapSizeY = Class296_Sub51_Sub1.mapSizes[type];
			Graphic.method292((byte) -85);
			StaticMethods.anIntArrayArray5929 = new int[Class198.currentMapSizeX][Class296_Sub38.currentMapSizeY];
			Class181.buildedMapParts = (new int[4][Class198.currentMapSizeX >> 3][Class296_Sub38.currentMapSizeY >> 3]);
			Class31.anIntArrayArray325 = new int[Class198.currentMapSizeX][Class296_Sub38.currentMapSizeY];
			for (int i = 0; i < 4; i++)
				BITConfigDefinition.mapClips[i] = ConfigsRegister.makeClipData(Class198.currentMapSizeX,
						Class296_Sub38.currentMapSizeY);
			Class198.aByteArrayArrayArray2003 = new byte[4][Class198.currentMapSizeX][Class296_Sub38.currentMapSizeY];
			Class298.method3237(Class198.currentMapSizeX, (byte) -119, 4, Class296_Sub38.currentMapSizeY);
			Class340.method3621(-63, Class198.currentMapSizeX >> 3, Class41_Sub13.aHa3774,
					Class296_Sub38.currentMapSizeY >> 3);
			Class152.mapSizeType = type;
		}
	}

	final int method380(int i, byte i_3_) {
		if (i_3_ != 41)
			aClass311_3812 = null;
		return 1;
	}

	static final void method513(String cmd, boolean bool, boolean bool_4_, int i) {
		try {
			if (cmd.equalsIgnoreCase("commands") || cmd.equalsIgnoreCase("help")) {
				Class41_Sub18.writeConsoleMessage("commands - This command");
				Class41_Sub18.writeConsoleMessage("cls - Clear console");
				Class41_Sub18.writeConsoleMessage("displayfps - Toggle FPS and other information");
				Class41_Sub18.writeConsoleMessage("renderer - Print graphics renderer information");
				Class41_Sub18.writeConsoleMessage("heap - Print java memory information");
				Class41_Sub18.writeConsoleMessage(
						"getcamerapos - Print location and direction of camera for use in bug reports");
				return;
			}
			if (cmd.equalsIgnoreCase("cls")) {
				za_Sub1.anInt6552 = 0;
				Class357.anInt3082 = 0;
				return;
			}
			if (i >= -72)
				anInt3817 = -39;
			if (cmd.equalsIgnoreCase("displayfps")) {
				Class68.drawFPS = !Class68.drawFPS;
				if (Class68.drawFPS)
					Class41_Sub18.writeConsoleMessage("FPS on");
				else {
					Class41_Sub18.writeConsoleMessage("FPS off");
					return;
				}
				return;
			}
			if (cmd.equals("renderer")) {
				Class33 class33 = Class41_Sub13.aHa3774.s();
				Class41_Sub18.writeConsoleMessage("Vendor: " + class33.anInt333);
				Class41_Sub18.writeConsoleMessage("Name: " + class33.aString332);
				Class41_Sub18.writeConsoleMessage("Version: " + class33.anInt327);
				Class41_Sub18.writeConsoleMessage("Device: " + class33.aString328);
				Class41_Sub18.writeConsoleMessage("Driver Version: " + class33.aLong330);
				return;
			}
			if (cmd.equals("heap")) {
				Class41_Sub18.writeConsoleMessage("Heap: " + FileWorker.anInt3004 + "MB");
				return;
			}
			if (cmd.equalsIgnoreCase("getcamerapos")) {
				Class41_Sub18.writeConsoleMessage(("Pos: " + (Class296_Sub51_Sub11.localPlayer.z) + ","
						+ (Class206.worldBaseX + (Class219.camPosX >> 9) >> 6) + ","
						+ ((Class124.camPosZ >> 9) + Class41_Sub26.worldBaseY >> 6) + ","
						+ ((Class219.camPosX >> 9) + Class206.worldBaseX & 0x3f) + ","
						+ ((Class124.camPosZ >> 9) + Class41_Sub26.worldBaseY & 0x3f) + " Height: "
						+ (aa_Sub1.method155(-1537652855, (Class296_Sub51_Sub11.localPlayer.z), Class219.camPosX,
								Class124.camPosZ) - TranslatableString.camPosY)));
				Class41_Sub18.writeConsoleMessage(("Look: " + (Class296_Sub51_Sub11.localPlayer.z) + ","
						+ (Class53.lookatX + Class206.worldBaseX >> 6) + ","
						+ (Class41_Sub26.worldBaseY + Graphic.lookatY >> 6) + ","
						+ (Class53.lookatX + Class206.worldBaseX & 0x3f) + ","
						+ (Graphic.lookatY + Class41_Sub26.worldBaseY & 0x3f) + " Height: "
						+ (aa_Sub1.method155(-1537652855, (Class296_Sub51_Sub11.localPlayer.z), Class53.lookatX,
								Graphic.lookatY) - Class338_Sub8_Sub2.lookatHeight)));
				return;
			}
		} catch (Exception exception) {
			Class41_Sub18.writeConsoleMessage(TranslatableString.aClass120_1198.getTranslation(Class394.langID));
			return;
		}
		if (Class41_Sub29.modeWhere != BITConfigsLoader.liveModeWhere || Class338_Sub3_Sub5.rights >= 2) {
			if (cmd.equalsIgnoreCase("errortest"))
				throw new RuntimeException();
			if (cmd.equals("nativememerror"))
				throw new OutOfMemoryError("native(MPR");
			try {
				if (cmd.equalsIgnoreCase("printfps")) {
					Class41_Sub18.writeConsoleMessage("FPS: " + Class360_Sub8.anInt5338);
					return;
				}
				if (cmd.equalsIgnoreCase("occlude")) {
					Class103.aBoolean1086 = !Class103.aBoolean1086;
					if (!Class103.aBoolean1086)
						Class41_Sub18.writeConsoleMessage("Occlsion now off!");
					else {
						Class41_Sub18.writeConsoleMessage("Occlsion now on!");
						return;
					}
					return;
				}
				if (cmd.startsWith("gpiinvisdebug")) {
					String[] data = cmd.split(" ");
					if (data.length <= 1) {
						for (int a = 0; a < PlayerUpdate.invisiblePlayers.length; a++) {
							if (PlayerUpdate.invisiblePlayers[a] != null) {
								InvisiblePlayer p = PlayerUpdate.invisiblePlayers[a];
								int hash = p.locationHash;
								int x = hash >> 8 & 0xff;
								int y = hash & 0xff;
								int z = hash >> 16;
								Class41_Sub18
										.writeConsoleMessage("[" + a + "]x=" + (x * 64) + ",y=" + (y * 64) + ",z=" + z);
							}
						}
					} else {
						int index = Integer.parseInt(data[1]);
						if (index < 0 || index > PlayerUpdate.invisiblePlayers.length
								|| PlayerUpdate.invisiblePlayers[index] == null) {
							Class41_Sub18.writeConsoleMessage("No data");
						} else {
							InvisiblePlayer p = PlayerUpdate.invisiblePlayers[index];
							int hash = p.locationHash;
							int x = hash >> 8 & 0xff;
							int y = hash & 0xff;
							int z = hash >> 16;
							Class41_Sub18
									.writeConsoleMessage("[" + index + "]x=" + (x * 64) + ",y=" + (y * 64) + ",z=" + z);
						}
					}
				}
				if (cmd.startsWith("gpivisdebug")) {
					for (int a = 0; a < PlayerUpdate.visiblePlayers.length; a++) {
						Player player = PlayerUpdate.visiblePlayers[a];
						if (player != null) {
							int fullX = (player.tileX - (player.getSize() * 256)) / 512;
							int fullY = (player.tileY - (player.getSize() * 256)) / 512;
							Class41_Sub18.writeConsoleMessage(
									"[" + player.index + "]x=" + fullX + ",y=" + fullY + ",z=" + player.z);
						}
					}
				}
				if (cmd.equalsIgnoreCase("fpson")) {
					Class68.drawFPS = true;
					Class41_Sub18.writeConsoleMessage("fps debug enabled");
					return;
				}
				if (cmd.equalsIgnoreCase("fpsoff")) {
					Class68.drawFPS = false;
					Class41_Sub18.writeConsoleMessage("fps debug disabled");
					return;
				}
				if (cmd.equals("systemmem")) {
					try {
						Class41_Sub18.writeConsoleMessage(
								("System memory: " + jagmisc.getAvailablePhysicalMemory() / 1048576L + "/"
										+ Class360_Sub3.aClass296_Sub28_5309.anInt4797 + "Mb"));
					} catch (Throwable throwable) {
						/* empty */
					}
					return;
				}
				if (cmd.equalsIgnoreCase("cleartext")) {
					Class296_Sub51_Sub39.aClass404_6546.method4161((byte) 53);
					Class41_Sub18.writeConsoleMessage("Text coords cleared");
					return;
				}
				if (cmd.equalsIgnoreCase("gc")) {
					Class119.method1027(16);
					for (int i_5_ = 0; i_5_ < 10; i_5_++)
						System.gc();
					Runtime runtime = Runtime.getRuntime();
					int i_6_ = (int) ((runtime.totalMemory() + -runtime.freeMemory()) / 1024L);
					Class41_Sub18.writeConsoleMessage("mem=" + i_6_ + "k");
					return;
				}
				if (cmd.equalsIgnoreCase("compact")) {
					Class119.method1027(16);
					for (int i_7_ = 0; i_7_ < 10; i_7_++)
						System.gc();
					Runtime runtime = Runtime.getRuntime();
					int i_8_ = (int) ((runtime.totalMemory() - runtime.freeMemory()) / 1024L);
					Class41_Sub18.writeConsoleMessage(("Memory before cleanup=" + i_8_ + "k"));
					Class384.method4016((byte) -78);
					Class119.method1027(16);
					for (int i_9_ = 0; i_9_ < 10; i_9_++)
						System.gc();
					i_8_ = (int) ((runtime.totalMemory() + -runtime.freeMemory()) / 1024L);
					Class41_Sub18.writeConsoleMessage(("Memory after cleanup=" + i_8_ + "k"));
					return;
				}
				if (cmd.equalsIgnoreCase("unloadnatives")) {
					Class41_Sub18.writeConsoleMessage(
							(!Class151.method1540((byte) -110) ? "Library unloading failed!" : "Libraries unloaded"));
					return;
				}
				if (cmd.equalsIgnoreCase("clientdrop")) {
					Class41_Sub18.writeConsoleMessage("Dropped client connection");
					if (Class366_Sub6.anInt5392 != 11) {
						if (Class366_Sub6.anInt5392 == 12)
							Class296_Sub45_Sub2.aClass204_6277.aBoolean2065 = true;
					} else
						Class41_Sub8.method423(0);
					return;
				}
				if (cmd.equalsIgnoreCase("rotateconnectmethods")) {
					Class296_Sub51_Sub27_Sub1.aClass112_6733.method978((byte) 127);
					Class41_Sub18.writeConsoleMessage("Rotated connection methods");
					return;
				}
				if (cmd.equalsIgnoreCase("clientjs5drop")) {
					Class42.aClass285_395.method2366((byte) 70);
					Class41_Sub18.writeConsoleMessage("Dropped client js5 net queue");
					return;
				}
				if (cmd.equalsIgnoreCase("serverjs5drop")) {
					Class42.aClass285_395.method2367(74);
					Class41_Sub18.writeConsoleMessage("Dropped server js5 net queue");
					return;
				}
				if (cmd.equalsIgnoreCase("breakcon")) {
					Class252.aClass398_2383.method4115(19631);
					Connection[] class204s = Class296_Sub45_Sub2.aClass204Array6278;
					for (int i_10_ = 0; i_10_ < class204s.length; i_10_++) {
						Connection class204 = class204s[i_10_];
						if (class204.aClass154_2045 != null)
							class204.aClass154_2045.method1555(-10035);
					}
					Class42.aClass285_395.method2376(49);
					Class41_Sub18.writeConsoleMessage("Breaking new connections for 5 seconds");
					return;
				}
				if (cmd.equalsIgnoreCase("rebuild")) {
					CS2Stack.method2137(74);
					Class296_Sub45_Sub4.method3018(-114);
					Class41_Sub18.writeConsoleMessage("Rebuilding map");
					return;
				}
				if (cmd.equalsIgnoreCase("rebuildprofile")) {
					Class403.aLong3380 = Class72.method771(-128);
					Class324.aBoolean2861 = true;
					CS2Stack.method2137(49);
					Class296_Sub45_Sub4.method3018(-121);
					Class41_Sub18.writeConsoleMessage("Rebuilding map (with profiling)");
					return;
				}
				if (cmd.equalsIgnoreCase("wm1")) {
					Class104.method904(1, false, 0, -1, -1);
					if (Class8.method192((byte) 101) != 1)
						Class41_Sub18.writeConsoleMessage("wm1 failed");
					else {
						Class41_Sub18.writeConsoleMessage("wm1 succeeded");
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("wm2")) {
					Class104.method904(2, false, 0, -1, -1);
					if (Class8.method192((byte) 101) != 2)
						Class41_Sub18.writeConsoleMessage("wm2 failed");
					else {
						Class41_Sub18.writeConsoleMessage("wm2 succeeded");
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("wm3")) {
					Class104.method904(3, false, 0, 1024, 768);
					if (Class8.method192((byte) 101) == 3)
						Class41_Sub18.writeConsoleMessage("wm3 succeeded");
					else {
						Class41_Sub18.writeConsoleMessage("wm3 failed");
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("tk0")) {
					Class33.method348(false, false, 0);
					if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(125) == 0) {
						Class41_Sub18.writeConsoleMessage("Entered tk0");
						Class343_Sub1.aClass296_Sub50_5282.method3060(0, 92,
								(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988));
						Class368_Sub4.method3820(1);
						Class348.aBoolean3033 = false;
					} else {
						Class41_Sub18.writeConsoleMessage("Failed to enter tk0");
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("tk1")) {
					Class33.method348(false, false, 1);
					if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(122) != 1)
						Class41_Sub18.writeConsoleMessage("Failed to enter tk1");
					else {
						Class41_Sub18.writeConsoleMessage("Entered tk1");
						Class343_Sub1.aClass296_Sub50_5282.method3060(1, 102,
								(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988));
						Class368_Sub4.method3820(1);
						Class348.aBoolean3033 = false;
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("tk2")) {
					Class33.method348(false, false, 2);
					if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(116) == 2) {
						Class41_Sub18.writeConsoleMessage("Entered tk2");
						Class343_Sub1.aClass296_Sub50_5282.method3060(2, 64,
								(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988));
						Class368_Sub4.method3820(1);
						Class348.aBoolean3033 = false;
					} else {
						Class41_Sub18.writeConsoleMessage("Failed to enter tk2");
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("tk3")) {
					Class33.method348(false, false, 3);
					if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(125) != 3)
						Class41_Sub18.writeConsoleMessage("Failed to enter tk3");
					else {
						Class41_Sub18.writeConsoleMessage("Entered tk3");
						Class343_Sub1.aClass296_Sub50_5282.method3060(3, 118,
								(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988));
						Class368_Sub4.method3820(1);
						Class348.aBoolean3033 = false;
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("tk5")) {
					Class33.method348(false, false, 5);
					if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(125) != 5)
						Class41_Sub18.writeConsoleMessage("Failed to enter tk5");
					else {
						Class41_Sub18.writeConsoleMessage("Entered tk5");
						Class343_Sub1.aClass296_Sub50_5282.method3060(5, 63,
								(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988));
						Class368_Sub4.method3820(1);
						Class348.aBoolean3033 = false;
						return;
					}
					return;
				}
				if (cmd.startsWith("setba")) {
					if (cmd.length() < 6)
						Class41_Sub18.writeConsoleMessage("Invalid buildarea value");
					else {
						int i_11_ = Class366_Sub2.method3774(-93, cmd.substring(6));
						if (i_11_ < 0 || i_11_ > Class16_Sub3.method247(FileWorker.anInt3004, 2))
							Class41_Sub18.writeConsoleMessage("Invalid buildarea value");
						else {
							Class343_Sub1.aClass296_Sub50_5282.method3060(i_11_, 88,
									(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987));
							Class368_Sub4.method3820(1);
							Class348.aBoolean3033 = false;
							Class41_Sub18.writeConsoleMessage(("maxbuildarea="
									+ Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987.method402(116)));
							return;
						}
						return;
					}
					return;
				}
				if (cmd.startsWith("rect_debug")) {
					if (cmd.length() < 10)
						Class41_Sub18.writeConsoleMessage("Invalid rect_debug value");
					else {
						Class296_Sub51_Sub28.anInt6487 = Class366_Sub2.method3774(-108, cmd.substring(10).trim());
						Class41_Sub18.writeConsoleMessage(("rect_debug=" + (Class296_Sub51_Sub28.anInt6487)));
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("qa_op_test")) {
					BillboardRaw.aBoolean1071 = true;
					Class41_Sub18.writeConsoleMessage(("qa_op_test=" + BillboardRaw.aBoolean1071));
					return;
				}
				if (cmd.equalsIgnoreCase("clipcomponents")) {
					Class320.aBoolean2822 = !Class320.aBoolean2822;
					Class41_Sub18.writeConsoleMessage(("clipcomponents=" + Class320.aBoolean2822));
					return;
				}
				if (cmd.startsWith("bloom")) {
					boolean bool_12_ = Class41_Sub13.aHa3774.b();
					if (AdvancedMemoryCache.method984(-108, !bool_12_)) {
						if (!bool_12_)
							Class41_Sub18.writeConsoleMessage("Bloom enabled");
						else
							Class41_Sub18.writeConsoleMessage("Bloom disabled");
					} else {
						Class41_Sub18.writeConsoleMessage("Failed to enable bloom");
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("tween")) {
					if (Class369.aBoolean3135) {
						Class369.aBoolean3135 = false;
						Class41_Sub18.writeConsoleMessage("Forced tweening disabled.");
					} else {
						Class369.aBoolean3135 = true;
						Class41_Sub18.writeConsoleMessage("Forced tweening ENABLED!");
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("shiftclick")) {
					if (StaticMethods.aBoolean5054) {
						Class41_Sub18.writeConsoleMessage("Shift-click disabled.");
						StaticMethods.aBoolean5054 = false;
					} else {
						Class41_Sub18.writeConsoleMessage("Shift-click ENABLED!");
						StaticMethods.aBoolean5054 = true;
						return;
					}
					return;
				}
				if (cmd.equalsIgnoreCase("getcgcoord")) {
					Class41_Sub18.writeConsoleMessage(("x:" + ((Class296_Sub51_Sub11.localPlayer.tileX) >> 9) + " z:"
							+ ((Class296_Sub51_Sub11.localPlayer.tileY) >> 9)));
					return;
				}
				if (cmd.equalsIgnoreCase("getheight")) {
					Class41_Sub18.writeConsoleMessage(
							("Height: " + (Class360_Sub2.aSArray5304[(Class296_Sub51_Sub11.localPlayer.z)].method3355(
									((Class296_Sub51_Sub11.localPlayer.tileY) >> 9), (byte) -117,
									((Class296_Sub51_Sub11.localPlayer.tileX) >> 9)))));
					return;
				}
				if (cmd.equalsIgnoreCase("resetminimap")) {
					Class205_Sub2.fs8.clearEntryBuffers_();
					Class205_Sub2.fs8.clearEntryBuffers((byte) -126);
					ConfigurationsLoader.aClass401_86.method4138(9);
					Class31.aClass245_324.method2182(2);
					Class296_Sub45_Sub4.method3018(-107);
					Class41_Sub18.writeConsoleMessage("Minimap reset");
					return;
				}
				if (cmd.startsWith("mc")) {
					if (Class41_Sub13.aHa3774.r()) {
						int i_13_ = Integer.parseInt(cmd.substring(3));
						if (i_13_ >= 1) {
							if (i_13_ > 4)
								i_13_ = 4;
						} else
							i_13_ = 1;
						Class338_Sub3_Sub5_Sub2.anInt6667 = i_13_;
						CS2Stack.method2137(69);
						Class41_Sub18.writeConsoleMessage(("Render cores now: " + (Class338_Sub3_Sub5_Sub2.anInt6667)));
					} else {
						Class41_Sub18.writeConsoleMessage("Current toolkit doesn't support multiple cores");
						return;
					}
					return;
				}
				if (cmd.startsWith("cachespace")) {
					Class41_Sub18.writeConsoleMessage(("I(s): " + Class296_Sub39_Sub13.aClass113_6205.getCapacity()
							+ "/" + Class296_Sub39_Sub13.aClass113_6205.getMaxCapacity()));
					Class41_Sub18.writeConsoleMessage(("I(m): " + Class338_Sub3_Sub1_Sub4.aClass113_6641.getCapacity()
							+ "/" + Class338_Sub3_Sub1_Sub4.aClass113_6641.getMaxCapacity()));
					Class41_Sub18.writeConsoleMessage(
							("O(s): " + Class296_Sub39_Sub1.itemDefinitionLoader.iconRequests.method3307((byte) -66)
									+ "/" + Class296_Sub39_Sub1.itemDefinitionLoader.iconRequests.method3314(-1)));
					return;
				}
				if (cmd.equals("renderprofile") || cmd.equals("rp")) {
					Class199.aBoolean2006 = !Class199.aBoolean2006;
					Class41_Sub13.aHa3774.a(Class199.aBoolean2006);
					Class97.method874((byte) -100);
					Class41_Sub18.writeConsoleMessage(("showprofiling=" + Class199.aBoolean2006));
					return;
				}
				if (cmd.startsWith("performancetest")) {
					int i_14_ = -1;
					int i_15_ = 1000;
					if (cmd.length() > 15) {
						String[] strings = Class41_Sub30.method522((byte) 63, cmd, ' ');
						try {
							if (strings.length > 1)
								i_15_ = Integer.parseInt(strings[1]);
						} catch (Throwable throwable) {
							/* empty */
						}
						try {
							if (strings.length > 2)
								i_14_ = Integer.parseInt(strings[2]);
						} catch (Throwable throwable) {
							/* empty */
						}
					}
					if (i_14_ == -1) {
						Class41_Sub18.writeConsoleMessage(
								("Java toolkit: " + Class296_Sub51_Sub21.method3134(0, i_15_, (byte) -121)));
						Class41_Sub18.writeConsoleMessage(
								("SSE toolkit:  " + Class296_Sub51_Sub21.method3134(2, i_15_, (byte) -121)));
						Class41_Sub18.writeConsoleMessage(
								("D3D toolkit:  " + Class296_Sub51_Sub21.method3134(3, i_15_, (byte) -121)));
						Class41_Sub18.writeConsoleMessage(
								("GL toolkit:   " + Class296_Sub51_Sub21.method3134(1, i_15_, (byte) -121)));
						Class41_Sub18.writeConsoleMessage(
								("GLX toolkit:  " + Class296_Sub51_Sub21.method3134(5, i_15_, (byte) -121)));
					} else {
						Class41_Sub18.writeConsoleMessage(
								("Performance: " + Class296_Sub51_Sub21.method3134(i_14_, i_15_, (byte) -121)));
						return;
					}
					return;
				}
				if (cmd.equals("nonpcs")) {
					Class366_Sub7.aBoolean5406 = !Class366_Sub7.aBoolean5406;
					Class41_Sub18.writeConsoleMessage(("nonpcs=" + Class366_Sub7.aBoolean5406));
					return;
				}
				if (cmd.equals("autoworld")) {
					NPCDefinition.method1495(-15477);
					Class41_Sub18.writeConsoleMessage("auto world selected");
					return;
				}
				if (cmd.startsWith("switchworld")) {
					int i_16_ = Integer.parseInt(cmd.substring(12));
					Class45.method582(i_16_, 7000, (GraphicsLoader.method2284(i_16_, -111).aString4543));
					Class41_Sub18.writeConsoleMessage("switched");
					return;
				}
				if (cmd.equals("getworld")) {
					Class41_Sub18.writeConsoleMessage("w: " + (Class296_Sub51_Sub27_Sub1.aClass112_6733.worldId));
					return;
				}
				if (cmd.startsWith("pc")) {
					Connection class204 = Class296_Sub51_Sub13.method3111(true);
					Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 108,
							InterfaceComponentSettings.aClass311_4823);
					class296_sub1.out.p1(0);
					int i_17_ = class296_sub1.out.pos;
					int i_18_ = cmd.indexOf(" ", 4);
					class296_sub1.out.writeString(cmd.substring(3, i_18_));
					Class86.method826(cmd.substring(i_18_), 0, class296_sub1.out);
					class296_sub1.out.method2604((-i_17_ + class296_sub1.out.pos));
					class204.sendPacket(class296_sub1, (byte) 119);
					return;
				}
				if (cmd.equals("savevarcs")) {
					Class188.method1891(3746);
					Class41_Sub18.writeConsoleMessage("perm varcs saved");
					return;
				}
				if (cmd.equals("scramblevarcs")) {
					for (int i_19_ = 0; i_19_ < Class269.globalIntVars.length; i_19_++) {
						if (Class14.aBooleanArray153[i_19_]) {
							Class269.globalIntVars[i_19_] = (int) (Math.random() * 99999.0);
							if (Math.random() > 0.5)
								Class269.globalIntVars[i_19_] *= -1;
						}
					}
					Class188.method1891(3746);
					Class41_Sub18.writeConsoleMessage("perm varcs scrambled");
					return;
				}
				if (cmd.equals("showcolmap")) {
					Class296_Sub39_Sub1.aBoolean6124 = true;
					Class296_Sub45_Sub4.method3018(-109);
					Class41_Sub18.writeConsoleMessage("colmap is shown");
					return;
				}
				if (cmd.equals("hidecolmap")) {
					Class296_Sub39_Sub1.aBoolean6124 = false;
					Class296_Sub45_Sub4.method3018(-122);
					Class41_Sub18.writeConsoleMessage("colmap is hidden");
					return;
				}
				if (cmd.equals("resetcache")) {
					TextureOperation.method3069(-127);
					Class41_Sub18.writeConsoleMessage("Caches reset");
					return;
				}
				if (cmd.equals("profilecpu")) {
					Class41_Sub18.writeConsoleMessage(String.valueOf(Class368_Sub5_Sub1.method3828((byte) 119)) + "ms");
					return;
				}
				if (cmd.startsWith("getclientvarpbit")) {
					int i_20_ = Integer.parseInt(cmd.substring(17));
					Class41_Sub18.writeConsoleMessage(
							"varpbit=" + Class16_Sub3_Sub1.configsRegister.getBITConfig(i_20_, (byte) -106));
					return;
				}
				if (cmd.startsWith("getclientvarp")) {
					int i_21_ = Integer.parseInt(cmd.substring(14));
					Class41_Sub18
							.writeConsoleMessage(("varp=" + Class16_Sub3_Sub1.configsRegister.getConfig(true, i_21_)));
					return;
				}
				if (cmd.startsWith("directlogin")) {
					String[] strings = Class41_Sub30.method522((byte) 63, cmd.substring(12), ' ');
					if (strings.length >= 2) {
						int i_22_ = (strings.length <= 2 ? 0 : Integer.parseInt(strings[2]));
						Class296_Sub51_Sub21.method3130(strings[1], strings[0], -2103592604, i_22_);
						return;
					}
				}
				if (cmd.startsWith("snlogin ")) {
					String[] strings = Class41_Sub30.method522((byte) 63, cmd.substring(8), ' ');
					int i_23_ = Integer.parseInt(strings[0]);
					int i_24_ = (strings.length != 2 ? 0 : Integer.parseInt(strings[1]));
					Class252.method2203(i_24_, i_23_, -69);
					return;
				}
				if (cmd.startsWith("csprofileclear")) {
					CS2Executor.method1537();
					return;
				}
				if (cmd.startsWith("csprofileoutputc")) {
					CS2Executor.method1529(100, false);
					return;
				}
				if (cmd.startsWith("csprofileoutputt")) {
					CS2Executor.method1529(10, true);
					return;
				}
				if (cmd.startsWith("texsize")) {
					int i_25_ = Integer.parseInt(cmd.substring(8));
					Class41_Sub13.aHa3774.a(i_25_);
					return;
				}
				if (cmd.equals("soundstreamcount")) {
					Class41_Sub18.writeConsoleMessage(
							("Active streams: " + Class16_Sub3.aClass296_Sub45_Sub5_3734.method3034()));
					return;
				}
				if (cmd.equals("autosetup")) {
					Class351.method3682(-513);
					Class41_Sub18.writeConsoleMessage(("Complete. Toolkit now: "
							+ Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(116)));
					return;
				}
				if (cmd.equals("errormessage")) {
					Class41_Sub18.writeConsoleMessage(Class246.aClient2332.method102((byte) -108));
					return;
				}
				if (cmd.equals("heapdump")) {
					if (!Class398.osName.startsWith("win"))
						Class334.method3420(75, false, new File("/tmp/heap.dump"));
					else
						Class334.method3420(92, false, new File("C:\\Temp\\heap.dump"));
					Class41_Sub18.writeConsoleMessage("Done");
					return;
				}
				if (cmd.equals("os")) {
					Class41_Sub18.writeConsoleMessage("Name: " + Class398.osName);
					Class41_Sub18.writeConsoleMessage("Arch: " + Class398.aString3327);
					Class41_Sub18.writeConsoleMessage("Ver: " + Class398.aString3323);
					return;
				}
				if (cmd.startsWith("w2debug")) {
					int i_26_ = Integer.parseInt(cmd.substring(8, 9));
					Class319.anInt3650 = i_26_;
					CS2Stack.method2137(60);
					Class41_Sub18.writeConsoleMessage("Toggled!");
					return;
				}
				if (cmd.startsWith("ortho ")) {
					int i_27_ = cmd.indexOf(' ');
					if (i_27_ < 0)
						Class41_Sub18.writeConsoleMessage("Syntax: ortho <n>");
					else {
						int i_28_ = Class366_Sub2.method3774(-89, cmd.substring(i_27_ + 1));
						Class343_Sub1.aClass296_Sub50_5282.method3060(i_28_, 74,
								(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022));
						Class368_Sub4.method3820(1);
						Class348.aBoolean3033 = false;
						Class41_Sub29.method520(-77);
						if (i_28_ == Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022.method456(118))
							Class41_Sub18.writeConsoleMessage("Successfully changed ortho mode");
						else {
							Class41_Sub18.writeConsoleMessage("Failed to change ortho mode");
							return;
						}
						return;
					}
					return;
				}
				if (cmd.startsWith("orthozoom ")) {
					if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022.method456(118) == 0)
						Class41_Sub18.writeConsoleMessage("enable ortho mode first (use 'ortho <n>')");
					else {
						int i_29_ = (Class366_Sub2.method3774(-91, cmd.substring(cmd.indexOf(' ') + 1)));
						ModeWhat.anInt1193 = i_29_;
						Class41_Sub18.writeConsoleMessage(("orthozoom=" + ModeWhat.anInt1193));
						return;
					}
					return;
				}
				if (cmd.startsWith("orthotilesize ")) {
					int i_30_ = (Class366_Sub2.method3774(-28, cmd.substring(cmd.indexOf(' ') + 1)));
					Class94.anInt1017 = Class296_Sub15_Sub3.anInt6011 = i_30_;
					Class41_Sub18.writeConsoleMessage("ortho tile size=" + i_30_);
					Class41_Sub29.method520(124);
					return;
				}
				if (cmd.equals("orthocamlock")) {
					Class315.aBoolean2789 = !Class315.aBoolean2789;
					Class41_Sub18
							.writeConsoleMessage(("ortho camera lock is " + (!Class315.aBoolean2789 ? "off" : "on")));
					return;
				}
				if (cmd.startsWith("skydetail ")) {
					int i_31_ = (Class366_Sub2.method3774(-67, cmd.substring(cmd.indexOf(' ') + 1)));
					Class343_Sub1.aClass296_Sub50_5282.method3060(i_31_, 90,
							(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011));
					Class41_Sub18.writeConsoleMessage(("skydetail is "
							+ (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011.method468(117) == 0 ? "low"
									: "high")));
					return;
				}
				if (cmd.startsWith("setoutput ")) {
					File file = new File(cmd.substring(10));
					if (file.exists()) {
						file = new File(cmd.substring(10) + "." + Class72.method771(-116) + ".log");
						if (file.exists()) {
							Class41_Sub18.writeConsoleMessage("file already exists!");
							return;
						}
					}
					if (MaterialRaw.aFileOutputStream1776 != null) {
						MaterialRaw.aFileOutputStream1776.close();
						MaterialRaw.aFileOutputStream1776 = null;
					}
					try {
						MaterialRaw.aFileOutputStream1776 = new FileOutputStream(file);
					} catch (java.io.FileNotFoundException filenotfoundexception) {
						Class41_Sub18.writeConsoleMessage(("Could not create " + file.getName()));
					} catch (SecurityException securityexception) {
						Class41_Sub18.writeConsoleMessage(("Cannot write to " + file.getName()));
					}
					return;
				}
				if (cmd.equals("closeoutput")) {
					if (MaterialRaw.aFileOutputStream1776 != null)
						MaterialRaw.aFileOutputStream1776.close();
					MaterialRaw.aFileOutputStream1776 = null;
					return;
				}
				if (cmd.startsWith("runscript ")) {
					File file = new File(cmd.substring(10));
					if (!file.exists()) {
						Class41_Sub18.writeConsoleMessage("No such file");
						return;
					}
					byte[] is = Class178_Sub3.method1794((byte) 85, file);
					if (is == null) {
						Class41_Sub18.writeConsoleMessage("Failed to read file");
						return;
					}
					String[] strings = (Class41_Sub30.method522((byte) 63,
							StaticMethods.method1710('\r', Class182.method1846(is, 102), "", -9271), '\n'));
					Class355.method3694(35044, strings);
				}
				if (cmd.startsWith("zoom ")) {
					short i_32_ = (short) Class366_Sub2.method3774(-111, cmd.substring(5));
					if (i_32_ > 0)
						Class366_Sub7.aShort5396 = i_32_;
					return;
				}
				if (cmd.startsWith("cs2debug")) {
					if (cmd.length() > 9 && cmd.charAt(8) == ' ') {
						CS2Executor.aString1548 = cmd.substring(9);
						CS2Executor.aBoolean1547 = true;
						Class41_Sub18.writeConsoleMessage(("cs2debug: (" + CS2Executor.aString1548 + ")"));
					} else {
						CS2Executor.aBoolean1547 = !CS2Executor.aBoolean1547;
						CS2Executor.aString1548 = null;
						Class41_Sub18.writeConsoleMessage(("cs2debug:" + CS2Executor.aBoolean1547));
						return;
					}
					return;
				}
				if (Class366_Sub6.anInt5392 == 11) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut(
							(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 120, r_Sub2.aClass311_6715);
					class296_sub1.out.p1(cmd.length() + 3);
					class296_sub1.out.p1(bool ? 1 : 0);
					class296_sub1.out.p1(bool_4_ ? 1 : 0);
					class296_sub1.out.writeString(cmd);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (cmd.startsWith("fps ") && (Class41_Sub29.modeWhere != BITConfigsLoader.liveModeWhere)) {
					Class122_Sub1.method1042(Class366_Sub2.method3774(-91, cmd.substring(4)), (byte) 6);
					return;
				}
			} catch (Exception exception) {
				Class41_Sub18.writeConsoleMessage(TranslatableString.aClass120_1198.getTranslation(Class394.langID));
				return;
			}
		}
		if (Class366_Sub6.anInt5392 != 11)
			Class41_Sub18
					.writeConsoleMessage((TranslatableString.aClass120_1199.getTranslation(Class394.langID)) + cmd);
	}

	Class41_Sub28(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	public static void method514(int i) {
		anIntArray3816 = null;
		aClass357_3815 = null;
		aClass311_3812 = null;
		aClass311_3818 = null;
		if (i != -16319)
			aClass311_3812 = null;
		aClass51_3814 = null;
	}

	final void method381(int i, byte i_33_) {
		if (i_33_ != -110)
			method513(null, false, true, -113);
		anInt389 = i;
	}

	final int method383(byte i) {
		if (i != 110)
			return -83;
		return 0;
	}

	static {
		aClass311_3812 = new OutgoingPacket(28, 7);
		aClass311_3818 = new OutgoingPacket(73, 2);
	}
}
