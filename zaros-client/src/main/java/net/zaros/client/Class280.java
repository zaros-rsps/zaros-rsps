package net.zaros.client;

/* Class280 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class280 {
	int anInt2550;
	int anInt2551;
	int anInt2552 = -1;
	private Class373[] aClass373Array2553;
	int anInt2554;
	static AdvancedMemoryCache aClass113_2555 = new AdvancedMemoryCache(32);
	int anInt2556;
	int anInt2557;
	int[][] anIntArrayArray2558;
	int[] anIntArray2559;
	int anInt2560;
	int[][] anIntArrayArray2561;
	int anInt2562 = -1;
	int anInt2563;
	int anInt2564;
	int anInt2565;
	int anInt2566;
	int anInt2567 = 0;
	int anInt2568;
	int anInt2569;
	int[] anIntArray2570;
	private int anInt2571;
	int anInt2572;
	int[] anIntArray2573;
	int[] anIntArray2574;
	int anInt2575;
	int anInt2576;
	int anInt2577;
	int anInt2578;
	int anInt2579;
	int anInt2580;
	int anInt2581;
	int anInt2582;
	int anInt2583;
	int anInt2584;
	int anInt2585;
	int anInt2586;
	int anInt2587;
	int anInt2588;
	static Class125 aClass125_2589 = new Class125();
	private int anInt2590;
	boolean aBoolean2591;
	int anInt2592;
	int anInt2593;
	Class62 aClass62_2594;
	int anInt2595;
	int anInt2596;
	int anInt2597;
	int anInt2598;
	int anInt2599;

	final boolean method2342(int i, int i_0_) {
		if (i_0_ == -1) {
			return false;
		}
		if (i_0_ == anInt2569) {
			return true;
		}
		if (anIntArray2574 != null) {
			for (int i_1_ = 0; anIntArray2574.length > i_1_; i_1_++) {
				if (anIntArray2574[i_1_] == i_0_) {
					return true;
				}
			}
		}
		return false;
	}

	final void method2343(int i, Packet buffer, boolean newBas) {
		if (newBas) {
			buffer.pos += 3;
		}
		for (;;) {
			int i_2_ = buffer.g1();
			if (i_2_ == 0) {
				break;
			}
			method2346(i_2_, i + 20468, buffer, newBas);
		}
		if (i != -20502) {
			anInt2579 = 74;
		}
	}

	final Class373[] method2344(ha var_ha, byte i) {
		if (aClass373Array2553 != null && var_ha.anInt1295 == anInt2590) {
			return aClass373Array2553;
		}
		if (anIntArrayArray2558 == null) {
			return null;
		}
		aClass373Array2553 = new Class373[anIntArrayArray2558.length];
		for (int i_3_ = 0; anIntArrayArray2558.length > i_3_; i_3_++) {
			int i_4_ = 0;
			int i_5_ = 0;
			int i_6_ = 0;
			int i_7_ = 0;
			int i_8_ = 0;
			int i_9_ = 0;
			if (anIntArrayArray2558[i_3_] != null) {
				i_7_ = anIntArrayArray2558[i_3_][3] << 3;
				i_9_ = anIntArrayArray2558[i_3_][5] << 3;
				i_5_ = anIntArrayArray2558[i_3_][1];
				i_4_ = anIntArrayArray2558[i_3_][0];
				i_8_ = anIntArrayArray2558[i_3_][4] << 3;
				i_6_ = anIntArrayArray2558[i_3_][2];
			}
			if (i_4_ != 0 || i_5_ != 0 || i_6_ != 0 || i_7_ != 0 || i_8_ != 0 || i_9_ != 0) {
				Class373 class373 = aClass373Array2553[i_3_] = var_ha.m();
				if (i_9_ != 0) {
					class373.method3900(i_9_);
				}
				if (i_7_ != 0) {
					class373.method3914(i_7_);
				}
				if (i_8_ != 0) {
					class373.method3917(i_8_);
				}
				class373.method3904(i_4_, i_5_, i_6_);
			}
		}
		if (i != 41) {
			return null;
		}
		return aClass373Array2553;
	}

	static final byte[] method2345(byte i, String string) {
		int i_10_ = string.length();
		int i_11_ = 43 % ((-27 - i) / 53);
		byte[] is = new byte[i_10_];
		for (int i_12_ = 0; i_10_ > i_12_; i_12_++) {
			char c = string.charAt(i_12_);
			if ((c <= 0 || c >= '\u0080') && (c < '\u00a0' || c > '\u00ff')) {
				if (c != '\u20ac') {
					if (c == '\u201a') {
						is[i_12_] = (byte) -126;
					} else if (c != '\u0192') {
						if (c != '\u201e') {
							if (c != '\u2026') {
								if (c == '\u2020') {
									is[i_12_] = (byte) -122;
								} else if (c == '\u2021') {
									is[i_12_] = (byte) -121;
								} else if (c != '\u02c6') {
									if (c != '\u2030') {
										if (c == '\u0160') {
											is[i_12_] = (byte) -118;
										} else if (c != '\u2039') {
											if (c != '\u0152') {
												if (c == '\u017d') {
													is[i_12_] = (byte) -114;
												} else if (c == '\u2018') {
													is[i_12_] = (byte) -111;
												} else if (c == '\u2019') {
													is[i_12_] = (byte) -110;
												} else if (c == '\u201c') {
													is[i_12_] = (byte) -109;
												} else if (c == '\u201d') {
													is[i_12_] = (byte) -108;
												} else if (c != '\u2022') {
													if (c == '\u2013') {
														is[i_12_] = (byte) -106;
													} else if (c != '\u2014') {
														if (c != '\u02dc') {
															if (c == '\u2122') {
																is[i_12_] = (byte) -103;
															} else if (c != '\u0161') {
																if (c == '\u203a') {
																	is[i_12_] = (byte) -101;
																} else if (c == '\u0153') {
																	is[i_12_] = (byte) -100;
																} else if (c == '\u017e') {
																	is[i_12_] = (byte) -98;
																} else if (c != '\u0178') {
																	is[i_12_] = (byte) 63;
																} else {
																	is[i_12_] = (byte) -97;
																}
															} else {
																is[i_12_] = (byte) -102;
															}
														} else {
															is[i_12_] = (byte) -104;
														}
													} else {
														is[i_12_] = (byte) -105;
													}
												} else {
													is[i_12_] = (byte) -107;
												}
											} else {
												is[i_12_] = (byte) -116;
											}
										} else {
											is[i_12_] = (byte) -117;
										}
									} else {
										is[i_12_] = (byte) -119;
									}
								} else {
									is[i_12_] = (byte) -120;
								}
							} else {
								is[i_12_] = (byte) -123;
							}
						} else {
							is[i_12_] = (byte) -124;
						}
					} else {
						is[i_12_] = (byte) -125;
					}
				} else {
					is[i_12_] = (byte) -128;
				}
			} else {
				is[i_12_] = (byte) c;
			}
		}
		return is;
	}

	private final void method2346(int i, int i_13_, Packet buffer, boolean newBas) {
		if (i == 1) {
			anInt2569 = readAnimId(buffer, newBas);
			anInt2597 = readAnimId(buffer, newBas);
		} else if (i != 2) {
			if (i != 3) {
				if (i == 4) {
					anInt2577 = readAnimId(buffer, newBas);
				} else if (i == 5) {
					anInt2583 = readAnimId(buffer, newBas);
				} else if (i != 6) {
					if (i == 7) {
						anInt2562 = readAnimId(buffer, newBas);
					} else if (i == 8) {
						anInt2566 = readAnimId(buffer, newBas);
					} else if (i != 9) {
						if (i == 26) {
							anInt2585 = (short) (buffer.g1() * 4);
							anInt2584 = (short) (buffer.g1() * 4);
						} else if (i == 27) {
							if (anIntArrayArray2558 == null) {
								anIntArrayArray2558 = new int[aClass62_2594.aClass312_713.colourData.length][];
							}
							int i_14_ = buffer.g1();
							anIntArrayArray2558[i_14_] = new int[6];
							for (int i_15_ = 0; i_15_ < 6; i_15_++) {
								anIntArrayArray2558[i_14_][i_15_] = buffer.g2b();
							}
						} else if (i != 28) {
							if (i == 29) {
								anInt2575 = buffer.g1();
							} else if (i == 30) {
								anInt2579 = buffer.g2();
							} else if (i == 31) {
								anInt2595 = buffer.g1();
							} else if (i != 32) {
								if (i == 33) {
									anInt2567 = buffer.g2b();
								} else if (i == 34) {
									anInt2593 = buffer.g1();
								} else if (i == 35) {
									anInt2588 = buffer.g2();
								} else if (i != 36) {
									if (i == 37) {
										anInt2568 = buffer.g1();
									} else if (i != 38) {
										if (i != 39) {
											if (i != 40) {
												if (i == 41) {
													anInt2599 = readAnimId(buffer, newBas);
												} else if (i == 42) {
													anInt2586 = readAnimId(buffer, newBas);
												} else if (i != 43) {
													if (i != 44) {
														if (i != 45) {
															if (i != 46) {
																if (i == 47) {
																	anInt2563 = readAnimId(buffer, newBas);
																} else if (i == 48) {
																	anInt2581 = readAnimId(buffer, newBas);
																} else if (i == 49) {
																	anInt2582 = readAnimId(buffer, newBas);
																} else if (i != 50) {
																	if (i == 51) {
																		anInt2552 = readAnimId(buffer, newBas);
																	} else if (i == 52) {
																		int i_16_ = buffer.g1();
																		anIntArray2570 = new int[i_16_];
																		anIntArray2574 = new int[i_16_];
																		for (int i_17_ = 0; i_17_ < i_16_; i_17_++) {
																			anIntArray2574[i_17_] = buffer.g2();
																			int i_18_ = buffer.g1();
																			anIntArray2570[i_17_] = i_18_;
																			anInt2571 += i_18_;
																		}
																	} else if (i != 53) {
																		if (i != 54) {
																			if (i != 55) {
																				if (i == 56) {
																					if (anIntArrayArray2561 == null) {
																						anIntArrayArray2561 = new int[aClass62_2594.aClass312_713.colourData.length][];
																					}
																					int i_19_ = buffer.g1();
																					anIntArrayArray2561[i_19_] = new int[3];
																					for (int i_20_ = 0; i_20_ < 3; i_20_++) {
																						anIntArrayArray2561[i_19_][i_20_] = buffer.g2b();
																					}
																				}
																			} else {
																				if (anIntArray2573 == null) {
																					anIntArray2573 = new int[aClass62_2594.aClass312_713.colourData.length];
																				}
																				int i_21_ = buffer.g1();
																				anIntArray2573[i_21_] = buffer.g2();
																			}
																		} else {
																			anInt2556 = buffer.g1() << 6;
																			anInt2580 = buffer.g1() << 6;
																		}
																	} else {
																		aBoolean2591 = false;
																	}
																} else {
																	anInt2596 = readAnimId(buffer, newBas);
																}
															} else {
																anInt2560 = readAnimId(buffer, newBas);
															}
														} else {
															anInt2578 = buffer.g2();
														}
													} else {
														anInt2557 = buffer.g2();
													}
												} else {
													anInt2572 = buffer.g2();
												}
											} else {
												anInt2551 = readAnimId(buffer, newBas);
											}
										} else {
											anInt2550 = readAnimId(buffer, newBas);
										}
									} else {
										anInt2592 = readAnimId(buffer, newBas);
									}
								} else {
									anInt2587 = buffer.g2b();
								}
							} else {
								anInt2576 = buffer.g2();
							}
						} else {
							int i_22_ = buffer.g1();
							anIntArray2559 = new int[i_22_];
							for (int i_23_ = 0; i_23_ < i_22_; i_23_++) {
								anIntArray2559[i_23_] = buffer.g1();
								if (anIntArray2559[i_23_] == 255) {
									anIntArray2559[i_23_] = -1;
								}
							}
						}
					} else {
						anInt2565 = readAnimId(buffer, newBas);
					}
				} else {
					anInt2554 = readAnimId(buffer, newBas);
				}
			} else {
				anInt2598 = readAnimId(buffer, newBas);
			}
		} else {
			anInt2564 = readAnimId(buffer, newBas);
		}
		if (i_13_ >= 0) {
			aClass113_2555 = null;
		}
	}

	private int readAnimId(Packet buffer, boolean newBas) {
		if (newBas) {
			return buffer.gSmart2or4s();
		}
		int seqid = buffer.g2();
		if (seqid == 65535) {
			seqid = -1;
		}
		return seqid;
	}

	public static void method2347(int i) {
		if (i != 1857370566) {
			aClass113_2555 = null;
		}
		aClass125_2589 = null;
		aClass113_2555 = null;
	}

	final int method2348(byte i) {
		if (anInt2569 != -1) {
			return anInt2569;
		}
		int i_24_ = -2 % ((i + 36) / 52);
		if (anIntArray2574 != null) {
			int i_25_ = (int) (anInt2571 * Math.random());
			int i_26_;
			for (i_26_ = 0; anIntArray2570[i_26_] <= i_25_; i_26_++) {
				i_25_ -= anIntArray2570[i_26_];
			}
			return anIntArray2574[i_26_];
		}
		return -1;
	}

	public Class280() {
		anInt2557 = -1;
		anInt2568 = -1;
		anInt2565 = -1;
		anInt2577 = -1;
		anIntArray2574 = null;
		anInt2572 = -1;
		anInt2585 = 0;
		anIntArray2570 = null;
		anInt2581 = -1;
		anInt2583 = -1;
		anInt2566 = -1;
		anInt2569 = -1;
		anInt2571 = 0;
		anInt2578 = -1;
		anInt2575 = 0;
		anInt2576 = 0;
		anInt2554 = -1;
		anInt2584 = 0;
		anInt2592 = -1;
		anInt2551 = -1;
		anInt2590 = -1;
		anInt2579 = 0;
		aBoolean2591 = true;
		anInt2560 = -1;
		anInt2550 = -1;
		anInt2580 = 0;
		anInt2564 = -1;
		anInt2582 = -1;
		anInt2596 = -1;
		anInt2597 = -1;
		anInt2556 = 0;
		anInt2595 = 0;
		anInt2593 = 0;
		anInt2586 = -1;
		anInt2598 = -1;
		anInt2563 = -1;
		anInt2588 = 0;
		anInt2587 = 0;
		anInt2599 = -1;
	}
}
