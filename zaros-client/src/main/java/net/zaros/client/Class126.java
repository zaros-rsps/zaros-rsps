package net.zaros.client;

/* Class126 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class126 {
	static NodeDeque aClass155_1289;
	static int[] anIntArray1290 = {0, 2, 2, 2, 1, 1, 3, 3, 1, 3, 3, 4, 4};
	static float[] aFloatArray1291;
	static float aFloat1292;

	static final boolean method1082(int i, int i_0_, int i_1_) {
		int i_2_ = 42 / ((i - 35) / 56);
		boolean bool = ((i_0_ & 0x37) == 0 ? Class184.method1857(i_0_, i_1_, (byte) -118) : Class47.method599((byte) -46, i_1_, i_0_));
		return (Class157.method1589(i_1_, -12205, i_0_) | (i_1_ & 0x10000) != 0 | bool);
	}

	static final void method1083(int i, boolean bool, Class296_Sub42 class296_sub42) {
		if (i != 3)
			method1082(-47, -58, -107);
		if (!class296_sub42.aBoolean4929) {
			if (class296_sub42.aBoolean4935 && class296_sub42.anInt4923 >= 1 && class296_sub42.anInt4928 >= 1 && class296_sub42.anInt4923 <= Class198.currentMapSizeX - 2 && Class296_Sub38.currentMapSizeY - 2 >= class296_sub42.anInt4928 && (class296_sub42.anInt4936 < 0 || Class338_Sub8_Sub1.method3604(class296_sub42.anInt4936, (byte) 103, (class296_sub42.anInt4933)))) {
				if (bool)
					Class338_Sub3_Sub4.method3567(class296_sub42.anInt4932, class296_sub42.anInt4923, class296_sub42.anInt4928, (class296_sub42.aClass375_4930), class296_sub42.anInt4927, (byte) 80);
				else
					Class218.method2042(class296_sub42.anInt4927, class296_sub42.anInt4923, -1, class296_sub42.anInt4932, class296_sub42.anInt4933, class296_sub42.anInt4928, class296_sub42.anInt4934, true, class296_sub42.anInt4936);
				class296_sub42.aBoolean4935 = false;
				if (bool || class296_sub42.anInt4925 != class296_sub42.anInt4936 || class296_sub42.anInt4925 != -1) {
					if (!bool && class296_sub42.anInt4925 == class296_sub42.anInt4936 && class296_sub42.anInt4924 == class296_sub42.anInt4934 && (class296_sub42.anInt4931 == class296_sub42.anInt4933))
						class296_sub42.unlink();
				} else
					class296_sub42.unlink();
			}
		} else if (class296_sub42.anInt4925 < 0 || Class338_Sub8_Sub1.method3604(class296_sub42.anInt4925, (byte) 66, (class296_sub42.anInt4931))) {
			if (!bool)
				Class218.method2042(class296_sub42.anInt4927, class296_sub42.anInt4923, -1, class296_sub42.anInt4932, class296_sub42.anInt4931, class296_sub42.anInt4928, class296_sub42.anInt4924, true, class296_sub42.anInt4925);
			else
				Class338_Sub3_Sub4.method3567(class296_sub42.anInt4932, class296_sub42.anInt4923, class296_sub42.anInt4928, null, class296_sub42.anInt4927, (byte) 80);
			class296_sub42.unlink();
		}
	}

	public static void method1084(byte i) {
		aClass155_1289 = null;
		anIntArray1290 = null;
		aFloatArray1291 = null;
		if (i <= 5)
			aClass155_1289 = null;
	}

	static {
		aClass155_1289 = new NodeDeque();
		aFloatArray1291 = new float[4];
	}
}
