package net.zaros.client;

import net.zaros.client.configs.objtype.ObjType;

/* Class41_Sub29 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub29 extends Class41 {
	static IncomingPacket aClass231_3819 = new IncomingPacket(34, 6);
	static int anInt3820 = -1;
	static ModeWhere modeWhere;

	final int method515(int i) {
		if (i <= 114)
			return 60;
		return anInt389;
	}

	final int method383(byte i) {
		if (i != 110)
			return 20;
		return 1;
	}

	Class41_Sub29(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	public static void method516(byte i) {
		if (i != 27)
			method519(null, -108, 79, -20);
		aClass231_3819 = null;
		modeWhere = null;
	}

	final int method380(int i, byte i_0_) {
		if (aClass296_Sub50_392.method3054(74))
			return 3;
		if (aClass296_Sub50_392.method3050(28520) == Class363.runescape) {
			if (i == 0) {
				if (aClass296_Sub50_392.aClass41_Sub1_5008.method389(122) == 1)
					return 2;
				if (aClass296_Sub50_392.aClass41_Sub26_4991.method497(115) == 1)
					return 2;
				if (aClass296_Sub50_392.aClass41_Sub2_5013.method393(122) > 0)
					return 2;
			}
			return 1;
		}
		if (i_0_ != 41)
			method518(-101, (byte) -93, null, -15, -106);
		return 3;
	}

	final boolean method517(int i) {
		if (aClass296_Sub50_392.method3054(76))
			return false;
		if (aClass296_Sub50_392.method3050(28520) == Class363.runescape)
			return true;
		if (i != -25952)
			modeWhere = null;
		return false;
	}

	final void method381(int i, byte i_1_) {
		if (i_1_ != -110)
			method516((byte) 17);
		anInt389 = i;
	}

	final void method386(int i) {
		if (aClass296_Sub50_392.method3050(28520) != Class363.runescape)
			anInt389 = 1;
		else if (aClass296_Sub50_392.method3054(75))
			anInt389 = 0;
		if (i != 2)
			method516((byte) 126);
		if (anInt389 != 0 && anInt389 != 1)
			anInt389 = method383((byte) 110);
	}

	static final void method518(int i, byte i_2_, Class296_Sub2 class296_sub2, int i_3_, int i_4_) {
		long l = (long) (i << 14 | i_4_ << 28 | i_3_);
		Class296_Sub56 class296_sub56 = (Class296_Sub56) Class296_Sub11.aClass263_4647.get(l);
		if (i_2_ == 92) {
			if (class296_sub56 == null) {
				class296_sub56 = new Class296_Sub56();
				Class296_Sub11.aClass263_4647.put(l, class296_sub56);
				class296_sub56.aClass155_5080.addLast((byte) -33, class296_sub2);
			} else {
				ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(class296_sub2.anInt4591);
				int i_5_ = class158.cost;
				if (class158.stackability == 1)
					i_5_ *= class296_sub2.anInt4592 + 1;
				for (Class296_Sub2 class296_sub2_6_ = (Class296_Sub2) class296_sub56.aClass155_5080.removeFirst((byte) 126); class296_sub2_6_ != null; class296_sub2_6_ = ((Class296_Sub2) class296_sub56.aClass155_5080.removeNext(1001))) {
					class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(class296_sub2_6_.anInt4591);
					int i_7_ = class158.cost;
					if (class158.stackability == 1)
						i_7_ *= class296_sub2_6_.anInt4592 + 1;
					if (i_7_ < i_5_) {
						Class296_Sub36.method2765(0, class296_sub2, class296_sub2_6_);
						return;
					}
				}
				class296_sub56.aClass155_5080.addLast((byte) -40, class296_sub2);
			}
		}
	}

	Class41_Sub29(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	static final void method519(Class338_Sub3 class338_sub3, int i, int i_8_, int i_9_) {
		if (i_8_ < Class228.anInt2201) {
			Class247 class247 = (Class338_Sub2.aClass247ArrayArrayArray5195[i][i_8_ + 1][i_9_]);
			if (class247 != null && class247.aClass338_Sub3_Sub5_2346 != null && class247.aClass338_Sub3_Sub5_2346.method3468(-55))
				class338_sub3.method3467(0, Js5TextureLoader.anInt3440, class247.aClass338_Sub3_Sub5_2346, true, 0, 111, Class16_Sub2.aHa3733);
		}
		if (i_9_ < Class228.anInt2201) {
			Class247 class247 = (Class338_Sub2.aClass247ArrayArrayArray5195[i][i_8_][i_9_ + 1]);
			if (class247 != null && class247.aClass338_Sub3_Sub5_2346 != null && class247.aClass338_Sub3_Sub5_2346.method3468(-82))
				class338_sub3.method3467(Js5TextureLoader.anInt3440, 0, class247.aClass338_Sub3_Sub5_2346, true, 0, 112, Class16_Sub2.aHa3733);
		}
		if (i_8_ < Class228.anInt2201 && i_9_ < Class368_Sub12.anInt5488) {
			Class247 class247 = (Class338_Sub2.aClass247ArrayArrayArray5195[i][i_8_ + 1][i_9_ + 1]);
			if (class247 != null && class247.aClass338_Sub3_Sub5_2346 != null && class247.aClass338_Sub3_Sub5_2346.method3468(-124))
				class338_sub3.method3467(Js5TextureLoader.anInt3440, Js5TextureLoader.anInt3440, class247.aClass338_Sub3_Sub5_2346, true, 0, 85, Class16_Sub2.aHa3733);
		}
		if (i_8_ < Class228.anInt2201 && i_9_ > 0) {
			Class247 class247 = (Class338_Sub2.aClass247ArrayArrayArray5195[i][i_8_ + 1][i_9_ - 1]);
			if (class247 != null && class247.aClass338_Sub3_Sub5_2346 != null && class247.aClass338_Sub3_Sub5_2346.method3468(-43))
				class338_sub3.method3467(-Js5TextureLoader.anInt3440, Js5TextureLoader.anInt3440, class247.aClass338_Sub3_Sub5_2346, true, 0, -49, Class16_Sub2.aHa3733);
		}
	}

	static final void method520(int i) {
		Class333.method3417(88);
		int i_10_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022.method456(119);
		do {
			if (i_10_ != 2) {
				if (i_10_ != 3)
					break;
			} else {
				Class357.method3709(16777215, 100, Class241.anInt2301, 100, Class384.anInt3254, Class41_Sub13.aHa3774);
				break;
			}
			Class338_Sub3_Sub1_Sub4.method3543(-124, Class41_Sub13.aHa3774, 2, Class94.anInt1017, Class296_Sub15_Sub3.anInt6011, Class384.anInt3254, Class241.anInt2301, 2);
		} while (false);
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022.method459(true))
			Class303.method3268(Class230.aCanvas2209, -2);
		if (Class41_Sub13.aHa3774 != null)
			Graphic.method292((byte) 119);
		Class296_Sub39_Sub10.aBoolean6177 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022.method456(122) != 0;
		Class368_Sub5_Sub2.aBoolean6597 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022.method459(true);
		i_10_ = -12 / ((46 - i) / 49);
	}
}
