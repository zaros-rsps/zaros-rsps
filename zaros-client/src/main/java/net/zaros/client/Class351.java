package net.zaros.client;

/* Class351 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class351 {
	static Class247[][][] aClass247ArrayArrayArray3041;
	static int[] anIntArray3042 = new int[1];

	public Class351() {
		/* empty */
	}

	static final void method3681(int i, int i_0_, byte i_1_) {
		BITConfigDefinition.anInt2604 = i - Class106.anInt1117;
		Class296_Sub51_Sub5.anInt6365 = -Class106.anInt1111 + i_0_;
		if (i_1_ != 10)
			anIntArray3042 = null;
	}

	static final int method3682(int i) {
		boolean bool = false;
		boolean bool_2_ = false;
		boolean bool_3_ = false;
		if (Class252.aClass398_2383.aBoolean3332 && !Class252.aClass398_2383.aBoolean3321) {
			bool = true;
			if (Class360_Sub3.aClass296_Sub28_5309.anInt4797 < 512 && Class360_Sub3.aClass296_Sub28_5309.anInt4797 != 0)
				bool = false;
			if (Class398.osName.startsWith("win")) {
				bool_3_ = true;
				bool_2_ = true;
			} else
				bool_2_ = true;
		}
		if (ha.aBoolean1294)
			bool = false;
		if (Class338_Sub3_Sub3_Sub1.aBoolean6621)
			bool_2_ = false;
		if (Class42_Sub3.aBoolean3845)
			bool_3_ = false;
		if (!bool && !bool_2_ && !bool_3_)
			return StaticMethods.method2456(-90);
		int i_4_ = -1;
		int i_5_ = -1;
		int i_6_ = -1;
		if (bool) {
			try {
				i_4_ = Class296_Sub51_Sub21.method3134(2, 1000, (byte) -121);
			} catch (Exception exception) {
				/* empty */
			}
		}
		do {
			if (bool_3_) {
				try {
					i_6_ = Class296_Sub51_Sub21.method3134(3, 1000, (byte) -121);
					if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(i + 633) == 3) {
						Class33 class33 = Class41_Sub13.aHa3774.s();
						long l = class33.aLong330 & 0xffffffffffffL;
						int i_7_ = class33.anInt333;
						if (i_7_ != 4318) {
							if (i_7_ != 4098)
								break;
						} else {
							bool_2_ = bool_2_ & l >= 64425238954L;
							break;
						}
						bool_2_ = bool_2_ & l >= 60129613779L;
					}
				} catch (Exception exception) {
					/* empty */
				}
			}
		} while (false);
		if (bool_2_) {
			try {
				i_5_ = Class296_Sub51_Sub21.method3134(1, 1000, (byte) -121);
			} catch (Exception exception) {
				/* empty */
			}
		}
		if (i_4_ == -1 && i_5_ == -1 && i_6_ == -1)
			return StaticMethods.method2456(i + 468);
		if (i != -513)
			aClass247ArrayArrayArray3041 = null;
		i_6_ *= 1.1F;
		i_5_ *= 1.1F;
		if (i_4_ <= i_6_ || i_5_ >= i_4_) {
			if (i_6_ > i_5_)
				return Class122_Sub2.method1046(3, i_6_, false);
			return Class122_Sub2.method1046(1, i_5_, false);
		}
		return Class180.method1815(63, i_4_);
	}

	public static void method3683(int i) {
		anIntArray3042 = null;
		if (i == -20291)
			aClass247ArrayArrayArray3041 = null;
	}

	static final boolean method3684(String string, String str0, String string_9_, String string_10_) {
		if (str0 == null || string == null)
			return false;
		if (str0.startsWith("#") || string.startsWith("#"))
			return str0.equals(string);
		return string_9_.equals(string_10_);
	}
}
