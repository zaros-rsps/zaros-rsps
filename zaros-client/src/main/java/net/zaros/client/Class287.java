package net.zaros.client;

/* Class287 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class287 {
	static int anInt2644;
	static Class289 aClass289_2645 = new Class289();

	static final void method2385(Js5 class138, boolean bool, int i, int i_0_, int i_1_, int i_2_, int i_3_) {
		if (i == -2899) {
			if (i_0_ > 0) {
				Class368_Sub1.anInt5423 = i_3_;
				Class296_Sub50.aBoolean5025 = bool;
				TextureOperation.aClass138_5040 = class138;
				Class392.anInt3488 = i_1_;
				Class249.aClass296_Sub45_Sub4_2357 = null;
				Class296_Sub51_Sub7.anInt6371 = i_2_;
				ReferenceWrapper.anInt6128 = 1;
				Class296_Sub30.anInt4816 = (Class235.aClass296_Sub45_Sub4_2229.method3029(i + 2901) / i_0_);
				if (Class296_Sub30.anInt4816 < 1)
					Class296_Sub30.anInt4816 = 1;
			} else
				Class338_Sub8.method3602(i_3_, class138, bool, -122, i_2_, i_1_);
		}
	}

	static final int npcChildID(int i) {
		return i & 0x7f;
	}

	public static void method2387(byte i) {
		aClass289_2645 = null;
		int i_4_ = -28 % ((i - 28) / 58);
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	public Class287() {
		/* empty */
	}

	static final boolean method2389(int i, byte i_7_) {
		if (i_7_ != 26)
			return false;
		if (i != 0 && i != 2)
			return false;
		return true;
	}

	static final boolean method2390(byte i, int i_8_, int i_9_) {
		if (i != -105)
			method2389(-2, (byte) 117);
		if (!((i_8_ & 0x70000) != 0 | r.method2859(i_8_, (byte) -15, i_9_)) && !Class296_Sub51_Sub26.method3150(i_8_, (byte) 70, i_9_))
			return false;
		return true;
	}
}
