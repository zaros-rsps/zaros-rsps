package net.zaros.client;
/* Class41 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Frame;

abstract class Class41 {
	static boolean aBoolean388 = true;
	int anInt389;
	static boolean aBoolean390;
	static OutgoingPacket aClass311_391 = new OutgoingPacket(40, 8);
	Class296_Sub50 aClass296_Sub50_392;

	static long method379(long l, long l_0_) {
		return l & l_0_;
	}

	abstract int method380(int i, byte i_1_);

	abstract void method381(int i, byte i_2_);

	public static void method382(byte i) {
		aClass311_391 = null;
		if (i >= -83)
			method384(true, false);
	}

	abstract int method383(byte i);

	static final void method384(boolean bool, boolean bool_3_) {
		int i = Class36.anInt356;
		int i_4_ = Class389.anInt3281;
		if (bool && Class296_Sub39_Sub10.aBoolean6177) {
			i <<= 1;
			i_4_ = -i;
		}
		Class41_Sub13.aHa3774.f(i_4_, i);
		if (bool_3_)
			aBoolean388 = true;
	}

	final void method385(int i, boolean bool) {
		if (bool)
			aBoolean390 = true;
		if (method380(i, (byte) 41) != 3)
			method381(i, (byte) -110);
	}

	abstract void method386(int i);

	static final void method387(Class338 class338, int i, Class338 class338_5_) {
		if (class338.aClass338_2972 != null)
			class338.method3438(false);
		class338.aClass338_2972 = class338_5_.aClass338_2972;
		class338.aClass338_2971 = class338_5_;
		class338.aClass338_2972.aClass338_2971 = class338;
		class338.aClass338_2971.aClass338_2972 = class338;
		if (i != -2)
			aBoolean390 = false;
	}

	static final void method388(int i, Frame frame, Class398 class398) {
		for (;;) {
			Class278 class278 = class398.method4124(111, frame);
			while (class278.anInt2540 == 0)
				Class106_Sub1.method942(10L, 0);
			if (class278.anInt2540 == 1)
				break;
			Class106_Sub1.method942(100L, i ^ 0x40a5);
		}
		frame.setVisible(false);
		frame.dispose();
		if (i != 16549)
			method388(23, null, null);
	}

	Class41(Class296_Sub50 class296_sub50) {
		aClass296_Sub50_392 = class296_sub50;
		anInt389 = method383((byte) 110);
	}

	Class41(int i, Class296_Sub50 class296_sub50) {
		anInt389 = i;
		aClass296_Sub50_392 = class296_sub50;
	}
}
