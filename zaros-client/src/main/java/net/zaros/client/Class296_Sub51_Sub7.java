package net.zaros.client;

/* Class296_Sub51_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub7 extends TextureOperation {
	static int[] anIntArray6370 = new int[8];
	static int anInt6371;

	final int[][] get_colour_output(int i, int i_0_) {
		if (i_0_ != 17621)
			anIntArray6370 = null;
		int[][] is = aClass86_5034.method823((byte) 32, i);
		if (aClass86_5034.aBoolean939) {
			int[] is_1_ = is[0];
			int[] is_2_ = is[1];
			int[] is_3_ = is[2];
			for (int i_4_ = 0; Class41_Sub10.anInt3769 > i_4_; i_4_++) {
				method3095(i, (byte) -49, i_4_);
				int[][] is_5_ = this.method3075((byte) 120, 0, Class368_Sub13.anInt5506);
				is_1_[i_4_] = is_5_[0][Class368_Sub3.anInt5435];
				is_2_[i_4_] = is_5_[1][Class368_Sub3.anInt5435];
				is_3_[i_4_] = is_5_[2][Class368_Sub3.anInt5435];
			}
		}
		return is;
	}

	public Class296_Sub51_Sub7() {
		super(1, false);
	}

	final int[] get_monochrome_output(int i, int i_6_) {
		int[] is = aClass318_5035.method3335(i_6_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			for (int i_7_ = 0; Class41_Sub10.anInt3769 > i_7_; i_7_++) {
				method3095(i_6_, (byte) -120, i_7_);
				int[] is_8_ = this.method3064(0, i, Class368_Sub13.anInt5506);
				is[i_7_] = is_8_[Class368_Sub3.anInt5435];
			}
		}
		if (i != 0)
			anInt6371 = 33;
		return is;
	}

	final void method3071(int i, Packet class296_sub17, int i_9_) {
		if (i <= -84) {
			if (i_9_ == 0)
				monochromatic = class296_sub17.g1() == 1;
		}
	}

	public static void method3094(boolean bool) {
		anIntArray6370 = null;
		if (bool != true)
			method3094(true);
	}

	private final void method3095(int i, byte i_10_, int i_11_) {
		int i_12_ = Class33.anIntArray334[i_11_];
		if (i_10_ < -1) {
			int i_13_ = Class294.anIntArray2686[i];
			float f = (float) Math.atan2((double) (i_12_ - 2048), (double) (i_13_ - 2048));
			if (!((double) f >= -3.141592653589793) || !((double) f <= -2.356194490192345)) {
				if (!((double) f <= -1.5707963267948966) || !((double) f >= -2.356194490192345)) {
					if (!((double) f <= -0.7853981633974483) || !((double) f >= -1.5707963267948966)) {
						if (!(f <= 0.0F) || !((double) f >= -0.7853981633974483)) {
							if (f >= 0.0F && (double) f <= 0.7853981633974483) {
								Class368_Sub13.anInt5506 = -i + Class296_Sub35_Sub2.anInt6114;
								Class368_Sub3.anInt5435 = -i_11_ + Class41_Sub10.anInt3769;
							} else if ((double) f >= 0.7853981633974483 && (double) f <= 1.5707963267948966) {
								Class368_Sub13.anInt5506 = Class296_Sub35_Sub2.anInt6114 - i_11_;
								Class368_Sub3.anInt5435 = Class41_Sub10.anInt3769 - i;
							} else if (!((double) f >= 1.5707963267948966) || !((double) f <= 2.356194490192345)) {
								if ((double) f >= 2.356194490192345 && (double) f <= 3.141592653589793) {
									Class368_Sub13.anInt5506 = i;
									Class368_Sub3.anInt5435 = Class41_Sub10.anInt3769 - i_11_;
								}
							} else {
								Class368_Sub3.anInt5435 = i;
								Class368_Sub13.anInt5506 = Class296_Sub35_Sub2.anInt6114 - i_11_;
							}
						} else {
							Class368_Sub3.anInt5435 = i_11_;
							Class368_Sub13.anInt5506 = -i + Class296_Sub35_Sub2.anInt6114;
						}
					} else {
						Class368_Sub3.anInt5435 = Class41_Sub10.anInt3769 - i;
						Class368_Sub13.anInt5506 = i_11_;
					}
				} else {
					Class368_Sub13.anInt5506 = i_11_;
					Class368_Sub3.anInt5435 = i;
				}
			} else {
				Class368_Sub13.anInt5506 = i;
				Class368_Sub3.anInt5435 = i_11_;
			}
			Class368_Sub13.anInt5506 &= Class67.anInt753;
			Class368_Sub3.anInt5435 &= Class41_Sub25.anInt3803;
		}
	}
}
