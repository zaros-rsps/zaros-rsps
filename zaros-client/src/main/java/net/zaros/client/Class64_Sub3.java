package net.zaros.client;
import jagdx.IDirect3DBaseTexture;
import jagdx.IDirect3DTexture;
import jagdx.PixelBuffer;
import jagdx.aj;

final class Class64_Sub3 extends Class64 implements Interface6_Impl1 {
	private int anInt3896;
	boolean aBoolean3897;
	private int anInt3898;
	private IDirect3DTexture anIDirect3DTexture3899;
	boolean aBoolean3900;

	public final int method70(int i) {
		int i_0_ = -120 / ((i + 33) / 37);
		return anInt3896;
	}

	public final void method67(boolean bool, int i, int i_1_, int i_2_, byte[] is, int i_3_, int i_4_, int i_5_, Class202 class202) {
		if (aClass202_729 != class202 || Class67.aClass67_745 != aClass67_730)
			throw new RuntimeException();
		PixelBuffer pixelbuffer = aHa_Sub1_Sub2_731.aPixelBuffer5867;
		if (bool != true)
			anInt3896 = 50;
		int i_6_ = anIDirect3DTexture3899.LockRect(0, i_3_, i_2_, i_1_, i_5_, 0, pixelbuffer);
		if (aj.a(i_6_, (int) 19)) {
			i *= aClass202_729.anInt2042;
			i_1_ *= aClass202_729.anInt2042;
			int i_7_ = pixelbuffer.getRowPitch();
			if (i_1_ == i_7_ && i_1_ == i)
				pixelbuffer.a(is, i_4_, 0, i_1_ * i_5_);
			else {
				for (int i_8_ = 0; i_5_ > i_8_; i_8_++)
					pixelbuffer.a(is, i_8_ * i + i_4_, i_8_ * i_7_, i_1_);
			}
			anIDirect3DTexture3899.UnlockRect(0);
		}
	}

	Class64_Sub3(ha_Sub1_Sub2 var_ha_Sub1_Sub2, int i, int i_9_, boolean bool, int[] is, int i_10_, int i_11_) {
		super(var_ha_Sub1_Sub2, za_Sub2.aClass202_6555, Class67.aClass67_745, bool && var_ha_Sub1_Sub2.aBoolean5869, i * i_9_);
		if (!aHa_Sub1_Sub2_731.aBoolean5879) {
			anInt3896 = Class8.get_next_high_pow2(i);
			anInt3898 = Class8.get_next_high_pow2(i_9_);
		} else {
			anInt3898 = i_9_;
			anInt3896 = i;
		}
		if (!bool)
			anIDirect3DTexture3899 = aHa_Sub1_Sub2_731.anIDirect3DDevice5865.a(anInt3896, anInt3898, 1, 0, 21, 1);
		else
			anIDirect3DTexture3899 = aHa_Sub1_Sub2_731.anIDirect3DDevice5865.a(anInt3896, anInt3898, 0, 1024, 21, 1);
		PixelBuffer pixelbuffer = aHa_Sub1_Sub2_731.aPixelBuffer5867;
		int i_12_ = anIDirect3DTexture3899.LockRect(0, 0, 0, i, i_9_, 0, pixelbuffer);
		if (aj.a(i_12_, (int) 102)) {
			if (i_11_ == 0)
				i_11_ = i;
			int i_13_ = pixelbuffer.getRowPitch();
			if (i_13_ != i * 4 || i != i_11_) {
				for (int i_14_ = 0; i_14_ < i_9_; i_14_++)
					pixelbuffer.b(is, i_10_ + i_11_ * i_14_, i_13_ * i_14_, i);
			} else
				pixelbuffer.b(is, i_10_, 0, i * i_9_);
			anIDirect3DTexture3899.UnlockRect(0);
		}
	}

	public final float method69(int i, float f) {
		if (i > -86)
			return 0.10468999F;
		return f / (float) anInt3896;
	}

	public final void method28(int i) {
		if (i != -9994)
			aBoolean3897 = false;
		aHa_Sub1_Sub2_731.method1251(this, false);
	}

	public final void method68(int i, int i_15_, int i_16_, int[] is, int i_17_, int i_18_, int i_19_, int i_20_) {
		if (za_Sub2.aClass202_6555 != aClass202_729 || aClass67_730 != Class67.aClass67_745)
			throw new RuntimeException();
		if (i_17_ == -25989) {
			PixelBuffer pixelbuffer = aHa_Sub1_Sub2_731.aPixelBuffer5867;
			int i_21_ = anIDirect3DTexture3899.LockRect(0, i, i_20_, i_15_, i_19_, 0, pixelbuffer);
			if (aj.a(i_21_, (int) 43)) {
				int i_22_ = pixelbuffer.getRowPitch();
				if (i_22_ == i_15_ * 4 && i_15_ == i_18_)
					pixelbuffer.b(is, i_16_, 0, i_19_ * i_15_);
				else {
					for (int i_23_ = 0; i_19_ > i_23_; i_23_++)
						pixelbuffer.b(is, i_16_ + i_18_ * i_23_, i_23_ * i_22_, i_15_);
				}
				anIDirect3DTexture3899.UnlockRect(0);
			}
		}
	}

	final IDirect3DBaseTexture method706(int i) {
		if (i != 12931)
			return null;
		return anIDirect3DTexture3899;
	}

	public final void method64(byte i, boolean bool, boolean bool_24_) {
		aBoolean3900 = bool;
		aBoolean3897 = bool_24_;
		if (i != 49)
			method70(12);
	}

	public final void method29(Class131 class131, int i) {
		super.method29(class131, i);
	}

	public final void method65(int i, int[] is, int i_25_, int i_26_, int i_27_, int i_28_, int i_29_) {
		if (za_Sub2.aClass202_6555 != aClass202_729 || aClass67_730 != Class67.aClass67_745)
			throw new RuntimeException();
		PixelBuffer pixelbuffer = aHa_Sub1_Sub2_731.aPixelBuffer5867;
		if (i_28_ == 16438) {
			int i_30_ = anIDirect3DTexture3899.LockRect(0, i, i_29_, i_26_, i_27_, 16, pixelbuffer);
			if (aj.a(i_30_, (int) 124)) {
				int i_31_ = pixelbuffer.getRowPitch();
				if (i_26_ * 4 == i_31_)
					pixelbuffer.a(is, i_25_, 0, is.length);
				else {
					for (int i_32_ = 0; i_32_ < i_27_; i_32_++)
						pixelbuffer.a(is, i_32_ * i_26_ + i_25_, i_31_ * i_32_, i_26_);
				}
				anIDirect3DTexture3899.UnlockRect(0);
			}
		}
	}

	Class64_Sub3(ha_Sub1_Sub2 var_ha_Sub1_Sub2, Class202 class202, int i, int i_33_, boolean bool, byte[] is, int i_34_, int i_35_) {
		super(var_ha_Sub1_Sub2, class202, Class67.aClass67_745, bool && var_ha_Sub1_Sub2.aBoolean5869, i_33_ * i);
		if (!aHa_Sub1_Sub2_731.aBoolean5879) {
			anInt3896 = Class8.get_next_high_pow2(i);
			anInt3898 = Class8.get_next_high_pow2(i_33_);
		} else {
			anInt3896 = i;
			anInt3898 = i_33_;
		}
		if (!bool)
			anIDirect3DTexture3899 = (aHa_Sub1_Sub2_731.anIDirect3DDevice5865.a(anInt3896, anInt3898, 1, 0, ha_Sub1_Sub2.method1253(0, Class67.aClass67_745, aClass202_729), 1));
		else
			anIDirect3DTexture3899 = (aHa_Sub1_Sub2_731.anIDirect3DDevice5865.a(anInt3896, anInt3898, 0, 1024, ha_Sub1_Sub2.method1253(0, Class67.aClass67_745, aClass202_729), 1));
		PixelBuffer pixelbuffer = aHa_Sub1_Sub2_731.aPixelBuffer5867;
		int i_36_ = anIDirect3DTexture3899.LockRect(0, 0, 0, i, i_33_, 0, pixelbuffer);
		if (aj.a(i_36_, (int) 118)) {
			if (i_35_ == 0)
				i_35_ = i;
			i_35_ *= aClass202_729.anInt2042;
			i *= aClass202_729.anInt2042;
			int i_37_ = pixelbuffer.getRowPitch();
			if (i_37_ == i && i_35_ == i)
				pixelbuffer.a(is, i_34_, 0, i * i_33_);
			else {
				for (int i_38_ = 0; i_38_ < i_33_; i_38_++)
					pixelbuffer.a(is, i_34_ + i_38_ * i_35_, i_38_ * i_37_, i);
			}
			anIDirect3DTexture3899.UnlockRect(0);
		}
	}

	public final float method71(float f, byte i) {
		int i_39_ = -91 / ((52 - i) / 54);
		return f / (float) anInt3898;
	}

	Class64_Sub3(ha_Sub1_Sub2 var_ha_Sub1_Sub2, Class202 class202, Class67 class67, int i, int i_40_) {
		super(var_ha_Sub1_Sub2, class202, class67, false, i_40_ * i);
		if (aHa_Sub1_Sub2_731.aBoolean5879) {
			anInt3898 = i_40_;
			anInt3896 = i;
		} else {
			anInt3896 = Class8.get_next_high_pow2(i);
			anInt3898 = Class8.get_next_high_pow2(i_40_);
		}
		anIDirect3DTexture3899 = (aHa_Sub1_Sub2_731.anIDirect3DDevice5865.a(i, i_40_, 1, 0, ha_Sub1_Sub2.method1253(0, aClass67_730, aClass202_729), 1));
	}

	public final boolean method72(int i) {
		int i_41_ = 41 / ((60 - i) / 37);
		return true;
	}

	public final int method66(int i) {
		if (i != 3776)
			return 124;
		return anInt3898;
	}
}
