package net.zaros.client;

/* Class131 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class131 {
	static IncomingPacket aClass231_1340;
	static int[] anIntArray1341 = new int[2];
	static int anInt1342;
	static double aDouble1343;

	static final void method1381(int i) {
		Class187.anInt1910 = i;
		Class220.anInt2150 = -1;
		Class377.loginConnection = Class296_Sub45_Sub2.aClass204_6276;
		String string = null;
		if (Class294.sessionKey != null) {
			Packet class296_sub17 = new Packet(Class294.sessionKey);
			string = Class296_Sub13.decodeBase37(class296_sub17.g8());
			Class238.aLong3633 = class296_sub17.g8();
		}
		if (string == null)
			BITConfigDefinition.method2352(-2, 35);
		else
			Class41_Sub27.method508(false, string, "", true, (byte) -100);
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	public Class131() {
		/* empty */
	}

	static final void method1382(int i) {
		Class368_Sub20.aClass217_5550.method2033(false);
		for (int i_0_ = 0; i_0_ < 32; i_0_++)
			Js5TextureLoader.aLongArray3446[i_0_] = 0L;
		if (i > 4) {
			for (int i_1_ = 0; i_1_ < 32; i_1_++)
				ModeWhat.aLongArray1188[i_1_] = 0L;
			Class36.anInt353 = 0;
		}
	}

	public static void method1383(int i) {
		anIntArray1341 = null;
		aClass231_1340 = null;
		if (i != -33)
			aDouble1343 = -0.2075965680621688;
	}

	static {
		aClass231_1340 = new IncomingPacket(113, 1);
	}
}
