package net.zaros.client;

/* Class54 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class54 implements Runnable {
	private Interface2 anInterface2_644 = new Class78();
	private volatile boolean aBoolean645;
	private boolean aBoolean646;
	private int anInt647;
	private Interface2 anInterface2_648 = null;
	private Class326 aClass326_649;
	private int anInt650;
	private String aString651;
	private long aLong652;
	private long aLong653;

	final synchronized void method643(String string, int i, Class326 class326, int i_0_, long l) {
		aString651 = string;
		anInt650 = i_0_;
		aLong652 = l;
		if (i <= -100)
			aClass326_649 = class326;
	}

	public final void run() {
		while (!aBoolean645) {
			long l = Class72.method771(-119);
			synchronized (this) {
				try {
					anInt647++;
					if (anInterface2_644 instanceof Class78)
						anInterface2_644.method8((byte) -127, aBoolean646);
					else {
						long l_1_ = Class72.method771(-106);
						if (Class41_Sub13.aHa3774 == null || anInterface2_648 == null || anInterface2_648.method6((byte) 127) == 0 || (aLong653 < l_1_ - (long) anInterface2_648.method6((byte) 38))) {
							if (anInterface2_648 != null) {
								aBoolean646 = true;
								anInterface2_648.method11(false);
								anInterface2_648 = null;
							}
							if (aBoolean646) {
								Class367.method3802(-1);
								if (Class41_Sub13.aHa3774 != null)
									Class41_Sub13.aHa3774.GA(0);
							}
							anInterface2_644.method8((byte) -127, (aBoolean646 || (Class41_Sub13.aHa3774 != null && Class41_Sub13.aHa3774.v())));
						} else {
							int i = (int) ((l_1_ + -aLong653) * 255L / (long) anInterface2_648.method6((byte) 32));
							int i_2_ = -i + 255;
							i = i << 24 | 0xffffff;
							i_2_ = i_2_ << 24 | 0xffffff;
							Class367.method3802(-1);
							Class41_Sub13.aHa3774.GA(0);
							Sprite class397 = Class41_Sub13.aHa3774.a(Class241.anInt2301, Class384.anInt3254, true);
							Class41_Sub13.aHa3774.method1099(class397, (byte) -72);
							anInterface2_648.method8((byte) -127, true);
							Class41_Sub13.aHa3774.y();
							class397.method4079(0, 0, 0, i_2_, 1);
							Class41_Sub13.aHa3774.method1099(class397, (byte) -72);
							Class41_Sub13.aHa3774.GA(0);
							anInterface2_644.method8((byte) -127, true);
							Class41_Sub13.aHa3774.y();
							class397.method4079(0, 0, 0, i, 1);
						}
						try {
							if (Class41_Sub13.aHa3774 != null && !(anInterface2_644 instanceof Class78))
								Class41_Sub13.aHa3774.method1090((byte) -106);
						} catch (Exception_Sub1 exception_sub1) {
							Class219_Sub1.method2062((exception_sub1.getMessage() + " (Recovered) " + Class246.aClient2332.method102((byte) -44)), (byte) -105, exception_sub1);
							Class33.method348(false, true, 0);
						}
					}
					java.awt.Container container;
					if (Class340.aFrame3707 != null)
						container = Class340.aFrame3707;
					else if (CS2Script.anApplet6140 != null)
						container = CS2Script.anApplet6140;
					else
						container = Class55.anApplet_Sub1_656;
					container.getSize();
					container.getSize();
					if (container == Class340.aFrame3707)
						Class340.aFrame3707.getInsets();
					aBoolean646 = false;
					if (Class41_Sub13.aHa3774 != null && !(anInterface2_644 instanceof Class78) && (aClass326_649.method3386((byte) -44) < Class326.aClass326_2897.method3386((byte) -44)))
						StaticMethods.method1598(-17779);
				} catch (Exception exception) {
					continue;
				}
			}
			long l_3_ = Class72.method771(-108);
			int i = (int) (l - l_3_ + 20L);
			if (i > 0)
				Class106_Sub1.method942((long) i, 0);
		}
	}

	final synchronized void method644(boolean bool, Interface2 interface2) {
		if (bool == true) {
			anInterface2_648 = anInterface2_644;
			anInterface2_644 = interface2;
			aLong653 = Class72.method771(-110);
		}
	}

	final Class326 method645(int i) {
		if (i < 80)
			anInt650 = -111;
		return aClass326_649;
	}

	final long method646(int i) {
		if (i != 31244)
			return -85L;
		return aLong652;
	}

	final synchronized boolean method647(int i) {
		if (i != 0)
			method645(-32);
		return anInterface2_644.method7(aLong653, (byte) -115);
	}

	final synchronized void method648(byte i) {
		aBoolean646 = true;
		int i_4_ = 79 % ((i - 49) / 56);
	}

	final void method649(int i) {
		aBoolean645 = true;
		if (i != 0)
			method645(-16);
	}

	final int method650(boolean bool) {
		if (aClass326_649 == null)
			return 0;
		if (bool != true)
			return 86;
		int i = aClass326_649.method3386((byte) -44);
		if (aClass326_649.aBoolean2881 && aClass326_649.anInt2876 > anInt650)
			return anInt650 + 1;
		if (i < 0 || Class41_Sub8.aClass326Array3764.length - 1 <= i)
			return 100;
		if (anInt650 == aClass326_649.anInt2875)
			return aClass326_649.anInt2876;
		return aClass326_649.anInt2875;
	}

	final String method651(boolean bool) {
		if (bool != true)
			return null;
		return aString651;
	}

	static final void method652(int i, byte i_5_, int i_6_, int i_7_, int i_8_) {
		if (i_6_ >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i_6_) {
			i = ParticleEmitterRaw.method1668(i, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -105);
			i_7_ = ParticleEmitterRaw.method1668(i_7_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 121);
			Class241_Sub2_Sub2.method2162(i_6_, i_7_, i, (byte) 57, i_8_);
		}
		if (i_5_ < 75) {
			/* empty */
		}
	}

	final int method653(int i) {
		if (i != -1239945704)
			aBoolean646 = false;
		return anInt647;
	}

	final int method654(boolean bool) {
		if (bool)
			anInt650 = 66;
		return anInt650;
	}

	public Class54() {
		/* empty */
	}
}
