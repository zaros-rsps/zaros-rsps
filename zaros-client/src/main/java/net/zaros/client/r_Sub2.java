package net.zaros.client;

/* r_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class r_Sub2 extends r {
	static int[] anIntArray6707 = new int[2];
	int anInt6708;
	int anInt6709;
	static SubInPacket aClass260_6710;
	byte[] aByteArray6711;
	int anInt6712;
	int anInt6713;
	static int anInt6714 = -1;
	static OutgoingPacket aClass311_6715;
	static Class209 aClass209_6716;
	static Class357 aClass357_6717;

	final void method2866(int i) {
		int i_0_ = i;
		int i_1_ = aByteArray6711.length - 8;
		while (i_0_ < i_1_) {
			aByteArray6711[++i_0_] = (byte) 0;
			aByteArray6711[++i_0_] = (byte) 0;
			aByteArray6711[++i_0_] = (byte) 0;
			aByteArray6711[++i_0_] = (byte) 0;
			aByteArray6711[++i_0_] = (byte) 0;
			aByteArray6711[++i_0_] = (byte) 0;
			aByteArray6711[++i_0_] = (byte) 0;
			aByteArray6711[++i_0_] = (byte) 0;
		}
		while (aByteArray6711.length - 1 > i_0_)
			aByteArray6711[++i_0_] = (byte) 0;
	}

	public static void method2867(int i) {
		if (i != -2813)
			aClass209_6716 = null;
		aClass260_6710 = null;
		aClass209_6716 = null;
		aClass357_6717 = null;
		aClass311_6715 = null;
		anIntArray6707 = null;
	}

	final void method2868(int i, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		int i_8_ = 0;
		if (i_6_ != i_2_)
			i_8_ = (i_4_ - i_3_ << 16) / (i_6_ - i_2_);
		int i_9_ = 0;
		if (i_5_ != i_6_)
			i_9_ = (i_7_ - i_4_ << 16) / (-i_6_ + i_5_);
		int i_10_ = 0;
		if (i_2_ != i_5_)
			i_10_ = (-i_7_ + i_3_ << 16) / (-i_5_ + i_2_);
		if (i != -709523440)
			method2866(78);
		if (i_6_ < i_2_ || i_2_ > i_5_) {
			if (i_5_ >= i_6_) {
				if (i_2_ > i_5_) {
					i_3_ = i_4_ <<= 16;
					if (i_6_ < 0) {
						i_4_ -= i_9_ * i_6_;
						i_3_ -= i_6_ * i_8_;
						i_6_ = 0;
					}
					i_7_ <<= 16;
					if (i_5_ < 0) {
						i_7_ -= i_5_ * i_10_;
						i_5_ = 0;
					}
					if ((i_6_ == i_5_ || i_8_ >= i_9_) && (i_5_ != i_6_ || i_10_ >= i_8_)) {
						i_2_ -= i_5_;
						i_5_ -= i_6_;
						i_6_ *= anInt6712;
						while (--i_5_ >= 0) {
							Class296_Sub39_Sub18.method2896(i_3_ >> 16, i_6_, i_4_ >> 16, 0, (byte) -85, aByteArray6711);
							i_3_ += i_8_;
							i_4_ += i_9_;
							i_6_ += anInt6712;
						}
						while (--i_2_ >= 0) {
							Class296_Sub39_Sub18.method2896(i_3_ >> 16, i_6_, i_7_ >> 16, 0, (byte) -85, aByteArray6711);
							i_3_ += i_8_;
							i_6_ += anInt6712;
							i_7_ += i_10_;
						}
					} else {
						i_2_ -= i_5_;
						i_5_ -= i_6_;
						i_6_ = anInt6712 * i_6_;
						while (--i_5_ >= 0) {
							Class296_Sub39_Sub18.method2896(i_4_ >> 16, i_6_, i_3_ >> 16, 0, (byte) -85, aByteArray6711);
							i_3_ += i_8_;
							i_4_ += i_9_;
							i_6_ += anInt6712;
						}
						while (--i_2_ >= 0) {
							Class296_Sub39_Sub18.method2896(i_7_ >> 16, i_6_, i_3_ >> 16, 0, (byte) -85, aByteArray6711);
							i_3_ += i_8_;
							i_6_ += anInt6712;
							i_7_ += i_10_;
						}
					}
				} else {
					i_7_ = i_4_ <<= 16;
					i_3_ <<= 16;
					if (i_6_ < 0) {
						i_4_ -= i_9_ * i_6_;
						i_7_ -= i_6_ * i_8_;
						i_6_ = 0;
					}
					if (i_2_ < 0) {
						i_3_ -= i_2_ * i_10_;
						i_2_ = 0;
					}
					if (i_9_ <= i_8_) {
						i_5_ -= i_2_;
						i_2_ -= i_6_;
						i_6_ *= anInt6712;
						while (--i_2_ >= 0) {
							Class296_Sub39_Sub18.method2896(i_7_ >> 16, i_6_, i_4_ >> 16, 0, (byte) -85, aByteArray6711);
							i_6_ += anInt6712;
							i_4_ += i_9_;
							i_7_ += i_8_;
						}
						while (--i_5_ >= 0) {
							Class296_Sub39_Sub18.method2896(i_3_ >> 16, i_6_, i_4_ >> 16, 0, (byte) -85, aByteArray6711);
							i_4_ += i_9_;
							i_3_ += i_10_;
							i_6_ += anInt6712;
						}
					} else {
						i_5_ -= i_2_;
						i_2_ -= i_6_;
						i_6_ = anInt6712 * i_6_;
						while (--i_2_ >= 0) {
							Class296_Sub39_Sub18.method2896(i_4_ >> 16, i_6_, i_7_ >> 16, 0, (byte) -85, aByteArray6711);
							i_4_ += i_9_;
							i_6_ += anInt6712;
							i_7_ += i_8_;
						}
						while (--i_5_ >= 0) {
							Class296_Sub39_Sub18.method2896(i_4_ >> 16, i_6_, i_3_ >> 16, 0, (byte) -85, aByteArray6711);
							i_6_ += anInt6712;
							i_4_ += i_9_;
							i_3_ += i_10_;
						}
					}
				}
			} else if (i_2_ < i_6_) {
				i_4_ = i_7_ <<= 16;
				i_3_ <<= 16;
				if (i_5_ < 0) {
					i_7_ -= i_10_ * i_5_;
					i_4_ -= i_5_ * i_9_;
					i_5_ = 0;
				}
				if (i_2_ < 0) {
					i_3_ -= i_8_ * i_2_;
					i_2_ = 0;
				}
				if (i_9_ < i_10_) {
					i_6_ -= i_2_;
					i_2_ -= i_5_;
					i_5_ *= anInt6712;
					while (--i_2_ >= 0) {
						Class296_Sub39_Sub18.method2896(i_7_ >> 16, i_5_, i_4_ >> 16, 0, (byte) -85, aByteArray6711);
						i_4_ += i_9_;
						i_5_ += anInt6712;
						i_7_ += i_10_;
					}
					while (--i_6_ >= 0) {
						Class296_Sub39_Sub18.method2896(i_3_ >> 16, i_5_, i_4_ >> 16, 0, (byte) -85, aByteArray6711);
						i_5_ += anInt6712;
						i_4_ += i_9_;
						i_3_ += i_8_;
					}
				} else {
					i_6_ -= i_2_;
					i_2_ -= i_5_;
					i_5_ *= anInt6712;
					while (--i_2_ >= 0) {
						Class296_Sub39_Sub18.method2896(i_4_ >> 16, i_5_, i_7_ >> 16, 0, (byte) -85, aByteArray6711);
						i_7_ += i_10_;
						i_5_ += anInt6712;
						i_4_ += i_9_;
					}
					while (--i_6_ >= 0) {
						Class296_Sub39_Sub18.method2896(i_4_ >> 16, i_5_, i_3_ >> 16, 0, (byte) -85, aByteArray6711);
						i_3_ += i_8_;
						i_5_ += anInt6712;
						i_4_ += i_9_;
					}
				}
			} else {
				i_3_ = i_7_ <<= 16;
				if (i_5_ < 0) {
					i_3_ -= i_5_ * i_9_;
					i_7_ -= i_10_ * i_5_;
					i_5_ = 0;
				}
				i_4_ <<= 16;
				if (i_6_ < 0) {
					i_4_ -= i_8_ * i_6_;
					i_6_ = 0;
				}
				if (i_9_ < i_10_) {
					i_2_ -= i_6_;
					i_6_ -= i_5_;
					i_5_ *= anInt6712;
					while (--i_6_ >= 0) {
						Class296_Sub39_Sub18.method2896(i_7_ >> 16, i_5_, i_3_ >> 16, 0, (byte) -85, aByteArray6711);
						i_3_ += i_9_;
						i_5_ += anInt6712;
						i_7_ += i_10_;
					}
					while (--i_2_ >= 0) {
						Class296_Sub39_Sub18.method2896(i_7_ >> 16, i_5_, i_4_ >> 16, 0, (byte) -85, aByteArray6711);
						i_5_ += anInt6712;
						i_7_ += i_10_;
						i_4_ += i_8_;
					}
				} else {
					i_2_ -= i_6_;
					i_6_ -= i_5_;
					i_5_ *= anInt6712;
					while (--i_6_ >= 0) {
						Class296_Sub39_Sub18.method2896(i_3_ >> 16, i_5_, i_7_ >> 16, 0, (byte) -85, aByteArray6711);
						i_3_ += i_9_;
						i_7_ += i_10_;
						i_5_ += anInt6712;
					}
					while (--i_2_ >= 0) {
						Class296_Sub39_Sub18.method2896(i_4_ >> 16, i_5_, i_7_ >> 16, 0, (byte) -85, aByteArray6711);
						i_5_ += anInt6712;
						i_4_ += i_8_;
						i_7_ += i_10_;
					}
				}
			}
		} else if (i_6_ >= i_5_) {
			i_4_ = i_3_ <<= 16;
			if (i_2_ < 0) {
				i_4_ -= i_2_ * i_10_;
				i_3_ -= i_2_ * i_8_;
				i_2_ = 0;
			}
			i_7_ <<= 16;
			if (i_5_ < 0) {
				i_7_ -= i_9_ * i_5_;
				i_5_ = 0;
			}
			if ((i_5_ == i_2_ || i_10_ >= i_8_) && (i_2_ != i_5_ || i_9_ <= i_8_)) {
				i_6_ -= i_5_;
				i_5_ -= i_2_;
				i_2_ = anInt6712 * i_2_;
				while (--i_5_ >= 0) {
					Class296_Sub39_Sub18.method2896(i_4_ >> 16, i_2_, i_3_ >> 16, 0, (byte) -85, aByteArray6711);
					i_3_ += i_8_;
					i_2_ += anInt6712;
					i_4_ += i_10_;
				}
				while (--i_6_ >= 0) {
					Class296_Sub39_Sub18.method2896(i_7_ >> 16, i_2_, i_3_ >> 16, 0, (byte) -85, aByteArray6711);
					i_7_ += i_9_;
					i_2_ += anInt6712;
					i_3_ += i_8_;
				}
			} else {
				i_6_ -= i_5_;
				i_5_ -= i_2_;
				i_2_ *= anInt6712;
				while (--i_5_ >= 0) {
					Class296_Sub39_Sub18.method2896(i_3_ >> 16, i_2_, i_4_ >> 16, 0, (byte) -85, aByteArray6711);
					i_4_ += i_10_;
					i_2_ += anInt6712;
					i_3_ += i_8_;
				}
				while (--i_6_ >= 0) {
					Class296_Sub39_Sub18.method2896(i_3_ >> 16, i_2_, i_7_ >> 16, 0, (byte) -85, aByteArray6711);
					i_3_ += i_8_;
					i_7_ += i_9_;
					i_2_ += anInt6712;
				}
			}
		} else {
			i_7_ = i_3_ <<= 16;
			if (i_2_ < 0) {
				i_7_ -= i_10_ * i_2_;
				i_3_ -= i_8_ * i_2_;
				i_2_ = 0;
			}
			i_4_ <<= 16;
			if (i_6_ < 0) {
				i_4_ -= i_6_ * i_9_;
				i_6_ = 0;
			}
			if ((i_6_ == i_2_ || i_8_ <= i_10_) && (i_6_ != i_2_ || i_9_ >= i_10_)) {
				i_5_ -= i_6_;
				i_6_ -= i_2_;
				i_2_ *= anInt6712;
				while (--i_6_ >= 0) {
					Class296_Sub39_Sub18.method2896(i_7_ >> 16, i_2_, i_3_ >> 16, 0, (byte) -85, aByteArray6711);
					i_7_ += i_10_;
					i_2_ += anInt6712;
					i_3_ += i_8_;
				}
				while (--i_5_ >= 0) {
					Class296_Sub39_Sub18.method2896(i_7_ >> 16, i_2_, i_4_ >> 16, 0, (byte) -85, aByteArray6711);
					i_7_ += i_10_;
					i_4_ += i_9_;
					i_2_ += anInt6712;
				}
			} else {
				i_5_ -= i_6_;
				i_6_ -= i_2_;
				i_2_ *= anInt6712;
				while (--i_6_ >= 0) {
					Class296_Sub39_Sub18.method2896(i_3_ >> 16, i_2_, i_7_ >> 16, 0, (byte) -85, aByteArray6711);
					i_3_ += i_8_;
					i_2_ += anInt6712;
					i_7_ += i_10_;
				}
				while (--i_5_ >= 0) {
					Class296_Sub39_Sub18.method2896(i_4_ >> 16, i_2_, i_7_ >> 16, 0, (byte) -85, aByteArray6711);
					i_7_ += i_10_;
					i_4_ += i_9_;
					i_2_ += anInt6712;
				}
			}
		}
	}

	final void method2869(int i, int i_11_, int i_12_, int i_13_, int i_14_) {
		anInt6709 = -i + i_13_;
		if (i_14_ < -102) {
			anInt6708 = i;
			anInt6713 = i_11_;
			anInt6712 = i_12_ - i_11_;
		}
	}

	final boolean method2870(int i, int i_15_, int i_16_) {
		if (i_15_ != -124914704)
			aClass311_6715 = null;
		if (i * i_16_ > aByteArray6711.length)
			return false;
		return true;
	}

	static final boolean method2871(int i, int i_17_, byte i_18_) {
		if (i_17_ < 0 || i < 0 || i_17_ >= Class41_Sub18.aByteArrayArrayArray3786[1].length || Class41_Sub18.aByteArrayArrayArray3786[1][i_17_].length <= i)
			return false;
		if (i_18_ != -47)
			aClass209_6716 = null;
		if ((Class41_Sub18.aByteArrayArrayArray3786[1][i_17_][i] & 0x2) != 0)
			return true;
		return false;
	}

	r_Sub2(ha_Sub1 var_ha_Sub1, int i, int i_19_) {
		aByteArray6711 = new byte[i_19_ * i];
	}

	static {
		aClass260_6710 = new SubInPacket(2, 3);
		aClass311_6715 = new OutgoingPacket(12, -1);
		aClass209_6716 = new Class209(50);
		aClass357_6717 = new Class357();
	}
}
