package net.zaros.client;


/* Class264 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class264 {
	static Class289 aClass289_2467;
	Class404 aClass404_2468 = new Class404();
	static char[] aCharArray2469 = {' ', '\u00a0', '_', '-', '\u00e0', '\u00e1', '\u00e2', '\u00e4', '\u00e3', '\u00c0', '\u00c1', '\u00c2', '\u00c4', '\u00c3', '\u00e8', '\u00e9', '\u00ea', '\u00eb', '\u00c8', '\u00c9', '\u00ca', '\u00cb', '\u00ed', '\u00ee', '\u00ef', '\u00cd', '\u00ce', '\u00cf', '\u00f2', '\u00f3', '\u00f4', '\u00f6', '\u00f5', '\u00d2', '\u00d3', '\u00d4', '\u00d6', '\u00d5', '\u00f9', '\u00fa', '\u00fb', '\u00fc', '\u00d9', '\u00da', '\u00db', '\u00dc', '\u00e7', '\u00c7', '\u00ff', '\u0178', '\u00f1', '\u00d1', '\u00df'};
	boolean aBoolean2470 = false;
	static AdvancedMemoryCache aClass113_2471;
	static Class256 aClass256_2472;
	static int anInt2473;
	static Class288 aClass288_2474;

	static final void method2276(int i) {
		Class332.method3416(InvisiblePlayer.aClass51_1982, (byte) 121);
		ha.anInt1293++;
		if (!PlayerEquipment.aBoolean3370 || !aa_Sub2.aBoolean3727) {
			if (ha.anInt1293 > 1)
				InvisiblePlayer.aClass51_1982 = null;
		} else {
			int i_0_ = 0;
			int i_1_ = 0;
			if (Class368_Sub5_Sub2.aBoolean6597) {
				i_0_ = Class387.method4034(true);
				i_1_ = GraphicsLoader.method2286(true);
			}
			int i_2_ = i_0_ + Class84.aClass189_924.method1895((byte) -55);
			int i_3_ = i_1_ + Class84.aClass189_924.method1897(0);
			i_2_ -= Class296_Sub38.anInt4899;
			i_3_ -= Class208.anInt2090;
			if (LookupTable.anInt52 > i_2_)
				i_2_ = LookupTable.anInt52;
			if (LookupTable.anInt52 + StaticMethods.aClass51_4842.anInt578 < i_2_ + InvisiblePlayer.aClass51_1982.anInt578)
				i_2_ = LookupTable.anInt52 + (StaticMethods.aClass51_4842.anInt578 - InvisiblePlayer.aClass51_1982.anInt578);
			if (i_3_ < Class296_Sub34_Sub2.anInt6096)
				i_3_ = Class296_Sub34_Sub2.anInt6096;
			if ((Class296_Sub34_Sub2.anInt6096 + StaticMethods.aClass51_4842.anInt623) < i_3_ + InvisiblePlayer.aClass51_1982.anInt623)
				i_3_ = (Class296_Sub34_Sub2.anInt6096 - (-StaticMethods.aClass51_4842.anInt623 + InvisiblePlayer.aClass51_1982.anInt623));
			if (i == 238) {
				int i_4_ = (-LookupTable.anInt52 + (i_2_ + StaticMethods.aClass51_4842.anInt624));
				int i_5_ = (i_3_ - Class296_Sub34_Sub2.anInt6096 + StaticMethods.aClass51_4842.anInt632);
				if (Class84.aClass189_924.method1900(119)) {
					if (InvisiblePlayer.aClass51_1982.anInt523 < ha.anInt1293) {
						int i_6_ = i_2_ - ByteStream.anInt6040;
						int i_7_ = i_3_ - Class21.anInt248;
						if (InvisiblePlayer.aClass51_1982.anInt579 < i_6_ || i_6_ < -InvisiblePlayer.aClass51_1982.anInt579 || i_7_ > InvisiblePlayer.aClass51_1982.anInt579 || i_7_ < -InvisiblePlayer.aClass51_1982.anInt579)
							Class245.aBoolean2327 = true;
					}
					if (InvisiblePlayer.aClass51_1982.anObjectArray570 != null && Class245.aBoolean2327) {
						CS2Call class296_sub46 = new CS2Call();
						class296_sub46.callerInterface = InvisiblePlayer.aClass51_1982;
						class296_sub46.anInt4964 = i_5_;
						class296_sub46.anInt4958 = i_4_;
						class296_sub46.callArgs = InvisiblePlayer.aClass51_1982.anObjectArray570;
						CS2Executor.runCS2(class296_sub46);
					}
				} else {
					if (Class245.aBoolean2327) {
						Class285.method2368(0);
						if (InvisiblePlayer.aClass51_1982.anObjectArray500 != null) {
							CS2Call class296_sub46 = new CS2Call();
							class296_sub46.anInt4958 = i_4_;
							class296_sub46.aClass51_4962 = Class41_Sub28.aClass51_3814;
							class296_sub46.anInt4964 = i_5_;
							class296_sub46.callArgs = InvisiblePlayer.aClass51_1982.anObjectArray500;
							class296_sub46.callerInterface = InvisiblePlayer.aClass51_1982;
							CS2Executor.runCS2(class296_sub46);
						}
						if (Class41_Sub28.aClass51_3814 != null && (GameClient.method118(InvisiblePlayer.aClass51_1982) != null))
							Class384.method4019(InvisiblePlayer.aClass51_1982, Class41_Sub28.aClass51_3814, -49);
					} else if ((Class296_Sub9_Sub1.anInt5982 != 1 && !Class189_Sub1.method1904(-7703)) || Class230.anInt2210 <= 2) {
						if (Class338_Sub3_Sub4_Sub1.method3577(i ^ 0x5d12))
							TextureOperation.method3068(Class21.anInt248 + Class208.anInt2090, (ByteStream.anInt6040 + Class296_Sub38.anInt4899), -17902);
					} else
						TextureOperation.method3068((Class21.anInt248 + Class208.anInt2090), ((ByteStream.anInt6040) + Class296_Sub38.anInt4899), -17902);
					InvisiblePlayer.aClass51_1982 = null;
				}
			}
		}
	}

	final void method2277(byte i) {
		for (;;) {
			Class338_Sub2 class338_sub2 = (Class338_Sub2) aClass404_2468.method4162(255);
			if (class338_sub2 == null)
				break;
			class338_sub2.method3438(false);
			SubCache.method3258(class338_sub2, 5362);
		}
		int i_8_ = 93 % ((58 - i) / 63);
	}

	final void method2278(Class338_Sub2 class338_sub2, int i) {
		if (i >= -4)
			aCharArray2469 = null;
		Class338_Sub3 class338_sub3 = class338_sub2.aClass338_Sub3_5193;
		boolean bool = true;
		Class338_Sub5[] class338_sub5s = class338_sub2.aClass338_Sub5Array5194;
		for (int i_9_ = 0; i_9_ < class338_sub5s.length; i_9_++) {
			if (class338_sub5s[i_9_].aBoolean5224) {
				bool = false;
				break;
			}
		}
		if (!bool) {
			if (aBoolean2470) {
				for (Class338_Sub2 class338_sub2_10_ = ((Class338_Sub2) aClass404_2468.method4160((byte) 101)); class338_sub2_10_ != null; class338_sub2_10_ = (Class338_Sub2) aClass404_2468.method4163(-24917)) {
					if (class338_sub3 == class338_sub2_10_.aClass338_Sub3_5193) {
						class338_sub2_10_.method3438(false);
						SubCache.method3258(class338_sub2_10_, 5362);
					}
				}
			}
			for (Class338_Sub2 class338_sub2_11_ = (Class338_Sub2) aClass404_2468.method4160((byte) -41); class338_sub2_11_ != null; class338_sub2_11_ = (Class338_Sub2) aClass404_2468.method4163(-24917)) {
				if (class338_sub2_11_.aClass338_Sub3_5193.anInt5208 <= class338_sub3.anInt5208) {
					Class41.method387(class338_sub2, -2, class338_sub2_11_);
					return;
				}
			}
			aClass404_2468.method4158(class338_sub2, 1);
		}
	}

	public static void method2279(int i) {
		aClass288_2474 = null;
		aClass289_2467 = null;
		aClass113_2471 = null;
		aCharArray2469 = null;
		if (i == 205)
			aClass256_2472 = null;
	}

	Class264(boolean bool) {
		aBoolean2470 = bool;
	}

	static {
		aClass289_2467 = new Class289();
		aClass113_2471 = new AdvancedMemoryCache(8);
	}
}
