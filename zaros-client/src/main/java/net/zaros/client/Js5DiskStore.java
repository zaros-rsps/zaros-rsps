package net.zaros.client;
import java.io.EOFException;

public class Js5DiskStore {
	private int index;
	private SeekableFile data_file = null;
	private SeekableFile index_file = null;
	static boolean aBoolean1699 = false;
	private int max_size = 65000;
	public static final byte[] SECTOR_BUFFER = new byte[520];
	static boolean aBoolean1701 = false;

	public byte[] load(int i, int groupId) {
		synchronized (data_file) {
			try {
				if (index_file.size() < groupId * 6 + 6) {
					return null;
				}
				index_file.seek(groupId * 6);
				index_file.read(SECTOR_BUFFER, 0, 6);
				int size = ((SECTOR_BUFFER[1] & 0xff) << 8) + ((SECTOR_BUFFER[0] & 0xff) << 16) + (SECTOR_BUFFER[2] & 0xff);
				int block = (SECTOR_BUFFER[3] << 16 & 0xff0000) + ((SECTOR_BUFFER[4] & 0xff) << 8) + (SECTOR_BUFFER[5] & 0xff);
				if (size < 0 || size > max_size) {
					return null;
				}
				if (block <= 0 || data_file.size() / 520L < block) {
					return null;
				}
				byte[] data = new byte[size];
				int offset = 0;
				int chunk = 0;
				while (offset < size) {
					if (block == 0) {
						return null;
					}
					data_file.seek(block * 520);
					int count = -offset + size;
					int myHeaderLength;
					int myGroup;
					int myChunk;
					int myNextBlock;
					int myIndex;
					if (groupId > 65535) {
						if (count > 510) {
							count = 510;
						}
						myHeaderLength = 10;
						data_file.read(SECTOR_BUFFER, 0, count + myHeaderLength);
						myGroup = g4(0);
						myChunk = g2(4);
						myNextBlock = g3(6);
						myIndex = g1(9);
					} else {
						if (count > 512) {
							count = 512;
						}
						myHeaderLength = 8;
						data_file.read(SECTOR_BUFFER, 0, count + myHeaderLength);
						myGroup = g2(0);
						myChunk = g2(2);
						myNextBlock = g3(4);
						myIndex = g1(7);
					}
					if (myGroup != groupId || myChunk != chunk || myIndex != index) {
						return null;
					}
					if (myNextBlock < 0 || myNextBlock > data_file.size() / 520L) {
						return null;
					}
					for (int i_10_ = 0; i_10_ < count; i_10_++) {
						data[offset++] = SECTOR_BUFFER[i_10_ + 8];
					}
					block = myNextBlock;
					chunk++;
				}
				return data;
			} catch (java.io.IOException ioexception) {
				return null;
			}
		}
	}

	final boolean writeFile(byte[] is, int i, int i_11_, byte i_12_) {
		synchronized (data_file) {
			if (i < 0 || i > max_size) {
				throw new IllegalArgumentException();
			}
			boolean bool = store(i, (byte) -104, true, i_11_, is);
			if (!bool) {
				bool = store(i, (byte) -94, false, i_11_, is);
			}
			return bool;
		}
	}

	Js5DiskStore(int i, SeekableFile class255, SeekableFile class255_14_, int len) {
		data_file = class255;
		index_file = class255_14_;
		max_size = len;
		index = i;
	}

	@Override
	public final String toString() {
		return "Cache:" + index;
	}

	private final boolean store(int size, byte i_16_, boolean exists, int groupId, byte[] is) {
		synchronized (data_file) {
			try {
				int block;
				if (!exists) {
					block = (int) ((data_file.size() + 519L) / 520L);
					if (block == 0) {
						block = 1;
					}
				} else {
					if (groupId * 6 + 6 > index_file.size()) {
						return false;
					}
					index_file.seek(groupId * 6);
					index_file.read(SECTOR_BUFFER, 0, 6);
					block = g3(3);
					if (block <= 0 || data_file.size() / 520L < block) {
						return false;
					}
				}
				p3(0, size);
				p3(3, block);
				index_file.seek(groupId * 6);
				index_file.write(SECTOR_BUFFER, 0, 6);
				int offset = 0;
				int chunk = 0;
				while (offset < size) {
					int myNextBlock = 0;
					if (exists) {
						data_file.seek(block * 520);
						int myGroup;
						int myChunk;
						int myIndex;
						if (groupId > 65535) {
							try {
								data_file.read(SECTOR_BUFFER, 0, 10);
							} catch (EOFException eof) {
								break;
							}
							myGroup = g4(0);
							myChunk = g2(4);
							myNextBlock = g3(6);
							myIndex = g1(9);
						} else {
							try {
								data_file.read(SECTOR_BUFFER, 0, 8);
							} catch (EOFException eofexception) {
								break;
							}
							myGroup = g2(0);
							myChunk = g2(2);
							myNextBlock = g3(4);
							myIndex = g1(7);
						}
						if (groupId != myGroup || chunk != myChunk || myIndex != index) {
							return false;
						}
						if (myNextBlock < 0 || myNextBlock > data_file.size() / 520L) {
							return false;
						}
					}
					if (myNextBlock == 0) {
						myNextBlock = (int) ((data_file.size() + 519L) / 520L);
						exists = false;
						if (myNextBlock == 0) {
							myNextBlock++;
						}
						if (block == myNextBlock) {
							myNextBlock++;
						}
					}
					if (size - offset <= 512) {
						myNextBlock = 0;
					}
					if (groupId > 65535) {
						p4(0, groupId);
						p2(4, chunk);
						p3(6, myNextBlock);
						p1(9, index);
						data_file.seek(block * 520);
						data_file.write(SECTOR_BUFFER, 0, 10);
						int numBytes = size - offset;
						if (numBytes > 510) {
							numBytes = 510;
						}
						data_file.write(is, offset, numBytes);
						offset += numBytes;
					} else {
						p2(0, groupId);
						p2(2, chunk);
						p3(4, myNextBlock);
						p1(7, index);
						data_file.seek(block * 520);
						data_file.write(SECTOR_BUFFER, 0, 8);
						int numBytes = size - offset;
						if (numBytes > 512) {
							numBytes = 512;
						}
						data_file.write(is, offset, numBytes);
						offset += numBytes;
					}
					block = myNextBlock;
					chunk++;
				}
				return true;
			} catch (java.io.IOException ioexception) {
				return false;
			}
		}
	}

	private static void p4(int offset, int value) {
		p1(offset++, value >> 24);
		p1(offset++, value >> 16);
		p1(offset++, value >> 8);
		p1(offset, value);
	}

	private static void p3(int offset, int value) {
		p1(offset++, value >> 16);
		p1(offset++, value >> 8);
		p1(offset, value);
	}

	private static void p2(int offset, int value) {
		p1(offset++, value >> 8);
		p1(offset, value);
	}

	private static void p1(int offset, int value) {
		SECTOR_BUFFER[offset] = (byte) value;
	}

	private static int g4(int offset) {
		return (g1(offset++) << 24) + (g1(offset++) << 16) + (g1(offset++) << 8) + g1(offset);
	}

	private static int g3(int offset) {
		return (g1(offset++) << 16) + (g1(offset++) << 8) + g1(offset);
	}

	private static int g2(int offset) {
		return (g1(offset++) << 8) + g1(offset);
	}

	private static int g1(int offset) {
		return SECTOR_BUFFER[offset] & 0xff;
	}
}
