package net.zaros.client;

/* Class41_Sub13 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub13 extends Class41 {
	static int[] anIntArray3772;
	static int anInt3773 = 0;
	static ha aHa3774;

	Class41_Sub13(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	static final void method443(int i) {
		Class296_Sub39_Sub13.aClass113_6205.clearSoftReferences();
		Class338_Sub3_Sub1_Sub4.aClass113_6641.clearSoftReferences();
		Class153.aClass113_1576.clearSoftReferences();
		Class41_Sub20.aClass113_3794.clearSoftReferences();
		if (i < 12)
			aHa3774 = null;
	}

	Class41_Sub13(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final int method380(int i, byte i_0_) {
		if (i_0_ != 41)
			method380(57, (byte) -123);
		return 1;
	}

	final void method381(int i, byte i_1_) {
		anInt389 = i;
		if (i_1_ != -110)
			method380(-94, (byte) -84);
	}

	final void method386(int i) {
		if (i != 2)
			anIntArray3772 = null;
		if (anInt389 < 1 || anInt389 > 3)
			anInt389 = method383((byte) 110);
	}

	final int method444(int i) {
		if (i < 114)
			method443(7);
		return anInt389;
	}

	final int method383(byte i) {
		if (i != 110)
			method380(-120, (byte) 92);
		if (!aClass296_Sub50_392.method3055(true).method325(-45))
			return 2;
		return 3;
	}

	public static void method445(int i) {
		if (i != -20511)
			method443(75);
		aHa3774 = null;
		anIntArray3772 = null;
	}
}
