package net.zaros.client;

/* Class296_Sub51_Sub17 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub17 extends TextureOperation {
	private int anInt6428;
	static Js5 aClass138_6429;
	private int anInt6430 = 409;
	private int anInt6431;
	private int[] anIntArray6432;
	private int anInt6433;

	static final void method3122(int i, int i_0_, int i_1_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i, 12);
		class296_sub39_sub5.insertIntoQueue_2();
		if (i_0_ <= 74)
			aClass138_6429 = null;
		class296_sub39_sub5.intParam = i_1_;
	}

	final int[][] get_colour_output(int i, int i_2_) {
		if (i_2_ != 17621)
			anInt6430 = -55;
		int[][] is = aClass86_5034.method823((byte) 121, i);
		if (aClass86_5034.aBoolean939) {
			int[][] is_3_ = this.method3075((byte) 109, 0, i);
			int[] is_4_ = is_3_[0];
			int[] is_5_ = is_3_[1];
			int[] is_6_ = is_3_[2];
			int[] is_7_ = is[0];
			int[] is_8_ = is[1];
			int[] is_9_ = is[2];
			for (int i_10_ = 0; i_10_ < Class41_Sub10.anInt3769; i_10_++) {
				int i_11_ = is_4_[i_10_];
				int i_12_ = i_11_ - anIntArray6432[0];
				if (i_12_ < 0)
					i_12_ = -i_12_;
				if (anInt6430 < i_12_) {
					is_7_[i_10_] = i_11_;
					is_8_[i_10_] = is_5_[i_10_];
					is_9_[i_10_] = is_6_[i_10_];
				} else {
					int i_13_ = is_5_[i_10_];
					i_12_ = -anIntArray6432[1] + i_13_;
					if (i_12_ < 0)
						i_12_ = -i_12_;
					if (i_12_ > anInt6430) {
						is_7_[i_10_] = i_11_;
						is_8_[i_10_] = i_13_;
						is_9_[i_10_] = is_6_[i_10_];
					} else {
						int i_14_ = is_6_[i_10_];
						i_12_ = -anIntArray6432[2] + i_14_;
						if (i_12_ < 0)
							i_12_ = -i_12_;
						if (i_12_ > anInt6430) {
							is_7_[i_10_] = i_11_;
							is_8_[i_10_] = i_13_;
							is_9_[i_10_] = i_14_;
						} else {
							is_7_[i_10_] = i_11_ * anInt6431 >> 12;
							is_8_[i_10_] = i_13_ * anInt6433 >> 12;
							is_9_[i_10_] = anInt6428 * i_14_ >> 12;
						}
					}
				}
			}
		}
		return is;
	}

	final void method3071(int i, Packet class296_sub17, int i_15_) {
		int i_16_ = i_15_;
		while_121_ : do {
			while_120_ : do {
				while_119_ : do {
					do {
						if (i_16_ != 0) {
							if (i_16_ != 1) {
								if (i_16_ != 2) {
									if (i_16_ != 3) {
										if (i_16_ == 4)
											break while_120_;
										break while_121_;
									}
								} else
									break;
								break while_119_;
							}
						} else {
							anInt6430 = class296_sub17.g2();
							break while_121_;
						}
						anInt6428 = class296_sub17.g2();
						break while_121_;
					} while (false);
					anInt6433 = class296_sub17.g2();
					break while_121_;
				} while (false);
				anInt6431 = class296_sub17.g2();
				break while_121_;
			} while (false);
			int i_17_ = class296_sub17.readUnsignedMedInt();
			anIntArray6432[0] = (i_17_ & 16711680) << 4;
			anIntArray6432[2] = 0 & i_17_ >> 12;
			anIntArray6432[1] = i_17_ >> 4 & 4080;
		} while (false);
		if (i > -84)
			get_colour_output(31, 63);
	}

	static final int method3123(int i, int i_18_, int i_19_, int i_20_) {
		if (i > 243)
			i_19_ >>= 4;
		else if (i <= 217) {
			if (i > 192)
				i_19_ >>= 2;
			else if (i > 179)
				i_19_ >>= 1;
		} else
			i_19_ >>= 3;
		if (i_20_ != -1)
			return -76;
		return (i >> 1) + (i_19_ >> 5 << 7) + ((i_18_ >> 2 & 0x3f) << 10);
	}

	public static void method3124(int i) {
		aClass138_6429 = null;
		if (i != 2)
			aClass138_6429 = null;
	}

	public Class296_Sub51_Sub17() {
		super(1, false);
		anInt6428 = 4096;
		anIntArray6432 = new int[3];
		anInt6431 = 4096;
		anInt6433 = 4096;
	}
}
