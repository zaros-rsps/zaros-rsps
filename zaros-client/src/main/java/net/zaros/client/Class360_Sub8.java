package net.zaros.client;
import jaggl.OpenGL;

final class Class360_Sub8 extends Class360 {
	static int anInt5338 = 0;
	private ha_Sub1_Sub1 aHa_Sub1_Sub1_5339;
	static int anInt5340;
	private Class369 aClass369_5341;
	private Class184 aClass184_5342;

	final void method3725(int i) {
		aHa_Sub1_3093.method1158((byte) -99, 0, Class199.aClass287_2007);
		OpenGL.glBindProgramARB(34336, 0);
		int i_0_ = 112 % ((i - 58) / 56);
		OpenGL.glDisable(34820);
		OpenGL.glDisable(34336);
	}

	final void method3732(int i, int i_1_, int i_2_) {
		if (i_1_ >= -6)
			anInt5340 = -45;
		if (!aClass184_5342.aBoolean1883) {
			int i_3_ = aHa_Sub1_3093.anInt4006 % 4000 * 16 / 4000;
			aHa_Sub1_3093.method1140((aClass184_5342.anInterface6_Impl1Array1887[i_3_]), false);
			OpenGL.glProgramLocalParameter4fARB(34336, 0, 0.0F, 0.0F, 0.0F, 1.0F);
		} else {
			float f = (float) (aHa_Sub1_3093.anInt4006 % 4000) / 4000.0F;
			aHa_Sub1_3093.method1140(aClass184_5342.anInterface6_Impl2_1886, false);
			OpenGL.glProgramLocalParameter4fARB(34336, 0, f, 0.0F, 0.0F, 1.0F);
		}
	}

	final boolean method3723(byte i) {
		int i_4_ = -125 % ((-49 - i) / 36);
		if (aClass369_5341 == null)
			return false;
		return true;
	}

	final void method3731(boolean bool, byte i) {
		aHa_Sub1_3093.method1177(Class41_Sub4.aClass125_3745, 9815, Class41_Sub1.aClass125_3735);
		if (i != -71) {
			/* empty */
		}
	}

	final void method3736(byte i, Interface6 interface6, int i_5_) {
		int i_6_ = -88 % ((i - 72) / 49);
	}

	final void method3733(byte i, boolean bool) {
		OpenGL.glBindProgramARB(34336, aClass369_5341.anInt3137);
		OpenGL.glEnable(34336);
		if (i >= -125)
			method3725(-9);
		aHa_Sub1_3093.method1158((byte) -112, 0, Class153.aClass287_1578);
	}

	Class360_Sub8(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Js5 class138, Class184 class184) {
		super(var_ha_Sub1_Sub1);
		aClass184_5342 = class184;
		aHa_Sub1_Sub1_5339 = var_ha_Sub1_Sub1;
		if (class138 != null && aClass184_5342.method1859(16) && aHa_Sub1_Sub1_5339.aBoolean5844)
			aClass369_5341 = (Class296_Sub48.method3045(0, class138.getFile_("transparent_water", "gl"), aHa_Sub1_Sub1_5339, 34336));
		else
			aClass369_5341 = null;
	}
}
