package net.zaros.client;
/* Class173 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

import java.io.IOException;
import java.net.Socket;
import java.net.URL;

import jaggl.OpenGL;

final class Class173 {
	
	private Class296_Sub17_Sub2 aClass296_Sub17_Sub2_1801;
	
	static OutgoingPacket aClass311_1802 = new OutgoingPacket(5, -1);
	
	private float[] aFloatArray1803 = new float[16];
	
	private Class272 aClass272_1804;
	
	private Interface5 anInterface5_1805;
	
	private Class272 aClass272_1806;
	
	private int anInt1807;
	
	private Class272 aClass272_1808;
	
	private Class338_Sub8_Sub2[][] aClass338_Sub8_Sub2ArrayArray1809;
	
	private int[] anIntArray1810;
	
	private int[] anIntArray1811;
	
	private int[] anIntArray1812;
	
	private int anInt1813;
	
	private Class338_Sub8_Sub2[][] aClass338_Sub8_Sub2ArrayArray1814;
	
	public static void method1682(int i) {
		if (i != 2) {
			aClass311_1802 = null;
		}
		aClass311_1802 = null;
	}
	
	private final void method1683(int i, ha_Sub3 var_ha_Sub3) {
		var_ha_Sub3.method1285(124, true);
		OpenGL.glEnable(i);
		OpenGL.glEnable(16385);
		if (var_ha_Sub3.aFloat4217 != Class360_Sub3.aFloat5307) {
			var_ha_Sub3.xa(Class360_Sub3.aFloat5307);
		}
	}
	
	private final void method1684(int i, boolean bool, ha_Sub3 var_ha_Sub3) {
		Class360_Sub3.aFloat5307 = var_ha_Sub3.aFloat4217;
		var_ha_Sub3.method1308(-58, i);
		var_ha_Sub3.method1302(8448);
		if (bool != true) {
			aClass311_1802 = null;
		}
		OpenGL.glDisable(16384);
		OpenGL.glDisable(16385);
		var_ha_Sub3.method1285(118, false);
		OpenGL.glNormal3f(0.0F, -1.0F, 0.0F);
	}
	
	private final void method1685(int i, ha_Sub3 var_ha_Sub3, int i_0_) {
		if (i > 0) {
			method1685(-30, null, 68);
		}
		OpenGL.glGetFloatv(2982, aFloatArray1803, 0);
		float f = aFloatArray1803[0];
		float f_1_ = aFloatArray1803[4];
		float f_2_ = aFloatArray1803[8];
		float f_3_ = aFloatArray1803[1];
		float f_4_ = aFloatArray1803[5];
		float f_5_ = aFloatArray1803[9];
		float f_6_ = f + f_3_;
		float f_7_ = f_1_ + f_4_;
		float f_8_ = f_2_ + f_5_;
		float f_9_ = f - f_3_;
		float f_10_ = -f_4_ + f_1_;
		float f_11_ = -f_5_ + f_2_;
		float f_12_ = -f + f_3_;
		float f_13_ = f_4_ - f_1_;
		float f_14_ = f_5_ - f_2_;
		aClass296_Sub17_Sub2_1801.pos = 0;
		if (var_ha_Sub3.aBoolean4184) {
			for (int i_15_ = i_0_ - 1; i_15_ >= 0; i_15_--) {
				int i_16_ = anIntArray1812[i_15_] > 64 ? 64 : anIntArray1812[i_15_];
				if (i_16_ > 0) {
					for (int i_17_ = i_16_ - 1; i_17_ >= 0; i_17_--) {
						Class338_Sub8_Sub2 class338_sub8_sub2 = aClass338_Sub8_Sub2ArrayArray1814[i_15_][i_17_];
						int i_18_ = class338_sub8_sub2.anInt6583;
						byte i_19_ = (byte) (i_18_ >> 16);
						byte i_20_ = (byte) (i_18_ >> 8);
						byte i_21_ = (byte) i_18_;
						byte i_22_ = (byte) (i_18_ >>> 24);
						float f_23_ = class338_sub8_sub2.anInt6580 >> 12;
						float f_24_ = class338_sub8_sub2.anInt6589 >> 12;
						float f_25_ = class338_sub8_sub2.anInt6581 >> 12;
						int i_26_ = class338_sub8_sub2.anInt6579 >> 12;
						aClass296_Sub17_Sub2_1801.method2636((byte) 59, 0.0F);
						aClass296_Sub17_Sub2_1801.method2636((byte) 76, 0.0F);
						aClass296_Sub17_Sub2_1801.method2636((byte) 63, f_23_ + f_6_ * -i_26_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 125, f_24_ + -i_26_ * f_7_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 106, f_8_ * -i_26_ + f_25_);
						aClass296_Sub17_Sub2_1801.p1(i_19_);
						aClass296_Sub17_Sub2_1801.p1(i_20_);
						aClass296_Sub17_Sub2_1801.p1(i_21_);
						aClass296_Sub17_Sub2_1801.p1(i_22_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 57, 1.0F);
						aClass296_Sub17_Sub2_1801.method2636((byte) 116, 0.0F);
						aClass296_Sub17_Sub2_1801.method2636((byte) 74, f_23_ + i_26_ * f_9_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 65, f_24_ + i_26_ * f_10_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 91, f_11_ * i_26_ + f_25_);
						aClass296_Sub17_Sub2_1801.p1(i_19_);
						aClass296_Sub17_Sub2_1801.p1(i_20_);
						aClass296_Sub17_Sub2_1801.p1(i_21_);
						aClass296_Sub17_Sub2_1801.p1(i_22_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 126, 1.0F);
						aClass296_Sub17_Sub2_1801.method2636((byte) 118, 1.0F);
						aClass296_Sub17_Sub2_1801.method2636((byte) 58, f_6_ * i_26_ + f_23_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 57, f_7_ * i_26_ + f_24_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 76, i_26_ * f_8_ + f_25_);
						aClass296_Sub17_Sub2_1801.p1(i_19_);
						aClass296_Sub17_Sub2_1801.p1(i_20_);
						aClass296_Sub17_Sub2_1801.p1(i_21_);
						aClass296_Sub17_Sub2_1801.p1(i_22_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 115, 0.0F);
						aClass296_Sub17_Sub2_1801.method2636((byte) 97, 1.0F);
						aClass296_Sub17_Sub2_1801.method2636((byte) 86, f_12_ * i_26_ + f_23_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 110, f_13_ * i_26_ + f_24_);
						aClass296_Sub17_Sub2_1801.method2636((byte) 73, f_25_ + f_14_ * i_26_);
						aClass296_Sub17_Sub2_1801.p1(i_19_);
						aClass296_Sub17_Sub2_1801.p1(i_20_);
						aClass296_Sub17_Sub2_1801.p1(i_21_);
						aClass296_Sub17_Sub2_1801.p1(i_22_);
					}
					if (anIntArray1812[i_15_] > 64) {
						int i_27_ = anIntArray1812[i_15_] - 64 - 1;
						for (int i_28_ = anIntArray1811[i_27_] - 1; i_28_ >= 0; i_28_--) {
							Class338_Sub8_Sub2 class338_sub8_sub2 = aClass338_Sub8_Sub2ArrayArray1809[i_27_][i_28_];
							int i_29_ = class338_sub8_sub2.anInt6583;
							byte i_30_ = (byte) (i_29_ >> 16);
							byte i_31_ = (byte) (i_29_ >> 8);
							byte i_32_ = (byte) i_29_;
							byte i_33_ = (byte) (i_29_ >>> 24);
							float f_34_ = class338_sub8_sub2.anInt6580 >> 12;
							float f_35_ = class338_sub8_sub2.anInt6589 >> 12;
							float f_36_ = class338_sub8_sub2.anInt6581 >> 12;
							int i_37_ = class338_sub8_sub2.anInt6579 >> 12;
							aClass296_Sub17_Sub2_1801.method2636((byte) 80, 0.0F);
							aClass296_Sub17_Sub2_1801.method2636((byte) 75, 0.0F);
							aClass296_Sub17_Sub2_1801.method2636((byte) 123, f_6_ * -i_37_ + f_34_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 74, f_7_ * -i_37_ + f_35_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 64, f_8_ * -i_37_ + f_36_);
							aClass296_Sub17_Sub2_1801.p1(i_30_);
							aClass296_Sub17_Sub2_1801.p1(i_31_);
							aClass296_Sub17_Sub2_1801.p1(i_32_);
							aClass296_Sub17_Sub2_1801.p1(i_33_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 106, 1.0F);
							aClass296_Sub17_Sub2_1801.method2636((byte) 116, 0.0F);
							aClass296_Sub17_Sub2_1801.method2636((byte) 126, f_34_ + f_9_ * i_37_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 116, f_10_ * i_37_ + f_35_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 79, f_36_ + i_37_ * f_11_);
							aClass296_Sub17_Sub2_1801.p1(i_30_);
							aClass296_Sub17_Sub2_1801.p1(i_31_);
							aClass296_Sub17_Sub2_1801.p1(i_32_);
							aClass296_Sub17_Sub2_1801.p1(i_33_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 71, 1.0F);
							aClass296_Sub17_Sub2_1801.method2636((byte) 121, 1.0F);
							aClass296_Sub17_Sub2_1801.method2636((byte) 79, i_37_ * f_6_ + f_34_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 62, f_35_ + i_37_ * f_7_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 121, f_36_ + i_37_ * f_8_);
							aClass296_Sub17_Sub2_1801.p1(i_30_);
							aClass296_Sub17_Sub2_1801.p1(i_31_);
							aClass296_Sub17_Sub2_1801.p1(i_32_);
							aClass296_Sub17_Sub2_1801.p1(i_33_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 80, 0.0F);
							aClass296_Sub17_Sub2_1801.method2636((byte) 59, 1.0F);
							aClass296_Sub17_Sub2_1801.method2636((byte) 126, f_12_ * i_37_ + f_34_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 101, f_35_ + i_37_ * f_13_);
							aClass296_Sub17_Sub2_1801.method2636((byte) 90, f_36_ + i_37_ * f_14_);
							aClass296_Sub17_Sub2_1801.p1(i_30_);
							aClass296_Sub17_Sub2_1801.p1(i_31_);
							aClass296_Sub17_Sub2_1801.p1(i_32_);
							aClass296_Sub17_Sub2_1801.p1(i_33_);
						}
					}
				}
			}
		} else {
			for (int i_38_ = i_0_ - 1; i_38_ >= 0; i_38_--) {
				int i_39_ = anIntArray1812[i_38_] <= 64 ? anIntArray1812[i_38_] : 64;
				if (i_39_ > 0) {
					for (int i_40_ = i_39_ - 1; i_40_ >= 0; i_40_--) {
						Class338_Sub8_Sub2 class338_sub8_sub2 = aClass338_Sub8_Sub2ArrayArray1814[i_38_][i_40_];
						int i_41_ = class338_sub8_sub2.anInt6583;
						byte i_42_ = (byte) (i_41_ >> 16);
						byte i_43_ = (byte) (i_41_ >> 8);
						byte i_44_ = (byte) i_41_;
						byte i_45_ = (byte) (i_41_ >>> 24);
						float f_46_ = class338_sub8_sub2.anInt6580 >> 12;
						float f_47_ = class338_sub8_sub2.anInt6589 >> 12;
						float f_48_ = class338_sub8_sub2.anInt6581 >> 12;
						int i_49_ = class338_sub8_sub2.anInt6579 >> 12;
						aClass296_Sub17_Sub2_1801.method2637((byte) -82, 0.0F);
						aClass296_Sub17_Sub2_1801.method2637((byte) -103, 0.0F);
						aClass296_Sub17_Sub2_1801.method2637((byte) -119, f_46_ + f_6_ * -i_49_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -83, -i_49_ * f_7_ + f_47_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -79, f_48_ + f_8_ * -i_49_);
						aClass296_Sub17_Sub2_1801.p1(i_42_);
						aClass296_Sub17_Sub2_1801.p1(i_43_);
						aClass296_Sub17_Sub2_1801.p1(i_44_);
						aClass296_Sub17_Sub2_1801.p1(i_45_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -115, 1.0F);
						aClass296_Sub17_Sub2_1801.method2637((byte) -111, 0.0F);
						aClass296_Sub17_Sub2_1801.method2637((byte) -85, i_49_ * f_9_ + f_46_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -86, f_47_ + i_49_ * f_10_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -128, i_49_ * f_11_ + f_48_);
						aClass296_Sub17_Sub2_1801.p1(i_42_);
						aClass296_Sub17_Sub2_1801.p1(i_43_);
						aClass296_Sub17_Sub2_1801.p1(i_44_);
						aClass296_Sub17_Sub2_1801.p1(i_45_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -83, 1.0F);
						aClass296_Sub17_Sub2_1801.method2637((byte) -90, 1.0F);
						aClass296_Sub17_Sub2_1801.method2637((byte) -102, f_6_ * i_49_ + f_46_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -95, f_7_ * i_49_ + f_47_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -109, f_8_ * i_49_ + f_48_);
						aClass296_Sub17_Sub2_1801.p1(i_42_);
						aClass296_Sub17_Sub2_1801.p1(i_43_);
						aClass296_Sub17_Sub2_1801.p1(i_44_);
						aClass296_Sub17_Sub2_1801.p1(i_45_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -92, 0.0F);
						aClass296_Sub17_Sub2_1801.method2637((byte) -104, 1.0F);
						aClass296_Sub17_Sub2_1801.method2637((byte) -98, f_46_ + f_12_ * i_49_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -105, f_47_ + f_13_ * i_49_);
						aClass296_Sub17_Sub2_1801.method2637((byte) -124, f_14_ * i_49_ + f_48_);
						aClass296_Sub17_Sub2_1801.p1(i_42_);
						aClass296_Sub17_Sub2_1801.p1(i_43_);
						aClass296_Sub17_Sub2_1801.p1(i_44_);
						aClass296_Sub17_Sub2_1801.p1(i_45_);
					}
					if (anIntArray1812[i_38_] > 64) {
						int i_50_ = anIntArray1812[i_38_] - 65;
						for (int i_51_ = anIntArray1811[i_50_] - 1; i_51_ >= 0; i_51_--) {
							Class338_Sub8_Sub2 class338_sub8_sub2 = aClass338_Sub8_Sub2ArrayArray1809[i_50_][i_51_];
							int i_52_ = class338_sub8_sub2.anInt6583;
							byte i_53_ = (byte) (i_52_ >> 16);
							byte i_54_ = (byte) (i_52_ >> 8);
							byte i_55_ = (byte) i_52_;
							byte i_56_ = (byte) (i_52_ >>> 24);
							float f_57_ = class338_sub8_sub2.anInt6580 >> 12;
							float f_58_ = class338_sub8_sub2.anInt6589 >> 12;
							float f_59_ = class338_sub8_sub2.anInt6581 >> 12;
							int i_60_ = class338_sub8_sub2.anInt6579 >> 12;
							aClass296_Sub17_Sub2_1801.method2637((byte) -120, 0.0F);
							aClass296_Sub17_Sub2_1801.method2637((byte) -102, 0.0F);
							aClass296_Sub17_Sub2_1801.method2637((byte) -104, -i_60_ * f_6_ + f_57_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -105, -i_60_ * f_7_ + f_58_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -116, f_8_ * -i_60_ + f_59_);
							aClass296_Sub17_Sub2_1801.p1(i_53_);
							aClass296_Sub17_Sub2_1801.p1(i_54_);
							aClass296_Sub17_Sub2_1801.p1(i_55_);
							aClass296_Sub17_Sub2_1801.p1(i_56_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -125, 1.0F);
							aClass296_Sub17_Sub2_1801.method2637((byte) -85, 0.0F);
							aClass296_Sub17_Sub2_1801.method2637((byte) -118, f_57_ + i_60_ * f_9_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -114, f_10_ * i_60_ + f_58_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -105, f_11_ * i_60_ + f_59_);
							aClass296_Sub17_Sub2_1801.p1(i_53_);
							aClass296_Sub17_Sub2_1801.p1(i_54_);
							aClass296_Sub17_Sub2_1801.p1(i_55_);
							aClass296_Sub17_Sub2_1801.p1(i_56_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -127, 1.0F);
							aClass296_Sub17_Sub2_1801.method2637((byte) -126, 1.0F);
							aClass296_Sub17_Sub2_1801.method2637((byte) -85, i_60_ * f_6_ + f_57_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -99, f_58_ + i_60_ * f_7_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -121, i_60_ * f_8_ + f_59_);
							aClass296_Sub17_Sub2_1801.p1(i_53_);
							aClass296_Sub17_Sub2_1801.p1(i_54_);
							aClass296_Sub17_Sub2_1801.p1(i_55_);
							aClass296_Sub17_Sub2_1801.p1(i_56_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -123, 0.0F);
							aClass296_Sub17_Sub2_1801.method2637((byte) -90, 1.0F);
							aClass296_Sub17_Sub2_1801.method2637((byte) -81, f_12_ * i_60_ + f_57_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -119, i_60_ * f_13_ + f_58_);
							aClass296_Sub17_Sub2_1801.method2637((byte) -121, i_60_ * f_14_ + f_59_);
							aClass296_Sub17_Sub2_1801.p1(i_53_);
							aClass296_Sub17_Sub2_1801.p1(i_54_);
							aClass296_Sub17_Sub2_1801.p1(i_55_);
							aClass296_Sub17_Sub2_1801.p1(i_56_);
						}
					}
				}
			}
		}
		if (aClass296_Sub17_Sub2_1801.pos != 0) {
			anInterface5_1805.method25(aClass296_Sub17_Sub2_1801.data, (byte) -95, 24, aClass296_Sub17_Sub2_1801.pos);
			var_ha_Sub3.method1323(null, aClass272_1804, aClass272_1808, false, aClass272_1806);
			var_ha_Sub3.method1281(0, aClass296_Sub17_Sub2_1801.pos / 24, 0, 7);
		}
	}
	
	static final void method1686(byte i) {
		if (Class296_Sub39_Sub12.loginState == 0 || Class296_Sub39_Sub12.loginState == 10) {
			return;
		}
		try {
			int i_61_;
			if (Class105_Sub1.anInt5689 != 0) {
				i_61_ = 2000;
			} else {
				i_61_ = 250;
			}
			if (Class187.anInt1910 != 2 || Class296_Sub39_Sub12.loginState != 20 && Class296_Sub39_Sub12.anInt6200 != 42) {
				Class296_Sub14.anInt4662++;
			}
			if (Js5DiskStore.aBoolean1701 && Class296_Sub39_Sub12.loginState >= 6) {
				i_61_ = 6000;
			}
			if (i_61_ < Class296_Sub14.anInt4662) {
				Class377.loginConnection.method1966(320);
				if (Class105_Sub1.anInt5689 < 3) {
					if (Class187.anInt1910 == 2) {
						Class296_Sub51_Sub27_Sub1.aClass112_6733.method978((byte) 124);
					} else {
						Class338_Sub3_Sub3_Sub1.lobbyWorld.method978((byte) 124);
					}
					Class296_Sub14.anInt4662 = 0;
					Class105_Sub1.anInt5689++;
					Class296_Sub39_Sub12.loginState = 1;
				} else {
					Class296_Sub39_Sub12.loginState = 0;
					BITConfigDefinition.method2352(-2, -5);
					return;
				}
			}
			if (Class296_Sub39_Sub12.loginState == 1) {
				if (Class187.anInt1910 == 2) {
					Class377.loginConnection.aClass278_2047 = Class296_Sub51_Sub27_Sub1.aClass112_6733.createWorldSocket(43594, Class252.aClass398_2383);
				} else {
					Class377.loginConnection.aClass278_2047 = Class338_Sub3_Sub3_Sub1.lobbyWorld.createSocket(43594, Class252.aClass398_2383);
				}
				Class296_Sub39_Sub12.loginState = 2;
			}
			if (Class296_Sub39_Sub12.loginState == 2) {
				if (Class377.loginConnection.aClass278_2047.anInt2540 == 2) {
					throw new IOException();
				}
				if (Class377.loginConnection.aClass278_2047.anInt2540 != 1) {
					return;
				}
				Class377.loginConnection.aClass154_2045 = Class41_Sub11.method436(15000, -1, (Socket) Class377.loginConnection.aClass278_2047.anObject2539);
				Class377.loginConnection.aClass278_2047 = null;
				Class377.loginConnection.method1968(0);
				Class296_Sub1 class296_sub1 = Class296_Sub51_Sub30.method3169(55);
				if (!Js5DiskStore.aBoolean1701) {
					class296_sub1.out.p1(Class292.aClass269_2661.protocolId);
				} else {
					class296_sub1.out.p1(Class292.aClass269_2677.protocolId);
					class296_sub1.out.p2(0);
					int i_62_ = class296_sub1.out.pos;
					class296_sub1.out.p4(666);
					if (Class187.anInt1910 == 2) {
						class296_sub1.out.p1(Class366_Sub6.anInt5392 == 14 ? 1 : 0);
					}
					Packet class296_sub17 = Class368_Sub2.method3811(true);
					class296_sub17.p1(Class296_Sub51_Sub14.anInt6423);
					class296_sub17.p2((int) (Math.random() * 9.9999999E7));
					class296_sub17.p1(Class394.langID);
					class296_sub17.p4(Class209.affliateID);
					for (int i_63_ = 0; i_63_ < 6; i_63_++) {
						class296_sub17.p4((int) (Math.random() * 9.9999999E7));
					}
					class296_sub17.writeLong(Class368_Sub1.aLong5424);
					class296_sub17.p1(Class296_Sub50.game.anInt340);
					class296_sub17.p1((int) (Math.random() * 9.9999999E7));
					class296_sub17.writeRsaEncryption(RSA.EXPONENT, RSA.MODULUS);
					class296_sub1.out.writeBytes(class296_sub17.data, 0, class296_sub17.pos);
					class296_sub1.out.endBytePacket(class296_sub1.out.pos - i_62_);
				}
				Class377.loginConnection.sendPacket(class296_sub1, (byte) 119);
				Class377.loginConnection.method1963(true);
				Class296_Sub39_Sub12.loginState = 3;
			}
			if (Class296_Sub39_Sub12.loginState == 3) {
				if (!Class377.loginConnection.aClass154_2045.method1556(true, 1)) {
					return;
				}
				Class377.loginConnection.aClass154_2045.method1559((byte) -69, 0, Class377.loginConnection.in_stream.data, 1);
				int i_64_ = Class377.loginConnection.in_stream.data[0] & 0xff;
				if (i_64_ != 0) {
					Class296_Sub39_Sub12.loginState = 0;
					BITConfigDefinition.method2352(-2, i_64_);
					Class377.loginConnection.method1966(320);
					Class240.method2148(true);
					return;
				}
				if (!Js5DiskStore.aBoolean1701) {
					Class296_Sub39_Sub12.loginState = 8;
				} else {
					Class296_Sub39_Sub12.loginState = 4;
				}
			}
			if (Class296_Sub39_Sub12.loginState == 4) {
				if (!Class377.loginConnection.aClass154_2045.method1556(true, 2)) {
					return;
				}
				Class377.loginConnection.aClass154_2045.method1559((byte) -13, 0, Class377.loginConnection.in_stream.data, 2);
				Class377.loginConnection.in_stream.pos = 0;
				Class377.loginConnection.in_stream.pos = Class377.loginConnection.in_stream.g2();
				Class296_Sub39_Sub12.loginState = 5;
			}
			if (Class296_Sub39_Sub12.loginState == 5) {
				if (!Class377.loginConnection.aClass154_2045.method1556(true, Class377.loginConnection.in_stream.pos)) {
					return;
				}
				Class377.loginConnection.aClass154_2045.method1559((byte) -113, 0, Class377.loginConnection.in_stream.data, Class377.loginConnection.in_stream.pos);
				Class377.loginConnection.in_stream.method2612(Class41_Sub28.anIntArray3816);
				Class377.loginConnection.in_stream.pos = 0;
				String string = Class377.loginConnection.in_stream.gjstr2();
				Class377.loginConnection.in_stream.pos = 0;
				String string_65_ = "opensn";
				if (!NodeDeque.js || Class354.method3692(Class252.aClass398_2383, string, 1, -19805, string_65_).anInt2540 == 2) {
					Class127.method1355(string_65_, Class252.aClass398_2383, string, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(122) == 1, true, 111);
				}
				Class296_Sub39_Sub12.loginState = 6;
			}
			if (Class296_Sub39_Sub12.loginState == 6) {
				if (!Class377.loginConnection.aClass154_2045.method1556(true, 1)) {
					return;
				}
				Class377.loginConnection.aClass154_2045.method1559((byte) 121, 0, Class377.loginConnection.in_stream.data, 1);
				if ((Class377.loginConnection.in_stream.data[0] & 0xff) == 1) {
					Class296_Sub39_Sub12.loginState = 7;
				}
			}
			if (i <= -9) {
				if (Class296_Sub39_Sub12.loginState == 7) {
					if (!Class377.loginConnection.aClass154_2045.method1556(true, 16)) {
						return;
					}
					Class377.loginConnection.aClass154_2045.method1559((byte) -25, 0, Class377.loginConnection.in_stream.data, 16);
					Class377.loginConnection.in_stream.pos = 16;
					Class377.loginConnection.in_stream.method2612(Class41_Sub28.anIntArray3816);
					Class377.loginConnection.in_stream.pos = 0;
					Class286.aString2643 = Class384.aString3252 = Class296_Sub13.decodeBase37(Class377.loginConnection.in_stream.g8());
					Class238.aLong3633 = Class377.loginConnection.in_stream.g8();
					Class296_Sub39_Sub12.loginState = 8;
				}
				if (Class296_Sub39_Sub12.loginState == 8) {
					Class377.loginConnection.in_stream.pos = 0;
					Class377.loginConnection.method1968(0);
					Class296_Sub1 class296_sub1 = Class296_Sub51_Sub30.method3169(51);
					ByteStream stream = class296_sub1.out;
					// world login
					if (Class187.anInt1910 == 2) {
						Class269 class269;
						if (Js5DiskStore.aBoolean1701) {
							class269 = Class292.aClass269_2678;
						} else {
							class269 = Class292.aClass269_2668;
						}
						stream.p1(class269.protocolId);
						stream.p2(0);
						int i_66_ = stream.pos;
						int i_67_ = stream.pos;
						if (!Js5DiskStore.aBoolean1701) {
							stream.p4(666);
							stream.p1(Class366_Sub6.anInt5392 == 14 ? 1 : 0);
							i_67_ = stream.pos;
							Packet class296_sub17 = Class391.method4041((byte) 78);
							stream.writeBytes(class296_sub17.data, 0, class296_sub17.pos);
							i_67_ = stream.pos;
							stream.writeString(Class286.aString2643);
						}
						stream.p1(Class220.anInt2150);
						stream.p1(Class8.method192((byte) 101));
						stream.p2(Class241.anInt2301);
						stream.p2(Class384.anInt3254);
						stream.p1(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5015.method475(117));
						Class366_Sub6.method3787(stream, (byte) 95);
						stream.writeString(Class347.settings);
						stream.p4(Class209.affliateID);
						Packet class296_sub17 = Class343_Sub1.aClass296_Sub50_5282.method3057(25);
						stream.p1(class296_sub17.pos);
						stream.writeBytes(class296_sub17.data, 0, class296_sub17.pos);
						Class348.aBoolean3033 = true;
						Packet class296_sub17_68_ = new Packet(Class360_Sub3.aClass296_Sub28_5309.method2682(true));
						Class360_Sub3.aClass296_Sub28_5309.method2684((byte) -113, class296_sub17_68_);
						stream.writeBytes(class296_sub17_68_.data, 0, class296_sub17_68_.data.length);
						stream.p4(Class296_Sub45_Sub2.interfaceCounter);
						stream.writeLong(Class189.userflow);
						stream.p1(Class368_Sub2.additionalInfo == null ? 0 : 1);
						if (Class368_Sub2.additionalInfo != null) {
							stream.writeString(Class368_Sub2.additionalInfo);
						}
						stream.p1(!Class194.method1930("jagtheora", 40) ? 0 : 1);
						stream.p1(!NodeDeque.js ? 0 : 1);
						Class219_Sub1.writeCrcValues(stream, (byte) 86);
						stream.encryptXteas(Class41_Sub28.anIntArray3816, i_67_, stream.pos);
						stream.endBytePacket(stream.pos - i_66_);
					} else {
						// lobby login
						Class269 class269;
						if (!Js5DiskStore.aBoolean1701) {
							class269 = Class292.aClass269_2670;
						} else {
							class269 = Class292.aClass269_2678;
						}
						stream.p1(class269.protocolId);
						stream.p2(0);
						int i_69_ = stream.pos;
						int i_70_ = stream.pos;
						if (!Js5DiskStore.aBoolean1701) {
							stream.p4(666);
							Packet class296_sub17 = Class391.method4041((byte) 126);
							stream.writeBytes(class296_sub17.data, 0, class296_sub17.pos);
							i_70_ = stream.pos;
							stream.writeString(Class286.aString2643);
						}
						stream.p1(Class296_Sub50.game.anInt340);
						stream.p1(Class394.langID);
						Class366_Sub6.method3787(stream, (byte) 95);
						stream.writeString(Class347.settings);
						stream.p4(Class209.affliateID);
						Class219_Sub1.writeCrcValues(stream, (byte) 86);
						stream.encryptXteas(Class41_Sub28.anIntArray3816, i_70_, stream.pos);
						stream.endBytePacket(-i_69_ + stream.pos);
					}
					Class377.loginConnection.sendPacket(class296_sub1, (byte) 119);
					Class377.loginConnection.method1963(true);
					Class377.loginConnection.aClass185_2053 = new ISAACCipher(Class41_Sub28.anIntArray3816);
					for (int i_71_ = 0; i_71_ < 4; i_71_++) {
						Class41_Sub28.anIntArray3816[i_71_] += 50;
					}
					Class377.loginConnection.in_stream.initCipher(Class41_Sub28.anIntArray3816);
					Class41_Sub28.anIntArray3816 = null;
					Class296_Sub39_Sub12.loginState = 9;
				}
				if (Class296_Sub39_Sub12.loginState == 9) {
					if (!Class377.loginConnection.aClass154_2045.method1556(true, 1)) {
						return;
					}
					Class377.loginConnection.aClass154_2045.method1559((byte) 110, 0, Class377.loginConnection.in_stream.data, 1);
					int responseId = Class377.loginConnection.in_stream.data[0] & 0xff;
					if (responseId == 21) {
						Class296_Sub39_Sub12.loginState = 12;
					} else if (responseId == 29 || responseId == 45) {
						Class296_Sub39_Sub12.loginState = 18;
						Class335.anInt2954 = responseId;
					} else if (responseId == 17) {
						// no acc was found, should show them the account creation box...
						Class296_Sub39_Sub12.loginState = 0;
						BITConfigDefinition.method2352(-2, responseId);
						Class377.loginConnection.aClass154_2045.method1561(126);
						Class377.loginConnection.aClass154_2045 = null;
						Class240.method2148(true);
						
						// sends acc creation
						CS2Call call = new CS2Call();
						call.callArgs = new Object[] { 2368 };
						CS2Executor.runCS2(call);
						return;
					} else {
						if (responseId == 1) {
							Class296_Sub39_Sub12.loginState = 10;
							BITConfigDefinition.method2352(-2, responseId);
							return;
						}
						if (responseId != 2) {
							if (responseId != 15) {
								if (responseId != 23 || Class105_Sub1.anInt5689 >= 3) {
									if (responseId == 42) {
										Class296_Sub39_Sub12.loginState = 20;
										BITConfigDefinition.method2352(-2, responseId);
									} else if (Class41_Sub5.aBoolean3752 && !Js5DiskStore.aBoolean1701 && Class296_Sub51_Sub14.anInt6423 != -1 && responseId == 35) {
										Js5DiskStore.aBoolean1701 = true;
										Class296_Sub39_Sub12.loginState = 1;
										Class296_Sub14.anInt4662 = 0;
										Class377.loginConnection.aClass154_2045.method1561(122);
										Class377.loginConnection.aClass154_2045 = null;
									} else {
										Class296_Sub39_Sub12.loginState = 0;
										BITConfigDefinition.method2352(-2, responseId);
										Class377.loginConnection.aClass154_2045.method1561(126);
										Class377.loginConnection.aClass154_2045 = null;
										Class240.method2148(true);
									}
								} else {
									Class296_Sub14.anInt4662 = 0;
									Class105_Sub1.anInt5689++;
									Class296_Sub39_Sub12.loginState = 1;
									Class377.loginConnection.aClass154_2045.method1561(124);
									Class377.loginConnection.aClass154_2045 = null;
									return;
								}
								return;
							}
							Class377.loginConnection.protSize = -2;
							Class296_Sub39_Sub12.loginState = 19;
						} else {
							Class296_Sub39_Sub12.loginState = 13;
						}
					}
				}
				if (Class296_Sub39_Sub12.loginState == 11) {
					Class377.loginConnection.method1968(0);
					Class296_Sub1 class296_sub1 = Class296_Sub51_Sub30.method3169(70);
					ByteStream class296_sub17_sub1 = class296_sub1.out;
					class296_sub17_sub1.setCipher(Class377.loginConnection.aClass185_2053);
					class296_sub17_sub1.writeOpcode(Class292.aClass269_2674.protocolId);
					Class377.loginConnection.sendPacket(class296_sub1, (byte) 119);
					Class377.loginConnection.method1963(true);
					Class296_Sub39_Sub12.loginState = 9;
				} else if (Class296_Sub39_Sub12.loginState == 12) {
					if (Class377.loginConnection.aClass154_2045.method1556(true, 1)) {
						Class377.loginConnection.aClass154_2045.method1559((byte) 111, 0, Class377.loginConnection.in_stream.data, 1);
						int i_73_ = Class377.loginConnection.in_stream.data[0] & 0xff;
						Class296_Sub40.anInt4914 = i_73_ * 50;
						Class296_Sub39_Sub12.loginState = 0;
						BITConfigDefinition.method2352(-2, 21);
						Class377.loginConnection.aClass154_2045.method1561(122);
						Class377.loginConnection.aClass154_2045 = null;
						Class240.method2148(true);
					}
				} else if (Class296_Sub39_Sub12.loginState == 20) {
					if (Class377.loginConnection.aClass154_2045.method1556(true, 2)) {
						Class377.loginConnection.aClass154_2045.method1559((byte) 111, 0, Class377.loginConnection.in_stream.data, 2);
						Class139.anInt1422 = ((Class377.loginConnection.in_stream.data[0] & 0xff) << 8) + (Class377.loginConnection.in_stream.data[1] & 0xff);
						Class296_Sub39_Sub12.loginState = 9;
					}
				} else if (Class296_Sub39_Sub12.loginState == 18) {
					if (Class335.anInt2954 != 29) {
						if (Class335.anInt2954 != 45) {
							throw new IllegalStateException("Login step 18 not valid for pendingResponse=" + Class335.anInt2954);
						}
						if (!Class377.loginConnection.aClass154_2045.method1556(true, 3)) {
							return;
						}
						Class377.loginConnection.aClass154_2045.method1559((byte) 124, 0, Class377.loginConnection.in_stream.data, 3);
						AdvancedMemoryCache.anInt1172 = ((Class377.loginConnection.in_stream.data[1] & 0xff) << 8) + (Class377.loginConnection.in_stream.data[2] & 0xff);
						ParticleEmitterRaw.anInt1771 = Class377.loginConnection.in_stream.data[0] & 0xff;
					} else {
						if (!Class377.loginConnection.aClass154_2045.method1556(true, 1)) {
							return;
						}
						Class377.loginConnection.aClass154_2045.method1559((byte) 110, 0, Class377.loginConnection.in_stream.data, 1);
						ParticleEmitterRaw.anInt1771 = Class377.loginConnection.in_stream.data[0] & 0xff;
					}
					Class296_Sub39_Sub12.loginState = 0;
					BITConfigDefinition.method2352(-2, Class335.anInt2954);
					Class377.loginConnection.aClass154_2045.method1561(124);
					Class377.loginConnection.aClass154_2045 = null;
					Class240.method2148(true);
				} else if (Class296_Sub39_Sub12.loginState == 13) {
					if (Class377.loginConnection.aClass154_2045.method1556(true, 1)) {
						Class377.loginConnection.aClass154_2045.method1559((byte) 116, 0, Class377.loginConnection.in_stream.data, 1);
						Class368_Sub12.anInt5490 = Class377.loginConnection.in_stream.data[0] & 0xff;
						Class296_Sub39_Sub12.loginState = 14;
					}
				} else {
					if (Class296_Sub39_Sub12.loginState == 14) {
						ByteStream class296_sub17_sub1 = Class377.loginConnection.in_stream;
						if (Class187.anInt1910 == 2) {
							if (!Class377.loginConnection.aClass154_2045.method1556(true, Class368_Sub12.anInt5490)) {
								return;
							}
							Class377.loginConnection.aClass154_2045.method1559((byte) 113, 0, class296_sub17_sub1.data, Class368_Sub12.anInt5490);
							class296_sub17_sub1.pos = 0;
							Class338_Sub3_Sub5.rights = class296_sub17_sub1.g1();
							RuntimeException_Sub1.anInt3395 = class296_sub17_sub1.g1();
							Class296_Sub40.somethingWithIgnore0 = class296_sub17_sub1.g1() == 1;
							Class41_Sub17.somethingWithIgnore1 = class296_sub17_sub1.g1() == 1;
							EffectiveVertex.aBoolean2221 = class296_sub17_sub1.g1() == 1;
							Class123.somethingWithIgnore2 = class296_sub17_sub1.g1() == 1;
							Class362.myPlayerIndex = class296_sub17_sub1.g2();
							aa_Sub1.memb1 = class296_sub17_sub1.g1() == 1;
							Class268.anInt2486 = class296_sub17_sub1.method2566();
							Class172.member = class296_sub17_sub1.g1() == 1;
							System.out.println("Logging to world: " + Class172.member);
							Class296_Sub38.aString4886 = class296_sub17_sub1.gstr();
							Class379.objectDefinitionLoader.setMembersOnly(Class172.member);
							Class296_Sub39_Sub1.itemDefinitionLoader.update_members(Class172.member);
							Class352.npcDefinitionLoader.method1416(-105, Class172.member);
						} else {
							if (!Class377.loginConnection.aClass154_2045.method1556(true, Class368_Sub12.anInt5490)) {
								return;
							}
							Class377.loginConnection.aClass154_2045.method1559((byte) -70, 0, class296_sub17_sub1.data, Class368_Sub12.anInt5490);
							class296_sub17_sub1.pos = 0;
							Class338_Sub3_Sub5.rights = class296_sub17_sub1.g1();
							RuntimeException_Sub1.anInt3395 = class296_sub17_sub1.g1();
							Class296_Sub40.somethingWithIgnore0 = class296_sub17_sub1.g1() == 1;
							Class41_Sub17.somethingWithIgnore1 = class296_sub17_sub1.g1() == 1;
							EffectiveVertex.aBoolean2221 = class296_sub17_sub1.g1() == 1;
							Class165.aLong1688 = class296_sub17_sub1.g8();
							Class340.aLong3708 = Class165.aLong1688 - (Class72.method771(-106) + class296_sub17_sub1.method2614());
							int i_74_ = class296_sub17_sub1.g1();
							aa_Sub1.memb1 = (i_74_ & 0x1) != 0;
							Class268.aBoolean2495 = (i_74_ & 0x2) != 0;
							Class366_Sub2.anInt5369 = class296_sub17_sub1.g4();
							Class366_Sub7.aBoolean5402 = class296_sub17_sub1.g1() == 1;
							Class104.anInt1088 = class296_sub17_sub1.g4();
							Class296_Sub51_Sub3.anInt6343 = class296_sub17_sub1.g2();
							Js5TextureLoader.anInt3442 = class296_sub17_sub1.g2();
							Class206.anInt2068 = class296_sub17_sub1.g2();
							Class373_Sub3.anInt5608 = class296_sub17_sub1.g4();
							Class296_Sub44.aClass278_4949 = Class252.aClass398_2383.method4121(20, Class373_Sub3.anInt5608);
							Class87.anInt942 = class296_sub17_sub1.g1();
							StaticMethods.anInt5058 = class296_sub17_sub1.g2();
							StaticMethods.anInt5968 = class296_sub17_sub1.g2();
							ModeWhere.aBoolean2354 = class296_sub17_sub1.g1() == 1;
							Class296_Sub51_Sub11.localPlayer.displayName = Class296_Sub51_Sub11.localPlayer.name = Class366_Sub6.name3 = class296_sub17_sub1.gjstr2();
							Class42_Sub1.anInt3834 = class296_sub17_sub1.g1();
							Class296_Sub35_Sub1.anInt6109 = class296_sub17_sub1.g4();
							Class373.aBoolean3178 = class296_sub17_sub1.g1() == 1;
							Class10.aClass112_3533 = new World();
							Class10.aClass112_3533.worldId = class296_sub17_sub1.g2();
							if (Class10.aClass112_3533.worldId == 65535) {
								Class10.aClass112_3533.worldId = -1;
							}
							Class10.aClass112_3533.ipAddress = class296_sub17_sub1.gjstr2();
							if (BITConfigsLoader.liveModeWhere != Class41_Sub29.modeWhere) {
								Class10.aClass112_3533.webPort = Class10.aClass112_3533.worldId + 50000;
								Class10.aClass112_3533.port = Class10.aClass112_3533.worldId + 40000;
							}
							if (Class296_Sub34_Sub2.localModeWhere != Class41_Sub29.modeWhere && (Class121.office2ModeWhere != Class41_Sub29.modeWhere || Class338_Sub3_Sub5.rights < 2) && Class296_Sub51_Sub27_Sub1.aClass112_6733.method980((byte) 122, Class338_Sub8.gameWorld)) {
								NPCDefinition.method1495(-15477);
							}
						}
						if (Class296_Sub40.somethingWithIgnore0 && !EffectiveVertex.aBoolean2221 || aa_Sub1.memb1) {
							try {
								Class297.method3231("zap", CS2Script.anApplet6140, false);
							} catch (Throwable throwable) {
								if (Class213.advert) {
									try {
										CS2Script.anApplet6140.getAppletContext().showDocument(new URL(CS2Script.anApplet6140.getCodeBase(), "blank.ws"), "tbi");
									} catch (Exception exception) {
										/* empty */
									}
								}
							}
						} else {
							try {
								Class297.method3231("unzap", CS2Script.anApplet6140, false);
							} catch (Throwable throwable) {
								/* empty */
							}
						}
						if (BITConfigsLoader.liveModeWhere == Class41_Sub29.modeWhere) {
							try {
								Class297.method3231("loggedin", CS2Script.anApplet6140, false);
							} catch (Throwable throwable) {
								/* empty */
							}
						}
						if (Class187.anInt1910 != 2) {
							Class296_Sub39_Sub12.loginState = 0;
							BITConfigDefinition.method2352(-2, 2);
							Class13.method225(11878);
							Class41_Sub8.method422(1, 7);
							Class377.loginConnection.incomingPacket = null;
							return;
						}
						Class296_Sub39_Sub12.loginState = 16;
					}
					if (Class296_Sub39_Sub12.loginState == 16) {
						if (!Class377.loginConnection.aClass154_2045.method1556(true, 3)) {
							return;
						}
						Class377.loginConnection.aClass154_2045.method1559((byte) -106, 0, Class377.loginConnection.in_stream.data, 3);
						Class296_Sub39_Sub12.loginState = 17;
					}
					if (Class296_Sub39_Sub12.loginState == 17) {
						ByteStream class296_sub17_sub1 = Class377.loginConnection.in_stream;
						class296_sub17_sub1.pos = 0;
						if (class296_sub17_sub1.nextOpcodeBig()) {
							if (!Class377.loginConnection.aClass154_2045.method1556(true, 1)) {
								return;
							}
							Class377.loginConnection.aClass154_2045.method1559((byte) 121, 3, class296_sub17_sub1.data, 1);
						}
						Class377.loginConnection.incomingPacket = Class223.method2078((byte) 34)[class296_sub17_sub1.readOpcode()];
						Class377.loginConnection.protSize = class296_sub17_sub1.g2();
						Class296_Sub39_Sub12.loginState = 15;
					}
					if (Class296_Sub39_Sub12.loginState == 15) {
						if (Class377.loginConnection.aClass154_2045.method1556(true, Class377.loginConnection.protSize)) {
							Class377.loginConnection.aClass154_2045.method1559((byte) -30, 0, Class377.loginConnection.in_stream.data, Class377.loginConnection.protSize);
							Class377.loginConnection.in_stream.pos = 0;
							Class296_Sub39_Sub12.loginState = 0;
							int i_75_ = Class377.loginConnection.protSize;
							BITConfigDefinition.method2352(-2, 2);
							Class41_Sub1.method391(-28107);
							PlayerUpdate.readLoginData(Class377.loginConnection.in_stream);
							Class41_Sub6.anInt3760 = -1;
							if (Class241_Sub2_Sub1.aClass231_5897 == Class377.loginConnection.incomingPacket) {
								Class119.readBuildMapDynamic();
							} else {
								Class360_Sub2.method3737(true);
							}
							if (Class377.loginConnection.in_stream.pos != i_75_) {
								throw new RuntimeException("lswp pos:" + Class377.loginConnection.in_stream.pos + " psize:" + i_75_);
							}
							Class377.loginConnection.incomingPacket = null;
						}
					} else if (Class296_Sub39_Sub12.loginState == 19) {
						if (Class377.loginConnection.protSize == -2) {
							if (!Class377.loginConnection.aClass154_2045.method1556(true, 2)) {
								return;
							}
							Class377.loginConnection.aClass154_2045.method1559((byte) 124, 0, Class377.loginConnection.in_stream.data, 2);
							Class377.loginConnection.in_stream.pos = 0;
							Class377.loginConnection.protSize = Class377.loginConnection.in_stream.g2();
						}
						if (Class377.loginConnection.aClass154_2045.method1556(true, Class377.loginConnection.protSize)) {
							Class377.loginConnection.aClass154_2045.method1559((byte) -7, 0, Class377.loginConnection.in_stream.data, Class377.loginConnection.protSize);
							Class377.loginConnection.in_stream.pos = 0;
							int i_76_ = Class377.loginConnection.protSize;
							Class296_Sub39_Sub12.loginState = 0;
							BITConfigDefinition.method2352(-2, 15);
							LookupTable.method165((byte) 103);
							PlayerUpdate.readLoginData(Class377.loginConnection.in_stream);
							if (i_76_ != Class377.loginConnection.in_stream.pos) {
								throw new RuntimeException("lswpr pos:" + Class377.loginConnection.in_stream.pos + " psize:" + i_76_);
							}
							Class377.loginConnection.incomingPacket = null;
						}
					}
				}
			}
		} catch (IOException ioexception) {
			Class377.loginConnection.method1966(320);
			if (Class105_Sub1.anInt5689 < 3) {
				if (Class187.anInt1910 == 2) {
					Class296_Sub51_Sub27_Sub1.aClass112_6733.method978((byte) 127);
				} else {
					Class338_Sub3_Sub3_Sub1.lobbyWorld.method978((byte) 123);
				}
				Class105_Sub1.anInt5689++;
				Class296_Sub39_Sub12.loginState = 1;
				Class296_Sub14.anInt4662 = 0;
			} else {
				Class296_Sub39_Sub12.loginState = 0;
				BITConfigDefinition.method2352(-2, -4);
				Class240.method2148(true);
			}
		}
	}
	
	private final void method1687(ha_Sub3 var_ha_Sub3, byte i) {
		Class360_Sub3.aFloat5307 = var_ha_Sub3.aFloat4217;
		if (i != -11) {
			method1689(38);
		}
		var_ha_Sub3.method1315(1);
		OpenGL.glDisable(16384);
		OpenGL.glDisable(16385);
		var_ha_Sub3.method1285(-39, false);
		OpenGL.glNormal3f(0.0F, -1.0F, 0.0F);
	}
	
	final void method1688(int i, ha_Sub3 var_ha_Sub3) {
		anInterface5_1805 = var_ha_Sub3.method1321(196584, true, 24, null, 5888);
		aClass272_1808 = new Class272(anInterface5_1805, 5126, 2, 0);
		aClass272_1806 = new Class272(anInterface5_1805, 5126, 3, 8);
		if (i == 1) {
			aClass272_1804 = new Class272(anInterface5_1805, 5121, 4, 20);
		}
	}
	
	static final void method1689(int i) {
		Class187.someAppereanceBuffer = null;
		Class296_Sub36.someSecondaryXteaBuffer = null;
		if (i <= 115) {
			aClass311_1802 = null;
		}
		StaticMethods.aBoolean1183 = false;
		za_Sub1.anInt6554 = 4;
		HardReferenceWrapper.method2795((byte) -127);
		Class296_Sub45_Sub2.aClass204_6277.sendPacket(Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 121, Class52.aClass311_641), (byte) 119);
	}
	
	final void method1690(int i, ha_Sub3 var_ha_Sub3, Class390 class390, int i_77_) {
		if (var_ha_Sub3.aClass373_Sub1_4168 != null) {
			if (i < 0) {
				method1687(var_ha_Sub3, (byte) -11);
			} else {
				method1684(i, true, var_ha_Sub3);
			}
			float f = var_ha_Sub3.aClass373_Sub1_4168.aFloat5587;
			float f_78_ = var_ha_Sub3.aClass373_Sub1_4168.aFloat5579;
			float f_79_ = var_ha_Sub3.aClass373_Sub1_4168.aFloat5589;
			float f_80_ = var_ha_Sub3.aClass373_Sub1_4168.aFloat5588;
			try {
				int i_81_ = 0;
				int i_82_ = 2147483647;
				int i_83_ = 0;
				Class338_Sub8 class338_sub8 = class390.aClass84_3282.aClass338_Sub8_923;
				Class338_Sub8 class338_sub8_84_ = class338_sub8.aClass338_Sub8_5253;
				if (i_77_ != 11321) {
					method1686((byte) -86);
				}
				for (/**/; class338_sub8_84_ != class338_sub8; class338_sub8_84_ = class338_sub8_84_.aClass338_Sub8_5253) {
					Class338_Sub8_Sub2 class338_sub8_sub2 = (Class338_Sub8_Sub2) class338_sub8_84_;
					int i_85_ = (int) (f_80_ + (f_79_ * (class338_sub8_sub2.anInt6581 >> 12) + (f_78_ * (class338_sub8_sub2.anInt6589 >> 12) + f * (class338_sub8_sub2.anInt6580 >> 12))));
					anIntArray1810[i_81_++] = i_85_;
					if (i_85_ < i_82_) {
						i_82_ = i_85_;
					}
					if (i_83_ < i_85_) {
						i_83_ = i_85_;
					}
				}
				int i_86_ = i_83_ - i_82_;
				int i_87_;
				if (i_86_ + 2 <= 1600) {
					i_86_ += 2;
					i_87_ = 0;
				} else {
					i_87_ = Class296_Sub29_Sub2.method2694((byte) 115, i_86_) - anInt1807 + 1;
					i_86_ = (i_86_ >> i_87_) + 2;
				}
				class338_sub8_84_ = class338_sub8.aClass338_Sub8_5253;
				i_81_ = 0;
				int i_88_ = -2;
				boolean bool = true;
				boolean bool_89_ = true;
				while (class338_sub8 != class338_sub8_84_) {
					anInt1813 = 0;
					for (int i_90_ = 0; i_86_ > i_90_; i_90_++) {
						anIntArray1812[i_90_] = 0;
					}
					for (int i_91_ = 0; i_91_ < 64; i_91_++) {
						anIntArray1811[i_91_] = 0;
					}
					for (/**/; class338_sub8 != class338_sub8_84_; class338_sub8_84_ = class338_sub8_84_.aClass338_Sub8_5253) {
						Class338_Sub8_Sub2 class338_sub8_sub2 = (Class338_Sub8_Sub2) class338_sub8_84_;
						if (bool_89_) {
							bool_89_ = false;
							i_88_ = class338_sub8_sub2.anInt6582;
							bool = class338_sub8_sub2.aBoolean6578;
						}
						if (i_81_ > 0 && (i_88_ != class338_sub8_sub2.anInt6582 || bool == !class338_sub8_sub2.aBoolean6578)) {
							bool_89_ = true;
							break;
						}
						int i_92_ = anIntArray1810[i_81_++] - i_82_ >> i_87_;
						if (i_92_ < 1600) {
							if (anIntArray1812[i_92_] < 64) {
								aClass338_Sub8_Sub2ArrayArray1814[i_92_][anIntArray1812[i_92_]++] = class338_sub8_sub2;
							} else {
								if (anIntArray1812[i_92_] == 64) {
									if (anInt1813 == 64) {
										continue;
									}
									anIntArray1812[i_92_] += anInt1813++ + 1;
								}
								aClass338_Sub8_Sub2ArrayArray1809[anIntArray1812[i_92_] - 64 - 1][anIntArray1811[anIntArray1812[i_92_] - 64 - 1]++] = class338_sub8_sub2;
							}
						}
					}
					if (i_88_ >= 0) {
						var_ha_Sub3.method1340(i_88_, (byte) 42);
					} else {
						var_ha_Sub3.method1340(-1, (byte) 106);
					}
					if (bool && Class360_Sub3.aFloat5307 != var_ha_Sub3.aFloat4217) {
						var_ha_Sub3.xa(Class360_Sub3.aFloat5307);
					} else if (var_ha_Sub3.aFloat4217 != 1.0F) {
						var_ha_Sub3.xa(1.0F);
					}
					method1685(i_77_ - 11388, var_ha_Sub3, i_86_);
				}
			} catch (Exception exception) {
				/* empty */
			}
			method1683(16384, var_ha_Sub3);
		}
	}
	
	Class173() {
		aClass296_Sub17_Sub2_1801 = new Class296_Sub17_Sub2(786336);
		anInt1807 = Class296_Sub29_Sub2.method2694((byte) -63, 1600);
		anIntArray1812 = new int[1600];
		aClass338_Sub8_Sub2ArrayArray1809 = new Class338_Sub8_Sub2[64][768];
		aClass338_Sub8_Sub2ArrayArray1814 = new Class338_Sub8_Sub2[1600][64];
		anIntArray1810 = new int[8191];
		anInt1813 = 0;
		anIntArray1811 = new int[64];
	}
}
