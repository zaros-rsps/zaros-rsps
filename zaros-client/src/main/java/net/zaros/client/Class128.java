package net.zaros.client;

/* Class128 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class128 {
	int anInt1305;
	int anInt1306;
	int anInt1307;
	int anInt1308;
	int anInt1309;
	int anInt1310;
	int anInt1311;
	int anInt1312 = 128;
	int anInt1313;
	int anInt1314 = 128;
	static int anInt1315;
	int anInt1316;

	final void method1361(Class128 class128_0_, byte i) {
		anInt1316 = class128_0_.anInt1316;
		anInt1307 = class128_0_.anInt1307;
		if (i != -108)
			anInt1313 = 31;
		anInt1312 = class128_0_.anInt1312;
		anInt1309 = class128_0_.anInt1309;
		anInt1311 = class128_0_.anInt1311;
		anInt1314 = class128_0_.anInt1314;
	}

	final Class128 method1362(int i) {
		if (i != -23485)
			return null;
		return new Class128(anInt1311, anInt1314, anInt1312, anInt1309, anInt1316, anInt1307);
	}

	static final Mobile method1363(int i, int i_1_, int i_2_, int i_3_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i_2_][i_3_][i_1_];
		if (class247 == null)
			return null;
		Mobile class338_sub3_sub1_sub3 = null;
		int i_4_ = -1;
		Class234 class234 = class247.aClass234_2341;
		if (i != 256)
			return null;
		for (/**/; class234 != null; class234 = class234.aClass234_2224) {
			Class338_Sub3_Sub1 class338_sub3_sub1 = class234.aClass338_Sub3_Sub1_2226;
			if (class338_sub3_sub1 instanceof Mobile) {
				Mobile class338_sub3_sub1_sub3_5_ = (Mobile) class338_sub3_sub1;
				int i_6_ = (class338_sub3_sub1_sub3_5_.getSize() * 256 - 4);
				int i_7_ = class338_sub3_sub1_sub3_5_.tileX - i_6_ >> 9;
				int i_8_ = -i_6_ + class338_sub3_sub1_sub3_5_.tileY >> 9;
				int i_9_ = class338_sub3_sub1_sub3_5_.tileX + i_6_ >> 9;
				int i_10_ = class338_sub3_sub1_sub3_5_.tileY + i_6_ >> 9;
				if (i_7_ <= i_3_ && i_1_ >= i_8_ && i_3_ <= i_9_ && i_1_ <= i_10_) {
					int i_11_ = (-i_1_ + 1 + i_10_) * (-i_3_ + 1 + i_9_);
					if (i_4_ < i_11_) {
						class338_sub3_sub1_sub3 = class338_sub3_sub1_sub3_5_;
						i_4_ = i_11_;
					}
				}
			}
		}
		return class338_sub3_sub1_sub3;
	}

	Class128(int i) {
		anInt1311 = i;
	}

	private Class128(int i, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_) {
		anInt1307 = i_16_;
		anInt1311 = i;
		anInt1314 = i_12_;
		anInt1312 = i_13_;
		anInt1309 = i_14_;
		anInt1316 = i_15_;
	}
}
