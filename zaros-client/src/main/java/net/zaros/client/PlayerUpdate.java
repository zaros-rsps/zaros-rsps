package net.zaros.client;

public class PlayerUpdate {
	
	static boolean gpiDebug = false;
	
	static byte[] skipData = new byte[2048];
	
	static int[] updatablePlayersIndexes = new int[2048];
	
	static int updatablePlayersCount = 0;
	
	static int invisiblePlayersCount = 0;
	
	static int visiblePlayersCount = 0;
	
	static int[] invisiblePlayersIndexes = new int[2048];
	
	static int[] visiblePlayersIndexes = new int[2048];
	
	static Player[] visiblePlayers = new Player[2048];
	
	static InvisiblePlayer[] invisiblePlayers = new InvisiblePlayer[2048];
	
	static Packet[] cachedAppereances = new Packet[2048];
	
	static byte[] playerWalkingTypes = new byte[2048];
	
	static final void parseUpdatePacket(ByteStream buffer, int packetSize) {
		PlayerUpdate.updatablePlayersCount = 0;
		PlayerUpdate.gpiDebug = false;
		PlayerUpdate.decodeGlobalPlayerUpdate(buffer);
		PlayerUpdate.parseMasks(buffer);
		if (PlayerUpdate.gpiDebug) {
			System.out.println("---endgpp---");
		}
		if (buffer.pos != packetSize) {
			throw new RuntimeException("gpi1 pos:" + buffer.pos + " psize:" + packetSize);
		}
	}
	
	static final void parseMasks(ByteStream buff) {
		for (int i = 0; i < PlayerUpdate.updatablePlayersCount; i++) {
			int index = PlayerUpdate.updatablePlayersIndexes[i];
			Player p = PlayerUpdate.visiblePlayers[index];
			int flag = buff.g1();
			if ((flag & 0x1) != 0) {
				flag += buff.g1() << 8;
			}
			if ((flag & 0x200) != 0) {
				flag += buff.g1() << 16;
			}
			PlayerUpdate.parseMasksFor(p, flag, index, buff);
		}
	}
	
	static void decodeGlobalPlayerUpdate(ByteStream buff) {
		int skipCount = 0;
		buff.prepareBITStream();
		for (int i = 0; i < PlayerUpdate.visiblePlayersCount; i++) {
			int index = PlayerUpdate.visiblePlayersIndexes[i];
			if (skipCount > 0) {
				skipCount--;
			} else {
				int shouldUpdate = buff.readBITS(1);
				if (shouldUpdate == 0) {
					skipCount = PlayerUpdate.readSkip(buff);
				} else {
					PlayerUpdate.updateVisiblePlayer(buff, index);
				}
			}
		}
		buff.endBITStream();
		if (skipCount != 0) {
			System.out.println("Local player rendering - wrong skipcount [" + skipCount + "].");
		}
		buff.prepareBITStream();
		for (int i = 0; PlayerUpdate.invisiblePlayersCount > i; i++) {
			int index = PlayerUpdate.invisiblePlayersIndexes[i];
			if (skipCount > 0) {
				skipCount--;
			} else {
				int shouldUpdate = buff.readBITS(1);
				if (shouldUpdate == 0) {
					skipCount = PlayerUpdate.readSkip(buff);
				} else if (PlayerUpdate.updateInvisiblePlayer(buff, index)) {
					// do nothing
				}
			}
		}
		buff.endBITStream();
		if (skipCount != 0) {
			System.out.println("Global player rendering - wrong skipcount [" + skipCount + "].");
		}
		PlayerUpdate.visiblePlayersCount = 0;
		PlayerUpdate.invisiblePlayersCount = 0;
		for (int pIndex = 1; pIndex < 2048; pIndex++) {
			Player p = PlayerUpdate.visiblePlayers[pIndex];
			if (p == null) {
				PlayerUpdate.invisiblePlayersIndexes[PlayerUpdate.invisiblePlayersCount++] = pIndex;
			} else {
				PlayerUpdate.visiblePlayersIndexes[PlayerUpdate.visiblePlayersCount++] = pIndex;
			}
		}
	}
	
	static final void parseMasksFor(Player player, int flag, int pIndex, ByteStream str) {
		if ((flag & 0x800) != 0) { // gfx2
			int i_16_ = str.g2();
			if (i_16_ == 65535) {
				i_16_ = -1;
			}
			int i_17_ = str.readInt_v1();
			int i_18_ = str.readUnsignedByteC();
			int i_19_ = i_18_ & 0x7;
			int i_20_ = i_18_ >> 3 & 0xf;
			if (i_20_ == 15) {
				i_20_ = -1;
			}
			boolean bool_21_ = (i_18_ & 0x83) >> 7 == 1;
			player.playGraphic(i_17_, 1, i_19_, i_20_, bool_21_, 65535, i_16_);
		}
		byte extraWalkType = -1;
		if ((flag & 0x20000) != 0) {
			player.aByte6808 = str.readSignedByteA();
			player.aByte6818 = str.readSignedByteC();
			player.aByte6822 = str.readSignedByteS();
			player.aByte6806 = (byte) str.readUnsignedByteA();
			player.anInt6820 = Class29.anInt307 + str.readUnsignedLEShort();
			player.anInt6803 = Class29.anInt307 + str.readUnsignedLEShort();
		}
		if ((flag & 0x8) != 0) { // hits
			int i_23_ = str.readUnsignedByteA();
			if (i_23_ > 0) {
				for (int i_24_ = 0; i_24_ < i_23_; i_24_++) {
					int i_25_ = -1;
					int i_26_ = -1;
					int i_27_ = str.readSmart();
					int i_28_ = -1;
					if (i_27_ == 32767) {
						i_27_ = str.readSmart();
						i_26_ = str.readSmart();
						i_25_ = str.readSmart();
						i_28_ = str.readSmart();
					} else if (i_27_ == 32766) {
						i_27_ = -1;
					} else {
						i_26_ = str.readSmart();
					}
					int i_29_ = str.readSmart();
					int i_30_ = str.g1();
					player.applyHit(i_27_, i_25_, i_26_, i_28_, i_29_, i_30_, Class29.anInt307);
				}
			}
		}
		if ((flag & 0x40000) != 0) {
			int i_31_ = str.g1();
			int[] is = new int[i_31_];
			int[] is_32_ = new int[i_31_];
			for (int i_33_ = 0; i_33_ < i_31_; i_33_++) {
				int i_34_ = str.readUnsignedLEShort();
				if ((i_34_ & 0xc000) == 49152) {
					int i_35_ = str.readUnsignedShortA();
					is[i_33_] = Class48.bitOR(i_34_ << 16, i_35_);
				} else {
					is[i_33_] = i_34_;
				}
				is_32_[i_33_] = str.readUnsignedShortA();
			}
			player.method3512((byte) -122, is, is_32_);
		}
		if ((flag & 0x100) != 0) {
			player.aBoolean6879 = str.readUnsignedByteS() == 1;
		}
		if ((flag & 0x100000) != 0) { // gfx4
			int i_36_ = str.readUnsignedLEShort();
			int i_37_ = str.g4();
			if (i_36_ == 65535) {
				i_36_ = -1;
			}
			int i_38_ = str.readUnsignedByteC();
			int i_39_ = i_38_ & 0x7;
			int i_40_ = i_38_ >> 3 & 0xf;
			if (i_40_ == 15) {
				i_40_ = -1;
			}
			boolean bool_41_ = (i_38_ & 0x95) >> 7 == 1;
			player.playGraphic(i_37_, 2, i_39_, i_40_, bool_41_, 65535, i_36_);
		}
		if ((flag & 0x2) != 0) { //appereance
			int length = str.g1();
			byte[] app = new byte[length];
			Packet buff = new Packet(app);
			str.readBytesA(app, 0, length);
			PlayerUpdate.cachedAppereances[pIndex] = buff;
			player.updateAppereance(buff);
		}
		if ((flag & 0x4000) != 0) { // forcechat
			String string = str.gstr();
			if (string.charAt(0) == '~') {
				string = string.substring(1);
				Class181.method1827(0, player.getFullName(true), 0, player.name, string, player.getName(false), 2);
			} else if (player == (Class296_Sub51_Sub11.localPlayer)) {
				Class181.method1827(0, player.getFullName(true), 0, player.name, string, player.getName(false), 2);
			}
			player.method3524(string, 0, 0, 126);
		}
		if ((flag & 0x1000) != 0) { // forcewalk
			player.anInt6813 = str.readSignedByteS();
			player.anInt6811 = str.g1b();
			player.anInt6821 = str.readSignedByteS();
			player.anInt6805 = str.readSignedByteS();
			player.anInt6809 = (str.g2() + Class29.anInt307);
			player.anInt6807 = (str.readUnsignedLEShortA() + Class29.anInt307);
			player.anInt6819 = str.readUnsignedByteC();
			if (!player.needUpdateMoving) {
				player.currentWayPoint = 1;
				player.anInt6811 += player.wayPointQueueY[0];
				player.anInt6813 += player.waypointQueueX[0];
				player.anInt6805 += player.wayPointQueueY[0];
				player.anInt6821 += player.waypointQueueX[0];
			} else {
				player.anInt6811 += player.moveY;
				player.anInt6821 += player.moveX;
				player.anInt6805 += player.moveY;
				player.anInt6813 += player.moveX;
				player.currentWayPoint = 0;
			}
			player.anInt6829 = 0;
		}
		if ((flag & 0x2000) != 0) {
			int i_43_ = str.g1();
			int[] is = new int[i_43_];
			int[] is_44_ = new int[i_43_];
			int[] is_45_ = new int[i_43_];
			for (int i_46_ = 0; i_46_ < i_43_; i_46_++) {
				int i_47_ = str.readUnsignedLEShort();
				if (i_47_ == 65535) {
					i_47_ = -1;
				}
				is[i_46_] = i_47_;
				is_44_[i_46_] = str.g1();
				is_45_[i_46_] = str.readUnsignedLEShort();
			}
			Class16_Sub2.method243(is, is_44_, is_45_, player, true);
		}
		if ((flag & 0x8000) != 0) { // secondary hp bar
			int i_48_ = str.readUnsignedLEShort();
			player.anInt6785 = str.readUnsignedByteC();
			player.anInt6765 = str.readUnsignedByteA();
			player.anInt6780 = i_48_ & 0x7fff;
			player.aBoolean6764 = (i_48_ & 0x8000) != 0;
			player.anInt6799 = (player.anInt6780 + (Class29.anInt307 + player.anInt6785));
		}
		if ((flag & 0x400) != 0) // teleport type
		{
			extraWalkType = str.readSignedByteC();
		}
		if ((flag & 0x80) != 0) { // gfx1
			int i_49_ = str.readUnsignedLEShort();
			if (i_49_ == 65535) {
				i_49_ = -1;
			}
			int i_50_ = str.readInt_v1();
			int i_51_ = str.readUnsignedByteA();
			int i_52_ = i_51_ & 0x7;
			int i_53_ = (i_51_ & 0x7c) >> 3;
			if (i_53_ == 15) {
				i_53_ = -1;
			}
			boolean bool_54_ = (i_51_ & 0xc1) >> 7 == 1;
			player.playGraphic(i_50_, 0, i_52_, i_53_, bool_54_, 65535, i_49_);
		}
		if ((flag & 0x10000) != 0) { // gfx3
			int i_55_ = str.readUnsignedLEShort();
			int i_56_ = str.g4();
			if (i_55_ == 65535) {
				i_55_ = -1;
			}
			int i_57_ = str.readUnsignedByteS();
			int i_58_ = i_57_ & 0x7;
			int i_59_ = (i_57_ & 0x7c) >> 3;
			if (i_59_ == 15) {
				i_59_ = -1;
			}
			boolean bool_60_ = (i_57_ >> 7 & 0x1) == 1;
			player.playGraphic(i_56_, 3, i_58_, i_59_, bool_60_, 65535, i_55_);
		}
		if ((flag & 0x20) != 0) {
			player.someFacingDirection = str.readUnsignedLEShortA();
			if (player.currentWayPoint == 0) {
				player.method3499(player.someFacingDirection);
				player.someFacingDirection = -1;
			}
		}
		if ((flag & 0x10) != 0) { // anim
			int[] is = new int[4];
			for (int i_61_ = 0; i_61_ < 4; i_61_++) {
				is[i_61_] = str.readUnsignedShortA();
				if (is[i_61_] == 65535) {
					is[i_61_] = -1;
				}
			}
			int i_62_ = str.g1();
			Class160.method1619(i_62_, 0, false, is, player);
		}
		if ((flag & 0x80000) != 0) {
			player.aBoolean6868 = str.g1() == 1;
		}
		if ((flag & 0x40) != 0) // walk type
		{
			PlayerUpdate.playerWalkingTypes[pIndex] = str.readSignedByteS();
		}
		if ((flag & 0x4) != 0) { // turnto
			int i_63_ = str.g2();
			if (i_63_ == 65535) {
				i_63_ = -1;
			}
			player.interactingWithIndex = i_63_;
		}
		if (player.needUpdateMoving) {
			if (extraWalkType != 127) {
				byte walkType;
				if (extraWalkType == -1) {
					walkType = PlayerUpdate.playerWalkingTypes[pIndex];
				} else {
					walkType = extraWalkType;
				}
				Class397_Sub3.method4110((byte) 1, walkType, player);
				player.updatePosition(player.moveX, player.moveY, walkType);
			} else {
				player.setPosition(player.moveX, player.moveY);
			}
		}
	}
	
	static final void readLoginData(ByteStream in) {
		in.prepareBITStream();
		int myIndex = Class362.myPlayerIndex;
		Player myPlayer = (Class296_Sub51_Sub11.localPlayer = PlayerUpdate.visiblePlayers[myIndex] = new Player());
		myPlayer.index = myIndex;
		int myLocHash = in.readBITS(30);
		int myX = (myLocHash & 0xfffc322) >> 14;
		int myY = myLocHash & 0x3fff;
		byte myZ = (byte) (myLocHash >> 28);
		myPlayer.waypointQueueX[0] = myX - Class206.worldBaseX;
		myPlayer.wayPointQueueY[0] = myY - Class41_Sub26.worldBaseY;
		myPlayer.tileX = ((myPlayer.waypointQueueX[0] << 9) + (myPlayer.getSize() << 8));
		myPlayer.tileY = ((myPlayer.wayPointQueueY[0] << 9) + (myPlayer.getSize() << 8));
		FileWorker.anInt3005 = myPlayer.z = myPlayer.aByte5203 = myZ;
		if (r_Sub2.method2871(myPlayer.wayPointQueueY[0], myPlayer.waypointQueueX[0], (byte) -47)) {
			myPlayer.aByte5203++;
		}
		if (PlayerUpdate.cachedAppereances[myIndex] != null) {
			myPlayer.updateAppereance(PlayerUpdate.cachedAppereances[myIndex]);
		}
		PlayerUpdate.visiblePlayersCount = 0;
		PlayerUpdate.visiblePlayersIndexes[PlayerUpdate.visiblePlayersCount++] = myIndex;
		PlayerUpdate.skipData[myIndex] = (byte) 0;
		PlayerUpdate.invisiblePlayersCount = 0;
		for (int pIndex = 1; pIndex < 2048; pIndex++) {
			if (pIndex != myIndex) {
				int hash = in.readBITS(18);
				int z = hash >> 16;
				int x = hash >> 8 & 0xff;
				int y = hash & 0xff;
				InvisiblePlayer inv = PlayerUpdate.invisiblePlayers[pIndex] = new InvisiblePlayer();
				inv.locationHash = (x << 14) + ((z << 28) + y);
				inv.aBoolean1979 = false;
				inv.aBoolean1980 = false;
				inv.interactingWithIndex = -1;
				inv.someFacingDirection = 0;
				PlayerUpdate.invisiblePlayersIndexes[PlayerUpdate.invisiblePlayersCount++] = pIndex;
				PlayerUpdate.skipData[pIndex] = (byte) 0;
			}
		}
		in.endBITStream();
	}
	
	static final int readSkip(ByteStream buff) {
		int type = buff.readBITS(2);
		int value;
		if (type != 0) {
			if (type != 1) {
				if (type == 2) {
					value = buff.readBITS(8);
				} else {
					value = buff.readBITS(11);
				}
			} else {
				value = buff.readBITS(5);
			}
		} else {
			value = 0;
		}
		return value;
	}
	
	static final boolean updateInvisiblePlayer(ByteStream buff, int pIndex) {
		int updateType = buff.readBITS(2);
		if (updateType == 0) { // add
			if (buff.readBITS(1) != 0) {
				updateInvisiblePlayer(buff, pIndex);
			}
			int localX = buff.readBITS(6);
			int localY = buff.readBITS(6);
			boolean requiresUpdate = buff.readBITS(1) == 1;
			if (requiresUpdate) {
				updatablePlayersIndexes[updatablePlayersCount++] = pIndex;
			}
			if (visiblePlayers[pIndex] != null) {
				throw new RuntimeException("hr:lr");
			}
			InvisiblePlayer invisiblePlayerInfo = PlayerUpdate.invisiblePlayers[pIndex];
			Player p = (visiblePlayers[pIndex] = new Player());
			p.index = pIndex;
			if (PlayerUpdate.cachedAppereances[pIndex] != null) {
				p.updateAppereance(PlayerUpdate.cachedAppereances[pIndex]);
			}
			p.method3517(true, true, invisiblePlayerInfo.someFacingDirection);
			p.interactingWithIndex = invisiblePlayerInfo.interactingWithIndex;
			int hash = invisiblePlayerInfo.locationHash;
			int z = hash >> 28;
			int regionX = (hash & 0x3fdcdd) >> 14;
			int regionY = hash & 0xff;
			int fullX = -Class206.worldBaseX + (regionX << 6) + localX;
			int fullY = -Class41_Sub26.worldBaseY + localY + (regionY << 6);
			p.aBoolean6868 = invisiblePlayerInfo.aBoolean1979;
			p.aBoolean6879 = invisiblePlayerInfo.aBoolean1980;
			p.walkingTypes[0] = PlayerUpdate.playerWalkingTypes[pIndex];
			p.z = p.aByte5203 = (byte) z;
			if (r_Sub2.method2871(fullY, fullX, (byte) -47)) {
				p.aByte5203++;
			}
			p.setPosition(fullX, fullY);
			p.needUpdateMoving = false;
			PlayerUpdate.invisiblePlayers[pIndex] = null;
			return true;
		}
		if (updateType == 1) { // z update
			int z = buff.readBITS(2);
			int hash = PlayerUpdate.invisiblePlayers[pIndex].locationHash;
			PlayerUpdate.invisiblePlayers[pIndex].locationHash = ((z + (hash >> 28) & 0x3) << 28) + (hash & 0xfffffff);
			return false;
		}
		if (updateType == 2) { // small move update
			int data = buff.readBITS(5);
			int z = data >> 3;
			int moveType = data & 0x7;
			int hash = PlayerUpdate.invisiblePlayers[pIndex].locationHash;
			int newZ = z + (hash >> 28) & 0x3;
			int newX = (hash & 0x3fc2f1) >> 14;
			int newY = hash & 0xff;
			if (moveType == 0) {
				newX--;
				newY--;
			}
			if (moveType == 1) {
				newY--;
			}
			if (moveType == 2) {
				newY--;
				newX++;
			}
			if (moveType == 3) {
				newX--;
			}
			if (moveType == 4) {
				newX++;
			}
			if (moveType == 5) {
				newX--;
				newY++;
			}
			if (moveType == 6) {
				newY++;
			}
			if (moveType == 7) {
				newY++;
				newX++;
			}
			PlayerUpdate.invisiblePlayers[pIndex].locationHash = (newX << 14) + (newZ << 28) + newY;
			return false;
		}
		// full
		{
			int data = buff.readBITS(18);
			int deltaZ = data >> 16;
			int deltaX = data >> 8 & 0xff;
			int deltaY = data & 0xff;
			int hash = PlayerUpdate.invisiblePlayers[pIndex].locationHash;
			int newZ = deltaZ + (hash >> 28) & 0x3;
			int newX = (hash >> 14) + deltaX & 0xff;
			int newY = deltaY + hash & 0xff;
			PlayerUpdate.invisiblePlayers[pIndex].locationHash = newY + ((newZ << 28) + (newX << 14));
			return false;
		}
	}
	
	static final void clearPlayers() {
		visiblePlayersCount = 0;
		for (int i = 0; i < 2048; i++) {
			cachedAppereances[i] = null;
			PlayerUpdate.playerWalkingTypes[i] = (byte) 1;
			PlayerUpdate.invisiblePlayers[i] = null;
		}
	}
	
	static final void updateVisiblePlayer(ByteStream buff, int pIndex) {
		boolean updateRequired = buff.readBITS(1) == 1;
		if (updateRequired) {
			updatablePlayersIndexes[updatablePlayersCount++] = pIndex;
		}
		int updateType = buff.readBITS(2);
		Player p = visiblePlayers[pIndex];
		if (updateType == 0) {
			if (updateRequired) // flag update only
			{
				p.needUpdateMoving = false;
			} else { // remove
				if (Class362.myPlayerIndex == pIndex) {
					throw new RuntimeException("s:lr");
				}
				InvisiblePlayer inv = invisiblePlayers[pIndex] = new InvisiblePlayer();
				inv.locationHash = (((p.waypointQueueX[0] + Class206.worldBaseX) >> 6 << 14) + (p.z << 28) + ((Class41_Sub26.worldBaseY + (p.wayPointQueueY[0])) >> 6));
				if (p.someFacingDirection != -1) {
					inv.someFacingDirection = p.someFacingDirection;
				} else {
					inv.someFacingDirection = p.aClass5_6804.method175((byte) -81);
				}
				inv.aBoolean1979 = p.aBoolean6868;
				inv.aBoolean1980 = p.aBoolean6879;
				inv.interactingWithIndex = p.interactingWithIndex;
				if (p.anInt6881 > 0) {
					Class366_Sub6.method3789(p, (byte) -59);
				}
				visiblePlayers[pIndex] = null;
				if (buff.readBITS(1) != 0) {
					updateInvisiblePlayer(buff, pIndex);
				}
			}
		} else if (updateType == 1) { // small loc
			int data = buff.readBITS(3);
			int x = p.waypointQueueX[0];
			int y = p.wayPointQueueY[0];
			if (data == 0) {
				y--;
				x--;
			} else if (data != 1) {
				if (data == 2) {
					y--;
					x++;
				} else if (data == 3) {
					x--;
				} else if (data != 4) {
					if (data != 5) {
						if (data == 6) {
							y++;
						} else if (data == 7) {
							x++;
							y++;
						}
					} else {
						y++;
						x--;
					}
				} else {
					x++;
				}
			} else {
				y--;
			}
			if (!updateRequired) {
				p.updatePosition(x, y, (playerWalkingTypes[pIndex]));
			} else {
				p.needUpdateMoving = true;
				p.moveX = x;
				p.moveY = y;
			}
		} else if (updateType == 2) {
			int data = buff.readBITS(4);
			int x = p.waypointQueueX[0];
			int y = p.wayPointQueueY[0];
			if (data == 0) {
				x -= 2;
				y -= 2;
			} else if (data == 1) {
				y -= 2;
				x--;
			} else if (data != 2) {
				if (data == 3) {
					y -= 2;
					x++;
				} else if (data != 4) {
					if (data == 5) {
						y--;
						x -= 2;
					} else if (data == 6) {
						y--;
						x += 2;
					} else if (data == 7) {
						x -= 2;
					} else if (data == 8) {
						x += 2;
					} else if (data == 9) {
						x -= 2;
						y++;
					} else if (data != 10) {
						if (data != 11) {
							if (data == 12) {
								y += 2;
								x--;
							} else if (data != 13) {
								if (data != 14) {
									if (data == 15) {
										y += 2;
										x += 2;
									}
								} else {
									y += 2;
									x++;
								}
							} else {
								y += 2;
							}
						} else {
							x -= 2;
							y += 2;
						}
					} else {
						x += 2;
						y++;
					}
				} else {
					x += 2;
					y -= 2;
				}
			} else {
				y -= 2;
			}
			if (!updateRequired) {
				p.updatePosition(x, y, (playerWalkingTypes[pIndex]));
			} else {
				p.moveY = y;
				p.needUpdateMoving = true;
				p.moveX = x;
			}
		} else { // big move update
			int type = buff.readBITS(1);
			if (type == 0) { // 12bit (small)
				int data = buff.readBITS(12);
				int deltaZ = data >> 10;
				int deltaX = data >> 5 & 0x1f;
				if (deltaX > 15) {
					deltaX -= 32;
				}
				int deltaY = data & 0x1f;
				if (deltaY > 15) {
					deltaY -= 32;
				}
				int fullX = (p.waypointQueueX[0] + deltaX);
				int fullY = (p.wayPointQueueY[0] + deltaY);
				if (updateRequired) {
					p.moveY = fullY;
					p.needUpdateMoving = true;
					p.moveX = fullX;
				} else {
					p.updatePosition(fullX, fullY, playerWalkingTypes[pIndex]);
				}
				p.z = p.aByte5203 = (byte) ((p.z + deltaZ) & 0x3);
				if (r_Sub2.method2871(fullY, fullX, (byte) -47)) {
					p.aByte5203++;
				}
				if (Class362.myPlayerIndex == pIndex) {
					if (p.z != FileWorker.anInt3005) {
						Class41.aBoolean388 = true;
					}
					FileWorker.anInt3005 = p.z;
				}
			} else {
				int data = buff.readBITS(30);
				int deltaZ = data >> 28;
				int deltaX = (data & 0xfffceaa) >> 14;
				int deltaY = data & 0x3fff;
				int fullX = (-Class206.worldBaseX + (deltaX + ((p.waypointQueueX[0]) + Class206.worldBaseX) & 0x3fff));
				int fullY = (-Class41_Sub26.worldBaseY + (deltaY + ((p.wayPointQueueY[0]) + Class41_Sub26.worldBaseY) & 0x3fff));
				if (updateRequired) {
					p.moveY = fullY;
					p.needUpdateMoving = true;
					p.moveX = fullX;
				} else {
					p.updatePosition(fullX, fullY, playerWalkingTypes[pIndex]);
				}
				p.z = p.aByte5203 = (byte) ((p.z + deltaZ) & 0x3);
				if (r_Sub2.method2871(fullY, fullX, (byte) -47)) {
					p.aByte5203++;
				}
				if (pIndex == Class362.myPlayerIndex) {
					FileWorker.anInt3005 = p.z;
				}
			}
		}
	}
	
}
