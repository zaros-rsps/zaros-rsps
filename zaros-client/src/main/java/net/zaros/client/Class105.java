package net.zaros.client;
import jaclib.memory.Buffer;
import jaclib.memory.Source;
import jaclib.memory.heap.NativeHeapBuffer;

import jaggl.MapBuffer;
import jaggl.OpenGL;

abstract class Class105 implements Interface15 {
	private int anInt3654 = 0;
	private int anInt3655;
	private boolean aBoolean3656;
	ha_Sub1_Sub1 aHa_Sub1_Sub1_3657;
	private NativeHeapBuffer aNativeHeapBuffer3658;
	private int anInt3659;
	private int anInt3660;
	static short aShort3661 = 256;
	private int anInt3662 = -1;
	static int anInt3663;
	static int anInt3664 = 0;

	final boolean method905(int i, Source source, int i_0_) {
		if (i_0_ != -1)
			method909(-87, null);
		if (i <= anInt3655) {
			if (anInt3662 <= 0)
				throw new RuntimeException("ARGH!");
			OpenGL.glBindBufferARB(anInt3659, anInt3662);
			OpenGL.glBufferSubDataARBa(anInt3659, 0, anInt3660, source.getAddress());
			aHa_Sub1_Sub1_3657.anInt3938 += -anInt3660 + i;
		} else {
			method910((byte) 93);
			if (anInt3662 > 0) {
				OpenGL.glBindBufferARB(anInt3659, anInt3662);
				OpenGL.glBufferDataARBa(anInt3659, i, source.getAddress(), !aBoolean3656 ? 35044 : 35040);
				aHa_Sub1_Sub1_3657.anInt3938 += i - anInt3660;
			} else
				throw new RuntimeException("ARGH!");
			anInt3655 = i;
		}
		anInt3660 = i;
		return true;
	}

	final void method906(int i) {
		if (aHa_Sub1_Sub1_3657.aBoolean5853)
			OpenGL.glBindBufferARB(anInt3659, anInt3662);
		if (i != 0)
			method908(-67);
	}

	void method31(byte i) {
		if (anInt3662 > 0) {
			aHa_Sub1_Sub1_3657.method1240(i ^ 0x3d, anInt3660, anInt3662);
			anInt3662 = -1;
		}
		if (i != 82)
			anInt3662 = -103;
	}

	final Buffer method907(MapBuffer mapbuffer, byte i, boolean bool) {
		if (i != -16)
			return null;
		if (anInt3654 == 0) {
			method910((byte) -102);
			if (anInt3662 <= 0) {
				anInt3654 = 2;
				return aNativeHeapBuffer3658;
			}
			OpenGL.glBindBufferARB(anInt3659, anInt3662);
			if (bool) {
				OpenGL.glBufferDataARBub(anInt3659, anInt3655, null, 0, aBoolean3656 ? 35040 : 35044);
				if (aHa_Sub1_Sub1_3657.aNativeHeapBuffer3931.b >= anInt3660) {
					anInt3654 = 1;
					return aHa_Sub1_Sub1_3657.aNativeHeapBuffer3931;
				}
			}
			if (!mapbuffer.b() && mapbuffer.a(anInt3659, anInt3660, 35001)) {
				anInt3654 = 2;
				return mapbuffer;
			}
		}
		return null;
	}

	protected final void finalize() throws Throwable {
		method31((byte) 82);
		super.finalize();
	}

	public int method61(byte i) {
		if (i <= 115)
			method32(25, -83);
		return anInt3660;
	}

	final long method908(int i) {
		if (i > -119)
			return -65L;
		if (anInt3662 != 0)
			return 0L;
		return aNativeHeapBuffer3658.getAddress();
	}

	final boolean method909(int i, MapBuffer mapbuffer) {
		if (i != -10218)
			method61((byte) -22);
		boolean bool = true;
		if (anInt3654 != 0) {
			if (anInt3662 > 0) {
				OpenGL.glBindBufferARB(anInt3659, anInt3662);
				if (anInt3654 != 1)
					bool = mapbuffer.a();
				else
					OpenGL.glBufferSubDataARBa(anInt3659, 0, anInt3655, aHa_Sub1_Sub1_3657.aNativeHeapBuffer3931.getAddress());
			}
			anInt3654 = 0;
		}
		return bool;
	}

	void method32(int i, int i_1_) {
		if (i > anInt3655) {
			method910((byte) -120);
			if (anInt3662 > 0) {
				OpenGL.glBindBufferARB(anInt3659, anInt3662);
				OpenGL.glBufferDataARBub(anInt3659, i, null, 0, !aBoolean3656 ? 35044 : 35040);
				aHa_Sub1_Sub1_3657.anInt3938 += i - anInt3655;
			} else
				aNativeHeapBuffer3658 = aHa_Sub1_Sub1_3657.method1109(false, (byte) -76, i);
			anInt3655 = i;
		}
		if (i_1_ != -21709)
			method909(53, null);
		anInt3660 = i;
	}

	private final void method910(byte i) {
		if (anInt3662 < 0) {
			if (aHa_Sub1_Sub1_3657.aBoolean5853) {
				OpenGL.glGenBuffersARB(1, Class309.anIntArray2757, 0);
				anInt3662 = Class309.anIntArray2757[0];
				OpenGL.glBindBufferARB(anInt3659, anInt3662);
			} else
				anInt3662 = 0;
			int i_2_ = 6 / ((i - 22) / 56);
		}
	}

	Class105(ha_Sub1_Sub1 var_ha_Sub1_Sub1, int i, boolean bool) {
		anInt3659 = i;
		aBoolean3656 = bool;
		aHa_Sub1_Sub1_3657 = var_ha_Sub1_Sub1;
	}
}
