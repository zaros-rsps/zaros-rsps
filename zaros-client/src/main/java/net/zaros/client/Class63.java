package net.zaros.client;
import jaclib.memory.Buffer;
import jaclib.memory.Stream;

final class Class63 {
	private int anInt714 = -1;
	private ha_Sub1 aHa_Sub1_715;
	private int anInt716;
	boolean aBoolean717 = true;
	private int anInt718;
	int anInt719;
	private Interface6_Impl1 anInterface6_Impl1_720;
	private int anInt721;
	static IncomingPacket VORBIS_SPEECH_SOUND = new IncomingPacket(61, 6);
	private Class386 aClass386_723;
	private int anInt724;
	private Interface15_Impl1 anInterface15_Impl1_725;
	static Js5 aClass138_726;
	static int anInt727;

	private final void method701(int i) {
		if (aBoolean717) {
			int i_0_ = 95 % ((42 - i) / 45);
			aBoolean717 = false;
			byte[] is = aClass386_723.aByteArray3265;
			int i_1_ = 0;
			int i_2_ = aClass386_723.anInt3267;
			int i_3_ = anInt716 * aClass386_723.anInt3267 + anInt721;
			for (int i_4_ = -128; i_4_ < 0; i_4_++) {
				i_1_ = -i_1_ + (i_1_ << 8);
				for (int i_5_ = -128; i_5_ < 0; i_5_++) {
					if (is[i_3_++] != 0)
						i_1_++;
				}
				i_3_ += i_2_ - 128;
			}
			if (anInterface6_Impl1_720 != null && i_1_ == anInt714)
				aBoolean717 = false;
			else {
				anInt714 = i_1_;
				int i_6_ = 0;
				i_3_ = anInt721 + i_2_ * anInt716;
				if (!aHa_Sub1_715.method1200(Class67.aClass67_745, Class13.aClass202_3516, (byte) 119)) {
					if (Class127_Sub1.anIntArray4285 == null)
						Class127_Sub1.anIntArray4285 = new int[16384];
					int[] is_7_ = Class127_Sub1.anIntArray4285;
					for (int i_8_ = -128; i_8_ < 0; i_8_++) {
						for (int i_9_ = -128; i_9_ < 0; i_9_++) {
							if (is[i_3_] != 0)
								is_7_[i_6_++] = 1140850688;
							else {
								int i_10_ = 0;
								if (is[i_3_ - 1] != 0)
									i_10_++;
								if (is[i_3_ + 1] != 0)
									i_10_++;
								if (is[-i_2_ + i_3_] != 0)
									i_10_++;
								if (is[i_2_ + i_3_] != 0)
									i_10_++;
								is_7_[i_6_++] = i_10_ * 17 << 24;
							}
							i_3_++;
						}
						i_3_ += aClass386_723.anInt3267 - 128;
					}
					if (anInterface6_Impl1_720 == null) {
						anInterface6_Impl1_720 = aHa_Sub1_715.method1179(128, 128, false, (Class127_Sub1.anIntArray4285), -461);
						anInterface6_Impl1_720.method64((byte) 49, false, false);
					} else
						anInterface6_Impl1_720.method68(0, 128, 0, (Class127_Sub1.anIntArray4285), -25989, 128, 128, 0);
				} else {
					if (Class352.aByteArray3046 == null)
						Class352.aByteArray3046 = new byte[16384];
					byte[] is_11_ = Class352.aByteArray3046;
					for (int i_12_ = -128; i_12_ < 0; i_12_++) {
						for (int i_13_ = -128; i_13_ < 0; i_13_++) {
							if (is[i_3_] != 0)
								is_11_[i_6_++] = (byte) 68;
							else {
								int i_14_ = 0;
								if (is[i_3_ - 1] != 0)
									i_14_++;
								if (is[i_3_ + 1] != 0)
									i_14_++;
								if (is[i_3_ - i_2_] != 0)
									i_14_++;
								if (is[i_3_ + i_2_] != 0)
									i_14_++;
								is_11_[i_6_++] = (byte) (i_14_ * 17);
							}
							i_3_++;
						}
						i_3_ += aClass386_723.anInt3267 - 128;
					}
					if (anInterface6_Impl1_720 == null) {
						anInterface6_Impl1_720 = aHa_Sub1_715.method1180(false, 7, Class13.aClass202_3516, Class352.aByteArray3046, 128, 128);
						anInterface6_Impl1_720.method64((byte) 49, false, false);
					} else
						anInterface6_Impl1_720.method67(true, 128, 128, 0, Class352.aByteArray3046, 0, 0, 128, Class13.aClass202_3516);
				}
			}
		}
	}

	static final void method702(int i, int i_15_, Class296_Sub39_Sub1 class296_sub39_sub1, int i_16_) {
		if (Class318.aBoolean2814) {
			int i_17_ = 0;
			for (Class296_Sub39_Sub9 class296_sub39_sub9 = ((Class296_Sub39_Sub9) class296_sub39_sub1.aClass145_6119.getFront()); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) class296_sub39_sub1.aClass145_6119.getNext()) {
				int i_18_ = BITConfigsLoader.method2100(class296_sub39_sub9, 0);
				if (i_18_ > i_17_)
					i_17_ = i_18_;
			}
			i_17_ += 8;
			Class95.anInt1036 = class296_sub39_sub1.anInt6117 * 16 + (SeekableFile.aBoolean2397 ? 26 : 22);
			int i_19_ = class296_sub39_sub1.anInt6117 * i + 21;
			int i_20_ = Class252.anInt2382 + Class81.anInt3666;
			if (Class241.anInt2301 < i_20_ + i_17_)
				i_20_ = Class252.anInt2382 - i_17_;
			if (i_20_ < 0)
				i_20_ = 0;
			int i_21_ = !SeekableFile.aBoolean2397 ? 31 : 33;
			int i_22_ = -i_21_ + i_15_ + 13;
			if (i_19_ + i_22_ > Class384.anInt3254)
				i_22_ = -i_19_ + Class384.anInt3254;
			if (i_22_ < 0)
				i_22_ = 0;
			Class41_Sub5.anInt3753 = i_20_;
			Class270.anInt2510 = i_22_;
			Class241.aClass296_Sub39_Sub1_2302 = class296_sub39_sub1;
			StaticMethods.anInt3152 = i_17_;
		}
	}

	final void method703(boolean bool) {
		if (bool)
			method702(-65, 91, null, 71);
		method704(anInterface15_Impl1_725, anInt719, (byte) -118);
	}

	final void method704(Interface15_Impl1 interface15_impl1, int i, byte i_23_) {
		if (i > 0) {
			method701(125);
			aHa_Sub1_715.method1140(anInterface6_Impl1_720, false);
			aHa_Sub1_715.method1232(Class166_Sub1.aClass289_4298, interface15_impl1, anInt718 - anInt724 + 1, i, 0, 29, anInt724);
		}
		int i_24_ = 41 / ((i_23_ + 76) / 41);
	}

	Class63(ha_Sub1 var_ha_Sub1, Class386 class386, s_Sub3 var_s_Sub3, int i, int i_25_, int i_26_, int i_27_, int i_28_) {
		anInt716 = i_28_;
		aHa_Sub1_715 = var_ha_Sub1;
		anInt721 = i_27_;
		aClass386_723 = class386;
		int i_29_ = 1 << i_26_;
		int i_30_ = 0;
		int i_31_ = i << i_26_;
		int i_32_ = i_25_ << i_26_;
		for (int i_33_ = 0; i_33_ < i_29_; i_33_++) {
			int i_34_ = i_31_ + var_s_Sub3.anInt2832 * (i_32_ + i_33_);
			for (int i_35_ = 0; i_29_ > i_35_; i_35_++) {
				short[] is = var_s_Sub3.aShortArrayArray5152[i_34_++];
				if (is != null)
					i_30_ += is.length;
			}
		}
		if (i_30_ > 0) {
			anInt718 = -2147483648;
			anInt724 = 2147483647;
			anInterface15_Impl1_725 = aHa_Sub1_715.method1154(false, 116);
			anInterface15_Impl1_725.method32(i_30_, -21709);
			for (int i_36_ = 0; i_36_ < 4; i_36_++) {
				Buffer buffer = anInterface15_Impl1_725.method30((byte) -38, true);
				if (buffer != null) {
					Stream stream = aHa_Sub1_715.method1156(buffer, -99);
					if (!Stream.a()) {
						for (int i_37_ = 0; i_29_ > i_37_; i_37_++) {
							int i_38_ = ((i_32_ + i_37_) * var_s_Sub3.anInt2832 + i_31_);
							for (int i_39_ = 0; i_39_ < i_29_; i_39_++) {
								short[] is = var_s_Sub3.aShortArrayArray5152[i_38_++];
								if (is != null) {
									for (int i_40_ = 0; i_40_ < is.length; i_40_++) {
										int i_41_ = is[i_40_] & 0xffff;
										if (anInt724 > i_41_)
											anInt724 = i_41_;
										if (i_41_ > anInt718)
											anInt718 = i_41_;
										stream.a(i_41_);
									}
								}
							}
						}
					} else {
						for (int i_42_ = 0; i_29_ > i_42_; i_42_++) {
							int i_43_ = ((i_32_ + i_42_) * var_s_Sub3.anInt2832 + i_31_);
							for (int i_44_ = 0; i_44_ < i_29_; i_44_++) {
								short[] is = var_s_Sub3.aShortArrayArray5152[i_43_++];
								if (is != null) {
									for (int i_45_ = 0; i_45_ < is.length; i_45_++) {
										int i_46_ = is[i_45_] & 0xffff;
										if (anInt724 > i_46_)
											anInt724 = i_46_;
										if (anInt718 < i_46_)
											anInt718 = i_46_;
										stream.d(i_46_);
									}
								}
							}
						}
					}
					stream.b();
					if (anInterface15_Impl1_725.method33(32185))
						break;
				}
			}
			anInt719 = i_30_ / 3;
		} else {
			anInterface6_Impl1_720 = null;
			anInt719 = 0;
		}
	}

	public static void method705(int i) {
		VORBIS_SPEECH_SOUND = null;
		int i_47_ = -41 / ((-57 - i) / 36);
		aClass138_726 = null;
	}
}
