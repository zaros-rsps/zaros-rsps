package net.zaros.client;

/* Class241_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class241_Sub2_Sub1 extends Class241_Sub2 {
	private Class69_Sub3 aClass69_Sub3_5893;
	private int anInt5894;
	private int anInt5895;
	private int anInt5896;
	static IncomingPacket aClass231_5897 = new IncomingPacket(134, -2);
	private int anInt5898;
	private int anInt5899;
	private int anInt5900;
	static Interface13 anInterface13_5901;
	private ha_Sub3 aHa_Sub3_5902;
	static IncomingPacket aClass231_5903 = new IncomingPacket(114, -1);

	public static void method2160(byte i) {
		aClass231_5903 = null;
		int i_0_ = -31 / (i / 48);
		anInterface13_5901 = null;
		aClass231_5897 = null;
	}

	final Class69_Sub3 method2157(boolean bool) {
		if (aClass69_Sub3_5893 == null) {
			d var_d = aHa_Sub3_5902.aD1299;
			Class368_Sub10.anIntArray5478[4] = anInt5898;
			Class368_Sub10.anIntArray5478[0] = anInt5900;
			Class368_Sub10.anIntArray5478[3] = anInt5896;
			Class368_Sub10.anIntArray5478[1] = anInt5895;
			Class368_Sub10.anIntArray5478[2] = anInt5899;
			Class368_Sub10.anIntArray5478[5] = anInt5894;
			boolean bool_1_ = false;
			int i = 0;
			for (int i_2_ = 0; i_2_ < 6; i_2_++) {
				if (!var_d.is_ready(Class368_Sub10.anIntArray5478[i_2_]))
					return null;
				MaterialRaw class170 = var_d.method14(Class368_Sub10.anIntArray5478[i_2_], -9412);
				int i_3_ = class170.small_sized ? 64 : 128;
				if (i < i_3_)
					i = i_3_;
				if (class170.aByte1795 > 0)
					bool_1_ = true;
			}
			for (int i_4_ = 0; i_4_ < 6; i_4_++)
				Class123_Sub2_Sub2.anIntArrayArray5820[i_4_] = var_d.get_transparent_pixels(i, i, false, (byte) 118, 1.0F, Class368_Sub10.anIntArray5478[i_4_]);
			aClass69_Sub3_5893 = new Class69_Sub3(aHa_Sub3_5902, 6407, i, bool_1_, Class123_Sub2_Sub2.anIntArrayArray5820);
		}
		if (bool)
			method2157(false);
		return aClass69_Sub3_5893;
	}

	Class241_Sub2_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_) {
		anInt5896 = i_7_;
		anInt5899 = i_6_;
		aHa_Sub3_5902 = var_ha_Sub3;
		anInt5894 = i_9_;
		anInt5895 = i_5_;
		anInt5898 = i_8_;
		anInt5900 = i;
	}

	static final byte[] method2161(float f, int i, float f_10_, int i_11_, Class226 class226, int i_12_, float f_13_, float f_14_, float f_15_, byte i_16_, int i_17_) {
		int i_18_ = -34 / ((i_16_ + 71) / 51);
		byte[] is = new byte[i_11_ * i_12_ * i];
		Class363.method3759((byte) -116, f_15_, f_13_, f, is, i_17_, class226, 0, i, f_10_, i_11_, i_12_, f_14_);
		return is;
	}
}
