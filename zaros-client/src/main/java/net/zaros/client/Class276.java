package net.zaros.client;

/* Class276 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class276 {
	private Js5 aClass138_2526;
	static Object[] anObjectArray2527;
	int anInt2528;
	static int[] anIntArray2529 = new int[3];

	static final void method2319(int i) {
		int[] is = PlayerUpdate.visiblePlayersIndexes;
		for (int i_0_ = i; PlayerUpdate.visiblePlayersCount > i_0_; i_0_++) {
			Player class338_sub3_sub1_sub3_sub1 = (PlayerUpdate.visiblePlayers[is[i_0_]]);
			if (class338_sub3_sub1_sub3_sub1 != null)
				class338_sub3_sub1_sub3_sub1.method3501(16383);
		}
		for (int i_1_ = 0; i_1_ < Class367.npcsCount; i_1_++) {
			long l = (long) ReferenceTable.npcIndexes[i_1_];
			NPCNode class296_sub7 = ((NPCNode) Class41_Sub18.localNpcs.get(l));
			if (class296_sub7 != null)
				class296_sub7.value.method3501(i + 16383);
		}
		if (za_Sub1.anInt6554 == 3) {
			for (int i_2_ = 0; i_2_ < Class379.aClass302Array3624.length; i_2_++) {
				Class302 class302 = Class379.aClass302Array3624[i_2_];
				if (class302.aBoolean2716)
					class302.method3262(0).method3501(16383);
			}
		}
	}

	public static void method2320(byte i) {
		if (i != 33)
			anObjectArray2527 = null;
		anIntArray2529 = null;
		anObjectArray2527 = null;
	}

	static final void method2321(int i) {
		Class343_Sub1.aClass296_Sub50_5282.method3060(i, i + 68, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5001));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 74, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 121, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5018));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 40, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5029));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, i + 65, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, i + 60, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub29_5002));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 56, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub11_5023));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 46, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub9_4999));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, i + 60, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub16_5019));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, i + 110, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 112, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 74, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub7_5004));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, i ^ 0x74, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 63, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub1_5008));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 112, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5020));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 116, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5015));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 94, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub8_5027));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 122, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 111, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub27_5010));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 115, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011));
		Class296_Sub45_Sub2.method2980(true);
		Class343_Sub1.aClass296_Sub50_5282.method3060(2, 106, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub10_5009));
		Class343_Sub1.aClass296_Sub50_5282.method3060(2, 104, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub15_4990));
		Class368_Sub4.method3817(-126);
		Class368_Sub22.method3866(i ^ 0xa);
		Class380.aBoolean3210 = true;
	}

	static final Class234 method2322(byte i, Class338_Sub3_Sub1 class338_sub3_sub1) {
		if (i != -106)
			return null;
		Class234 class234;
		if (Class30.aClass234_320 != null) {
			class234 = Class30.aClass234_320;
			Class30.aClass234_320 = Class30.aClass234_320.aClass234_2224;
			class234.aClass234_2224 = null;
			StaticMethods.anInt3161--;
		} else
			class234 = new Class234();
		class234.aClass338_Sub3_Sub1_2226 = class338_sub3_sub1;
		return class234;
	}

	Class276(GameType class35, int i, Js5 class138) {
		new AdvancedMemoryCache(64);
		aClass138_2526 = class138;
		anInt2528 = aClass138_2526.getLastFileId(15);
	}
}
