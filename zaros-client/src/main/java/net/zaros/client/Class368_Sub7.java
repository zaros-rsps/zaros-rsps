package net.zaros.client;

/* Class368_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub7 extends Class368 {
	static Class161 aClass161_5460;
	static Class262 aClass262_5461;
	private int anInt5462;
	static int anInt5463 = 765;
	private int anInt5464;

	final void method3807(byte i) {
		int i_0_ = 109 / ((i - 52) / 52);
		Class45 class45 = Class41_Sub14.aClass45Array3777[anInt5462];
		Class314.method3320(class45.anInt441, class45.anInt436, -126, class45.anInt437, (StaticMethods.anIntArray6082[class45.anInt441]), class45.anInt442, class45.anInt443, anInt5464);
	}

	static final void method3833(int i, int i_1_) {
		if (ReferenceWrapper.anInt6128 != i_1_)
			Class392.anInt3488 = i;
		else
			Class235.aClass296_Sub45_Sub4_2229.method3019(i, 56);
	}

	Class368_Sub7(Packet class296_sub17) {
		super(class296_sub17);
		anInt5462 = class296_sub17.g2();
		anInt5464 = class296_sub17.gSmart2or4s();
	}

	static final boolean method3834(ha var_ha, int i, int i_2_, Class18 class18, Class296_Sub53 class296_sub53, int i_3_) {
		int i_4_ = 2147483647;
		int i_5_ = -2147483648;
		int i_6_ = 2147483647;
		int i_7_ = -2147483648;
		if (i_3_ != -632057695)
			method3834(null, 5, -119, null, null, 112);
		if (class18.anIntArray211 != null) {
			i_4_ = (((-Class106.anInt1128 + (class296_sub53.anInt5047 + class18.anInt219)) * (-Class106.anInt1130 + Class106.anInt1123) / (-Class106.anInt1128 + Class106.anInt1112)) + Class106.anInt1130);
			i_5_ = (Class106.anInt1130 + ((-Class106.anInt1128 + class18.anInt210 + class296_sub53.anInt5047) * (Class106.anInt1123 - Class106.anInt1130) / (-Class106.anInt1128 + Class106.anInt1112)));
			i_7_ = (-((Class106.anInt1129 - Class106.anInt1126) * (-Class106.anInt1120 + (class18.anInt223 + class296_sub53.anInt5042)) / (-Class106.anInt1120 + Class106.anInt1121)) + Class106.anInt1129);
			i_6_ = (Class106.anInt1129 - ((-Class106.anInt1126 + Class106.anInt1129) * (class18.anInt222 + class296_sub53.anInt5042 - Class106.anInt1120) / (-Class106.anInt1120 + Class106.anInt1121)));
		}
		Sprite class397 = null;
		int i_8_ = 0;
		int i_9_ = 0;
		int i_10_ = 0;
		int i_11_ = 0;
		if (class18.anInt203 != -1) {
			if (class296_sub53.aBoolean5044 && class18.anInt197 != -1)
				class397 = class18.method275(true, (byte) 111, var_ha);
			else
				class397 = class18.method275(false, (byte) 111, var_ha);
			if (class397 != null) {
				i_8_ = class296_sub53.anInt5041 - (class397.method4099() + 1 >> 1);
				i_9_ = class296_sub53.anInt5041 + (class397.method4099() + 1 >> 1);
				if (i_4_ > i_8_)
					i_4_ = i_8_;
				if (i_9_ > i_5_)
					i_5_ = i_9_;
				i_10_ = class296_sub53.anInt5043 - (class397.method4088() + 1 >> 1);
				i_11_ = class296_sub53.anInt5043 + (class397.method4088() + 1 >> 1);
				if (i_10_ < i_6_)
					i_6_ = i_10_;
				if (i_7_ < i_11_)
					i_7_ = i_11_;
			}
		}
		Class256 class256 = null;
		int i_12_ = 0;
		int i_13_ = 0;
		int i_14_ = 0;
		int i_15_ = 0;
		int i_16_ = 0;
		int i_17_ = 0;
		int i_18_ = 0;
		int i_19_ = 0;
		if (class18.aString191 != null) {
			class256 = Class122_Sub3.method1050(class18.anInt214, (byte) -91);
			if (class256 != null) {
				i_12_ = Class154_Sub1.aClass92_4292.method846(null, (Class57.aStringArray666), (byte) -75, class18.aString191, null);
				i_13_ = (class296_sub53.anInt5041 + ((Class106.anInt1123 - Class106.anInt1130) * class18.anInt199 / (Class106.anInt1112 - Class106.anInt1128)));
				i_14_ = (-(class18.anInt218 * (Class106.anInt1129 - Class106.anInt1126) / (Class106.anInt1121 - Class106.anInt1120)) + class296_sub53.anInt5043);
				if (class397 != null)
					i_14_ -= ((class397.method4088() >> 1) + class256.method2230() * i_12_);
				else
					i_14_ -= class256.method2232() * i_12_ / 2;
				for (int i_20_ = 0; i_20_ < i_12_; i_20_++) {
					String string = Class57.aStringArray666[i_20_];
					if (i_12_ - 1 > i_20_)
						string = string.substring(0, string.length() - 4);
					int i_21_ = class256.method2234(string);
					if (i_21_ > i_15_)
						i_15_ = i_21_;
				}
				i_16_ = i_13_ + (-(i_15_ / 2) + i);
				if (i_16_ < i_4_)
					i_4_ = i_16_;
				i_17_ = i + (i_15_ / 2 + i_13_);
				i_18_ = i_2_ + i_14_;
				if (i_17_ > i_5_)
					i_5_ = i_17_;
				if (i_6_ > i_18_)
					i_6_ = i_18_;
				i_19_ = i_14_ + i_12_ * class256.method2230() + i_2_;
				if (i_19_ > i_7_)
					i_7_ = i_19_;
			}
		}
		if (Class106.anInt1130 > i_5_ || i_4_ > Class106.anInt1123 || i_7_ < Class106.anInt1126 || i_6_ > Class106.anInt1129)
			return true;
		Class106.method926(var_ha, class296_sub53, class18);
		if (class397 != null) {
			if (aa_Sub2.anInt3728 > 0 && ((Class41_Sub26.anInt3807 != -1 && Class41_Sub26.anInt3807 == class296_sub53.anInt5046) || (Class325.anInt2868 != -1 && Class325.anInt2868 == class18.anInt217))) {
				int i_22_;
				if (Class139.anInt1421 > 50)
					i_22_ = (100 - Class139.anInt1421) * 2;
				else
					i_22_ = Class139.anInt1421 * 2;
				int i_23_ = i_22_ << 24 | 0xffff00;
				var_ha.method1097(class296_sub53.anInt5041, class296_sub53.anInt5043, class397.method4087() / 2 + 7, i_23_, i_3_ + 632082698);
				var_ha.method1097(class296_sub53.anInt5041, class296_sub53.anInt5043, class397.method4087() / 2 + 5, i_23_, i_3_ + 632082698);
				var_ha.method1097(class296_sub53.anInt5041, class296_sub53.anInt5043, class397.method4087() / 2 + 3, i_23_, 25003);
				var_ha.method1097(class296_sub53.anInt5041, class296_sub53.anInt5043, class397.method4087() / 2 + 1, i_23_, 25003);
				var_ha.method1097(class296_sub53.anInt5041, class296_sub53.anInt5043, class397.method4087() / 2, i_23_, i_3_ ^ ~0x25ac0ef5);
			}
			class397.method4096((class296_sub53.anInt5041 - (class397.method4099() >> 1)), (class296_sub53.anInt5043 - (class397.method4088() >> 1)));
		}
		if (class18.aString191 != null && class256 != null)
			Class41_Sub22.method482(i_12_, i_13_, i_15_, class18, var_ha, class296_sub53, class256, i_14_, 89);
		if (class18.anInt203 != -1 || class18.aString191 != null) {
			Class296_Sub38 class296_sub38 = new Class296_Sub38(class296_sub53);
			class296_sub38.anInt4896 = i_10_;
			class296_sub38.anInt4895 = i_18_;
			class296_sub38.anInt4887 = i_17_;
			class296_sub38.anInt4890 = i_9_;
			class296_sub38.anInt4888 = i_16_;
			class296_sub38.anInt4897 = i_8_;
			class296_sub38.anInt4893 = i_19_;
			class296_sub38.anInt4894 = i_11_;
			Class366_Sub8.aClass155_5409.addLast((byte) -51, class296_sub38);
		}
		return false;
	}

	public static void method3835(int i) {
		aClass161_5460 = null;
		aClass262_5461 = null;
		if (i != 2)
			method3833(13, 50);
	}

	static {
		aClass161_5460 = new Class161(14, 0, 4, 1);
	}
}
