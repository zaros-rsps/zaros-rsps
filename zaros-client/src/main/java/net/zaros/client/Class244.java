package net.zaros.client;

/* Class244 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class244 {
	private int anInt2318 = 0;
	private HashTable aClass263_2319;
	static s[] aSArray2320;
	static int anInt2321;
	private Node aClass296_2322;

	public static void method2174(int i) {
		aSArray2320 = null;
		if (i != 0)
			method2176(null, -91);
	}

	final Node method2175(int i) {
		int i_0_ = -22 % ((i - 74) / 40);
		if (anInt2318 > 0 && (aClass296_2322 != aClass263_2319.buckets[anInt2318 - 1])) {
			Node class296 = aClass296_2322;
			aClass296_2322 = class296.next;
			return class296;
		}
		while (aClass263_2319.bucketCount > anInt2318) {
			Node class296 = (aClass263_2319.buckets[anInt2318++].next);
			if (aClass263_2319.buckets[anInt2318 - 1] != class296) {
				aClass296_2322 = class296.next;
				return class296;
			}
		}
		return null;
	}

	static final Class379_Sub2 method2176(Packet class296_sub17, int i) {
		if (i < 0)
			aSArray2320 = null;
		Class379 class379 = Class296_Sub39_Sub1.method2784(class296_sub17, 0);
		int i_1_ = class296_sub17.g2();
		int i_2_ = class296_sub17.g2();
		int i_3_ = class296_sub17.g2();
		int i_4_ = class296_sub17.g2();
		int i_5_ = class296_sub17.g2();
		int i_6_ = class296_sub17.g2();
		return new Class379_Sub2(class379.aClass252_3616, class379.aClass357_3621, class379.anInt3623, class379.anInt3615, class379.anInt3613, class379.anInt3626, class379.anInt3619, class379.anInt3620, class379.anInt3617, i_1_, i_2_, i_3_, i_4_, i_5_, i_6_);
	}

	final Node method2177(int i) {
		anInt2318 = 0;
		if (i > -11)
			return null;
		return method2175(26);
	}

	public Class244() {
		/* empty */
	}

	Class244(HashTable class263) {
		aClass263_2319 = class263;
	}

	static final boolean method2178(int i, int i_7_, int i_8_) {
		if (i_7_ != 0)
			return true;
		if ((i_8_ & 0x10) == 0)
			return false;
		return true;
	}
}
