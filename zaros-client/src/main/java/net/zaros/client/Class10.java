package net.zaros.client;

/* Class10 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class10 implements Interface9 {
	static IncomingPacket aClass231_3530 = new IncomingPacket(74, 6);
	static long aLong3531;
	static int colorID = 0;
	static World aClass112_3533;
	private Class266 aClass266_3534;
	private Js5 aClass138_3535;
	private Class55 aClass55_3536;
	static int anInt3537;
	private Js5 aClass138_3538;

	public final void method43(boolean bool, int i) {
		if (bool) {
			int i_0_ = (aClass266_3534.aClass252_3612.method2204((aClass266_3534.anInt3609), 23236, (Class368_Sub7.anInt5463)) + aClass266_3534.anInt3604);
			int i_1_ = ((aClass266_3534.aClass357_3607.method3711(Class296_Sub15_Sub1.anInt5996, aClass266_3534.anInt3601, (byte) 123)) + aClass266_3534.anInt3606);
			aClass55_3536.a((byte) 98, null, aClass266_3534.anInt3610, null, aClass266_3534.anInt3611, aClass266_3534.anInt3609, i_1_, i_0_, aClass266_3534.aString3602, aClass266_3534.anInt3608, aClass266_3534.anInt3605, aClass266_3534.anInt3601, 0, 0, null, aClass266_3534.anInt3603);
		}
		int i_2_ = -112 % ((i + 32) / 52);
	}

	static final void method195(byte[] buff, boolean bool) {
		if (Class76.aClass296_Sub17_875 == null)
			Class76.aClass296_Sub17_875 = new Packet(20000);
		Class76.aClass296_Sub17_875.writeBytes(buff, 0, buff.length);
		if (bool) {
			Class338_Sub9.method3615(-104, Class76.aClass296_Sub17_875.data);
			AnimationsLoader.aClass210_Sub1Array2760 = new Class210_Sub1[Class296_Sub15_Sub1.anInt5998];
			int i = 0;
			for (int i_4_ = Class368_Sub15.anInt5520; i_4_ <= Class338_Sub3_Sub3_Sub1.anInt6617; i_4_++) {
				Class210_Sub1 class210_sub1 = GraphicsLoader.method2284(i_4_, -110);
				if (class210_sub1 != null)
					AnimationsLoader.aClass210_Sub1Array2760[i++] = class210_sub1;
			}
			Class296_Sub39_Sub17.aBoolean6238 = false;
			Class99.aLong1065 = Class72.method771(-118);
			Class76.aClass296_Sub17_875 = null;
		}
	}

	static final boolean method196(Js5 class138, Class296_Sub45_Sub4 class296_sub45_sub4, int i, Js5 class138_5_, Class381 class381, Js5 class138_6_) {
		Class375.aClass138_3182 = class138;
		Class88.aClass138_944 = class138_5_;
		Class41_Sub13.anIntArray3772 = new int[16];
		Class235.aClass296_Sub45_Sub4_2229 = class296_sub45_sub4;
		if (i >= -96)
			anInt3537 = 74;
		Class286.aClass138_2639 = class138_6_;
		Class296_Sub39_Sub9.aClass381_6163 = class381;
		for (int i_7_ = 0; i_7_ < 16; i_7_++)
			Class41_Sub13.anIntArray3772[i_7_] = 255;
		return true;
	}

	public final boolean method44(byte i) {
		boolean bool = true;
		if (i <= 91)
			return false;
		if (!aClass138_3538.hasEntryBuffer(aClass266_3534.anInt3600))
			bool = false;
		if (!aClass138_3535.hasEntryBuffer(aClass266_3534.anInt3600))
			bool = false;
		return bool;
	}

	public static void method197(int i) {
		aClass112_3533 = null;
		aClass231_3530 = null;
		if (i != -32746)
			anInt3537 = 64;
	}

	public final void method42(byte i) {
		Class92 class92 = Class259.method2240(aClass138_3535, (byte) 10, aClass266_3534.anInt3600);
		if (i != 88)
			method43(false, 31);
		aClass55_3536 = Class41_Sub13.aHa3774.a(class92, Class186.method1868(aClass138_3538, (aClass266_3534.anInt3600)), true);
	}

	Class10(Js5 class138, Js5 class138_8_, Class266 class266) {
		aClass266_3534 = class266;
		aClass138_3538 = class138;
		aClass138_3535 = class138_8_;
	}

	static {
		aLong3531 = 1L;
	}
}
