package net.zaros.client;
/* Class44 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Frame;

public class Animator {
	int anInt423;
	private Class12 aClass12_424;
	private int anInt425;
	private boolean aBoolean426;
	static int anInt427 = 0;
	private int anInt428;
	private boolean aBoolean429;
	Animation aClass43_430;
	private int anInt431 = 0;
	int anInt432;
	private int anInt433;
	Class12 aClass12_434;
	static Frame aFrame435;

	final boolean method545(int i) {
		if (i != 0) {
			method571(104, -48, -70);
		}
		if (aClass43_430 != null) {
			boolean bool = aClass12_434.method220(false, aClass43_430.anIntArray402, anInt433, Class377.aClass310_3188, anInt432, aClass43_430);
			if (bool && aBoolean429 && aClass43_430.anIntArray419 != null) {
				aClass12_424.method220(false, aClass43_430.anIntArray419, anInt433, Class377.aClass310_3188, anInt432, aClass43_430);
			}
			return bool;
		}
		return false;
	}

	final boolean method546(int i) {
		int i_0_ = -23 / ((-56 - i) / 49);
		return aBoolean426;
	}

	final void method547(int i) {
		if (i != 14899) {
			method555(-36, 3);
		}
		method564(0, (byte) -37);
	}

	final void method548(int i, int i_1_, int i_2_, boolean bool, int i_3_) {
		if (method557((byte) -102) != i_2_) {
			if (i_2_ != -1) {
				if (aClass43_430 == null || aClass43_430.ID != i_2_) {
					aClass43_430 = Class377.aClass310_3188.getAnimation(i_2_, (byte) -124);
				} else if (aClass43_430.anInt410 == 0) {
					return;
				}
				anInt425 = 0;
				anInt431 = i_1_;
				anInt428 = i;
				if (!bool) {
					anInt432 = 0;
					anInt423 = 0;
				} else {
					anInt432 = (int) (aClass43_430.anIntArray402.length * Math.random());
					anInt423 = (int) (aClass43_430.anIntArray405[anInt432] * Math.random());
				}
				anInt433 = anInt432 + 1;
				if (anInt433 < 0 || anInt433 >= aClass43_430.anIntArray402.length) {
					anInt433 = -1;
				}
				if (anInt428 == 0) {
					method565(anInt432, aClass43_430, (byte) -95);
				}
				aBoolean426 = false;
			} else {
				aClass43_430 = null;
			}
			method566((byte) -123);
		}
		if (i_3_ != 21847) {
			method549((byte) -67, 23);
		}
	}

	final void method549(byte i, int i_4_) {
		if (i != 115) {
			aClass12_434 = null;
		}
		method548(0, 0, i_4_, false, i + 21732);
	}

	final void method550(int i, Animator class44_5_) {
		anInt423 = class44_5_.anInt423;
		aClass43_430 = class44_5_.aClass43_430;
		anInt425 = class44_5_.anInt425;
		anInt428 = class44_5_.anInt428;
		aBoolean429 = class44_5_.aBoolean429;
		aBoolean426 = class44_5_.aBoolean426;
		anInt433 = class44_5_.anInt433;
		anInt432 = class44_5_.anInt432;
		if (i == -1) {
			method566((byte) -26);
		}
	}

	final void method551(int i, int i_6_, int i_7_, Model class178) {
		if (method545(0)) {
			class178.method1727(aClass12_434.aClass296_Sub39_Sub6_135, aClass12_434.anInt131, i_6_, aClass12_434.aClass296_Sub39_Sub6_129, null, i, anInt423, 122, aClass43_430.anIntArray405[anInt432], aClass12_434.anInt130, aClass43_430.aBoolean413);
			if (aBoolean429 && aClass43_430.anIntArray419 != null && aClass12_424.aBoolean134) {
				class178.method1727(aClass12_424.aClass296_Sub39_Sub6_135, aClass12_424.anInt131, i_6_, aClass12_424.aClass296_Sub39_Sub6_129, null, i, anInt423, 127, aClass43_430.anIntArray405[anInt432], aClass12_424.anInt130, aClass43_430.aBoolean413);
			}
		}
		if (i_7_ != 24529) {
			method551(-44, -11, 5, null);
		}
	}

	public static void method552(int i) {
		aFrame435 = null;
		if (i != 0) {
			method562(null, null, 103, 77, -99, null);
		}
	}

	final void method553(int i, int i_8_, boolean bool) {
		if (i >= -111) {
			method570((byte) -52);
		}
		method548(0, 0, i_8_, bool, 21847);
	}

	final void method554(byte i) {
		int i_9_ = -107 / ((i + 50) / 52);
		anInt425 = 0;
	}

	final boolean method555(int i, int i_10_) {
		if ((i_10_ -= anInt428) <= 0 | aClass43_430 == null) {
			return false;
		}
		if (i > -68) {
			anInt431 = -13;
		}
		return aClass43_430.anIntArray405[anInt432] < anInt423 + i_10_ | aClass43_430.isTweened;
	}

	public void method556(Model class178, int i, byte i_11_) {
		if (i_11_ != -63) {
			aClass12_424 = null;
		}
		if (aClass43_430 != null) {
			if (method545(i_11_ + 63)) {
				class178.method1741(aClass43_430.aBoolean413, aClass43_430.anIntArray405[anInt432], aClass12_434.anInt130, 0, i, aClass12_434.anInt131, aClass12_434.aClass296_Sub39_Sub6_129, anInt423, aClass12_434.aClass296_Sub39_Sub6_135);
				if (aBoolean429 && aClass43_430.anIntArray419 != null && aClass12_424.aBoolean134) {
					class178.method1741(aClass43_430.aBoolean413, aClass43_430.anIntArray405[anInt432], aClass12_424.anInt130, 0, i, aClass12_424.anInt131, aClass12_424.aClass296_Sub39_Sub6_129, anInt423, aClass12_424.aClass296_Sub39_Sub6_135);
				}
			}
		}
	}

	final int method557(byte i) {
		if (i >= -62) {
			method558(true, -121);
		}
		if (aClass43_430 != null) {
			return aClass43_430.ID;
		}
		return -1;
	}

	final void method558(boolean bool, int i) {
		if (!bool) {
			anInt428 = i;
		}
	}

	final boolean method559(int i, byte i_12_) {
		if (aClass43_430 == null || i == 0) {
			return false;
		}
		if (anInt428 > 0) {
			if (i <= anInt428) {
				anInt428 -= i;
				return false;
			}
			i -= anInt428;
			anInt428 = 0;
			method565(anInt432, aClass43_430, (byte) -95);
		}
		i += anInt423;
		boolean bool = Class369.aBoolean3135 | aClass43_430.isTweened;
		if (i > 100 && aClass43_430.anInt399 > 0) {
			int i_13_;
			for (i_13_ = -aClass43_430.anInt399 + aClass43_430.anIntArray402.length; i_13_ > anInt432; anInt432++) {
				if (i <= aClass43_430.anIntArray405[anInt432]) {
					break;
				}
				i -= aClass43_430.anIntArray405[anInt432];
			}
			if (anInt432 >= i_13_) {
				int i_14_ = 0;
				for (int i_15_ = i_13_; aClass43_430.anIntArray402.length > i_15_; i_15_++) {
					i_14_ = i_14_ + aClass43_430.anIntArray405[i_15_];
				}
				if (anInt431 == 0) {
					anInt425 += i / i_14_;
				}
				i %= i_14_;
			}
			anInt433 = anInt432 + 1;
			bool = true;
			if (aClass43_430.anIntArray402.length <= anInt433) {
				anInt433 -= aClass43_430.anInt399;
				if (anInt433 < 0 || aClass43_430.anIntArray402.length <= anInt433) {
					anInt433 = -1;
				}
			}
		}
		while (aClass43_430.anIntArray405[anInt432] < i) {
			bool = true;
			i -= aClass43_430.anIntArray405[anInt432++];
			if (anInt432 >= aClass43_430.anIntArray402.length) {
				if (aClass43_430.anInt399 != -1 && anInt431 != 2) {
					if (anInt431 == 0) {
						anInt425++;
					}
					anInt432 -= aClass43_430.anInt399;
				}
				if (anInt425 >= aClass43_430.anInt403 || anInt432 < 0 || aClass43_430.anIntArray402.length <= anInt432) {
					aBoolean426 = true;
					break;
				}
			}
			method565(anInt432, aClass43_430, (byte) -95);
			anInt433 = anInt432 + 1;
			if (aClass43_430.anIntArray402.length <= anInt433) {
				anInt433 -= aClass43_430.anInt399;
				if (anInt433 < 0 || anInt433 >= aClass43_430.anIntArray402.length) {
					anInt433 = -1;
				}
			}
		}
		anInt423 = i;
		if (i_12_ < 122) {
			return false;
		}
		if (bool) {
			method566((byte) -31);
		}
		return bool;
	}

	final int method560(boolean bool) {
		if (bool != true) {
			return -43;
		}
		return anInt428;
	}

	final void method561(byte i, Model class178) {
		if (method545(0)) {
			class178.method1742(aClass12_434.aClass296_Sub39_Sub6_135, -31633, aClass12_434.anInt130);
			if (aBoolean429 && aClass43_430.anIntArray419 != null && aClass12_424.aBoolean134) {
				class178.method1742(aClass12_424.aClass296_Sub39_Sub6_135, -31633, aClass12_424.anInt130);
			}
		}
		if (i != 107) {
			/* empty */
		}
	}

	static final ha method562(Js5 class138, Canvas canvas, int i, int i_16_, int i_17_, d var_d) {
		if (i <= 81) {
			return null;
		}
		int i_18_ = 0;
		int i_19_ = 0;
		if (canvas != null) {
			Dimension dimension = canvas.getSize();
			i_18_ = dimension.width;
			i_19_ = dimension.height;
		}
		return ha.method1092(i_16_, var_d, canvas, i_19_, i_17_, (byte) 120, class138, i_18_);
	}

	final Animation method563(int i) {
		if (i != -1) {
			method547(105);
		}
		return aClass43_430;
	}

	final void method564(int i, byte i_20_) {
		anInt432 = 0;
		anInt433 = aClass43_430.anIntArray402.length <= 1 ? -1 : 1;
		if (i_20_ != -37) {
			method554((byte) 67);
		}
		anInt425 = 0;
		anInt428 = i;
		aBoolean426 = false;
		anInt423 = 0;
		if (aClass43_430 != null) {
			method565(anInt432, aClass43_430, (byte) -95);
			method566((byte) -92);
		}
	}

	void method565(int i, Animation class43, byte i_21_) {
		if (i_21_ != -95) {
			anInt423 = 119;
		}
	}

	private final void method566(byte i) {
		if (i <= -18) {
			aClass12_434.method219((byte) -63);
			if (aBoolean429) {
				aClass12_424.method219((byte) -57);
			}
		}
	}

	final boolean method567(int i) {
		if (i != 1) {
			method550(32, null);
		}
		if (anInt428 == 0) {
			return false;
		}
		return true;
	}

	public int method568(int i) {
		if (method545(i)) {
			int i_22_ = 0;
			if (method545(0)) {
				i_22_ |= aClass12_434.anInt138;
				if (aBoolean429 && aClass43_430.anIntArray419 != null) {
					i_22_ |= aClass12_424.anInt138;
				}
			}
			return i_22_;
		}
		return 0;
	}

	static final void method569(boolean bool, byte i) {
		Class78.method797(bool, Class241.anInt2301, Class384.anInt3254, -10111, Class99.anInt1064);
		if (i <= 46) {
			method552(34);
		}
	}

	final boolean method570(byte i) {
		if (i != 40) {
			aFrame435 = null;
		}
		if (aClass43_430 == null) {
			return false;
		}
		return true;
	}

	Animator(boolean bool) {
		aBoolean429 = false;
		aBoolean426 = false;
		aBoolean429 = bool;
		aClass12_434 = new Class12();
		if (aBoolean429) {
			aClass12_424 = new Class12();
		} else {
			aClass12_424 = null;
		}
	}

	final void method571(int i, int i_23_, int i_24_) {
		int i_25_ = 45 / ((-13 - i_23_) / 63);
		method548(i, 0, i_24_, false, 21847);
	}
}
