package net.zaros.client;
import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/20/2017
 */
public class FrameWindowAdapter extends WindowAdapter {
	
	@Override
	public void windowClosing(WindowEvent we) {
		final String ObjButtons[] = { "Yes", "No" };
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int PromptResult = JOptionPane.showOptionDialog(null, "Are you sure you want to exit?", "Confirm", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, ObjButtons, ObjButtons[1]);
				if (PromptResult == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});
	}
}
