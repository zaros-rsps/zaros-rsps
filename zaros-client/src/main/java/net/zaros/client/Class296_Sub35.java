package net.zaros.client;

public abstract class Class296_Sub35 extends Node {
	public int anInt4844;
	private int anInt4845;
	public float aFloat4846;
	private int anInt4847;
	public int anInt4848;
	public int anInt4849;

	abstract void method2744(float f, int i);

	public float method2745(byte i) {
		if (i != 100)
			method2751(100);
		return aFloat4846;
	}

	public int method2746(int i) {
		if (i != -24996)
			method2746(-65);
		return anInt4847;
	}

	abstract void method2747(int i, int i_0_, int i_1_, int i_2_);

	public int method2748(int i) {
		if (i <= 39)
			return -82;
		return anInt4845;
	}

	public int method2749(boolean bool) {
		if (bool != true)
			anInt4849 = 36;
		return anInt4848;
	}

	public int method2750(int i) {
		if (i != -4444)
			method2750(-60);
		return anInt4844;
	}

	public int method2751(int i) {
		if (i != -28925)
			return 122;
		return anInt4849;
	}

	public Class296_Sub35(int i, int i_3_, int i_4_, int i_5_, int i_6_, float f) {
		anInt4845 = i_5_;
		anInt4849 = i_3_;
		anInt4848 = i;
		anInt4847 = i_6_;
		anInt4844 = i_4_;
		aFloat4846 = f;
	}
}
