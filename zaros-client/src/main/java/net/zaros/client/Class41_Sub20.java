package net.zaros.client;

/* Class41_Sub20 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub20 extends Class41 {
	static AdvancedMemoryCache aClass113_3794;
	static int anInt3795 = 1;
	static int anInt3796;
	static OutgoingPacket aClass311_3797;

	final int method470(int i) {
		if (i < 114)
			method381(47, (byte) -25);
		return anInt389;
	}

	Class41_Sub20(int i, Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final void method386(int i) {
		if (anInt389 != 1 && anInt389 != 0)
			anInt389 = method383((byte) 110);
		if (i != 2)
			method383((byte) 42);
	}

	final void method381(int i, byte i_0_) {
		if (i_0_ != -110)
			anInt3795 = 52;
		anInt389 = i;
	}

	Class41_Sub20(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final int method380(int i, byte i_1_) {
		if (i_1_ != 41)
			return -113;
		return 1;
	}

	static final boolean method471(int i, int i_2_, int i_3_) {
		if (i != 0)
			aClass113_3794 = null;
		if ((i_2_ & 0x34) == 0)
			return false;
		return true;
	}

	final int method383(byte i) {
		if (i != 110)
			return -98;
		return 1;
	}

	public static void method472(int i) {
		aClass113_3794 = null;
		if (i != 34)
			method474(-23, 88, -76);
		aClass311_3797 = null;
	}

	static final void method473(NodeDeque class155, int i, ha var_ha, int i_4_, int i_5_) {
		Class366_Sub8.aClass155_5409.method1581(327680);
		if (i_5_ == 34 && !Class83.aBoolean914) {
			for (Class296_Sub53 class296_sub53 = (Class296_Sub53) class155.removeFirst((byte) 125); class296_sub53 != null; class296_sub53 = (Class296_Sub53) class155.removeNext(i_5_ ^ 0x3cb)) {
				Class18 class18 = Class106.aClass245_1094.method2179(11702, class296_sub53.anInt5046);
				if (Class296_Sub52.method3203(-73, class18)) {
					boolean bool = Class368_Sub7.method3834(var_ha, i, i_4_, class18, class296_sub53, -632057695);
					if (bool)
						Class41_Sub17.method457(class296_sub53, (byte) -80, var_ha, class18);
				}
			}
		}
	}

	static final void method474(int i, int i_6_, int i_7_) {
		if (i_6_ == 4) {
			GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_7_, 16);
			class296_sub39_sub5.insertIntoQueue_2();
			class296_sub39_sub5.intParam = i;
		}
	}

	static {
		aClass113_3794 = new AdvancedMemoryCache(4);
		aClass311_3797 = new OutgoingPacket(34, 6);
	}
}
