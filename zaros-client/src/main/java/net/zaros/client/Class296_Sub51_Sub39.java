package net.zaros.client;

/* Class296_Sub51_Sub39 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub39 extends TextureOperation {
	private int[] anIntArray6544;
	private int anInt6545 = 3216;
	static Class404 aClass404_6546;
	private int anInt6547;
	private int anInt6548;
	static boolean aBoolean6549 = false;
	static float[] aFloatArray6550;
	static int anInt6551;

	public static void method3199(int i) {
		if (i == 65977057) {
			aFloatArray6550 = null;
			aClass404_6546 = null;
		}
	}

	public Class296_Sub51_Sub39() {
		super(1, true);
		anIntArray6544 = new int[3];
		anInt6548 = 3216;
		anInt6547 = 4096;
	}

	private final void method3200(int i) {
		double d = Math.cos((double) ((float) anInt6545 / 4096.0F));
		anIntArray6544[0] = (int) (Math.sin((double) ((float) anInt6548 / 4096.0F)) * d * 4096.0);
		anIntArray6544[1] = (int) (d * Math.cos((double) ((float) anInt6548 / 4096.0F)) * 4096.0);
		anIntArray6544[2] = (int) (Math.sin((double) ((float) anInt6545 / 4096.0F)) * 4096.0);
		int i_0_ = anIntArray6544[0] * anIntArray6544[0] >> 12;
		int i_1_ = anIntArray6544[1] * anIntArray6544[1] >> 12;
		int i_2_ = anIntArray6544[2] * anIntArray6544[2] >> 12;
		int i_3_ = (int) (Math.sqrt((double) (i_2_ + (i_1_ + i_0_) >> 12)) * 4096.0);
		if (i_3_ != 0) {
			anIntArray6544[1] = (anIntArray6544[1] << 12) / i_3_;
			anIntArray6544[0] = (anIntArray6544[0] << 12) / i_3_;
			anIntArray6544[2] = (anIntArray6544[2] << 12) / i_3_;
		}
		if (i != -9154)
			method3201(21, -59, null, -50, -32, null, 109, -107, 0.61131346F, -92, -117);
	}

	final int[] get_monochrome_output(int i, int i_4_) {
		if (i != 0)
			aFloatArray6550 = null;
		int[] is = aClass318_5035.method3335(i_4_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int i_5_ = anInt6547 * StaticMethods.anInt5924 >> 12;
			int[] is_6_ = this.method3064(0, 0, Class67.anInt753 & i_4_ - 1);
			int[] is_7_ = this.method3064(0, 0, i_4_);
			int[] is_8_ = this.method3064(0, 0, Class67.anInt753 & i_4_ + 1);
			for (int i_9_ = 0; Class41_Sub10.anInt3769 > i_9_; i_9_++) {
				int i_10_ = i_5_ * (-is_6_[i_9_] + is_8_[i_9_]) >> 12;
				int i_11_ = ((is_7_[i_9_ - 1 & Class41_Sub25.anInt3803] - is_7_[i_9_ + 1 & Class41_Sub25.anInt3803]) * i_5_ >> 12);
				int i_12_ = i_11_ >> 4;
				int i_13_ = i_10_ >> 4;
				if (i_12_ < 0)
					i_12_ = -i_12_;
				if (i_12_ > 255)
					i_12_ = 255;
				if (i_13_ < 0)
					i_13_ = -i_13_;
				if (i_13_ > 255)
					i_13_ = 255;
				int i_14_ = Class368_Sub8.aByteArray5465[i_12_ + ((i_13_ + 1) * i_13_ >> 1)] & 0xff;
				int i_15_ = i_14_ * 4096 >> 8;
				int i_16_ = i_14_ * i_11_ >> 8;
				int i_17_ = i_14_ * i_10_ >> 8;
				i_17_ = anIntArray6544[1] * i_17_ >> 12;
				i_16_ = anIntArray6544[0] * i_16_ >> 12;
				i_15_ = i_15_ * anIntArray6544[2] >> 12;
				is[i_9_] = i_16_ - (-i_17_ - i_15_);
			}
		}
		return is;
	}

	static final void method3201(int i, int i_18_, float[] fs, int i_19_, int i_20_, float[] fs_21_, int i_22_, int i_23_, float f, int i_24_, int i_25_) {
		i_19_ -= i_18_;
		i_25_ -= i_24_;
		i_22_ -= i_23_;
		float f_26_ = ((float) i_19_ * fs[0] + fs[1] * (float) i_22_ + fs[2] * (float) i_25_);
		int i_27_ = -22 % ((i_20_ + 33) / 46);
		float f_28_ = (fs[5] * (float) i_25_ + ((float) i_19_ * fs[3] + fs[4] * (float) i_22_));
		float f_29_ = (fs[6] * (float) i_19_ + fs[7] * (float) i_22_ + fs[8] * (float) i_25_);
		float f_30_ = (float) Math.sqrt((double) (f_28_ * f_28_ + f_26_ * f_26_ + f_29_ * f_29_));
		float f_31_ = ((float) Math.atan2((double) f_26_, (double) f_29_) / 6.2831855F + 0.5F);
		float f_32_ = f + ((float) Math.asin((double) (f_28_ / f_30_)) / 3.1415927F + 0.5F);
		if (i == 1) {
			float f_33_ = f_31_;
			f_31_ = -f_32_;
			f_32_ = f_33_;
		} else if (i != 2) {
			if (i == 3) {
				float f_34_ = f_31_;
				f_31_ = f_32_;
				f_32_ = -f_34_;
			}
		} else {
			f_31_ = -f_31_;
			f_32_ = -f_32_;
		}
		fs_21_[0] = f_31_;
		fs_21_[1] = f_32_;
	}

	final void method3071(int i, Packet class296_sub17, int i_35_) {
		int i_36_ = i_35_;
		while_270_ : do {
			do {
				if (i_36_ != 0) {
					if (i_36_ != 1) {
						if (i_36_ == 2)
							break;
						break while_270_;
					}
				} else {
					anInt6547 = class296_sub17.g2();
					break while_270_;
				}
				anInt6548 = class296_sub17.g2();
				break while_270_;
			} while (false);
			anInt6545 = class296_sub17.g2();
		} while (false);
		if (i >= -84) {
			/* empty */
		}
	}

	final void method3076(byte i) {
		int i_37_ = 38 / ((-58 - i) / 40);
		method3200(-9154);
	}

	static final int method3202(int i) {
		if (i > -86)
			return 48;
		return Class220.anInt2150;
	}

	static {
		aClass404_6546 = new Class404();
		anInt6551 = 0;
		aFloatArray6550 = new float[4];
	}
}
