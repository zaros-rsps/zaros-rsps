package net.zaros.client;

/* Class296_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class NPCNode extends Node {
	static int anInt4625;
	static int anInt4626 = -1;
	NPC value;

	static final void method2447(Js5 class138, byte i) {
		if (i == 70)
			Class296_Sub39_Sub11.aClass138_6189 = class138;
	}

	static final void method2448(String playerName) {
		if (Class127.aBoolean1304 && (Class321.anInt2824 & 0x18) != 0) {
			boolean bool = false;
			int i_0_ = PlayerUpdate.visiblePlayersCount;
			int[] is = PlayerUpdate.visiblePlayersIndexes;
			for (int a = 0; i_0_ > a; a++) {
				Player targ = (PlayerUpdate.visiblePlayers[is[a]]);
				if (targ.displayName != null && targ.displayName.equalsIgnoreCase(playerName) && ((((Class296_Sub51_Sub11.localPlayer) == targ) && (Class321.anInt2824 & 0x10) != 0) || (Class321.anInt2824 & 0x8) != 0)) {
					Class123_Sub1_Sub2.anInt5816++;
					Class296_Sub1 bf = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 100, (Class296_Sub39_Sub7.aClass311_6157));
					bf.out.writeLEShort(is[a]);
					bf.out.p1(0);
					bf.out.writeShortA(Class69.anInt3689);
					bf.out.writeLEInt(Class366_Sub4.anInt5375);
					bf.out.writeLEShortA(Class180.anInt1857);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(bf, (byte) 119);
					bool = true;
					Class405.findPathStandart(-2, targ.wayPointQueueY[0], targ.waypointQueueX[0], targ.getSize(), targ.getSize(), 0, 0, true);
					break;
				}
			}
			if (!bool)
				Class34.method351(4, true, (TranslatableString.cantFind.getTranslation(Class394.langID) + playerName));
			if (Class127.aBoolean1304)
				Class285.method2368(0);
		}
	}

	static final short[] method2449(int i, int i_2_, short[] is) {
		short[] is_3_ = new short[i_2_];
		Class291.copy(is, 0, is_3_, i, i_2_);
		return is_3_;
	}

	static final void method2450(int i, Packet class296_sub17, int i_4_, Class398 class398) {
		Class296_Sub24 class296_sub24 = new Class296_Sub24();
		class296_sub24.anInt4756 = class296_sub17.g1();
		class296_sub24.anInt4758 = class296_sub17.g4();
		class296_sub24.aClass278Array4749 = new Class278[class296_sub24.anInt4756];
		class296_sub24.anIntArray4754 = new int[class296_sub24.anInt4756];
		class296_sub24.anIntArray4750 = new int[class296_sub24.anInt4756];
		class296_sub24.aClass278Array4755 = new Class278[class296_sub24.anInt4756];
		class296_sub24.anIntArray4757 = new int[class296_sub24.anInt4756];
		class296_sub24.aByteArrayArrayArray4751 = new byte[class296_sub24.anInt4756][][];
		for (int i_5_ = i_4_; class296_sub24.anInt4756 > i_5_; i_5_++) {
			try {
				int i_6_ = class296_sub17.g1();
				if (i_6_ == 0 || i_6_ == 1 || i_6_ == 2) {
					String string = class296_sub17.gstr();
					String string_7_ = class296_sub17.gstr();
					int i_8_ = 0;
					if (i_6_ == 1)
						i_8_ = class296_sub17.g4();
					class296_sub24.anIntArray4750[i_5_] = i_6_;
					class296_sub24.anIntArray4757[i_5_] = i_8_;
					class296_sub24.aClass278Array4749[i_5_] = class398.method4118(Class261.method2248(0, string), (byte) -128, string_7_);
				} else if (i_6_ == 3 || i_6_ == 4) {
					String string = class296_sub17.gstr();
					String string_9_ = class296_sub17.gstr();
					int i_10_ = class296_sub17.g1();
					String[] strings = new String[i_10_];
					for (int i_11_ = 0; i_10_ > i_11_; i_11_++)
						strings[i_11_] = class296_sub17.gstr();
					byte[][] is = new byte[i_10_][];
					if (i_6_ == 3) {
						for (int i_12_ = 0; i_10_ > i_12_; i_12_++) {
							int i_13_ = class296_sub17.g4();
							is[i_12_] = new byte[i_13_];
							class296_sub17.readBytes(is[i_12_], 0, i_13_);
						}
					}
					class296_sub24.anIntArray4750[i_5_] = i_6_;
					Class[] var_classes = new Class[i_10_];
					for (int i_14_ = 0; i_14_ < i_10_; i_14_++)
						var_classes[i_14_] = Class261.method2248(i_4_, strings[i_14_]);
					class296_sub24.aClass278Array4755[i_5_] = class398.method4119(string_9_, Class261.method2248(0, string), var_classes, true);
					class296_sub24.aByteArrayArrayArray4751[i_5_] = is;
				}
			} catch (ClassNotFoundException classnotfoundexception) {
				class296_sub24.anIntArray4754[i_5_] = -1;
			} catch (SecurityException securityexception) {
				class296_sub24.anIntArray4754[i_5_] = -2;
			} catch (NullPointerException nullpointerexception) {
				class296_sub24.anIntArray4754[i_5_] = -3;
			} catch (Exception exception) {
				class296_sub24.anIntArray4754[i_5_] = -4;
			} catch (Throwable throwable) {
				class296_sub24.anIntArray4754[i_5_] = -5;
			}
		}
		Class328.aClass155_2912.addLast((byte) 109, class296_sub24);
	}

	static final boolean playerIgnored(String name) {
		if (name == null)
			return false;
		for (int i = 0; Class317.ignoresSize > i; i++) {
			if (name.equalsIgnoreCase(Class296_Sub51_Sub20.ignoreNames2[i]))
				return true;
			if (name.equalsIgnoreCase(Class123.ignoreNames4[i]))
				return true;
		}
		return false;
	}

	NPCNode(NPC class338_sub3_sub1_sub3_sub2) {
		value = class338_sub3_sub1_sub3_sub2;
	}
}
