package net.zaros.client;

/* Class15 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class15 {
	int anInt175;
	private Js5 aClass138_176;
	private AdvancedMemoryCache aClass113_177 = new AdvancedMemoryCache(64);
	static IncomingPacket CAM_LOOKAT = new IncomingPacket(26, 6);
	int anInt179 = 0;
	static OutgoingPacket aClass311_180 = new OutgoingPacket(71, 16);

	final void method227(int i) {
		synchronized (aClass113_177) {
			aClass113_177.clear();
		}
		if (i != 64)
			CAM_LOOKAT = null;
	}

	final void method228(int i, boolean bool) {
		if (!bool) {
			synchronized (aClass113_177) {
				aClass113_177.clean(i);
			}
		}
	}

	public static void method229(int i) {
		if (i > -9)
			aClass311_180 = null;
		aClass311_180 = null;
		CAM_LOOKAT = null;
	}

	final void method230(int i) {
		synchronized (aClass113_177) {
			aClass113_177.clearSoftReferences();
			if (i <= 23)
				aClass113_177 = null;
		}
	}

	static final void method231(int i, ha var_ha) {
		if (i < 91)
			CAM_LOOKAT = null;
		for (Class338_Sub1 class338_sub1 = ((Class338_Sub1) Class368_Sub19.aClass404_5542.method4160((byte) -49)); class338_sub1 != null; class338_sub1 = (Class338_Sub1) Class368_Sub19.aClass404_5542.method4163(-24917)) {
			if (class338_sub1.aBoolean5191)
				class338_sub1.method3454(var_ha);
		}
	}

	final Class61 method232(boolean bool, int i) {
		Class61 class61;
		synchronized (aClass113_177) {
			class61 = (Class61) aClass113_177.get((long) i);
		}
		if (class61 != null)
			return class61;
		byte[] is;
		synchronized (aClass138_176) {
			is = aClass138_176.getFile(4, i);
		}
		class61 = new Class61();
		class61.aClass15_696 = this;
		class61.anInt698 = i;
		if (is != null)
			class61.method689(new Packet(is), (byte) 115);
		class61.method690(!bool);
		synchronized (aClass113_177) {
			aClass113_177.put(class61, (long) i);
			if (bool != true)
				aClass311_180 = null;
		}
		return class61;
	}

	Class15(GameType class35, int i, Js5 class138) {
		aClass138_176 = class138;
		anInt175 = aClass138_176.getLastFileId(4);
	}
}
