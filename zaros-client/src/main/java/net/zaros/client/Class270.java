package net.zaros.client;

/* Class270 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class270 {
	static int[] anIntArray2505 = new int[256];
	int anInt2506;
	static Js5 fs1;
	int[] anIntArray2508;
	Class296_Sub39_Sub10 aClass296_Sub39_Sub10_2509;
	static int anInt2510;

	public static void method2304(int i) {
		anIntArray2505 = null;
		int i_0_ = 117 % ((i + 24) / 55);
		fs1 = null;
	}

	static final void method2305(int i, Packet class296_sub17) {
		if (-class296_sub17.pos + class296_sub17.data.length >= 1) {
			int i_1_ = class296_sub17.g1();
			if (i_1_ >= 0 && i_1_ <= 1 && (-class296_sub17.pos + class296_sub17.data.length) >= 2) {
				int i_2_ = -36 % ((i - 1) / 41);
				int i_3_ = class296_sub17.g2();
				if ((class296_sub17.data.length - class296_sub17.pos) >= i_3_ * 6) {
					for (int i_4_ = 0; i_4_ < i_3_; i_4_++) {
						int i_5_ = class296_sub17.g2();
						int i_6_ = class296_sub17.g4();
						if (Class269.globalIntVars.length > i_5_ && Class14.aBooleanArray153[i_5_] && ((Class220.aClass151_2151.method1542((byte) -42, i_5_).aChar323) != '1' || i_6_ >= -1 && i_6_ <= 1))
							Class269.globalIntVars[i_5_] = i_6_;
					}
				}
			}
		}
	}

	static final void method2306(int i, byte i_7_) {
		if (i_7_ >= -50)
			method2304(-50);
		Class296_Sub14 class296_sub14 = ((Class296_Sub14) Class296_Sub51_Sub11.aClass263_6399.get((long) i));
		if (class296_sub14 != null) {
			class296_sub14.aBoolean4665 = !class296_sub14.aBoolean4665;
			class296_sub14.aClass219_Sub1_4667.method2047(-103, class296_sub14.aBoolean4665);
		}
	}

	public Class270() {
		/* empty */
	}

	static final int method2307(Class210_Sub1 class210_sub1, boolean bool, int i, int i_8_, Class210_Sub1 class210_sub1_9_) {
		if (i == i_8_) {
			int i_10_ = class210_sub1.anInt2096;
			int i_11_ = class210_sub1_9_.anInt2096;
			if (!bool) {
				if (i_10_ == -1)
					i_10_ = 2001;
				if (i_11_ == -1)
					i_11_ = 2001;
			}
			return i_10_ - i_11_;
		}
		if (i == 2)
			return Class106_Sub1.method941((class210_sub1_9_.method2010(true).aString2724), Class394.langID, -63, (class210_sub1.method2010(true).aString2724));
		if (i == 3) {
			if (class210_sub1.aString4542.equals("-")) {
				if (class210_sub1_9_.aString4542.equals("-"))
					return 0;
				if (!bool)
					return 1;
				return -1;
			}
			if (class210_sub1_9_.aString4542.equals("-")) {
				if (!bool)
					return -1;
				return 1;
			}
			return Class106_Sub1.method941(class210_sub1_9_.aString4542, Class394.langID, 120, class210_sub1.aString4542);
		}
		if (i == 4) {
			if (!class210_sub1.method2008(107)) {
				if (class210_sub1_9_.method2008(97))
					return -1;
				return 0;
			}
			if (class210_sub1_9_.method2008(i_8_ ^ 0x67))
				return 0;
			return 1;
		}
		if (i == 5) {
			if (class210_sub1.method2006(i_8_ - 75)) {
				if (!class210_sub1_9_.method2006(-127))
					return 1;
				return 0;
			}
			if (class210_sub1_9_.method2006(-63))
				return -1;
			return 0;
		}
		if (i == 6) {
			if (class210_sub1.method2007(16384)) {
				if (!class210_sub1_9_.method2007(16384))
					return 1;
				return 0;
			}
			if (class210_sub1_9_.method2007(16384))
				return -1;
			return 0;
		}
		if (i == 7) {
			if (class210_sub1.method2005(1)) {
				if (!class210_sub1_9_.method2005(1))
					return 1;
				return 0;
			}
			if (!class210_sub1_9_.method2005(1))
				return 0;
			return -1;
		}
		if (i == 8) {
			int i_12_ = class210_sub1.anInt4538;
			int i_13_ = class210_sub1_9_.anInt4538;
			if (bool) {
				if (i_13_ == 1000)
					i_13_ = -1;
				if (i_12_ == 1000)
					i_12_ = -1;
			} else {
				if (i_13_ == -1)
					i_13_ = 1000;
				if (i_12_ == -1)
					i_12_ = 1000;
			}
			return i_12_ - i_13_;
		}
		return class210_sub1.anInt4540 - class210_sub1_9_.anInt4540;
	}

	static {
		for (int i = 0; i < 256; i++) {
			int i_14_ = i;
			for (int i_15_ = 0; i_15_ < 8; i_15_++) {
				if ((i_14_ & 0x1) != 1)
					i_14_ >>>= 1;
				else
					i_14_ = i_14_ >>> 1 ^ ~0x12477cdf;
			}
			anIntArray2505[i] = i_14_;
		}
	}
}
