package net.zaros.client;

/* Class122_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class122_Sub2 extends Class122 {
	static boolean aBoolean5662;

	Class122_Sub2(Js5 class138, Js5 class138_0_, Class379_Sub1 class379_sub1) {
		super(class138, class138_0_, (Class379) class379_sub1);
	}

	static final int method1046(int i, int i_1_, boolean bool) {
		int i_2_;
		if (i_1_ <= 20000) {
			if (i_1_ <= 10000) {
				if (i_1_ <= 5000) {
					i_2_ = 1;
					Class368_Sub20.method3857(true, (byte) 121);
				} else {
					Class276.method2321(1);
					i_2_ = 2;
				}
			} else {
				i_2_ = 3;
				Class205_Sub4.method1986((byte) -55);
			}
		} else {
			i_2_ = 4;
			Class230.method2108(-73);
		}
		if (i != Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(125)) {
			Class343_Sub1.aClass296_Sub50_5282.method3060(i, 42, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988);
			Class33.method348(false, false, i);
		}
		if (bool)
			return -89;
		Class368_Sub4.method3820(1);
		return i_2_;
	}

	final void method1036(boolean bool, int i, int i_3_, byte i_4_) {
		if (i_4_ != 80)
			method1046(68, -35, false);
		int i_5_ = this.method1039(true) * aClass379_3543.anInt3613 / 10000;
		Class41_Sub13.aHa3774.aa(i, i_3_ + 2, i_5_, aClass379_3543.anInt3626 - 2, ((Class379_Sub1) aClass379_3543).anInt5675, 0);
		Class41_Sub13.aHa3774.aa(i + i_5_, i_3_ + 2, -i_5_ + aClass379_3543.anInt3613, aClass379_3543.anInt3626 - 2, 0, 0);
	}

	final void method1035(int i, boolean bool, int i_6_, int i_7_) {
		if (i_6_ == -24228) {
			Class41_Sub13.aHa3774.b(i_7_ - 2, i, aClass379_3543.anInt3613 + 4, aClass379_3543.anInt3626 + 2, ((Class379_Sub1) aClass379_3543).anInt5673, 0);
			Class41_Sub13.aHa3774.b(i_7_ - 1, i + 1, aClass379_3543.anInt3613 + 2, aClass379_3543.anInt3626, 0, 0);
		}
	}
}
