package net.zaros.client;

/* Class187 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class187 {
	boolean aBoolean1908;
	int anInt1909 = 16777215;
	static int anInt1910;
	int anInt1911;
	static long aLong1912;
	int anInt1913;
	int anInt1914;
	int anInt1915;
	int anInt1916;
	int anInt1917;
	int anInt1918;
	int anInt1919;
	static Packet someAppereanceBuffer;
	int anInt1921 = 8;
	static long aLong1922;
	static InterfaceComponent aClass51_1923 = null;

	private final void method1883(byte i, int i_0_, Packet class296_sub17) {
		if (i_0_ != 1) {
			if (i_0_ == 2)
				aBoolean1908 = true;
			else if (i_0_ != 3) {
				if (i_0_ != 4) {
					if (i_0_ == 5)
						anInt1917 = class296_sub17.g2();
					else if (i_0_ != 6) {
						if (i_0_ == 7) {
							anInt1915 = class296_sub17.g2b();
							anInt1918 = class296_sub17.g2b();
							anInt1919 = class296_sub17.g2b();
						}
					} else
						anInt1909 = class296_sub17.readUnsignedMedInt();
				} else
					anInt1914 = class296_sub17.g1();
			} else {
				anInt1911 = class296_sub17.g2b();
				anInt1913 = class296_sub17.g2b();
				anInt1916 = class296_sub17.g2b();
			}
		} else
			anInt1921 = class296_sub17.g2();
		if (i <= 93)
			anInt1915 = -71;
	}

	static final void method1884(byte i, int i_1_) {
		if (i == -22) {
			synchronized (Class280.aClass113_2555) {
				Class280.aClass113_2555.clean(i_1_);
			}
		}
	}

	static final Class69_Sub1_Sub1 method1885(int[] is, boolean bool, int i, ha_Sub3 var_ha_Sub3, int i_2_, byte i_3_, int i_4_, int i_5_) {
		if (i_3_ != -27)
			someAppereanceBuffer = null;
		if (!var_ha_Sub3.aBoolean4219 && (!Class30.method329(2844, i_5_) || !Class30.method329(2844, i_4_))) {
			if (var_ha_Sub3.aBoolean4257)
				return new Class69_Sub1_Sub1(var_ha_Sub3, 34037, i_5_, i_4_, bool, is, i_2_, i);
			return new Class69_Sub1_Sub1(var_ha_Sub3, i_5_, i_4_, Class8.get_next_high_pow2(i_5_), Class8.get_next_high_pow2(i_4_), is);
		}
		return new Class69_Sub1_Sub1(var_ha_Sub3, 3553, i_5_, i_4_, bool, is, i_2_, i);
	}

	public static void method1886(byte i) {
		someAppereanceBuffer = null;
		if (i != -10)
			aLong1922 = 86L;
		aClass51_1923 = null;
	}

	final void method1887(byte i, Packet class296_sub17) {
		for (;;) {
			int i_6_ = class296_sub17.g1();
			if (i_6_ == 0)
				break;
			method1883((byte) 95, i_6_, class296_sub17);
		}
		int i_7_ = -109 % ((42 - i) / 51);
	}

	static final void method1888(byte i) {
		Class192.openedInterfaceComponents = new InterfaceComponent[BillboardRaw.interfacesFS.getLastGroupId()][];
		Class338_Sub10.aClass51ArrayArray5272 = new InterfaceComponent[BillboardRaw.interfacesFS.getLastGroupId()][];
		if (i >= -41)
			anInt1910 = -11;
		Class219_Sub1.loadedInterfaces = new boolean[BillboardRaw.interfacesFS.getLastGroupId()];
	}

	public Class187() {
		/* empty */
	}
}
