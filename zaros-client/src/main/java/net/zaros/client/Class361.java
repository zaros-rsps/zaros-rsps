package net.zaros.client;

/* Class361 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class361 {
	Class69_Sub1[] aClass69_Sub1Array3095 = null;
	static boolean aBoolean3096 = false;
	Class69_Sub2 aClass69_Sub2_3097;
	Class69_Sub2 aClass69_Sub2_3098 = null;
	static int anInt3099;
	Class69_Sub2 aClass69_Sub2_3100;
	Class69_Sub1[] aClass69_Sub1Array3101 = null;
	boolean aBoolean3102;
	static int anInt3103;
	static IncomingPacket aClass231_3104 = new IncomingPacket(8, 6);

	public static void method3755(int i) {
		if (i == 31372)
			aClass231_3104 = null;
	}

	Class361(ha_Sub3 var_ha_Sub3) {
		aClass69_Sub2_3097 = null;
		aClass69_Sub2_3100 = null;
		aBoolean3102 = var_ha_Sub3.aBoolean4225;
		Class41_Sub30.method524(128, var_ha_Sub3);
		if (!aBoolean3102) {
			aClass69_Sub1Array3095 = new Class69_Sub1[16];
			for (int i = 0; i < 16; i++) {
				byte[] is = Class366_Sub9.method3799(i * 128 * 2 * 128, 0, ConfigsRegister.anObject3669, 32768);
				aClass69_Sub1Array3095[i] = new Class69_Sub1(var_ha_Sub3, 3553, 6410, 128, 128, true, is, 6410, false);
			}
			aClass69_Sub1Array3101 = new Class69_Sub1[16];
			for (int i = 0; i < 16; i++) {
				byte[] is = Class366_Sub9.method3799(i * 128 * 256, 0, InputStream_Sub2.anObject35, 32768);
				aClass69_Sub1Array3101[i] = new Class69_Sub1(var_ha_Sub3, 3553, 6410, 128, 128, true, is, 6410, false);
			}
		} else {
			byte[] is = Class325.unwrapBuffer(false, -1, ConfigsRegister.anObject3669);
			aClass69_Sub2_3097 = new Class69_Sub2(var_ha_Sub3, 6410, 128, 128, 16, is, 6410);
			is = Class325.unwrapBuffer(false, -1, InputStream_Sub2.anObject35);
			aClass69_Sub2_3098 = new Class69_Sub2(var_ha_Sub3, 6410, 128, 128, 16, is, 6410);
			Class91 class91 = var_ha_Sub3.aClass91_4137;
			if (class91.method841(1)) {
				is = Class325.unwrapBuffer(false, -1, GameType.anObject339);
				aClass69_Sub2_3100 = new Class69_Sub2(var_ha_Sub3, 6408, 128, 128, 16);
				Class69_Sub2 class69_sub2 = new Class69_Sub2(var_ha_Sub3, 6409, 128, 128, 16, is, 6409);
				if (class91.method840(aClass69_Sub2_3100, class69_sub2, -56, 2.0F))
					aClass69_Sub2_3100.method719(10240);
				else {
					aClass69_Sub2_3100.method726(0);
					aClass69_Sub2_3100 = null;
				}
				class69_sub2.method726(0);
			}
		}
	}
}
