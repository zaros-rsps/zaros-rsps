package net.zaros.client;

public class Js5 {
	private ReferenceTable referenceTable = null;
	static Class125 aClass125_1411 = new Class125();
	int discardUnpacked_;
	private Object[] entryBuffers;
	private boolean discardEntryBuffers_;
	private FileWorker worker;
	static int anInt1416;
	private Object[][] childBuffers;
	static Class278 aClass278_1418;

	private static int tnHash(String string) {
		int strlen = string.length();
		int sum = 0;
		for (int i = 0; i < strlen; i++) {
			sum = Class296_Sub39_Sub9.method2827(string.charAt(i), (byte) -113) + (sum << 5) - sum;
		}
		return sum;
	}

	private boolean hasReferenceTable() {
		if (referenceTable == null) {
			referenceTable = worker.getReferenceTable((byte) 109);
			if (referenceTable == null) {
				return false;
			}
			childBuffers = new Object[referenceTable.enryIndexCount][];
			entryBuffers = new Object[referenceTable.enryIndexCount];
		}
		return true;
	}

	void clearIdentifiers(boolean bool, byte i, boolean bool_3_) {
		if (i >= 62 && hasReferenceTable()) {
			if (bool_3_) {
				referenceTable.entryIdentifiers = null;
				referenceTable.entryIdentTable = null;
			}
			if (bool) {
				referenceTable.childIdentTables = null;
				referenceTable.childIdentifiers = null;
			}
		}
	}

	boolean hasFileBuffer_(String name) {
		if (!hasReferenceTable()) {
			return false;
		}
		name = name.toLowerCase();
		int id = referenceTable.entryIdentTable.lookup(tnHash(name));
		return hasFileBuffer((byte) -111, id);
	}

	boolean method1431(String string) {
		int i_5_ = getFileIndex("");
		if (i_5_ != -1) {
			return hasEntryBuffer(string, "", (byte) -70);
		}
		return hasEntryBuffer("", string, (byte) -70);
	}

	void clearEntryBuffers(byte i) {
		if (childBuffers != null) {
			for (int i_7_ = 0; i_7_ < childBuffers.length; i_7_++) {
				childBuffers[i_7_] = null;
			}
		}
		if (i != -126) {
			anInt1416 = -91;
		}
	}

	byte[] getFile(int file, int child, int[] keys) {
		if (!validIndices(child, file, (byte) -105)) {
			return null;
		}
		if (childBuffers[file] == null || childBuffers[file][child] == null) {
			boolean prepared = prepareChildBuffers(keys, child, (byte) -117, file);
			if (!prepared) {
				loadBuffer(file, 0);
				prepared = prepareChildBuffers(keys, child, (byte) -124, file);
				if (!prepared) {
					return null;
				}
			}
		}
		byte[] is_10_ = Class325.unwrapBuffer(false, -1, childBuffers[file][child]);
		if (discardUnpacked_ != 1) {
			if (discardUnpacked_ == 2) {
				childBuffers[file] = null;
			}
		} else {
			childBuffers[file][child] = null;
			if (referenceTable.childIndexCounts[file] == 1) {
				childBuffers[file] = null;
			}
		}
		return is_10_;
	}

	int getReferenceCRC(int i) {
		if (!hasReferenceTable()) {
			throw new IllegalStateException("");
		}
		if (i != 22354) {
			hasFileBuffer_(null);
		}
		return referenceTable.crc;
	}

	boolean filesCompleted(int i) {
		if (!hasReferenceTable()) {
			return false;
		}
		boolean bool = true;
		if (i != -2) {
			discardEntryBuffers_ = false;
		}
		for (int i_12_ : referenceTable.entryIndices) {
			if (entryBuffers[i_12_] == null) {
				loadBuffer(i_12_, i ^ ~0x1);
				if (entryBuffers[i_12_] == null) {
					bool = false;
				}
			}
		}
		return bool;
	}

	private int getFileCompletion(int fileID) {
		if (!validEntryIndex(fileID)) {
			return 0;
		}
		if (entryBuffers[fileID] != null) {
			return 100;
		}
		return worker.getFileCompletion(fileID, true);
	}

	boolean hasFile(String string, boolean bool) {
		if (bool != true) {
			aClass125_1411 = null;
		}
		if (!hasReferenceTable()) {
			return false;
		}
		string = string.toLowerCase();
		int i = referenceTable.entryIdentTable.lookup(tnHash(string));
		if (i < 0) {
			return false;
		}
		return true;
	}

	private void loadBuffer(int i, int i_14_) {
		if (i_14_ == 0) {
			if (discardEntryBuffers_) {
				entryBuffers[i] = worker.getFileBuffer(-19808, i);
			} else {
				entryBuffers[i] = Class166.wrapBuffer(false, 59, worker.getFileBuffer(i_14_ - 19808, i));
			}
		}
	}

	byte[] getFile_(String childName, String fileName) {
		if (!hasReferenceTable()) {
			return null;
		}
		fileName = fileName.toLowerCase();
		childName = childName.toLowerCase();
		int i_16_ = referenceTable.entryIdentTable.lookup(tnHash(fileName));
		if (!validEntryIndex(i_16_)) {
			return null;
		}
		int i_17_ = referenceTable.childIdentTables[i_16_].lookup(tnHash(childName));
		return getFile(i_16_, i_17_);
	}

	static void method1440(int i) {
		if (Class296_Sub39_Sub12.loginState != 0) {
			Class377.loginConnection.method1966(320);
			Class368_Sub21.method3864(-97);
			Class240.method2148(true);
		}
		if (i >= -12) {
			anInt1416 = 120;
		}
	}

	public byte[] getFile(int fileID, int childID) {
		return getFile(fileID, childID, null);
	}

	public int[] getChildIndicies(int i_20_) {
		if (!validEntryIndex(i_20_)) {
			return null;
		}
		int[] ci = referenceTable.childIndices[i_20_];
		if (ci == null) {
			ci = new int[referenceTable.entryChildCounts[i_20_]];
			for (int i = 0; i < ci.length; i++) {
				ci[i] = i;
			}
		}
		return ci;
	}

	void requestFile(String name, int i) {
		if (hasReferenceTable()) {
			name = name.toLowerCase();
			int r = referenceTable.entryIdentTable.lookup(tnHash(name));
			requestFile(r);
		}
	}

	private boolean hasEntryBuffer(String string, String string_23_, byte i) {
		if (!hasReferenceTable()) {
			return false;
		}
		string_23_ = string_23_.toLowerCase();
		string = string.toLowerCase();
		int i_24_ = referenceTable.entryIdentTable.lookup(tnHash(string_23_));
		if (!validEntryIndex(i_24_)) {
			return false;
		}
		int i_25_ = referenceTable.childIdentTables[i_24_].lookup(tnHash(string));
		return fileExists(i_24_, i_25_);
	}

	private boolean validEntryIndex(int i_26_) {
		if (!hasReferenceTable()) {
			return false;
		}
		if (i_26_ < 0 || i_26_ >= referenceTable.childIndexCounts.length || referenceTable.childIndexCounts[i_26_] == 0) {
			if (ModeWhere.aBoolean2351) {
				throw new IllegalArgumentException(Integer.toString(i_26_));
			}
			return false;
		}
		return true;
	}

	private void requestFile(int i) {
		worker.requestFile(-1 + 1, i);
	}

	int getTotalCompletion(int i) {
		if (!hasReferenceTable()) {
			return 0;
		}
		int total = 0;
		int completed = i;
		for (int i_31_ = 0; i_31_ < entryBuffers.length; i_31_++) {
			if (referenceTable.entryChildCounts[i_31_] > 0) {
				total += 100;
				completed += getFileCompletion(i_31_);
			}
		}
		if (total == 0) {
			return 100;
		}
		return completed * 100 / total;
	}

	int getFileIndex(int i_33_) {
		if (!hasReferenceTable()) {
			return -1;
		}
		int indx = referenceTable.entryIdentTable.lookup(i_33_);
		if (!validEntryIndex(indx)) {
			return -1;
		}
		return indx;
	}

	int getFileIndex(String name) {
		if (!hasReferenceTable()) {
			return -1;
		}
		name = name.toLowerCase();
		int i_35_ = referenceTable.entryIdentTable.lookup(tnHash(name));
		if (!validEntryIndex(i_35_)) {
			return -1;
		}
		return i_35_;
	}

	public int getLastFileId(int file) {
		if (!validEntryIndex(file)) {
			return 0;
		}
		return referenceTable.childIndexCounts[file];
	}

	void clearEntryBuffers_() {
		if (entryBuffers != null) {
			for (int i_37_ = 0; entryBuffers.length > i_37_; i_37_++) {
				entryBuffers[i_37_] = null;
			}
		}
	}

	int getFileCompletion_(String string) {
		if (!hasReferenceTable()) {
			return 0;
		}
		string = string.toLowerCase();
		int i_39_ = referenceTable.entryIdentTable.lookup(tnHash(string));
		return getFileCompletion(i_39_);
	}

	byte[] get(int fileID) {
		if (!hasReferenceTable()) {
			return null;
		}
		if (referenceTable.childIndexCounts.length == 1) {
			return getFile(0, fileID);
		}
		if (!validEntryIndex(fileID)) {
			return null;
		}
		if (referenceTable.childIndexCounts[fileID] == 1) {
			return getFile(fileID, 0);
		}
		throw new RuntimeException();
	}

	static void method1454(int i, int i_42_, int i_43_, int i_44_, int i_45_) {
		Class188.anInt1927 = i_44_;
		anInt1416 = i_42_;
		NPCNode.anInt4625 = i_43_;
		int i_46_ = -6 % ((i - 64) / 56);
		Class360_Sub8.anInt5340 = i_45_;
	}

	void clearChildBuffers(int i, boolean bool) {
		if (!bool && validEntryIndex(i)) {
			if (childBuffers != null) {
				childBuffers[i] = null;
			}
		}
	}

	public boolean fileExists(int i_47_, int i_48_) {
		if (!validIndices(i_48_, i_47_, (byte) -105)) {
			return false;
		}
		if (childBuffers[i_47_] != null && childBuffers[i_47_][i_48_] != null) {
			return true;
		}
		if (entryBuffers[i_47_] != null) {
			return true;
		}
		loadBuffer(i_47_, 0);
		if (entryBuffers[i_47_] != null) {
			return true;
		}
		return false;
	}

	public int getLastGroupId() {
		if (!hasReferenceTable()) {
			return -1;
		}
		return referenceTable.childIndexCounts.length;
	}

	private boolean prepareChildBuffers(int[] keys, int childID, byte i_49_, int fileID) {
		if (!validEntryIndex(fileID)) {
			return false;
		}
		if (entryBuffers[fileID] == null) {
			return false;
		}
		int childCount = referenceTable.entryChildCounts[fileID];
		int[] childIndices = referenceTable.childIndices[fileID];
		if (childBuffers[fileID] == null) {
			childBuffers[fileID] = new Object[referenceTable.childIndexCounts[fileID]];
		}
		Object[] buffers = childBuffers[fileID];
		boolean prepared = true;
		for (int i_53_ = 0; i_53_ < childCount; i_53_++) {
			int i_54_;
			if (childIndices != null) {
				i_54_ = childIndices[i_53_];
			} else {
				i_54_ = i_53_;
			}
			if (buffers[i_54_] == null) {
				prepared = false;
				break;
			}
		}
		if (prepared) {
			return true;
		}
		byte[] unwrapped;
		if (keys == null || keys[0] == 0 && keys[1] == 0 && keys[2] == 0 && keys[3] == 0) {
			unwrapped = Class325.unwrapBuffer(false, -1, entryBuffers[fileID]);
		} else {
			unwrapped = Class325.unwrapBuffer(true, -1, entryBuffers[fileID]);
			Packet buff = new Packet(unwrapped);
			buff.decryptXTEA(5, buff.data.length, keys);
		}
		byte[] unpacked;
		try {
			unpacked = Class296_Sub51_Sub9.unpackContainer(false, unwrapped);
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "T3 - " + (keys != null) + "," + fileID + "," + unwrapped.length + "," + Class277.method2328(-120, unwrapped, unwrapped.length) + "," + Class277.method2328(-85, unwrapped, unwrapped.length - 2) + "," + referenceTable.entryCRCS[fileID] + "," + referenceTable.crc);
		}
		if (discardEntryBuffers_) {
			entryBuffers[fileID] = null;
		}
		if (childCount > 1) {
			if (discardUnpacked_ == 2) {
				int i_57_ = unpacked.length;
				int i_58_ = unpacked[--i_57_] & 0xff;
				i_57_ -= i_58_ * childCount * 4;
				Packet class296_sub17 = new Packet(unpacked);
				int i_59_ = 0;
				class296_sub17.pos = i_57_;
				int i_60_ = 0;
				for (int i_61_ = 0; i_58_ > i_61_; i_61_++) {
					int i_62_ = 0;
					for (int i_63_ = 0; i_63_ < childCount; i_63_++) {
						i_62_ += class296_sub17.g4();
						int i_64_;
						if (childIndices != null) {
							i_64_ = childIndices[i_63_];
						} else {
							i_64_ = i_63_;
						}
						if (childID == i_64_) {
							i_60_ = i_64_;
							i_59_ += i_62_;
						}
					}
				}
				if (i_59_ == 0) {
					return true;
				}
				byte[] is_65_ = new byte[i_59_];
				class296_sub17.pos = i_57_;
				i_59_ = 0;
				int i_66_ = 0;
				for (int i_67_ = 0; i_58_ > i_67_; i_67_++) {
					int i_68_ = 0;
					for (int i_69_ = 0; childCount > i_69_; i_69_++) {
						i_68_ += class296_sub17.g4();
						int i_70_;
						if (childIndices != null) {
							i_70_ = childIndices[i_69_];
						} else {
							i_70_ = i_69_;
						}
						if (childID == i_70_) {
							ArrayTools.removeElement(unpacked, i_66_, is_65_, i_59_, i_68_);
							i_59_ += i_68_;
						}
						i_66_ += i_68_;
					}
				}
				buffers[i_60_] = is_65_;
			} else {
				int i_71_ = unpacked.length;
				int i_72_ = unpacked[--i_71_] & 0xff;
				i_71_ -= i_72_ * childCount * 4;
				Packet class296_sub17 = new Packet(unpacked);
				int[] is_73_ = new int[childCount];
				class296_sub17.pos = i_71_;
				for (int i_74_ = 0; i_72_ > i_74_; i_74_++) {
					int i_75_ = 0;
					for (int i_76_ = 0; childCount > i_76_; i_76_++) {
						i_75_ += class296_sub17.g4();
						is_73_[i_76_] += i_75_;
					}
				}
				byte[][] is_77_ = new byte[childCount][];
				for (int i_78_ = 0; i_78_ < childCount; i_78_++) {
					is_77_[i_78_] = new byte[is_73_[i_78_]];
					is_73_[i_78_] = 0;
				}
				class296_sub17.pos = i_71_;
				int i_79_ = 0;
				for (int i_80_ = 0; i_80_ < i_72_; i_80_++) {
					int i_81_ = 0;
					for (int i_82_ = 0; childCount > i_82_; i_82_++) {
						i_81_ += class296_sub17.g4();
						ArrayTools.removeElement(unpacked, i_79_, is_77_[i_82_], is_73_[i_82_], i_81_);
						is_73_[i_82_] += i_81_;
						i_79_ += i_81_;
					}
				}
				for (int i_83_ = 0; childCount > i_83_; i_83_++) {
					int i_84_;
					if (childIndices != null) {
						i_84_ = childIndices[i_83_];
					} else {
						i_84_ = i_83_;
					}
					if (discardUnpacked_ != 0) {
						buffers[i_84_] = is_77_[i_83_];
					} else {
						buffers[i_84_] = Class166.wrapBuffer(false, -87, is_77_[i_83_]);
					}
				}
			}
		} else {
			int i_85_;
			if (childIndices == null) {
				i_85_ = 0;
			} else {
				i_85_ = childIndices[0];
			}
			if (discardUnpacked_ == 0) {
				buffers[i_85_] = Class166.wrapBuffer(false, 91, unpacked);
			} else {
				buffers[i_85_] = unpacked;
			}
		}
		if (i_49_ > -91) {
			return true;
		}
		return true;
	}

	private boolean validIndices(int i, int i_86_, byte i_87_) {
		if (!hasReferenceTable()) {
			return false;
		}
		if (i_87_ != -105) {
			tnHash(null);
		}
		if (i_86_ < 0 || i < 0 || referenceTable.childIndexCounts.length <= i_86_ || i >= referenceTable.childIndexCounts[i_86_]) {
			if (ModeWhere.aBoolean2351) {
				throw new IllegalArgumentException(String.valueOf(i_86_) + "," + i);
			}
			return false;
		}
		return true;
	}

	boolean hasFile(String string, String string_88_) {
		if (!hasReferenceTable()) {
			return false;
		}
		string_88_ = string_88_.toLowerCase();
		string = string.toLowerCase();
		int i_89_ = referenceTable.entryIdentTable.lookup(tnHash(string_88_));
		if (i_89_ < 0) {
			return false;
		}
		int i_90_ = referenceTable.childIdentTables[i_89_].lookup(tnHash(string));
		if (i_90_ < 0) {
			return false;
		}
		return true;
	}

	boolean hasEntryBuffer(int i) {
		if (!hasReferenceTable()) {
			return false;
		}
		if (referenceTable.childIndexCounts.length == 1) {
			return fileExists(0, i);
		}
		if (!validEntryIndex(i)) {
			return false;
		}
		if (referenceTable.childIndexCounts[i] == 1) {
			return fileExists(i, 0);
		}
		throw new RuntimeException();
	}

	boolean hasFileBuffer(byte i, int i_92_) {
		if (i >= -57) {
			return true;
		}
		if (!validEntryIndex(i_92_)) {
			return false;
		}
		if (entryBuffers[i_92_] != null) {
			return true;
		}
		loadBuffer(i_92_, 0);
		if (entryBuffers[i_92_] != null) {
			return true;
		}
		return false;
	}

	public Js5(FileWorker class343, boolean discardEntryBuffers, int discardUnpacked) {
		if (discardUnpacked < 0 || discardUnpacked > 2) {
			throw new IllegalArgumentException("js5: Invalid value " + discardUnpacked + " supplied for discardunpacked");
		}
		discardUnpacked_ = discardUnpacked;
		discardEntryBuffers_ = discardEntryBuffers;
		worker = class343;
	}

	static void method1463(int i) {
		aClass278_1418 = null;
		aClass125_1411 = null;
		if (i != 0) {
			anInt1416 = 71;
		}
	}
}
