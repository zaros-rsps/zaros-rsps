package net.zaros.client;

/* Class400 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class400 {
	private long aLong3358;
	private Queuable aClass296_Sub39_3359;
	private Queuable[] aClass296_Sub39Array3360;
	private int anInt3361;

	final Queuable method4135(boolean bool) {
		if (aClass296_Sub39_3359 == null)
			return null;
		Queuable class296_sub39 = (aClass296_Sub39Array3360[(int) ((long) (anInt3361 - 1) & aLong3358)]);
		if (bool != true)
			method4135(true);
		for (/**/; class296_sub39 != aClass296_Sub39_3359; aClass296_Sub39_3359 = aClass296_Sub39_3359.queue_next) {
			if (aLong3358 == aClass296_Sub39_3359.queue_uid) {
				Queuable class296_sub39_0_ = aClass296_Sub39_3359;
				aClass296_Sub39_3359 = aClass296_Sub39_3359.queue_next;
				return class296_sub39_0_;
			}
		}
		aClass296_Sub39_3359 = null;
		return null;
	}

	final void method4136(Queuable class296_sub39, byte i, long l) {
		if (i <= 118)
			method4136(null, (byte) 72, -41L);
		if (class296_sub39.queue_previous != null)
			class296_sub39.queue_unlink();
		Queuable class296_sub39_1_ = aClass296_Sub39Array3360[(int) ((long) (anInt3361 - 1) & l)];
		class296_sub39.queue_next = class296_sub39_1_;
		class296_sub39.queue_previous = class296_sub39_1_.queue_previous;
		class296_sub39.queue_previous.queue_next = class296_sub39;
		class296_sub39.queue_uid = l;
		class296_sub39.queue_next.queue_previous = class296_sub39;
	}

	final Queuable method4137(int i, long l) {
		aLong3358 = l;
		int i_2_ = 124 / ((-50 - i) / 57);
		Queuable class296_sub39 = aClass296_Sub39Array3360[(int) ((long) (anInt3361 - 1) & l)];
		for (aClass296_Sub39_3359 = class296_sub39.queue_next; aClass296_Sub39_3359 != class296_sub39; aClass296_Sub39_3359 = aClass296_Sub39_3359.queue_next) {
			if (aClass296_Sub39_3359.queue_uid == l) {
				Queuable class296_sub39_3_ = aClass296_Sub39_3359;
				aClass296_Sub39_3359 = aClass296_Sub39_3359.queue_next;
				return class296_sub39_3_;
			}
		}
		aClass296_Sub39_3359 = null;
		return null;
	}

	Class400(int i) {
		aClass296_Sub39Array3360 = new Queuable[i];
		anInt3361 = i;
		for (int i_4_ = 0; i > i_4_; i_4_++) {
			Queuable class296_sub39 = aClass296_Sub39Array3360[i_4_] = new Queuable();
			class296_sub39.queue_next = class296_sub39;
			class296_sub39.queue_previous = class296_sub39;
		}
	}
}
