package net.zaros.client;

/* s_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;
import java.lang.reflect.Constructor;

import jaclib.memory.Stream;

import jaggl.OpenGL;

public class s_Sub2 extends s {
	short[][] aShortArrayArray5111;
	private int anInt5112;
	private int anInt5113;
	private int[][][] anIntArrayArrayArray5114;
	private NodeDeque aClass155_5115 = new NodeDeque();
	int[][][] anIntArrayArrayArray5116;
	int[][][] anIntArrayArrayArray5117;
	private int anInt5118;
	private int[][][] anIntArrayArrayArray5119;
	private Class296_Sub32[][][] aClass296_Sub32ArrayArrayArray5120;
	private int anInt5121 = anInt2835 - 2;
	int anInt5122;
	private byte[][] aByteArrayArray5123;
	private int[][][] anIntArrayArrayArray5124;
	private Class207 aClass207_5125;
	ha_Sub3 aHa_Sub3_5126;
	int[][][] anIntArrayArrayArray5127;
	private byte[][] aByteArrayArray5128;
	private Class272 aClass272_5129;
	Class272 aClass272_5130;
	private float[][] aFloatArrayArray5131;
	private Class296_Sub32[] aClass296_Sub32Array5132;
	Class272 aClass272_5133;
	private Interface5 anInterface5_5134;
	private HashTable aClass263_5135;
	private float[][] aFloatArrayArray5136;
	private float[][] aFloatArrayArray5137;
	private int anInt5138;
	Class272 aClass272_5139;
	private int anInt5140;

	public void method3356(int i, int i_0_, int[] is, int[] is_1_, int[] is_2_, int[] is_3_, int[] is_4_, int[] is_5_,
			int[] is_6_, int[] is_7_, int[] is_8_, int[] is_9_, int[] is_10_, int i_11_, int i_12_, int i_13_,
			boolean bool) {
		int i_14_ = is_7_.length;
		int[] is_15_ = new int[i_14_ * 3];
		int[] is_16_ = new int[i_14_ * 3];
		int[] is_17_ = new int[i_14_ * 3];
		int[] is_18_ = new int[i_14_ * 3];
		int[] is_19_ = new int[i_14_ * 3];
		int[] is_20_ = new int[i_14_ * 3];
		int[] is_21_ = is_1_ == null ? null : new int[i_14_ * 3];
		int[] is_22_ = is_3_ != null ? new int[i_14_ * 3] : null;
		int i_23_ = 0;
		for (int i_24_ = 0; i_24_ < i_14_; i_24_++) {
			int i_25_ = is_4_[i_24_];
			int i_26_ = is_5_[i_24_];
			int i_27_ = is_6_[i_24_];
			is_15_[i_23_] = is[i_25_];
			is_16_[i_23_] = is_2_[i_25_];
			is_17_[i_23_] = is_7_[i_24_];
			is_19_[i_23_] = is_9_[i_24_];
			is_20_[i_23_] = is_10_[i_24_];
			is_18_[i_23_] = is_8_ == null ? is_7_[i_24_] : is_8_[i_24_];
			if (is_1_ != null)
				is_21_[i_23_] = is_1_[i_25_];
			if (is_3_ != null)
				is_22_[i_23_] = is_3_[i_25_];
			i_23_++;
			is_15_[i_23_] = is[i_26_];
			is_16_[i_23_] = is_2_[i_26_];
			is_17_[i_23_] = is_7_[i_24_];
			is_19_[i_23_] = is_9_[i_24_];
			is_20_[i_23_] = is_10_[i_24_];
			is_18_[i_23_] = is_8_ == null ? is_7_[i_24_] : is_8_[i_24_];
			if (is_1_ != null)
				is_21_[i_23_] = is_1_[i_26_];
			if (is_3_ != null)
				is_22_[i_23_] = is_3_[i_26_];
			i_23_++;
			is_15_[i_23_] = is[i_27_];
			is_16_[i_23_] = is_2_[i_27_];
			is_17_[i_23_] = is_7_[i_24_];
			is_19_[i_23_] = is_9_[i_24_];
			is_20_[i_23_] = is_10_[i_24_];
			is_18_[i_23_] = is_8_ != null ? is_8_[i_24_] : is_7_[i_24_];
			if (is_1_ != null)
				is_21_[i_23_] = is_1_[i_27_];
			if (is_3_ != null)
				is_22_[i_23_] = is_3_[i_27_];
			i_23_++;
		}
		U(i, i_0_, is_15_, is_21_, is_16_, is_22_, is_17_, is_18_, is_19_, is_20_, i_11_, i_12_, i_13_, bool);
	}

	public void ka(int i, int i_28_, int i_29_) {
		if (i_29_ > (aByteArrayArray5128[i][i_28_] & 0xff))
			aByteArrayArray5128[i][i_28_] = (byte) i_29_;
	}

	public void YA() {
		if (anInt5140 <= 0)
			aClass207_5125 = null;
		else {
			byte[][] is = new byte[anInt2832 + 1][anInt2834 + 1];
			for (int i = 1; i < anInt2832; i++) {
				for (int i_30_ = 1; i_30_ < anInt2834; i_30_++)
					is[i][i_30_] = (byte) ((aByteArrayArray5128[i][i_30_ + 1] >> 3)
							+ (aByteArrayArray5128[i - 1][i_30_] >> 2) + (aByteArrayArray5128[i + 1][i_30_] >> 3)
							+ ((aByteArrayArray5128[i][i_30_ - 1] >> 2) + (aByteArrayArray5128[i][i_30_] >> 1)));
			}
			aClass296_Sub32Array5132 = new Class296_Sub32[aClass263_5135.size((byte) -4)];
			aClass263_5135.toArray(5125, aClass296_Sub32Array5132);
			for (int i = 0; i < aClass296_Sub32Array5132.length; i++)
				aClass296_Sub32Array5132[i].method2713(anInt5140);
			int i = 24;
			if (anIntArrayArrayArray5114 != null)
				i += 4;
			if ((anInt5122 & 0x7) != 0)
				i += 12;
			jaclib.memory.heap.NativeHeapBuffer nativeheapbuffer = aHa_Sub3_5126.aNativeHeap4138.a(anInt5140 * i,
					false);
			Stream stream = new Stream(nativeheapbuffer);
			Class296_Sub32[] class296_sub32s = new Class296_Sub32[anInt5140];
			int i_31_ = Statics.method629(false, anInt5140 / 4);
			if (i_31_ < 1)
				i_31_ = 1;
			HashTable class263 = new HashTable(i_31_);
			Class296_Sub32[] class296_sub32s_32_ = new Class296_Sub32[anInt5138];
			for (int i_33_ = 0; i_33_ < anInt2832; i_33_++) {
				for (int i_34_ = 0; anInt2834 > i_34_; i_34_++) {
					if (anIntArrayArrayArray5117[i_33_][i_34_] != null) {
						Class296_Sub32[] class296_sub32s_35_ = aClass296_Sub32ArrayArrayArray5120[i_33_][i_34_];
						int[] is_36_ = anIntArrayArrayArray5116[i_33_][i_34_];
						int[] is_37_ = anIntArrayArrayArray5127[i_33_][i_34_];
						int[] is_38_ = anIntArrayArrayArray5124[i_33_][i_34_];
						int[] is_39_ = anIntArrayArrayArray5117[i_33_][i_34_];
						int[] is_40_ = (anIntArrayArrayArray5119 == null ? null
								: anIntArrayArrayArray5119[i_33_][i_34_]);
						int[] is_41_ = (anIntArrayArrayArray5114 == null ? null
								: anIntArrayArrayArray5114[i_33_][i_34_]);
						if (is_38_ == null)
							is_38_ = is_39_;
						float f = aFloatArrayArray5137[i_33_][i_34_];
						float f_42_ = aFloatArrayArray5131[i_33_][i_34_];
						float f_43_ = aFloatArrayArray5136[i_33_][i_34_];
						float f_44_ = aFloatArrayArray5137[i_33_][i_34_ + 1];
						float f_45_ = aFloatArrayArray5131[i_33_][i_34_ + 1];
						float f_46_ = aFloatArrayArray5136[i_33_][i_34_ + 1];
						float f_47_ = aFloatArrayArray5137[i_33_ + 1][i_34_ + 1];
						float f_48_ = aFloatArrayArray5131[i_33_ + 1][i_34_ + 1];
						float f_49_ = aFloatArrayArray5136[i_33_ + 1][i_34_ + 1];
						float f_50_ = aFloatArrayArray5137[i_33_ + 1][i_34_];
						float f_51_ = aFloatArrayArray5131[i_33_ + 1][i_34_];
						float f_52_ = aFloatArrayArray5136[i_33_ + 1][i_34_];
						int i_53_ = is[i_33_][i_34_] & 0xff;
						int i_54_ = is[i_33_][i_34_ + 1] & 0xff;
						int i_55_ = is[i_33_ + 1][i_34_ + 1] & 0xff;
						int i_56_ = is[i_33_ + 1][i_34_] & 0xff;
						int i_57_ = 0;
						while_250_: for (int i_58_ = 0; i_58_ < is_39_.length; i_58_++) {
							Class296_Sub32 class296_sub32 = class296_sub32s_35_[i_58_];
							for (int i_59_ = 0; i_59_ < i_57_; i_59_++) {
								if (class296_sub32 == class296_sub32s_32_[i_59_])
									continue while_250_;
							}
							class296_sub32s_32_[i_57_++] = class296_sub32;
						}
						short[] is_60_ = (aShortArrayArray5111[i_33_ + anInt2832 * i_34_] = new short[is_39_.length]);
						for (int i_61_ = 0; i_61_ < is_39_.length; i_61_++) {
							int i_62_ = (i_33_ << anInt2835) + is_36_[i_61_];
							int i_63_ = (i_34_ << anInt2835) + is_37_[i_61_];
							int i_64_ = i_62_ >> anInt5121;
							int i_65_ = i_63_ >> anInt5121;
							int i_66_ = is_39_[i_61_];
							int i_67_ = is_38_[i_61_];
							int i_68_ = is_40_ != null ? is_40_[i_61_] : 0;
							long l = (long) i_65_ | ((long) i_67_ << 48 | (long) i_66_ << 32 | (long) (i_64_ << 16));
							int i_69_ = is_36_[i_61_];
							int i_70_ = is_37_[i_61_];
							int i_71_ = 74;
							int i_72_ = 0;
							float f_73_;
							float f_74_;
							float f_75_;
							if (i_69_ != 0 || i_70_ != 0) {
								if (i_69_ == 0 && anInt2836 == i_70_) {
									i_71_ -= i_54_;
									f_73_ = f_46_;
									f_75_ = f_45_;
									f_74_ = f_44_;
								} else if (i_69_ != anInt2836 || anInt2836 != i_70_) {
									if (anInt2836 == i_69_ && i_70_ == 0) {
										f_74_ = f_50_;
										i_71_ -= i_56_;
										f_73_ = f_52_;
										f_75_ = f_51_;
									} else {
										float f_76_ = ((float) i_69_ / (float) anInt2836);
										float f_77_ = ((float) i_70_ / (float) anInt2836);
										float f_78_ = (-f + f_50_) * f_76_ + f;
										float f_79_ = f_76_ * (-f_42_ + f_51_) + f_42_;
										float f_80_ = (f_52_ - f_43_) * f_76_ + f_43_;
										float f_81_ = (f_47_ - f_44_) * f_76_ + f_44_;
										float f_82_ = f_45_ + f_76_ * (-f_45_ + f_48_);
										f_75_ = f_77_ * (f_82_ - f_79_) + f_79_;
										f_74_ = f_78_ + f_77_ * (f_81_ - f_78_);
										float f_83_ = (-f_46_ + f_49_) * f_76_ + f_46_;
										f_73_ = f_80_ + f_77_ * (f_83_ - f_80_);
										int i_84_ = i_53_ + ((-i_53_ + i_56_) * i_69_ >> anInt2835);
										int i_85_ = i_54_ + (i_69_ * (-i_54_ + i_55_) >> anInt2835);
										i_71_ -= i_84_ + ((i_85_ - i_84_) * i_70_ >> anInt2835);
									}
								} else {
									f_75_ = f_48_;
									f_73_ = f_49_;
									i_71_ -= i_55_;
									f_74_ = f_47_;
								}
							} else {
								f_73_ = f_43_;
								f_74_ = f;
								i_71_ -= i_53_;
								f_75_ = f_42_;
							}
							float f_86_ = 1.0F;
							if (i_66_ != -1) {
								int i_87_ = (i_66_ & 0x7f) * i_71_ >> 7;
								if (i_87_ < 2)
									i_87_ = 2;
								else if (i_87_ > 126)
									i_87_ = 126;
								if ((anInt5122 & 0x7) == 0) {
									f_86_ = ((f_73_ * aHa_Sub3_5126.aFloatArray4202[2])
											+ ((aHa_Sub3_5126.aFloatArray4202[0] * f_74_)
													+ (aHa_Sub3_5126.aFloatArray4202[1]) * f_75_));
									f_86_ = (aHa_Sub3_5126.aFloat4217 + (f_86_
											* (!(f_86_ > 0.0F) ? aHa_Sub3_5126.aFloat4248 : aHa_Sub3_5126.aFloat4272)));
								}
								i_72_ = (Class166_Sub1.anIntArray4300[i_87_ | i_66_ & 0xff80]);
							}
							Node class296 = null;
							if ((i_62_ & anInt5118 - 1) == 0 && (i_63_ & anInt5118 - 1) == 0)
								class296 = class263.get(l);
							int i_88_;
							if (class296 != null) {
								is_60_[i_61_] = ((Class296_Sub20) class296).aShort4715;
								i_88_ = is_60_[i_61_] & 0xffff;
								if (i_66_ != -1 && (class296_sub32s[i_88_].uid > (class296_sub32s_35_[i_61_].uid)))
									class296_sub32s[i_88_] = class296_sub32s_35_[i_61_];
							} else {
								int i_89_;
								if (i_67_ != i_66_) {
									int i_90_ = i_71_ * (i_67_ & 0x7f) >> 7;
									if (i_90_ < 2)
										i_90_ = 2;
									else if (i_90_ > 126)
										i_90_ = 126;
									i_89_ = (Class166_Sub1.anIntArray4300[i_90_ | i_67_ & 0xff80]);
									if ((anInt5122 & 0x7) == 0) {
										float f_91_ = ((aHa_Sub3_5126.aFloatArray4202[2]) * f_73_
												+ (f_75_ * (aHa_Sub3_5126.aFloatArray4202[1])
														+ ((aHa_Sub3_5126.aFloatArray4202[0]) * f_74_)));
										f_91_ = (aHa_Sub3_5126.aFloat4217 + ((!(f_86_ > 0.0F) ? aHa_Sub3_5126.aFloat4248
												: aHa_Sub3_5126.aFloat4272) * f_86_));
										int i_92_ = i_89_ >> 16 & 0xff;
										int i_93_ = i_89_ >> 8 & 0xff;
										i_92_ *= f_91_;
										int i_94_ = i_89_ & 0xff;
										i_93_ *= f_91_;
										if (i_92_ >= 0) {
											if (i_92_ > 255)
												i_92_ = 255;
										} else
											i_92_ = 0;
										if (i_93_ >= 0) {
											if (i_93_ > 255)
												i_93_ = 255;
										} else
											i_93_ = 0;
										i_94_ *= f_91_;
										if (i_94_ >= 0) {
											if (i_94_ > 255)
												i_94_ = 255;
										} else
											i_94_ = 0;
										i_89_ = i_93_ << 8 | i_92_ << 16 | i_94_;
									}
								} else
									i_89_ = i_72_;
								if (!aHa_Sub3_5126.aBoolean4184) {
									stream.b((float) i_62_);
									stream.b((float) (i_68_ + this.method3349(0, i_63_, i_62_)));
									stream.b((float) i_63_);
									stream.e((byte) (i_89_ >> 16));
									stream.e((byte) (i_89_ >> 8));
									stream.e((byte) i_89_);
									stream.e(-1);
									stream.b((float) i_62_);
									stream.b((float) i_63_);
									if (anIntArrayArrayArray5114 != null)
										stream.b((float) (is_41_ == null ? 0 : (is_41_[i_61_] - 1)));
									if ((anInt5122 & 0x7) != 0) {
										stream.b(f_74_);
										stream.b(f_75_);
										stream.b(f_73_);
									}
								} else {
									stream.a((float) i_62_);
									stream.a((float) (this.method3349(0, i_63_, i_62_) + i_68_));
									stream.a((float) i_63_);
									stream.e((byte) (i_89_ >> 16));
									stream.e((byte) (i_89_ >> 8));
									stream.e((byte) i_89_);
									stream.e(-1);
									stream.a((float) i_62_);
									stream.a((float) i_63_);
									if (anIntArrayArrayArray5114 != null)
										stream.a((float) (is_41_ == null ? 0 : (is_41_[i_61_] - 1)));
									if ((anInt5122 & 0x7) != 0) {
										stream.a(f_74_);
										stream.a(f_75_);
										stream.a(f_73_);
									}
								}
								i_88_ = anInt5112++;
								is_60_[i_61_] = (short) i_88_;
								if (i_66_ != -1)
									class296_sub32s[i_88_] = class296_sub32s_35_[i_61_];
								class263.put(l, new Class296_Sub20(is_60_[i_61_]));
							}
							for (int i_95_ = 0; i_95_ < i_57_; i_95_++)
								class296_sub32s_32_[i_95_].method2716(i_71_, -16022, f_86_, i_72_, i_88_);
							anInt5113++;
						}
					}
				}
			}
			for (int i_96_ = 0; anInt5112 > i_96_; i_96_++) {
				Class296_Sub32 class296_sub32 = class296_sub32s[i_96_];
				if (class296_sub32 != null)
					class296_sub32.method2710(i_96_, -28);
			}
			for (int i_97_ = 0; i_97_ < anInt2832; i_97_++) {
				for (int i_98_ = 0; i_98_ < anInt2834; i_98_++) {
					short[] is_99_ = aShortArrayArray5111[i_97_ + anInt2832 * i_98_];
					if (is_99_ != null) {
						int i_100_ = 0;
						int i_101_ = 0;
						while (is_99_.length > i_101_) {
							int i_102_ = is_99_[i_101_++] & 0xffff;
							int i_103_ = is_99_[i_101_++] & 0xffff;
							int i_104_ = is_99_[i_101_++] & 0xffff;
							Class296_Sub32 class296_sub32 = class296_sub32s[i_102_];
							Class296_Sub32 class296_sub32_105_ = class296_sub32s[i_103_];
							Class296_Sub32 class296_sub32_106_ = class296_sub32s[i_104_];
							Class296_Sub32 class296_sub32_107_ = null;
							if (class296_sub32 != null) {
								class296_sub32.method2711((byte) -103, i_100_, i_98_, i_97_);
								class296_sub32_107_ = class296_sub32;
							}
							if (class296_sub32_105_ != null) {
								class296_sub32_105_.method2711((byte) -39, i_100_, i_98_, i_97_);
								if (class296_sub32_107_ == null || (class296_sub32_105_.uid < class296_sub32_107_.uid))
									class296_sub32_107_ = class296_sub32_105_;
							}
							if (class296_sub32_106_ != null) {
								class296_sub32_106_.method2711((byte) -50, i_100_, i_98_, i_97_);
								if (class296_sub32_107_ == null || (class296_sub32_106_.uid < class296_sub32_107_.uid))
									class296_sub32_107_ = class296_sub32_106_;
							}
							if (class296_sub32_107_ != null) {
								if (class296_sub32 != null)
									class296_sub32_107_.method2710(i_102_, -67);
								if (class296_sub32_105_ != null)
									class296_sub32_107_.method2710(i_103_, -121);
								if (class296_sub32_106_ != null)
									class296_sub32_107_.method2710(i_104_, -43);
								class296_sub32_107_.method2711((byte) -112, i_100_, i_98_, i_97_);
							}
							i_100_++;
						}
					}
				}
			}
			stream.b();
			anInterface5_5134 = aHa_Sub3_5126.method1347(-1, nativeheapbuffer, false, i, stream.c());
			aClass272_5139 = new Class272(anInterface5_5134, 5126, 3, 0);
			aClass272_5129 = new Class272(anInterface5_5134, 5121, 4, 12);
			int i_108_;
			if (anIntArrayArrayArray5114 != null) {
				i_108_ = 28;
				aClass272_5130 = new Class272(anInterface5_5134, 5126, 3, 16);
			} else {
				aClass272_5130 = new Class272(anInterface5_5134, 5126, 2, 16);
				i_108_ = 24;
			}
			if ((anInt5122 & 0x7) != 0)
				aClass272_5133 = new Class272(anInterface5_5134, 5126, 3, i_108_);
			long[] ls = new long[aClass296_Sub32Array5132.length];
			for (int i_109_ = 0; i_109_ < aClass296_Sub32Array5132.length; i_109_++) {
				Class296_Sub32 class296_sub32 = aClass296_Sub32Array5132[i_109_];
				ls[i_109_] = class296_sub32.uid;
				class296_sub32.method2717(anInt5112, 4);
			}
			Class308.method3289(-1791647608, aClass296_Sub32Array5132, ls);
			if (aClass207_5125 != null)
				aClass207_5125.method1999(3418);
		}
		aClass263_5135 = null;
		anIntArrayArrayArray5114 = null;
		anIntArrayArrayArray5116 = anIntArrayArrayArray5127 = null;
		aClass296_Sub32ArrayArrayArray5120 = null;
		aFloatArrayArray5137 = aFloatArrayArray5131 = aFloatArrayArray5136 = null;
		aByteArrayArray5128 = null;
		anIntArrayArrayArray5124 = null;
		anIntArrayArrayArray5119 = null;
		anIntArrayArrayArray5117 = null;
	}

	static public Class189 method3367(boolean bool, Component component, byte i) {
		try {
			return new Class189_Sub2(component, bool);
		} catch (Throwable throwable) {
			return new Class189_Sub1(component, bool);
		}
	}

	public void CA(r var_r, int i, int i_110_, int i_111_, int i_112_, boolean bool) {
		if (aClass207_5125 != null && var_r != null) {
			int i_113_ = (-(i_110_ * aHa_Sub3_5126.anInt4212 >> 8) + i >> aHa_Sub3_5126.anInt4139);
			int i_114_ = (i_111_ - (i_110_ * aHa_Sub3_5126.anInt4179 >> 8) >> aHa_Sub3_5126.anInt4139);
			aClass207_5125.method1994(var_r, i_113_, -128, i_114_);
		}
	}

	public void method3348(int i, int i_115_, int i_116_, boolean[][] bools, boolean bool, int i_117_, int i_118_) {
		method3369(i_117_, bool, (byte) 99, i_115_, bools, i_116_, i, i_118_);
	}

	public void method3357(Class296_Sub35 class296_sub35, int[] is) {
		aClass155_5115.addLast((byte) -92, new Class296_Sub55(aHa_Sub3_5126, this, class296_sub35, is));
	}

	static public void method3368(int i, byte i_119_, int i_120_, Js5 class138, boolean bool,
			Class296_Sub45_Sub4 class296_sub45_sub4, int i_121_) {
		Class338_Sub8.method3602(i_120_, class138, bool, -122, i, i_121_);
		if (i_119_ >= 77)
			Class249.aClass296_Sub45_Sub4_2357 = class296_sub45_sub4;
	}

	public void method3354(int i, int i_122_, int i_123_, boolean[][] bools, boolean bool, int i_124_) {
		method3369(-1, bool, (byte) 118, i_122_, bools, i_123_, i, i_124_);
	}

	public void method3352(int i, int i_125_) {
		/* empty */
	}

	public void method3350(int i, int i_126_, int i_127_, int i_128_, int i_129_, int i_130_, int i_131_,
			boolean[][] bools) {
		if (anInt5140 > 0) {
			aHa_Sub3_5126.method1284(-31628);
			aHa_Sub3_5126.method1311(false, 8577);
			aHa_Sub3_5126.method1278(1553921480, false);
			aHa_Sub3_5126.method1345(false, 88);
			aHa_Sub3_5126.method1285(113, false);
			aHa_Sub3_5126.method1336((byte) -102, 0);
			aHa_Sub3_5126.method1340(-2, (byte) 111);
			aHa_Sub3_5126.method1316(null, (byte) -119);
			StaticMethods.aFloatArray3637[1] = 0.0F;
			StaticMethods.aFloatArray3637[0] = (float) i_127_
					/ ((float) aHa_Sub3_5126.anInt4114 * ((float) anInt2836 * 128.0F));
			StaticMethods.aFloatArray3637[12] = -1.0F
					- (((float) -(i * 2) + (float) (i_128_ * i_127_) / 128.0F) / (float) aHa_Sub3_5126.anInt4114);
			StaticMethods.aFloatArray3637[9] = 0.0F;
			StaticMethods.aFloatArray3637[4] = 0.0F;
			StaticMethods.aFloatArray3637[7] = 0.0F;
			StaticMethods.aFloatArray3637[14] = 0.0F;
			StaticMethods.aFloatArray3637[6] = 0.0F;
			StaticMethods.aFloatArray3637[8] = 0.0F;
			StaticMethods.aFloatArray3637[15] = 1.0F;
			StaticMethods.aFloatArray3637[11] = 0.0F;
			StaticMethods.aFloatArray3637[3] = 0.0F;
			StaticMethods.aFloatArray3637[10] = 0.0F;
			StaticMethods.aFloatArray3637[2] = 0.0F;
			StaticMethods.aFloatArray3637[13] = -(((float) (i_127_ * i_131_) / 128.0F + (float) (i_126_ * 2))
					/ (float) aHa_Sub3_5126.anInt4113) + 1.0F;
			StaticMethods.aFloatArray3637[5] = (float) i_127_
					/ ((float) anInt2836 * 128.0F * (float) aHa_Sub3_5126.anInt4113);
			OpenGL.glMatrixMode(5889);
			OpenGL.glLoadMatrixf(StaticMethods.aFloatArray3637, 0);
			StaticMethods.aFloatArray3637[12] = 0.0F;
			StaticMethods.aFloatArray3637[10] = 0.0F;
			StaticMethods.aFloatArray3637[13] = 0.0F;
			StaticMethods.aFloatArray3637[4] = 0.0F;
			StaticMethods.aFloatArray3637[6] = 1.0F;
			StaticMethods.aFloatArray3637[14] = 0.0F;
			StaticMethods.aFloatArray3637[1] = 0.0F;
			StaticMethods.aFloatArray3637[7] = 0.0F;
			StaticMethods.aFloatArray3637[3] = 0.0F;
			StaticMethods.aFloatArray3637[2] = 0.0F;
			StaticMethods.aFloatArray3637[0] = 1.0F;
			StaticMethods.aFloatArray3637[11] = 0.0F;
			StaticMethods.aFloatArray3637[9] = 1.0F;
			StaticMethods.aFloatArray3637[8] = 0.0F;
			StaticMethods.aFloatArray3637[5] = 0.0F;
			StaticMethods.aFloatArray3637[15] = 1.0F;
			OpenGL.glMatrixMode(5888);
			OpenGL.glLoadMatrixf(StaticMethods.aFloatArray3637, 0);
			if ((anInt5122 & 0x7) == 0)
				aHa_Sub3_5126.method1278(1553921480, false);
			else {
				aHa_Sub3_5126.method1278(1553921480, true);
				aHa_Sub3_5126.method1339(8705);
			}
			aHa_Sub3_5126.method1323(aClass272_5133, aClass272_5129, aClass272_5130, false, aClass272_5139);
			if (anInt5113 * 2 > (aHa_Sub3_5126.aClass296_Sub17_Sub2_4194.data).length)
				aHa_Sub3_5126.aClass296_Sub17_Sub2_4194 = new Class296_Sub17_Sub2(anInt5113 * 2);
			else
				aHa_Sub3_5126.aClass296_Sub17_Sub2_4194.pos = 0;
			int i_132_ = 0;
			Class296_Sub17_Sub2 class296_sub17_sub2 = aHa_Sub3_5126.aClass296_Sub17_Sub2_4194;
			if (aHa_Sub3_5126.aBoolean4184) {
				for (int i_133_ = i_129_; i_131_ > i_133_; i_133_++) {
					int i_134_ = i_128_ + anInt2832 * i_133_;
					for (int i_135_ = i_128_; i_135_ < i_130_; i_135_++) {
						if (bools[i_135_ - i_128_][i_133_ - i_129_]) {
							short[] is = aShortArrayArray5111[i_134_];
							if (is != null) {
								for (int i_136_ = 0; i_136_ < is.length; i_136_++) {
									i_132_++;
									class296_sub17_sub2.p2(is[i_136_] & 0xffff);
								}
							}
						}
						i_134_++;
					}
				}
			} else {
				for (int i_137_ = i_129_; i_131_ > i_137_; i_137_++) {
					int i_138_ = anInt2832 * i_137_ + i_128_;
					for (int i_139_ = i_128_; i_130_ > i_139_; i_139_++) {
						if (bools[-i_128_ + i_139_][i_137_ - i_129_]) {
							short[] is = aShortArrayArray5111[i_138_];
							if (is != null) {
								for (int i_140_ = 0; is.length > i_140_; i_140_++) {
									i_132_++;
									class296_sub17_sub2.method2590((is[i_140_] & 0xffff));
								}
							}
						}
						i_138_++;
					}
				}
			}
			if (i_132_ > 0) {
				Class295_Sub1 class295_sub1 = new Class295_Sub1(aHa_Sub3_5126, 5123, class296_sub17_sub2.data,
						class296_sub17_sub2.pos);
				aHa_Sub3_5126.method1348(i_132_, class295_sub1, 4, (byte) -17, 0);
			}
		}
	}

	private void method3369(int i, boolean bool, byte i_141_, int i_142_, boolean[][] bools, int i_143_, int i_144_,
			int i_145_) {
		if (aClass296_Sub32Array5132 != null) {
			int i_146_ = i_143_ + 1 + i_143_;
			i_146_ *= i_146_;
			if (aHa_Sub3_5126.anIntArray4280.length < i_146_)
				aHa_Sub3_5126.anIntArray4280 = new int[i_146_];
			if (anInt5113 * 2 > (aHa_Sub3_5126.aClass296_Sub17_Sub2_4194.data).length)
				aHa_Sub3_5126.aClass296_Sub17_Sub2_4194 = new Class296_Sub17_Sub2(anInt5113 * 2);
			int i_147_ = i_144_ - i_143_;
			int i_148_ = i_147_;
			if (i_147_ < 0)
				i_147_ = 0;
			int i_149_ = i_142_ - i_143_;
			int i_150_ = i_149_;
			if (i_149_ < 0)
				i_149_ = 0;
			int i_151_ = i_143_ + i_144_;
			if (i_151_ > anInt2832 - 1)
				i_151_ = anInt2832 - 1;
			int i_152_ = i_143_ + i_142_;
			if (anInt2834 - 1 < i_152_)
				i_152_ = anInt2834 - 1;
			int i_153_ = 0;
			int[] is = aHa_Sub3_5126.anIntArray4280;
			for (int i_154_ = i_147_; i_151_ >= i_154_; i_154_++) {
				boolean[] bools_155_ = bools[i_154_ - i_148_];
				for (int i_156_ = i_149_; i_152_ >= i_156_; i_156_++) {
					if (bools_155_[i_156_ - i_150_])
						is[i_153_++] = i_154_ + anInt2832 * i_156_;
				}
			}
			if (i != -1) {
				aHa_Sub3_5126.method1308(-98, (float) i);
				aHa_Sub3_5126.method1302(8448);
			} else
				aHa_Sub3_5126.method1315(1);
			aHa_Sub3_5126.method1278(1553921480, (anInt5122 & 0x7) != 0);
			for (int i_157_ = 0; aClass296_Sub32Array5132.length > i_157_; i_157_++)
				aClass296_Sub32Array5132[i_157_].method2718(is, 256, i_153_);
			if (!aClass155_5115.method1577((byte) -77)) {
				int i_158_ = aHa_Sub3_5126.anInt4250;
				int i_159_ = aHa_Sub3_5126.anInt4181;
				aHa_Sub3_5126.L(0, i_159_, aHa_Sub3_5126.anInt4223);
				aHa_Sub3_5126.method1278(1553921480, false);
				aHa_Sub3_5126.method1285(86, false);
				aHa_Sub3_5126.method1336((byte) 46, 128);
				aHa_Sub3_5126.method1340(-2, (byte) 48);
				aHa_Sub3_5126.method1316(aHa_Sub3_5126.aClass69_Sub1_4195, (byte) -104);
				aHa_Sub3_5126.method1306(8448, 7681, -22394);
				aHa_Sub3_5126.method1283(0, 34166, 770, (byte) -116);
				aHa_Sub3_5126.method1346(true, 770, 0, 34167);
				for (Node class296 = aClass155_5115.removeFirst((byte) 120); class296 != null; class296 = aClass155_5115
						.removeNext(1001)) {
					Class296_Sub55 class296_sub55 = (Class296_Sub55) class296;
					class296_sub55.method3215(i_142_, bools, i_144_, i_143_, -7732);
				}
				aHa_Sub3_5126.method1283(0, 5890, 768, (byte) -125);
				aHa_Sub3_5126.method1346(true, 770, 0, 5890);
				aHa_Sub3_5126.method1316(null, (byte) -108);
				aHa_Sub3_5126.L(i_158_, i_159_, aHa_Sub3_5126.anInt4223);
			}
			if (aClass207_5125 != null) {
				OpenGL.glPushMatrix();
				OpenGL.glTranslatef(0.0F, -1.0F, 0.0F);
				aHa_Sub3_5126.method1323(null, null, aClass272_5130, false, aClass272_5139);
				aClass207_5125.method1996(i_144_, 5890, bools, bool, i_143_, i_142_);
				OpenGL.glPopMatrix();
			}
		}
		if (i_141_ <= 95)
			aFloatArrayArray5136 = null;
	}

	public void wa(r var_r, int i, int i_160_, int i_161_, int i_162_, boolean bool) {
		if (aClass207_5125 != null && var_r != null) {
			int i_163_ = (i - (i_160_ * aHa_Sub3_5126.anInt4212 >> 8) >> aHa_Sub3_5126.anInt4139);
			int i_164_ = (-(aHa_Sub3_5126.anInt4179 * i_160_ >> 8) + i_161_ >> aHa_Sub3_5126.anInt4139);
			aClass207_5125.method1997(i_163_, 81, var_r, i_164_);
		}
	}

	public r fa(int i, int i_165_, r var_r) {
		if ((aByteArrayArray5123[i][i_165_] & 0x1) == 0)
			return null;
		int i_166_ = anInt2836 >> aHa_Sub3_5126.anInt4139;
		r_Sub1 var_r_Sub1 = (r_Sub1) var_r;
		r_Sub1 var_r_Sub1_167_;
		if (var_r_Sub1 == null || !var_r_Sub1.method2861(i_166_, i_166_, (byte) 67))
			var_r_Sub1_167_ = new r_Sub1(aHa_Sub3_5126, i_166_, i_166_);
		else {
			var_r_Sub1_167_ = var_r_Sub1;
			var_r_Sub1_167_.method2862((byte) 71);
		}
		var_r_Sub1_167_.method2865(0, 0, i_166_, i_166_, -100);
		method3370(26463, i, i_165_, var_r_Sub1_167_);
		return var_r_Sub1_167_;
	}

	public boolean method3351(r var_r, int i, int i_168_, int i_169_, int i_170_, boolean bool) {
		if (aClass207_5125 == null || var_r == null)
			return false;
		int i_171_ = (i - (aHa_Sub3_5126.anInt4212 * i_168_ >> 8) >> aHa_Sub3_5126.anInt4139);
		int i_172_ = (i_169_ - (i_168_ * aHa_Sub3_5126.anInt4179 >> 8) >> aHa_Sub3_5126.anInt4139);
		return aClass207_5125.method2000(-112, i_172_, var_r, i_171_);
	}

	public void U(int i, int i_173_, int[] is, int[] is_174_, int[] is_175_, int[] is_176_, int[] is_177_,
			int[] is_178_, int[] is_179_, int[] is_180_, int i_181_, int i_182_, int i_183_, boolean bool) {
		if (is_174_ != null && anIntArrayArrayArray5119 == null)
			anIntArrayArrayArray5119 = new int[anInt2832][anInt2834][];
		d var_d = aHa_Sub3_5126.aD1299;
		if (is_176_ != null && anIntArrayArrayArray5114 == null)
			anIntArrayArrayArray5114 = new int[anInt2832][anInt2834][];
		anIntArrayArrayArray5116[i][i_173_] = is;
		anIntArrayArrayArray5127[i][i_173_] = is_175_;
		anIntArrayArrayArray5117[i][i_173_] = is_177_;
		anIntArrayArrayArray5124[i][i_173_] = is_178_;
		if (anIntArrayArrayArray5114 != null)
			anIntArrayArrayArray5114[i][i_173_] = is_176_;
		if (anIntArrayArrayArray5119 != null)
			anIntArrayArrayArray5119[i][i_173_] = is_174_;
		Class296_Sub32[] class296_sub32s = (aClass296_Sub32ArrayArrayArray5120[i][i_173_] = new Class296_Sub32[is_177_.length]);
		for (int i_184_ = 0; is_177_.length > i_184_; i_184_++) {
			int i_185_ = is_179_[i_184_];
			int i_186_ = is_180_[i_184_];
			if ((anInt5122 & 0x20) != 0 && i_185_ != -1 && var_d.method14(i_185_, -9412).aBoolean1792) {
				i_186_ = 128;
				i_185_ = -1;
			}
			long l = ((long) i_185_
					| ((long) i_181_ << 28 | ((long) i_182_ << 42 | (long) i_183_ << 48) | (long) (i_186_ << 14)));
			Node class296;
			for (class296 = aClass263_5135.get(l); class296 != null; class296 = aClass263_5135.getUnknown(false)) {
				Class296_Sub32 class296_sub32 = (Class296_Sub32) class296;
				if (class296_sub32.anInt4833 == i_185_ && class296_sub32.aFloat4831 == (float) i_186_
						&& i_181_ == class296_sub32.anInt4839 && i_182_ == class296_sub32.anInt4837
						&& class296_sub32.anInt4830 == i_183_)
					break;
			}
			if (class296 != null)
				class296_sub32s[i_184_] = (Class296_Sub32) class296;
			else {
				class296_sub32s[i_184_] = new Class296_Sub32(this, i_185_, i_186_, i_181_, i_182_, i_183_);
				aClass263_5135.put(l, class296_sub32s[i_184_]);
			}
		}
		if (bool)
			aByteArrayArray5123[i][i_173_] = (byte) Class48.bitOR(aByteArrayArray5123[i][i_173_], 1);
		if (anInt5138 < is_177_.length)
			anInt5138 = is_177_.length;
		anInt5140 += is_177_.length;
	}

	s_Sub2(ha_Sub3 var_ha_Sub3, int i, int i_187_, int i_188_, int i_189_, int[][] is, int[][] is_190_, int i_191_) {
		super(i_188_, i_189_, i_191_, is);
		aHa_Sub3_5126 = var_ha_Sub3;
		aByteArrayArray5128 = new byte[i_188_ + 1][i_189_ + 1];
		aShortArrayArray5111 = new short[i_189_ * i_188_][];
		anIntArrayArrayArray5127 = new int[i_188_][i_189_][];
		anIntArrayArrayArray5124 = new int[i_188_][i_189_][];
		anInt5118 = 1 << anInt5121;
		aByteArrayArray5123 = new byte[i_188_][i_189_];
		aFloatArrayArray5136 = new float[anInt2832 + 1][anInt2834 + 1];
		aFloatArrayArray5137 = new float[anInt2832 + 1][anInt2834 + 1];
		aClass296_Sub32ArrayArrayArray5120 = new Class296_Sub32[i_188_][i_189_][];
		aFloatArrayArray5131 = new float[anInt2832 + 1][anInt2834 + 1];
		anInt5122 = i_187_;
		anIntArrayArrayArray5117 = new int[i_188_][i_189_][];
		anIntArrayArrayArray5119 = new int[i_188_][i_189_][];
		anIntArrayArrayArray5116 = new int[i_188_][i_189_][];
		for (int i_192_ = 1; i_192_ < anInt2834; i_192_++) {
			for (int i_193_ = 1; anInt2832 > i_193_; i_193_++) {
				int i_194_ = (is_190_[i_193_ + 1][i_192_] - is_190_[i_193_ - 1][i_192_]);
				int i_195_ = (-is_190_[i_193_][i_192_ - 1] + is_190_[i_193_][i_192_ + 1]);
				float f = (float) (1.0
						/ Math.sqrt((double) (i_195_ * i_195_ + (i_191_ * 4 * i_191_ + i_194_ * i_194_))));
				aFloatArrayArray5137[i_193_][i_192_] = (float) i_194_ * f;
				aFloatArrayArray5131[i_193_][i_192_] = (float) (-i_191_ * 2) * f;
				aFloatArrayArray5136[i_193_][i_192_] = f * (float) i_195_;
			}
		}
		aClass263_5135 = new HashTable(128);
		if ((anInt5122 & 0x10) != 0)
			aClass207_5125 = new Class207(aHa_Sub3_5126, this);
	}

	private void method3370(int i, int i_196_, int i_197_, r_Sub1 var_r_Sub1) {
		int[] is = anIntArrayArrayArray5116[i_196_][i_197_];
		if (i == 26463) {
			int[] is_198_ = anIntArrayArrayArray5127[i_196_][i_197_];
			int i_199_ = is.length;
			if (aHa_Sub3_5126.anIntArray4277.length < i_199_) {
				aHa_Sub3_5126.anIntArray4275 = new int[i_199_];
				aHa_Sub3_5126.anIntArray4277 = new int[i_199_];
			}
			int[] is_200_ = aHa_Sub3_5126.anIntArray4277;
			int[] is_201_ = aHa_Sub3_5126.anIntArray4275;
			for (int i_202_ = 0; i_199_ > i_202_; i_202_++) {
				is_200_[i_202_] = is[i_202_] >> aHa_Sub3_5126.anInt4139;
				is_201_[i_202_] = is_198_[i_202_] >> aHa_Sub3_5126.anInt4139;
			}
			int i_203_ = 0;
			while (i_199_ > i_203_) {
				int i_204_ = is_200_[i_203_];
				int i_205_ = is_201_[i_203_++];
				int i_206_ = is_200_[i_203_];
				int i_207_ = is_201_[i_203_++];
				int i_208_ = is_200_[i_203_];
				int i_209_ = is_201_[i_203_++];
				if (((-i_206_ + i_204_) * (-i_209_ + i_207_) - (i_208_ - i_206_) * (-i_205_ + i_207_)) > 0)
					var_r_Sub1.method2864(i_204_, i_208_, i_206_, i_209_, i_205_, (byte) 0, i_207_);
			}
		}
	}
}
