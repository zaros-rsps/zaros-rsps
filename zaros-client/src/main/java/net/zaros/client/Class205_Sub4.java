package net.zaros.client;

import java.awt.Graphics;
/* Class205_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;

import jaggl.OpenGL;

final class Class205_Sub4 extends Class205 implements Interface6_Impl2 {
	private int anInt5633;
	private int anInt5634;
	static long aLong5635;
	private int anInt5636;

	Class205_Sub4(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Class202 class202, int i, int i_0_, int i_1_, byte[] is) {
		super(var_ha_Sub1_Sub1, 32879, class202, Class67.aClass67_745, i * i_0_ * i_1_, false);
		anInt5633 = i_0_;
		anInt5634 = i_1_;
		anInt5636 = i;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glPixelStorei(3317, 1);
		OpenGL.glTexImage3Dub(anInt3507, 0, method1971(100), anInt5636, anInt5633, anInt5634, 0, BITConfigDefinition.method2350(6409, aClass202_3514), 5121, is, 0);
		OpenGL.glPixelStorei(3317, 4);
	}

	static final void method1985(ha var_ha, byte i) {
		if (i < -115) {
			int i_2_ = 0;
			int i_3_ = 0;
			if (Class368_Sub5_Sub2.aBoolean6597) {
				i_2_ = Class387.method4034(true);
				i_3_ = GraphicsLoader.method2286(true);
			}
			int i_4_ = Class252.anInt2382 + i_2_;
			int i_5_ = i_3_ + Class100.anInt1067;
			int i_6_ = Class81.anInt3666;
			int i_7_ = Class296_Sub39_Sub20.anInt6254 - 3;
			int i_8_ = 20;
			Class296_Sub11.method2500(i_2_ + Class252.anInt2382, Class81.anInt3666, TranslatableString.aClass120_1220.getTranslation(Class394.langID), 0, i_8_, var_ha, Class100.anInt1067 + i_3_, Class296_Sub39_Sub20.anInt6254);
			int i_9_ = i_2_ + Class84.aClass189_924.method1895((byte) -55);
			int i_10_ = Class84.aClass189_924.method1897(0) + i_3_;
			if (Class262.aBoolean1324) {
				int i_11_ = 0;
				for (Class296_Sub39_Sub1 class296_sub39_sub1 = (Class296_Sub39_Sub1) Class152.aClass145_1558.getFront(); class296_sub39_sub1 != null; class296_sub39_sub1 = (Class296_Sub39_Sub1) Class152.aClass145_1558.getNext()) {
					int i_12_ = i_5_ + 13 + i_8_ + i_11_ * 16;
					if (i_9_ > Class252.anInt2382 + i_2_ && i_9_ < i_2_ + Class252.anInt2382 + Class81.anInt3666 && i_12_ - 13 < i_10_ && i_10_ < i_12_ + 4 && (class296_sub39_sub1.anInt6117 > 1 || ((Class296_Sub39_Sub9) class296_sub39_sub1.aClass145_6119.head.queue_next).aBoolean6175)) {
						var_ha.aa(Class252.anInt2382 + i_2_, i_12_ - 12, Class81.anInt3666, 16, 255 - Class279.anInt2547 << 24 | Class314.anInt2781, 1);
					}
					i_11_++;
				}
				if (Class241.aClass296_Sub39_Sub1_2302 != null) {
					Class296_Sub11.method2500(Class41_Sub5.anInt3753, StaticMethods.anInt3152, Class241.aClass296_Sub39_Sub1_2302.aString6116, 0, i_8_, var_ha, Class270.anInt2510, Class95.anInt1036);
					i_11_ = 0;
					for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) Class241.aClass296_Sub39_Sub1_2302.aClass145_6119.getFront(); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) Class241.aClass296_Sub39_Sub1_2302.aClass145_6119.getNext()) {
						int i_13_ = i_11_ * 16 + Class270.anInt2510 + 13 + i_8_;
						i_11_++;
						if (Class41_Sub5.anInt3753 < i_9_ && Class41_Sub5.anInt3753 + StaticMethods.anInt3152 > i_9_ && i_13_ - 13 < i_10_ && i_10_ < i_13_ + 4 && class296_sub39_sub9.aBoolean6175) {
							var_ha.aa(Class41_Sub5.anInt3753, i_13_ - 12, StaticMethods.anInt3152, 16, Class314.anInt2781 | -Class279.anInt2547 + 255 << 24, 1);
						}
					}
					Class62.method699(Class41_Sub5.anInt3753, i_8_, 124, Class270.anInt2510, Class95.anInt1036, StaticMethods.anInt3152, var_ha);
				}
			} else {
				int i_14_ = 0;
				for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeFirst((byte) 124); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeNext(1001)) {
					int i_15_ = (-i_14_ - 1 + Class230.anInt2210) * 16 + i_5_ + 13 + i_8_;
					if (i_2_ + Class252.anInt2382 < i_9_ && i_2_ + Class252.anInt2382 + Class81.anInt3666 > i_9_ && i_15_ - 13 < i_10_ && i_15_ + 4 > i_10_ && class296_sub39_sub9.aBoolean6175) {
						var_ha.aa(Class252.anInt2382 + i_2_, i_15_ - 12, Class81.anInt3666, 16, Class314.anInt2781 | -Class279.anInt2547 + 255 << 24, 1);
					}
					i_14_++;
				}
			}
			Class62.method699(i_2_ + Class252.anInt2382, i_8_, -3, Class100.anInt1067 + i_3_, Class296_Sub39_Sub20.anInt6254, Class81.anInt3666, var_ha);
			if (Class262.aBoolean1324) {
				int i_16_ = 0;
				for (Class296_Sub39_Sub1 class296_sub39_sub1 = (Class296_Sub39_Sub1) Class152.aClass145_1558.getFront(); class296_sub39_sub1 != null; class296_sub39_sub1 = (Class296_Sub39_Sub1) Class152.aClass145_1558.getNext()) {
					int i_17_ = i_8_ + i_3_ + Class100.anInt1067 + i_16_ * 16 + 13;
					i_16_++;
					if (class296_sub39_sub1.anInt6117 == 1) {
						Class296_Sub51_Sub38.method3198(Class81.anInt3666, Class296_Sub39_Sub20.anInt6254, -89, Class25.anInt285 | ~0xffffff, i_10_, i_17_, Class100.anInt1067 + i_3_, var_ha, Class252.anInt2382 + i_2_, i_9_, (Class296_Sub39_Sub9) class296_sub39_sub1.aClass145_6119.head.queue_next, Class5.anInt80 | ~0xffffff);
					} else {
						Class56.method667(Class252.anInt2382 + i_2_, i_10_, class296_sub39_sub1, Class296_Sub39_Sub20.anInt6254, Class25.anInt285 | ~0xffffff, (byte) -120, Class5.anInt80 | ~0xffffff, i_9_, Class81.anInt3666, i_17_, var_ha, Class100.anInt1067 + i_3_);
					}
				}
				if (Class241.aClass296_Sub39_Sub1_2302 != null) {
					i_16_ = 0;
					for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) Class241.aClass296_Sub39_Sub1_2302.aClass145_6119.getFront(); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) Class241.aClass296_Sub39_Sub1_2302.aClass145_6119.getNext()) {
						int i_18_ = i_16_ * 16 + Class270.anInt2510 + i_8_ + 13;
						Class296_Sub51_Sub38.method3198(StaticMethods.anInt3152, Class95.anInt1036, -80, Class25.anInt285 | ~0xffffff, i_10_, i_18_, Class270.anInt2510, var_ha, Class41_Sub5.anInt3753, i_9_, class296_sub39_sub9, Class5.anInt80 | ~0xffffff);
						i_16_++;
					}
					Class395.method4068(Class270.anInt2510, Class41_Sub5.anInt3753, Class95.anInt1036, -30544, StaticMethods.anInt3152);
				}
			} else {
				int i_19_ = 0;
				for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeFirst((byte) 123); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeNext(1001)) {
					int i_20_ = (Class230.anInt2210 + -1 - i_19_) * 16 + i_5_ + i_8_ + 13;
					i_19_++;
					Class296_Sub51_Sub38.method3198(i_6_, i_7_, -106, Class25.anInt285 | ~0xffffff, i_10_, i_20_, i_5_, var_ha, i_4_, i_9_, class296_sub39_sub9, Class5.anInt80 | ~0xffffff);
				}
			}
			Class395.method4068(i_3_ + Class100.anInt1067, i_2_ + Class252.anInt2382, Class296_Sub39_Sub20.anInt6254, -30544, Class81.anInt3666);
		}
	}

	static final void method1986(byte i) {
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 47, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5001);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 61, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026);
		Class343_Sub1.aClass296_Sub50_5282.method3060(2, 91, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5018);
		Class343_Sub1.aClass296_Sub50_5282.method3060(2, 91, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5029);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 92, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 126, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub29_5002);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 71, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub11_5023);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 71, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub9_4999);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 43, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub16_5019);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 58, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 91, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 55, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub7_5004);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 77, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 101, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub1_5008);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 54, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5020);
		int i_21_ = -65 / ((-5 - i) / 45);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 106, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5015);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 54, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub8_5027);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 52, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 96, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub27_5010);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 104, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011);
		Class296_Sub45_Sub2.method2980(true);
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 74, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub10_5009);
		Class343_Sub1.aClass296_Sub50_5282.method3060(3, 54, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub15_4990);
		Class368_Sub4.method3817(-125);
		Class368_Sub22.method3866(11);
		Class380.aBoolean3210 = true;
	}

	static final Sprite method1987(int i, byte[] is) {
		if (is == null) {
			throw new RuntimeException("");
		}
		if (i < 48) {
			method1985(null, (byte) -106);
		}
		for (;;) {
			try {
				Image image = Toolkit.getDefaultToolkit().createImage(is);
				MediaTracker mediatracker = new MediaTracker(Class246.aClient2332);
				mediatracker.addImage(image, 0);
				mediatracker.waitForAll();
				int i_22_ = image.getWidth(Class246.aClient2332);
				int i_23_ = image.getHeight(Class246.aClient2332);
				if (mediatracker.isErrorAny() || i_22_ < 0 || i_23_ < 0) {
					throw new RuntimeException("");
				}
				int[] is_24_ = new int[i_22_ * i_23_];
				PixelGrabber pixelgrabber = new PixelGrabber(image, 0, 0, i_22_, i_23_, is_24_, 0, i_22_);
				pixelgrabber.grabPixels();
				return Class41_Sub13.aHa3774.method1096(112, 0, i_23_, i_22_, i_22_, is_24_);
			} catch (InterruptedException interruptedexception) {
				/* empty */
			}
		}
	}

	private static BufferedImage toBufferedImage(Image image) {
		BufferedImage newImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
		Graphics g = newImage.getGraphics();
		g.drawImage(image, 0, 0, null);
		g.dispose();
		return newImage;
	}
}
