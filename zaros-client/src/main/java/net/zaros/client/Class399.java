package net.zaros.client;
/* Class399 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

final class Class399 {
	static int[] anIntArray3353 = new int[8];
	static Class209 aClass209_3354 = new Class209(1);
	static Class247[][][] aClass247ArrayArrayArray3355;
	static int someXTEARelatedFileID = -1;
	static int anInt3357 = 0;

	public static void method4133(byte i) {
		anIntArray3353 = null;
		aClass209_3354 = null;
		aClass247ArrayArrayArray3355 = null;
		if (i >= -93)
			anInt3357 = 25;
	}

	static final String method4134(int i, Throwable throwable) throws IOException {
		if (i <= 70)
			method4133((byte) -18);
		String string;
		if (!(throwable instanceof RuntimeException_Sub1))
			string = "";
		else {
			RuntimeException_Sub1 runtimeexception_sub1 = (RuntimeException_Sub1) throwable;
			string = runtimeexception_sub1.aString3394 + " | ";
			throwable = runtimeexception_sub1.aThrowable3390;
		}
		StringWriter stringwriter = new StringWriter();
		PrintWriter printwriter = new PrintWriter(stringwriter);
		throwable.printStackTrace(printwriter);
		printwriter.close();
		String string_0_ = stringwriter.toString();
		BufferedReader bufferedreader = new BufferedReader(new StringReader(string_0_));
		String string_1_ = bufferedreader.readLine();
		for (;;) {
			String string_2_ = bufferedreader.readLine();
			if (string_2_ == null)
				break;
			int i_3_ = string_2_.indexOf('(');
			int i_4_ = string_2_.indexOf(')', i_3_ + 1);
			String string_5_;
			if (i_3_ != -1)
				string_5_ = string_2_.substring(0, i_3_);
			else
				string_5_ = string_2_;
			string_5_ = string_5_.trim();
			string_5_ = string_5_.substring(string_5_.lastIndexOf(' ') + 1);
			string_5_ = string_5_.substring(string_5_.lastIndexOf('\t') + 1);
			string += string_5_;
			if (i_3_ != -1 && i_4_ != -1) {
				int i_6_ = string_2_.indexOf(".java:", i_3_);
				if (i_6_ >= 0)
					string += string_2_.substring(i_6_ + 5, i_4_);
			}
			string += ' ';
		}
		string += "| " + (String) string_1_;
		return string;
	}
}
