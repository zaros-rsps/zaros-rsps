package net.zaros.client;

public class Class338 {
	public Class338 aClass338_2971;
	public Class338 aClass338_2972;

	static final boolean method3435(int i, boolean bool) {
		if (bool != true)
			method3437(true, false, true);
		if (i != 23 && i != 20 && i != 1006 && i != 53 && i != 30)
			return false;
		return true;
	}

	static final void method3436(byte i) {
		if (i < -48) {
			if (Class294.sessionKey == null) {
				if (Class296_Sub51_Sub14.anInt6423 != -1)
					Class296_Sub39_Sub15_Sub1.method2884(-71);
				else
					Class359.method3718(Class379.aString3625, (byte) -55, Class286.aString2643);
			} else
				Class131.method1381(1);
		}
	}

	public static final void method3437(boolean bool, boolean bool_0_, boolean bool_1_) {
		if (bool_0_)
			method3437(true, false, true);
		if (bool_1_) {
			StaticMethods.anInt2915++;
			Class296_Sub51_Sub27_Sub1.method3162(52);
		}
		if (bool) {
			Class296_Sub34.anInt4843++;
			FileWorker.method3641(-120);
		}
	}

	public Class338() {
		/* empty */
	}

	final void method3438(boolean bool) {
		if (aClass338_2972 != null) {
			aClass338_2972.aClass338_2971 = aClass338_2971;
			aClass338_2971.aClass338_2972 = aClass338_2972;
			aClass338_2971 = null;
			if (bool)
				method3436((byte) 74);
			aClass338_2972 = null;
		}
	}
}
