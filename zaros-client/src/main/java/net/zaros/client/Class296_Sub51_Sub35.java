package net.zaros.client;

/* Class296_Sub51_Sub35 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub35 extends TextureOperation {
	private int anInt6524;
	private int anInt6525 = 1;
	private int anInt6526;
	static IncomingPacket aClass231_6527 = new IncomingPacket(23, -2);

	static final void method3186(int i, int i_0_, byte i_1_, int i_2_, Class296_Sub1 class296_sub1) {
		class296_sub1.out.writeInt_v2(i_0_);
		if (i_1_ <= -88) {
			class296_sub1.out.writeShortA(i_2_);
			class296_sub1.out.writeShortA(i);
		}
	}

	static final void method3187(byte i) {
		if (Class154.anInt1580 != -1) {
			Class368_Sub13.method3848(-1, Class154.anInt1580, -1, true, false);
			Class154.anInt1580 = -1;
		}
		if (i > -3)
			aClass231_6527 = null;
	}

	static final void method3188(int i, int i_3_) {
		Class146.anInt1442 = i_3_;
		if (i != 23)
			method3189(false);
		World.aClass113_1160.clear();
	}

	public static void method3189(boolean bool) {
		if (bool == true)
			aClass231_6527 = null;
	}

	final int[] get_monochrome_output(int i, int i_4_) {
		int[] is = aClass318_5035.method3335(i_4_, (byte) 28);
		if (i != 0)
			anInt6524 = 124;
		if (aClass318_5035.aBoolean2819) {
			int i_5_ = 0;
			for (/**/; Class41_Sub10.anInt3769 > i_5_; i_5_++) {
				int i_6_ = Class33.anIntArray334[i_5_];
				int i_7_ = Class294.anIntArray2686[i_4_];
				int i_8_ = anInt6525 * i_6_ >> 12;
				int i_9_ = i_7_ * anInt6526 >> 12;
				int i_10_ = i_6_ % (4096 / anInt6525) * anInt6525;
				int i_11_ = anInt6526 * (i_7_ % (4096 / anInt6526));
				if (i_11_ < anInt6524) {
					for (i_8_ -= i_9_; i_8_ < 0; i_8_ += 4) {
						/* empty */
					}
					for (/**/; i_8_ > 3; i_8_ -= 4) {
						/* empty */
					}
					if (i_8_ != 1) {
						is[i_5_] = 0;
						continue;
					}
					if (anInt6524 > i_10_) {
						is[i_5_] = 0;
						continue;
					}
				}
				if (anInt6524 > i_10_) {
					for (i_8_ -= i_9_; i_8_ < 0; i_8_ += 4) {
						/* empty */
					}
					for (/**/; i_8_ > 3; i_8_ -= 4) {
						/* empty */
					}
					if (i_8_ > 0) {
						is[i_5_] = 0;
						continue;
					}
				}
				is[i_5_] = 4096;
			}
		}
		return is;
	}

	public Class296_Sub51_Sub35() {
		super(0, true);
		anInt6524 = 204;
		anInt6526 = 1;
	}

	final void method3071(int i, Packet class296_sub17, int i_12_) {
		int i_13_ = i_12_;
		while_251_ : do {
			do {
				if (i_13_ != 0) {
					if (i_13_ != 1) {
						if (i_13_ == 2)
							break;
						break while_251_;
					}
				} else {
					anInt6525 = class296_sub17.g1();
					break while_251_;
				}
				anInt6526 = class296_sub17.g1();
				break while_251_;
			} while (false);
			anInt6524 = class296_sub17.g2();
		} while (false);
		if (i >= -84)
			method3071(-81, null, 83);
	}
}
