package net.zaros.client;

/* Class296_Sub51_Sub30 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub30 extends TextureOperation {
	static int[] anIntArray6495 = {-1, 8192, 0, -1, 12288, 10240, 14336, -1, 4096, 6144, 2048};
	private int anInt6496 = 32768;
	static Class161 aClass161_6497 = new Class161(13, 0, 1, 0);
	static boolean aBoolean6498 = false;
	static int[] bodyTypes = {8, 11, 4, 6, 9, 7, 10, 0};

	static final Class368 method3167(int i, Packet class296_sub17) {
		int i_0_ = class296_sub17.g1();
		Class209 class209 = Class296_Sub15.method2515(i_0_, 1188);
		Class368 class368 = null;
		if (i != 0)
			method3168(null, (byte) 39, false, -44, true);
		Class209 class209_1_ = class209;
		while_192_ : do {
			while_191_ : do {
				while_190_ : do {
					while_189_ : do {
						while_188_ : do {
							while_187_ : do {
								while_186_ : do {
									while_185_ : do {
										while_184_ : do {
											while_183_ : do {
												while_182_ : do {
													while_181_ : do {
														while_180_ : do {
															while_179_ : do {
																while_178_ : do {
																	while_177_ : do {
																		while_176_ : do {
																			while_175_ : do {
																				while_174_ : do {
																					while_173_ : do {
																						while_172_ : do {
																							while_171_ : do {
																								while_170_ : do {
																									while_169_ : do {
																										while_168_ : do {
																											while_167_ : do {
																												do {
																													if (Class296_Sub35_Sub2.aClass209_6110 != class209_1_) {
																														if (Class245.aClass209_2329 != class209_1_) {
																															if (Class41_Sub12.aClass209_3770 != class209_1_) {
																																if (class209_1_ != Class144.aClass209_1434) {
																																	if (class209_1_ != PlayerEquipment.aClass209_3376) {
																																		if (Class296_Sub51_Sub12.aClass209_6409 != class209_1_) {
																																			if (Texture.aClass209_6232 != class209_1_) {
																																				if (class209_1_ != Class296_Sub45_Sub3.aClass209_6291) {
																																					if (class209_1_ != Class135.aClass209_1394) {
																																						if (class209_1_ != Class44_Sub1.aClass209_3856) {
																																							if (class209_1_ != Class399.aClass209_3354) {
																																								if (Class313.aClass209_2776 != class209_1_) {
																																									if (class209_1_ != TranslatableString.aClass209_1260) {
																																										if (Class164.aClass209_1682 != class209_1_) {
																																											if (class209_1_ != Class78.aClass209_3411) {
																																												if (aa_Sub1.aClass209_3722 != class209_1_) {
																																													if (Class157.aClass209_1596 != class209_1_) {
																																														if (Class79.aClass209_885 != class209_1_) {
																																															if (class209_1_ != Class368_Sub10.aClass209_5479) {
																																																if (Class41_Sub21.aClass209_3798 != class209_1_) {
																																																	if (r_Sub2.aClass209_6716 != class209_1_) {
																																																		if (class209_1_ != Class16_Sub3_Sub1.aClass209_5805) {
																																																			if (Class41_Sub5.aClass209_3755 != class209_1_) {
																																																				if (Class379_Sub2.aClass209_5677 != class209_1_) {
																																																					if (class209_1_ != Class181_Sub1.aClass209_4513) {
																																																						if (Class83.aClass209_919 != class209_1_) {
																																																							if (Class107.aClass209_3574 != class209_1_) {
																																																								if (StaticMethods.aClass209_5941 != class209_1_)
																																																									break while_192_;
																																																							} else
																																																								break while_190_;
																																																							break while_191_;
																																																						}
																																																					} else
																																																						break while_188_;
																																																					break while_189_;
																																																				}
																																																			} else
																																																				break while_186_;
																																																			break while_187_;
																																																		}
																																																	} else
																																																		break while_184_;
																																																	break while_185_;
																																																}
																																															} else
																																																break while_182_;
																																															break while_183_;
																																														}
																																													} else
																																														break while_180_;
																																													break while_181_;
																																												}
																																											} else
																																												break while_178_;
																																											break while_179_;
																																										}
																																									} else
																																										break while_176_;
																																									break while_177_;
																																								}
																																							} else
																																								break while_174_;
																																							break while_175_;
																																						}
																																					} else
																																						break while_172_;
																																					break while_173_;
																																				}
																																			} else
																																				break while_170_;
																																			break while_171_;
																																		}
																																	} else
																																		break while_168_;
																																	break while_169_;
																																}
																															} else
																																break;
																															break while_167_;
																														}
																													} else {
																														class368 = new Class368_Sub19(class296_sub17);
																														break while_192_;
																													}
																													class368 = new Class368_Sub1(class296_sub17);
																													break while_192_;
																												} while (false);
																												class368 = new Class368_Sub7(class296_sub17);
																												break while_192_;
																											} while (false);
																											class368 = new Class368_Sub14(class296_sub17);
																											break while_192_;
																										} while (false);
																										class368 = new Class368_Sub8(class296_sub17);
																										break while_192_;
																									} while (false);
																									class368 = new Class368_Sub16(class296_sub17);
																									break while_192_;
																								} while (false);
																								class368 = new Class368_Sub6(class296_sub17);
																								break while_192_;
																							} while (false);
																							class368 = new Class368_Sub5_Sub1(class296_sub17);
																							break while_192_;
																						} while (false);
																						class368 = new Class368_Sub3(class296_sub17);
																						break while_192_;
																					} while (false);
																					class368 = new Class368_Sub11(class296_sub17);
																					break while_192_;
																				} while (false);
																				class368 = new Class368_Sub23(class296_sub17);
																				break while_192_;
																			} while (false);
																			class368 = new Class368_Sub15(class296_sub17);
																			break while_192_;
																		} while (false);
																		class368 = new Class368_Sub17(class296_sub17);
																		break while_192_;
																	} while (false);
																	class368 = (new Class368_Sub4(class296_sub17));
																	break while_192_;
																} while (false);
																class368 = (new Class368_Sub9(class296_sub17));
																break while_192_;
															} while (false);
															class368 = (new Class368_Sub12(class296_sub17));
															break while_192_;
														} while (false);
														class368 = (new Class368_Sub20(class296_sub17));
														break while_192_;
													} while (false);
													class368 = (new Class368_Sub21(class296_sub17));
													break while_192_;
												} while (false);
												class368 = (new Class368_Sub18(class296_sub17));
												break while_192_;
											} while (false);
											class368 = (new Class368_Sub5_Sub2(class296_sub17));
											break while_192_;
										} while (false);
										class368 = (new Class368_Sub13(class296_sub17, 1, 1));
										break while_192_;
									} while (false);
									class368 = new Class368_Sub13(class296_sub17, 0, 1);
									break while_192_;
								} while (false);
								class368 = new Class368_Sub13(class296_sub17, 0, 0);
								break while_192_;
							} while (false);
							class368 = new Class368_Sub13(class296_sub17, 1, 0);
							break while_192_;
						} while (false);
						class368 = new Class368_Sub22(class296_sub17, false);
						break while_192_;
					} while (false);
					class368 = new Class368_Sub22(class296_sub17, true);
					break while_192_;
				} while (false);
				class368 = new Class368_Sub2(class296_sub17);
				break while_192_;
			} while (false);
			class368 = new Class368_Sub10(class296_sub17);
		} while (false);
		return class368;
	}

	final void method3071(int i, Packet class296_sub17, int i_2_) {
		int i_3_ = i_2_;
		do {
			if (i_3_ != 0) {
				if (i_3_ != 1)
					break;
			} else {
				anInt6496 = class296_sub17.g2() << 4;
				break;
			}
			monochromatic = class296_sub17.g1() == 1;
		} while (false);
		if (i > -84)
			method3167(24, null);
	}

	public Class296_Sub51_Sub30() {
		super(3, false);
	}

	final int[] get_monochrome_output(int i, int i_4_) {
		int[] is = aClass318_5035.method3335(i_4_, (byte) 28);
		if (i != 0)
			method3169(30);
		if (aClass318_5035.aBoolean2819) {
			int[] is_5_ = this.method3064(1, 0, i_4_);
			int[] is_6_ = this.method3064(2, i, i_4_);
			for (int i_7_ = 0; i_7_ < Class41_Sub10.anInt3769; i_7_++) {
				int i_8_ = is_5_[i_7_] >> 4 & 0xff;
				int i_9_ = is_6_[i_7_] * anInt6496 >> 12;
				int i_10_ = Class259.anIntArray2419[i_8_] * i_9_ >> 12;
				int i_11_ = i_9_ * Class2.anIntArray56[i_8_] >> 12;
				int i_12_ = i_7_ + (i_10_ >> 12) & Class41_Sub25.anInt3803;
				int i_13_ = Class67.anInt753 & i_4_ + (i_11_ >> 12);
				int[] is_14_ = this.method3064(0, 0, i_13_);
				is[i_7_] = is_14_[i_12_];
			}
		}
		return is;
	}

	static final Class55 method3168(ha var_ha, byte i, boolean bool, int i_15_, boolean bool_16_) {
		if (i > -32)
			aClass161_6497 = null;
		Class322 class322 = Class296_Sub51_Sub14.method3114(bool, var_ha, 9057, bool_16_, i_15_);
		if (class322 == null)
			return null;
		return class322.aClass55_2829;
	}

	static final Class296_Sub1 method3169(int i) {
		if (i <= 40)
			method3169(-47);
		Class296_Sub1 class296_sub1 = Class368_Sub4.method3821((byte) -68);
		class296_sub1.anInt4585 = 0;
		class296_sub1.aClass311_4581 = null;
		class296_sub1.out = new ByteStream(5000);
		return class296_sub1;
	}

	public static void method3170(int i) {
		aClass161_6497 = null;
		bodyTypes = null;
		anIntArray6495 = null;
		if (i != 2)
			aBoolean6498 = false;
	}

	final int[][] get_colour_output(int i, int i_17_) {
		if (i_17_ != 17621)
			get_colour_output(-16, 24);
		int[][] is = aClass86_5034.method823((byte) 5, i);
		if (aClass86_5034.aBoolean939) {
			int[] is_18_ = this.method3064(1, 0, i);
			int[] is_19_ = this.method3064(2, 0, i);
			int[] is_20_ = is[0];
			int[] is_21_ = is[1];
			int[] is_22_ = is[2];
			for (int i_23_ = 0; i_23_ < Class41_Sub10.anInt3769; i_23_++) {
				int i_24_ = is_18_[i_23_] * 255 >> 12 & 0xff;
				int i_25_ = anInt6496 * is_19_[i_23_] >> 12;
				int i_26_ = Class259.anIntArray2419[i_24_] * i_25_ >> 12;
				int i_27_ = i_25_ * Class2.anIntArray56[i_24_] >> 12;
				int i_28_ = Class41_Sub25.anInt3803 & i_23_ + (i_26_ >> 12);
				int i_29_ = Class67.anInt753 & (i_27_ >> 12) + i;
				int[][] is_30_ = this.method3075((byte) 114, 0, i_29_);
				is_20_[i_23_] = is_30_[0][i_28_];
				is_21_[i_23_] = is_30_[1][i_28_];
				is_22_[i_23_] = is_30_[2][i_28_];
			}
		}
		return is;
	}

	final void method3076(byte i) {
		int i_31_ = -49 / ((i + 58) / 40);
		Class367.method3800(4096);
	}

	static final void method3171(byte i) {
		if (Class253.aClass326_2389 != null) {
			if (i > -92)
				method3169(-5);
			Class296_Sub51_Sub15.aClass54_6426 = new Class54();
			Class296_Sub51_Sub15.aClass54_6426.method643(Class253.aClass326_2389.aClass120_2879.getTranslation(Class394.langID), -113, Class253.aClass326_2389, Class253.aClass326_2389.anInt2875, Class296_Sub39_Sub4.aLong5743);
			Class373_Sub3.aThread5609 = new Thread(Class296_Sub51_Sub15.aClass54_6426, "");
			Class373_Sub3.aThread5609.start();
		}
	}
}
