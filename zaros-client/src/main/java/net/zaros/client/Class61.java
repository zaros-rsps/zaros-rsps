package net.zaros.client;

/* Class61 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class61 {
	Class15 aClass15_696;
	boolean aBoolean697;
	int anInt698;
	int anInt699 = 1190717;
	int anInt700;
	int anInt701;
	int anInt702;
	boolean aBoolean703 = true;
	static Class296_Sub37 aClass296_Sub37_704 = new Class296_Sub37(0, 0);
	boolean aBoolean705;
	int anInt706;
	int anInt707;
	int anInt708;
	int anInt709;
	static int[] anIntArray710 = new int[5];

	final void method689(Packet class296_sub17, byte i) {
		for (;;) {
			int i_0_ = class296_sub17.g1();
			if (i_0_ == 0)
				break;
			method691(i_0_, class296_sub17, (byte) -106);
		}
		int i_1_ = -94 / ((i - 23) / 52);
	}

	final void method690(boolean bool) {
		anInt708 = anInt698 | anInt708 << 8;
		if (bool)
			anInt706 = -35;
	}

	private final void method691(int i, Packet class296_sub17, byte i_2_) {
		if (i_2_ >= -30)
			anInt706 = 47;
		if (i == 1)
			anInt702 = StaticMethods.method2723(11588, class296_sub17.readUnsignedMedInt());
		else if (i == 2)
			anInt709 = class296_sub17.g1();
		else if (i == 3) {
			anInt709 = class296_sub17.g2();
			if (anInt709 == 65535)
				anInt709 = -1;
		} else if (i != 5) {
			if (i == 7)
				anInt707 = (StaticMethods.method2723(11588, class296_sub17.readUnsignedMedInt()));
			else if (i == 8)
				aClass15_696.anInt179 = anInt698;
			else if (i != 9) {
				if (i != 10) {
					if (i == 11)
						anInt708 = class296_sub17.g1();
					else if (i != 12) {
						if (i == 13)
							anInt699 = class296_sub17.readUnsignedMedInt();
						else if (i != 14) {
							if (i == 16)
								anInt700 = class296_sub17.g1();
						} else
							anInt701 = class296_sub17.g1() << 2;
					} else
						aBoolean705 = true;
				} else
					aBoolean697 = false;
			} else
				anInt706 = class296_sub17.g2() << 2;
		} else
			aBoolean703 = false;
	}

	public static void method692(int i) {
		aClass296_Sub37_704 = null;
		if (i == 9)
			anIntArray710 = null;
	}

	static final void method693(int i) {
		if (i == 2779) {
			synchronized (Class280.aClass113_2555) {
				Class280.aClass113_2555.clearSoftReferences();
			}
		}
	}

	static final int method694(int i, int i_3_, int i_4_) {
		if (i_3_ == -1)
			return 12345678;
		i = i * (i_3_ & i_4_) >> 7;
		if (i < 2)
			i = 2;
		else if (i > 126)
			i = 126;
		return i + (i_3_ & 0xff80);
	}

	static final void method695(boolean bool, int i, InterfaceComponent class51) {
		int i_5_ = class51.scrollMaxH != 0 ? class51.scrollMaxH : class51.anInt578;
		int i_6_ = class51.scrollMaxW != 0 ? class51.scrollMaxW : class51.anInt623;
		Class37.method364((Class192.openedInterfaceComponents[class51.uid >> 16]), i_5_, bool, class51.uid, (byte) 120, i_6_);
		if (class51.aClass51Array538 != null)
			Class37.method364(class51.aClass51Array538, i_5_, bool, class51.uid, (byte) 127, i_6_);
		if (i <= -45) {
			Class296_Sub13 class296_sub13 = ((Class296_Sub13) Class386.aClass263_3264.get((long) class51.uid));
			if (class296_sub13 != null)
				Class78.method797(bool, i_5_, i_6_, -10111, class296_sub13.anInt4657);
		}
	}

	public Class61() {
		aBoolean697 = true;
		anInt700 = 127;
		anInt706 = 512;
		anInt702 = 0;
		anInt701 = 64;
		anInt708 = 8;
		aBoolean705 = false;
		anInt709 = -1;
		anInt707 = -1;
	}
}
