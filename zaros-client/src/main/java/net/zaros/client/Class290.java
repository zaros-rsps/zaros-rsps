package net.zaros.client;

/* Class290 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class290 {
	static NodeDeque aClass155_2655 = new NodeDeque();
	static int anInt2656;

	public static void method2400(int i) {
		if (i != 0)
			method2401(null, 2, -65, -25, -26, -119);
		aClass155_2655 = null;
	}

	static final void method2401(ha var_ha, int i, int i_0_, int i_1_, int i_2_, int i_3_) {
		var_ha.KA(i_3_, i_0_, i_3_ + i_2_, i_0_ + i_1_);
		var_ha.method1088(i_3_, i_2_, -16777216, 1, i_0_, i_1_);
		if (i <= Class41_Sub13.anInt3773) {
			float f = (float) Class106.anInt1114 / (float) Class106.anInt1116;
			int i_4_ = i_2_;
			int i_5_ = i_1_;
			if (f < 1.0F)
				i_5_ = (int) ((float) i_2_ * f);
			else
				i_4_ = (int) ((float) i_1_ / f);
			i_0_ += (i_1_ - i_5_) / 2;
			i_3_ += (i_2_ - i_4_) / 2;
			if (Class152.aClass397_1570 == null || Class152.aClass397_1570.method4087() != i_2_ || Class152.aClass397_1570.method4092() != i_1_) {
				Class106.method928(Class106.anInt1111, Class106.anInt1114 + Class106.anInt1117, Class106.anInt1116 + Class106.anInt1111, Class106.anInt1117, i_3_, i_0_, i_4_ + i_3_, i_5_ + i_0_);
				Class106.method930(var_ha);
				Class152.aClass397_1570 = var_ha.a(i_3_, i_0_, i_4_, i_5_, false);
			}
			Class152.aClass397_1570.method4096(i_3_, i_0_);
			int i_6_ = Class368_Sub5.anInt5450 * i_4_ / Class106.anInt1116;
			int i_7_ = Class269.anInt2503 * i_5_ / Class106.anInt1114;
			int i_8_ = i_3_ + Class293.anInt2684 * i_4_ / Class106.anInt1116;
			int i_9_ = (-(i_5_ * Class224.anInt2169 / Class106.anInt1114) + i_5_ + (i_0_ - i_7_));
			int i_10_ = -1996554240;
			if (Class296_Sub32.stellardawn == Class296_Sub50.game)
				i_10_ = -1996488705;
			var_ha.aa(i_8_, i_9_, i_6_, i_7_, i_10_, 1);
			var_ha.b(i_8_, i_9_, i_6_, i_7_, i_10_, 0);
			if (aa_Sub2.anInt3728 > 0) {
				int i_11_;
				if (Class139.anInt1421 > 50)
					i_11_ = (100 - Class139.anInt1421) * 5;
				else
					i_11_ = Class139.anInt1421 * 5;
				for (Class296_Sub53 class296_sub53 = ((Class296_Sub53) Class106.aClass155_1101.removeFirst((byte) 124)); class296_sub53 != null; class296_sub53 = ((Class296_Sub53) Class106.aClass155_1101.removeNext(i ^ 0x38d))) {
					Class18 class18 = Class106.aClass245_1094.method2179(11702, class296_sub53.anInt5046);
					if (Class296_Sub52.method3203(-55, class18)) {
						if (class296_sub53.anInt5046 == Class41_Sub26.anInt3807) {
							int i_12_ = ((i_4_ * class296_sub53.anInt5047 / Class106.anInt1116) + i_3_);
							int i_13_ = ((i_5_ * (Class106.anInt1114 - class296_sub53.anInt5042) / Class106.anInt1114) + i_0_);
							var_ha.method1088(i_12_ - 2, 4, i_11_ << 24 | 0xffff00, 1, i_13_ - 2, 4);
						} else if (Class325.anInt2868 != -1 && class18.anInt217 == Class325.anInt2868) {
							int i_14_ = i_3_ + (class296_sub53.anInt5047 * i_4_ / Class106.anInt1116);
							int i_15_ = (((-class296_sub53.anInt5042 + Class106.anInt1114) * i_5_ / Class106.anInt1114) + i_0_);
							var_ha.method1088(i_14_ - 2, 4, i_11_ << 24 | 0xffff00, 1, i_15_ - 2, 4);
						}
					}
				}
			}
		}
	}
}
