package net.zaros.client;

/* Class296_Sub51_Sub22 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub22 extends TextureOperation {
	private int anInt6452 = 4096;
	private int anInt6453 = 4096;
	private int anInt6454 = 4096;

	final int[][] get_colour_output(int i, int i_0_) {
		int[][] is = aClass86_5034.method823((byte) 126, i);
		if (i_0_ != 17621)
			method3071(-117, null, -31);
		if (aClass86_5034.aBoolean939) {
			int[][] is_1_ = this.method3075((byte) 16, 0, i);
			int[] is_2_ = is_1_[0];
			int[] is_3_ = is_1_[1];
			int[] is_4_ = is_1_[2];
			int[] is_5_ = is[0];
			int[] is_6_ = is[1];
			int[] is_7_ = is[2];
			for (int i_8_ = 0; i_8_ < Class41_Sub10.anInt3769; i_8_++) {
				int i_9_ = is_2_[i_8_];
				int i_10_ = is_4_[i_8_];
				int i_11_ = is_3_[i_8_];
				if (i_9_ != i_10_ || i_10_ != i_11_) {
					is_5_[i_8_] = anInt6454;
					is_6_[i_8_] = anInt6453;
					is_7_[i_8_] = anInt6452;
				} else {
					is_5_[i_8_] = anInt6454 * i_9_ >> 12;
					is_6_[i_8_] = anInt6453 * i_10_ >> 12;
					is_7_[i_8_] = anInt6452 * i_11_ >> 12;
				}
			}
		}
		return is;
	}

	final void method3071(int i, Packet class296_sub17, int i_352_) {
		if (i > -84)
			method3071(33, null, -119);
		int i_353_ = i_352_;
		while_136_ : do {
			do {
				if (i_353_ != 0) {
					if (i_353_ != 1) {
						if (i_353_ == 2)
							break;
						break while_136_;
					}
				} else {
					anInt6454 = class296_sub17.g2();
					break while_136_;
				}
				anInt6453 = class296_sub17.g2();
				break while_136_;
			} while (false);
			anInt6452 = class296_sub17.g2();
		} while (false);
	}

	public Class296_Sub51_Sub22() {
		super(1, false);
	}
}
