package net.zaros.client;
/* Class296_Sub51_Sub21 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Dimension;

final class Class296_Sub51_Sub21 extends TextureOperation {
	static Class321 softReferenceFactory;
	private int anInt6446;
	static Class92 aClass92_6447;
	private int anInt6448;
	private int anInt6449;
	static int[] anIntArray6450 = new int[13];
	static Js5 fs28;

	private Class296_Sub51_Sub21(int i) {
		super(0, false);
		method3132(3, i);
	}

	static final void method3130(String string, String string_0_, int i, int i_1_) {
		if (string_0_.length() <= 320 && Class338_Sub3_Sub1_Sub1.method3481(i ^ ~0x7d6246e7) && i == -2103592604) {
			Class181.method1817(false);
			Class286.aString2643 = string_0_;
			Class220.anInt2150 = i_1_;
			Class379.aString3625 = string;
			Class41_Sub8.method422(i + 2103592605, 6);
		}
	}

	final int[][] get_colour_output(int i, int i_2_) {
		if (i_2_ != 17621)
			return null;
		int[][] is = aClass86_5034.method823((byte) 11, i);
		if (aClass86_5034.aBoolean939) {
			int[] is_3_ = is[0];
			int[] is_4_ = is[1];
			int[] is_5_ = is[2];
			for (int i_6_ = 0; Class41_Sub10.anInt3769 > i_6_; i_6_++) {
				is_3_[i_6_] = anInt6448;
				is_4_[i_6_] = anInt6446;
				is_5_[i_6_] = anInt6449;
			}
		}
		return is;
	}

	static final void method3131(int i, Mobile class338_sub3_sub1_sub3) {
		Animator class44 = class338_sub3_sub1_sub3.aClass44_6802;
		if (Class29.anInt307 == class338_sub3_sub1_sub3.anInt6807 || !class44.method570((byte) 40) || class44.method555(i - 75, 1)) {
			int i_7_ = (-class338_sub3_sub1_sub3.anInt6809 + class338_sub3_sub1_sub3.anInt6807);
			int i_8_ = Class29.anInt307 - class338_sub3_sub1_sub3.anInt6809;
			int i_9_ = (class338_sub3_sub1_sub3.anInt6813 * 512 + class338_sub3_sub1_sub3.getSize() * 256);
			int i_10_ = (class338_sub3_sub1_sub3.anInt6811 * 512 + class338_sub3_sub1_sub3.getSize() * 256);
			int i_11_ = (class338_sub3_sub1_sub3.anInt6821 * 512 + class338_sub3_sub1_sub3.getSize() * 256);
			int i_12_ = (class338_sub3_sub1_sub3.anInt6805 * 512 + class338_sub3_sub1_sub3.getSize() * 256);
			class338_sub3_sub1_sub3.tileX = ((-i_8_ + i_7_) * i_9_ + i_8_ * i_11_) / i_7_;
			class338_sub3_sub1_sub3.tileY = ((i_7_ - i_8_) * i_10_ + i_8_ * i_12_) / i_7_;
		}
		class338_sub3_sub1_sub3.anInt6836 = i;
		if (class338_sub3_sub1_sub3.anInt6819 == 0)
			class338_sub3_sub1_sub3.method3517(false, true, 8192);
		if (class338_sub3_sub1_sub3.anInt6819 == 1)
			class338_sub3_sub1_sub3.method3517(false, true, 12288);
		if (class338_sub3_sub1_sub3.anInt6819 == 2)
			class338_sub3_sub1_sub3.method3517(false, true, 0);
		if (class338_sub3_sub1_sub3.anInt6819 == 3)
			class338_sub3_sub1_sub3.method3517(false, true, 4096);
	}

	private final void method3132(int i, int i_13_) {
		int i_14_ = -90 % ((i - 81) / 32);
		anInt6449 = (i_13_ & 0xff) << 4;
		anInt6446 = (i_13_ & 0xff00) >> 4;
		anInt6448 = (i_13_ & 0xff0000) >> 12;
	}

	static final void method3133(int i) {
		int i_15_ = -127 / ((18 - i) / 50);
		int i_16_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5029.method449(124);
		if (i_16_ == 0) {
			Class198.aByteArrayArrayArray2003 = null;
			Class354.method3693(0, 68);
		} else if (i_16_ != 1) {
			Class368_Sub5_Sub1.method3825(true, (byte) (Class296_Sub2.anInt4590 - 4 & 0xff));
			Class354.method3693(2, 115);
		} else {
			Class368_Sub5_Sub1.method3825(true, (byte) 0);
			Class354.method3693(512, -49);
			if (Class41_Sub18.aByteArrayArrayArray3786 != null)
				Class296_Sub42.method2920((byte) 125);
		}
		Class52.anInt642 = FileWorker.anInt3005;
	}

	static final int method3134(int i, int i_17_, byte i_18_) {
		if (NPCDefinition.aClass268_1478.anInt2497 == -1)
			return 1;
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(i_18_ + 238) != i) {
			Class84.method819((byte) -17, TranslatableString.aClass120_1209.getTranslation(Class394.langID), true, i);
			if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(i_18_ ^ ~0x4) != i)
				return -1;
		}
		try {
			Dimension dimension = Class230.aCanvas2209.getSize();
			ModeWhat.method1021(Class123_Sub1_Sub1.aClass92_5814, 8493, TranslatableString.aClass120_1209.getTranslation(Class394.langID), Class41_Sub13.aHa3774, true, Class205_Sub1.aClass55_5642);
			Mesh class132 = Class296_Sub51_Sub1.fromJs5(Class184.fs7, (NPCDefinition.aClass268_1478.anInt2497), 0);
			long l = Class72.method771(-112);
			Class41_Sub13.aHa3774.la();
			Class223.aClass373_2162.method3902(0, Class304.anInt2737, 0);
			Class41_Sub13.aHa3774.a(Class223.aClass373_2162);
			Class41_Sub13.aHa3774.DA(dimension.width / 2, dimension.height / 2, 512, 512);
			Class41_Sub13.aHa3774.xa(1.0F);
			Class41_Sub13.aHa3774.ZA(16777215, 0.5F, 0.5F, 20.0F, -50.0F, 30.0F);
			Model class178 = Class41_Sub13.aHa3774.a(class132, 2048, 64, 64, 768);
			int i_19_ = 0;
			int i_20_ = 0;
			if (i_18_ != -121)
				method3134(-65, 46, (byte) 44);
			while_129_ : for (/**/; i_20_ < 500; i_20_++) {
				Class41_Sub13.aHa3774.GA(0);
				Class41_Sub13.aHa3774.ya();
				for (int i_21_ = 15; i_21_ >= 0; i_21_--) {
					for (int i_22_ = 0; i_21_ >= i_22_; i_22_++) {
						Class253.aClass373_2388.method3902((int) ((-((float) i_21_ / 2.0F) + (float) i_22_) * (float) Js5TextureLoader.anInt3440), 0, (i_21_ + 1) * Js5TextureLoader.anInt3440);
						class178.method1715(Class253.aClass373_2388, null, 0);
						i_19_++;
						if (-l + Class72.method771(i_18_ + 12) >= (long) i_17_)
							break while_129_;
					}
				}
			}
			Class41_Sub13.aHa3774.A();
			long l_23_ = (long) (i_19_ * 1000) / (-l + Class72.method771(-115));
			Class41_Sub13.aHa3774.GA(0);
			Class41_Sub13.aHa3774.ya();
			return (int) l_23_;
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			return -1;
		}
	}

	public Class296_Sub51_Sub21() {
		this(0);
	}

	final void method3071(int i, Packet class296_sub17, int i_24_) {
		int i_25_ = i_24_;
		if (i_25_ == 0)
			method3132(-24, class296_sub17.readUnsignedMedInt());
		if (i >= -84)
			method3133(85);
	}

	public static void method3135(byte i) {
		anIntArray6450 = null;
		softReferenceFactory = null;
		fs28 = null;
		int i_26_ = 104 % ((-52 - i) / 35);
		aClass92_6447 = null;
	}

	static {
		softReferenceFactory = Class226.method2087((byte) -78);
	}
}
