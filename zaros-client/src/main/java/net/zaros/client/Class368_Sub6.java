package net.zaros.client;

/* Class368_Sub6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub6 extends Class368 {
	private int anInt5456;
	private int anInt5457;
	private int anInt5458;
	private int anInt5459;

	Class368_Sub6(Packet class296_sub17) {
		super(class296_sub17);
		anInt5458 = class296_sub17.g2();
		anInt5456 = class296_sub17.g1();
		anInt5457 = class296_sub17.g1();
		anInt5459 = class296_sub17.g1();
	}

	static final void method3832(boolean bool, int i, Player class338_sub3_sub1_sub3_sub1) {
		if (Class230.anInt2210 < 400) {
			if (class338_sub3_sub1_sub3_sub1 == Class296_Sub51_Sub11.localPlayer) {
				if (Class127.aBoolean1304 && (Class321.anInt2824 & 0x10) != 0)
					StaticMethods.method1008(39, false, 0, IOException_Sub1.anInt36, -1, Class228.aString2200, 0, 0L, 52, true, (long) class338_sub3_sub1_sub3_sub1.index, (ConfigsRegister.aString3672 + " -> <col=ffffff>" + TranslatableString.aClass120_1233.getTranslation(Class394.langID)), false);
			} else {
				String string;
				if (class338_sub3_sub1_sub3_sub1.anInt6888 == 0) {
					boolean bool_0_ = true;
					if ((Class296_Sub51_Sub11.localPlayer.anInt6876) != -1 && class338_sub3_sub1_sub3_sub1.anInt6876 != -1) {
						int i_1_ = (((Class296_Sub51_Sub11.localPlayer.anInt6876) >= class338_sub3_sub1_sub3_sub1.anInt6876) ? class338_sub3_sub1_sub3_sub1.anInt6876 : (Class296_Sub51_Sub11.localPlayer.anInt6876));
						int i_2_ = (-class338_sub3_sub1_sub3_sub1.combatLevel + (Class296_Sub51_Sub11.localPlayer.combatLevel));
						if (i_2_ < 0)
							i_2_ = -i_2_;
						if (i_1_ < i_2_)
							bool_0_ = false;
					}
					String string_3_ = ((Class296_Sub32.stellardawn != Class296_Sub50.game) ? TranslatableString.aClass120_1224.getTranslation(Class394.langID) : TranslatableString.aClass120_1226.getTranslation(Class394.langID));
					if (class338_sub3_sub1_sub3_sub1.combatLevel >= class338_sub3_sub1_sub3_sub1.combatLevelWithSummoning)
						string = (class338_sub3_sub1_sub3_sub1.getFullName(true) + (bool_0_ ? (Class21.method288(i - 2104, (Class296_Sub51_Sub11.localPlayer.combatLevel), class338_sub3_sub1_sub3_sub1.combatLevel)) : "<col=ffffff>") + " (" + string_3_ + class338_sub3_sub1_sub3_sub1.combatLevel + ")");
					else
						string = (class338_sub3_sub1_sub3_sub1.getFullName(true) + (bool_0_ ? (Class21.method288(-111, (Class296_Sub51_Sub11.localPlayer.combatLevel), class338_sub3_sub1_sub3_sub1.combatLevel)) : "<col=ffffff>") + " (" + string_3_ + class338_sub3_sub1_sub3_sub1.combatLevel + "+" + (class338_sub3_sub1_sub3_sub1.combatLevelWithSummoning - class338_sub3_sub1_sub3_sub1.combatLevel) + ")");
				} else if (class338_sub3_sub1_sub3_sub1.anInt6888 == -1)
					string = class338_sub3_sub1_sub3_sub1.getFullName(true);
				else
					string = (class338_sub3_sub1_sub3_sub1.getFullName(true) + " (" + TranslatableString.aClass120_1225.getTranslation(Class394.langID) + class338_sub3_sub1_sub3_sub1.anInt6888 + ")");
				if (Class127.aBoolean1304 && !bool && (Class321.anInt2824 & 0x8) != 0)
					StaticMethods.method1008(-117, false, 0, IOException_Sub1.anInt36, -1, Class228.aString2200, 0, (long) class338_sub3_sub1_sub3_sub1.index, 5, true, (long) class338_sub3_sub1_sub3_sub1.index, ConfigsRegister.aString3672 + " -> <col=ffffff>" + string, false);
				if (!bool) {
					for (int i_4_ = 7; i_4_ >= 0; i_4_--) {
						if (NodeDeque.aStringArray1591[i_4_] != null) {
							short i_5_ = 0;
							if ((Class296_Sub50.game == Class363.runescape) && (NodeDeque.aStringArray1591[i_4_].equalsIgnoreCase(TranslatableString.aClass120_1219.getTranslation(Class394.langID)))) {
								if (FileWorker.aBoolean3006 && ((Class296_Sub51_Sub11.localPlayer.combatLevel) < (class338_sub3_sub1_sub3_sub1.combatLevel)))
									i_5_ = (short) 2000;
								if ((Class296_Sub51_Sub11.localPlayer.team) != 0 && (class338_sub3_sub1_sub3_sub1.team != 0)) {
									if (class338_sub3_sub1_sub3_sub1.team != (Class296_Sub51_Sub11.localPlayer.team))
										i_5_ = (short) 0;
									else
										i_5_ = (short) 2000;
								} else if (class338_sub3_sub1_sub3_sub1.aBoolean6868)
									i_5_ = (short) 2000;
							} else if (Class294.aBooleanArray2690[i_4_])
								i_5_ = (short) 2000;
							short i_6_ = (short) (Class251.aShortArray2368[i_4_] + i_5_);
							int i_7_ = ((Class296_Sub39_Sub19.anIntArray6252[i_4_] != -1) ? Class296_Sub39_Sub19.anIntArray6252[i_4_] : Class106_Sub1.anInt3903);
							StaticMethods.method1008(-128, false, 0, i_7_, -1, NodeDeque.aStringArray1591[i_4_], 0, (long) class338_sub3_sub1_sub3_sub1.index, i_6_, true, (long) class338_sub3_sub1_sub3_sub1.index, "<col=ffffff>" + string, false);
						}
					}
				} else
					StaticMethods.method1008(30, false, 0, -1, 0, "<col=cccccc>" + string, 0, 0L, -1, false, (long) (class338_sub3_sub1_sub3_sub1.index), "", true);
				if (i == 2009) {
					if (!bool) {
						for (Class296_Sub39_Sub9 class296_sub39_sub9 = ((Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeFirst((byte) 115)); class296_sub39_sub9 != null; class296_sub39_sub9 = ((Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeNext(i ^ 0x430))) {
							if (class296_sub39_sub9.anInt6165 == 44) {
								class296_sub39_sub9.aString6171 = "<col=ffffff>" + string;
								break;
							}
						}
					}
				}
			}
		}
	}

	final void method3807(byte i) {
		Class338_Sub3_Sub4.method3571(anInt5456, false, anInt5457, 0, anInt5458, 114, anInt5459);
		int i_8_ = -54 % ((52 - i) / 52);
	}
}
