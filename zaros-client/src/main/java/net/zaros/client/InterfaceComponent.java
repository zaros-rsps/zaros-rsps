package net.zaros.client;

import net.zaros.client.configs.objtype.ObjType;
import net.zaros.client.configs.objtype.ObjTypeList;

/* Class51 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class InterfaceComponent {
	
	int anInt476;
	
	Object[] anObjectArray477;
	
	int textureID;
	
	int uid;
	
	int anInt480;
	
	private short[] aShortArray481;
	
	boolean aBoolean482;
	
	int locX;
	
	int[] anIntArray484;
	
	Object[] anObjectArray485;
	
	int anInt486;
	
	int anInt487;
	
	int anInt488;
	
	int height;
	
	int anInt490;
	
	Object[] anObjectArray491;
	
	Object[] anObjectArray492;
	
	int contentType = 0;
	
	Object[] anObjectArray494;
	
	byte[] aByteArray495;
	
	int anInt496;
	
	int anInt497;
	
	int modelType;
	
	boolean isFilled;
	
	Object[] anObjectArray500;
	
	int anInt501;
	
	Object[] anObjectArray502;
	
	Object[] anObjectArray503;
	
	Object[] anObjectArray504;
	
	int anInt505;
	
	Object[] anObjectArray506;
	
	boolean aBoolean507;
	
	byte[] aByteArray508;
	
	Object[] anObjectArray509;
	
	int anInt510;
	
	int anInt511;
	
	Object[] anObjectArray512;
	
	int scrollMaxW;
	
	boolean aBoolean514;
	
	int anInt515;
	
	int anInt516;
	
	int anInt517;
	
	Object[] anObjectArray518;
	
	int anInt519;
	
	int anInt520;
	
	int anInt521;
	
	int shadowColor;
	
	int anInt523;
	
	boolean aBoolean524;
	
	Object[] anObjectArray525;
	
	int[] anIntArray526;
	
	Object[] anObjectArray527;
	
	int anInt528;
	
	int anInt529;
	
	int anInt530;
	
	Object[] anObjectArray531;
	
	int modelID;
	
	String text;
	
	int width;
	
	boolean aBoolean535;
	
	int anInt536;
	
	Object[] anObjectArray537;
	
	InterfaceComponent[] aClass51Array538;
	
	Object[] anObjectArray539;
	
	Object[] anObjectArray540;
	
	Object[] anObjectArray541;
	
	int anInt542;
	
	private HashTable aClass263_543;
	
	Object[] anObjectArray544;
	
	int anInt545;
	
	boolean aBoolean546;
	
	boolean hasScripts;
	
	int anInt548;
	
	Object[] anObjectArray549;
	
	boolean aBoolean550;
	
	int fontID;
	
	byte aByte552;
	
	int anInt553;
	
	Animator aClass44_554;
	
	InterfaceComponent[] aClass51Array555;
	
	boolean hiden;
	
	Object[] anObjectArray557;
	
	int anInt558;
	
	int clickedItem;
	
	int anInt560;
	
	int anInt561;
	
	boolean aBoolean562;
	
	Object[] anObjectArray563;
	
	int[] anIntArray564;
	
	boolean flippedHorizontally;
	
	int anInt566;
	
	InterfaceComponentSettings settings_;
	
	Object[] anObjectArray568;
	
	int[] anIntArray569;
	
	Object[] anObjectArray570;
	
	int anInt571;
	
	int[] anIntArray572;
	
	int anInt573;
	
	private short[] aShortArray574;
	
	int scrollMaxH;
	
	int anInt576;
	
	String aString577;
	
	int anInt578;
	
	int anInt579;
	
	int borderThickness;
	
	int anInt581;
	
	Object[] anObjectArray582;
	
	Object[] anObjectArray583;
	
	Object[] anObjectArray584;
	
	Object[] anObjectArray585;
	
	private short[] aShortArray586;
	
	String[] actions;
	
	int anInt588;
	
	int anInt589;
	
	int anInt590;
	
	int anInt591;
	
	int anInt592;
	
	boolean aBoolean593;
	
	Object[] anObjectArray594;
	
	byte aByte595;
	
	int anInt596;
	
	Object[] anObjectArray597;
	
	int parentId;
	
	int[] anIntArray599;
	
	private byte[] aByteArray422 = { 83, 101, 108, 101, 99, 116 }; //u can hide more in other functions if you want
	
	int anInt600;
	
	int anInt601;
	
	String aString602;
	
	int type;
	
	byte aByte604;
	
	int anInt605;
	
	int anInt606;
	
	InterfaceComponent aClass51_607;
	
	Object[] anObjectArray608;
	
	int locY;
	
	boolean aBoolean610;
	
	int anInt611;
	
	int anInt612;
	
	boolean aBoolean613;
	
	int anInt614;
	
	int[] cursors;
	
	private short[] aShortArray616;
	
	boolean aBoolean617;
	
	boolean aBoolean618;
	
	int[] anIntArray619;
	
	String selectedAction;
	
	int textColour;
	
	int anInt622;
	
	int anInt623;
	
	int anInt624;
	
	int[] anIntArray625;
	
	byte aByte626;
	
	Object[] anObjectArray627;
	
	String componentName;
	
	Class338_Sub1 aClass338_Sub1_629;
	
	boolean isInventory;
	
	boolean flippedVertically;
	
	int anInt632;
	
	int anInt633;
	
	final Sprite method617(int i, ha var_ha) {
		Class41_Sub28.aBoolean3813 = false;
		long l = (((flippedHorizontally ? 1L : 0L) << 39) + (((flippedVertically ? 1L : 0L) << 38) + ((aBoolean546 ? 1L : 0L) << 35) + ((long) textureID + ((long) borderThickness << 36) - -((long) shadowColor << 40))));
		Sprite class397 = (Sprite) Class296_Sub39_Sub13.aClass113_6205.get(l);
		if (class397 != null) {
			return class397;
		}
		Class186 class186 = Class186.method1876(Class296_Sub51_Sub31.aClass138_6510, textureID, 0);
		if (class186 == null) {
			Class41_Sub28.aBoolean3813 = true;
			return null;
		}
		if (flippedVertically) {
			class186.method1875();
		}
		if (flippedHorizontally) {
			class186.method1879();
		}
		if (borderThickness <= 0) {
			if (shadowColor != 0) {
				class186.method1870(1);
			}
		} else {
			class186.method1870(borderThickness);
		}
		if (borderThickness >= 1) {
			class186.method1878(1);
		}
		if (borderThickness >= 2) {
			class186.method1878(16777215);
		}
		if (i != 16353) {
			return null;
		}
		if (shadowColor != 0) {
			class186.method1877(shadowColor | ~0xffffff);
		}
		class397 = var_ha.a(class186, true);
		Class296_Sub39_Sub13.aClass113_6205.putInternal(0, class397.method4087() * 4 * class397.method4092(), l, class397);
		return class397;
	}
	
	final void method618(int i, int i_0_) {
		if (i != 0) {
			unpack(null, 39);
		}
		if (aClass263_543 != null) {
			Node class296 = aClass263_543.get((long) i_0_);
			if (class296 != null) {
				class296.unlink();
			}
		}
	}
	
	final void method619(int i, byte i_1_, short i_2_, short i_3_) {
		if (i < 5) {
			if (i_1_ != 20) {
				readScript(null);
			}
			if (aShortArray481 == null) {
				aShortArray481 = new short[5];
				aShortArray586 = new short[5];
			}
			aShortArray481[i] = i_3_;
			aShortArray586[i] = i_2_;
		}
	}
	
	final Class262 method620(int i, Class279 class279, Class182 class182) {
		if (anInt542 == -1) {
			return null;
		}
		long l = ((long) anInt542 & 0xffffL | ((long) anInt571 << 48 & 65535L << 48 | ((long) anInt488 & 0xffffL) << 32 | ((long) anInt606 & 0xffffL) << 16));
		Class262 class262 = (Class262) Class41_Sub20.aClass113_3794.get(l);
		if (class262 == null) {
			class262 = class279.method2340(anInt571, true, class182, anInt488, anInt606, anInt542);
			Class41_Sub20.aClass113_3794.put(class262, l);
		}
		if (i != 17286) {
			return null;
		}
		return class262;
	}
	
	final void method621(int i, String string, byte i_4_) {
		if (actions == null || i >= actions.length) {
			String[] strings = new String[i + 1];
			if (actions != null) {
				for (int i_5_ = 0; i_5_ < actions.length; i_5_++) {
					strings[i_5_] = actions[i_5_];
				}
			}
			actions = strings;
		}
		if (i_4_ != -88) {
			parentId = 71;
		}
		if (string != null && string.equalsIgnoreCase("quest journals")) {
			string = "General Tab";
		}
		actions[i] = string;
	}
	
	private final int[] readTriggers(Packet in) {
		int count = in.g1();
		if (count == 0) {
			return null;
		}
		int[] trig = new int[count];
		for (int i = 0; count > i; i++) {
			trig[i] = in.g4();
		}
		return trig;
	}
	
	private final Object[] readScript(Packet buff) {
		int data = buff.g1();
		if (data == 0) {
			return null;
		}
		Object[] callArgs = new Object[data];
		for (int i = 0; i < data; i++) {
			int type = buff.g1();
			if (type == 0) {
				callArgs[i] = new Integer(buff.g4());
			} else if (type == 1) {
				callArgs[i] = buff.gstr();
			}
		}
		hasScripts = true;
		return callArgs;
	}
	
	final void method624(int i, String string, byte i_11_) {
		if (aClass263_543 == null) {
			aClass263_543 = new HashTable(16);
			aClass263_543.put((long) i, new StringNode(string));
		} else {
			StringNode class296_sub5 = (StringNode) aClass263_543.get((long) i);
			if (class296_sub5 != null) {
				class296_sub5.value = string;
			} else {
				aClass263_543.put((long) i, new StringNode(string));
			}
			if (i_11_ != 64) {
				method621(67, null, (byte) 105);
			}
		}
	}
	
	final void method625(Model class178, Class373 class373, ha var_ha, int i, int i_12_) {
		class178.method1718(class373);
		EmissiveTriangle[] class89s = class178.method1735();
		if (i < -55) {
			EffectiveVertex[] class232s = class178.method1729();
			if ((aClass338_Sub1_629 == null || aClass338_Sub1_629.aBoolean5177) && (class89s != null || class232s != null)) {
				aClass338_Sub1_629 = Class338_Sub1.method3442(i_12_, false);
			}
			if (aClass338_Sub1_629 != null) {
				aClass338_Sub1_629.method3452(var_ha, (long) i_12_, class89s, class232s, false);
			}
		}
	}
	
	final Model method627(NPCDefinitionLoader class136, IConfigsRegister interface17, ha var_ha, Class62 class62, PlayerEquipment class402, Animator class44, ClotchesLoader class24, int i, AnimationsLoader class310, boolean bool, Class286 class286, ObjTypeList class129) {
		Class41_Sub28.aBoolean3813 = false;
		if (modelType == 0) {
			return null;
		}
		if (modelType == 1 && modelID == -1) {
			return null;
		}
		if (modelType == 1) {
			int i_16_ = i;
			if (class44 != null) {
				i |= class44.method568(0);
			}
			long l = -1L;
			long[] ls = TextureOperation.someHashTable;
			if (aShortArray481 != null) {
				for (int i_17_ = 0; aShortArray481.length > i_17_; i_17_++) {
					l = ls[(int) ((l ^ (long) (aShortArray481[i_17_] >> 8)) & 0xffL)] ^ l >>> 8;
					l = (ls[(int) (((long) aShortArray481[i_17_] ^ l) & 0xffL)] ^ l >>> 8);
					l = l >>> 8 ^ ls[(int) ((l ^ (long) (aShortArray586[i_17_] >> 8)) & 0xffL)];
					l = l >>> 8 ^ ls[(int) (((long) aShortArray586[i_17_] ^ l) & 0xffL)];
				}
				i |= 0x4000;
			}
			if (aShortArray616 != null) {
				i |= 0x8000;
				for (int i_18_ = 0; aShortArray616.length > i_18_; i_18_++) {
					l = ls[(int) (((long) (aShortArray616[i_18_] >> 8) ^ l) & 0xffL)] ^ l >>> 8;
					l = l >>> 8 ^ ls[(int) ((l ^ (long) aShortArray616[i_18_]) & 0xffL)];
					l = l >>> 8 ^ ls[(int) ((l ^ (long) (aShortArray574[i_18_] >> 8)) & 0xffL)];
					l = l >>> 8 ^ ls[(int) (((long) aShortArray574[i_18_] ^ l) & 0xffL)];
				}
			}
			long l_19_ = (l & 0x3fffffffffL | ((long) var_ha.anInt1295 << 59 | (long) modelType << 54 | (long) modelID << 38));
			Model class178 = ((Model) Class338_Sub3_Sub1_Sub4.aClass113_6641.get(l_19_));
			if (class178 == null || var_ha.e(class178.ua(), i) != 0) {
				if (class178 != null) {
					i = var_ha.d(i, class178.ua());
				}
				Mesh class132 = Class296_Sub51_Sub1.fromJs5((Class226_Sub1.aClass138_4573), modelID, 0);
				if (class132 == null) {
					Class41_Sub28.aBoolean3813 = true;
					return null;
				}
				if (class132.version_number < 13) {
					class132.scale(2);
				}
				class178 = var_ha.a(class132, i, World.anInt1159, 64, 768);
				if (aShortArray481 != null) {
					for (int i_20_ = 0; aShortArray481.length > i_20_; i_20_++) {
						class178.ia(aShortArray481[i_20_], aShortArray586[i_20_]);
					}
				}
				if (aShortArray616 != null) {
					for (int i_21_ = 0; aShortArray616.length > i_21_; i_21_++) {
						class178.aa(aShortArray616[i_21_], aShortArray574[i_21_]);
					}
				}
				Class338_Sub3_Sub1_Sub4.aClass113_6641.put(class178, l_19_);
			}
			if (class44 != null) {
				class178 = class178.method1728((byte) 1, i, true);
				class44.method556(class178, 0, (byte) -63);
			}
			class178.s(i_16_);
			return class178;
		}
		if (modelType == 2) {
			Model class178 = class136.getDefinition(modelID).method1504(var_ha, class44, class286, i, interface17, 0);
			if (class178 == null) {
				Class41_Sub28.aBoolean3813 = true;
				return null;
			}
			return class178;
		}
		if (modelType == 3) {
			if (class402 == null) {
				return null;
			}
			Model class178 = class402.method4150(class310, class129, var_ha, class136, i, class44, class24, interface17, -3790);
			if (class178 == null) {
				Class41_Sub28.aBoolean3813 = true;
				return null;
			}
			return class178;
		}
		if (modelType == 4) {
			ObjType class158 = class129.list(modelID);
			Model class178 = class158.getModel(10, var_ha, class44, i, (byte) 4, class402);
			if (class178 == null) {
				Class41_Sub28.aBoolean3813 = true;
				return null;
			}
			return class178;
		}
		if (bool != true) {
			return null;
		}
		if (modelType == 6) {
			Model class178 = class136.getDefinition(modelID).method1496(null, i, var_ha, -144, null, 0, interface17, class44, class62, class286, null);
			if (class178 == null) {
				Class41_Sub28.aBoolean3813 = true;
				return null;
			}
			return class178;
		}
		if (modelType == 7) {
			if (class402 == null) {
				return null;
			}
			int i_22_ = modelID >>> 16;
			int i_23_ = modelID & 0xffff;
			int i_24_ = anInt545;
			Model class178 = class402.method4149(class24, i, i_23_, i_22_, i_24_, class44, class310, var_ha, (byte) 34);
			if (class178 == null) {
				Class41_Sub28.aBoolean3813 = true;
				return null;
			}
			return class178;
		}
		return null;
	}
	
	final void method628(int i, int i_25_, int i_26_) {
		if (aClass263_543 == null) {
			aClass263_543 = new HashTable(16);
			aClass263_543.put((long) i_25_, new IntegerNode(i_26_));
		} else {
			IntegerNode class296_sub16 = (IntegerNode) aClass263_543.get((long) i_25_);
			if (class296_sub16 == null) {
				aClass263_543.put((long) i_25_, new IntegerNode(i_26_));
			} else {
				class296_sub16.value = i_26_;
			}
			if (i <= 8) {
				unpack(null, 12);
			}
		}
	}
	
	final void method630(int i) {
		anObjectArray583 = null;
		anObjectArray537 = null;
		anObjectArray531 = null;
		anObjectArray539 = null;
		anObjectArray608 = null;
		anObjectArray504 = null;
		anObjectArray570 = null;
		anObjectArray557 = null;
		anObjectArray563 = null;
		anObjectArray492 = null;
		anObjectArray512 = null;
		anIntArray526 = null;
		anObjectArray541 = null;
		anObjectArray525 = null;
		anObjectArray544 = null;
		anObjectArray509 = null;
		anObjectArray627 = null;
		anObjectArray584 = null;
		anIntArray619 = null;
		anObjectArray585 = null;
		anObjectArray568 = null;
		anObjectArray500 = null;
		anObjectArray494 = null;
		anObjectArray485 = null;
		anObjectArray527 = null;
		anIntArray564 = null;
		anObjectArray477 = null;
		if (i != 4) {
			flippedVertically = false;
		}
		anIntArray625 = null;
		anObjectArray540 = null;
		anObjectArray549 = null;
		anObjectArray503 = null;
		anObjectArray502 = null;
		anObjectArray582 = null;
		anObjectArray594 = null;
		anObjectArray518 = null;
		anIntArray484 = null;
		anObjectArray491 = null;
	}
	
	final int method631(int i, int i_28_, int i_29_) {
		if (aClass263_543 == null) {
			return i_29_;
		}
		if (i_28_ < 80) {
			anObjectArray502 = null;
		}
		IntegerNode class296_sub16 = (IntegerNode) aClass263_543.get((long) i);
		if (class296_sub16 == null) {
			return i_29_;
		}
		return class296_sub16.value;
	}
	
	final String method632(int i, String string, byte i_30_) {
		if (aClass263_543 == null) {
			return string;
		}
		StringNode class296_sub5 = (StringNode) aClass263_543.get((long) i);
		if (class296_sub5 == null) {
			return string;
		}
		if (i_30_ < 19) {
			method631(-26, 38, 20);
		}
		return class296_sub5.value;
	}
	
	final Class98 method633(byte i, ha var_ha) {
		long l = (long) anInt592 & 0xffffffffL | (long) uid << 32;
		Class98 class98 = (Class98) Class153.aClass113_1576.get(l);
		if (class98 != null) {
			if (class98.anInt1058 != textureID) {
				class98 = null;
				Class153.aClass113_1576.remove(l);
			}
			if (class98 != null) {
				return class98;
			}
		}
		Class186 class186 = Class186.method1876(Class296_Sub51_Sub31.aClass138_6510, textureID, 0);
		if (class186 == null) {
			return null;
		}
		int i_31_ = class186.anInt1903 + class186.anInt1899 + class186.anInt1907;
		int i_32_ = class186.anInt1902 + (class186.anInt1904 + class186.anInt1906);
		int[] is = new int[i_32_];
		int[] is_33_ = new int[i_32_];
		for (int i_34_ = 0; i_34_ < class186.anInt1904; i_34_++) {
			int i_35_ = 0;
			int i_36_ = class186.anInt1899;
			for (int i_37_ = 0; i_37_ < class186.anInt1899; i_37_++) {
				if (class186.aByteArray1901[i_37_ + class186.anInt1899 * i_34_] != 0) {
					i_35_ = i_37_;
					break;
				}
			}
			for (int i_38_ = class186.anInt1899 - 1; i_38_ >= i_35_; i_38_--) {
				if (class186.aByteArray1901[class186.anInt1899 * i_34_ + i_38_] != 0) {
					i_36_ = i_38_ + 1;
					break;
				}
			}
			is[class186.anInt1906 + i_34_] = class186.anInt1903 + i_35_;
			is_33_[i_34_ + class186.anInt1906] = i_36_ - i_35_;
		}
		aa var_aa = var_ha.a(i_31_, i_32_, is, is_33_);
		if (var_aa == null) {
			return null;
		}
		class98 = new Class98(i_31_, i_32_, is_33_, is, var_aa, textureID);
		Class153.aClass113_1576.put(class98, l);
		if (i > -47) {
			anObjectArray583 = null;
		}
		return class98;
	}
	
	final void method635(int i, int i_40_, byte i_41_) {
		if (cursors == null || i >= cursors.length) {
			int[] is = new int[i + 1];
			if (cursors != null) {
				for (int i_42_ = 0; cursors.length > i_42_; i_42_++) {
					is[i_42_] = cursors[i_42_];
				}
				for (int i_43_ = cursors.length; i_43_ < i; i_43_++) {
					is[i_43_] = -1;
				}
			}
			cursors = is;
		}
		if (i_41_ < 17) {
			method620(80, null, null);
		}
		cursors[i] = i_40_;
	}
	
	final Class55 method636(int i, ha var_ha) {
		Class55 class55 = Class296_Sub51_Sub30.method3168(var_ha, (byte) -101, aBoolean617, fontID, false);
		Class41_Sub28.aBoolean3813 = class55 == null;
		if (i < 107) {
			anObjectArray537 = null;
		}
		return class55;
	}
	
	final void unpack(Packet in, int i) {
		int unkn = in.g1();
		if (unkn == 255) {
			unkn = -1;
		}
		type = in.g1();
		if ((type & 0x80) != 0) {
			type &= 0x7f;
			aString602 = in.gstr();
		}
		contentType = in.g2();
		locX = in.g2b();
		locY = in.g2b();
		width = in.g2();
		height = in.g2();
		aByte552 = in.g1b();
		aByte604 = in.g1b();
		aByte626 = in.g1b();
		aByte595 = in.g1b();
		parentId = in.g2();
		int parId = parentId;
		if (parentId == 65535) {
			parentId = -1;
		} else {
			parentId = parentId + (uid & ~0xffff);
		}
		int flag = in.g1();
		hiden = (flag & 0x1) != 0;
		if (unkn >= 0) {
			aBoolean610 = (flag & 0x2) != 0;
		}
		if (type == 0) {
			scrollMaxH = in.g2();
			scrollMaxW = in.g2();
			if (unkn < 0) {
				aBoolean610 = in.g1() == 1;
			}
		}
		if (type == 5) {
			textureID = in.g4();
			anInt573 = in.g2();
			int flg = in.g1();
			aBoolean546 = (flg & 0x2) != 0;
			aBoolean562 = (flg & 0x1) != 0;
			anInt517 = in.g1();
			borderThickness = in.g1();
			shadowColor = in.g4();
			flippedVertically = in.g1() == 1;
			flippedHorizontally = in.g1() == 1;
			textColour = in.g4();
			if (unkn >= 3) {
				aBoolean550 = in.g1() == 1;
			}
		}
		int anInt3099 = parentId >> 16;
		int anInt9832 = uid & 0xffff;
		if (anInt3099 == 596 && (/*anInt9832 == 61 ||*/ anInt9832 == 1 || (anInt9832 >= 48 && anInt9832 <= 54))) {
			hiden = true;
		}
		if (anInt3099 == 596 && anInt9832 == 4) {
			locY = -40;
			height = 230;
		}
		if (anInt3099 == 596 && (anInt9832 == 61)) {
			locY = locY - 70;
		}
		if (anInt3099 == 596 && parId == 6) {
			locY = locY + 40;
		}
		if (type == 6) {
			modelType = 1;
			modelID = in.g2();
			if (modelID == 65535) {
				modelID = -1;
			}
			int i_47_ = in.g1();
			aBoolean507 = (i_47_ & 0x4) == 4;
			boolean bool = (i_47_ & 0x1) == 1;
			aBoolean593 = (i_47_ & 0x2) == 2;
			aBoolean535 = (i_47_ & 0x8) == 8;
			if (bool) {
				anInt591 = in.g2b();
				anInt487 = in.g2b();
				anInt612 = in.g2();
				anInt515 = in.g2();
				anInt548 = in.g2();
				anInt520 = in.g2();
			} else if (aBoolean593) {
				anInt591 = in.g2b();
				anInt487 = in.g2b();
				anInt505 = in.g2b();
				anInt612 = in.g2();
				anInt515 = in.g2();
				anInt548 = in.g2();
				anInt520 = in.g2b();
			}
			anInt497 = in.g2();
			if (anInt497 == 65535) {
				anInt497 = -1;
			}
			if (aByte552 != 0) {
				anInt516 = in.g2();
			}
			if (aByte604 != 0) {
				anInt486 = in.g2();
			}
		}
		if (type == 4) {
			fontID = in.g2();
			if (fontID == 65535) {
				fontID = -1;
			}
			if (unkn >= 2) {
				aBoolean617 = in.g1() == 1;
			}
			text = in.gstr();
			anInt589 = in.g1();
			anInt480 = in.g1();
			anInt600 = in.g1();
			isInventory = in.g1() == 1;
			textColour = in.g4();
			anInt517 = in.g1();
			if (unkn >= 0) {
				anInt510 = in.g1();
			}
			
			if (text.toLowerCase().contains("runescape")) {
				text = text.replace("RuneScape", ClientUtility.NAME).replace("runescape", ClientUtility.NAME).replace("Runescape", ClientUtility.NAME);
			} else if (text.equalsIgnoreCase("Email Address (Login)")) {
				text = "Username (Login)";
			} else if (text.equalsIgnoreCase("Retype Email Address")) {
				text = "Retype Username";
			}
		}
		if (type == 3) {
			textColour = in.g4();
			isFilled = in.g1() == 1;
			anInt517 = in.g1();
		}
		if (type == 9) {
			anInt476 = in.g1();
			textColour = in.g4();
			aBoolean514 = in.g1() == 1;
		}
		int settings = in.readUnsignedMedInt();
		int i_49_ = in.g1();
		if (i_49_ != 0) {
			anIntArray572 = new int[11];
			aByteArray508 = new byte[11];
			aByteArray495 = new byte[11];
			for (/**/; i_49_ != 0; i_49_ = in.g1()) {
				int i_50_ = (i_49_ >> 4) - 1;
				i_49_ = i_49_ << 8 | in.g1();
				i_49_ &= 0xfff;
				if (i_49_ == 4095) {
					i_49_ = -1;
				}
				byte i_51_ = in.g1b();
				if (i_51_ != 0) {
					aBoolean482 = true;
				}
				byte i_52_ = in.g1b();
				anIntArray572[i_50_] = i_49_;
				aByteArray508[i_50_] = i_51_;
				aByteArray495[i_50_] = i_52_;
			}
		}
		componentName = in.gstr();
		int i_53_ = in.g1();
		int optionAmount = i_53_ & 0xf;
		int interfaceId = parentId >> 16;
		
		if (optionAmount == 0 && locX == 22 && width == (2 * 150) + 29) {
			//			System.out.println("No Options: " + i_25_ + " : " + interfaceId);
			optionAmount = 1;
			i_53_ = 1;
			settings = 2;
			actions = new String[1];
			actions[0] = "";
			for (int k = 0; k < aByteArray422.length; k++) {
				actions[0] += (char) aByteArray422[k];
			}
		} else if (optionAmount > 0) {
			actions = new String[optionAmount];
			for (int idx = 0; idx < optionAmount; idx++) {
				String option = in.gstr();
				if (option.equalsIgnoreCase("task system")) {
					option = "Information Tab";
				} else if (option.equalsIgnoreCase("quest journals")) {
					option = "PvP Tab";
				}
				actions[idx] = option;
			}
		}
		if (interfaceId == 1144) {
			for (int idx = 0; idx < optionAmount; idx++) {
				actions[idx] = "Select";
			}
		}
		
		int i_55_ = i_53_ >> 4;
		if (i_55_ > 0) {
			int i_57_ = in.g1();
			cursors = new int[i_57_ + 1];
			for (int i_58_ = 0; cursors.length > i_58_; i_58_++) {
				cursors[i_58_] = -1;
			}
			cursors[i_57_] = in.g2();
		}
		if (i_55_ > 1) {
			int i_59_ = in.g1();
			cursors[i_59_] = in.g2();
		}
		aString577 = in.gstr();
		if (aString577.equals("")) {
			aString577 = null;
		}
		anInt579 = in.g1();
		anInt523 = in.g1();
		anInt622 = in.g1();
		selectedAction = in.gstr();
		int i_60_ = -1;
		if (Class296_Sub51_Sub23.method3140((byte) 75, settings) != 0) {
			i_60_ = in.g2();
			anInt519 = in.g2();
			if (i_60_ == 65535) {
				i_60_ = -1;
			}
			if (anInt519 == 65535) {
				anInt519 = -1;
			}
			anInt501 = in.g2();
			if (anInt501 == 65535) {
				anInt501 = -1;
			}
		}
		if (unkn >= 0) {
			anInt611 = in.g2();
			if (anInt611 == 65535) {
				anInt611 = -1;
			}
		}
		settings_ = new InterfaceComponentSettings(settings, i_60_);
		if (i >= (unkn ^ 0xffffffff)) {
			int i_61_ = in.g1();
			for (int i_62_ = 0; i_61_ > i_62_; i_62_++) {
				int i_63_ = in.readUnsignedMedInt();
				int i_64_ = in.g4();
				aClass263_543.put((long) i_63_, new IntegerNode(i_64_));
			}
			int i_65_ = in.g1();
			for (int i_66_ = 0; i_65_ > i_66_; i_66_++) {
				int i_67_ = in.readUnsignedMedInt();
				String string = in.gjstr2();
				aClass263_543.put((long) i_67_, new StringNode(string));
			}
		}
		anObjectArray492 = readScript(in);
		anObjectArray584 = readScript(in);
		anObjectArray503 = readScript(in);
		anObjectArray557 = readScript(in);
		anObjectArray512 = readScript(in);
		anObjectArray594 = readScript(in);
		anObjectArray541 = readScript(in);
		anObjectArray477 = readScript(in);
		anObjectArray540 = readScript(in);
		anObjectArray585 = readScript(in);
		if (unkn >= 0) {
			anObjectArray494 = readScript(in);
		}
		anObjectArray608 = readScript(in);
		anObjectArray563 = readScript(in);
		anObjectArray531 = readScript(in);
		anObjectArray537 = readScript(in);
		anObjectArray582 = readScript(in);
		anObjectArray570 = readScript(in);
		anObjectArray500 = readScript(in);
		anObjectArray527 = readScript(in);
		anObjectArray544 = readScript(in);
		anObjectArray525 = readScript(in);
		anIntArray484 = readTriggers(in);
		anIntArray526 = readTriggers(in);
		anIntArray625 = readTriggers(in);
		anIntArray564 = readTriggers(in);
		anIntArray619 = readTriggers(in);
	}
	
	final void method638(short i, short i_68_, byte i_69_, int i_70_) {
		if (i_69_ != -34) {
			aBoolean562 = true;
		}
		if (i_70_ < 5) {
			if (aShortArray616 == null) {
				aShortArray616 = new short[5];
				aShortArray574 = new short[5];
			}
			aShortArray616[i_70_] = i_68_;
			aShortArray574[i_70_] = i;
		}
	}
	
	static final boolean loadInterface(int interfaceID) {
		if (Class219_Sub1.loadedInterfaces[interfaceID]) {
			return true;
		}
		if (!BillboardRaw.interfacesFS.hasFileBuffer((byte) -71, interfaceID)) {
			return false;
		}
		int numComponents = BillboardRaw.interfacesFS.getLastFileId(interfaceID);
		if (numComponents == 0) {
			Class219_Sub1.loadedInterfaces[interfaceID] = true;
			return true;
		}
		if (Class192.openedInterfaceComponents[interfaceID] == null) {
			Class192.openedInterfaceComponents[interfaceID] = new InterfaceComponent[numComponents];
		}
		for (int i = 0; i < numComponents; i++) {
			if (Class192.openedInterfaceComponents[interfaceID][i] == null) {
				byte[] data = BillboardRaw.interfacesFS.getFile(interfaceID, i);
				if (data != null) {
					InterfaceComponent component = (Class192.openedInterfaceComponents[interfaceID][i] = new InterfaceComponent());
					component.uid = i + (interfaceID << 16);
					if (data[0] == -1) {
						component.unpack(new Packet(data), -1);
					} else {
						throw new IllegalStateException("if1");
					}
				}
			}
		}
		Class219_Sub1.loadedInterfaces[interfaceID] = true;
		return true;
	}
	
	static final InterfaceComponent getInterfaceComponent(int uid) {
		int interfaceID = uid >> 16;
		int componentID = uid & 0xffff;
		if (Class192.openedInterfaceComponents[interfaceID] == null || Class192.openedInterfaceComponents[interfaceID][componentID] == null) {
			boolean sucess = InterfaceComponent.loadInterface(interfaceID);
			if (!sucess) {
				return null;
			}
		}
		return Class192.openedInterfaceComponents[interfaceID][componentID];
	}
	
	public InterfaceComponent() {
		uid = -1;
		height = 0;
		anInt497 = -1;
		anInt516 = 0;
		textureID = -1;
		text = "";
		anInt521 = 0;
		aBoolean535 = false;
		anInt528 = 0;
		anInt505 = 0;
		anInt510 = 0;
		anInt487 = 0;
		anInt486 = 0;
		aBoolean562 = false;
		anInt476 = 1;
		anInt560 = 0;
		anInt517 = 0;
		aBoolean524 = false;
		scrollMaxW = 0;
		aBoolean514 = false;
		hiden = false;
		locX = 0;
		settings_ = GameType.aClass296_Sub31_343;
		anInt545 = -1;
		anInt558 = -1;
		anInt480 = 0;
		anInt561 = -1;
		isFilled = false;
		aBoolean507 = false;
		borderThickness = 0;
		fontID = -1;
		anInt542 = -1;
		aBoolean550 = true;
		anInt589 = 0;
		anInt592 = -1;
		aByte552 = (byte) 0;
		anInt501 = -1;
		anInt520 = 100;
		anInt588 = 0;
		anInt530 = 1;
		shadowColor = 0;
		aClass51_607 = null;
		anInt511 = 0;
		hasScripts = false;
		anInt579 = 0;
		anInt490 = 2;
		clickedItem = -1;
		locY = 0;
		aBoolean610 = false;
		anInt553 = 0;
		anInt605 = 0;
		aByte595 = (byte) 0;
		width = 0;
		anInt596 = 0;
		anInt581 = 0;
		anInt519 = -1;
		scrollMaxH = 0;
		anInt601 = -1;
		aBoolean617 = true;
		anInt523 = 0;
		modelType = 1;
		anInt600 = 0;
		aBoolean482 = false;
		textColour = 0;
		parentId = -1;
		anInt566 = 1;
		anInt591 = 0;
		anInt496 = 0;
		componentName = "";
		anInt622 = Class225.anInt2181;
		anInt576 = -1;
		selectedAction = "";
		anInt573 = 0;
		anInt614 = 0;
		aBoolean613 = false;
		anInt578 = 0;
		aBoolean546 = false;
		isInventory = false;
		anInt632 = 0;
		anInt624 = 0;
		aBoolean618 = false;
		aByte626 = (byte) 0;
		anInt623 = 0;
		anInt612 = 0;
		aByte604 = (byte) 0;
		anInt515 = 0;
		anInt548 = 0;
		anInt633 = -1;
		anInt611 = -1;
	}
}