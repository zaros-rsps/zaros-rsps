package net.zaros.client;

/* Class296_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub2 extends Node {
	static boolean aBoolean4588 = false;
	static Sprite aClass397_4589;
	static int anInt4590 = 0;
	int anInt4591;
	int anInt4592;

	public static void method2435(int i) {
		aClass397_4589 = null;
		if (i > -8)
			aBoolean4588 = true;
	}

	static final void method2436(int i, float f, Class226 class226, int i_0_, byte[] is, boolean bool, int i_1_, int i_2_, float f_3_, float f_4_, int i_5_, float f_6_, int i_7_, float f_8_) {
		int i_9_ = i_1_ * i_5_;
		float[] fs = new float[i_9_];
		if (bool)
			aClass397_4589 = null;
		for (int i_10_ = 0; i_10_ < i_2_; i_10_++) {
			int i_11_ = i_7_;
			class226.method2088(f_4_ * 127.0F, f_3_ / (float) i_5_, f_6_ / (float) i_0_, i_5_, i_0_, -74, i_1_, i, fs, 0, f_8_ / (float) i_1_);
			f_8_ *= 2.0F;
			f_3_ *= 2.0F;
			f_4_ *= f;
			for (int i_12_ = 0; i_9_ > i_12_; i_12_++) {
				is[i_11_] += fs[i_12_];
				i_11_++;
			}
			f_6_ *= 2.0F;
		}
		int i_13_ = i_7_;
		for (int i_14_ = 0; i_14_ < i_9_; i_14_++) {
			is[i_13_] = (byte) (is[i_13_] + 127);
			i_13_++;
		}
	}

	Class296_Sub2(int i, int i_15_) {
		anInt4591 = i;
		anInt4592 = i_15_;
	}
}
