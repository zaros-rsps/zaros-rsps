package net.zaros.client;

/* Class296_Sub39_Sub9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub9 extends Queuable {
	long aLong6161;
	long aLong6162;
	static Class381 aClass381_6163;
	int anInt6164;
	int anInt6165;
	String aString6166;
	int anInt6167;
	String aString6168;
	boolean aBoolean6170;
	String aString6171;
	int anInt6172;
	boolean aBoolean6173;
	int anInt6174;
	boolean aBoolean6175;

	static final byte method2827(char c, byte i) {
		try {
			int i_21_ = -40 % ((i - 9) / 54);
			byte i_22_;
			if ((c <= 0 || c >= '\u0080') && (c < '\u00a0' || c > '\u00ff')) {
				if (c != '\u20ac') {
					if (c == '\u201a')
						i_22_ = (byte) -126;
					else if (c != '\u0192') {
						if (c == '\u201e')
							i_22_ = (byte) -124;
						else if (c != '\u2026') {
							if (c != '\u2020') {
								if (c != '\u2021') {
									if (c == '\u02c6')
										i_22_ = (byte) -120;
									else if (c != '\u2030') {
										if (c != '\u0160') {
											if (c == '\u2039')
												i_22_ = (byte) -117;
											else if (c == '\u0152')
												i_22_ = (byte) -116;
											else if (c == '\u017d')
												i_22_ = (byte) -114;
											else if (c == '\u2018')
												i_22_ = (byte) -111;
											else if (c != '\u2019') {
												if (c == '\u201c')
													i_22_ = (byte) -109;
												else if (c != '\u201d') {
													if (c == '\u2022')
														i_22_ = (byte) -107;
													else if (c == '\u2013')
														i_22_ = (byte) -106;
													else if (c != '\u2014') {
														if (c == '\u02dc')
															i_22_ = (byte) -104;
														else if (c != '\u2122') {
															if (c == '\u0161')
																i_22_ = (byte) -102;
															else if (c != '\u203a') {
																if (c != '\u0153') {
																	if (c == '\u017e')
																		i_22_ = (byte) -98;
																	else if (c == '\u0178')
																		i_22_ = (byte) -97;
																	else
																		i_22_ = (byte) 63;
																} else
																	i_22_ = (byte) -100;
															} else
																i_22_ = (byte) -101;
														} else
															i_22_ = (byte) -103;
													} else
														i_22_ = (byte) -105;
												} else
													i_22_ = (byte) -108;
											} else
												i_22_ = (byte) -110;
										} else
											i_22_ = (byte) -118;
									} else
										i_22_ = (byte) -119;
								} else
									i_22_ = (byte) -121;
							} else
								i_22_ = (byte) -122;
						} else
							i_22_ = (byte) -123;
					} else
						i_22_ = (byte) -125;
				} else
					i_22_ = (byte) -128;
			} else
				i_22_ = (byte) c;
			return i_22_;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "mv.A(" + c + ',' + i + ')');
		}
	}

	static final String method2828(long l, int i, byte i_23_) {
		Class188.method1889(l, i_23_ ^ 0x690b);
		if (i_23_ != 125)
			PlayerUpdate.visiblePlayersCount = 18;
		int i_24_ = Class296_Sub38.aCalendar4889.get(5);
		int i_25_ = Class296_Sub38.aCalendar4889.get(2);
		int i_26_ = Class296_Sub38.aCalendar4889.get(1);
		if (i == 3)
			return Exception_Sub1.method137(l, (byte) -31, i);
		return (Integer.toString(i_24_ / 10) + i_24_ % 10 + "-" + Class296_Sub24.aStringArrayArray4761[i][i_25_] + "-" + i_26_);
	}

	public static void method2829(byte i) {
		if (i > 110)
			aClass381_6163 = null;
	}

	static final boolean method2830(byte i, int i_27_) {
		if (i < 78)
			aClass381_6163 = null;
		if (i_27_ != 3 && i_27_ != 4)
			return false;
		return true;
	}

	static final boolean method2831(int i, int i_28_, int i_29_) {
		if (i <= 38)
			method2830((byte) 51, 24);
		if ((i_28_ & 0x800) == 0)
			return false;
		return true;
	}

	Class296_Sub39_Sub9(String string, String string_30_, int i, int i_31_, int i_32_, long l, int i_33_, int i_34_, boolean bool, boolean bool_35_, long l_36_, boolean bool_37_) {
		anInt6167 = i_32_;
		aBoolean6173 = bool_37_;
		aString6166 = string_30_;
		anInt6165 = i_31_;
		aBoolean6175 = bool;
		anInt6174 = i;
		aLong6162 = l;
		anInt6172 = i_33_;
		aString6168 = string;
		aBoolean6170 = bool_35_;
		anInt6164 = i_34_;
		aLong6161 = l_36_;
	}
}
