package net.zaros.client;

public class NodeDeque {
	static Class161 aClass161_1585 = new Class161(9, 0, 4, 1);
	Node aClass296_1586 = new Node();
	static boolean js = false;
	static short aShort1589 = 205;
	private Node aClass296_1590;
	static String[] aStringArray1591 = new String[8];
	static int anInt1592 = 0;

	public Node removeFirst(byte i) {
		Node class296 = aClass296_1586.next;
		if (i < 111)
			aClass161_1585 = null;
		if (class296 == aClass296_1586) {
			aClass296_1590 = null;
			return null;
		}
		aClass296_1590 = class296.next;
		return class296;
	}

	public Node removeNext(int i) {
		Node class296 = aClass296_1590;
		if (class296 == aClass296_1586) {
			aClass296_1590 = null;
			return null;
		}
		if (i != 1001)
			method1577((byte) -119);
		aClass296_1590 = class296.next;
		return class296;
	}

	public Node method1569(int i) {
		Node class296 = aClass296_1586.prev;
		if (class296 == aClass296_1586) {
			aClass296_1590 = null;
			return null;
		}
		aClass296_1590 = class296.prev;
		int i_0_ = -128 / ((54 - i) / 53);
		return class296;
	}

	public void method1570(boolean bool, Node class296) {
		if (class296.prev != null)
			class296.unlink();
		if (bool != true)
			aStringArray1591 = null;
		class296.prev = aClass296_1586;
		class296.next = aClass296_1586.next;
		class296.prev.next = class296;
		class296.next.prev = class296;
	}

	public void method1571(NodeDeque class155_1_, int i) {
		method1578(class155_1_, i ^ ~0x7d, aClass296_1586.next);
		if (i != 2)
			method1578(null, -10, null);
	}

	static public long method1572(int i) {
		if (i < 2)
			aStringArray1591 = null;
		return Class368_Sub20.aClass217_5550.method2036(true);
	}

	public Node method1573(int i) {
		Node class296 = aClass296_1586.next;
		if (aClass296_1586 == class296)
			return null;
		class296.unlink();
		if (i != 1)
			addLast((byte) 45, null);
		return class296;
	}

	public void addLast(byte i, Node class296) {
		if (class296.prev != null)
			class296.unlink();
		class296.prev = aClass296_1586.prev;
		int i_2_ = 123 / ((i - 41) / 57);
		class296.next = aClass296_1586;
		class296.prev.next = class296;
		class296.next.prev = class296;
	}

	static public void method1575(int i, int i_3_, int i_4_, int i_5_, int i_6_, byte i_7_, int i_8_) {
		int i_9_ = PlayerUpdate.visiblePlayersCount;
		AnimBase.anInt4767 = 0;
		int[] is = PlayerUpdate.visiblePlayersIndexes;
		int i_10_;
		if (za_Sub1.anInt6554 != 3)
			i_10_ = Class367.npcsCount + i_9_;
		else
			i_10_ = Class379.aClass302Array3624.length;
		for (int i_11_ = 0; i_11_ < i_10_; i_11_++) {
			NPCDefinition class147 = null;
			Mobile class338_sub3_sub1_sub3;
			if (za_Sub1.anInt6554 != 3) {
				if (i_9_ > i_11_)
					class338_sub3_sub1_sub3 = (PlayerUpdate.visiblePlayers[is[i_11_]]);
				else {
					class338_sub3_sub1_sub3 = (((NPCNode) (Class41_Sub18.localNpcs
							.get((long) ReferenceTable.npcIndexes[i_11_ - i_9_]))).value);
					class147 = ((NPC) class338_sub3_sub1_sub3).definition;
					if (class147.configData != null) {
						class147 = class147.getNPCDefinitionByConfigID((Class16_Sub3_Sub1.configsRegister));
						if (class147 == null)
							continue;
					}
				}
				if (class338_sub3_sub1_sub3.anInt6766 < 0
						|| ((class338_sub3_sub1_sub3.anInt6781 != Class296_Sub2.anInt4590)
								&& ((Class296_Sub51_Sub11.localPlayer.z) != class338_sub3_sub1_sub3.z)))
					continue;
			} else {
				Class302 class302 = Class379.aClass302Array3624[i_11_];
				if (!class302.aBoolean2716)
					continue;
				class338_sub3_sub1_sub3 = class302.method3262(0);
				if (Class296_Sub2.anInt4590 != class338_sub3_sub1_sub3.anInt6781)
					continue;
				if (class302.anInt2719 >= 0) {
					class147 = ((NPC) class338_sub3_sub1_sub3).definition;
					if (class147.configData != null) {
						class147 = class147.getNPCDefinitionByConfigID((Class16_Sub3_Sub1.configsRegister));
						if (class147 == null)
							continue;
					}
				}
			}
			Class338_Sub3_Sub3.method3558(class338_sub3_sub1_sub3, -7966, i_8_ >> 1, i_5_ >> 1, i, i_4_,
					class338_sub3_sub1_sub3.method3513((byte) -124));
			if (StaticMethods.anIntArray4629[0] >= 0) {
				if (class338_sub3_sub1_sub3.method3494((byte) -125)) {
					Class79 class79 = class338_sub3_sub1_sub3.method3497(77);
					if (class79 != null && Class47.anInt444 > AnimBase.anInt4767) {
						Class47.anIntArray449[AnimBase.anInt4767] = Class304.aClass92_2729.method851(-100,
								class79.method804(15)) / 2;
						Class47.anIntArray448[AnimBase.anInt4767] = StaticMethods.anIntArray4629[0];
						Class47.anIntArray451[AnimBase.anInt4767] = StaticMethods.anIntArray4629[1];
						Class47.aClass79Array447[AnimBase.anInt4767] = class79;
						AnimBase.anInt4767++;
					}
				}
				int i_12_ = StaticMethods.anIntArray4629[1] + i_3_;
				if (class338_sub3_sub1_sub3.aBoolean6774 || Class29.anInt307 >= class338_sub3_sub1_sub3.anInt6779)
					i_12_ -= Math.max(Class304.aClass92_2729.anInt989, Class69_Sub4.aClass397Array5732[0].method4092());
				else {
					int i_13_ = -1;
					int i_14_ = 1;
					if (class147 == null) {
						Player class338_sub3_sub1_sub3_sub1 = (PlayerUpdate.visiblePlayers[is[i_11_]]);
						i_13_ = (class338_sub3_sub1_sub3.method3516(false).anInt2572);
						if (class338_sub3_sub1_sub3_sub1.aBoolean6879)
							i_14_ = 2;
					} else {
						i_13_ = class147.anInt1485;
						if (i_13_ == -1)
							i_13_ = (class338_sub3_sub1_sub3.method3516(false).anInt2572);
					}
					Sprite[] class397s = Class69_Sub4.aClass397Array5732;
					if (i_13_ != -1) {
						Sprite[] class397s_15_ = (Sprite[]) Class121.aClass113_1269.get((long) i_13_);
						if (class397s_15_ == null) {
							Class186[] class186s = Class186.method1871((Class205_Sub2.fs8), i_13_, 0);
							if (class186s != null) {
								class397s_15_ = new Sprite[class186s.length];
								for (int i_16_ = 0; i_16_ < class186s.length; i_16_++)
									class397s_15_[i_16_] = Class41_Sub13.aHa3774.a(class186s[i_16_], true);
								Class121.aClass113_1269.put(class397s_15_, (long) i_13_);
							}
						}
						if (class397s_15_ != null && class397s_15_.length >= 2)
							class397s = class397s_15_;
					}
					if (class397s.length <= i_14_)
						i_14_ = 1;
					Sprite class397 = class397s[0];
					Sprite class397_17_ = class397s[i_14_];
					i_12_ -= Math.max(Class304.aClass92_2729.anInt989, class397.method4092());
					int i_18_ = (StaticMethods.anIntArray4629[0] + (i_6_ - (class397.method4087() >> 1)));
					int i_19_ = (class397.method4087() * class338_sub3_sub1_sub3.anInt6768 / 255);
					int i_20_ = class397.method4092();
					if (class338_sub3_sub1_sub3.anInt6768 > 0 && i_19_ < 2)
						i_19_ = 2;
					class397.method4096(i_18_, i_12_);
					Class41_Sub13.aHa3774.T(i_18_, i_12_, i_19_ + i_18_, i_12_ + i_20_);
					class397_17_.method4096(i_18_, i_12_);
					Class41_Sub13.aHa3774.KA(i_6_, i_3_, i_6_ + i_8_, i_5_ + i_3_);
					Class368_Sub8.method3838(false, i_18_, i_12_, i_20_ + i_12_, i_18_ + class397.method4099());
				}
				i_12_ -= 2;
				if (!class338_sub3_sub1_sub3.aBoolean6774) {
					if (class338_sub3_sub1_sub3.anInt6799 > Class29.anInt307) {
						Sprite class397 = (Class368_Sub10.aClass397Array5482[(!class338_sub3_sub1_sub3.aBoolean6764
								? 0
								: 2)]);
						Sprite class397_21_ = (Class368_Sub10.aClass397Array5482[class338_sub3_sub1_sub3.aBoolean6764
								? 3
								: 1]);
						int i_22_ = -1;
						if (class338_sub3_sub1_sub3 instanceof NPC) {
							i_22_ = class147.anInt1465;
							if (i_22_ == -1)
								i_22_ = (class338_sub3_sub1_sub3.method3516(false).anInt2557);
						} else
							i_22_ = (class338_sub3_sub1_sub3.method3516(false).anInt2557);
						if (i_22_ != -1) {
							Sprite[] class397s = ((Sprite[]) Class296_Sub15_Sub1.aClass113_5994.get((long) i_22_));
							if (class397s == null) {
								Class186[] class186s = Class186.method1871((Class205_Sub2.fs8), i_22_, 0);
								if (class186s != null) {
									class397s = new Sprite[class186s.length];
									for (int i_23_ = 0; i_23_ < class186s.length; i_23_++)
										class397s[i_23_] = Class41_Sub13.aHa3774.a(class186s[i_23_], true);
									Class296_Sub15_Sub1.aClass113_5994.put(class397s, (long) i_22_);
								}
							}
							if (class397s != null && class397s.length == 4) {
								class397_21_ = class397s[(class338_sub3_sub1_sub3.aBoolean6764) ? 3 : 1];
								class397 = class397s[!(class338_sub3_sub1_sub3.aBoolean6764) ? 0 : 2];
							}
						}
						int i_24_ = (-Class29.anInt307 + class338_sub3_sub1_sub3.anInt6799);
						int i_25_;
						if (class338_sub3_sub1_sub3.anInt6785 >= i_24_)
							i_25_ = class397.method4087();
						else {
							i_24_ -= class338_sub3_sub1_sub3.anInt6785;
							int i_26_ = (class338_sub3_sub1_sub3.anInt6765 == 0 ? 0
									: ((-i_24_ + class338_sub3_sub1_sub3.anInt6780) / class338_sub3_sub1_sub3.anInt6765
											* class338_sub3_sub1_sub3.anInt6765));
							i_25_ = (i_26_ * class397.method4087() / class338_sub3_sub1_sub3.anInt6780);
						}
						int i_27_ = class397.method4092();
						i_12_ -= i_27_;
						int i_28_ = (StaticMethods.anIntArray4629[0] + i_6_ - (class397.method4087() >> 1));
						class397.method4096(i_28_, i_12_);
						Class41_Sub13.aHa3774.T(i_28_, i_12_, i_28_ + i_25_, i_12_ + i_27_);
						class397_21_.method4096(i_28_, i_12_);
						Class41_Sub13.aHa3774.KA(i_6_, i_3_, i_8_ + i_6_, i_5_ + i_3_);
						Class368_Sub8.method3838(false, i_28_, i_12_, i_12_ + i_27_, (i_28_ + class397.method4099()));
						i_12_ -= 2;
					}
					if (class147 != null) {
						if (class147.headIcon >= 0
								&& (class147.headIcon < (Class296_Sub9_Sub1.aClass397Array5986).length)) {
							Sprite class397 = (Class296_Sub9_Sub1.aClass397Array5986[class147.headIcon]);
							i_12_ -= class397.method4092();
							class397.method4096(
									(i_6_ + (StaticMethods.anIntArray4629[0]) - (class397.method4087() >> 1)), i_12_);
							Class368_Sub8.method3838(false,
									(i_6_ + StaticMethods.anIntArray4629[0] - (class397.method4087() >> 1)), i_12_,
									i_12_ + class397.method4088(), i_6_ + (StaticMethods.anIntArray4629[0]
											+ (-(class397.method4087() >> 1) + class397.method4099())));
							i_12_ -= 2;
						}
					} else {
						Player class338_sub3_sub1_sub3_sub1 = ((Player) class338_sub3_sub1_sub3);
						if (class338_sub3_sub1_sub3_sub1.prayerICON != -1) {
							Sprite class397 = (Class296_Sub51_Sub24.aClass397Array6463[class338_sub3_sub1_sub3_sub1.prayerICON]);
							i_12_ -= class397.method4092();
							class397.method4096(i_6_ - 12 + (StaticMethods.anIntArray4629[0]), i_12_);
							Class368_Sub8.method3838(false, i_6_ + StaticMethods.anIntArray4629[0] - 12, i_12_,
									i_12_ + class397.method4088(),
									(i_6_ + StaticMethods.anIntArray4629[0] - 12 + class397.method4099()));
							i_12_ -= 2;
						}
						if (class338_sub3_sub1_sub3_sub1.skullICON != -1) {
							Sprite class397 = (Class296_Sub9_Sub1.aClass397Array5986[class338_sub3_sub1_sub3_sub1.skullICON]);
							i_12_ -= class397.method4092();
							class397.method4096(i_6_ - 12 + (StaticMethods.anIntArray4629[0]), i_12_);
							Class368_Sub8.method3838(false, i_6_ - 12 + StaticMethods.anIntArray4629[0], i_12_,
									i_12_ + class397.method4088(),
									(StaticMethods.anIntArray4629[0] - 12 + i_6_ + class397.method4099()));
							i_12_ -= 2;
						}
					}
				}
				if (class338_sub3_sub1_sub3 instanceof Player) {
					if (i_11_ >= 0) {
						int i_29_ = 0;
						Class225[] class225s = Class338_Sub2.aClass225Array5200;
						for (int i_30_ = 0; class225s.length > i_30_; i_30_++) {
							Class225 class225 = class225s[i_30_];
							if (class225 != null && class225.iconType == 10 && is[i_11_] == class225.targetIndex) {
								Sprite class397 = (Class151.aClass397Array1555[class225.modelId]);
								if (class397.method4092() > i_29_)
									i_29_ = class397.method4092();
								class397.method4096(((StaticMethods.anIntArray4629[0]) + i_6_ - 12),
										(-class397.method4092() + i_12_));
								Class368_Sub8.method3838(false, (StaticMethods.anIntArray4629[0] - 12 + i_6_),
										i_12_ - class397.method4092(),
										(-class397.method4092() + i_12_ + class397.method4088()),
										(StaticMethods.anIntArray4629[0] + i_6_ + class397.method4099() - 12));
							}
						}
						if (i_29_ > 0)
							i_12_ -= i_29_ + 2;
					}
				} else {
					int i_31_ = 0;
					Class225[] class225s = Class338_Sub2.aClass225Array5200;
					for (int i_32_ = 0; class225s.length > i_32_; i_32_++) {
						Class225 class225 = class225s[i_32_];
						if (class225 != null && class225.iconType == 1
								&& (ReferenceTable.npcIndexes[i_11_ - i_9_] == class225.targetIndex)) {
							Sprite class397 = (Class151.aClass397Array1555[class225.modelId]);
							if (i_31_ < class397.method4092())
								i_31_ = class397.method4092();
							boolean bool;
							if (class225.anInt2180 == 0)
								bool = true;
							else {
								int i_33_ = (Class338_Sub3_Sub4.method3566((byte) 43) * 1000 / class225.anInt2180 / 2);
								bool = i_33_ > Class29.anInt307 % (i_33_ * 2);
							}
							if (bool) {
								class397.method4096(((StaticMethods.anIntArray4629[0]) + (i_6_ - 12)),
										(i_12_ - class397.method4092()));
								Class368_Sub8.method3838(false, i_6_ - (-StaticMethods.anIntArray4629[0] + 12),
										-class397.method4092() + i_12_,
										(-class397.method4092() + (i_12_ + class397.method4088())),
										(i_6_ + StaticMethods.anIntArray4629[0] - 12 + class397.method4099()));
							}
						}
					}
					if (i_31_ > 0)
						i_12_ -= i_31_ + 2;
				}
				int i_34_ = 0;
				for (/**/; i_34_ < NPCDefinition.aClass268_1478.anInt2491; i_34_++) {
					int i_35_ = class338_sub3_sub1_sub3.hitDelayQueue[i_34_];
					int i_36_ = class338_sub3_sub1_sub3.hitTypeQueue[i_34_];
					Class330 class330 = null;
					int i_37_ = 0;
					if (i_36_ >= 0) {
						if (i_35_ <= Class29.anInt307)
							continue;
						class330 = (ParamType.aClass164_3248.method1634(class338_sub3_sub1_sub3.hitTypeQueue[i_34_],
								101));
						i_37_ = class330.anInt2919;
					} else if (i_35_ < 0)
						continue;
					int i_38_ = class338_sub3_sub1_sub3.soakTypeQueue[i_34_];
					Class330 class330_39_ = null;
					if (i_38_ >= 0)
						class330_39_ = ParamType.aClass164_3248.method1634(i_38_, i_7_ ^ 0x72);
					if (Class29.anInt307 >= -i_37_ + i_35_) {
						int i_40_ = class338_sub3_sub1_sub3.hpBarRatioQueue[i_34_];
						if (i_40_ >= 0) {
							class338_sub3_sub1_sub3.anInt6779 = Class29.anInt307 + 300;
							class338_sub3_sub1_sub3.anInt6768 = i_40_;
							class338_sub3_sub1_sub3.hpBarRatioQueue[i_34_] = -1;
						}
						if (class330 == null)
							class338_sub3_sub1_sub3.hitDelayQueue[i_34_] = -1;
						else {
							int i_41_ = (class338_sub3_sub1_sub3.method3513((byte) -43) / 2);
							Class338_Sub3_Sub3.method3558(class338_sub3_sub1_sub3, -7966, i_8_ >> 1, i_5_ >> 1, i, i_4_,
									i_41_);
							if (StaticMethods.anIntArray4629[0] > -1) {
								StaticMethods.anIntArray4629[0] += (NPCDefinition.aClass268_1478.anIntArray2501[i_34_]);
								StaticMethods.anIntArray4629[1] += (NPCDefinition.aClass268_1478.anIntArray2485[i_34_]);
								Object object = null;
								Object object_42_ = null;
								Object object_43_ = null;
								Object object_44_ = null;
								int i_45_ = 0;
								int i_46_ = 0;
								int i_47_ = 0;
								int i_48_ = 0;
								int i_49_ = 0;
								int i_50_ = 0;
								int i_51_ = 0;
								int i_52_ = 0;
								Sprite class397 = null;
								Sprite class397_53_ = null;
								Sprite class397_54_ = null;
								Sprite class397_55_ = null;
								int i_56_ = 0;
								int i_57_ = 0;
								int i_58_ = 0;
								int i_59_ = 0;
								int i_60_ = 0;
								int i_61_ = 0;
								int i_62_ = 0;
								int i_63_ = 0;
								int i_64_ = 0;
								Sprite class397_65_ = class330.method3401((Class41_Sub13.aHa3774), 99);
								if (class397_65_ != null) {
									i_45_ = class397_65_.method4087();
									int i_66_ = class397_65_.method4092();
									class397_65_.method4076(Class296_Sub1.anIntArray4582);
									if (i_66_ > i_64_)
										i_64_ = i_66_;
									i_49_ = Class296_Sub1.anIntArray4582[0];
								}
								Sprite class397_67_ = class330.method3402(i_7_ - 1, (Class41_Sub13.aHa3774));
								if (class397_67_ != null) {
									i_46_ = class397_67_.method4087();
									int i_68_ = class397_67_.method4092();
									class397_67_.method4076(Class296_Sub1.anIntArray4582);
									if (i_68_ > i_64_)
										i_64_ = i_68_;
									i_50_ = Class296_Sub1.anIntArray4582[0];
								}
								Sprite class397_69_ = class330.method3397((byte) 25, (Class41_Sub13.aHa3774));
								if (class397_69_ != null) {
									i_47_ = class397_69_.method4087();
									int i_70_ = class397_69_.method4092();
									if (i_64_ < i_70_)
										i_64_ = i_70_;
									class397_69_.method4076(Class296_Sub1.anIntArray4582);
									i_51_ = Class296_Sub1.anIntArray4582[0];
								}
								Sprite class397_71_ = class330.method3405((Class41_Sub13.aHa3774), (byte) 121);
								if (class397_71_ != null) {
									i_48_ = class397_71_.method4087();
									int i_72_ = class397_71_.method4092();
									class397_71_.method4076(Class296_Sub1.anIntArray4582);
									if (i_64_ < i_72_)
										i_64_ = i_72_;
									i_52_ = Class296_Sub1.anIntArray4582[0];
								}
								if (class330_39_ != null) {
									class397 = (class330_39_.method3401(Class41_Sub13.aHa3774, 70));
									if (class397 != null) {
										i_56_ = class397.method4087();
										int i_73_ = class397.method4092();
										if (i_64_ < i_73_)
											i_64_ = i_73_;
										class397.method4076(Class296_Sub1.anIntArray4582);
										i_60_ = Class296_Sub1.anIntArray4582[0];
									}
									class397_53_ = (class330_39_.method3402(-41, Class41_Sub13.aHa3774));
									if (class397_53_ != null) {
										i_57_ = class397_53_.method4087();
										int i_74_ = class397_53_.method4092();
										if (i_64_ < i_74_)
											i_64_ = i_74_;
										class397_53_.method4076(Class296_Sub1.anIntArray4582);
										i_61_ = Class296_Sub1.anIntArray4582[0];
									}
									class397_54_ = (class330_39_.method3397((byte) 65, Class41_Sub13.aHa3774));
									if (class397_54_ != null) {
										i_58_ = class397_54_.method4087();
										int i_75_ = class397_54_.method4092();
										class397_54_.method4076(Class296_Sub1.anIntArray4582);
										if (i_75_ > i_64_)
											i_64_ = i_75_;
										i_62_ = Class296_Sub1.anIntArray4582[0];
									}
									class397_55_ = (class330_39_.method3405(Class41_Sub13.aHa3774, (byte) 120));
									if (class397_55_ != null) {
										i_59_ = class397_55_.method4087();
										int i_76_ = class397_55_.method4092();
										class397_55_.method4076(Class296_Sub1.anIntArray4582);
										if (i_76_ > i_64_)
											i_64_ = i_76_;
										i_63_ = Class296_Sub1.anIntArray4582[0];
									}
								}
								Class55 class55 = Class41_Sub2.aClass55_3742;
								Class55 class55_77_ = Class41_Sub2.aClass55_3742;
								Class92 class92 = Class154_Sub1.aClass92_4292;
								int i_78_ = class330.anInt2926;
								Class92 class92_79_ = Class154_Sub1.aClass92_4292;
								if (i_78_ >= 0) {
									Class55 class55_80_ = (Class296_Sub51_Sub30.method3168(Class41_Sub13.aHa3774,
											(byte) -51, true, i_78_, true));
									Class92 class92_81_ = Class157.method1587(i_7_ - 37, (Class41_Sub13.aHa3774),
											i_78_);
									if (class55_80_ != null && class92_81_ != null) {
										class92 = class92_81_;
										class55 = class55_80_;
									}
								}
								if (class330_39_ != null) {
									i_78_ = class330_39_.anInt2926;
									if (i_78_ >= 0) {
										Class55 class55_82_ = (Class296_Sub51_Sub30.method3168(Class41_Sub13.aHa3774,
												(byte) -83, true, i_78_, true));
										Class92 class92_83_ = (Class157.method1587(-76, Class41_Sub13.aHa3774, i_78_));
										if (class55_82_ != null && class92_83_ != null) {
											class92_79_ = class92_83_;
											class55_77_ = class55_82_;
										}
									}
								}
								Object object_84_ = null;
								String string = null;
								boolean bool = false;
								String string_85_ = (class330
										.method3398((class338_sub3_sub1_sub3.hitDamageQueue[i_34_]), (byte) -100));
								int i_86_ = 0;
								int i_87_ = class92.method851(-110, string_85_);
								if (class330_39_ != null) {
									string = (class330_39_.method3398((class338_sub3_sub1_sub3.soakDamageQueue[i_34_]),
											(byte) -120));
									i_86_ = class92_79_.method851(-107, string);
								}
								int i_88_ = 0;
								if (i_46_ > 0)
									i_88_ = i_87_ / i_46_ + 1;
								int i_89_ = 0;
								if (class330_39_ != null && i_57_ > 0)
									i_89_ = i_86_ / i_57_ + 1;
								int i_90_ = 0;
								int i_91_ = i_90_;
								if (i_45_ > 0)
									i_90_ += i_45_;
								i_90_ += 2;
								int i_92_ = i_90_;
								if (i_47_ > 0)
									i_90_ += i_47_;
								int i_93_ = i_90_;
								int i_94_ = i_90_;
								if (i_46_ <= 0)
									i_90_ += i_87_;
								else {
									int i_95_ = i_88_ * i_46_;
									i_90_ += i_95_;
									i_94_ += (i_95_ - i_87_) / 2;
								}
								int i_96_ = i_90_;
								if (i_48_ > 0)
									i_90_ += i_48_;
								int i_97_ = 0;
								int i_98_ = 0;
								int i_99_ = 0;
								int i_100_ = 0;
								int i_101_ = 0;
								if (class330_39_ != null) {
									i_90_ += 2;
									i_97_ = i_90_;
									if (i_56_ > 0)
										i_90_ += i_56_;
									i_90_ += 2;
									i_98_ = i_90_;
									if (i_58_ > 0)
										i_90_ += i_58_;
									i_101_ = i_90_;
									i_99_ = i_90_;
									if (i_57_ > 0) {
										int i_102_ = i_89_ * i_57_;
										i_101_ += (i_102_ - i_86_) / 2;
										i_90_ += i_102_;
									} else
										i_90_ += i_86_;
									i_100_ = i_90_;
									if (i_59_ > 0)
										i_90_ += i_59_;
								}
								int i_103_ = (-Class29.anInt307 + (class338_sub3_sub1_sub3.hitDelayQueue[i_34_]));
								int i_104_ = (-(i_103_ * class330.anInt2929 / class330.anInt2919) + class330.anInt2929);
								int i_105_ = (-class330.anInt2928 + (i_103_ * class330.anInt2928 / class330.anInt2919));
								int i_106_ = (StaticMethods.anIntArray4629[0] + i_6_ + (-(i_90_ >> 1) + i_104_));
								int i_107_ = (i_3_ - 12 + (StaticMethods.anIntArray4629[1] + i_105_));
								int i_108_ = i_107_;
								int i_109_ = i_64_ + i_107_;
								int i_110_ = class330.anInt2924 + 15 + i_107_;
								int i_111_ = -class92.anInt989 + i_110_;
								int i_112_ = i_110_ + class92.anInt992;
								if (i_111_ < i_108_)
									i_108_ = i_111_;
								if (i_109_ < i_112_)
									i_109_ = i_112_;
								int i_113_ = 0;
								if (class330_39_ != null) {
									i_113_ = i_107_ + class330_39_.anInt2924 + 15;
									int i_114_ = -class92_79_.anInt989 + i_113_;
									int i_115_ = class92_79_.anInt992 + i_113_;
									if (i_114_ < i_108_)
										i_108_ = i_114_;
									if (i_109_ < i_115_)
										i_109_ = i_115_;
								}
								int i_116_ = 255;
								if (class330.anInt2932 >= 0)
									i_116_ = ((i_103_ << 8) / (class330.anInt2919 - class330.anInt2932));
								if (i_116_ >= 0 && i_116_ < 255) {
									int i_117_ = i_116_ << 24;
									int i_118_ = i_117_ | 0xffffff;
									if (class397_65_ != null)
										class397_65_.method4079((i_106_ + i_91_ - i_49_), i_107_, 0, i_118_, 1);
									if (class397_69_ != null)
										class397_69_.method4079((-i_51_ + (i_92_ + i_106_)), i_107_, 0, i_118_, 1);
									if (class397_67_ != null) {
										for (int i_119_ = 0; i_119_ < i_88_; i_119_++)
											class397_67_.method4079((i_46_ * i_119_ + (i_93_ + i_106_ - i_50_)), i_107_,
													0, i_118_, 1);
									}
									if (class397_71_ != null)
										class397_71_.method4079((-i_52_ + (i_96_ + i_106_)), i_107_, 0, i_118_, 1);
									class55.a(0, 1659, class330.anInt2931 | i_117_, i_94_ + i_106_, string_85_, i_110_);
									if (class330_39_ != null) {
										if (class397 != null)
											class397.method4079((-i_60_ + (i_97_ + i_106_)), i_107_, 0, i_118_, 1);
										if (class397_54_ != null)
											class397_54_.method4079(-i_62_ + (i_98_ + i_106_), i_107_, 0, i_118_, 1);
										if (class397_53_ != null) {
											for (int i_120_ = 0; i_89_ > i_120_; i_120_++)
												class397_53_.method4079((i_120_ * i_57_ + (i_106_ + i_99_ - i_61_)),
														i_107_, 0, i_118_, 1);
										}
										if (class397_55_ != null)
											class397_55_.method4079((-i_63_ + i_100_ + i_106_), i_107_, 0, i_118_, 1);
										class55_77_.a(0, 1659, i_117_ | (class330_39_.anInt2931), i_101_ + i_106_,
												string, i_113_);
									}
								} else {
									if (class397_65_ != null)
										class397_65_.method4096((-i_49_ + i_106_ + i_91_), i_107_);
									if (class397_69_ != null)
										class397_69_.method4096((i_92_ + i_106_ - i_51_), i_107_);
									if (class397_67_ != null) {
										for (int i_121_ = 0; i_88_ > i_121_; i_121_++)
											class397_67_.method4096(-i_50_ + (i_93_ + i_106_ + i_46_ * i_121_), i_107_);
									}
									if (class397_71_ != null)
										class397_71_.method4096((-i_52_ + (i_106_ + i_96_)), i_107_);
									class55.a(0, 1659, class330.anInt2931 | ~0xffffff, i_94_ + i_106_, string_85_,
											i_110_);
									if (class330_39_ != null) {
										if (class397 != null)
											class397.method4096((-i_60_ + (i_106_ + i_97_)), i_107_);
										if (class397_54_ != null)
											class397_54_.method4096(i_106_ + (i_98_ - i_62_), i_107_);
										if (class397_53_ != null) {
											for (int i_122_ = 0; i_122_ < i_89_; i_122_++)
												class397_53_.method4096((-i_61_ + (i_106_ + i_99_) + i_57_ * i_122_),
														i_107_);
										}
										if (class397_55_ != null)
											class397_55_.method4096(-i_63_ + (i_106_ + i_100_), i_107_);
										class55_77_.a(0, 1659, (class330_39_.anInt2931 | ~0xffffff), i_106_ + i_101_,
												string, i_113_);
									}
								}
								Class368_Sub8.method3838(false, i_106_, i_108_, i_109_ + 1, i_106_ + i_90_);
							}
						}
					}
				}
			}
		}
		for (int i_123_ = 0; Class296_Sub27.anInt4786 > i_123_; i_123_++) {
			int i_124_ = InvisiblePlayer.anIntArray1978[i_123_];
			Mobile class338_sub3_sub1_sub3;
			if (i_124_ >= 2048)
				class338_sub3_sub1_sub3 = (((NPCNode) Class41_Sub18.localNpcs.get((long) (i_124_ - 2048))).value);
			else
				class338_sub3_sub1_sub3 = (PlayerUpdate.visiblePlayers[i_124_]);
			int i_125_ = Class369.anIntArray3138[i_123_];
			Mobile class338_sub3_sub1_sub3_126_;
			if (i_125_ < 2048)
				class338_sub3_sub1_sub3_126_ = (PlayerUpdate.visiblePlayers[i_125_]);
			else
				class338_sub3_sub1_sub3_126_ = (((NPCNode) Class41_Sub18.localNpcs.get((long) (i_125_ - 2048))).value);
			Applet_Sub1.method92(i_3_, i, i_5_, i_7_ - 82, i_4_, class338_sub3_sub1_sub3,
					--class338_sub3_sub1_sub3.anInt6767, i_8_, i_6_, class338_sub3_sub1_sub3_126_);
		}
		int i_127_ = (Class304.aClass92_2729.anInt989 + Class304.aClass92_2729.anInt992 + 2);
		for (int i_128_ = 0; AnimBase.anInt4767 > i_128_; i_128_++) {
			int i_129_ = Class47.anIntArray448[i_128_];
			int i_130_ = Class47.anIntArray451[i_128_];
			int i_131_ = Class47.anIntArray449[i_128_];
			boolean bool = true;
			while (bool) {
				bool = false;
				for (int i_132_ = 0; i_132_ < i_128_; i_132_++) {
					if (-i_127_ + Class47.anIntArray451[i_132_] < i_130_ + 2
							&& -i_127_ + i_130_ < Class47.anIntArray451[i_132_] + 2
							&& (Class47.anIntArray448[i_132_] + Class47.anIntArray449[i_132_]) > i_129_ - i_131_
							&& i_131_ + i_129_ > (Class47.anIntArray448[i_132_] - Class47.anIntArray449[i_132_])
							&& Class47.anIntArray451[i_132_] - i_127_ < i_130_) {
						bool = true;
						i_130_ = -i_127_ + Class47.anIntArray451[i_132_];
					}
				}
			}
			Class47.anIntArray451[i_128_] = i_130_;
			String string = Class47.aClass79Array447[i_128_].method804(15);
			int i_133_ = Class304.aClass92_2729.method851(i_7_ - 47, string);
			int i_134_ = i_6_ + i_129_;
			int i_135_ = i_3_ - (-i_130_ + Class304.aClass92_2729.anInt989);
			int i_136_ = i_134_ + i_133_;
			int i_137_ = Class304.aClass92_2729.anInt992 + i_3_ + i_130_;
			if (Class177.anInt1845 == 0) {
				int i_138_ = 16776960;
				int i_139_ = Class47.aClass79Array447[i_128_].method801((byte) -101);
				if (i_139_ < 6)
					i_138_ = Class48.anIntArray454[i_139_];
				if (i_139_ == 6)
					i_138_ = (Class296_Sub2.anInt4590 % 20 < 10 ? 16711680 : 16776960);
				if (i_139_ == 7)
					i_138_ = Class296_Sub2.anInt4590 % 20 < 10 ? 255 : 65535;
				if (i_139_ == 8)
					i_138_ = Class296_Sub2.anInt4590 % 20 >= 10 ? 8454016 : 45056;
				if (i_139_ == 9) {
					int i_140_ = (-(Class47.aClass79Array447[i_128_].method807(false) * 150
							/ Class47.aClass79Array447[i_128_].method806(i_7_ + 24283)) + 150);
					if (i_140_ >= 50) {
						if (i_140_ >= 100) {
							if (i_140_ < 150)
								i_138_ = 65280 - (-(i_140_ * 5) + 500);
						} else
							i_138_ = -((i_140_ - 50) * 327680) + 16776960;
					} else
						i_138_ = i_140_ * 1280 + 16711680;
				}
				if (i_139_ == 10) {
					int i_141_ = (-(Class47.aClass79Array447[i_128_].method807(false) * 150
							/ Class47.aClass79Array447[i_128_].method806(24243)) + 150);
					if (i_141_ < 50)
						i_138_ = i_141_ * 5 + 16711680;
					else if (i_141_ >= 100) {
						if (i_141_ < 150)
							i_138_ = (-(i_141_ * 5) + 500 + (i_141_ * 327680 - 32768000) + 255);
					} else
						i_138_ = -((i_141_ - 50) * 327680) + 16711935;
				}
				if (i_139_ == 11) {
					int i_142_ = (-(Class47.aClass79Array447[i_128_].method807(false) * 150
							/ Class47.aClass79Array447[i_128_].method806(24243)) + 150);
					if (i_142_ < 50)
						i_138_ = -(i_142_ * 327685) + 16777215;
					else if (i_142_ < 100)
						i_138_ = (i_142_ - 50) * 327685 + 65280;
					else if (i_142_ < 150)
						i_138_ = -((i_142_ - 100) * 327680) + 16777215;
				}
				int i_143_ = i_138_ | ~0xffffff;
				int i_144_ = Class47.aClass79Array447[i_128_].method803((byte) -27);
				if (i_144_ == 0) {
					i_136_ -= i_133_ >> 1;
					Class49.aClass55_461.a(15897, string, -16777216, i_143_, i_129_ + i_6_, i_130_ + i_3_);
					i_134_ -= i_133_ >> 1;
				}
				if (i_144_ == 1) {
					Class49.aClass55_461.a(i_6_ + i_129_, -16777216, string, i_143_, i_130_ + i_3_, 0,
							Class296_Sub2.anInt4590);
					i_136_ -= i_133_ >> 1;
					i_134_ -= i_133_ >> 1;
					i_135_ -= 5;
					i_137_ += 5;
				}
				if (i_144_ == 2) {
					i_134_ -= (i_133_ >> 1) + 5;
					i_136_ -= (i_133_ >> 1) - 5;
					i_135_ -= 5;
					i_137_ += 5;
					Class49.aClass55_461.a((byte) 81, i_130_ + i_3_, i_143_, i_6_ + i_129_, -16777216, string,
							Class296_Sub2.anInt4590);
				}
				if (i_144_ == 3) {
					int i_145_ = (150 - (Class47.aClass79Array447[i_128_].method807(false) * 150
							/ Class47.aClass79Array447[i_128_].method806(24243)));
					i_136_ -= i_133_ >> 1;
					i_137_ += 7;
					i_134_ -= i_133_ >> 1;
					Class49.aClass55_461.a(i_6_ + i_129_, i_3_ + i_130_, Class296_Sub2.anInt4590, string, (byte) -14,
							i_145_, i_143_, -16777216);
					i_135_ -= 7;
				}
				if (i_144_ == 4) {
					int i_146_ = (-(Class47.aClass79Array447[i_128_].method807(false) * 150
							/ Class47.aClass79Array447[i_128_].method806(24243)) + 150);
					int i_147_ = ((Class304.aClass92_2729.method851(i_7_ - 50, string) + 100) * i_146_ / 150);
					Class41_Sub13.aHa3774.T(i_129_ - 50 + i_6_, i_3_, i_6_ + i_129_ + 50, i_3_ + i_5_);
					Class49.aClass55_461.a(-16777216, i_7_ + 1699, i_143_, -i_147_ + (i_6_ + i_129_ + 50), string,
							i_130_ + i_3_);
					i_136_ += -i_147_ + 50;
					i_134_ += -i_147_ + 50;
					Class41_Sub13.aHa3774.KA(i_6_, i_3_, i_6_ + i_8_, i_3_ + i_5_);
				}
				if (i_144_ == 5) {
					int i_148_ = (-(Class47.aClass79Array447[i_128_].method807(false) * 150
							/ Class47.aClass79Array447[i_128_].method806(24243)) + 150);
					int i_149_ = 0;
					if (i_148_ >= 25) {
						if (i_148_ > 125)
							i_149_ = i_148_ - 125;
					} else
						i_149_ = i_148_ - 25;
					int i_150_ = (Class304.aClass92_2729.anInt992 + Class304.aClass92_2729.anInt989);
					Class41_Sub13.aHa3774.T(i_6_, i_130_ + (i_3_ - i_150_) - 1, i_6_ + i_8_, i_3_ + i_130_ + 5);
					i_134_ -= i_133_ >> 1;
					i_135_ += i_149_;
					i_137_ += i_149_;
					i_136_ -= i_133_ >> 1;
					Class49.aClass55_461.a(i_7_ + 15937, string, -16777216, i_143_, i_6_ + i_129_,
							i_149_ + i_130_ + i_3_);
					Class41_Sub13.aHa3774.KA(i_6_, i_3_, i_8_ + i_6_, i_3_ + i_5_);
				}
			} else {
				i_136_ -= i_133_ >> 1;
				Class49.aClass55_461.a(15897, string, -16777216, -256, i_6_ + i_129_, i_130_ + i_3_);
				i_134_ -= i_133_ >> 1;
			}
			Class368_Sub8.method3838(false, i_134_, i_135_, i_137_ + 1, i_136_ + 1);
		}
		if (i_7_ != -40)
			js = false;
	}

	public Node method1576(int i) {
		if (i != 1725258273)
			method1577((byte) 120);
		Node class296 = aClass296_1590;
		if (class296 == aClass296_1586) {
			aClass296_1590 = null;
			return null;
		}
		aClass296_1590 = class296.prev;
		return class296;
	}

	public boolean method1577(byte i) {
		if (i != -77)
			return false;
		if (aClass296_1586 != aClass296_1586.next)
			return false;
		return true;
	}

	private void method1578(NodeDeque class155_151_, int i, Node class296) {
		if (i >= -123)
			js = true;
		Node class296_152_ = aClass296_1586.prev;
		aClass296_1586.prev = class296.prev;
		class296.prev.next = aClass296_1586;
		if (aClass296_1586 != class296) {
			class296.prev = class155_151_.aClass296_1586.prev;
			class296.prev.next = class296;
			class155_151_.aClass296_1586.prev = class296_152_;
			class296_152_.next = class155_151_.aClass296_1586;
		}
	}

	public static void method1579(int i) {
		if (i != -16777216)
			method1575(-119, 83, -120, 68, 93, (byte) 96, -58);
		aStringArray1591 = null;
		PlayerUpdate.cachedAppereances = null;
		aClass161_1585 = null;
	}

	public int method1580(int i) {
		int i_153_ = -71 % ((i + 40) / 63);
		int i_154_ = 0;
		for (Node class296 = aClass296_1586.next; aClass296_1586 != class296; class296 = class296.next)
			i_154_++;
		return i_154_;
	}

	public NodeDeque() {
		aClass296_1586.prev = aClass296_1586;
		aClass296_1586.next = aClass296_1586;
	}

	public void method1581(int i) {
		for (;;) {
			Node class296 = aClass296_1586.next;
			if (aClass296_1586 == class296)
				break;
			class296.unlink();
		}
		aClass296_1590 = null;
		if (i != 327680)
			anInt1592 = -70;
	}
}
