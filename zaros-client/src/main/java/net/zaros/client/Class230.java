package net.zaros.client;
/* Class230 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;

final class Class230 {
	static long aLong2208;
	static Canvas aCanvas2209;
	static int anInt2210 = 0;

	static final boolean method2105(int i, int i_0_, int i_1_) {
		if (i <= 78)
			anInt2210 = -21;
		if ((i_1_ & 0x800) == 0)
			return false;
		return true;
	}

	static final void method2106(Class338_Sub3 class338_sub3) {
		if (class338_sub3 != null) {
			for (int i = 0; i < 2; i++) {
				Class338_Sub3 class338_sub3_2_ = null;
				for (Class338_Sub3 class338_sub3_3_ = Class368_Sub10.aClass338_Sub3Array5481[i]; class338_sub3_3_ != null; class338_sub3_3_ = class338_sub3_3_.aClass338_Sub3_5202) {
					if (class338_sub3_3_ == class338_sub3) {
						if (class338_sub3_2_ != null)
							class338_sub3_2_.aClass338_Sub3_5202 = class338_sub3_3_.aClass338_Sub3_5202;
						else
							Class368_Sub10.aClass338_Sub3Array5481[i] = class338_sub3_3_.aClass338_Sub3_5202;
						Class41.aBoolean388 = true;
						return;
					}
					class338_sub3_2_ = class338_sub3_3_;
				}
				class338_sub3_2_ = null;
				for (Class338_Sub3 class338_sub3_4_ = Class368_Sub16.aClass338_Sub3Array5525[i]; class338_sub3_4_ != null; class338_sub3_4_ = class338_sub3_4_.aClass338_Sub3_5202) {
					if (class338_sub3_4_ == class338_sub3) {
						if (class338_sub3_2_ != null)
							class338_sub3_2_.aClass338_Sub3_5202 = class338_sub3_4_.aClass338_Sub3_5202;
						else
							Class368_Sub16.aClass338_Sub3Array5525[i] = class338_sub3_4_.aClass338_Sub3_5202;
						Class41.aBoolean388 = true;
						return;
					}
					class338_sub3_2_ = class338_sub3_4_;
				}
				class338_sub3_2_ = null;
				for (Class338_Sub3 class338_sub3_5_ = Class338_Sub3_Sub3.aClass338_Sub3Array6568[i]; class338_sub3_5_ != null; class338_sub3_5_ = class338_sub3_5_.aClass338_Sub3_5202) {
					if (class338_sub3_5_ == class338_sub3) {
						if (class338_sub3_2_ != null)
							class338_sub3_2_.aClass338_Sub3_5202 = class338_sub3_5_.aClass338_Sub3_5202;
						else
							Class338_Sub3_Sub3.aClass338_Sub3Array6568[i] = class338_sub3_5_.aClass338_Sub3_5202;
						Class41.aBoolean388 = true;
						return;
					}
					class338_sub3_2_ = class338_sub3_5_;
				}
			}
		}
	}

	public Class230() {
		/* empty */
	}

	static final void method2107(int i, int i_6_, int i_7_, boolean bool, int i_8_, int i_9_, int i_10_, ha var_ha) {
		if (bool != true)
			anInt2210 = -79;
		var_ha.method1088(i_10_, i_9_, i, 1, i_7_, i_8_);
		var_ha.method1088(i_10_ + 1, i_9_ - 2, i_6_, 1, i_7_ + 1, 16);
		var_ha.method1086(i_7_ + 18, i_10_ + 1, i_6_, (byte) 53, i_8_ - 19, i_9_ - 2);
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	static final void method2108(int i) {
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 88, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5001));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 39, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026));
		Class343_Sub1.aClass296_Sub50_5282.method3060(2, 80, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5018));
		Class343_Sub1.aClass296_Sub50_5282.method3060(2, 52, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5029));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 120, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 79, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub29_5002));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 86, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub11_5023));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 87, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub9_4999));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 82, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub16_5019));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 66, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991));
		Class343_Sub1.aClass296_Sub50_5282.method3060(2, 121, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 96, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub7_5004));
		Class343_Sub1.aClass296_Sub50_5282.method3060(2, 106, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 69, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub1_5008));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 82, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5020));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 113, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5015));
		Class343_Sub1.aClass296_Sub50_5282.method3060(2, 38, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub8_5027));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 106, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987));
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 40, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub27_5010));
		Class343_Sub1.aClass296_Sub50_5282.method3060(1, 72, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011));
		Class296_Sub45_Sub2.method2980(true);
		Class343_Sub1.aClass296_Sub50_5282.method3060(0, 45, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub10_5009));
		Class343_Sub1.aClass296_Sub50_5282.method3060(4, 78, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub15_4990));
		Class368_Sub4.method3817(-125);
		Class368_Sub22.method3866(11);
		Class380.aBoolean3210 = true;
		if (i > -25)
			aCanvas2209 = null;
	}

	public static void method2109(int i) {
		aCanvas2209 = null;
		if (i != 0)
			aCanvas2209 = null;
	}
}
