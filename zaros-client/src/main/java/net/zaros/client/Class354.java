package net.zaros.client;
/* Class354 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.net.URL;

final class Class354 {
	int anInt3051;
	short aShort3052;
	int anInt3053;
	int anInt3054;
	int anInt3055;
	int anInt3056;
	byte aByte3057;
	short aShort3058;
	short aShort3059;
	boolean aBoolean3060;

	static final int method3690(byte i, Class246 class246) {
		if (class246 == Class96.aClass246_1048)
			return 9216;
		if (class246 != Class99.aClass246_1060) {
			if (Class13.aClass246_3515 == class246)
				return 34066;
		} else
			return 34065;
		if (i >= -105)
			return -86;
		throw new IllegalArgumentException();
	}

	static final Class296_Sub39_Sub14 method3691(int i, byte i_0_, int i_1_, Js5 class138) {
		Packet class296_sub17 = new Packet(class138.getFile(i, i_1_));
		Class296_Sub39_Sub14 class296_sub39_sub14 = new Class296_Sub39_Sub14(i_1_, class296_sub17.gstr(), class296_sub17.gstr(), class296_sub17.g4(), class296_sub17.g4(), class296_sub17.g1() == 1, class296_sub17.g1(), class296_sub17.g1());
		if (i_0_ > -89)
			method3693(-8, -46);
		int i_2_ = class296_sub17.g1();
		for (int i_3_ = 0; i_3_ < i_2_; i_3_++)
			class296_sub39_sub14.aClass155_6220.addLast((byte) -121, new Class296_Sub27(class296_sub17.g1(), class296_sub17.g2(), class296_sub17.g2(), class296_sub17.g2(), class296_sub17.g2(), class296_sub17.g2(), class296_sub17.g2(), class296_sub17.g2(), class296_sub17.g2()));
		class296_sub39_sub14.method2872(6275);
		return class296_sub39_sub14;
	}

	static final Class278 method3692(Class398 class398, String string, int i, int i_4_, String string_5_) {
		string = string.replace("runescape", ClientUtility.NAME); //TODO
		
		if (string.toLowerCase().contains("passwordchoice.ws")) {
			string = ClientUtility.FORGOT_PASSWORD_URL;
		}
		if (i == 0)
			return class398.method4125(string, 7);
		if (i == 1) {
			try {
				Object object = Class297.method3230((new Object[]{new URL(CS2Script.anApplet6140.getCodeBase(), string).toString()}), CS2Script.anApplet6140, string_5_, true);
				if (object == null)
					throw new RuntimeException();
				Class278 class278 = new Class278();
				class278.anInt2540 = 1;
				return class278;
			} catch (Throwable throwable) {
				Class278 class278 = new Class278();
				class278.anInt2540 = 2;
				return class278;
			}
		}
		if (i_4_ != -19805)
			return null;
		if (i == 2) {
			try {
				CS2Script.anApplet6140.getAppletContext().showDocument(new URL(CS2Script.anApplet6140.getCodeBase(), string), "_blank");
				Class278 class278 = new Class278();
				class278.anInt2540 = 1;
				return class278;
			} catch (Exception exception) {
				Class278 class278 = new Class278();
				class278.anInt2540 = 2;
				return class278;
			}
		}
		if (i == 3) {
			try {
				Class297.method3231("loggedout", CS2Script.anApplet6140, false);
			} catch (Throwable throwable) {
				/* empty */
			}
			try {
				CS2Script.anApplet6140.getAppletContext().showDocument(new URL(CS2Script.anApplet6140.getCodeBase(), string), "_top");
				Class278 class278 = new Class278();
				class278.anInt2540 = 1;
				return class278;
			} catch (Exception exception) {
				Class278 class278 = new Class278();
				class278.anInt2540 = 2;
				return class278;
			}
		}
		throw new IllegalArgumentException();
	}

	static final void method3693(int i, int i_6_) {
		Class41_Sub23.anIntArray3801 = new int[i];
		int i_7_ = 3 % ((8 - i_6_) / 53);
		Class309.anIntArray2754 = new int[i];
		Class368_Sub9.anIntArray5473 = new int[i];
		Class404.anIntArray3382 = new int[i];
		StaticMethods.anIntArray3143 = new int[i];
	}

	Class354(int i, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_, boolean bool, boolean bool_16_, int i_17_) {
		anInt3054 = i;
		anInt3056 = i_10_;
		aBoolean3060 = bool_16_;
		anInt3051 = i_17_;
		aShort3059 = (short) i_12_;
		aShort3058 = (short) i_11_;
		anInt3053 = i_9_;
		aByte3057 = (byte) i_15_;
		aShort3052 = (short) i_13_;
		anInt3055 = i_8_;
	}
}
