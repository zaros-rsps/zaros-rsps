package net.zaros.client;

/* Class139 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class139 {
	static int countryID;
	static String[] globalStringVars;
	static int anInt1421;
	static int anInt1422 = 0;
	static int anInt1423 = 2;

	public static void method1464(boolean bool) {
		if (!bool)
			globalStringVars = null;
	}

	static final boolean method1465(int i, int i_0_, int i_1_) {
		if (i_1_ < 42)
			return true;
		if ((i_0_ & 0x8000) == 0)
			return false;
		return true;
	}
	
	static final int[] writeIsaacKeys(Class296_Sub1 class296_sub1, int i) {
		Packet byteBuffer = new Packet(518);
		int[] keys = new int[4];
		for (int index = 0; index < 4; index++) {
			keys[index] = (int) (Math.random() * 9.9999999E7);
		}
		byteBuffer.p1(10);
		byteBuffer.p4(keys[0]);
		byteBuffer.p4(keys[1]);
		byteBuffer.p4(keys[2]);
		byteBuffer.p4(keys[3]);
		if (i != -126) {
			method1465(76, -97, 113);
		}
		for (int i_3_ = 0; i_3_ < 10; i_3_++) {
			byteBuffer.p4((int) (Math.random() * 9.9999999E7));
		}
		byteBuffer.p2((int) (Math.random() * 9.9999999E7));
		byteBuffer.writeRsaEncryption(RSA.EXPONENT, RSA.MODULUS);
		class296_sub1.out.writeBytes((byteBuffer.data), 0, (byteBuffer.pos));
		return keys;
	}
}
