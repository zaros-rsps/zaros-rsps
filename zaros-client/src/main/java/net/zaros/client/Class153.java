package net.zaros.client;

/* Class153 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class153 {
	int anInt1571;
	static float aFloat1572 = 1024.0F;
	int anInt1573 = 0;
	int anInt1574;
	int anInt1575;
	static AdvancedMemoryCache aClass113_1576 = new AdvancedMemoryCache(8);
	static IncomingPacket aClass231_1577 = new IncomingPacket(107, 5);
	static Class287 aClass287_1578;
	static int[] anIntArray1579 = {2047, 16383, 65535};

	static final void method1551(int i, int i_0_, boolean bool, String string, int i_1_, int i_2_, int i_3_, int i_4_) {
		Class338_Sub9 class338_sub9 = new Class338_Sub9();
		class338_sub9.anInt5261 = i_3_;
		class338_sub9.anInt5259 = i_0_ + Class29.anInt307;
		class338_sub9.aString5265 = string;
		class338_sub9.anInt5262 = i;
		class338_sub9.anInt5260 = i_4_;
		if (bool)
			aClass231_1577 = null;
		class338_sub9.anInt5258 = i_1_;
		class338_sub9.anInt5264 = i_2_;
		Class296_Sub51_Sub39.aClass404_6546.method4158(class338_sub9, 1);
	}

	public static void method1552(int i) {
		aClass231_1577 = null;
		aClass287_1578 = null;
		anIntArray1579 = null;
		aClass113_1576 = null;
		if (i > -75)
			method1551(-65, -116, true, null, 25, 64, -93, -106);
	}

	final void method1553(boolean bool, Packet class296_sub17) {
		for (;;) {
			int i = class296_sub17.g1();
			if (i == 0)
				break;
			method1554(i, (byte) 47, class296_sub17);
		}
		if (bool)
			aFloat1572 = 2.1249595F;
	}

	private final void method1554(int i, byte i_5_, Packet class296_sub17) {
		if (i == 1)
			anInt1575 = class296_sub17.g1();
		else if (i == 2)
			anInt1574 = class296_sub17.g2();
		else if (i != 3) {
			if (i == 4)
				anInt1573 = class296_sub17.g2b();
		} else
			anInt1571 = class296_sub17.g2();
		if (i_5_ < 28)
			anInt1575 = -115;
	}

	public Class153() {
		anInt1571 = 2048;
		anInt1574 = 2048;
		anInt1575 = 0;
	}

	static {
		aClass287_1578 = new Class287();
	}
}
