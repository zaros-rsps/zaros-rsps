package net.zaros.client;

/* Class55_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class55_Sub3 extends Class55 {
	private int[] anIntArray3874;
	private int[] anIntArray3875;
	private byte[][] aByteArrayArray3876;
	private int[] anIntArray3877;
	private int[] anIntArray3878;
	private ha_Sub2 aHa_Sub2_3879;

	public void a(char c, int i, int i_0_, int i_1_, boolean bool, aa var_aa, int i_2_, int i_3_) {
		if (var_aa == null)
			fa(c, i, i_0_, i_1_, bool);
		else {
			i += anIntArray3875[c];
			i_0_ += anIntArray3878[c];
			int i_4_ = anIntArray3877[c];
			int i_5_ = anIntArray3874[c];
			int i_6_ = aHa_Sub2_3879.anInt4084;
			int i_7_ = i + i_0_ * i_6_;
			int i_8_ = i_6_ - i_4_;
			int i_9_ = 0;
			int i_10_ = 0;
			if (i_0_ < aHa_Sub2_3879.anInt4085) {
				int i_11_ = aHa_Sub2_3879.anInt4085 - i_0_;
				i_5_ -= i_11_;
				i_0_ = aHa_Sub2_3879.anInt4085;
				i_10_ += i_11_ * i_4_;
				i_7_ += i_11_ * i_6_;
			}
			if (i_0_ + i_5_ > aHa_Sub2_3879.anInt4078)
				i_5_ -= i_0_ + i_5_ - aHa_Sub2_3879.anInt4078;
			if (i < aHa_Sub2_3879.anInt4079) {
				int i_12_ = aHa_Sub2_3879.anInt4079 - i;
				i_4_ -= i_12_;
				i = aHa_Sub2_3879.anInt4079;
				i_10_ += i_12_;
				i_7_ += i_12_;
				i_9_ += i_12_;
				i_8_ += i_12_;
			}
			if (i + i_4_ > aHa_Sub2_3879.anInt4104) {
				int i_13_ = i + i_4_ - aHa_Sub2_3879.anInt4104;
				i_4_ -= i_13_;
				i_9_ += i_13_;
				i_8_ += i_13_;
			}
			if (i_4_ > 0 && i_5_ > 0)
				method660(aByteArrayArray3876[c], aHa_Sub2_3879.anIntArray4108, i_1_, i_10_, i_7_, i_4_, i_5_, i_8_,
						i_9_, i, i_0_, anIntArray3877[c], var_aa, i_2_, i_3_);
		}
	}

	private final void method659(byte[] is, int[] is_14_, int i, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_,
			int i_20_) {
		for (int i_21_ = -i_18_; i_21_ < 0; i_21_++) {
			for (int i_22_ = -i_17_; i_22_ < 0; i_22_++) {
				int i_23_ = is[i_15_++] & 0xff;
				if (i_23_ != 0) {
					int i_24_ = ((((i & 0xff00ff) * i_23_ & ~0xff00ff) + ((i & 0xff00) * i_23_ & 0xff0000)) >> 8);
					i_23_ = 256 - i_23_;
					int i_25_ = is_14_[i_16_];
					is_14_[i_16_++] = ((((i_25_ & 0xff00ff) * i_23_ & ~0xff00ff)
							+ ((i_25_ & 0xff00) * i_23_ & 0xff0000)) >> 8) + i_24_;
				} else
					i_16_++;
			}
			i_16_ += i_19_;
			i_15_ += i_20_;
		}
	}

	public void fa(char c, int i, int i_26_, int i_27_, boolean bool) {
		i += anIntArray3875[c];
		i_26_ += anIntArray3878[c];
		int i_28_ = anIntArray3877[c];
		int i_29_ = anIntArray3874[c];
		int i_30_ = aHa_Sub2_3879.anInt4084;
		int i_31_ = i + i_26_ * i_30_;
		int i_32_ = i_30_ - i_28_;
		int i_33_ = 0;
		int i_34_ = 0;
		if (i_26_ < aHa_Sub2_3879.anInt4085) {
			int i_35_ = aHa_Sub2_3879.anInt4085 - i_26_;
			i_29_ -= i_35_;
			i_26_ = aHa_Sub2_3879.anInt4085;
			i_34_ += i_35_ * i_28_;
			i_31_ += i_35_ * i_30_;
		}
		if (i_26_ + i_29_ > aHa_Sub2_3879.anInt4078)
			i_29_ -= i_26_ + i_29_ - aHa_Sub2_3879.anInt4078;
		if (i < aHa_Sub2_3879.anInt4079) {
			int i_36_ = aHa_Sub2_3879.anInt4079 - i;
			i_28_ -= i_36_;
			i = aHa_Sub2_3879.anInt4079;
			i_34_ += i_36_;
			i_31_ += i_36_;
			i_33_ += i_36_;
			i_32_ += i_36_;
		}
		if (i + i_28_ > aHa_Sub2_3879.anInt4104) {
			int i_37_ = i + i_28_ - aHa_Sub2_3879.anInt4104;
			i_28_ -= i_37_;
			i_33_ += i_37_;
			i_32_ += i_37_;
		}
		if (i_28_ > 0 && i_29_ > 0)
			method659(aByteArrayArray3876[c], aHa_Sub2_3879.anIntArray4108, i_27_, i_34_, i_31_, i_28_, i_29_, i_32_,
					i_33_);
	}

	Class55_Sub3(ha_Sub2 var_ha_Sub2, Class92 class92, Class186[] class186s, int[] is, int[] is_38_) {
		super(var_ha_Sub2, class92);
		aHa_Sub2_3879 = var_ha_Sub2;
		aHa_Sub2_3879 = var_ha_Sub2;
		anIntArray3877 = is;
		anIntArray3874 = is_38_;
		aByteArrayArray3876 = new byte[class186s.length][];
		anIntArray3878 = new int[class186s.length];
		anIntArray3875 = new int[class186s.length];
		for (int i = 0; i < class186s.length; i++) {
			Class186 class186 = class186s[i];
			if (class186.aByteArray1905 != null)
				aByteArrayArray3876[i] = class186.aByteArray1905;
			else {
				byte[] is_39_ = class186.aByteArray1901;
				byte[] is_40_ = aByteArrayArray3876[i] = new byte[is_39_.length];
				for (int i_41_ = 0; i_41_ < is_39_.length; i_41_++)
					is_40_[i_41_] = (byte) (is_39_[i_41_] == 0 ? 0 : -1);
			}
			anIntArray3878[i] = class186.anInt1906;
			anIntArray3875[i] = class186.anInt1903;
		}
	}

	private final void method660(byte[] is, int[] is_42_, int i, int i_43_, int i_44_, int i_45_, int i_46_, int i_47_,
			int i_48_, int i_49_, int i_50_, int i_51_, aa var_aa, int i_52_, int i_53_) {
		aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
		int[] is_54_ = var_aa_Sub2.anIntArray3726;
		int[] is_55_ = var_aa_Sub2.anIntArray3729;
		int i_56_ = i_49_ - aHa_Sub2_3879.anInt4079;
		int i_57_ = i_50_;
		if (i_53_ > i_57_) {
			i_57_ = i_53_;
			i_44_ += (i_53_ - i_50_) * aHa_Sub2_3879.anInt4084;
			i_43_ += (i_53_ - i_50_) * i_51_;
		}
		int i_58_ = (i_53_ + is_54_.length < i_50_ + i_46_ ? i_53_ + is_54_.length : i_50_ + i_46_);
		for (int i_59_ = i_57_; i_59_ < i_58_; i_59_++) {
			int i_60_ = is_54_[i_59_ - i_53_] + i_52_;
			int i_61_ = is_55_[i_59_ - i_53_];
			int i_62_ = i_45_;
			if (i_56_ > i_60_) {
				int i_63_ = i_56_ - i_60_;
				if (i_63_ >= i_61_) {
					i_43_ += i_45_ + i_48_;
					i_44_ += i_45_ + i_47_;
					continue;
				}
				i_61_ -= i_63_;
			} else {
				int i_64_ = i_60_ - i_56_;
				if (i_64_ >= i_45_) {
					i_43_ += i_45_ + i_48_;
					i_44_ += i_45_ + i_47_;
					continue;
				}
				i_43_ += i_64_;
				i_62_ -= i_64_;
				i_44_ += i_64_;
			}
			int i_65_ = 0;
			if (i_62_ < i_61_)
				i_61_ = i_62_;
			else
				i_65_ = i_62_ - i_61_;
			for (int i_66_ = -i_61_; i_66_ < 0; i_66_++) {
				int i_67_ = is[i_43_++] & 0xff;
				if (i_67_ != 0) {
					int i_68_ = ((((i & 0xff00ff) * i_67_ & ~0xff00ff) + ((i & 0xff00) * i_67_ & 0xff0000)) >> 8);
					i_67_ = 256 - i_67_;
					int i_69_ = is_42_[i_44_];
					is_42_[i_44_++] = ((((i_69_ & 0xff00ff) * i_67_ & ~0xff00ff)
							+ ((i_69_ & 0xff00) * i_67_ & 0xff0000)) >> 8) + i_68_;
				} else
					i_44_++;
			}
			i_43_ += i_65_ + i_48_;
			i_44_ += i_65_ + i_47_;
		}
	}
}
