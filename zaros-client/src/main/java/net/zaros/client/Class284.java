package net.zaros.client;
/* Class284 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Frame;

import jaggl.OpenGL;

final class Class284 {
	static int[] anIntArray2618 = {16, 32, 64, 128};
	static int anInt2619;
	static float aFloat2620;
	static int[] anIntArray2621 = new int[1];

	static final Class39 method2361(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Class179[] class179s, int i) {
		for (int i_0_ = 0; class179s.length > i_0_; i_0_++) {
			if (class179s[i_0_] == null || class179s[i_0_].aLong1852 <= 0L)
				return null;
		}
		long l = OpenGL.glCreateProgramObjectARB();
		for (int i_1_ = 0; i_1_ < class179s.length; i_1_++)
			OpenGL.glAttachObjectARB(l, class179s[i_1_].aLong1852);
		OpenGL.glLinkProgramARB(l);
		OpenGL.glGetObjectParameterivARB(l, 35714, Class131.anIntArray1341, i);
		if (Class131.anIntArray1341[0] == 0) {
			if (Class131.anIntArray1341[0] == 0)
				System.out.println("Shader linking failed:");
			OpenGL.glGetObjectParameterivARB(l, 35716, Class131.anIntArray1341, 1);
			if (Class131.anIntArray1341[1] > 1) {
				byte[] is = new byte[Class131.anIntArray1341[1]];
				OpenGL.glGetInfoLogARB(l, Class131.anIntArray1341[1], Class131.anIntArray1341, 0, is, 0);
				System.out.println(new String(is));
			}
			if (Class131.anIntArray1341[0] == 0) {
				for (int i_2_ = 0; class179s.length > i_2_; i_2_++)
					OpenGL.glDetachObjectARB(l, class179s[i_2_].aLong1852);
				OpenGL.glDeleteObjectARB(l);
				return null;
			}
		}
		return new Class39(var_ha_Sub1_Sub1, l, class179s);
	}

	static final Frame method2362(int i, int i_3_, int i_4_, Class398 class398, int i_5_, int i_6_) {
		if (!class398.method4131(i_5_ - 1343340912))
			return null;
		if (i_5_ == i_3_) {
			Class224[] class224s = StaticMethods.method3207(class398, (byte) 68);
			if (class224s == null)
				return null;
			boolean bool = false;
			for (int i_7_ = 0; i_7_ < class224s.length; i_7_++) {
				if (class224s[i_7_].anInt2167 == i_6_ && i == class224s[i_7_].anInt2171 && (i_4_ == 0 || class224s[i_7_].anInt2170 == i_4_) && (!bool || class224s[i_7_].anInt2168 > i_3_)) {
					bool = true;
					i_3_ = class224s[i_7_].anInt2168;
				}
			}
			if (!bool)
				return null;
		}
		Class278 class278 = class398.method4128(i, i_3_, i_6_, i_4_, (byte) -100);
		while (class278.anInt2540 == 0)
			Class106_Sub1.method942(10L, 0);
		Frame frame = (Frame) class278.anObject2539;
		if (frame == null)
			return null;
		if (class278.anInt2540 == 2) {
			Class41.method388(16549, frame, class398);
			return null;
		}
		return frame;
	}

	static final Class144 method2363(boolean bool) {
		try {
			if (bool != true)
				aFloat2620 = 0.6746367F;
			return (Class144) Class144_Sub1.class.newInstance();
		} catch (Throwable throwable) {
			return null;
		}
	}

	public static void method2364(int i) {
		if (i != 1)
			method2364(83);
		anIntArray2621 = null;
		anIntArray2618 = null;
	}

	static final boolean method2365(int i, int i_8_, int i_9_) {
		int i_10_ = 95 / ((i + 8) / 60);
		if (!((i_9_ & 0x40000) != 0 | Class367.method3803(i_9_, -119, i_8_)) && !Class184.method1857(i_8_, i_9_, (byte) 120))
			return false;
		return true;
	}
}
