package net.zaros.client;

/* Class310 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class AnimationsLoader {
	static Class210_Sub1[] aClass210_Sub1Array2760;
	private Js5 animFS;
	static boolean aBoolean2762 = false;
	static boolean aBoolean2763;
	private AdvancedMemoryCache cachedAnimations = new AdvancedMemoryCache(64);
	private AdvancedMemoryCache aClass113_2765 = new AdvancedMemoryCache(100);

	final void updateReferences(int c) {
		synchronized (cachedAnimations) {
			cachedAnimations.clean(c);
		}
		synchronized (aClass113_2765) {
			aClass113_2765.clean(c);
		}
	}

	final void clearCachedAnimations() {
		synchronized (cachedAnimations) {
			cachedAnimations.clear();
		}
		synchronized (aClass113_2765) {
			aClass113_2765.clear();
		}
	}

	final Animation getAnimation(int i, byte i_1_) {
		Animation anim;
		synchronized (cachedAnimations) {
			anim = (Animation) cachedAnimations.get((long) i);
		}
		if (anim != null)
			return anim;
		byte[] data;
		synchronized (animFS) {
			data = animFS.getFile(Class41_Sub4.animFileID(i), Class338_Sub3_Sub1_Sub1.animChildID(i));
		}
		anim = new Animation();
		anim.ID = i;
		if (data != null)
			anim.init(new Packet(data));
		anim.method544(true);
		synchronized (cachedAnimations) {
			cachedAnimations.put(anim, (long) i);
		}
		return anim;
	}

	static final void method3297(int i, Class296_Sub39_Sub9 class296_sub39_sub9) {
		if (!Class318.aBoolean2814) {
			class296_sub39_sub9.unlink();
			if (i != 26036)
				aBoolean2762 = true;
			Class230.anInt2210--;
			if (class296_sub39_sub9.aBoolean6173) {
				for (Class296_Sub39_Sub1 class296_sub39_sub1 = ((Class296_Sub39_Sub1) Class152.aClass145_1558.getFront()); class296_sub39_sub1 != null; class296_sub39_sub1 = ((Class296_Sub39_Sub1) Class152.aClass145_1558.getNext())) {
					if (class296_sub39_sub1.aString6116.equals(class296_sub39_sub9.aString6166)) {
						boolean bool = false;
						for (Class296_Sub39_Sub9 class296_sub39_sub9_3_ = ((Class296_Sub39_Sub9) class296_sub39_sub1.aClass145_6119.getFront()); class296_sub39_sub9_3_ != null; class296_sub39_sub9_3_ = ((Class296_Sub39_Sub9) class296_sub39_sub1.aClass145_6119.getNext())) {
							if (class296_sub39_sub9 == class296_sub39_sub9_3_) {
								bool = true;
								if (class296_sub39_sub1.method2783(3553, class296_sub39_sub9))
									HardReferenceWrapper.method2793(i - 25969, class296_sub39_sub1);
								break;
							}
						}
						if (bool)
							break;
					}
				}
			} else {
				long l = class296_sub39_sub9.aLong6161;
				Class296_Sub39_Sub1 class296_sub39_sub1;
				for (class296_sub39_sub1 = ((Class296_Sub39_Sub1) FileOnDisk.aClass263_670.get(l)); class296_sub39_sub1 != null; class296_sub39_sub1 = ((Class296_Sub39_Sub1) FileOnDisk.aClass263_670.getUnknown(false))) {
					if (class296_sub39_sub1.aString6116.equals(class296_sub39_sub9.aString6166))
						break;
				}
				if (class296_sub39_sub1 != null && class296_sub39_sub1.method2783(3553, class296_sub39_sub9))
					HardReferenceWrapper.method2793(62, class296_sub39_sub1);
			}
		}
	}

	final Class296_Sub39_Sub6 method3298(int i, int i_4_) {
		int i_5_ = 73 % ((i + 61) / 33);
		Class296_Sub39_Sub6 class296_sub39_sub6;
		synchronized (aClass113_2765) {
			class296_sub39_sub6 = (Class296_Sub39_Sub6) aClass113_2765.get((long) i_4_);
			if (class296_sub39_sub6 == null) {
				class296_sub39_sub6 = new Class296_Sub39_Sub6(i_4_);
				aClass113_2765.put(class296_sub39_sub6, (long) i_4_);
			}
			if (!class296_sub39_sub6.method2815(1))
				return null;
		}
		return class296_sub39_sub6;
	}

	public static void method3299(byte i) {
		aClass210_Sub1Array2760 = null;
		if (i != 32)
			method3301('\ufffa', -105);
	}

	final void method3300(int i) {
		synchronized (cachedAnimations) {
			cachedAnimations.clearSoftReferences();
		}
		synchronized (aClass113_2765) {
			if (i != 2) {
				/* empty */
			} else
				aClass113_2765.clearSoftReferences();
		}
	}

	static final boolean method3301(char c, int i) {
		try {
			if (c > 0 && c < '\u0080' || c >= '\u00a0' && c <= '\u00ff')
				return true;
			int i_6_ = 101 % ((i + 10) / 59);
			if (c != 0) {
				char[] cs = r.aCharArray6207;
				for (int i_7_ = 0; i_7_ < cs.length; i_7_++) {
					char c_8_ = cs[i_7_];
					if (c_8_ == c)
						return true;
				}
			}
			return false;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "rga.D(" + c + ',' + i + ')');
		}
	}

	AnimationsLoader(GameType class35, int i, Js5 class138, Js5 class138_9_, Js5 class138_10_) {
		animFS = class138;
		if (animFS != null) {
			int i_11_ = animFS.getLastGroupId() - 1;
			animFS.getLastFileId(i_11_);
		}
		Class296_Sub51_Sub8.method3098(2, (byte) -65, class138_9_, class138_10_);
	}

	static {
		aClass210_Sub1Array2760 = new Class210_Sub1[0];
		aBoolean2763 = false;
	}
}
