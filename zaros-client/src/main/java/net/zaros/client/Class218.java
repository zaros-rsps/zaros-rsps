package net.zaros.client;

/* Class218 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class218 {
	private int[] anIntArray2122;
	private int[] anIntArray2123;
	static Class252 aClass252_2124 = new Class252();
	static Class190 aClass190_2125;

	final void method2040(Class302 class302, int i, byte i_0_) {
		int i_1_ = anIntArray2122[0];
		class302.method3264(i_1_ & 0xffff, i_1_ >>> 16, i_0_ - 87, i);
		Mobile class338_sub3_sub1_sub3 = class302.method3262(0);
		class338_sub3_sub1_sub3.currentWayPoint = 0;
		for (int i_2_ = anIntArray2123.length - 1; i_2_ >= 0; i_2_--) {
			int i_3_ = anIntArray2123[i_2_];
			int i_4_ = anIntArray2122[i_2_];
			class338_sub3_sub1_sub3.waypointQueueX[(class338_sub3_sub1_sub3.currentWayPoint)] = i_4_ >> 16;
			class338_sub3_sub1_sub3.wayPointQueueY[(class338_sub3_sub1_sub3.currentWayPoint)] = i_4_ & 65535;
			byte i_5_ = 1;
			if (i_3_ == 0)
				i_5_ = (byte) 0;
			else if (i_3_ == 2)
				i_5_ = (byte) 2;
			class338_sub3_sub1_sub3.walkingTypes[(class338_sub3_sub1_sub3.currentWayPoint)] = i_5_;
			class338_sub3_sub1_sub3.currentWayPoint++;
		}
		if (i_0_ != 87)
			anIntArray2123 = null;
	}

	Class218(Packet class296_sub17) {
		int i = class296_sub17.readSmart();
		anIntArray2122 = new int[i];
		anIntArray2123 = new int[i];
		for (int i_6_ = 0; i > i_6_; i_6_++) {
			int i_7_ = class296_sub17.g1();
			anIntArray2123[i_6_] = i_7_;
			int i_8_ = class296_sub17.g2();
			int i_9_ = class296_sub17.g2();
			anIntArray2122[i_6_] = (i_8_ << 16) + i_9_;
		}
	}

	public static void method2041(byte i) {
		aClass252_2124 = null;
		int i_10_ = -76 % ((-32 - i) / 58);
		aClass190_2125 = null;
	}

	static final void method2042(int i, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_, boolean bool, int i_17_) {
		if (bool == true) {
			if (i_11_ >= 1 && i_15_ >= 1 && i_11_ <= Class198.currentMapSizeX - 2 && i_15_ <= Class296_Sub38.currentMapSizeY - 2) {
				int i_18_ = i_13_;
				if (i_18_ < 3 && r_Sub2.method2871(i_15_, i_11_, (byte) -47))
					i_18_++;
				if ((Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026.method496(122) != 0 || Class296_Sub40.method2912(23266, i_15_, NPCDefinitionLoader.anInt1401, i_11_, i_18_)) && Class338_Sub2.aClass247ArrayArrayArray5195 != null) {
					StaticMethods.aClass181_Sub1_5960.method1835(Class41_Sub13.aHa3774, i_15_, i_11_, i_13_, i, BITConfigDefinition.mapClips[i_13_], false);
					if (i_17_ >= 0) {
						int i_19_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997.method484(115);
						Class343_Sub1.aClass296_Sub50_5282.method3060(1, 126, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997));
						StaticMethods.aClass181_Sub1_5960.method1838(i_17_, Class41_Sub13.aHa3774, i_14_, BITConfigDefinition.mapClips[i_13_], i_11_, i_18_, i_16_, i_15_, i_13_, i_12_, -95);
						Class343_Sub1.aClass296_Sub50_5282.method3060(i_19_, 126, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997));
					}
				}
			}
		}
	}
}
