package net.zaros.client;
import jagdx.IDirect3DBaseTexture;

abstract class Class64 {
	Class131 aClass131_728 = ISAACCipher.aClass131_1892;
	Class202 aClass202_729;
	Class67 aClass67_730;
	ha_Sub1_Sub2 aHa_Sub1_Sub2_731;
	boolean aBoolean732;

	abstract IDirect3DBaseTexture method706(int i);

	void method29(Class131 class131, int i) {
		if (i == 16518)
			aClass131_728 = class131;
	}

	Class64(ha_Sub1_Sub2 var_ha_Sub1_Sub2, Class202 class202, Class67 class67, boolean bool, int i) {
		aHa_Sub1_Sub2_731 = var_ha_Sub1_Sub2;
		aClass67_730 = class67;
		aClass202_729 = class202;
		aBoolean732 = bool;
	}
}
