package net.zaros.client;

import java.awt.Canvas;
import java.awt.Rectangle;

public abstract class ha {
	public static int anInt1293;
	public static boolean aBoolean1294 = false;
	public int anInt1295;
	public static int[][] anIntArrayArray1296 = { { 0, 1, 2, 3 }, { 1, 2, 3, 0 }, { 1, 2, -1, 0 }, { 2, 0, -1, 1 },
			{ 0, 1, -1, 2 }, { 1, 2, -1, 0 }, { -1, 4, -1, 1 }, { -1, 1, 3, -1 }, { -1, 0, 2, -1 }, { 3, 5, 2, 0 },
			{ 0, 2, 5, 3 }, { 0, 2, 3, 5 }, { 0, 1, 2, 3 } };
	public static float[] aFloatArray1297;
	public static float[] aFloatArray1298 = new float[16384];
	public d aD1299;

	public abstract void P(int i, int i_0_, int i_1_, int i_2_, int i_3_);

	public abstract void a(Class390 class390, int i);

	public abstract void A(int i, aa var_aa, int i_4_, int i_5_);

	public abstract void i(int i);

	public abstract void a(boolean bool);

	public abstract void U(int i, int i_6_, int i_7_, int i_8_, int i_9_);

	public abstract void t();

	public abstract void a(int[] is);

	public abstract void H(int i, int i_10_, int i_11_, int[] is);

	public abstract void aa(int i, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_);

	public abstract void HA(int i, int i_17_, int i_18_, int i_19_, int[] is);

	public abstract Class241 a(Class241 class241, Class241 class241_20_, float f, Class241 class241_21_);

	public abstract void f(int i, int i_22_);

	public abstract void Q(int i, int i_23_, int i_24_, int i_25_, int i_26_, int i_27_, byte[] is, int i_28_,
			int i_29_);

	public abstract boolean x();

	public abstract Class373 o();

	public abstract void a(int i, int i_30_, int i_31_, int i_32_, int i_33_, int i_34_, int i_35_);

	static final void method1085(int i, int i_36_, int i_37_, Class338_Sub3_Sub3 class338_sub3_sub3,
			Class338_Sub3_Sub3 class338_sub3_sub3_38_) {
		Class247 class247 = Node.method2428(i, i_36_, i_37_);
		if (class247 != null) {
			class247.aClass338_Sub3_Sub3_2342 = class338_sub3_sub3;
			class247.aClass338_Sub3_Sub3_2337 = class338_sub3_sub3_38_;
			int i_39_ = Class360_Sub2.aSArray5304 == Class52.aSArray636 ? 1 : 0;
			if (class338_sub3_sub3.method3459(0)) {
				if (class338_sub3_sub3.method3469(110)) {
					class338_sub3_sub3.aClass338_Sub3_5202 = Class368_Sub16.aClass338_Sub3Array5525[i_39_];
					Class368_Sub16.aClass338_Sub3Array5525[i_39_] = class338_sub3_sub3;
				} else {
					class338_sub3_sub3.aClass338_Sub3_5202 = Class368_Sub10.aClass338_Sub3Array5481[i_39_];
					Class368_Sub10.aClass338_Sub3Array5481[i_39_] = class338_sub3_sub3;
					Class41.aBoolean388 = true;
				}
			} else {
				class338_sub3_sub3.aClass338_Sub3_5202 = Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_39_];
				Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_39_] = class338_sub3_sub3;
			}
			if (class338_sub3_sub3_38_ != null) {
				if (class338_sub3_sub3_38_.method3459(0)) {
					if (class338_sub3_sub3_38_.method3469(87)) {
						class338_sub3_sub3_38_.aClass338_Sub3_5202 = Class368_Sub16.aClass338_Sub3Array5525[i_39_];
						Class368_Sub16.aClass338_Sub3Array5525[i_39_] = class338_sub3_sub3_38_;
					} else {
						class338_sub3_sub3_38_.aClass338_Sub3_5202 = Class368_Sub10.aClass338_Sub3Array5481[i_39_];
						Class368_Sub10.aClass338_Sub3Array5481[i_39_] = class338_sub3_sub3_38_;
						Class41.aBoolean388 = true;
					}
				} else {
					class338_sub3_sub3_38_.aClass338_Sub3_5202 = Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_39_];
					Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_39_] = class338_sub3_sub3_38_;
				}
			}
		}
	}

	public abstract void a(int i, int i_40_, int i_41_, int i_42_, int i_43_, int i_44_, aa var_aa, int i_45_,
			int i_46_);

	public abstract aa a(int i, int i_47_, int[] is, int[] is_48_);

	public abstract void T(int i, int i_49_, int i_50_, int i_51_);

	public abstract int JA(int i, int i_52_, int i_53_, int i_54_, int i_55_, int i_56_);

	final void method1086(int i, int i_57_, int i_58_, byte i_59_, int i_60_, int i_61_) {
		int i_62_ = -114 / ((90 - i_59_) / 33);
		b(i_57_, i, i_61_, i_60_, i_58_, 1);
	}

	public abstract int XA();

	public abstract void ra(int i, int i_63_, int i_64_, int i_65_);

	public abstract boolean n();

	public abstract boolean z();

	public abstract void h(int i);

	static final void method1087(int i, Sprite[] class397s) {
		OutgoingPacket.anInt2766 = class397s.length;
		Class123_Sub1_Sub2.anIntArray5817 = new int[OutgoingPacket.anInt2766 + 10];
		Class44_Sub1_Sub1.aClass397Array5811 = new Sprite[OutgoingPacket.anInt2766 + 10];
		ArrayTools.removeElement(class397s, 0, Class44_Sub1_Sub1.aClass397Array5811, 0, OutgoingPacket.anInt2766);
		if (i != 1) {
			anInt1293 = 90;
		}
		for (int i_66_ = 0; i_66_ < OutgoingPacket.anInt2766; i_66_++) {
			Class123_Sub1_Sub2.anIntArray5817[i_66_] = Class44_Sub1_Sub1.aClass397Array5811[i_66_].method4088();
		}
		for (int i_67_ = OutgoingPacket.anInt2766; i_67_ < Class44_Sub1_Sub1.aClass397Array5811.length; i_67_++) {
			Class123_Sub1_Sub2.anIntArray5817[i_67_] = 12;
		}
	}

	public abstract void ZA(int i, float f, float f_68_, float f_69_, float f_70_, float f_71_);

	public abstract Class241 c(int i, int i_72_, int i_73_, int i_74_, int i_75_, int i_76_);

	public abstract boolean l();

	public abstract Model a(Mesh class132, int i, int i_77_, int i_78_, int i_79_);

	public abstract void w();

	public abstract boolean u();

	public abstract void a(int i, Class296_Sub35[] class296_sub35s);

	public abstract za b(int i);

	public abstract void c(int i, int i_80_) throws Exception_Sub1;

	public abstract void A();

	public abstract boolean B();

	public abstract void xa(float f);

	public abstract Sprite a(int i, int i_81_, int i_82_, int i_83_, boolean bool);

	public abstract void a(int i, int i_84_, int i_85_, int i_86_);

	public abstract void a(int i);

	public abstract void a(Class241 class241);

	public abstract void EA(int i, int i_87_, int i_88_, int i_89_);

	public abstract int E();

	public abstract void L(int i, int i_90_, int i_91_);

	public abstract void DA(int i, int i_92_, int i_93_, int i_94_);

	public abstract int[] Y();

	public abstract void a(int i, int i_95_, int i_96_, int i_97_, int i_98_, int i_99_, aa var_aa, int i_100_,
			int i_101_, int i_102_, int i_103_, int i_104_);

	final void method1088(int i, int i_105_, int i_106_, int i_107_, int i_108_, int i_109_) {
		aa(i, i_108_, i_105_, i_109_, i_106_, i_107_);
	}

	public abstract void c(int i);

	final void method1089(int i, int i_110_, int i_111_, int i_112_, int i_113_) {
		U(i_112_, i_111_, i, i_110_, 1);
		if (i_113_ > -93) {
			aFloatArray1298 = null;
		}
	}

	final void method1090(byte i) throws Exception_Sub1 {
		int i_114_ = 67 / ((-4 - i) / 32);
		c(0, 0);
	}

	public abstract int[] na(int i, int i_115_, int i_116_, int i_117_);

	public abstract void a(Canvas canvas, int i, int i_118_);

	public abstract void pa();

	public abstract boolean k();

	public abstract void za(int i, int i_119_, int i_120_, int i_121_, int i_122_);

	public abstract int q();

	public abstract void c();

	public void method1091(byte i) {
		Class41_Sub14.aBooleanArray3778[anInt1295] = false;
		if (i >= -76) {
			aD1299 = null;
		}
		t();
	}

	public abstract int M();

	public abstract void e();

	static final synchronized ha method1092(int i, d var_d, Canvas canvas, int i_123_, int i_124_, byte i_125_,
			Js5 class138, int i_126_) {
		if (i == 0) {
			return Class296_Sub51_Sub10.method3104(i_126_, i_123_, i_125_ - 5, canvas, var_d);
		}
		if (i == 2) {
			return Class31.method333(i_125_ - 121, canvas, i_123_, i_126_, var_d);
		}
		if (i == 1) {
			return Class338_Sub3_Sub1_Sub2.method3492(canvas, 124, var_d, i_124_);
		}
		if (i == 5) {
			return Class203.method1961(canvas, var_d, i_124_, false, class138);
		}
		if (i_125_ != 120) {
			return null;
		}
		if (i == 3) {
			return Class141.method1471(i_124_, canvas, class138, var_d, (byte) 53);
		}
		throw new IllegalArgumentException("UM");
	}

	public abstract Class55 a(Class92 class92, Class186[] class186s, boolean bool);

	public abstract void b(Canvas canvas, int i, int i_127_);

	public abstract void la();

	public abstract int d(int i, int i_128_);

	public abstract boolean r();

	public abstract Class33 s();

	public static void method1093(int i) {
		aFloatArray1297 = null;
		aFloatArray1298 = null;
		anIntArrayArray1296 = null;
		if (i != 19128) {
			aFloatArray1297 = null;
		}
	}

	public abstract void X(int i);

	public abstract void g(int i);

	public abstract void b(int i, int i_129_, int i_130_, int i_131_, int i_132_, int i_133_);

	public abstract Class373 m();

	public abstract void a(Canvas canvas);

	public abstract int I();

	public abstract void C(boolean bool);

	public abstract void K(int[] is);

	public abstract Sprite a(int[] is, int i, int i_134_, int i_135_, int i_136_, boolean bool);

	public abstract void a(Class390 class390);

	final void method1094(int i, int i_137_, int i_138_, int i_139_, int i_140_, int i_141_) {
		if (i_140_ < 122) {
			k();
		}
		e(i_139_, i_137_, i_138_, i, i_141_, 1);
	}

	public abstract boolean v();

	public abstract void b(Canvas canvas);

	public abstract void a(int i, int i_142_, int i_143_, int i_144_, int i_145_, int i_146_, int i_147_, int i_148_,
			int i_149_);

	@Override
	protected void finalize() {
		method1091((byte) -85);
	}

	public abstract Interface11 a(int i, int i_150_);

	public abstract int r(int i, int i_151_, int i_152_, int i_153_, int i_154_, int i_155_, int i_156_);

	public abstract boolean j();

	public abstract void f(int i);

	public abstract Interface13 a(Interface19 interface19, Interface11 interface11);

	public abstract Sprite a(Class186 class186, boolean bool);

	public abstract s a(int i, int i_157_, int[][] is, int[][] is_158_, int i_159_, int i_160_, int i_161_);

	public abstract Class296_Sub35 a(int i, int i_162_, int i_163_, int i_164_, int i_165_, float f);

	public abstract boolean b();

	public abstract void a(Rectangle[] rectangles, int i, int i_166_, int i_167_) throws Exception_Sub1;

	public abstract void a(float f, float f_168_, float f_169_);

	public abstract void e(int i, int i_170_, int i_171_, int i_172_, int i_173_, int i_174_);

	public abstract void ya();

	public abstract void da(int i, int i_175_, int i_176_, int[] is);

	final void method1095(int i, int i_177_, int i_178_, int i_179_, int i_180_) {
		if (i_180_ < 78) {
			b(null, 32, -110);
		}
		P(i_177_, i, i_179_, i_178_, 1);
	}

	public abstract void a(Class373 class373);

	public abstract void GA(int i);

	public abstract int e(int i, int i_181_);

	public abstract int i();

	public abstract Class373 f();

	public abstract void b(int i, int i_182_, int i_183_, int i_184_, double d);

	public abstract boolean a();

	public abstract void a(int i, int i_185_, int i_186_, int i_187_, int i_188_, int i_189_, int i_190_, int i_191_,
			int i_192_, int i_193_, int i_194_, int i_195_, int i_196_);

	public abstract Sprite a(int i, int i_197_, boolean bool);

	public abstract Interface19 b(int i, int i_198_);

	public abstract void F(int i, int i_199_);

	public Sprite method1096(int i, int i_200_, int i_201_, int i_202_, int i_203_, int[] is) {
		if (i < 101) {
			a((Class390) null, 117);
		}
		return a(is, i_200_, i_203_, i_202_, i_201_, true);
	}

	public abstract void a(za var_za);

	public abstract boolean p();

	final void method1097(int i, int i_204_, int i_205_, int i_206_, int i_207_) {
		if (i_207_ != 25003) {
			a(-19, 59, 38, -22, -93, -36, -87, -127, 29);
		}
		za(i, i_204_, i_205_, i_206_, 1);
	}

	public abstract void KA(int i, int i_208_, int i_209_, int i_210_);

	public ha(d var_d) {
		aD1299 = var_d;
		int i = -1;
		for (int i_211_ = 0; i_211_ < 8; i_211_++) {
			if (!Class41_Sub14.aBooleanArray3778[i_211_]) {
				Class41_Sub14.aBooleanArray3778[i_211_] = true;
				i = i_211_;
				break;
			}
		}
		if (i == -1) {
			throw new IllegalStateException("NFTI");
		}
		anInt1295 = i;
	}

	public abstract void a(Interface13 interface13);

	public abstract void b(boolean bool);

	final void method1098(int i, int i_212_, Rectangle[] rectangles) throws Exception_Sub1 {
		if (i != 2) {
			h();
		}
		a(rectangles, i_212_, 0, 0);
	}

	public abstract void y();

	public abstract boolean d();

	public abstract void h();

	final void method1099(Sprite class397, byte i) {
		a(a(class397, a(class397.method4087(), class397.method4092())));
		if (i != -72) {
			anInt1293 = -72;
		}
	}

	static {
		aFloatArray1297 = new float[16384];
		double d = 3.834951969714103E-4;
		for (int i = 0; i < 16384; i++) {
			aFloatArray1297[i] = (float) Math.sin(i * d);
			aFloatArray1298[i] = (float) Math.cos(i * d);
		}
	}
}
