package net.zaros.client;

/* Class41_Sub27 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub27 extends Class41 {
	static int anInt3810;
	static Sprite aClass397_3811;

	static final void method504(Class264 class264) {
		CS2Stack.aClass264_2249 = class264;
	}

	final int method505(int i) {
		if (i <= 114)
			return 126;
		return anInt389;
	}

	final boolean method506(int i) {
		if (!Class125.method1080((byte) 126, aClass296_Sub50_392.aClass41_Sub30_5006.method521(120)))
			return false;
		if (i != -25952)
			anInt3810 = -90;
		return true;
	}

	final int method380(int i, byte i_0_) {
		if (i_0_ != 41)
			method386(-124);
		if (!Class125.method1080((byte) -41, aClass296_Sub50_392.aClass41_Sub30_5006.method521(124)))
			return 3;
		return 1;
	}

	Class41_Sub27(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final void method381(int i, byte i_1_) {
		anInt389 = i;
		if (i_1_ != -110)
			method506(-93);
	}

	static final void method507(int i, int i_2_, Player class338_sub3_sub1_sub3_sub1, int i_3_) {
		if (i_2_ != 1)
			method510((byte) -55);
		int[] is = new int[4];
		Class291.method2407(is, 0, is.length, i_3_);
		Class160.method1619(i, 0, false, is, class338_sub3_sub1_sub3_sub1);
	}

	static final void method508(boolean bool, String string, String string_4_, boolean bool_5_, byte i) {
		Class286.aString2643 = string;
		Class41_Sub5.aBoolean3752 = bool_5_;
		if (!bool_5_)
			Class296_Sub51_Sub14.anInt6423 = -1;
		Js5DiskStore.aBoolean1701 = bool;
		Class379.aString3625 = string_4_;
		if (!Class41_Sub5.aBoolean3752 && (Class286.aString2643.equals("") || Class379.aString3625.equals("")))
			BITConfigDefinition.method2352(-2, 3);
		else {
			Class377.loginConnection.aBoolean2065 = false;
			if (i >= -86)
				anInt3810 = 118;
			if (Class187.anInt1910 != 1) {
				Class296_Sub40.anInt4914 = 0;
				AdvancedMemoryCache.anInt1172 = -1;
				ParticleEmitterRaw.anInt1771 = -1;
			}
			BITConfigDefinition.method2352(-2, -3);
			Class296_Sub14.anInt4662 = 0;
			Class105_Sub1.anInt5689 = 0;
			Class296_Sub39_Sub12.loginState = 1;
		}
	}

	final void method386(int i) {
		if (i != 2)
			method506(-9);
		if (aClass296_Sub50_392.aClass41_Sub30_5006.method527(-114) && !Class125.method1080((byte) 91, aClass296_Sub50_392.aClass41_Sub30_5006.method521(i ^ 0x7e)))
			anInt389 = 0;
		if (anInt389 < 0 || anInt389 > 1)
			anInt389 = method383((byte) 110);
	}

	public static void method509(byte i) {
		aClass397_3811 = null;
		if (i != 4)
			method509((byte) -38);
	}

	Class41_Sub27(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final int method383(byte i) {
		if (i != 110)
			anInt3810 = 32;
		return 0;
	}

	static final Class327 method510(byte i) {
		try {
			if (i != 4)
				anInt3810 = -25;
			return (Class327) Class327_Sub1.class.newInstance();
		} catch (Throwable throwable) {
			return new Class327_Sub2();
		}
	}
}
