package net.zaros.client;
import jaclib.memory.heap.NativeHeap;

final class za_Sub2 extends za {
	static Class202 aClass202_6555 = new Class202(4);
	NativeHeap aNativeHeap6556;

	static final String method3225(boolean bool, int i, int i_0_, int i_1_, long l) {
		char c = ',';
		char c_2_ = '.';
		if (i == 0) {
			c_2_ = ',';
			c = '.';
		}
		if (i == 2)
			c_2_ = '\u00a0';
		boolean bool_3_ = false;
		if (i_1_ > -55)
			aClass202_6555 = null;
		if (l < 0L) {
			bool_3_ = true;
			l = -l;
		}
		StringBuffer stringbuffer = new StringBuffer(26);
		if (i_0_ > 0) {
			for (int i_4_ = 0; i_4_ < i_0_; i_4_++) {
				int i_5_ = (int) l;
				l /= 10L;
				stringbuffer.append((char) (i_5_ + (-((int) l * 10) + 48)));
			}
			stringbuffer.append(c);
		}
		int i_6_ = 0;
		for (;;) {
			int i_7_ = (int) l;
			l /= 10L;
			stringbuffer.append((char) (-((int) l * 10) + 48 + i_7_));
			if (l == 0L)
				break;
			if (bool && ++i_6_ % 3 == 0)
				stringbuffer.append(c_2_);
		}
		if (bool_3_)
			stringbuffer.append('-');
		return stringbuffer.reverse().toString();
	}

	public static void method3226(int i) {
		if (i != -1209185758)
			method3227(-6, -67, 52, true, -41, -34, 42, -43);
		aClass202_6555 = null;
	}

	static final void method3227(int i, int i_8_, int i_9_, boolean bool, int i_10_, int i_11_, int i_12_, int i_13_) {
		int i_14_ = 0;
		int i_15_ = i;
		int i_16_ = 0;
		int i_17_ = i_10_ - i_9_;
		int i_18_ = i - i_9_;
		int i_19_ = i_10_ * i_10_;
		int i_20_ = i * i;
		int i_21_ = i_17_ * i_17_;
		int i_22_ = i_18_ * i_18_;
		int i_23_ = i_20_ << 1;
		int i_24_ = i_19_ << 1;
		int i_25_ = i_22_ << 1;
		int i_26_ = i_21_ << 1;
		int i_27_ = i << 1;
		int i_28_ = i_18_ << 1;
		int i_29_ = i_23_ + (-i_27_ + 1) * i_19_;
		int i_30_ = i_20_ - (i_27_ - 1) * i_24_;
		int i_31_ = i_21_ * (1 - i_28_) + i_25_;
		int i_32_ = -(i_26_ * (i_28_ - 1)) + i_22_;
		int i_33_ = i_19_ << 2;
		int i_34_ = i_20_ << 2;
		int i_35_ = i_21_ << 2;
		int i_36_ = i_22_ << 2;
		if (bool != true)
			aClass202_6555 = null;
		int i_37_ = i_23_ * 3;
		int i_38_ = (i_27_ - 3) * i_24_;
		int i_39_ = i_25_ * 3;
		int i_40_ = (i_28_ - 3) * i_26_;
		int i_41_ = i_34_;
		int i_42_ = i_33_ * (i - 1);
		int i_43_ = i_36_;
		if (i_12_ >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i_12_) {
			int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_12_];
			int i_44_ = ParticleEmitterRaw.method1668(i_8_ - i_10_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 123);
			int i_45_ = ParticleEmitterRaw.method1668(i_8_ + i_10_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 121);
			int i_46_ = ParticleEmitterRaw.method1668(i_8_ - i_17_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 124);
			int i_47_ = ParticleEmitterRaw.method1668(i_17_ + i_8_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 124);
			Class296_Sub14.method2511(is, i_46_, (byte) -22, i_11_, i_44_);
			Class296_Sub14.method2511(is, i_47_, (byte) -78, i_13_, i_46_);
			Class296_Sub14.method2511(is, i_45_, (byte) 125, i_11_, i_47_);
		}
		int i_48_ = i_35_ * (i_18_ - 1);
		while (i_15_ > 0) {
			boolean bool_49_ = i_18_ >= i_15_;
			if (i_29_ < 0) {
				while (i_29_ < 0) {
					i_29_ += i_37_;
					i_30_ += i_41_;
					i_37_ += i_34_;
					i_14_++;
					i_41_ += i_34_;
				}
			}
			if (bool_49_) {
				if (i_31_ < 0) {
					while (i_31_ < 0) {
						i_32_ += i_43_;
						i_31_ += i_39_;
						i_16_++;
						i_43_ += i_36_;
						i_39_ += i_36_;
					}
				}
				if (i_32_ < 0) {
					i_32_ += i_43_;
					i_31_ += i_39_;
					i_16_++;
					i_43_ += i_36_;
					i_39_ += i_36_;
				}
				i_31_ -= i_48_;
				i_32_ -= i_40_;
				i_48_ -= i_35_;
				i_40_ -= i_35_;
			}
			if (i_30_ < 0) {
				i_30_ += i_41_;
				i_29_ += i_37_;
				i_14_++;
				i_41_ += i_34_;
				i_37_ += i_34_;
			}
			i_30_ -= i_38_;
			i_29_ -= i_42_;
			i_15_--;
			i_42_ -= i_33_;
			i_38_ -= i_33_;
			int i_50_ = -i_15_ + i_12_;
			int i_51_ = i_12_ + i_15_;
			if (i_51_ >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i_50_) {
				int i_52_ = ParticleEmitterRaw.method1668(i_14_ + i_8_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 121);
				int i_53_ = ParticleEmitterRaw.method1668(-i_14_ + i_8_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 124);
				if (!bool_49_) {
					if (i_50_ >= EmissiveTriangle.anInt952)
						Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_50_]), i_52_, (byte) 124, i_11_, i_53_);
					if (i_51_ <= RuntimeException_Sub1.anInt3391)
						Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_51_]), i_52_, (byte) -82, i_11_, i_53_);
				} else {
					int i_54_ = ParticleEmitterRaw.method1668(i_16_ + i_8_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -3);
					int i_55_ = ParticleEmitterRaw.method1668(-i_16_ + i_8_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 127);
					if (i_50_ >= EmissiveTriangle.anInt952) {
						int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_50_];
						Class296_Sub14.method2511(is, i_55_, (byte) -83, i_11_, i_53_);
						Class296_Sub14.method2511(is, i_54_, (byte) 111, i_13_, i_55_);
						Class296_Sub14.method2511(is, i_52_, (byte) -59, i_11_, i_54_);
					}
					if (RuntimeException_Sub1.anInt3391 >= i_51_) {
						int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_51_];
						Class296_Sub14.method2511(is, i_55_, (byte) -43, i_11_, i_53_);
						Class296_Sub14.method2511(is, i_54_, (byte) -22, i_13_, i_55_);
						Class296_Sub14.method2511(is, i_52_, (byte) 112, i_11_, i_54_);
					}
				}
			}
		}
	}

	static final void method3228(int i, int i_56_, boolean bool, int i_57_, int i_58_) {
		if (bool)
			method3225(false, -51, -40, -72, 113L);
		if (i == 8 || i == 16) {
			for (int i_59_ = 0; i_59_ < Class22.anInt249; i_59_++) {
				Class324 class324 = Class343_Sub1.aClass324Array5273[i_59_];
				if ((i == class324.aByte2852 && class324.aShort2855 == i_58_ && i_57_ == class324.aShort2853) || (class324.aShort2850 == i_58_ && i_57_ == class324.aShort2853)) {
					if (Class22.anInt249 != i_59_)
						ArrayTools.removeElement(Class343_Sub1.aClass324Array5273, i_59_ + 1, Class343_Sub1.aClass324Array5273, i_59_, ((Class343_Sub1.aClass324Array5273).length + (-i_59_ - 1)));
					Class22.anInt249--;
					break;
				}
			}
		} else {
			Class247 class247 = (Class338_Sub2.aClass247ArrayArrayArray5195[i_56_][i_58_][i_57_]);
			if (class247 != null) {
				if (i == 1)
					class247.aShort2347 = (short) 0;
				else if (i == 2)
					class247.aShort2340 = (short) 0;
			}
			Class320.method3341(false);
		}
	}

	final void method3229(int i) {
		if (i > -110)
			method3226(-73);
		aNativeHeap6556.a();
	}

	za_Sub2(int i) {
		aNativeHeap6556 = new NativeHeap(i);
	}
}
