package net.zaros.client;

/* Class360_Sub6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class360_Sub6 extends Class360 {
	static int[] anIntArray5329;
	static OutgoingPacket aClass311_5330 = new OutgoingPacket(70, 3);
	static int anInt5331 = 0;
	static Js5 fs3;
	static boolean aBoolean5333 = false;

	public static void method3744(int i) {
		anIntArray5329 = null;
		if (i > 98) {
			aClass311_5330 = null;
			fs3 = null;
		}
	}

	static final Class42_Sub4 method3745(boolean bool, Packet class296_sub17) {
		if (bool != true)
			method3747(1, false);
		return new Class42_Sub4(class296_sub17.g2b(), class296_sub17.g2b(), class296_sub17.g2b(), class296_sub17.g2b(), class296_sub17.readUnsignedMedInt(), class296_sub17.readUnsignedMedInt(), class296_sub17.g1());
	}

	final boolean method3723(byte i) {
		int i_0_ = -58 / ((-49 - i) / 36);
		return false;
	}

	final void method3736(byte i, Interface6 interface6, int i_1_) {
		aHa_Sub1_3093.method1140(interface6, false);
		int i_2_ = 53 % ((72 - i) / 49);
		aHa_Sub1_3093.method1197((byte) 22, i_1_);
	}

	final void method3733(byte i, boolean bool) {
		if (i >= -125)
			fs3 = null;
	}

	static final void method3746(int i, int i_3_) {
		r var_r = null;
		for (int i_4_ = i; i_4_ < i_3_; i_4_++) {
			s var_s = Class244.aSArray2320[i_4_];
			if (var_s != null) {
				for (int i_5_ = 0; i_5_ < Class368_Sub12.anInt5488; i_5_++) {
					for (int i_6_ = 0; i_6_ < Class228.anInt2201; i_6_++) {
						var_r = var_s.fa(i_6_, i_5_, var_r);
						if (var_r != null) {
							int i_7_ = i_6_ << Class313.anInt2779;
							int i_8_ = i_5_ << Class313.anInt2779;
							for (int i_9_ = i_4_ - 1; i_9_ >= 0; i_9_--) {
								s var_s_10_ = Class244.aSArray2320[i_9_];
								if (var_s_10_ != null) {
									int i_11_ = (var_s.method3355(i_5_, (byte) -114, i_6_) - var_s_10_.method3355(i_5_, (byte) -107, i_6_));
									int i_12_ = (var_s.method3355(i_5_, (byte) -125, i_6_ + 1) - var_s_10_.method3355(i_5_, (byte) -117, i_6_ + 1));
									int i_13_ = (var_s.method3355(i_5_ + 1, (byte) -114, i_6_ + 1) - var_s_10_.method3355(i_5_ + 1, (byte) -114, i_6_ + 1));
									int i_14_ = (var_s.method3355(i_5_ + 1, (byte) -122, i_6_) - var_s_10_.method3355(i_5_ + 1, (byte) -123, i_6_));
									var_s_10_.CA(var_r, i_7_, (i_11_ + i_12_ + i_13_ + i_14_) / 4, i_8_, 0, false);
								}
							}
						}
					}
				}
			}
		}
	}

	static final void method3747(int i, boolean bool) {
		if (Class296_Sub51_Sub15.aClass54_6426 == null)
			Class296_Sub51_Sub30.method3171((byte) -113);
		if (i != 1)
			method3747(30, false);
		if (bool)
			Class296_Sub51_Sub15.aClass54_6426.method648((byte) 120);
	}

	final void method3725(int i) {
		int i_15_ = -31 / ((58 - i) / 56);
	}

	final void method3732(int i, int i_16_, int i_17_) {
		if (i_16_ > -6)
			method3746(-113, 32);
	}

	final void method3731(boolean bool, byte i) {
		if (i != -71)
			aBoolean5333 = true;
	}

	Class360_Sub6(ha_Sub1 var_ha_Sub1) {
		super(var_ha_Sub1);
	}

	static final void method3748(int i, int i_18_, byte i_19_, int i_20_, int i_21_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_20_, 8);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.intParam = i;
		if (i_19_ <= 39)
			aClass311_5330 = null;
		class296_sub39_sub5.anInt6149 = i_21_;
		class296_sub39_sub5.anInt6145 = i_18_;
	}
}
