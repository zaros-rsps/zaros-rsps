package net.zaros.client;

/* Class178_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class178_Sub1 extends Model {
	private int[] anIntArray4308;
	private int[] anIntArray4309;
	private int[] anIntArray4310;
	private int[] anIntArray4311;
	private int[] anIntArray4312;
	private Class316[] aClass316Array4313;
	private EffectiveVertex[] aClass232Array4314;
	private boolean aBoolean4315;
	private Class128[] aClass128Array4316;
	private int[] anIntArray4317;
	private int anInt4318;
	private Class339[] aClass339Array4319;
	private Class178_Sub1[] aClass178_Sub1Array4320;
	private short aShort4321;
	private int[] anIntArray4322;
	private int[] anIntArray4323;
	private short[] aShortArray4324;
	private Class339[] aClass339Array4325;
	private int anInt4326;
	private short[] aShortArray4327;
	private byte[] aByteArray4328;
	private int[] anIntArray4329;
	private int[] anIntArray4330;
	private int[] anIntArray4331;
	private int anInt4332;
	private int[][] anIntArrayArray4333;
	private byte[] aByteArray4334;
	private int[] anIntArray4335;
	private int[] anIntArray4336;
	private int[] anIntArray4337;
	private int[] anIntArray4338;
	private Class364[] aClass364Array4339;
	private boolean aBoolean4340;
	private short[] aShortArray4341;
	private int[] anIntArray4342;
	private int[] anIntArray4343;
	private short aShort4344;
	private short[] aShortArray4345;
	private short aShort4346;
	private Class178_Sub1[] aClass178_Sub1Array4347;
	private int[] anIntArray4348;
	private int anInt4349;
	private short[] aShortArray4350;
	private int[] anIntArray4351;
	private Class240 aClass240_4352;
	private int anInt4353;
	private boolean aBoolean4354;
	private Class373_Sub3 aClass373_Sub3_4355;
	private int anInt4356 = 0;
	private int anInt4357;
	private int[] anIntArray4358;
	private int anInt4359;
	private int[] anIntArray4360;
	private int anInt4361;
	private boolean aBoolean4362;
	private short[] aShortArray4363;
	private boolean aBoolean4364 = false;
	private boolean aBoolean4365 = false;
	private ha_Sub2 aHa_Sub2_4366;
	private int[][] anIntArrayArray4367;
	private int[][] anIntArrayArray4368;
	private short aShort4369;
	private short aShort4370;
	private int anInt4371;
	private static int anInt4372 = 0;
	private int anInt4373;
	private EmissiveTriangle[] aClass89Array4374;
	private int[] anIntArray4375;
	private float[][] aFloatArrayArray4376;
	private short aShort4377;
	static int anInt4378;
	private int[] anIntArray4379;
	static int anInt4380 = 4096;
	private short aShort4381;
	private boolean aBoolean4382;
	private Class11 aClass11_4383;
	private float[][] aFloatArrayArray4384;
	private short[] aShortArray4385;
	private byte[] aByteArray4386;
	private short[] aShortArray4387;
	private Class240 aClass240_4388;
	private short aShort4389;

	public boolean method1732(int i, int i_0_, Class373 class373, boolean bool, int i_1_) {
		return method1754(i, i_0_, class373, bool, i_1_, -1);
	}

	private void method1745() {
		synchronized (this) {
			for (int i = 0; i < anInt4356; i++) {
				int i_2_ = anIntArray4375[i];
				anIntArray4375[i] = anIntArray4311[i];
				anIntArray4311[i] = -i_2_;
				if (aClass339Array4325[i] != null) {
					i_2_ = aClass339Array4325[i].anInt2973;
					aClass339Array4325[i].anInt2973 = aClass339Array4325[i].anInt2974;
					aClass339Array4325[i].anInt2974 = -i_2_;
				}
			}
			if (aClass364Array4339 != null) {
				for (int i = 0; i < anInt4361; i++) {
					if (aClass364Array4339[i] != null) {
						int i_3_ = aClass364Array4339[i].anInt3113;
						aClass364Array4339[i].anInt3113 = aClass364Array4339[i].anInt3112;
						aClass364Array4339[i].anInt3112 = -i_3_;
					}
				}
			}
			for (int i = anInt4356; i < anInt4373; i++) {
				int i_4_ = anIntArray4375[i];
				anIntArray4375[i] = anIntArray4311[i];
				anIntArray4311[i] = -i_4_;
			}
			anInt4357 = 0;
			aBoolean4362 = false;
		}
	}

	public int HA() {
		if (!aBoolean4362)
			method1759();
		return aShort4389;
	}

	public void method1719(Class373 class373, Class338_Sub5 class338_sub5, int i, int i_5_) {
		method1747(class373, class338_sub5, i, i_5_);
	}

	public int EA() {
		if (!aBoolean4362)
			method1759();
		return aShort4377;
	}

	public boolean r() {
		return aBoolean4340;
	}

	private void method1746(int i) {
		short i_6_ = aShortArray4387[i];
		short i_7_ = aShortArray4324[i];
		short i_8_ = aShortArray4327[i];
		if (aShortArray4350 == null || aShortArray4350[i] == -1) {
			if (aByteArray4328 == null)
				aClass11_4383.anInt108 = 0;
			else
				aClass11_4383.anInt108 = aByteArray4328[i] & 0xff;
			if (anIntArray4331[i] == -1)
				aClass11_4383.method206((float) anIntArray4337[i_6_], (float) anIntArray4337[i_7_],
						(float) anIntArray4337[i_8_], (float) anIntArray4323[i_6_], (float) anIntArray4323[i_7_],
						(float) anIntArray4323[i_8_], (float) anIntArray4360[i_6_], (float) anIntArray4360[i_7_],
						(float) anIntArray4360[i_8_], (Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]));
			else
				aClass11_4383.method205((float) anIntArray4337[i_6_], (float) anIntArray4337[i_7_],
						(float) anIntArray4337[i_8_], (float) anIntArray4323[i_6_], (float) anIntArray4323[i_7_],
						(float) anIntArray4323[i_8_], (float) anIntArray4360[i_6_], (float) anIntArray4360[i_7_],
						(float) anIntArray4360[i_8_], (float) (anIntArray4308[i] & 0xffff),
						(float) (anIntArray4338[i] & 0xffff), (float) (anIntArray4331[i] & 0xffff));
		} else {
			int i_9_ = -16777216;
			if (aByteArray4328 != null)
				i_9_ = 255 - (aByteArray4328[i] & 0xff) << 24;
			if (anIntArray4331[i] == -1) {
				int i_10_ = i_9_ | anIntArray4308[i] & 0xffffff;
				aClass11_4383.method214((float) anIntArray4337[i_6_], (float) anIntArray4337[i_7_],
						(float) anIntArray4337[i_8_], (float) anIntArray4323[i_6_], (float) anIntArray4323[i_7_],
						(float) anIntArray4323[i_8_], (float) anIntArray4360[i_6_], (float) anIntArray4360[i_7_],
						(float) anIntArray4360[i_8_], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
						aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
						aFloatArrayArray4376[i][2], i_10_, i_10_, i_10_, aClass240_4388.anInt2265, 0, 0, 0,
						aShortArray4350[i]);
			} else
				aClass11_4383.method214((float) anIntArray4337[i_6_], (float) anIntArray4337[i_7_],
						(float) anIntArray4337[i_8_], (float) anIntArray4323[i_6_], (float) anIntArray4323[i_7_],
						(float) anIntArray4323[i_8_], (float) anIntArray4360[i_6_], (float) anIntArray4360[i_7_],
						(float) anIntArray4360[i_8_], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
						aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
						aFloatArrayArray4376[i][2], i_9_ | anIntArray4308[i] & 0xffffff,
						i_9_ | anIntArray4338[i] & 0xffffff, i_9_ | anIntArray4331[i] & 0xffffff,
						aClass240_4388.anInt2265, 0, 0, 0, aShortArray4350[i]);
		}
	}

	public int WA() {
		return anInt4353;
	}

	public r ba(r var_r) {
		return null;
	}

	private void method1747(Class373 class373, Class338_Sub5 class338_sub5, int i, int i_11_) {
		if (anInt4356 >= 1) {
			aClass373_Sub3_4355 = (Class373_Sub3) class373;
			Class373_Sub3 class373_sub3 = aHa_Sub2_4366.aClass373_Sub3_4090;
			if (!aBoolean4362)
				method1759();
			boolean bool = false;
			if (aClass373_Sub3_4355.aFloat5606 == 16384.0F && aClass373_Sub3_4355.aFloat5613 == 0.0F
					&& aClass373_Sub3_4355.aFloat5610 == 0.0F && aClass373_Sub3_4355.aFloat5607 == 0.0F
					&& aClass373_Sub3_4355.aFloat5615 == 16384.0F && aClass373_Sub3_4355.aFloat5616 == 0.0F
					&& aClass373_Sub3_4355.aFloat5614 == 0.0F && aClass373_Sub3_4355.aFloat5617 == 0.0F
					&& aClass373_Sub3_4355.aFloat5604 == 16384.0F)
				bool = true;
			float f = (class373_sub3.aFloat5618 + class373_sub3.aFloat5614 * aClass373_Sub3_4355.aFloat5611
					+ class373_sub3.aFloat5617 * aClass373_Sub3_4355.aFloat5619
					+ (class373_sub3.aFloat5604 * aClass373_Sub3_4355.aFloat5618));
			float f_12_ = (bool ? class373_sub3.aFloat5617
					: (class373_sub3.aFloat5614 * aClass373_Sub3_4355.aFloat5613
							+ (class373_sub3.aFloat5617 * aClass373_Sub3_4355.aFloat5615)
							+ (class373_sub3.aFloat5604 * aClass373_Sub3_4355.aFloat5617)));
			int i_13_ = (int) (f + (float) aShort4381 * f_12_);
			int i_14_ = (int) (f + (float) aShort4377 * f_12_);
			int i_15_;
			int i_16_;
			if (i_13_ > i_14_) {
				i_15_ = i_14_ - aShort4344;
				i_16_ = i_13_ + aShort4344;
			} else {
				i_15_ = i_13_ - aShort4344;
				i_16_ = i_14_ + aShort4344;
			}
			if (i_15_ < aHa_Sub2_4366.anInt4087 && i_16_ > aHa_Sub2_4366.anInt4107) {
				float f_17_ = (class373_sub3.aFloat5611 + (class373_sub3.aFloat5606 * aClass373_Sub3_4355.aFloat5611)
						+ (class373_sub3.aFloat5613 * aClass373_Sub3_4355.aFloat5619)
						+ (class373_sub3.aFloat5610 * aClass373_Sub3_4355.aFloat5618));
				float f_18_ = (bool ? class373_sub3.aFloat5613
						: ((class373_sub3.aFloat5606 * aClass373_Sub3_4355.aFloat5613)
								+ (class373_sub3.aFloat5613 * aClass373_Sub3_4355.aFloat5615)
								+ (class373_sub3.aFloat5610 * aClass373_Sub3_4355.aFloat5617)));
				int i_19_ = (int) (f_17_ + (float) aShort4381 * f_18_);
				int i_20_ = (int) (f_17_ + (float) aShort4377 * f_18_);
				int i_21_;
				int i_22_;
				if (i_19_ > i_20_) {
					i_21_ = (i_20_ - aShort4344) * aHa_Sub2_4366.anInt4086;
					i_22_ = (i_19_ + aShort4344) * aHa_Sub2_4366.anInt4086;
				} else {
					i_21_ = (i_19_ - aShort4344) * aHa_Sub2_4366.anInt4086;
					i_22_ = (i_20_ + aShort4344) * aHa_Sub2_4366.anInt4086;
				}
				if (i == -1) {
					if (i_21_ / i_16_ >= aHa_Sub2_4366.anInt4106 || i_22_ / i_16_ <= aHa_Sub2_4366.anInt4082)
						return;
				} else if (i_21_ / i >= aHa_Sub2_4366.anInt4106 || i_22_ / i <= aHa_Sub2_4366.anInt4082)
					return;
				float f_23_ = (class373_sub3.aFloat5619 + (class373_sub3.aFloat5607 * aClass373_Sub3_4355.aFloat5611)
						+ (class373_sub3.aFloat5615 * aClass373_Sub3_4355.aFloat5619)
						+ (class373_sub3.aFloat5616 * aClass373_Sub3_4355.aFloat5618));
				float f_24_ = (bool ? class373_sub3.aFloat5615
						: ((class373_sub3.aFloat5607 * aClass373_Sub3_4355.aFloat5613)
								+ (class373_sub3.aFloat5615 * aClass373_Sub3_4355.aFloat5615)
								+ (class373_sub3.aFloat5616 * aClass373_Sub3_4355.aFloat5617)));
				int i_25_ = (int) (f_23_ + (float) aShort4381 * f_24_);
				int i_26_ = (int) (f_23_ + (float) aShort4377 * f_24_);
				int i_27_;
				int i_28_;
				if (i_25_ > i_26_) {
					i_27_ = (i_26_ - aShort4344) * aHa_Sub2_4366.anInt4072;
					i_28_ = (i_25_ + aShort4344) * aHa_Sub2_4366.anInt4072;
				} else {
					i_27_ = (i_25_ - aShort4344) * aHa_Sub2_4366.anInt4072;
					i_28_ = (i_26_ + aShort4344) * aHa_Sub2_4366.anInt4072;
				}
				if (i == -1) {
					if (i_27_ / i_16_ >= aHa_Sub2_4366.anInt4077 || i_28_ / i_16_ <= aHa_Sub2_4366.anInt4094)
						return;
				} else if (i_27_ / i >= aHa_Sub2_4366.anInt4077 || i_28_ / i <= aHa_Sub2_4366.anInt4094)
					return;
				float f_29_;
				float f_30_;
				float f_31_;
				float f_32_;
				float f_33_;
				float f_34_;
				if (bool) {
					f_29_ = class373_sub3.aFloat5606;
					f_30_ = class373_sub3.aFloat5607;
					f_31_ = class373_sub3.aFloat5614;
					f_32_ = class373_sub3.aFloat5610;
					f_33_ = class373_sub3.aFloat5616;
					f_34_ = class373_sub3.aFloat5604;
				} else {
					f_29_ = ((class373_sub3.aFloat5606 * aClass373_Sub3_4355.aFloat5606)
							+ (class373_sub3.aFloat5613 * aClass373_Sub3_4355.aFloat5607)
							+ (class373_sub3.aFloat5610 * aClass373_Sub3_4355.aFloat5614));
					f_30_ = ((class373_sub3.aFloat5607 * aClass373_Sub3_4355.aFloat5606)
							+ (class373_sub3.aFloat5615 * aClass373_Sub3_4355.aFloat5607)
							+ (class373_sub3.aFloat5616 * aClass373_Sub3_4355.aFloat5614));
					f_31_ = ((class373_sub3.aFloat5614 * aClass373_Sub3_4355.aFloat5606)
							+ (class373_sub3.aFloat5617 * aClass373_Sub3_4355.aFloat5607)
							+ (class373_sub3.aFloat5604 * aClass373_Sub3_4355.aFloat5614));
					f_32_ = ((class373_sub3.aFloat5606 * aClass373_Sub3_4355.aFloat5610)
							+ (class373_sub3.aFloat5613 * aClass373_Sub3_4355.aFloat5616)
							+ (class373_sub3.aFloat5610 * aClass373_Sub3_4355.aFloat5604));
					f_33_ = ((class373_sub3.aFloat5607 * aClass373_Sub3_4355.aFloat5610)
							+ (class373_sub3.aFloat5615 * aClass373_Sub3_4355.aFloat5616)
							+ (class373_sub3.aFloat5616 * aClass373_Sub3_4355.aFloat5604));
					f_34_ = ((class373_sub3.aFloat5614 * aClass373_Sub3_4355.aFloat5610)
							+ (class373_sub3.aFloat5617 * aClass373_Sub3_4355.aFloat5616)
							+ (class373_sub3.aFloat5604 * aClass373_Sub3_4355.aFloat5604));
				}
				if (aHa_Sub2_4366.anInt4105 > 1) {
					synchronized (this) {
						while (aBoolean4365) {
							try {
								this.wait();
							} catch (InterruptedException interruptedexception) {
								/* empty */
							}
						}
						aBoolean4365 = true;
					}
				}
				method1766(Thread.currentThread());
				if ((i_11_ & 0x2) != 0)
					aClass11_4383.method208(true);
				else
					aClass11_4383.method208(false);
				boolean bool_35_ = false;
				boolean bool_36_ = i_15_ <= aHa_Sub2_4366.anInt4107;
				boolean bool_37_ = (bool_36_ || aClass89Array4374 != null || aClass232Array4314 != null);
				aClass240_4388.anInt2273 = aClass11_4383.anInt107;
				aClass240_4388.anInt2271 = aClass11_4383.anInt109;
				aClass240_4388.anInt2272 = aClass11_4383.anInt95;
				int i_38_ = aHa_Sub2_4366.anInt4086;
				int i_39_ = aHa_Sub2_4366.anInt4072;
				int i_40_ = aHa_Sub2_4366.anInt4107;
				if (i == -1) {
					for (int i_41_ = 0; i_41_ < anInt4373; i_41_++) {
						int i_42_ = anIntArray4311[i_41_];
						int i_43_ = anIntArray4342[i_41_];
						int i_44_ = anIntArray4375[i_41_];
						float f_45_ = (f_17_ + f_29_ * (float) i_42_ + f_18_ * (float) i_43_ + f_32_ * (float) i_44_);
						float f_46_ = (f_23_ + f_30_ * (float) i_42_ + f_24_ * (float) i_43_ + f_33_ * (float) i_44_);
						float f_47_ = (f + f_31_ * (float) i_42_ + f_12_ * (float) i_43_ + f_34_ * (float) i_44_);
						anIntArray4360[i_41_] = (int) f_47_;
						if (f_47_ >= (float) i_40_) {
							anIntArray4323[i_41_] = (aClass240_4388.anInt2271 + (int) (f_45_ * (float) i_38_ / f_47_));
							anIntArray4337[i_41_] = (aClass240_4388.anInt2272 + (int) (f_46_ * (float) i_39_ / f_47_));
						} else {
							anIntArray4323[i_41_] = -5000;
							bool_35_ = true;
						}
						if (bool_37_) {
							anIntArray4322[i_41_] = (int) f_45_;
							anIntArray4351[i_41_] = (int) f_46_;
							anIntArray4310[i_41_] = (int) f_47_;
						}
						if (aClass240_4388.aBoolean2269)
							anIntArray4336[i_41_] = (int) (aClass373_Sub3_4355.aFloat5619
									+ ((aClass373_Sub3_4355.aFloat5607 * (float) i_42_)
											+ (aClass373_Sub3_4355.aFloat5615 * (float) i_43_)
											+ (aClass373_Sub3_4355.aFloat5616 * (float) i_44_)));
					}
					if (aClass316Array4313 != null) {
						for (int i_48_ = 0; i_48_ < anInt4359; i_48_++) {
							Class316 class316 = aClass316Array4313[i_48_];
							Class128 class128 = aClass128Array4316[i_48_];
							short i_49_ = aShortArray4387[class316.anInt2804];
							short i_50_ = aShortArray4324[class316.anInt2804];
							short i_51_ = aShortArray4327[class316.anInt2804];
							int i_52_ = ((anIntArray4311[i_49_] + anIntArray4311[i_50_] + anIntArray4311[i_51_]) / 3);
							int i_53_ = ((anIntArray4342[i_49_] + anIntArray4342[i_50_] + anIntArray4342[i_51_]) / 3);
							int i_54_ = ((anIntArray4375[i_49_] + anIntArray4375[i_50_] + anIntArray4375[i_51_]) / 3);
							float f_55_ = ((float) class128.anInt1309
									+ (f_17_ + f_29_ * (float) i_52_ + f_18_ * (float) i_53_ + f_32_ * (float) i_54_));
							float f_56_ = ((float) class128.anInt1316
									+ (f_23_ + f_30_ * (float) i_52_ + f_24_ * (float) i_53_ + f_33_ * (float) i_54_));
							float f_57_ = (f + f_31_ * (float) i_52_ + f_12_ * (float) i_53_ + f_34_ * (float) i_54_);
							if (f_57_ > (float) aHa_Sub2_4366.anInt4107) {
								class128.anInt1305 = (aHa_Sub2_4366.anInt4101 + (int) (f_55_ * (float) i_38_ / f_57_));
								class128.anInt1308 = (aHa_Sub2_4366.anInt4100 + (int) (f_56_ * (float) i_39_ / f_57_));
								class128.anInt1306 = (int) f_57_ - class316.anInt2799;
								class128.anInt1313 = (int) ((float) (class128.anInt1314 * class316.aShort2798 * i_38_)
										/ (f_57_ * 128.0F));
								class128.anInt1310 = (int) ((float) (class128.anInt1312 * class316.aShort2796 * i_39_)
										/ (f_57_ * 128.0F));
							} else
								class128.anInt1313 = class128.anInt1310 = 0;
						}
					}
				} else {
					for (int i_58_ = 0; i_58_ < anInt4373; i_58_++) {
						int i_59_ = anIntArray4311[i_58_];
						int i_60_ = anIntArray4342[i_58_];
						int i_61_ = anIntArray4375[i_58_];
						float f_62_ = (f_17_ + f_29_ * (float) i_59_ + f_18_ * (float) i_60_ + f_32_ * (float) i_61_);
						float f_63_ = (f_23_ + f_30_ * (float) i_59_ + f_24_ * (float) i_60_ + f_33_ * (float) i_61_);
						float f_64_ = (f + f_31_ * (float) i_59_ + f_12_ * (float) i_60_ + f_34_ * (float) i_61_);
						anIntArray4360[i_58_] = (int) f_64_;
						anIntArray4323[i_58_] = (aClass240_4388.anInt2271 + (int) (f_62_ * (float) i_38_ / (float) i));
						anIntArray4337[i_58_] = (aClass240_4388.anInt2272 + (int) (f_63_ * (float) i_39_ / (float) i));
						if (bool_37_) {
							anIntArray4322[i_58_] = (int) f_62_;
							anIntArray4351[i_58_] = (int) f_63_;
							anIntArray4310[i_58_] = i;
						}
						if (aClass240_4388.aBoolean2269)
							anIntArray4336[i_58_] = (int) (aClass373_Sub3_4355.aFloat5619
									+ ((aClass373_Sub3_4355.aFloat5607 * (float) i_59_)
											+ (aClass373_Sub3_4355.aFloat5615 * (float) i_60_)
											+ (aClass373_Sub3_4355.aFloat5616 * (float) i_61_)));
					}
					if (aClass316Array4313 != null) {
						for (int i_65_ = 0; i_65_ < anInt4359; i_65_++) {
							Class316 class316 = aClass316Array4313[i_65_];
							Class128 class128 = aClass128Array4316[i_65_];
							short i_66_ = aShortArray4387[class316.anInt2804];
							short i_67_ = aShortArray4324[class316.anInt2804];
							short i_68_ = aShortArray4327[class316.anInt2804];
							int i_69_ = ((anIntArray4311[i_66_] + anIntArray4311[i_67_] + anIntArray4311[i_68_]) / 3);
							int i_70_ = ((anIntArray4342[i_66_] + anIntArray4342[i_67_] + anIntArray4342[i_68_]) / 3);
							int i_71_ = ((anIntArray4375[i_66_] + anIntArray4375[i_67_] + anIntArray4375[i_68_]) / 3);
							float f_72_ = (f_17_ + f_29_ * (float) i_69_ + f_18_ * (float) i_70_
									+ f_32_ * (float) i_71_);
							float f_73_ = (f_23_ + f_30_ * (float) i_69_ + f_24_ * (float) i_70_
									+ f_33_ * (float) i_71_);
							float f_74_ = (f + f_31_ * (float) i_69_ + f_12_ * (float) i_70_ + f_34_ * (float) i_71_);
							class128.anInt1305 = (aHa_Sub2_4366.anInt4101 + (int) (f_72_ * (float) i_38_ / (float) i));
							class128.anInt1308 = (aHa_Sub2_4366.anInt4100 + (int) (f_73_ * (float) i_39_ / (float) i));
							class128.anInt1306 = i - class316.anInt2799;
							class128.anInt1313 = (class128.anInt1314 * class316.aShort2798 * i_38_ / (i << 7));
							class128.anInt1310 = (class128.anInt1312 * class316.aShort2796 * i_39_ / (i << 7));
						}
					}
				}
				if (class338_sub5 != null) {
					boolean bool_75_ = false;
					boolean bool_76_ = true;
					int i_77_ = aShort4321 + aShort4370 >> 1;
					int i_78_ = aShort4389 + aShort4346 >> 1;
					int i_79_ = i_77_;
					short i_80_ = aShort4381;
					int i_81_ = i_78_;
					float f_82_ = (f_17_ + f_29_ * (float) i_79_ + f_18_ * (float) i_80_ + f_32_ * (float) i_81_);
					float f_83_ = (f_23_ + f_30_ * (float) i_79_ + f_24_ * (float) i_80_ + f_33_ * (float) i_81_);
					float f_84_ = (f + f_31_ * (float) i_79_ + f_12_ * (float) i_80_ + f_34_ * (float) i_81_);
					if (f_84_ >= (float) i_40_) {
						int i_85_ = (int) f_84_;
						if (i != -1)
							i_85_ = i;
						class338_sub5.anInt5225 = (aHa_Sub2_4366.anInt4101
								+ (int) (f_82_ * (float) i_38_ / (float) i_85_));
						class338_sub5.anInt5223 = (aHa_Sub2_4366.anInt4100
								+ (int) (f_83_ * (float) i_39_ / (float) i_85_));
					} else
						bool_75_ = true;
					i_79_ = i_77_;
					i_80_ = aShort4377;
					i_81_ = i_78_;
					float f_86_ = (f_17_ + f_29_ * (float) i_79_ + f_18_ * (float) i_80_ + f_32_ * (float) i_81_);
					float f_87_ = (f_23_ + f_30_ * (float) i_79_ + f_24_ * (float) i_80_ + f_33_ * (float) i_81_);
					float f_88_ = (f + f_31_ * (float) i_79_ + f_12_ * (float) i_80_ + f_34_ * (float) i_81_);
					if (f_88_ >= (float) i_40_) {
						int i_89_ = (int) f_88_;
						if (i != -1)
							i_89_ = i;
						class338_sub5.anInt5222 = (aHa_Sub2_4366.anInt4101
								+ (int) (f_86_ * (float) i_38_ / (float) i_89_));
						class338_sub5.anInt5221 = (aHa_Sub2_4366.anInt4100
								+ (int) (f_87_ * (float) i_39_ / (float) i_89_));
					} else
						bool_75_ = true;
					if (bool_75_) {
						if (f_84_ < (float) i_40_ && f_88_ < (float) i_40_)
							bool_76_ = false;
						else if (f_84_ < (float) i_40_) {
							float f_90_ = ((f_88_ - (float) aHa_Sub2_4366.anInt4107) / (f_88_ - f_84_));
							int i_91_ = (int) (f_86_ + (f_86_ - f_82_) * f_90_);
							int i_92_ = (int) (f_87_ + (f_87_ - f_83_) * f_90_);
							int i_93_ = i_40_;
							if (i != -1)
								i_93_ = i;
							class338_sub5.anInt5225 = (aHa_Sub2_4366.anInt4101 + i_91_ * i_38_ / i_93_);
							class338_sub5.anInt5223 = (aHa_Sub2_4366.anInt4100 + i_92_ * i_39_ / i_93_);
						} else if (f_88_ < (float) i_40_) {
							float f_94_ = (f_84_ - (float) i_40_) / (f_84_ - f_88_);
							int i_95_ = (int) (f_82_ + (f_82_ - f_86_) * f_94_);
							int i_96_ = (int) (f_83_ + (f_83_ - f_87_) * f_94_);
							int i_97_ = i_40_;
							if (i != -1)
								i_97_ = i;
							class338_sub5.anInt5225 = (aHa_Sub2_4366.anInt4101 + i_95_ * i_38_ / i_97_);
							class338_sub5.anInt5223 = (aHa_Sub2_4366.anInt4100 + i_96_ * i_39_ / i_97_);
						}
					}
					if (bool_76_) {
						if (f_84_ > f_88_) {
							int i_98_ = (int) f_84_;
							if (i != -1)
								i_98_ = i;
							class338_sub5.anInt5226 = (aHa_Sub2_4366.anInt4101
									+ (int) ((f_82_ + (float) aShort4344) * (float) i_38_ / (float) i_98_)
									- class338_sub5.anInt5225);
						} else {
							int i_99_ = (int) f_88_;
							if (i != -1)
								i_99_ = i;
							class338_sub5.anInt5226 = (aHa_Sub2_4366.anInt4101
									+ (int) ((f_86_ + (float) aShort4344) * (float) i_38_ / (float) i_99_)
									- class338_sub5.anInt5222);
						}
						class338_sub5.aBoolean5224 = true;
					}
				}
				method1761(true);
				aClass11_4383.aBoolean102 = (i_11_ & 0x1) == 0;
				aClass11_4383.aBoolean106 = false;
				try {
					boolean bool_100_ = (i_11_ & 0x4) != 0;
					if (bool_100_)
						method1763(bool_35_, ((aClass240_4388.aBoolean2259 && i_16_ > aClass240_4388.anInt2267)
								|| aClass240_4388.aBoolean2269), i_15_, i_16_ - i_15_);
					else
						method1748(bool_35_, ((aClass240_4388.aBoolean2259 && i_16_ > aClass240_4388.anInt2267)
								|| aClass240_4388.aBoolean2269), i_15_, i_16_ - i_15_);
				} catch (Exception exception) {
					/* empty */
				}
				if (aClass316Array4313 != null) {
					for (int i_101_ = 0; i_101_ < anInt4361; i_101_++)
						anIntArray4329[i_101_] = -1;
				}
				aClass11_4383 = null;
				if (aHa_Sub2_4366.anInt4105 > 1) {
					synchronized (this) {
						aBoolean4365 = false;
						this.notifyAll();
					}
				}
			}
		}
	}

	public void P(int i, int i_102_, int i_103_, int i_104_) {
		if (i == 0) {
			int i_105_ = 0;
			anInt4318 = 0;
			anInt4349 = 0;
			anInt4332 = 0;
			for (int i_106_ = 0; i_106_ < anInt4373; i_106_++) {
				anInt4318 += anIntArray4311[i_106_];
				anInt4349 += anIntArray4342[i_106_];
				anInt4332 += anIntArray4375[i_106_];
				i_105_++;
			}
			if (i_105_ > 0) {
				anInt4318 = anInt4318 / i_105_ + i_102_;
				anInt4349 = anInt4349 / i_105_ + i_103_;
				anInt4332 = anInt4332 / i_105_ + i_104_;
			} else {
				anInt4318 = i_102_;
				anInt4349 = i_103_;
				anInt4332 = i_104_;
			}
		} else if (i == 1) {
			for (int i_107_ = 0; i_107_ < anInt4373; i_107_++) {
				anIntArray4311[i_107_] += i_102_;
				anIntArray4342[i_107_] += i_103_;
				anIntArray4375[i_107_] += i_104_;
			}
		} else if (i == 2) {
			for (int i_108_ = 0; i_108_ < anInt4373; i_108_++) {
				anIntArray4311[i_108_] -= anInt4318;
				anIntArray4342[i_108_] -= anInt4349;
				anIntArray4375[i_108_] -= anInt4332;
				if (i_104_ != 0) {
					int i_109_ = Class296_Sub4.anIntArray4598[i_104_];
					int i_110_ = Class296_Sub4.anIntArray4618[i_104_];
					int i_111_ = ((anIntArray4342[i_108_] * i_109_ + anIntArray4311[i_108_] * i_110_ + 16383) >> 14);
					anIntArray4342[i_108_] = (anIntArray4342[i_108_] * i_110_ - anIntArray4311[i_108_] * i_109_
							+ 16383) >> 14;
					anIntArray4311[i_108_] = i_111_;
				}
				if (i_102_ != 0) {
					int i_112_ = Class296_Sub4.anIntArray4598[i_102_];
					int i_113_ = Class296_Sub4.anIntArray4618[i_102_];
					int i_114_ = ((anIntArray4342[i_108_] * i_113_ - anIntArray4375[i_108_] * i_112_ + 16383) >> 14);
					anIntArray4375[i_108_] = (anIntArray4342[i_108_] * i_112_ + anIntArray4375[i_108_] * i_113_
							+ 16383) >> 14;
					anIntArray4342[i_108_] = i_114_;
				}
				if (i_103_ != 0) {
					int i_115_ = Class296_Sub4.anIntArray4598[i_103_];
					int i_116_ = Class296_Sub4.anIntArray4618[i_103_];
					int i_117_ = ((anIntArray4375[i_108_] * i_115_ + anIntArray4311[i_108_] * i_116_ + 16383) >> 14);
					anIntArray4375[i_108_] = (anIntArray4375[i_108_] * i_116_ - anIntArray4311[i_108_] * i_115_
							+ 16383) >> 14;
					anIntArray4311[i_108_] = i_117_;
				}
				anIntArray4311[i_108_] += anInt4318;
				anIntArray4342[i_108_] += anInt4349;
				anIntArray4375[i_108_] += anInt4332;
			}
		} else if (i == 3) {
			for (int i_118_ = 0; i_118_ < anInt4373; i_118_++) {
				anIntArray4311[i_118_] -= anInt4318;
				anIntArray4342[i_118_] -= anInt4349;
				anIntArray4375[i_118_] -= anInt4332;
				anIntArray4311[i_118_] = anIntArray4311[i_118_] * i_102_ / 128;
				anIntArray4342[i_118_] = anIntArray4342[i_118_] * i_103_ / 128;
				anIntArray4375[i_118_] = anIntArray4375[i_118_] * i_104_ / 128;
				anIntArray4311[i_118_] += anInt4318;
				anIntArray4342[i_118_] += anInt4349;
				anIntArray4375[i_118_] += anInt4332;
			}
		} else if (i == 5) {
			for (int i_119_ = 0; i_119_ < anInt4361; i_119_++) {
				int i_120_ = (aByteArray4328[i_119_] & 0xff) + i_102_ * 8;
				if (i_120_ < 0)
					i_120_ = 0;
				else if (i_120_ > 255)
					i_120_ = 255;
				aByteArray4328[i_119_] = (byte) i_120_;
			}
			if (aClass316Array4313 != null) {
				for (int i_121_ = 0; i_121_ < anInt4359; i_121_++) {
					Class316 class316 = aClass316Array4313[i_121_];
					Class128 class128 = aClass128Array4316[i_121_];
					class128.anInt1311 = (class128.anInt1311 & 0xffffff
							| (255 - (aByteArray4328[class316.anInt2804] & 0xff) << 24));
				}
			}
		} else if (i == 7) {
			for (int i_122_ = 0; i_122_ < anInt4361; i_122_++) {
				int i_123_ = aShortArray4341[i_122_] & 0xffff;
				int i_124_ = i_123_ >> 10 & 0x3f;
				int i_125_ = i_123_ >> 7 & 0x7;
				int i_126_ = i_123_ & 0x7f;
				i_124_ = i_124_ + i_102_ & 0x3f;
				i_125_ += i_103_;
				if (i_125_ < 0)
					i_125_ = 0;
				else if (i_125_ > 7)
					i_125_ = 7;
				i_126_ += i_104_;
				if (i_126_ < 0)
					i_126_ = 0;
				else if (i_126_ > 127)
					i_126_ = 127;
				aShortArray4341[i_122_] = (short) (i_124_ << 10 | i_125_ << 7 | i_126_);
			}
			aBoolean4382 = true;
			if (aClass316Array4313 != null) {
				for (int i_127_ = 0; i_127_ < anInt4359; i_127_++) {
					Class316 class316 = aClass316Array4313[i_127_];
					Class128 class128 = aClass128Array4316[i_127_];
					class128.anInt1311 = (class128.anInt1311 & ~0xffffff
							| (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1
									.method3576(aShortArray4341[class316.anInt2804] & 0xffff, (byte) -46)) & 0xffff])
									& 0xffffff);
				}
			}
		} else if (i == 8) {
			for (int i_128_ = 0; i_128_ < anInt4359; i_128_++) {
				Class128 class128 = aClass128Array4316[i_128_];
				class128.anInt1309 += i_102_;
				class128.anInt1316 += i_103_;
			}
		} else if (i == 10) {
			for (int i_129_ = 0; i_129_ < anInt4359; i_129_++) {
				Class128 class128 = aClass128Array4316[i_129_];
				class128.anInt1314 = class128.anInt1314 * i_102_ >> 7;
				class128.anInt1312 = class128.anInt1312 * i_103_ >> 7;
			}
		} else if (i == 9) {
			for (int i_130_ = 0; i_130_ < anInt4359; i_130_++) {
				Class128 class128 = aClass128Array4316[i_130_];
				class128.anInt1307 = class128.anInt1307 + i_102_ & 0x3fff;
			}
		}
	}

	private void method1748(boolean bool, boolean bool_131_, int i, int i_132_) {
		if (aClass316Array4313 != null) {
			for (int i_133_ = 0; i_133_ < anInt4359; i_133_++) {
				Class316 class316 = aClass316Array4313[i_133_];
				anIntArray4329[class316.anInt2804] = i_133_;
			}
		}
		if (aBoolean4354 || aClass316Array4313 != null) {
			if ((anInt4326 & 0x100) == 0 && aShortArray4385 != null) {
				for (int i_134_ = 0; i_134_ < anInt4361; i_134_++) {
					short i_135_ = aShortArray4385[i_134_];
					method1751(i_135_, bool, bool_131_);
				}
			} else {
				for (int i_136_ = 0; i_136_ < anInt4361; i_136_++) {
					if (!method1767(i_136_) && !method1764(i_136_))
						method1751(i_136_, bool, bool_131_);
				}
				if (aByteArray4334 == null) {
					for (int i_137_ = 0; i_137_ < anInt4361; i_137_++) {
						if (method1767(i_137_) || method1764(i_137_))
							method1751(i_137_, bool, bool_131_);
					}
				} else {
					for (int i_138_ = 0; i_138_ < 12; i_138_++) {
						for (int i_139_ = 0; i_139_ < anInt4361; i_139_++) {
							if (aByteArray4334[i_139_] == i_138_ && (method1767(i_139_) || method1764(i_139_)))
								method1751(i_139_, bool, bool_131_);
						}
					}
				}
			}
		} else {
			for (int i_140_ = 0; i_140_ < anInt4361; i_140_++)
				method1751(i_140_, bool, bool_131_);
		}
	}

	private void method1749() {
		for (int i = 0; i < anInt4361; i++) {
			short i_141_ = aShortArray4350 != null ? aShortArray4350[i] : (short) -1;
			if (i_141_ == -1) {
				int i_142_ = aShortArray4341[i] & 0xffff;
				int i_143_ = (i_142_ & 0x7f) * anInt4353 >> 7;
				short i_144_ = Class338_Sub3_Sub4_Sub1.method3576(i_142_ & ~0x7f | i_143_, (byte) -104);
				if (anIntArray4331[i] == -1) {
					int i_145_ = anIntArray4308[i] & ~0x1ffff;
					anIntArray4308[i] = i_145_ | ByteStream.method2632(25051, i_145_ >> 17, i_144_);
				} else if (anIntArray4331[i] != -2) {
					int i_146_ = anIntArray4308[i] & ~0x1ffff;
					anIntArray4308[i] = i_146_ | ByteStream.method2632(25051, i_146_ >> 17, i_144_);
					i_146_ = anIntArray4338[i] & ~0x1ffff;
					anIntArray4338[i] = i_146_ | ByteStream.method2632(25051, i_146_ >> 17, i_144_);
					i_146_ = anIntArray4331[i] & ~0x1ffff;
					anIntArray4331[i] = i_146_ | ByteStream.method2632(25051, i_146_ >> 17, i_144_);
				}
			}
		}
		anInt4357 = 2;
	}

	public void aa(short i, short i_147_) {
		if (aShortArray4350 != null) {
			if (!aBoolean4340 && i_147_ >= 0) {
				MaterialRaw class170 = aHa_Sub2_4366.aD1299.method14(i_147_ & 0xffff, -9412);
				if (class170.speed_u != 0 || class170.speed_v != 0)
					aBoolean4340 = true;
			}
			for (int i_148_ = 0; i_148_ < anInt4361; i_148_++) {
				if (aShortArray4350[i_148_] == i)
					aShortArray4350[i_148_] = i_147_;
			}
		}
	}

	public boolean F() {
		return aBoolean4354;
	}

	public void s(int i) {
		if (aHa_Sub2_4366.anInt4105 > 1) {
			synchronized (this) {
				if ((anInt4326 & 0x10000) == 65536 && (i & 0x10000) == 0)
					method1761(true);
				anInt4326 = i;
			}
		} else {
			if ((anInt4326 & 0x10000) == 65536 && (i & 0x10000) == 0)
				method1761(true);
			anInt4326 = i;
		}
	}

	private int method1750(int i, int i_149_) {
		i_149_ = i_149_ * (i & 0x7f) >> 7;
		if (i_149_ < 2)
			i_149_ = 2;
		else if (i_149_ > 126)
			i_149_ = 126;
		return (i & 0xff80) + i_149_;
	}

	private void method1751(int i, boolean bool, boolean bool_150_) {
		if (anIntArray4331[i] != -2) {
			short i_151_ = aShortArray4387[i];
			short i_152_ = aShortArray4324[i];
			short i_153_ = aShortArray4327[i];
			int i_154_ = anIntArray4323[i_151_];
			int i_155_ = anIntArray4323[i_152_];
			int i_156_ = anIntArray4323[i_153_];
			if (bool && (i_154_ == -5000 || i_155_ == -5000 || i_156_ == -5000)) {
				int i_157_ = anIntArray4322[i_151_];
				int i_158_ = anIntArray4322[i_152_];
				int i_159_ = anIntArray4322[i_153_];
				int i_160_ = anIntArray4351[i_151_];
				int i_161_ = anIntArray4351[i_152_];
				int i_162_ = anIntArray4351[i_153_];
				int i_163_ = anIntArray4310[i_151_];
				int i_164_ = anIntArray4310[i_152_];
				int i_165_ = anIntArray4310[i_153_];
				i_157_ -= i_158_;
				i_159_ -= i_158_;
				i_160_ -= i_161_;
				i_162_ -= i_161_;
				i_163_ -= i_164_;
				i_165_ -= i_164_;
				int i_166_ = i_160_ * i_165_ - i_163_ * i_162_;
				int i_167_ = i_163_ * i_159_ - i_157_ * i_165_;
				int i_168_ = i_157_ * i_162_ - i_160_ * i_159_;
				if (i_158_ * i_166_ + i_161_ * i_167_ + i_164_ * i_168_ > 0)
					method1755(i);
			} else if (anIntArray4329[i] != -1 || ((i_154_ - i_155_) * (anIntArray4337[i_153_] - anIntArray4337[i_152_])
					- ((anIntArray4337[i_151_] - anIntArray4337[i_152_]) * (i_156_ - i_155_))) > 0) {
				if (i_154_ < 0 || i_155_ < 0 || i_156_ < 0 || i_154_ > aClass240_4388.anInt2273
						|| i_155_ > aClass240_4388.anInt2273 || i_156_ > aClass240_4388.anInt2273)
					aClass11_4383.aBoolean103 = true;
				else
					aClass11_4383.aBoolean103 = false;
				if (bool_150_) {
					int i_169_ = anIntArray4329[i];
					if (i_169_ == -1 || !aClass316Array4313[i_169_].aBoolean2805)
						method1771(i);
				} else {
					int i_170_ = anIntArray4329[i];
					if (i_170_ != -1) {
						Class316 class316 = aClass316Array4313[i_170_];
						Class128 class128 = aClass128Array4316[i_170_];
						if (!class316.aBoolean2805)
							method1746(i);
						aHa_Sub2_4366.method1255(class128.anInt1305, class128.anInt1308, class128.anInt1306,
								class128.anInt1313, class128.anInt1310, class128.anInt1307,
								class316.aShort2793 & 0xffff, class128.anInt1311, class316.aByte2802,
								class316.aByte2794);
					} else
						method1746(i);
				}
			}
		}
	}

	private int method1752(int i, short i_171_, int i_172_) {
		int i_173_ = Class166_Sub1.anIntArray4300[method1750(i, i_172_)];
		MaterialRaw class170 = aHa_Sub2_4366.aD1299.method14(i_171_ & 0xffff, -9412);
		int i_174_ = class170.aByte1773 & 0xff;
		if (i_174_ != 0) {
			int i_175_ = i_172_ * 131586;
			if (i_174_ == 256)
				i_173_ = i_175_;
			else {
				int i_176_ = i_174_;
				int i_177_ = 256 - i_174_;
				i_173_ = ((((i_175_ & 0xff00ff) * i_176_ + (i_173_ & 0xff00ff) * i_177_) & ~0xff00ff)
						+ (((i_175_ & 0xff00) * i_176_ + (i_173_ & 0xff00) * i_177_) & 0xff0000)) >> 8;
			}
		}
		int i_178_ = class170.aByte1793 & 0xff;
		if (i_178_ != 0) {
			i_178_ += 256;
			int i_179_ = ((i_173_ & 0xff0000) >> 16) * i_178_;
			if (i_179_ > 65535)
				i_179_ = 65535;
			int i_180_ = ((i_173_ & 0xff00) >> 8) * i_178_;
			if (i_180_ > 65535)
				i_180_ = 65535;
			int i_181_ = (i_173_ & 0xff) * i_178_;
			if (i_181_ > 65535)
				i_181_ = 65535;
			i_173_ = (i_179_ << 8 & 0xff0000) + (i_180_ & 0xff00) + (i_181_ >> 8);
		}
		return i_173_;
	}

	public void method1718(Class373 class373) {
		Class373_Sub3 class373_sub3 = (Class373_Sub3) class373;
		if (aClass89Array4374 != null) {
			for (int i = 0; i < aClass89Array4374.length; i++) {
				EmissiveTriangle class89 = aClass89Array4374[i];
				EmissiveTriangle class89_182_ = class89;
				if (class89.aClass89_956 != null)
					class89_182_ = class89.aClass89_956;
				class89_182_.anInt951 = (int) (class373_sub3.aFloat5611
						+ ((class373_sub3.aFloat5606 * (float) anIntArray4311[class89.anInt954])
								+ (class373_sub3.aFloat5613 * (float) anIntArray4342[class89.anInt954])
								+ (class373_sub3.aFloat5610 * (float) (anIntArray4375[class89.anInt954]))));
				class89_182_.anInt950 = (int) (class373_sub3.aFloat5619
						+ ((class373_sub3.aFloat5607 * (float) anIntArray4311[class89.anInt954])
								+ (class373_sub3.aFloat5615 * (float) anIntArray4342[class89.anInt954])
								+ (class373_sub3.aFloat5616 * (float) (anIntArray4375[class89.anInt954]))));
				class89_182_.anInt957 = (int) (class373_sub3.aFloat5618
						+ ((class373_sub3.aFloat5614 * (float) anIntArray4311[class89.anInt954])
								+ (class373_sub3.aFloat5617 * (float) anIntArray4342[class89.anInt954])
								+ (class373_sub3.aFloat5604 * (float) (anIntArray4375[class89.anInt954]))));
				class89_182_.anInt961 = (int) (class373_sub3.aFloat5611
						+ ((class373_sub3.aFloat5606 * (float) anIntArray4311[class89.anInt953])
								+ (class373_sub3.aFloat5613 * (float) anIntArray4342[class89.anInt953])
								+ (class373_sub3.aFloat5610 * (float) (anIntArray4375[class89.anInt953]))));
				class89_182_.anInt963 = (int) (class373_sub3.aFloat5619
						+ ((class373_sub3.aFloat5607 * (float) anIntArray4311[class89.anInt953])
								+ (class373_sub3.aFloat5615 * (float) anIntArray4342[class89.anInt953])
								+ (class373_sub3.aFloat5616 * (float) (anIntArray4375[class89.anInt953]))));
				class89_182_.anInt960 = (int) (class373_sub3.aFloat5618
						+ ((class373_sub3.aFloat5614 * (float) anIntArray4311[class89.anInt953])
								+ (class373_sub3.aFloat5617 * (float) anIntArray4342[class89.anInt953])
								+ (class373_sub3.aFloat5604 * (float) (anIntArray4375[class89.anInt953]))));
				class89_182_.anInt965 = (int) (class373_sub3.aFloat5611
						+ ((class373_sub3.aFloat5606 * (float) anIntArray4311[class89.anInt964])
								+ (class373_sub3.aFloat5613 * (float) anIntArray4342[class89.anInt964])
								+ (class373_sub3.aFloat5610 * (float) (anIntArray4375[class89.anInt964]))));
				class89_182_.anInt948 = (int) (class373_sub3.aFloat5619
						+ ((class373_sub3.aFloat5607 * (float) anIntArray4311[class89.anInt964])
								+ (class373_sub3.aFloat5615 * (float) anIntArray4342[class89.anInt964])
								+ (class373_sub3.aFloat5616 * (float) (anIntArray4375[class89.anInt964]))));
				class89_182_.anInt949 = (int) (class373_sub3.aFloat5618
						+ ((class373_sub3.aFloat5614 * (float) anIntArray4311[class89.anInt964])
								+ (class373_sub3.aFloat5617 * (float) anIntArray4342[class89.anInt964])
								+ (class373_sub3.aFloat5604 * (float) (anIntArray4375[class89.anInt964]))));
			}
		}
		if (aClass232Array4314 != null) {
			for (int i = 0; i < aClass232Array4314.length; i++) {
				EffectiveVertex class232 = aClass232Array4314[i];
				EffectiveVertex class232_183_ = class232;
				if (class232.aClass232_2223 != null)
					class232_183_ = class232.aClass232_2223;
				if (class232.aClass373_2222 != null)
					class232.aClass373_2222.method3915(class373_sub3);
				else
					class232.aClass373_2222 = class373_sub3.method3916();
				class232_183_.anInt2219 = (int) (class373_sub3.aFloat5611
						+ ((class373_sub3.aFloat5606 * (float) anIntArray4311[class232.anInt2220])
								+ (class373_sub3.aFloat5613 * (float) (anIntArray4342[class232.anInt2220]))
								+ (class373_sub3.aFloat5610 * (float) (anIntArray4375[class232.anInt2220]))));
				class232_183_.anInt2217 = (int) (class373_sub3.aFloat5619
						+ ((class373_sub3.aFloat5607 * (float) anIntArray4311[class232.anInt2220])
								+ (class373_sub3.aFloat5615 * (float) (anIntArray4342[class232.anInt2220]))
								+ (class373_sub3.aFloat5616 * (float) (anIntArray4375[class232.anInt2220]))));
				class232_183_.anInt2214 = (int) (class373_sub3.aFloat5618
						+ ((class373_sub3.aFloat5614 * (float) anIntArray4311[class232.anInt2220])
								+ (class373_sub3.aFloat5617 * (float) (anIntArray4342[class232.anInt2220]))
								+ (class373_sub3.aFloat5604 * (float) (anIntArray4375[class232.anInt2220]))));
			}
		}
	}

	private void method1753(boolean bool) {
		if (anInt4357 == 1)
			method1758();
		else if (anInt4357 == 2) {
			if ((anInt4326 & 0x97098) == 0 && aFloatArrayArray4384 == null)
				aShortArray4341 = null;
			if (bool)
				aByteArray4386 = null;
		} else {
			method1757();
			int i = aHa_Sub2_4366.anInt4093;
			int i_184_ = aHa_Sub2_4366.anInt4109;
			int i_185_ = aHa_Sub2_4366.anInt4091;
			int i_186_ = aHa_Sub2_4366.anInt4075 >> 8;
			int i_187_ = aHa_Sub2_4366.anInt4076 * 768 / anInt4371;
			int i_188_ = aHa_Sub2_4366.anInt4080 * 768 / anInt4371;
			if (anIntArray4308 == null) {
				anIntArray4308 = new int[anInt4361];
				anIntArray4338 = new int[anInt4361];
				anIntArray4331 = new int[anInt4361];
			}
			for (int i_189_ = 0; i_189_ < anInt4361; i_189_++) {
				byte i_190_;
				if (aByteArray4386 == null)
					i_190_ = (byte) 0;
				else
					i_190_ = aByteArray4386[i_189_];
				byte i_191_;
				if (aByteArray4328 == null)
					i_191_ = (byte) 0;
				else
					i_191_ = aByteArray4328[i_189_];
				short i_192_;
				if (aShortArray4350 == null)
					i_192_ = (short) -1;
				else
					i_192_ = aShortArray4350[i_189_];
				if (i_191_ == -2)
					i_190_ = (byte) 3;
				if (i_191_ == -1)
					i_190_ = (byte) 2;
				if (i_192_ == -1) {
					if (i_190_ == 0) {
						int i_193_ = aShortArray4341[i_189_] & 0xffff;
						int i_194_ = (i_193_ & 0x7f) * anInt4353 >> 7;
						short i_195_ = Class338_Sub3_Sub4_Sub1.method3576(((i_193_ & ~0x7f) | i_194_), (byte) -10);
						Class339 class339;
						if (aClass339Array4319 != null && (aClass339Array4319[aShortArray4387[i_189_]] != null))
							class339 = aClass339Array4319[aShortArray4387[i_189_]];
						else
							class339 = aClass339Array4325[aShortArray4387[i_189_]];
						int i_196_ = (((i * class339.anInt2974 + i_184_ * class339.anInt2976
								+ i_185_ * class339.anInt2973) / class339.anInt2975) >> 16);
						int i_197_ = i_196_ > 256 ? i_187_ : i_188_;
						int i_198_ = (i_186_ >> 1) + (i_197_ * i_196_ >> 17);
						anIntArray4308[i_189_] = (i_198_ << 17 | ByteStream.method2632(25051, i_198_, i_195_));
						if (aClass339Array4319 != null && (aClass339Array4319[aShortArray4324[i_189_]] != null))
							class339 = aClass339Array4319[aShortArray4324[i_189_]];
						else
							class339 = aClass339Array4325[aShortArray4324[i_189_]];
						i_196_ = ((i * class339.anInt2974 + i_184_ * class339.anInt2976 + i_185_ * class339.anInt2973)
								/ class339.anInt2975) >> 16;
						i_197_ = i_196_ > 256 ? i_187_ : i_188_;
						i_198_ = (i_186_ >> 1) + (i_197_ * i_196_ >> 17);
						anIntArray4338[i_189_] = (i_198_ << 17 | ByteStream.method2632(25051, i_198_, i_195_));
						if (aClass339Array4319 != null && (aClass339Array4319[aShortArray4327[i_189_]] != null))
							class339 = aClass339Array4319[aShortArray4327[i_189_]];
						else
							class339 = aClass339Array4325[aShortArray4327[i_189_]];
						i_196_ = ((i * class339.anInt2974 + i_184_ * class339.anInt2976 + i_185_ * class339.anInt2973)
								/ class339.anInt2975) >> 16;
						i_197_ = i_196_ > 256 ? i_187_ : i_188_;
						i_198_ = (i_186_ >> 1) + (i_197_ * i_196_ >> 17);
						anIntArray4331[i_189_] = (i_198_ << 17 | ByteStream.method2632(25051, i_198_, i_195_));
					} else if (i_190_ == 1) {
						int i_199_ = aShortArray4341[i_189_] & 0xffff;
						int i_200_ = (i_199_ & 0x7f) * anInt4353 >> 7;
						short i_201_ = Class338_Sub3_Sub4_Sub1.method3576(((i_199_ & ~0x7f) | i_200_), (byte) -79);
						Class364 class364 = aClass364Array4339[i_189_];
						int i_202_ = ((i * class364.anInt3112 + i_184_ * class364.anInt3114
								+ i_185_ * class364.anInt3113) >> 16);
						int i_203_ = i_202_ > 256 ? i_187_ : i_188_;
						int i_204_ = (i_186_ >> 1) + (i_203_ * i_202_ >> 17);
						anIntArray4308[i_189_] = (i_204_ << 17 | ByteStream.method2632(25051, i_204_, i_201_));
						anIntArray4331[i_189_] = -1;
					} else if (i_190_ == 3) {
						anIntArray4308[i_189_] = 128;
						anIntArray4331[i_189_] = -1;
					} else
						anIntArray4331[i_189_] = -2;
				} else {
					int i_205_ = aShortArray4341[i_189_] & 0xffff;
					if (i_190_ == 0) {
						Class339 class339;
						if (aClass339Array4319 != null && (aClass339Array4319[aShortArray4387[i_189_]] != null))
							class339 = aClass339Array4319[aShortArray4387[i_189_]];
						else
							class339 = aClass339Array4325[aShortArray4387[i_189_]];
						int i_206_ = (((i * class339.anInt2974 + i_184_ * class339.anInt2976
								+ i_185_ * class339.anInt2973) / class339.anInt2975) >> 16);
						int i_207_ = i_206_ > 256 ? i_187_ : i_188_;
						int i_208_ = method1770((i_186_ >> 2) + (i_207_ * i_206_ >> 18));
						anIntArray4308[i_189_] = i_208_ << 24 | method1752(i_205_, i_192_, i_208_);
						if (aClass339Array4319 != null && (aClass339Array4319[aShortArray4324[i_189_]] != null))
							class339 = aClass339Array4319[aShortArray4324[i_189_]];
						else
							class339 = aClass339Array4325[aShortArray4324[i_189_]];
						i_206_ = ((i * class339.anInt2974 + i_184_ * class339.anInt2976 + i_185_ * class339.anInt2973)
								/ class339.anInt2975) >> 16;
						i_207_ = i_206_ > 256 ? i_187_ : i_188_;
						i_208_ = method1770((i_186_ >> 2) + (i_207_ * i_206_ >> 18));
						anIntArray4338[i_189_] = i_208_ << 24 | method1752(i_205_, i_192_, i_208_);
						if (aClass339Array4319 != null && (aClass339Array4319[aShortArray4327[i_189_]] != null))
							class339 = aClass339Array4319[aShortArray4327[i_189_]];
						else
							class339 = aClass339Array4325[aShortArray4327[i_189_]];
						i_206_ = ((i * class339.anInt2974 + i_184_ * class339.anInt2976 + i_185_ * class339.anInt2973)
								/ class339.anInt2975) >> 16;
						i_207_ = i_206_ > 256 ? i_187_ : i_188_;
						i_208_ = method1770((i_186_ >> 2) + (i_207_ * i_206_ >> 18));
						anIntArray4331[i_189_] = i_208_ << 24 | method1752(i_205_, i_192_, i_208_);
					} else if (i_190_ == 1) {
						Class364 class364 = aClass364Array4339[i_189_];
						int i_209_ = ((i * class364.anInt3112 + i_184_ * class364.anInt3114
								+ i_185_ * class364.anInt3113) >> 16);
						int i_210_ = i_209_ > 256 ? i_187_ : i_188_;
						int i_211_ = method1770((i_186_ >> 2) + (i_210_ * i_209_ >> 18));
						anIntArray4308[i_189_] = i_211_ << 24 | method1752(i_205_, i_192_, i_211_);
						anIntArray4331[i_189_] = -1;
					} else
						anIntArray4331[i_189_] = -2;
				}
			}
			aClass339Array4325 = null;
			aClass339Array4319 = null;
			aClass364Array4339 = null;
			if ((anInt4326 & 0x97098) == 0 && aFloatArrayArray4384 == null)
				aShortArray4341 = null;
			if (bool)
				aByteArray4386 = null;
			anInt4357 = 2;
		}
	}

	private boolean method1754(int i, int i_212_, Class373 class373, boolean bool, int i_213_, int i_214_) {
		aClass373_Sub3_4355 = (Class373_Sub3) class373;
		Class373_Sub3 class373_sub3 = aHa_Sub2_4366.aClass373_Sub3_4090;
		float f = (class373_sub3.aFloat5611 + (class373_sub3.aFloat5606 * aClass373_Sub3_4355.aFloat5611
				+ class373_sub3.aFloat5613 * aClass373_Sub3_4355.aFloat5619
				+ (class373_sub3.aFloat5610 * aClass373_Sub3_4355.aFloat5618)));
		float f_215_ = (class373_sub3.aFloat5619 + (class373_sub3.aFloat5607 * aClass373_Sub3_4355.aFloat5611
				+ class373_sub3.aFloat5615 * aClass373_Sub3_4355.aFloat5619
				+ (class373_sub3.aFloat5616 * aClass373_Sub3_4355.aFloat5618)));
		float f_216_ = (class373_sub3.aFloat5618 + (class373_sub3.aFloat5614 * aClass373_Sub3_4355.aFloat5611
				+ class373_sub3.aFloat5617 * aClass373_Sub3_4355.aFloat5619
				+ (class373_sub3.aFloat5604 * aClass373_Sub3_4355.aFloat5618)));
		float f_217_ = (class373_sub3.aFloat5606 * aClass373_Sub3_4355.aFloat5606
				+ class373_sub3.aFloat5613 * aClass373_Sub3_4355.aFloat5607
				+ class373_sub3.aFloat5610 * aClass373_Sub3_4355.aFloat5614);
		float f_218_ = (class373_sub3.aFloat5606 * aClass373_Sub3_4355.aFloat5613
				+ class373_sub3.aFloat5613 * aClass373_Sub3_4355.aFloat5615
				+ class373_sub3.aFloat5610 * aClass373_Sub3_4355.aFloat5617);
		float f_219_ = (class373_sub3.aFloat5606 * aClass373_Sub3_4355.aFloat5610
				+ class373_sub3.aFloat5613 * aClass373_Sub3_4355.aFloat5616
				+ class373_sub3.aFloat5610 * aClass373_Sub3_4355.aFloat5604);
		float f_220_ = (class373_sub3.aFloat5607 * aClass373_Sub3_4355.aFloat5606
				+ class373_sub3.aFloat5615 * aClass373_Sub3_4355.aFloat5607
				+ class373_sub3.aFloat5616 * aClass373_Sub3_4355.aFloat5614);
		float f_221_ = (class373_sub3.aFloat5607 * aClass373_Sub3_4355.aFloat5613
				+ class373_sub3.aFloat5615 * aClass373_Sub3_4355.aFloat5615
				+ class373_sub3.aFloat5616 * aClass373_Sub3_4355.aFloat5617);
		float f_222_ = (class373_sub3.aFloat5607 * aClass373_Sub3_4355.aFloat5610
				+ class373_sub3.aFloat5615 * aClass373_Sub3_4355.aFloat5616
				+ class373_sub3.aFloat5616 * aClass373_Sub3_4355.aFloat5604);
		float f_223_ = (class373_sub3.aFloat5614 * aClass373_Sub3_4355.aFloat5606
				+ class373_sub3.aFloat5617 * aClass373_Sub3_4355.aFloat5607
				+ class373_sub3.aFloat5604 * aClass373_Sub3_4355.aFloat5614);
		float f_224_ = (class373_sub3.aFloat5614 * aClass373_Sub3_4355.aFloat5613
				+ class373_sub3.aFloat5617 * aClass373_Sub3_4355.aFloat5615
				+ class373_sub3.aFloat5604 * aClass373_Sub3_4355.aFloat5617);
		float f_225_ = (class373_sub3.aFloat5614 * aClass373_Sub3_4355.aFloat5610
				+ class373_sub3.aFloat5617 * aClass373_Sub3_4355.aFloat5616
				+ class373_sub3.aFloat5604 * aClass373_Sub3_4355.aFloat5604);
		boolean bool_226_ = false;
		int i_227_ = aHa_Sub2_4366.anInt4101;
		int i_228_ = aHa_Sub2_4366.anInt4100;
		int i_229_ = aHa_Sub2_4366.anInt4086;
		int i_230_ = aHa_Sub2_4366.anInt4072;
		int i_231_ = 2147483647;
		int i_232_ = -2147483648;
		int i_233_ = 2147483647;
		int i_234_ = -2147483648;
		method1766(Thread.currentThread());
		if (!aBoolean4362)
			method1759();
		int i_235_ = aShort4370 - aShort4321 >> 1;
		int i_236_ = aShort4377 - aShort4381 >> 1;
		int i_237_ = aShort4346 - aShort4389 >> 1;
		int i_238_ = aShort4321 + i_235_;
		int i_239_ = aShort4381 + i_236_;
		int i_240_ = aShort4389 + i_237_;
		int i_241_ = i_238_ - (i_235_ << i_213_);
		int i_242_ = i_239_ - (i_236_ << i_213_);
		int i_243_ = i_240_ - (i_237_ << i_213_);
		int i_244_ = i_238_ + (i_235_ << i_213_);
		int i_245_ = i_239_ + (i_236_ << i_213_);
		int i_246_ = i_240_ + (i_237_ << i_213_);
		anIntArray4358[0] = i_241_;
		anIntArray4317[0] = i_242_;
		anIntArray4335[0] = i_243_;
		anIntArray4358[1] = i_244_;
		anIntArray4317[1] = i_242_;
		anIntArray4335[1] = i_243_;
		anIntArray4358[2] = i_241_;
		anIntArray4317[2] = i_245_;
		anIntArray4335[2] = i_243_;
		anIntArray4358[3] = i_244_;
		anIntArray4317[3] = i_245_;
		anIntArray4335[3] = i_243_;
		anIntArray4358[4] = i_241_;
		anIntArray4317[4] = i_242_;
		anIntArray4335[4] = i_246_;
		anIntArray4358[5] = i_244_;
		anIntArray4317[5] = i_242_;
		anIntArray4335[5] = i_246_;
		anIntArray4358[6] = i_241_;
		anIntArray4317[6] = i_245_;
		anIntArray4335[6] = i_246_;
		anIntArray4358[7] = i_244_;
		anIntArray4317[7] = i_245_;
		anIntArray4335[7] = i_246_;
		for (int i_247_ = 0; i_247_ < 8; i_247_++) {
			int i_248_ = anIntArray4358[i_247_];
			int i_249_ = anIntArray4317[i_247_];
			int i_250_ = anIntArray4335[i_247_];
			float f_251_ = f + (f_217_ * (float) i_248_ + f_218_ * (float) i_249_ + f_219_ * (float) i_250_);
			float f_252_ = f_215_ + (f_220_ * (float) i_248_ + f_221_ * (float) i_249_ + f_222_ * (float) i_250_);
			float f_253_ = f_216_ + (f_223_ * (float) i_248_ + f_224_ * (float) i_249_ + f_225_ * (float) i_250_);
			if (f_253_ >= (float) aHa_Sub2_4366.anInt4107) {
				if (i_214_ > 0)
					f_253_ = (float) i_214_;
				int i_254_ = i_227_ + (int) (f_251_ * (float) i_229_ / f_253_);
				int i_255_ = i_228_ + (int) (f_252_ * (float) i_230_ / f_253_);
				if (i_254_ < i_231_)
					i_231_ = i_254_;
				if (i_254_ > i_232_)
					i_232_ = i_254_;
				if (i_255_ < i_233_)
					i_233_ = i_255_;
				if (i_255_ > i_234_)
					i_234_ = i_255_;
				bool_226_ = true;
			}
		}
		if (bool_226_ && i > i_231_ && i < i_232_ && i_212_ > i_233_ && i_212_ < i_234_) {
			if (bool)
				return true;
			for (int i_256_ = 0; i_256_ < anInt4373; i_256_++) {
				int i_257_ = anIntArray4311[i_256_];
				int i_258_ = anIntArray4342[i_256_];
				int i_259_ = anIntArray4375[i_256_];
				float f_260_ = f + (f_217_ * (float) i_257_ + f_218_ * (float) i_258_ + f_219_ * (float) i_259_);
				float f_261_ = f_215_ + (f_220_ * (float) i_257_ + f_221_ * (float) i_258_ + f_222_ * (float) i_259_);
				float f_262_ = f_216_ + (f_223_ * (float) i_257_ + f_224_ * (float) i_258_ + f_225_ * (float) i_259_);
				if (f_262_ >= (float) aHa_Sub2_4366.anInt4107) {
					if (i_214_ > 0)
						f_262_ = (float) i_214_;
					anIntArray4323[i_256_] = i_227_ + (int) (f_260_ * (float) i_229_ / f_262_);
					anIntArray4337[i_256_] = i_228_ + (int) (f_261_ * (float) i_230_ / f_262_);
				} else
					anIntArray4323[i_256_] = -999999;
			}
			for (int i_263_ = 0; i_263_ < anInt4361; i_263_++) {
				if (anIntArray4323[aShortArray4387[i_263_]] != -999999
						&& anIntArray4323[aShortArray4324[i_263_]] != -999999
						&& anIntArray4323[aShortArray4327[i_263_]] != -999999
						&& method1779(i, i_212_, anIntArray4337[aShortArray4387[i_263_]],
								anIntArray4337[aShortArray4324[i_263_]], anIntArray4337[aShortArray4327[i_263_]],
								anIntArray4323[aShortArray4387[i_263_]], anIntArray4323[aShortArray4324[i_263_]],
								anIntArray4323[aShortArray4327[i_263_]]))
					return true;
			}
		}
		return false;
	}

	private void method1755(int i) {
		int i_264_ = 0;
		int i_265_ = aHa_Sub2_4366.anInt4107;
		short i_266_ = aShortArray4387[i];
		short i_267_ = aShortArray4324[i];
		short i_268_ = aShortArray4327[i];
		int i_269_ = anIntArray4310[i_266_];
		int i_270_ = anIntArray4310[i_267_];
		int i_271_ = anIntArray4310[i_268_];
		if (aByteArray4328 == null)
			aClass11_4383.anInt108 = 0;
		else
			aClass11_4383.anInt108 = aByteArray4328[i] & 0xff;
		if (i_269_ >= i_265_) {
			anIntArray4379[i_264_] = anIntArray4323[i_266_];
			anIntArray4348[i_264_] = anIntArray4337[i_266_];
			anIntArray4343[i_264_] = anIntArray4360[i_266_];
			anIntArray4330[i_264_++] = anIntArray4308[i] & 0xffff;
		} else {
			int i_272_ = anIntArray4322[i_266_];
			int i_273_ = anIntArray4351[i_266_];
			int i_274_ = anIntArray4308[i] & 0xffff;
			if (i_271_ >= i_265_) {
				int i_275_ = (i_265_ - i_269_) * (65536 / (i_271_ - i_269_));
				anIntArray4379[i_264_] = (aClass240_4388.anInt2271
						+ ((i_272_ + ((anIntArray4322[i_268_] - i_272_) * i_275_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_265_));
				anIntArray4348[i_264_] = (aClass240_4388.anInt2272
						+ ((i_273_ + ((anIntArray4351[i_268_] - i_273_) * i_275_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_265_));
				anIntArray4343[i_264_] = i_265_;
				anIntArray4330[i_264_++] = (i_274_ + (((anIntArray4331[i] & 0xffff) - i_274_) * i_275_ >> 16));
			}
			if (i_270_ >= i_265_) {
				int i_276_ = (i_265_ - i_269_) * (65536 / (i_270_ - i_269_));
				anIntArray4379[i_264_] = (aClass240_4388.anInt2271
						+ ((i_272_ + ((anIntArray4322[i_267_] - i_272_) * i_276_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_265_));
				anIntArray4348[i_264_] = (aClass240_4388.anInt2272
						+ ((i_273_ + ((anIntArray4351[i_267_] - i_273_) * i_276_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_265_));
				anIntArray4343[i_264_] = i_265_;
				anIntArray4330[i_264_++] = (i_274_ + (((anIntArray4338[i] & 0xffff) - i_274_) * i_276_ >> 16));
			}
		}
		if (i_270_ >= i_265_) {
			anIntArray4379[i_264_] = anIntArray4323[i_267_];
			anIntArray4348[i_264_] = anIntArray4337[i_267_];
			anIntArray4343[i_264_] = anIntArray4360[i_267_];
			anIntArray4330[i_264_++] = anIntArray4338[i] & 0xffff;
		} else {
			int i_277_ = anIntArray4322[i_267_];
			int i_278_ = anIntArray4351[i_267_];
			int i_279_ = anIntArray4338[i] & 0xffff;
			if (i_269_ >= i_265_) {
				int i_280_ = (i_265_ - i_270_) * (65536 / (i_269_ - i_270_));
				anIntArray4379[i_264_] = (aClass240_4388.anInt2271
						+ ((i_277_ + ((anIntArray4322[i_266_] - i_277_) * i_280_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_265_));
				anIntArray4348[i_264_] = (aClass240_4388.anInt2272
						+ ((i_278_ + ((anIntArray4351[i_266_] - i_278_) * i_280_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_265_));
				anIntArray4343[i_264_] = i_265_;
				anIntArray4330[i_264_++] = (i_279_ + (((anIntArray4308[i] & 0xffff) - i_279_) * i_280_ >> 16));
			}
			if (i_271_ >= i_265_) {
				int i_281_ = (i_265_ - i_270_) * (65536 / (i_271_ - i_270_));
				anIntArray4379[i_264_] = (aClass240_4388.anInt2271
						+ ((i_277_ + ((anIntArray4322[i_268_] - i_277_) * i_281_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_265_));
				anIntArray4348[i_264_] = (aClass240_4388.anInt2272
						+ ((i_278_ + ((anIntArray4351[i_268_] - i_278_) * i_281_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_265_));
				anIntArray4343[i_264_] = i_265_;
				anIntArray4330[i_264_++] = (i_279_ + (((anIntArray4331[i] & 0xffff) - i_279_) * i_281_ >> 16));
			}
		}
		if (i_271_ >= i_265_) {
			anIntArray4379[i_264_] = anIntArray4323[i_268_];
			anIntArray4348[i_264_] = anIntArray4337[i_268_];
			anIntArray4343[i_264_] = anIntArray4360[i_268_];
			anIntArray4330[i_264_++] = anIntArray4331[i] & 0xffff;
		} else {
			int i_282_ = anIntArray4322[i_268_];
			int i_283_ = anIntArray4351[i_268_];
			int i_284_ = anIntArray4331[i] & 0xffff;
			if (i_270_ >= i_265_) {
				int i_285_ = (i_265_ - i_271_) * (65536 / (i_270_ - i_271_));
				anIntArray4379[i_264_] = (aClass240_4388.anInt2271
						+ ((i_282_ + ((anIntArray4322[i_267_] - i_282_) * i_285_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_265_));
				anIntArray4348[i_264_] = (aClass240_4388.anInt2272
						+ ((i_283_ + ((anIntArray4351[i_267_] - i_283_) * i_285_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_265_));
				anIntArray4343[i_264_] = i_265_;
				anIntArray4330[i_264_++] = (i_284_ + (((anIntArray4338[i] & 0xffff) - i_284_) * i_285_ >> 16));
			}
			if (i_269_ >= i_265_) {
				int i_286_ = (i_265_ - i_271_) * (65536 / (i_269_ - i_271_));
				anIntArray4379[i_264_] = (aClass240_4388.anInt2271
						+ ((i_282_ + ((anIntArray4322[i_266_] - i_282_) * i_286_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_265_));
				anIntArray4348[i_264_] = (aClass240_4388.anInt2272
						+ ((i_283_ + ((anIntArray4351[i_266_] - i_283_) * i_286_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_265_));
				anIntArray4343[i_264_] = i_265_;
				anIntArray4330[i_264_++] = (i_284_ + (((anIntArray4308[i] & 0xffff) - i_284_) * i_286_ >> 16));
			}
		}
		int i_287_ = anIntArray4379[0];
		int i_288_ = anIntArray4379[1];
		int i_289_ = anIntArray4379[2];
		int i_290_ = anIntArray4348[0];
		int i_291_ = anIntArray4348[1];
		int i_292_ = anIntArray4348[2];
		i_269_ = anIntArray4343[0];
		i_270_ = anIntArray4343[1];
		i_271_ = anIntArray4343[2];
		aClass11_4383.aBoolean103 = false;
		if (i_264_ == 3) {
			if (i_287_ < 0 || i_288_ < 0 || i_289_ < 0 || i_287_ > aClass240_4388.anInt2273
					|| i_288_ > aClass240_4388.anInt2273 || i_289_ > aClass240_4388.anInt2273)
				aClass11_4383.aBoolean103 = true;
			if (aShortArray4350 == null || aShortArray4350[i] == -1) {
				if (anIntArray4331[i] == -1)
					aClass11_4383.method206((float) i_290_, (float) i_291_, (float) i_292_, (float) i_287_,
							(float) i_288_, (float) i_289_, (float) i_269_, (float) i_270_, (float) i_271_,
							(Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]));
				else
					aClass11_4383.method205((float) i_290_, (float) i_291_, (float) i_292_, (float) i_287_,
							(float) i_288_, (float) i_289_, (float) i_269_, (float) i_270_, (float) i_271_,
							(float) anIntArray4330[0], (float) anIntArray4330[1], (float) anIntArray4330[2]);
			} else {
				int i_293_ = -16777216;
				if (aByteArray4328 != null)
					i_293_ = 255 - (aByteArray4328[i] & 0xff) << 24;
				int i_294_ = i_293_ | anIntArray4308[i] & 0xffffff;
				if (anIntArray4331[i] == -1)
					aClass11_4383.method214((float) i_290_, (float) i_291_, (float) i_292_, (float) i_287_,
							(float) i_288_, (float) i_289_, (float) i_269_, (float) i_270_, (float) i_271_,
							aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2],
							aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2], i_294_,
							i_294_, i_294_, aClass240_4388.anInt2265, 0, 0, 0, aShortArray4350[i]);
				else
					aClass11_4383.method214((float) i_290_, (float) i_291_, (float) i_292_, (float) i_287_,
							(float) i_288_, (float) i_289_, (float) i_269_, (float) i_270_, (float) i_271_,
							aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2],
							aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2], i_294_,
							i_294_, i_294_, aClass240_4388.anInt2265, 0, 0, 0, aShortArray4350[i]);
			}
		}
		if (i_264_ == 4) {
			if (i_287_ < 0 || i_288_ < 0 || i_289_ < 0 || i_287_ > aClass240_4388.anInt2273
					|| i_288_ > aClass240_4388.anInt2273 || i_289_ > aClass240_4388.anInt2273 || anIntArray4379[3] < 0
					|| anIntArray4379[3] > aClass240_4388.anInt2273)
				aClass11_4383.aBoolean103 = true;
			if (aShortArray4350 == null || aShortArray4350[i] == -1) {
				if (anIntArray4331[i] == -1) {
					int i_295_ = (Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]);
					aClass11_4383.method206((float) i_290_, (float) i_291_, (float) i_292_, (float) i_287_,
							(float) i_288_, (float) i_289_, (float) i_269_, (float) i_270_, (float) i_271_, i_295_);
					aClass11_4383.method206((float) i_290_, (float) i_292_, (float) anIntArray4348[3], (float) i_287_,
							(float) i_289_, (float) anIntArray4379[3], (float) i_269_, (float) i_270_,
							(float) anIntArray4343[3], i_295_);
				} else {
					aClass11_4383.method205((float) i_290_, (float) i_291_, (float) i_292_, (float) i_287_,
							(float) i_288_, (float) i_289_, (float) i_269_, (float) i_270_, (float) i_271_,
							(float) anIntArray4330[0], (float) anIntArray4330[1], (float) anIntArray4330[2]);
					aClass11_4383.method205((float) i_290_, (float) i_292_, (float) anIntArray4348[3], (float) i_287_,
							(float) i_289_, (float) anIntArray4379[3], (float) i_269_, (float) i_270_,
							(float) anIntArray4343[3], (float) anIntArray4330[0], (float) anIntArray4330[2],
							(float) anIntArray4330[3]);
				}
			} else {
				int i_296_ = -16777216;
				if (aByteArray4328 != null)
					i_296_ = 255 - (aByteArray4328[i] & 0xff) << 24;
				int i_297_ = i_296_ | anIntArray4308[i] & 0xffffff;
				if (anIntArray4331[i] == -1) {
					aClass11_4383.method214((float) i_290_, (float) i_291_, (float) i_292_, (float) i_287_,
							(float) i_288_, (float) i_289_, (float) i_269_, (float) i_270_, (float) i_271_,
							aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2],
							aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2], i_297_,
							i_297_, i_297_, aClass240_4388.anInt2265, 0, 0, 0, aShortArray4350[i]);
					aClass11_4383.method214((float) i_290_, (float) i_292_, (float) anIntArray4348[3], (float) i_287_,
							(float) i_289_, (float) anIntArray4379[3], (float) i_269_, (float) i_271_,
							(float) anIntArray4343[3], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
							aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
							aFloatArrayArray4376[i][2], i_297_, i_297_, i_297_, aClass240_4388.anInt2265, 0, 0, 0,
							aShortArray4350[i]);
				} else {
					aClass11_4383.method214((float) i_290_, (float) i_291_, (float) i_292_, (float) i_287_,
							(float) i_288_, (float) i_289_, (float) i_269_, (float) i_270_, (float) i_271_,
							aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2],
							aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2], i_297_,
							i_297_, i_297_, aClass240_4388.anInt2265, 0, 0, 0, aShortArray4350[i]);
					aClass11_4383.method214((float) i_290_, (float) i_292_, (float) anIntArray4348[3], (float) i_287_,
							(float) i_289_, (float) anIntArray4379[3], (float) i_269_, (float) i_271_,
							(float) anIntArray4343[3], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
							aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
							aFloatArrayArray4376[i][2], i_297_, i_297_, i_297_, aClass240_4388.anInt2265, 0, 0, 0,
							aShortArray4350[i]);
				}
			}
		}
	}

	public void method1725(int i, int[] is, int i_298_, int i_299_, int i_300_, int i_301_, boolean bool) {
		int i_302_ = is.length;
		if (i == 0) {
			i_298_ <<= 4;
			i_299_ <<= 4;
			i_300_ <<= 4;
			if (!aBoolean4315) {
				for (int i_303_ = 0; i_303_ < anInt4373; i_303_++) {
					anIntArray4311[i_303_] <<= 4;
					anIntArray4342[i_303_] <<= 4;
					anIntArray4375[i_303_] <<= 4;
				}
				aBoolean4315 = true;
			}
			int i_304_ = 0;
			anInt4318 = 0;
			anInt4349 = 0;
			anInt4332 = 0;
			for (int i_305_ = 0; i_305_ < i_302_; i_305_++) {
				int i_306_ = is[i_305_];
				if (i_306_ < anIntArrayArray4367.length) {
					int[] is_307_ = anIntArrayArray4367[i_306_];
					for (int i_308_ = 0; i_308_ < is_307_.length; i_308_++) {
						int i_309_ = is_307_[i_308_];
						anInt4318 += anIntArray4311[i_309_];
						anInt4349 += anIntArray4342[i_309_];
						anInt4332 += anIntArray4375[i_309_];
						i_304_++;
					}
				}
			}
			if (i_304_ > 0) {
				anInt4318 = anInt4318 / i_304_ + i_298_;
				anInt4349 = anInt4349 / i_304_ + i_299_;
				anInt4332 = anInt4332 / i_304_ + i_300_;
			} else {
				anInt4318 = i_298_;
				anInt4349 = i_299_;
				anInt4332 = i_300_;
			}
		} else if (i == 1) {
			i_298_ <<= 4;
			i_299_ <<= 4;
			i_300_ <<= 4;
			if (!aBoolean4315) {
				for (int i_310_ = 0; i_310_ < anInt4373; i_310_++) {
					anIntArray4311[i_310_] <<= 4;
					anIntArray4342[i_310_] <<= 4;
					anIntArray4375[i_310_] <<= 4;
				}
				aBoolean4315 = true;
			}
			for (int i_311_ = 0; i_311_ < i_302_; i_311_++) {
				int i_312_ = is[i_311_];
				if (i_312_ < anIntArrayArray4367.length) {
					int[] is_313_ = anIntArrayArray4367[i_312_];
					for (int i_314_ = 0; i_314_ < is_313_.length; i_314_++) {
						int i_315_ = is_313_[i_314_];
						anIntArray4311[i_315_] += i_298_;
						anIntArray4342[i_315_] += i_299_;
						anIntArray4375[i_315_] += i_300_;
					}
				}
			}
		} else if (i == 2) {
			for (int i_316_ = 0; i_316_ < i_302_; i_316_++) {
				int i_317_ = is[i_316_];
				if (i_317_ < anIntArrayArray4367.length) {
					int[] is_318_ = anIntArrayArray4367[i_317_];
					if ((i_301_ & 0x1) == 0) {
						for (int i_319_ = 0; i_319_ < is_318_.length; i_319_++) {
							int i_320_ = is_318_[i_319_];
							anIntArray4311[i_320_] -= anInt4318;
							anIntArray4342[i_320_] -= anInt4349;
							anIntArray4375[i_320_] -= anInt4332;
							if (i_300_ != 0) {
								int i_321_ = Class296_Sub4.anIntArray4598[i_300_];
								int i_322_ = Class296_Sub4.anIntArray4618[i_300_];
								int i_323_ = (anIntArray4342[i_320_] * i_321_ + anIntArray4311[i_320_] * i_322_
										+ 16383) >> 14;
								anIntArray4342[i_320_] = (anIntArray4342[i_320_] * i_322_
										- anIntArray4311[i_320_] * i_321_ + 16383) >> 14;
								anIntArray4311[i_320_] = i_323_;
							}
							if (i_298_ != 0) {
								int i_324_ = Class296_Sub4.anIntArray4598[i_298_];
								int i_325_ = Class296_Sub4.anIntArray4618[i_298_];
								int i_326_ = (anIntArray4342[i_320_] * i_325_ - anIntArray4375[i_320_] * i_324_
										+ 16383) >> 14;
								anIntArray4375[i_320_] = (anIntArray4342[i_320_] * i_324_
										+ anIntArray4375[i_320_] * i_325_ + 16383) >> 14;
								anIntArray4342[i_320_] = i_326_;
							}
							if (i_299_ != 0) {
								int i_327_ = Class296_Sub4.anIntArray4598[i_299_];
								int i_328_ = Class296_Sub4.anIntArray4618[i_299_];
								int i_329_ = (anIntArray4375[i_320_] * i_327_ + anIntArray4311[i_320_] * i_328_
										+ 16383) >> 14;
								anIntArray4375[i_320_] = (anIntArray4375[i_320_] * i_328_
										- anIntArray4311[i_320_] * i_327_ + 16383) >> 14;
								anIntArray4311[i_320_] = i_329_;
							}
							anIntArray4311[i_320_] += anInt4318;
							anIntArray4342[i_320_] += anInt4349;
							anIntArray4375[i_320_] += anInt4332;
						}
					} else {
						for (int i_330_ = 0; i_330_ < is_318_.length; i_330_++) {
							int i_331_ = is_318_[i_330_];
							anIntArray4311[i_331_] -= anInt4318;
							anIntArray4342[i_331_] -= anInt4349;
							anIntArray4375[i_331_] -= anInt4332;
							if (i_298_ != 0) {
								int i_332_ = Class296_Sub4.anIntArray4598[i_298_];
								int i_333_ = Class296_Sub4.anIntArray4618[i_298_];
								int i_334_ = (anIntArray4342[i_331_] * i_333_ - anIntArray4375[i_331_] * i_332_
										+ 16383) >> 14;
								anIntArray4375[i_331_] = (anIntArray4342[i_331_] * i_332_
										+ anIntArray4375[i_331_] * i_333_ + 16383) >> 14;
								anIntArray4342[i_331_] = i_334_;
							}
							if (i_300_ != 0) {
								int i_335_ = Class296_Sub4.anIntArray4598[i_300_];
								int i_336_ = Class296_Sub4.anIntArray4618[i_300_];
								int i_337_ = (anIntArray4342[i_331_] * i_335_ + anIntArray4311[i_331_] * i_336_
										+ 16383) >> 14;
								anIntArray4342[i_331_] = (anIntArray4342[i_331_] * i_336_
										- anIntArray4311[i_331_] * i_335_ + 16383) >> 14;
								anIntArray4311[i_331_] = i_337_;
							}
							if (i_299_ != 0) {
								int i_338_ = Class296_Sub4.anIntArray4598[i_299_];
								int i_339_ = Class296_Sub4.anIntArray4618[i_299_];
								int i_340_ = (anIntArray4375[i_331_] * i_338_ + anIntArray4311[i_331_] * i_339_
										+ 16383) >> 14;
								anIntArray4375[i_331_] = (anIntArray4375[i_331_] * i_339_
										- anIntArray4311[i_331_] * i_338_ + 16383) >> 14;
								anIntArray4311[i_331_] = i_340_;
							}
							anIntArray4311[i_331_] += anInt4318;
							anIntArray4342[i_331_] += anInt4349;
							anIntArray4375[i_331_] += anInt4332;
						}
					}
				}
			}
		} else if (i == 3) {
			for (int i_341_ = 0; i_341_ < i_302_; i_341_++) {
				int i_342_ = is[i_341_];
				if (i_342_ < anIntArrayArray4367.length) {
					int[] is_343_ = anIntArrayArray4367[i_342_];
					for (int i_344_ = 0; i_344_ < is_343_.length; i_344_++) {
						int i_345_ = is_343_[i_344_];
						anIntArray4311[i_345_] -= anInt4318;
						anIntArray4342[i_345_] -= anInt4349;
						anIntArray4375[i_345_] -= anInt4332;
						anIntArray4311[i_345_] = anIntArray4311[i_345_] * i_298_ / 128;
						anIntArray4342[i_345_] = anIntArray4342[i_345_] * i_299_ / 128;
						anIntArray4375[i_345_] = anIntArray4375[i_345_] * i_300_ / 128;
						anIntArray4311[i_345_] += anInt4318;
						anIntArray4342[i_345_] += anInt4349;
						anIntArray4375[i_345_] += anInt4332;
					}
				}
			}
		} else if (i == 5) {
			if (anIntArrayArray4368 != null && aByteArray4328 != null) {
				for (int i_346_ = 0; i_346_ < i_302_; i_346_++) {
					int i_347_ = is[i_346_];
					if (i_347_ < anIntArrayArray4368.length) {
						int[] is_348_ = anIntArrayArray4368[i_347_];
						for (int i_349_ = 0; i_349_ < is_348_.length; i_349_++) {
							int i_350_ = is_348_[i_349_];
							int i_351_ = (aByteArray4328[i_350_] & 0xff) + i_298_ * 8;
							if (i_351_ < 0)
								i_351_ = 0;
							else if (i_351_ > 255)
								i_351_ = 255;
							aByteArray4328[i_350_] = (byte) i_351_;
						}
					}
				}
				if (aClass316Array4313 != null) {
					for (int i_352_ = 0; i_352_ < anInt4359; i_352_++) {
						Class316 class316 = aClass316Array4313[i_352_];
						Class128 class128 = aClass128Array4316[i_352_];
						class128.anInt1311 = (class128.anInt1311 & 0xffffff
								| 255 - (aByteArray4328[class316.anInt2804] & 0xff) << 24);
					}
				}
			}
		} else if (i == 7) {
			if (anIntArrayArray4368 != null) {
				for (int i_353_ = 0; i_353_ < i_302_; i_353_++) {
					int i_354_ = is[i_353_];
					if (i_354_ < anIntArrayArray4368.length) {
						int[] is_355_ = anIntArrayArray4368[i_354_];
						for (int i_356_ = 0; i_356_ < is_355_.length; i_356_++) {
							int i_357_ = is_355_[i_356_];
							int i_358_ = aShortArray4341[i_357_] & 0xffff;
							int i_359_ = i_358_ >> 10 & 0x3f;
							int i_360_ = i_358_ >> 7 & 0x7;
							int i_361_ = i_358_ & 0x7f;
							i_359_ = i_359_ + i_298_ & 0x3f;
							i_360_ += i_299_;
							if (i_360_ < 0)
								i_360_ = 0;
							else if (i_360_ > 7)
								i_360_ = 7;
							i_361_ += i_300_;
							if (i_361_ < 0)
								i_361_ = 0;
							else if (i_361_ > 127)
								i_361_ = 127;
							aShortArray4341[i_357_] = (short) (i_359_ << 10 | i_360_ << 7 | i_361_);
						}
						aBoolean4382 = true;
					}
				}
				if (aClass316Array4313 != null) {
					for (int i_362_ = 0; i_362_ < anInt4359; i_362_++) {
						Class316 class316 = aClass316Array4313[i_362_];
						Class128 class128 = aClass128Array4316[i_362_];
						class128.anInt1311 = (class128.anInt1311 & ~0xffffff
								| (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1
										.method3576((aShortArray4341[class316.anInt2804] & 0xffff), (byte) -16))
										& 0xffff]) & 0xffffff);
					}
				}
			}
		} else if (i == 8) {
			if (anIntArrayArray4333 != null) {
				for (int i_363_ = 0; i_363_ < i_302_; i_363_++) {
					int i_364_ = is[i_363_];
					if (i_364_ < anIntArrayArray4333.length) {
						int[] is_365_ = anIntArrayArray4333[i_364_];
						for (int i_366_ = 0; i_366_ < is_365_.length; i_366_++) {
							Class128 class128 = aClass128Array4316[is_365_[i_366_]];
							class128.anInt1309 += i_298_;
							class128.anInt1316 += i_299_;
						}
					}
				}
			}
		} else if (i == 10) {
			if (anIntArrayArray4333 != null) {
				for (int i_367_ = 0; i_367_ < i_302_; i_367_++) {
					int i_368_ = is[i_367_];
					if (i_368_ < anIntArrayArray4333.length) {
						int[] is_369_ = anIntArrayArray4333[i_368_];
						for (int i_370_ = 0; i_370_ < is_369_.length; i_370_++) {
							Class128 class128 = aClass128Array4316[is_369_[i_370_]];
							class128.anInt1314 = class128.anInt1314 * i_298_ >> 7;
							class128.anInt1312 = class128.anInt1312 * i_299_ >> 7;
						}
					}
				}
			}
		} else if (i == 9) {
			if (anIntArrayArray4333 != null) {
				for (int i_371_ = 0; i_371_ < i_302_; i_371_++) {
					int i_372_ = is[i_371_];
					if (i_372_ < anIntArrayArray4333.length) {
						int[] is_373_ = anIntArrayArray4333[i_372_];
						for (int i_374_ = 0; i_374_ < is_373_.length; i_374_++) {
							Class128 class128 = aClass128Array4316[is_373_[i_374_]];
							class128.anInt1307 = class128.anInt1307 + i_298_ & 0x3fff;
						}
					}
				}
			}
		}
	}

	public void LA(int i) {
		if ((anInt4326 & 0x2000) != 8192)
			throw new IllegalStateException();
		anInt4371 = i;
		anInt4357 = 0;
	}

	public Model method1728(byte i, int i_375_, boolean bool) {
		method1772(Thread.currentThread());
		boolean bool_376_ = false;
		Class178_Sub1 class178_sub1_377_;
		Class178_Sub1 class178_sub1_378_;
		if (i > 0 && i <= 7) {
			class178_sub1_378_ = aClass178_Sub1Array4320[i - 1];
			class178_sub1_377_ = aClass178_Sub1Array4347[i - 1];
			bool_376_ = true;
		} else
			class178_sub1_377_ = class178_sub1_378_ = new Class178_Sub1(aHa_Sub2_4366);
		return method1760(class178_sub1_377_, class178_sub1_378_, i_375_, bool_376_, bool);
	}

	public void VA(int i) {
		if ((anInt4326 & 0x3) != 3)
			throw new IllegalStateException();
		int i_379_ = Class296_Sub4.anIntArray4598[i];
		int i_380_ = Class296_Sub4.anIntArray4618[i];
		synchronized (this) {
			for (int i_381_ = 0; i_381_ < anInt4373; i_381_++) {
				int i_382_ = ((anIntArray4342[i_381_] * i_379_ + anIntArray4311[i_381_] * i_380_) >> 14);
				anIntArray4342[i_381_] = (anIntArray4342[i_381_] * i_380_ - anIntArray4311[i_381_] * i_379_) >> 14;
				anIntArray4311[i_381_] = i_382_;
			}
			method1777();
		}
	}

	private void method1756(int i) {
		if (!aClass240_4388.aBoolean2269) {
			short i_383_ = aShortArray4387[i];
			short i_384_ = aShortArray4324[i];
			short i_385_ = aShortArray4327[i];
			int i_386_ = anIntArray4360[i_383_] - aClass240_4388.anInt2267;
			if (i_386_ > 255)
				i_386_ = 255;
			else if (i_386_ < 0)
				i_386_ = 0;
			int i_387_ = anIntArray4360[i_384_] - aClass240_4388.anInt2267;
			if (i_387_ > 255)
				i_387_ = 255;
			else if (i_387_ < 0)
				i_387_ = 0;
			int i_388_ = anIntArray4360[i_385_] - aClass240_4388.anInt2267;
			if (i_388_ > 255)
				i_388_ = 255;
			else if (i_388_ < 0)
				i_388_ = 0;
			int i_389_ = i_386_ + i_387_ + i_388_;
			if (i_389_ != 765) {
				if (i_389_ == 0)
					method1765(i);
				else {
					if (aByteArray4328 == null)
						aClass11_4383.anInt108 = 0;
					else
						aClass11_4383.anInt108 = aByteArray4328[i] & 0xff;
					if (aShortArray4350 == null || aShortArray4350[i] == -1) {
						if (anIntArray4331[i] == -1)
							aClass11_4383.method198((float) anIntArray4337[i_383_], (float) anIntArray4337[i_384_],
									(float) anIntArray4337[i_385_], (float) anIntArray4323[i_383_],
									(float) anIntArray4323[i_384_], (float) anIntArray4323[i_385_],
									(float) anIntArray4360[i_383_], (float) anIntArray4360[i_384_],
									(float) anIntArray4360[i_385_],
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]),
											16711680, i_386_ << 24 | aClass240_4388.anInt2265)),
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]),
											16711680, i_387_ << 24 | aClass240_4388.anInt2265)),
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]),
											16711680, i_388_ << 24 | aClass240_4388.anInt2265)));
						else
							aClass11_4383.method198((float) anIntArray4337[i_383_], (float) anIntArray4337[i_384_],
									(float) anIntArray4337[i_385_], (float) anIntArray4323[i_383_],
									(float) anIntArray4323[i_384_], (float) anIntArray4323[i_385_],
									(float) anIntArray4360[i_383_], (float) anIntArray4360[i_384_],
									(float) anIntArray4360[i_385_],
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]),
											16711680, i_386_ << 24 | aClass240_4388.anInt2265)),
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4338[i] & 0xffff]),
											16711680, i_387_ << 24 | aClass240_4388.anInt2265)),
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4331[i] & 0xffff]),
											16711680, i_388_ << 24 | aClass240_4388.anInt2265)));
					} else {
						int i_390_ = -16777216;
						if (aByteArray4328 != null)
							i_390_ = 255 - (aByteArray4328[i] & 0xff) << 24;
						if (anIntArray4331[i] == -1) {
							int i_391_ = i_390_ | anIntArray4308[i] & 0xffffff;
							aClass11_4383.method209((float) anIntArray4337[i_383_], (float) anIntArray4337[i_384_],
									(float) anIntArray4337[i_385_], (float) anIntArray4323[i_383_],
									(float) anIntArray4323[i_384_], (float) anIntArray4323[i_385_],
									(float) anIntArray4360[i_383_], (float) anIntArray4360[i_384_],
									(float) anIntArray4360[i_385_], aFloatArrayArray4384[i][0],
									aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0],
									aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2], i_391_, i_391_, i_391_,
									aClass240_4388.anInt2265, i_386_, i_387_, i_388_, aShortArray4350[i]);
						} else
							aClass11_4383.method209((float) anIntArray4337[i_383_], (float) anIntArray4337[i_384_],
									(float) anIntArray4337[i_385_], (float) anIntArray4323[i_383_],
									(float) anIntArray4323[i_384_], (float) anIntArray4323[i_385_],
									(float) anIntArray4360[i_383_], (float) anIntArray4360[i_384_],
									(float) anIntArray4360[i_385_], aFloatArrayArray4384[i][0],
									aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0],
									aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2],
									i_390_ | anIntArray4308[i] & 0xffffff, i_390_ | anIntArray4338[i] & 0xffffff,
									i_390_ | anIntArray4331[i] & 0xffffff, aClass240_4388.anInt2265, i_386_, i_387_,
									i_388_, aShortArray4350[i]);
					}
				}
			}
		} else {
			short i_392_ = aShortArray4387[i];
			short i_393_ = aShortArray4324[i];
			short i_394_ = aShortArray4327[i];
			int i_395_ = 0;
			int i_396_ = 0;
			int i_397_ = 0;
			if (anIntArray4336[i_392_] > aClass240_4388.anInt2260)
				i_395_ = 255;
			else if (anIntArray4336[i_392_] > aClass240_4388.anInt2266)
				i_395_ = ((aClass240_4388.anInt2266 - anIntArray4336[i_392_]) * 255
						/ (aClass240_4388.anInt2266 - aClass240_4388.anInt2260));
			if (anIntArray4336[i_393_] > aClass240_4388.anInt2260)
				i_396_ = 255;
			else if (anIntArray4336[i_393_] > aClass240_4388.anInt2266)
				i_396_ = ((aClass240_4388.anInt2266 - anIntArray4336[i_393_]) * 255
						/ (aClass240_4388.anInt2266 - aClass240_4388.anInt2260));
			if (anIntArray4336[i_394_] > aClass240_4388.anInt2260)
				i_397_ = 255;
			else if (anIntArray4336[i_394_] > aClass240_4388.anInt2266)
				i_397_ = ((aClass240_4388.anInt2266 - anIntArray4336[i_394_]) * 255
						/ (aClass240_4388.anInt2266 - aClass240_4388.anInt2260));
			if (aByteArray4328 == null)
				aClass11_4383.anInt108 = 0;
			else
				aClass11_4383.anInt108 = aByteArray4328[i] & 0xff;
			if (aShortArray4350 == null || aShortArray4350[i] == -1) {
				if (anIntArray4331[i] == -1)
					aClass11_4383.method198((float) anIntArray4337[i_392_], (float) anIntArray4337[i_393_],
							(float) anIntArray4337[i_394_], (float) anIntArray4323[i_392_],
							(float) anIntArray4323[i_393_], (float) anIntArray4323[i_394_],
							(float) anIntArray4360[i_392_], (float) anIntArray4360[i_393_],
							(float) anIntArray4360[i_394_],
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]), 16711680,
									(i_395_ << 24 | aClass240_4388.anInt2265)),
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]), 16711680,
									(i_396_ << 24 | aClass240_4388.anInt2265)),
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]), 16711680,
									(i_397_ << 24 | aClass240_4388.anInt2265)));
				else
					aClass11_4383.method198((float) anIntArray4337[i_392_], (float) anIntArray4337[i_393_],
							(float) anIntArray4337[i_394_], (float) anIntArray4323[i_392_],
							(float) anIntArray4323[i_393_], (float) anIntArray4323[i_394_],
							(float) anIntArray4360[i_392_], (float) anIntArray4360[i_393_],
							(float) anIntArray4360[i_394_],
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]), 16711680,
									(i_395_ << 24 | aClass240_4388.anInt2265)),
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4338[i] & 0xffff]), 16711680,
									(i_396_ << 24 | aClass240_4388.anInt2265)),
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4331[i] & 0xffff]), 16711680,
									(i_397_ << 24 | aClass240_4388.anInt2265)));
			} else {
				int i_398_ = -16777216;
				if (aByteArray4328 != null)
					i_398_ = 255 - (aByteArray4328[i] & 0xff) << 24;
				if (anIntArray4331[i] == -1) {
					int i_399_ = i_398_ | anIntArray4308[i] & 0xffffff;
					aClass11_4383.method209((float) anIntArray4337[i_392_], (float) anIntArray4337[i_393_],
							(float) anIntArray4337[i_394_], (float) anIntArray4323[i_392_],
							(float) anIntArray4323[i_393_], (float) anIntArray4323[i_394_],
							(float) anIntArray4360[i_392_], (float) anIntArray4360[i_393_],
							(float) anIntArray4360[i_394_], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
							aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
							aFloatArrayArray4376[i][2], i_399_, i_399_, i_399_, aClass240_4388.anInt2265, i_395_,
							i_396_, i_397_, aShortArray4350[i]);
				} else
					aClass11_4383.method209((float) anIntArray4337[i_392_], (float) anIntArray4337[i_393_],
							(float) anIntArray4337[i_394_], (float) anIntArray4323[i_392_],
							(float) anIntArray4323[i_393_], (float) anIntArray4323[i_394_],
							(float) anIntArray4360[i_392_], (float) anIntArray4360[i_393_],
							(float) anIntArray4360[i_394_], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
							aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
							aFloatArrayArray4376[i][2], i_398_ | anIntArray4308[i] & 0xffffff,
							i_398_ | anIntArray4338[i] & 0xffffff, i_398_ | anIntArray4331[i] & 0xffffff,
							aClass240_4388.anInt2265, i_395_, i_396_, i_397_, aShortArray4350[i]);
			}
		}
	}

	private void method1757() {
		if (anInt4357 == 0 && aClass339Array4325 == null) {
			if (aHa_Sub2_4366.anInt4105 > 1) {
				synchronized (this) {
					method1769();
				}
			} else
				method1769();
		}
	}

	private void method1758() {
		if (anInt4357 == 0)
			method1761(false);
		else if (aHa_Sub2_4366.anInt4105 > 1) {
			synchronized (this) {
				method1749();
			}
		} else
			method1749();
	}

	private void method1759() {
		if (!aBoolean4362) {
			int i = 0;
			int i_400_ = 0;
			int i_401_ = 32767;
			int i_402_ = 32767;
			int i_403_ = 32767;
			int i_404_ = -32768;
			int i_405_ = -32768;
			int i_406_ = -32768;
			for (int i_407_ = 0; i_407_ < anInt4356; i_407_++) {
				int i_408_ = anIntArray4311[i_407_];
				int i_409_ = anIntArray4342[i_407_];
				int i_410_ = anIntArray4375[i_407_];
				if (i_408_ < i_401_)
					i_401_ = i_408_;
				if (i_408_ > i_404_)
					i_404_ = i_408_;
				if (i_409_ < i_402_)
					i_402_ = i_409_;
				if (i_409_ > i_405_)
					i_405_ = i_409_;
				if (i_410_ < i_403_)
					i_403_ = i_410_;
				if (i_410_ > i_406_)
					i_406_ = i_410_;
				int i_411_ = i_408_ * i_408_ + i_410_ * i_410_;
				if (i_411_ > i)
					i = i_411_;
				i_411_ += i_409_ * i_409_;
				if (i_411_ > i_400_)
					i_400_ = i_411_;
			}
			aShort4321 = (short) i_401_;
			aShort4370 = (short) i_404_;
			aShort4381 = (short) i_402_;
			aShort4377 = (short) i_405_;
			aShort4389 = (short) i_403_;
			aShort4346 = (short) i_406_;
			aShort4344 = (short) (int) (Math.sqrt((double) i) + 0.99);
			aShort4369 = (short) (int) (Math.sqrt((double) i_400_) + 0.99);
			aBoolean4362 = true;
		}
	}

	private Model method1760(Class178_Sub1 class178_sub1_412_, Class178_Sub1 class178_sub1_413_, int i, boolean bool,
			boolean bool_414_) {
		class178_sub1_412_.aBoolean4362 = aBoolean4362;
		if (aBoolean4362) {
			class178_sub1_412_.aShort4370 = aShort4370;
			class178_sub1_412_.aShort4377 = aShort4377;
			class178_sub1_412_.aShort4346 = aShort4346;
			class178_sub1_412_.aShort4321 = aShort4321;
			class178_sub1_412_.aShort4381 = aShort4381;
			class178_sub1_412_.aShort4389 = aShort4389;
			class178_sub1_412_.aShort4344 = aShort4344;
			class178_sub1_412_.aShort4369 = aShort4369;
		}
		class178_sub1_412_.anInt4353 = anInt4353;
		class178_sub1_412_.anInt4371 = anInt4371;
		class178_sub1_412_.anInt4373 = anInt4373;
		class178_sub1_412_.anInt4356 = anInt4356;
		class178_sub1_412_.anInt4361 = anInt4361;
		class178_sub1_412_.anInt4359 = anInt4359;
		if ((i & 0x100) != 0)
			class178_sub1_412_.aBoolean4354 = true;
		else
			class178_sub1_412_.aBoolean4354 = aBoolean4354;
		class178_sub1_412_.aBoolean4340 = aBoolean4340;
		boolean bool_415_ = (i & 0x7) == 7 | (i & 0x20) != 0;
		boolean bool_416_ = bool_415_ || (i & 0x1) != 0;
		boolean bool_417_ = bool_415_ || (i & 0x2) != 0;
		boolean bool_418_ = bool_415_ || (i & 0x4) != 0 || (i & 0x10) != 0;
		if (bool_416_ || bool_417_ || bool_418_) {
			if (bool_416_) {
				if (class178_sub1_413_.anIntArray4311 == null || class178_sub1_413_.anIntArray4311.length < anInt4373)
					class178_sub1_412_.anIntArray4311 = class178_sub1_413_.anIntArray4311 = new int[anInt4373];
				else
					class178_sub1_412_.anIntArray4311 = class178_sub1_413_.anIntArray4311;
				for (int i_419_ = 0; i_419_ < anInt4373; i_419_++)
					class178_sub1_412_.anIntArray4311[i_419_] = anIntArray4311[i_419_];
			} else
				class178_sub1_412_.anIntArray4311 = anIntArray4311;
			if (bool_417_) {
				if (class178_sub1_413_.anIntArray4342 == null || class178_sub1_413_.anIntArray4342.length < anInt4373)
					class178_sub1_412_.anIntArray4342 = class178_sub1_413_.anIntArray4342 = new int[anInt4373];
				else
					class178_sub1_412_.anIntArray4342 = class178_sub1_413_.anIntArray4342;
				for (int i_420_ = 0; i_420_ < anInt4373; i_420_++)
					class178_sub1_412_.anIntArray4342[i_420_] = anIntArray4342[i_420_];
			} else
				class178_sub1_412_.anIntArray4342 = anIntArray4342;
			if (bool_418_) {
				if (class178_sub1_413_.anIntArray4375 == null || class178_sub1_413_.anIntArray4375.length < anInt4373)
					class178_sub1_412_.anIntArray4375 = class178_sub1_413_.anIntArray4375 = new int[anInt4373];
				else
					class178_sub1_412_.anIntArray4375 = class178_sub1_413_.anIntArray4375;
				for (int i_421_ = 0; i_421_ < anInt4373; i_421_++)
					class178_sub1_412_.anIntArray4375[i_421_] = anIntArray4375[i_421_];
			} else
				class178_sub1_412_.anIntArray4375 = anIntArray4375;
		} else {
			class178_sub1_412_.anIntArray4311 = anIntArray4311;
			class178_sub1_412_.anIntArray4342 = anIntArray4342;
			class178_sub1_412_.anIntArray4375 = anIntArray4375;
		}
		if ((i & 0x84080) != 0) {
			if (class178_sub1_413_.aShortArray4341 == null || class178_sub1_413_.aShortArray4341.length < anInt4361) {
				int i_422_ = anInt4361;
				class178_sub1_412_.aShortArray4341 = class178_sub1_413_.aShortArray4341 = new short[i_422_];
			} else
				class178_sub1_412_.aShortArray4341 = class178_sub1_413_.aShortArray4341;
			for (int i_423_ = 0; i_423_ < anInt4361; i_423_++)
				class178_sub1_412_.aShortArray4341[i_423_] = aShortArray4341[i_423_];
		} else
			class178_sub1_412_.aShortArray4341 = aShortArray4341;
		if ((i & 0x97018) != 0) {
			class178_sub1_412_.anInt4357 = 0;
			class178_sub1_412_.anIntArray4308 = class178_sub1_412_.anIntArray4338 = class178_sub1_412_.anIntArray4331 = null;
		} else if ((i & 0x80) != 0) {
			if (bool_414_)
				method1761(false);
			if (anIntArray4308 != null) {
				if (class178_sub1_413_.anIntArray4308 == null || class178_sub1_413_.anIntArray4308.length < anInt4361) {
					int i_424_ = anInt4361;
					class178_sub1_412_.anIntArray4308 = class178_sub1_413_.anIntArray4308 = new int[i_424_];
					class178_sub1_412_.anIntArray4338 = class178_sub1_413_.anIntArray4338 = new int[i_424_];
					class178_sub1_412_.anIntArray4331 = class178_sub1_413_.anIntArray4331 = new int[i_424_];
				} else {
					class178_sub1_412_.anIntArray4308 = class178_sub1_413_.anIntArray4308;
					class178_sub1_412_.anIntArray4338 = class178_sub1_413_.anIntArray4338;
					class178_sub1_412_.anIntArray4331 = class178_sub1_413_.anIntArray4331;
				}
				for (int i_425_ = 0; i_425_ < anInt4361; i_425_++) {
					class178_sub1_412_.anIntArray4308[i_425_] = anIntArray4308[i_425_];
					class178_sub1_412_.anIntArray4338[i_425_] = anIntArray4338[i_425_];
					class178_sub1_412_.anIntArray4331[i_425_] = anIntArray4331[i_425_];
				}
			}
			class178_sub1_412_.anInt4357 = anInt4357;
		} else {
			if (bool_414_)
				method1761(false);
			class178_sub1_412_.anIntArray4308 = anIntArray4308;
			class178_sub1_412_.anIntArray4338 = anIntArray4338;
			class178_sub1_412_.anIntArray4331 = anIntArray4331;
			class178_sub1_412_.anInt4357 = anInt4357;
		}
		if ((i & 0x100) != 0) {
			if (class178_sub1_413_.aByteArray4328 == null || class178_sub1_413_.aByteArray4328.length < anInt4361) {
				int i_426_ = anInt4361;
				class178_sub1_412_.aByteArray4328 = class178_sub1_413_.aByteArray4328 = new byte[i_426_];
			} else
				class178_sub1_412_.aByteArray4328 = class178_sub1_413_.aByteArray4328;
			if (aByteArray4328 != null) {
				for (int i_427_ = 0; i_427_ < anInt4361; i_427_++)
					class178_sub1_412_.aByteArray4328[i_427_] = aByteArray4328[i_427_];
			} else {
				for (int i_428_ = 0; i_428_ < anInt4361; i_428_++)
					class178_sub1_412_.aByteArray4328[i_428_] = (byte) 0;
			}
		} else
			class178_sub1_412_.aByteArray4328 = aByteArray4328;
		if ((i & 0x8) != 0 || (i & 0x10) != 0) {
			if (class178_sub1_413_.aClass339Array4325 == null
					|| class178_sub1_413_.aClass339Array4325.length < anInt4356) {
				int i_429_ = anInt4356;
				class178_sub1_412_.aClass339Array4325 = class178_sub1_413_.aClass339Array4325 = new Class339[i_429_];
			} else
				class178_sub1_412_.aClass339Array4325 = class178_sub1_413_.aClass339Array4325;
			if (aClass339Array4325 != null) {
				for (int i_430_ = 0; i_430_ < anInt4356; i_430_++)
					class178_sub1_412_.aClass339Array4325[i_430_] = new Class339(aClass339Array4325[i_430_]);
			} else
				class178_sub1_412_.aClass339Array4325 = null;
			if (aClass364Array4339 != null) {
				if (class178_sub1_413_.aClass364Array4339 == null
						|| (class178_sub1_413_.aClass364Array4339.length < anInt4361)) {
					int i_431_ = anInt4361;
					class178_sub1_412_.aClass364Array4339 = class178_sub1_413_.aClass364Array4339 = new Class364[i_431_];
				} else
					class178_sub1_412_.aClass364Array4339 = class178_sub1_413_.aClass364Array4339;
				for (int i_432_ = 0; i_432_ < anInt4361; i_432_++)
					class178_sub1_412_.aClass364Array4339[i_432_] = (aClass364Array4339[i_432_] != null
							? new Class364(aClass364Array4339[i_432_])
							: null);
			} else
				class178_sub1_412_.aClass364Array4339 = null;
		} else {
			if (bool_414_)
				method1757();
			class178_sub1_412_.aClass339Array4325 = aClass339Array4325;
			class178_sub1_412_.aClass364Array4339 = aClass364Array4339;
		}
		if ((i & 0x8000) != 0) {
			if (aShortArray4350 == null)
				class178_sub1_412_.aShortArray4350 = null;
			else {
				if (class178_sub1_413_.aShortArray4350 == null
						|| class178_sub1_413_.aShortArray4350.length < anInt4361) {
					int i_433_ = anInt4361;
					class178_sub1_412_.aShortArray4350 = class178_sub1_413_.aShortArray4350 = new short[i_433_];
				} else
					class178_sub1_412_.aShortArray4350 = class178_sub1_413_.aShortArray4350;
				for (int i_434_ = 0; i_434_ < anInt4361; i_434_++)
					class178_sub1_412_.aShortArray4350[i_434_] = aShortArray4350[i_434_];
			}
		} else
			class178_sub1_412_.aShortArray4350 = aShortArray4350;
		if ((i & 0x10000) != 0) {
			if (aByteArray4386 == null)
				class178_sub1_412_.aByteArray4386 = null;
			else {
				if (class178_sub1_413_.aByteArray4386 == null || class178_sub1_413_.aByteArray4386.length < anInt4361) {
					int i_435_ = bool ? anInt4361 + 100 : anInt4361;
					class178_sub1_412_.aByteArray4386 = class178_sub1_413_.aByteArray4386 = new byte[i_435_];
				} else
					class178_sub1_412_.aByteArray4386 = class178_sub1_413_.aByteArray4386;
				for (int i_436_ = 0; i_436_ < anInt4361; i_436_++)
					class178_sub1_412_.aByteArray4386[i_436_] = aByteArray4386[i_436_];
			}
		} else
			class178_sub1_412_.aByteArray4386 = aByteArray4386;
		if ((i & 0xc580) != 0) {
			if (class178_sub1_413_.aClass128Array4316 == null
					|| class178_sub1_413_.aClass128Array4316.length < anInt4359) {
				int i_437_ = anInt4359;
				class178_sub1_412_.aClass128Array4316 = class178_sub1_413_.aClass128Array4316 = new Class128[i_437_];
				for (int i_438_ = 0; i_438_ < anInt4359; i_438_++)
					class178_sub1_412_.aClass128Array4316[i_438_] = aClass128Array4316[i_438_].method1362(-23485);
			} else {
				class178_sub1_412_.aClass128Array4316 = class178_sub1_413_.aClass128Array4316;
				for (int i_439_ = 0; i_439_ < anInt4359; i_439_++)
					class178_sub1_412_.aClass128Array4316[i_439_].method1361(aClass128Array4316[i_439_], (byte) -108);
			}
		} else
			class178_sub1_412_.aClass128Array4316 = aClass128Array4316;
		if (aFloatArrayArray4384 != null && (i & 0x10) != 0) {
			if (class178_sub1_413_.aFloatArrayArray4384 == null
					|| (class178_sub1_413_.aFloatArrayArray4384.length < anInt4361)) {
				int i_440_ = bool ? anInt4361 + 100 : anInt4361;
				class178_sub1_412_.aFloatArrayArray4384 = class178_sub1_413_.aFloatArrayArray4384 = new float[i_440_][3];
			} else
				class178_sub1_412_.aFloatArrayArray4384 = class178_sub1_413_.aFloatArrayArray4384;
			for (int i_441_ = 0; i_441_ < anInt4361; i_441_++) {
				if (aFloatArrayArray4384[i_441_] != null) {
					class178_sub1_412_.aFloatArrayArray4384[i_441_][0] = aFloatArrayArray4384[i_441_][0];
					class178_sub1_412_.aFloatArrayArray4384[i_441_][1] = aFloatArrayArray4384[i_441_][1];
					class178_sub1_412_.aFloatArrayArray4384[i_441_][2] = aFloatArrayArray4384[i_441_][2];
				}
			}
			if (class178_sub1_413_.aFloatArrayArray4376 == null
					|| (class178_sub1_413_.aFloatArrayArray4376.length < anInt4361)) {
				int i_442_ = bool ? anInt4361 + 100 : anInt4361;
				class178_sub1_412_.aFloatArrayArray4376 = class178_sub1_413_.aFloatArrayArray4376 = new float[i_442_][3];
			} else
				class178_sub1_412_.aFloatArrayArray4376 = class178_sub1_413_.aFloatArrayArray4376;
			for (int i_443_ = 0; i_443_ < anInt4361; i_443_++) {
				if (aFloatArrayArray4376[i_443_] != null) {
					class178_sub1_412_.aFloatArrayArray4376[i_443_][0] = aFloatArrayArray4376[i_443_][0];
					class178_sub1_412_.aFloatArrayArray4376[i_443_][1] = aFloatArrayArray4376[i_443_][1];
					class178_sub1_412_.aFloatArrayArray4376[i_443_][2] = aFloatArrayArray4376[i_443_][2];
				}
			}
		} else {
			class178_sub1_412_.aFloatArrayArray4384 = aFloatArrayArray4384;
			class178_sub1_412_.aFloatArrayArray4376 = aFloatArrayArray4376;
		}
		class178_sub1_412_.anIntArrayArray4367 = anIntArrayArray4367;
		class178_sub1_412_.anIntArrayArray4368 = anIntArrayArray4368;
		class178_sub1_412_.anIntArrayArray4333 = anIntArrayArray4333;
		class178_sub1_412_.aShortArray4345 = aShortArray4345;
		class178_sub1_412_.aShortArray4363 = aShortArray4363;
		class178_sub1_412_.aByteArray4334 = aByteArray4334;
		class178_sub1_412_.aShortArray4387 = aShortArray4387;
		class178_sub1_412_.aShortArray4324 = aShortArray4324;
		class178_sub1_412_.aShortArray4327 = aShortArray4327;
		class178_sub1_412_.aClass89Array4374 = aClass89Array4374;
		class178_sub1_412_.aClass232Array4314 = aClass232Array4314;
		class178_sub1_412_.aClass316Array4313 = aClass316Array4313;
		class178_sub1_412_.aShortArray4385 = aShortArray4385;
		class178_sub1_412_.anInt4326 = i;
		return class178_sub1_412_;
	}

	private void method1761(boolean bool) {
		if (aHa_Sub2_4366.anInt4105 > 1) {
			synchronized (this) {
				method1753(bool);
			}
		} else
			method1753(bool);
	}

	public boolean NA() {
		if (anIntArrayArray4367 == null)
			return false;
		anInt4318 = 0;
		anInt4349 = 0;
		anInt4332 = 0;
		return true;
	}

	public void C(int i) {
		if ((anInt4326 & 0x1000) != 4096)
			throw new IllegalStateException();
		anInt4353 = i;
		anInt4357 = 0;
	}

	public void method1717(int i, int i_444_, int i_445_, int i_446_) {
		if ((anInt4326 & 0x80000) != 524288)
			throw new IllegalStateException("FMT");
		for (int i_447_ = 0; i_447_ < anInt4361; i_447_++) {
			int i_448_ = aShortArray4341[i_447_] & 0xffff;
			int i_449_ = i_448_ >> 10 & 0x3f;
			int i_450_ = i_448_ >> 7 & 0x7;
			int i_451_ = i_448_ & 0x7f;
			if (i != -1)
				i_449_ += (i - i_449_) * i_446_ >> 7;
			if (i_444_ != -1)
				i_450_ += (i_444_ - i_450_) * i_446_ >> 7;
			if (i_445_ != -1)
				i_451_ += (i_445_ - i_451_) * i_446_ >> 7;
			aShortArray4341[i_447_] = (short) (i_449_ << 10 | i_450_ << 7 | i_451_);
		}
		if (aClass316Array4313 != null) {
			for (int i_452_ = 0; i_452_ < anInt4359; i_452_++) {
				Class316 class316 = aClass316Array4313[i_452_];
				Class128 class128 = aClass128Array4316[i_452_];
				class128.anInt1311 = (class128.anInt1311 & ~0xffffff
						| (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1
								.method3576(aShortArray4341[class316.anInt2804] & 0xffff, (byte) -12)) & 0xffff])
								& 0xffffff);
			}
		}
		if (anInt4357 == 2)
			anInt4357 = 1;
	}

	private void method1762() {
		synchronized (this) {
			for (int i = 0; i < anInt4373; i++) {
				anIntArray4311[i] = -anIntArray4311[i];
				anIntArray4375[i] = -anIntArray4375[i];
			}
			method1777();
		}
	}

	private void method1763(boolean bool, boolean bool_453_, int i, int i_454_) {
		if (aClass316Array4313 != null) {
			for (int i_455_ = 0; i_455_ < anInt4359; i_455_++) {
				Class316 class316 = aClass316Array4313[i_455_];
				anIntArray4329[class316.anInt2804] = i_455_;
			}
		}
		if (aBoolean4354 || aClass316Array4313 != null) {
			if ((anInt4326 & 0x100) == 0 && aShortArray4385 != null) {
				for (int i_456_ = 0; i_456_ < anInt4361; i_456_++) {
					short i_457_ = aShortArray4385[i_456_];
					method1776(i_457_, bool, bool_453_);
				}
			} else {
				for (int i_458_ = 0; i_458_ < anInt4361; i_458_++) {
					if (!method1767(i_458_) && !method1764(i_458_))
						method1776(i_458_, bool, bool_453_);
				}
				if (aByteArray4334 == null) {
					for (int i_459_ = 0; i_459_ < anInt4361; i_459_++) {
						if (method1767(i_459_) || method1764(i_459_))
							method1776(i_459_, bool, bool_453_);
					}
				} else {
					for (int i_460_ = 0; i_460_ < 12; i_460_++) {
						for (int i_461_ = 0; i_461_ < anInt4361; i_461_++) {
							if (aByteArray4334[i_461_] == i_460_ && (method1767(i_461_) || method1764(i_461_)))
								method1776(i_461_, bool, bool_453_);
						}
					}
				}
			}
		} else {
			for (int i_462_ = 0; i_462_ < anInt4361; i_462_++)
				method1776(i_462_, bool, bool_453_);
		}
	}

	private boolean method1764(int i) {
		if (anIntArray4329 == null)
			return false;
		if (anIntArray4329[i] == -1)
			return false;
		return true;
	}

	private void method1765(int i) {
		short i_463_ = aShortArray4387[i];
		short i_464_ = aShortArray4324[i];
		short i_465_ = aShortArray4327[i];
		if (aShortArray4350 == null || aShortArray4350[i] == -1) {
			if (aByteArray4328 == null)
				aClass11_4383.anInt108 = 0;
			else
				aClass11_4383.anInt108 = aByteArray4328[i] & 0xff;
			if (anIntArray4331[i] == -1)
				aClass11_4383.method212((float) anIntArray4337[i_463_], (float) anIntArray4337[i_464_],
						(float) anIntArray4337[i_465_], (float) anIntArray4323[i_463_], (float) anIntArray4323[i_464_],
						(float) anIntArray4323[i_465_], (float) anIntArray4360[i_463_], (float) anIntArray4360[i_464_],
						(float) anIntArray4360[i_465_], (Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]));
			else
				aClass11_4383.method216((float) anIntArray4337[i_463_], (float) anIntArray4337[i_464_],
						(float) anIntArray4337[i_465_], (float) anIntArray4323[i_463_], (float) anIntArray4323[i_464_],
						(float) anIntArray4323[i_465_], (float) anIntArray4360[i_463_], (float) anIntArray4360[i_464_],
						(float) anIntArray4360[i_465_], (float) (anIntArray4308[i] & 0xffff),
						(float) (anIntArray4338[i] & 0xffff), (float) (anIntArray4331[i] & 0xffff));
		} else {
			int i_466_ = -16777216;
			if (aByteArray4328 != null)
				i_466_ = 255 - (aByteArray4328[i] & 0xff) << 24;
			if (anIntArray4331[i] == -1) {
				int i_467_ = i_466_ | anIntArray4308[i] & 0xffffff;
				aClass11_4383.method209((float) anIntArray4337[i_463_], (float) anIntArray4337[i_464_],
						(float) anIntArray4337[i_465_], (float) anIntArray4323[i_463_], (float) anIntArray4323[i_464_],
						(float) anIntArray4323[i_465_], (float) anIntArray4360[i_463_], (float) anIntArray4360[i_464_],
						(float) anIntArray4360[i_465_], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
						aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
						aFloatArrayArray4376[i][2], i_467_, i_467_, i_467_, aClass240_4388.anInt2265, 0, 0, 0,
						aShortArray4350[i]);
			} else
				aClass11_4383.method209((float) anIntArray4337[i_463_], (float) anIntArray4337[i_464_],
						(float) anIntArray4337[i_465_], (float) anIntArray4323[i_463_], (float) anIntArray4323[i_464_],
						(float) anIntArray4323[i_465_], (float) anIntArray4360[i_463_], (float) anIntArray4360[i_464_],
						(float) anIntArray4360[i_465_], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
						aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
						aFloatArrayArray4376[i][2], i_466_ | anIntArray4308[i] & 0xffffff,
						i_466_ | anIntArray4338[i] & 0xffffff, i_466_ | anIntArray4331[i] & 0xffffff,
						aClass240_4388.anInt2265, 0, 0, 0, aShortArray4350[i]);
		}
	}

	public void H(int i, int i_468_, int i_469_) {
		if (i != 0 && (anInt4326 & 0x1) != 1)
			throw new IllegalStateException();
		if (i_468_ != 0 && (anInt4326 & 0x2) != 2)
			throw new IllegalStateException();
		if (i_469_ != 0 && (anInt4326 & 0x4) != 4)
			throw new IllegalStateException();
		synchronized (this) {
			for (int i_470_ = 0; i_470_ < anInt4373; i_470_++) {
				anIntArray4311[i_470_] += i;
				anIntArray4342[i_470_] += i_468_;
				anIntArray4375[i_470_] += i_469_;
			}
		}
	}

	public void I(int i, int[] is, int i_471_, int i_472_, int i_473_, boolean bool, int i_474_, int[] is_475_) {
		int i_476_ = is.length;
		if (i == 0) {
			i_471_ <<= 4;
			i_472_ <<= 4;
			i_473_ <<= 4;
			if (!aBoolean4315) {
				for (int i_477_ = 0; i_477_ < anInt4373; i_477_++) {
					anIntArray4311[i_477_] <<= 4;
					anIntArray4342[i_477_] <<= 4;
					anIntArray4375[i_477_] <<= 4;
				}
				aBoolean4315 = true;
			}
			int i_478_ = 0;
			anInt4318 = 0;
			anInt4349 = 0;
			anInt4332 = 0;
			for (int i_479_ = 0; i_479_ < i_476_; i_479_++) {
				int i_480_ = is[i_479_];
				if (i_480_ < anIntArrayArray4367.length) {
					int[] is_481_ = anIntArrayArray4367[i_480_];
					for (int i_482_ = 0; i_482_ < is_481_.length; i_482_++) {
						int i_483_ = is_481_[i_482_];
						if (aShortArray4345 == null || (i_474_ & aShortArray4345[i_483_]) != 0) {
							anInt4318 += anIntArray4311[i_483_];
							anInt4349 += anIntArray4342[i_483_];
							anInt4332 += anIntArray4375[i_483_];
							i_478_++;
						}
					}
				}
			}
			if (i_478_ > 0) {
				anInt4318 = anInt4318 / i_478_ + i_471_;
				anInt4349 = anInt4349 / i_478_ + i_472_;
				anInt4332 = anInt4332 / i_478_ + i_473_;
				aBoolean4364 = true;
			} else {
				anInt4318 = i_471_;
				anInt4349 = i_472_;
				anInt4332 = i_473_;
			}
		} else if (i == 1) {
			if (is_475_ != null) {
				int i_484_ = ((is_475_[0] * i_471_ + is_475_[1] * i_472_ + is_475_[2] * i_473_ + 8192) >> 14);
				int i_485_ = ((is_475_[3] * i_471_ + is_475_[4] * i_472_ + is_475_[5] * i_473_ + 8192) >> 14);
				int i_486_ = ((is_475_[6] * i_471_ + is_475_[7] * i_472_ + is_475_[8] * i_473_ + 8192) >> 14);
				i_471_ = i_484_;
				i_472_ = i_485_;
				i_473_ = i_486_;
			}
			i_471_ <<= 4;
			i_472_ <<= 4;
			i_473_ <<= 4;
			if (!aBoolean4315) {
				for (int i_487_ = 0; i_487_ < anInt4373; i_487_++) {
					anIntArray4311[i_487_] <<= 4;
					anIntArray4342[i_487_] <<= 4;
					anIntArray4375[i_487_] <<= 4;
				}
				aBoolean4315 = true;
			}
			for (int i_488_ = 0; i_488_ < i_476_; i_488_++) {
				int i_489_ = is[i_488_];
				if (i_489_ < anIntArrayArray4367.length) {
					int[] is_490_ = anIntArrayArray4367[i_489_];
					for (int i_491_ = 0; i_491_ < is_490_.length; i_491_++) {
						int i_492_ = is_490_[i_491_];
						if (aShortArray4345 == null || (i_474_ & aShortArray4345[i_492_]) != 0) {
							anIntArray4311[i_492_] += i_471_;
							anIntArray4342[i_492_] += i_472_;
							anIntArray4375[i_492_] += i_473_;
						}
					}
				}
			}
		} else if (i == 2) {
			if (is_475_ != null) {
				if (!aBoolean4315) {
					for (int i_493_ = 0; i_493_ < anInt4373; i_493_++) {
						anIntArray4311[i_493_] <<= 4;
						anIntArray4342[i_493_] <<= 4;
						anIntArray4375[i_493_] <<= 4;
					}
					aBoolean4315 = true;
				}
				int i_494_ = is_475_[9] << 4;
				int i_495_ = is_475_[10] << 4;
				int i_496_ = is_475_[11] << 4;
				int i_497_ = is_475_[12] << 4;
				int i_498_ = is_475_[13] << 4;
				int i_499_ = is_475_[14] << 4;
				if (aBoolean4364) {
					int i_500_ = ((is_475_[0] * anInt4318 + is_475_[3] * anInt4349 + is_475_[6] * anInt4332
							+ 8192) >> 14);
					int i_501_ = ((is_475_[1] * anInt4318 + is_475_[4] * anInt4349 + is_475_[7] * anInt4332
							+ 8192) >> 14);
					int i_502_ = ((is_475_[2] * anInt4318 + is_475_[5] * anInt4349 + is_475_[8] * anInt4332
							+ 8192) >> 14);
					i_500_ += i_497_;
					i_501_ += i_498_;
					i_502_ += i_499_;
					anInt4318 = i_500_;
					anInt4349 = i_501_;
					anInt4332 = i_502_;
					aBoolean4364 = false;
				}
				int[] is_503_ = new int[9];
				int i_504_ = Class296_Sub4.anIntArray4618[i_471_];
				int i_505_ = Class296_Sub4.anIntArray4598[i_471_];
				int i_506_ = Class296_Sub4.anIntArray4618[i_472_];
				int i_507_ = Class296_Sub4.anIntArray4598[i_472_];
				int i_508_ = Class296_Sub4.anIntArray4618[i_473_];
				int i_509_ = Class296_Sub4.anIntArray4598[i_473_];
				int i_510_ = i_505_ * i_508_ + 8192 >> 14;
				int i_511_ = i_505_ * i_509_ + 8192 >> 14;
				is_503_[0] = i_506_ * i_508_ + i_507_ * i_511_ + 8192 >> 14;
				is_503_[1] = -i_506_ * i_509_ + i_507_ * i_510_ + 8192 >> 14;
				is_503_[2] = i_507_ * i_504_ + 8192 >> 14;
				is_503_[3] = i_504_ * i_509_ + 8192 >> 14;
				is_503_[4] = i_504_ * i_508_ + 8192 >> 14;
				is_503_[5] = -i_505_;
				is_503_[6] = -i_507_ * i_508_ + i_506_ * i_511_ + 8192 >> 14;
				is_503_[7] = i_507_ * i_509_ + i_506_ * i_510_ + 8192 >> 14;
				is_503_[8] = i_506_ * i_504_ + 8192 >> 14;
				int i_512_ = (is_503_[0] * -anInt4318 + is_503_[1] * -anInt4349 + is_503_[2] * -anInt4332 + 8192) >> 14;
				int i_513_ = (is_503_[3] * -anInt4318 + is_503_[4] * -anInt4349 + is_503_[5] * -anInt4332 + 8192) >> 14;
				int i_514_ = (is_503_[6] * -anInt4318 + is_503_[7] * -anInt4349 + is_503_[8] * -anInt4332 + 8192) >> 14;
				int i_515_ = i_512_ + anInt4318;
				int i_516_ = i_513_ + anInt4349;
				int i_517_ = i_514_ + anInt4332;
				int[] is_518_ = new int[9];
				for (int i_519_ = 0; i_519_ < 3; i_519_++) {
					for (int i_520_ = 0; i_520_ < 3; i_520_++) {
						int i_521_ = 0;
						for (int i_522_ = 0; i_522_ < 3; i_522_++)
							i_521_ += (is_503_[i_519_ * 3 + i_522_] * is_475_[i_520_ * 3 + i_522_]);
						is_518_[i_519_ * 3 + i_520_] = i_521_ + 8192 >> 14;
					}
				}
				int i_523_ = ((is_503_[0] * i_497_ + is_503_[1] * i_498_ + is_503_[2] * i_499_ + 8192) >> 14);
				int i_524_ = ((is_503_[3] * i_497_ + is_503_[4] * i_498_ + is_503_[5] * i_499_ + 8192) >> 14);
				int i_525_ = ((is_503_[6] * i_497_ + is_503_[7] * i_498_ + is_503_[8] * i_499_ + 8192) >> 14);
				i_523_ += i_515_;
				i_524_ += i_516_;
				i_525_ += i_517_;
				int[] is_526_ = new int[9];
				for (int i_527_ = 0; i_527_ < 3; i_527_++) {
					for (int i_528_ = 0; i_528_ < 3; i_528_++) {
						int i_529_ = 0;
						for (int i_530_ = 0; i_530_ < 3; i_530_++)
							i_529_ += (is_475_[i_527_ * 3 + i_530_] * is_518_[i_528_ + i_530_ * 3]);
						is_526_[i_527_ * 3 + i_528_] = i_529_ + 8192 >> 14;
					}
				}
				int i_531_ = ((is_475_[0] * i_523_ + is_475_[1] * i_524_ + is_475_[2] * i_525_ + 8192) >> 14);
				int i_532_ = ((is_475_[3] * i_523_ + is_475_[4] * i_524_ + is_475_[5] * i_525_ + 8192) >> 14);
				int i_533_ = ((is_475_[6] * i_523_ + is_475_[7] * i_524_ + is_475_[8] * i_525_ + 8192) >> 14);
				i_531_ += i_494_;
				i_532_ += i_495_;
				i_533_ += i_496_;
				for (int i_534_ = 0; i_534_ < i_476_; i_534_++) {
					int i_535_ = is[i_534_];
					if (i_535_ < anIntArrayArray4367.length) {
						int[] is_536_ = anIntArrayArray4367[i_535_];
						for (int i_537_ = 0; i_537_ < is_536_.length; i_537_++) {
							int i_538_ = is_536_[i_537_];
							if (aShortArray4345 == null || (i_474_ & aShortArray4345[i_538_]) != 0) {
								int i_539_ = ((is_526_[0] * anIntArray4311[i_538_] + is_526_[1] * anIntArray4342[i_538_]
										+ is_526_[2] * anIntArray4375[i_538_] + 8192) >> 14);
								int i_540_ = ((is_526_[3] * anIntArray4311[i_538_] + is_526_[4] * anIntArray4342[i_538_]
										+ is_526_[5] * anIntArray4375[i_538_] + 8192) >> 14);
								int i_541_ = ((is_526_[6] * anIntArray4311[i_538_] + is_526_[7] * anIntArray4342[i_538_]
										+ is_526_[8] * anIntArray4375[i_538_] + 8192) >> 14);
								i_539_ += i_531_;
								i_540_ += i_532_;
								i_541_ += i_533_;
								anIntArray4311[i_538_] = i_539_;
								anIntArray4342[i_538_] = i_540_;
								anIntArray4375[i_538_] = i_541_;
							}
						}
					}
				}
			} else {
				for (int i_542_ = 0; i_542_ < i_476_; i_542_++) {
					int i_543_ = is[i_542_];
					if (i_543_ < anIntArrayArray4367.length) {
						int[] is_544_ = anIntArrayArray4367[i_543_];
						for (int i_545_ = 0; i_545_ < is_544_.length; i_545_++) {
							int i_546_ = is_544_[i_545_];
							if (aShortArray4345 == null || (i_474_ & aShortArray4345[i_546_]) != 0) {
								anIntArray4311[i_546_] -= anInt4318;
								anIntArray4342[i_546_] -= anInt4349;
								anIntArray4375[i_546_] -= anInt4332;
								if (i_473_ != 0) {
									int i_547_ = Class296_Sub4.anIntArray4598[i_473_];
									int i_548_ = Class296_Sub4.anIntArray4618[i_473_];
									int i_549_ = ((anIntArray4342[i_546_] * i_547_ + anIntArray4311[i_546_] * i_548_
											+ 16383) >> 14);
									anIntArray4342[i_546_] = (anIntArray4342[i_546_] * i_548_
											- anIntArray4311[i_546_] * i_547_ + 16383) >> 14;
									anIntArray4311[i_546_] = i_549_;
								}
								if (i_471_ != 0) {
									int i_550_ = Class296_Sub4.anIntArray4598[i_471_];
									int i_551_ = Class296_Sub4.anIntArray4618[i_471_];
									int i_552_ = ((anIntArray4342[i_546_] * i_551_ - anIntArray4375[i_546_] * i_550_
											+ 16383) >> 14);
									anIntArray4375[i_546_] = (anIntArray4342[i_546_] * i_550_
											+ anIntArray4375[i_546_] * i_551_ + 16383) >> 14;
									anIntArray4342[i_546_] = i_552_;
								}
								if (i_472_ != 0) {
									int i_553_ = Class296_Sub4.anIntArray4598[i_472_];
									int i_554_ = Class296_Sub4.anIntArray4618[i_472_];
									int i_555_ = ((anIntArray4375[i_546_] * i_553_ + anIntArray4311[i_546_] * i_554_
											+ 16383) >> 14);
									anIntArray4375[i_546_] = (anIntArray4375[i_546_] * i_554_
											- anIntArray4311[i_546_] * i_553_ + 16383) >> 14;
									anIntArray4311[i_546_] = i_555_;
								}
								anIntArray4311[i_546_] += anInt4318;
								anIntArray4342[i_546_] += anInt4349;
								anIntArray4375[i_546_] += anInt4332;
							}
						}
					}
				}
			}
		} else if (i == 3) {
			if (is_475_ != null) {
				if (!aBoolean4315) {
					for (int i_556_ = 0; i_556_ < anInt4373; i_556_++) {
						anIntArray4311[i_556_] <<= 4;
						anIntArray4342[i_556_] <<= 4;
						anIntArray4375[i_556_] <<= 4;
					}
					aBoolean4315 = true;
				}
				int i_557_ = is_475_[9] << 4;
				int i_558_ = is_475_[10] << 4;
				int i_559_ = is_475_[11] << 4;
				int i_560_ = is_475_[12] << 4;
				int i_561_ = is_475_[13] << 4;
				int i_562_ = is_475_[14] << 4;
				if (aBoolean4364) {
					int i_563_ = ((is_475_[0] * anInt4318 + is_475_[3] * anInt4349 + is_475_[6] * anInt4332
							+ 8192) >> 14);
					int i_564_ = ((is_475_[1] * anInt4318 + is_475_[4] * anInt4349 + is_475_[7] * anInt4332
							+ 8192) >> 14);
					int i_565_ = ((is_475_[2] * anInt4318 + is_475_[5] * anInt4349 + is_475_[8] * anInt4332
							+ 8192) >> 14);
					i_563_ += i_560_;
					i_564_ += i_561_;
					i_565_ += i_562_;
					anInt4318 = i_563_;
					anInt4349 = i_564_;
					anInt4332 = i_565_;
					aBoolean4364 = false;
				}
				int i_566_ = i_471_ << 15 >> 7;
				int i_567_ = i_472_ << 15 >> 7;
				int i_568_ = i_473_ << 15 >> 7;
				int i_569_ = i_566_ * -anInt4318 + 8192 >> 14;
				int i_570_ = i_567_ * -anInt4349 + 8192 >> 14;
				int i_571_ = i_568_ * -anInt4332 + 8192 >> 14;
				int i_572_ = i_569_ + anInt4318;
				int i_573_ = i_570_ + anInt4349;
				int i_574_ = i_571_ + anInt4332;
				int[] is_575_ = new int[9];
				is_575_[0] = i_566_ * is_475_[0] + 8192 >> 14;
				is_575_[1] = i_566_ * is_475_[3] + 8192 >> 14;
				is_575_[2] = i_566_ * is_475_[6] + 8192 >> 14;
				is_575_[3] = i_567_ * is_475_[1] + 8192 >> 14;
				is_575_[4] = i_567_ * is_475_[4] + 8192 >> 14;
				is_575_[5] = i_567_ * is_475_[7] + 8192 >> 14;
				is_575_[6] = i_568_ * is_475_[2] + 8192 >> 14;
				is_575_[7] = i_568_ * is_475_[5] + 8192 >> 14;
				is_575_[8] = i_568_ * is_475_[8] + 8192 >> 14;
				int i_576_ = i_566_ * i_560_ + 8192 >> 14;
				int i_577_ = i_567_ * i_561_ + 8192 >> 14;
				int i_578_ = i_568_ * i_562_ + 8192 >> 14;
				i_576_ += i_572_;
				i_577_ += i_573_;
				i_578_ += i_574_;
				int[] is_579_ = new int[9];
				for (int i_580_ = 0; i_580_ < 3; i_580_++) {
					for (int i_581_ = 0; i_581_ < 3; i_581_++) {
						int i_582_ = 0;
						for (int i_583_ = 0; i_583_ < 3; i_583_++)
							i_582_ += (is_475_[i_580_ * 3 + i_583_] * is_575_[i_581_ + i_583_ * 3]);
						is_579_[i_580_ * 3 + i_581_] = i_582_ + 8192 >> 14;
					}
				}
				int i_584_ = ((is_475_[0] * i_576_ + is_475_[1] * i_577_ + is_475_[2] * i_578_ + 8192) >> 14);
				int i_585_ = ((is_475_[3] * i_576_ + is_475_[4] * i_577_ + is_475_[5] * i_578_ + 8192) >> 14);
				int i_586_ = ((is_475_[6] * i_576_ + is_475_[7] * i_577_ + is_475_[8] * i_578_ + 8192) >> 14);
				i_584_ += i_557_;
				i_585_ += i_558_;
				i_586_ += i_559_;
				for (int i_587_ = 0; i_587_ < i_476_; i_587_++) {
					int i_588_ = is[i_587_];
					if (i_588_ < anIntArrayArray4367.length) {
						int[] is_589_ = anIntArrayArray4367[i_588_];
						for (int i_590_ = 0; i_590_ < is_589_.length; i_590_++) {
							int i_591_ = is_589_[i_590_];
							if (aShortArray4345 == null || (i_474_ & aShortArray4345[i_591_]) != 0) {
								int i_592_ = ((is_579_[0] * anIntArray4311[i_591_] + is_579_[1] * anIntArray4342[i_591_]
										+ is_579_[2] * anIntArray4375[i_591_] + 8192) >> 14);
								int i_593_ = ((is_579_[3] * anIntArray4311[i_591_] + is_579_[4] * anIntArray4342[i_591_]
										+ is_579_[5] * anIntArray4375[i_591_] + 8192) >> 14);
								int i_594_ = ((is_579_[6] * anIntArray4311[i_591_] + is_579_[7] * anIntArray4342[i_591_]
										+ is_579_[8] * anIntArray4375[i_591_] + 8192) >> 14);
								i_592_ += i_584_;
								i_593_ += i_585_;
								i_594_ += i_586_;
								anIntArray4311[i_591_] = i_592_;
								anIntArray4342[i_591_] = i_593_;
								anIntArray4375[i_591_] = i_594_;
							}
						}
					}
				}
			} else {
				for (int i_595_ = 0; i_595_ < i_476_; i_595_++) {
					int i_596_ = is[i_595_];
					if (i_596_ < anIntArrayArray4367.length) {
						int[] is_597_ = anIntArrayArray4367[i_596_];
						for (int i_598_ = 0; i_598_ < is_597_.length; i_598_++) {
							int i_599_ = is_597_[i_598_];
							if (aShortArray4345 == null || (i_474_ & aShortArray4345[i_599_]) != 0) {
								anIntArray4311[i_599_] -= anInt4318;
								anIntArray4342[i_599_] -= anInt4349;
								anIntArray4375[i_599_] -= anInt4332;
								anIntArray4311[i_599_] = anIntArray4311[i_599_] * i_471_ / 128;
								anIntArray4342[i_599_] = anIntArray4342[i_599_] * i_472_ / 128;
								anIntArray4375[i_599_] = anIntArray4375[i_599_] * i_473_ / 128;
								anIntArray4311[i_599_] += anInt4318;
								anIntArray4342[i_599_] += anInt4349;
								anIntArray4375[i_599_] += anInt4332;
							}
						}
					}
				}
			}
		} else if (i == 5) {
			if (anIntArrayArray4368 != null && aByteArray4328 != null) {
				for (int i_600_ = 0; i_600_ < i_476_; i_600_++) {
					int i_601_ = is[i_600_];
					if (i_601_ < anIntArrayArray4368.length) {
						int[] is_602_ = anIntArrayArray4368[i_601_];
						for (int i_603_ = 0; i_603_ < is_602_.length; i_603_++) {
							int i_604_ = is_602_[i_603_];
							if (aShortArray4363 == null || (i_474_ & aShortArray4363[i_604_]) != 0) {
								int i_605_ = ((aByteArray4328[i_604_] & 0xff) + i_471_ * 8);
								if (i_605_ < 0)
									i_605_ = 0;
								else if (i_605_ > 255)
									i_605_ = 255;
								aByteArray4328[i_604_] = (byte) i_605_;
							}
						}
					}
				}
				if (aClass316Array4313 != null) {
					for (int i_606_ = 0; i_606_ < anInt4359; i_606_++) {
						Class316 class316 = aClass316Array4313[i_606_];
						Class128 class128 = aClass128Array4316[i_606_];
						class128.anInt1311 = (class128.anInt1311 & 0xffffff
								| 255 - (aByteArray4328[class316.anInt2804] & 0xff) << 24);
					}
				}
			}
		} else if (i == 7) {
			if (anIntArrayArray4368 != null) {
				for (int i_607_ = 0; i_607_ < i_476_; i_607_++) {
					int i_608_ = is[i_607_];
					if (i_608_ < anIntArrayArray4368.length) {
						int[] is_609_ = anIntArrayArray4368[i_608_];
						for (int i_610_ = 0; i_610_ < is_609_.length; i_610_++) {
							int i_611_ = is_609_[i_610_];
							if (aShortArray4363 == null || (i_474_ & aShortArray4363[i_611_]) != 0) {
								int i_612_ = aShortArray4341[i_611_] & 0xffff;
								int i_613_ = i_612_ >> 10 & 0x3f;
								int i_614_ = i_612_ >> 7 & 0x7;
								int i_615_ = i_612_ & 0x7f;
								i_613_ = i_613_ + i_471_ & 0x3f;
								i_614_ += i_472_;
								if (i_614_ < 0)
									i_614_ = 0;
								else if (i_614_ > 7)
									i_614_ = 7;
								i_615_ += i_473_;
								if (i_615_ < 0)
									i_615_ = 0;
								else if (i_615_ > 127)
									i_615_ = 127;
								aShortArray4341[i_611_] = (short) (i_613_ << 10 | i_614_ << 7 | i_615_);
							}
						}
						aBoolean4382 = true;
					}
				}
				if (aClass316Array4313 != null) {
					for (int i_616_ = 0; i_616_ < anInt4359; i_616_++) {
						Class316 class316 = aClass316Array4313[i_616_];
						Class128 class128 = aClass128Array4316[i_616_];
						class128.anInt1311 = (class128.anInt1311 & ~0xffffff
								| (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1
										.method3576((aShortArray4341[class316.anInt2804] & 0xffff), (byte) -50))
										& 0xffff]) & 0xffffff);
					}
				}
			}
		} else if (i == 8) {
			if (anIntArrayArray4333 != null) {
				for (int i_617_ = 0; i_617_ < i_476_; i_617_++) {
					int i_618_ = is[i_617_];
					if (i_618_ < anIntArrayArray4333.length) {
						int[] is_619_ = anIntArrayArray4333[i_618_];
						for (int i_620_ = 0; i_620_ < is_619_.length; i_620_++) {
							Class128 class128 = aClass128Array4316[is_619_[i_620_]];
							class128.anInt1309 += i_471_;
							class128.anInt1316 += i_472_;
						}
					}
				}
			}
		} else if (i == 10) {
			if (anIntArrayArray4333 != null) {
				for (int i_621_ = 0; i_621_ < i_476_; i_621_++) {
					int i_622_ = is[i_621_];
					if (i_622_ < anIntArrayArray4333.length) {
						int[] is_623_ = anIntArrayArray4333[i_622_];
						for (int i_624_ = 0; i_624_ < is_623_.length; i_624_++) {
							Class128 class128 = aClass128Array4316[is_623_[i_624_]];
							class128.anInt1314 = class128.anInt1314 * i_471_ >> 7;
							class128.anInt1312 = class128.anInt1312 * i_472_ >> 7;
						}
					}
				}
			}
		} else if (i == 9) {
			if (anIntArrayArray4333 != null) {
				for (int i_625_ = 0; i_625_ < i_476_; i_625_++) {
					int i_626_ = is[i_625_];
					if (i_626_ < anIntArrayArray4333.length) {
						int[] is_627_ = anIntArrayArray4333[i_626_];
						for (int i_628_ = 0; i_628_ < is_627_.length; i_628_++) {
							Class128 class128 = aClass128Array4316[is_627_[i_628_]];
							class128.anInt1307 = class128.anInt1307 + i_471_ & 0x3fff;
						}
					}
				}
			}
		}
	}

	public int V() {
		if (!aBoolean4362)
			method1759();
		return aShort4321;
	}

	private void method1766(Thread thread) {
		Class240 class240 = aHa_Sub2_4366.method1270(thread);
		aClass11_4383 = class240.aClass11_2285;
		if (class240 != aClass240_4388) {
			aClass240_4388 = class240;
			anIntArray4336 = aClass240_4388.anIntArray2278;
			anIntArray4322 = aClass240_4388.anIntArray2290;
			anIntArray4351 = aClass240_4388.anIntArray2294;
			anIntArray4310 = aClass240_4388.anIntArray2282;
			anIntArray4323 = aClass240_4388.anIntArray2288;
			anIntArray4337 = aClass240_4388.anIntArray2276;
			anIntArray4360 = aClass240_4388.anIntArray2286;
			anIntArray4358 = aClass240_4388.anIntArray2292;
			anIntArray4317 = aClass240_4388.anIntArray2275;
			anIntArray4335 = aClass240_4388.anIntArray2295;
			anIntArray4379 = aClass240_4388.anIntArray2274;
			anIntArray4348 = aClass240_4388.anIntArray2296;
			anIntArray4343 = aClass240_4388.anIntArray2297;
			anIntArray4330 = aClass240_4388.anIntArray2287;
			anIntArray4312 = aClass240_4388.anIntArray2280;
			anIntArray4309 = aClass240_4388.anIntArray2298;
			anIntArray4329 = aClass240_4388.anIntArray2291;
		}
	}

	public EffectiveVertex[] method1729() {
		return aClass232Array4314;
	}

	private boolean method1767(int i) {
		if (aByteArray4328 == null)
			return false;
		if (aByteArray4328[i] == 0)
			return false;
		return true;
	}

	public void v() {
		if ((anInt4326 & 0x10) != 16)
			throw new IllegalStateException();
		synchronized (this) {
			for (int i = 0; i < anInt4373; i++)
				anIntArray4375[i] = -anIntArray4375[i];
			if (aClass339Array4325 != null) {
				for (int i = 0; i < anInt4356; i++) {
					if (aClass339Array4325[i] != null)
						aClass339Array4325[i].anInt2973 = -aClass339Array4325[i].anInt2973;
				}
			}
			if (aClass339Array4319 != null) {
				for (int i = 0; i < anInt4356; i++) {
					if (aClass339Array4319[i] != null)
						aClass339Array4319[i].anInt2973 = -aClass339Array4319[i].anInt2973;
				}
			}
			if (aClass364Array4339 != null) {
				for (int i = 0; i < anInt4361; i++) {
					if (aClass364Array4339[i] != null)
						aClass364Array4339[i].anInt3113 = -aClass364Array4339[i].anInt3113;
				}
			}
			short[] is = aShortArray4387;
			aShortArray4387 = aShortArray4327;
			aShortArray4327 = is;
			if (aFloatArrayArray4384 != null) {
				for (int i = 0; i < anInt4361; i++) {
					if (aFloatArrayArray4384[i] != null) {
						float f = aFloatArrayArray4384[i][0];
						aFloatArrayArray4384[i][0] = aFloatArrayArray4384[i][2];
						aFloatArrayArray4384[i][2] = f;
					}
					if (aFloatArrayArray4376[i] != null) {
						float f = aFloatArrayArray4376[i][0];
						aFloatArrayArray4376[i][0] = aFloatArrayArray4376[i][2];
						aFloatArrayArray4376[i][2] = f;
					}
				}
			}
			aBoolean4362 = false;
			anInt4357 = 0;
		}
	}

	private void method1768(int i) {
		int i_629_ = 0;
		int i_630_ = aHa_Sub2_4366.anInt4107;
		short i_631_ = aShortArray4387[i];
		short i_632_ = aShortArray4324[i];
		short i_633_ = aShortArray4327[i];
		int i_634_ = anIntArray4310[i_631_];
		int i_635_ = anIntArray4310[i_632_];
		int i_636_ = anIntArray4310[i_633_];
		if (aByteArray4328 == null)
			aClass11_4383.anInt108 = 0;
		else
			aClass11_4383.anInt108 = aByteArray4328[i] & 0xff;
		if (i_634_ >= i_630_) {
			anIntArray4379[i_629_] = anIntArray4323[i_631_];
			anIntArray4348[i_629_] = anIntArray4337[i_631_];
			anIntArray4343[i_629_] = anIntArray4360[i_631_];
			anIntArray4330[i_629_++] = anIntArray4308[i] & 0xffff;
		} else {
			int i_637_ = anIntArray4322[i_631_];
			int i_638_ = anIntArray4351[i_631_];
			int i_639_ = anIntArray4308[i] & 0xffff;
			if (i_636_ >= i_630_) {
				int i_640_ = (i_630_ - i_634_) * (65536 / (i_636_ - i_634_));
				anIntArray4379[i_629_] = (aClass240_4388.anInt2271
						+ ((i_637_ + ((anIntArray4322[i_633_] - i_637_) * i_640_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_630_));
				anIntArray4348[i_629_] = (aClass240_4388.anInt2272
						+ ((i_638_ + ((anIntArray4351[i_633_] - i_638_) * i_640_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_630_));
				anIntArray4343[i_629_] = i_630_;
				anIntArray4330[i_629_++] = (i_639_ + (((anIntArray4331[i] & 0xffff) - i_639_) * i_640_ >> 16));
			}
			if (i_635_ >= i_630_) {
				int i_641_ = (i_630_ - i_634_) * (65536 / (i_635_ - i_634_));
				anIntArray4379[i_629_] = (aClass240_4388.anInt2271
						+ ((i_637_ + ((anIntArray4322[i_632_] - i_637_) * i_641_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_630_));
				anIntArray4348[i_629_] = (aClass240_4388.anInt2272
						+ ((i_638_ + ((anIntArray4351[i_632_] - i_638_) * i_641_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_630_));
				anIntArray4343[i_629_] = i_630_;
				anIntArray4330[i_629_++] = (i_639_ + (((anIntArray4338[i] & 0xffff) - i_639_) * i_641_ >> 16));
			}
		}
		if (i_635_ >= i_630_) {
			anIntArray4379[i_629_] = anIntArray4323[i_632_];
			anIntArray4348[i_629_] = anIntArray4337[i_632_];
			anIntArray4343[i_629_] = anIntArray4360[i_632_];
			anIntArray4330[i_629_++] = anIntArray4338[i] & 0xffff;
		} else {
			int i_642_ = anIntArray4322[i_632_];
			int i_643_ = anIntArray4351[i_632_];
			int i_644_ = anIntArray4338[i] & 0xffff;
			if (i_634_ >= i_630_) {
				int i_645_ = (i_630_ - i_635_) * (65536 / (i_634_ - i_635_));
				anIntArray4379[i_629_] = (aClass240_4388.anInt2271
						+ ((i_642_ + ((anIntArray4322[i_631_] - i_642_) * i_645_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_630_));
				anIntArray4348[i_629_] = (aClass240_4388.anInt2272
						+ ((i_643_ + ((anIntArray4351[i_631_] - i_643_) * i_645_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_630_));
				anIntArray4343[i_629_] = i_630_;
				anIntArray4330[i_629_++] = (i_644_ + (((anIntArray4308[i] & 0xffff) - i_644_) * i_645_ >> 16));
			}
			if (i_636_ >= i_630_) {
				int i_646_ = (i_630_ - i_635_) * (65536 / (i_636_ - i_635_));
				anIntArray4379[i_629_] = (aClass240_4388.anInt2271
						+ ((i_642_ + ((anIntArray4322[i_633_] - i_642_) * i_646_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_630_));
				anIntArray4348[i_629_] = (aClass240_4388.anInt2272
						+ ((i_643_ + ((anIntArray4351[i_633_] - i_643_) * i_646_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_630_));
				anIntArray4343[i_629_] = i_630_;
				anIntArray4330[i_629_++] = (i_644_ + (((anIntArray4331[i] & 0xffff) - i_644_) * i_646_ >> 16));
			}
		}
		if (i_636_ >= i_630_) {
			anIntArray4379[i_629_] = anIntArray4323[i_633_];
			anIntArray4348[i_629_] = anIntArray4337[i_633_];
			anIntArray4343[i_629_] = anIntArray4360[i_633_];
			anIntArray4330[i_629_++] = anIntArray4331[i] & 0xffff;
		} else {
			int i_647_ = anIntArray4322[i_633_];
			int i_648_ = anIntArray4351[i_633_];
			int i_649_ = anIntArray4331[i] & 0xffff;
			if (i_635_ >= i_630_) {
				int i_650_ = (i_630_ - i_636_) * (65536 / (i_635_ - i_636_));
				anIntArray4379[i_629_] = (aClass240_4388.anInt2271
						+ ((i_647_ + ((anIntArray4322[i_632_] - i_647_) * i_650_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_630_));
				anIntArray4348[i_629_] = (aClass240_4388.anInt2272
						+ ((i_648_ + ((anIntArray4351[i_632_] - i_648_) * i_650_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_630_));
				anIntArray4343[i_629_] = i_630_;
				anIntArray4330[i_629_++] = (i_649_ + (((anIntArray4338[i] & 0xffff) - i_649_) * i_650_ >> 16));
			}
			if (i_634_ >= i_630_) {
				int i_651_ = (i_630_ - i_636_) * (65536 / (i_634_ - i_636_));
				anIntArray4379[i_629_] = (aClass240_4388.anInt2271
						+ ((i_647_ + ((anIntArray4322[i_631_] - i_647_) * i_651_ >> 16)) * aHa_Sub2_4366.anInt4086
								/ i_630_));
				anIntArray4348[i_629_] = (aClass240_4388.anInt2272
						+ ((i_648_ + ((anIntArray4351[i_631_] - i_648_) * i_651_ >> 16)) * aHa_Sub2_4366.anInt4072
								/ i_630_));
				anIntArray4343[i_629_] = i_630_;
				anIntArray4330[i_629_++] = (i_649_ + (((anIntArray4308[i] & 0xffff) - i_649_) * i_651_ >> 16));
			}
		}
		int i_652_ = anIntArray4379[0];
		int i_653_ = anIntArray4379[1];
		int i_654_ = anIntArray4379[2];
		int i_655_ = anIntArray4348[0];
		int i_656_ = anIntArray4348[1];
		int i_657_ = anIntArray4348[2];
		i_634_ = anIntArray4343[0];
		i_635_ = anIntArray4343[1];
		i_636_ = anIntArray4343[2];
		aClass11_4383.aBoolean103 = false;
		if (i_629_ == 3) {
			if (i_652_ < 0 || i_653_ < 0 || i_654_ < 0 || i_652_ > aClass240_4388.anInt2273
					|| i_653_ > aClass240_4388.anInt2273 || i_654_ > aClass240_4388.anInt2273)
				aClass11_4383.aBoolean103 = true;
			if (aShortArray4350 == null || aShortArray4350[i] == -1) {
				if (anIntArray4331[i] == -1)
					aClass11_4383.method212((float) i_655_, (float) i_656_, (float) i_657_, (float) i_652_,
							(float) i_653_, (float) i_654_, (float) i_634_, (float) i_635_, (float) i_636_,
							(Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]));
				else
					aClass11_4383.method216((float) i_655_, (float) i_656_, (float) i_657_, (float) i_652_,
							(float) i_653_, (float) i_654_, (float) i_634_, (float) i_635_, (float) i_636_,
							(float) anIntArray4330[0], (float) anIntArray4330[1], (float) anIntArray4330[2]);
			} else {
				int i_658_ = -16777216;
				if (aByteArray4328 != null)
					i_658_ = 255 - (aByteArray4328[i] & 0xff) << 24;
				int i_659_ = i_658_ | anIntArray4308[i] & 0xffffff;
				if (anIntArray4331[i] == -1)
					aClass11_4383.method209((float) i_655_, (float) i_656_, (float) i_657_, (float) i_652_,
							(float) i_653_, (float) i_654_, (float) i_634_, (float) i_635_, (float) i_636_,
							aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2],
							aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2], i_659_,
							i_659_, i_659_, aClass240_4388.anInt2265, 0, 0, 0, aShortArray4350[i]);
				else
					aClass11_4383.method209((float) i_655_, (float) i_656_, (float) i_657_, (float) i_652_,
							(float) i_653_, (float) i_654_, (float) i_634_, (float) i_635_, (float) i_636_,
							aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2],
							aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2], i_659_,
							i_659_, i_659_, aClass240_4388.anInt2265, 0, 0, 0, aShortArray4350[i]);
			}
		}
		if (i_629_ == 4) {
			if (i_652_ < 0 || i_653_ < 0 || i_654_ < 0 || i_652_ > aClass240_4388.anInt2273
					|| i_653_ > aClass240_4388.anInt2273 || i_654_ > aClass240_4388.anInt2273 || anIntArray4379[3] < 0
					|| anIntArray4379[3] > aClass240_4388.anInt2273)
				aClass11_4383.aBoolean103 = true;
			if (aShortArray4350 == null || aShortArray4350[i] == -1) {
				if (anIntArray4331[i] == -1) {
					int i_660_ = (Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]);
					aClass11_4383.method212((float) i_655_, (float) i_656_, (float) i_657_, (float) i_652_,
							(float) i_653_, (float) i_654_, (float) i_634_, (float) i_635_, (float) i_636_, i_660_);
					aClass11_4383.method212((float) i_655_, (float) i_657_, (float) anIntArray4348[3], (float) i_652_,
							(float) i_654_, (float) anIntArray4379[3], (float) i_634_, (float) i_635_,
							(float) anIntArray4343[3], i_660_);
				} else {
					aClass11_4383.method216((float) i_655_, (float) i_656_, (float) i_657_, (float) i_652_,
							(float) i_653_, (float) i_654_, (float) i_634_, (float) i_635_, (float) i_636_,
							(float) anIntArray4330[0], (float) anIntArray4330[1], (float) anIntArray4330[2]);
					aClass11_4383.method216((float) i_655_, (float) i_657_, (float) anIntArray4348[3], (float) i_652_,
							(float) i_654_, (float) anIntArray4379[3], (float) i_634_, (float) i_635_,
							(float) anIntArray4343[3], (float) anIntArray4330[0], (float) anIntArray4330[2],
							(float) anIntArray4330[3]);
				}
			} else {
				int i_661_ = -16777216;
				if (aByteArray4328 != null)
					i_661_ = 255 - (aByteArray4328[i] & 0xff) << 24;
				int i_662_ = i_661_ | anIntArray4308[i] & 0xffffff;
				if (anIntArray4331[i] == -1) {
					aClass11_4383.method209((float) i_655_, (float) i_656_, (float) i_657_, (float) i_652_,
							(float) i_653_, (float) i_654_, (float) i_634_, (float) i_635_, (float) i_636_,
							aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2],
							aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2], i_662_,
							i_662_, i_662_, aClass240_4388.anInt2265, 0, 0, 0, aShortArray4350[i]);
					aClass11_4383.method209((float) i_655_, (float) i_657_, (float) anIntArray4348[3], (float) i_652_,
							(float) i_654_, (float) anIntArray4379[3], (float) i_634_, (float) i_636_,
							(float) anIntArray4343[3], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
							aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
							aFloatArrayArray4376[i][2], i_662_, i_662_, i_662_, aClass240_4388.anInt2265, 0, 0, 0,
							aShortArray4350[i]);
				} else {
					aClass11_4383.method209((float) i_655_, (float) i_656_, (float) i_657_, (float) i_652_,
							(float) i_653_, (float) i_654_, (float) i_634_, (float) i_635_, (float) i_636_,
							aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2],
							aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2], i_662_,
							i_662_, i_662_, aClass240_4388.anInt2265, 0, 0, 0, aShortArray4350[i]);
					aClass11_4383.method209((float) i_655_, (float) i_657_, (float) anIntArray4348[3], (float) i_652_,
							(float) i_654_, (float) anIntArray4379[3], (float) i_634_, (float) i_636_,
							(float) anIntArray4343[3], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
							aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
							aFloatArrayArray4376[i][2], i_662_, i_662_, i_662_, aClass240_4388.anInt2265, 0, 0, 0,
							aShortArray4350[i]);
				}
			}
		}
	}

	public void O(int i, int i_663_, int i_664_) {
		if (i != 128 && (anInt4326 & 0x1) != 1)
			throw new IllegalStateException();
		if (i_663_ != 128 && (anInt4326 & 0x2) != 2)
			throw new IllegalStateException();
		if (i_664_ != 128 && (anInt4326 & 0x4) != 4)
			throw new IllegalStateException();
		synchronized (this) {
			for (int i_665_ = 0; i_665_ < anInt4373; i_665_++) {
				anIntArray4311[i_665_] = anIntArray4311[i_665_] * i >> 7;
				anIntArray4342[i_665_] = anIntArray4342[i_665_] * i_663_ >> 7;
				anIntArray4375[i_665_] = anIntArray4375[i_665_] * i_664_ >> 7;
			}
			aBoolean4362 = false;
		}
	}

	public void method1724(byte i, byte[] is) {
		if ((anInt4326 & 0x100000) == 0)
			throw new RuntimeException();
		if (aByteArray4328 == null)
			aByteArray4328 = new byte[anInt4361];
		if (is == null) {
			for (int i_666_ = 0; i_666_ < anInt4361; i_666_++)
				aByteArray4328[i_666_] = i;
		} else {
			for (int i_667_ = 0; i_667_ < anInt4361; i_667_++) {
				int i_668_ = 255 - ((255 - (is[i_667_] & 0xff)) * (255 - (i & 0xff)) / 255);
				aByteArray4328[i_667_] = (byte) i_668_;
			}
		}
		if (anInt4357 == 2)
			anInt4357 = 1;
	}

	private void method1769() {
		aClass339Array4325 = new Class339[anInt4356];
		for (int i = 0; i < anInt4356; i++)
			aClass339Array4325[i] = new Class339();
		for (int i = 0; i < anInt4361; i++) {
			short i_669_ = aShortArray4387[i];
			short i_670_ = aShortArray4324[i];
			short i_671_ = aShortArray4327[i];
			int i_672_ = anIntArray4311[i_670_] - anIntArray4311[i_669_];
			int i_673_ = anIntArray4342[i_670_] - anIntArray4342[i_669_];
			int i_674_ = anIntArray4375[i_670_] - anIntArray4375[i_669_];
			int i_675_ = anIntArray4311[i_671_] - anIntArray4311[i_669_];
			int i_676_ = anIntArray4342[i_671_] - anIntArray4342[i_669_];
			int i_677_ = anIntArray4375[i_671_] - anIntArray4375[i_669_];
			int i_678_ = i_673_ * i_677_ - i_676_ * i_674_;
			int i_679_ = i_674_ * i_675_ - i_677_ * i_672_;
			int i_680_;
			for (i_680_ = i_672_ * i_676_ - i_675_ * i_673_; (i_678_ > 8192 || i_679_ > 8192 || i_680_ > 8192
					|| i_678_ < -8192 || i_679_ < -8192 || i_680_ < -8192); i_680_ >>= 1) {
				i_678_ >>= 1;
				i_679_ >>= 1;
			}
			int i_681_ = (int) Math.sqrt((double) (i_678_ * i_678_ + i_679_ * i_679_ + i_680_ * i_680_));
			if (i_681_ <= 0)
				i_681_ = 1;
			i_678_ = i_678_ * 256 / i_681_;
			i_679_ = i_679_ * 256 / i_681_;
			i_680_ = i_680_ * 256 / i_681_;
			byte i_682_;
			if (aByteArray4386 == null)
				i_682_ = (byte) 0;
			else
				i_682_ = aByteArray4386[i];
			if (i_682_ == 0) {
				Class339 class339 = aClass339Array4325[i_669_];
				class339.anInt2974 += i_678_;
				class339.anInt2976 += i_679_;
				class339.anInt2973 += i_680_;
				class339.anInt2975++;
				class339 = aClass339Array4325[i_670_];
				class339.anInt2974 += i_678_;
				class339.anInt2976 += i_679_;
				class339.anInt2973 += i_680_;
				class339.anInt2975++;
				class339 = aClass339Array4325[i_671_];
				class339.anInt2974 += i_678_;
				class339.anInt2976 += i_679_;
				class339.anInt2973 += i_680_;
				class339.anInt2975++;
			} else if (i_682_ == 1) {
				if (aClass364Array4339 == null)
					aClass364Array4339 = new Class364[anInt4361];
				Class364 class364 = aClass364Array4339[i] = new Class364();
				class364.anInt3112 = i_678_;
				class364.anInt3114 = i_679_;
				class364.anInt3113 = i_680_;
			}
		}
	}

	private int method1770(int i) {
		if (i < 2)
			i = 2;
		else if (i > 126)
			i = 126;
		return i;
	}

	public int da() {
		return anInt4371;
	}

	public int ma() {
		if (!aBoolean4362)
			method1759();
		return aShort4369;
	}

	public void method1722() {
		/* empty */
	}

	private void method1771(int i) {
		if (!aClass240_4388.aBoolean2269) {
			short i_683_ = aShortArray4387[i];
			short i_684_ = aShortArray4324[i];
			short i_685_ = aShortArray4327[i];
			int i_686_ = anIntArray4360[i_683_] - aClass240_4388.anInt2267;
			if (i_686_ > 255)
				i_686_ = 255;
			else if (i_686_ < 0)
				i_686_ = 0;
			int i_687_ = anIntArray4360[i_684_] - aClass240_4388.anInt2267;
			if (i_687_ > 255)
				i_687_ = 255;
			else if (i_687_ < 0)
				i_687_ = 0;
			int i_688_ = anIntArray4360[i_685_] - aClass240_4388.anInt2267;
			if (i_688_ > 255)
				i_688_ = 255;
			else if (i_688_ < 0)
				i_688_ = 0;
			int i_689_ = i_686_ + i_687_ + i_688_;
			if (i_689_ != 765) {
				if (i_689_ == 0)
					method1746(i);
				else {
					if (aByteArray4328 == null)
						aClass11_4383.anInt108 = 0;
					else
						aClass11_4383.anInt108 = aByteArray4328[i] & 0xff;
					if (aShortArray4350 == null || aShortArray4350[i] == -1) {
						if (anIntArray4331[i] == -1)
							aClass11_4383.method199((float) anIntArray4337[i_683_], (float) anIntArray4337[i_684_],
									(float) anIntArray4337[i_685_], (float) anIntArray4323[i_683_],
									(float) anIntArray4323[i_684_], (float) anIntArray4323[i_685_],
									(float) anIntArray4360[i_683_], (float) anIntArray4360[i_684_],
									(float) anIntArray4360[i_685_],
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]),
											16711680, i_686_ << 24 | aClass240_4388.anInt2265)),
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]),
											16711680, i_687_ << 24 | aClass240_4388.anInt2265)),
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]),
											16711680, i_688_ << 24 | aClass240_4388.anInt2265)));
						else
							aClass11_4383.method199((float) anIntArray4337[i_683_], (float) anIntArray4337[i_684_],
									(float) anIntArray4337[i_685_], (float) anIntArray4323[i_683_],
									(float) anIntArray4323[i_684_], (float) anIntArray4323[i_685_],
									(float) anIntArray4360[i_683_], (float) anIntArray4360[i_684_],
									(float) anIntArray4360[i_685_],
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]),
											16711680, i_686_ << 24 | aClass240_4388.anInt2265)),
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4338[i] & 0xffff]),
											16711680, i_687_ << 24 | aClass240_4388.anInt2265)),
									(Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4331[i] & 0xffff]),
											16711680, i_688_ << 24 | aClass240_4388.anInt2265)));
					} else {
						int i_690_ = -16777216;
						if (aByteArray4328 != null)
							i_690_ = 255 - (aByteArray4328[i] & 0xff) << 24;
						if (anIntArray4331[i] == -1) {
							int i_691_ = i_690_ | anIntArray4308[i] & 0xffffff;
							aClass11_4383.method214((float) anIntArray4337[i_683_], (float) anIntArray4337[i_684_],
									(float) anIntArray4337[i_685_], (float) anIntArray4323[i_683_],
									(float) anIntArray4323[i_684_], (float) anIntArray4323[i_685_],
									(float) anIntArray4360[i_683_], (float) anIntArray4360[i_684_],
									(float) anIntArray4360[i_685_], aFloatArrayArray4384[i][0],
									aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0],
									aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2], i_691_, i_691_, i_691_,
									aClass240_4388.anInt2265, i_686_, i_687_, i_688_, aShortArray4350[i]);
						} else
							aClass11_4383.method214((float) anIntArray4337[i_683_], (float) anIntArray4337[i_684_],
									(float) anIntArray4337[i_685_], (float) anIntArray4323[i_683_],
									(float) anIntArray4323[i_684_], (float) anIntArray4323[i_685_],
									(float) anIntArray4360[i_683_], (float) anIntArray4360[i_684_],
									(float) anIntArray4360[i_685_], aFloatArrayArray4384[i][0],
									aFloatArrayArray4384[i][1], aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0],
									aFloatArrayArray4376[i][1], aFloatArrayArray4376[i][2],
									i_690_ | anIntArray4308[i] & 0xffffff, i_690_ | anIntArray4338[i] & 0xffffff,
									i_690_ | anIntArray4331[i] & 0xffffff, aClass240_4388.anInt2265, i_686_, i_687_,
									i_688_, aShortArray4350[i]);
					}
				}
			}
		} else {
			short i_692_ = aShortArray4387[i];
			short i_693_ = aShortArray4324[i];
			short i_694_ = aShortArray4327[i];
			int i_695_ = 0;
			int i_696_ = 0;
			int i_697_ = 0;
			if (anIntArray4336[i_692_] > aClass240_4388.anInt2260)
				i_695_ = 255;
			else if (anIntArray4336[i_692_] > aClass240_4388.anInt2266)
				i_695_ = ((aClass240_4388.anInt2266 - anIntArray4336[i_692_]) * 255
						/ (aClass240_4388.anInt2266 - aClass240_4388.anInt2260));
			if (anIntArray4336[i_693_] > aClass240_4388.anInt2260)
				i_696_ = 255;
			else if (anIntArray4336[i_693_] > aClass240_4388.anInt2266)
				i_696_ = ((aClass240_4388.anInt2266 - anIntArray4336[i_693_]) * 255
						/ (aClass240_4388.anInt2266 - aClass240_4388.anInt2260));
			if (anIntArray4336[i_694_] > aClass240_4388.anInt2260)
				i_697_ = 255;
			else if (anIntArray4336[i_694_] > aClass240_4388.anInt2266)
				i_697_ = ((aClass240_4388.anInt2266 - anIntArray4336[i_694_]) * 255
						/ (aClass240_4388.anInt2266 - aClass240_4388.anInt2260));
			if (aByteArray4328 == null)
				aClass11_4383.anInt108 = 0;
			else
				aClass11_4383.anInt108 = aByteArray4328[i] & 0xff;
			if (aShortArray4350 == null || aShortArray4350[i] == -1) {
				if (anIntArray4331[i] == -1)
					aClass11_4383.method199((float) anIntArray4337[i_692_], (float) anIntArray4337[i_693_],
							(float) anIntArray4337[i_694_], (float) anIntArray4323[i_692_],
							(float) anIntArray4323[i_693_], (float) anIntArray4323[i_694_],
							(float) anIntArray4360[i_692_], (float) anIntArray4360[i_693_],
							(float) anIntArray4360[i_694_],
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]), 16711680,
									(i_695_ << 24 | aClass240_4388.anInt2265)),
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]), 16711680,
									(i_696_ << 24 | aClass240_4388.anInt2265)),
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]), 16711680,
									(i_697_ << 24 | aClass240_4388.anInt2265)));
				else
					aClass11_4383.method199((float) anIntArray4337[i_692_], (float) anIntArray4337[i_693_],
							(float) anIntArray4337[i_694_], (float) anIntArray4323[i_692_],
							(float) anIntArray4323[i_693_], (float) anIntArray4323[i_694_],
							(float) anIntArray4360[i_692_], (float) anIntArray4360[i_693_],
							(float) anIntArray4360[i_694_],
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4308[i] & 0xffff]), 16711680,
									(i_695_ << 24 | aClass240_4388.anInt2265)),
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4338[i] & 0xffff]), 16711680,
									(i_696_ << 24 | aClass240_4388.anInt2265)),
							Class69_Sub4.method745((Class295_Sub1.anIntArray3691[anIntArray4331[i] & 0xffff]), 16711680,
									(i_697_ << 24 | aClass240_4388.anInt2265)));
			} else {
				int i_698_ = -16777216;
				if (aByteArray4328 != null)
					i_698_ = 255 - (aByteArray4328[i] & 0xff) << 24;
				if (anIntArray4331[i] == -1) {
					int i_699_ = i_698_ | anIntArray4308[i] & 0xffffff;
					aClass11_4383.method214((float) anIntArray4337[i_692_], (float) anIntArray4337[i_693_],
							(float) anIntArray4337[i_694_], (float) anIntArray4323[i_692_],
							(float) anIntArray4323[i_693_], (float) anIntArray4323[i_694_],
							(float) anIntArray4360[i_692_], (float) anIntArray4360[i_693_],
							(float) anIntArray4360[i_694_], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
							aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
							aFloatArrayArray4376[i][2], i_699_, i_699_, i_699_, aClass240_4388.anInt2265, i_695_,
							i_696_, i_697_, aShortArray4350[i]);
				} else
					aClass11_4383.method214((float) anIntArray4337[i_692_], (float) anIntArray4337[i_693_],
							(float) anIntArray4337[i_694_], (float) anIntArray4323[i_692_],
							(float) anIntArray4323[i_693_], (float) anIntArray4323[i_694_],
							(float) anIntArray4360[i_692_], (float) anIntArray4360[i_693_],
							(float) anIntArray4360[i_694_], aFloatArrayArray4384[i][0], aFloatArrayArray4384[i][1],
							aFloatArrayArray4384[i][2], aFloatArrayArray4376[i][0], aFloatArrayArray4376[i][1],
							aFloatArrayArray4376[i][2], i_698_ | anIntArray4308[i] & 0xffffff,
							i_698_ | anIntArray4338[i] & 0xffffff, i_698_ | anIntArray4331[i] & 0xffffff,
							aClass240_4388.anInt2265, i_695_, i_696_, i_697_, aShortArray4350[i]);
			}
		}
	}

	public void method1720() {
		if (aHa_Sub2_4366.anInt4105 > 1) {
			synchronized (this) {
				while (aBoolean1849) {
					try {
						this.wait();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
				aBoolean1849 = true;
			}
		}
	}

	private void method1772(Thread thread) {
		Class240 class240 = aHa_Sub2_4366.method1270(thread);
		if (class240 != aClass240_4352) {
			aClass240_4352 = class240;
			aClass178_Sub1Array4347 = aClass240_4352.aClass178_Sub1Array2283;
			aClass178_Sub1Array4320 = aClass240_4352.aClass178_Sub1Array2289;
		}
	}

	private void method1773() {
		synchronized (this) {
			for (int i = 0; i < anInt4373; i++) {
				int i_700_ = anIntArray4311[i];
				anIntArray4311[i] = anIntArray4375[i];
				anIntArray4375[i] = -i_700_;
			}
			method1777();
		}
	}

	public int RA() {
		if (!aBoolean4362)
			method1759();
		return aShort4370;
	}

	private void method1774() {
		synchronized (this) {
			for (int i = 0; i < anInt4373; i++) {
				int i_701_ = anIntArray4375[i];
				anIntArray4375[i] = anIntArray4311[i];
				anIntArray4311[i] = -i_701_;
			}
			method1777();
		}
	}

	private void method1775() {
		synchronized (this) {
			for (int i = 0; i < anInt4356; i++) {
				int i_702_ = anIntArray4311[i];
				anIntArray4311[i] = anIntArray4375[i];
				anIntArray4375[i] = -i_702_;
				if (aClass339Array4325[i] != null) {
					i_702_ = aClass339Array4325[i].anInt2974;
					aClass339Array4325[i].anInt2974 = aClass339Array4325[i].anInt2973;
					aClass339Array4325[i].anInt2973 = -i_702_;
				}
			}
			if (aClass364Array4339 != null) {
				for (int i = 0; i < anInt4361; i++) {
					if (aClass364Array4339[i] != null) {
						int i_703_ = aClass364Array4339[i].anInt3112;
						aClass364Array4339[i].anInt3112 = aClass364Array4339[i].anInt3113;
						aClass364Array4339[i].anInt3113 = -i_703_;
					}
				}
			}
			for (int i = anInt4356; i < anInt4373; i++) {
				int i_704_ = anIntArray4311[i];
				anIntArray4311[i] = anIntArray4375[i];
				anIntArray4375[i] = -i_704_;
			}
			anInt4357 = 0;
			aBoolean4362 = false;
		}
	}

	public byte[] method1733() {
		return aByteArray4328;
	}

	public int ua() {
		return anInt4326;
	}

	public boolean method1731(int i, int i_705_, Class373 class373, boolean bool, int i_706_, int i_707_) {
		return method1754(i, i_705_, class373, bool, i_706_, i_707_);
	}

	public void k(int i) {
		if ((anInt4326 & 0xd) != 13)
			throw new IllegalStateException();
		if (aClass339Array4325 != null) {
			if (i == 4096)
				method1775();
			else if (i == 8192)
				method1778();
			else if (i == 12288)
				method1745();
			else {
				int i_708_ = Class296_Sub4.anIntArray4598[i];
				int i_709_ = Class296_Sub4.anIntArray4618[i];
				synchronized (this) {
					for (int i_710_ = 0; i_710_ < anInt4356; i_710_++) {
						int i_711_ = ((anIntArray4375[i_710_] * i_708_ + anIntArray4311[i_710_] * i_709_) >> 14);
						anIntArray4375[i_710_] = (anIntArray4375[i_710_] * i_709_
								- anIntArray4311[i_710_] * i_708_) >> 14;
						anIntArray4311[i_710_] = i_711_;
						if (aClass339Array4325[i_710_] != null) {
							i_711_ = ((aClass339Array4325[i_710_].anInt2973 * i_708_)
									+ (aClass339Array4325[i_710_].anInt2974 * i_709_)) >> 14;
							aClass339Array4325[i_710_].anInt2973 = ((aClass339Array4325[i_710_].anInt2973 * i_709_)
									- (aClass339Array4325[i_710_].anInt2974 * i_708_)) >> 14;
							aClass339Array4325[i_710_].anInt2974 = i_711_;
						}
					}
					if (aClass364Array4339 != null) {
						for (int i_712_ = 0; i_712_ < anInt4361; i_712_++) {
							if (aClass364Array4339[i_712_] != null) {
								int i_713_ = (((aClass364Array4339[i_712_].anInt3113 * i_708_)
										+ (aClass364Array4339[i_712_].anInt3112 * i_709_)) >> 14);
								aClass364Array4339[i_712_].anInt3113 = ((aClass364Array4339[i_712_].anInt3113 * i_709_)
										- (aClass364Array4339[i_712_].anInt3112 * i_708_)) >> 14;
								aClass364Array4339[i_712_].anInt3112 = i_713_;
							}
						}
					}
					for (int i_714_ = anInt4356; i_714_ < anInt4373; i_714_++) {
						int i_715_ = ((anIntArray4375[i_714_] * i_708_ + anIntArray4311[i_714_] * i_709_) >> 14);
						anIntArray4375[i_714_] = (anIntArray4375[i_714_] * i_709_
								- anIntArray4311[i_714_] * i_708_) >> 14;
						anIntArray4311[i_714_] = i_715_;
					}
					anInt4357 = 0;
					aBoolean4362 = false;
				}
			}
		} else
			a(i);
	}

	private void method1776(int i, boolean bool, boolean bool_716_) {
		if (anIntArray4331[i] != -2) {
			short i_717_ = aShortArray4387[i];
			short i_718_ = aShortArray4324[i];
			short i_719_ = aShortArray4327[i];
			int i_720_ = anIntArray4323[i_717_];
			int i_721_ = anIntArray4323[i_718_];
			int i_722_ = anIntArray4323[i_719_];
			if (bool && (i_720_ == -5000 || i_721_ == -5000 || i_722_ == -5000)) {
				int i_723_ = anIntArray4322[i_717_];
				int i_724_ = anIntArray4322[i_718_];
				int i_725_ = anIntArray4322[i_719_];
				int i_726_ = anIntArray4351[i_717_];
				int i_727_ = anIntArray4351[i_718_];
				int i_728_ = anIntArray4351[i_719_];
				int i_729_ = anIntArray4310[i_717_];
				int i_730_ = anIntArray4310[i_718_];
				int i_731_ = anIntArray4310[i_719_];
				i_723_ -= i_724_;
				i_725_ -= i_724_;
				i_726_ -= i_727_;
				i_728_ -= i_727_;
				i_729_ -= i_730_;
				i_731_ -= i_730_;
				int i_732_ = i_726_ * i_731_ - i_729_ * i_728_;
				int i_733_ = i_729_ * i_725_ - i_723_ * i_731_;
				int i_734_ = i_723_ * i_728_ - i_726_ * i_725_;
				if (i_724_ * i_732_ + i_727_ * i_733_ + i_730_ * i_734_ > 0)
					method1768(i);
			} else if (anIntArray4329[i] != -1 || ((i_720_ - i_721_) * (anIntArray4337[i_719_] - anIntArray4337[i_718_])
					- ((anIntArray4337[i_717_] - anIntArray4337[i_718_]) * (i_722_ - i_721_))) > 0) {
				if (i_720_ < 0 || i_721_ < 0 || i_722_ < 0 || i_720_ > aClass240_4388.anInt2273
						|| i_721_ > aClass240_4388.anInt2273 || i_722_ > aClass240_4388.anInt2273)
					aClass11_4383.aBoolean103 = true;
				else
					aClass11_4383.aBoolean103 = false;
				if (bool_716_) {
					int i_735_ = anIntArray4329[i];
					if (i_735_ == -1 || !aClass316Array4313[i_735_].aBoolean2805)
						method1756(i);
				} else {
					int i_736_ = anIntArray4329[i];
					if (i_736_ != -1) {
						Class316 class316 = aClass316Array4313[i_736_];
						Class128 class128 = aClass128Array4316[i_736_];
						if (!class316.aBoolean2805)
							method1765(i);
						aHa_Sub2_4366.method1254(class128.anInt1305, class128.anInt1308, class128.anInt1306,
								class128.anInt1313, class128.anInt1310, class128.anInt1307,
								class316.aShort2793 & 0xffff, class128.anInt1311, class316.aByte2802,
								class316.aByte2794);
					} else
						method1765(i);
				}
			}
		}
	}

	public void method1736(Model class178, int i, int i_737_, int i_738_, boolean bool) {
		Class178_Sub1 class178_sub1_739_ = (Class178_Sub1) class178;
		if ((anInt4326 & 0x10000) != 65536)
			throw new IllegalStateException("");
		if ((class178_sub1_739_.anInt4326 & 0x10000) != 65536)
			throw new IllegalStateException("");
		method1766(Thread.currentThread());
		method1759();
		method1757();
		class178_sub1_739_.method1759();
		class178_sub1_739_.method1757();
		anInt4372++;
		int i_740_ = 0;
		int[] is = class178_sub1_739_.anIntArray4311;
		int i_741_ = class178_sub1_739_.anInt4356;
		for (int i_742_ = 0; i_742_ < anInt4356; i_742_++) {
			Class339 class339 = aClass339Array4325[i_742_];
			if (class339.anInt2975 != 0) {
				int i_743_ = anIntArray4342[i_742_] - i_737_;
				if (i_743_ >= class178_sub1_739_.aShort4381 && i_743_ <= class178_sub1_739_.aShort4377) {
					int i_744_ = anIntArray4311[i_742_] - i;
					if (i_744_ >= class178_sub1_739_.aShort4321 && i_744_ <= class178_sub1_739_.aShort4370) {
						int i_745_ = anIntArray4375[i_742_] - i_738_;
						if (i_745_ >= class178_sub1_739_.aShort4389 && i_745_ <= class178_sub1_739_.aShort4346) {
							for (int i_746_ = 0; i_746_ < i_741_; i_746_++) {
								Class339 class339_747_ = (class178_sub1_739_.aClass339Array4325[i_746_]);
								if (i_744_ == is[i_746_] && i_745_ == (class178_sub1_739_.anIntArray4375[i_746_])
										&& i_743_ == (class178_sub1_739_.anIntArray4342[i_746_])
										&& class339_747_.anInt2975 != 0) {
									if (aClass339Array4319 == null)
										aClass339Array4319 = new Class339[anInt4356];
									if (class178_sub1_739_.aClass339Array4319 == null)
										class178_sub1_739_.aClass339Array4319 = new Class339[i_741_];
									Class339 class339_748_ = aClass339Array4319[i_742_];
									if (class339_748_ == null)
										class339_748_ = aClass339Array4319[i_742_] = new Class339(class339);
									Class339 class339_749_ = (class178_sub1_739_.aClass339Array4319[i_746_]);
									if (class339_749_ == null)
										class339_749_ = class178_sub1_739_.aClass339Array4319[i_746_] = new Class339(
												class339_747_);
									class339_748_.anInt2974 += class339_747_.anInt2974;
									class339_748_.anInt2976 += class339_747_.anInt2976;
									class339_748_.anInt2973 += class339_747_.anInt2973;
									class339_748_.anInt2975 += class339_747_.anInt2975;
									class339_749_.anInt2974 += class339.anInt2974;
									class339_749_.anInt2976 += class339.anInt2976;
									class339_749_.anInt2973 += class339.anInt2973;
									class339_749_.anInt2975 += class339.anInt2975;
									i_740_++;
									anIntArray4312[i_742_] = anInt4372;
									anIntArray4309[i_746_] = anInt4372;
								}
							}
						}
					}
				}
			}
		}
		if (i_740_ >= 3 && bool) {
			for (int i_750_ = 0; i_750_ < anInt4361; i_750_++) {
				if (anIntArray4312[aShortArray4387[i_750_]] == anInt4372
						&& anIntArray4312[aShortArray4324[i_750_]] == anInt4372
						&& anIntArray4312[aShortArray4327[i_750_]] == anInt4372) {
					if (aByteArray4386 == null)
						aByteArray4386 = new byte[anInt4361];
					aByteArray4386[i_750_] = (byte) 2;
				}
			}
			for (int i_751_ = 0; i_751_ < class178_sub1_739_.anInt4361; i_751_++) {
				if ((anIntArray4309[class178_sub1_739_.aShortArray4387[i_751_]] == anInt4372)
						&& anIntArray4309[(class178_sub1_739_.aShortArray4324[i_751_])] == anInt4372
						&& anIntArray4309[(class178_sub1_739_.aShortArray4327[i_751_])] == anInt4372) {
					if (class178_sub1_739_.aByteArray4386 == null)
						class178_sub1_739_.aByteArray4386 = new byte[class178_sub1_739_.anInt4361];
					class178_sub1_739_.aByteArray4386[i_751_] = (byte) 2;
				}
			}
		}
	}

	public void method1715(Class373 class373, Class338_Sub5 class338_sub5, int i) {
		method1747(class373, class338_sub5, -1, i);
	}

	public void method1730() {
		if (aHa_Sub2_4366.anInt4105 > 1) {
			synchronized (this) {
				aBoolean1849 = false;
				this.notifyAll();
			}
		}
	}

	public void ia(short i, short i_752_) {
		for (int i_753_ = 0; i_753_ < anInt4361; i_753_++) {
			if (aShortArray4341[i_753_] == i)
				aShortArray4341[i_753_] = i_752_;
		}
		if (aClass316Array4313 != null) {
			for (int i_754_ = 0; i_754_ < anInt4359; i_754_++) {
				Class316 class316 = aClass316Array4313[i_754_];
				Class128 class128 = aClass128Array4316[i_754_];
				class128.anInt1311 = (class128.anInt1311 & ~0xffffff
						| (Class295_Sub1.anIntArray3691[((Class338_Sub3_Sub4_Sub1
								.method3576(aShortArray4341[class316.anInt2804], (byte) -99)) & 0xffff)]) & 0xffffff);
			}
		}
		if (anInt4357 == 2)
			anInt4357 = 1;
	}

	public int fa() {
		if (!aBoolean4362)
			method1759();
		return aShort4381;
	}

	public void FA(int i) {
		if ((anInt4326 & 0x6) != 6)
			throw new IllegalStateException();
		int i_755_ = Class296_Sub4.anIntArray4598[i];
		int i_756_ = Class296_Sub4.anIntArray4618[i];
		synchronized (this) {
			for (int i_757_ = 0; i_757_ < anInt4373; i_757_++) {
				int i_758_ = ((anIntArray4342[i_757_] * i_756_ - anIntArray4375[i_757_] * i_755_) >> 14);
				anIntArray4375[i_757_] = (anIntArray4342[i_757_] * i_755_ + anIntArray4375[i_757_] * i_756_) >> 14;
				anIntArray4342[i_757_] = i_758_;
			}
			method1777();
		}
	}

	private void method1777() {
		aClass339Array4325 = null;
		aClass339Array4319 = null;
		aClass364Array4339 = null;
		aBoolean4362 = false;
	}

	public void p(int i, int i_759_, s var_s, s var_s_760_, int i_761_, int i_762_, int i_763_) {
		if (i == 3) {
			if ((anInt4326 & 0x7) != 7)
				throw new IllegalStateException();
		} else if ((anInt4326 & 0x2) != 2)
			throw new IllegalStateException();
		if (!aBoolean4362)
			method1759();
		int i_764_ = i_761_ + aShort4321;
		int i_765_ = i_761_ + aShort4370;
		int i_766_ = i_763_ + aShort4389;
		int i_767_ = i_763_ + aShort4346;
		if (i == 4 || (i_764_ >= 0 && (i_765_ + var_s.anInt2836 >> var_s.anInt2835 < var_s.anInt2832) && i_766_ >= 0
				&& (i_767_ + var_s.anInt2836 >> var_s.anInt2835 < var_s.anInt2834))) {
			int[][] is = ((s_Sub1) var_s).anIntArrayArray2831;
			int[][] is_768_ = null;
			if (var_s_760_ != null)
				is_768_ = ((s_Sub1) var_s_760_).anIntArrayArray2831;
			if (i == 4 || i == 5) {
				if (var_s_760_ == null || (i_764_ < 0
						|| (i_765_ + var_s_760_.anInt2836 >> var_s_760_.anInt2835) >= var_s_760_.anInt2832 || i_766_ < 0
						|| (i_767_ + var_s_760_.anInt2836 >> var_s_760_.anInt2835) >= var_s_760_.anInt2834))
					return;
			} else {
				i_764_ >>= var_s.anInt2835;
				i_765_ = i_765_ + (var_s.anInt2836 - 1) >> var_s.anInt2835;
				i_766_ >>= var_s.anInt2835;
				i_767_ = i_767_ + (var_s.anInt2836 - 1) >> var_s.anInt2835;
				if (is[i_764_][i_766_] == i_762_ && is[i_765_][i_766_] == i_762_ && is[i_764_][i_767_] == i_762_
						&& is[i_765_][i_767_] == i_762_)
					return;
			}
			synchronized (this) {
				if (i == 1) {
					int i_769_ = var_s.anInt2836 - 1;
					for (int i_770_ = 0; i_770_ < anInt4356; i_770_++) {
						int i_771_ = anIntArray4311[i_770_] + i_761_;
						int i_772_ = anIntArray4375[i_770_] + i_763_;
						int i_773_ = i_771_ & i_769_;
						int i_774_ = i_772_ & i_769_;
						int i_775_ = i_771_ >> var_s.anInt2835;
						int i_776_ = i_772_ >> var_s.anInt2835;
						int i_777_ = ((is[i_775_][i_776_] * (var_s.anInt2836 - i_773_)
								+ is[i_775_ + 1][i_776_] * i_773_) >> var_s.anInt2835);
						int i_778_ = ((is[i_775_][i_776_ + 1] * (var_s.anInt2836 - i_773_)
								+ is[i_775_ + 1][i_776_ + 1] * i_773_) >> var_s.anInt2835);
						int i_779_ = ((i_777_ * (var_s.anInt2836 - i_774_) + i_778_ * i_774_) >> var_s.anInt2835);
						anIntArray4342[i_770_] = anIntArray4342[i_770_] + i_779_ - i_762_;
					}
					for (int i_780_ = anInt4356; i_780_ < anInt4373; i_780_++) {
						int i_781_ = anIntArray4311[i_780_] + i_761_;
						int i_782_ = anIntArray4375[i_780_] + i_763_;
						int i_783_ = i_781_ & i_769_;
						int i_784_ = i_782_ & i_769_;
						int i_785_ = i_781_ >> var_s.anInt2835;
						int i_786_ = i_782_ >> var_s.anInt2835;
						if (i_785_ >= 0 && i_785_ < is.length - 1 && i_786_ >= 0 && i_786_ < is[0].length - 1) {
							int i_787_ = ((is[i_785_][i_786_] * (var_s.anInt2836 - i_783_)
									+ is[i_785_ + 1][i_786_] * i_783_) >> var_s.anInt2835);
							int i_788_ = ((is[i_785_][i_786_ + 1] * (var_s.anInt2836 - i_783_)
									+ is[i_785_ + 1][i_786_ + 1] * i_783_) >> var_s.anInt2835);
							int i_789_ = ((i_787_ * (var_s.anInt2836 - i_784_) + i_788_ * i_784_) >> var_s.anInt2835);
							anIntArray4342[i_780_] = anIntArray4342[i_780_] + i_789_ - i_762_;
						}
					}
				} else if (i == 2) {
					int i_790_ = var_s.anInt2836 - 1;
					for (int i_791_ = 0; i_791_ < anInt4356; i_791_++) {
						int i_792_ = (anIntArray4342[i_791_] << 16) / aShort4381;
						if (i_792_ < i_759_) {
							int i_793_ = anIntArray4311[i_791_] + i_761_;
							int i_794_ = anIntArray4375[i_791_] + i_763_;
							int i_795_ = i_793_ & i_790_;
							int i_796_ = i_794_ & i_790_;
							int i_797_ = i_793_ >> var_s.anInt2835;
							int i_798_ = i_794_ >> var_s.anInt2835;
							int i_799_ = ((is[i_797_][i_798_] * (var_s.anInt2836 - i_795_)
									+ is[i_797_ + 1][i_798_] * i_795_) >> var_s.anInt2835);
							int i_800_ = ((is[i_797_][i_798_ + 1] * (var_s.anInt2836 - i_795_)
									+ is[i_797_ + 1][i_798_ + 1] * i_795_) >> var_s.anInt2835);
							int i_801_ = ((i_799_ * (var_s.anInt2836 - i_796_) + i_800_ * i_796_) >> var_s.anInt2835);
							anIntArray4342[i_791_] = anIntArray4342[i_791_]
									+ ((i_801_ - i_762_) * (i_759_ - i_792_) / i_759_);
						} else
							anIntArray4342[i_791_] = anIntArray4342[i_791_];
					}
					for (int i_802_ = anInt4356; i_802_ < anInt4373; i_802_++) {
						int i_803_ = (anIntArray4342[i_802_] << 16) / aShort4381;
						if (i_803_ < i_759_) {
							int i_804_ = anIntArray4311[i_802_] + i_761_;
							int i_805_ = anIntArray4375[i_802_] + i_763_;
							int i_806_ = i_804_ & i_790_;
							int i_807_ = i_805_ & i_790_;
							int i_808_ = i_804_ >> var_s.anInt2835;
							int i_809_ = i_805_ >> var_s.anInt2835;
							if (i_808_ >= 0 && i_808_ < var_s.anInt2832 - 1 && i_809_ >= 0
									&& i_809_ < var_s.anInt2834 - 1) {
								int i_810_ = ((is[i_808_][i_809_] * (var_s.anInt2836 - i_806_)
										+ is[i_808_ + 1][i_809_] * i_806_) >> var_s.anInt2835);
								int i_811_ = (((is[i_808_][i_809_ + 1] * (var_s.anInt2836 - i_806_))
										+ is[i_808_ + 1][i_809_ + 1] * i_806_) >> var_s.anInt2835);
								int i_812_ = ((i_810_ * (var_s.anInt2836 - i_807_)
										+ i_811_ * i_807_) >> var_s.anInt2835);
								anIntArray4342[i_802_] = (anIntArray4342[i_802_]
										+ ((i_812_ - i_762_) * (i_759_ - i_803_) / i_759_));
							}
						} else
							anIntArray4342[i_802_] = anIntArray4342[i_802_];
					}
				} else if (i == 3) {
					int i_813_ = (i_759_ & 0xff) * 4;
					int i_814_ = (i_759_ >> 8 & 0xff) * 4;
					int i_815_ = (i_759_ >> 16 & 0xff) << 6;
					int i_816_ = (i_759_ >> 24 & 0xff) << 6;
					if (i_761_ - (i_813_ >> 1) < 0
							|| (i_761_ + (i_813_ >> 1) + var_s.anInt2836 >= var_s.anInt2832 << var_s.anInt2835)
							|| i_763_ - (i_814_ >> 1) < 0
							|| (i_763_ + (i_814_ >> 1) + var_s.anInt2836 >= var_s.anInt2834 << var_s.anInt2835))
						return;
					this.method1734(i_813_, var_s, i_762_, i_815_, i_761_, i_816_, i_763_, i_814_, 65535);
				} else if (i == 4) {
					int i_817_ = var_s_760_.anInt2836 - 1;
					int i_818_ = aShort4377 - aShort4381;
					for (int i_819_ = 0; i_819_ < anInt4356; i_819_++) {
						int i_820_ = anIntArray4311[i_819_] + i_761_;
						int i_821_ = anIntArray4375[i_819_] + i_763_;
						int i_822_ = i_820_ & i_817_;
						int i_823_ = i_821_ & i_817_;
						int i_824_ = i_820_ >> var_s_760_.anInt2835;
						int i_825_ = i_821_ >> var_s_760_.anInt2835;
						int i_826_ = ((is_768_[i_824_][i_825_] * (var_s_760_.anInt2836 - i_822_)
								+ is_768_[i_824_ + 1][i_825_] * i_822_) >> var_s_760_.anInt2835);
						int i_827_ = (((is_768_[i_824_][i_825_ + 1] * (var_s_760_.anInt2836 - i_822_))
								+ is_768_[i_824_ + 1][i_825_ + 1] * i_822_) >> var_s_760_.anInt2835);
						int i_828_ = ((i_826_ * (var_s_760_.anInt2836 - i_823_)
								+ i_827_ * i_823_) >> var_s_760_.anInt2835);
						anIntArray4342[i_819_] = (anIntArray4342[i_819_] + (i_828_ - i_762_) + i_818_);
					}
					for (int i_829_ = anInt4356; i_829_ < anInt4373; i_829_++) {
						int i_830_ = anIntArray4311[i_829_] + i_761_;
						int i_831_ = anIntArray4375[i_829_] + i_763_;
						int i_832_ = i_830_ & i_817_;
						int i_833_ = i_831_ & i_817_;
						int i_834_ = i_830_ >> var_s_760_.anInt2835;
						int i_835_ = i_831_ >> var_s_760_.anInt2835;
						if (i_834_ >= 0 && i_834_ < var_s_760_.anInt2832 - 1 && i_835_ >= 0
								&& i_835_ < var_s_760_.anInt2834 - 1) {
							int i_836_ = (((is_768_[i_834_][i_835_] * (var_s_760_.anInt2836 - i_832_))
									+ is_768_[i_834_ + 1][i_835_] * i_832_) >> var_s_760_.anInt2835);
							int i_837_ = (((is_768_[i_834_][i_835_ + 1] * (var_s_760_.anInt2836 - i_832_))
									+ is_768_[i_834_ + 1][i_835_ + 1] * i_832_) >> var_s_760_.anInt2835);
							int i_838_ = (i_836_ * (var_s_760_.anInt2836 - i_833_)
									+ i_837_ * i_833_ >> var_s_760_.anInt2835);
							anIntArray4342[i_829_] = (anIntArray4342[i_829_] + (i_838_ - i_762_) + i_818_);
						}
					}
				} else if (i == 5) {
					int i_839_ = var_s_760_.anInt2836 - 1;
					int i_840_ = aShort4377 - aShort4381;
					for (int i_841_ = 0; i_841_ < anInt4356; i_841_++) {
						int i_842_ = anIntArray4311[i_841_] + i_761_;
						int i_843_ = anIntArray4375[i_841_] + i_763_;
						int i_844_ = i_842_ & i_839_;
						int i_845_ = i_843_ & i_839_;
						int i_846_ = i_842_ >> var_s.anInt2835;
						int i_847_ = i_843_ >> var_s.anInt2835;
						int i_848_ = ((is[i_846_][i_847_] * (var_s.anInt2836 - i_844_)
								+ is[i_846_ + 1][i_847_] * i_844_) >> var_s.anInt2835);
						int i_849_ = ((is[i_846_][i_847_ + 1] * (var_s.anInt2836 - i_844_)
								+ is[i_846_ + 1][i_847_ + 1] * i_844_) >> var_s.anInt2835);
						int i_850_ = ((i_848_ * (var_s.anInt2836 - i_845_) + i_849_ * i_845_) >> var_s.anInt2835);
						i_848_ = ((is_768_[i_846_][i_847_] * (var_s_760_.anInt2836 - i_844_)
								+ is_768_[i_846_ + 1][i_847_] * i_844_) >> var_s_760_.anInt2835);
						i_849_ = (((is_768_[i_846_][i_847_ + 1] * (var_s_760_.anInt2836 - i_844_))
								+ is_768_[i_846_ + 1][i_847_ + 1] * i_844_) >> var_s_760_.anInt2835);
						int i_851_ = ((i_848_ * (var_s_760_.anInt2836 - i_845_)
								+ i_849_ * i_845_) >> var_s_760_.anInt2835);
						int i_852_ = i_850_ - i_851_ - i_759_;
						anIntArray4342[i_841_] = ((anIntArray4342[i_841_] << 8) / i_840_ * i_852_ >> 8)
								- (i_762_ - i_850_);
					}
					for (int i_853_ = anInt4356; i_853_ < anInt4373; i_853_++) {
						int i_854_ = anIntArray4311[i_853_] + i_761_;
						int i_855_ = anIntArray4375[i_853_] + i_763_;
						int i_856_ = i_854_ & i_839_;
						int i_857_ = i_855_ & i_839_;
						int i_858_ = i_854_ >> var_s.anInt2835;
						int i_859_ = i_855_ >> var_s.anInt2835;
						if (i_858_ >= 0 && i_858_ < var_s.anInt2832 - 1 && i_858_ < var_s_760_.anInt2832 - 1
								&& i_859_ >= 0 && i_859_ < var_s.anInt2834 - 1 && i_859_ < var_s_760_.anInt2834 - 1) {
							int i_860_ = ((is[i_858_][i_859_] * (var_s.anInt2836 - i_856_)
									+ is[i_858_ + 1][i_859_] * i_856_) >> var_s.anInt2835);
							int i_861_ = ((is[i_858_][i_859_ + 1] * (var_s.anInt2836 - i_856_)
									+ is[i_858_ + 1][i_859_ + 1] * i_856_) >> var_s.anInt2835);
							int i_862_ = ((i_860_ * (var_s.anInt2836 - i_857_) + i_861_ * i_857_) >> var_s.anInt2835);
							i_860_ = (((is_768_[i_858_][i_859_] * (var_s_760_.anInt2836 - i_856_))
									+ is_768_[i_858_ + 1][i_859_] * i_856_) >> var_s_760_.anInt2835);
							i_861_ = (((is_768_[i_858_][i_859_ + 1] * (var_s_760_.anInt2836 - i_856_))
									+ is_768_[i_858_ + 1][i_859_ + 1] * i_856_) >> var_s_760_.anInt2835);
							int i_863_ = (i_860_ * (var_s_760_.anInt2836 - i_857_)
									+ i_861_ * i_857_ >> var_s_760_.anInt2835);
							int i_864_ = i_862_ - i_863_ - i_759_;
							anIntArray4342[i_853_] = (((anIntArray4342[i_853_] << 8) / i_840_ * i_864_) >> 8)
									- (i_762_ - i_862_);
						}
					}
				}
				aBoolean4362 = false;
			}
		}
	}

	private void method1778() {
		synchronized (this) {
			for (int i = 0; i < anInt4356; i++) {
				anIntArray4311[i] = -anIntArray4311[i];
				anIntArray4375[i] = -anIntArray4375[i];
				if (aClass339Array4325[i] != null) {
					aClass339Array4325[i].anInt2974 = -aClass339Array4325[i].anInt2974;
					aClass339Array4325[i].anInt2973 = -aClass339Array4325[i].anInt2973;
				}
			}
			if (aClass364Array4339 != null) {
				for (int i = 0; i < anInt4361; i++) {
					if (aClass364Array4339[i] != null) {
						aClass364Array4339[i].anInt3112 = -aClass364Array4339[i].anInt3112;
						aClass364Array4339[i].anInt3113 = -aClass364Array4339[i].anInt3113;
					}
				}
			}
			for (int i = anInt4356; i < anInt4373; i++) {
				anIntArray4311[i] = -anIntArray4311[i];
				anIntArray4375[i] = -anIntArray4375[i];
			}
			anInt4357 = 0;
			aBoolean4362 = false;
		}
	}

	public EmissiveTriangle[] method1735() {
		return aClass89Array4374;
	}

	public int na() {
		if (!aBoolean4362)
			method1759();
		return aShort4344;
	}

	public void wa() {
		if (aBoolean4315) {
			for (int i = 0; i < anInt4373; i++) {
				anIntArray4311[i] = anIntArray4311[i] + 7 >> 4;
				anIntArray4342[i] = anIntArray4342[i] + 7 >> 4;
				anIntArray4375[i] = anIntArray4375[i] + 7 >> 4;
			}
			aBoolean4315 = false;
		}
		if (aBoolean4382) {
			method1758();
			aBoolean4382 = false;
		}
		aBoolean4362 = false;
	}

	public boolean method1737() {
		if (aShortArray4350 == null)
			return true;
		for (int i = 0; i < aShortArray4350.length; i++) {
			if (aShortArray4350[i] != -1 && !aHa_Sub2_4366.method1267(aShortArray4350[i]))
				return false;
		}
		return true;
	}

	public void method1726(Class373 class373, int i, boolean bool) {
		if (aShortArray4345 != null) {
			int[] is = new int[3];
			for (int i_865_ = 0; i_865_ < anInt4356; i_865_++) {
				if ((i & aShortArray4345[i_865_]) != 0) {
					if (bool)
						class373.method3905(anIntArray4311[i_865_], anIntArray4342[i_865_], anIntArray4375[i_865_], is);
					else
						class373.method3903(anIntArray4311[i_865_], anIntArray4342[i_865_], anIntArray4375[i_865_], is);
					anIntArray4311[i_865_] = is[0];
					anIntArray4342[i_865_] = is[1];
					anIntArray4375[i_865_] = is[2];
				}
			}
		}
	}

	private boolean method1779(int i, int i_866_, int i_867_, int i_868_, int i_869_, int i_870_, int i_871_,
			int i_872_) {
		if (i_866_ < i_867_ && i_866_ < i_868_ && i_866_ < i_869_)
			return false;
		if (i_866_ > i_867_ && i_866_ > i_868_ && i_866_ > i_869_)
			return false;
		if (i < i_870_ && i < i_871_ && i < i_872_)
			return false;
		if (i > i_870_ && i > i_871_ && i > i_872_)
			return false;
		return true;
	}

	public int G() {
		if (!aBoolean4362)
			method1759();
		return aShort4346;
	}

	public void a(int i) {
		if ((anInt4326 & 0x5) != 5)
			throw new IllegalStateException();
		if (i == 4096)
			method1773();
		else if (i == 8192)
			method1762();
		else if (i == 12288)
			method1774();
		else {
			int i_873_ = Class296_Sub4.anIntArray4598[i];
			int i_874_ = Class296_Sub4.anIntArray4618[i];
			synchronized (this) {
				for (int i_875_ = 0; i_875_ < anInt4373; i_875_++) {
					int i_876_ = ((anIntArray4375[i_875_] * i_873_ + anIntArray4311[i_875_] * i_874_) >> 14);
					anIntArray4375[i_875_] = (anIntArray4375[i_875_] * i_874_ - anIntArray4311[i_875_] * i_873_) >> 14;
					anIntArray4311[i_875_] = i_876_;
				}
				method1777();
			}
		}
	}

	Class178_Sub1(ha_Sub2 var_ha_Sub2) {
		anInt4361 = 0;
		anInt4357 = 0;
		aBoolean4354 = false;
		aBoolean4315 = false;
		aBoolean4340 = false;
		anInt4373 = 0;
		aBoolean4362 = false;
		aHa_Sub2_4366 = var_ha_Sub2;
	}

	Class178_Sub1(ha_Sub2 var_ha_Sub2, Mesh class132, int i, int i_877_, int i_878_, int i_879_) {
		anInt4361 = 0;
		anInt4357 = 0;
		aBoolean4354 = false;
		aBoolean4315 = false;
		aBoolean4340 = false;
		anInt4373 = 0;
		aBoolean4362 = false;
		aHa_Sub2_4366 = var_ha_Sub2;
		anInt4326 = i;
		anInt4353 = i_877_;
		anInt4371 = i_878_;
		d var_d = aHa_Sub2_4366.aD1299;
		anInt4373 = class132.num_vertices;
		anInt4356 = class132.highest_face_index;
		anIntArray4311 = class132.vertices_x;
		anIntArray4342 = class132.vertices_y;
		anIntArray4375 = class132.vertices_z;
		anInt4361 = class132.num_faces;
		aShortArray4387 = class132.faces_a;
		aShortArray4324 = class132.faces_b;
		aShortArray4327 = class132.faceS_c;
		aByteArray4334 = class132.faces_priority;
		aShortArray4341 = class132.faces_colour;
		aByteArray4328 = class132.faces_alpha;
		aShortArray4363 = class132.aShortArray1352;
		aByteArray4386 = class132.aByteArray1354;
		aClass89Array4374 = class132.emitters;
		aClass232Array4314 = class132.effectors;
		aShortArray4345 = class132.aShortArray1366;
		int[] is = new int[anInt4361];
		for (int i_880_ = 0; i_880_ < anInt4361; i_880_++)
			is[i_880_] = i_880_;
		long[] ls = new long[anInt4361];
		boolean bool = (anInt4326 & 0x100) != 0;
		for (int i_881_ = 0; i_881_ < anInt4361; i_881_++) {
			int i_882_ = is[i_881_];
			MaterialRaw class170 = null;
			int i_883_ = 0;
			int i_884_ = 0;
			int i_885_ = 0;
			int i_886_ = 0;
			if (class132.billboards != null) {
				boolean bool_887_ = false;
				for (int i_888_ = 0; i_888_ < class132.billboards.length; i_888_++) {
					Billboard class385 = class132.billboards[i_888_];
					if (i_882_ == class385.face) {
						BillboardRaw class101 = Class296_Sub51_Sub4.method3086((byte) 32, class385.id);
						if (class101.aBoolean1077)
							bool_887_ = true;
						if (class101.texture_id != -1) {
							MaterialRaw class170_889_ = var_d.method14(class101.texture_id, -9412);
							if (class170_889_.anInt1788 == 2)
								aBoolean4354 = true;
						}
					}
				}
				if (bool_887_)
					ls[i_881_] = 9223372036854775807L;
			}
			int i_890_ = -1;
			if (class132.faces_material != null) {
				i_890_ = class132.faces_material[i_882_];
				if (i_890_ != -1) {
					class170 = var_d.method14(i_890_ & 0xffff, -9412);
					if ((i_879_ & 0x40) == 0 || !class170.aBoolean1792) {
						i_885_ = class170.aByte1774;
						i_886_ = class170.aByte1784;
					} else
						i_890_ = -1;
				}
			}
			boolean bool_891_ = (aByteArray4328 != null && aByteArray4328[i_882_] != 0
					|| class170 != null && class170.anInt1788 == 2);
			if ((bool || bool_891_) && aByteArray4334 != null)
				i_883_ += aByteArray4334[i_882_] << 17;
			if (bool_891_)
				i_883_ += 65536;
			i_883_ += (i_885_ & 0xff) << 8;
			i_883_ += i_886_ & 0xff;
			i_884_ += (i_890_ & 0xffff) << 16;
			i_884_ += i_881_ & 0xffff;
			ls[i_881_] = ((long) i_883_ << 32) + (long) i_884_;
			aBoolean4354 |= bool_891_;
		}
		Connection.method1965(ls, -19851, is);
		if (class132.billboards != null) {
			anInt4359 = class132.billboards.length;
			aClass316Array4313 = new Class316[anInt4359];
			aClass128Array4316 = new Class128[anInt4359];
			for (int i_892_ = 0; i_892_ < class132.billboards.length; i_892_++) {
				Billboard class385 = class132.billboards[i_892_];
				BillboardRaw class101 = Class296_Sub51_Sub4.method3086((byte) -109, class385.id);
				int i_893_ = ((Class295_Sub1.anIntArray3691[(class132.faces_colour[class385.face] & 0xffff)])
						& 0xffffff);
				i_893_ = i_893_
						| 255 - (class132.faces_alpha != null ? (class132.faces_alpha[class385.face]) & 0xff : 0) << 24;
				aClass316Array4313[i_892_] = new Class316(class385.face, (class132.faces_a[class385.face]),
						(class132.faces_b[class385.face]), (class132.faceS_c[class385.face]), class101.anInt1072,
						class101.anInt1073, class101.texture_id, class101.anInt1079, class101.anInt1076,
						class101.aBoolean1077, class385.anInt3257);
				aClass128Array4316[i_892_] = new Class128(i_893_);
			}
		}
		aFloatArrayArray4384 = new float[anInt4361][];
		aFloatArrayArray4376 = new float[anInt4361][];
		Class221 class221 = Class41_Sub23.method490(-741, is, anInt4361, class132);
		Class240 class240 = aHa_Sub2_4366.method1270(Thread.currentThread());
		float[] fs = class240.aFloatArray2279;
		boolean bool_894_ = false;
		for (int i_895_ = 0; i_895_ < anInt4361; i_895_++) {
			int i_896_ = is[i_895_];
			int i_897_;
			if (class132.faces_texture == null)
				i_897_ = -1;
			else
				i_897_ = class132.faces_texture[i_896_];
			int i_898_ = (class132.faces_material == null ? (int) -1 : class132.faces_material[i_896_]);
			if (i_898_ != -1 && (i_879_ & 0x40) != 0) {
				MaterialRaw class170 = var_d.method14(i_898_ & 0xffff, -9412);
				if (class170.aBoolean1792)
					i_898_ = -1;
			}
			if (i_898_ != -1) {
				bool_894_ = true;
				float[] fs_899_ = aFloatArrayArray4384[i_896_] = new float[3];
				float[] fs_900_ = aFloatArrayArray4376[i_896_] = new float[3];
				boolean bool_901_ = false;
				if (i_897_ == -1) {
					fs_899_[0] = 0.0F;
					fs_900_[0] = 1.0F;
					fs_899_[1] = 1.0F;
					fs_900_[1] = 1.0F;
					fs_899_[2] = 0.0F;
					fs_900_[2] = 0.0F;
				} else {
					i_897_ &= 0xff;
					byte i_902_ = class132.textures_mapping_type[i_897_];
					if (i_902_ == 0) {
						short i_903_ = aShortArray4387[i_896_];
						short i_904_ = aShortArray4324[i_896_];
						short i_905_ = aShortArray4327[i_896_];
						short i_906_ = class132.textures_mapping_p[i_897_];
						short i_907_ = class132.textures_mapping_m[i_897_];
						short i_908_ = class132.textures_mapping_n[i_897_];
						float f = (float) anIntArray4311[i_906_];
						float f_909_ = (float) anIntArray4342[i_906_];
						float f_910_ = (float) anIntArray4375[i_906_];
						float f_911_ = (float) anIntArray4311[i_907_] - f;
						float f_912_ = (float) anIntArray4342[i_907_] - f_909_;
						float f_913_ = (float) anIntArray4375[i_907_] - f_910_;
						float f_914_ = (float) anIntArray4311[i_908_] - f;
						float f_915_ = (float) anIntArray4342[i_908_] - f_909_;
						float f_916_ = (float) anIntArray4375[i_908_] - f_910_;
						float f_917_ = (float) anIntArray4311[i_903_] - f;
						float f_918_ = (float) anIntArray4342[i_903_] - f_909_;
						float f_919_ = (float) anIntArray4375[i_903_] - f_910_;
						float f_920_ = (float) anIntArray4311[i_904_] - f;
						float f_921_ = (float) anIntArray4342[i_904_] - f_909_;
						float f_922_ = (float) anIntArray4375[i_904_] - f_910_;
						float f_923_ = (float) anIntArray4311[i_905_] - f;
						float f_924_ = (float) anIntArray4342[i_905_] - f_909_;
						float f_925_ = (float) anIntArray4375[i_905_] - f_910_;
						float f_926_ = f_912_ * f_916_ - f_913_ * f_915_;
						float f_927_ = f_913_ * f_914_ - f_911_ * f_916_;
						float f_928_ = f_911_ * f_915_ - f_912_ * f_914_;
						float f_929_ = f_915_ * f_928_ - f_916_ * f_927_;
						float f_930_ = f_916_ * f_926_ - f_914_ * f_928_;
						float f_931_ = f_914_ * f_927_ - f_915_ * f_926_;
						float f_932_ = 1.0F / (f_929_ * f_911_ + f_930_ * f_912_ + f_931_ * f_913_);
						fs_899_[0] = (f_929_ * f_917_ + f_930_ * f_918_ + f_931_ * f_919_) * f_932_;
						fs_899_[1] = (f_929_ * f_920_ + f_930_ * f_921_ + f_931_ * f_922_) * f_932_;
						fs_899_[2] = (f_929_ * f_923_ + f_930_ * f_924_ + f_931_ * f_925_) * f_932_;
						f_929_ = f_912_ * f_928_ - f_913_ * f_927_;
						f_930_ = f_913_ * f_926_ - f_911_ * f_928_;
						f_931_ = f_911_ * f_927_ - f_912_ * f_926_;
						f_932_ = 1.0F / (f_929_ * f_914_ + f_930_ * f_915_ + f_931_ * f_916_);
						fs_900_[0] = (f_929_ * f_917_ + f_930_ * f_918_ + f_931_ * f_919_) * f_932_;
						fs_900_[1] = (f_929_ * f_920_ + f_930_ * f_921_ + f_931_ * f_922_) * f_932_;
						fs_900_[2] = (f_929_ * f_923_ + f_930_ * f_924_ + f_931_ * f_925_) * f_932_;
					} else {
						short i_933_ = aShortArray4387[i_896_];
						short i_934_ = aShortArray4324[i_896_];
						short i_935_ = aShortArray4327[i_896_];
						int i_936_ = class221.anIntArray2153[i_897_];
						int i_937_ = class221.anIntArray2157[i_897_];
						int i_938_ = class221.anIntArray2154[i_897_];
						float[] fs_939_ = class221.aFloatArrayArray2156[i_897_];
						byte i_940_ = class132.aByteArray1367[i_897_];
						float f = (float) class132.anIntArray1371[i_897_] / 256.0F;
						if (i_902_ == 1) {
							float f_941_ = ((float) class132.anIntArray1379[i_897_] / 1024.0F);
							Class215.method2027(anIntArray4311[i_933_], fs_939_, i_936_, f_941_, anIntArray4342[i_933_],
									i_938_, -3, f, fs, anIntArray4375[i_933_], i_940_, i_937_);
							fs_899_[0] = fs[0];
							fs_900_[0] = fs[1];
							Class215.method2027(anIntArray4311[i_934_], fs_939_, i_936_, f_941_, anIntArray4342[i_934_],
									i_938_, -3, f, fs, anIntArray4375[i_934_], i_940_, i_937_);
							fs_899_[1] = fs[0];
							fs_900_[1] = fs[1];
							Class215.method2027(anIntArray4311[i_935_], fs_939_, i_936_, f_941_, anIntArray4342[i_935_],
									i_938_, -3, f, fs, anIntArray4375[i_935_], i_940_, i_937_);
							fs_899_[2] = fs[0];
							fs_900_[2] = fs[1];
							float f_942_ = f_941_ / 2.0F;
							if ((i_940_ & 0x1) == 0) {
								if (fs_899_[1] - fs_899_[0] > f_942_)
									fs_899_[1] -= f_941_;
								else if (fs_899_[0] - fs_899_[1] > f_942_)
									fs_899_[1] += f_941_;
								if (fs_899_[2] - fs_899_[0] > f_942_)
									fs_899_[2] -= f_941_;
								else if (fs_899_[0] - fs_899_[2] > f_942_)
									fs_899_[2] += f_941_;
							} else {
								if (fs_900_[1] - fs_900_[0] > f_942_)
									fs_900_[1] -= f_941_;
								else if (fs_900_[0] - fs_900_[1] > f_942_)
									fs_900_[1] += f_941_;
								if (fs_900_[2] - fs_900_[0] > f_942_)
									fs_900_[2] -= f_941_;
								else if (fs_900_[0] - fs_900_[2] > f_942_)
									fs_900_[2] += f_941_;
							}
						} else if (i_902_ == 2) {
							float f_943_ = ((float) class132.anIntArray1347[i_897_] / 256.0F);
							float f_944_ = ((float) class132.anIntArray1348[i_897_] / 256.0F);
							int i_945_ = (anIntArray4311[i_934_] - anIntArray4311[i_933_]);
							int i_946_ = (anIntArray4342[i_934_] - anIntArray4342[i_933_]);
							int i_947_ = (anIntArray4375[i_934_] - anIntArray4375[i_933_]);
							int i_948_ = (anIntArray4311[i_935_] - anIntArray4311[i_933_]);
							int i_949_ = (anIntArray4342[i_935_] - anIntArray4342[i_933_]);
							int i_950_ = (anIntArray4375[i_935_] - anIntArray4375[i_933_]);
							int i_951_ = i_946_ * i_950_ - i_949_ * i_947_;
							int i_952_ = i_947_ * i_948_ - i_950_ * i_945_;
							int i_953_ = i_945_ * i_949_ - i_948_ * i_946_;
							float f_954_ = (64.0F / (float) class132.anIntArray1344[i_897_]);
							float f_955_ = (64.0F / (float) class132.anIntArray1365[i_897_]);
							float f_956_ = (64.0F / (float) class132.anIntArray1379[i_897_]);
							float f_957_ = (((float) i_951_ * fs_939_[0] + (float) i_952_ * fs_939_[1]
									+ (float) i_953_ * fs_939_[2]) / f_954_);
							float f_958_ = (((float) i_951_ * fs_939_[3] + (float) i_952_ * fs_939_[4]
									+ (float) i_953_ * fs_939_[5]) / f_955_);
							float f_959_ = (((float) i_951_ * fs_939_[6] + (float) i_952_ * fs_939_[7]
									+ (float) i_953_ * fs_939_[8]) / f_956_);
							int i_960_ = ConfigurationsLoader.method184(f_958_, (byte) 83, f_957_, f_959_);
							Class245.method2185(i_940_, fs, i_960_, anIntArray4342[i_933_], i_937_, f_944_, f_943_, 87,
									i_936_, f, i_938_, anIntArray4375[i_933_], fs_939_, anIntArray4311[i_933_]);
							fs_899_[0] = fs[0];
							fs_900_[0] = fs[1];
							Class245.method2185(i_940_, fs, i_960_, anIntArray4342[i_934_], i_937_, f_944_, f_943_, 119,
									i_936_, f, i_938_, anIntArray4375[i_934_], fs_939_, anIntArray4311[i_934_]);
							fs_899_[1] = fs[0];
							fs_900_[1] = fs[1];
							Class245.method2185(i_940_, fs, i_960_, anIntArray4342[i_935_], i_937_, f_944_, f_943_, 124,
									i_936_, f, i_938_, anIntArray4375[i_935_], fs_939_, anIntArray4311[i_935_]);
							fs_899_[2] = fs[0];
							fs_900_[2] = fs[1];
						} else if (i_902_ == 3) {
							Class296_Sub51_Sub39.method3201(i_940_, i_936_, fs_939_, anIntArray4311[i_933_], -90, fs,
									anIntArray4342[i_933_], i_937_, f, i_938_, anIntArray4375[i_933_]);
							fs_899_[0] = fs[0];
							fs_900_[0] = fs[1];
							Class296_Sub51_Sub39.method3201(i_940_, i_936_, fs_939_, anIntArray4311[i_934_], 108, fs,
									anIntArray4342[i_934_], i_937_, f, i_938_, anIntArray4375[i_934_]);
							fs_899_[1] = fs[0];
							fs_900_[1] = fs[1];
							Class296_Sub51_Sub39.method3201(i_940_, i_936_, fs_939_, anIntArray4311[i_935_], 22, fs,
									anIntArray4342[i_935_], i_937_, f, i_938_, anIntArray4375[i_935_]);
							fs_899_[2] = fs[0];
							fs_900_[2] = fs[1];
							if ((i_940_ & 0x1) == 0) {
								if (fs_899_[1] - fs_899_[0] > 0.5F)
									fs_899_[1]--;
								else if (fs_899_[0] - fs_899_[1] > 0.5F)
									fs_899_[1]++;
								if (fs_899_[2] - fs_899_[0] > 0.5F)
									fs_899_[2]--;
								else if (fs_899_[0] - fs_899_[2] > 0.5F)
									fs_899_[2]++;
							} else {
								if (fs_900_[1] - fs_900_[0] > 0.5F)
									fs_900_[1]--;
								else if (fs_900_[0] - fs_900_[1] > 0.5F)
									fs_900_[1]++;
								if (fs_900_[2] - fs_900_[0] > 0.5F)
									fs_900_[2]--;
								else if (fs_900_[0] - fs_900_[2] > 0.5F)
									fs_900_[2]++;
							}
						}
					}
				}
			}
		}
		if (!bool_894_)
			aFloatArrayArray4384 = aFloatArrayArray4376 = null;
		if (class132.vertices_label != null && (anInt4326 & 0x20) != 0)
			anIntArrayArray4367 = class132.method1385((byte) 111, true);
		if (class132.faces_label != null && (anInt4326 & 0x180) != 0)
			anIntArrayArray4368 = class132.method1384(false);
		if (class132.billboards != null && (anInt4326 & 0x400) != 0)
			anIntArrayArray4333 = class132.method1390((byte) 104);
		if (class132.faces_material != null) {
			aShortArray4350 = new short[anInt4361];
			boolean bool_961_ = false;
			for (int i_962_ = 0; i_962_ < anInt4361; i_962_++) {
				short i_963_ = class132.faces_material[i_962_];
				if (i_963_ != -1) {
					MaterialRaw class170 = aHa_Sub2_4366.aD1299.method14(i_963_, -9412);
					if ((i_879_ & 0x40) == 0 || !class170.aBoolean1792) {
						aShortArray4350[i_962_] = i_963_;
						bool_961_ = true;
						if (class170.anInt1788 == 2)
							aBoolean4354 = true;
						if (class170.speed_u != 0 || class170.speed_v != 0)
							aBoolean4340 = true;
					} else
						aShortArray4350[i_962_] = (short) -1;
				} else
					aShortArray4350[i_962_] = (short) -1;
			}
			if (!bool_961_)
				aShortArray4350 = null;
		} else
			aShortArray4350 = null;
		if (aBoolean4354 || aClass316Array4313 != null) {
			aShortArray4385 = new short[anInt4361];
			for (int i_964_ = 0; i_964_ < anInt4361; i_964_++)
				aShortArray4385[i_964_] = (short) is[i_964_];
		}
	}

	static {
		anInt4378 = 4096;
	}
}
