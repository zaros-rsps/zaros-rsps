package net.zaros.client;
/* Class29 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;
import java.io.OutputStream;

final class Class29 implements Runnable {
	private int anInt304;
	static Class324[] aClass324Array305;
	private IOException anIOException306;
	public static int anInt307 = 0;
	private int anInt308;
	private Thread aThread309;
	private int anInt310 = 0;
	private OutputStream anOutputStream311;
	private byte[] aByteArray312;

	final void method319(byte i) {
		if (i != 83)
			anInt307 = -57;
		anOutputStream311 = new OutputStream_Sub1();
	}

	final void method320(int i, int i_0_, int i_1_, byte[] is) throws IOException {
		if (i_0_ < (i_1_ ^ 0xffffffff) || i < 0 || i_1_ + i > is.length)
			throw new IOException();
		synchronized (this) {
			if (anIOException306 != null)
				throw new IOException(anIOException306.toString());
			int i_2_;
			if (anInt310 > anInt304)
				i_2_ = -anInt304 - 1 + anInt310;
			else
				i_2_ = anInt310 - 1 + (anInt308 - anInt304);
			if (i_2_ < i_1_)
				throw new IOException("");
			if (anInt308 >= i_1_ + anInt304)
				ArrayTools.removeElement(is, i, aByteArray312, anInt304, i_1_);
			else {
				int i_3_ = anInt308 - anInt304;
				ArrayTools.removeElement(is, i, aByteArray312, anInt304, i_3_);
				ArrayTools.removeElement(is, i_3_ + i, aByteArray312, 0, -i_3_ + i_1_);
			}
			anInt304 = (i_1_ + anInt304) % anInt308;
			this.notifyAll();
		}
	}

	final void method321(int i) {
		if (i != -9359)
			method319((byte) 53);
		synchronized (this) {
			if (anIOException306 == null)
				anIOException306 = new IOException("");
			this.notifyAll();
		}
		try {
			aThread309.join();
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
	}

	static final void method322(int i, int i_4_, int i_5_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i | (long) i_4_ << 32, 18);
		class296_sub39_sub5.insertIntoQueue();
		if (i_5_ != 0)
			method322(82, 18, -35);
	}

	public final void run() {
		for (;;) {
			int i;
			synchronized (this) {
				for (;;) {
					if (anIOException306 != null)
						return;
					if (anInt310 > anInt304)
						i = anInt308 - anInt310 + anInt304;
					else
						i = anInt304 - anInt310;
					if (i > 0)
						break;
					try {
						anOutputStream311.flush();
					} catch (IOException ioexception) {
						anIOException306 = ioexception;
						return;
					}
					try {
						this.wait();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
			}
			try {
				if (anInt308 >= anInt310 + i)
					anOutputStream311.write(aByteArray312, anInt310, i);
				else {
					int i_6_ = anInt308 - anInt310;
					anOutputStream311.write(aByteArray312, anInt310, i_6_);
					anOutputStream311.write(aByteArray312, 0, i - i_6_);
				}
			} catch (IOException ioexception) {
				synchronized (this) {
					anIOException306 = ioexception;
					break;
				}
			}
			synchronized (this) {
				anInt310 = (i + anInt310) % anInt308;
			}
		}
	}

	public static void method323(int i) {
		if (i < 38)
			method322(-71, 92, 2);
		aClass324Array305 = null;
	}

	static final boolean method324(byte i, char c) {
		try {
			if (Character.isISOControl(c))
				return false;
			if (Class350.method3679((byte) 102, c))
				return true;
			char[] cs = Class264.aCharArray2469;
			for (int i_7_ = 0; cs.length > i_7_; i_7_++) {
				char c_8_ = cs[i_7_];
				if (c_8_ == c)
					return true;
			}
			if (i <= 123)
				method323(-115);
			char[] cs_9_ = Class296_Sub35_Sub2.aCharArray6111;
			for (int i_10_ = 0; i_10_ < cs_9_.length; i_10_++) {
				char c_11_ = cs_9_[i_10_];
				if (c == c_11_)
					return true;
			}
			return false;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "bj.D(" + i + ',' + c + ')');
		}
	}

	Class29(OutputStream outputstream, int i) {
		anInt304 = 0;
		anOutputStream311 = outputstream;
		anInt308 = i + 1;
		aByteArray312 = new byte[anInt308];
		aThread309 = new Thread(this);
		aThread309.setDaemon(true);
		aThread309.start();
	}
}
