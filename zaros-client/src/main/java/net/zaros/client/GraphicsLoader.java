package net.zaros.client;
/* Class265 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class GraphicsLoader {
	Js5 modelCache;
	private Js5 gfxCache;
	private AdvancedMemoryCache gfxcache = new AdvancedMemoryCache(64);
	static Sprite[] aClass397Array2478;
	static OutgoingPacket aClass311_2479;
	static int anInt2480 = 0;
	AdvancedMemoryCache modelcache = new AdvancedMemoryCache(60);
	int anInt2482;

	static final int method2280(int i, long l) {
		if (i > -96) {
			aClass397Array2478 = null;
		}
		Class188.method1889(l, 26998);
		return Class296_Sub38.aCalendar4889.get(1);
	}

	final void clearCached(int i) {
		synchronized (gfxcache) {
			gfxcache.clear();
		}
		synchronized (modelcache) {
			if (i != 22896) {
				method2288(9, -126);
			}
			modelcache.clear();
		}
	}

	final Graphic getGraphic(int graphicID) {
		Graphic gfx;
		synchronized (gfxcache) {
			gfx = (Graphic) gfxcache.get(graphicID);
		}
		if (gfx != null) {
			return gfx;
		}
		byte[] data;
		synchronized (gfxCache) {
			data = gfxCache.getFile(GraphicsLoader.getGraphicContainerID(graphicID), GraphicsLoader.getGraphicChildID(graphicID));
		}
		gfx = new Graphic();
		gfx.loader = this;
		gfx.gfxID = graphicID;
		if (data != null) {
			boolean newGfx = data[0] == 'N' && data[1] == 'E' && data[2] == 'W';
			gfx.init(new Packet(data),newGfx);
		}
		synchronized (gfxcache) {
			gfxcache.put(gfx, graphicID);
		}
		return gfx;
	}

	public static void method2283(boolean bool) {
		aClass397Array2478 = null;
		if (bool != true) {
			anInt2480 = -86;
		}
		aClass311_2479 = null;
	}

	static final Class210_Sub1 method2284(int i, int i_2_) {
		if (i_2_ > -83) {
			method2283(false);
		}
		if (!Class404.aBoolean3386 || i < Class368_Sub15.anInt5520 || Class338_Sub3_Sub3_Sub1.anInt6617 < i) {
			return null;
		}
		return SubCache.aClass210_Sub1Array2713[i - Class368_Sub15.anInt5520];
	}

	final void method2285(int i, boolean bool) {
		synchronized (gfxcache) {
			gfxcache.clean(i);
		}
		synchronized (modelcache) {
			modelcache.clean(i);
			if (bool) {
				getGraphic(123);
			}
		}
	}

	static final int method2286(boolean bool) {
		if (bool != true) {
			method2286(false);
		}
		if (ConfigsRegister.anInt3674 == 1) {
			return Class391.anInt3298;
		}
		return 0;
	}

	final void method2287(int i) {
		synchronized (gfxcache) {
			gfxcache.clearSoftReferences();
		}
		synchronized (modelcache) {
			modelcache.clearSoftReferences();
			int i_3_ = 69 % ((i + 86) / 36);
		}
	}

	final void method2288(int i, int i_4_) {
		if (i_4_ == 0) {
			anInt2482 = i;
			synchronized (modelcache) {
				modelcache.clear();
			}
		}
	}

	static final int getGraphicChildID(int i) {
		return i & 0xff;
	}

	static final int getGraphicContainerID(int gfxID) {
		return gfxID >>> 8;
	}

	GraphicsLoader(GameType class35, int i, Js5 class138, Js5 class138_5_) {
		gfxCache = class138;
		modelCache = class138_5_;
		int i_6_ = gfxCache.getLastGroupId() - 1;
		gfxCache.getLastFileId(i_6_);
	}

	static {
		aClass311_2479 = new OutgoingPacket(92, 4);
	}
}
