package net.zaros.client;

/* Class296_Sub51_Sub36 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub36 extends TextureOperation {
	static Class338_Sub8_Sub2_Sub1[] aClass338_Sub8_Sub2_Sub1Array6528;
	static ha aHa6529;
	static Class315 aClass315_6530;

	public Class296_Sub51_Sub36() {
		super(0, true);
	}

	final int[] get_monochrome_output(int i, int i_0_) {
		int[] is = aClass318_5035.method3335(i_0_, (byte) 28);
		if (i != 0)
			aHa6529 = null;
		if (aClass318_5035.aBoolean2819) {
			int i_1_ = Class294.anIntArray2686[i_0_];
			for (int i_2_ = 0; Class41_Sub10.anInt3769 > i_2_; i_2_++)
				is[i_2_] = (method3190(-117, Class33.anIntArray334[i_2_], i_1_) % 4096);
		}
		return is;
	}

	private final int method3190(int i, int i_3_, int i_4_) {
		int i_5_ = i_3_ + i_4_ * 57;
		if (i > -103)
			return 59;
		i_5_ ^= i_5_ << 1;
		return (-(((i_5_ * i_5_ * 15731 + 789221) * i_5_ + 1376312589 & 0x7fffffff) / 262144) + 4096);
	}

	public static void method3191(byte i) {
		aHa6529 = null;
		if (i < -65) {
			aClass338_Sub8_Sub2_Sub1Array6528 = null;
			aClass315_6530 = null;
		}
	}
}
