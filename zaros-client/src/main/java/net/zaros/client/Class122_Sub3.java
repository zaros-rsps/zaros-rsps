package net.zaros.client;

/* Class122_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class122_Sub3 extends Class122 {
	static Class294 aClass294_5663;
	private Sprite aClass397_5664;
	static long aLong5665 = -1L;
	static int anInt5666;

	public static void method1047(byte i) {
		aClass294_5663 = null;
		if (i >= -125)
			method1047((byte) 118);
	}

	static final void method1048() {
		int i = 10;
		int i_0_ = 30;
		if (Class296_Sub13.anInt4654 != 0 && Class166_Sub1.aClass55_4303 != null) {
			Class16_Sub2.aHa3733.K(Class404.anIntArray3383);
			for (int i_1_ = 0; i_1_ < Class379_Sub2_Sub1.anIntArray6605.length; i_1_++)
				Class16_Sub2.aHa3733.method1095(Class404.anIntArray3383[1], (Class379_Sub2_Sub1.anIntArray6605[i_1_] + Class377.anIntArray3192[i_1_]), -256, Class404.anIntArray3383[3] - Class404.anIntArray3383[1], 113);
			for (int i_2_ = 0; i_2_ < Class360_Sub9.anInt5343; i_2_++) {
				Class324 class324 = Class29.aClass324Array305[i_2_];
				Class16_Sub2.aHa3733.H(class324.anIntArray2854[0], class324.anIntArray2863[0], class324.anIntArray2849[0], ObjectDefinitionLoader.anIntArray380);
				Class16_Sub2.aHa3733.H(class324.anIntArray2854[1], class324.anIntArray2863[1], class324.anIntArray2849[1], za.anIntArray5086);
				Class16_Sub2.aHa3733.H(class324.anIntArray2854[2], class324.anIntArray2863[2], class324.anIntArray2849[2], CS2Call.anIntArray4956);
				Class16_Sub2.aHa3733.H(class324.anIntArray2854[3], class324.anIntArray2863[3], class324.anIntArray2849[3], Class41_Sub4.anIntArray3748);
				if (ObjectDefinitionLoader.anIntArray380[2] != -1 && za.anIntArray5086[2] != -1 && CS2Call.anIntArray4956[2] != -1 && Class41_Sub4.anIntArray3748[2] != -1) {
					int i_3_ = -65536;
					if (class324.aByte2852 == 4)
						i_3_ = -16776961;
					Class16_Sub2.aHa3733.method1094(za.anIntArray5086[1], ObjectDefinitionLoader.anIntArray380[1], za.anIntArray5086[0], ObjectDefinitionLoader.anIntArray380[0], 126, i_3_);
					Class16_Sub2.aHa3733.method1094((CS2Call.anIntArray4956[1]), za.anIntArray5086[1], (CS2Call.anIntArray4956[0]), za.anIntArray5086[0], 126, i_3_);
					Class16_Sub2.aHa3733.method1094(Class41_Sub4.anIntArray3748[1], CS2Call.anIntArray4956[1], Class41_Sub4.anIntArray3748[0], CS2Call.anIntArray4956[0], 124, i_3_);
					Class16_Sub2.aHa3733.method1094(ObjectDefinitionLoader.anIntArray380[1], (Class41_Sub4.anIntArray3748[1]), ObjectDefinitionLoader.anIntArray380[0], (Class41_Sub4.anIntArray3748[0]), 126, i_3_);
					Class16_Sub2.aHa3733.method1094((CS2Call.anIntArray4956[1]), ObjectDefinitionLoader.anIntArray380[1], (CS2Call.anIntArray4956[0]), ObjectDefinitionLoader.anIntArray380[0], 123, i_3_);
				}
			}
			Class166_Sub1.aClass55_4303.a(-16777216, 1659, -256, i, ("Dynamic: " + Class127.anInt1303 + "/" + 5000), i_0_ + 45);
			Class166_Sub1.aClass55_4303.a(-16777216, 1659, -256, i, ("Total Opaque Onscreen: " + Class338_Sub8_Sub2.anInt6584 + "/" + 10000), i_0_ + 60);
			Class166_Sub1.aClass55_4303.a(-16777216, 1659, -256, i, ("Total Trans Onscreen: " + CS2Call.anInt4963 + "/" + 5000), i_0_ + 75);
			Class166_Sub1.aClass55_4303.a(-16777216, 1659, -256, i, ("Occluders: " + (Class349.anInt3036 + Class77.anInt880) + " Active: " + Class360_Sub9.anInt5343), i_0_ + 90);
			Class166_Sub1.aClass55_4303.a(-16777216, 1659, -256, i, ("Occluded: Ground:" + Class367.anInt3126 + " Walls: " + Class338_Sub3_Sub5.anInt6572 + " CPs: " + Class264.anInt2473 + " Pixels: " + Class296_Sub39_Sub20.anInt6255), i_0_ + 105);
			Class166_Sub1.aClass55_4303.a(-16777216, 1659, -256, i, ("Occlude Calc Took: " + Class84.aLong925 / 1000L + "us"), i_0_ + 120);
			if (Class296_Sub13.anInt4654 == 2 && Class365.anIntArray3115 != null) {
				for (int i_4_ = 0; i_4_ < Class365.anIntArray3115.length; i_4_++) {
					float f = (float) Class365.anIntArray3115[i_4_];
					f /= 4194304.0F;
					if (f > 1.0F)
						f = 1.0F;
					f *= 255.0F;
					f = 255.0F - f;
					int i_5_ = (int) f;
					Class365.anIntArray3115[i_4_] = i_5_ | i_5_ << 8 | i_5_ << 16 | ~0xffffff;
				}
				Sprite class397 = Class16_Sub2.aHa3733.method1096(104, 0, (Class123_Sub1_Sub1.anInt5813), Class213.anInt2110, Class213.anInt2110, Class365.anIntArray3115);
				class397.method4079(i, 170, 1, 0, 0);
			}
		}
	}

	final void method1035(int i, boolean bool, int i_6_, int i_7_) {
		Class41_Sub13.aHa3774.b(i_7_ - 2, i, aClass379_3543.anInt3613 + 4, aClass379_3543.anInt3626 + 2, ((Class379_Sub3) aClass379_3543).anInt5688, 0);
		if (i_6_ != -24228)
			anInt5666 = 74;
		Class41_Sub13.aHa3774.b(i_7_ - 1, i + 1, aClass379_3543.anInt3613 + 2, aClass379_3543.anInt3626, 0, 0);
	}

	public final void method42(byte i) {
		super.method42(i);
		aClass397_5664 = CS2Script.method2798(aClass138_3546, (((Class379_Sub3) aClass379_3543).anInt5686), -3);
	}

	final void method1036(boolean bool, int i, int i_8_, byte i_9_) {
		int i_10_ = this.method1039(true) * aClass379_3543.anInt3613 / 10000;
		int[] is = new int[4];
		Class41_Sub13.aHa3774.K(is);
		if (i_9_ == 80) {
			Class41_Sub13.aHa3774.KA(i, i_8_ + 2, i_10_ + i, aClass379_3543.anInt3626 + i_8_);
			aClass397_5664.method4085(i, i_8_ + 2, aClass379_3543.anInt3613, aClass379_3543.anInt3626);
			Class41_Sub13.aHa3774.KA(is[0], is[1], is[2], is[3]);
		}
	}

	Class122_Sub3(Js5 class138, Js5 class138_11_, Class379_Sub3 class379_sub3) {
		super(class138, class138_11_, (Class379) class379_sub3);
	}

	static final int method1049(int i, boolean bool, int i_12_) {
		if (i_12_ > i) {
			int i_13_ = i;
			i = i_12_;
			i_12_ = i_13_;
		}
		if (bool)
			anInt5666 = 78;
		int i_14_;
		for (/**/; i_12_ != 0; i_12_ = i_14_) {
			i_14_ = i % i_12_;
			i = i_12_;
		}
		return i;
	}

	static final Class256 method1050(int i, byte i_15_) {
		if (i_15_ > -48)
			aClass294_5663 = null;
		if (i != 0) {
			if (i != 1) {
				if (i == 2) {
					if ((double) Class106.aFloat1100 == 3.0)
						return Class183.aClass256_1879;
					if ((double) Class106.aFloat1100 == 4.0)
						return ParamTypeList.aClass256_1694;
					if ((double) Class106.aFloat1100 == 6.0)
						return Class49.aClass256_464;
					if ((double) Class106.aFloat1100 >= 8.0)
						return Class264.aClass256_2472;
				}
			} else {
				if ((double) Class106.aFloat1100 == 3.0)
					return Class134.aClass256_1386;
				if ((double) Class106.aFloat1100 == 4.0)
					return Class41_Sub1.aClass256_3737;
				if ((double) Class106.aFloat1100 == 6.0)
					return Class183.aClass256_1879;
				if ((double) Class106.aFloat1100 >= 8.0)
					return ParamTypeList.aClass256_1694;
			}
		} else {
			if ((double) Class106.aFloat1100 == 3.0)
				return Class41_Sub14.aClass256_3776;
			if ((double) Class106.aFloat1100 == 4.0)
				return Class162.aClass256_3559;
			if ((double) Class106.aFloat1100 == 6.0)
				return Class134.aClass256_1386;
			if ((double) Class106.aFloat1100 >= 8.0)
				return Class41_Sub1.aClass256_3737;
		}
		return null;
	}

	public final boolean method44(byte i) {
		if (i <= 91)
			method44((byte) 119);
		if (!super.method44((byte) 124))
			return false;
		return aClass138_3546.hasEntryBuffer(((Class379_Sub3) aClass379_3543).anInt5686);
	}

	static {
		aClass294_5663 = new Class294(1, 2);
	}
}
