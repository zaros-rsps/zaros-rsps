package net.zaros.client;

/* Class171 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class171 {
	private int anInt1796;
	private Class193[] aClass193Array1797;
	private int anInt1798;

	static final Class171 method1675(Js5 class138, int i, int i_0_) {
		byte[] is = class138.getFile(i, i_0_);
		if (is == null)
			return null;
		return new Class171(new Packet(is));
	}

	final Class296_Sub19_Sub1 method1676() {
		byte[] is = method1677();
		return new Class296_Sub19_Sub1(22050, is, anInt1796 * 22050 / 1000, anInt1798 * 22050 / 1000);
	}

	private final byte[] method1677() {
		int i = 0;
		for (int i_1_ = 0; i_1_ < 10; i_1_++) {
			if (aClass193Array1797[i_1_] != null && (aClass193Array1797[i_1_].anInt1966 + aClass193Array1797[i_1_].anInt1956) > i)
				i = (aClass193Array1797[i_1_].anInt1966 + aClass193Array1797[i_1_].anInt1956);
		}
		if (i == 0)
			return new byte[0];
		int i_2_ = i * 22050 / 1000;
		byte[] is = new byte[i_2_];
		for (int i_3_ = 0; i_3_ < 10; i_3_++) {
			if (aClass193Array1797[i_3_] != null) {
				int i_4_ = aClass193Array1797[i_3_].anInt1966 * 22050 / 1000;
				int i_5_ = aClass193Array1797[i_3_].anInt1956 * 22050 / 1000;
				int[] is_6_ = aClass193Array1797[i_3_].method1927(i_4_, (aClass193Array1797[i_3_].anInt1966));
				for (int i_7_ = 0; i_7_ < i_4_; i_7_++) {
					int i_8_ = is[i_7_ + i_5_] + (is_6_[i_7_] >> 8);
					if ((i_8_ + 128 & ~0xff) != 0)
						i_8_ = i_8_ >> 31 ^ 0x7f;
					is[i_7_ + i_5_] = (byte) i_8_;
				}
			}
		}
		return is;
	}

	final int method1678() {
		int i = 9999999;
		for (int i_9_ = 0; i_9_ < 10; i_9_++) {
			if (aClass193Array1797[i_9_] != null && aClass193Array1797[i_9_].anInt1956 / 20 < i)
				i = aClass193Array1797[i_9_].anInt1956 / 20;
		}
		if (anInt1796 < anInt1798 && anInt1796 / 20 < i)
			i = anInt1796 / 20;
		if (i == 9999999 || i == 0)
			return 0;
		for (int i_10_ = 0; i_10_ < 10; i_10_++) {
			if (aClass193Array1797[i_10_] != null)
				aClass193Array1797[i_10_].anInt1956 -= i * 20;
		}
		if (anInt1796 < anInt1798) {
			anInt1796 -= i * 20;
			anInt1798 -= i * 20;
		}
		return i;
	}

	private Class171(Packet class296_sub17) {
		aClass193Array1797 = new Class193[10];
		for (int i = 0; i < 10; i++) {
			int i_11_ = class296_sub17.g1();
			if (i_11_ != 0) {
				class296_sub17.pos--;
				aClass193Array1797[i] = new Class193();
				aClass193Array1797[i].method1924(class296_sub17);
			}
		}
		anInt1796 = class296_sub17.g2();
		anInt1798 = class296_sub17.g2();
	}

	private Class171() {
		aClass193Array1797 = new Class193[10];
	}
}
