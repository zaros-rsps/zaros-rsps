package net.zaros.client;

/* Class241_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class241_Sub1_Sub1 extends Class241_Sub1 {
	private int anInt5883;
	private int anInt5884;
	private int anInt5885;
	static int anInt5886;
	private int anInt5887;
	private ha_Sub1 aHa_Sub1_5888;
	private int anInt5889;
	static boolean aBoolean5890 = false;
	private int anInt5891;
	private Interface6_Impl3 anInterface6_Impl3_5892;

	static final String method2155(boolean bool, Class296_Sub39_Sub9 class296_sub39_sub9) {
		if (bool)
			method2156((byte) 63, null);
		if (class296_sub39_sub9.aString6171 == null || class296_sub39_sub9.aString6171.length() == 0) {
			if (class296_sub39_sub9.aString6166 != null && class296_sub39_sub9.aString6166.length() > 0)
				return (class296_sub39_sub9.aString6168 + TranslatableString.aClass120_1228.getTranslation(Class394.langID) + class296_sub39_sub9.aString6166);
			return class296_sub39_sub9.aString6168;
		}
		if (class296_sub39_sub9.aString6166 != null && class296_sub39_sub9.aString6166.length() > 0)
			return (class296_sub39_sub9.aString6168 + TranslatableString.aClass120_1228.getTranslation(Class394.langID) + class296_sub39_sub9.aString6166 + TranslatableString.aClass120_1228.getTranslation(Class394.langID) + class296_sub39_sub9.aString6171);
		return (class296_sub39_sub9.aString6168 + TranslatableString.aClass120_1228.getTranslation(Class394.langID) + class296_sub39_sub9.aString6171);
	}

	static final void method2156(byte i, String string) {
		if (i != -29)
			method2156((byte) -108, null);
		if (Class338_Sub8.aClass94Array5257 != null) {
			Connection class204 = Class296_Sub51_Sub13.method3111(true);
			Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 101, Class25.aClass311_282);
			class296_sub1.out.p1(Class117.method1015((byte) -107, string));
			class296_sub1.out.writeString(string);
			class204.sendPacket(class296_sub1, (byte) 119);
		}
	}

	final Interface6_Impl3 method2152(int i) {
		int i_0_ = -127 % ((37 - i) / 63);
		if (anInterface6_Impl3_5892 == null) {
			d var_d = aHa_Sub1_5888.aD1299;
			Class254.anIntArray3599[4] = anInt5885;
			Class254.anIntArray3599[2] = anInt5891;
			Class254.anIntArray3599[0] = anInt5883;
			Class254.anIntArray3599[5] = anInt5889;
			Class254.anIntArray3599[1] = anInt5887;
			Class254.anIntArray3599[3] = anInt5884;
			boolean bool = false;
			int i_1_ = 0;
			for (int i_2_ = 0; i_2_ < 6; i_2_++) {
				if (!var_d.is_ready(Class254.anIntArray3599[i_2_]))
					return null;
				MaterialRaw class170 = var_d.method14(Class254.anIntArray3599[i_2_], -9412);
				int i_3_ = class170.small_sized ? 64 : 128;
				if (i_3_ > i_1_)
					i_1_ = i_3_;
				if (class170.aByte1795 > 0)
					bool = true;
			}
			for (int i_4_ = 0; i_4_ < 6; i_4_++)
				ConfigurationDefinition.anIntArrayArray678[i_4_] = var_d.get_transparent_pixels(i_1_, i_1_, false, (byte) 106, 1.0F, Class254.anIntArray3599[i_4_]);
			anInterface6_Impl3_5892 = aHa_Sub1_5888.method1104(bool, 0, i_1_, ConfigurationDefinition.anIntArrayArray678);
		}
		return anInterface6_Impl3_5892;
	}

	Class241_Sub1_Sub1(ha_Sub1 var_ha_Sub1, int i, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_) {
		anInt5885 = i_8_;
		anInt5891 = i_6_;
		anInt5889 = i_9_;
		aHa_Sub1_5888 = var_ha_Sub1;
		anInt5884 = i_7_;
		anInt5887 = i_5_;
		anInt5883 = i;
	}

	static {
		anInt5886 = 2;
	}
}
