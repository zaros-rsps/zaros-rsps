package net.zaros.client;
/* Class189_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

final class Class189_Sub1 extends Class189 implements MouseListener, MouseMotionListener {
	private int anInt4517;
	private int anInt4518;
	private int anInt4519;
	private NodeDeque aClass155_4520 = new NodeDeque();
	private int anInt4521;
	private NodeDeque aClass155_4522 = new NodeDeque();
	private int anInt4523;
	private int anInt4524;
	private Component aComponent4525;
	private boolean aBoolean4526;

	public final synchronized void mouseExited(MouseEvent mouseevent) {
		method1909((byte) -122, mouseevent.getY(), mouseevent.getX());
	}

	public final synchronized void mouseMoved(MouseEvent mouseevent) {
		method1909((byte) -126, mouseevent.getY(), mouseevent.getX());
	}

	private final int method1903(MouseEvent mouseevent, int i) {
		int i_0_ = mouseevent.getModifiers();
		if (i != 1)
			return 69;
		boolean bool = (i_0_ & 0x10) != 0;
		boolean bool_1_ = (i_0_ & 0x8) != 0;
		boolean bool_2_ = (i_0_ & 0x4) != 0;
		if (bool_1_ && (bool || bool_2_))
			bool_1_ = false;
		if (bool && bool_2_)
			return 4;
		if (bool)
			return 1;
		if (bool_1_)
			return 2;
		if (bool_2_)
			return 4;
		return 0;
	}

	public final synchronized void mouseClicked(MouseEvent mouseevent) {
		if (mouseevent.isPopupTrigger())
			mouseevent.consume();
	}

	static final boolean method1904(int i) {
		if (StaticMethods.aClass296_Sub39_Sub9_6085 == null)
			return false;
		if (i != -7703)
			return false;
		if (StaticMethods.aClass296_Sub39_Sub9_6085.anInt6165 >= 2000)
			StaticMethods.aClass296_Sub39_Sub9_6085.anInt6165 -= 2000;
		if (StaticMethods.aClass296_Sub39_Sub9_6085.anInt6165 == 1006)
			return true;
		return false;
	}

	final synchronized void method1893(int i) {
		anInt4518 = anInt4523;
		anInt4519 = anInt4521;
		anInt4517 = anInt4524;
		if (i != 0)
			anInt4517 = -64;
		NodeDeque class155 = aClass155_4520;
		aClass155_4520 = aClass155_4522;
		aClass155_4522 = class155;
		aClass155_4522.method1581(327680);
	}

	final boolean method1902(int i) {
		int i_3_ = 85 % ((-11 - i) / 61);
		if ((anInt4517 & 0x2) == 0)
			return false;
		return true;
	}

	public final synchronized void mousePressed(MouseEvent mouseevent) {
		int i = method1903(mouseevent, 1);
		if (i != 1) {
			if (i != 4) {
				if (i == 2)
					method1907(-109, 1, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY());
			} else
				method1907(-67, 2, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY());
		} else
			method1907(-64, 0, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY());
		anInt4524 |= i;
		if (mouseevent.isPopupTrigger())
			mouseevent.consume();
	}

	public final synchronized void mouseEntered(MouseEvent mouseevent) {
		method1909((byte) -121, mouseevent.getY(), mouseevent.getX());
	}

	static final int method1905(int i, int i_4_, int i_5_, int i_6_) {
		if (i_5_ != 16)
			return 80;
		i &= 0x3;
		if (i == 0)
			return i_4_;
		if (i == 1)
			return -i_6_ + 4095;
		if (i == 2)
			return -i_4_ + 4095;
		return i_6_;
	}

	public final synchronized void mouseReleased(MouseEvent mouseevent) {
		int i = method1903(mouseevent, 1);
		if ((anInt4524 & i) == 0)
			i = anInt4524;
		if ((i & 0x1) != 0)
			method1907(-127, 3, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY());
		if ((i & 0x4) != 0)
			method1907(-92, 5, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY());
		if ((i & 0x2) != 0)
			method1907(-111, 4, mouseevent.getX(), mouseevent.getClickCount(), mouseevent.getY());
		anInt4524 &= i ^ 0xffffffff;
		if (mouseevent.isPopupTrigger())
			mouseevent.consume();
	}

	public final synchronized void mouseDragged(MouseEvent mouseevent) {
		method1909((byte) -121, mouseevent.getY(), mouseevent.getX());
	}

	private final void method1906(int i, Component component) {
		method1910(21821);
		if (i == 2) {
			aComponent4525 = component;
			aComponent4525.addMouseListener(this);
			aComponent4525.addMouseMotionListener(this);
		}
	}

	private final void method1907(int i, int i_7_, int i_8_, int i_9_, int i_10_) {
		Class296_Sub34_Sub2 class296_sub34_sub2 = new Class296_Sub34_Sub2();
		class296_sub34_sub2.anInt6097 = i_10_;
		class296_sub34_sub2.anInt6103 = i_9_;
		if (i < -55) {
			class296_sub34_sub2.anInt6100 = i_8_;
			class296_sub34_sub2.anInt6099 = i_7_;
			class296_sub34_sub2.aLong6102 = Class72.method771(-122);
			aClass155_4522.addLast((byte) -20, class296_sub34_sub2);
		}
	}

	static final String method1908(int i, byte i_11_) {
		if (i_11_ != 86)
			return null;
		return (String.valueOf(i >> 24 & 0xff) + "." + (i >> 16 & 0xff) + "." + (i >> 8 & 0xff) + "." + (i & 0xff));
	}

	final void method1896(boolean bool) {
		if (!bool)
			method1910(21821);
	}

	final Class296_Sub34 method1899(int i) {
		int i_12_ = -53 % ((13 - i) / 40);
		return (Class296_Sub34) aClass155_4520.method1573(1);
	}

	private final void method1909(byte i, int i_13_, int i_14_) {
		if (i > -115)
			mouseReleased(null);
		anInt4521 = i_13_;
		anInt4523 = i_14_;
		if (aBoolean4526)
			method1907(-64, -1, i_14_, 0, i_13_);
	}

	private final void method1910(int i) {
		if (aComponent4525 != null) {
			aComponent4525.removeMouseMotionListener(this);
			aComponent4525.removeMouseListener(this);
			anInt4523 = anInt4521 = anInt4524 = 0;
			anInt4518 = anInt4519 = anInt4517 = 0;
			aClass155_4520 = null;
			if (i != 21821)
				anInt4517 = 91;
			aComponent4525 = null;
			aClass155_4522 = null;
		}
	}

	final int method1897(int i) {
		if (i != 0)
			method1897(87);
		return anInt4519;
	}

	final boolean method1901(byte i) {
		int i_15_ = 103 % ((-11 - i) / 55);
		if ((anInt4517 & 0x1) == 0)
			return false;
		return true;
	}

	final int method1895(byte i) {
		if (i != -55)
			return 48;
		return anInt4518;
	}

	final boolean method1892(byte i) {
		if (i != -28)
			method1907(31, -39, 121, 52, -116);
		if ((anInt4517 & 0x4) == 0)
			return false;
		return true;
	}

	Class189_Sub1(Component component, boolean bool) {
		method1906(2, component);
		aBoolean4526 = bool;
	}
}
