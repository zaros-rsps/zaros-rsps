package net.zaros.client.configs.objtype;

import net.zaros.client.AdvancedMemoryCache;
import net.zaros.client.Class313;
import net.zaros.client.Class363;
import net.zaros.client.Class55;
import net.zaros.client.GameType;
import net.zaros.client.Js5;
import net.zaros.client.Node;
import net.zaros.client.Packet;
import net.zaros.client.ParamType;
import net.zaros.client.ParamTypeList;
import net.zaros.client.PlayerEquipment;
import net.zaros.client.Sprite;
import net.zaros.client.TranslatableString;
import net.zaros.client.ha;

public class ObjTypeList {

	// static stuff.
	public static final int[] anIntArray1321 = new int[13];
	public static int anInt1320 = 0;

	// the passed properties.
	public GameType gameType;
	public int language;
	public boolean members_world;
	public int detail_flags;

	// the caching stuff.
	public final AdvancedMemoryCache typeCache = new AdvancedMemoryCache(64);
	public final AdvancedMemoryCache modelCache = new AdvancedMemoryCache(50);

	// the icon requests.
	public final Class313 iconRequests = new Class313(250);
	public final ObjIconRequest lastIconRequest = new ObjIconRequest();

	// the cache js5.
	public Js5 itemsJs5;
	public Js5 modelsJs5;

	// the default actions.
	private String[] defaultGroundActions;
	private String[] defaultInventoryActions;

	// other stuff.
	public ParamTypeList paramsList;
	public int size;

	public ObjTypeList(GameType gameType, int language, boolean members_world, ParamTypeList paramsList, Js5 itemsJs5, Js5 modelsJs5) {
		this.gameType = gameType;
		this.language = language;
		this.members_world = members_world;
		this.itemsJs5 = itemsJs5;
		this.modelsJs5 = modelsJs5;
		this.paramsList = paramsList;
		if (itemsJs5 == null) {
			size = 0;
		} else {
			int groupId = itemsJs5.getLastGroupId() - 1;
			size = groupId * 256 + itemsJs5.getLastFileId(groupId);
		}
		if (Class363.runescape != gameType) {
			defaultGroundActions = new String[] { null, null, TranslatableString.take.getTranslation(language), null, null, null };
		} else {
			defaultGroundActions = new String[] { null, null, TranslatableString.take.getTranslation(language), null, null, TranslatableString.aClass120_1218.getTranslation(language) };
		}
		defaultInventoryActions = new String[] { null, null, null, null, TranslatableString.drop.getTranslation(language) };
	}

	public ObjType list(int objid) {
		ObjType type;
		synchronized (typeCache) {
			type = (ObjType) typeCache.get(objid);
		}
		if (type != null) {
			return type;
		}
		byte[] data;
		synchronized (itemsJs5) {
			data = itemsJs5.getFile(ObjTypeList.getGroupId(objid), ObjTypeList.getFileId(objid));
		}
		type = new ObjType();
		type.id = objid;
		type.myList = this;
		type.groundOptions = defaultGroundActions.clone();
		type.inventoryOptions = defaultInventoryActions.clone();
		if (data != null) {
			type.decode(new Packet(data));
		}
		type.postDecode();
		if (type.certTemplate != -1) {
			type.makeCert(list(type.certTemplate), list(type.certLink));
		}
		if (type.lendTemplate != -1) {
			type.makeLent(list(type.lendTemplate), list(type.lentLink));
		}
		if (type.boughtTemplate != -1) {
			type.makebought(list(type.boughtTemplate), list(type.boughtLink));
		}
		if (!members_world && type.members) {
			type.name = TranslatableString.aClass120_1201.getTranslation(language);
			type.groundOptions = defaultGroundActions;
			type.team = 0;
			type.quests = null;
			type.inventoryOptions = defaultInventoryActions;
			type.stockmarket = false;
			if (type.params != null) {
				boolean bool = false;
				for (Node class296 = type.params.getFirst(true); class296 != null; class296 = type.params.getNext(0)) {
					ParamType param = paramsList.list((int) class296.uid);
					if (param.disabled) {
						class296.unlink();
					} else {
						bool = true;
					}
				}
				if (!bool) {
					type.params = null;
				}
			}
		}
		synchronized (typeCache) {
			typeCache.put(type, objid);
		}
		return type;
	}

	public Sprite getIcon(boolean bool, int i, ha var_ha, int i_7_, boolean bool_8_, ha var_ha_9_, Class55 class55, PlayerEquipment class402, int i_10_, int i_11_, int i_12_, int i_13_) {
		Sprite class397 = requestIcon(i_13_, false, var_ha_9_, i_11_, class402, i, i_10_, i_12_);
		if (i_7_ != 13) {
			requestIcon(-83, true, null, 120, null, -31, -13, 127);
		}
		if (class397 != null) {
			return class397;
		}
		ObjType class158 = list(i_13_);
		if (i_11_ > 1 && class158.stackObjs != null) {
			int i_14_ = -1;
			for (int i_15_ = 0; i_15_ < 10; i_15_++) {
				if (class158.stackCounts[i_15_] <= i_11_ && class158.stackCounts[i_15_] != 0) {
					i_14_ = class158.stackObjs[i_15_];
				}
			}
			if (i_14_ != -1) {
				class158 = list(i_14_);
			}
		}
		int[] is = class158.createIconPixels(i_12_, var_ha_9_, bool_8_, i_10_, i, i_11_, class402, class55, -96, var_ha);
		if (is == null) {
			return null;
		}
		Sprite class397_16_;
		if (!bool) {
			class397_16_ = var_ha_9_.method1096(104, 0, 32, 36, 36, is);
		} else {
			class397_16_ = var_ha.method1096(110, 0, 32, 36, 36, is);
		}
		if (!bool) {
			ObjIconRequest class233 = new ObjIconRequest();
			class233.anInt3636 = i;
			class233.anInt3642 = i_13_;
			class233.anInt3640 = i_11_;
			class233.anInt3638 = i_12_;
			class233.anInt3639 = var_ha_9_.anInt1295;
			class233.anInt3634 = i_10_;
			class233.aBoolean3641 = class402 != null;
			iconRequests.method3317(class397_16_, 89, class233);
		}
		return class397_16_;
	}

	public Sprite requestIcon(int i, boolean bool, ha var_ha, int i_0_, PlayerEquipment class402, int i_1_, int i_2_, int i_3_) {
		lastIconRequest.anInt3638 = i_3_;
		lastIconRequest.anInt3639 = var_ha.anInt1295;
		lastIconRequest.anInt3634 = i_2_;
		lastIconRequest.anInt3636 = i_1_;
		lastIconRequest.anInt3642 = i;
		if (bool) {
			defaultInventoryActions = null;
		}
		lastIconRequest.anInt3640 = i_0_;
		lastIconRequest.aBoolean3641 = class402 != null;
		return (Sprite) iconRequests.method3312(lastIconRequest, 0);
	}

	public void update_details(int detail_flags) {
		this.detail_flags = detail_flags;
		synchronized (modelCache) {
			modelCache.clear();
		}
	}

	public void update_members(boolean members_world) {
		if (!this.members_world == members_world) {
			this.members_world = members_world;
			uncacheAll();
		}
	}

	public void clean(int maxAge) {
		synchronized (typeCache) {
			typeCache.clean(maxAge);
		}
		synchronized (modelCache) {
			modelCache.clean(maxAge);
		}
		synchronized (iconRequests) {
			iconRequests.clean(maxAge);
		}
	}

	public void removeSoftReferences() {
		synchronized (typeCache) {
			typeCache.clearSoftReferences();
		}
		synchronized (modelCache) {
			modelCache.clearSoftReferences();
		}
		synchronized (iconRequests) {
			iconRequests.method3316(false);
		}
	}

	public void uncacheModels() {
		synchronized (modelCache) {
			modelCache.clear();
		}
	}

	public void uncacheRequests() {
		synchronized (iconRequests) {
			iconRequests.clear();
		}
	}

	public void uncacheAll() {
		synchronized (typeCache) {
			typeCache.clear();
		}
		synchronized (modelCache) {
			modelCache.clear();
		}
		synchronized (iconRequests) {
			iconRequests.clear();
		}
	}

	public static int getGroupId(int objid) {
		return objid >>> 8;
	}

	public static int getFileId(int objid) {
		return objid & 0xff;
	}
}
