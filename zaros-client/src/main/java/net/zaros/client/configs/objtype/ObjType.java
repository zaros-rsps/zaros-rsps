package net.zaros.client.configs.objtype;

import net.zaros.client.Animator;
import net.zaros.client.CS2Call;
import net.zaros.client.Class296_Sub4;
import net.zaros.client.Class296_Sub51_Sub1;
import net.zaros.client.Class296_Sub9;
import net.zaros.client.Class373;
import net.zaros.client.Class42_Sub4;
import net.zaros.client.Class48;
import net.zaros.client.Class55;
import net.zaros.client.Class8;
import net.zaros.client.HashTable;
import net.zaros.client.IntegerNode;
import net.zaros.client.Mesh;
import net.zaros.client.Model;
import net.zaros.client.Node;
import net.zaros.client.Packet;
import net.zaros.client.PlayerEquipment;
import net.zaros.client.Sprite;
import net.zaros.client.StringNode;
import net.zaros.client.TranslatableString;
import net.zaros.client.ha;

public class ObjType {

	public HashTable params;
	public int femaleEquip2;
	public ObjTypeList myList;
	public short[] retextureDst;
	public int certLink;
	private int resizeX;
	public boolean stockmarket = false;
	private int manWearZOffset;
	public int lentLink;
	public int boughtLink;
	public int maleEquip2;
	public int cursor4;
	public int femaleEquip3;
	public int[] stackCounts;
	public int cursor3op;
	public int manHead2;
	public int multiStackSize;
	public int certTemplate;
	public int boughtTemplate;
	public int femaleEquip1;
	private int ambient;
	public int womanHead;
	private int contrast;
	public int yAngle2d;
	public short[] recolourDst;
	private short[] retextureSrc;
	public int yOffest2d;
	public int opcode96;
	public int cursor3;
	public int xOffest2d;
	public boolean members;
	public int cursor4op;
	public int id;
	public int cursor1op;
	private byte[] clientPaletteIndices;
	public int cursor2op;
	public int[] stackObjs;
	public int manHead;
	public int cursor2;
	private int resizeZ;
	public int maleEquip1;
	private int womanWearXOffset;
	private int womanWearZOffset;
	private int resizeY;
	public int cost;
	public int womanHead2;
	public int zAngle2d;
	private int manWearYOffset;
	public int[] quests;
	private int modelid;
	public String[] groundOptions;
	public int xAngle2d;
	public int maleEquip3;
	public int stackability;
	public int zoom2d;
	public String[] inventoryOptions;
	private int manWearXOffset;
	private short[] recolourSrc;
	public int lendTemplate;
	public String name;
	public int cursor1;
	public int team;
	private int womanWearYOffset;
	public int pickShiftSize;

	public ObjType() {
		resizeX = 128;
		femaleEquip2 = -1;
		lentLink = -1;
		cursor3op = -1;
		boughtLink = -1;
		manHead2 = -1;
		ambient = 0;
		manWearZOffset = 0;
		cursor4op = -1;
		contrast = 0;
		cursor2op = -1;
		yOffest2d = 0;
		femaleEquip1 = -1;
		manHead = -1;
		boughtTemplate = -1;
		femaleEquip3 = -1;
		maleEquip2 = -1;
		maleEquip1 = -1;
		yAngle2d = 0;
		zAngle2d = 0;
		certLink = -1;
		opcode96 = 0;
		resizeZ = 128;
		cursor1op = -1;
		womanWearXOffset = 0;
		womanWearZOffset = 0;
		zoom2d = 2000;
		manWearYOffset = 0;
		xAngle2d = 0;
		cost = 1;
		cursor3 = -1;
		cursor2 = -1;
		certTemplate = -1;
		manWearXOffset = 0;
		cursor4 = -1;
		maleEquip3 = -1;
		resizeY = 128;
		lendTemplate = -1;
		stackability = 0;
		xOffest2d = 0;
		multiStackSize = -1;
		name = "null";
		team = 0;
		members = false;
		womanHead = -1;
		womanHead2 = -1;
		cursor1 = -1;
		pickShiftSize = 0;
		womanWearYOffset = 0;
	}

	public void decode(Packet packet) {
		for (;;) {
			int opc = packet.g1();
			if (opc == 0) {
				break;
			}
			decode(packet, opc);
		}
	}

	private void decode(Packet buffer, int opcode) {
		if (opcode == 1) {
			modelid = buffer.gSmart2or4s();
		} else if (opcode == 2) {
			name = buffer.gstr();
		} else if (opcode == 3) {
			buffer.gstr();
		} else if (opcode == 4) {
			zoom2d = buffer.g2();
		} else if (opcode == 5) {
			xAngle2d = buffer.g2();
		} else if (opcode == 6) {
			yAngle2d = buffer.g2();
		} else if (opcode == 7) {
			xOffest2d = buffer.g2();
			if (xOffest2d > 32767) {
				xOffest2d -= 65536;
			}
		} else if (opcode == 8) {
			yOffest2d = buffer.g2();
			if (yOffest2d > 32767) {
				yOffest2d -= 65536;
			}
		} else if (opcode == 9) {
			buffer.g4();
		} else if (opcode == 11) {
			stackability = 1;
		} else if (opcode == 12) {
			cost = buffer.g4();
		} else if (opcode == 13) {
			buffer.g1();
		} else if (opcode == 14) {
			buffer.g1();
		} else if (opcode == 16) {
			members = true;
		} else if (opcode == 18) {
			multiStackSize = buffer.g2();
		} else if (opcode == 23) {
			maleEquip1 = buffer.gSmart2or4s();
		} else if (opcode == 24) {
			maleEquip2 = buffer.gSmart2or4s();
		} else if (opcode == 25) {
			femaleEquip1 = buffer.gSmart2or4s();
		} else if (opcode == 26) {
			femaleEquip2 = buffer.gSmart2or4s();
		} else if (opcode >= 30 && opcode < 35) {
			groundOptions[opcode - 30] = buffer.gstr();
		} else if (opcode >= 35 && opcode < 40) {
			inventoryOptions[opcode - 35] = buffer.gstr();
		} else if (opcode == 40) {
			int count = buffer.g1();
			recolourSrc = new short[count];
			recolourDst = new short[count];
			for (int index = 0; index < count; index++) {
				recolourSrc[index] = (short) buffer.g2();
				recolourDst[index] = (short) buffer.g2();
			}
		} else if (opcode == 41) {
			int count = buffer.g1();
			retextureSrc = new short[count];
			retextureDst = new short[count];
			for (int index = 0; index < count; index++) {
				retextureSrc[index] = (short) buffer.g2();
				retextureDst[index] = (short) buffer.g2();
			}
		} else if (opcode == 42) {
			int count = buffer.g1();
			clientPaletteIndices = new byte[count];
			for (int index = 0; index < count; index++) {
				clientPaletteIndices[index] = buffer.g1b();
			}
		} else if (opcode == 65) {
			stockmarket = true;
		} else if (opcode == 78) {
			maleEquip3 = buffer.gSmart2or4s();
		} else if (opcode == 79) {
			femaleEquip3 = buffer.gSmart2or4s();
		} else if (opcode == 90) {
			manHead = buffer.g2();
		} else if (opcode == 91) {
			womanHead = buffer.g2();
		} else if (opcode == 92) {
			manHead2 = buffer.g2();
		} else if (opcode == 93) {
			womanHead2 = buffer.g2();
		} else if (opcode == 95) {
			zAngle2d = buffer.g2();
		} else if (opcode == 96) {
			opcode96 = buffer.g1();
		} else if (opcode == 97) {
			certLink = buffer.g2();
		} else if (opcode == 98) {
			certTemplate = buffer.g2();
		} else if (opcode >= 100 && opcode < 110) {
			if (stackObjs == null) {
				stackCounts = new int[10];
				stackObjs = new int[10];
			}
			stackObjs[opcode - 100] = buffer.g2();
			stackCounts[opcode - 100] = buffer.g2();
		} else if (opcode == 110) {
			resizeX = buffer.g2();
		} else if (opcode == 111) {
			resizeY = buffer.g2();
		} else if (opcode == 112) {
			resizeZ = buffer.g2();
		} else if (opcode == 113) {
			ambient = buffer.g1b();
		} else if (opcode == 114) {
			contrast = buffer.g1b() * 5;
		} else if (opcode == 115) {
			team = buffer.g1();
		} else if (opcode == 121) {
			lentLink = buffer.g2();
		} else if (opcode == 122) {
			lendTemplate = buffer.g2();
		} else if (opcode == 125) {
			manWearXOffset = buffer.g1b() << 2;
			manWearYOffset = buffer.g1b() << 2;
			manWearZOffset = buffer.g1b() << 2;
		} else if (opcode == 126) {
			womanWearXOffset = buffer.g1b() << 2;
			womanWearYOffset = buffer.g1b() << 2;
			womanWearZOffset = buffer.g1b() << 2;
		} else if (opcode == 127) {
			cursor1 = buffer.g1();
			cursor1op = buffer.g2();
		} else if (opcode == 128) {
			cursor2 = buffer.g1();
			cursor2op = buffer.g2();
		} else if (opcode == 129) {
			cursor3 = buffer.g1();
			cursor3op = buffer.g2();
		} else if (opcode == 130) {
			cursor4 = buffer.g1();
			cursor4op = buffer.g2();
		} else if (opcode == 132) {
			int count = buffer.g1();
			quests = new int[count];
			for (int index = 0; index < count; index++) {
				quests[index] = buffer.g2();
			}
		} else if (opcode == 134) {
			pickShiftSize = buffer.g1();
		} else if (opcode == 139) {
			boughtLink = buffer.g2();
		} else if (opcode == 140) {
			boughtTemplate = buffer.g2();
		} else if (opcode == 249) {
			int numParams = buffer.g1();
			if (params == null) {
				int resolution = Class8.get_next_high_pow2(numParams);
				params = new HashTable(resolution);
			}
			for (int index = 0; index < numParams; index++) {
				boolean textual = buffer.g1() == 1;
				int key = buffer.readUnsignedMedInt();
				Node value;
				if (!textual) {
					value = new IntegerNode(buffer.g4());
				} else {
					value = new StringNode(buffer.gstr());
				}
				params.put(key, value);
			}
		} else {
//			throw new RuntimeException("Missing opcode:" + opcode + ", at item: " + id);
		}
	}

	public void postDecode() {
	}

	public void makeCert(ObjType template, ObjType link) {
		members = link.members;
		modelid = template.modelid;
		recolourSrc = template.recolourSrc;
		name = link.name;
		yOffest2d = template.yOffest2d;
		retextureDst = template.retextureDst;
		clientPaletteIndices = template.clientPaletteIndices;
		cost = link.cost;
		yAngle2d = template.yAngle2d;
		stackability = 1;
		recolourDst = template.recolourDst;
		xAngle2d = template.xAngle2d;
		zoom2d = template.zoom2d;
		retextureSrc = template.retextureSrc;
		xOffest2d = template.xOffest2d;
		zAngle2d = template.zAngle2d;
	}

	public void makeLent(ObjType template, ObjType link) {
		maleEquip2 = link.maleEquip2;
		manHead = link.manHead;
		femaleEquip1 = link.femaleEquip1;
		manWearYOffset = link.manWearYOffset;
		members = link.members;
		modelid = template.modelid;
		retextureDst = link.retextureDst;
		womanWearXOffset = link.womanWearXOffset;
		groundOptions = link.groundOptions;
		clientPaletteIndices = link.clientPaletteIndices;
		xAngle2d = template.xAngle2d;
		yAngle2d = template.yAngle2d;
		recolourSrc = link.recolourSrc;
		name = link.name;
		womanHead2 = link.womanHead2;
		maleEquip1 = link.maleEquip1;
		manWearZOffset = link.manWearZOffset;
		manHead2 = link.manHead2;
		maleEquip3 = link.maleEquip3;
		team = link.team;
		yOffest2d = template.yOffest2d;
		params = link.params;
		recolourDst = link.recolourDst;
		manWearXOffset = link.manWearXOffset;
		femaleEquip3 = link.femaleEquip3;
		womanHead = link.womanHead;
		xOffest2d = template.xOffest2d;
		womanWearZOffset = link.womanWearZOffset;
		retextureSrc = link.retextureSrc;
		cost = 0;
		zAngle2d = template.zAngle2d;
		zoom2d = template.zoom2d;
		womanWearYOffset = link.womanWearYOffset;
		inventoryOptions = new String[5];
		femaleEquip2 = link.femaleEquip2;
		if (link.inventoryOptions != null) {
			for (int i_77_ = 0; i_77_ < 4; i_77_++) {
				inventoryOptions[i_77_] = link.inventoryOptions[i_77_];
			}
		}
		inventoryOptions[4] = TranslatableString.aClass120_1202.getTranslation(myList.language);
	}

	public void makebought(ObjType template, ObjType link) {
		femaleEquip2 = link.femaleEquip2;
		womanWearXOffset = link.womanWearXOffset;
		inventoryOptions = new String[5];
		yAngle2d = template.yAngle2d;
		name = link.name;
		maleEquip1 = link.maleEquip1;
		yOffest2d = template.yOffest2d;
		manWearXOffset = link.manWearXOffset;
		maleEquip2 = link.maleEquip2;
		femaleEquip1 = link.femaleEquip1;
		manHead = link.manHead;
		zAngle2d = template.zAngle2d;
		xOffest2d = template.xOffest2d;
		clientPaletteIndices = link.clientPaletteIndices;
		stackability = link.stackability;
		xAngle2d = template.xAngle2d;
		retextureSrc = link.retextureSrc;
		manHead2 = link.manHead2;
		womanHead2 = link.womanHead2;
		maleEquip3 = link.maleEquip3;
		retextureDst = link.retextureDst;
		womanWearZOffset = link.womanWearZOffset;
		recolourDst = link.recolourDst;
		zoom2d = template.zoom2d;
		femaleEquip3 = link.femaleEquip3;
		team = link.team;
		cost = 0;
		groundOptions = link.groundOptions;
		recolourSrc = link.recolourSrc;
		manWearYOffset = link.manWearYOffset;
		members = link.members;
		womanWearYOffset = link.womanWearYOffset;
		params = link.params;
		manWearZOffset = link.manWearZOffset;
		modelid = template.modelid;
		womanHead = link.womanHead;
		if (link.inventoryOptions != null) {
			for (int i_2_ = 0; i_2_ < 4; i_2_++) {
				inventoryOptions[i_2_] = link.inventoryOptions[i_2_];
			}
		}
		inventoryOptions[4] = TranslatableString.aClass120_1203.getTranslation(myList.language);
	}

	public int[] createIconPixels(int i, ha var_ha, boolean bool, int i_5_, int i_6_, int i_7_, PlayerEquipment class402, Class55 class55, int i_8_, ha var_ha_9_) {
		Mesh class132 = Class296_Sub51_Sub1.fromJs5(myList.modelsJs5, modelid, 0);
		if (class132 == null) {
			return null;
		}
		if (class132.version_number < 13) {
			class132.scale(2);
		}
		if (recolourSrc != null) {
			for (int i_10_ = 0; i_10_ < recolourSrc.length; i_10_++) {
				if (clientPaletteIndices == null || clientPaletteIndices.length <= i_10_) {
					class132.recolour(recolourSrc[i_10_], recolourDst[i_10_]);
				} else {
					class132.recolour(recolourSrc[i_10_], Class296_Sub9.aShortArray4630[clientPaletteIndices[i_10_] & 0xff]);
				}
			}
		}
		if (retextureSrc != null) {
			for (int i_11_ = 0; i_11_ < retextureSrc.length; i_11_++) {
				class132.retexture(retextureSrc[i_11_], retextureDst[i_11_]);
			}
		}
		if (class402 != null) {
			for (int i_12_ = 0; i_12_ < 10; i_12_++) {
				for (int i_13_ = 0; CS2Call.aShortArrayArray4971[i_12_].length > i_13_; i_13_++) {
					if (Class42_Sub4.aShortArrayArrayArray3846[i_12_][i_13_].length > class402.body_colours[i_12_]) {
						class132.recolour(CS2Call.aShortArrayArray4971[i_12_][i_13_], Class42_Sub4.aShortArrayArrayArray3846[i_12_][i_13_][class402.body_colours[i_12_]]);
					}
				}
			}
		}
		int i_14_ = 2048;
		boolean bool_15_ = false;
		if (resizeX != 128 || resizeY != 128 || resizeZ != 128) {
			bool_15_ = true;
			i_14_ |= 0x7;
		}
		Model class178 = var_ha_9_.a(class132, i_14_, 64, ambient + 64, 768 + contrast);
		if (!class178.method1737()) {
			return null;
		}
		if (bool_15_) {
			class178.O(resizeX, resizeY, resizeZ);
		}
		Sprite class397 = null;
		if (certTemplate == -1) {
			if (lendTemplate != -1) {
				class397 = myList.getIcon(true, i_6_, var_ha_9_, 13, false, var_ha, class55, class402, i_5_, i_7_, 0, lentLink);
				if (class397 == null) {
					return null;
				}
			} else if (boughtTemplate != -1) {
				class397 = myList.getIcon(true, i_6_, var_ha_9_, 13, false, var_ha, class55, class402, i_5_, i_7_, 0, boughtLink);
				if (class397 == null) {
					return null;
				}
			}
		} else {
			class397 = myList.getIcon(true, 1, var_ha_9_, 13, true, var_ha, class55, class402, 0, 10, 0, certLink);
			if (class397 == null) {
				return null;
			}
		}
		int i_16_;
		if (bool) {
			i_16_ = (int) (zoom2d * 1.5) << 2;
		} else if (i_6_ != 2) {
			i_16_ = zoom2d << 2;
		} else {
			i_16_ = (int) (zoom2d * 1.04) << 2;
		}
		var_ha_9_.DA(16, 16, 512, 512);
		Class373 class373 = var_ha_9_.m();
		class373.method3910();
		var_ha_9_.a(class373);
		var_ha_9_.xa(1.0F);
		var_ha_9_.ZA(16777215, 1.0F, 1.0F, -50.0F, -10.0F, -50.0F);
		Class373 class373_17_ = var_ha_9_.f();
		class373_17_.method3911(-zAngle2d << 3);
		class373_17_.method3917(yAngle2d << 3);
		class373_17_.method3904(xOffest2d << 2, (Class296_Sub4.anIntArray4598[xAngle2d << 3] * i_16_ >> 14) - class178.fa() / 2 + (yOffest2d << 2), (yOffest2d << 2) + (Class296_Sub4.anIntArray4618[xAngle2d << 3] * i_16_ >> 14));
		class373_17_.method3914(xAngle2d << 3);
		int i_18_ = var_ha_9_.i();
		int i_19_ = var_ha_9_.XA();
		var_ha_9_.f(50, 2147483647);
		var_ha_9_.ya();
		var_ha_9_.la();
		var_ha_9_.aa(0, 0, 36, 32, 0, 0);
		class178.method1715(class373_17_, null, 1);
		var_ha_9_.f(i_18_, i_19_);
		int[] is = var_ha_9_.na(0, 0, 36, 32);
		if (i_6_ >= 1) {
			is = addOutline(is, -16777214);
			if (i_6_ >= 2) {
				is = addOutline(is, -1);
			}
		}
		if (i_5_ != 0) {
			changeBackground(is, i_5_);
		}
		var_ha_9_.method1096(103, 0, 32, 36, 36, is).method4096(0, 0);
		if (certTemplate == -1) {
			if (lendTemplate == -1) {
				if (boughtTemplate != -1) {
					class397.method4096(0, 0);
				}
			} else {
				class397.method4096(0, 0);
			}
		} else {
			class397.method4096(0, 0);
		}
		if (i == 1 || i == 2 && (stackability == 1 || i_7_ != 1) && i_7_ != -1) {
			class55.a(-16777215, 1659, -256, 0, getFormattedCount(i_7_), 9);
		}
		is = var_ha_9_.na(0, 0, 36, 32);
		for (int i_20_ = 0; is.length > i_20_; i_20_++) {
			if ((is[i_20_] & 0xffffff) == 0) {
				is[i_20_] = 0;
			} else {
				is[i_20_] = Class48.bitOR(is[i_20_], -16777216);
			}
		}
		return is;
	}

	private int[] addOutline(int[] pixels, int color) {
		int[] is_90_ = new int[1152];
		int i_91_ = 0;
		for (int i_92_ = 0; i_92_ < 32; i_92_++) {
			for (int i_93_ = 0; i_93_ < 36; i_93_++) {
				int i_94_ = pixels[i_91_];
				if (i_94_ == 0) {
					if (i_93_ > 0 && pixels[i_91_ - 1] != 0) {
						i_94_ = color;
					} else if (i_92_ <= 0 || pixels[i_91_ - 36] == 0) {
						if (i_93_ < 35 && pixels[i_91_ + 1] != 0) {
							i_94_ = color;
						} else if (i_92_ < 31 && pixels[i_91_ + 36] != 0) {
							i_94_ = color;
						}
					} else {
						i_94_ = color;
					}
				}
				is_90_[i_91_++] = i_94_;
			}
		}
		return is_90_;
	}

	private void changeBackground(int[] pixels, int color) {
		for (int i_22_ = 31; i_22_ > 0; i_22_--) {
			int i_23_ = i_22_ * 36;
			for (int i_24_ = 35; i_24_ > 0; i_24_--) {
				if (pixels[i_24_ + i_23_] == 0 && pixels[i_24_ - 1 + i_23_ - 36] != 0) {
					pixels[i_23_ + i_24_] = color;
				}
			}
		}
	}

	public Model getModel(int count, ha toolkit, Animator animator, int flag, byte i_99_, PlayerEquipment equipment) {
		if (stackObjs != null && count > 1) {
			int stack_obj = -1;
			for (int i_101_ = 0; i_101_ < 10; i_101_++) {
				if (stackCounts[i_101_] <= count && stackCounts[i_101_] != 0) {
					stack_obj = stackObjs[i_101_];
				}
			}
			if (stack_obj != -1) {
				return myList.list(stack_obj).getModel(1, toolkit, animator, flag, (byte) 4, equipment);
			}
		}
		int animator_flag = flag;
		if (animator != null) {
			animator_flag |= animator.method568(0);
		}
		if (i_99_ != 4) {
			return null;
		}
		Model model;
		synchronized (myList.modelCache) {
			model = (Model) myList.modelCache.get(id | toolkit.anInt1295 << 29);
		}
		if (model == null || toolkit.e(model.ua(), animator_flag) != 0) {
			if (model != null) {
				animator_flag = toolkit.d(animator_flag, model.ua());
			}
			int rendersettings = animator_flag;
			if (retextureSrc != null) {
				rendersettings |= 0x8000;
			}
			if (recolourSrc != null || equipment != null) {
				rendersettings |= 0x4000;
			}
			if (resizeX != 128) {
				rendersettings |= 0x1;
			}
			if (resizeX != 128) {
				rendersettings |= 0x2;
			}
			if (resizeX != 128) {
				rendersettings |= 0x4;
			}
			Mesh class132 = Class296_Sub51_Sub1.fromJs5(myList.modelsJs5, modelid, 0);
			if (class132 == null) {
				return null;
			}
			if (class132.version_number < 13) {
				class132.scale(2);
			}
			model = toolkit.a(class132, rendersettings, myList.detail_flags, 64 + ambient, contrast + 850);
			if (resizeX != 128 || resizeY != 128 || resizeZ != 128) {
				model.O(resizeX, resizeY, resizeZ);
			}
			if (recolourSrc != null) {
				for (int i_104_ = 0; recolourSrc.length > i_104_; i_104_++) {
					if (clientPaletteIndices == null || clientPaletteIndices.length <= i_104_) {
						model.ia(recolourSrc[i_104_], recolourDst[i_104_]);
					} else {
						model.ia(recolourSrc[i_104_], Class296_Sub9.aShortArray4630[clientPaletteIndices[i_104_] & 0xff]);
					}
				}
			}
			if (retextureSrc != null) {
				for (int i_105_ = 0; i_105_ < retextureSrc.length; i_105_++) {
					model.aa(retextureSrc[i_105_], retextureDst[i_105_]);
				}
			}
			if (equipment != null) {
				for (int i_106_ = 0; i_106_ < 10; i_106_++) {
					for (int i_107_ = 0; CS2Call.aShortArrayArray4971[i_106_].length > i_107_; i_107_++) {
						if (equipment.body_colours[i_106_] < Class42_Sub4.aShortArrayArrayArray3846[i_106_][i_107_].length) {
							model.ia(CS2Call.aShortArrayArray4971[i_106_][i_107_], Class42_Sub4.aShortArrayArrayArray3846[i_106_][i_107_][equipment.body_colours[i_106_]]);
						}
					}
				}
			}
			model.s(animator_flag);
			synchronized (myList.modelCache) {
				myList.modelCache.put(model, toolkit.anInt1295 << 29 | id);
			}
		}
		if (animator != null) {
			model = model.method1728((byte) 1, animator_flag, true);
			animator.method556(model, 0, (byte) -63);
		}
		model.s(flag);
		return model;
	}

	public Mesh getEquipMesh(boolean female, ObjCustomisation customisation) {
		int equip1_id;
		int equip2_id;
		int equip3_id;
		if (!female) {
			if (customisation != null && customisation.maleEquipModel != null) {
				equip1_id = customisation.maleEquipModel[0];
				equip2_id = customisation.maleEquipModel[1];
				equip3_id = customisation.maleEquipModel[2];
			} else {
				equip1_id = maleEquip1;
				equip2_id = maleEquip2;
				equip3_id = maleEquip3;
			}
		} else if (customisation != null && customisation.femaleEquipModel != null) {
			equip3_id = customisation.femaleEquipModel[2];
			equip2_id = customisation.femaleEquipModel[1];
			equip1_id = customisation.femaleEquipModel[0];
		} else {
			equip1_id = femaleEquip1;
			equip2_id = femaleEquip2;
			equip3_id = femaleEquip3;
		}
		if (equip1_id == -1) {
			return null;
		}
		Mesh mesh = Class296_Sub51_Sub1.fromJs5(myList.modelsJs5, equip1_id, 0);
		if (mesh == null) {
			return null;
		}
		if (mesh.version_number < 13) {
			mesh.scale(2);
		}
		if (equip2_id != -1) {
			Mesh equip2 = Class296_Sub51_Sub1.fromJs5(myList.modelsJs5, equip2_id, 0);
			if (equip2.version_number < 13) {
				equip2.scale(2);
			}
			if (equip3_id != -1) {
				Mesh equip3 = Class296_Sub51_Sub1.fromJs5(myList.modelsJs5, equip3_id, 0);
				if (equip3.version_number < 13) {
					equip3.scale(2);
				}
				Mesh[] class132s = { mesh, equip2, equip3 };
				mesh = new Mesh(class132s, 3);
			} else {
				Mesh[] class132s = { mesh, equip2 };
				mesh = new Mesh(class132s, 2);
			}
		}
		if (!female && (manWearXOffset != 0 || manWearYOffset != 0 || manWearZOffset != 0)) {
			mesh.translate(manWearXOffset, manWearZOffset, manWearYOffset);
		}
		if (female && (womanWearXOffset != 0 || womanWearYOffset != 0 || womanWearZOffset != 0)) {
			mesh.translate(womanWearXOffset, womanWearZOffset, womanWearYOffset);
		}
		if (recolourSrc != null) {
			short[] is;
			if (customisation != null && customisation.recolourDst != null) {
				is = customisation.recolourDst;
			} else {
				is = recolourDst;
			}
			for (int index = 0; index < recolourSrc.length; index++) {
				mesh.recolour(recolourSrc[index], is[index]);
			}
		}
		if (retextureSrc != null) {
			short[] retexture_dst;
			if (customisation == null || customisation.retextureDst == null) {
				retexture_dst = retextureDst;
			} else {
				retexture_dst = customisation.retextureDst;
			}
			for (int index = 0; index < retextureSrc.length; index++) {
				mesh.retexture(retextureSrc[index], retexture_dst[index]);
			}
		}
		return mesh;
	}

	public boolean checkCustomize(boolean female, ObjCustomisation customisation) {
		int equip_id1;
		int equip_id2;
		int equip_id3;
		if (female) {
			if (customisation == null || customisation.femaleEquipModel == null) {
				equip_id1 = femaleEquip1;
				equip_id2 = femaleEquip2;
				equip_id3 = femaleEquip3;
			} else {
				equip_id1 = customisation.femaleEquipModel[0];
				equip_id2 = customisation.femaleEquipModel[1];
				equip_id3 = customisation.femaleEquipModel[2];
			}
		} else if (customisation == null || customisation.maleEquipModel == null) {
			equip_id1 = maleEquip1;
			equip_id2 = maleEquip2;
			equip_id3 = maleEquip3;
		} else {
			equip_id1 = customisation.maleEquipModel[0];
			equip_id2 = customisation.maleEquipModel[1];
			equip_id3 = customisation.maleEquipModel[2];
		}
		if (equip_id1 == -1) {
			return true;
		}
		boolean ready = true;
		if (!myList.modelsJs5.fileExists(equip_id1, 0)) {
			ready = false;
		}
		if (equip_id2 != -1 && !myList.modelsJs5.fileExists(equip_id2, 0)) {
			ready = false;
		}
		if (equip_id3 != -1 && !myList.modelsJs5.fileExists(equip_id3, 0)) {
			ready = false;
		}
		return ready;
	}

	public Mesh getHeadMesh(boolean female, ObjCustomisation customisation) {
		int headmodel_id1;
		int headmodel_id2;
		if (female) {
			if (customisation == null || customisation.femaleHeadModels == null) {
				headmodel_id2 = womanHead2;
				headmodel_id1 = womanHead;
			} else {
				headmodel_id1 = customisation.femaleHeadModels[0];
				headmodel_id2 = customisation.femaleHeadModels[1];
			}
		} else if (customisation == null || customisation.maleHeadModels == null) {
			headmodel_id1 = manHead;
			headmodel_id2 = manHead2;
		} else {
			headmodel_id1 = customisation.maleHeadModels[0];
			headmodel_id2 = customisation.maleHeadModels[1];
		}
		if (headmodel_id1 == -1) {
			return null;
		}
		Mesh head_mesh = Class296_Sub51_Sub1.fromJs5(myList.modelsJs5, headmodel_id1, 0);
		if (head_mesh.version_number < 13) {
			head_mesh.scale(2);
		}
		if (headmodel_id2 != -1) {
			Mesh headmesh2 = Class296_Sub51_Sub1.fromJs5(myList.modelsJs5, headmodel_id2, 0);
			if (headmesh2.version_number < 13) {
				headmesh2.scale(2);
			}
			Mesh[] class132s = { head_mesh, headmesh2 };
			head_mesh = new Mesh(class132s, 2);
		}
		if (recolourSrc != null) {
			short[] recolour_dst;
			if (customisation != null && customisation.recolourDst != null) {
				recolour_dst = customisation.recolourDst;
			} else {
				recolour_dst = recolourDst;
			}
			for (int index = 0; index < recolourSrc.length; index++) {
				head_mesh.recolour(recolourSrc[index], recolour_dst[index]);
			}
		}
		if (retextureSrc != null) {
			short[] retextureDst;
			if (customisation == null || customisation.retextureDst == null) {
				retextureDst = this.retextureDst;
			} else {
				retextureDst = customisation.retextureDst;
			}
			for (int index = 0; index < retextureSrc.length; index++) {
				head_mesh.retexture(retextureSrc[index], retextureDst[index]);
			}
		}
		return head_mesh;
	}

	public boolean isHeadMeshReady(boolean female, ObjCustomisation customisation) {
		int head_id1;
		int head_id2;
		if (female) {
			if (customisation == null || customisation.femaleHeadModels == null) {
				head_id1 = womanHead;
				head_id2 = womanHead2;
			} else {
				head_id1 = customisation.femaleHeadModels[0];
				head_id2 = customisation.femaleHeadModels[1];
			}
		} else if (customisation == null || customisation.maleHeadModels == null) {
			head_id1 = manHead;
			head_id2 = manHead2;
		} else {
			head_id1 = customisation.maleHeadModels[0];
			head_id2 = customisation.maleHeadModels[1];
		}
		if (head_id1 == -1) {
			return true;
		}
		boolean ready = true;
		if (!myList.modelsJs5.fileExists(head_id1, 0)) {
			ready = false;
		}
		if (head_id2 != -1 && !myList.modelsJs5.fileExists(head_id2, 0)) {
			ready = false;
		}
		return ready;
	}

	public String getStringParam(int paramId, String defaultValue) {
		if (params == null) {
			return defaultValue;
		}
		StringNode value = (StringNode) params.get(paramId);
		if (value == null) {
			return defaultValue;
		}
		return value.value;
	}

	public int getIntParam(int paramId, int defaultValue) {
		if (params == null) {
			return defaultValue;
		}
		IntegerNode value = (IntegerNode) params.get(paramId);
		if (value == null) {
			return defaultValue;
		}
		return value.value;
	}

	public ObjType getStackObj(int count) {
		if (stackObjs != null && count > 1) {
			int stack_obj = -1;
			for (int index = 0; index < 10; index++) {
				if (stackCounts[index] != 0 && count >= stackCounts[index]) {
					stack_obj = stackObjs[index];
				}
			}
			if (stack_obj != -1) {
				return myList.list(stack_obj);
			}
		}
		return this;
	}

	private String getFormattedCount(int count) {
		if (count < 100000) {
			return "<col=ffff00>" + count + "</col>";
		}
		if (count < 10000000) {
			return "<col=ffffff>" + count / 1000 + TranslatableString.aClass120_1232.getTranslation(myList.language) + "</col>";
		}
		return "<col=00ff80>" + count / 1000000 + TranslatableString.aClass120_1230.getTranslation(myList.language) + "</col>";
	}

}
