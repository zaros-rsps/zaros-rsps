package net.zaros.client.configs.objtype;

import net.zaros.client.Class291;

public class ObjCustomisation {

	public int[] maleEquipModel;
	public int[] femaleEquipModel;
	public int[] maleHeadModels;
	public int[] femaleHeadModels;
	public short[] recolourDst;
	public short[] retextureDst;

	public ObjCustomisation(ObjType type) {
		maleEquipModel = new int[3];
		femaleEquipModel = new int[3];
		maleHeadModels = new int[2];
		femaleHeadModels = new int[2];
		maleEquipModel[0] = type.maleEquip1;
		maleEquipModel[1] = type.maleEquip2;
		maleEquipModel[2] = type.maleEquip3;
		femaleEquipModel[0] = type.femaleEquip1;
		femaleEquipModel[1] = type.femaleEquip2;
		femaleEquipModel[2] = type.femaleEquip3;
		maleHeadModels[0] = type.manHead;
		maleHeadModels[1] = type.manHead2;
		femaleHeadModels[0] = type.womanHead;
		femaleHeadModels[1] = type.womanHead2;
		if (type.recolourDst != null) {
			recolourDst = new short[type.recolourDst.length];
			Class291.copy(type.recolourDst, 0, recolourDst, 0, recolourDst.length);
		}
		if (type.retextureDst != null) {
			retextureDst = new short[type.retextureDst.length];
			Class291.copy(type.retextureDst, 0, retextureDst, 0, retextureDst.length);
		}
	}
}
