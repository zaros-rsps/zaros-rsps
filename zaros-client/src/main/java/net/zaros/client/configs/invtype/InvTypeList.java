package net.zaros.client.configs.invtype;

import net.zaros.client.AdvancedMemoryCache;
import net.zaros.client.GameType;
import net.zaros.client.Js5;
import net.zaros.client.Packet;

public class InvTypeList {

	private AdvancedMemoryCache recentUse = new AdvancedMemoryCache(64);
	private Js5 configsJs5;

	public InvTypeList(GameType gameType, int language, Js5 configsJs5) {
		this.configsJs5 = configsJs5;
	}

	public InvType list(int invid) {
		InvType type;
		synchronized (recentUse) {
			type = (InvType) recentUse.get(invid);
		}
		if (type != null) {
			return type;
		}
		byte[] data;
		synchronized (configsJs5) {
			data = configsJs5.getFile(5, invid);
		}
		type = new InvType();
		if (data != null) {
			type.decode(new Packet(data));
		}
		synchronized (recentUse) {
			recentUse.put(type, invid);
		}
		return type;
	}
}
