package net.zaros.client.configs.objtype;

import net.zaros.client.Interface12;
import net.zaros.client.TextureOperation;

public class ObjIconRequest implements Interface12 {

	public int anInt3634;
	public int anInt3636;
	public int anInt3638;
	public int anInt3639;
	public int anInt3640;
	public boolean aBoolean3641;
	public int anInt3642;

	@Override
	public final long method51(byte i) {
		long[] ls = TextureOperation.someHashTable;
		long l = -1L;
		l = ls[(int) ((anInt3639 ^ l) & 0xffL)] ^ l >>> 8;
		l = l >>> 8 ^ ls[(int) ((l ^ anInt3642 >> 8) & 0xffL)];
		l = ls[(int) ((l ^ anInt3642) & 0xffL)] ^ l >>> 8;
		l = l >>> 8 ^ ls[(int) ((anInt3640 >> 24 ^ l) & 0xffL)];
		int i_0_ = -83 / ((i + 3) / 59);
		l = ls[(int) ((l ^ anInt3640 >> 16) & 0xffL)] ^ l >>> 8;
		l = l >>> 8 ^ ls[(int) ((anInt3640 >> 8 ^ l) & 0xffL)];
		l = ls[(int) ((anInt3640 ^ l) & 0xffL)] ^ l >>> 8;
		l = ls[(int) ((anInt3636 ^ l) & 0xffL)] ^ l >>> 8;
		l = l >>> 8 ^ ls[(int) ((l ^ anInt3634 >> 24) & 0xffL)];
		l = l >>> 8 ^ ls[(int) ((anInt3634 >> 16 ^ l) & 0xffL)];
		l = ls[(int) ((anInt3634 >> 8 ^ l) & 0xffL)] ^ l >>> 8;
		l = ls[(int) ((anInt3634 ^ l) & 0xffL)] ^ l >>> 8;
		l = ls[(int) ((anInt3638 ^ l) & 0xffL)] ^ l >>> 8;
		l = l >>> 8 ^ ls[(int) (((aBoolean3641 ? 1 : 0) ^ l) & 0xffL)];
		return l;
	}

	@Override
	public final boolean method50(byte i, Interface12 interface12) {
		if (!(interface12 instanceof ObjIconRequest)) {
			return false;
		}
		ObjIconRequest class233_8_ = (ObjIconRequest) interface12;
		if (anInt3639 != class233_8_.anInt3639) {
			return false;
		}
		int i_9_ = 17 % ((50 - i) / 46);
		if (anInt3642 != class233_8_.anInt3642) {
			return false;
		}
		if (class233_8_.anInt3640 != anInt3640) {
			return false;
		}
		if (class233_8_.anInt3636 != anInt3636) {
			return false;
		}
		if (anInt3634 != class233_8_.anInt3634) {
			return false;
		}
		if (class233_8_.anInt3638 != anInt3638) {
			return false;
		}
		if (!aBoolean3641 != !class233_8_.aBoolean3641) {
			return false;
		}
		return true;
	}
}
