package net.zaros.client.configs.invtype;


import net.zaros.client.Packet;
import net.zaros.client.Queuable;

public class InvType extends Queuable {

	public int capacity = 0;

	public void decode(Packet packet) {
		for (;;) {
			int opcode = packet.g1();
			if (opcode == 0) {
				break;
			}
			decode(packet, opcode);
		}
	}

	private void decode(Packet buffer, int opcode) {
		if (opcode == 2) {
			capacity = buffer.g2();
		}
	}
}
