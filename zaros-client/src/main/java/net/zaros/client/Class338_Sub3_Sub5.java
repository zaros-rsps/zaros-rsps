package net.zaros.client;

/* Class338_Sub3_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class338_Sub3_Sub5 extends Class338_Sub3 {
	static int rights = 0;
	static int anInt6572;
	short aShort6573;

	static final Class69_Sub1_Sub1 method3583(byte[] is, ha_Sub3 var_ha_Sub3, int i, int i_0_, int i_1_, int i_2_, boolean bool, int i_3_) {
		if (i_2_ > -109)
			return null;
		if (!var_ha_Sub3.aBoolean4219 && (!Class30.method329(2844, i_1_) || !Class30.method329(2844, i))) {
			if (!var_ha_Sub3.aBoolean4257)
				return new Class69_Sub1_Sub1(var_ha_Sub3, i_0_, i_1_, i, Class8.get_next_high_pow2(i_1_), Class8.get_next_high_pow2(i), is, i_3_);
			return new Class69_Sub1_Sub1(var_ha_Sub3, 34037, i_0_, i_1_, i, bool, is, i_3_);
		}
		return new Class69_Sub1_Sub1(var_ha_Sub3, 3553, i_0_, i_1_, i, bool, is, i_3_);
	}

	final int method3463(int i, Class296_Sub35[] class296_sub35s) {
		if (i != -1)
			return -106;
		return this.method3474(tileX >> Class313.anInt2779, class296_sub35s, tileY >> Class313.anInt2779, 4);
	}

	final boolean method3465(int i) {
		if (i != 0)
			return false;
		return (Class296_Sub15_Sub2.aBooleanArrayArray6006[((tileX >> Class313.anInt2779) - Class296_Sub45_Sub2.anInt6288 + Class379_Sub2.anInt5684)][(Class379_Sub2.anInt5684 + ((tileY >> Class313.anInt2779) - Class296_Sub39_Sub20_Sub2.anInt6728))]);
	}

	final boolean method3470(int i, ha var_ha) {
		if (i != -30488)
			return true;
		return Class50.method614((byte) -117, tileX >> Class313.anInt2779, tileY >> Class313.anInt2779, aByte5203);
	}

	Class338_Sub3_Sub5(int i, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		tileX = i;
		tileY = i_5_;
		aShort6573 = (short) i_8_;
		anInt5213 = i_4_;
		aByte5203 = (byte) i_7_;
		z = (byte) i_6_;
	}
}
