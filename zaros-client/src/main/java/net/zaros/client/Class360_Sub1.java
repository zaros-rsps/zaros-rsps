package net.zaros.client;
import jagdx.IDirect3DDevice;
import jagdx.IDirect3DPixelShader;
import jagdx.IDirect3DVertexShader;

final class Class360_Sub1 extends Class360 {
	private boolean aBoolean5295;
	private IDirect3DPixelShader anIDirect3DPixelShader5296;
	private boolean aBoolean5297;
	private IDirect3DVertexShader anIDirect3DVertexShader5298;
	private Class184 aClass184_5299;
	private ha_Sub1_Sub2 aHa_Sub1_Sub2_5300;
	private static float[] aFloatArray5301 = new float[16];

	final void method3731(boolean bool, byte i) {
		if (i != -71)
			method3723((byte) -21);
	}

	final boolean method3723(byte i) {
		int i_0_ = 49 / ((-49 - i) / 36);
		return aBoolean5297;
	}

	final void method3732(int i, int i_1_, int i_2_) {
		if (i_1_ >= -6)
			method3720(18);
		if (aBoolean5295) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5300.anIDirect3DDevice5865;
			int i_3_ = 1 << (i_2_ & 0x3);
			float f = (float) (1 << ((i_2_ & 0x3a) >> 3)) / 32.0F;
			int i_4_ = i & 0xffff;
			float f_5_ = (float) (i >> 16 & 0x3) / 8.0F;
			idirect3ddevice.b(14, ((float) (i_3_ * aHa_Sub1_3093.anInt4006 % 40000) / 40000.0F), 0.0F, 0.0F, 0.0F);
			idirect3ddevice.b(15, f, 0.0F, 0.0F, 0.0F);
			idirect3ddevice.a(4, (float) i_4_, 0.0F, 0.0F, 0.0F);
			idirect3ddevice.a(5, f_5_, 0.0F, 0.0F, 0.0F);
		}
	}

	final void method3733(byte i, boolean bool) {
		if (i > -125)
			method3724((byte) -54);
		Interface6_Impl3 interface6_impl3 = aHa_Sub1_3093.method1116((byte) -72);
		if (aBoolean5297 && interface6_impl3 != null) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5300.anIDirect3DDevice5865;
			aHa_Sub1_Sub2_5300.method1248(1326468944, anIDirect3DVertexShader5298);
			aHa_Sub1_Sub2_5300.method1241((byte) 79, anIDirect3DPixelShader5296);
			aHa_Sub1_3093.method1151(1, 16760);
			aHa_Sub1_3093.method1140(interface6_impl3, false);
			aHa_Sub1_3093.method1151(0, 16760);
			aHa_Sub1_3093.method1140(aClass184_5299.anInterface6_Impl2_1885, false);
			aBoolean5295 = true;
			method3720(8250);
			method3730(false);
			method3734(-13412);
			method3719((byte) 91);
			idirect3ddevice.a(1, -aHa_Sub1_3093.aFloatArray4030[0], -aHa_Sub1_3093.aFloatArray4030[1], -aHa_Sub1_3093.aFloatArray4030[2], 0.0F);
			idirect3ddevice.a(2, aHa_Sub1_3093.aFloat4000, aHa_Sub1_3093.aFloat4019, aHa_Sub1_3093.aFloat4043, 1.0F);
			idirect3ddevice.a(3, (Math.abs(aHa_Sub1_3093.aFloatArray4030[1]) * 928.0F) + 96.0F, 0.0F, 0.0F, 0.0F);
		}
	}

	final void method3720(int i) {
		if (aBoolean5295) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5300.anIDirect3DDevice5865;
			Class373_Sub2 class373_sub2 = aHa_Sub1_Sub2_5300.method1174(i ^ 0xdf3a);
			Class373_Sub2 class373_sub2_6_ = aHa_Sub1_Sub2_5300.method1122(i - 8126);
			idirect3ddevice.a(0, class373_sub2_6_.method3944(aFloatArray5301, (byte) 31));
			idirect3ddevice.a(4, class373_sub2.method3949(aFloatArray5301, i ^ 0x203a));
		}
		if (i != 8250)
			method3725(-69);
	}

	final void method3734(int i) {
		if (aBoolean5295) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5300.anIDirect3DDevice5865;
			idirect3ddevice.a(8, aHa_Sub1_3093.method1182(1, aFloatArray5301));
		}
		if (i != -13412)
			method3724((byte) 44);
	}

	Class360_Sub1(ha_Sub1_Sub2 var_ha_Sub1_Sub2, Js5 class138, Class184 class184) {
		super(var_ha_Sub1_Sub2);
		aHa_Sub1_Sub2_5300 = var_ha_Sub1_Sub2;
		aClass184_5299 = class184;
		if (class138 == null || !aHa_Sub1_3093.aBoolean3977 || !aHa_Sub1_3093.aBoolean3968 || ((aHa_Sub1_Sub2_5300.aD3DCAPS5858.VertexShaderVersion & 0xffff) < 257)) {
			anIDirect3DPixelShader5296 = null;
			anIDirect3DVertexShader5298 = null;
			aBoolean5297 = false;
		} else {
			anIDirect3DVertexShader5298 = (aHa_Sub1_Sub2_5300.anIDirect3DDevice5865.a(class138.getFile_("environment_mapped_water_v", "dx")));
			anIDirect3DPixelShader5296 = (aHa_Sub1_Sub2_5300.anIDirect3DDevice5865.b(class138.getFile_("environment_mapped_water_f", "dx")));
			aBoolean5297 = (anIDirect3DVertexShader5298 != null && anIDirect3DPixelShader5296 != null && aClass184_5299.method1858((byte) -87));
		}
	}

	final void method3724(byte i) {
		if (i != -50)
			method3719((byte) -119);
		if (aBoolean5295) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5300.anIDirect3DDevice5865;
			Class373_Sub2 class373_sub2 = aHa_Sub1_Sub2_5300.method1122(i ^ ~0x5d);
			idirect3ddevice.a(0, class373_sub2.method3944(aFloatArray5301, (byte) 31));
		}
	}

	final void method3719(byte i) {
		if (aBoolean5295) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5300.anIDirect3DDevice5865;
			if (aHa_Sub1_3093.anInt3990 <= 0)
				idirect3ddevice.b(16, 0.0F, 0.0F, 0.0F, 0.0F);
			else {
				float f = aHa_Sub1_3093.aFloat3964;
				float f_7_ = aHa_Sub1_3093.aFloat3989;
				idirect3ddevice.b(16, f, 1.0F / (-f_7_ + f), 0.0F, 0.0F);
			}
			idirect3ddevice.a(0, (float) (aHa_Sub1_3093.anInt3947 >> 16 & 0xff) / 255.0F, (float) ((aHa_Sub1_3093.anInt3947 & 0xff66) >> 8) / 255.0F, (float) (aHa_Sub1_3093.anInt3947 & 0xff) / 255.0F, 0.0F);
		}
		int i_8_ = -48 % ((-37 - i) / 43);
	}

	final void method3725(int i) {
		int i_9_ = 124 / ((i - 58) / 56);
		if (aBoolean5295) {
			aHa_Sub1_Sub2_5300.method1248(1326468944, null);
			aHa_Sub1_Sub2_5300.method1241((byte) 79, null);
			aHa_Sub1_3093.method1151(1, 16760);
			aHa_Sub1_3093.method1140(null, false);
			aHa_Sub1_3093.method1151(0, 16760);
			aHa_Sub1_3093.method1140(null, false);
			aBoolean5295 = false;
		}
	}

	final void method3736(byte i, Interface6 interface6, int i_10_) {
		int i_11_ = 15 % ((i - 72) / 49);
	}

	final void method3730(boolean bool) {
		if (!bool) {
			if (aBoolean5295) {
				IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5300.anIDirect3DDevice5865;
				Class373_Sub2 class373_sub2 = aHa_Sub1_3093.method1226(-23501);
				idirect3ddevice.SetVertexShaderConstantF(12, class373_sub2.method3945(aFloatArray5301, (byte) 88), 2);
			}
		}
	}
}
