package net.zaros.client;
/* Class78 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.lang.reflect.Field;

final class Class78 implements Interface2 {
	private FontMetrics aFontMetrics3403;
	private Font aFont3404;
	private Image anImage3405;
	private int anInt3406;
	private boolean aBoolean3407;
	private int anInt3408;
	private Image anImage3409;
	private int anInt3410;
	static Class209 aClass209_3411 = new Class209(12);
	private int anInt3412;
	private Image anImage3413;
	private int anInt3414;
	private int anInt3415;
	private Image anImage3416;
	private boolean aBoolean3417;
	private Image anImage3418;
	private Image anImage3419;
	private Image anImage3420;
	private int anInt3421;
	private Color aColor3422;
	private Image anImage3423;
	private Image anImage3424;
	private int anInt3425;
	private int anInt3426;
	static OutgoingPacket aClass311_3427 = new OutgoingPacket(10, 3);
	private boolean aBoolean3428;
	private Image anImage3429;
	static volatile boolean aBoolean3430;
	static int anInt3431 = 0;

	public final int method9(int i) {
		if (i != 4739)
			method7(93L, (byte) 49);
		return 100;
	}

	private final int method792(byte i, int i_0_) {
		if (aBoolean3428)
			return (Class241.anInt2301 - i_0_) / 2;
		if (i != -8)
			method793(77);
		return 0;
	}

	public static void method793(int i) {
		if (i < 113)
			method793(79);
		aClass311_3427 = null;
		aClass209_3411 = null;
	}

	private final boolean method794(String string, Class var_class, Object object, int i) throws IllegalAccessException, NoSuchFieldException {
		if (i != -10118)
			method9(114);
		Field field = var_class.getDeclaredField(string);
		return field.getBoolean(object);
	}

	private final void method795(byte i) {
		if (i > 93)
			Class296_Sub29_Sub2.method2698(Class140.aColorArray3587[Class10.colorID], (byte) 96, Class357.aColorArray3083[Class10.colorID], Class296_Sub51_Sub15.aClass54_6426.method651(true), ParticleEmitterRaw.aColorArray1756[Class10.colorID], Class296_Sub51_Sub15.aClass54_6426.method654(false));
	}

	public final int method6(byte i) {
		if (i <= 27)
			method8((byte) -3, true);
		return 0;
	}

	public final void method11(boolean bool) {
		Class296_Sub51_Sub26.method3151(-117);
		if (bool) {
			/* empty */
		}
	}

	private final int method796(byte i, int i_1_) {
		if (i <= 19)
			method793(-82);
		if (aBoolean3417)
			return (Class384.anInt3254 - i_1_) / 2;
		return 0;
	}

	static final void method797(boolean bool, int i, int i_2_, int i_3_, int i_4_) {
		if (InterfaceComponent.loadInterface(i_4_)) {
			if (i_3_ != -10111)
				aClass311_3427 = null;
			Class37.method364(Class192.openedInterfaceComponents[i_4_], i, bool, -1, (byte) 117, i_2_);
		}
	}

	private final void method798(int i) throws IllegalAccessException, NoSuchFieldException {
		if (i > 15) {
			Class var_class = CS2Script.anApplet6140.getClass();
			anImage3418 = (Image) method800(var_class, "bar", -4);
			anImage3405 = (Image) method800(var_class, "background", -4);
			anImage3416 = (Image) method800(var_class, "left", -4);
			anImage3420 = (Image) method800(var_class, "right", -4);
			anImage3423 = (Image) method800(var_class, "top", -4);
			anImage3424 = (Image) method800(var_class, "bottom", -4);
			anImage3429 = (Image) method800(var_class, "bodyLeft", -4);
			anImage3409 = (Image) method800(var_class, "bodyRight", -4);
			anImage3419 = (Image) method800(var_class, "bodyFill", -4);
			aFont3404 = (Font) method800(var_class, "bf", -4);
			aFontMetrics3403 = (FontMetrics) method800(var_class, "bfm", -4);
			aColor3422 = (Color) method800(var_class, "colourtext", -4);
			Object object = method800(var_class, "lb", -4);
			Class var_class_5_ = object.getClass();
			aBoolean3428 = method794("xMiddle", var_class_5_, object, -10118);
			aBoolean3417 = method794("yMiddle", var_class_5_, object, -10118);
			anInt3415 = method799(2, "xOffset", var_class_5_, object);
			anInt3426 = method799(2, "yOffset", var_class_5_, object);
			anInt3406 = method799(2, "width", var_class_5_, object);
			anInt3414 = method799(2, "height", var_class_5_, object);
			anInt3425 = method799(2, "boxXOffset", var_class_5_, object);
			anInt3412 = method799(2, "boxYOffset", var_class_5_, object);
			anInt3408 = method799(2, "boxWidth", var_class_5_, object);
			anInt3410 = method799(2, "textYOffset", var_class_5_, object);
			anInt3421 = method799(2, "offsetPerTenCycles", var_class_5_, object);
		}
	}

	public final boolean method7(long l, byte i) {
		if (i > -103)
			return true;
		return true;
	}

	private final int method799(int i, String string, Class var_class, Object object) throws IllegalAccessException, NoSuchFieldException {
		if (i != 2)
			method797(true, -92, -66, 24, -23);
		Field field = var_class.getDeclaredField(string);
		return field.getInt(object);
	}

	public final void method8(byte i, boolean bool) {
		if (!aBoolean3407) {
			if (CS2Script.anApplet6140 == null)
				aBoolean3407 = true;
			else if (aFont3404 == null) {
				try {
					method798(34);
				} catch (Exception exception) {
					aBoolean3407 = true;
				}
			}
		}
		if (i != -127)
			anInt3408 = -91;
		if (aBoolean3407)
			method795((byte) 99);
		else {
			Graphics graphics = Class230.aCanvas2209.getGraphics();
			if (graphics != null) {
				try {
					int i_6_ = Class296_Sub51_Sub15.aClass54_6426.method654(false);
					String string = Class296_Sub51_Sub15.aClass54_6426.method651(true);
					if (Class387.anImage3713 == null)
						Class387.anImage3713 = Class230.aCanvas2209.createImage((Class241.anInt2301), (Class384.anInt3254));
					Graphics graphics_7_ = Class387.anImage3713.getGraphics();
					graphics_7_.clearRect(0, 0, Class241.anInt2301, Class384.anInt3254);
					int i_8_ = anImage3429.getWidth(null);
					int i_9_ = anImage3409.getWidth(null);
					int i_10_ = anImage3419.getWidth(null);
					int i_11_ = anImage3429.getHeight(null);
					int i_12_ = anImage3409.getHeight(null);
					int i_13_ = anImage3419.getHeight(null);
					graphics_7_.drawImage(anImage3429, (method792((byte) -8, i_8_) + anInt3425 - anInt3408 / 2), (method796((byte) 105, i_11_) + anInt3412), null);
					int i_14_ = i_8_ + (-(anInt3408 / 2) + anInt3425);
					int i_15_ = anInt3425 + anInt3408 / 2;
					for (int i_16_ = i_14_; i_15_ >= i_16_; i_16_ += i_10_)
						graphics_7_.drawImage(anImage3419, i_16_ + (method792((byte) -8, i_8_) + anInt3425), (method796((byte) 106, i_13_) + anInt3412), null);
					graphics_7_.drawImage(anImage3409, (method792((byte) -8, i_9_) + (anInt3425 + anInt3408 / 2)), (method796((byte) 60, i_12_) + anInt3412), null);
					int i_17_ = anImage3416.getWidth(null);
					int i_18_ = anImage3416.getHeight(null);
					int i_19_ = anImage3420.getWidth(null);
					int i_20_ = anImage3420.getHeight(null);
					int i_21_ = anImage3424.getHeight(null);
					int i_22_ = anImage3423.getWidth(null);
					int i_23_ = anImage3423.getHeight(null);
					int i_24_ = anImage3424.getWidth(null);
					int i_25_ = anImage3418.getWidth(null);
					int i_26_ = anImage3405.getWidth(null);
					int i_27_ = method792((byte) -8, anInt3406) + anInt3415;
					int i_28_ = method796((byte) 113, anInt3414) + anInt3426;
					graphics_7_.drawImage(anImage3416, i_27_, i_28_ + (-i_18_ + anInt3414) / 2, null);
					graphics_7_.drawImage(anImage3420, -i_19_ + (i_27_ + anInt3406), (anInt3414 - i_20_) / 2 + i_28_, null);
					if (anImage3413 == null)
						anImage3413 = Class230.aCanvas2209.createImage((-i_19_ + anInt3406 - i_17_), anInt3414);
					Graphics graphics_29_ = anImage3413.getGraphics();
					for (int i_30_ = 0; i_30_ < -i_19_ - i_17_ + anInt3406; i_30_ += i_22_)
						graphics_29_.drawImage(anImage3423, i_30_, 0, null);
					for (int i_31_ = 0; i_31_ < -i_19_ + anInt3406 - i_17_; i_31_ = i_31_ + i_24_)
						graphics_29_.drawImage(anImage3424, i_31_, anInt3414 - i_21_, null);
					int i_32_ = (-i_19_ + anInt3406 - i_17_) * i_6_ / 100;
					if (i_32_ > 0) {
						Image image = Class230.aCanvas2209.createImage(i_32_, (anInt3414 - (i_23_ + i_21_)));
						int i_33_ = image.getWidth(null);
						Graphics graphics_34_ = image.getGraphics();
						int i_35_ = (anInt3421 * Class360_Sub9.method3749(71) / 10 % i_25_);
						for (int i_36_ = i_35_ - i_25_; i_33_ > i_36_; i_36_ += i_25_)
							graphics_34_.drawImage(anImage3418, i_36_, 0, null);
						graphics_29_.drawImage(image, 0, i_23_, null);
					}
					int i_37_ = i_32_;
					i_32_ = -i_32_ + (-i_19_ + (-i_17_ + anInt3406));
					if (i_32_ > 0) {
						Image image = Class230.aCanvas2209.createImage(i_32_, (-i_21_ + anInt3414 - i_23_));
						int i_38_ = image.getWidth(null);
						Graphics graphics_39_ = image.getGraphics();
						for (int i_40_ = 0; i_40_ < i_38_; i_40_ += i_26_)
							graphics_39_.drawImage(anImage3405, i_40_, 0, null);
						graphics_29_.drawImage(image, i_37_, i_23_, null);
					}
					graphics_7_.drawImage(anImage3413, i_17_ + i_27_, i_28_, null);
					graphics_7_.setFont(aFont3404);
					graphics_7_.setColor(aColor3422);
					graphics_7_.drawString(string, ((anInt3406 - aFontMetrics3403.stringWidth(string)) / 2 + i_27_), anInt3414 / 2 + (i_28_ - (-anInt3410 - 4)));
					graphics.drawImage(Class387.anImage3713, 0, 0, null);
				} catch (Exception exception) {
					aBoolean3407 = true;
				}
			} else
				Class230.aCanvas2209.repaint();
		}
	}

	public final void method10(byte i) {
		if (i != -97)
			anInt3421 = -8;
	}

	private final Object method800(Class var_class, String string, int i) throws IllegalAccessException, NoSuchFieldException {
		Field field = var_class.getDeclaredField(string);
		if (i != -4)
			return null;
		Object object = field.get(CS2Script.anApplet6140);
		field.set(CS2Script.anApplet6140, null);
		return object;
	}

	public Class78() {
		/* empty */
	}

	static {
		aBoolean3430 = true;
	}
}
