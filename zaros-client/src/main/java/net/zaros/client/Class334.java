package net.zaros.client;

/* Class334 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.File;
import java.lang.reflect.Method;

final class Class334 {
	int anInt2947;
	int anInt2948;
	Interface10[] anInterface10Array2949;

	static final void method3420(int i, boolean bool, File file) {
		if (Class350.anObject3040 == null)
			Class216.method2030(10661);
		try {
			Class var_class = Class.forName("com.sun.management.HotSpotDiagnosticMXBean");
			Method method = (var_class.getDeclaredMethod("dumpHeap", new Class[] { (String.class), Boolean.TYPE }));
			method.invoke(Class350.anObject3040, new Object[] { file.getAbsolutePath(), new Boolean(bool) });
		} catch (Exception exception) {
			System.out.println("HeapDump error:");
			exception.printStackTrace();
		}
	}

	final void method3421(int i, Packet class296_sub17) {
		anInt2948 = class296_sub17.readUnsignedMedInt();
		anInt2947 = class296_sub17.g2();
		anInterface10Array2949 = new Interface10[class296_sub17.g1()];
		Class294[] class294s = Class294.method2478(-4652);
		for (int i_1_ = 0; anInterface10Array2949.length > i_1_; i_1_++)
			anInterface10Array2949[i_1_] = method3422(class296_sub17, class294s[class296_sub17.g1()], false);
		int i_2_ = -89 % ((i + 42) / 37);
	}

	public Class334() {
		/* empty */
	}

	private final Interface10 method3422(Packet class296_sub17, Class294 class294, boolean bool) {
		if (Class296_Sub34_Sub2.aClass294_6098 == class294)
			return Class41_Sub2.method398((byte) -128, class296_sub17);
		if (class294 == Class122_Sub3.aClass294_5663)
			return Class296_Sub15_Sub2.method2530(class296_sub17, 2);
		if (class294 == Animation.aClass294_422)
			return SubInPacket.method2245(-2, class296_sub17);
		if (Class110.aClass294_1140 == class294)
			return Class285.method2369(-126, class296_sub17);
		if (Class397_Sub3.aClass294_5798 == class294)
			return Class338_Sub3_Sub3_Sub1.method3559(class296_sub17, (byte) -80);
		if (bool)
			return null;
		if (class294 == Class391.aClass294_3291)
			return Class331.method3410(-126, class296_sub17);
		if (Class296_Sub51_Sub20.aClass294_6441 == class294)
			return Class244.method2176(class296_sub17, 67);
		if (class294 == Class227.aClass294_2186)
			return Class219.method2051(125, class296_sub17);
		if (Class296_Sub39_Sub14.aClass294_6218 == class294)
			return Class315.method3328((byte) 67, class296_sub17);
		if (class294 == Class83.aClass294_917)
			return Class368_Sub15.method3851(82, class296_sub17);
		return null;
	}

	static final long method3423(byte i, String string) {
		int i_3_ = 41 % ((-2 - i) / 51);
		long l = 0L;
		int i_4_ = string.length();
		for (int i_5_ = 0; i_5_ < i_4_; i_5_++) {
			l *= 37L;
			char c = string.charAt(i_5_);
			if (c < 'A' || c > 'Z') {
				if (c < 'a' || c > 'z') {
					if (c >= '0' && c <= '9')
						l += (long) (c + '\033' - '0');
				} else
					l += (long) (c + '\001' - 'a');
			} else
				l += (long) (c + '\001' - 'A');
			if (l >= 177917621779460413L)
				break;
		}
		for (/**/; l % 37L == 0L; l /= 37L) {
			if (l == 0L)
				break;
		}
		return l;
	}
}
