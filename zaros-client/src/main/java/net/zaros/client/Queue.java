package net.zaros.client;
/* Class145 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.zip.CRC32;

final class Queue {
	static IncomingPacket aClass231_1436 = new IncomingPacket(112, -2);
	Queuable head = new Queuable();
	private Queuable current;
	static Class258 aClass258_1439;
	static boolean force64MB = false;
	static CRC32 aCRC32_1441;

	final Queuable getNext() {
		Queuable ns = current;
		if (head == ns) {
			current = null;
			return null;
		}
		current = ns.queue_next;
		return ns;
	}

	final int getSize() {
		int count = 0;
		Queuable class296_sub39 = head.queue_next;
		while (head != class296_sub39) {
			class296_sub39 = class296_sub39.queue_next;
			count++;
		}
		return count;
	}

	final Queuable remove() {
		Queuable n = head.queue_next;
		if (n == head)
			return null;
		n.queue_unlink();
		return n;
	}

	public static void method1488(int i) {
		aClass258_1439 = null;
		aCRC32_1441 = null;
		if (i >= -87)
			method1488(-49);
		aClass231_1436 = null;
	}

	final void clear() {
		for (;;) {
			Queuable n = head.queue_next;
			if (head == n)
				break;
			n.queue_unlink();
		}
		current = null;
	}

	final void insert(Queuable n, int i) {
		if (n.queue_previous != null)
			n.queue_unlink();
		if (i == -2) {
			n.queue_next = head;
			n.queue_previous = head.queue_previous;
			n.queue_previous.queue_next = n;
			n.queue_next.queue_previous = n;
		}
	}

	final Queuable getFront() {
		Queuable class296_sub39 = head.queue_next;
		if (class296_sub39 == head) {
			current = null;
			return null;
		}
		current = class296_sub39.queue_next;
		return class296_sub39;
	}

	public Queue() {
		head.queue_next = head;
		head.queue_previous = head;
	}

	static {
		aClass258_1439 = new Class258();
		aCRC32_1441 = new CRC32();
	}
}
