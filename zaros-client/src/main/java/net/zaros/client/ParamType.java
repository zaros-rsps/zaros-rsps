package net.zaros.client;
/* Class383 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Calendar;

public class ParamType {
	static int[] anIntArray3242 = {4, 4, 1, 2, 6, 4, 2, 44, 2, 2, 2, 2, 2, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1};
	static IncomingPacket aClass231_3243;
	int defaultValue;
	public boolean disabled = true;
	static int anInt3246;
	String name;
	static Class164 aClass164_3248;
	static int anInt3249 = 0;
	private char aChar3250;
	static int anInt3251;

	static final String method4010(int i, boolean bool, long l, int i_0_) {
		if (i_0_ >= -96) {
			method4010(-108, false, 46L, 124);
		}
		Calendar calendar;
		if (!bool) {
			Class188.method1889(l, 26998);
			calendar = Class296_Sub38.aCalendar4889;
		} else {
			Class296_Sub51_Sub27_Sub1.method3159(109, l);
			calendar = Class296_Sub38.aCalendar4898;
		}
		int i_1_ = calendar.get(5);
		int i_2_ = calendar.get(2);
		int i_3_ = calendar.get(1);
		int i_4_ = calendar.get(11);
		int i_5_ = calendar.get(12);
		if (i == 3) {
			return Class177.method1713(l, i, bool, (byte) 77);
		}
		return Integer.toString(i_1_ / 10) + i_1_ % 10 + "-" + Class296_Sub24.aStringArrayArray4761[i][i_2_] + "-" + i_3_ + " " + i_4_ / 10 + i_4_ % 10 + ":" + i_5_ / 10 + i_5_ % 10;
	}

	private final void method4011(byte i, int i_6_, Packet class296_sub17) {
		if (i_6_ == 1) {
			aChar3250 = Mesh.method1387((byte) -58, class296_sub17.g1b());
		} else if (i_6_ != 2) {
			if (i_6_ == 4) {
				disabled = false;
			} else if (i_6_ == 5) {
				name = class296_sub17.gstr();
			}
		} else {
			defaultValue = class296_sub17.g4();
		}
		if (i <= 120) {
			method4012(99);
		}
	}

	public static void method4012(int i) {
		aClass164_3248 = null;
		anIntArray3242 = null;
		int i_7_ = 62 / ((77 - i) / 45);
		aClass231_3243 = null;
	}

	final boolean method4013(int i) {
		if (i >= -114) {
			method4013(86);
		}
		if (aChar3250 != 's') {
			return false;
		}
		return true;
	}

	public ParamType() {
		/* empty */
	}

	final void init(Packet in, boolean bool) {
		for (;;) {
			int i = in.g1();
			if (i == 0) {
				break;
			}
			method4011((byte) 126, i, in);
		}
		if (bool != true) {
			disabled = false;
		}
	}

	static {
		anInt3246 = 0;
		aClass231_3243 = new IncomingPacket(31, 10);
	}
}
