package net.zaros.client;
/* Class296_Sub39_Sub15_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.lang.ref.SoftReference;

final class Class296_Sub39_Sub15_Sub2 extends Class296_Sub39_Sub15 {
	private SoftReference aSoftReference6721;

	final boolean method2882(int i) {
		if (i != -3913)
			aSoftReference6721 = null;
		return true;
	}

	final Object method2883(byte i) {
		if (i > -82)
			aSoftReference6721 = null;
		return aSoftReference6721.get();
	}

	Class296_Sub39_Sub15_Sub2(Interface12 interface12, Object object, int i) {
		super(interface12, i);
		aSoftReference6721 = new SoftReference(object);
	}
}
