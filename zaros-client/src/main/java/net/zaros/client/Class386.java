package net.zaros.client;
import jaclib.memory.Buffer;
import jaclib.memory.Stream;

final class Class386 {
	static OutgoingPacket closeInterfacePacket = new OutgoingPacket(64, 0);
	private int anInt3260;
	private s_Sub3 aS_Sub3_3261;
	private ha_Sub1 aHa_Sub1_3262;
	private int anInt3263;
	static HashTable aClass263_3264 = new HashTable(8);
	byte[] aByteArray3265;
	private Class63[][] aClass63ArrayArray3266;
	int anInt3267;
	static Class335 aClass335_3268;
	private int anInt3269;
	private int anInt3270;

	final void method4026(int i) {
		if (i >= 86) {
			aClass63ArrayArray3266 = new Class63[anInt3263][anInt3269];
			for (int i_0_ = 0; i_0_ < anInt3269; i_0_++) {
				for (int i_1_ = 0; anInt3263 > i_1_; i_1_++) {
					aClass63ArrayArray3266[i_1_][i_0_] = new Class63(aHa_Sub1_3262, this, aS_Sub3_3261, i_1_, i_0_, anInt3260, i_1_ * 128 + 1, i_0_ * 128 + 1);
					if (aClass63ArrayArray3266[i_1_][i_0_].anInt719 == 0)
						aClass63ArrayArray3266[i_1_][i_0_] = null;
				}
			}
		}
	}

	final boolean method4027(int i, int i_2_, r var_r, byte i_3_) {
		r_Sub2 var_r_Sub2 = (r_Sub2) var_r;
		i_2_ += var_r_Sub2.anInt6708 + 1;
		i += var_r_Sub2.anInt6713 + 1;
		int i_4_ = -4 / ((-38 - i_3_) / 52);
		int i_5_ = i + anInt3267 * i_2_;
		int i_6_ = var_r_Sub2.anInt6709;
		int i_7_ = var_r_Sub2.anInt6712;
		if (i_2_ <= 0) {
			int i_8_ = -i_2_ + 1;
			i_5_ += anInt3267 * i_8_;
			i_2_ = 1;
			i_6_ -= i_8_;
		}
		int i_9_ = anInt3267 - i_7_;
		if (anInt3270 <= i_2_ + i_6_) {
			int i_10_ = i_2_ + i_6_ + 1 - anInt3270;
			i_6_ -= i_10_;
		}
		if (i <= 0) {
			int i_11_ = 1 - i;
			i_9_ += i_11_;
			i_7_ -= i_11_;
			i_5_ += i_11_;
			i = 1;
		}
		if (i + i_7_ >= anInt3267) {
			int i_12_ = -anInt3267 + (i_7_ + i + 1);
			i_9_ += i_12_;
			i_7_ -= i_12_;
		}
		if (i_7_ <= 0 || i_6_ <= 0)
			return false;
		int i_13_ = 8;
		i_9_ += (i_13_ - 1) * anInt3267;
		return EffectiveVertex.method2117(i_5_, i_9_, i_7_, aByteArray3265, i_13_, i_6_, true);
	}

	private final void method4028(int i, int i_14_, int i_15_, int i_16_, byte i_17_) {
		if (aClass63ArrayArray3266 != null) {
			int i_18_ = i_16_ - 1 >> 7;
			int i_19_ = i_16_ + i_14_ - 2 >> 7;
			int i_20_ = i_15_ - 1 >> 7;
			int i_21_ = i - 1 - 1 + i_15_ >> 7;
			if (i_17_ != -42)
				aHa_Sub1_3262 = null;
			for (int i_22_ = i_18_; i_19_ >= i_22_; i_22_++) {
				Class63[] class63s = aClass63ArrayArray3266[i_22_];
				for (int i_23_ = i_20_; i_21_ >= i_23_; i_23_++) {
					if (class63s[i_23_] != null)
						class63s[i_23_].aBoolean717 = true;
				}
			}
		}
	}

	static final int method4029(char c, int i) {
		try {
			if (c >= 0 && Class44_Sub2.anIntArray3859.length > c)
				return Class44_Sub2.anIntArray3859[c];
			if (i != -1)
				method4032(-117);
			return -1;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "vv.G(" + c + ',' + i + ')');
		}
	}

	final void method4030(int i, byte i_24_, int i_25_, r var_r) {
		r_Sub2 var_r_Sub2 = (r_Sub2) var_r;
		i_25_ += var_r_Sub2.anInt6708 + 1;
		i += var_r_Sub2.anInt6713 + 1;
		int i_26_ = -41 / ((55 - i_24_) / 50);
		int i_27_ = i + i_25_ * anInt3267;
		int i_28_ = 0;
		int i_29_ = var_r_Sub2.anInt6709;
		int i_30_ = var_r_Sub2.anInt6712;
		int i_31_ = -i_30_ + anInt3267;
		if (i_25_ <= 0) {
			int i_32_ = -i_25_ + 1;
			i_27_ += i_32_ * anInt3267;
			i_25_ = 1;
			i_29_ -= i_32_;
			i_28_ += i_32_ * i_30_;
		}
		int i_33_ = 0;
		if (i_29_ + i_25_ >= anInt3270) {
			int i_34_ = i_29_ + i_25_ + (-anInt3270 + 1);
			i_29_ -= i_34_;
		}
		if (i <= 0) {
			int i_35_ = -i + 1;
			i_33_ += i_35_;
			i = 1;
			i_27_ += i_35_;
			i_28_ += i_35_;
			i_30_ -= i_35_;
			i_31_ += i_35_;
		}
		if (i_30_ + i >= anInt3267) {
			int i_36_ = -anInt3267 + (i_30_ + i + 1);
			i_33_ += i_36_;
			i_30_ -= i_36_;
			i_31_ += i_36_;
		}
		if (i_30_ > 0 && i_29_ > 0) {
			Class395.method4067(i_33_, aByteArray3265, i_28_, i_29_, var_r_Sub2.aByteArray6711, i_30_, i_31_, i_27_, -101);
			method4028(i_29_, i_30_, i_25_, i, (byte) -42);
		}
	}

	final void method4031(int i, int i_37_, int i_38_, int i_39_, boolean bool, boolean[][] bools) {
		aHa_Sub1_3262.method1137(4, false);
		aHa_Sub1_3262.method1106(-74, false);
		aHa_Sub1_3262.method1161(31156, 1);
		aHa_Sub1_3262.method1197((byte) 22, 1);
		aHa_Sub1_3262.method1133(false, -2, 0, false);
		float f = 1.0F / (float) (aHa_Sub1_3262.anInt3951 * 128);
		if (!bool) {
			for (int i_40_ = 0; i_40_ < anInt3269; i_40_++) {
				int i_41_ = i_40_ << anInt3260;
				int i_42_ = i_40_ + 1 << anInt3260;
				for (int i_43_ = 0; anInt3263 > i_43_; i_43_++) {
					Class63 class63 = aClass63ArrayArray3266[i_43_][i_40_];
					if (class63 != null) {
						Interface15_Impl1 interface15_impl1 = aHa_Sub1_3262.method1223(class63.anInt719 * 3, (byte) -103);
						Buffer buffer = interface15_impl1.method30((byte) -20, true);
						if (buffer != null) {
							Stream stream = aHa_Sub1_3262.method1156(buffer, -103);
							int i_44_ = 0;
							int i_45_ = i_43_ << anInt3260;
							int i_46_ = i_43_ + 1 << anInt3260;
							for (int i_47_ = i_41_; i_42_ > i_47_; i_47_++) {
								if (-i_38_ <= -i + i_47_ && i_47_ - i <= i_38_) {
									int i_48_ = (i_45_ + aS_Sub3_3261.anInt2832 * i_47_);
									for (int i_49_ = i_45_; i_46_ > i_49_; i_49_++) {
										if (-i_37_ + i_49_ >= -i_38_ && -i_37_ + i_49_ <= i_38_ && (bools[-i_37_ + (i_49_ + i_38_)][i_47_ - i + i_38_])) {
											short[] is = (aS_Sub3_3261.aShortArrayArray5152[i_48_]);
											if (is != null) {
												if (!Stream.a()) {
													for (int i_50_ = 0; i_50_ < is.length; i_50_++) {
														stream.a(is[i_50_] & 0xffff);
														i_44_++;
													}
												} else {
													for (int i_51_ = 0; is.length > i_51_; i_51_++) {
														i_44_++;
														stream.d(is[i_51_] & 0xffff);
													}
												}
											}
										}
										i_48_++;
									}
								}
							}
							stream.b();
							if (interface15_impl1.method33(32185) && i_44_ > 0) {
								Class373_Sub2 class373_sub2 = aHa_Sub1_3262.method1153(103);
								class373_sub2.method3931(1.0F, f, f, -103);
								class373_sub2.method3904(-i_43_, -i_40_, 0);
								aHa_Sub1_3262.method1181((Class360_Sub5.aClass230_5325), (byte) 125);
								class63.method704(interface15_impl1, i_44_ / 3, (byte) 26);
							}
						}
					}
				}
			}
		} else {
			for (int i_52_ = 0; i_52_ < anInt3269; i_52_++) {
				int i_53_ = i_52_ << anInt3260;
				int i_54_ = i_52_ + 1 << anInt3260;
				for (int i_55_ = 0; i_55_ < anInt3263; i_55_++) {
					if (aClass63ArrayArray3266[i_55_][i_52_] != null) {
						int i_56_ = i_55_ << anInt3260;
						int i_57_ = i_55_ + 1 << anInt3260;
						while_267_ : for (int i_58_ = i_56_; i_57_ > i_58_; i_58_++) {
							if (i_58_ - i_37_ >= -i_38_ && i_38_ >= i_58_ - i_37_) {
								for (int i_59_ = i_53_; i_54_ > i_59_; i_59_++) {
									if (-i + i_59_ >= -i_38_ && i_38_ >= -i + i_59_ && (bools[i_58_ - i_37_ + i_38_][i_38_ - i + i_59_])) {
										Class373_Sub2 class373_sub2 = aHa_Sub1_3262.method1153(114);
										class373_sub2.method3931(1.0F, f, f, -124);
										class373_sub2.method3904(-i_55_, -i_52_, 0);
										aHa_Sub1_3262.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
										aClass63ArrayArray3266[i_55_][i_52_].method703(false);
										break while_267_;
									}
								}
							}
						}
					}
				}
			}
		}
		aHa_Sub1_3262.method1125(0);
		if (i_39_ <= 30)
			aClass63ArrayArray3266 = null;
	}

	public static void method4032(int i) {
		if (i == -1) {
			aClass263_3264 = null;
			closeInterfacePacket = null;
			aClass335_3268 = null;
		}
	}

	final void method4033(int i, byte i_60_, int i_61_, r var_r) {
		if (i_60_ == -75) {
			r_Sub2 var_r_Sub2 = (r_Sub2) var_r;
			i_61_ += var_r_Sub2.anInt6713 + 1;
			i += var_r_Sub2.anInt6708 + 1;
			int i_62_ = i * anInt3267 + i_61_;
			int i_63_ = 0;
			int i_64_ = var_r_Sub2.anInt6709;
			int i_65_ = var_r_Sub2.anInt6712;
			int i_66_ = -i_65_ + anInt3267;
			if (i <= 0) {
				int i_67_ = -i + 1;
				i_64_ -= i_67_;
				i_62_ += anInt3267 * i_67_;
				i = 1;
				i_63_ += i_67_ * i_65_;
			}
			int i_68_ = 0;
			if (anInt3270 <= i_64_ + i) {
				int i_69_ = -anInt3270 + (i + 1 + i_64_);
				i_64_ -= i_69_;
			}
			if (i_61_ <= 0) {
				int i_70_ = 1 - i_61_;
				i_66_ += i_70_;
				i_65_ -= i_70_;
				i_68_ += i_70_;
				i_63_ += i_70_;
				i_62_ += i_70_;
				i_61_ = 1;
			}
			if (i_65_ + i_61_ >= anInt3267) {
				int i_71_ = i_61_ + i_65_ - (-1 + anInt3267);
				i_65_ -= i_71_;
				i_66_ += i_71_;
				i_68_ += i_71_;
			}
			if (i_65_ > 0 && i_64_ > 0) {
				Class338_Sub3_Sub1.method3476(aByteArray3265, var_r_Sub2.aByteArray6711, i_62_, i_64_, i_68_, i_63_, i_66_, i_65_, -127);
				method4028(i_64_, i_65_, i, i_61_, (byte) -42);
			}
		}
	}

	Class386(ha_Sub1 var_ha_Sub1, s_Sub3 var_s_Sub3) {
		aS_Sub3_3261 = var_s_Sub3;
		aHa_Sub1_3262 = var_ha_Sub1;
		anInt3267 = 2 + (aS_Sub3_3261.anInt2836 * aS_Sub3_3261.anInt2832 >> aHa_Sub1_3262.anInt4036);
		anInt3270 = (aS_Sub3_3261.anInt2834 * aS_Sub3_3261.anInt2836 >> aHa_Sub1_3262.anInt4036) + 2;
		anInt3260 = -aS_Sub3_3261.anInt2835 + (aHa_Sub1_3262.anInt4036 + 7);
		aByteArray3265 = new byte[anInt3270 * anInt3267];
		anInt3263 = aS_Sub3_3261.anInt2832 >> anInt3260;
		anInt3269 = aS_Sub3_3261.anInt2834 >> anInt3260;
	}
}
