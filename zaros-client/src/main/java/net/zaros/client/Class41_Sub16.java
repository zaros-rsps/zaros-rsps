package net.zaros.client;

/* Class41_Sub16 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub16 extends Class41 {
	static boolean[][] aBooleanArrayArray3780 = {new boolean[4], new boolean[4], {false, false, true, false}, {false, false, true, false}, {false, false, true, false}, {false, false, true, false}, {true, false, true, false}, {true, false, false, true}, {true, false, false, true}, new boolean[4], new boolean[4], new boolean[4], new boolean[4]};
	static IncomingPacket aClass231_3781 = new IncomingPacket(63, -1);

	final int method453(int i) {
		if (i < 114)
			aBooleanArrayArray3780 = null;
		return anInt389;
	}

	final int method383(byte i) {
		if (i != 110)
			aClass231_3781 = null;
		return 1;
	}

	Class41_Sub16(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final void method386(int i) {
		if (aClass296_Sub50_392.method3054(91))
			anInt389 = 0;
		if (anInt389 != 1 && anInt389 != 0)
			anInt389 = method383((byte) 110);
		if (i != 2)
			aBooleanArrayArray3780 = null;
	}

	final void method381(int i, byte i_0_) {
		if (i_0_ != -110)
			aBooleanArrayArray3780 = null;
		anInt389 = i;
	}

	final int method380(int i, byte i_1_) {
		if (i_1_ != 41)
			method454(19);
		if (aClass296_Sub50_392.method3054(i_1_ ^ 0x7d))
			return 3;
		return 1;
	}

	Class41_Sub16(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final boolean method454(int i) {
		if (aClass296_Sub50_392.method3054(126))
			return false;
		if (i != -25952)
			method454(-112);
		return true;
	}

	public static void method455(int i) {
		aBooleanArrayArray3780 = null;
		aClass231_3781 = null;
		if (i != 28541)
			method455(-83);
	}
}
