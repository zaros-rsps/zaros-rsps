package net.zaros.client;
/* ha_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Dimension;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;

import jaclib.memory.Buffer;
import jaclib.memory.Stream;
import jaclib.memory.heap.NativeHeap;
import jaclib.memory.heap.NativeHeapBuffer;

import jagex3.graphics2.hw.NativeInterface;

abstract class ha_Sub1 extends ha {
	int anInt3920;
	int anInt3921;
	private int anInt3922;
	NativeInterface aNativeInterface3923;
	private Object anObject3924;
	private Hashtable aHashtable3925;
	Canvas aCanvas3926;
	Object anObject3927;
	Js5 aClass138_3928;
	Class184 aClass184_3929;
	private Canvas aCanvas3930;
	NativeHeapBuffer aNativeHeapBuffer3931;
	NativeHeap aNativeHeap3932;
	private NodeDeque aClass155_3933 = new NodeDeque();
	long aLong3934;
	private int anInt3935;
	int anInt3936;
	int anInt3937;
	int anInt3938;
	Class373_Sub2 aClass373_Sub2_3939;
	boolean aBoolean3940 = true;
	Class373_Sub2 aClass373_Sub2_3941;
	Class373_Sub2 aClass373_Sub2_3942;
	Class373_Sub2 aClass373_Sub2_3943;
	private Class373_Sub2 aClass373_Sub2_3944;
	private Class373_Sub2 aClass373_Sub2_3945;
	boolean aBoolean3946;
	int anInt3947;
	boolean aBoolean3948;
	float[] aFloatArray3949;
	int anInt3950;
	int anInt3951;
	boolean aBoolean3952;
	private float aFloat3953;
	Class108 aClass108_3954;
	private int anInt3955;
	int anInt3956;
	private int anInt3957;
	private float[] aFloatArray3958;
	private boolean aBoolean3959;
	int anInt3960;
	private boolean aBoolean3961;
	private float[] aFloatArray3962;
	boolean aBoolean3963;
	float aFloat3964;
	Class373_Sub2[] aClass373_Sub2Array3965;
	Class230[] aClass230Array3966;
	private float[] aFloatArray3967;
	boolean aBoolean3968;
	int anInt3969;
	int anInt3970;
	private int anInt3971;
	private int anInt3972;
	boolean aBoolean3973;
	boolean aBoolean3974;
	private Stream aStream3975;
	float aFloat3976;
	boolean aBoolean3977;
	int anInt3978;
	boolean aBoolean3979;
	private boolean aBoolean3980;
	int anInt3981;
	int anInt3982;
	int anInt3983;
	private Class241_Sub1 aClass241_Sub1_3984;
	private float aFloat3985;
	private Interface6[] anInterface6Array3986;
	int anInt3987;
	int anInt3988;
	float aFloat3989;
	int anInt3990;
	int anInt3991;
	int anInt3992;
	int anInt3993;
	private float[] aFloatArray3994;
	Class125[] aClass125Array3995;
	private float[] aFloatArray3996;
	boolean aBoolean3997;
	private float[] aFloatArray3998;
	private Class360[] aClass360Array3999;
	float aFloat4000;
	private float aFloat4001;
	private Class395 aClass395_4002;
	Class125[] aClass125Array4003;
	private boolean aBoolean4004;
	Class296_Sub35[] aClass296_Sub35Array4005;
	int anInt4006;
	float aFloat4007;
	private int anInt4008;
	private boolean aBoolean4009;
	float aFloat4010;
	int anInt4011;
	private int anInt4012;
	private int anInt4013;
	float aFloat4014;
	boolean aBoolean4015;
	private Class360 aClass360_4016;
	private int anInt4017;
	float aFloat4018;
	float aFloat4019;
	private int anInt4020;
	private boolean aBoolean4021;
	float aFloat4022;
	private int anInt4023;
	float aFloat4024;
	private int anInt4025;
	int anInt4026;
	int anInt4027;
	float[] aFloatArray4028;
	Class258 aClass258_4029;
	float[] aFloatArray4030;
	int anInt4031;
	boolean aBoolean4032;
	int anInt4033;
	float aFloat4034;
	int anInt4035;
	int anInt4036;
	Interface6 anInterface6_4037;
	boolean aBoolean4038;
	int anInt4039;
	int anInt4040;
	boolean aBoolean4041;
	int anInt4042;
	float aFloat4043;
	float aFloat4044;
	private Class152 aClass152_4045;
	int anInt4046;
	Class127 aClass127_4047;
	private Class127 aClass127_4048;
	private Class127 aClass127_4049;
	private Interface15_Impl1 anInterface15_Impl1_4050;
	Class127 aClass127_4051;
	private Interface15_Impl2 anInterface15_Impl2_4052;
	private Class127 aClass127_4053;
	private Interface15_Impl2 anInterface15_Impl2_4054;
	private Interface15_Impl2 anInterface15_Impl2_4055;
	Class178_Sub3[] aClass178_Sub3Array4056;
	Class127 aClass127_4057;
	Class178_Sub3[] aClass178_Sub3Array4058;
	Class127 aClass127_4059;
	private Class373_Sub2 aClass373_Sub2_4060;
	private int anInt4061;
	boolean aBoolean4062;

	public void ra(int i, int i_0_, int i_1_, int i_2_) {
		aBoolean4038 = true;
		anInt4008 = i;
		anInt4013 = i_0_;
		anInt4012 = i_1_;
		anInt3955 = i_2_;
	}

	public void a(Class241 class241) {
		aClass241_Sub1_3984 = (Class241_Sub1) class241;
	}

	abstract Object method1100(int i, Canvas canvas);

	public void b(int i, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		float f = method1144(1);
		method1196((byte) 59);
		method1139(0, i_6_);
		method1158((byte) -114, 0, Class151.aClass287_1553);
		method1219((byte) 54, Class151.aClass287_1553, 0);
		method1161(31156, i_7_);
		aClass373_Sub2_3939.method3931(1.0F, (float) (i_5_ - 1), (float) (i_4_ - 1), -82);
		aClass373_Sub2_3939.method3946((float) i_3_ - f, 0.0F, 16383, (float) i - f);
		method1202(0);
		method1101(false, (byte) -94);
		method1118(4, Class287.aClass289_2645, 0);
		method1101(true, (byte) 94);
		method1219((byte) 54, Class199.aClass287_2007, 0);
		method1158((byte) -118, 0, Class199.aClass287_2007);
	}

	abstract void method1101(boolean bool, byte i);

	public void method1102(int i) {
		if (i < -115) {
			aClass373_Sub2_3939.method3910();
			aBoolean3940 = true;
			method1163(17830);
		}
	}

	private void method1103(byte i) {
		method1194((byte) -57);
		if (i != 14)
			aFloatArray4028 = null;
		if (aClass360_4016 != null)
			aClass360_4016.method3734(-13412);
	}

	abstract Interface6_Impl3 method1104(boolean bool, int i, int i_8_, int[][] is);

	public void a(Canvas canvas) {
		if (aCanvas3926 == canvas)
			throw new RuntimeException();
		if (aHashtable3925.containsKey(canvas)) {
			method1217(26104, aHashtable3925.get(canvas), canvas);
			aHashtable3925.remove(canvas);
		}
	}

	public void t() {
		if (!aBoolean4009) {
			for (Node class296 = aClass155_3933.removeFirst((byte) 124); class296 != null; class296 = aClass155_3933.removeNext(1001))
				((za_Sub1) class296).method3223((byte) 109);
			Enumeration enumeration = aHashtable3925.keys();
			while (enumeration.hasMoreElements()) {
				Canvas canvas = (Canvas) enumeration.nextElement();
				method1217(26104, aHashtable3925.get(canvas), canvas);
			}
			Class355_Sub1.method3702((byte) 78, true, false);
			aNativeInterface3923.release();
			aBoolean4009 = true;
		}
	}

	public void method1105(byte i) {
		if (aClass108_3954 != Class137.aClass108_1407) {
			Class108 class108 = aClass108_3954;
			aClass108_3954 = Class137.aClass108_1407;
			if (class108.method947(27229))
				method1231(false);
			aFloatArray3949 = aFloatArray3996;
			anInt4020 &= ~0x1f;
		}
		int i_9_ = -34 % ((-4 - i) / 37);
	}

	public void method1106(int i, boolean bool) {
		if (!bool == aBoolean3997) {
			aBoolean3997 = bool;
			method1186(0);
			anInt4020 &= ~0x1f;
		}
		int i_10_ = -72 / ((i - 2) / 56);
	}

	abstract Interface6_Impl1 method1107(int i, int i_11_, int i_12_, Class67 class67, Class202 class202);

	public s a(int i, int i_13_, int[][] is, int[][] is_14_, int i_15_, int i_16_, int i_17_) {
		return new s_Sub3(this, i_16_, i_17_, i, i_13_, is, is_14_, i_15_);
	}

	public void a(int i) {
		if (i < 128 || i > 1024)
			throw new IllegalArgumentException();
		if (aClass395_4002 != null)
			aClass395_4002.method4065((byte) 34);
		anInt4026 = i;
	}

	abstract void method1108(boolean bool);

	public void P(int i, int i_18_, int i_19_, int i_20_, int i_21_) {
		e(i, i_18_, i, i_19_ + i_18_, i_20_, i_21_);
	}

	public NativeHeapBuffer method1109(boolean bool, byte i, int i_22_) {
		int i_23_ = 99 / ((i - 39) / 49);
		return aNativeHeap3932.a(i_22_, bool);
	}

	public void f(int i) {
		if (i != 1)
			throw new IllegalArgumentException("");
	}

	public void method1110(boolean bool, int i) {
		if (!bool == aBoolean3948) {
			aBoolean3948 = bool;
			method1128(false);
			anInt4020 &= ~0x1f;
		}
		if (i >= -47)
			method1224(true);
	}

	private void method1111(int i) {
		if (aClass108_3954 != Class130.aClass108_1337) {
			Class108 class108 = aClass108_3954;
			aClass108_3954 = Class130.aClass108_1337;
			if (!class108.method947(27229))
				method1231(false);
			method1198(-123);
			aFloatArray3949 = aFloatArray3994;
			method1103((byte) 14);
			anInt4020 &= ~0x7;
		}
		if (i != 3)
			anInterface6Array3986 = null;
	}

	abstract void method1112(boolean bool);

	public Class373_Sub2 method1113(int i) {
		if (i != 27200)
			aClass127_4053 = null;
		return aClass373_Sub2_3944;
	}

	public int method1114(boolean bool) {
		if (bool)
			return 72;
		return anInt3991;
	}

	ha_Sub1(Canvas canvas, Object object, d var_d, Js5 class138, int i, int i_24_) {
		super(var_d);
		aClass373_Sub2_3939 = new Class373_Sub2();
		aClass373_Sub2_3941 = new Class373_Sub2();
		aClass373_Sub2_3942 = new Class373_Sub2();
		aClass373_Sub2_3943 = new Class373_Sub2();
		aClass373_Sub2_3944 = new Class373_Sub2();
		aClass373_Sub2_3945 = new Class373_Sub2();
		aBoolean3961 = false;
		aBoolean3948 = false;
		aBoolean3952 = true;
		aFloat3953 = 1.0F;
		anInt3955 = 0;
		anInt3987 = 0;
		aBoolean3973 = false;
		aBoolean3980 = false;
		aBoolean3979 = true;
		anInt3991 = 0;
		anInt3947 = -1;
		anInt3988 = 0;
		aFloatArray3994 = new float[16];
		aBoolean3946 = true;
		aBoolean3959 = false;
		anInt3951 = 8;
		aFloatArray3958 = new float[16];
		aFloatArray3998 = new float[16];
		aFloatArray3996 = new float[]{1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F};
		aBoolean4004 = false;
		anInt3992 = 512;
		anInt3981 = 3584;
		anInt3972 = -1;
		anInt3960 = 0;
		anInt4013 = -1;
		aFloat4014 = 3584.0F;
		anInt3971 = 16777215;
		aClass360Array3999 = new Class360[10];
		anInt3990 = -1;
		aBoolean4009 = false;
		aFloat4000 = 1.0F;
		aFloatArray3967 = new float[]{0.0F, 0.0F, 1.0F, 0.0F};
		anInt4011 = 0;
		aFloatArray3962 = new float[]{0.0F, 0.0F, 1.0F, 0.0F};
		aFloat4022 = -1.0F;
		aFloat4024 = 3584.0F;
		aFloatArray3949 = aFloatArray3996;
		aClass108_3954 = Class137.aClass108_1407;
		aFloatArray4028 = new float[]{0.0F, 0.0F, -1.0F, 0.0F};
		aFloat4007 = -1.0F;
		aBoolean4021 = false;
		anInt4036 = 3;
		aClass258_4029 = StaticMethods.aClass258_5957;
		aBoolean4032 = false;
		aFloat4010 = 1.0F;
		anInt3993 = 512;
		anInt3957 = 0;
		aBoolean4015 = true;
		aBoolean3997 = false;
		anInt4023 = 0;
		anInt4017 = 0;
		anInt3970 = 0;
		anInt4039 = 0;
		anInt3982 = 0;
		anInt4026 = 128;
		anInt4035 = 50;
		aBoolean4041 = true;
		aFloatArray4030 = new float[]{0.0F, 0.0F, 1.0F, 0.0F};
		aFloat4019 = 1.0F;
		anInt4025 = 1;
		anInt4040 = 0;
		aFloat4043 = 1.0F;
		anInt4012 = -1;
		anInt4042 = 0;
		aStream3975 = new Stream();
		aClass178_Sub3Array4056 = new Class178_Sub3[7];
		aClass178_Sub3Array4058 = new Class178_Sub3[7];
		aClass373_Sub2_4060 = new Class373_Sub2();
		try {
			anInt3978 = i;
			anObject3924 = anObject3927 = object;
			aClass138_3928 = class138;
			aCanvas3930 = aCanvas3926 = canvas;
			Dimension dimension = canvas.getSize();
			anInt3921 = anInt3935 = dimension.width;
			anInt3920 = anInt3922 = dimension.height;
			anInt3956 = i_24_;
			Class338.method3437(true, false, false);
			if (aD1299 == null) {
				aNativeInterface3923 = new NativeInterface(0, anInt3956);
				aClass395_4002 = null;
			} else {
				aClass395_4002 = new Class395(this, aD1299);
				aNativeInterface3923 = new NativeInterface(aD1299.method13((byte) -89), anInt3956);
				for (int i_25_ = 0; i_25_ < aD1299.method13((byte) -89); i_25_++) {
					MaterialRaw class170 = aD1299.method14(i_25_, -9412);
					if (class170 != null)
						aNativeInterface3923.initTextureMetrics(i_25_, class170.aByte1773, class170.aByte1793);
				}
			}
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			this.method1091((byte) -90);
			throw new RuntimeException("");
		}
	}

	abstract void method1115(int i);

	public Interface6_Impl3 method1116(byte i) {
		if (i > -50)
			method1201(-59, true);
		if (aClass241_Sub1_3984 != null)
			return aClass241_Sub1_3984.method2152(-38);
		return null;
	}

	public void Q(int i, int i_26_, int i_27_, int i_28_, int i_29_, int i_30_, byte[] is, int i_31_, int i_32_) {
		/* empty */
	}

	public void method1117(int i, float f) {
		int i_33_ = -45 / ((i - 85) / 37);
		if (f != aFloat3953) {
			aFloat3953 = f;
			method1155(-29995);
		}
	}

	private void method1118(int i, Class289 class289, int i_34_) {
		method1178(anInterface15_Impl2_4054, (byte) -128, 0);
		method1209(aClass127_4053, (byte) -121);
		method1185(i_34_, i_34_, class289, i);
	}

	Class360 method1119(int i, int i_35_) {
		if (i != 22940)
			return null;
		int i_36_ = i_35_;
		while_18_ : do {
			while_17_ : do {
				do {
					if (i_36_ != 6) {
						if (i_36_ != 1) {
							if (i_36_ != 2) {
								if (i_36_ != 7)
									break while_18_;
							} else
								break;
							break while_17_;
						}
					} else
						return new Class360_Sub5(this);
					return new Class360_Sub9(this);
				} while (false);
				return new Class360_Sub2(this, aClass184_3929);
			} while (false);
			return new Class360_Sub11(this);
		} while (false);
		return new Class360_Sub6(this);
	}

	public Class296_Sub35 a(int i, int i_37_, int i_38_, int i_39_, int i_40_, float f) {
		return new Class296_Sub35_Sub2(i, i_37_, i_38_, i_39_, i_40_, f);
	}

	public void method1120(boolean bool, byte i) {
		if (i <= 117)
			method1205(true, -109);
		if (bool == !aBoolean4032) {
			aBoolean4032 = bool;
			method1211((byte) 85);
		}
	}

	public void a(int i, int i_41_, int i_42_, int i_43_, int i_44_, int i_45_, int i_46_, int i_47_, int i_48_, int i_49_, int i_50_, int i_51_, int i_52_) {
		/* empty */
	}

	private void method1121(byte i) {
		method1227(-1);
		if (i == 125) {
			if (aClass360_4016 != null)
				aClass360_4016.method3730(false);
		}
	}

	public Class373_Sub2 method1122(int i) {
		if (i < 37)
			anInterface15_Impl2_4054 = null;
		if (!aBoolean3961) {
			aClass373_Sub2_3945.method3938(aClass373_Sub2_3943, aClass373_Sub2_3939);
			aBoolean3961 = true;
		}
		return aClass373_Sub2_3945;
	}

	public int method1123(byte i) {
		if (i != -68)
			aClass125Array4003 = null;
		return anInt3955;
	}

	public boolean u() {
		return true;
	}

	public void method1124(int i) {
		if (anInt4020 != i) {
			method1111(3);
			method1201(i - 25373, true);
			method1110(true, -92);
			method1106(-92, true);
			method1161(31156, 1);
			anInt4020 = 16;
		}
	}

	public Sprite a(int i, int i_53_, int i_54_, int i_55_, boolean bool) {
		Class397_Sub2 class397_sub2 = new Class397_Sub2(this, i_54_, i_55_, bool);
		class397_sub2.method4090(0, 0, i_54_, i_55_, i, i_53_);
		return class397_sub2;
	}

	public void method1125(int i) {
		if (i == 0) {
			if (aClass230Array3966[anInt3991] != Class67.aClass230_754) {
				aClass230Array3966[anInt3991] = Class67.aClass230_754;
				aClass373_Sub2Array3965[anInt3991].method3910();
				method1121((byte) 125);
			}
		}
	}

	private void method1126(byte i) {
		if (i == -55) {
			method1178(anInterface15_Impl2_4055, (byte) -120, 0);
			method1209(aClass127_4049, (byte) -106);
			method1185(0, 0, ISAACCipher.aClass289_1896, 1);
		}
	}

	public Class241 c(int i, int i_56_, int i_57_, int i_58_, int i_59_, int i_60_) {
		return new Class241_Sub1_Sub1(this, i, i_56_, i_57_, i_58_, i_59_, i_60_);
	}

	private void method1127(boolean bool) {
		method1112(false);
		method1189((byte) 75);
		method1211((byte) 85);
		method1225(102);
		method1138(0);
		method1162(bool);
		method1173(-3394);
		method1128(false);
		method1186(0);
		method1148(84);
		method1224(true);
		method1216(-105);
		method1184((byte) -109);
		method1108(true);
		for (int i = anInt4031 - 1; i >= 0; i--) {
			method1151(i, 16760);
			method1150(true);
			method1165(2147483647);
			method1125(0);
		}
		method1228(-25685);
		method1204(true);
		method1194((byte) -57);
		method1152(111);
		method1210(-10667);
	}

	abstract void method1128(boolean bool);

	public void h(int i) {
		/* empty */
	}

	private void method1129(int i, int i_61_, boolean bool, int i_62_, boolean bool_63_, int i_64_) {
		if (i_62_ == 4) {
			bool_63_ &= l();
			if (!bool_63_ && (i == 4 || i == 8 || i == 9)) {
				i = 2;
				i_64_ = i == 4 ? i_61_ & 0x1 : 1;
				i_61_ = 0;
			}
			if (i != 0 && bool)
				i |= ~0x7fffffff;
			if (i == anInt4017) {
				if (anInt4017 != 0) {
					aClass360Array3999[anInt4017 & 0x7fffffff].method3731(bool, (byte) -71);
					if (anInt4023 != i_61_ || i_64_ != anInt3957) {
						aClass360Array3999[anInt4017 & 0x7fffffff].method3732(i_64_, i_62_ - 125, i_61_);
						anInt4023 = i_61_;
						anInt3957 = i_64_;
					}
				}
			} else {
				if (anInt4017 != 0)
					aClass360Array3999[anInt4017 & 0x7fffffff].method3725(-79);
				if (i == 0)
					aClass360_4016 = null;
				else {
					aClass360_4016 = aClass360Array3999[i & 0x7fffffff];
					aClass360_4016.method3733((byte) -126, bool);
					aClass360_4016.method3731(bool, (byte) -71);
					aClass360_4016.method3732(i_64_, -58, i_61_);
				}
				anInt4017 = i;
				anInt4023 = i_61_;
				anInt3957 = i_64_;
			}
		}
	}

	public void method1130(int i) {
		if (i == 8) {
			if (anInt4020 != 8) {
				method1192(i ^ 0x8);
				method1201(-25357, true);
				method1110(true, -121);
				method1106(81, true);
				method1161(31156, 1);
				anInt4020 = 8;
			}
		}
	}

	private void method1131(boolean bool) {
		if (bool != true)
			anInterface6Array3986 = null;
		aFloatArray3958[14] = aFloat4001;
		aFloatArray3958[10] = aFloat3985;
		aFloat4024 = ((float) -anInt3981 + aFloatArray3958[14]) / aFloatArray3958[10];
	}

	private void method1132(int i) {
		anInterface15_Impl2_4055 = method1205(true, 47);
		if (i != 27595)
			method1220(-89);
		anInterface15_Impl2_4055.method46(24, i ^ ~0x6ba5, 12);
		aClass127_4049 = method1233((byte) -46, (new Class211[]{new Class211(Class356.aClass356_3072)}));
	}

	public void method1133(boolean bool, int i, int i_65_, boolean bool_66_) {
		if (anInt3972 != i || !aBoolean3980 == aBoolean4038) {
			Interface6_Impl1 interface6_impl1 = null;
			int i_67_ = 0;
			byte i_68_ = 0;
			int i_69_ = 0;
			byte i_70_ = !aBoolean4038 ? (byte) 0 : (byte) 3;
			if (i >= 0) {
				interface6_impl1 = aClass395_4002.method4071(i, (byte) -2);
				MaterialRaw class170 = aD1299.method14(i, -9412);
				if (class170.speed_u == 0 && class170.speed_v == 0)
					method1125(i_65_);
				else {
					int i_71_ = !class170.small_sized ? 128 : 64;
					int i_72_ = i_71_ * 50;
					Class373_Sub2 class373_sub2 = method1153(113);
					class373_sub2.method3943(((float) (anInt4006 % i_72_ * class170.speed_v) / (float) i_72_), 0.0F, ((float) (anInt4006 % i_72_ * class170.speed_u) / (float) i_72_), 0);
					method1181(Class360_Sub5.aClass230_5325, (byte) 125);
				}
				i_67_ = class170.anInt1782;
				if (!aBoolean4038) {
					i_68_ = class170.aByte1784;
					i_69_ = class170.anInt1794;
					i_70_ = class170.aByte1774;
				}
			} else
				method1125(0);
			method1129(i_70_, i_68_, bool_66_, 4, bool, i_69_);
			if (aClass360_4016 == null) {
				method1140(interface6_impl1, false);
				method1197((byte) 22, i_67_);
			} else
				aClass360_4016.method3736((byte) 126, interface6_impl1, i_67_);
			aBoolean3980 = aBoolean4038;
			anInt3972 = i;
		}
		if (i_65_ == 0)
			anInt4020 &= ~0x7;
	}

	abstract void method1134(int i);

	public int r(int i, int i_73_, int i_74_, int i_75_, int i_76_, int i_77_, int i_78_) {
		int i_79_ = 0;
		float f = ((float) i * aClass373_Sub2_3941.aFloat5601 + (float) i_73_ * aClass373_Sub2_3941.aFloat5599 + (float) i_74_ * aClass373_Sub2_3941.aFloat5591 + aClass373_Sub2_3941.aFloat5594);
		float f_80_ = (aClass373_Sub2_3941.aFloat5594 + (aClass373_Sub2_3941.aFloat5591 * (float) i_77_ + ((float) i_75_ * aClass373_Sub2_3941.aFloat5601 + (float) i_76_ * aClass373_Sub2_3941.aFloat5599)));
		if (!((float) anInt4035 > f) || !(f_80_ < (float) anInt4035)) {
			if ((float) anInt3981 < f && f_80_ > (float) anInt3981)
				i_79_ |= 0x20;
		} else
			i_79_ |= 0x10;
		int i_81_ = (int) ((float) anInt3992 * (aClass373_Sub2_3941.aFloat5602 * (float) i_74_ + (aClass373_Sub2_3941.aFloat5598 * (float) i + (float) i_73_ * aClass373_Sub2_3941.aFloat5595) + aClass373_Sub2_3941.aFloat5593) / (float) i_78_);
		int i_82_ = (int) ((aClass373_Sub2_3941.aFloat5593 + (aClass373_Sub2_3941.aFloat5598 * (float) i_75_ + (float) i_76_ * aClass373_Sub2_3941.aFloat5595 + aClass373_Sub2_3941.aFloat5602 * (float) i_77_)) * (float) anInt3992 / (float) i_78_);
		if (!((float) i_81_ < aFloat4018) || !((float) i_82_ < aFloat4018)) {
			if (aFloat4034 < (float) i_81_ && (float) i_82_ > aFloat4034)
				i_79_ |= 0x2;
		} else
			i_79_ |= 0x1;
		int i_83_ = (int) ((aClass373_Sub2_3941.aFloat5597 * (float) i_74_ + ((float) i * aClass373_Sub2_3941.aFloat5603 + aClass373_Sub2_3941.aFloat5596 * (float) i_73_) + aClass373_Sub2_3941.aFloat5590) * (float) anInt3993 / (float) i_78_);
		int i_84_ = (int) ((aClass373_Sub2_3941.aFloat5597 * (float) i_77_ + (aClass373_Sub2_3941.aFloat5596 * (float) i_76_ + (float) i_75_ * aClass373_Sub2_3941.aFloat5603) + aClass373_Sub2_3941.aFloat5590) * (float) anInt3993 / (float) i_78_);
		if (aFloat3976 > (float) i_83_ && (float) i_84_ < aFloat3976)
			i_79_ |= 0x4;
		else if (aFloat4044 < (float) i_83_ && aFloat4044 < (float) i_84_)
			i_79_ |= 0x8;
		return i_79_;
	}

	public float[] method1135(int i) {
		if (i != 6)
			anInt3969 = -103;
		return aFloatArray3996;
	}

	public void method1136(Class373_Sub2 class373_sub2, int i) {
		aClass373_Sub2_3939.method3915(class373_sub2);
		aBoolean3940 = false;
		if (i != 11)
			aFloat3964 = -0.7512781F;
		method1163(17830);
	}

	public void method1137(int i, boolean bool) {
		if (i == 4) {
			if (bool == !aBoolean3973) {
				aBoolean3973 = bool;
				method1211((byte) 85);
				anInt4020 &= ~0x7;
			}
		}
	}

	abstract void method1138(int i);

	public void method1139(int i, int i_85_) {
		if (i_85_ != anInt4033) {
			anInt4033 = i_85_;
			method1228(i - 25685);
		}
		if (i != 0)
			a(null, 58, 42, -35, 41);
	}

	public void method1140(Interface6 interface6, boolean bool) {
		if (interface6 != anInterface6Array3986[anInt3991]) {
			anInterface6Array3986[anInt3991] = interface6;
			if (interface6 == null)
				method1134(25993);
			else
				interface6.method28(-9994);
			anInt4020 &= ~0x1;
		}
		if (bool) {
			/* empty */
		}
	}

	private void method1141(boolean bool) {
		aBoolean4021 = false;
		if (bool != true)
			aClass125Array3995 = null;
		method1183(false);
		if (Class130.aClass108_1339 == aClass108_3954)
			method1103((byte) 14);
	}

	public boolean j() {
		return true;
	}

	public void f(int i, int i_86_) {
		if (i != anInt4035 || i_86_ != anInt3981) {
			anInt3981 = i_86_;
			anInt4035 = i;
			method1141(true);
			method1155(-29995);
			method1166(28215);
		}
	}

	public void method1142(int i) {
		method1118(2, aa_Sub2.aClass289_3730, 0);
		if (i > -63)
			method1199(54);
	}

	public int q() {
		return anInt3950 - 2;
	}

	private void method1143(byte i) {
		anInterface15_Impl2_4052 = method1205(false, 63);
		anInterface15_Impl2_4052.method46(3096, -87, 12);
		if (i != 8)
			method1143((byte) -92);
		for (int i_87_ = 0; i_87_ < 4; i_87_++) {
			Buffer buffer = anInterface15_Impl2_4052.method47(true, i ^ ~0x1fad);
			if (buffer != null) {
				Stream stream = method1156(buffer, -122);
				stream.a(0.0F);
				stream.a(0.0F);
				stream.a(0.0F);
				for (int i_88_ = 0; i_88_ <= 256; i_88_++) {
					double d = (double) (i_88_ * 2) * 3.141592653589793 / 256.0;
					float f = (float) Math.cos(d);
					float f_89_ = (float) Math.sin(d);
					if (!Stream.a()) {
						stream.b(f_89_);
						stream.b(f);
						stream.b(0.0F);
					} else {
						stream.a(f_89_);
						stream.a(f);
						stream.a(0.0F);
					}
				}
				stream.b();
				if (anInterface15_Impl2_4052.method49(2968))
					break;
			}
		}
		aClass127_4048 = method1233((byte) -46, (new Class211[]{new Class211(Class356.aClass356_3072)}));
	}

	abstract float method1144(int i);

	public void method1145(byte i, int i_90_) {
		if (i_90_ != 1)
			anInt3982 = -126;
		method1139(0, i | (i << 8 | (i << 16 | i << 24)));
	}

	void method1146(byte i) {
		method1127(false);
		if (i != 62)
			method1112(true);
	}

	abstract boolean method1147(Class67 class67, Class202 class202, byte i);

	abstract void method1148(int i);

	abstract void method1149(int i);

	abstract void method1150(boolean bool);

	public int M() {
		return anInt4061;
	}

	public void method1151(int i, int i_91_) {
		if (anInt3991 != i) {
			anInt3991 = i;
			method1188(94);
		}
		if (i_91_ != 16760)
			aClass127_4051 = null;
	}

	abstract void method1152(int i);

	public void za(int i, int i_92_, int i_93_, int i_94_, int i_95_) {
		method1196((byte) -90);
		method1139(0, i_94_);
		method1158((byte) -125, 0, Class151.aClass287_1553);
		method1219((byte) 54, Class151.aClass287_1553, 0);
		method1161(31156, i_95_);
		aClass373_Sub2_3939.method3931(1.0F, (float) i_93_, (float) i_93_, -121);
		aClass373_Sub2_3939.method3904(i, i_92_, 0);
		method1202(0);
		method1101(false, (byte) -96);
		method1178(anInterface15_Impl2_4052, (byte) -122, 0);
		method1209(aClass127_4048, (byte) -100);
		method1185(0, 0, aa_Sub2.aClass289_3730, 256);
		method1101(true, (byte) -69);
		method1219((byte) 54, Class199.aClass287_2007, 0);
		method1158((byte) -116, 0, Class199.aClass287_2007);
	}

	public Class373_Sub2 method1153(int i) {
		if (i <= 93)
			anInt4061 = 13;
		return aClass373_Sub2Array3965[anInt3991];
	}

	public boolean B() {
		return true;
	}

	public boolean d() {
		return aBoolean3974;
	}

	abstract Interface15_Impl1 method1154(boolean bool, int i);

	private void method1155(int i) {
		aBoolean3959 = false;
		if (i != -29995)
			a((Canvas) null);
		method1198(i + 29899);
		if (aClass108_3954 == Class130.aClass108_1337)
			method1103((byte) 14);
	}

	public Stream method1156(Buffer buffer, int i) {
		aStream3975.a(buffer);
		if (i >= -98)
			method1202(110);
		return aStream3975;
	}

	public void a(int i, int i_96_, int i_97_, int i_98_, int i_99_, int i_100_, int i_101_) {
		float f = (float) -i + (float) i_97_;
		float f_102_ = (float) i_98_ - (float) i_96_;
		float f_103_ = 0.0F;
		float f_104_ = 1.0F;
		if (f != 0.0F || f_102_ != 0.0F) {
			f_104_ = (float) Math.sqrt((double) (f_102_ * f_102_ + f * f));
			f_103_ = (float) Math.atan2((double) f_102_, (double) f);
		}
		method1196((byte) -94);
		method1139(0, i_99_);
		method1158((byte) -104, 0, Class151.aClass287_1553);
		method1219((byte) 54, Class151.aClass287_1553, 0);
		method1161(31156, i_101_);
		aClass373_Sub2_3939.method3931(1.0F, (float) i_100_, f_104_, -61);
		aClass373_Sub2_3939.method3904(0, -i_100_ / 2, 0);
		aClass373_Sub2_3939.method3900((int) ((double) f_103_ * 2607.5945876176133) & 0x3fff);
		aClass373_Sub2_3939.method3904(i, i_96_, 0);
		method1202(0);
		method1101(false, (byte) 87);
		method1142(-71);
		method1101(true, (byte) -114);
		method1219((byte) 54, Class199.aClass287_2007, 0);
		method1158((byte) -120, 0, Class199.aClass287_2007);
	}

	public void a(Class390 class390, int i) {
		aClass152_4045.method1547(class390, this, i, (byte) 125);
	}

	abstract void method1157(boolean bool, Class246 class246);

	public void method1158(byte i, int i_105_, Class287 class287) {
		if (i >= -96)
			anInt4031 = -114;
		method1170(i_105_, class287, false, false, (byte) -46);
	}

	abstract Interface6_Impl2 method1159(boolean bool, int i, Class202 class202, int i_106_, int i_107_, byte[] is);

	public Sprite a(int[] is, int i, int i_108_, int i_109_, int i_110_, boolean bool) {
		return new Class397_Sub2(this, i_109_, i_110_, is, i, i_108_);
	}

	public void da(int i, int i_111_, int i_112_, int[] is) {
		float f = aClass373_Sub2_3941.method3926((float) i_111_, (byte) 53, (float) i, (float) i_112_);
		int i_113_;
		int i_114_;
		if (!(f < -0.0078125F) && !(f > 0.0078125F)) {
			i_113_ = anInt3970;
			i_114_ = anInt3988;
		} else {
			i_113_ = (int) ((float) anInt3992 * aClass373_Sub2_3941.method3948((float) i, (float) i_111_, 12, (float) i_112_) / f);
			i_114_ = (int) ((float) anInt3993 * aClass373_Sub2_3941.method3941((float) i, (byte) -125, (float) i_112_, (float) i_111_) / f);
		}
		is[1] = (int) ((float) i_114_ - aFloat3976);
		is[2] = (int) f;
		is[0] = (int) (-aFloat4018 + (float) i_113_);
	}

	public boolean z() {
		return true;
	}

	public void ZA(int i, float f, float f_115_, float f_116_, float f_117_, float f_118_) {
		boolean bool = anInt3971 != i;
		if (bool || aFloat4022 != f || f_115_ != aFloat4007) {
			aFloat4007 = f_115_;
			anInt3971 = i;
			aFloat4022 = f;
			if (bool) {
				aFloat4000 = (float) (anInt3971 & 0xff0000) / 1.671168E7F;
				aFloat4043 = (float) (anInt3971 & 0xff) / 255.0F;
				aFloat4019 = (float) (anInt3971 & 0xff00) / 65280.0F;
				method1112(false);
			}
			aNativeInterface3923.setSunColour(aFloat4000, aFloat4019, aFloat4043, f, f_115_);
			method1189((byte) 75);
		}
		if (f_116_ != aFloatArray3967[0] || aFloatArray3967[1] != f_117_ || f_118_ != aFloatArray3967[2]) {
			aFloatArray3967[2] = f_118_;
			aFloatArray3967[1] = f_117_;
			aFloatArray3967[0] = f_116_;
			aFloatArray3962[1] = -f_117_;
			aFloatArray3962[0] = -f_116_;
			aFloatArray3962[2] = -f_118_;
			float f_119_ = (float) (1.0 / Math.sqrt((double) (f_117_ * f_117_ + f_116_ * f_116_ + f_118_ * f_118_)));
			aFloatArray4030[0] = f_116_ * f_119_;
			aFloatArray4030[2] = f_118_ * f_119_;
			aFloatArray4030[1] = f_119_ * f_117_;
			aFloatArray4028[2] = -aFloatArray4030[2];
			aFloatArray4028[1] = -aFloatArray4030[1];
			aFloatArray4028[0] = -aFloatArray4030[0];
			aNativeInterface3923.setSunDirection(aFloatArray4030[0], aFloatArray4030[1], aFloatArray4030[2]);
			method1138(0);
			anInt4027 = (int) (f_118_ * 256.0F / f_117_);
			anInt3969 = (int) (f_116_ * 256.0F / f_117_);
		}
		method1162(false);
	}

	abstract Interface6_Impl1 method1160(Class202 class202, int i, boolean bool, int i_120_, float[] fs, int i_121_, int i_122_, int i_123_);

	public void method1161(int i, int i_124_) {
		if (i_124_ != anInt4025) {
			Class258 class258;
			boolean bool;
			boolean bool_125_;
			if (i_124_ != 1) {
				if (i_124_ == 2) {
					bool = false;
					class258 = HardReferenceWrapper.aClass258_6699;
					bool_125_ = true;
				} else if (i_124_ != 128) {
					class258 = Queue.aClass258_1439;
					bool_125_ = false;
					bool = false;
				} else {
					class258 = ConfigurationsLoader.aClass258_89;
					bool = true;
					bool_125_ = true;
				}
			} else {
				class258 = StaticMethods.aClass258_5957;
				bool = true;
				bool_125_ = true;
			}
			if (!bool != !aBoolean3979) {
				aBoolean3979 = bool;
				method1108(true);
			}
			if (aBoolean4015 != bool_125_) {
				aBoolean4015 = bool_125_;
				method1216(i - 31273);
			}
			if (aClass258_4029 != class258) {
				aClass258_4029 = class258;
				method1184((byte) -109);
			}
			anInt4025 = i_124_;
			anInt4020 &= ~0x1c;
		}
		if (i != 31156)
			anInt3956 = 76;
	}

	abstract void method1162(boolean bool);

	private void method1163(int i) {
		if (Class296_Sub45_Sub3.aClass108_6293 == aClass108_3954) {
			float f = method1144(1);
			aClass373_Sub2_3939.method3946(f, 0.0F, i - 1447, f);
		}
		aBoolean3961 = false;
		method1152(111);
		if (i != 17830)
			method1120(false, (byte) 0);
		if (aClass360_4016 != null)
			aClass360_4016.method3724((byte) -50);
	}

	public int e(int i, int i_126_) {
		return i_126_ ^ i & i_126_;
	}

	private void method1164(int i) {
		anInterface15_Impl2_4054 = method1205(false, i ^ ~0x7b);
		anInterface15_Impl2_4054.method46(140, -121, 28);
		for (int i_127_ = i; i_127_ < 4; i_127_++) {
			Buffer buffer = anInterface15_Impl2_4054.method47(true, -8102);
			if (buffer != null) {
				Stream stream = method1156(buffer, -108);
				if (!Stream.a()) {
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(1.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(1.0F);
					stream.b(0.0F);
					stream.b(1.0F);
					stream.b(1.0F);
					stream.b(1.0F);
					stream.b(0.0F);
					stream.b(1.0F);
					stream.b(1.0F);
					stream.b(1.0F);
					stream.b(1.0F);
					stream.b(1.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(1.0F);
					stream.b(0.0F);
					stream.b(1.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(0.0F);
				} else {
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(1.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(1.0F);
					stream.a(0.0F);
					stream.a(1.0F);
					stream.a(1.0F);
					stream.a(1.0F);
					stream.a(0.0F);
					stream.a(1.0F);
					stream.a(1.0F);
					stream.a(1.0F);
					stream.a(1.0F);
					stream.a(1.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(1.0F);
					stream.a(0.0F);
					stream.a(1.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(0.0F);
				}
				stream.b();
				if (anInterface15_Impl2_4054.method49(2968))
					break;
			}
		}
		aClass127_4053 = method1233((byte) -46, (new Class211[]{new Class211(new Class356[]{Class356.aClass356_3072, Class356.aClass356_3077, Class356.aClass356_3077})}));
	}

	abstract void method1165(int i);

	private void method1166(int i) {
		if (aClass360_4016 != null)
			aClass360_4016.method3719((byte) 76);
		if (i == 28215)
			method1224(true);
	}

	public void method1167(byte i) {
		if (anInt4020 != 4) {
			method1176(-25);
			method1201(-25357, false);
			method1137(i + 100, false);
			method1110(false, -89);
			method1106(-81, false);
			method1133(false, -2, 0, false);
			method1161(31156, 1);
			method1197((byte) 22, 0);
			anInt4020 = 4;
		}
		if (i != -96)
			aBoolean3946 = true;
	}

	public Sprite a(int i, int i_128_, boolean bool) {
		return new Class397_Sub2(this, i, i_128_, bool);
	}

	public void method1168(boolean bool) {
		method1105((byte) -125);
		method1103((byte) 14);
		if (bool)
			r(-11, -22, -95, -58, 30, -102, 51);
	}

	public void a(Class373 class373) {
		aClass373_Sub2_3941 = (Class373_Sub2) class373;
		aClass373_Sub2_3943.method3915(aClass373_Sub2_3941);
		aClass373_Sub2_3943.method3928(11554);
		aClass373_Sub2_3944.method3935(4, aClass373_Sub2_3943);
		aClass373_Sub2_3942.method3935(4, aClass373_Sub2_3941);
		if (aClass108_3954.method947(27229))
			method1231(false);
	}

	public int method1169(byte i) {
		if (i != 118)
			a((Class390) null, -25);
		return anInt4012;
	}

	abstract void method1170(int i, Class287 class287, boolean bool, boolean bool_129_, byte i_130_);

	public void la() {
		anInt4040 = anInt3920;
		anInt3987 = 0;
		anInt4039 = 0;
		anInt3982 = anInt3921;
		if (aBoolean4062) {
			aBoolean4062 = false;
			method1149(25154);
		}
		method1191(-101);
	}

	public void A(int i, aa var_aa, int i_131_, int i_132_) {
		aa_Sub3 var_aa_Sub3 = (aa_Sub3) var_aa;
		Interface6_Impl1 interface6_impl1 = var_aa_Sub3.anInterface6_Impl1_3731;
		method1229(-3);
		method1140(interface6_impl1, false);
		method1161(31156, 1);
		method1177(Class41_Sub4.aClass125_3745, 9815, Class41_Sub4.aClass125_3745);
		method1158((byte) -97, 0, Class151.aClass287_1553);
		method1139(0, i);
		aClass373_Sub2_3939.method3931(0.0F, (float) anInt3920, (float) anInt3921, -19);
		method1202(0);
		aClass373_Sub2Array3965[0].method3931(1.0F, interface6_impl1.method71((float) anInt3920, (byte) -66), interface6_impl1.method69(-108, (float) anInt3921), -54);
		aClass373_Sub2Array3965[0].method3946(interface6_impl1.method71((float) -i_132_, (byte) 109), 0.0F, 16383, interface6_impl1.method69(-108, (float) -i_131_));
		aClass230Array3966[0] = Class360_Sub5.aClass230_5325;
		method1121((byte) 125);
		method1142(-88);
		method1125(0);
		method1158((byte) -120, 0, Class199.aClass287_2007);
	}

	public int XA() {
		return anInt3981;
	}

	public void a(int i, int i_133_, int i_134_, int i_135_, int i_136_, int i_137_, int i_138_, int i_139_, int i_140_) {
		float f = (float) -i + (float) i_134_;
		float f_141_ = (float) -i_133_ + (float) i_135_;
		if (f == 0.0F && f_141_ == 0.0F)
			f = 1.0F;
		else {
			float f_142_ = (float) (1.0 / Math.sqrt((double) (f_141_ * f_141_ + f * f)));
			f *= f_142_;
			f_141_ *= f_142_;
		}
		method1196((byte) -64);
		method1139(0, i_136_);
		method1158((byte) -101, 0, Class151.aClass287_1553);
		method1219((byte) 54, Class151.aClass287_1553, 0);
		method1161(31156, i_137_);
		method1102(-116);
		i_140_ %= i_138_ + i_139_;
		method1101(false, (byte) -49);
		float f_143_ = f * (float) i_138_;
		float f_144_ = (float) i_138_ * f_141_;
		float f_145_ = 0.0F;
		float f_146_ = 0.0F;
		float f_147_ = f_143_;
		float f_148_ = f_144_;
		if (i_140_ <= i_138_) {
			f_148_ = (float) (i_138_ - i_140_) * f_141_;
			f_147_ = (float) (-i_140_ + i_138_) * f;
		} else {
			f_145_ = f * (float) (i_139_ + (i_138_ - i_140_));
			f_146_ = f_141_ * (float) (i_138_ + i_139_ - i_140_);
		}
		float f_149_ = (float) i + f_145_;
		float f_150_ = (float) i_133_ + f_146_;
		float f_151_ = (float) i_139_ * f;
		float f_152_ = f_141_ * (float) i_139_;
		for (;;) {
			if (i_134_ > i) {
				if (f_149_ > (float) i_134_)
					break;
				if ((float) i_134_ < f_147_ + f_149_)
					f_147_ = -f_149_ + (float) i_134_;
			} else {
				if ((float) i_134_ > f_149_)
					break;
				if (f_147_ + f_149_ < (float) i_134_)
					f_147_ = (float) i_134_ - f_149_;
			}
			if (i_135_ <= i_133_) {
				if ((float) i_135_ > f_150_)
					break;
				if ((float) i_135_ > f_148_ + f_150_)
					f_148_ = -f_150_ + (float) i_135_;
			} else {
				if ((float) i_135_ < f_150_)
					break;
				if ((float) i_135_ < f_148_ + f_150_)
					f_148_ = (float) i_135_ - f_150_;
			}
			if (!method1212(f_148_ + f_150_, f_150_, 0.0F, f_149_, 0.0F, true, f_147_ + f_149_))
				return;
			f_150_ += f_148_ + f_152_;
			method1126((byte) -55);
			f_149_ += f_151_ + f_147_;
			f_148_ = f_144_;
			f_147_ = f_143_;
		}
		method1101(true, (byte) 77);
		method1219((byte) 54, Class199.aClass287_2007, 0);
		method1158((byte) -113, 0, Class199.aClass287_2007);
	}

	public int d(int i, int i_153_) {
		return i | i_153_;
	}

	public void aa(int i, int i_154_, int i_155_, int i_156_, int i_157_, int i_158_) {
		method1196((byte) 104);
		method1139(0, i_157_);
		method1158((byte) -104, 0, Class151.aClass287_1553);
		method1219((byte) 54, Class151.aClass287_1553, 0);
		method1161(31156, i_158_);
		aClass373_Sub2_3939.method3931(1.0F, (float) i_156_, (float) i_155_, -92);
		aClass373_Sub2_3939.method3904(i, i_154_, 0);
		method1202(0);
		method1101(false, (byte) 73);
		method1142(-112);
		method1101(true, (byte) -84);
		method1219((byte) 54, Class199.aClass287_2007, 0);
		method1158((byte) -99, 0, Class199.aClass287_2007);
	}

	abstract void method1171(byte i, boolean bool, int i_159_, Class287 class287);

	private void method1172(boolean bool) {
		if (aCanvas3930 != null) {
			Dimension dimension = aCanvas3930.getSize();
			anInt3935 = dimension.width;
			anInt3922 = dimension.height;
		} else
			anInt3935 = anInt3922 = 1;
		anInt3921 = anInt3935;
		anInt3920 = anInt3922;
		method1175(0);
		method1141(true);
		method1155(-29995);
		method1204(!bool);
		method1191(-92);
		if (!bool) {
			method1105((byte) 108);
			la();
		}
	}

	abstract void method1173(int i);

	public boolean a() {
		return true;
	}

	public Class373_Sub2 method1174(int i) {
		if (i != 65280)
			return null;
		return aClass373_Sub2_3943;
	}

	public void a(int i, int i_160_, int i_161_, int i_162_, int i_163_, int i_164_, aa var_aa, int i_165_, int i_166_) {
		/* empty */
	}

	private void method1175(int i) {
		aBoolean4004 = false;
		if (Class296_Sub45_Sub3.aClass108_6293 == aClass108_3954) {
			method1222((byte) -106);
			method1103((byte) 14);
		}
		if (i != 0)
			method1211((byte) 114);
	}

	public int[] Y() {
		return new int[]{anInt3970, anInt3988, anInt3992, anInt3993};
	}

	public void a(Canvas canvas, int i, int i_167_) {
		Object object = null;
		if (canvas != null && canvas != aCanvas3926) {
			if (aHashtable3925.containsKey(canvas))
				object = aHashtable3925.get(canvas);
		} else
			object = anObject3924;
		if (object == null)
			throw new RuntimeException();
		method1208(object, canvas, (byte) 124);
		if (aCanvas3930 == canvas)
			method1172(false);
	}

	private void method1176(int i) {
		if (Class296_Sub45_Sub3.aClass108_6293 != aClass108_3954) {
			Class108 class108 = aClass108_3954;
			aClass108_3954 = Class296_Sub45_Sub3.aClass108_6293;
			if (class108.method947(i + 27254))
				method1231(false);
			method1222((byte) -106);
			aFloatArray3949 = aFloatArray3998;
			method1103((byte) 14);
			anInt4020 &= ~0x18;
		}
		if (i != -25)
			method1111(55);
	}

	public void a(int i, int i_168_, int i_169_, int i_170_, int i_171_, int i_172_, aa var_aa, int i_173_, int i_174_, int i_175_, int i_176_, int i_177_) {
		/* empty */
	}

	public void H(int i, int i_178_, int i_179_, int[] is) {
		float f = aClass373_Sub2_3941.method3926((float) i_178_, (byte) 53, (float) i, (float) i_179_);
		int i_180_;
		int i_181_;
		if (f < -0.0078125F || f > 0.0078125F) {
			i_181_ = (int) ((float) anInt3992 * aClass373_Sub2_3941.method3948((float) i, (float) i_178_, 12, (float) i_179_) / f);
			i_180_ = (int) ((float) anInt3993 * aClass373_Sub2_3941.method3941((float) i, (byte) -125, (float) i_179_, (float) i_178_) / f);
		} else {
			i_180_ = anInt3988;
			i_181_ = anInt3970;
		}
		is[2] = (int) f;
		is[0] = (int) (-aFloat4018 + (float) i_181_);
		is[1] = (int) ((float) i_180_ - aFloat3976);
	}

	public void L(int i, int i_182_, int i_183_) {
		if (i != anInt3947 || i_182_ != anInt3990 || i_183_ != anInt3960) {
			anInt3960 = i_183_;
			anInt3947 = i;
			anInt3990 = i_182_;
			method1166(28215);
			method1148(30);
		}
	}

	public Class373 o() {
		return aClass373_Sub2_3941;
	}

	public void method1177(Class125 class125, int i, Class125 class125_184_) {
		if (i != 9815)
			method1219((byte) 126, null, -124);
		boolean bool = false;
		if (aClass125Array4003[anInt3991] != class125) {
			aClass125Array4003[anInt3991] = class125;
			method1150(true);
			bool = true;
		}
		if (aClass125Array3995[anInt3991] != class125_184_) {
			aClass125Array3995[anInt3991] = class125_184_;
			bool = true;
			method1165(2147483647);
		}
		if (bool)
			anInt4020 &= ~0x1d;
	}

	public boolean l() {
		return aClass360Array3999[3].method3723((byte) 55);
	}

	public int JA(int i, int i_185_, int i_186_, int i_187_, int i_188_, int i_189_) {
		int i_190_ = 0;
		float f = (aClass373_Sub2_3941.aFloat5594 + (aClass373_Sub2_3941.aFloat5601 * (float) i + aClass373_Sub2_3941.aFloat5599 * (float) i_185_ + aClass373_Sub2_3941.aFloat5591 * (float) i_186_));
		if (f < 1.0F)
			f = 1.0F;
		float f_191_ = (aClass373_Sub2_3941.aFloat5601 * (float) i_187_ + aClass373_Sub2_3941.aFloat5599 * (float) i_188_ + aClass373_Sub2_3941.aFloat5591 * (float) i_189_ + aClass373_Sub2_3941.aFloat5594);
		if (f_191_ < 1.0F)
			f_191_ = 1.0F;
		if ((float) anInt4035 > f && f_191_ < (float) anInt4035)
			i_190_ |= 0x10;
		else if (f > (float) anInt3981 && (float) anInt3981 < f_191_)
			i_190_ |= 0x20;
		int i_192_ = (int) ((float) anInt3992 * (aClass373_Sub2_3941.aFloat5593 + ((float) i_185_ * aClass373_Sub2_3941.aFloat5595 + (float) i * aClass373_Sub2_3941.aFloat5598 + aClass373_Sub2_3941.aFloat5602 * (float) i_186_)) / f);
		int i_193_ = (int) ((float) anInt3992 * (aClass373_Sub2_3941.aFloat5593 + (aClass373_Sub2_3941.aFloat5602 * (float) i_189_ + (aClass373_Sub2_3941.aFloat5598 * (float) i_187_ + (aClass373_Sub2_3941.aFloat5595 * (float) i_188_)))) / f_191_);
		if (!(aFloat4018 > (float) i_192_) || !(aFloat4018 > (float) i_193_)) {
			if ((float) i_192_ > aFloat4034 && (float) i_193_ > aFloat4034)
				i_190_ |= 0x2;
		} else
			i_190_ |= 0x1;
		int i_194_ = (int) ((float) anInt3993 * (aClass373_Sub2_3941.aFloat5590 + (aClass373_Sub2_3941.aFloat5603 * (float) i + (float) i_185_ * aClass373_Sub2_3941.aFloat5596 + (float) i_186_ * aClass373_Sub2_3941.aFloat5597)) / f);
		int i_195_ = (int) ((float) anInt3993 * (aClass373_Sub2_3941.aFloat5590 + ((float) i_187_ * aClass373_Sub2_3941.aFloat5603 + (float) i_188_ * aClass373_Sub2_3941.aFloat5596 + aClass373_Sub2_3941.aFloat5597 * (float) i_189_)) / f_191_);
		if ((float) i_194_ < aFloat3976 && aFloat3976 > (float) i_195_)
			i_190_ |= 0x4;
		else if ((float) i_194_ > aFloat4044 && (float) i_195_ > aFloat4044)
			i_190_ |= 0x8;
		return i_190_;
	}

	abstract void method1178(Interface15_Impl2 interface15_impl2, byte i, int i_196_);

	public Interface6_Impl1 method1179(int i, int i_197_, boolean bool, int[] is, int i_198_) {
		if (i_198_ != -461)
			aClass125Array4003 = null;
		return method1187(i, (byte) -122, is, 0, 0, i_197_, bool);
	}

	public Interface6_Impl1 method1180(boolean bool, int i, Class202 class202, byte[] is, int i_199_, int i_200_) {
		if (i != 7)
			return null;
		return method1195(true, i_200_, is, class202, i_199_, bool, 0, 0);
	}

	public void method1181(Class230 class230, byte i) {
		aClass230Array3966[anInt3991] = class230;
		method1121(i);
	}

	public float[] method1182(int i, float[] fs) {
		fs[8] = aFloatArray3949[2];
		fs[4] = aFloatArray3949[1];
		fs[0] = aFloatArray3949[0];
		fs[12] = aFloatArray3949[3];
		fs[9] = aFloatArray3949[6];
		fs[13] = aFloatArray3949[7];
		fs[5] = aFloatArray3949[5];
		fs[i] = aFloatArray3949[4];
		fs[2] = aFloatArray3949[8];
		fs[3] = aFloatArray3949[12];
		fs[10] = aFloatArray3949[10];
		fs[7] = aFloatArray3949[13];
		fs[6] = aFloatArray3949[9];
		fs[14] = aFloatArray3949[11];
		fs[11] = aFloatArray3949[14];
		fs[15] = aFloatArray3949[15];
		return fs;
	}

	private void method1183(boolean bool) {
		if (!aBoolean4021) {
			float[] fs = aFloatArray3958;
			float f = (float) (anInt4035 * -anInt3970) / (float) anInt3992;
			float f_201_ = ((float) ((-anInt3970 + anInt3921) * anInt4035) / (float) anInt3992);
			float f_202_ = (float) (anInt3988 * anInt4035) / (float) anInt3993;
			float f_203_ = ((float) (anInt4035 * (anInt3988 - anInt3920)) / (float) anInt3993);
			if (f_201_ == f || f_202_ == f_203_) {
				fs[15] = 1.0F;
				fs[0] = 1.0F;
				fs[7] = 0.0F;
				fs[5] = 1.0F;
				fs[4] = 0.0F;
				fs[11] = 0.0F;
				fs[9] = 0.0F;
				fs[10] = 1.0F;
				fs[14] = 0.0F;
				fs[13] = 0.0F;
				fs[6] = 0.0F;
				fs[1] = 0.0F;
				fs[12] = 0.0F;
				fs[8] = 0.0F;
				fs[3] = 0.0F;
				fs[2] = 0.0F;
			} else {
				float f_204_ = (float) anInt4035 * 2.0F;
				fs[12] = 0.0F;
				fs[10] = aFloat3985 = (float) anInt3981 / (float) (anInt4035 - anInt3981);
				fs[3] = 0.0F;
				fs[0] = f_204_ / (f_201_ - f);
				fs[13] = 0.0F;
				fs[2] = 0.0F;
				fs[4] = 0.0F;
				fs[14] = aFloat4001 = (float) (anInt3981 * anInt4035) / (float) (-anInt3981 + anInt4035);
				fs[8] = (f + f_201_) / (-f + f_201_);
				fs[15] = 0.0F;
				fs[9] = (f_202_ + f_203_) / (-f_203_ + f_202_);
				fs[7] = 0.0F;
				fs[11] = -1.0F;
				fs[5] = f_204_ / (-f_203_ + f_202_);
				fs[1] = 0.0F;
				fs[6] = 0.0F;
			}
			method1131(!bool);
			aBoolean4021 = true;
		}
		if (bool)
			c(-76, 1, -81, -13, 59, -57);
	}

	abstract void method1184(byte i);

	abstract void method1185(int i, int i_205_, Class289 class289, int i_206_);

	public Class55 a(Class92 class92, Class186[] class186s, boolean bool) {
		return new Class55_Sub2(this, class92, class186s, bool);
	}

	public boolean n() {
		return false;
	}

	abstract void method1186(int i);

	public void X(int i) {
		anInt4036 = 0;
		for (/**/; i > 1; i >>= 1)
			anInt4036++;
		anInt3951 = 1 << anInt4036;
	}

	abstract Interface6_Impl1 method1187(int i, byte i_207_, int[] is, int i_208_, int i_209_, int i_210_, boolean bool);

	abstract void method1188(int i);

	abstract void method1189(byte i);

	abstract void method1190(Object object, byte i, Canvas canvas);

	public void U(int i, int i_211_, int i_212_, int i_213_, int i_214_) {
		e(i, i_211_, i + i_212_, i_211_, i_213_, i_214_);
	}

	private void method1191(int i) {
		aFloat4034 = (float) (anInt3982 - anInt3970);
		aFloat4044 = (float) (-anInt3988 + anInt4040);
		aFloat4018 = (float) (anInt3987 - anInt3970);
		aFloat3976 = (float) (anInt4039 - anInt3988);
		if (i >= -31)
			aClass127_4059 = null;
	}

	public Class373 f() {
		return aClass373_Sub2_4060;
	}

	private void method1192(int i) {
		if (aClass108_3954 != Class130.aClass108_1339) {
			Class108 class108 = aClass108_3954;
			aClass108_3954 = Class130.aClass108_1339;
			if (!class108.method947(i ^ 0x6a5d))
				method1231(false);
			method1183(false);
			aFloatArray3949 = aFloatArray3958;
			method1103((byte) 14);
			anInt4020 &= ~0x7;
		}
		if (i != 0)
			anInt4011 = -128;
	}

	public void b(Canvas canvas, int i, int i_215_) {
		if (aCanvas3926 == canvas)
			throw new RuntimeException();
		if (!aHashtable3925.containsKey(canvas)) {
			if (!canvas.isShowing())
				throw new RuntimeException();
			try {
				Class var_class = java.awt.Canvas.class;
				Method method = var_class.getMethod("setIgnoreRepaint", new Class[]{Boolean.TYPE});
				method.invoke(canvas, new Object[]{Boolean.TRUE});
			} catch (Exception exception) {
				/* empty */
			}
			Object object = method1100(116, canvas);
			if (object == null)
				throw new RuntimeException();
			aHashtable3925.put(canvas, object);
		}
	}

	public void method1193(int i) {
		aClass125Array3995 = new Class125[anInt4031];
		aClass230Array3966 = new Class230[anInt4031];
		anInterface6Array3986 = new Interface6[anInt4031];
		aClass125Array4003 = new Class125[anInt4031];
		aClass373_Sub2Array3965 = new Class373_Sub2[anInt4031];
		for (int i_216_ = 0; anInt4031 > i_216_; i_216_++) {
			aClass125Array3995[i_216_] = Js5.aClass125_1411;
			aClass125Array4003[i_216_] = Js5.aClass125_1411;
			aClass230Array3966[i_216_] = Class67.aClass230_754;
			aClass373_Sub2Array3965[i_216_] = new Class373_Sub2();
		}
		aClass296_Sub35Array4005 = new Class296_Sub35[anInt3950 - 2];
		anInterface6_4037 = method1107(1, -88, 1, Class67.aClass67_745, za_Sub2.aClass202_6555);
		a(new za_Sub1(262144));
		aClass127_4059 = method1233((byte) -46, (new Class211[]{new Class211(new Class356[]{Class356.aClass356_3072, Class356.aClass356_3077})}));
		aClass127_4057 = method1233((byte) -46, (new Class211[]{new Class211(new Class356[]{Class356.aClass356_3072, Class356.aClass356_3075})}));
		aClass127_4047 = method1233((byte) -46, (new Class211[]{new Class211(Class356.aClass356_3072), new Class211(Class356.aClass356_3075), new Class211(Class356.aClass356_3077), new Class211(Class356.aClass356_3073)}));
		aClass127_4051 = method1233((byte) -46, (new Class211[]{new Class211(Class356.aClass356_3072), new Class211(Class356.aClass356_3075), new Class211(Class356.aClass356_3077)}));
		for (int i_217_ = 0; i_217_ < 7; i_217_++) {
			aClass178_Sub3Array4056[i_217_] = new Class178_Sub3(this, 0, 0, false, false);
			aClass178_Sub3Array4058[i_217_] = new Class178_Sub3(this, 0, 0, true, true);
		}
		aClass152_4045 = new Class152(this);
		anInterface15_Impl1_4050 = method1154(true, -107);
		method1213((byte) 74);
		aClass184_3929 = new Class184(this);
		if (i == -16649) {
			aClass360Array3999[1] = method1119(22940, 1);
			aClass360Array3999[2] = method1119(i + 39589, 2);
			aClass360Array3999[4] = method1119(22940, 4);
			aClass360Array3999[5] = method1119(Class294.method2420(i, -6293), 5);
			aClass360Array3999[6] = method1119(22940, 6);
			aClass360Array3999[7] = method1119(22940, 7);
			aClass360Array3999[3] = method1119(Class294.method2420(i, -6293), 3);
			aClass360Array3999[8] = method1119(22940, 8);
			aClass360Array3999[9] = method1119(22940, 9);
			if (!aClass360Array3999[2].method3723((byte) 49))
				aClass360Array3999[2] = method1119(i + 39589, 0);
			if (!aClass360Array3999[4].method3723((byte) 6))
				aClass360Array3999[4] = aClass360Array3999[2];
			if (!aClass360Array3999[8].method3723((byte) -109))
				aClass360Array3999[8] = aClass360Array3999[4];
			if (!aClass360Array3999[9].method3723((byte) 1))
				aClass360Array3999[9] = aClass360Array3999[8];
			method1146((byte) 62);
			la();
			this.w();
		}
	}

	abstract void method1194(byte i);

	abstract Interface6_Impl1 method1195(boolean bool, int i, byte[] is, Class202 class202, int i_218_, boolean bool_219_, int i_220_, int i_221_);

	private void method1196(byte i) {
		int i_222_ = -102 % ((i + 10) / 43);
		if (anInt4020 != 1) {
			method1176(-25);
			method1201(-25357, false);
			method1137(4, false);
			method1110(false, -60);
			method1106(-108, false);
			method1133(false, -2, 0, false);
			method1197((byte) 22, 1);
			method1140(anInterface6_4037, false);
			anInt4020 = 1;
		}
	}

	public void method1197(byte i, int i_223_) {
		if (i != 22)
			x();
		if (i_223_ == 1)
			method1177(Class41_Sub4.aClass125_3745, 9815, Class41_Sub4.aClass125_3745);
		else if (i_223_ == 0)
			method1177(Js5.aClass125_1411, 9815, Js5.aClass125_1411);
		else if (i_223_ == 2)
			method1177(Class280.aClass125_2589, 9815, Class41_Sub4.aClass125_3745);
		else if (i_223_ == 3)
			method1177(Class41_Sub1.aClass125_3735, 9815, Js5.aClass125_1411);
		else if (i_223_ == 4)
			method1177(Class390.aClass125_3283, 9815, Class390.aClass125_3283);
	}

	private void method1198(int i) {
		if (!aBoolean3959) {
			float[] fs = aFloatArray3994;
			float f = (float) anInt4035;
			float f_224_ = (float) anInt3981;
			float f_225_ = aFloat3953 * (float) -anInt3988 / (float) anInt3993;
			float f_226_ = (float) -anInt3970 * aFloat3953 / (float) anInt3992;
			float f_227_ = (aFloat3953 * (float) (-anInt3970 + anInt3921) / (float) anInt3992);
			float f_228_ = (aFloat3953 * (float) (anInt3920 - anInt3988) / (float) anInt3993);
			if (f_227_ == f_226_ || f_228_ == f_225_) {
				fs[13] = 0.0F;
				fs[1] = 0.0F;
				fs[11] = 0.0F;
				fs[3] = 0.0F;
				fs[12] = 0.0F;
				fs[7] = 0.0F;
				fs[14] = 0.0F;
				fs[5] = 1.0F;
				fs[9] = 0.0F;
				fs[6] = 0.0F;
				fs[2] = 0.0F;
				fs[0] = 1.0F;
				fs[4] = 0.0F;
				fs[10] = 1.0F;
				fs[8] = 0.0F;
				fs[15] = 1.0F;
			} else {
				fs[8] = 0.0F;
				fs[12] = (f_227_ + f_226_) / (-f_227_ + f_226_);
				fs[6] = 0.0F;
				fs[10] = 1.0F / (-f_224_ + f);
				fs[0] = 2.0F / (f_227_ - f_226_);
				fs[7] = 0.0F;
				fs[4] = 0.0F;
				fs[15] = 1.0F;
				fs[9] = 0.0F;
				fs[11] = 0.0F;
				fs[5] = 2.0F / (f_228_ - f_225_);
				fs[13] = (f_228_ + f_225_) / (-f_225_ + f_228_);
				fs[2] = 0.0F;
				fs[1] = 0.0F;
				fs[3] = 0.0F;
				fs[14] = f / (f - f_224_);
			}
			method1207((byte) 24);
			aBoolean3959 = true;
		}
		if (i >= -83) {
			/* empty */
		}
	}

	public void EA(int i, int i_229_, int i_230_, int i_231_) {
		if (!aBoolean4038)
			throw new RuntimeException("");
		anInt3955 = i_231_;
		anInt4008 = i;
		anInt4012 = i_230_;
		anInt4013 = i_229_;
		if (aBoolean3980) {
			aClass360Array3999[3].method3726((byte) -109);
			aClass360Array3999[3].method3719((byte) 57);
		}
	}

	public Class373 m() {
		return new Class373_Sub2();
	}

	public void method1199(int i) {
		Enumeration enumeration = aHashtable3925.keys();
		while (enumeration.hasMoreElements()) {
			Canvas canvas = (Canvas) enumeration.nextElement();
			method1217(26104, aHashtable3925.get(canvas), canvas);
		}
		if (i != 697)
			method1206(null, -89);
		anInterface15_Impl2_4054.method31((byte) 82);
		anInterface15_Impl2_4055.method31((byte) 82);
		anInterface15_Impl2_4052.method31((byte) 82);
		for (int i_232_ = 0; i_232_ < 7; i_232_++)
			aClass178_Sub3Array4058[i_232_].method1795(i - 705);
		aClass152_4045.method1545((byte) 77);
		anInterface15_Impl1_4050.method31((byte) 82);
	}

	public aa a(int i, int i_233_, int[] is, int[] is_234_) {
		return Class373_Sub2.method3936(is_234_, i_233_, is, i, 118, this);
	}

	abstract boolean method1200(Class67 class67, Class202 class202, byte i);

	public void K(int[] is) {
		is[3] = anInt4040;
		is[0] = anInt3987;
		is[2] = anInt3982;
		is[1] = anInt4039;
	}

	public void method1201(int i, boolean bool) {
		if (aBoolean3963 != bool) {
			aBoolean3963 = bool;
			method1148(i ^ ~0x6361);
			anInt4020 &= ~0x1f;
		}
		if (i != -25357) {
			/* empty */
		}
	}

	public void method1202(int i) {
		aBoolean3940 = false;
		if (i != 0)
			aClass127_4053 = null;
		method1163(i + 17830);
	}

	public int method1203(byte i) {
		if (i != -60)
			aFloatArray3998 = null;
		return anInt4008;
	}

	public boolean r() {
		return false;
	}

	public void HA(int i, int i_235_, int i_236_, int i_237_, int[] is) {
		float f = aClass373_Sub2_3941.method3926((float) i_235_, (byte) 53, (float) i, (float) i_236_);
		if (f < (float) anInt4035 || f > (float) anInt3981)
			is[0] = is[1] = is[2] = -1;
		else {
			int i_238_ = (int) ((float) anInt3992 * aClass373_Sub2_3941.method3948((float) i, (float) i_235_, 12, (float) i_236_) / (float) i_237_);
			int i_239_ = (int) ((float) anInt3993 * aClass373_Sub2_3941.method3941((float) i, (byte) -125, (float) i_236_, (float) i_235_) / (float) i_237_);
			is[1] = (int) (-aFloat3976 + (float) i_239_);
			is[2] = (int) f;
			is[0] = (int) (-aFloat4018 + (float) i_238_);
		}
	}

	abstract void method1204(boolean bool);

	abstract Interface15_Impl2 method1205(boolean bool, int i);

	public boolean p() {
		return false;
	}

	static public String method1206(String string, int i) {
		String string_240_ = Class296_Sub43.method2922((byte) 72, Class334.method3423((byte) -102, string));
		if (string_240_ == null)
			string_240_ = "";
		int i_241_ = 21 / ((i - 12) / 47);
		return string_240_;
	}

	public void T(int i, int i_242_, int i_243_, int i_244_) {
		boolean bool = false;
		if (i > anInt3987) {
			anInt3987 = i;
			bool = true;
		}
		if (i_243_ < anInt3982) {
			anInt3982 = i_243_;
			bool = true;
		}
		if (anInt4039 < i_242_) {
			bool = true;
			anInt4039 = i_242_;
		}
		if (anInt4040 > i_244_) {
			anInt4040 = i_244_;
			bool = true;
		}
		if (bool) {
			if (!aBoolean4062) {
				aBoolean4062 = true;
				method1149(25154);
			}
			method1115(13);
			method1191(-61);
		}
	}

	private void method1207(byte i) {
		if (i != 24)
			aFloat3989 = -1.8283483F;
		aFloat4014 = (float) anInt3981;
	}

	public int E() {
		return anInt3936 + (anInt3938 + anInt3937);
	}

	public void DA(int i, int i_245_, int i_246_, int i_247_) {
		anInt3992 = i_246_;
		anInt3988 = i_245_;
		anInt3993 = i_247_;
		anInt3970 = i;
		method1155(-29995);
		method1141(true);
		method1105((byte) -41);
		method1191(-47);
	}

	abstract void method1208(Object object, Canvas canvas, byte i);

	abstract void method1209(Class127 class127, byte i);

	public int i() {
		return anInt4035;
	}

	abstract void method1210(int i);

	abstract void method1211(byte i);

	private boolean method1212(float f, float f_248_, float f_249_, float f_250_, float f_251_, boolean bool, float f_252_) {
		Buffer buffer = anInterface15_Impl2_4055.method47(bool, -8102);
		if (buffer == null)
			return false;
		Stream stream = method1156(buffer, -128);
		if (Stream.a()) {
			stream.a(f_250_);
			stream.a(f_248_);
			stream.a(f_249_);
			stream.a(f_252_);
			stream.a(f);
			stream.a(f_251_);
		} else {
			stream.b(f_250_);
			stream.b(f_248_);
			stream.b(f_249_);
			stream.b(f_252_);
			stream.b(f);
			stream.b(f_251_);
		}
		stream.b();
		return anInterface15_Impl2_4055.method49(2968);
	}

	public void c(int i) {
		/* empty */
	}

	public void pa() {
		aBoolean4038 = false;
	}

	public void method1213(byte i) {
		Hashtable hashtable = new Hashtable();
		if (aHashtable3925 != null && !aHashtable3925.isEmpty()) {
			Enumeration enumeration = aHashtable3925.keys();
			while (enumeration.hasMoreElements()) {
				Canvas canvas = (Canvas) enumeration.nextElement();
				hashtable.put(canvas, method1100(110, canvas));
			}
		}
		if (i == 74) {
			aHashtable3925 = hashtable;
			method1164(i - 74);
			method1132(i ^ 0x6b81);
			method1143((byte) 8);
			aClass152_4045.method1543(i ^ 0x4a, this);
		}
	}

	public za b(int i) {
		za_Sub1 var_za_Sub1 = new za_Sub1(i);
		aClass155_3933.addLast((byte) -57, var_za_Sub1);
		return var_za_Sub1;
	}

	public boolean x() {
		return true;
	}

	public void method1214(boolean bool, int i) {
		int i_253_ = -52 / ((72 - i) / 34);
		if (!aBoolean3952 == bool) {
			aBoolean3952 = bool;
			method1148(60);
		}
	}

	public void a(za var_za) {
		aNativeHeap3932 = ((za_Sub1) var_za).aNativeHeap6553;
		aNativeHeapBuffer3931 = aNativeHeap3932.a(32768, false);
	}

	public void b(Canvas canvas) {
		aCanvas3930 = null;
		anObject3924 = null;
		if (canvas != null && canvas != aCanvas3926) {
			if (aHashtable3925.containsKey(canvas)) {
				anObject3924 = aHashtable3925.get(canvas);
				aCanvas3930 = canvas;
			}
		} else {
			aCanvas3930 = aCanvas3926;
			anObject3924 = anObject3927;
		}
		if (aCanvas3930 == null || anObject3924 == null)
			throw new RuntimeException();
		method1190(anObject3924, (byte) -65, aCanvas3930);
		method1172(false);
	}

	public Class373_Sub2 method1215(int i) {
		int i_254_ = 26 / ((i - 26) / 36);
		return aClass373_Sub2_3939;
	}

	abstract void method1216(int i);

	public void e(int i, int i_255_, int i_256_, int i_257_, int i_258_, int i_259_) {
		float f = (float) i_256_ - (float) i;
		float f_260_ = (float) i_257_ - (float) i_255_;
		if (f == 0.0F && f_260_ == 0.0F)
			f = 1.0F;
		else {
			float f_261_ = (float) (1.0 / Math.sqrt((double) (f * f + f_260_ * f_260_)));
			f_260_ *= f_261_;
			f *= f_261_;
		}
		if (method1212((float) i_257_ + f_260_, (float) i_255_, 0.0F, (float) i, 0.0F, true, (float) i_256_ + f)) {
			method1196((byte) -80);
			method1139(0, i_258_);
			method1158((byte) -107, 0, Class151.aClass287_1553);
			method1219((byte) 54, Class151.aClass287_1553, 0);
			method1161(31156, i_259_);
			method1102(-123);
			method1101(false, (byte) 108);
			method1126((byte) -55);
			method1101(true, (byte) 127);
			method1219((byte) 54, Class199.aClass287_2007, 0);
			method1158((byte) -100, 0, Class199.aClass287_2007);
		}
	}

	public void g(int i) {
		if (aClass395_4002 != null)
			aClass395_4002.method4070((byte) -128);
		anInt4006 = i & 0x7fffffff;
	}

	public Model a(Mesh class132, int i, int i_262_, int i_263_, int i_264_) {
		return new Class178_Sub3(this, class132, i, i_263_, i_264_, i_262_);
	}

	public Sprite a(Class186 class186, boolean bool) {
		Sprite class397;
		if (class186.anInt1899 != 0 && class186.anInt1904 != 0) {
			int[] is = new int[class186.anInt1904 * class186.anInt1899];
			int i = 0;
			int i_265_ = 0;
			if (class186.aByteArray1905 == null) {
				for (int i_266_ = 0; class186.anInt1904 > i_266_; i_266_++) {
					for (int i_267_ = 0; i_267_ < class186.anInt1899; i_267_++) {
						int i_268_ = (class186.anIntArray1900[class186.aByteArray1901[i++] & 0xff]);
						is[i_265_++] = i_268_ == 0 ? 0 : Class48.bitOR(-16777216, i_268_);
					}
				}
			} else {
				for (int i_269_ = 0; class186.anInt1904 > i_269_; i_269_++) {
					for (int i_270_ = 0; class186.anInt1899 > i_270_; i_270_++) {
						is[i_265_++] = (Class48.bitOR((class186.anIntArray1900[(255 & (class186.aByteArray1901[i]))]), class186.aByteArray1905[i] << 24));
						i++;
					}
				}
			}
			class397 = this.method1096(124, 0, class186.anInt1904, class186.anInt1899, class186.anInt1899, is);
		} else
			class397 = this.method1096(116, 0, 1, 1, 1, new int[1]);
		class397.method4097(class186.anInt1903, class186.anInt1906, class186.anInt1907, class186.anInt1902);
		return class397;
	}

	public void a(int[] is) {
		is[1] = anInt3920;
		is[0] = anInt3921;
	}

	abstract void method1217(int i, Object object, Canvas canvas);

	abstract void method1218(int i, int i_271_);

	public void KA(int i, int i_272_, int i_273_, int i_274_) {
		if (i <= 0 && anInt3921 - 1 <= i_273_ && i_272_ <= 0 && i_274_ >= anInt3920 - 1)
			la();
		else {
			anInt3987 = i >= 0 ? i : 0;
			anInt3982 = anInt3921 < i_273_ ? 0 : i_273_;
			anInt4039 = i_272_ >= 0 ? i_272_ : 0;
			anInt4040 = i_274_ > anInt3921 ? 0 : i_274_;
			if (!aBoolean4062) {
				aBoolean4062 = true;
				method1149(25154);
			}
			method1115(13);
			method1191(-113);
		}
	}

	public void method1219(byte i, Class287 class287, int i_275_) {
		if (i == 54)
			method1171((byte) -82, false, i_275_, class287);
	}

	public int method1220(int i) {
		if (i <= 116)
			anInt4027 = -51;
		return anInt4013;
	}

	public void method1221(boolean bool, int i) {
		if (i < -11) {
			if (!aBoolean4041 == bool) {
				aBoolean4041 = bool;
				method1173(-3394);
			}
		}
	}

	public void a(Class390 class390) {
		aClass152_4045.method1547(class390, this, -1, (byte) 123);
	}

	private void method1222(byte i) {
		if (!aBoolean4004) {
			float[] fs = aFloatArray3998;
			if (anInt3921 != 0 && anInt3920 != 0) {
				fs[11] = 0.0F;
				fs[15] = 1.0F;
				fs[1] = 0.0F;
				fs[6] = 0.0F;
				fs[14] = 0.5F;
				fs[4] = 0.0F;
				fs[7] = 0.0F;
				fs[12] = -1.0F;
				fs[3] = 0.0F;
				fs[8] = 0.0F;
				fs[0] = 2.0F / (float) anInt3921;
				fs[9] = 0.0F;
				fs[2] = 0.0F;
				fs[10] = 0.5F;
				fs[13] = 1.0F;
				fs[5] = -2.0F / (float) anInt3920;
			} else {
				fs[0] = 1.0F;
				fs[9] = 0.0F;
				fs[8] = 0.0F;
				fs[12] = 0.0F;
				fs[6] = 0.0F;
				fs[5] = 1.0F;
				fs[4] = 0.0F;
				fs[13] = 0.0F;
				fs[14] = 0.0F;
				fs[3] = 0.0F;
				fs[2] = 0.0F;
				fs[7] = 0.0F;
				fs[10] = 1.0F;
				fs[15] = 1.0F;
				fs[1] = 0.0F;
				fs[11] = 0.0F;
			}
			aBoolean4004 = true;
		}
		if (i != -106)
			aFloat4044 = 0.5568158F;
	}

	public Interface15_Impl1 method1223(int i, byte i_276_) {
		if (i_276_ > -79)
			return null;
		if (anInterface15_Impl1_4050.method61((byte) 125) < i * 2)
			anInterface15_Impl1_4050.method32(i, -21709);
		return anInterface15_Impl1_4050;
	}

	abstract void method1224(boolean bool);

	public void e() {
		if (aClass395_4002 != null)
			aClass395_4002.method4065((byte) 111);
	}

	public void b(boolean bool) {
		/* empty */
	}

	public void xa(float f) {
		if (f != aFloat4010) {
			aFloat4010 = f;
			aNativeInterface3923.setAmbient(f);
			method1112(false);
			method1162(false);
		}
	}

	public boolean v() {
		return true;
	}

	void method1225(int i) {
		anInt3983 = anInt4046;
		int i_277_ = 68 / ((33 - i) / 61);
		anInt4046 = 0;
	}

	public void a(int i, Class296_Sub35[] class296_sub35s) {
		for (int i_278_ = 0; i_278_ < i; i_278_++)
			aClass296_Sub35Array4005[i_278_] = class296_sub35s[i_278_];
		anInt4046 = i;
		if (aClass108_3954.method947(27229))
			method1225(-73);
	}

	public Class373_Sub2 method1226(int i) {
		if (i != -23501)
			method1194((byte) 25);
		return aClass373_Sub2Array3965[anInt3991];
	}

	abstract void method1227(int i);

	abstract void method1228(int i);

	public void method1229(int i) {
		if (i != (anInt4020 ^ 0xffffffff)) {
			method1176(-25);
			method1201(i - 25354, false);
			method1137(4, false);
			method1110(false, i ^ 0x56);
			method1106(112, false);
			method1133(false, -2, i + 3, false);
			anInt4020 = 2;
		}
	}

	public void C(boolean bool) {
		aBoolean3946 = bool;
		method1186(0);
	}

	public Interface6_Impl1 method1230(float[] fs, Class202 class202, boolean bool, int i, int i_279_, int i_280_) {
		if (i != 0)
			method1121((byte) 116);
		return method1160(class202, i_280_, bool, 0, fs, i_279_, 0, 28426);
	}

	public void i(int i) {
		/* empty */
	}

	private void method1231(boolean bool) {
		aBoolean3961 = bool;
		if (aClass360_4016 != null)
			aClass360_4016.method3720(8250);
		method1210(-10667);
	}

	abstract void method1232(Class289 class289, Interface15_Impl1 interface15_impl1, int i, int i_281_, int i_282_, int i_283_, int i_284_);

	abstract Class127 method1233(byte i, Class211[] class211s);
}
