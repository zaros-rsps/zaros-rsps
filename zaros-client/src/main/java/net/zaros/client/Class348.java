package net.zaros.client;

/* Class348 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class348 {
	int anInt3029;
	int anInt3030;
	int anInt3031;
	int anInt3032;
	static boolean aBoolean3033 = true;
	public static int anInt3034;

	static final void method3672(Class324 class324, int i, ha var_ha, int i_0_) {
		if (i != 0)
			anInt3034 = 100;
		if (Class96.anIntArray1049 != null && class324.aByte2857 >= i_0_) {
			for (int i_1_ = 0; Class96.anIntArray1049.length > i_1_; i_1_++) {
				if (Class96.anIntArray1049[i_1_] != -1000000 && ((class324.anIntArray2863[0] <= Class96.anIntArray1049[i_1_]) || (class324.anIntArray2863[1] <= Class96.anIntArray1049[i_1_]) || (Class96.anIntArray1049[i_1_] >= class324.anIntArray2863[2]) || (class324.anIntArray2863[3] <= Class96.anIntArray1049[i_1_])) && ((class324.anIntArray2854[0] <= Class262.anIntArray2447[i_1_]) || (Class262.anIntArray2447[i_1_] >= class324.anIntArray2854[1]) || (class324.anIntArray2854[2] <= Class262.anIntArray2447[i_1_]) || (Class262.anIntArray2447[i_1_] >= class324.anIntArray2854[3])) && ((TextureOperation.anIntArray5039[i_1_] <= class324.anIntArray2854[0]) || (class324.anIntArray2854[1] >= TextureOperation.anIntArray5039[i_1_]) || (TextureOperation.anIntArray5039[i_1_] <= class324.anIntArray2854[2]) || (TextureOperation.anIntArray5039[i_1_] <= class324.anIntArray2854[3])) && ((Class368_Sub23.anIntArray5575[i_1_] >= class324.anIntArray2849[0]) || (class324.anIntArray2849[1] <= Class368_Sub23.anIntArray5575[i_1_]) || (class324.anIntArray2849[2] <= Class368_Sub23.anIntArray5575[i_1_]) || (Class368_Sub23.anIntArray5575[i_1_] >= class324.anIntArray2849[3])) && ((class324.anIntArray2849[0] >= Class360_Sub6.anIntArray5329[i_1_]) || (Class360_Sub6.anIntArray5329[i_1_] <= class324.anIntArray2849[1]) || (Class360_Sub6.anIntArray5329[i_1_] <= class324.anIntArray2849[2]) || (class324.anIntArray2849[3] >= Class360_Sub6.anIntArray5329[i_1_])))
					return;
			}
		}
		if (class324.aByte2852 == 1) {
			int i_2_ = (-Class296_Sub45_Sub2.anInt6288 + (class324.aShort2855 + Class379_Sub2.anInt5684));
			if (i_2_ >= 0 && Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 >= i_2_) {
				int i_3_ = (Class379_Sub2.anInt5684 + (class324.aShort2853 - Class296_Sub39_Sub20_Sub2.anInt6728));
				if (i_3_ < 0)
					i_3_ = 0;
				else if (i_3_ > Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684)
					return;
				int i_4_ = (class324.aShort2864 + (-Class296_Sub39_Sub20_Sub2.anInt6728 + Class379_Sub2.anInt5684));
				if (Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 < i_4_)
					i_4_ = Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684;
				else if (i_4_ < 0)
					return;
				boolean bool = false;
				while (i_3_ <= i_4_) {
					if (Class296_Sub15_Sub2.aBooleanArrayArray6006[i_2_][i_3_++]) {
						bool = true;
						break;
					}
				}
				if (bool) {
					float f = (float) (-class324.anIntArray2854[0] + Class395.anInt3315);
					if (f < 0.0F)
						f *= -1.0F;
					if (!((float) Class57.anInt668 > f) && Class12.method221(class324, 24790, 0) && Class12.method221(class324, i + 24790, 1) && Class12.method221(class324, 24790, 2) && Class12.method221(class324, 24790, 3))
						Class29.aClass324Array305[Class360_Sub9.anInt5343++] = class324;
				}
			}
		} else if (class324.aByte2852 == 2) {
			int i_5_ = (Class379_Sub2.anInt5684 + (class324.aShort2853 - Class296_Sub39_Sub20_Sub2.anInt6728));
			if (i_5_ >= 0 && Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 >= i_5_) {
				int i_6_ = (Class379_Sub2.anInt5684 + class324.aShort2855 - Class296_Sub45_Sub2.anInt6288);
				if (i_6_ >= 0) {
					if (i_6_ > Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684)
						return;
				} else
					i_6_ = 0;
				int i_7_ = Class379_Sub2.anInt5684 + (-Class296_Sub45_Sub2.anInt6288 + class324.aShort2850);
				if (i_7_ > Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684)
					i_7_ = Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684;
				else if (i_7_ < 0)
					return;
				boolean bool = false;
				while (i_6_ <= i_7_) {
					if (Class296_Sub15_Sub2.aBooleanArrayArray6006[i_6_++][i_5_]) {
						bool = true;
						break;
					}
				}
				if (bool) {
					float f = (float) (StaticMethods.anInt5942 - class324.anIntArray2849[0]);
					if (f < 0.0F)
						f *= -1.0F;
					if (!((float) Class57.anInt668 > f) && Class12.method221(class324, 24790, 0) && Class12.method221(class324, 24790, 1) && Class12.method221(class324, i + 24790, 2) && Class12.method221(class324, i ^ 0x60d6, 3))
						Class29.aClass324Array305[Class360_Sub9.anInt5343++] = class324;
				}
			}
		} else if (class324.aByte2852 == 16 || class324.aByte2852 == 8) {
			int i_8_ = class324.aShort2855 + (-Class296_Sub45_Sub2.anInt6288 + Class379_Sub2.anInt5684);
			if (i_8_ >= 0 && Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 >= i_8_) {
				int i_9_ = (class324.aShort2853 - Class296_Sub39_Sub20_Sub2.anInt6728 + Class379_Sub2.anInt5684);
				if (i_9_ >= 0 && (i_9_ <= Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684) && (Class296_Sub15_Sub2.aBooleanArrayArray6006[i_8_][i_9_])) {
					float f = (float) (-class324.anIntArray2854[0] + Class395.anInt3315);
					if (f < 0.0F)
						f *= -1.0F;
					float f_10_ = (float) (StaticMethods.anInt5942 - class324.anIntArray2849[0]);
					if (f_10_ < 0.0F)
						f_10_ *= -1.0F;
					if ((!((float) Class57.anInt668 > f) || !(f_10_ < (float) Class57.anInt668)) && Class12.method221(class324, i ^ 0x60d6, 0) && Class12.method221(class324, i ^ 0x60d6, 1) && Class12.method221(class324, 24790, 2) && Class12.method221(class324, 24790, 3))
						Class29.aClass324Array305[Class360_Sub9.anInt5343++] = class324;
				}
			}
		} else if (class324.aByte2852 == 4) {
			float f = (float) (-Class41_Sub19.anInt3791 + class324.anIntArray2863[0]);
			if (!((float) EffectiveVertex.anInt2213 >= f)) {
				int i_11_ = (Class379_Sub2.anInt5684 + (class324.aShort2853 - Class296_Sub39_Sub20_Sub2.anInt6728));
				if (i_11_ >= 0) {
					if (i_11_ > Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684)
						return;
				} else
					i_11_ = 0;
				int i_12_ = (Class379_Sub2.anInt5684 + class324.aShort2864 - Class296_Sub39_Sub20_Sub2.anInt6728);
				if (Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 >= i_12_) {
					if (i_12_ < 0)
						return;
				} else
					i_12_ = Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684;
				int i_13_ = (Class379_Sub2.anInt5684 + class324.aShort2855 - Class296_Sub45_Sub2.anInt6288);
				if (i_13_ >= 0) {
					if (Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 < i_13_)
						return;
				} else
					i_13_ = 0;
				int i_14_ = Class379_Sub2.anInt5684 + (-Class296_Sub45_Sub2.anInt6288 + class324.aShort2850);
				if (i_14_ <= Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684) {
					if (i_14_ < 0)
						return;
				} else
					i_14_ = Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684;
				boolean bool = false;
				while_258_ : for (int i_15_ = i_13_; i_14_ >= i_15_; i_15_++) {
					for (int i_16_ = i_11_; i_12_ >= i_16_; i_16_++) {
						if (Class296_Sub15_Sub2.aBooleanArrayArray6006[i_15_][i_16_]) {
							bool = true;
							break while_258_;
						}
					}
				}
				if (bool && Class12.method221(class324, 24790, 0) && Class12.method221(class324, 24790, 1) && Class12.method221(class324, 24790, 2) && Class12.method221(class324, 24790, 3))
					Class29.aClass324Array305[Class360_Sub9.anInt5343++] = class324;
			}
		}
	}

	static final int method3673(int i, int i_17_, byte i_18_, int i_19_, int i_20_) {
		int i_21_ = i_17_ & 0xf;
		int i_22_ = i_21_ >= 8 ? i : i_19_;
		if (i_18_ >= -12)
			aBoolean3033 = true;
		int i_23_ = i_21_ >= 4 ? i_21_ != 12 && i_21_ != 14 ? i_20_ : i_19_ : i;
		return (((i_21_ & 0x1) == 0 ? i_22_ : -i_22_) + ((i_21_ & 0x2) == 0 ? i_23_ : -i_23_));
	}

	public Class348() {
		/* empty */
	}

	static final void method3674(Js5 class138, int i) {
		Class41_Sub25.anInt3804 = class138.getFileIndex("hitbar_default");
		Class368_Sub5.anInt5446 = class138.getFileIndex("timerbar_default");
		Class379_Sub1.anInt5672 = class138.getFileIndex("headicons_pk");
		int i_24_ = 86 % ((i + 6) / 36);
		Class16.anInt183 = class138.getFileIndex("headicons_prayer");
		Class116.anInt3676 = class138.getFileIndex("hint_headicons");
		Class296_Sub11.anInt4650 = class138.getFileIndex("hint_mapmarkers");
		Class307.anInt2749 = class138.getFileIndex("mapflag");
		Class206.anInt2067 = class138.getFileIndex("cross");
		Class389.anInt3278 = class138.getFileIndex("mapdots");
		Class366_Sub2.anInt5370 = class138.getFileIndex("scrollbar");
		Class284.anInt2619 = class138.getFileIndex("name_icons");
		Class292.anInt2659 = class138.getFileIndex("floorshadows");
		Class296_Sub39_Sub15_Sub1.anInt6718 = class138.getFileIndex("compass");
		Class205_Sub2.anInt5630 = class138.getFileIndex("otherlevel");
		ISAACCipher.anInt1897 = class138.getFileIndex("hint_mapedge");
	}
}
