package net.zaros.client;

/* Class125 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class125 {
	static Class202 aClass202_1285;
	static short aShort1286 = 32767;
	static float aFloat1287;
	static float[][] aFloatArrayArray1288;

	static final boolean method1079(Class338_Sub3_Sub1 class338_sub3_sub1, boolean bool) {
		boolean bool_0_ = Class360_Sub2.aSArray5304 == Class52.aSArray636;
		int i = 0;
		short i_1_ = 0;
		byte i_2_ = 0;
		class338_sub3_sub1.method3477(-1);
		if (class338_sub3_sub1.aShort6560 < 0 || class338_sub3_sub1.aShort6564 < 0 || class338_sub3_sub1.aShort6559 >= Class228.anInt2201 || class338_sub3_sub1.aShort6558 >= Class368_Sub12.anInt5488)
			return false;
		short i_3_ = 0;
		for (int i_4_ = class338_sub3_sub1.aShort6560; i_4_ <= class338_sub3_sub1.aShort6559; i_4_++) {
			for (int i_5_ = class338_sub3_sub1.aShort6564; i_5_ <= class338_sub3_sub1.aShort6558; i_5_++) {
				Class247 class247 = Node.method2428(class338_sub3_sub1.z, i_4_, i_5_);
				if (class247 != null) {
					Class234 class234 = Class276.method2322((byte) -106, class338_sub3_sub1);
					Class234 class234_6_ = class247.aClass234_2341;
					if (class234_6_ == null)
						class247.aClass234_2341 = class234;
					else {
						for (/**/; class234_6_.aClass234_2224 != null; class234_6_ = class234_6_.aClass234_2224) {
							/* empty */
						}
						class234_6_.aClass234_2224 = class234;
					}
					if (bool_0_ && ((Class296_Sub51_Sub32.anIntArrayArray6514[i_4_][i_5_]) & ~0xffffff) != 0) {
						i = (Class296_Sub51_Sub32.anIntArrayArray6514[i_4_][i_5_]);
						i_1_ = (StaticMethods.aShortArrayArray5919[i_4_][i_5_]);
						i_2_ = (Mobile.aByteArrayArray6776[i_4_][i_5_]);
					}
					if (!bool && class247.aClass338_Sub3_Sub5_2346 != null && class247.aClass338_Sub3_Sub5_2346.aShort6573 > i_3_)
						i_3_ = class247.aClass338_Sub3_Sub5_2346.aShort6573;
				}
			}
		}
		if (bool_0_ && (i & ~0xffffff) != 0) {
			for (int i_7_ = class338_sub3_sub1.aShort6560; i_7_ <= class338_sub3_sub1.aShort6559; i_7_++) {
				for (int i_8_ = class338_sub3_sub1.aShort6564; i_8_ <= class338_sub3_sub1.aShort6558; i_8_++) {
					if ((Class296_Sub51_Sub32.anIntArrayArray6514[i_7_][i_8_] & ~0xffffff) == 0) {
						Class296_Sub51_Sub32.anIntArrayArray6514[i_7_][i_8_] = i;
						StaticMethods.aShortArrayArray5919[i_7_][i_8_] = i_1_;
						Mobile.aByteArrayArray6776[i_7_][i_8_] = i_2_;
					}
				}
			}
		}
		if (bool)
			s.aClass338_Sub3_Sub1Array2833[Class127.anInt1303++] = class338_sub3_sub1;
		else {
			int i_9_ = Class360_Sub2.aSArray5304 == Class52.aSArray636 ? 1 : 0;
			if (class338_sub3_sub1.method3459(0)) {
				if (class338_sub3_sub1.method3469(127)) {
					class338_sub3_sub1.aClass338_Sub3_5202 = Class368_Sub16.aClass338_Sub3Array5525[i_9_];
					Class368_Sub16.aClass338_Sub3Array5525[i_9_] = class338_sub3_sub1;
				} else {
					class338_sub3_sub1.aClass338_Sub3_5202 = Class368_Sub10.aClass338_Sub3Array5481[i_9_];
					Class368_Sub10.aClass338_Sub3Array5481[i_9_] = class338_sub3_sub1;
					Class41.aBoolean388 = true;
				}
			} else {
				class338_sub3_sub1.aClass338_Sub3_5202 = Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_9_];
				Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_9_] = class338_sub3_sub1;
			}
		}
		if (bool)
			class338_sub3_sub1.anInt5213 -= i_3_;
		return true;
	}

	static final boolean method1080(byte i, int i_10_) {
		int i_11_ = 2 / ((i - 18) / 57);
		if (i_10_ != 1 && i_10_ != 3 && i_10_ != 5)
			return false;
		return true;
	}

	public Class125() {
		/* empty */
	}

	public static void method1081(byte i) {
		aClass202_1285 = null;
		aFloatArrayArray1288 = null;
		if (i != -104)
			aClass202_1285 = null;
	}

	static {
		aClass202_1285 = new Class202(2);
		aFloatArrayArray1288 = new float[][]{{-0.333333F, -0.333333F, -0.333333F}, {0.333333F, -0.333333F, -0.333333F}, {-0.333333F, 0.333333F, -0.333333F}, {0.333333F, 0.333333F, -0.333333F}, {-0.333333F, -0.333333F, 0.333333F}, {0.333333F, -0.333333F, 0.333333F}, {-0.333333F, 0.333333F, 0.333333F}, {0.333333F, 0.333333F, 0.333333F}};
	}
}
