package net.zaros.client;

/* Class296_Sub45_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub45_Sub4 extends Class296_Sub45 {
	private int[] anIntArray6296;
	private int[] anIntArray6297;
	private int[] anIntArray6298;
	private int[] anIntArray6299;
	private int[] anIntArray6300;
	private int[] anIntArray6301;
	static Class161 aClass161_6302 = new Class161(12, 0, 1, 0);
	private HashTable aClass263_6303;
	int[] anIntArray6304;
	private int[] anIntArray6305;
	private Class296_Sub4[][] aClass296_Sub4ArrayArray6306;
	private int[] anIntArray6307;
	int[] anIntArray6308;
	private int[] anIntArray6309;
	private int[] anIntArray6310;
	private MidiEvent aClass109_6311;
	private int[] anIntArray6312;
	private int[] anIntArray6313;
	private int anInt6314;
	int[] anIntArray6315;
	static int anInt6316 = 0;
	static int anInt6317;
	private int[] anIntArray6318;
	static boolean aBoolean6319 = false;
	private int anInt6320;
	private Class296_Sub4[][] aClass296_Sub4ArrayArray6321;
	private boolean aBoolean6322;
	private Class296_Sub45_Sub3 aClass296_Sub45_Sub3_6323;
	private long aLong6324;
	private long aLong6325;
	private int anInt6326;
	private int anInt6327;
	private MidiDecoder aClass296_Sub47_6328;
	private boolean aBoolean6329;
	private int anInt6330;

	private final void method2992(int i, int i_0_, int i_1_) {
		anIntArray6307[i_0_] = i_1_;
		anIntArray6305[i_0_] = i_1_ & i;
		method2995(i_0_, i_1_, (byte) 112);
	}

	private final void method2993(int i) {
		int i_2_ = anInt6327;
		int i_3_ = anInt6326;
		long l = aLong6324;
		if (aClass296_Sub47_6328 != null && i_3_ == anInt6330) {
			method3028(aBoolean6322, aBoolean6329, true, aClass296_Sub47_6328);
			method2993(i);
		} else {
			while (anInt6326 == i_3_) {
				while (i_3_ == aClass109_6311.tick_durations[i_2_]) {
					aClass109_6311.loadTrackBuffer(i_2_);
					int i_4_ = aClass109_6311.method965(i_2_);
					if (i_4_ == 1) {
						aClass109_6311.discardTrackBuffer();
						aClass109_6311.saveTrackBuffer(i_2_);
						if (aClass109_6311.isFinished()) {
							if (aClass296_Sub47_6328 != null) {
								method3014(aClass296_Sub47_6328, aBoolean6322, true);
								method2993(1758790920);
								return;
							}
							if (aBoolean6322 && i_3_ != 0)
								aClass109_6311.method960(l);
							else {
								method3010(true, 13224);
								aClass109_6311.method964();
								return;
							}
						}
						break;
					}
					if ((i_4_ & 0x80) != 0)
						method3003(i_4_, (byte) 11);
					aClass109_6311.readTickDuration(i_2_);
					aClass109_6311.saveTrackBuffer(i_2_);
				}
				i_2_ = aClass109_6311.getFastestTrack();
				i_3_ = aClass109_6311.tick_durations[i_2_];
				l = aClass109_6311.method962(i_3_);
			}
			if (i != 1758790920)
				method2992(-125, 26, 63);
			anInt6326 = i_3_;
			aLong6324 = l;
			anInt6327 = i_2_;
			if (aClass296_Sub47_6328 != null && anInt6330 < i_3_) {
				anInt6327 = -1;
				anInt6326 = anInt6330;
				aLong6324 = aClass109_6311.method962(anInt6326);
			}
		}
	}

	private final void method2994(int i, byte i_5_) {
		if (i_5_ > 99) {
			for (Class296_Sub4 class296_sub4 = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.removeFirst((byte) 127); class296_sub4 != null; class296_sub4 = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.removeNext(1001)) {
				if ((i < 0 || i == class296_sub4.anInt4597) && class296_sub4.anInt4617 < 0) {
					aClass296_Sub4ArrayArray6306[class296_sub4.anInt4597][class296_sub4.anInt4608] = null;
					class296_sub4.anInt4617 = 0;
				}
			}
		}
	}

	private final void method2995(int i, int i_6_, byte i_7_) {
		if (i_6_ != anIntArray6299[i]) {
			anIntArray6299[i] = i_6_;
			for (int i_8_ = 0; i_8_ < 128; i_8_++)
				aClass296_Sub4ArrayArray6321[i][i_8_] = null;
		}
		if (i_7_ < 46)
			aClass296_Sub4ArrayArray6321 = null;
	}

	private final void method2996(int i, long l) {
		long l_9_;
		for (/**/; l >= aLong6324; aLong6324 = l_9_) {
			int i_10_ = anInt6327;
			int i_11_ = anInt6326;
			l_9_ = aLong6324;
			while (anInt6326 == i_11_) {
				while (i_11_ == aClass109_6311.tick_durations[i_10_]) {
					aClass109_6311.loadTrackBuffer(i_10_);
					int i_12_ = aClass109_6311.method965(i_10_);
					if (i_12_ == 1) {
						aClass109_6311.discardTrackBuffer();
						aClass109_6311.saveTrackBuffer(i_10_);
						if (aClass109_6311.isFinished()) {
							if (aBoolean6322 && i_11_ != 0)
								aClass109_6311.method960(l_9_);
							else {
								method3010(true, 13224);
								aClass109_6311.method964();
								return;
							}
						}
						break;
					}
					if ((i_12_ & 0x80) != 0 && (i_12_ & 0xf0) != 144)
						method3003(i_12_, (byte) 11);
					aClass109_6311.readTickDuration(i_10_);
					aClass109_6311.saveTrackBuffer(i_10_);
				}
				aLong6325 = l_9_;
				i_10_ = aClass109_6311.getFastestTrack();
				i_11_ = aClass109_6311.tick_durations[i_10_];
				l_9_ = aClass109_6311.method962(i_11_);
			}
			anInt6326 = i_11_;
			anInt6327 = i_10_;
		}
		if (i != 98)
			method2933(-96);
	}

	private final void method2997(int i, boolean bool, int i_13_) {
		if (bool != true)
			aBoolean6319 = false;
	}

	final synchronized void method2998(int i, int i_14_, int i_15_) {
		if (i != -1444068849)
			method3030(-70, false, -25);
		method2992(i ^ 0x5612bd8f, i_14_, i_15_);
	}

	private final void method2999(int i, int i_16_, int i_17_, byte i_18_) {
		method3002((byte) -32, i_16_, i_17_, 64);
		if ((anIntArray6304[i_17_] & 0x2) != 0) {
			for (Class296_Sub4 class296_sub4 = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.method1569(117); class296_sub4 != null; class296_sub4 = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.method1576(i_18_ + 1725258304)) {
				if (class296_sub4.anInt4597 == i_17_ && class296_sub4.anInt4617 < 0) {
					aClass296_Sub4ArrayArray6306[i_17_][(class296_sub4.anInt4608)] = null;
					aClass296_Sub4ArrayArray6306[i_17_][i_16_] = class296_sub4;
					int i_19_ = ((class296_sub4.anInt4606 * class296_sub4.anInt4604 >> 12) + class296_sub4.anInt4603);
					class296_sub4.anInt4603 += i_16_ - class296_sub4.anInt4608 << 8;
					class296_sub4.anInt4604 = i_19_ - class296_sub4.anInt4603;
					class296_sub4.anInt4606 = 4096;
					class296_sub4.anInt4608 = i_16_;
					return;
				}
			}
		}
		Class296_Sub41 class296_sub41 = ((Class296_Sub41) aClass263_6303.get((long) anIntArray6299[i_17_]));
		if (i_18_ != -31)
			method3011(86, 110);
		if (class296_sub41 != null) {
			Class296_Sub19_Sub1 class296_sub19_sub1 = class296_sub41.aClass296_Sub19_Sub1Array4917[i_16_];
			if (class296_sub19_sub1 != null) {
				Class296_Sub4 class296_sub4 = new Class296_Sub4();
				class296_sub4.aClass296_Sub41_4610 = class296_sub41;
				class296_sub4.anInt4597 = i_17_;
				class296_sub4.aClass296_Sub19_Sub1_4605 = class296_sub19_sub1;
				class296_sub4.aClass242_4599 = class296_sub41.aClass242Array4919[i_16_];
				class296_sub4.anInt4613 = class296_sub41.aByteArray4915[i_16_];
				class296_sub4.anInt4608 = i_16_;
				class296_sub4.anInt4616 = (class296_sub41.anInt4922 * i * i * class296_sub41.aByteArray4916[i_16_]) + 1024 >> 11;
				class296_sub4.anInt4600 = class296_sub41.aByteArray4921[i_16_] & 0xff;
				class296_sub4.anInt4603 = (-(class296_sub41.aShortArray4918[i_16_] & 0x7fff) + (i_16_ << 8));
				class296_sub4.anInt4617 = -1;
				class296_sub4.anInt4602 = 0;
				class296_sub4.anInt4611 = 0;
				class296_sub4.anInt4609 = 0;
				class296_sub4.anInt4612 = 0;
				if (anIntArray6315[i_17_] != 0) {
					class296_sub4.aClass296_Sub45_Sub1_4607 = (Class296_Sub45_Sub1.method2970(class296_sub19_sub1, method3016(class296_sub4, (byte) 120), 0, method3004(false, class296_sub4)));
					method3012(class296_sub41.aShortArray4918[i_16_] < 0, class296_sub4, 13610);
				} else
					class296_sub4.aClass296_Sub45_Sub1_4607 = (Class296_Sub45_Sub1.method2970(class296_sub19_sub1, method3016(class296_sub4, (byte) 127), method3021(class296_sub4, (byte) 106), method3004(false, class296_sub4)));
				if (class296_sub41.aShortArray4918[i_16_] < 0)
					class296_sub4.aClass296_Sub45_Sub1_4607.method2960(-1);
				if (class296_sub4.anInt4613 >= 0) {
					Class296_Sub4 class296_sub4_20_ = (aClass296_Sub4ArrayArray6321[i_17_][class296_sub4.anInt4613]);
					if (class296_sub4_20_ != null && class296_sub4_20_.anInt4617 < 0) {
						aClass296_Sub4ArrayArray6306[i_17_][(class296_sub4_20_.anInt4608)] = null;
						class296_sub4_20_.anInt4617 = 0;
					}
					aClass296_Sub4ArrayArray6321[i_17_][(class296_sub4.anInt4613)] = class296_sub4;
				}
				aClass296_Sub45_Sub3_6323.aClass155_6289.addLast((byte) 111, class296_sub4);
				aClass296_Sub4ArrayArray6306[i_17_][i_16_] = class296_sub4;
			}
		}
	}

	final synchronized void method3000(MidiDecoder class296_sub47, int i, boolean bool, long l, boolean bool_21_) {
		if (i >= -43)
			method3009(-45);
		method3028(bool, bool_21_, true, class296_sub47);
		method2996(98, l * (long) aClass109_6311.anInt1138);
	}

	final synchronized void method3001(int i) {
		method3027(true, 2);
		if (i != -532329720)
			anInt6327 = -49;
	}

	private final void method3002(byte i, int i_22_, int i_23_, int i_24_) {
		int i_25_ = 69 / ((i - 27) / 37);
		Class296_Sub4 class296_sub4 = aClass296_Sub4ArrayArray6306[i_23_][i_22_];
		if (class296_sub4 != null) {
			aClass296_Sub4ArrayArray6306[i_23_][i_22_] = null;
			if ((anIntArray6304[i_23_] & 0x2) != 0) {
				for (Class296_Sub4 class296_sub4_26_ = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.removeFirst((byte) 121); class296_sub4_26_ != null; class296_sub4_26_ = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.removeNext(1001)) {
					if (class296_sub4.anInt4597 == class296_sub4_26_.anInt4597 && class296_sub4_26_.anInt4617 < 0 && class296_sub4 != class296_sub4_26_) {
						class296_sub4.anInt4617 = 0;
						break;
					}
				}
			} else
				class296_sub4.anInt4617 = 0;
		}
	}

	private final void method3003(int i, byte i_27_) {
		int i_28_ = i & 0xf0;
		if (i_27_ != 11)
			method2996(-70, 92L);
		if (i_28_ == 128) {
			int i_29_ = i & 0xf;
			int i_30_ = (i & 0x7f53) >> 8;
			int i_31_ = i >> 16 & 0x7f;
			method3002((byte) 91, i_30_, i_29_, i_31_);
		} else if (i_28_ == 144) {
			int i_32_ = i & 0xf;
			int i_33_ = i >> 8 & 0x7f;
			int i_34_ = (i & 0x7f496b) >> 16;
			if (i_34_ > 0)
				method2999(i_34_, i_33_, i_32_, (byte) -31);
			else
				method3002((byte) -105, i_33_, i_32_, 64);
		} else if (i_28_ == 160) {
			int i_35_ = i & 0xf;
			int i_36_ = (i & 0x7ffa) >> 8;
			int i_37_ = (i & 0x7fd8e5) >> 16;
			method3007(i_35_, i_37_, i_36_, (byte) -114);
		} else if (i_28_ == 176) {
			int i_38_ = i & 0xf;
			int i_39_ = i >> 8 & 0x7f;
			int i_40_ = i >> 16 & 0x7f;
			if (i_39_ == 0)
				anIntArray6305[i_38_] = ((i_40_ << 14) + (-2080769 & anIntArray6305[i_38_]));
			if (i_39_ == 32)
				anIntArray6305[i_38_] = ((anIntArray6305[i_38_] & -16257) + (i_40_ << 7));
			if (i_39_ == 1)
				anIntArray6318[i_38_] = ((anIntArray6318[i_38_] & -16257) + (i_40_ << 7));
			if (i_39_ == 33)
				anIntArray6318[i_38_] = (-128 & anIntArray6318[i_38_]) + i_40_;
			if (i_39_ == 5)
				anIntArray6298[i_38_] = ((-16257 & anIntArray6298[i_38_]) + (i_40_ << 7));
			if (i_39_ == 37)
				anIntArray6298[i_38_] = i_40_ + (anIntArray6298[i_38_] & -128);
			if (i_39_ == 7)
				anIntArray6309[i_38_] = ((i_40_ << 7) + (-16257 & anIntArray6309[i_38_]));
			if (i_39_ == 39)
				anIntArray6309[i_38_] = i_40_ + (anIntArray6309[i_38_] & -128);
			if (i_39_ == 10)
				anIntArray6310[i_38_] = ((-16257 & anIntArray6310[i_38_]) + (i_40_ << 7));
			if (i_39_ == 42)
				anIntArray6310[i_38_] = i_40_ + (-128 & anIntArray6310[i_38_]);
			if (i_39_ == 11)
				anIntArray6300[i_38_] = ((i_40_ << 7) + (-16257 & anIntArray6300[i_38_]));
			if (i_39_ == 43)
				anIntArray6300[i_38_] = (-128 & anIntArray6300[i_38_]) + i_40_;
			if (i_39_ == 64) {
				if (i_40_ >= 64)
					anIntArray6304[i_38_] = anIntArray6304[i_38_] | 1;
				else
					anIntArray6304[i_38_] = anIntArray6304[i_38_] & -2;
			}
			if (i_39_ == 65) {
				if (i_40_ < 64) {
					method3011(i_38_, -41);
					anIntArray6304[i_38_] = anIntArray6304[i_38_] & -3;
				} else
					anIntArray6304[i_38_] = anIntArray6304[i_38_] | 2;
			}
			if (i_39_ == 99)
				anIntArray6297[i_38_] = (127 & anIntArray6297[i_38_]) + (i_40_ << 7);
			if (i_39_ == 98)
				anIntArray6297[i_38_] = ((16256 & anIntArray6297[i_38_]) + i_40_);
			if (i_39_ == 101)
				anIntArray6297[i_38_] = (16384 + (anIntArray6297[i_38_] & 127) + (i_40_ << 7));
			if (i_39_ == 100)
				anIntArray6297[i_38_] = i_40_ + ((16256 & anIntArray6297[i_38_]) + 16384);
			if (i_39_ == 120)
				method3024(i_27_ - 91, i_38_);
			if (i_39_ == 121)
				method3023((byte) 83, i_38_);
			if (i_39_ == 123)
				method2994(i_38_, (byte) 118);
			if (i_39_ == 6) {
				int i_41_ = anIntArray6297[i_38_];
				if (i_41_ == 16384)
					anIntArray6301[i_38_] = ((i_40_ << 7) + (anIntArray6301[i_38_] & -16257));
			}
			if (i_39_ == 38) {
				int i_42_ = anIntArray6297[i_38_];
				if (i_42_ == 16384)
					anIntArray6301[i_38_] = i_40_ + (anIntArray6301[i_38_] & -128);
			}
			if (i_39_ == 16)
				anIntArray6315[i_38_] = (i_40_ << 7) + (anIntArray6315[i_38_] & -16257);
			if (i_39_ == 48)
				anIntArray6315[i_38_] = i_40_ + (anIntArray6315[i_38_] & -128);
			if (i_39_ == 81) {
				if (i_40_ < 64) {
					method3020(i_38_, false);
					anIntArray6304[i_38_] = anIntArray6304[i_38_] & -5;
				} else
					anIntArray6304[i_38_] = anIntArray6304[i_38_] | 4;
			}
			if (i_39_ == 17)
				method3025(i_38_, -2, (anIntArray6296[i_38_] & ~0x3f80) + (i_40_ << 7));
			if (i_39_ == 49)
				method3025(i_38_, i_27_ - 13, i_40_ + (anIntArray6296[i_38_] & ~0x7f));
		} else if (i_28_ == 192) {
			int i_43_ = i & 0xf;
			int i_44_ = i >> 8 & 0x7f;
			method2995(i_43_, anIntArray6305[i_43_] + i_44_, (byte) 104);
		} else if (i_28_ == 208) {
			int i_45_ = i & 0xf;
			int i_46_ = i >> 8 & 0x7f;
			method2997(i_46_, true, i_45_);
		} else if (i_28_ == 224) {
			int i_47_ = i & 0xf;
			int i_48_ = (i >> 9 & 0x3f80) + (i >> 8 & 0x7f);
			method3030(i_48_, true, i_47_);
		} else {
			i_28_ = i & 0xff;
			if (i_28_ == 255)
				method3010(true, i_27_ + 13213);
		}
	}

	private final int method3004(boolean bool, Class296_Sub4 class296_sub4) {
		if (bool)
			method3015(null, false);
		int i = anIntArray6310[class296_sub4.anInt4597];
		if (i >= 8192)
			return (-((128 - class296_sub4.anInt4600) * (-i + 16384) + 32 >> 6) + 16384);
		return class296_sub4.anInt4600 * i + 32 >> 6;
	}

	final synchronized void method3005(boolean bool) {
		if (bool)
			anIntArray6307 = null;
		for (Class296_Sub41 class296_sub41 = (Class296_Sub41) aClass263_6303.getFirst(!bool); class296_sub41 != null; class296_sub41 = (Class296_Sub41) aClass263_6303.getNext(0))
			class296_sub41.unlink();
	}

	public static void method3006(boolean bool) {
		aClass161_6302 = null;
		if (bool)
			aBoolean6319 = false;
	}

	private final void method3007(int i, int i_49_, int i_50_, byte i_51_) {
		int i_52_ = 53 % ((i_51_ + 57) / 49);
	}

	final synchronized boolean method3008(Js5 class138, MidiDecoder class296_sub47, Class25 class25, boolean bool, int i) {
		class296_sub47.method3040();
		boolean bool_53_ = true;
		int[] is = null;
		if (i > 0)
			is = new int[]{i};
		for (Class296_Sub25 class296_sub25 = ((Class296_Sub25) class296_sub47.aClass263_4973.getFirst(true)); class296_sub25 != null; class296_sub25 = ((Class296_Sub25) class296_sub47.aClass263_4973.getNext(0))) {
			int i_54_ = (int) class296_sub25.uid;
			Class296_Sub41 class296_sub41 = ((Class296_Sub41) aClass263_6303.get((long) i_54_));
			if (class296_sub41 == null) {
				class296_sub41 = Class296_Sub22.method2659(class138, (byte) 0, i_54_);
				if (class296_sub41 == null) {
					bool_53_ = false;
					continue;
				}
				aClass263_6303.put((long) i_54_, class296_sub41);
			}
			if (!class296_sub41.method2916(class25, false, is, class296_sub25.aByteArray4763))
				bool_53_ = false;
		}
		if (bool)
			anIntArray6304 = null;
		if (bool_53_)
			class296_sub47.method3041();
		return bool_53_;
	}

	final synchronized boolean method3009(int i) {
		int i_55_ = -69 / ((73 - i) / 35);
		return aClass109_6311.method952();
	}

	private final void method3010(boolean bool, int i) {
		if (bool)
			method3024(105, -1);
		else
			method2994(-1, (byte) 110);
		if (i != 13224)
			method3020(66, false);
		method3023((byte) 83, -1);
		for (int i_56_ = 0; i_56_ < 16; i_56_++)
			anIntArray6299[i_56_] = anIntArray6307[i_56_];
		for (int i_57_ = 0; i_57_ < 16; i_57_++)
			anIntArray6305[i_57_] = anIntArray6307[i_57_] & -128;
	}

	private final void method3011(int i, int i_58_) {
		if ((anIntArray6304[i] & 0x2) != 0) {
			for (Class296_Sub4 class296_sub4 = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.removeFirst((byte) 126); class296_sub4 != null; class296_sub4 = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.removeNext(1001)) {
				if (i == class296_sub4.anInt4597 && (aClass296_Sub4ArrayArray6306[i][class296_sub4.anInt4608]) == null && class296_sub4.anInt4617 < 0)
					class296_sub4.anInt4617 = 0;
			}
		}
		int i_59_ = 20 / ((46 - i_58_) / 42);
	}

	final synchronized void method2934(int[] is, int i, int i_60_) {
		if (aClass109_6311.method952()) {
			int i_61_ = aClass109_6311.anInt1138 * anInt6320 / Class298.anInt2699;
			do {
				long l = aLong6325 + (long) i_61_ * (long) i_60_;
				if (aLong6324 + -l >= 0L) {
					aLong6325 = l;
					break;
				}
				int i_62_ = (int) ((-aLong6325 + -1L + aLong6324 + (long) i_61_) / (long) i_61_);
				aLong6325 += (long) i_62_ * (long) i_61_;
				aClass296_Sub45_Sub3_6323.method2934(is, i, i_62_);
				method2993(1758790920);
				i_60_ -= i_62_;
				i += i_62_;
			} while (aClass109_6311.method952());
		}
		aClass296_Sub45_Sub3_6323.method2934(is, i, i_60_);
	}

	final void method3012(boolean bool, Class296_Sub4 class296_sub4, int i) {
		int i_63_ = class296_sub4.aClass296_Sub19_Sub1_4605.aByteArray6051.length;
		if (i == 13610) {
			int i_64_;
			if (!bool || !class296_sub4.aClass296_Sub19_Sub1_4605.aBoolean6053)
				i_64_ = (int) (((long) i_63_ * (long) anIntArray6315[class296_sub4.anInt4597]) >> 6);
			else {
				int i_65_ = (-class296_sub4.aClass296_Sub19_Sub1_4605.anInt6049 + i_63_ + i_63_);
				i_63_ <<= 8;
				i_64_ = (int) (((long) anIntArray6315[class296_sub4.anInt4597] * (long) i_65_) >> 6);
				if (i_64_ >= i_63_) {
					class296_sub4.aClass296_Sub45_Sub1_4607.method2951(true);
					i_64_ = i_63_ - 1 + i_63_ - i_64_;
				}
			}
			class296_sub4.aClass296_Sub45_Sub1_4607.method2961(i_64_);
		}
	}

	final synchronized void method3013(byte i) {
		if (i > -9)
			anInt6317 = -87;
		for (Class296_Sub41 class296_sub41 = (Class296_Sub41) aClass263_6303.getFirst(true); class296_sub41 != null; class296_sub41 = (Class296_Sub41) aClass263_6303.getNext(0))
			class296_sub41.method2914(110);
	}

	final synchronized void method3014(MidiDecoder class296_sub47, boolean bool, boolean bool_66_) {
		method3028(bool, true, bool_66_, class296_sub47);
	}

	final synchronized Class296_Sub45 method2930() {
		return aClass296_Sub45_Sub3_6323;
	}

	final boolean method3015(Class296_Sub4 class296_sub4, boolean bool) {
		if (bool != true)
			return false;
		if (class296_sub4.aClass296_Sub45_Sub1_4607 == null) {
			if (class296_sub4.anInt4617 >= 0) {
				class296_sub4.unlink();
				if (class296_sub4.anInt4613 > 0 && class296_sub4 == (aClass296_Sub4ArrayArray6321[class296_sub4.anInt4597][class296_sub4.anInt4613]))
					aClass296_Sub4ArrayArray6321[class296_sub4.anInt4597][class296_sub4.anInt4613] = null;
			}
			return true;
		}
		return false;
	}

	private final int method3016(Class296_Sub4 class296_sub4, byte i) {
		int i_67_ = (class296_sub4.anInt4603 + (class296_sub4.anInt4606 * class296_sub4.anInt4604 >> 12));
		i_67_ += (anIntArray6301[class296_sub4.anInt4597] * (anIntArray6312[class296_sub4.anInt4597] - 8192)) >> 12;
		Class242 class242 = class296_sub4.aClass242_4599;
		if (class242.anInt2311 > 0 && (class242.anInt2314 > 0 || anIntArray6318[class296_sub4.anInt4597] > 0)) {
			int i_68_ = class242.anInt2314 << 2;
			int i_69_ = class242.anInt2312 << 1;
			if (class296_sub4.anInt4620 < i_69_)
				i_68_ = i_68_ * class296_sub4.anInt4620 / i_69_;
			i_68_ += anIntArray6318[class296_sub4.anInt4597] >> 7;
			double d = Math.sin((double) (class296_sub4.anInt4601 & 0x1ff) * 0.01227184630308513);
			i_67_ += (int) (d * (double) i_68_);
		}
		if (i <= 118)
			return -41;
		int i_70_ = (int) (((double) ((class296_sub4.aClass296_Sub19_Sub1_4605.anInt6052) * 256) * Math.pow(2.0, (double) i_67_ * 3.255208333333333E-4) / (double) Class298.anInt2699) + 0.5);
		if (i_70_ >= 1)
			return i_70_;
		return 1;
	}

	static final void method3017(r var_r, int i, int i_71_, int i_72_, boolean[] bools) {
		if (Class360_Sub2.aSArray5304 != Class52.aSArray636) {
			int i_73_ = Class244.aSArray2320[i].method3349(0, i_72_, i_71_);
			for (int i_74_ = 0; i_74_ <= i; i_74_++) {
				if (bools == null || bools[i_74_]) {
					s var_s = Class244.aSArray2320[i_74_];
					if (var_s != null)
						var_s.wa(var_r, i_71_, i_73_ - var_s.method3349(0, i_72_, i_71_), i_72_, 0, false);
				}
			}
		}
	}

	final synchronized void method2933(int i) {
		if (aClass109_6311.method952()) {
			int i_75_ = aClass109_6311.anInt1138 * anInt6320 / Class298.anInt2699;
			do {
				long l = (long) i_75_ * (long) i + aLong6325;
				if (aLong6324 - l >= 0L) {
					aLong6325 = l;
					break;
				}
				int i_76_ = (int) (((long) i_75_ + -aLong6325 + aLong6324 + -1L) / (long) i_75_);
				aLong6325 += (long) i_76_ * (long) i_75_;
				aClass296_Sub45_Sub3_6323.method2933(i_76_);
				i -= i_76_;
				method2993(1758790920);
			} while (aClass109_6311.method952());
		}
		aClass296_Sub45_Sub3_6323.method2933(i);
	}

	static final void method3018(int i) {
		Class69_Sub3.aClass397_5722 = null;
		Class296_Sub39_Sub19.anInt6250 = -1;
		if (i >= -106)
			method3006(true);
	}

	final synchronized int method2929() {
		return 0;
	}

	final synchronized void method3019(int i, int i_77_) {
		if (i_77_ < 35)
			anInt6320 = -50;
		anInt6314 = i;
	}

	private final void method3020(int i, boolean bool) {
		if (bool)
			anIntArray6296 = null;
		if ((anIntArray6304[i] & 0x4) != 0) {
			for (Class296_Sub4 class296_sub4 = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.removeFirst((byte) 118); class296_sub4 != null; class296_sub4 = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.removeNext(1001)) {
				if (i == class296_sub4.anInt4597)
					class296_sub4.anInt4619 = 0;
			}
		}
	}

	private final int method3021(Class296_Sub4 class296_sub4, byte i) {
		if (anIntArray6313[class296_sub4.anInt4597] == 0)
			return 0;
		Class242 class242 = class296_sub4.aClass242_4599;
		int i_78_ = ((anIntArray6300[class296_sub4.anInt4597] * anIntArray6309[class296_sub4.anInt4597]) + 4096 >> 13);
		i_78_ = i_78_ * i_78_ + 16384 >> 15;
		i_78_ = i_78_ * class296_sub4.anInt4616 + 16384 >> 15;
		i_78_ = i_78_ * anInt6314 + 128 >> 8;
		if (i <= 70)
			method3004(true, null);
		i_78_ = i_78_ * anIntArray6313[class296_sub4.anInt4597] + 128 >> 8;
		if (class242.anInt2307 > 0)
			i_78_ = (int) (Math.pow(0.5, ((double) class242.anInt2307 * ((double) class296_sub4.anInt4611 * 1.953125E-5))) * (double) i_78_ + 0.5);
		if (class242.aByteArray2306 != null) {
			int i_79_ = class296_sub4.anInt4602;
			int i_80_ = class242.aByteArray2306[class296_sub4.anInt4612 + 1];
			if (class242.aByteArray2306.length - 2 > class296_sub4.anInt4612) {
				int i_81_ = ((class242.aByteArray2306[class296_sub4.anInt4612] & 0xff) << 8);
				int i_82_ = ((class242.aByteArray2306[class296_sub4.anInt4612 + 2] << 8) & 0xff00);
				i_80_ += ((-i_81_ + i_79_) * (-i_80_ + (class242.aByteArray2306[class296_sub4.anInt4612 + 3])) / (i_82_ - i_81_));
			}
			i_78_ = i_80_ * i_78_ + 32 >> 6;
		}
		if (class296_sub4.anInt4617 > 0 && class242.aByteArray2305 != null) {
			int i_83_ = class296_sub4.anInt4617;
			int i_84_ = class242.aByteArray2305[class296_sub4.anInt4609 + 1];
			if (class242.aByteArray2305.length - 2 > class296_sub4.anInt4609) {
				int i_85_ = ((class242.aByteArray2305[class296_sub4.anInt4609] & 0xff) << 8);
				int i_86_ = ((class242.aByteArray2305[class296_sub4.anInt4609 + 2] & 0xff) << 8);
				i_84_ += ((-i_85_ + i_83_) * (-i_84_ + (class242.aByteArray2305[class296_sub4.anInt4609 + 3])) / (i_86_ - i_85_));
			}
			i_78_ = i_84_ * i_78_ + 32 >> 6;
		}
		return i_78_;
	}

	final synchronized void method3022(int i, int i_87_, int i_88_) {
		if (i_87_ <= i_88_)
			anIntArray6313[i_88_] = i;
		else {
			for (int i_89_ = 0; i_89_ < 16; i_89_++)
				anIntArray6313[i_89_] = i;
		}
	}

	private final void method3023(byte i, int i_90_) {
		if (i_90_ < 0) {
			for (i_90_ = 0; i_90_ < 16; i_90_++)
				method3023((byte) 83, i_90_);
		} else {
			anIntArray6309[i_90_] = 12800;
			anIntArray6310[i_90_] = 8192;
			anIntArray6300[i_90_] = 16383;
			anIntArray6312[i_90_] = 8192;
			anIntArray6318[i_90_] = 0;
			anIntArray6298[i_90_] = 8192;
			method3011(i_90_, i - 100);
			method3020(i_90_, false);
			anIntArray6304[i_90_] = 0;
			anIntArray6297[i_90_] = 32767;
			if (i == 83) {
				anIntArray6301[i_90_] = 256;
				anIntArray6315[i_90_] = 0;
				method3025(i_90_, -2, 8192);
			}
		}
	}

	private final void method3024(int i, int i_91_) {
		int i_92_ = -70 % ((i - 14) / 55);
		for (Class296_Sub4 class296_sub4 = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.removeFirst((byte) 120); class296_sub4 != null; class296_sub4 = (Class296_Sub4) aClass296_Sub45_Sub3_6323.aClass155_6289.removeNext(1001)) {
			if (i_91_ < 0 || i_91_ == class296_sub4.anInt4597) {
				if (class296_sub4.aClass296_Sub45_Sub1_4607 != null) {
					class296_sub4.aClass296_Sub45_Sub1_4607.method2967(Class298.anInt2699 / 100);
					if (class296_sub4.aClass296_Sub45_Sub1_4607.method2971())
						aClass296_Sub45_Sub3_6323.aClass296_Sub45_Sub5_6292.method3035(class296_sub4.aClass296_Sub45_Sub1_4607);
					class296_sub4.method2443(67);
				}
				if (class296_sub4.anInt4617 < 0)
					aClass296_Sub4ArrayArray6306[class296_sub4.anInt4597][class296_sub4.anInt4608] = null;
				class296_sub4.unlink();
			}
		}
	}

	private final void method3025(int i, int i_93_, int i_94_) {
		if (i_93_ == -2) {
			anIntArray6296[i] = i_94_;
			anIntArray6308[i] = (int) ((Math.pow(2.0, (double) i_94_ * 5.4931640625E-4) * 2097152.0) + 0.5);
		}
	}

	final boolean method3026(int i, byte i_95_, int i_96_, Class296_Sub4 class296_sub4, int[] is) {
		class296_sub4.anInt4614 = Class298.anInt2699 / 100;
		if (class296_sub4.anInt4617 >= 0 && (class296_sub4.aClass296_Sub45_Sub1_4607 == null || class296_sub4.aClass296_Sub45_Sub1_4607.method2964())) {
			class296_sub4.method2443(99);
			class296_sub4.unlink();
			if (class296_sub4.anInt4613 > 0 && (aClass296_Sub4ArrayArray6321[class296_sub4.anInt4597][class296_sub4.anInt4613]) == class296_sub4)
				aClass296_Sub4ArrayArray6321[class296_sub4.anInt4597][class296_sub4.anInt4613] = null;
			return true;
		}
		int i_97_ = class296_sub4.anInt4606;
		if (i_97_ > 0) {
			i_97_ -= (int) (Math.pow(2.0, ((double) (anIntArray6298[class296_sub4.anInt4597]) * 4.921259842519685E-4)) * 16.0 + 0.5);
			if (i_97_ < 0)
				i_97_ = 0;
			class296_sub4.anInt4606 = i_97_;
		}
		class296_sub4.aClass296_Sub45_Sub1_4607.method2952(method3016(class296_sub4, (byte) 127));
		Class242 class242 = class296_sub4.aClass242_4599;
		class296_sub4.anInt4601 += class242.anInt2311;
		class296_sub4.anInt4620++;
		if (i_95_ != -119)
			anIntArray6318 = null;
		boolean bool = false;
		double d = ((double) ((class296_sub4.anInt4604 * class296_sub4.anInt4606 >> 12) + (class296_sub4.anInt4608 - 60 << 8)) * 5.086263020833333E-6);
		if (class242.anInt2307 > 0) {
			if (class242.anInt2308 <= 0)
				class296_sub4.anInt4611 += 128;
			else
				class296_sub4.anInt4611 += (int) ((Math.pow(2.0, (double) class242.anInt2308 * d) * 128.0) + 0.5);
			if (class242.anInt2307 * class296_sub4.anInt4611 >= 819200)
				bool = true;
		}
		if (class242.aByteArray2306 != null) {
			if (class242.anInt2310 > 0)
				class296_sub4.anInt4602 += (int) ((Math.pow(2.0, (double) class242.anInt2310 * d) * 128.0) + 0.5);
			else
				class296_sub4.anInt4602 += 128;
			for (/**/; class242.aByteArray2306.length - 2 > class296_sub4.anInt4612; class296_sub4.anInt4612 += 2) {
				if (class296_sub4.anInt4602 <= (class242.aByteArray2306[class296_sub4.anInt4612 + 2] & 0xff) << 8)
					break;
			}
			if (class242.aByteArray2306.length - 2 == class296_sub4.anInt4612 && class242.aByteArray2306[class296_sub4.anInt4612 + 1] == 0)
				bool = true;
		}
		if (class296_sub4.anInt4617 >= 0 && class242.aByteArray2305 != null && (anIntArray6304[class296_sub4.anInt4597] & 0x1) == 0 && (class296_sub4.anInt4613 < 0 || (aClass296_Sub4ArrayArray6321[class296_sub4.anInt4597][class296_sub4.anInt4613]) != class296_sub4)) {
			if (class242.anInt2313 <= 0)
				class296_sub4.anInt4617 += 128;
			else
				class296_sub4.anInt4617 += (int) ((Math.pow(2.0, d * (double) class242.anInt2313) * 128.0) + 0.5);
			for (/**/; class296_sub4.anInt4609 < class242.aByteArray2305.length - 2; class296_sub4.anInt4609 += 2) {
				if (class296_sub4.anInt4617 <= (class242.aByteArray2305[class296_sub4.anInt4609 + 2] & 0xff) << 8)
					break;
			}
			if (class242.aByteArray2305.length - 2 == class296_sub4.anInt4609)
				bool = true;
		}
		if (bool) {
			class296_sub4.aClass296_Sub45_Sub1_4607.method2967(class296_sub4.anInt4614);
			if (is == null)
				class296_sub4.aClass296_Sub45_Sub1_4607.method2933(i_96_);
			else
				class296_sub4.aClass296_Sub45_Sub1_4607.method2934(is, i, i_96_);
			if (class296_sub4.aClass296_Sub45_Sub1_4607.method2971())
				aClass296_Sub45_Sub3_6323.aClass296_Sub45_Sub5_6292.method3035(class296_sub4.aClass296_Sub45_Sub1_4607);
			class296_sub4.method2443(89);
			if (class296_sub4.anInt4617 >= 0) {
				class296_sub4.unlink();
				if (class296_sub4.anInt4613 > 0 && (aClass296_Sub4ArrayArray6321[class296_sub4.anInt4597][class296_sub4.anInt4613]) == class296_sub4)
					aClass296_Sub4ArrayArray6321[class296_sub4.anInt4597][class296_sub4.anInt4613] = null;
			}
			return true;
		}
		class296_sub4.aClass296_Sub45_Sub1_4607.method2956(class296_sub4.anInt4614, method3021(class296_sub4, (byte) 100), method3004(false, class296_sub4));
		return false;
	}

	private final synchronized void method3027(boolean bool, int i) {
		aClass109_6311.method964();
		aClass296_Sub47_6328 = null;
		method3010(bool, 13224);
		if (i != 2)
			method2999(-38, 93, 115, (byte) -94);
	}

	final synchronized Class296_Sub45 method2932() {
		return null;
	}

	private final synchronized void method3028(boolean bool, boolean bool_98_, boolean bool_99_, MidiDecoder class296_sub47) {
		method3027(bool_98_, 2);
		aClass109_6311.method954(class296_sub47.buffer);
		aLong6325 = 0L;
		aBoolean6322 = bool;
		int i = aClass109_6311.getTracksCount();
		for (int i_100_ = 0; i > i_100_; i_100_++) {
			aClass109_6311.loadTrackBuffer(i_100_);
			aClass109_6311.readTickDuration(i_100_);
			aClass109_6311.saveTrackBuffer(i_100_);
		}
		anInt6327 = aClass109_6311.getFastestTrack();
		if (bool_99_ != true)
			anInt6327 = -42;
		anInt6326 = aClass109_6311.tick_durations[anInt6327];
		aLong6324 = aClass109_6311.method962(anInt6326);
	}

	final int method3029(int i) {
		if (i != 2)
			method3004(false, null);
		return anInt6314;
	}

	private final void method3030(int i, boolean bool, int i_101_) {
		if (bool != true)
			method2993(51);
		anIntArray6312[i_101_] = i;
	}

	public Class296_Sub45_Sub4() {
		anIntArray6297 = new int[16];
		anIntArray6301 = new int[16];
		anIntArray6300 = new int[16];
		anIntArray6304 = new int[16];
		anIntArray6305 = new int[16];
		anIntArray6309 = new int[16];
		anIntArray6308 = new int[16];
		anIntArray6299 = new int[16];
		aClass296_Sub4ArrayArray6306 = new Class296_Sub4[16][128];
		anIntArray6298 = new int[16];
		anIntArray6296 = new int[16];
		anIntArray6307 = new int[16];
		anIntArray6318 = new int[16];
		anIntArray6310 = new int[16];
		anInt6314 = 256;
		anIntArray6313 = new int[16];
		anInt6320 = 1000000;
		anIntArray6312 = new int[16];
		aClass296_Sub4ArrayArray6321 = new Class296_Sub4[16][128];
		anIntArray6315 = new int[16];
		aClass109_6311 = new MidiEvent();
		aClass296_Sub45_Sub3_6323 = new Class296_Sub45_Sub3(this);
		aClass263_6303 = new HashTable(128);
		method3022(256, 0, -1);
		method3010(true, 13224);
	}

	Class296_Sub45_Sub4(Class296_Sub45_Sub4 class296_sub45_sub4_102_) {
		anIntArray6297 = new int[16];
		anIntArray6301 = new int[16];
		anIntArray6300 = new int[16];
		anIntArray6304 = new int[16];
		anIntArray6305 = new int[16];
		anIntArray6309 = new int[16];
		anIntArray6308 = new int[16];
		anIntArray6299 = new int[16];
		aClass296_Sub4ArrayArray6306 = new Class296_Sub4[16][128];
		anIntArray6298 = new int[16];
		anIntArray6296 = new int[16];
		anIntArray6307 = new int[16];
		anIntArray6318 = new int[16];
		anIntArray6310 = new int[16];
		anInt6314 = 256;
		anIntArray6313 = new int[16];
		anInt6320 = 1000000;
		anIntArray6312 = new int[16];
		aClass296_Sub4ArrayArray6321 = new Class296_Sub4[16][128];
		anIntArray6315 = new int[16];
		aClass109_6311 = new MidiEvent();
		aClass296_Sub45_Sub3_6323 = new Class296_Sub45_Sub3(this);
		aClass263_6303 = class296_sub45_sub4_102_.aClass263_6303;
		method3022(256, 0, -1);
		method3010(true, 13224);
	}

	static {
		anInt6317 = 1405;
	}
}
