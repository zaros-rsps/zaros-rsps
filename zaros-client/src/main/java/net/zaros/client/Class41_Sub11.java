package net.zaros.client;
/* Class41_Sub11 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;
import java.net.Socket;

final class Class41_Sub11 extends Class41 {
	Class41_Sub11(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	Class41_Sub11(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final int method380(int i, byte i_0_) {
		if (i_0_ != 41)
			return 7;
		return 1;
	}

	final int method383(byte i) {
		if (i != 110)
			return -74;
		return 1;
	}

	final void method386(int i) {
		if (i == 2) {
			if (aClass296_Sub50_392.method3050(28520) == Class296_Sub32.stellardawn)
				anInt389 = 2;
			if (anInt389 < 0 || anInt389 > 2)
				anInt389 = method383((byte) 110);
		}
	}

	final void method381(int i, byte i_1_) {
		if (i_1_ == -110)
			anInt389 = i;
	}

	static final Class154 method436(int i, int i_2_, Socket socket) throws IOException {
		if (i_2_ != -1)
			return null;
		return new Class154_Sub1(socket, i);
	}

	final int method437(int i) {
		if (i <= 114)
			return -12;
		return anInt389;
	}
}
