package net.zaros.client;
/* Class104_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.nio.ByteBuffer;

final class Class104_Sub1 extends Class104 {
	private ByteBuffer aByteBuffer3901;

	final void method903(byte[] is, byte i) {
		aByteBuffer3901 = ByteBuffer.allocateDirect(is.length);
		if (i > -22)
			method899(-44);
		aByteBuffer3901.position(0);
		aByteBuffer3901.put(is);
	}

	final byte[] method901(int i, int i_0_, int i_1_) {
		byte[] is = new byte[i_1_];
		aByteBuffer3901.position(i_0_);
		aByteBuffer3901.get(is, 0, i_1_);
		if (i > -111)
			method901(107, -5, -22);
		return is;
	}

	final byte[] method899(int i) {
		if (i != 9629)
			aByteBuffer3901 = null;
		byte[] is = new byte[aByteBuffer3901.capacity()];
		aByteBuffer3901.position(0);
		aByteBuffer3901.get(is);
		return is;
	}
}
