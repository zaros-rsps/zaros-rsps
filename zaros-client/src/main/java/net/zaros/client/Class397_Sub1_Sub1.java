package net.zaros.client;

/* Class397_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class397_Sub1_Sub1 extends Class397_Sub1 {
	int[] anIntArray6690;

	public void method4093(int i, int i_0_, aa var_aa, int i_1_, int i_2_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		i += anInt5772;
		i_0_ += anInt5761;
		int i_3_ = 0;
		int i_4_ = aHa_Sub2_5768.anInt4084;
		int i_5_ = anInt5758;
		int i_6_ = anInt5756;
		int i_7_ = i_4_ - i_5_;
		int i_8_ = 0;
		int i_9_ = i + i_0_ * i_4_;
		if (i_0_ < aHa_Sub2_5768.anInt4085) {
			int i_10_ = aHa_Sub2_5768.anInt4085 - i_0_;
			i_6_ -= i_10_;
			i_0_ = aHa_Sub2_5768.anInt4085;
			i_3_ += i_10_ * i_5_;
			i_9_ += i_10_ * i_4_;
		}
		if (i_0_ + i_6_ > aHa_Sub2_5768.anInt4078)
			i_6_ -= i_0_ + i_6_ - aHa_Sub2_5768.anInt4078;
		if (i < aHa_Sub2_5768.anInt4079) {
			int i_11_ = aHa_Sub2_5768.anInt4079 - i;
			i_5_ -= i_11_;
			i = aHa_Sub2_5768.anInt4079;
			i_3_ += i_11_;
			i_9_ += i_11_;
			i_8_ += i_11_;
			i_7_ += i_11_;
		}
		if (i + i_5_ > aHa_Sub2_5768.anInt4104) {
			int i_12_ = i + i_5_ - aHa_Sub2_5768.anInt4104;
			i_5_ -= i_12_;
			i_8_ += i_12_;
			i_7_ += i_12_;
		}
		if (i_5_ > 0 && i_6_ > 0) {
			aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
			int[] is = var_aa_Sub2.anIntArray3726;
			int[] is_13_ = var_aa_Sub2.anIntArray3729;
			int[] is_14_ = aHa_Sub2_5768.anIntArray4108;
			int i_15_ = i_0_;
			if (i_2_ > i_15_) {
				i_15_ = i_2_;
				i_9_ += (i_2_ - i_0_) * i_4_;
				i_3_ += (i_2_ - i_0_) * anInt5758;
			}
			int i_16_ = (i_2_ + is.length < i_0_ + i_6_ ? i_2_ + is.length : i_0_ + i_6_);
			for (int i_17_ = i_15_; i_17_ < i_16_; i_17_++) {
				int i_18_ = is[i_17_ - i_2_] + i_1_;
				int i_19_ = is_13_[i_17_ - i_2_];
				int i_20_ = i_5_;
				if (i > i_18_) {
					int i_21_ = i - i_18_;
					if (i_21_ >= i_19_) {
						i_3_ += i_5_ + i_8_;
						i_9_ += i_5_ + i_7_;
						continue;
					}
					i_19_ -= i_21_;
				} else {
					int i_22_ = i_18_ - i;
					if (i_22_ >= i_5_) {
						i_3_ += i_5_ + i_8_;
						i_9_ += i_5_ + i_7_;
						continue;
					}
					i_3_ += i_22_;
					i_20_ -= i_22_;
					i_9_ += i_22_;
				}
				int i_23_ = 0;
				if (i_20_ < i_19_)
					i_19_ = i_20_;
				else
					i_23_ = i_20_ - i_19_;
				for (int i_24_ = -i_19_; i_24_ < 0; i_24_++) {
					int i_25_ = anIntArray6690[i_3_++];
					int i_26_ = i_25_ >>> 24;
					int i_27_ = 256 - i_26_;
					int i_28_ = is_14_[i_9_];
					is_14_[i_9_++] = ((((i_25_ & 0xff00ff) * i_26_ + (i_28_ & 0xff00ff) * i_27_) & ~0xff00ff)
							+ (((i_25_ & 0xff00) * i_26_ + (i_28_ & 0xff00) * i_27_) & 0xff0000)) >> 8;
				}
				i_3_ += i_23_ + i_8_;
				i_9_ += i_23_ + i_7_;
			}
		}
	}

	public void method4084(int i, int i_29_, int i_30_) {
		if (i_30_ == 0) {
			int[] is = aHa_Sub2_5768.anIntArray4108;
			for (int i_31_ = 0; i_31_ < anInt5756; i_31_++) {
				int i_32_ = i_31_ * anInt5758;
				int i_33_ = (i_29_ + i_31_) * aHa_Sub2_5768.anInt4084 + i;
				for (int i_34_ = 0; i_34_ < anInt5758; i_34_++)
					anIntArray6690[i_32_ + i_34_] = (anIntArray6690[i_32_ + i_34_] & 0xffffff
							| is[i_33_ + i_34_] << 8 & ~0xffffff);
			}
		} else if (i_30_ == 1) {
			int[] is = aHa_Sub2_5768.anIntArray4108;
			for (int i_35_ = 0; i_35_ < anInt5756; i_35_++) {
				int i_36_ = i_35_ * anInt5758;
				int i_37_ = (i_29_ + i_35_) * aHa_Sub2_5768.anInt4084 + i;
				for (int i_38_ = 0; i_38_ < anInt5758; i_38_++)
					anIntArray6690[i_36_ + i_38_] = (anIntArray6690[i_36_ + i_38_] & 0xffffff
							| is[i_37_ + i_38_] << 16 & ~0xffffff);
			}
		} else if (i_30_ == 2) {
			int[] is = aHa_Sub2_5768.anIntArray4108;
			for (int i_39_ = 0; i_39_ < anInt5756; i_39_++) {
				int i_40_ = i_39_ * anInt5758;
				int i_41_ = (i_29_ + i_39_) * aHa_Sub2_5768.anInt4084 + i;
				for (int i_42_ = 0; i_42_ < anInt5758; i_42_++)
					anIntArray6690[i_40_ + i_42_] = (anIntArray6690[i_40_ + i_42_] & 0xffffff
							| is[i_41_ + i_42_] << 24 & ~0xffffff);
			}
		} else if (i_30_ == 3) {
			int[] is = aHa_Sub2_5768.anIntArray4108;
			for (int i_43_ = 0; i_43_ < anInt5756; i_43_++) {
				int i_44_ = i_43_ * anInt5758;
				int i_45_ = (i_29_ + i_43_) * aHa_Sub2_5768.anInt4084 + i;
				for (int i_46_ = 0; i_46_ < anInt5758; i_46_++)
					anIntArray6690[i_44_ + i_46_] = (anIntArray6690[i_44_ + i_46_] & 0xffffff
							| (is[i_45_ + i_46_] != 0 ? -16777216 : 0));
			}
		}
	}

	public void method4103(int i, int i_47_, int i_48_, int i_49_, int i_50_, int i_51_, int i_52_, int i_53_,
			int i_54_) {
		if (i_49_ > 0 && i_50_ > 0) {
			int i_55_ = 0;
			int i_56_ = 0;
			int i_57_ = anInt5772 + anInt5758 + anInt5757;
			int i_58_ = anInt5761 + anInt5756 + anInt5750;
			int i_59_ = (i_57_ << 16) / i_49_;
			int i_60_ = (i_58_ << 16) / i_50_;
			if (anInt5772 > 0) {
				int i_61_ = ((anInt5772 << 16) + i_59_ - 1) / i_59_;
				i += i_61_;
				i_55_ += i_61_ * i_59_ - (anInt5772 << 16);
			}
			if (anInt5761 > 0) {
				int i_62_ = ((anInt5761 << 16) + i_60_ - 1) / i_60_;
				i_47_ += i_62_;
				i_56_ += i_62_ * i_60_ - (anInt5761 << 16);
			}
			if (anInt5758 < i_57_)
				i_49_ = ((anInt5758 << 16) - i_55_ + i_59_ - 1) / i_59_;
			if (anInt5756 < i_58_)
				i_50_ = ((anInt5756 << 16) - i_56_ + i_60_ - 1) / i_60_;
			int i_63_ = i + i_47_ * aHa_Sub2_5768.anInt4084;
			int i_64_ = aHa_Sub2_5768.anInt4084 - i_49_;
			if (i_47_ + i_50_ > aHa_Sub2_5768.anInt4078)
				i_50_ -= i_47_ + i_50_ - aHa_Sub2_5768.anInt4078;
			if (i_47_ < aHa_Sub2_5768.anInt4085) {
				int i_65_ = aHa_Sub2_5768.anInt4085 - i_47_;
				i_50_ -= i_65_;
				i_63_ += i_65_ * aHa_Sub2_5768.anInt4084;
				i_56_ += i_60_ * i_65_;
			}
			if (i + i_49_ > aHa_Sub2_5768.anInt4104) {
				int i_66_ = i + i_49_ - aHa_Sub2_5768.anInt4104;
				i_49_ -= i_66_;
				i_64_ += i_66_;
			}
			if (i < aHa_Sub2_5768.anInt4079) {
				int i_67_ = aHa_Sub2_5768.anInt4079 - i;
				i_49_ -= i_67_;
				i_63_ += i_67_;
				i_55_ += i_59_ * i_67_;
				i_64_ += i_67_;
			}
			float[] fs = aHa_Sub2_5768.aFloatArray4074;
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_53_ == 0) {
				if (i_51_ == 1) {
					int i_68_ = i_55_;
					for (int i_69_ = -i_50_; i_69_ < 0; i_69_++) {
						int i_70_ = (i_56_ >> 16) * anInt5758;
						for (int i_71_ = -i_49_; i_71_ < 0; i_71_++) {
							if ((float) i_48_ < fs[i_63_]) {
								is[i_63_] = anIntArray6690[(i_55_ >> 16) + i_70_];
								fs[i_63_] = (float) i_48_;
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_68_;
						i_63_ += i_64_;
					}
					return;
				}
				if (i_51_ == 0) {
					int i_72_ = (i_52_ & 0xff0000) >> 16;
					int i_73_ = (i_52_ & 0xff00) >> 8;
					int i_74_ = i_52_ & 0xff;
					int i_75_ = i_55_;
					for (int i_76_ = -i_50_; i_76_ < 0; i_76_++) {
						int i_77_ = (i_56_ >> 16) * anInt5758;
						for (int i_78_ = -i_49_; i_78_ < 0; i_78_++) {
							if ((float) i_48_ < fs[i_63_]) {
								int i_79_ = anIntArray6690[(i_55_ >> 16) + i_77_];
								int i_80_ = (i_79_ & 0xff0000) * i_72_ & ~0xffffff;
								int i_81_ = (i_79_ & 0xff00) * i_73_ & 0xff0000;
								int i_82_ = (i_79_ & 0xff) * i_74_ & 0xff00;
								is[i_63_] = (i_80_ | i_81_ | i_82_) >>> 8;
								fs[i_63_] = (float) i_48_;
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_75_;
						i_63_ += i_64_;
					}
					return;
				}
				if (i_51_ == 3) {
					int i_83_ = i_55_;
					for (int i_84_ = -i_50_; i_84_ < 0; i_84_++) {
						int i_85_ = (i_56_ >> 16) * anInt5758;
						for (int i_86_ = -i_49_; i_86_ < 0; i_86_++) {
							if ((float) i_48_ < fs[i_63_]) {
								int i_87_ = anIntArray6690[(i_55_ >> 16) + i_85_];
								int i_88_ = i_87_ + i_52_;
								int i_89_ = (i_87_ & 0xff00ff) + (i_52_ & 0xff00ff);
								int i_90_ = ((i_89_ & 0x1000100) + (i_88_ - i_89_ & 0x10000));
								is[i_63_] = i_88_ - i_90_ | i_90_ - (i_90_ >>> 8);
								fs[i_63_] = (float) i_48_;
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_83_;
						i_63_ += i_64_;
					}
					return;
				}
				if (i_51_ == 2) {
					int i_91_ = i_52_ >>> 24;
					int i_92_ = 256 - i_91_;
					int i_93_ = (i_52_ & 0xff00ff) * i_92_ & ~0xff00ff;
					int i_94_ = (i_52_ & 0xff00) * i_92_ & 0xff0000;
					i_52_ = (i_93_ | i_94_) >>> 8;
					int i_95_ = i_55_;
					for (int i_96_ = -i_50_; i_96_ < 0; i_96_++) {
						int i_97_ = (i_56_ >> 16) * anInt5758;
						for (int i_98_ = -i_49_; i_98_ < 0; i_98_++) {
							if ((float) i_48_ < fs[i_63_]) {
								int i_99_ = anIntArray6690[(i_55_ >> 16) + i_97_];
								i_93_ = (i_99_ & 0xff00ff) * i_91_ & ~0xff00ff;
								i_94_ = (i_99_ & 0xff00) * i_91_ & 0xff0000;
								is[i_63_] = ((i_93_ | i_94_) >>> 8) + i_52_;
								fs[i_63_] = (float) i_48_;
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_95_;
						i_63_ += i_64_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_53_ == 1) {
				if (i_51_ == 1) {
					int i_100_ = i_55_;
					for (int i_101_ = -i_50_; i_101_ < 0; i_101_++) {
						int i_102_ = (i_56_ >> 16) * anInt5758;
						for (int i_103_ = -i_49_; i_103_ < 0; i_103_++) {
							if ((float) i_48_ < fs[i_63_]) {
								int i_104_ = anIntArray6690[(i_55_ >> 16) + i_102_];
								int i_105_ = i_104_ >>> 24;
								int i_106_ = 256 - i_105_;
								int i_107_ = is[i_63_];
								is[i_63_] = (((((i_104_ & 0xff00ff) * i_105_ + (i_107_ & 0xff00ff) * i_106_)
										& ~0xff00ff) >> 8)
										+ (((((i_104_ & ~0xff00ff) >>> 8) * i_105_)
												+ ((i_107_ & ~0xff00ff) >>> 8) * i_106_) & ~0xff00ff));
								fs[i_63_] = (float) i_48_;
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_100_;
						i_63_ += i_64_;
					}
					return;
				}
				if (i_51_ == 0) {
					int i_108_ = i_55_;
					if ((i_52_ & 0xffffff) == 16777215) {
						for (int i_109_ = -i_50_; i_109_ < 0; i_109_++) {
							int i_110_ = (i_56_ >> 16) * anInt5758;
							for (int i_111_ = -i_49_; i_111_ < 0; i_111_++) {
								if ((float) i_48_ < fs[i_63_]) {
									int i_112_ = (anIntArray6690[(i_55_ >> 16) + i_110_]);
									int i_113_ = ((i_112_ >>> 24) * (i_52_ >>> 24) >> 8);
									int i_114_ = 256 - i_113_;
									int i_115_ = is[i_63_];
									is[i_63_] = ((((i_112_ & 0xff00ff) * i_113_ + (i_115_ & 0xff00ff) * i_114_)
											& ~0xff00ff)
											+ (((i_112_ & 0xff00) * i_113_ + (i_115_ & 0xff00) * i_114_)
													& 0xff0000)) >> 8;
									fs[i_63_] = (float) i_48_;
								}
								i_55_ += i_59_;
								i_63_++;
							}
							i_56_ += i_60_;
							i_55_ = i_108_;
							i_63_ += i_64_;
						}
					} else {
						int i_116_ = (i_52_ & 0xff0000) >> 16;
						int i_117_ = (i_52_ & 0xff00) >> 8;
						int i_118_ = i_52_ & 0xff;
						for (int i_119_ = -i_50_; i_119_ < 0; i_119_++) {
							int i_120_ = (i_56_ >> 16) * anInt5758;
							for (int i_121_ = -i_49_; i_121_ < 0; i_121_++) {
								if ((float) i_48_ < fs[i_63_]) {
									int i_122_ = (anIntArray6690[(i_55_ >> 16) + i_120_]);
									int i_123_ = ((i_122_ >>> 24) * (i_52_ >>> 24) >> 8);
									int i_124_ = 256 - i_123_;
									if (i_123_ != 255) {
										int i_125_ = ((i_122_ & 0xff0000) * i_116_ & ~0xffffff);
										int i_126_ = ((i_122_ & 0xff00) * i_117_ & 0xff0000);
										int i_127_ = ((i_122_ & 0xff) * i_118_ & 0xff00);
										i_122_ = (i_125_ | i_126_ | i_127_) >>> 8;
										int i_128_ = is[i_63_];
										is[i_63_] = ((((i_122_ & 0xff00ff) * i_123_ + ((i_128_ & 0xff00ff) * i_124_))
												& ~0xff00ff)
												+ (((i_122_ & 0xff00) * i_123_ + ((i_128_ & 0xff00) * i_124_))
														& 0xff0000)) >> 8;
										fs[i_63_] = (float) i_48_;
									} else {
										int i_129_ = ((i_122_ & 0xff0000) * i_116_ & ~0xffffff);
										int i_130_ = ((i_122_ & 0xff00) * i_117_ & 0xff0000);
										int i_131_ = ((i_122_ & 0xff) * i_118_ & 0xff00);
										is[i_63_] = (i_129_ | i_130_ | i_131_) >>> 8;
										fs[i_63_] = (float) i_48_;
									}
								}
								i_55_ += i_59_;
								i_63_++;
							}
							i_56_ += i_60_;
							i_55_ = i_108_;
							i_63_ += i_64_;
						}
						return;
					}
					return;
				}
				if (i_51_ == 3) {
					int i_132_ = i_55_;
					for (int i_133_ = -i_50_; i_133_ < 0; i_133_++) {
						int i_134_ = (i_56_ >> 16) * anInt5758;
						for (int i_135_ = -i_49_; i_135_ < 0; i_135_++) {
							if ((float) i_48_ < fs[i_63_]) {
								int i_136_ = anIntArray6690[(i_55_ >> 16) + i_134_];
								int i_137_ = i_136_ + i_52_;
								int i_138_ = (i_136_ & 0xff00ff) + (i_52_ & 0xff00ff);
								int i_139_ = ((i_138_ & 0x1000100) + (i_137_ - i_138_ & 0x10000));
								i_139_ = i_137_ - i_139_ | i_139_ - (i_139_ >>> 8);
								int i_140_ = (i_139_ >>> 24) * (i_52_ >>> 24) >> 8;
								int i_141_ = 256 - i_140_;
								if (i_140_ != 255) {
									i_136_ = i_139_;
									i_139_ = is[i_63_];
									i_139_ = ((((i_136_ & 0xff00ff) * i_140_ + (i_139_ & 0xff00ff) * i_141_)
											& ~0xff00ff)
											+ (((i_136_ & 0xff00) * i_140_ + (i_139_ & 0xff00) * i_141_)
													& 0xff0000)) >> 8;
								}
								is[i_63_] = i_139_;
								fs[i_63_] = (float) i_48_;
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_132_;
						i_63_ += i_64_;
					}
					return;
				}
				if (i_51_ == 2) {
					int i_142_ = i_52_ >>> 24;
					int i_143_ = 256 - i_142_;
					int i_144_ = (i_52_ & 0xff00ff) * i_143_ & ~0xff00ff;
					int i_145_ = (i_52_ & 0xff00) * i_143_ & 0xff0000;
					i_52_ = (i_144_ | i_145_) >>> 8;
					int i_146_ = i_55_;
					for (int i_147_ = -i_50_; i_147_ < 0; i_147_++) {
						int i_148_ = (i_56_ >> 16) * anInt5758;
						for (int i_149_ = -i_49_; i_149_ < 0; i_149_++) {
							if ((float) i_48_ < fs[i_63_]) {
								int i_150_ = anIntArray6690[(i_55_ >> 16) + i_148_];
								int i_151_ = i_150_ >>> 24;
								int i_152_ = 256 - i_151_;
								i_144_ = (i_150_ & 0xff00ff) * i_142_ & ~0xff00ff;
								i_145_ = (i_150_ & 0xff00) * i_142_ & 0xff0000;
								i_150_ = ((i_144_ | i_145_) >>> 8) + i_52_;
								int i_153_ = is[i_63_];
								is[i_63_] = ((((i_150_ & 0xff00ff) * i_151_ + (i_153_ & 0xff00ff) * i_152_) & ~0xff00ff)
										+ (((i_150_ & 0xff00) * i_151_ + (i_153_ & 0xff00) * i_152_) & 0xff0000)) >> 8;
								fs[i_63_] = (float) i_48_;
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_146_;
						i_63_ += i_64_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_53_ == 2) {
				if (i_51_ == 1) {
					int i_154_ = i_55_;
					for (int i_155_ = -i_50_; i_155_ < 0; i_155_++) {
						int i_156_ = (i_56_ >> 16) * anInt5758;
						for (int i_157_ = -i_49_; i_157_ < 0; i_157_++) {
							if ((float) i_48_ < fs[i_63_]) {
								int i_158_ = anIntArray6690[(i_55_ >> 16) + i_156_];
								if (i_158_ != 0) {
									int i_159_ = is[i_63_];
									int i_160_ = i_158_ + i_159_;
									int i_161_ = ((i_158_ & 0xff00ff) + (i_159_ & 0xff00ff));
									i_159_ = ((i_161_ & 0x1000100) + (i_160_ - i_161_ & 0x10000));
									is[i_63_] = i_160_ - i_159_ | i_159_ - (i_159_ >>> 8);
									fs[i_63_] = (float) i_48_;
								}
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_154_;
						i_63_ += i_64_;
					}
					return;
				}
				if (i_51_ == 0) {
					int i_162_ = i_55_;
					int i_163_ = (i_52_ & 0xff0000) >> 16;
					int i_164_ = (i_52_ & 0xff00) >> 8;
					int i_165_ = i_52_ & 0xff;
					for (int i_166_ = -i_50_; i_166_ < 0; i_166_++) {
						int i_167_ = (i_56_ >> 16) * anInt5758;
						for (int i_168_ = -i_49_; i_168_ < 0; i_168_++) {
							if ((float) i_48_ < fs[i_63_]) {
								int i_169_ = anIntArray6690[(i_55_ >> 16) + i_167_];
								if (i_169_ != 0) {
									int i_170_ = ((i_169_ & 0xff0000) * i_163_ & ~0xffffff);
									int i_171_ = ((i_169_ & 0xff00) * i_164_ & 0xff0000);
									int i_172_ = (i_169_ & 0xff) * i_165_ & 0xff00;
									i_169_ = (i_170_ | i_171_ | i_172_) >>> 8;
									int i_173_ = is[i_63_];
									int i_174_ = i_169_ + i_173_;
									int i_175_ = ((i_169_ & 0xff00ff) + (i_173_ & 0xff00ff));
									i_173_ = ((i_175_ & 0x1000100) + (i_174_ - i_175_ & 0x10000));
									is[i_63_] = i_174_ - i_173_ | i_173_ - (i_173_ >>> 8);
									fs[i_63_] = (float) i_48_;
								}
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_162_;
						i_63_ += i_64_;
					}
					return;
				}
				if (i_51_ == 3) {
					int i_176_ = i_55_;
					for (int i_177_ = -i_50_; i_177_ < 0; i_177_++) {
						int i_178_ = (i_56_ >> 16) * anInt5758;
						for (int i_179_ = -i_49_; i_179_ < 0; i_179_++) {
							if ((float) i_48_ < fs[i_63_]) {
								int i_180_ = anIntArray6690[(i_55_ >> 16) + i_178_];
								int i_181_ = i_180_ + i_52_;
								int i_182_ = (i_180_ & 0xff00ff) + (i_52_ & 0xff00ff);
								int i_183_ = ((i_182_ & 0x1000100) + (i_181_ - i_182_ & 0x10000));
								i_180_ = i_181_ - i_183_ | i_183_ - (i_183_ >>> 8);
								i_183_ = is[i_63_];
								i_181_ = i_180_ + i_183_;
								i_182_ = (i_180_ & 0xff00ff) + (i_183_ & 0xff00ff);
								i_183_ = (i_182_ & 0x1000100) + (i_181_ - i_182_ & 0x10000);
								is[i_63_] = i_181_ - i_183_ | i_183_ - (i_183_ >>> 8);
								fs[i_63_] = (float) i_48_;
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_176_;
						i_63_ += i_64_;
					}
					return;
				}
				if (i_51_ == 2) {
					int i_184_ = i_52_ >>> 24;
					int i_185_ = 256 - i_184_;
					int i_186_ = (i_52_ & 0xff00ff) * i_185_ & ~0xff00ff;
					int i_187_ = (i_52_ & 0xff00) * i_185_ & 0xff0000;
					i_52_ = (i_186_ | i_187_) >>> 8;
					int i_188_ = i_55_;
					for (int i_189_ = -i_50_; i_189_ < 0; i_189_++) {
						int i_190_ = (i_56_ >> 16) * anInt5758;
						for (int i_191_ = -i_49_; i_191_ < 0; i_191_++) {
							if ((float) i_48_ < fs[i_63_]) {
								int i_192_ = anIntArray6690[(i_55_ >> 16) + i_190_];
								if (i_192_ != 0) {
									i_186_ = ((i_192_ & 0xff00ff) * i_184_ & ~0xff00ff);
									i_187_ = ((i_192_ & 0xff00) * i_184_ & 0xff0000);
									i_192_ = ((i_186_ | i_187_) >>> 8) + i_52_;
									int i_193_ = is[i_63_];
									int i_194_ = i_192_ + i_193_;
									int i_195_ = ((i_192_ & 0xff00ff) + (i_193_ & 0xff00ff));
									i_193_ = ((i_195_ & 0x1000100) + (i_194_ - i_195_ & 0x10000));
									is[i_63_] = i_194_ - i_193_ | i_193_ - (i_193_ >>> 8);
									fs[i_63_] = (float) i_48_;
								}
							}
							i_55_ += i_59_;
							i_63_++;
						}
						i_56_ += i_60_;
						i_55_ = i_188_;
						i_63_ += i_64_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	Class397_Sub1_Sub1(ha_Sub2 var_ha_Sub2, int[] is, int i, int i_196_, int i_197_, int i_198_, boolean bool) {
		super(var_ha_Sub2, i_197_, i_198_);
		if (bool)
			anIntArray6690 = new int[i_197_ * i_198_];
		else
			anIntArray6690 = is;
		i_196_ -= anInt5758;
		int i_199_ = 0;
		for (int i_200_ = 0; i_200_ < i_198_; i_200_++) {
			for (int i_201_ = 0; i_201_ < i_197_; i_201_++)
				anIntArray6690[i_199_++] = is[i++];
			i += i_196_;
		}
	}

	public void method4104(int i, int i_202_, int i_203_, int i_204_, int i_205_, int i_206_, int i_207_, int i_208_,
			int i_209_) {
		if (i_204_ > 0 && i_205_ > 0) {
			int i_210_ = 0;
			int i_211_ = 0;
			int i_212_ = anInt5772 + anInt5758 + anInt5757;
			int i_213_ = anInt5761 + anInt5756 + anInt5750;
			int i_214_ = (i_212_ << 16) / i_204_;
			int i_215_ = (i_213_ << 16) / i_205_;
			if (anInt5772 > 0) {
				int i_216_ = ((anInt5772 << 16) + i_214_ - 1) / i_214_;
				i += i_216_;
				i_210_ += i_216_ * i_214_ - (anInt5772 << 16);
			}
			if (anInt5761 > 0) {
				int i_217_ = ((anInt5761 << 16) + i_215_ - 1) / i_215_;
				i_202_ += i_217_;
				i_211_ += i_217_ * i_215_ - (anInt5761 << 16);
			}
			if (anInt5758 < i_212_)
				i_204_ = ((anInt5758 << 16) - i_210_ + i_214_ - 1) / i_214_;
			if (anInt5756 < i_213_)
				i_205_ = ((anInt5756 << 16) - i_211_ + i_215_ - 1) / i_215_;
			int i_218_ = i + i_202_ * aHa_Sub2_5768.anInt4084;
			int i_219_ = aHa_Sub2_5768.anInt4084 - i_204_;
			if (i_202_ + i_205_ > aHa_Sub2_5768.anInt4078)
				i_205_ -= i_202_ + i_205_ - aHa_Sub2_5768.anInt4078;
			if (i_202_ < aHa_Sub2_5768.anInt4085) {
				int i_220_ = aHa_Sub2_5768.anInt4085 - i_202_;
				i_205_ -= i_220_;
				i_218_ += i_220_ * aHa_Sub2_5768.anInt4084;
				i_211_ += i_215_ * i_220_;
			}
			if (i + i_204_ > aHa_Sub2_5768.anInt4104) {
				int i_221_ = i + i_204_ - aHa_Sub2_5768.anInt4104;
				i_204_ -= i_221_;
				i_219_ += i_221_;
			}
			if (i < aHa_Sub2_5768.anInt4079) {
				int i_222_ = aHa_Sub2_5768.anInt4079 - i;
				i_204_ -= i_222_;
				i_218_ += i_222_;
				i_210_ += i_214_ * i_222_;
				i_219_ += i_222_;
			}
			float[] fs = aHa_Sub2_5768.aFloatArray4074;
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_208_ == 0) {
				if (i_206_ == 1) {
					int i_223_ = i_210_;
					for (int i_224_ = -i_205_; i_224_ < 0; i_224_++) {
						int i_225_ = (i_211_ >> 16) * anInt5758;
						for (int i_226_ = -i_204_; i_226_ < 0; i_226_++) {
							if ((float) i_203_ < fs[i_218_]) {
								is[i_218_] = anIntArray6690[(i_210_ >> 16) + i_225_];
								fs[i_218_] = (float) i_203_;
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_223_;
						i_218_ += i_219_;
					}
					return;
				}
				if (i_206_ == 0) {
					int i_227_ = (i_207_ & 0xff0000) >> 16;
					int i_228_ = (i_207_ & 0xff00) >> 8;
					int i_229_ = i_207_ & 0xff;
					int i_230_ = i_210_;
					for (int i_231_ = -i_205_; i_231_ < 0; i_231_++) {
						int i_232_ = (i_211_ >> 16) * anInt5758;
						for (int i_233_ = -i_204_; i_233_ < 0; i_233_++) {
							if ((float) i_203_ < fs[i_218_]) {
								int i_234_ = anIntArray6690[(i_210_ >> 16) + i_232_];
								int i_235_ = (i_234_ & 0xff0000) * i_227_ & ~0xffffff;
								int i_236_ = (i_234_ & 0xff00) * i_228_ & 0xff0000;
								int i_237_ = (i_234_ & 0xff) * i_229_ & 0xff00;
								is[i_218_] = (i_235_ | i_236_ | i_237_) >>> 8;
								fs[i_218_] = (float) i_203_;
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_230_;
						i_218_ += i_219_;
					}
					return;
				}
				if (i_206_ == 3) {
					int i_238_ = i_210_;
					for (int i_239_ = -i_205_; i_239_ < 0; i_239_++) {
						int i_240_ = (i_211_ >> 16) * anInt5758;
						for (int i_241_ = -i_204_; i_241_ < 0; i_241_++) {
							if ((float) i_203_ < fs[i_218_]) {
								int i_242_ = anIntArray6690[(i_210_ >> 16) + i_240_];
								int i_243_ = i_242_ + i_207_;
								int i_244_ = ((i_242_ & 0xff00ff) + (i_207_ & 0xff00ff));
								int i_245_ = ((i_244_ & 0x1000100) + (i_243_ - i_244_ & 0x10000));
								is[i_218_] = i_243_ - i_245_ | i_245_ - (i_245_ >>> 8);
								fs[i_218_] = (float) i_203_;
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_238_;
						i_218_ += i_219_;
					}
					return;
				}
				if (i_206_ == 2) {
					int i_246_ = i_207_ >>> 24;
					int i_247_ = 256 - i_246_;
					int i_248_ = (i_207_ & 0xff00ff) * i_247_ & ~0xff00ff;
					int i_249_ = (i_207_ & 0xff00) * i_247_ & 0xff0000;
					i_207_ = (i_248_ | i_249_) >>> 8;
					int i_250_ = i_210_;
					for (int i_251_ = -i_205_; i_251_ < 0; i_251_++) {
						int i_252_ = (i_211_ >> 16) * anInt5758;
						for (int i_253_ = -i_204_; i_253_ < 0; i_253_++) {
							if ((float) i_203_ < fs[i_218_]) {
								int i_254_ = anIntArray6690[(i_210_ >> 16) + i_252_];
								i_248_ = (i_254_ & 0xff00ff) * i_246_ & ~0xff00ff;
								i_249_ = (i_254_ & 0xff00) * i_246_ & 0xff0000;
								is[i_218_] = ((i_248_ | i_249_) >>> 8) + i_207_;
								fs[i_218_] = (float) i_203_;
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_250_;
						i_218_ += i_219_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_208_ == 1) {
				if (i_206_ == 1) {
					int i_255_ = i_210_;
					for (int i_256_ = -i_205_; i_256_ < 0; i_256_++) {
						int i_257_ = (i_211_ >> 16) * anInt5758;
						for (int i_258_ = -i_204_; i_258_ < 0; i_258_++) {
							if ((float) i_203_ < fs[i_218_]) {
								int i_259_ = anIntArray6690[(i_210_ >> 16) + i_257_];
								int i_260_ = i_259_ >>> 24;
								int i_261_ = 256 - i_260_;
								int i_262_ = is[i_218_];
								is[i_218_] = (((((i_259_ & 0xff00ff) * i_260_ + (i_262_ & 0xff00ff) * i_261_)
										& ~0xff00ff) >> 8)
										+ (((((i_259_ & ~0xff00ff) >>> 8) * i_260_)
												+ (((i_262_ & ~0xff00ff) >>> 8) * i_261_)) & ~0xff00ff));
								fs[i_218_] = (float) i_203_;
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_255_;
						i_218_ += i_219_;
					}
					return;
				}
				if (i_206_ == 0) {
					int i_263_ = i_210_;
					if ((i_207_ & 0xffffff) == 16777215) {
						for (int i_264_ = -i_205_; i_264_ < 0; i_264_++) {
							int i_265_ = (i_211_ >> 16) * anInt5758;
							for (int i_266_ = -i_204_; i_266_ < 0; i_266_++) {
								if ((float) i_203_ < fs[i_218_]) {
									int i_267_ = (anIntArray6690[(i_210_ >> 16) + i_265_]);
									int i_268_ = ((i_267_ >>> 24) * (i_207_ >>> 24) >> 8);
									int i_269_ = 256 - i_268_;
									int i_270_ = is[i_218_];
									is[i_218_] = ((((i_267_ & 0xff00ff) * i_268_ + (i_270_ & 0xff00ff) * i_269_)
											& ~0xff00ff)
											+ (((i_267_ & 0xff00) * i_268_ + (i_270_ & 0xff00) * i_269_)
													& 0xff0000)) >> 8;
									fs[i_218_] = (float) i_203_;
								}
								i_210_ += i_214_;
								i_218_++;
							}
							i_211_ += i_215_;
							i_210_ = i_263_;
							i_218_ += i_219_;
						}
					} else {
						int i_271_ = (i_207_ & 0xff0000) >> 16;
						int i_272_ = (i_207_ & 0xff00) >> 8;
						int i_273_ = i_207_ & 0xff;
						for (int i_274_ = -i_205_; i_274_ < 0; i_274_++) {
							int i_275_ = (i_211_ >> 16) * anInt5758;
							for (int i_276_ = -i_204_; i_276_ < 0; i_276_++) {
								if ((float) i_203_ < fs[i_218_]) {
									int i_277_ = (anIntArray6690[(i_210_ >> 16) + i_275_]);
									int i_278_ = ((i_277_ >>> 24) * (i_207_ >>> 24) >> 8);
									int i_279_ = 256 - i_278_;
									if (i_278_ != 255) {
										int i_280_ = ((i_277_ & 0xff0000) * i_271_ & ~0xffffff);
										int i_281_ = ((i_277_ & 0xff00) * i_272_ & 0xff0000);
										int i_282_ = ((i_277_ & 0xff) * i_273_ & 0xff00);
										i_277_ = (i_280_ | i_281_ | i_282_) >>> 8;
										int i_283_ = is[i_218_];
										is[i_218_] = ((((i_277_ & 0xff00ff) * i_278_ + ((i_283_ & 0xff00ff) * i_279_))
												& ~0xff00ff)
												+ (((i_277_ & 0xff00) * i_278_ + ((i_283_ & 0xff00) * i_279_))
														& 0xff0000)) >> 8;
										fs[i_218_] = (float) i_203_;
										int i_284_ = (i_283_ >>> 24) + i_278_;
										if (i_284_ > 255)
											i_284_ = 255;
										is[i_218_] |= i_284_ << 24;
									} else {
										int i_285_ = ((i_277_ & 0xff0000) * i_271_ & ~0xffffff);
										int i_286_ = ((i_277_ & 0xff00) * i_272_ & 0xff0000);
										int i_287_ = ((i_277_ & 0xff) * i_273_ & 0xff00);
										is[i_218_] = (i_285_ | i_286_ | i_287_) >>> 8;
										fs[i_218_] = (float) i_203_;
									}
								}
								i_210_ += i_214_;
								i_218_++;
							}
							i_211_ += i_215_;
							i_210_ = i_263_;
							i_218_ += i_219_;
						}
						return;
					}
					return;
				}
				if (i_206_ == 3) {
					int i_288_ = i_210_;
					for (int i_289_ = -i_205_; i_289_ < 0; i_289_++) {
						int i_290_ = (i_211_ >> 16) * anInt5758;
						for (int i_291_ = -i_204_; i_291_ < 0; i_291_++) {
							if ((float) i_203_ < fs[i_218_]) {
								int i_292_ = anIntArray6690[(i_210_ >> 16) + i_290_];
								int i_293_ = i_292_ + i_207_;
								int i_294_ = ((i_292_ & 0xff00ff) + (i_207_ & 0xff00ff));
								int i_295_ = ((i_294_ & 0x1000100) + (i_293_ - i_294_ & 0x10000));
								i_295_ = i_293_ - i_295_ | i_295_ - (i_295_ >>> 8);
								int i_296_ = (i_295_ >>> 24) * (i_207_ >>> 24) >> 8;
								int i_297_ = 256 - i_296_;
								if (i_296_ != 255) {
									i_292_ = i_295_;
									i_295_ = is[i_218_];
									i_295_ = ((((i_292_ & 0xff00ff) * i_296_ + (i_295_ & 0xff00ff) * i_297_)
											& ~0xff00ff)
											+ (((i_292_ & 0xff00) * i_296_ + (i_295_ & 0xff00) * i_297_)
													& 0xff0000)) >> 8;
								}
								is[i_218_] = i_295_;
								fs[i_218_] = (float) i_203_;
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_288_;
						i_218_ += i_219_;
					}
					return;
				}
				if (i_206_ == 2) {
					int i_298_ = i_207_ >>> 24;
					int i_299_ = 256 - i_298_;
					int i_300_ = (i_207_ & 0xff00ff) * i_299_ & ~0xff00ff;
					int i_301_ = (i_207_ & 0xff00) * i_299_ & 0xff0000;
					i_207_ = (i_300_ | i_301_) >>> 8;
					int i_302_ = i_210_;
					for (int i_303_ = -i_205_; i_303_ < 0; i_303_++) {
						int i_304_ = (i_211_ >> 16) * anInt5758;
						for (int i_305_ = -i_204_; i_305_ < 0; i_305_++) {
							if ((float) i_203_ < fs[i_218_]) {
								int i_306_ = anIntArray6690[(i_210_ >> 16) + i_304_];
								int i_307_ = i_306_ >>> 24;
								int i_308_ = 256 - i_307_;
								i_300_ = (i_306_ & 0xff00ff) * i_298_ & ~0xff00ff;
								i_301_ = (i_306_ & 0xff00) * i_298_ & 0xff0000;
								i_306_ = ((i_300_ | i_301_) >>> 8) + i_207_;
								int i_309_ = is[i_218_];
								is[i_218_] = ((((i_306_ & 0xff00ff) * i_307_ + (i_309_ & 0xff00ff) * i_308_)
										& ~0xff00ff)
										+ (((i_306_ & 0xff00) * i_307_ + (i_309_ & 0xff00) * i_308_) & 0xff0000)) >> 8;
								fs[i_218_] = (float) i_203_;
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_302_;
						i_218_ += i_219_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_208_ == 2) {
				if (i_206_ == 1) {
					int i_310_ = i_210_;
					for (int i_311_ = -i_205_; i_311_ < 0; i_311_++) {
						int i_312_ = (i_211_ >> 16) * anInt5758;
						for (int i_313_ = -i_204_; i_313_ < 0; i_313_++) {
							if ((float) i_203_ < fs[i_218_]) {
								int i_314_ = anIntArray6690[(i_210_ >> 16) + i_312_];
								if (i_314_ != 0) {
									int i_315_ = is[i_218_];
									int i_316_ = i_314_ + i_315_;
									int i_317_ = ((i_314_ & 0xff00ff) + (i_315_ & 0xff00ff));
									i_315_ = ((i_317_ & 0x1000100) + (i_316_ - i_317_ & 0x10000));
									is[i_218_] = i_316_ - i_315_ | i_315_ - (i_315_ >>> 8);
									fs[i_218_] = (float) i_203_;
								}
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_310_;
						i_218_ += i_219_;
					}
					return;
				}
				if (i_206_ == 0) {
					int i_318_ = i_210_;
					int i_319_ = (i_207_ & 0xff0000) >> 16;
					int i_320_ = (i_207_ & 0xff00) >> 8;
					int i_321_ = i_207_ & 0xff;
					for (int i_322_ = -i_205_; i_322_ < 0; i_322_++) {
						int i_323_ = (i_211_ >> 16) * anInt5758;
						for (int i_324_ = -i_204_; i_324_ < 0; i_324_++) {
							if ((float) i_203_ < fs[i_218_]) {
								int i_325_ = anIntArray6690[(i_210_ >> 16) + i_323_];
								if (i_325_ != 0) {
									int i_326_ = ((i_325_ & 0xff0000) * i_319_ & ~0xffffff);
									int i_327_ = ((i_325_ & 0xff00) * i_320_ & 0xff0000);
									int i_328_ = (i_325_ & 0xff) * i_321_ & 0xff00;
									i_325_ = (i_326_ | i_327_ | i_328_) >>> 8;
									int i_329_ = is[i_218_];
									int i_330_ = i_325_ + i_329_;
									int i_331_ = ((i_325_ & 0xff00ff) + (i_329_ & 0xff00ff));
									i_329_ = ((i_331_ & 0x1000100) + (i_330_ - i_331_ & 0x10000));
									is[i_218_] = i_330_ - i_329_ | i_329_ - (i_329_ >>> 8);
									fs[i_218_] = (float) i_203_;
								}
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_318_;
						i_218_ += i_219_;
					}
					return;
				}
				if (i_206_ == 3) {
					int i_332_ = i_210_;
					for (int i_333_ = -i_205_; i_333_ < 0; i_333_++) {
						int i_334_ = (i_211_ >> 16) * anInt5758;
						for (int i_335_ = -i_204_; i_335_ < 0; i_335_++) {
							if ((float) i_203_ < fs[i_218_]) {
								int i_336_ = anIntArray6690[(i_210_ >> 16) + i_334_];
								int i_337_ = i_336_ + i_207_;
								int i_338_ = ((i_336_ & 0xff00ff) + (i_207_ & 0xff00ff));
								int i_339_ = ((i_338_ & 0x1000100) + (i_337_ - i_338_ & 0x10000));
								i_336_ = i_337_ - i_339_ | i_339_ - (i_339_ >>> 8);
								i_339_ = is[i_218_];
								i_337_ = i_336_ + i_339_;
								i_338_ = (i_336_ & 0xff00ff) + (i_339_ & 0xff00ff);
								i_339_ = (i_338_ & 0x1000100) + (i_337_ - i_338_ & 0x10000);
								is[i_218_] = i_337_ - i_339_ | i_339_ - (i_339_ >>> 8);
								fs[i_218_] = (float) i_203_;
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_332_;
						i_218_ += i_219_;
					}
					return;
				}
				if (i_206_ == 2) {
					int i_340_ = i_207_ >>> 24;
					int i_341_ = 256 - i_340_;
					int i_342_ = (i_207_ & 0xff00ff) * i_341_ & ~0xff00ff;
					int i_343_ = (i_207_ & 0xff00) * i_341_ & 0xff0000;
					i_207_ = (i_342_ | i_343_) >>> 8;
					int i_344_ = i_210_;
					for (int i_345_ = -i_205_; i_345_ < 0; i_345_++) {
						int i_346_ = (i_211_ >> 16) * anInt5758;
						for (int i_347_ = -i_204_; i_347_ < 0; i_347_++) {
							if ((float) i_203_ < fs[i_218_]) {
								int i_348_ = anIntArray6690[(i_210_ >> 16) + i_346_];
								if (i_348_ != 0) {
									i_342_ = ((i_348_ & 0xff00ff) * i_340_ & ~0xff00ff);
									i_343_ = ((i_348_ & 0xff00) * i_340_ & 0xff0000);
									i_348_ = ((i_342_ | i_343_) >>> 8) + i_207_;
									int i_349_ = is[i_218_];
									int i_350_ = i_348_ + i_349_;
									int i_351_ = ((i_348_ & 0xff00ff) + (i_349_ & 0xff00ff));
									i_349_ = ((i_351_ & 0x1000100) + (i_350_ - i_351_ & 0x10000));
									is[i_218_] = i_350_ - i_349_ | i_349_ - (i_349_ >>> 8);
									fs[i_218_] = (float) i_203_;
								}
							}
							i_210_ += i_214_;
							i_218_++;
						}
						i_211_ += i_215_;
						i_210_ = i_344_;
						i_218_ += i_219_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	public void method4101(int i, int i_352_) {
		int[] is = aHa_Sub2_5768.anIntArray4108;
		if (Class397_Sub1.anInt5766 == 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_353_ = Class397_Sub1.anInt5771;
				while (i_353_ < 0) {
					int i_354_ = Class397_Sub1.anInt5759;
					int i_355_ = Class397_Sub1.anInt5765;
					int i_356_ = Class397_Sub1.anInt5775;
					int i_357_ = Class397_Sub1.anInt5770;
					if (i_355_ >= 0 && i_356_ >= 0 && i_355_ - (anInt5758 << 12) < 0
							&& i_356_ - (anInt5756 << 12) < 0) {
						for (/**/; i_357_ < 0; i_357_++) {
							int i_358_ = (i_356_ >> 12) * anInt5758 + (i_355_ >> 12);
							int i_359_ = i_354_++;
							int[] is_360_ = is;
							int i_361_ = i;
							int i_362_ = i_352_;
							if (i_362_ == 0) {
								if (i_361_ == 1)
									is_360_[i_359_] = anIntArray6690[i_358_];
								else if (i_361_ == 0) {
									int i_363_ = anIntArray6690[i_358_++];
									int i_364_ = (((i_363_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_365_ = (((i_363_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_366_ = (((i_363_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_360_[i_359_] = (i_364_ | i_365_ | i_366_) >>> 8;
								} else if (i_361_ == 3) {
									int i_367_ = anIntArray6690[i_358_++];
									int i_368_ = Class397_Sub1.anInt5767;
									int i_369_ = i_367_ + i_368_;
									int i_370_ = ((i_367_ & 0xff00ff) + (i_368_ & 0xff00ff));
									int i_371_ = ((i_370_ & 0x1000100) + (i_369_ - i_370_ & 0x10000));
									is_360_[i_359_] = i_369_ - i_371_ | i_371_ - (i_371_ >>> 8);
								} else if (i_361_ == 2) {
									int i_372_ = anIntArray6690[i_358_];
									int i_373_ = (((i_372_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_374_ = (((i_372_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_360_[i_359_] = (((i_373_ | i_374_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_362_ == 1) {
								if (i_361_ == 1) {
									int i_375_ = anIntArray6690[i_358_];
									int i_376_ = i_375_ >>> 24;
									int i_377_ = 256 - i_376_;
									int i_378_ = is_360_[i_359_];
									is_360_[i_359_] = ((((i_375_ & 0xff00ff) * i_376_ + (i_378_ & 0xff00ff) * i_377_)
											& ~0xff00ff)
											+ (((i_375_ & 0xff00) * i_376_ + (i_378_ & 0xff00) * i_377_)
													& 0xff0000)) >> 8;
								} else if (i_361_ == 0) {
									int i_379_ = anIntArray6690[i_358_];
									int i_380_ = (((i_379_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
									int i_381_ = 256 - i_380_;
									if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
										int i_382_ = is_360_[i_359_];
										is_360_[i_359_] = ((((i_379_ & 0xff00ff) * i_380_
												+ ((i_382_ & 0xff00ff) * i_381_)) & ~0xff00ff)
												+ (((i_379_ & 0xff00) * i_380_ + ((i_382_ & 0xff00) * i_381_))
														& 0xff0000)) >> 8;
									} else if (i_380_ != 255) {
										int i_383_ = (((i_379_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_384_ = (((i_379_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_385_ = (((i_379_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_379_ = (i_383_ | i_384_ | i_385_) >>> 8;
										int i_386_ = is_360_[i_359_];
										is_360_[i_359_] = ((((i_379_ & 0xff00ff) * i_380_
												+ ((i_386_ & 0xff00ff) * i_381_)) & ~0xff00ff)
												+ (((i_379_ & 0xff00) * i_380_ + ((i_386_ & 0xff00) * i_381_))
														& 0xff0000)) >> 8;
									} else {
										int i_387_ = (((i_379_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_388_ = (((i_379_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_389_ = (((i_379_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										is_360_[i_359_] = (i_387_ | i_388_ | i_389_) >>> 8;
									}
								} else if (i_361_ == 3) {
									int i_390_ = anIntArray6690[i_358_];
									int i_391_ = Class397_Sub1.anInt5767;
									int i_392_ = i_390_ + i_391_;
									int i_393_ = ((i_390_ & 0xff00ff) + (i_391_ & 0xff00ff));
									int i_394_ = ((i_393_ & 0x1000100) + (i_392_ - i_393_ & 0x10000));
									i_394_ = i_392_ - i_394_ | i_394_ - (i_394_ >>> 8);
									int i_395_ = (((i_390_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
									int i_396_ = 256 - i_395_;
									if (i_395_ != 255) {
										i_390_ = i_394_;
										i_394_ = is_360_[i_359_];
										i_394_ = ((((i_390_ & 0xff00ff) * i_395_ + ((i_394_ & 0xff00ff) * i_396_))
												& ~0xff00ff)
												+ (((i_390_ & 0xff00) * i_395_ + ((i_394_ & 0xff00) * i_396_))
														& 0xff0000)) >> 8;
									}
									is_360_[i_359_] = i_394_;
								} else if (i_361_ == 2) {
									int i_397_ = anIntArray6690[i_358_];
									int i_398_ = i_397_ >>> 24;
									int i_399_ = 256 - i_398_;
									int i_400_ = (((i_397_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_401_ = (((i_397_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_397_ = (((i_400_ | i_401_) >>> 8) + Class397_Sub1.anInt5773);
									int i_402_ = is_360_[i_359_];
									is_360_[i_359_] = ((((i_397_ & 0xff00ff) * i_398_ + (i_402_ & 0xff00ff) * i_399_)
											& ~0xff00ff)
											+ (((i_397_ & 0xff00) * i_398_ + (i_402_ & 0xff00) * i_399_)
													& 0xff0000)) >> 8;
								} else
									throw new IllegalArgumentException();
							} else if (i_362_ == 2) {
								if (i_361_ == 1) {
									int i_403_ = anIntArray6690[i_358_];
									int i_404_ = is_360_[i_359_];
									int i_405_ = i_403_ + i_404_;
									int i_406_ = ((i_403_ & 0xff00ff) + (i_404_ & 0xff00ff));
									i_404_ = ((i_406_ & 0x1000100) + (i_405_ - i_406_ & 0x10000));
									is_360_[i_359_] = i_405_ - i_404_ | i_404_ - (i_404_ >>> 8);
								} else if (i_361_ == 0) {
									int i_407_ = anIntArray6690[i_358_];
									int i_408_ = (((i_407_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_409_ = (((i_407_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_410_ = (((i_407_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_407_ = (i_408_ | i_409_ | i_410_) >>> 8;
									int i_411_ = is_360_[i_359_];
									int i_412_ = i_407_ + i_411_;
									int i_413_ = ((i_407_ & 0xff00ff) + (i_411_ & 0xff00ff));
									i_411_ = ((i_413_ & 0x1000100) + (i_412_ - i_413_ & 0x10000));
									is_360_[i_359_] = i_412_ - i_411_ | i_411_ - (i_411_ >>> 8);
								} else if (i_361_ == 3) {
									int i_414_ = anIntArray6690[i_358_];
									int i_415_ = Class397_Sub1.anInt5767;
									int i_416_ = i_414_ + i_415_;
									int i_417_ = ((i_414_ & 0xff00ff) + (i_415_ & 0xff00ff));
									int i_418_ = ((i_417_ & 0x1000100) + (i_416_ - i_417_ & 0x10000));
									i_414_ = i_416_ - i_418_ | i_418_ - (i_418_ >>> 8);
									i_418_ = is_360_[i_359_];
									i_416_ = i_414_ + i_418_;
									i_417_ = (i_414_ & 0xff00ff) + (i_418_ & 0xff00ff);
									i_418_ = ((i_417_ & 0x1000100) + (i_416_ - i_417_ & 0x10000));
									is_360_[i_359_] = i_416_ - i_418_ | i_418_ - (i_418_ >>> 8);
								} else if (i_361_ == 2) {
									int i_419_ = anIntArray6690[i_358_];
									int i_420_ = (((i_419_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_421_ = (((i_419_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_419_ = (((i_420_ | i_421_) >>> 8) + Class397_Sub1.anInt5773);
									int i_422_ = is_360_[i_359_];
									int i_423_ = i_419_ + i_422_;
									int i_424_ = ((i_419_ & 0xff00ff) + (i_422_ & 0xff00ff));
									i_422_ = ((i_424_ & 0x1000100) + (i_423_ - i_424_ & 0x10000));
									is_360_[i_359_] = i_423_ - i_422_ | i_422_ - (i_422_ >>> 8);
								}
							} else
								throw new IllegalArgumentException();
						}
					}
					i_353_++;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_425_ = Class397_Sub1.anInt5771;
				while (i_425_ < 0) {
					int i_426_ = Class397_Sub1.anInt5759;
					int i_427_ = Class397_Sub1.anInt5765;
					int i_428_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_429_ = Class397_Sub1.anInt5770;
					if (i_427_ >= 0 && i_427_ - (anInt5758 << 12) < 0) {
						int i_430_;
						if ((i_430_ = i_428_ - (anInt5756 << 12)) >= 0) {
							i_430_ = ((Class397_Sub1.anInt5777 - i_430_) / Class397_Sub1.anInt5777);
							i_429_ += i_430_;
							i_428_ += Class397_Sub1.anInt5777 * i_430_;
							i_426_ += i_430_;
						}
						if ((i_430_ = ((i_428_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_429_)
							i_429_ = i_430_;
						for (/**/; i_429_ < 0; i_429_++) {
							int i_431_ = (i_428_ >> 12) * anInt5758 + (i_427_ >> 12);
							int i_432_ = i_426_++;
							int[] is_433_ = is;
							int i_434_ = i;
							int i_435_ = i_352_;
							if (i_435_ == 0) {
								if (i_434_ == 1)
									is_433_[i_432_] = anIntArray6690[i_431_];
								else if (i_434_ == 0) {
									int i_436_ = anIntArray6690[i_431_++];
									int i_437_ = (((i_436_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_438_ = (((i_436_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_439_ = (((i_436_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_433_[i_432_] = (i_437_ | i_438_ | i_439_) >>> 8;
								} else if (i_434_ == 3) {
									int i_440_ = anIntArray6690[i_431_++];
									int i_441_ = Class397_Sub1.anInt5767;
									int i_442_ = i_440_ + i_441_;
									int i_443_ = ((i_440_ & 0xff00ff) + (i_441_ & 0xff00ff));
									int i_444_ = ((i_443_ & 0x1000100) + (i_442_ - i_443_ & 0x10000));
									is_433_[i_432_] = i_442_ - i_444_ | i_444_ - (i_444_ >>> 8);
								} else if (i_434_ == 2) {
									int i_445_ = anIntArray6690[i_431_];
									int i_446_ = (((i_445_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_447_ = (((i_445_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_433_[i_432_] = (((i_446_ | i_447_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_435_ == 1) {
								if (i_434_ == 1) {
									int i_448_ = anIntArray6690[i_431_];
									int i_449_ = i_448_ >>> 24;
									int i_450_ = 256 - i_449_;
									int i_451_ = is_433_[i_432_];
									is_433_[i_432_] = ((((i_448_ & 0xff00ff) * i_449_ + (i_451_ & 0xff00ff) * i_450_)
											& ~0xff00ff)
											+ (((i_448_ & 0xff00) * i_449_ + (i_451_ & 0xff00) * i_450_)
													& 0xff0000)) >> 8;
								} else if (i_434_ == 0) {
									int i_452_ = anIntArray6690[i_431_];
									int i_453_ = (((i_452_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
									int i_454_ = 256 - i_453_;
									if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
										int i_455_ = is_433_[i_432_];
										is_433_[i_432_] = ((((i_452_ & 0xff00ff) * i_453_
												+ ((i_455_ & 0xff00ff) * i_454_)) & ~0xff00ff)
												+ (((i_452_ & 0xff00) * i_453_ + ((i_455_ & 0xff00) * i_454_))
														& 0xff0000)) >> 8;
									} else if (i_453_ != 255) {
										int i_456_ = (((i_452_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_457_ = (((i_452_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_458_ = (((i_452_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_452_ = (i_456_ | i_457_ | i_458_) >>> 8;
										int i_459_ = is_433_[i_432_];
										is_433_[i_432_] = ((((i_452_ & 0xff00ff) * i_453_
												+ ((i_459_ & 0xff00ff) * i_454_)) & ~0xff00ff)
												+ (((i_452_ & 0xff00) * i_453_ + ((i_459_ & 0xff00) * i_454_))
														& 0xff0000)) >> 8;
									} else {
										int i_460_ = (((i_452_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_461_ = (((i_452_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_462_ = (((i_452_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										is_433_[i_432_] = (i_460_ | i_461_ | i_462_) >>> 8;
									}
								} else if (i_434_ == 3) {
									int i_463_ = anIntArray6690[i_431_];
									int i_464_ = Class397_Sub1.anInt5767;
									int i_465_ = i_463_ + i_464_;
									int i_466_ = ((i_463_ & 0xff00ff) + (i_464_ & 0xff00ff));
									int i_467_ = ((i_466_ & 0x1000100) + (i_465_ - i_466_ & 0x10000));
									i_467_ = i_465_ - i_467_ | i_467_ - (i_467_ >>> 8);
									int i_468_ = (((i_463_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
									int i_469_ = 256 - i_468_;
									if (i_468_ != 255) {
										i_463_ = i_467_;
										i_467_ = is_433_[i_432_];
										i_467_ = ((((i_463_ & 0xff00ff) * i_468_ + ((i_467_ & 0xff00ff) * i_469_))
												& ~0xff00ff)
												+ (((i_463_ & 0xff00) * i_468_ + ((i_467_ & 0xff00) * i_469_))
														& 0xff0000)) >> 8;
									}
									is_433_[i_432_] = i_467_;
								} else if (i_434_ == 2) {
									int i_470_ = anIntArray6690[i_431_];
									int i_471_ = i_470_ >>> 24;
									int i_472_ = 256 - i_471_;
									int i_473_ = (((i_470_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_474_ = (((i_470_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_470_ = (((i_473_ | i_474_) >>> 8) + Class397_Sub1.anInt5773);
									int i_475_ = is_433_[i_432_];
									is_433_[i_432_] = ((((i_470_ & 0xff00ff) * i_471_ + (i_475_ & 0xff00ff) * i_472_)
											& ~0xff00ff)
											+ (((i_470_ & 0xff00) * i_471_ + (i_475_ & 0xff00) * i_472_)
													& 0xff0000)) >> 8;
								} else
									throw new IllegalArgumentException();
							} else if (i_435_ == 2) {
								if (i_434_ == 1) {
									int i_476_ = anIntArray6690[i_431_];
									int i_477_ = is_433_[i_432_];
									int i_478_ = i_476_ + i_477_;
									int i_479_ = ((i_476_ & 0xff00ff) + (i_477_ & 0xff00ff));
									i_477_ = ((i_479_ & 0x1000100) + (i_478_ - i_479_ & 0x10000));
									is_433_[i_432_] = i_478_ - i_477_ | i_477_ - (i_477_ >>> 8);
								} else if (i_434_ == 0) {
									int i_480_ = anIntArray6690[i_431_];
									int i_481_ = (((i_480_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_482_ = (((i_480_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_483_ = (((i_480_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_480_ = (i_481_ | i_482_ | i_483_) >>> 8;
									int i_484_ = is_433_[i_432_];
									int i_485_ = i_480_ + i_484_;
									int i_486_ = ((i_480_ & 0xff00ff) + (i_484_ & 0xff00ff));
									i_484_ = ((i_486_ & 0x1000100) + (i_485_ - i_486_ & 0x10000));
									is_433_[i_432_] = i_485_ - i_484_ | i_484_ - (i_484_ >>> 8);
								} else if (i_434_ == 3) {
									int i_487_ = anIntArray6690[i_431_];
									int i_488_ = Class397_Sub1.anInt5767;
									int i_489_ = i_487_ + i_488_;
									int i_490_ = ((i_487_ & 0xff00ff) + (i_488_ & 0xff00ff));
									int i_491_ = ((i_490_ & 0x1000100) + (i_489_ - i_490_ & 0x10000));
									i_487_ = i_489_ - i_491_ | i_491_ - (i_491_ >>> 8);
									i_491_ = is_433_[i_432_];
									i_489_ = i_487_ + i_491_;
									i_490_ = (i_487_ & 0xff00ff) + (i_491_ & 0xff00ff);
									i_491_ = ((i_490_ & 0x1000100) + (i_489_ - i_490_ & 0x10000));
									is_433_[i_432_] = i_489_ - i_491_ | i_491_ - (i_491_ >>> 8);
								} else if (i_434_ == 2) {
									int i_492_ = anIntArray6690[i_431_];
									int i_493_ = (((i_492_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_494_ = (((i_492_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_492_ = (((i_493_ | i_494_) >>> 8) + Class397_Sub1.anInt5773);
									int i_495_ = is_433_[i_432_];
									int i_496_ = i_492_ + i_495_;
									int i_497_ = ((i_492_ & 0xff00ff) + (i_495_ & 0xff00ff));
									i_495_ = ((i_497_ & 0x1000100) + (i_496_ - i_497_ & 0x10000));
									is_433_[i_432_] = i_496_ - i_495_ | i_495_ - (i_495_ >>> 8);
								}
							} else
								throw new IllegalArgumentException();
							i_428_ += Class397_Sub1.anInt5777;
						}
					}
					i_425_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_498_ = Class397_Sub1.anInt5771;
				while (i_498_ < 0) {
					int i_499_ = Class397_Sub1.anInt5759;
					int i_500_ = Class397_Sub1.anInt5765;
					int i_501_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_502_ = Class397_Sub1.anInt5770;
					if (i_500_ >= 0 && i_500_ - (anInt5758 << 12) < 0) {
						if (i_501_ < 0) {
							int i_503_ = ((Class397_Sub1.anInt5777 - 1 - i_501_) / Class397_Sub1.anInt5777);
							i_502_ += i_503_;
							i_501_ += Class397_Sub1.anInt5777 * i_503_;
							i_499_ += i_503_;
						}
						int i_504_;
						if ((i_504_ = ((i_501_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
								/ Class397_Sub1.anInt5777)) > i_502_)
							i_502_ = i_504_;
						for (/**/; i_502_ < 0; i_502_++) {
							int i_505_ = (i_501_ >> 12) * anInt5758 + (i_500_ >> 12);
							int i_506_ = i_499_++;
							int[] is_507_ = is;
							int i_508_ = i;
							int i_509_ = i_352_;
							if (i_509_ == 0) {
								if (i_508_ == 1)
									is_507_[i_506_] = anIntArray6690[i_505_];
								else if (i_508_ == 0) {
									int i_510_ = anIntArray6690[i_505_++];
									int i_511_ = (((i_510_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_512_ = (((i_510_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_513_ = (((i_510_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_507_[i_506_] = (i_511_ | i_512_ | i_513_) >>> 8;
								} else if (i_508_ == 3) {
									int i_514_ = anIntArray6690[i_505_++];
									int i_515_ = Class397_Sub1.anInt5767;
									int i_516_ = i_514_ + i_515_;
									int i_517_ = ((i_514_ & 0xff00ff) + (i_515_ & 0xff00ff));
									int i_518_ = ((i_517_ & 0x1000100) + (i_516_ - i_517_ & 0x10000));
									is_507_[i_506_] = i_516_ - i_518_ | i_518_ - (i_518_ >>> 8);
								} else if (i_508_ == 2) {
									int i_519_ = anIntArray6690[i_505_];
									int i_520_ = (((i_519_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_521_ = (((i_519_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_507_[i_506_] = (((i_520_ | i_521_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_509_ == 1) {
								if (i_508_ == 1) {
									int i_522_ = anIntArray6690[i_505_];
									int i_523_ = i_522_ >>> 24;
									int i_524_ = 256 - i_523_;
									int i_525_ = is_507_[i_506_];
									is_507_[i_506_] = ((((i_522_ & 0xff00ff) * i_523_ + (i_525_ & 0xff00ff) * i_524_)
											& ~0xff00ff)
											+ (((i_522_ & 0xff00) * i_523_ + (i_525_ & 0xff00) * i_524_)
													& 0xff0000)) >> 8;
								} else if (i_508_ == 0) {
									int i_526_ = anIntArray6690[i_505_];
									int i_527_ = (((i_526_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
									int i_528_ = 256 - i_527_;
									if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
										int i_529_ = is_507_[i_506_];
										is_507_[i_506_] = ((((i_526_ & 0xff00ff) * i_527_
												+ ((i_529_ & 0xff00ff) * i_528_)) & ~0xff00ff)
												+ (((i_526_ & 0xff00) * i_527_ + ((i_529_ & 0xff00) * i_528_))
														& 0xff0000)) >> 8;
									} else if (i_527_ != 255) {
										int i_530_ = (((i_526_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_531_ = (((i_526_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_532_ = (((i_526_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_526_ = (i_530_ | i_531_ | i_532_) >>> 8;
										int i_533_ = is_507_[i_506_];
										is_507_[i_506_] = ((((i_526_ & 0xff00ff) * i_527_
												+ ((i_533_ & 0xff00ff) * i_528_)) & ~0xff00ff)
												+ (((i_526_ & 0xff00) * i_527_ + ((i_533_ & 0xff00) * i_528_))
														& 0xff0000)) >> 8;
									} else {
										int i_534_ = (((i_526_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_535_ = (((i_526_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_536_ = (((i_526_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										is_507_[i_506_] = (i_534_ | i_535_ | i_536_) >>> 8;
									}
								} else if (i_508_ == 3) {
									int i_537_ = anIntArray6690[i_505_];
									int i_538_ = Class397_Sub1.anInt5767;
									int i_539_ = i_537_ + i_538_;
									int i_540_ = ((i_537_ & 0xff00ff) + (i_538_ & 0xff00ff));
									int i_541_ = ((i_540_ & 0x1000100) + (i_539_ - i_540_ & 0x10000));
									i_541_ = i_539_ - i_541_ | i_541_ - (i_541_ >>> 8);
									int i_542_ = (((i_537_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
									int i_543_ = 256 - i_542_;
									if (i_542_ != 255) {
										i_537_ = i_541_;
										i_541_ = is_507_[i_506_];
										i_541_ = ((((i_537_ & 0xff00ff) * i_542_ + ((i_541_ & 0xff00ff) * i_543_))
												& ~0xff00ff)
												+ (((i_537_ & 0xff00) * i_542_ + ((i_541_ & 0xff00) * i_543_))
														& 0xff0000)) >> 8;
									}
									is_507_[i_506_] = i_541_;
								} else if (i_508_ == 2) {
									int i_544_ = anIntArray6690[i_505_];
									int i_545_ = i_544_ >>> 24;
									int i_546_ = 256 - i_545_;
									int i_547_ = (((i_544_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_548_ = (((i_544_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_544_ = (((i_547_ | i_548_) >>> 8) + Class397_Sub1.anInt5773);
									int i_549_ = is_507_[i_506_];
									is_507_[i_506_] = ((((i_544_ & 0xff00ff) * i_545_ + (i_549_ & 0xff00ff) * i_546_)
											& ~0xff00ff)
											+ (((i_544_ & 0xff00) * i_545_ + (i_549_ & 0xff00) * i_546_)
													& 0xff0000)) >> 8;
								} else
									throw new IllegalArgumentException();
							} else if (i_509_ == 2) {
								if (i_508_ == 1) {
									int i_550_ = anIntArray6690[i_505_];
									int i_551_ = is_507_[i_506_];
									int i_552_ = i_550_ + i_551_;
									int i_553_ = ((i_550_ & 0xff00ff) + (i_551_ & 0xff00ff));
									i_551_ = ((i_553_ & 0x1000100) + (i_552_ - i_553_ & 0x10000));
									is_507_[i_506_] = i_552_ - i_551_ | i_551_ - (i_551_ >>> 8);
								} else if (i_508_ == 0) {
									int i_554_ = anIntArray6690[i_505_];
									int i_555_ = (((i_554_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_556_ = (((i_554_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_557_ = (((i_554_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_554_ = (i_555_ | i_556_ | i_557_) >>> 8;
									int i_558_ = is_507_[i_506_];
									int i_559_ = i_554_ + i_558_;
									int i_560_ = ((i_554_ & 0xff00ff) + (i_558_ & 0xff00ff));
									i_558_ = ((i_560_ & 0x1000100) + (i_559_ - i_560_ & 0x10000));
									is_507_[i_506_] = i_559_ - i_558_ | i_558_ - (i_558_ >>> 8);
								} else if (i_508_ == 3) {
									int i_561_ = anIntArray6690[i_505_];
									int i_562_ = Class397_Sub1.anInt5767;
									int i_563_ = i_561_ + i_562_;
									int i_564_ = ((i_561_ & 0xff00ff) + (i_562_ & 0xff00ff));
									int i_565_ = ((i_564_ & 0x1000100) + (i_563_ - i_564_ & 0x10000));
									i_561_ = i_563_ - i_565_ | i_565_ - (i_565_ >>> 8);
									i_565_ = is_507_[i_506_];
									i_563_ = i_561_ + i_565_;
									i_564_ = (i_561_ & 0xff00ff) + (i_565_ & 0xff00ff);
									i_565_ = ((i_564_ & 0x1000100) + (i_563_ - i_564_ & 0x10000));
									is_507_[i_506_] = i_563_ - i_565_ | i_565_ - (i_565_ >>> 8);
								} else if (i_508_ == 2) {
									int i_566_ = anIntArray6690[i_505_];
									int i_567_ = (((i_566_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_568_ = (((i_566_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_566_ = (((i_567_ | i_568_) >>> 8) + Class397_Sub1.anInt5773);
									int i_569_ = is_507_[i_506_];
									int i_570_ = i_566_ + i_569_;
									int i_571_ = ((i_566_ & 0xff00ff) + (i_569_ & 0xff00ff));
									i_569_ = ((i_571_ & 0x1000100) + (i_570_ - i_571_ & 0x10000));
									is_507_[i_506_] = i_570_ - i_569_ | i_569_ - (i_569_ >>> 8);
								}
							} else
								throw new IllegalArgumentException();
							i_501_ += Class397_Sub1.anInt5777;
						}
					}
					i_498_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5766 < 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_572_ = Class397_Sub1.anInt5771;
				while (i_572_ < 0) {
					int i_573_ = Class397_Sub1.anInt5759;
					int i_574_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_575_ = Class397_Sub1.anInt5775;
					int i_576_ = Class397_Sub1.anInt5770;
					if (i_575_ >= 0 && i_575_ - (anInt5756 << 12) < 0) {
						int i_577_;
						if ((i_577_ = i_574_ - (anInt5758 << 12)) >= 0) {
							i_577_ = ((Class397_Sub1.anInt5766 - i_577_) / Class397_Sub1.anInt5766);
							i_576_ += i_577_;
							i_574_ += Class397_Sub1.anInt5766 * i_577_;
							i_573_ += i_577_;
						}
						if ((i_577_ = ((i_574_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_576_)
							i_576_ = i_577_;
						for (/**/; i_576_ < 0; i_576_++) {
							int i_578_ = (i_575_ >> 12) * anInt5758 + (i_574_ >> 12);
							int i_579_ = i_573_++;
							int[] is_580_ = is;
							int i_581_ = i;
							int i_582_ = i_352_;
							if (i_582_ == 0) {
								if (i_581_ == 1)
									is_580_[i_579_] = anIntArray6690[i_578_];
								else if (i_581_ == 0) {
									int i_583_ = anIntArray6690[i_578_++];
									int i_584_ = (((i_583_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_585_ = (((i_583_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_586_ = (((i_583_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_580_[i_579_] = (i_584_ | i_585_ | i_586_) >>> 8;
								} else if (i_581_ == 3) {
									int i_587_ = anIntArray6690[i_578_++];
									int i_588_ = Class397_Sub1.anInt5767;
									int i_589_ = i_587_ + i_588_;
									int i_590_ = ((i_587_ & 0xff00ff) + (i_588_ & 0xff00ff));
									int i_591_ = ((i_590_ & 0x1000100) + (i_589_ - i_590_ & 0x10000));
									is_580_[i_579_] = i_589_ - i_591_ | i_591_ - (i_591_ >>> 8);
								} else if (i_581_ == 2) {
									int i_592_ = anIntArray6690[i_578_];
									int i_593_ = (((i_592_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_594_ = (((i_592_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_580_[i_579_] = (((i_593_ | i_594_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_582_ == 1) {
								if (i_581_ == 1) {
									int i_595_ = anIntArray6690[i_578_];
									int i_596_ = i_595_ >>> 24;
									int i_597_ = 256 - i_596_;
									int i_598_ = is_580_[i_579_];
									is_580_[i_579_] = ((((i_595_ & 0xff00ff) * i_596_ + (i_598_ & 0xff00ff) * i_597_)
											& ~0xff00ff)
											+ (((i_595_ & 0xff00) * i_596_ + (i_598_ & 0xff00) * i_597_)
													& 0xff0000)) >> 8;
								} else if (i_581_ == 0) {
									int i_599_ = anIntArray6690[i_578_];
									int i_600_ = (((i_599_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
									int i_601_ = 256 - i_600_;
									if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
										int i_602_ = is_580_[i_579_];
										is_580_[i_579_] = ((((i_599_ & 0xff00ff) * i_600_
												+ ((i_602_ & 0xff00ff) * i_601_)) & ~0xff00ff)
												+ (((i_599_ & 0xff00) * i_600_ + ((i_602_ & 0xff00) * i_601_))
														& 0xff0000)) >> 8;
									} else if (i_600_ != 255) {
										int i_603_ = (((i_599_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_604_ = (((i_599_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_605_ = (((i_599_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_599_ = (i_603_ | i_604_ | i_605_) >>> 8;
										int i_606_ = is_580_[i_579_];
										is_580_[i_579_] = ((((i_599_ & 0xff00ff) * i_600_
												+ ((i_606_ & 0xff00ff) * i_601_)) & ~0xff00ff)
												+ (((i_599_ & 0xff00) * i_600_ + ((i_606_ & 0xff00) * i_601_))
														& 0xff0000)) >> 8;
									} else {
										int i_607_ = (((i_599_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_608_ = (((i_599_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_609_ = (((i_599_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										is_580_[i_579_] = (i_607_ | i_608_ | i_609_) >>> 8;
									}
								} else if (i_581_ == 3) {
									int i_610_ = anIntArray6690[i_578_];
									int i_611_ = Class397_Sub1.anInt5767;
									int i_612_ = i_610_ + i_611_;
									int i_613_ = ((i_610_ & 0xff00ff) + (i_611_ & 0xff00ff));
									int i_614_ = ((i_613_ & 0x1000100) + (i_612_ - i_613_ & 0x10000));
									i_614_ = i_612_ - i_614_ | i_614_ - (i_614_ >>> 8);
									int i_615_ = (((i_610_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
									int i_616_ = 256 - i_615_;
									if (i_615_ != 255) {
										i_610_ = i_614_;
										i_614_ = is_580_[i_579_];
										i_614_ = ((((i_610_ & 0xff00ff) * i_615_ + ((i_614_ & 0xff00ff) * i_616_))
												& ~0xff00ff)
												+ (((i_610_ & 0xff00) * i_615_ + ((i_614_ & 0xff00) * i_616_))
														& 0xff0000)) >> 8;
									}
									is_580_[i_579_] = i_614_;
								} else if (i_581_ == 2) {
									int i_617_ = anIntArray6690[i_578_];
									int i_618_ = i_617_ >>> 24;
									int i_619_ = 256 - i_618_;
									int i_620_ = (((i_617_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_621_ = (((i_617_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_617_ = (((i_620_ | i_621_) >>> 8) + Class397_Sub1.anInt5773);
									int i_622_ = is_580_[i_579_];
									is_580_[i_579_] = ((((i_617_ & 0xff00ff) * i_618_ + (i_622_ & 0xff00ff) * i_619_)
											& ~0xff00ff)
											+ (((i_617_ & 0xff00) * i_618_ + (i_622_ & 0xff00) * i_619_)
													& 0xff0000)) >> 8;
								} else
									throw new IllegalArgumentException();
							} else if (i_582_ == 2) {
								if (i_581_ == 1) {
									int i_623_ = anIntArray6690[i_578_];
									int i_624_ = is_580_[i_579_];
									int i_625_ = i_623_ + i_624_;
									int i_626_ = ((i_623_ & 0xff00ff) + (i_624_ & 0xff00ff));
									i_624_ = ((i_626_ & 0x1000100) + (i_625_ - i_626_ & 0x10000));
									is_580_[i_579_] = i_625_ - i_624_ | i_624_ - (i_624_ >>> 8);
								} else if (i_581_ == 0) {
									int i_627_ = anIntArray6690[i_578_];
									int i_628_ = (((i_627_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_629_ = (((i_627_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_630_ = (((i_627_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_627_ = (i_628_ | i_629_ | i_630_) >>> 8;
									int i_631_ = is_580_[i_579_];
									int i_632_ = i_627_ + i_631_;
									int i_633_ = ((i_627_ & 0xff00ff) + (i_631_ & 0xff00ff));
									i_631_ = ((i_633_ & 0x1000100) + (i_632_ - i_633_ & 0x10000));
									is_580_[i_579_] = i_632_ - i_631_ | i_631_ - (i_631_ >>> 8);
								} else if (i_581_ == 3) {
									int i_634_ = anIntArray6690[i_578_];
									int i_635_ = Class397_Sub1.anInt5767;
									int i_636_ = i_634_ + i_635_;
									int i_637_ = ((i_634_ & 0xff00ff) + (i_635_ & 0xff00ff));
									int i_638_ = ((i_637_ & 0x1000100) + (i_636_ - i_637_ & 0x10000));
									i_634_ = i_636_ - i_638_ | i_638_ - (i_638_ >>> 8);
									i_638_ = is_580_[i_579_];
									i_636_ = i_634_ + i_638_;
									i_637_ = (i_634_ & 0xff00ff) + (i_638_ & 0xff00ff);
									i_638_ = ((i_637_ & 0x1000100) + (i_636_ - i_637_ & 0x10000));
									is_580_[i_579_] = i_636_ - i_638_ | i_638_ - (i_638_ >>> 8);
								} else if (i_581_ == 2) {
									int i_639_ = anIntArray6690[i_578_];
									int i_640_ = (((i_639_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_641_ = (((i_639_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_639_ = (((i_640_ | i_641_) >>> 8) + Class397_Sub1.anInt5773);
									int i_642_ = is_580_[i_579_];
									int i_643_ = i_639_ + i_642_;
									int i_644_ = ((i_639_ & 0xff00ff) + (i_642_ & 0xff00ff));
									i_642_ = ((i_644_ & 0x1000100) + (i_643_ - i_644_ & 0x10000));
									is_580_[i_579_] = i_643_ - i_642_ | i_642_ - (i_642_ >>> 8);
								}
							} else
								throw new IllegalArgumentException();
							i_574_ += Class397_Sub1.anInt5766;
						}
					}
					i_572_++;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_645_ = Class397_Sub1.anInt5771;
				while (i_645_ < 0) {
					int i_646_ = Class397_Sub1.anInt5759;
					int i_647_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_648_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_649_ = Class397_Sub1.anInt5770;
					int i_650_;
					if ((i_650_ = i_647_ - (anInt5758 << 12)) >= 0) {
						i_650_ = ((Class397_Sub1.anInt5766 - i_650_) / Class397_Sub1.anInt5766);
						i_649_ += i_650_;
						i_647_ += Class397_Sub1.anInt5766 * i_650_;
						i_648_ += Class397_Sub1.anInt5777 * i_650_;
						i_646_ += i_650_;
					}
					if ((i_650_ = ((i_647_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_649_)
						i_649_ = i_650_;
					if ((i_650_ = i_648_ - (anInt5756 << 12)) >= 0) {
						i_650_ = ((Class397_Sub1.anInt5777 - i_650_) / Class397_Sub1.anInt5777);
						i_649_ += i_650_;
						i_647_ += Class397_Sub1.anInt5766 * i_650_;
						i_648_ += Class397_Sub1.anInt5777 * i_650_;
						i_646_ += i_650_;
					}
					if ((i_650_ = ((i_648_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_649_)
						i_649_ = i_650_;
					for (/**/; i_649_ < 0; i_649_++) {
						int i_651_ = (i_648_ >> 12) * anInt5758 + (i_647_ >> 12);
						int i_652_ = i_646_++;
						int[] is_653_ = is;
						int i_654_ = i;
						int i_655_ = i_352_;
						if (i_655_ == 0) {
							if (i_654_ == 1)
								is_653_[i_652_] = anIntArray6690[i_651_];
							else if (i_654_ == 0) {
								int i_656_ = anIntArray6690[i_651_++];
								int i_657_ = (((i_656_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_658_ = (((i_656_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_659_ = (((i_656_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_653_[i_652_] = (i_657_ | i_658_ | i_659_) >>> 8;
							} else if (i_654_ == 3) {
								int i_660_ = anIntArray6690[i_651_++];
								int i_661_ = Class397_Sub1.anInt5767;
								int i_662_ = i_660_ + i_661_;
								int i_663_ = ((i_660_ & 0xff00ff) + (i_661_ & 0xff00ff));
								int i_664_ = ((i_663_ & 0x1000100) + (i_662_ - i_663_ & 0x10000));
								is_653_[i_652_] = i_662_ - i_664_ | i_664_ - (i_664_ >>> 8);
							} else if (i_654_ == 2) {
								int i_665_ = anIntArray6690[i_651_];
								int i_666_ = (((i_665_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_667_ = (((i_665_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_653_[i_652_] = (((i_666_ | i_667_) >>> 8) + Class397_Sub1.anInt5773);
							} else
								throw new IllegalArgumentException();
						} else if (i_655_ == 1) {
							if (i_654_ == 1) {
								int i_668_ = anIntArray6690[i_651_];
								int i_669_ = i_668_ >>> 24;
								int i_670_ = 256 - i_669_;
								int i_671_ = is_653_[i_652_];
								is_653_[i_652_] = ((((i_668_ & 0xff00ff) * i_669_ + (i_671_ & 0xff00ff) * i_670_)
										& ~0xff00ff)
										+ (((i_668_ & 0xff00) * i_669_ + (i_671_ & 0xff00) * i_670_) & 0xff0000)) >> 8;
							} else if (i_654_ == 0) {
								int i_672_ = anIntArray6690[i_651_];
								int i_673_ = (((i_672_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
								int i_674_ = 256 - i_673_;
								if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
									int i_675_ = is_653_[i_652_];
									is_653_[i_652_] = ((((i_672_ & 0xff00ff) * i_673_ + (i_675_ & 0xff00ff) * i_674_)
											& ~0xff00ff)
											+ (((i_672_ & 0xff00) * i_673_ + (i_675_ & 0xff00) * i_674_)
													& 0xff0000)) >> 8;
								} else if (i_673_ != 255) {
									int i_676_ = (((i_672_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_677_ = (((i_672_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_678_ = (((i_672_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_672_ = (i_676_ | i_677_ | i_678_) >>> 8;
									int i_679_ = is_653_[i_652_];
									is_653_[i_652_] = ((((i_672_ & 0xff00ff) * i_673_ + (i_679_ & 0xff00ff) * i_674_)
											& ~0xff00ff)
											+ (((i_672_ & 0xff00) * i_673_ + (i_679_ & 0xff00) * i_674_)
													& 0xff0000)) >> 8;
								} else {
									int i_680_ = (((i_672_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_681_ = (((i_672_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_682_ = (((i_672_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_653_[i_652_] = (i_680_ | i_681_ | i_682_) >>> 8;
								}
							} else if (i_654_ == 3) {
								int i_683_ = anIntArray6690[i_651_];
								int i_684_ = Class397_Sub1.anInt5767;
								int i_685_ = i_683_ + i_684_;
								int i_686_ = ((i_683_ & 0xff00ff) + (i_684_ & 0xff00ff));
								int i_687_ = ((i_686_ & 0x1000100) + (i_685_ - i_686_ & 0x10000));
								i_687_ = i_685_ - i_687_ | i_687_ - (i_687_ >>> 8);
								int i_688_ = (((i_683_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
								int i_689_ = 256 - i_688_;
								if (i_688_ != 255) {
									i_683_ = i_687_;
									i_687_ = is_653_[i_652_];
									i_687_ = ((((i_683_ & 0xff00ff) * i_688_ + (i_687_ & 0xff00ff) * i_689_)
											& ~0xff00ff)
											+ (((i_683_ & 0xff00) * i_688_ + (i_687_ & 0xff00) * i_689_)
													& 0xff0000)) >> 8;
								}
								is_653_[i_652_] = i_687_;
							} else if (i_654_ == 2) {
								int i_690_ = anIntArray6690[i_651_];
								int i_691_ = i_690_ >>> 24;
								int i_692_ = 256 - i_691_;
								int i_693_ = (((i_690_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_694_ = (((i_690_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								i_690_ = (((i_693_ | i_694_) >>> 8) + Class397_Sub1.anInt5773);
								int i_695_ = is_653_[i_652_];
								is_653_[i_652_] = ((((i_690_ & 0xff00ff) * i_691_ + (i_695_ & 0xff00ff) * i_692_)
										& ~0xff00ff)
										+ (((i_690_ & 0xff00) * i_691_ + (i_695_ & 0xff00) * i_692_) & 0xff0000)) >> 8;
							} else
								throw new IllegalArgumentException();
						} else if (i_655_ == 2) {
							if (i_654_ == 1) {
								int i_696_ = anIntArray6690[i_651_];
								int i_697_ = is_653_[i_652_];
								int i_698_ = i_696_ + i_697_;
								int i_699_ = ((i_696_ & 0xff00ff) + (i_697_ & 0xff00ff));
								i_697_ = (i_699_ & 0x1000100) + (i_698_ - i_699_ & 0x10000);
								is_653_[i_652_] = i_698_ - i_697_ | i_697_ - (i_697_ >>> 8);
							} else if (i_654_ == 0) {
								int i_700_ = anIntArray6690[i_651_];
								int i_701_ = (((i_700_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_702_ = (((i_700_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_703_ = (((i_700_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								i_700_ = (i_701_ | i_702_ | i_703_) >>> 8;
								int i_704_ = is_653_[i_652_];
								int i_705_ = i_700_ + i_704_;
								int i_706_ = ((i_700_ & 0xff00ff) + (i_704_ & 0xff00ff));
								i_704_ = (i_706_ & 0x1000100) + (i_705_ - i_706_ & 0x10000);
								is_653_[i_652_] = i_705_ - i_704_ | i_704_ - (i_704_ >>> 8);
							} else if (i_654_ == 3) {
								int i_707_ = anIntArray6690[i_651_];
								int i_708_ = Class397_Sub1.anInt5767;
								int i_709_ = i_707_ + i_708_;
								int i_710_ = ((i_707_ & 0xff00ff) + (i_708_ & 0xff00ff));
								int i_711_ = ((i_710_ & 0x1000100) + (i_709_ - i_710_ & 0x10000));
								i_707_ = i_709_ - i_711_ | i_711_ - (i_711_ >>> 8);
								i_711_ = is_653_[i_652_];
								i_709_ = i_707_ + i_711_;
								i_710_ = (i_707_ & 0xff00ff) + (i_711_ & 0xff00ff);
								i_711_ = (i_710_ & 0x1000100) + (i_709_ - i_710_ & 0x10000);
								is_653_[i_652_] = i_709_ - i_711_ | i_711_ - (i_711_ >>> 8);
							} else if (i_654_ == 2) {
								int i_712_ = anIntArray6690[i_651_];
								int i_713_ = (((i_712_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_714_ = (((i_712_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								i_712_ = (((i_713_ | i_714_) >>> 8) + Class397_Sub1.anInt5773);
								int i_715_ = is_653_[i_652_];
								int i_716_ = i_712_ + i_715_;
								int i_717_ = ((i_712_ & 0xff00ff) + (i_715_ & 0xff00ff));
								i_715_ = (i_717_ & 0x1000100) + (i_716_ - i_717_ & 0x10000);
								is_653_[i_652_] = i_716_ - i_715_ | i_715_ - (i_715_ >>> 8);
							}
						} else
							throw new IllegalArgumentException();
						i_647_ += Class397_Sub1.anInt5766;
						i_648_ += Class397_Sub1.anInt5777;
					}
					i_645_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_718_ = Class397_Sub1.anInt5771;
				while (i_718_ < 0) {
					int i_719_ = Class397_Sub1.anInt5759;
					int i_720_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_721_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_722_ = Class397_Sub1.anInt5770;
					int i_723_;
					if ((i_723_ = i_720_ - (anInt5758 << 12)) >= 0) {
						i_723_ = ((Class397_Sub1.anInt5766 - i_723_) / Class397_Sub1.anInt5766);
						i_722_ += i_723_;
						i_720_ += Class397_Sub1.anInt5766 * i_723_;
						i_721_ += Class397_Sub1.anInt5777 * i_723_;
						i_719_ += i_723_;
					}
					if ((i_723_ = ((i_720_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_722_)
						i_722_ = i_723_;
					if (i_721_ < 0) {
						i_723_ = ((Class397_Sub1.anInt5777 - 1 - i_721_) / Class397_Sub1.anInt5777);
						i_722_ += i_723_;
						i_720_ += Class397_Sub1.anInt5766 * i_723_;
						i_721_ += Class397_Sub1.anInt5777 * i_723_;
						i_719_ += i_723_;
					}
					if ((i_723_ = ((i_721_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
							/ Class397_Sub1.anInt5777)) > i_722_)
						i_722_ = i_723_;
					for (/**/; i_722_ < 0; i_722_++) {
						int i_724_ = (i_721_ >> 12) * anInt5758 + (i_720_ >> 12);
						int i_725_ = i_719_++;
						int[] is_726_ = is;
						int i_727_ = i;
						int i_728_ = i_352_;
						if (i_728_ == 0) {
							if (i_727_ == 1)
								is_726_[i_725_] = anIntArray6690[i_724_];
							else if (i_727_ == 0) {
								int i_729_ = anIntArray6690[i_724_++];
								int i_730_ = (((i_729_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_731_ = (((i_729_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_732_ = (((i_729_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_726_[i_725_] = (i_730_ | i_731_ | i_732_) >>> 8;
							} else if (i_727_ == 3) {
								int i_733_ = anIntArray6690[i_724_++];
								int i_734_ = Class397_Sub1.anInt5767;
								int i_735_ = i_733_ + i_734_;
								int i_736_ = ((i_733_ & 0xff00ff) + (i_734_ & 0xff00ff));
								int i_737_ = ((i_736_ & 0x1000100) + (i_735_ - i_736_ & 0x10000));
								is_726_[i_725_] = i_735_ - i_737_ | i_737_ - (i_737_ >>> 8);
							} else if (i_727_ == 2) {
								int i_738_ = anIntArray6690[i_724_];
								int i_739_ = (((i_738_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_740_ = (((i_738_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_726_[i_725_] = (((i_739_ | i_740_) >>> 8) + Class397_Sub1.anInt5773);
							} else
								throw new IllegalArgumentException();
						} else if (i_728_ == 1) {
							if (i_727_ == 1) {
								int i_741_ = anIntArray6690[i_724_];
								int i_742_ = i_741_ >>> 24;
								int i_743_ = 256 - i_742_;
								int i_744_ = is_726_[i_725_];
								is_726_[i_725_] = ((((i_741_ & 0xff00ff) * i_742_ + (i_744_ & 0xff00ff) * i_743_)
										& ~0xff00ff)
										+ (((i_741_ & 0xff00) * i_742_ + (i_744_ & 0xff00) * i_743_) & 0xff0000)) >> 8;
							} else if (i_727_ == 0) {
								int i_745_ = anIntArray6690[i_724_];
								int i_746_ = (((i_745_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
								int i_747_ = 256 - i_746_;
								if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
									int i_748_ = is_726_[i_725_];
									is_726_[i_725_] = ((((i_745_ & 0xff00ff) * i_746_ + (i_748_ & 0xff00ff) * i_747_)
											& ~0xff00ff)
											+ (((i_745_ & 0xff00) * i_746_ + (i_748_ & 0xff00) * i_747_)
													& 0xff0000)) >> 8;
								} else if (i_746_ != 255) {
									int i_749_ = (((i_745_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_750_ = (((i_745_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_751_ = (((i_745_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_745_ = (i_749_ | i_750_ | i_751_) >>> 8;
									int i_752_ = is_726_[i_725_];
									is_726_[i_725_] = ((((i_745_ & 0xff00ff) * i_746_ + (i_752_ & 0xff00ff) * i_747_)
											& ~0xff00ff)
											+ (((i_745_ & 0xff00) * i_746_ + (i_752_ & 0xff00) * i_747_)
													& 0xff0000)) >> 8;
								} else {
									int i_753_ = (((i_745_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_754_ = (((i_745_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_755_ = (((i_745_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_726_[i_725_] = (i_753_ | i_754_ | i_755_) >>> 8;
								}
							} else if (i_727_ == 3) {
								int i_756_ = anIntArray6690[i_724_];
								int i_757_ = Class397_Sub1.anInt5767;
								int i_758_ = i_756_ + i_757_;
								int i_759_ = ((i_756_ & 0xff00ff) + (i_757_ & 0xff00ff));
								int i_760_ = ((i_759_ & 0x1000100) + (i_758_ - i_759_ & 0x10000));
								i_760_ = i_758_ - i_760_ | i_760_ - (i_760_ >>> 8);
								int i_761_ = (((i_756_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
								int i_762_ = 256 - i_761_;
								if (i_761_ != 255) {
									i_756_ = i_760_;
									i_760_ = is_726_[i_725_];
									i_760_ = ((((i_756_ & 0xff00ff) * i_761_ + (i_760_ & 0xff00ff) * i_762_)
											& ~0xff00ff)
											+ (((i_756_ & 0xff00) * i_761_ + (i_760_ & 0xff00) * i_762_)
													& 0xff0000)) >> 8;
								}
								is_726_[i_725_] = i_760_;
							} else if (i_727_ == 2) {
								int i_763_ = anIntArray6690[i_724_];
								int i_764_ = i_763_ >>> 24;
								int i_765_ = 256 - i_764_;
								int i_766_ = (((i_763_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_767_ = (((i_763_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								i_763_ = (((i_766_ | i_767_) >>> 8) + Class397_Sub1.anInt5773);
								int i_768_ = is_726_[i_725_];
								is_726_[i_725_] = ((((i_763_ & 0xff00ff) * i_764_ + (i_768_ & 0xff00ff) * i_765_)
										& ~0xff00ff)
										+ (((i_763_ & 0xff00) * i_764_ + (i_768_ & 0xff00) * i_765_) & 0xff0000)) >> 8;
							} else
								throw new IllegalArgumentException();
						} else if (i_728_ == 2) {
							if (i_727_ == 1) {
								int i_769_ = anIntArray6690[i_724_];
								int i_770_ = is_726_[i_725_];
								int i_771_ = i_769_ + i_770_;
								int i_772_ = ((i_769_ & 0xff00ff) + (i_770_ & 0xff00ff));
								i_770_ = (i_772_ & 0x1000100) + (i_771_ - i_772_ & 0x10000);
								is_726_[i_725_] = i_771_ - i_770_ | i_770_ - (i_770_ >>> 8);
							} else if (i_727_ == 0) {
								int i_773_ = anIntArray6690[i_724_];
								int i_774_ = (((i_773_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_775_ = (((i_773_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_776_ = (((i_773_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								i_773_ = (i_774_ | i_775_ | i_776_) >>> 8;
								int i_777_ = is_726_[i_725_];
								int i_778_ = i_773_ + i_777_;
								int i_779_ = ((i_773_ & 0xff00ff) + (i_777_ & 0xff00ff));
								i_777_ = (i_779_ & 0x1000100) + (i_778_ - i_779_ & 0x10000);
								is_726_[i_725_] = i_778_ - i_777_ | i_777_ - (i_777_ >>> 8);
							} else if (i_727_ == 3) {
								int i_780_ = anIntArray6690[i_724_];
								int i_781_ = Class397_Sub1.anInt5767;
								int i_782_ = i_780_ + i_781_;
								int i_783_ = ((i_780_ & 0xff00ff) + (i_781_ & 0xff00ff));
								int i_784_ = ((i_783_ & 0x1000100) + (i_782_ - i_783_ & 0x10000));
								i_780_ = i_782_ - i_784_ | i_784_ - (i_784_ >>> 8);
								i_784_ = is_726_[i_725_];
								i_782_ = i_780_ + i_784_;
								i_783_ = (i_780_ & 0xff00ff) + (i_784_ & 0xff00ff);
								i_784_ = (i_783_ & 0x1000100) + (i_782_ - i_783_ & 0x10000);
								is_726_[i_725_] = i_782_ - i_784_ | i_784_ - (i_784_ >>> 8);
							} else if (i_727_ == 2) {
								int i_785_ = anIntArray6690[i_724_];
								int i_786_ = (((i_785_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_787_ = (((i_785_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								i_785_ = (((i_786_ | i_787_) >>> 8) + Class397_Sub1.anInt5773);
								int i_788_ = is_726_[i_725_];
								int i_789_ = i_785_ + i_788_;
								int i_790_ = ((i_785_ & 0xff00ff) + (i_788_ & 0xff00ff));
								i_788_ = (i_790_ & 0x1000100) + (i_789_ - i_790_ & 0x10000);
								is_726_[i_725_] = i_789_ - i_788_ | i_788_ - (i_788_ >>> 8);
							}
						} else
							throw new IllegalArgumentException();
						i_720_ += Class397_Sub1.anInt5766;
						i_721_ += Class397_Sub1.anInt5777;
					}
					i_718_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5777 == 0) {
			int i_791_ = Class397_Sub1.anInt5771;
			while (i_791_ < 0) {
				int i_792_ = Class397_Sub1.anInt5759;
				int i_793_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
				int i_794_ = Class397_Sub1.anInt5775;
				int i_795_ = Class397_Sub1.anInt5770;
				if (i_794_ >= 0 && i_794_ - (anInt5756 << 12) < 0) {
					if (i_793_ < 0) {
						int i_796_ = ((Class397_Sub1.anInt5766 - 1 - i_793_) / Class397_Sub1.anInt5766);
						i_795_ += i_796_;
						i_793_ += Class397_Sub1.anInt5766 * i_796_;
						i_792_ += i_796_;
					}
					int i_797_;
					if ((i_797_ = ((i_793_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
							/ Class397_Sub1.anInt5766)) > i_795_)
						i_795_ = i_797_;
					for (/**/; i_795_ < 0; i_795_++) {
						int i_798_ = (i_794_ >> 12) * anInt5758 + (i_793_ >> 12);
						int i_799_ = i_792_++;
						int[] is_800_ = is;
						int i_801_ = i;
						int i_802_ = i_352_;
						if (i_802_ == 0) {
							if (i_801_ == 1)
								is_800_[i_799_] = anIntArray6690[i_798_];
							else if (i_801_ == 0) {
								int i_803_ = anIntArray6690[i_798_++];
								int i_804_ = (((i_803_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_805_ = (((i_803_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_806_ = (((i_803_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_800_[i_799_] = (i_804_ | i_805_ | i_806_) >>> 8;
							} else if (i_801_ == 3) {
								int i_807_ = anIntArray6690[i_798_++];
								int i_808_ = Class397_Sub1.anInt5767;
								int i_809_ = i_807_ + i_808_;
								int i_810_ = ((i_807_ & 0xff00ff) + (i_808_ & 0xff00ff));
								int i_811_ = ((i_810_ & 0x1000100) + (i_809_ - i_810_ & 0x10000));
								is_800_[i_799_] = i_809_ - i_811_ | i_811_ - (i_811_ >>> 8);
							} else if (i_801_ == 2) {
								int i_812_ = anIntArray6690[i_798_];
								int i_813_ = (((i_812_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_814_ = (((i_812_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_800_[i_799_] = (((i_813_ | i_814_) >>> 8) + Class397_Sub1.anInt5773);
							} else
								throw new IllegalArgumentException();
						} else if (i_802_ == 1) {
							if (i_801_ == 1) {
								int i_815_ = anIntArray6690[i_798_];
								int i_816_ = i_815_ >>> 24;
								int i_817_ = 256 - i_816_;
								int i_818_ = is_800_[i_799_];
								is_800_[i_799_] = ((((i_815_ & 0xff00ff) * i_816_ + (i_818_ & 0xff00ff) * i_817_)
										& ~0xff00ff)
										+ (((i_815_ & 0xff00) * i_816_ + (i_818_ & 0xff00) * i_817_) & 0xff0000)) >> 8;
							} else if (i_801_ == 0) {
								int i_819_ = anIntArray6690[i_798_];
								int i_820_ = (((i_819_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
								int i_821_ = 256 - i_820_;
								if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
									int i_822_ = is_800_[i_799_];
									is_800_[i_799_] = ((((i_819_ & 0xff00ff) * i_820_ + (i_822_ & 0xff00ff) * i_821_)
											& ~0xff00ff)
											+ (((i_819_ & 0xff00) * i_820_ + (i_822_ & 0xff00) * i_821_)
													& 0xff0000)) >> 8;
								} else if (i_820_ != 255) {
									int i_823_ = (((i_819_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_824_ = (((i_819_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_825_ = (((i_819_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_819_ = (i_823_ | i_824_ | i_825_) >>> 8;
									int i_826_ = is_800_[i_799_];
									is_800_[i_799_] = ((((i_819_ & 0xff00ff) * i_820_ + (i_826_ & 0xff00ff) * i_821_)
											& ~0xff00ff)
											+ (((i_819_ & 0xff00) * i_820_ + (i_826_ & 0xff00) * i_821_)
													& 0xff0000)) >> 8;
								} else {
									int i_827_ = (((i_819_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_828_ = (((i_819_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_829_ = (((i_819_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_800_[i_799_] = (i_827_ | i_828_ | i_829_) >>> 8;
								}
							} else if (i_801_ == 3) {
								int i_830_ = anIntArray6690[i_798_];
								int i_831_ = Class397_Sub1.anInt5767;
								int i_832_ = i_830_ + i_831_;
								int i_833_ = ((i_830_ & 0xff00ff) + (i_831_ & 0xff00ff));
								int i_834_ = ((i_833_ & 0x1000100) + (i_832_ - i_833_ & 0x10000));
								i_834_ = i_832_ - i_834_ | i_834_ - (i_834_ >>> 8);
								int i_835_ = (((i_830_ >>> 24) * Class397_Sub1.anInt5755) >> 8);
								int i_836_ = 256 - i_835_;
								if (i_835_ != 255) {
									i_830_ = i_834_;
									i_834_ = is_800_[i_799_];
									i_834_ = ((((i_830_ & 0xff00ff) * i_835_ + (i_834_ & 0xff00ff) * i_836_)
											& ~0xff00ff)
											+ (((i_830_ & 0xff00) * i_835_ + (i_834_ & 0xff00) * i_836_)
													& 0xff0000)) >> 8;
								}
								is_800_[i_799_] = i_834_;
							} else if (i_801_ == 2) {
								int i_837_ = anIntArray6690[i_798_];
								int i_838_ = i_837_ >>> 24;
								int i_839_ = 256 - i_838_;
								int i_840_ = (((i_837_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_841_ = (((i_837_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								i_837_ = (((i_840_ | i_841_) >>> 8) + Class397_Sub1.anInt5773);
								int i_842_ = is_800_[i_799_];
								is_800_[i_799_] = ((((i_837_ & 0xff00ff) * i_838_ + (i_842_ & 0xff00ff) * i_839_)
										& ~0xff00ff)
										+ (((i_837_ & 0xff00) * i_838_ + (i_842_ & 0xff00) * i_839_) & 0xff0000)) >> 8;
							} else
								throw new IllegalArgumentException();
						} else if (i_802_ == 2) {
							if (i_801_ == 1) {
								int i_843_ = anIntArray6690[i_798_];
								int i_844_ = is_800_[i_799_];
								int i_845_ = i_843_ + i_844_;
								int i_846_ = ((i_843_ & 0xff00ff) + (i_844_ & 0xff00ff));
								i_844_ = (i_846_ & 0x1000100) + (i_845_ - i_846_ & 0x10000);
								is_800_[i_799_] = i_845_ - i_844_ | i_844_ - (i_844_ >>> 8);
							} else if (i_801_ == 0) {
								int i_847_ = anIntArray6690[i_798_];
								int i_848_ = (((i_847_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_849_ = (((i_847_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_850_ = (((i_847_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								i_847_ = (i_848_ | i_849_ | i_850_) >>> 8;
								int i_851_ = is_800_[i_799_];
								int i_852_ = i_847_ + i_851_;
								int i_853_ = ((i_847_ & 0xff00ff) + (i_851_ & 0xff00ff));
								i_851_ = (i_853_ & 0x1000100) + (i_852_ - i_853_ & 0x10000);
								is_800_[i_799_] = i_852_ - i_851_ | i_851_ - (i_851_ >>> 8);
							} else if (i_801_ == 3) {
								int i_854_ = anIntArray6690[i_798_];
								int i_855_ = Class397_Sub1.anInt5767;
								int i_856_ = i_854_ + i_855_;
								int i_857_ = ((i_854_ & 0xff00ff) + (i_855_ & 0xff00ff));
								int i_858_ = ((i_857_ & 0x1000100) + (i_856_ - i_857_ & 0x10000));
								i_854_ = i_856_ - i_858_ | i_858_ - (i_858_ >>> 8);
								i_858_ = is_800_[i_799_];
								i_856_ = i_854_ + i_858_;
								i_857_ = (i_854_ & 0xff00ff) + (i_858_ & 0xff00ff);
								i_858_ = (i_857_ & 0x1000100) + (i_856_ - i_857_ & 0x10000);
								is_800_[i_799_] = i_856_ - i_858_ | i_858_ - (i_858_ >>> 8);
							} else if (i_801_ == 2) {
								int i_859_ = anIntArray6690[i_798_];
								int i_860_ = (((i_859_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_861_ = (((i_859_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								i_859_ = (((i_860_ | i_861_) >>> 8) + Class397_Sub1.anInt5773);
								int i_862_ = is_800_[i_799_];
								int i_863_ = i_859_ + i_862_;
								int i_864_ = ((i_859_ & 0xff00ff) + (i_862_ & 0xff00ff));
								i_862_ = (i_864_ & 0x1000100) + (i_863_ - i_864_ & 0x10000);
								is_800_[i_799_] = i_863_ - i_862_ | i_862_ - (i_862_ >>> 8);
							}
						} else
							throw new IllegalArgumentException();
						i_793_ += Class397_Sub1.anInt5766;
					}
				}
				i_791_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else if (Class397_Sub1.anInt5777 < 0) {
			for (int i_865_ = Class397_Sub1.anInt5771; i_865_ < 0; i_865_++) {
				int i_866_ = Class397_Sub1.anInt5759;
				int i_867_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
				int i_868_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
				int i_869_ = Class397_Sub1.anInt5770;
				if (i_867_ < 0) {
					int i_870_ = ((Class397_Sub1.anInt5766 - 1 - i_867_) / Class397_Sub1.anInt5766);
					i_869_ += i_870_;
					i_867_ += Class397_Sub1.anInt5766 * i_870_;
					i_868_ += Class397_Sub1.anInt5777 * i_870_;
					i_866_ += i_870_;
				}
				int i_871_;
				if ((i_871_ = (i_867_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
						/ Class397_Sub1.anInt5766) > i_869_)
					i_869_ = i_871_;
				if ((i_871_ = i_868_ - (anInt5756 << 12)) >= 0) {
					i_871_ = ((Class397_Sub1.anInt5777 - i_871_) / Class397_Sub1.anInt5777);
					i_869_ += i_871_;
					i_867_ += Class397_Sub1.anInt5766 * i_871_;
					i_868_ += Class397_Sub1.anInt5777 * i_871_;
					i_866_ += i_871_;
				}
				if ((i_871_ = ((i_868_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_869_)
					i_869_ = i_871_;
				for (/**/; i_869_ < 0; i_869_++) {
					int i_872_ = (i_868_ >> 12) * anInt5758 + (i_867_ >> 12);
					int i_873_ = i_866_++;
					int[] is_874_ = is;
					int i_875_ = i;
					int i_876_ = i_352_;
					if (i_876_ == 0) {
						if (i_875_ == 1)
							is_874_[i_873_] = anIntArray6690[i_872_];
						else if (i_875_ == 0) {
							int i_877_ = anIntArray6690[i_872_++];
							int i_878_ = (((i_877_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
							int i_879_ = ((i_877_ & 0xff00) * Class397_Sub1.anInt5753 & 0xff0000);
							int i_880_ = ((i_877_ & 0xff) * Class397_Sub1.anInt5752 & 0xff00);
							is_874_[i_873_] = (i_878_ | i_879_ | i_880_) >>> 8;
						} else if (i_875_ == 3) {
							int i_881_ = anIntArray6690[i_872_++];
							int i_882_ = Class397_Sub1.anInt5767;
							int i_883_ = i_881_ + i_882_;
							int i_884_ = (i_881_ & 0xff00ff) + (i_882_ & 0xff00ff);
							int i_885_ = ((i_884_ & 0x1000100) + (i_883_ - i_884_ & 0x10000));
							is_874_[i_873_] = i_883_ - i_885_ | i_885_ - (i_885_ >>> 8);
						} else if (i_875_ == 2) {
							int i_886_ = anIntArray6690[i_872_];
							int i_887_ = (((i_886_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
							int i_888_ = ((i_886_ & 0xff00) * Class397_Sub1.anInt5755 & 0xff0000);
							is_874_[i_873_] = (((i_887_ | i_888_) >>> 8) + Class397_Sub1.anInt5773);
						} else
							throw new IllegalArgumentException();
					} else if (i_876_ == 1) {
						if (i_875_ == 1) {
							int i_889_ = anIntArray6690[i_872_];
							int i_890_ = i_889_ >>> 24;
							int i_891_ = 256 - i_890_;
							int i_892_ = is_874_[i_873_];
							is_874_[i_873_] = ((((i_889_ & 0xff00ff) * i_890_ + (i_892_ & 0xff00ff) * i_891_)
									& ~0xff00ff)
									+ (((i_889_ & 0xff00) * i_890_ + (i_892_ & 0xff00) * i_891_) & 0xff0000)) >> 8;
						} else if (i_875_ == 0) {
							int i_893_ = anIntArray6690[i_872_];
							int i_894_ = ((i_893_ >>> 24) * Class397_Sub1.anInt5755 >> 8);
							int i_895_ = 256 - i_894_;
							if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
								int i_896_ = is_874_[i_873_];
								is_874_[i_873_] = ((((i_893_ & 0xff00ff) * i_894_ + (i_896_ & 0xff00ff) * i_895_)
										& ~0xff00ff)
										+ (((i_893_ & 0xff00) * i_894_ + (i_896_ & 0xff00) * i_895_) & 0xff0000)) >> 8;
							} else if (i_894_ != 255) {
								int i_897_ = (((i_893_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_898_ = (((i_893_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_899_ = (((i_893_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								i_893_ = (i_897_ | i_898_ | i_899_) >>> 8;
								int i_900_ = is_874_[i_873_];
								is_874_[i_873_] = ((((i_893_ & 0xff00ff) * i_894_ + (i_900_ & 0xff00ff) * i_895_)
										& ~0xff00ff)
										+ (((i_893_ & 0xff00) * i_894_ + (i_900_ & 0xff00) * i_895_) & 0xff0000)) >> 8;
							} else {
								int i_901_ = (((i_893_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_902_ = (((i_893_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_903_ = (((i_893_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_874_[i_873_] = (i_901_ | i_902_ | i_903_) >>> 8;
							}
						} else if (i_875_ == 3) {
							int i_904_ = anIntArray6690[i_872_];
							int i_905_ = Class397_Sub1.anInt5767;
							int i_906_ = i_904_ + i_905_;
							int i_907_ = (i_904_ & 0xff00ff) + (i_905_ & 0xff00ff);
							int i_908_ = ((i_907_ & 0x1000100) + (i_906_ - i_907_ & 0x10000));
							i_908_ = i_906_ - i_908_ | i_908_ - (i_908_ >>> 8);
							int i_909_ = ((i_904_ >>> 24) * Class397_Sub1.anInt5755 >> 8);
							int i_910_ = 256 - i_909_;
							if (i_909_ != 255) {
								i_904_ = i_908_;
								i_908_ = is_874_[i_873_];
								i_908_ = ((((i_904_ & 0xff00ff) * i_909_ + (i_908_ & 0xff00ff) * i_910_) & ~0xff00ff)
										+ (((i_904_ & 0xff00) * i_909_ + (i_908_ & 0xff00) * i_910_) & 0xff0000)) >> 8;
							}
							is_874_[i_873_] = i_908_;
						} else if (i_875_ == 2) {
							int i_911_ = anIntArray6690[i_872_];
							int i_912_ = i_911_ >>> 24;
							int i_913_ = 256 - i_912_;
							int i_914_ = (((i_911_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
							int i_915_ = ((i_911_ & 0xff00) * Class397_Sub1.anInt5755 & 0xff0000);
							i_911_ = (((i_914_ | i_915_) >>> 8) + Class397_Sub1.anInt5773);
							int i_916_ = is_874_[i_873_];
							is_874_[i_873_] = ((((i_911_ & 0xff00ff) * i_912_ + (i_916_ & 0xff00ff) * i_913_)
									& ~0xff00ff)
									+ (((i_911_ & 0xff00) * i_912_ + (i_916_ & 0xff00) * i_913_) & 0xff0000)) >> 8;
						} else
							throw new IllegalArgumentException();
					} else if (i_876_ == 2) {
						if (i_875_ == 1) {
							int i_917_ = anIntArray6690[i_872_];
							int i_918_ = is_874_[i_873_];
							int i_919_ = i_917_ + i_918_;
							int i_920_ = (i_917_ & 0xff00ff) + (i_918_ & 0xff00ff);
							i_918_ = (i_920_ & 0x1000100) + (i_919_ - i_920_ & 0x10000);
							is_874_[i_873_] = i_919_ - i_918_ | i_918_ - (i_918_ >>> 8);
						} else if (i_875_ == 0) {
							int i_921_ = anIntArray6690[i_872_];
							int i_922_ = (((i_921_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
							int i_923_ = ((i_921_ & 0xff00) * Class397_Sub1.anInt5753 & 0xff0000);
							int i_924_ = ((i_921_ & 0xff) * Class397_Sub1.anInt5752 & 0xff00);
							i_921_ = (i_922_ | i_923_ | i_924_) >>> 8;
							int i_925_ = is_874_[i_873_];
							int i_926_ = i_921_ + i_925_;
							int i_927_ = (i_921_ & 0xff00ff) + (i_925_ & 0xff00ff);
							i_925_ = (i_927_ & 0x1000100) + (i_926_ - i_927_ & 0x10000);
							is_874_[i_873_] = i_926_ - i_925_ | i_925_ - (i_925_ >>> 8);
						} else if (i_875_ == 3) {
							int i_928_ = anIntArray6690[i_872_];
							int i_929_ = Class397_Sub1.anInt5767;
							int i_930_ = i_928_ + i_929_;
							int i_931_ = (i_928_ & 0xff00ff) + (i_929_ & 0xff00ff);
							int i_932_ = ((i_931_ & 0x1000100) + (i_930_ - i_931_ & 0x10000));
							i_928_ = i_930_ - i_932_ | i_932_ - (i_932_ >>> 8);
							i_932_ = is_874_[i_873_];
							i_930_ = i_928_ + i_932_;
							i_931_ = (i_928_ & 0xff00ff) + (i_932_ & 0xff00ff);
							i_932_ = (i_931_ & 0x1000100) + (i_930_ - i_931_ & 0x10000);
							is_874_[i_873_] = i_930_ - i_932_ | i_932_ - (i_932_ >>> 8);
						} else if (i_875_ == 2) {
							int i_933_ = anIntArray6690[i_872_];
							int i_934_ = (((i_933_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
							int i_935_ = ((i_933_ & 0xff00) * Class397_Sub1.anInt5755 & 0xff0000);
							i_933_ = (((i_934_ | i_935_) >>> 8) + Class397_Sub1.anInt5773);
							int i_936_ = is_874_[i_873_];
							int i_937_ = i_933_ + i_936_;
							int i_938_ = (i_933_ & 0xff00ff) + (i_936_ & 0xff00ff);
							i_936_ = (i_938_ & 0x1000100) + (i_937_ - i_938_ & 0x10000);
							is_874_[i_873_] = i_937_ - i_936_ | i_936_ - (i_936_ >>> 8);
						}
					} else
						throw new IllegalArgumentException();
					i_867_ += Class397_Sub1.anInt5766;
					i_868_ += Class397_Sub1.anInt5777;
				}
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else {
			for (int i_939_ = Class397_Sub1.anInt5771; i_939_ < 0; i_939_++) {
				int i_940_ = Class397_Sub1.anInt5759;
				int i_941_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
				int i_942_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
				int i_943_ = Class397_Sub1.anInt5770;
				if (i_941_ < 0) {
					int i_944_ = ((Class397_Sub1.anInt5766 - 1 - i_941_) / Class397_Sub1.anInt5766);
					i_943_ += i_944_;
					i_941_ += Class397_Sub1.anInt5766 * i_944_;
					i_942_ += Class397_Sub1.anInt5777 * i_944_;
					i_940_ += i_944_;
				}
				int i_945_;
				if ((i_945_ = (i_941_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
						/ Class397_Sub1.anInt5766) > i_943_)
					i_943_ = i_945_;
				if (i_942_ < 0) {
					i_945_ = ((Class397_Sub1.anInt5777 - 1 - i_942_) / Class397_Sub1.anInt5777);
					i_943_ += i_945_;
					i_941_ += Class397_Sub1.anInt5766 * i_945_;
					i_942_ += Class397_Sub1.anInt5777 * i_945_;
					i_940_ += i_945_;
				}
				if ((i_945_ = (i_942_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
						/ Class397_Sub1.anInt5777) > i_943_)
					i_943_ = i_945_;
				for (/**/; i_943_ < 0; i_943_++) {
					int i_946_ = (i_942_ >> 12) * anInt5758 + (i_941_ >> 12);
					int i_947_ = i_940_++;
					int[] is_948_ = is;
					int i_949_ = i;
					int i_950_ = i_352_;
					if (i_950_ == 0) {
						if (i_949_ == 1)
							is_948_[i_947_] = anIntArray6690[i_946_];
						else if (i_949_ == 0) {
							int i_951_ = anIntArray6690[i_946_++];
							int i_952_ = (((i_951_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
							int i_953_ = ((i_951_ & 0xff00) * Class397_Sub1.anInt5753 & 0xff0000);
							int i_954_ = ((i_951_ & 0xff) * Class397_Sub1.anInt5752 & 0xff00);
							is_948_[i_947_] = (i_952_ | i_953_ | i_954_) >>> 8;
						} else if (i_949_ == 3) {
							int i_955_ = anIntArray6690[i_946_++];
							int i_956_ = Class397_Sub1.anInt5767;
							int i_957_ = i_955_ + i_956_;
							int i_958_ = (i_955_ & 0xff00ff) + (i_956_ & 0xff00ff);
							int i_959_ = ((i_958_ & 0x1000100) + (i_957_ - i_958_ & 0x10000));
							is_948_[i_947_] = i_957_ - i_959_ | i_959_ - (i_959_ >>> 8);
						} else if (i_949_ == 2) {
							int i_960_ = anIntArray6690[i_946_];
							int i_961_ = (((i_960_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
							int i_962_ = ((i_960_ & 0xff00) * Class397_Sub1.anInt5755 & 0xff0000);
							is_948_[i_947_] = (((i_961_ | i_962_) >>> 8) + Class397_Sub1.anInt5773);
						} else
							throw new IllegalArgumentException();
					} else if (i_950_ == 1) {
						if (i_949_ == 1) {
							int i_963_ = anIntArray6690[i_946_];
							int i_964_ = i_963_ >>> 24;
							int i_965_ = 256 - i_964_;
							int i_966_ = is_948_[i_947_];
							is_948_[i_947_] = ((((i_963_ & 0xff00ff) * i_964_ + (i_966_ & 0xff00ff) * i_965_)
									& ~0xff00ff)
									+ (((i_963_ & 0xff00) * i_964_ + (i_966_ & 0xff00) * i_965_) & 0xff0000)) >> 8;
						} else if (i_949_ == 0) {
							int i_967_ = anIntArray6690[i_946_];
							int i_968_ = ((i_967_ >>> 24) * Class397_Sub1.anInt5755 >> 8);
							int i_969_ = 256 - i_968_;
							if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
								int i_970_ = is_948_[i_947_];
								is_948_[i_947_] = ((((i_967_ & 0xff00ff) * i_968_ + (i_970_ & 0xff00ff) * i_969_)
										& ~0xff00ff)
										+ (((i_967_ & 0xff00) * i_968_ + (i_970_ & 0xff00) * i_969_) & 0xff0000)) >> 8;
							} else if (i_968_ != 255) {
								int i_971_ = (((i_967_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_972_ = (((i_967_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_973_ = (((i_967_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								i_967_ = (i_971_ | i_972_ | i_973_) >>> 8;
								int i_974_ = is_948_[i_947_];
								is_948_[i_947_] = ((((i_967_ & 0xff00ff) * i_968_ + (i_974_ & 0xff00ff) * i_969_)
										& ~0xff00ff)
										+ (((i_967_ & 0xff00) * i_968_ + (i_974_ & 0xff00) * i_969_) & 0xff0000)) >> 8;
							} else {
								int i_975_ = (((i_967_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_976_ = (((i_967_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_977_ = (((i_967_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_948_[i_947_] = (i_975_ | i_976_ | i_977_) >>> 8;
							}
						} else if (i_949_ == 3) {
							int i_978_ = anIntArray6690[i_946_];
							int i_979_ = Class397_Sub1.anInt5767;
							int i_980_ = i_978_ + i_979_;
							int i_981_ = (i_978_ & 0xff00ff) + (i_979_ & 0xff00ff);
							int i_982_ = ((i_981_ & 0x1000100) + (i_980_ - i_981_ & 0x10000));
							i_982_ = i_980_ - i_982_ | i_982_ - (i_982_ >>> 8);
							int i_983_ = ((i_978_ >>> 24) * Class397_Sub1.anInt5755 >> 8);
							int i_984_ = 256 - i_983_;
							if (i_983_ != 255) {
								i_978_ = i_982_;
								i_982_ = is_948_[i_947_];
								i_982_ = ((((i_978_ & 0xff00ff) * i_983_ + (i_982_ & 0xff00ff) * i_984_) & ~0xff00ff)
										+ (((i_978_ & 0xff00) * i_983_ + (i_982_ & 0xff00) * i_984_) & 0xff0000)) >> 8;
							}
							is_948_[i_947_] = i_982_;
						} else if (i_949_ == 2) {
							int i_985_ = anIntArray6690[i_946_];
							int i_986_ = i_985_ >>> 24;
							int i_987_ = 256 - i_986_;
							int i_988_ = (((i_985_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
							int i_989_ = ((i_985_ & 0xff00) * Class397_Sub1.anInt5755 & 0xff0000);
							i_985_ = (((i_988_ | i_989_) >>> 8) + Class397_Sub1.anInt5773);
							int i_990_ = is_948_[i_947_];
							is_948_[i_947_] = ((((i_985_ & 0xff00ff) * i_986_ + (i_990_ & 0xff00ff) * i_987_)
									& ~0xff00ff)
									+ (((i_985_ & 0xff00) * i_986_ + (i_990_ & 0xff00) * i_987_) & 0xff0000)) >> 8;
						} else
							throw new IllegalArgumentException();
					} else if (i_950_ == 2) {
						if (i_949_ == 1) {
							int i_991_ = anIntArray6690[i_946_];
							int i_992_ = is_948_[i_947_];
							int i_993_ = i_991_ + i_992_;
							int i_994_ = (i_991_ & 0xff00ff) + (i_992_ & 0xff00ff);
							i_992_ = (i_994_ & 0x1000100) + (i_993_ - i_994_ & 0x10000);
							is_948_[i_947_] = i_993_ - i_992_ | i_992_ - (i_992_ >>> 8);
						} else if (i_949_ == 0) {
							int i_995_ = anIntArray6690[i_946_];
							int i_996_ = (((i_995_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
							int i_997_ = ((i_995_ & 0xff00) * Class397_Sub1.anInt5753 & 0xff0000);
							int i_998_ = ((i_995_ & 0xff) * Class397_Sub1.anInt5752 & 0xff00);
							i_995_ = (i_996_ | i_997_ | i_998_) >>> 8;
							int i_999_ = is_948_[i_947_];
							int i_1000_ = i_995_ + i_999_;
							int i_1001_ = (i_995_ & 0xff00ff) + (i_999_ & 0xff00ff);
							i_999_ = (i_1001_ & 0x1000100) + (i_1000_ - i_1001_ & 0x10000);
							is_948_[i_947_] = i_1000_ - i_999_ | i_999_ - (i_999_ >>> 8);
						} else if (i_949_ == 3) {
							int i_1002_ = anIntArray6690[i_946_];
							int i_1003_ = Class397_Sub1.anInt5767;
							int i_1004_ = i_1002_ + i_1003_;
							int i_1005_ = (i_1002_ & 0xff00ff) + (i_1003_ & 0xff00ff);
							int i_1006_ = ((i_1005_ & 0x1000100) + (i_1004_ - i_1005_ & 0x10000));
							i_1002_ = i_1004_ - i_1006_ | i_1006_ - (i_1006_ >>> 8);
							i_1006_ = is_948_[i_947_];
							i_1004_ = i_1002_ + i_1006_;
							i_1005_ = (i_1002_ & 0xff00ff) + (i_1006_ & 0xff00ff);
							i_1006_ = (i_1005_ & 0x1000100) + (i_1004_ - i_1005_ & 0x10000);
							is_948_[i_947_] = i_1004_ - i_1006_ | i_1006_ - (i_1006_ >>> 8);
						} else if (i_949_ == 2) {
							int i_1007_ = anIntArray6690[i_946_];
							int i_1008_ = (((i_1007_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
							int i_1009_ = ((i_1007_ & 0xff00) * Class397_Sub1.anInt5755 & 0xff0000);
							i_1007_ = (((i_1008_ | i_1009_) >>> 8) + Class397_Sub1.anInt5773);
							int i_1010_ = is_948_[i_947_];
							int i_1011_ = i_1007_ + i_1010_;
							int i_1012_ = (i_1007_ & 0xff00ff) + (i_1010_ & 0xff00ff);
							i_1010_ = (i_1012_ & 0x1000100) + (i_1011_ - i_1012_ & 0x10000);
							is_948_[i_947_] = i_1011_ - i_1010_ | i_1010_ - (i_1010_ >>> 8);
						}
					} else
						throw new IllegalArgumentException();
					i_941_ += Class397_Sub1.anInt5766;
					i_942_ += Class397_Sub1.anInt5777;
				}
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		}
	}

	public void method4094(int i, int i_1013_, int i_1014_, int i_1015_, int i_1016_, int i_1017_, int i_1018_,
			int i_1019_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		if (i_1014_ > 0 && i_1015_ > 0) {
			int i_1020_ = 0;
			int i_1021_ = 0;
			int i_1022_ = aHa_Sub2_5768.anInt4084;
			int i_1023_ = anInt5772 + anInt5758 + anInt5757;
			int i_1024_ = anInt5761 + anInt5756 + anInt5750;
			int i_1025_ = (i_1023_ << 16) / i_1014_;
			int i_1026_ = (i_1024_ << 16) / i_1015_;
			if (anInt5772 > 0) {
				int i_1027_ = ((anInt5772 << 16) + i_1025_ - 1) / i_1025_;
				i += i_1027_;
				i_1020_ += i_1027_ * i_1025_ - (anInt5772 << 16);
			}
			if (anInt5761 > 0) {
				int i_1028_ = ((anInt5761 << 16) + i_1026_ - 1) / i_1026_;
				i_1013_ += i_1028_;
				i_1021_ += i_1028_ * i_1026_ - (anInt5761 << 16);
			}
			if (anInt5758 < i_1023_)
				i_1014_ = ((anInt5758 << 16) - i_1020_ + i_1025_ - 1) / i_1025_;
			if (anInt5756 < i_1024_)
				i_1015_ = ((anInt5756 << 16) - i_1021_ + i_1026_ - 1) / i_1026_;
			int i_1029_ = i + i_1013_ * i_1022_;
			int i_1030_ = i_1022_ - i_1014_;
			if (i_1013_ + i_1015_ > aHa_Sub2_5768.anInt4078)
				i_1015_ -= i_1013_ + i_1015_ - aHa_Sub2_5768.anInt4078;
			if (i_1013_ < aHa_Sub2_5768.anInt4085) {
				int i_1031_ = aHa_Sub2_5768.anInt4085 - i_1013_;
				i_1015_ -= i_1031_;
				i_1029_ += i_1031_ * i_1022_;
				i_1021_ += i_1026_ * i_1031_;
			}
			if (i + i_1014_ > aHa_Sub2_5768.anInt4104) {
				int i_1032_ = i + i_1014_ - aHa_Sub2_5768.anInt4104;
				i_1014_ -= i_1032_;
				i_1030_ += i_1032_;
			}
			if (i < aHa_Sub2_5768.anInt4079) {
				int i_1033_ = aHa_Sub2_5768.anInt4079 - i;
				i_1014_ -= i_1033_;
				i_1029_ += i_1033_;
				i_1020_ += i_1025_ * i_1033_;
				i_1030_ += i_1033_;
			}
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_1018_ == 0) {
				if (i_1016_ == 1) {
					int i_1034_ = i_1020_;
					for (int i_1035_ = -i_1015_; i_1035_ < 0; i_1035_++) {
						int i_1036_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1037_ = -i_1014_; i_1037_ < 0; i_1037_++) {
							is[i_1029_++] = anIntArray6690[(i_1020_ >> 16) + i_1036_];
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1034_;
						i_1029_ += i_1030_;
					}
					return;
				}
				if (i_1016_ == 0) {
					int i_1038_ = (i_1017_ & 0xff0000) >> 16;
					int i_1039_ = (i_1017_ & 0xff00) >> 8;
					int i_1040_ = i_1017_ & 0xff;
					int i_1041_ = i_1020_;
					for (int i_1042_ = -i_1015_; i_1042_ < 0; i_1042_++) {
						int i_1043_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1044_ = -i_1014_; i_1044_ < 0; i_1044_++) {
							int i_1045_ = anIntArray6690[(i_1020_ >> 16) + i_1043_];
							int i_1046_ = (i_1045_ & 0xff0000) * i_1038_ & ~0xffffff;
							int i_1047_ = (i_1045_ & 0xff00) * i_1039_ & 0xff0000;
							int i_1048_ = (i_1045_ & 0xff) * i_1040_ & 0xff00;
							is[i_1029_++] = (i_1046_ | i_1047_ | i_1048_) >>> 8;
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1041_;
						i_1029_ += i_1030_;
					}
					return;
				}
				if (i_1016_ == 3) {
					int i_1049_ = i_1020_;
					for (int i_1050_ = -i_1015_; i_1050_ < 0; i_1050_++) {
						int i_1051_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1052_ = -i_1014_; i_1052_ < 0; i_1052_++) {
							int i_1053_ = anIntArray6690[(i_1020_ >> 16) + i_1051_];
							int i_1054_ = i_1053_ + i_1017_;
							int i_1055_ = (i_1053_ & 0xff00ff) + (i_1017_ & 0xff00ff);
							int i_1056_ = ((i_1055_ & 0x1000100) + (i_1054_ - i_1055_ & 0x10000));
							is[i_1029_++] = i_1054_ - i_1056_ | i_1056_ - (i_1056_ >>> 8);
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1049_;
						i_1029_ += i_1030_;
					}
					return;
				}
				if (i_1016_ == 2) {
					int i_1057_ = i_1017_ >>> 24;
					int i_1058_ = 256 - i_1057_;
					int i_1059_ = (i_1017_ & 0xff00ff) * i_1058_ & ~0xff00ff;
					int i_1060_ = (i_1017_ & 0xff00) * i_1058_ & 0xff0000;
					i_1017_ = (i_1059_ | i_1060_) >>> 8;
					int i_1061_ = i_1020_;
					for (int i_1062_ = -i_1015_; i_1062_ < 0; i_1062_++) {
						int i_1063_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1064_ = -i_1014_; i_1064_ < 0; i_1064_++) {
							int i_1065_ = anIntArray6690[(i_1020_ >> 16) + i_1063_];
							i_1059_ = (i_1065_ & 0xff00ff) * i_1057_ & ~0xff00ff;
							i_1060_ = (i_1065_ & 0xff00) * i_1057_ & 0xff0000;
							is[i_1029_++] = ((i_1059_ | i_1060_) >>> 8) + i_1017_;
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1061_;
						i_1029_ += i_1030_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_1018_ == 1) {
				if (i_1016_ == 1) {
					int i_1066_ = i_1020_;
					for (int i_1067_ = -i_1015_; i_1067_ < 0; i_1067_++) {
						int i_1068_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1069_ = -i_1014_; i_1069_ < 0; i_1069_++) {
							int i_1070_ = anIntArray6690[(i_1020_ >> 16) + i_1068_];
							int i_1071_ = i_1070_ >>> 24;
							int i_1072_ = 256 - i_1071_;
							int i_1073_ = is[i_1029_];
							is[i_1029_++] = (((((i_1070_ & 0xff00ff) * i_1071_ + (i_1073_ & 0xff00ff) * i_1072_)
									& ~0xff00ff) >> 8)
									+ ((((i_1070_ & ~0xff00ff) >>> 8) * i_1071_
											+ (((i_1073_ & ~0xff00ff) >>> 8) * i_1072_)) & ~0xff00ff));
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1066_;
						i_1029_ += i_1030_;
					}
					return;
				}
				if (i_1016_ == 0) {
					int i_1074_ = i_1020_;
					if ((i_1017_ & 0xffffff) == 16777215) {
						for (int i_1075_ = -i_1015_; i_1075_ < 0; i_1075_++) {
							int i_1076_ = (i_1021_ >> 16) * anInt5758;
							for (int i_1077_ = -i_1014_; i_1077_ < 0; i_1077_++) {
								int i_1078_ = (anIntArray6690[(i_1020_ >> 16) + i_1076_]);
								int i_1079_ = (i_1078_ >>> 24) * (i_1017_ >>> 24) >> 8;
								int i_1080_ = 256 - i_1079_;
								int i_1081_ = is[i_1029_];
								is[i_1029_++] = ((((i_1078_ & 0xff00ff) * i_1079_ + (i_1081_ & 0xff00ff) * i_1080_)
										& ~0xff00ff)
										+ (((i_1078_ & 0xff00) * i_1079_ + (i_1081_ & 0xff00) * i_1080_)
												& 0xff0000)) >> 8;
								i_1020_ += i_1025_;
							}
							i_1021_ += i_1026_;
							i_1020_ = i_1074_;
							i_1029_ += i_1030_;
						}
					} else {
						int i_1082_ = (i_1017_ & 0xff0000) >> 16;
						int i_1083_ = (i_1017_ & 0xff00) >> 8;
						int i_1084_ = i_1017_ & 0xff;
						for (int i_1085_ = -i_1015_; i_1085_ < 0; i_1085_++) {
							int i_1086_ = (i_1021_ >> 16) * anInt5758;
							for (int i_1087_ = -i_1014_; i_1087_ < 0; i_1087_++) {
								int i_1088_ = (anIntArray6690[(i_1020_ >> 16) + i_1086_]);
								int i_1089_ = (i_1088_ >>> 24) * (i_1017_ >>> 24) >> 8;
								int i_1090_ = 256 - i_1089_;
								if (i_1089_ != 255) {
									int i_1091_ = ((i_1088_ & 0xff0000) * i_1082_ & ~0xffffff);
									int i_1092_ = ((i_1088_ & 0xff00) * i_1083_ & 0xff0000);
									int i_1093_ = (i_1088_ & 0xff) * i_1084_ & 0xff00;
									i_1088_ = (i_1091_ | i_1092_ | i_1093_) >>> 8;
									int i_1094_ = is[i_1029_];
									is[i_1029_++] = ((((i_1088_ & 0xff00ff) * i_1089_ + (i_1094_ & 0xff00ff) * i_1090_)
											& ~0xff00ff)
											+ (((i_1088_ & 0xff00) * i_1089_ + (i_1094_ & 0xff00) * i_1090_)
													& 0xff0000)) >> 8;
								} else {
									int i_1095_ = ((i_1088_ & 0xff0000) * i_1082_ & ~0xffffff);
									int i_1096_ = ((i_1088_ & 0xff00) * i_1083_ & 0xff0000);
									int i_1097_ = (i_1088_ & 0xff) * i_1084_ & 0xff00;
									is[i_1029_++] = (i_1095_ | i_1096_ | i_1097_) >>> 8;
								}
								i_1020_ += i_1025_;
							}
							i_1021_ += i_1026_;
							i_1020_ = i_1074_;
							i_1029_ += i_1030_;
						}
						return;
					}
					return;
				}
				if (i_1016_ == 3) {
					int i_1098_ = i_1020_;
					for (int i_1099_ = -i_1015_; i_1099_ < 0; i_1099_++) {
						int i_1100_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1101_ = -i_1014_; i_1101_ < 0; i_1101_++) {
							int i_1102_ = anIntArray6690[(i_1020_ >> 16) + i_1100_];
							int i_1103_ = i_1102_ + i_1017_;
							int i_1104_ = (i_1102_ & 0xff00ff) + (i_1017_ & 0xff00ff);
							int i_1105_ = ((i_1104_ & 0x1000100) + (i_1103_ - i_1104_ & 0x10000));
							i_1105_ = i_1103_ - i_1105_ | i_1105_ - (i_1105_ >>> 8);
							int i_1106_ = (i_1105_ >>> 24) * (i_1017_ >>> 24) >> 8;
							int i_1107_ = 256 - i_1106_;
							if (i_1106_ != 255) {
								i_1102_ = i_1105_;
								i_1105_ = is[i_1029_];
								i_1105_ = ((((i_1102_ & 0xff00ff) * i_1106_ + (i_1105_ & 0xff00ff) * i_1107_)
										& ~0xff00ff)
										+ (((i_1102_ & 0xff00) * i_1106_ + (i_1105_ & 0xff00) * i_1107_)
												& 0xff0000)) >> 8;
							}
							is[i_1029_++] = i_1105_;
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1098_;
						i_1029_ += i_1030_;
					}
					return;
				}
				if (i_1016_ == 2) {
					int i_1108_ = i_1017_ >>> 24;
					int i_1109_ = 256 - i_1108_;
					int i_1110_ = (i_1017_ & 0xff00ff) * i_1109_ & ~0xff00ff;
					int i_1111_ = (i_1017_ & 0xff00) * i_1109_ & 0xff0000;
					i_1017_ = (i_1110_ | i_1111_) >>> 8;
					int i_1112_ = i_1020_;
					for (int i_1113_ = -i_1015_; i_1113_ < 0; i_1113_++) {
						int i_1114_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1115_ = -i_1014_; i_1115_ < 0; i_1115_++) {
							int i_1116_ = anIntArray6690[(i_1020_ >> 16) + i_1114_];
							int i_1117_ = i_1116_ >>> 24;
							int i_1118_ = 256 - i_1117_;
							i_1110_ = (i_1116_ & 0xff00ff) * i_1108_ & ~0xff00ff;
							i_1111_ = (i_1116_ & 0xff00) * i_1108_ & 0xff0000;
							i_1116_ = ((i_1110_ | i_1111_) >>> 8) + i_1017_;
							int i_1119_ = is[i_1029_];
							is[i_1029_++] = ((((i_1116_ & 0xff00ff) * i_1117_ + (i_1119_ & 0xff00ff) * i_1118_)
									& ~0xff00ff)
									+ (((i_1116_ & 0xff00) * i_1117_ + (i_1119_ & 0xff00) * i_1118_) & 0xff0000)) >> 8;
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1112_;
						i_1029_ += i_1030_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_1018_ == 2) {
				if (i_1016_ == 1) {
					int i_1120_ = i_1020_;
					for (int i_1121_ = -i_1015_; i_1121_ < 0; i_1121_++) {
						int i_1122_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1123_ = -i_1014_; i_1123_ < 0; i_1123_++) {
							int i_1124_ = anIntArray6690[(i_1020_ >> 16) + i_1122_];
							if (i_1124_ != 0) {
								int i_1125_ = is[i_1029_];
								int i_1126_ = i_1124_ + i_1125_;
								int i_1127_ = ((i_1124_ & 0xff00ff) + (i_1125_ & 0xff00ff));
								i_1125_ = ((i_1127_ & 0x1000100) + (i_1126_ - i_1127_ & 0x10000));
								is[i_1029_++] = i_1126_ - i_1125_ | i_1125_ - (i_1125_ >>> 8);
							} else
								i_1029_++;
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1120_;
						i_1029_ += i_1030_;
					}
					return;
				}
				if (i_1016_ == 0) {
					int i_1128_ = i_1020_;
					int i_1129_ = (i_1017_ & 0xff0000) >> 16;
					int i_1130_ = (i_1017_ & 0xff00) >> 8;
					int i_1131_ = i_1017_ & 0xff;
					for (int i_1132_ = -i_1015_; i_1132_ < 0; i_1132_++) {
						int i_1133_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1134_ = -i_1014_; i_1134_ < 0; i_1134_++) {
							int i_1135_ = anIntArray6690[(i_1020_ >> 16) + i_1133_];
							if (i_1135_ != 0) {
								int i_1136_ = ((i_1135_ & 0xff0000) * i_1129_ & ~0xffffff);
								int i_1137_ = (i_1135_ & 0xff00) * i_1130_ & 0xff0000;
								int i_1138_ = (i_1135_ & 0xff) * i_1131_ & 0xff00;
								i_1135_ = (i_1136_ | i_1137_ | i_1138_) >>> 8;
								int i_1139_ = is[i_1029_];
								int i_1140_ = i_1135_ + i_1139_;
								int i_1141_ = ((i_1135_ & 0xff00ff) + (i_1139_ & 0xff00ff));
								i_1139_ = ((i_1141_ & 0x1000100) + (i_1140_ - i_1141_ & 0x10000));
								is[i_1029_++] = i_1140_ - i_1139_ | i_1139_ - (i_1139_ >>> 8);
							} else
								i_1029_++;
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1128_;
						i_1029_ += i_1030_;
					}
					return;
				}
				if (i_1016_ == 3) {
					int i_1142_ = i_1020_;
					for (int i_1143_ = -i_1015_; i_1143_ < 0; i_1143_++) {
						int i_1144_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1145_ = -i_1014_; i_1145_ < 0; i_1145_++) {
							int i_1146_ = anIntArray6690[(i_1020_ >> 16) + i_1144_];
							int i_1147_ = i_1146_ + i_1017_;
							int i_1148_ = (i_1146_ & 0xff00ff) + (i_1017_ & 0xff00ff);
							int i_1149_ = ((i_1148_ & 0x1000100) + (i_1147_ - i_1148_ & 0x10000));
							i_1146_ = i_1147_ - i_1149_ | i_1149_ - (i_1149_ >>> 8);
							i_1149_ = is[i_1029_];
							i_1147_ = i_1146_ + i_1149_;
							i_1148_ = (i_1146_ & 0xff00ff) + (i_1149_ & 0xff00ff);
							i_1149_ = (i_1148_ & 0x1000100) + (i_1147_ - i_1148_ & 0x10000);
							is[i_1029_++] = i_1147_ - i_1149_ | i_1149_ - (i_1149_ >>> 8);
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1142_;
						i_1029_ += i_1030_;
					}
					return;
				}
				if (i_1016_ == 2) {
					int i_1150_ = i_1017_ >>> 24;
					int i_1151_ = 256 - i_1150_;
					int i_1152_ = (i_1017_ & 0xff00ff) * i_1151_ & ~0xff00ff;
					int i_1153_ = (i_1017_ & 0xff00) * i_1151_ & 0xff0000;
					i_1017_ = (i_1152_ | i_1153_) >>> 8;
					int i_1154_ = i_1020_;
					for (int i_1155_ = -i_1015_; i_1155_ < 0; i_1155_++) {
						int i_1156_ = (i_1021_ >> 16) * anInt5758;
						for (int i_1157_ = -i_1014_; i_1157_ < 0; i_1157_++) {
							int i_1158_ = anIntArray6690[(i_1020_ >> 16) + i_1156_];
							if (i_1158_ != 0) {
								i_1152_ = ((i_1158_ & 0xff00ff) * i_1150_ & ~0xff00ff);
								i_1153_ = (i_1158_ & 0xff00) * i_1150_ & 0xff0000;
								i_1158_ = ((i_1152_ | i_1153_) >>> 8) + i_1017_;
								int i_1159_ = is[i_1029_];
								int i_1160_ = i_1158_ + i_1159_;
								int i_1161_ = ((i_1158_ & 0xff00ff) + (i_1159_ & 0xff00ff));
								i_1159_ = ((i_1161_ & 0x1000100) + (i_1160_ - i_1161_ & 0x10000));
								is[i_1029_++] = i_1160_ - i_1159_ | i_1159_ - (i_1159_ >>> 8);
							} else
								i_1029_++;
							i_1020_ += i_1025_;
						}
						i_1021_ += i_1026_;
						i_1020_ = i_1154_;
						i_1029_ += i_1030_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	public void method4079(int i, int i_1162_, int i_1163_, int i_1164_, int i_1165_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		int i_1166_ = aHa_Sub2_5768.anInt4084;
		i += anInt5772;
		i_1162_ += anInt5761;
		int i_1167_ = i_1162_ * i_1166_ + i;
		int i_1168_ = 0;
		int i_1169_ = anInt5756;
		int i_1170_ = anInt5758;
		int i_1171_ = i_1166_ - i_1170_;
		int i_1172_ = 0;
		if (i_1162_ < aHa_Sub2_5768.anInt4085) {
			int i_1173_ = aHa_Sub2_5768.anInt4085 - i_1162_;
			i_1169_ -= i_1173_;
			i_1162_ = aHa_Sub2_5768.anInt4085;
			i_1168_ += i_1173_ * i_1170_;
			i_1167_ += i_1173_ * i_1166_;
		}
		if (i_1162_ + i_1169_ > aHa_Sub2_5768.anInt4078)
			i_1169_ -= i_1162_ + i_1169_ - aHa_Sub2_5768.anInt4078;
		if (i < aHa_Sub2_5768.anInt4079) {
			int i_1174_ = aHa_Sub2_5768.anInt4079 - i;
			i_1170_ -= i_1174_;
			i = aHa_Sub2_5768.anInt4079;
			i_1168_ += i_1174_;
			i_1167_ += i_1174_;
			i_1172_ += i_1174_;
			i_1171_ += i_1174_;
		}
		if (i + i_1170_ > aHa_Sub2_5768.anInt4104) {
			int i_1175_ = i + i_1170_ - aHa_Sub2_5768.anInt4104;
			i_1170_ -= i_1175_;
			i_1172_ += i_1175_;
			i_1171_ += i_1175_;
		}
		if (i_1170_ > 0 && i_1169_ > 0) {
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_1165_ == 0) {
				if (i_1163_ == 1) {
					for (int i_1176_ = -i_1169_; i_1176_ < 0; i_1176_++) {
						int i_1177_ = i_1167_ + i_1170_ - 3;
						while (i_1167_ < i_1177_) {
							is[i_1167_++] = anIntArray6690[i_1168_++];
							is[i_1167_++] = anIntArray6690[i_1168_++];
							is[i_1167_++] = anIntArray6690[i_1168_++];
							is[i_1167_++] = anIntArray6690[i_1168_++];
						}
						i_1177_ += 3;
						while (i_1167_ < i_1177_)
							is[i_1167_++] = anIntArray6690[i_1168_++];
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				if (i_1163_ == 0) {
					int i_1178_ = (i_1164_ & 0xff0000) >> 16;
					int i_1179_ = (i_1164_ & 0xff00) >> 8;
					int i_1180_ = i_1164_ & 0xff;
					for (int i_1181_ = -i_1169_; i_1181_ < 0; i_1181_++) {
						for (int i_1182_ = -i_1170_; i_1182_ < 0; i_1182_++) {
							int i_1183_ = anIntArray6690[i_1168_++];
							int i_1184_ = (i_1183_ & 0xff0000) * i_1178_ & ~0xffffff;
							int i_1185_ = (i_1183_ & 0xff00) * i_1179_ & 0xff0000;
							int i_1186_ = (i_1183_ & 0xff) * i_1180_ & 0xff00;
							is[i_1167_++] = (i_1184_ | i_1185_ | i_1186_) >>> 8;
						}
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				if (i_1163_ == 3) {
					for (int i_1187_ = -i_1169_; i_1187_ < 0; i_1187_++) {
						for (int i_1188_ = -i_1170_; i_1188_ < 0; i_1188_++) {
							int i_1189_ = anIntArray6690[i_1168_++];
							int i_1190_ = i_1189_ + i_1164_;
							int i_1191_ = (i_1189_ & 0xff00ff) + (i_1164_ & 0xff00ff);
							int i_1192_ = ((i_1191_ & 0x1000100) + (i_1190_ - i_1191_ & 0x10000));
							is[i_1167_++] = i_1190_ - i_1192_ | i_1192_ - (i_1192_ >>> 8);
						}
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				if (i_1163_ == 2) {
					int i_1193_ = i_1164_ >>> 24;
					int i_1194_ = 256 - i_1193_;
					int i_1195_ = (i_1164_ & 0xff00ff) * i_1194_ & ~0xff00ff;
					int i_1196_ = (i_1164_ & 0xff00) * i_1194_ & 0xff0000;
					i_1164_ = (i_1195_ | i_1196_) >>> 8;
					for (int i_1197_ = -i_1169_; i_1197_ < 0; i_1197_++) {
						for (int i_1198_ = -i_1170_; i_1198_ < 0; i_1198_++) {
							int i_1199_ = anIntArray6690[i_1168_++];
							i_1195_ = (i_1199_ & 0xff00ff) * i_1193_ & ~0xff00ff;
							i_1196_ = (i_1199_ & 0xff00) * i_1193_ & 0xff0000;
							is[i_1167_++] = ((i_1195_ | i_1196_) >>> 8) + i_1164_;
						}
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_1165_ == 1) {
				if (i_1163_ == 1) {
					for (int i_1200_ = -i_1169_; i_1200_ < 0; i_1200_++) {
						for (int i_1201_ = -i_1170_; i_1201_ < 0; i_1201_++) {
							int i_1202_ = anIntArray6690[i_1168_++];
							int i_1203_ = i_1202_ >>> 24;
							int i_1204_ = 256 - i_1203_;
							int i_1205_ = is[i_1167_];
							is[i_1167_++] = (((((i_1202_ & 0xff00ff) * i_1203_ + (i_1205_ & 0xff00ff) * i_1204_)
									& ~0xff00ff) >> 8)
									+ ((((i_1202_ & ~0xff00ff) >>> 8) * i_1203_
											+ (((i_1205_ & ~0xff00ff) >>> 8) * i_1204_)) & ~0xff00ff));
						}
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				if (i_1163_ == 0) {
					if ((i_1164_ & 0xffffff) == 16777215) {
						for (int i_1206_ = -i_1169_; i_1206_ < 0; i_1206_++) {
							for (int i_1207_ = -i_1170_; i_1207_ < 0; i_1207_++) {
								int i_1208_ = anIntArray6690[i_1168_++];
								int i_1209_ = (i_1208_ >>> 24) * (i_1164_ >>> 24) >> 8;
								int i_1210_ = 256 - i_1209_;
								int i_1211_ = is[i_1167_];
								is[i_1167_++] = ((((i_1208_ & 0xff00ff) * i_1209_ + (i_1211_ & 0xff00ff) * i_1210_)
										& ~0xff00ff)
										+ (((i_1208_ & 0xff00) * i_1209_ + (i_1211_ & 0xff00) * i_1210_)
												& 0xff0000)) >> 8;
							}
							i_1167_ += i_1171_;
							i_1168_ += i_1172_;
						}
					} else {
						int i_1212_ = (i_1164_ & 0xff0000) >> 16;
						int i_1213_ = (i_1164_ & 0xff00) >> 8;
						int i_1214_ = i_1164_ & 0xff;
						for (int i_1215_ = -i_1169_; i_1215_ < 0; i_1215_++) {
							for (int i_1216_ = -i_1170_; i_1216_ < 0; i_1216_++) {
								int i_1217_ = anIntArray6690[i_1168_++];
								int i_1218_ = (i_1217_ >>> 24) * (i_1164_ >>> 24) >> 8;
								int i_1219_ = 256 - i_1218_;
								if (i_1218_ != 255) {
									int i_1220_ = ((i_1217_ & 0xff0000) * i_1212_ & ~0xffffff);
									int i_1221_ = ((i_1217_ & 0xff00) * i_1213_ & 0xff0000);
									int i_1222_ = (i_1217_ & 0xff) * i_1214_ & 0xff00;
									i_1217_ = (i_1220_ | i_1221_ | i_1222_) >>> 8;
									int i_1223_ = is[i_1167_];
									is[i_1167_++] = ((((i_1217_ & 0xff00ff) * i_1218_ + (i_1223_ & 0xff00ff) * i_1219_)
											& ~0xff00ff)
											+ (((i_1217_ & 0xff00) * i_1218_ + (i_1223_ & 0xff00) * i_1219_)
													& 0xff0000)) >> 8;
								} else {
									int i_1224_ = ((i_1217_ & 0xff0000) * i_1212_ & ~0xffffff);
									int i_1225_ = ((i_1217_ & 0xff00) * i_1213_ & 0xff0000);
									int i_1226_ = (i_1217_ & 0xff) * i_1214_ & 0xff00;
									is[i_1167_++] = (i_1224_ | i_1225_ | i_1226_) >>> 8;
								}
							}
							i_1167_ += i_1171_;
							i_1168_ += i_1172_;
						}
						return;
					}
					return;
				}
				if (i_1163_ == 3) {
					for (int i_1227_ = -i_1169_; i_1227_ < 0; i_1227_++) {
						for (int i_1228_ = -i_1170_; i_1228_ < 0; i_1228_++) {
							int i_1229_ = anIntArray6690[i_1168_++];
							int i_1230_ = i_1229_ + i_1164_;
							int i_1231_ = (i_1229_ & 0xff00ff) + (i_1164_ & 0xff00ff);
							int i_1232_ = ((i_1231_ & 0x1000100) + (i_1230_ - i_1231_ & 0x10000));
							i_1232_ = i_1230_ - i_1232_ | i_1232_ - (i_1232_ >>> 8);
							int i_1233_ = (i_1232_ >>> 24) * (i_1164_ >>> 24) >> 8;
							int i_1234_ = 256 - i_1233_;
							if (i_1233_ != 255) {
								i_1229_ = i_1232_;
								i_1232_ = is[i_1167_];
								i_1232_ = ((((i_1229_ & 0xff00ff) * i_1233_ + (i_1232_ & 0xff00ff) * i_1234_)
										& ~0xff00ff)
										+ (((i_1229_ & 0xff00) * i_1233_ + (i_1232_ & 0xff00) * i_1234_)
												& 0xff0000)) >> 8;
							}
							is[i_1167_++] = i_1232_;
						}
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				if (i_1163_ == 2) {
					int i_1235_ = i_1164_ >>> 24;
					int i_1236_ = 256 - i_1235_;
					int i_1237_ = (i_1164_ & 0xff00ff) * i_1236_ & ~0xff00ff;
					int i_1238_ = (i_1164_ & 0xff00) * i_1236_ & 0xff0000;
					i_1164_ = (i_1237_ | i_1238_) >>> 8;
					for (int i_1239_ = -i_1169_; i_1239_ < 0; i_1239_++) {
						for (int i_1240_ = -i_1170_; i_1240_ < 0; i_1240_++) {
							int i_1241_ = anIntArray6690[i_1168_++];
							int i_1242_ = i_1241_ >>> 24;
							int i_1243_ = 256 - i_1242_;
							i_1237_ = (i_1241_ & 0xff00ff) * i_1235_ & ~0xff00ff;
							i_1238_ = (i_1241_ & 0xff00) * i_1235_ & 0xff0000;
							i_1241_ = ((i_1237_ | i_1238_) >>> 8) + i_1164_;
							int i_1244_ = is[i_1167_];
							is[i_1167_++] = ((((i_1241_ & 0xff00ff) * i_1242_ + (i_1244_ & 0xff00ff) * i_1243_)
									& ~0xff00ff)
									+ (((i_1241_ & 0xff00) * i_1242_ + (i_1244_ & 0xff00) * i_1243_) & 0xff0000)) >> 8;
						}
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_1165_ == 2) {
				if (i_1163_ == 1) {
					for (int i_1245_ = -i_1169_; i_1245_ < 0; i_1245_++) {
						for (int i_1246_ = -i_1170_; i_1246_ < 0; i_1246_++) {
							int i_1247_ = anIntArray6690[i_1168_++];
							if (i_1247_ != 0) {
								int i_1248_ = is[i_1167_];
								int i_1249_ = i_1247_ + i_1248_;
								int i_1250_ = ((i_1247_ & 0xff00ff) + (i_1248_ & 0xff00ff));
								i_1248_ = ((i_1250_ & 0x1000100) + (i_1249_ - i_1250_ & 0x10000));
								is[i_1167_++] = i_1249_ - i_1248_ | i_1248_ - (i_1248_ >>> 8);
							} else
								i_1167_++;
						}
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				if (i_1163_ == 0) {
					int i_1251_ = (i_1164_ & 0xff0000) >> 16;
					int i_1252_ = (i_1164_ & 0xff00) >> 8;
					int i_1253_ = i_1164_ & 0xff;
					for (int i_1254_ = -i_1169_; i_1254_ < 0; i_1254_++) {
						for (int i_1255_ = -i_1170_; i_1255_ < 0; i_1255_++) {
							int i_1256_ = anIntArray6690[i_1168_++];
							if (i_1256_ != 0) {
								int i_1257_ = ((i_1256_ & 0xff0000) * i_1251_ & ~0xffffff);
								int i_1258_ = (i_1256_ & 0xff00) * i_1252_ & 0xff0000;
								int i_1259_ = (i_1256_ & 0xff) * i_1253_ & 0xff00;
								i_1256_ = (i_1257_ | i_1258_ | i_1259_) >>> 8;
								int i_1260_ = is[i_1167_];
								int i_1261_ = i_1256_ + i_1260_;
								int i_1262_ = ((i_1256_ & 0xff00ff) + (i_1260_ & 0xff00ff));
								i_1260_ = ((i_1262_ & 0x1000100) + (i_1261_ - i_1262_ & 0x10000));
								is[i_1167_++] = i_1261_ - i_1260_ | i_1260_ - (i_1260_ >>> 8);
							} else
								i_1167_++;
						}
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				if (i_1163_ == 3) {
					for (int i_1263_ = -i_1169_; i_1263_ < 0; i_1263_++) {
						for (int i_1264_ = -i_1170_; i_1264_ < 0; i_1264_++) {
							int i_1265_ = anIntArray6690[i_1168_++];
							int i_1266_ = i_1265_ + i_1164_;
							int i_1267_ = (i_1265_ & 0xff00ff) + (i_1164_ & 0xff00ff);
							int i_1268_ = ((i_1267_ & 0x1000100) + (i_1266_ - i_1267_ & 0x10000));
							i_1265_ = i_1266_ - i_1268_ | i_1268_ - (i_1268_ >>> 8);
							i_1268_ = is[i_1167_];
							i_1266_ = i_1265_ + i_1268_;
							i_1267_ = (i_1265_ & 0xff00ff) + (i_1268_ & 0xff00ff);
							i_1268_ = (i_1267_ & 0x1000100) + (i_1266_ - i_1267_ & 0x10000);
							is[i_1167_++] = i_1266_ - i_1268_ | i_1268_ - (i_1268_ >>> 8);
						}
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				if (i_1163_ == 2) {
					int i_1269_ = i_1164_ >>> 24;
					int i_1270_ = 256 - i_1269_;
					int i_1271_ = (i_1164_ & 0xff00ff) * i_1270_ & ~0xff00ff;
					int i_1272_ = (i_1164_ & 0xff00) * i_1270_ & 0xff0000;
					i_1164_ = (i_1271_ | i_1272_) >>> 8;
					for (int i_1273_ = -i_1169_; i_1273_ < 0; i_1273_++) {
						for (int i_1274_ = -i_1170_; i_1274_ < 0; i_1274_++) {
							int i_1275_ = anIntArray6690[i_1168_++];
							if (i_1275_ != 0) {
								i_1271_ = ((i_1275_ & 0xff00ff) * i_1269_ & ~0xff00ff);
								i_1272_ = (i_1275_ & 0xff00) * i_1269_ & 0xff0000;
								i_1275_ = ((i_1271_ | i_1272_) >>> 8) + i_1164_;
								int i_1276_ = is[i_1167_];
								int i_1277_ = i_1275_ + i_1276_;
								int i_1278_ = ((i_1275_ & 0xff00ff) + (i_1276_ & 0xff00ff));
								i_1276_ = ((i_1278_ & 0x1000100) + (i_1277_ - i_1278_ & 0x10000));
								is[i_1167_++] = i_1277_ - i_1276_ | i_1276_ - (i_1276_ >>> 8);
							} else
								i_1167_++;
						}
						i_1167_ += i_1171_;
						i_1168_ += i_1172_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	public void method4090(int i, int i_1279_, int i_1280_, int i_1281_, int i_1282_, int i_1283_) {
		int[] is = aHa_Sub2_5768.anIntArray4108;
		for (int i_1284_ = 0; i_1284_ < i_1281_; i_1284_++) {
			int i_1285_ = (i_1279_ + i_1284_) * i_1280_ + i;
			int i_1286_ = (i_1283_ + i_1284_) * i_1280_ + i_1282_;
			for (int i_1287_ = 0; i_1287_ < i_1280_; i_1287_++)
				anIntArray6690[i_1285_ + i_1287_] = is[i_1286_ + i_1287_] & 0xffffff;
		}
	}

	Class397_Sub1_Sub1(ha_Sub2 var_ha_Sub2, int[] is, int i, int i_1288_) {
		super(var_ha_Sub2, i, i_1288_);
		anIntArray6690 = is;
	}

	public void method4102(int[] is, int[] is_1289_, int i, int i_1290_) {
		int[] is_1291_ = aHa_Sub2_5768.anIntArray4108;
		if (Class397_Sub1.anInt5766 == 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_1292_ = Class397_Sub1.anInt5771;
				while (i_1292_ < 0) {
					int i_1293_ = i_1292_ + i_1290_;
					if (i_1293_ >= 0) {
						if (i_1293_ >= is.length)
							break;
						int i_1294_ = Class397_Sub1.anInt5759;
						int i_1295_ = Class397_Sub1.anInt5765;
						int i_1296_ = Class397_Sub1.anInt5775;
						int i_1297_ = Class397_Sub1.anInt5770;
						if (i_1295_ >= 0 && i_1296_ >= 0 && i_1295_ - (anInt5758 << 12) < 0
								&& i_1296_ - (anInt5756 << 12) < 0) {
							int i_1298_ = is[i_1293_] - i;
							int i_1299_ = -is_1289_[i_1293_];
							int i_1300_ = (i_1298_ - (i_1294_ - Class397_Sub1.anInt5759));
							if (i_1300_ > 0) {
								i_1294_ += i_1300_;
								i_1297_ += i_1300_;
								i_1295_ += Class397_Sub1.anInt5766 * i_1300_;
								i_1296_ += Class397_Sub1.anInt5777 * i_1300_;
							} else
								i_1299_ -= i_1300_;
							if (i_1297_ < i_1299_)
								i_1297_ = i_1299_;
							for (/**/; i_1297_ < 0; i_1297_++) {
								int i_1301_ = (anIntArray6690[((i_1296_ >> 12) * anInt5758 + (i_1295_ >> 12))]);
								int i_1302_ = i_1301_ >>> 24;
								int i_1303_ = 256 - i_1302_;
								int i_1304_ = is_1291_[i_1294_];
								is_1291_[i_1294_++] = ((((i_1301_ & 0xff00ff) * i_1302_
										+ (i_1304_ & 0xff00ff) * i_1303_) & ~0xff00ff)
										+ (((i_1301_ & 0xff00) * i_1302_ + (i_1304_ & 0xff00) * i_1303_)
												& 0xff0000)) >> 8;
							}
						}
					}
					i_1292_++;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_1305_ = Class397_Sub1.anInt5771;
				while (i_1305_ < 0) {
					int i_1306_ = i_1305_ + i_1290_;
					if (i_1306_ >= 0) {
						if (i_1306_ >= is.length)
							break;
						int i_1307_ = Class397_Sub1.anInt5759;
						int i_1308_ = Class397_Sub1.anInt5765;
						int i_1309_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1310_ = Class397_Sub1.anInt5770;
						if (i_1308_ >= 0 && i_1308_ - (anInt5758 << 12) < 0) {
							int i_1311_;
							if ((i_1311_ = i_1309_ - (anInt5756 << 12)) >= 0) {
								i_1311_ = ((Class397_Sub1.anInt5777 - i_1311_) / Class397_Sub1.anInt5777);
								i_1310_ += i_1311_;
								i_1309_ += Class397_Sub1.anInt5777 * i_1311_;
								i_1307_ += i_1311_;
							}
							if ((i_1311_ = ((i_1309_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_1310_)
								i_1310_ = i_1311_;
							int i_1312_ = is[i_1306_] - i;
							int i_1313_ = -is_1289_[i_1306_];
							int i_1314_ = (i_1312_ - (i_1307_ - Class397_Sub1.anInt5759));
							if (i_1314_ > 0) {
								i_1307_ += i_1314_;
								i_1310_ += i_1314_;
								i_1308_ += Class397_Sub1.anInt5766 * i_1314_;
								i_1309_ += Class397_Sub1.anInt5777 * i_1314_;
							} else
								i_1313_ -= i_1314_;
							if (i_1310_ < i_1313_)
								i_1310_ = i_1313_;
							for (/**/; i_1310_ < 0; i_1310_++) {
								int i_1315_ = (anIntArray6690[((i_1309_ >> 12) * anInt5758 + (i_1308_ >> 12))]);
								int i_1316_ = i_1315_ >>> 24;
								int i_1317_ = 256 - i_1316_;
								int i_1318_ = is_1291_[i_1307_];
								is_1291_[i_1307_++] = ((((i_1315_ & 0xff00ff) * i_1316_
										+ (i_1318_ & 0xff00ff) * i_1317_) & ~0xff00ff)
										+ (((i_1315_ & 0xff00) * i_1316_ + (i_1318_ & 0xff00) * i_1317_)
												& 0xff0000)) >> 8;
								i_1309_ += Class397_Sub1.anInt5777;
							}
						}
					}
					i_1305_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_1319_ = Class397_Sub1.anInt5771;
				while (i_1319_ < 0) {
					int i_1320_ = i_1319_ + i_1290_;
					if (i_1320_ >= 0) {
						if (i_1320_ >= is.length)
							break;
						int i_1321_ = Class397_Sub1.anInt5759;
						int i_1322_ = Class397_Sub1.anInt5765;
						int i_1323_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1324_ = Class397_Sub1.anInt5770;
						if (i_1322_ >= 0 && i_1322_ - (anInt5758 << 12) < 0) {
							if (i_1323_ < 0) {
								int i_1325_ = ((Class397_Sub1.anInt5777 - 1 - i_1323_) / Class397_Sub1.anInt5777);
								i_1324_ += i_1325_;
								i_1323_ += Class397_Sub1.anInt5777 * i_1325_;
								i_1321_ += i_1325_;
							}
							int i_1326_;
							if ((i_1326_ = ((i_1323_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
									/ Class397_Sub1.anInt5777)) > i_1324_)
								i_1324_ = i_1326_;
							int i_1327_ = is[i_1320_] - i;
							int i_1328_ = -is_1289_[i_1320_];
							int i_1329_ = (i_1327_ - (i_1321_ - Class397_Sub1.anInt5759));
							if (i_1329_ > 0) {
								i_1321_ += i_1329_;
								i_1324_ += i_1329_;
								i_1322_ += Class397_Sub1.anInt5766 * i_1329_;
								i_1323_ += Class397_Sub1.anInt5777 * i_1329_;
							} else
								i_1328_ -= i_1329_;
							if (i_1324_ < i_1328_)
								i_1324_ = i_1328_;
							for (/**/; i_1324_ < 0; i_1324_++) {
								int i_1330_ = (anIntArray6690[((i_1323_ >> 12) * anInt5758 + (i_1322_ >> 12))]);
								int i_1331_ = i_1330_ >>> 24;
								int i_1332_ = 256 - i_1331_;
								int i_1333_ = is_1291_[i_1321_];
								is_1291_[i_1321_++] = ((((i_1330_ & 0xff00ff) * i_1331_
										+ (i_1333_ & 0xff00ff) * i_1332_) & ~0xff00ff)
										+ (((i_1330_ & 0xff00) * i_1331_ + (i_1333_ & 0xff00) * i_1332_)
												& 0xff0000)) >> 8;
								i_1323_ += Class397_Sub1.anInt5777;
							}
						}
					}
					i_1319_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5766 < 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_1334_ = Class397_Sub1.anInt5771;
				while (i_1334_ < 0) {
					int i_1335_ = i_1334_ + i_1290_;
					if (i_1335_ >= 0) {
						if (i_1335_ >= is.length)
							break;
						int i_1336_ = Class397_Sub1.anInt5759;
						int i_1337_ = (Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764);
						int i_1338_ = Class397_Sub1.anInt5775;
						int i_1339_ = Class397_Sub1.anInt5770;
						if (i_1338_ >= 0 && i_1338_ - (anInt5756 << 12) < 0) {
							int i_1340_;
							if ((i_1340_ = i_1337_ - (anInt5758 << 12)) >= 0) {
								i_1340_ = ((Class397_Sub1.anInt5766 - i_1340_) / Class397_Sub1.anInt5766);
								i_1339_ += i_1340_;
								i_1337_ += Class397_Sub1.anInt5766 * i_1340_;
								i_1336_ += i_1340_;
							}
							if ((i_1340_ = ((i_1337_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_1339_)
								i_1339_ = i_1340_;
							int i_1341_ = is[i_1335_] - i;
							int i_1342_ = -is_1289_[i_1335_];
							int i_1343_ = (i_1341_ - (i_1336_ - Class397_Sub1.anInt5759));
							if (i_1343_ > 0) {
								i_1336_ += i_1343_;
								i_1339_ += i_1343_;
								i_1337_ += Class397_Sub1.anInt5766 * i_1343_;
								i_1338_ += Class397_Sub1.anInt5777 * i_1343_;
							} else
								i_1342_ -= i_1343_;
							if (i_1339_ < i_1342_)
								i_1339_ = i_1342_;
							for (/**/; i_1339_ < 0; i_1339_++) {
								int i_1344_ = (anIntArray6690[((i_1338_ >> 12) * anInt5758 + (i_1337_ >> 12))]);
								int i_1345_ = i_1344_ >>> 24;
								int i_1346_ = 256 - i_1345_;
								int i_1347_ = is_1291_[i_1336_];
								is_1291_[i_1336_++] = ((((i_1344_ & 0xff00ff) * i_1345_
										+ (i_1347_ & 0xff00ff) * i_1346_) & ~0xff00ff)
										+ (((i_1344_ & 0xff00) * i_1345_ + (i_1347_ & 0xff00) * i_1346_)
												& 0xff0000)) >> 8;
								i_1337_ += Class397_Sub1.anInt5766;
							}
						}
					}
					i_1334_++;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_1348_ = Class397_Sub1.anInt5771;
				while (i_1348_ < 0) {
					int i_1349_ = i_1348_ + i_1290_;
					if (i_1349_ >= 0) {
						if (i_1349_ >= is.length)
							break;
						int i_1350_ = Class397_Sub1.anInt5759;
						int i_1351_ = (Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764);
						int i_1352_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1353_ = Class397_Sub1.anInt5770;
						int i_1354_;
						if ((i_1354_ = i_1351_ - (anInt5758 << 12)) >= 0) {
							i_1354_ = ((Class397_Sub1.anInt5766 - i_1354_) / Class397_Sub1.anInt5766);
							i_1353_ += i_1354_;
							i_1351_ += Class397_Sub1.anInt5766 * i_1354_;
							i_1352_ += Class397_Sub1.anInt5777 * i_1354_;
							i_1350_ += i_1354_;
						}
						if ((i_1354_ = ((i_1351_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_1353_)
							i_1353_ = i_1354_;
						if ((i_1354_ = i_1352_ - (anInt5756 << 12)) >= 0) {
							i_1354_ = ((Class397_Sub1.anInt5777 - i_1354_) / Class397_Sub1.anInt5777);
							i_1353_ += i_1354_;
							i_1351_ += Class397_Sub1.anInt5766 * i_1354_;
							i_1352_ += Class397_Sub1.anInt5777 * i_1354_;
							i_1350_ += i_1354_;
						}
						if ((i_1354_ = ((i_1352_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_1353_)
							i_1353_ = i_1354_;
						int i_1355_ = is[i_1349_] - i;
						int i_1356_ = -is_1289_[i_1349_];
						int i_1357_ = i_1355_ - (i_1350_ - Class397_Sub1.anInt5759);
						if (i_1357_ > 0) {
							i_1350_ += i_1357_;
							i_1353_ += i_1357_;
							i_1351_ += Class397_Sub1.anInt5766 * i_1357_;
							i_1352_ += Class397_Sub1.anInt5777 * i_1357_;
						} else
							i_1356_ -= i_1357_;
						if (i_1353_ < i_1356_)
							i_1353_ = i_1356_;
						for (/**/; i_1353_ < 0; i_1353_++) {
							int i_1358_ = (anIntArray6690[(i_1352_ >> 12) * anInt5758 + (i_1351_ >> 12)]);
							int i_1359_ = i_1358_ >>> 24;
							int i_1360_ = 256 - i_1359_;
							int i_1361_ = is_1291_[i_1350_];
							is_1291_[i_1350_++] = ((((i_1358_ & 0xff00ff) * i_1359_ + (i_1361_ & 0xff00ff) * i_1360_)
									& ~0xff00ff)
									+ (((i_1358_ & 0xff00) * i_1359_ + (i_1361_ & 0xff00) * i_1360_) & 0xff0000)) >> 8;
							i_1351_ += Class397_Sub1.anInt5766;
							i_1352_ += Class397_Sub1.anInt5777;
						}
					}
					i_1348_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_1362_ = Class397_Sub1.anInt5771;
				while (i_1362_ < 0) {
					int i_1363_ = i_1362_ + i_1290_;
					if (i_1363_ >= 0) {
						if (i_1363_ >= is.length)
							break;
						int i_1364_ = Class397_Sub1.anInt5759;
						int i_1365_ = (Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764);
						int i_1366_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1367_ = Class397_Sub1.anInt5770;
						int i_1368_;
						if ((i_1368_ = i_1365_ - (anInt5758 << 12)) >= 0) {
							i_1368_ = ((Class397_Sub1.anInt5766 - i_1368_) / Class397_Sub1.anInt5766);
							i_1367_ += i_1368_;
							i_1365_ += Class397_Sub1.anInt5766 * i_1368_;
							i_1366_ += Class397_Sub1.anInt5777 * i_1368_;
							i_1364_ += i_1368_;
						}
						if ((i_1368_ = ((i_1365_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_1367_)
							i_1367_ = i_1368_;
						if (i_1366_ < 0) {
							i_1368_ = ((Class397_Sub1.anInt5777 - 1 - i_1366_) / Class397_Sub1.anInt5777);
							i_1367_ += i_1368_;
							i_1365_ += Class397_Sub1.anInt5766 * i_1368_;
							i_1366_ += Class397_Sub1.anInt5777 * i_1368_;
							i_1364_ += i_1368_;
						}
						if ((i_1368_ = ((i_1366_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
								/ Class397_Sub1.anInt5777)) > i_1367_)
							i_1367_ = i_1368_;
						int i_1369_ = is[i_1363_] - i;
						int i_1370_ = -is_1289_[i_1363_];
						int i_1371_ = i_1369_ - (i_1364_ - Class397_Sub1.anInt5759);
						if (i_1371_ > 0) {
							i_1364_ += i_1371_;
							i_1367_ += i_1371_;
							i_1365_ += Class397_Sub1.anInt5766 * i_1371_;
							i_1366_ += Class397_Sub1.anInt5777 * i_1371_;
						} else
							i_1370_ -= i_1371_;
						if (i_1367_ < i_1370_)
							i_1367_ = i_1370_;
						for (/**/; i_1367_ < 0; i_1367_++) {
							int i_1372_ = (anIntArray6690[(i_1366_ >> 12) * anInt5758 + (i_1365_ >> 12)]);
							int i_1373_ = i_1372_ >>> 24;
							int i_1374_ = 256 - i_1373_;
							int i_1375_ = is_1291_[i_1364_];
							is_1291_[i_1364_++] = ((((i_1372_ & 0xff00ff) * i_1373_ + (i_1375_ & 0xff00ff) * i_1374_)
									& ~0xff00ff)
									+ (((i_1372_ & 0xff00) * i_1373_ + (i_1375_ & 0xff00) * i_1374_) & 0xff0000)) >> 8;
							i_1365_ += Class397_Sub1.anInt5766;
							i_1366_ += Class397_Sub1.anInt5777;
						}
					}
					i_1362_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5777 == 0) {
			int i_1376_ = Class397_Sub1.anInt5771;
			while (i_1376_ < 0) {
				int i_1377_ = i_1376_ + i_1290_;
				if (i_1377_ >= 0) {
					if (i_1377_ >= is.length)
						break;
					int i_1378_ = Class397_Sub1.anInt5759;
					int i_1379_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_1380_ = Class397_Sub1.anInt5775;
					int i_1381_ = Class397_Sub1.anInt5770;
					if (i_1380_ >= 0 && i_1380_ - (anInt5756 << 12) < 0) {
						if (i_1379_ < 0) {
							int i_1382_ = ((Class397_Sub1.anInt5766 - 1 - i_1379_) / Class397_Sub1.anInt5766);
							i_1381_ += i_1382_;
							i_1379_ += Class397_Sub1.anInt5766 * i_1382_;
							i_1378_ += i_1382_;
						}
						int i_1383_;
						if ((i_1383_ = ((i_1379_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
								/ Class397_Sub1.anInt5766)) > i_1381_)
							i_1381_ = i_1383_;
						int i_1384_ = is[i_1377_] - i;
						int i_1385_ = -is_1289_[i_1377_];
						int i_1386_ = i_1384_ - (i_1378_ - Class397_Sub1.anInt5759);
						if (i_1386_ > 0) {
							i_1378_ += i_1386_;
							i_1381_ += i_1386_;
							i_1379_ += Class397_Sub1.anInt5766 * i_1386_;
							i_1380_ += Class397_Sub1.anInt5777 * i_1386_;
						} else
							i_1385_ -= i_1386_;
						if (i_1381_ < i_1385_)
							i_1381_ = i_1385_;
						for (/**/; i_1381_ < 0; i_1381_++) {
							int i_1387_ = (anIntArray6690[(i_1380_ >> 12) * anInt5758 + (i_1379_ >> 12)]);
							int i_1388_ = i_1387_ >>> 24;
							int i_1389_ = 256 - i_1388_;
							int i_1390_ = is_1291_[i_1378_];
							is_1291_[i_1378_++] = ((((i_1387_ & 0xff00ff) * i_1388_ + (i_1390_ & 0xff00ff) * i_1389_)
									& ~0xff00ff)
									+ (((i_1387_ & 0xff00) * i_1388_ + (i_1390_ & 0xff00) * i_1389_) & 0xff0000)) >> 8;
							i_1379_ += Class397_Sub1.anInt5766;
						}
					}
				}
				i_1376_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else if (Class397_Sub1.anInt5777 < 0) {
			int i_1391_ = Class397_Sub1.anInt5771;
			while (i_1391_ < 0) {
				int i_1392_ = i_1391_ + i_1290_;
				if (i_1392_ >= 0) {
					if (i_1392_ >= is.length)
						break;
					int i_1393_ = Class397_Sub1.anInt5759;
					int i_1394_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_1395_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_1396_ = Class397_Sub1.anInt5770;
					if (i_1394_ < 0) {
						int i_1397_ = ((Class397_Sub1.anInt5766 - 1 - i_1394_) / Class397_Sub1.anInt5766);
						i_1396_ += i_1397_;
						i_1394_ += Class397_Sub1.anInt5766 * i_1397_;
						i_1395_ += Class397_Sub1.anInt5777 * i_1397_;
						i_1393_ += i_1397_;
					}
					int i_1398_;
					if ((i_1398_ = ((i_1394_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
							/ Class397_Sub1.anInt5766)) > i_1396_)
						i_1396_ = i_1398_;
					if ((i_1398_ = i_1395_ - (anInt5756 << 12)) >= 0) {
						i_1398_ = ((Class397_Sub1.anInt5777 - i_1398_) / Class397_Sub1.anInt5777);
						i_1396_ += i_1398_;
						i_1394_ += Class397_Sub1.anInt5766 * i_1398_;
						i_1395_ += Class397_Sub1.anInt5777 * i_1398_;
						i_1393_ += i_1398_;
					}
					if ((i_1398_ = ((i_1395_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_1396_)
						i_1396_ = i_1398_;
					int i_1399_ = is[i_1392_] - i;
					int i_1400_ = -is_1289_[i_1392_];
					int i_1401_ = i_1399_ - (i_1393_ - Class397_Sub1.anInt5759);
					if (i_1401_ > 0) {
						i_1393_ += i_1401_;
						i_1396_ += i_1401_;
						i_1394_ += Class397_Sub1.anInt5766 * i_1401_;
						i_1395_ += Class397_Sub1.anInt5777 * i_1401_;
					} else
						i_1400_ -= i_1401_;
					if (i_1396_ < i_1400_)
						i_1396_ = i_1400_;
					for (/**/; i_1396_ < 0; i_1396_++) {
						int i_1402_ = (anIntArray6690[(i_1395_ >> 12) * anInt5758 + (i_1394_ >> 12)]);
						int i_1403_ = i_1402_ >>> 24;
						int i_1404_ = 256 - i_1403_;
						int i_1405_ = is_1291_[i_1393_];
						is_1291_[i_1393_++] = ((((i_1402_ & 0xff00ff) * i_1403_ + (i_1405_ & 0xff00ff) * i_1404_)
								& ~0xff00ff)
								+ (((i_1402_ & 0xff00) * i_1403_ + (i_1405_ & 0xff00) * i_1404_) & 0xff0000)) >> 8;
						i_1394_ += Class397_Sub1.anInt5766;
						i_1395_ += Class397_Sub1.anInt5777;
					}
				}
				i_1391_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else {
			int i_1406_ = Class397_Sub1.anInt5771;
			while (i_1406_ < 0) {
				int i_1407_ = i_1406_ + i_1290_;
				if (i_1407_ >= 0) {
					if (i_1407_ >= is.length)
						break;
					int i_1408_ = Class397_Sub1.anInt5759;
					int i_1409_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_1410_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_1411_ = Class397_Sub1.anInt5770;
					if (i_1409_ < 0) {
						int i_1412_ = ((Class397_Sub1.anInt5766 - 1 - i_1409_) / Class397_Sub1.anInt5766);
						i_1411_ += i_1412_;
						i_1409_ += Class397_Sub1.anInt5766 * i_1412_;
						i_1410_ += Class397_Sub1.anInt5777 * i_1412_;
						i_1408_ += i_1412_;
					}
					int i_1413_;
					if ((i_1413_ = ((i_1409_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
							/ Class397_Sub1.anInt5766)) > i_1411_)
						i_1411_ = i_1413_;
					if (i_1410_ < 0) {
						i_1413_ = ((Class397_Sub1.anInt5777 - 1 - i_1410_) / Class397_Sub1.anInt5777);
						i_1411_ += i_1413_;
						i_1409_ += Class397_Sub1.anInt5766 * i_1413_;
						i_1410_ += Class397_Sub1.anInt5777 * i_1413_;
						i_1408_ += i_1413_;
					}
					if ((i_1413_ = ((i_1410_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
							/ Class397_Sub1.anInt5777)) > i_1411_)
						i_1411_ = i_1413_;
					int i_1414_ = is[i_1407_] - i;
					int i_1415_ = -is_1289_[i_1407_];
					int i_1416_ = i_1414_ - (i_1408_ - Class397_Sub1.anInt5759);
					if (i_1416_ > 0) {
						i_1408_ += i_1416_;
						i_1411_ += i_1416_;
						i_1409_ += Class397_Sub1.anInt5766 * i_1416_;
						i_1410_ += Class397_Sub1.anInt5777 * i_1416_;
					} else
						i_1415_ -= i_1416_;
					if (i_1411_ < i_1415_)
						i_1411_ = i_1415_;
					for (/**/; i_1411_ < 0; i_1411_++) {
						int i_1417_ = (anIntArray6690[(i_1410_ >> 12) * anInt5758 + (i_1409_ >> 12)]);
						int i_1418_ = i_1417_ >>> 24;
						int i_1419_ = 256 - i_1418_;
						int i_1420_ = is_1291_[i_1408_];
						is_1291_[i_1408_++] = ((((i_1417_ & 0xff00ff) * i_1418_ + (i_1420_ & 0xff00ff) * i_1419_)
								& ~0xff00ff)
								+ (((i_1417_ & 0xff00) * i_1418_ + (i_1420_ & 0xff00) * i_1419_) & 0xff0000)) >> 8;
						i_1409_ += Class397_Sub1.anInt5766;
						i_1410_ += Class397_Sub1.anInt5777;
					}
				}
				i_1406_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		}
	}

	Class397_Sub1_Sub1(ha_Sub2 var_ha_Sub2, int i, int i_1421_) {
		super(var_ha_Sub2, i, i_1421_);
		anIntArray6690 = new int[i * i_1421_];
	}
}
