package net.zaros.client;

/* Class137 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class137 {
	static Class108 aClass108_1407;
	static ha aHa1408;
	static int anInt1409 = 1406;

	static final int method1422(NPC class338_sub3_sub1_sub3_sub2, byte i) {
		int i_0_ = 101 % ((-34 - i) / 41);
		NPCDefinition class147 = class338_sub3_sub1_sub3_sub2.definition;
		if (class147.configData != null) {
			class147 = class147.getNPCDefinitionByConfigID(Class16_Sub3_Sub1.configsRegister);
			if (class147 == null)
				return -1;
		}
		int i_1_ = class147.anInt1500;
		Class280 class280 = class338_sub3_sub1_sub3_sub2.method3516(false);
		int i_2_ = class338_sub3_sub1_sub3_sub2.aClass44_6777.method557((byte) -128);
		if (i_2_ == -1 || class338_sub3_sub1_sub3_sub2.aBoolean6783)
			i_1_ = class147.anInt1486;
		else if (i_2_ != class280.anInt2554 && class280.anInt2562 != i_2_ && i_2_ != class280.anInt2565 && class280.anInt2566 != i_2_) {
			if (i_2_ == class280.anInt2564 || class280.anInt2598 == i_2_ || i_2_ == class280.anInt2583 || class280.anInt2577 == i_2_)
				i_1_ = class147.anInt1501;
		} else
			i_1_ = class147.anInt1484;
		return i_1_;
	}

	static final void method1423(byte i) {
		Class41_Sub8.method422(1, 12);
		AdvancedMemoryCache.method987(1);
		if (i < 117)
			method1422(null, (byte) 38);
		System.gc();
	}

	static final void method1424(int i) {
		StaticMethods.scriptsCache.clear();
		if (i != 0) {
			/* empty */
		}
	}

	public static void method1425(int i) {
		if (i != 0)
			aClass108_1407 = null;
		aClass108_1407 = null;
		aHa1408 = null;
	}

	static final void method1426(long l, int i) {
		try {
			if (i != 1406)
				method1425(-114);
			Thread.sleep(l);
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
	}

	static {
		aClass108_1407 = new Class108();
	}
}
