package net.zaros.client;

/* Class16_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class16_Sub1_Sub1 extends Class16_Sub1 {
	static int[] anIntArray5802 = new int[64];
	static int anInt5803 = 0;
	static IncomingPacket aClass231_5804 = new IncomingPacket(133, 6);

	static final boolean method239(int i, int i_0_, int i_1_) {
		if (i != 0)
			method239(-5, 43, 96);
		return (StaticMethods.method2477(10, i_1_, i_0_) & Class123_Sub2.method1068(i_1_, (byte) 114, i_0_));
	}

	public static void method240(byte i) {
		aClass231_5804 = null;
		if (i > -40)
			anInt5803 = -60;
		anIntArray5802 = null;
	}

	static final boolean method241(int i, int i_2_, byte i_3_) {
		if (i_3_ != 24)
			return false;
		if (!(Class244.method2178(i_2_, 0, i) | (i & 0x60000) != 0) && !Class368_Sub8.method3836(i_2_, i, -1) && !Class338_Sub3_Sub4.method3569(i, i_2_, (byte) -22))
			return false;
		return true;
	}
}
