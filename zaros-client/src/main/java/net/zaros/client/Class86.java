package net.zaros.client;
/* Class86 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.BitSet;

final class Class86 {
	private int anInt930;
	private int anInt931 = 0;
	private int[][][] anIntArrayArrayArray932;
	static int[] anIntArray933 = new int[32];
	private int anInt934;
	private NodeDeque aClass155_935;
	static int anInt936;
	private Class296_Sub57[] aClass296_Sub57Array937;
	private int anInt938 = -1;
	boolean aBoolean939;

	final int[][] method823(byte i, int i_0_) {
		if (i <= 2)
			return null;
		if (anInt934 != anInt930) {
			if (anInt934 == 1) {
				aBoolean939 = i_0_ != anInt938;
				anInt938 = i_0_;
				return anIntArrayArrayArray932[0];
			}
			Class296_Sub57 class296_sub57 = aClass296_Sub57Array937[i_0_];
			if (class296_sub57 == null) {
				aBoolean939 = true;
				if (anInt934 > anInt931) {
					class296_sub57 = new Class296_Sub57(i_0_, anInt931);
					anInt931++;
				} else {
					Class296_Sub57 class296_sub57_1_ = (Class296_Sub57) aClass155_935.method1569(113);
					class296_sub57 = new Class296_Sub57(i_0_, class296_sub57_1_.anInt5082);
					aClass296_Sub57Array937[class296_sub57_1_.anInt5083] = null;
					class296_sub57_1_.unlink();
				}
				aClass296_Sub57Array937[i_0_] = class296_sub57;
			} else
				aBoolean939 = false;
			aClass155_935.method1570(true, class296_sub57);
			return anIntArrayArrayArray932[class296_sub57.anInt5082];
		}
		aBoolean939 = aClass296_Sub57Array937[i_0_] == null;
		aClass296_Sub57Array937[i_0_] = StaticMethods.aClass296_Sub57_6074;
		return anIntArrayArrayArray932[i_0_];
	}

	final int[][][] method824(byte i) {
		if (anInt934 != anInt930)
			throw new RuntimeException("Can only retrieve a full image cache");
		if (i > -80)
			return null;
		for (int i_2_ = 0; i_2_ < anInt934; i_2_++)
			aClass296_Sub57Array937[i_2_] = StaticMethods.aClass296_Sub57_6074;
		return anIntArrayArrayArray932;
	}

	public static void method825(byte i) {
		anIntArray933 = null;
		int i_3_ = -110 / ((i - 54) / 39);
	}

	static final int method826(String string, int i, Packet class296_sub17) {
		int i_4_ = class296_sub17.pos;
		byte[] is = Class280.method2345((byte) 123, string);
		class296_sub17.method2611(is.length);
		class296_sub17.pos += HashTable.aClass130_2463.method1375(7, class296_sub17.pos, i, (class296_sub17.data), is.length, is);
		return class296_sub17.pos - i_4_;
	}

	final void method827(int i) {
		for (int i_5_ = 0; i_5_ < anInt934; i_5_++) {
			anIntArrayArrayArray932[i_5_][0] = null;
			anIntArrayArrayArray932[i_5_][1] = null;
			anIntArrayArrayArray932[i_5_][2] = null;
			anIntArrayArrayArray932[i_5_] = null;
		}
		if (i < 101)
			method826(null, 85, null);
		aClass296_Sub57Array937 = null;
		anIntArrayArrayArray932 = null;
		aClass155_935.method1581(327680);
		aClass155_935 = null;
	}

	Class86(int i, int i_6_, int i_7_) {
		aClass155_935 = new NodeDeque();
		aBoolean939 = false;
		anInt934 = i;
		anInt930 = i_6_;
		anIntArrayArrayArray932 = new int[anInt934][3][i_7_];
		aClass296_Sub57Array937 = new Class296_Sub57[anInt930];
	}

	static {
		new BitSet(65536);
	}
}
