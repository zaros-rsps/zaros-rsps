package net.zaros.client;

/* Class347 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class347 {
	static String settings = null;
	static Class287 aClass287_3025 = new Class287();
	static int anInt3026;
	static int[] anIntArray3027 = new int[13];
	static OutgoingPacket aClass311_3028 = new OutgoingPacket(51, 3);

	public static void method3669(int i) {
		aClass287_3025 = null;
		anIntArray3027 = null;
		settings = null;
		aClass311_3028 = null;
		if (i != 13)
			settings = null;
	}

	static final void method3670(int i, int i_0_, int i_1_, int i_2_, boolean bool, int i_3_, int i_4_) {
		if (bool != true)
			method3669(57);
		Class296_Sub11.anInt4649 = i_0_;
		ParamType.anInt3251 = i_3_;
		Class27.anInt296 = i_2_;
		Class75.anInt870 = i_4_;
		Class296_Sub39_Sub12.anInt6195 = i_1_;
		MaterialRaw.anInt1781 = i;
	}

	static final void method3671(Class338_Sub3 class338_sub3, boolean bool, boolean bool_5_) {
		class338_sub3.aBoolean5206 = bool_5_;
		if (Class338_Sub3_Sub2.aBoolean6566) {
			if (bool)
				Class127.aClass299Array1301[Class127.aClass299Array1301.length - 1].method3238(class338_sub3, 61);
			else {
				int i = ModeWhere.method2191(class338_sub3.anInt5204);
				int i_6_ = (Class296_Sub39_Sub19.anIntArray6248[2] * class338_sub3.method3462((byte) 28) / class338_sub3.anInt5208);
				int i_7_ = ModeWhere.method2191(class338_sub3.anInt5204 - i_6_);
				int i_8_ = ModeWhere.method2191(class338_sub3.anInt5204 + i_6_);
				if (i_7_ == i_8_)
					Class127.aClass299Array1301[i].method3238(class338_sub3, 22);
				else if (i_8_ - i_7_ == 1)
					Class127.aClass299Array1301[Class247.anInt2336 + i_7_].method3238(class338_sub3, 8);
				else
					Class127.aClass299Array1301[Class127.aClass299Array1301.length - 1].method3238(class338_sub3, 74);
			}
		} else
			Class338_Sub3_Sub4_Sub1.method3572(class338_sub3, (Class368_Sub3.aClass296_Sub35Array5439));
	}
}
