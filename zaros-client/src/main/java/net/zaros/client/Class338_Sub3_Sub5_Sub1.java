package net.zaros.client;

/* Class338_Sub3_Sub5_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;

final class Class338_Sub3_Sub5_Sub1 extends Class338_Sub3_Sub5 implements Interface14 {
	static int anInt6645 = 0;
	private boolean aBoolean6646 = true;
	static boolean aBoolean6647 = false;
	private boolean aBoolean6648;
	private Class96 aClass96_6649;
	Class380 aClass380_6650;

	Class338_Sub3_Sub5_Sub1(ha var_ha, ObjectDefinition definition, int i, int i_18_, int i_19_, int i_20_, int i_21_, boolean bool, int i_22_, int i_23_) {
		super(i_19_, i_20_, i_21_, i, i_18_, definition.anInt776);
		aClass380_6650 = new Class380(var_ha, definition, 22, i_22_, i, i_18_, this, bool, i_23_);
		aBoolean6648 = definition.interactonType != 0 && !bool;
	}

	final void method3467(int i, int i_0_, Class338_Sub3 class338_sub3, boolean bool, int i_1_, int i_2_, ha var_ha) {
		int i_3_ = -26 % ((20 - i_2_) / 48);
		throw new IllegalStateException();
	}

	final boolean method3475(int i, int i_4_, ha var_ha, int i_5_) {
		Model class178 = aClass380_6650.method3975(false, false, var_ha, 131072, (byte) 39);
		if (class178 == null)
			return false;
		Class373 class373 = var_ha.f();
		class373.method3902(tileX, anInt5213, tileY);
		if (i_4_ > -48)
			return true;
		if (!Class296_Sub39_Sub10.aBoolean6177)
			return class178.method1732(i_5_, i, class373, false, 0);
		return class178.method1731(i_5_, i, class373, false, 0, ModeWhat.anInt1192);
	}

	static final void method3584(byte i, Class296_Sub45 class296_sub45) {
		if (i == -52) {
			if (class296_sub45.aClass296_Sub19_4951 != null)
				class296_sub45.aClass296_Sub19_4951.anInt4714 = 0;
			class296_sub45.aBoolean4952 = false;
			for (Class296_Sub45 class296_sub45_6_ = class296_sub45.method2930(); class296_sub45_6_ != null; class296_sub45_6_ = class296_sub45.method2932())
				method3584((byte) -52, class296_sub45_6_);
		}
	}

	final Class96 method3461(ha var_ha, int i) {
		int i_7_ = 31 / ((79 - i) / 44);
		return aClass96_6649;
	}

	final boolean method3468(int i) {
		if (i > -29)
			anInt6645 = 45;
		return false;
	}

	static final Class381 method3585(Component component, int i, int i_8_, int i_9_, Class398 class398) {
		if (Class298.anInt2699 == 0)
			throw new IllegalStateException();
		if (i < 0 || i >= 2)
			throw new IllegalArgumentException();
		if (i_8_ < 256)
			i_8_ = 256;
		try {
			Class381 class381 = (Class381) Class381_Sub2.class.newInstance();
			class381.anIntArray3222 = new int[256 * (HardReferenceWrapper.aBoolean6695 ? 2 : 1)];
			class381.anInt3234 = i_8_;
			class381.method4003(component);
			class381.anInt3235 = (i_8_ & ~0x3ff) + 1024;
			if (class381.anInt3235 > 16384)
				class381.anInt3235 = 16384;
			class381.method3999(class381.anInt3235);
			if (CS2Call.anInt4961 > 0 && Class296_Sub15_Sub4.aClass22_6028 == null) {
				Class296_Sub15_Sub4.aClass22_6028 = new Class22();
				Class296_Sub15_Sub4.aClass22_6028.aClass398_251 = class398;
				class398.method4123(Class296_Sub15_Sub4.aClass22_6028, CS2Call.anInt4961, -14396);
			}
			if (Class296_Sub15_Sub4.aClass22_6028 != null) {
				if (Class296_Sub15_Sub4.aClass22_6028.aClass381Array250[i] != null)
					throw new IllegalArgumentException();
				Class296_Sub15_Sub4.aClass22_6028.aClass381Array250[i] = class381;
			}
			return class381;
		} catch (Throwable throwable) {
			try {
				if (i_9_ != -14959)
					method3585(null, -16, -74, 4, null);
				Class381_Sub1 class381_sub1 = new Class381_Sub1(class398, i);
				class381_sub1.anIntArray3222 = new int[((!HardReferenceWrapper.aBoolean6695 ? 1 : 2) * 256)];
				class381_sub1.anInt3234 = i_8_;
				class381_sub1.method4003(component);
				class381_sub1.anInt3235 = 16384;
				class381_sub1.method3999(class381_sub1.anInt3235);
				if (CS2Call.anInt4961 > 0 && Class296_Sub15_Sub4.aClass22_6028 == null) {
					Class296_Sub15_Sub4.aClass22_6028 = new Class22();
					Class296_Sub15_Sub4.aClass22_6028.aClass398_251 = class398;
					class398.method4123(Class296_Sub15_Sub4.aClass22_6028, CS2Call.anInt4961, -14396);
				}
				if (Class296_Sub15_Sub4.aClass22_6028 != null) {
					if (Class296_Sub15_Sub4.aClass22_6028.aClass381Array250[i] != null)
						throw new IllegalArgumentException();
					Class296_Sub15_Sub4.aClass22_6028.aClass381Array250[i] = class381_sub1;
				}
				return class381_sub1;
			} catch (Throwable throwable_10_) {
				return new Class381();
			}
		}
	}

	final void method3460(int i, ha var_ha) {
		int i_11_ = -11 / ((-41 - i) / 62);
		Model class178 = aClass380_6650.method3975(true, true, var_ha, 262144, (byte) 39);
		if (class178 != null) {
			int i_12_ = tileX >> 9;
			int i_13_ = tileY >> 9;
			Class373 class373 = var_ha.f();
			class373.method3902(tileX, anInt5213, tileY);
			aClass380_6650.method3977(class373, false, class178, i_12_, false, var_ha, i_13_, i_12_, i_13_);
		}
	}

	public final int method59(int i) {
		if (i <= 16)
			method3469(-2);
		return aClass380_6650.anInt3200;
	}

	final boolean method3469(int i) {
		if (i <= 82)
			method54(95);
		return aBoolean6646;
	}

	final int method3466(byte i) {
		if (i < 77)
			aBoolean6646 = true;
		return aClass380_6650.method3980(true);
	}

	final Class338_Sub2 method3473(ha var_ha, byte i) {
		Model class178 = aClass380_6650.method3975(true, false, var_ha, 2048, (byte) 39);
		if (class178 == null)
			return null;
		Class373 class373 = var_ha.f();
		class373.method3902(tileX, anInt5213, tileY);
		Class338_Sub2 class338_sub2 = Class144.method1481(1, true, aBoolean6648);
		int i_14_ = tileX >> 9;
		int i_15_ = tileY >> 9;
		aClass380_6650.method3977(class373, true, class178, i_14_, false, var_ha, i_15_, i_14_, i_15_);
		if (!Class296_Sub39_Sub10.aBoolean6177)
			class178.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
		else
			class178.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
		if (i >= -84)
			return null;
		if (aClass380_6650.aClass338_Sub1_3202 != null) {
			Class390 class390 = aClass380_6650.aClass338_Sub1_3202.method3444();
			if (!Class296_Sub39_Sub10.aBoolean6177)
				var_ha.a(class390);
			else
				var_ha.a(class390, ModeWhat.anInt1192);
		}
		aBoolean6646 = class178.F() || aClass380_6650.aClass338_Sub1_3202 != null;
		if (aClass96_6649 != null)
			Billboard.method4025(class178, -115, tileY, anInt5213, aClass96_6649, tileX);
		else
			aClass96_6649 = Class41_Sub21.method478(anInt5213, class178, tileX, tileY, (byte) -118);
		return class338_sub2;
	}

	final void method3472(byte i) {
		int i_16_ = -41 % ((-56 - i) / 38);
		throw new IllegalStateException();
	}

	public final void method60(byte i, ha var_ha) {
		int i_17_ = 66 / ((-23 - i) / 35);
		aClass380_6650.method3987(var_ha, (byte) -53);
	}

	public final void method58(int i) {
		if (i != -19727)
			method3459(-112);
	}

	public final int method57(byte i) {
		if (i < 83)
			aBoolean6648 = true;
		return aClass380_6650.anInt3216;
	}

	public final int method54(int i) {
		if (i != -11077)
			aClass96_6649 = null;
		return aClass380_6650.anInt3203;
	}

	final int method3462(byte i) {
		if (i != 28)
			return -33;
		return aClass380_6650.method3982(14356);
	}

	public final void method56(ha var_ha, byte i) {
		aClass380_6650.method3976(262144, var_ha);
		if (i != -117)
			method3461(null, 112);
	}

	final void method3586(int i, Class375 class375) {
		if (i > -123)
			aClass96_6649 = null;
		aClass380_6650.method3984(-5581, class375);
	}

	public final boolean method55(byte i) {
		if (i != -57)
			method3461(null, 66);
		return aClass380_6650.method3988(4);
	}

	final boolean method3459(int i) {
		if (i != 0)
			method3466((byte) -24);
		return false;
	}
}
