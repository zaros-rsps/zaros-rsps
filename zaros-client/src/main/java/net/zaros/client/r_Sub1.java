package net.zaros.client;

/* r_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class r_Sub1 extends r {
	int anInt6702;
	int anInt6703;
	int anInt6704;
	int anInt6705;
	byte[] aByteArray6706;

	final boolean method2861(int i, int i_0_, byte i_1_) {
		if (i_1_ < 66)
			method2864(97, -18, -124, 107, 4, (byte) -25, 79);
		if (i_0_ * i > aByteArray6706.length)
			return false;
		return true;
	}

	final void method2862(byte i) {
		if (i != 71)
			aByteArray6706 = null;
		int i_2_ = -1;
		int i_3_ = aByteArray6706.length - 8;
		while (i_2_ < i_3_) {
			aByteArray6706[++i_2_] = (byte) 0;
			aByteArray6706[++i_2_] = (byte) 0;
			aByteArray6706[++i_2_] = (byte) 0;
			aByteArray6706[++i_2_] = (byte) 0;
			aByteArray6706[++i_2_] = (byte) 0;
			aByteArray6706[++i_2_] = (byte) 0;
			aByteArray6706[++i_2_] = (byte) 0;
			aByteArray6706[++i_2_] = (byte) 0;
		}
		while (aByteArray6706.length - 1 > i_2_)
			aByteArray6706[++i_2_] = (byte) 0;
	}

	static final boolean method2863(Class96 class96, byte i) {
		if (i > -14)
			return true;
		if (class96 == null)
			return false;
		return Class236.method2132(class96.anInt1038, -class96.anInt1038 + class96.anInt1046, class96.anInt1040, -class96.anInt1040 + class96.anInt1047, class96.anInt1042, 0, class96.anInt1045 - class96.anInt1042);
	}

	final void method2864(int i, int i_4_, int i_5_, int i_6_, int i_7_, byte i_8_, int i_9_) {
		if (i_8_ == 0) {
			int i_10_ = 0;
			if (i_7_ != i_9_)
				i_10_ = (i_5_ - i << 16) / (-i_7_ + i_9_);
			int i_11_ = 0;
			if (i_9_ != i_6_)
				i_11_ = (i_4_ - i_5_ << 16) / (i_6_ - i_9_);
			int i_12_ = 0;
			if (i_7_ != i_6_)
				i_12_ = (-i_4_ + i << 16) / (-i_6_ + i_7_);
			if (i_7_ > i_9_ || i_6_ < i_7_) {
				if (i_9_ > i_6_) {
					if (i_7_ < i_9_) {
						i_5_ = i_4_ <<= 16;
						i <<= 16;
						if (i_6_ < 0) {
							i_5_ -= i_11_ * i_6_;
							i_4_ -= i_6_ * i_12_;
							i_6_ = 0;
						}
						if (i_7_ < 0) {
							i -= i_7_ * i_10_;
							i_7_ = 0;
						}
						if (i_12_ <= i_11_) {
							i_9_ -= i_7_;
							i_7_ -= i_6_;
							i_6_ *= anInt6703;
							while (--i_7_ >= 0) {
								Class262.method2255(i_5_ >> 16, aByteArray6706, i_4_ >> 16, 0, -1, i_6_);
								i_5_ += i_11_;
								i_6_ += anInt6703;
								i_4_ += i_12_;
							}
							while (--i_9_ >= 0) {
								Class262.method2255(i_5_ >> 16, aByteArray6706, i >> 16, 0, i_8_ - 1, i_6_);
								i_6_ += anInt6703;
								i_5_ += i_11_;
								i += i_10_;
							}
						} else {
							i_9_ -= i_7_;
							i_7_ -= i_6_;
							i_6_ *= anInt6703;
							while (--i_7_ >= 0) {
								Class262.method2255(i_4_ >> 16, aByteArray6706, i_5_ >> 16, 0, -1, i_6_);
								i_6_ += anInt6703;
								i_4_ += i_12_;
								i_5_ += i_11_;
							}
							while (--i_9_ >= 0) {
								Class262.method2255(i >> 16, aByteArray6706, i_5_ >> 16, 0, -1, i_6_);
								i += i_10_;
								i_5_ += i_11_;
								i_6_ += anInt6703;
							}
						}
					} else {
						i = i_4_ <<= 16;
						if (i_6_ < 0) {
							i -= i_11_ * i_6_;
							i_4_ -= i_6_ * i_12_;
							i_6_ = 0;
						}
						i_5_ <<= 16;
						if (i_9_ < 0) {
							i_5_ -= i_10_ * i_9_;
							i_9_ = 0;
						}
						if (i_11_ >= i_12_) {
							i_7_ -= i_9_;
							i_9_ -= i_6_;
							i_6_ *= anInt6703;
							while (--i_9_ >= 0) {
								Class262.method2255(i >> 16, aByteArray6706, i_4_ >> 16, 0, -1, i_6_);
								i_6_ += anInt6703;
								i += i_11_;
								i_4_ += i_12_;
							}
							while (--i_7_ >= 0) {
								Class262.method2255(i_5_ >> 16, aByteArray6706, i_4_ >> 16, 0, -1, i_6_);
								i_5_ += i_10_;
								i_6_ += anInt6703;
								i_4_ += i_12_;
							}
						} else {
							i_7_ -= i_9_;
							i_9_ -= i_6_;
							i_6_ = anInt6703 * i_6_;
							while (--i_9_ >= 0) {
								Class262.method2255(i_4_ >> 16, aByteArray6706, i >> 16, 0, i_8_ - 1, i_6_);
								i += i_11_;
								i_4_ += i_12_;
								i_6_ += anInt6703;
							}
							while (--i_7_ >= 0) {
								Class262.method2255(i_4_ >> 16, aByteArray6706, i_5_ >> 16, 0, -1, i_6_);
								i_6_ += anInt6703;
								i_4_ += i_12_;
								i_5_ += i_10_;
							}
						}
					}
				} else if (i_7_ > i_6_) {
					i = i_5_ <<= 16;
					if (i_9_ < 0) {
						i_5_ -= i_9_ * i_11_;
						i -= i_10_ * i_9_;
						i_9_ = 0;
					}
					i_4_ <<= 16;
					if (i_6_ < 0) {
						i_4_ -= i_12_ * i_6_;
						i_6_ = 0;
					}
					if ((i_9_ == i_6_ || i_10_ >= i_11_) && (i_9_ != i_6_ || i_12_ >= i_10_)) {
						i_7_ -= i_6_;
						i_6_ -= i_9_;
						i_9_ *= anInt6703;
						while (--i_6_ >= 0) {
							Class262.method2255(i >> 16, aByteArray6706, i_5_ >> 16, 0, -1, i_9_);
							i_9_ += anInt6703;
							i += i_10_;
							i_5_ += i_11_;
						}
						while (--i_7_ >= 0) {
							Class262.method2255(i >> 16, aByteArray6706, i_4_ >> 16, 0, -1, i_9_);
							i += i_10_;
							i_9_ += anInt6703;
							i_4_ += i_12_;
						}
					} else {
						i_7_ -= i_6_;
						i_6_ -= i_9_;
						i_9_ = anInt6703 * i_9_;
						while (--i_6_ >= 0) {
							Class262.method2255(i_5_ >> 16, aByteArray6706, i >> 16, 0, -1, i_9_);
							i_5_ += i_11_;
							i_9_ += anInt6703;
							i += i_10_;
						}
						while (--i_7_ >= 0) {
							Class262.method2255(i_4_ >> 16, aByteArray6706, i >> 16, 0, i_8_ - 1, i_9_);
							i_9_ += anInt6703;
							i += i_10_;
							i_4_ += i_12_;
						}
					}
				} else {
					i_4_ = i_5_ <<= 16;
					i <<= 16;
					if (i_9_ < 0) {
						i_5_ -= i_11_ * i_9_;
						i_4_ -= i_10_ * i_9_;
						i_9_ = 0;
					}
					if (i_7_ < 0) {
						i -= i_12_ * i_7_;
						i_7_ = 0;
					}
					if (i_10_ >= i_11_) {
						i_6_ -= i_7_;
						i_7_ -= i_9_;
						i_9_ *= anInt6703;
						while (--i_7_ >= 0) {
							Class262.method2255(i_4_ >> 16, aByteArray6706, i_5_ >> 16, 0, -1, i_9_);
							i_9_ += anInt6703;
							i_5_ += i_11_;
							i_4_ += i_10_;
						}
						while (--i_6_ >= 0) {
							Class262.method2255(i >> 16, aByteArray6706, i_5_ >> 16, 0, i_8_ ^ 0xffffffff, i_9_);
							i_9_ += anInt6703;
							i += i_12_;
							i_5_ += i_11_;
						}
					} else {
						i_6_ -= i_7_;
						i_7_ -= i_9_;
						i_9_ *= anInt6703;
						while (--i_7_ >= 0) {
							Class262.method2255(i_5_ >> 16, aByteArray6706, i_4_ >> 16, 0, -1, i_9_);
							i_9_ += anInt6703;
							i_4_ += i_10_;
							i_5_ += i_11_;
						}
						while (--i_6_ >= 0) {
							Class262.method2255(i_5_ >> 16, aByteArray6706, i >> 16, 0, i_8_ ^ 0xffffffff, i_9_);
							i_5_ += i_11_;
							i += i_12_;
							i_9_ += anInt6703;
						}
					}
				}
			} else if (i_9_ < i_6_) {
				i_4_ = i <<= 16;
				if (i_7_ < 0) {
					i_4_ -= i_7_ * i_12_;
					i -= i_7_ * i_10_;
					i_7_ = 0;
				}
				i_5_ <<= 16;
				if (i_9_ < 0) {
					i_5_ -= i_11_ * i_9_;
					i_9_ = 0;
				}
				if (i_9_ != i_7_ && i_12_ < i_10_ || i_9_ == i_7_ && i_11_ < i_12_) {
					i_6_ -= i_9_;
					i_9_ -= i_7_;
					i_7_ *= anInt6703;
					while (--i_9_ >= 0) {
						Class262.method2255(i >> 16, aByteArray6706, i_4_ >> 16, 0, -1, i_7_);
						i_7_ += anInt6703;
						i += i_10_;
						i_4_ += i_12_;
					}
					while (--i_6_ >= 0) {
						Class262.method2255(i_5_ >> 16, aByteArray6706, i_4_ >> 16, 0, -1, i_7_);
						i_4_ += i_12_;
						i_5_ += i_11_;
						i_7_ += anInt6703;
					}
				} else {
					i_6_ -= i_9_;
					i_9_ -= i_7_;
					i_7_ *= anInt6703;
					while (--i_9_ >= 0) {
						Class262.method2255(i_4_ >> 16, aByteArray6706, i >> 16, 0, -1, i_7_);
						i_4_ += i_12_;
						i_7_ += anInt6703;
						i += i_10_;
					}
					while (--i_6_ >= 0) {
						Class262.method2255(i_4_ >> 16, aByteArray6706, i_5_ >> 16, 0, -1, i_7_);
						i_4_ += i_12_;
						i_7_ += anInt6703;
						i_5_ += i_11_;
					}
				}
			} else {
				i_5_ = i <<= 16;
				i_4_ <<= 16;
				if (i_7_ < 0) {
					i -= i_7_ * i_10_;
					i_5_ -= i_7_ * i_12_;
					i_7_ = 0;
				}
				if (i_6_ < 0) {
					i_4_ -= i_6_ * i_11_;
					i_6_ = 0;
				}
				if (i_7_ != i_6_ && i_12_ < i_10_ || i_6_ == i_7_ && i_11_ > i_10_) {
					i_9_ -= i_6_;
					i_6_ -= i_7_;
					i_7_ *= anInt6703;
					while (--i_6_ >= 0) {
						Class262.method2255(i >> 16, aByteArray6706, i_5_ >> 16, 0, -1, i_7_);
						i_7_ += anInt6703;
						i_5_ += i_12_;
						i += i_10_;
					}
					while (--i_9_ >= 0) {
						Class262.method2255(i >> 16, aByteArray6706, i_4_ >> 16, 0, i_8_ - 1, i_7_);
						i += i_10_;
						i_7_ += anInt6703;
						i_4_ += i_11_;
					}
				} else {
					i_9_ -= i_6_;
					i_6_ -= i_7_;
					i_7_ = anInt6703 * i_7_;
					while (--i_6_ >= 0) {
						Class262.method2255(i_5_ >> 16, aByteArray6706, i >> 16, 0, -1, i_7_);
						i += i_10_;
						i_7_ += anInt6703;
						i_5_ += i_12_;
					}
					while (--i_9_ >= 0) {
						Class262.method2255(i_4_ >> 16, aByteArray6706, i >> 16, 0, -1, i_7_);
						i_7_ += anInt6703;
						i_4_ += i_11_;
						i += i_10_;
					}
				}
			}
		}
	}

	final void method2865(int i, int i_13_, int i_14_, int i_15_, int i_16_) {
		anInt6705 = i_15_ - i_13_;
		anInt6702 = i_13_;
		if (i_16_ >= -97)
			anInt6705 = 104;
		anInt6703 = i_14_ - i;
		anInt6704 = i;
	}

	r_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_17_) {
		aByteArray6706 = new byte[i_17_ * i];
	}
}
