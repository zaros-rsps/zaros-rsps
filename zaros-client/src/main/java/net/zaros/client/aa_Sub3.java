package net.zaros.client;

/* aa_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class aa_Sub3 extends aa {
	Interface6_Impl1 anInterface6_Impl1_3731;

	static final Class213 method159(int i, int i_0_) {
		int i_1_ = 81 % ((i_0_ - 65) / 42);
		Class213[] class213s = Class296_Sub13.method2505(-10406);
		for (int i_2_ = 0; class213s.length > i_2_; i_2_++) {
			Class213 class213 = class213s[i_2_];
			if (i == class213.anInt2111) {
				return class213;
			}
		}
		return null;
	}

	static final void cameraLookat(int localx, int localy, int localHeight, int localSpeed, int localVelocity) {
		Class53.lookatX = localx;
		Graphic.lookatY = localy;
		Class338_Sub8_Sub2.lookatHeight = localHeight;
		StaticMethods.lookatSpeed = localSpeed;
		Class366_Sub2.lookatVelocity = localVelocity;
		if (Class366_Sub2.lookatVelocity >= 100) {
			int i_11_ = Class53.lookatX * 512 + 256;
			int i_12_ = Graphic.lookatY * 512 + 256;
			int i_13_ = aa_Sub1.method155(-1537652855, FileWorker.anInt3005, i_11_, i_12_) - Class338_Sub8_Sub2.lookatHeight;
			int i_14_ = i_11_ - Class219.camPosX;
			int i_15_ = i_13_ - TranslatableString.camPosY;
			int i_16_ = -Class124.camPosZ + i_12_;
			int i_17_ = (int) Math.sqrt(i_14_ * i_14_ + i_16_ * i_16_);
			Class296_Sub17_Sub2.camRotX = (int) (Math.atan2(i_15_, i_17_) * 2607.5945876176133) & 0x3fff;
			Class44_Sub1.camRotY = (int) (Math.atan2(i_14_, i_16_) * -2607.5945876176133) & 0x3fff;
			if (Class296_Sub17_Sub2.camRotX < 1024) {
				Class296_Sub17_Sub2.camRotX = 1024;
			}
			Class268.anInt2488 = 0;
			if (Class296_Sub17_Sub2.camRotX > 3072) {
				Class296_Sub17_Sub2.camRotX = 3072;
			}
		}
		Class361.anInt3103 = 2;
		Class368_Sub11.anInt5486 = Class242.anInt2309 = -1;
	}

	aa_Sub3(ha_Sub1 var_ha_Sub1, int i, int i_15_, byte[] is) {
		anInterface6_Impl1_3731 = var_ha_Sub1.method1180(false, 7, Class13.aClass202_3516, is, i_15_, i);
		anInterface6_Impl1_3731.method64((byte) 49, false, false);
	}

	aa_Sub3(ha_Sub1 var_ha_Sub1, int i, int i_16_, int[] is) {
		anInterface6_Impl1_3731 = var_ha_Sub1.method1179(i_16_, i, false, is, -461);
		anInterface6_Impl1_3731.method64((byte) 49, false, false);
	}
}
