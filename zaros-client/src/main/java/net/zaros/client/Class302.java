package net.zaros.client;
/* Class302 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;

final class Class302 {
	private NPC aClass338_Sub3_Sub1_Sub3_Sub2_2715;
	boolean aBoolean2716;
	private int anInt2717;
	private Player aClass338_Sub3_Sub1_Sub3_Sub1_2718 = null;
	int anInt2719;
	static OutgoingPacket aClass311_2720 = new OutgoingPacket(44, 3);
	static Class93[] aClass93Array2721;
	static int[] anIntArray2722 = {2, 1, 1, 1, 2, 2, 2, 1, 3, 3, 3, 2, 0, 4, 0};

	final void method3259(int i) {
		aBoolean2716 = false;
		aClass338_Sub3_Sub1_Sub3_Sub1_2718 = null;
		int i_0_ = 86 / ((50 - i) / 62);
		aClass338_Sub3_Sub1_Sub3_Sub2_2715 = null;
	}

	static final void method3260(byte i) {
		FileOnDisk class58 = null;
		try {
			Class278 class278 = Class252.aClass398_2383.method4117("2", (byte) 53, true);
			while (class278.anInt2540 == 0)
				Class106_Sub1.method942(1L, 0);
			if (class278.anInt2540 == 1) {
				class58 = (FileOnDisk) class278.anObject2539;
				byte[] is = new byte[(int) class58.getLength(-2)];
				int i_1_;
				for (int i_2_ = 0; i_2_ < is.length; i_2_ += i_1_) {
					i_1_ = class58.read((byte) 9, i_2_, is, is.length - i_2_);
					if (i_1_ == -1)
						throw new IOException("EOF");
				}
				Class270.method2305(-79, new Packet(is));
			}
		} catch (Exception exception) {
			/* empty */
		}
		try {
			if (class58 != null)
				class58.close(i ^ ~0x3f);
		} catch (Exception exception) {
			/* empty */
		}
		if (i != -64)
			aClass311_2720 = null;
	}

	static final void method3261(int i) {
		if (i != 3)
			aClass311_2720 = null;
		Class296_Sub51_Sub1.anInt6335 = 0;
		Class336.aClass391Array2956 = new Class391[50];
	}

	final Mobile method3262(int i) {
		if (i != 0)
			method3264(-78, -97, 115, -122);
		if (aClass338_Sub3_Sub1_Sub3_Sub2_2715 != null)
			return aClass338_Sub3_Sub1_Sub3_Sub2_2715;
		return aClass338_Sub3_Sub1_Sub3_Sub1_2718;
	}

	static final int method3263(int i) {
		if ((ConfigsRegister.anInt3674 ^ 0xffffffff) == i)
			return Class296_Sub39_Sub12.anInt6197;
		return Class235.anInt2227;
	}

	final void method3264(int i, int i_3_, int i_4_, int i_5_) {
		if (i_4_ == 0) {
			if (aClass338_Sub3_Sub1_Sub3_Sub2_2715 != null)
				aClass338_Sub3_Sub1_Sub3_Sub2_2715.updatePosition(i, true, 64, aClass338_Sub3_Sub1_Sub3_Sub2_2715.getSize(), i_5_, i_3_);
			else {
				aClass338_Sub3_Sub1_Sub3_Sub1_2718.z = aClass338_Sub3_Sub1_Sub3_Sub1_2718.aByte5203 = (byte) i_5_;
				aClass338_Sub3_Sub1_Sub3_Sub1_2718.setPosition(i_3_, i);
			}
		}
	}

	public static void method3265(int i) {
		aClass311_2720 = null;
		anIntArray2722 = null;
		if (i >= -10)
			aClass93Array2721 = null;
		aClass93Array2721 = null;
	}

	final void method3266(int i, int i_6_, int i_7_, boolean bool, int i_8_) {
		if (bool == true) {
			if (!aBoolean2716) {
				aBoolean2716 = true;
				if (anInt2719 >= 0) {
					aClass338_Sub3_Sub1_Sub3_Sub2_2715 = new NPC(25);
					aClass338_Sub3_Sub1_Sub3_Sub2_2715.lastUpdate = Class29.anInt307;
					aClass338_Sub3_Sub1_Sub3_Sub2_2715.index = anInt2717;
					aClass338_Sub3_Sub1_Sub3_Sub2_2715.setDefinition(Class352.npcDefinitionLoader.getDefinition(anInt2719));
					aClass338_Sub3_Sub1_Sub3_Sub2_2715.setSize((aClass338_Sub3_Sub1_Sub3_Sub2_2715.definition.size));
					aClass338_Sub3_Sub1_Sub3_Sub2_2715.anInt6766 = Class294.anInt2687++;
					aClass338_Sub3_Sub1_Sub3_Sub2_2715.anInt6815 = (aClass338_Sub3_Sub1_Sub3_Sub2_2715.definition.anInt1459) << 3;
				} else {
					aClass338_Sub3_Sub1_Sub3_Sub1_2718 = new Player(25);
					aClass338_Sub3_Sub1_Sub3_Sub1_2718.updateAppereance(Class187.someAppereanceBuffer);
					aClass338_Sub3_Sub1_Sub3_Sub1_2718.anInt6766 = Class294.anInt2687++;
					aClass338_Sub3_Sub1_Sub3_Sub1_2718.index = anInt2717;
					aClass338_Sub3_Sub1_Sub3_Sub1_2718.lastUpdate = Class29.anInt307;
				}
			}
			if (anInt2719 < 0) {
				aClass338_Sub3_Sub1_Sub3_Sub1_2718.z = aClass338_Sub3_Sub1_Sub3_Sub1_2718.aByte5203 = (byte) i;
				aClass338_Sub3_Sub1_Sub3_Sub1_2718.setPosition(i_6_, i_7_);
				aClass338_Sub3_Sub1_Sub3_Sub1_2718.method3517(true, true, i_8_);
			} else {
				aClass338_Sub3_Sub1_Sub3_Sub2_2715.updatePosition(i_7_, true, 55, aClass338_Sub3_Sub1_Sub3_Sub2_2715.getSize(), i, i_6_);
				aClass338_Sub3_Sub1_Sub3_Sub2_2715.method3517(true, true, i_8_);
			}
		}
	}

	Class302(Packet class296_sub17, int i) {
		aClass338_Sub3_Sub1_Sub3_Sub2_2715 = null;
		aBoolean2716 = false;
		anInt2717 = i;
		int i_9_ = class296_sub17.g1();
		int i_10_ = i_9_;
		while_244_ : do {
			do {
				if (i_10_ != 0) {
					if (i_10_ != 1)
						break;
				} else {
					anInt2719 = class296_sub17.gSmart2or4s();
					break while_244_;
				}
				anInt2719 = -1;
				break while_244_;
			} while (false);
			anInt2719 = -1;
		} while (false);
		class296_sub17.gstr();
	}
}
