package net.zaros.client;
import jaclib.hardware_info.HardwareInfo;

final class Class296_Sub28 extends Node {
	private int anInt4788;
	private int anInt4789;
	private String aString4790;
	private String aString4791;
	private String aString4792;
	private int anInt4793;
	private int anInt4794;
	private int heapSize;
	private int anInt4796;
	int anInt4797;
	private int anInt4798;
	static int anInt4799;
	private int anInt4800;
	private boolean webclient;
	int anInt4802;
	private boolean aBoolean4803;
	private int availableProcessors;
	private int anInt4805;
	int anInt4806;
	private int anInt4807;
	private int anInt4808;
	private String aString4809;

	static final void method2681(int i) {
		Class296_Sub51_Sub36.aHa6529.a(Class94.aClass373_1016);
		if (i != -19895)
			method2683((byte) 79, 75, -15);
		Class296_Sub51_Sub36.aHa6529.DA(Class296_Sub39_Sub19.anInt6251, ha_Sub1_Sub1.anInt5825, IntegerNode.anInt4674, Class379_Sub2.anInt5685);
	}

	final int method2682(boolean bool) {
		int i = 23;
		if (bool != true)
			return 100;
		i += Class331.method3411(aString4792, 2);
		i += Class331.method3411(aString4790, 2);
		i += Class331.method3411(aString4809, 2);
		i += Class331.method3411(aString4791, 2);
		return i;
	}

	static final int method2683(byte i, int i_0_, int i_1_) {
		if (i <= 60)
			return -80;
		int i_2_ = i_0_ >> 31 & i_1_ - 1;
		return ((i_0_ >>> 31) + i_0_) % i_1_ + i_2_;
	}

	final void method2684(byte i, Packet class296_sub17) {
		if (i <= -15) {
			class296_sub17.p1(5);
			class296_sub17.p1(anInt4789);
			class296_sub17.p1(aBoolean4803 ? 1 : 0);
			class296_sub17.p1(anInt4808);
			class296_sub17.p1(anInt4794);
			class296_sub17.p1(anInt4802);
			class296_sub17.p1(anInt4793);
			class296_sub17.p1(anInt4806);
			class296_sub17.p1(webclient ? 1 : 0);
			class296_sub17.p2(heapSize);
			class296_sub17.p1(availableProcessors);
			class296_sub17.write24BitInt(anInt4797);
			class296_sub17.p2(anInt4798);
			class296_sub17.p1(anInt4805);
			class296_sub17.p1(anInt4800);
			class296_sub17.p1(anInt4796);
			class296_sub17.method2619(aString4792);
			class296_sub17.method2619(aString4790);
			class296_sub17.method2619(aString4809);
			class296_sub17.method2619(aString4791);
			class296_sub17.p1(anInt4807);
			class296_sub17.p2(anInt4788);
		}
	}

	static final long method2685(byte i, int i_3_) {
		if (i != 41)
			anInt4799 = 109;
		return (long) (i_3_ + 11745) * 86400000L;
	}

	static final void method2686(int i, int i_4_, byte i_5_) {
		if (InterfaceComponent.loadInterface(i_4_) && i_5_ == 84)
			Class268.method2301((byte) -70, Class192.openedInterfaceComponents[i_4_], i);
	}

	private final void method2687(int i) {
		if (aString4792.length() > 40)
			aString4792 = aString4792.substring(0, 40);
		if (aString4790.length() > 40)
			aString4790 = aString4790.substring(0, 40);
		if (i != 5)
			anInt4796 = -90;
		if (aString4809.length() > 10)
			aString4809 = aString4809.substring(0, 10);
		if (aString4791.length() > 10)
			aString4791 = aString4791.substring(0, 10);
	}

	public Class296_Sub28() {
		/* empty */
	}

	Class296_Sub28(boolean bool, Class398 class398) {
		if (bool) {
			if (!Class398.osName.startsWith("win")) {
				if (Class398.osName.startsWith("mac"))
					anInt4789 = 2;
				else if (Class398.osName.startsWith("linux"))
					anInt4789 = 3;
				else
					anInt4789 = 4;
			} else
				anInt4789 = 1;
			if (!Class398.aString3327.startsWith("amd64") && !Class398.aString3327.startsWith("x86_64"))
				aBoolean4803 = false;
			else
				aBoolean4803 = true;
			if (anInt4789 != 1) {
				if (anInt4789 == 2) {
					if (Class398.aString3323.indexOf("10.4") != -1)
						anInt4808 = 20;
					else if (Class398.aString3323.indexOf("10.5") == -1) {
						if (Class398.aString3323.indexOf("10.6") == -1) {
							if (Class398.aString3323.indexOf("10.7") != -1)
								anInt4808 = 23;
						} else
							anInt4808 = 22;
					} else
						anInt4808 = 21;
				}
			} else if (Class398.aString3323.indexOf("4.0") == -1) {
				if (Class398.aString3323.indexOf("4.1") == -1) {
					if (Class398.aString3323.indexOf("4.9") != -1)
						anInt4808 = 3;
					else if (Class398.aString3323.indexOf("5.0") == -1) {
						if (Class398.aString3323.indexOf("5.1") == -1) {
							if (Class398.aString3323.indexOf("6.0") != -1)
								anInt4808 = 6;
							else if (Class398.aString3323.indexOf("6.1") != -1)
								anInt4808 = 7;
						} else
							anInt4808 = 5;
					} else
						anInt4808 = 4;
				} else
					anInt4808 = 2;
			} else
				anInt4808 = 1;
			if (Class398.aString3343.toLowerCase().indexOf("sun") == -1) {
				if (Class398.aString3343.toLowerCase().indexOf("microsoft") == -1) {
					if (Class398.aString3343.toLowerCase().indexOf("apple") != -1)
						anInt4794 = 3;
					else
						anInt4794 = 4;
				} else
					anInt4794 = 2;
			} else
				anInt4794 = 1;
			int i = 2;
			int i_6_ = 0;
			try {
				while (i < Class398.aString3333.length()) {
					int i_7_ = Class398.aString3333.charAt(i);
					if (i_7_ < 48 || i_7_ > 57)
						break;
					i++;
					i_6_ = i_6_ * 10 - 48 + i_7_;
				}
			} catch (Exception exception) {
				/* empty */
			}
			anInt4802 = i_6_;
			i_6_ = 0;
			i = Class398.aString3333.indexOf('.', 2) + 1;
			try {
				for (/**/; Class398.aString3333.length() > i; i++) {
					int i_8_ = Class398.aString3333.charAt(i);
					if (i_8_ < 48 || i_8_ > 57)
						break;
					i_6_ = i_8_ - 48 + i_6_ * 10;
				}
			} catch (Exception exception) {
				/* empty */
			}
			anInt4793 = i_6_;
			i_6_ = 0;
			i = Class398.aString3333.indexOf('_', 4) + 1;
			try {
				while (Class398.aString3333.length() > i) {
					int i_9_ = Class398.aString3333.charAt(i);
					if (i_9_ < 48 || i_9_ > 57)
						break;
					i++;
					i_6_ = -48 - (-i_9_ - i_6_ * 10);
				}
			} catch (Exception exception) {
				/* empty */
			}
			if (class398.aBoolean3332)
				webclient = false;
			else
				webclient = true;
			if (anInt4802 > 3)
				availableProcessors = Class296_Sub30.anInt4820;
			else
				availableProcessors = 0;
			anInt4806 = i_6_;
			heapSize = FileWorker.anInt3004;
			try {
				int[] is = HardwareInfo.getCPUInfo();
				if (is != null && is.length == 7) {
					anInt4797 = is[6];
					anInt4796 = is[5];
					anInt4805 = is[3];
					anInt4800 = is[4];
					anInt4798 = is[2];
				}
			} catch (Throwable throwable) {
				anInt4797 = 0;
			}
		}
		if (aString4790 == null)
			aString4790 = "";
		if (aString4792 == null)
			aString4792 = "";
		if (aString4791 == null)
			aString4791 = "";
		if (aString4809 == null)
			aString4809 = "";
		method2687(5);
	}
}
