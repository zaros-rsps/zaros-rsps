package net.zaros.client;
/* Interface6_Impl1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

interface Interface6_Impl1 extends Interface6 {
	public void method64(byte i, boolean bool, boolean bool_0_);

	public void method65(int i, int[] is, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_);

	public int method66(int i);

	public void method67(boolean bool, int i, int i_6_, int i_7_, byte[] is, int i_8_, int i_9_, int i_10_, Class202 class202);

	public void method68(int i, int i_11_, int i_12_, int[] is, int i_13_, int i_14_, int i_15_, int i_16_);

	public float method69(int i, float f);

	public int method70(int i);

	public float method71(float f, byte i);

	public boolean method72(int i);
}
