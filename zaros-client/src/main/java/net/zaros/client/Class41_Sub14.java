package net.zaros.client;

/* Class41_Sub14 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub14 extends Class41 {
	static int anInt3775 = 0;
	static Class256 aClass256_3776;
	static Class45[] aClass45Array3777;
	static boolean[] aBooleanArray3778 = new boolean[8];

	static final void method446(byte i) {
		int i_0_ = -49 % ((i - 33) / 53);
		for (int i_1_ = 0; i_1_ < Class296_Sub51_Sub1.anInt6335; i_1_++) {
			Class391 class391 = Class336.aClass391Array2956[i_1_];
			boolean bool = false;
			if (class391.aClass296_Sub45_Sub1_3288 == null) {
				class391.anInt3292--;
				if (class391.anInt3292 < (!class391.method4044(372) ? -10 : -1500))
					bool = true;
				else {
					if (class391.aByte3285 != 1 || class391.aClass171_3300 != null) {
						if (class391.method4044(372) && (class391.aClass296_Sub18_3286 == null || (class391.aClass296_Sub19_Sub1_3287 == null))) {
							if (class391.aClass296_Sub18_3286 == null)
								class391.aClass296_Sub18_3286 = (Class296_Sub18.method2640(Class296_Sub15.aClass138_4671, class391.anInt3295));
							if (class391.aClass296_Sub18_3286 == null)
								continue;
							if (class391.aClass296_Sub19_Sub1_3287 == null) {
								class391.aClass296_Sub19_Sub1_3287 = class391.aClass296_Sub18_3286.method2644(new int[]{22050});
								if (class391.aClass296_Sub19_Sub1_3287 == null)
									continue;
							}
						}
					} else {
						class391.aClass171_3300 = Class171.method1675(Class93.fs4, class391.anInt3295, 0);
						if (class391.aClass171_3300 == null)
							continue;
						class391.anInt3292 += class391.aClass171_3300.method1678();
					}
					if (class391.anInt3292 < 0) {
						int i_2_ = 8192;
						int i_3_;
						if (class391.anInt3293 != 0) {
							int i_4_ = (class391.anInt3293 & 0x36789ff) >> 24;
							if ((Class296_Sub51_Sub11.localPlayer.z) == i_4_) {
								int i_5_ = class391.anInt3293 << 9 & 0x1fe00;
								int i_6_ = (Class296_Sub51_Sub11.localPlayer.getSize() << 8);
								int i_7_ = (class391.anInt3293 & 0xff09cd) >> 16;
								int i_8_ = (i_6_ + ((i_7_ << 9) + (256 - (Class296_Sub51_Sub11.localPlayer.tileX))));
								int i_9_ = class391.anInt3293 >> 8 & 0xff;
								int i_10_ = (i_6_ - (Class296_Sub51_Sub11.localPlayer.tileY) + ((i_9_ << 9) + 256));
								int i_11_ = Math.abs(i_8_) + (Math.abs(i_10_) - 512);
								if (i_5_ < i_11_) {
									class391.anInt3292 = -99999;
									continue;
								}
								if (i_11_ < 0)
									i_11_ = 0;
								i_3_ = ((-i_11_ + i_5_) * Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5005.method489(125) * class391.anInt3294 / i_5_) >> 2;
								if (class391.aClass338_Sub3_3290 != null && (class391.aClass338_Sub3_3290 instanceof Class338_Sub3_Sub1)) {
									Class338_Sub3_Sub1 class338_sub3_sub1 = ((Class338_Sub3_Sub1) class391.aClass338_Sub3_3290);
									short i_12_ = class338_sub3_sub1.aShort6560;
									short i_13_ = class338_sub3_sub1.aShort6564;
								}
								if (i_8_ != 0 || i_10_ != 0) {
									int i_14_ = ((-Class44_Sub1.camRotY - (int) (Math.atan2((double) i_8_, (double) i_10_) * 2607.5945876176133) - 4096) & 0x3fff);
									if (i_14_ > 8192)
										i_14_ = -i_14_ + 16384;
									int i_15_;
									if (i_11_ > 0) {
										if (i_11_ < 4096)
											i_15_ = (8192 + (-i_11_ + 8192) / 4096);
										else
											i_15_ = 16384;
									} else
										i_15_ = 8192;
									i_2_ = i_15_ * i_14_ / 8192 + (16384 - i_15_ >> 1);
								}
							} else
								i_3_ = 0;
						} else
							i_3_ = (class391.anInt3294 * (class391.aByte3285 == 3 ? Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4998.method489(119) : Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4995.method489(127))) >> 2;
						if (i_3_ > 0) {
							Class296_Sub19_Sub1 class296_sub19_sub1 = null;
							if (class391.aByte3285 == 1)
								class296_sub19_sub1 = class391.aClass171_3300.method1676().method2652(Class264.aClass288_2474);
							else if (class391.method4044(372))
								class296_sub19_sub1 = class391.aClass296_Sub19_Sub1_3287;
							Class296_Sub45_Sub1 class296_sub45_sub1 = (class391.aClass296_Sub45_Sub1_3288 = (Class296_Sub45_Sub1.method2970(class296_sub19_sub1, class391.anInt3284, i_3_, i_2_)));
							class296_sub45_sub1.method2960(class391.anInt3289 - 1);
							Class16_Sub3.aClass296_Sub45_Sub5_3734.method3035(class296_sub45_sub1);
						}
					}
				}
			} else if (!class391.aClass296_Sub45_Sub1_3288.isLinked((byte) -91))
				bool = true;
			if (bool) {
				Class296_Sub51_Sub1.anInt6335--;
				for (int i_16_ = i_1_; Class296_Sub51_Sub1.anInt6335 > i_16_; i_16_++)
					Class336.aClass391Array2956[i_16_] = Class336.aClass391Array2956[i_16_ + 1];
				i_1_--;
			}
		}
		if (Class124.aBoolean1282 && !Class103.method898(-49)) {
			if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5017.method489(124) != 0 && Class30.anInt318 != -1) {
				if (Class296_Sub20.aClass296_Sub45_Sub4_4719 != null)
					s_Sub2.method3368(Class30.anInt318, (byte) 118, 0, Class42_Sub4.fs6, false, Class296_Sub20.aClass296_Sub45_Sub4_4719, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5017.method489(118));
				else
					Class338_Sub8.method3602(0, Class42_Sub4.fs6, false, 4, Class30.anInt318, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5017.method489(126));
			}
			Class296_Sub20.aClass296_Sub45_Sub4_4719 = null;
			Class124.aBoolean1282 = false;
		} else if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5017.method489(122) != 0 && Class30.anInt318 != -1 && !Class103.method898(-112)) {
			Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 91, Class18.aClass311_230);
			class296_sub1.out.p4(Class30.anInt318);
			Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
			Class30.anInt318 = -1;
		}
	}

	public static void method447(int i) {
		aBooleanArray3778 = null;
		aClass256_3776 = null;
		int i_17_ = 112 % ((10 - i) / 52);
		aClass45Array3777 = null;
	}

	final int method380(int i, byte i_18_) {
		if (i_18_ != 41)
			aClass256_3776 = null;
		return 1;
	}

	final int method383(byte i) {
		if (i != 110)
			anInt3775 = 6;
		return 2;
	}

	final void method381(int i, byte i_19_) {
		if (i_19_ == -110)
			anInt389 = i;
	}

	Class41_Sub14(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	static final boolean method448(int i, int i_20_, boolean bool) {
		if (bool != true)
			anInt3775 = 25;
		if (!(Class41_Sub20.method471(0, i_20_, i) | (i_20_ & 0x70000) != 0) && !Class296_Sub51_Sub26.method3150(i_20_, (byte) 93, i))
			return false;
		return true;
	}

	Class41_Sub14(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final int method449(int i) {
		if (i < 114)
			aBooleanArray3778 = null;
		return anInt389;
	}

	static final Class161[] method450(byte i) {
		if (i > -3)
			return null;
		return (new Class161[]{Class332.aClass161_2941, Class296_Sub51_Sub11.aClass161_6401, Class105_Sub2.aClass161_5693, StaticMethods.aClass161_5053, Class288.aClass161_2653, Class163.aClass161_1677, HardReferenceWrapper.aClass161_6694, NodeDeque.aClass161_1585, RuntimeException_Sub1.aClass161_3392, Class161.aClass161_3193, Class296_Sub45_Sub4.aClass161_6302, Class296_Sub51_Sub30.aClass161_6497, Class368_Sub7.aClass161_5460, Class296_Sub51_Sub27.aClass161_6483});
	}

	static final int method451(char c, int i, int i_21_) {
		try {
			int i_22_ = c << 4;
			if (i <= 6)
				return -16;
			if (Character.isUpperCase(c) || Character.isTitleCase(c)) {
				c = Character.toLowerCase(c);
				i_22_ = (c << 4) + 1;
			}
			return i_22_;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, ("hja.J(" + c + ',' + i + ',' + i_21_ + ')'));
		}
	}

	final void method386(int i) {
		if (aClass296_Sub50_392.aClass41_Sub17_5022.method459(true) && anInt389 == 2)
			anInt389 = 1;
		if (anInt389 < 0 || anInt389 > 2)
			anInt389 = method383((byte) 110);
		if (i != 2)
			method451('\026', 11, 82);
	}
}
