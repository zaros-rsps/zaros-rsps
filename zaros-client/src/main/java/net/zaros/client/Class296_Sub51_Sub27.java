package net.zaros.client;

/* Class296_Sub51_Sub27 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

class Class296_Sub51_Sub27 extends TextureOperation {
	int[] anIntArray6478;
	private int anInt6479 = -1;
	static ModeWhat liveModeWhat = new ModeWhat("LIVE", 0);
	int anInt6481;
	int anInt6482;
	static Class161 aClass161_6483 = new Class161(15, 0, 1, 0);
	static long[] aLongArray6484 = new long[11];
	static long[][] aLongArrayArray6485 = new long[8][256];

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		if (i < -84) {
			if (i_0_ == 0)
				anInt6479 = class296_sub17.g2();
		}
	}

	int[][] get_colour_output(int i, int i_1_) {
		int[][] is = aClass86_5034.method823((byte) 108, i);
		if (aClass86_5034.aBoolean939 && method3155(1696640376)) {
			int[] is_2_ = is[0];
			int[] is_3_ = is[1];
			int[] is_4_ = is[2];
			int i_5_ = (anInt6481 * (Class296_Sub35_Sub2.anInt6114 != anInt6482 ? i * anInt6482 / Class296_Sub35_Sub2.anInt6114 : i));
			if (Class41_Sub10.anInt3769 == anInt6481) {
				for (int i_6_ = 0; Class41_Sub10.anInt3769 > i_6_; i_6_++) {
					int i_7_ = anIntArray6478[i_5_++];
					is_4_[i_6_] = i_7_ << 4 & 4080;
					is_3_[i_6_] = i_7_ >> 4 & 4080;
					is_2_[i_6_] = (i_7_ & 16711680) >> 12;
				}
			} else {
				for (int i_8_ = 0; i_8_ < Class41_Sub10.anInt3769; i_8_++) {
					int i_9_ = anInt6481 * i_8_ / Class41_Sub10.anInt3769;
					int i_10_ = anIntArray6478[i_9_ + i_5_];
					is_4_[i_8_] = 4080 & i_10_ << 4;
					is_3_[i_8_] = (65280 & i_10_) >> 4;
					is_2_[i_8_] = 4080 & i_10_ >> 12;
				}
			}
		}
		if (i_1_ != 17621)
			anInt6481 = -122;
		return is;
	}

	public static void method3152(int i) {
		aClass161_6483 = null;
		if (i == -1678246399) {
			liveModeWhat = null;
			aLongArrayArray6485 = null;
			aLongArray6484 = null;
		}
	}

	static final int npcFileID(int i_11_) {
		return i_11_ >>> 7;
	}

	static final Class270 method3154(Packet class296_sub17, boolean bool) {
		Class270 class270 = new Class270();
		if (bool)
			method3152(-104);
		class270.anInt2506 = class296_sub17.g2();
		class270.aClass296_Sub39_Sub10_2509 = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(class270.anInt2506, -128);
		return class270;
	}

	final boolean method3155(int i) {
		if (i != 1696640376)
			method3152(-27);
		if (anIntArray6478 != null)
			return true;
		if (anInt6479 >= 0) {
			Class186 class186 = (Class41_Sub6.texture_group < 0 ? Class186.method1872(StaticMethods.aClass138_294, anInt6479) : Class186.method1876(StaticMethods.aClass138_294, Class41_Sub6.texture_group, anInt6479));
			class186.method1874();
			anIntArray6478 = class186.method1869();
			anInt6481 = class186.anInt1899;
			anInt6482 = class186.anInt1904;
			return true;
		}
		return false;
	}

	final int method3067(byte i) {
		if (i < 90)
			anInt6479 = -94;
		return anInt6479;
	}

	public Class296_Sub51_Sub27() {
		super(0, false);
	}

	final void method3073(int i) {
		int i_12_ = -89 % ((-58 - i) / 39);
		super.method3073(-104);
		anIntArray6478 = null;
	}

	static {
		for (int i = 0; i < 256; i++) {
			int i_13_ = "\u1823\uc6e8\u87b8\u014f\u36a6\ud2f5\u796f\u9152\u60bc\u9b8e\ua30c\u7b35\u1de0\ud7c2\u2e4b\ufe57\u1577\u37e5\u9ff0\u4ada\u58c9\u290a\ub1a0\u6b85\ubd5d\u10f4\ucb3e\u0567\ue427\u418b\ua77d\u95d8\ufbee\u7c66\udd17\u479e\uca2d\ubf07\uad5a\u8333\u6302\uaa71\uc819\u49d9\uf2e3\u5b88\u9a26\u32b0\ue90f\ud580\ubecd\u3448\uff7a\u905f\u2068\u1aae\ub454\u9322\u64f1\u7312\u4008\uc3ec\udba1\u8d3d\u9700\ucf2b\u7682\ud61b\ub5af\u6a50\u45f3\u30ef\u3f55\ua2ea\u65ba\u2fc0\ude1c\ufd4d\u9275\u068a\ub2e6\u0e1f\u62d4\ua896\uf9c5\u2559\u8472\u394c\u5e78\u388c\ud1a5\ue261\ub321\u9c1e\u43c7\ufc04\u5199\u6d0d\ufadf\u7e24\u3bab\uce11\u8f4e\ub7eb\u3c81\u94f7\ub913\u2cd3\ue76e\uc403\u5644\u7fa9\u2abb\uc153\udc0b\u9d6c\u3174\uf646\uac89\u14e1\u163a\u6909\u70b6\ud0ed\ucc42\u98a4\u285c\uf886".charAt(i / 2);
			long l = (long) ((i & 0x1) != 0 ? i_13_ & 0xff : i_13_ >>> 8);
			long l_14_ = l << 1;
			if (l_14_ >= 256L)
				l_14_ ^= 0x11dL;
			long l_15_ = l_14_ << 1;
			if (l_15_ >= 256L)
				l_15_ ^= 0x11dL;
			long l_16_ = l ^ l_15_;
			long l_17_ = l_15_ << 1;
			if (l_17_ >= 256L)
				l_17_ ^= 0x11dL;
			long l_18_ = l ^ l_17_;
			aLongArrayArray6485[0][i] = (r.method2860(l_18_, (r.method2860(l_14_ << 8, (r.method2860((r.method2860(r.method2860(l << 32, r.method2860(l_15_ << 40, r.method2860(l << 48, l << 56))), l_17_ << 24)), l_16_ << 16))))));
			for (int i_19_ = 1; i_19_ < 8; i_19_++)
				aLongArrayArray6485[i_19_][i] = r.method2860(aLongArrayArray6485[i_19_ - 1][i] >>> 8, aLongArrayArray6485[i_19_ - 1][i] << 56);
		}
		aLongArray6484[0] = 0L;
		for (int i = 1; i <= 10; i++) {
			int i_20_ = i * 8 - 8;
			aLongArray6484[i] = (Class235.method2129((Class235.method2129(Class41.method379(aLongArrayArray6485[6][i_20_ + 6], 65280L), (Class235.method2129((Class235.method2129(Class41.method379(4278190080L, aLongArrayArray6485[4][i_20_ + 4]), (Class235.method2129(Class41.method379(1095216660480L, (aLongArrayArray6485[3][i_20_ + 3])), (Class235.method2129((Class235.method2129(Class41.method379((aLongArrayArray6485[1][i_20_ + 1]), 71776119061217280L), Class41.method379((aLongArrayArray6485[0][i_20_]), -72057594037927936L))), Class41.method379((aLongArrayArray6485[2][i_20_ + 2]), 280375465082880L))))))), Class41.method379(aLongArrayArray6485[5][i_20_ + 5], 16711680L))))), Class41.method379(255L, aLongArrayArray6485[7][i_20_ + 7])));
		}
	}
}
