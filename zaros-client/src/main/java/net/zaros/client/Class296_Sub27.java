package net.zaros.client;

/* Class296_Sub27 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub27 extends Node {
	private int anInt4772;
	int anInt4773;
	int anInt4774;
	int anInt4775;
	private int anInt4776;
	private int anInt4777;
	private int anInt4778;
	static OutgoingPacket aClass311_4779 = new OutgoingPacket(23, 7);
	private int anInt4780;
	int anInt4781;
	static IncomingPacket aClass231_4782 = new IncomingPacket(100, 3);
	static short aShort4784 = 1;
	static String quitURL;
	static int anInt4786 = 0;
	static float aFloat4787;

	final boolean method2673(int i, int i_0_, int i_1_) {
		if (i_1_ != 23) {
			return false;
		}
		if (i_0_ >= anInt4773 && i_0_ <= anInt4781 && anInt4775 <= i && anInt4774 >= i) {
			return true;
		}
		return false;
	}

	public static void method2674(byte i) {
		if (i != 57) {
			method2674((byte) 46);
		}
		aClass231_4782 = null;
		aClass311_4779 = null;
		quitURL = null;
	}

	static final byte[] method2675(byte[] is, boolean bool) {
		int i = is.length;
		byte[] is_2_ = new byte[i];
		ArrayTools.removeElement(is, 0, is_2_, 0, i);
		return is_2_;
	}

	final void method2676(int[] is, int i, int i_3_, int i_4_) {
		is[0] = anInt4780;
		is[i_3_] = i - anInt4773 + anInt4778;
		is[2] = i_4_ - anInt4775 + anInt4776;
	}

	static final void method2677(Class190 class190, int i, byte i_5_) {
		if (Class361.aBoolean3096) {
			i = 0;
			Class361.aBoolean3096 = false;
		}
		if (AnimBase.aClass190_4769 == null || !AnimBase.aClass190_4769.method1916(64, class190)) {
			AnimBase.aClass190_4769 = class190;
			Animation.aLong411 = Class72.method771(-111);
			InterfaceComponentSettings.anInt4826 = Class69_Sub3.anInt5723 = i;
			if (InterfaceComponentSettings.anInt4826 == 0) {
				MaterialRaw.method1671((byte) -23);
			} else {
				StaticMethods.aFloat5948 = Class30.aFloat316;
				ObjectDefinitionLoader.anInt379 = Class296_Sub28.anInt4799;
				Class205.anInt3512 = ha_Sub3.anInt4115;
				Class296_Sub51_Sub9.aFloat6388 = Class69.aFloat3690;
				Class105_Sub2.aClass241_5692 = Class296_Sub51_Sub23.aClass241_6458;
				Class307.aFloat2748 = Class67.aFloat752;
				Class368_Sub7.aClass262_5461 = s_Sub3.aClass262_5164;
				Class338_Sub3_Sub1_Sub5.aFloat6844 = OutputStream_Sub2.aFloat41;
				Class42_Sub1.anInt3835 = Mobile.anInt6787;
				Class317.aFloat3704 = Animation.aFloat406;
				CS2Call.aFloat4967 = r.aFloat6208;
				if (s_Sub3.aClass262_5164 != null) {
					if (s_Sub3.aClass262_5164.method2256(true)) {
						Class368_Sub7.aClass262_5461 = s_Sub3.aClass262_5164.method2259((byte) -24);
						s_Sub3.aClass262_5164 = Class368_Sub7.aClass262_5461;
					}
					if (s_Sub3.aClass262_5164 != null && s_Sub3.aClass262_5164 != AnimBase.aClass190_4769.aClass262_1935) {
						s_Sub3.aClass262_5164.method2257(114, AnimBase.aClass190_4769.aClass262_1935);
					}
				}
			}
		}
		if (i_5_ > -52) {
			anInt4786 = -97;
		}
	}

	final boolean method2678(int i, byte i_6_, int i_7_) {
		if (i_6_ != -127) {
			anInt4780 = 47;
		}
		if (i < anInt4778 || i > anInt4777 || anInt4776 > i_7_ || i_7_ > anInt4772) {
			return false;
		}
		return true;
	}

	final void method2679(int i, byte i_8_, int[] is, int i_9_) {
		is[0] = 0;
		is[2] = i + -anInt4776 + anInt4775;
		is[1] = i_9_ - (-anInt4773 + anInt4778);
		if (i_8_ != 11) {
			anInt4780 = 109;
		}
	}

	final boolean method2680(int i, boolean bool, int i_10_, int i_11_) {
		if (bool) {
			return true;
		}
		if (anInt4780 != i_10_ || anInt4778 > i_11_ || i_11_ > anInt4777 || anInt4776 > i || anInt4772 < i) {
			return false;
		}
		return true;
	}

	Class296_Sub27(int i, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_) {
		anInt4777 = i_14_;
		anInt4776 = i_13_;
		anInt4780 = i;
		anInt4774 = i_19_;
		anInt4775 = i_17_;
		anInt4778 = i_12_;
		anInt4772 = i_15_;
		anInt4781 = i_18_;
		anInt4773 = i_16_;
	}
}
