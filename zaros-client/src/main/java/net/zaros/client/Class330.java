package net.zaros.client;

/* Class330 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class330 {
	private int anInt2918 = -1;
	int anInt2919 = 70;
	private int anInt2920;
	private String aString2921 = "";
	static IncomingPacket aClass231_2922 = new IncomingPacket(138, -2);
	Class164 aClass164_2923;
	int anInt2924;
	int anInt2925;
	int anInt2926;
	private int anInt2927;
	int anInt2928;
	int anInt2929;
	private int anInt2930;
	int anInt2931;
	int anInt2932;

	final Sprite method3397(byte i, ha var_ha) {
		if (anInt2927 < 0)
			return null;
		if (i < 10)
			return null;
		Sprite class397 = ((Sprite) aClass164_2923.aClass113_1680.get((long) anInt2927));
		if (class397 == null) {
			method3404(var_ha, 0);
			class397 = (Sprite) aClass164_2923.aClass113_1680.get((long) anInt2927);
		}
		return class397;
	}

	final String method3398(int i, byte i_0_) {
		String string = aString2921;
		if (i_0_ > -80)
			method3400(59, null);
		for (;;) {
			int i_1_ = string.indexOf("%1");
			if (i_1_ < 0)
				break;
			string = (string.substring(0, i_1_) + InputStream_Sub1.method129(i, false, -1) + string.substring(i_1_ + 2));
		}
		return string;
	}

	private final void method3399(Packet class296_sub17, int i, int i_2_) {
		int i_3_ = 67 % ((-73 - i) / 36);
		if (i_2_ == 1)
			anInt2926 = class296_sub17.g2();
		else if (i_2_ == 2)
			anInt2931 = class296_sub17.readUnsignedMedInt();
		else if (i_2_ != 3) {
			if (i_2_ == 4)
				anInt2927 = class296_sub17.g2();
			else if (i_2_ != 5) {
				if (i_2_ != 6) {
					if (i_2_ == 7)
						anInt2929 = class296_sub17.g2b();
					else if (i_2_ == 8)
						aString2921 = class296_sub17.gjstr2();
					else if (i_2_ == 9)
						anInt2919 = class296_sub17.g2();
					else if (i_2_ != 10) {
						if (i_2_ == 11)
							anInt2932 = 0;
						else if (i_2_ == 12)
							anInt2925 = class296_sub17.g1();
						else if (i_2_ == 13)
							anInt2924 = class296_sub17.g2b();
						else if (i_2_ == 14)
							anInt2932 = class296_sub17.g2();
					} else
						anInt2928 = class296_sub17.g2b();
				} else
					anInt2918 = class296_sub17.g2();
			} else
				anInt2920 = class296_sub17.g2();
		} else
			anInt2930 = class296_sub17.g2();
	}

	final void method3400(int i, Packet class296_sub17) {
		for (;;) {
			int i_4_ = class296_sub17.g1();
			if (i_4_ == 0)
				break;
			method3399(class296_sub17, -118, i_4_);
		}
		int i_5_ = 64 % ((i - 59) / 61);
	}

	final Sprite method3401(ha var_ha, int i) {
		if (anInt2930 < 0)
			return null;
		if (i < 17)
			aString2921 = null;
		Sprite class397 = ((Sprite) aClass164_2923.aClass113_1680.get((long) anInt2930));
		if (class397 == null) {
			method3404(var_ha, 0);
			class397 = (Sprite) aClass164_2923.aClass113_1680.get((long) anInt2930);
		}
		return class397;
	}

	final Sprite method3402(int i, ha var_ha) {
		if (i > -40)
			method3398(-63, (byte) -48);
		if (anInt2920 < 0)
			return null;
		Sprite class397 = ((Sprite) aClass164_2923.aClass113_1680.get((long) anInt2920));
		if (class397 == null) {
			method3404(var_ha, 0);
			class397 = (Sprite) aClass164_2923.aClass113_1680.get((long) anInt2920);
		}
		return class397;
	}

	public static void method3403(byte i) {
		if (i != 108)
			aClass231_2922 = null;
		aClass231_2922 = null;
	}

	private final void method3404(ha var_ha, int i) {
		Js5 class138 = aClass164_2923.aClass138_1684;
		if (anInt2930 >= 0 && (aClass164_2923.aClass113_1680.get((long) anInt2930) == null) && class138.hasEntryBuffer(anInt2930)) {
			Class186 class186 = Class186.method1872(class138, anInt2930);
			aClass164_2923.aClass113_1680.put(var_ha.a(class186, true), (long) anInt2930);
		}
		if (i <= anInt2920 && (aClass164_2923.aClass113_1680.get((long) anInt2920) == null) && class138.hasEntryBuffer(anInt2920)) {
			Class186 class186 = Class186.method1872(class138, anInt2920);
			aClass164_2923.aClass113_1680.put(var_ha.a(class186, true), (long) anInt2920);
		}
		if (anInt2927 >= 0 && (aClass164_2923.aClass113_1680.get((long) anInt2927) == null) && class138.hasEntryBuffer(anInt2927)) {
			Class186 class186 = Class186.method1872(class138, anInt2927);
			aClass164_2923.aClass113_1680.put(var_ha.a(class186, true), (long) anInt2927);
		}
		if (anInt2918 >= 0 && (aClass164_2923.aClass113_1680.get((long) anInt2918) == null) && class138.hasEntryBuffer(anInt2918)) {
			Class186 class186 = Class186.method1872(class138, anInt2918);
			aClass164_2923.aClass113_1680.put(var_ha.a(class186, true), (long) anInt2918);
		}
	}

	public Class330() {
		anInt2920 = -1;
		anInt2924 = 0;
		anInt2927 = -1;
		anInt2930 = -1;
		anInt2929 = 0;
		anInt2928 = 0;
		anInt2926 = -1;
		anInt2932 = -1;
		anInt2931 = 16777215;
		anInt2925 = -1;
	}

	final Sprite method3405(ha var_ha, byte i) {
		if (anInt2918 < 0)
			return null;
		Sprite class397 = ((Sprite) aClass164_2923.aClass113_1680.get((long) anInt2918));
		if (class397 == null) {
			method3404(var_ha, 0);
			class397 = (Sprite) aClass164_2923.aClass113_1680.get((long) anInt2918);
		}
		int i_6_ = -119 % ((i - 41) / 53);
		return class397;
	}
}
