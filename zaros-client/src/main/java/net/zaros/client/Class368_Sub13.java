package net.zaros.client;

/* Class368_Sub13 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub13 extends Class368 {
	private int anInt5493;
	private int anInt5494;
	private int anInt5495;
	static IncomingPacket aClass231_5496 = new IncomingPacket(130, -1);
	private int anInt5497;
	private int anInt5498;
	private int anInt5499;
	private int anInt5500;
	private int anInt5501;
	private int anInt5502;
	private int anInt5503;
	private int anInt5504;
	private int anInt5505;
	static int anInt5506 = 0;
	private int anInt5507;

	Class368_Sub13(Packet class296_sub17, int i, int i_0_) {
		super(class296_sub17);
		if (i != 0) {
			anInt5494 = -1;
			anInt5503 = -1;
			anInt5504 = class296_sub17.g2();
		} else {
			int i_1_ = class296_sub17.g4();
			anInt5504 = -1;
			anInt5503 = i_1_ & 0xffff;
			anInt5494 = i_1_ >>> 16;
		}
		if (i_0_ == 0) {
			int i_2_ = class296_sub17.g4();
			anInt5493 = -1;
			anInt5499 = i_2_ >>> 16;
			anInt5501 = i_2_ & 0xffff;
		} else {
			anInt5499 = -1;
			anInt5501 = -1;
			anInt5493 = class296_sub17.g2();
		}
		if (i != 0 || i_0_ != 0)
			anInt5498 = -1;
		else
			anInt5498 = class296_sub17.g1();
		anInt5495 = class296_sub17.g2();
		anInt5505 = class296_sub17.g1();
		anInt5507 = class296_sub17.g1();
		anInt5497 = class296_sub17.readUnsignedMedInt();
		anInt5502 = class296_sub17.g2();
		anInt5500 = class296_sub17.g1();
	}

	public static void method3847(int i) {
		aClass231_5496 = null;
		if (i != 0)
			method3847(56);
	}

	final void method3807(byte i) {
		int i_3_ = -29 / ((52 - i) / 52);
		int i_4_;
		int i_5_;
		int i_6_;
		if (anInt5494 >= 0) {
			i_6_ = anInt5494 * 512 + 256;
			i_5_ = anInt5498;
			i_4_ = anInt5503 * 512 + 256;
		} else {
			Mobile class338_sub3_sub1_sub3 = Class379.aClass302Array3624[anInt5504].method3262(0);
			i_4_ = class338_sub3_sub1_sub3.tileY;
			i_5_ = class338_sub3_sub1_sub3.z;
			i_6_ = class338_sub3_sub1_sub3.tileX;
		}
		int i_7_;
		int i_8_;
		if (anInt5503 >= 0) {
			i_8_ = anInt5501 * 512 + 256;
			i_7_ = anInt5499 * 512 + 256;
		} else {
			Mobile class338_sub3_sub1_sub3 = Class379.aClass302Array3624[anInt5493].method3262(0);
			i_7_ = class338_sub3_sub1_sub3.tileX;
			i_8_ = class338_sub3_sub1_sub3.tileY;
			if (i_5_ < 0)
				i_5_ = class338_sub3_sub1_sub3.z;
		}
		int i_9_ = anInt5500 << 2;
		Class338_Sub3_Sub1_Sub2 class338_sub3_sub1_sub2 = new Class338_Sub3_Sub1_Sub2(anInt5495, i_5_, i_5_, i_6_, i_4_, anInt5505 << 2, Class29.anInt307, Class29.anInt307 + anInt5497, anInt5502, i_9_, anInt5504 + 1, anInt5493 + 1, anInt5507 << 2, false, 0);
		class338_sub3_sub1_sub2.method3491(i_7_, anInt5507 << 2, Class29.anInt307 + anInt5497, i_8_, -1926441266);
		Class72.aClass155_844.addLast((byte) -61, new Class296_Sub39_Sub18(class338_sub3_sub1_sub2));
	}

	static final void method3848(int i, int i_10_, int i_11_, boolean bool, boolean bool_12_) {
		if (bool != true)
			aClass231_5496 = null;
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(115) != 0) {
			r_Sub2.anInt6714 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(119);
			Class33.method348(false, true, 0);
		} else
			Class254.method2212(28461, false);
		StaticMethods.anInt5947 = i;
		Class368_Sub23.aBoolean5573 = bool_12_;
		Class368_Sub9.anInt5476 = i_11_;
		Class106.method918(i_10_);
	}

	static final void method3849(int i, InterfaceComponent class51, ha var_ha) {
		boolean bool = ((Class296_Sub39_Sub1.itemDefinitionLoader.requestIcon(class51.clickedItem, false, var_ha, class51.anInt511, (!class51.aBoolean613 ? null : (Class296_Sub51_Sub11.localPlayer.composite)), class51.borderThickness, class51.shadowColor | i, class51.anInt490)) == null);
		if (bool) {
			Class163.aClass155_1679.addLast((byte) 101, new Class296_Sub44(class51.clickedItem, class51.anInt511, class51.borderThickness, class51.shadowColor | ~0xffffff, class51.anInt490, class51.aBoolean613));
			Class332.method3416(class51, (byte) 87);
		}
	}
}
