package net.zaros.client;
/* Class312 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class EquipmentData {
	int somethingWithWeaponDisplay = -1;
	int[] somethingWithWeapon;
	int[] somethingWithShield;
	int somethingWithShieldDisplay = -1;
	int[] colourData;

	static final boolean method3303(Class338_Sub3 class338_sub3, boolean bool, byte[][][] is, int i, byte i_0_) {
		if (!Class208.aBoolean2089)
			return false;
		int i_1_ = class338_sub3.tileX >> Class313.anInt2779;
		int i_2_ = i_1_;
		int i_3_ = class338_sub3.tileY >> Class313.anInt2779;
		int i_4_ = i_3_;
		if (class338_sub3 instanceof Class338_Sub3_Sub1) {
			i_2_ = ((Class338_Sub3_Sub1) class338_sub3).aShort6559;
			i_4_ = ((Class338_Sub3_Sub1) class338_sub3).aShort6558;
			i_1_ = ((Class338_Sub3_Sub1) class338_sub3).aShort6560;
			i_3_ = ((Class338_Sub3_Sub1) class338_sub3).aShort6564;
		}
		for (int i_5_ = i_1_; i_5_ <= i_2_; i_5_++) {
			for (int i_6_ = i_3_; i_6_ <= i_4_; i_6_++) {
				if (class338_sub3.aByte5203 < Class368_Sub9.anInt5477 && i_5_ >= Class296_Sub39_Sub4.anInt5744 && i_5_ < Class124.anInt1281 && i_6_ >= AdvancedMemoryCache.anInt1166 && i_6_ < Class41_Sub22.anInt3800) {
					if ((is != null && class338_sub3.z >= i && is[class338_sub3.z][i_5_][i_6_] == i_0_) || !class338_sub3.method3465(0) || class338_sub3.method3470(-30488, Class16_Sub2.aHa3733)) {
						if (!bool && i_5_ >= Class296_Sub45_Sub2.anInt6288 - 16 && i_5_ <= Class296_Sub45_Sub2.anInt6288 + 16 && i_6_ >= Class296_Sub39_Sub20_Sub2.anInt6728 - 16 && (i_6_ <= Class296_Sub39_Sub20_Sub2.anInt6728 + 16)) {
							if (Class338_Sub3_Sub2.aBoolean6566) {
								Class127.aClass299Array1301[Class368_Sub4.anInt5442++].method3241(class338_sub3, 0);
								Class368_Sub4.anInt5442 %= Class247.anInt2336;
							} else
								class338_sub3.method3460(-126, Class16_Sub2.aHa3733);
						}
					} else
						return false;
				}
			}
		}
		return true;
	}

	static final void method3304(byte i) {
		if (i != 76)
			method3304((byte) -102);
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask(0L, 15);
		class296_sub39_sub5.insertIntoQueue();
	}

	static final boolean method3305(byte i, String string) {
		if (i <= 47)
			return true;
		return Class41_Sub24.method493(string, 10, true, (byte) -96);
	}

	private final void init(Packet buff) {
		for (;;) {
			int opcode = buff.g1();
			if (opcode == 0)
				break;
			if (opcode != 1) {
				if (opcode != 3) {
					if (opcode == 4)
						somethingWithWeaponDisplay = buff.g1();
					else if (opcode != 5) {
						if (opcode == 6) {
							somethingWithWeapon = new int[buff.g1()];
							for (int i = 0; i < somethingWithWeapon.length; i++)
								somethingWithWeapon[i] = (buff.g1());
						}
					} else {
						somethingWithShield = new int[buff.g1()];
						for (int i = 0; i < somethingWithShield.length; i++)
							somethingWithShield[i] = buff.g1();
					}
				} else
					somethingWithShieldDisplay = buff.g1();
			} else {
				int length = buff.g1();
				colourData = new int[length];
				for (int i = 0; i < colourData.length; i++)
					colourData[i] = buff.g1();
			}
		}
	}

	EquipmentData(Js5 fs28) {
		byte[] data = fs28.get(6);
		init(new Packet(data));
		if (colourData == null)
			throw new RuntimeException("");
	}

	protected EquipmentData() {
		colourData = new int[0];
	}
}
