package net.zaros.client;

/* Class385 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public  class Billboard {
	public int id;
	public int face;
	public int anInt3257;
	public int anInt3258;

	public Billboard(int i, int i_43_, int i_44_, int i_45_) {
		anInt3258 = i_44_;
		anInt3257 = i_45_;
		face = i_43_;
		id = i;
	}

	public  Billboard method4021(int i, int i_0_) {
		if (i > -40) {
			anInt3258 = -41;
		}
		return new Billboard(id, i_0_, anInt3258, anInt3257);
	}

	public static final void method4022(int i, int i_1_, int i_2_, int i_3_, int i_4_, boolean bool, int i_5_, int i_6_) {
		if (i_1_ != ((!bool ? Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4995.method489(i_1_ + 127) : Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4998.method489(115)) ^ 0xffffffff) && i_3_ != 0 && Class296_Sub51_Sub1.anInt6335 < 50 && i_2_ != -1) {
			Class336.aClass391Array2956[Class296_Sub51_Sub1.anInt6335++] = new Class391(!bool ? (byte) 2 : (byte) 3, i_2_, i_3_, i_5_, i_6_, i_4_, i, null);
		}
	}

	public static final void method4023(int i) {
		Class296_Sub40.currentMapLoadType = Class338_Sub2.mapLoadType = -1;
		Class41_Sub28.setMapSize(0);
		for (int i_7_ = 0; i_7_ < 4; i_7_++) {
			for (int i_8_ = 0; Class198.currentMapSizeX >> 3 > i_8_; i_8_++) {
				for (int i_9_ = 0; i_9_ < Class296_Sub38.currentMapSizeY >> 3; i_9_++) {
					Class181.buildedMapParts[i_7_][i_8_][i_9_] = -1;
				}
			}
		}
		for (Class296_Sub49 class296_sub49 = (Class296_Sub49) Class98.aClass155_1055.removeFirst((byte) 115); class296_sub49 != null; class296_sub49 = (Class296_Sub49) Class98.aClass155_1055.removeNext(i ^ ~0xeb7)) {
			int i_10_ = class296_sub49.anInt4979;
			boolean bool = (i_10_ & 0x1) == 1;
			int i_11_ = class296_sub49.anInt4986 >> 3;
			int i_12_ = class296_sub49.anInt4980 >> 3;
			int i_13_ = class296_sub49.anInt4978;
			int i_14_ = class296_sub49.anInt4984;
			int i_15_ = class296_sub49.anInt4982;
			int i_16_ = class296_sub49.anInt4981;
			int i_17_ = class296_sub49.anInt4977;
			int i_18_ = class296_sub49.anInt4985;
			int i_19_ = 0;
			int i_20_ = 0;
			int i_21_ = 1;
			int i_22_ = 1;
			if (i_10_ != 1) {
				if (i_10_ == 2) {
					i_22_ = -1;
					i_19_ = i_18_ - 1;
					i_20_ = i_17_ - 1;
					i_21_ = -1;
				} else if (i_10_ == 3) {
					i_21_ = 1;
					i_19_ = i_18_ - 1;
					i_22_ = -1;
				}
			} else {
				i_20_ = i_17_ - 1;
				i_21_ = -1;
			}
			int i_23_ = i_12_;
			while (i_23_ < i_18_ + i_12_) {
				int i_24_ = i_20_;
				int i_25_ = i_11_;
				while (i_25_ < i_17_ + i_11_) {
					if (!bool) {
						Class181.buildedMapParts[i_16_][i_13_ + i_24_][i_14_ + i_19_] = (i_10_ << 1) + (i_25_ << 14) + (i_15_ << 24) + (i_23_ << 3);
					} else {
						Class181.buildedMapParts[i_16_][i_19_ + i_13_][i_24_ + i_14_] = (i_10_ << 1) + (i_25_ << 14) + (i_15_ << 24) + (i_23_ << 3);
					}
					i_25_++;
					i_24_ += i_21_;
				}
				i_23_++;
				i_19_ += i_22_;
			}
		}
		int i_26_ = Class296_Sub36.someSecondaryXteaBuffer.length;
		StaticMethods.anIntArray1844 = new int[i_26_];
		Class296_Sub51_Sub31.aByteArrayArray6509 = new byte[i_26_][];
		if (i == -3423) {
			Class166.aByteArrayArray1691 = new byte[i_26_][];
			ParticleEmitterRaw.aByteArrayArray1772 = null;
			Class243.anIntArray2317 = null;
			Class4.anIntArray70 = new int[i_26_];
			Class296_Sub43.anIntArray4940 = new int[i_26_];
			StaticMethods.aByteArrayArray3167 = new byte[i_26_][];
			Class188.anIntArray1924 = new int[i_26_];
			Class107_Sub1.aByteArrayArray5667 = new byte[i_26_][];
			Class56.anIntArray659 = new int[i_26_];
			i_26_ = 0;
			for (Class296_Sub49 class296_sub49 = (Class296_Sub49) Class98.aClass155_1055.removeFirst((byte) 114); class296_sub49 != null; class296_sub49 = (Class296_Sub49) Class98.aClass155_1055.removeNext(1001)) {
				int i_27_ = class296_sub49.anInt4986 >>> 3;
				int i_28_ = class296_sub49.anInt4980 >>> 3;
				int i_29_ = i_27_ + class296_sub49.anInt4977;
				if ((i_29_ & 0x7) == 0) {
					i_29_--;
				}
				i_29_ >>>= 3;
				int i_30_ = class296_sub49.anInt4985 + i_28_;
				if ((i_30_ & 0x7) == 0) {
					i_30_--;
				}
				i_30_ >>>= 3;
				for (int i_31_ = i_27_ >>> 3; i_31_ <= i_29_; i_31_++) {
					while_266_: for (int i_32_ = i_28_ >>> 3; i_30_ >= i_32_; i_32_++) {
						int i_33_ = i_31_ << 8 | i_32_;
						for (int i_34_ = 0; i_34_ < i_26_; i_34_++) {
							if (i_33_ == Class296_Sub43.anIntArray4940[i_34_]) {
								continue while_266_;
							}
						}
						Class296_Sub43.anIntArray4940[i_26_] = i_33_;
						Class56.anIntArray659[i_26_] = Class324.fs5.getFileIndex("m" + i_31_ + "_" + i_32_);
						Class4.anIntArray70[i_26_] = Class324.fs5.getFileIndex("l" + i_31_ + "_" + i_32_);
						Class188.anIntArray1924[i_26_] = Class324.fs5.getFileIndex("um" + i_31_ + "_" + i_32_);
						StaticMethods.anIntArray1844[i_26_] = Class324.fs5.getFileIndex("ul" + i_31_ + "_" + i_32_);
						i_26_++;
					}
				}
			}
			Class355_Sub2.xteaKeys = Class296_Sub36.someSecondaryXteaBuffer;
			Class296_Sub36.someSecondaryXteaBuffer = null;
			StaticMethods.method1010(0, 12, Class296_Sub38.currentMapSizeY >> 4, false, Class198.currentMapSizeX >> 4);
		}
	}

	static final void method4024(Class247[][][] class247s, int i) {
		if (i == -30633) {
			for (int i_35_ = 0; i_35_ < class247s.length; i_35_++) {
				Class247[][] class247s_36_ = class247s[i_35_];
				for (int i_37_ = 0; i_37_ < class247s_36_.length; i_37_++) {
					for (int i_38_ = 0; i_38_ < class247s_36_[i_37_].length; i_38_++) {
						Class247 class247 = class247s_36_[i_37_][i_38_];
						if (class247 != null) {
							if (class247.aClass338_Sub3_Sub5_2346 instanceof Interface14) {
								((Interface14) class247.aClass338_Sub3_Sub5_2346).method58(-19727);
							}
							if (class247.aClass338_Sub3_Sub3_2342 instanceof Interface14) {
								((Interface14) class247.aClass338_Sub3_Sub3_2342).method58(-19727);
							}
							if (class247.aClass338_Sub3_Sub3_2337 instanceof Interface14) {
								((Interface14) class247.aClass338_Sub3_Sub3_2337).method58(i + 10906);
							}
							if (class247.aClass338_Sub3_Sub4_2348 instanceof Interface14) {
								((Interface14) class247.aClass338_Sub3_Sub4_2348).method58(i + 10906);
							}
							if (class247.aClass338_Sub3_Sub4_2343 instanceof Interface14) {
								((Interface14) class247.aClass338_Sub3_Sub4_2343).method58(-19727);
							}
							for (Class234 class234 = class247.aClass234_2341; class234 != null; class234 = class234.aClass234_2224) {
								Class338_Sub3_Sub1 class338_sub3_sub1 = class234.aClass338_Sub3_Sub1_2226;
								if (class338_sub3_sub1 instanceof Interface14) {
									((Interface14) class338_sub3_sub1).method58(-19727);
								}
							}
						}
					}
				}
			}
		}
	}

	static final void method4025(Model class178, int i, int i_39_, int i_40_, Class96 class96, int i_41_) {
		int i_42_ = 32 / ((39 - i) / 54);
		if (class178 != null) {
			class96.method869(class178.HA(), 125, class178.EA(), class178.V(), i_41_, class178.RA(), class178.na(), class178.G(), class178.fa(), i_39_, i_40_);
		}
	}

}
