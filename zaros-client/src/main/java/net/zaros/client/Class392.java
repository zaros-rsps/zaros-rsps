package net.zaros.client;
import jaggl.OpenGL;

final class Class392 implements Interface4 {
	private Interface18[] anInterface18Array3486 = new Interface18[9];
	static int[] anIntArray3487 = new int[13];
	static int anInt3488;
	private int anInt3489;
	private int anInt3490;
	static SeekableFile[] aClass255Array3491 = new SeekableFile[37];
	private ha_Sub3 aHa_Sub3_3492;
	private int anInt3493 = 0;
	private int anInt3494 = -1;
	private int anInt3495;
	private int anInt3496;

	final void method4045(int i, int i_0_) {
		if (i == (anInt3494 ^ 0xffffffff))
			throw new RuntimeException();
		OpenGL.glDrawBuffer(Class296_Sub20.anIntArray4718[i_0_]);
	}

	final void method4046(byte i, int i_1_, Class296_Sub39_Sub4 class296_sub39_sub4) {
		if (anInt3494 == -1)
			throw new RuntimeException();
		int i_2_ = 1 << i_1_;
		if (((i_2_ ^ 0xffffffff) & anInt3490) != 0) {
			if (class296_sub39_sub4.anInt5738 != anInt3495 || class296_sub39_sub4.anInt5739 != anInt3496)
				throw new RuntimeException();
		} else {
			anInt3496 = class296_sub39_sub4.anInt5739;
			anInt3495 = class296_sub39_sub4.anInt5738;
		}
		class296_sub39_sub4.method2799(Class296_Sub20.anIntArray4718[i_1_], anInt3494, -238823728);
		anInterface18Array3486[i_1_] = class296_sub39_sub4;
		if (i >= 100)
			anInt3490 |= i_2_;
	}

	protected final void finalize() throws Throwable {
		aHa_Sub3_3492.method1273(true, anInt3489);
		super.finalize();
	}

	final void method4047(byte i, int i_3_) {
		if (anInterface18Array3486[i_3_] != null)
			anInterface18Array3486[i_3_].method73(true);
		anInt3490 &= 1 << i_3_ ^ 0xffffffff;
		anInterface18Array3486[i_3_] = null;
		if (i > -36)
			method4046((byte) -94, -66, null);
	}

	public final void method22(int i) {
		if (i != -18449)
			method18((byte) -4);
		OpenGL.glBindFramebufferEXT(36008, anInt3489);
		anInt3493 |= 0x1;
		anInt3494 = method4054(105);
	}

	public final void method20(int i) {
		OpenGL.glBindFramebufferEXT(36160, anInt3489);
		anInt3493 |= 0x4;
		if (i == -30874)
			anInt3494 = method4054(58);
	}

	private final void method4048(int i, Class69_Sub3 class69_sub3, int i_4_, int i_5_, int i_6_) {
		if (anInt3494 == -1)
			throw new RuntimeException();
		if (i_5_ != 30537)
			method20(-45);
		int i_7_ = 1 << i_6_;
		if (((i_7_ ^ 0xffffffff) & anInt3490) != 0) {
			if (anInt3495 != class69_sub3.anInt5725 || anInt3496 != class69_sub3.anInt5725)
				throw new RuntimeException();
		} else {
			anInt3496 = class69_sub3.anInt5725;
			anInt3495 = class69_sub3.anInt5725;
		}
		class69_sub3.method742(i_4_, Class296_Sub20.anIntArray4718[i_6_], i, i_5_ ^ ~0x23c1, anInt3494);
		anInterface18Array3486[i_6_] = class69_sub3;
		anInt3490 |= i_7_;
	}

	final void method4049(int i, int i_8_, int i_9_, Class69_Sub3 class69_sub3) {
		if (i_9_ != -1)
			method21(false);
		method4048(0, class69_sub3, i_8_, i_9_ + 30538, i);
	}

	public final void method19(int i) {
		OpenGL.glBindFramebufferEXT(36008, 0);
		if (i >= -23)
			anInt3493 = 12;
		anInt3493 &= ~0x1;
		anInt3494 = method4054(59);
	}

	final void method4050(Class69_Sub1 class69_sub1, int i, int i_10_) {
		method4055(i_10_, -1, class69_sub1, i);
	}

	public final void method23(int i) {
		OpenGL.glBindFramebufferEXT(36009, 0);
		anInt3493 &= ~0x2;
		if (i != 26044)
			method4050(null, 112, -1);
		anInt3494 = method4054(-101);
	}

	final void method4051(int i, byte i_11_) {
		if (i_11_ != 64)
			method4051(15, (byte) -106);
		if (anInt3494 == -1)
			throw new RuntimeException();
		OpenGL.glReadBuffer(Class296_Sub20.anIntArray4718[i]);
	}

	public static void method4052(int i) {
		aClass255Array3491 = null;
		anIntArray3487 = null;
		if (i != 0)
			anIntArray3487 = null;
	}

	public final void method21(boolean bool) {
		OpenGL.glBindFramebufferEXT(36009, anInt3489);
		if (bool)
			aHa_Sub3_3492 = null;
		anInt3493 |= 0x2;
		anInt3494 = method4054(32);
	}

	public final void method18(byte i) {
		OpenGL.glBindFramebufferEXT(36160, 0);
		anInt3493 &= ~0x4;
		if (i > -93)
			anInt3490 = -19;
		anInt3494 = method4054(-101);
	}

	final boolean method4053(byte i) {
		if (i != 118)
			method4047((byte) 33, -127);
		int i_12_ = OpenGL.glCheckFramebufferStatusEXT(anInt3494);
		if (i_12_ != 36053)
			return false;
		return true;
	}

	private final int method4054(int i) {
		if ((anInt3493 & 0x4) != 0)
			return 36160;
		if ((anInt3493 & 0x2) != 0)
			return 36009;
		if ((anInt3493 & 0x1) != 0)
			return 36008;
		int i_13_ = 118 % ((-44 - i) / 49);
		return -1;
	}

	private final void method4055(int i, int i_14_, Class69_Sub1 class69_sub1, int i_15_) {
		if (anInt3494 == i_14_)
			throw new RuntimeException();
		int i_16_ = 1 << i_15_;
		if ((anInt3490 & (i_16_ ^ 0xffffffff)) != 0) {
			if (class69_sub1.anInt5711 != anInt3495 || class69_sub1.anInt5715 != anInt3496)
				throw new RuntimeException();
		} else {
			anInt3495 = class69_sub1.anInt5711;
			anInt3496 = class69_sub1.anInt5715;
		}
		class69_sub1.method729(anInt3494, Class296_Sub20.anIntArray4718[i_15_], i, 126);
		anInterface18Array3486[i_15_] = class69_sub1;
		anInt3490 |= i_16_;
	}

	Class392(ha_Sub3 var_ha_Sub3) {
		if (!var_ha_Sub3.aBoolean4190)
			throw new IllegalStateException("");
		aHa_Sub3_3492 = var_ha_Sub3;
		OpenGL.glGenFramebuffersEXT(1, Class234.anIntArray2225, 0);
		anInt3489 = Class234.anIntArray2225[0];
	}
}
