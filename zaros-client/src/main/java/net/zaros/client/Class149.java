package net.zaros.client;

/* Class149 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class149 {
	private AdvancedMemoryCache aClass113_1520 = new AdvancedMemoryCache(64);
	static HashTable interfaceSettings = new HashTable(512);
	private Js5 aClass138_1522;
	static int anInt1523 = 0;

	final Class7 method1515(int i, int i_0_) {
		Class7 class7;
		synchronized (aClass113_1520) {
			class7 = (Class7) aClass113_1520.get((long) i);
		}
		if (class7 != null)
			return class7;
		byte[] is;
		synchronized (aClass138_1522) {
			is = aClass138_1522.getFile(54, i);
		}
		if (i_0_ != -15156)
			method1515(55, -74);
		class7 = new Class7();
		if (is != null)
			class7.method186(new Packet(is), 0);
		synchronized (aClass113_1520) {
			aClass113_1520.put(class7, (long) i);
		}
		return class7;
	}

	final void method1516(int i) {
		synchronized (aClass113_1520) {
			aClass113_1520.clearSoftReferences();
		}
		if (i != 17474)
			method1515(46, 82);
	}

	final void method1517(byte i) {
		synchronized (aClass113_1520) {
			if (i != 115)
				interfaceSettings = null;
			aClass113_1520.clear();
		}
	}

	final void method1518(byte i, int i_1_) {
		synchronized (aClass113_1520) {
			aClass113_1520.clean(i_1_);
		}
		if (i < 50)
			method1516(74);
	}

	public static void method1519(int i) {
		if (i == 0)
			interfaceSettings = null;
	}

	Class149(GameType class35, int i, Js5 class138) {
		aClass138_1522 = class138;
		if (aClass138_1522 != null)
			aClass138_1522.getLastFileId(54);
	}
}
