package net.zaros.client;
/* Class154 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;
import java.util.Hashtable;

abstract class Class154 {
	static int anInt1580;
	static Hashtable aHashtable1581 = new Hashtable();
	static IncomingPacket aClass231_1582 = new IncomingPacket(36, -1);
	static Class186[] aClass186Array1583;
	static boolean aBoolean1584 = false;

	abstract void method1555(int i);

	abstract boolean method1556(boolean bool, int i) throws IOException;

	public Class154() {
		/* empty */
	}

	static final void method1557(byte i) {
		if (za.aClass324Array5088 != null) {
			for (int i_0_ = 0; Class349.anInt3036 > i_0_; i_0_++)
				za.aClass324Array5088[i_0_] = null;
			za.aClass324Array5088 = null;
		}
		if (Class116.aClass324Array3678 != null) {
			for (int i_1_ = 0; Class77.anInt880 > i_1_; i_1_++)
				Class116.aClass324Array3678[i_1_] = null;
			Class116.aClass324Array3678 = null;
		}
		if (Class343_Sub1.aClass324Array5273 != null) {
			for (int i_2_ = 0; Class22.anInt249 > i_2_; i_2_++)
				Class343_Sub1.aClass324Array5273[i_2_] = null;
			Class343_Sub1.aClass324Array5273 = null;
		}
		Class213.anInt2110 = Class123_Sub1_Sub1.anInt5813 = -1;
		Class296_Sub36.anIntArrayArrayArray4870 = null;
		if (i >= -58)
			aBoolean1584 = true;
		Class29.aClass324Array305 = null;
		Class365.anIntArray3115 = null;
	}

	abstract void method1558(int i, byte[] is, int i_3_, int i_4_) throws IOException;

	abstract int method1559(byte i, int i_5_, byte[] is, int i_6_) throws IOException;

	static final void method1560(boolean bool, Js5 class138, Class398 class398) {
		Class344.aString3008 = "";
		Class366_Sub9.aClass398_5420 = class398;
		if (bool == true) {
			Class268.aClass138_2494 = class138;
			if (Class5.aString81.startsWith("win"))
				Class344.aString3008 += "windows/";
			else if (!Class5.aString81.startsWith("linux")) {
				if (Class5.aString81.startsWith("mac"))
					Class344.aString3008 += "macos/";
			} else
				Class344.aString3008 += "linux/";
			if (Class366_Sub9.aClass398_5420.aBoolean3321)
				Class344.aString3008 += "msjava/";
			else if (!Class5.aString82.startsWith("amd64") && !Class5.aString82.startsWith("x86_64")) {
				if (!Class5.aString82.startsWith("i386") && !Class5.aString82.startsWith("i486") && !Class5.aString82.startsWith("i586") && !Class5.aString82.startsWith("x86")) {
					if (!Class5.aString82.startsWith("ppc"))
						Class344.aString3008 += "universal/";
					else
						Class344.aString3008 += "ppc/";
				} else
					Class344.aString3008 += "x86/";
			} else
				Class344.aString3008 += "x86_64/";
		}
	}

	abstract void method1561(int i);

	public static void method1562(boolean bool) {
		aClass231_1582 = null;
		aClass186Array1583 = null;
		aHashtable1581 = null;
		if (bool != true)
			aBoolean1584 = true;
	}
}
