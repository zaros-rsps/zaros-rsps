package net.zaros.client;
/* Class215 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.lang.reflect.Field;

final class Class215 {
	static IncomingPacket aClass231_2112 = new IncomingPacket(142, 10);

	public static void method2023(byte i) {
		aClass231_2112 = null;
		if (i <= 126)
			aClass231_2112 = null;
	}

	static final int method2024(boolean bool) {
		if (GraphicsLoader.anInt2480 == 0) {
			Class200.aClass200_2011.method1951(4977, new Class340("jaclib"));
			if (Class200.aClass200_2011.method1953(0).method79(20667) != 100)
				return 1;
			if (!((Class340) Class200.aClass200_2011.method1953(0)).method3620((byte) -109))
				Class246.aClient2332.method93(false);
			GraphicsLoader.anInt2480 = 1;
		}
		if (GraphicsLoader.anInt2480 == 1) {
			Class296_Sub9_Sub1.aClass200Array5978 = Class200.method1948((byte) -77);
			Class200.aClass200_2010.method1951(4977, new Class300(Class296_Sub51_Sub21.fs28));
			Class200.aClass200_2013.method1951(4977, new Class340("jaggl"));
			Class200.aClass200_2014.method1951(4977, new Class340("jagdx"));
			Class200.aClass200_2015.method1951(4977, new Class340("jagmisc"));
			Class200.aClass200_2016.method1951(4977, new Class340("sw3d"));
			Class200.aClass200_2017.method1951(4977, new Class340("hw3d"));
			Class200.aClass200_2018.method1951(4977, new Class340("jagtheora"));
			Class200.aClass200_2019.method1951(4977, new Class300(Class122_Sub1_Sub1.aClass138_6604));
			Class200.aClass200_2020.method1951(4977, new Class300(StaticMethods.aClass138_5927));
			Class200.aClass200_2021.method1951(4977, new Class300(Class296_Sub39_Sub1.fs2));
			Class200.aClass200_2022.method1951(4977, new Class300(Class326.fs16));
			Class200.aClass200_2023.method1951(4977, new Class300(Class296_Sub30.aClass138_4822));
			Class200.aClass200_2024.method1951(4977, new Class300(Class199.aClass138_2005));
			Class200.aClass200_2025.method1951(4977, new Class300(Class63.aClass138_726));
			Class200.aClass200_2026.method1951(4977, new Class300(Class196.fs20));
			Class200.aClass200_2027.method1951(4977, new Class300(Class365.gfxFS));
			Class200.aClass200_2028.method1951(4977, new Class300(Class296_Sub51_Sub17.aClass138_6429));
			Class200.aClass200_2029.method1951(4977, new Class300(Class296_Sub51_Sub34.aClass138_6523));
			Class200.aClass200_2030.method1951(4977, new Class300(Class296_Sub13.aClass138_4653));
			Class200.aClass200_2031.method1951(4977, new Class300(Class324.aClass138_2862));
			Class200.aClass200_2032.method1951(4977, new Class300(Class338_Sub2.aClass138_5201));
			Class200.aClass200_2033.method1951(4977, new Class317(Class296_Sub51_Sub29.aClass138_6494, "huffman"));
			Class200.aClass200_2034.method1951(4977, new Class300(Class360_Sub6.fs3));
			Class200.aClass200_2035.method1951(4977, new Class300(Class72.cs2FS));
			Class200.aClass200_2036.method1951(4977, new Class300(LookupTable.aClass138_53));
			Class200.aClass200_2037.method1951(4977, new Class387((Class205_Sub2.aClass138_5631), "details"));
			for (int i = 0; Class296_Sub9_Sub1.aClass200Array5978.length > i; i++) {
				if (Class296_Sub9_Sub1.aClass200Array5978[i].method1953(0) == null)
					throw new RuntimeException();
			}
			int i = 0;
			Class200[] class200s = Class296_Sub9_Sub1.aClass200Array5978;
			for (int i_0_ = 0; i_0_ < class200s.length; i_0_++) {
				Class200 class200 = class200s[i_0_];
				int i_1_ = class200.method1950(0);
				int i_2_ = class200.method1953(0).method79(20667);
				i += i_2_ * i_1_ / 100;
			}
			Class42_Sub4.anInt3851 = i;
			GraphicsLoader.anInt2480 = 2;
		}
		if (Class296_Sub9_Sub1.aClass200Array5978 == null)
			return 100;
		int i = 0;
		int i_3_ = 0;
		boolean bool_4_ = true;
		if (bool)
			method2025((byte) -107, -55, 23);
		Class200[] class200s = Class296_Sub9_Sub1.aClass200Array5978;
		for (int i_5_ = 0; i_5_ < class200s.length; i_5_++) {
			Class200 class200 = class200s[i_5_];
			int i_6_ = class200.method1950(0);
			int i_7_ = class200.method1953(0).method79(20667);
			i += i_6_;
			i_3_ += i_6_ * i_7_ / 100;
			if (i_7_ < 100)
				bool_4_ = false;
		}
		if (bool_4_) {
			if (!((Class340) Class200.aClass200_2015.method1953(0)).method3620((byte) -123))
				Class246.aClient2332.method87(-103);
			if (!((Class340) Class200.aClass200_2018.method1953(0)).method3620((byte) 64))
				AnimationsLoader.aBoolean2763 = Class246.aClient2332.method101(-1);
			Class296_Sub9_Sub1.aClass200Array5978 = null;
		}
		i -= Class42_Sub4.anInt3851;
		i_3_ -= Class42_Sub4.anInt3851;
		int i_8_ = i > 0 ? i_3_ * 100 / i : 100;
		if (!bool_4_ && i_8_ > 99)
			i_8_ = 99;
		return i_8_;
	}

	static final boolean method2025(byte i, int i_9_, int i_10_) {
		if (i != -20)
			return false;
		return false;
	}

	static final int method2026(int i) {
		int i_11_ = 0;
		if (i < 53)
			aClass231_2112 = null;
		Field[] fields = Class296_Sub50.class.getDeclaredFields();
		Field[] fields_12_ = fields;
		for (int i_13_ = 0; i_13_ < fields_12_.length; i_13_++) {
			Field field = fields_12_[i_13_];
			if (Class41.class.isAssignableFrom(field.getType()))
				i_11_++;
		}
		return i_11_ + 1;
	}

	static final void method2027(int i, float[] fs, int i_14_, float f, int i_15_, int i_16_, int i_17_, float f_18_, float[] fs_19_, int i_20_, int i_21_, int i_22_) {
		i -= i_14_;
		i_20_ -= i_16_;
		i_15_ -= i_22_;
		float f_23_ = ((float) i_20_ * fs[2] + (fs[1] * (float) i_15_ + (float) i * fs[0]));
		float f_24_ = (fs[3] * (float) i + (float) i_15_ * fs[4] + (float) i_20_ * fs[5]);
		float f_25_ = ((float) i_20_ * fs[8] + (fs[7] * (float) i_15_ + (float) i * fs[6]));
		if (i_17_ != -3)
			method2025((byte) 69, 81, 120);
		float f_26_ = ((float) Math.atan2((double) f_23_, (double) f_25_) / 6.2831855F + 0.5F);
		if (f != 1.0F)
			f_26_ *= f;
		float f_27_ = f_24_ + 0.5F + f_18_;
		if (i_21_ == 1) {
			float f_28_ = f_26_;
			f_26_ = -f_27_;
			f_27_ = f_28_;
		} else if (i_21_ == 2) {
			f_27_ = -f_27_;
			f_26_ = -f_26_;
		} else if (i_21_ == 3) {
			float f_29_ = f_26_;
			f_26_ = f_27_;
			f_27_ = -f_29_;
		}
		fs_19_[1] = f_27_;
		fs_19_[0] = f_26_;
	}
}
