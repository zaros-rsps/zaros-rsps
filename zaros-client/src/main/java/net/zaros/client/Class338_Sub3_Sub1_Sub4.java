package net.zaros.client;

/* Class338_Sub3_Sub1_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub3_Sub1_Sub4 extends Class338_Sub3_Sub1 implements Interface14 {
	private boolean aBoolean6639;
	Class380 aClass380_6640;
	static AdvancedMemoryCache aClass113_6641 = new AdvancedMemoryCache(50);
	private Class96 aClass96_6642;
	private boolean aBoolean6643 = true;
	static OutgoingPacket aClass311_6644 = new OutgoingPacket(60, 1);

	final int method3466(byte i) {
		if (i < 77)
			method3467(121, -109, null, false, -17, -46, null);
		return aClass380_6640.method3980(true);
	}

	final boolean method3459(int i) {
		if (i != 0)
			return true;
		return false;
	}

	final Class338_Sub2 method3473(ha var_ha, byte i) {
		Model class178 = aClass380_6640.method3975(true, false, var_ha, 2048, (byte) 39);
		if (class178 == null)
			return null;
		if (i > -84)
			return null;
		Class373 class373 = var_ha.f();
		class373.method3902(tileX, anInt5213, tileY);
		Class338_Sub2 class338_sub2 = Class144.method1481(1, true, aBoolean6639);
		aClass380_6640.method3977(class373, true, class178, aShort6559, false, var_ha, aShort6558, aShort6560, aShort6564);
		if (!Class296_Sub39_Sub10.aBoolean6177)
			class178.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
		else
			class178.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
		if (aClass380_6640.aClass338_Sub1_3202 != null) {
			Class390 class390 = aClass380_6640.aClass338_Sub1_3202.method3444();
			if (Class296_Sub39_Sub10.aBoolean6177)
				var_ha.a(class390, ModeWhat.anInt1192);
			else
				var_ha.a(class390);
		}
		aBoolean6643 = class178.F() || aClass380_6640.aClass338_Sub1_3202 != null;
		if (aClass96_6642 != null)
			Billboard.method4025(class178, 96, tileY, anInt5213, aClass96_6642, tileX);
		else
			aClass96_6642 = Class41_Sub21.method478(anInt5213, class178, tileX, tileY, (byte) -34);
		return class338_sub2;
	}

	final void method3460(int i, ha var_ha) {
		Model class178 = aClass380_6640.method3975(true, true, var_ha, 262144, (byte) 39);
		int i_0_ = -31 / ((-41 - i) / 62);
		if (class178 != null) {
			Class373 class373 = var_ha.f();
			class373.method3902(tileX, anInt5213, tileY);
			aClass380_6640.method3977(class373, false, class178, aShort6559, false, var_ha, aShort6558, aShort6560, aShort6564);
		}
	}

	public final void method56(ha var_ha, byte i) {
		aClass380_6640.method3976(262144, var_ha);
		if (i != -117)
			method3475(-17, -128, null, 64);
	}

	final boolean method3468(int i) {
		if (i > -29)
			aBoolean6643 = false;
		return false;
	}

	public final int method59(int i) {
		if (i < 16)
			aClass380_6640 = null;
		return aClass380_6640.anInt3200;
	}

	public final int method54(int i) {
		if (i != -11077)
			aClass380_6640 = null;
		return aClass380_6640.anInt3203;
	}

	final int method3462(byte i) {
		if (i != 28)
			method3459(-115);
		return aClass380_6640.method3982(i ^ 0x3808);
	}

	public static void method3541(byte i) {
		aClass311_6644 = null;
		aClass113_6641 = null;
		if (i >= -26)
			loadCS2(2, -44);
	}

	Class338_Sub3_Sub1_Sub4(ha var_ha, ObjectDefinition class70, int i, int i_1_, int i_2_, int i_3_, int i_4_, boolean bool, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_) {
		super(i, i_1_, i_2_, i_3_, i_4_, i_5_, i_6_, i_7_, i_8_, class70.anInt804 == 1, StaticMethods.method2457(i_9_, i_10_, 0));
		aClass380_6640 = new Class380(var_ha, class70, i_9_, i_10_, z, i_1_, this, bool, i_11_);
		aBoolean6639 = class70.interactonType != 0 && !bool;
	}

	public final void method58(int i) {
		if (i != -19727)
			method3462((byte) 69);
	}

	static final CS2Script loadCS2(int fileID, int childID) {
		/*if (fileID != 3238 && fileID != 2948) {
			System.out.println("Class338_Sub3_Sub1_Sub4.loadCS2");
			System.out.println("fileID = [" + fileID + "], childID = [" + childID + "]");
		}*/
		CS2Script stack = ((CS2Script) StaticMethods.scriptsCache.get((long) fileID));
		if (stack != null)
			return stack;
		byte[] data = Class72.cs2FS.getFile(fileID, childID);
		if (data == null || data.length <= 1)
			return null;
		try {
			stack = MaterialRaw.readScript(data);
		} catch (Exception exception) {
			throw new RuntimeException(exception.getMessage() + " S: " + fileID);
		}
		StaticMethods.scriptsCache.put((long) fileID, stack);
		return stack;
	}

	public final int method57(byte i) {
		if (i <= 83)
			aClass311_6644 = null;
		return aClass380_6640.anInt3216;
	}

	public final void method60(byte i, ha var_ha) {
		aClass380_6640.method3987(var_ha, (byte) -53);
		int i_13_ = 62 / ((-23 - i) / 35);
	}

	final Class96 method3461(ha var_ha, int i) {
		int i_14_ = 30 / ((79 - i) / 44);
		return aClass96_6642;
	}

	final boolean method3469(int i) {
		if (i < 82)
			aClass311_6644 = null;
		return aBoolean6643;
	}

	final boolean method3475(int i, int i_15_, ha var_ha, int i_16_) {
		if (i_15_ > -48)
			return true;
		Model class178 = aClass380_6640.method3975(false, false, var_ha, 131072, (byte) 39);
		if (class178 == null)
			return false;
		Class373 class373 = var_ha.f();
		class373.method3902(tileX, anInt5213, tileY);
		if (!Class296_Sub39_Sub10.aBoolean6177)
			return class178.method1732(i_16_, i, class373, false, 0);
		return class178.method1731(i_16_, i, class373, false, 0, ModeWhat.anInt1192);
	}

	final void method3472(byte i) {
		int i_17_ = -100 % ((-56 - i) / 38);
		throw new IllegalStateException();
	}

	final void method3467(int i, int i_18_, Class338_Sub3 class338_sub3, boolean bool, int i_19_, int i_20_, ha var_ha) {
		int i_21_ = -110 % ((20 - i_20_) / 48);
		throw new IllegalStateException();
	}

	static final void method3543(int i, ha var_ha, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_, int i_27_) {
		if (i > -118)
			aClass113_6641 = null;
		Class296_Sub51_Sub36.aHa6529 = var_ha;
		Class373_Sub3.aClass373_5612 = Class296_Sub51_Sub36.aHa6529.m();
		Class338_Sub9.aClass373_5269 = Class296_Sub51_Sub36.aHa6529.m();
		Class94.aClass373_1016 = Class296_Sub51_Sub36.aHa6529.m();
		Class290.anInt2656 = i_23_;
		ConfigsRegister.anInt3674 = 1;
		Class292.anInt2665 = 0;
		Class241_Sub2_Sub1.anInterface13_5901 = null;
		Class244.anInt2321 = i_27_;
		Class277.anInt2530 = i_22_;
		Class395.anInt3317 = i_24_;
		Class404.anInt3385 = 0;
		Class296_Sub15_Sub3.method2539(i_26_, i_25_, (byte) -116);
	}

	public final boolean method55(byte i) {
		if (i != -57)
			aClass380_6640 = null;
		return aClass380_6640.method3988(i + 61);
	}

	final void method3544(int i, Class375 class375) {
		if (i != 0)
			method3541((byte) 0);
		aClass380_6640.method3984(-5581, class375);
	}
}
