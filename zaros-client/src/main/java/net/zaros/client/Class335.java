package net.zaros.client;

/* Class335 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class335 {
	private static char[] aCharArray2951 = new char[64];
	private Js5 aClass138_2952;
	static SubInPacket aClass260_2953;
	static int anInt2954;
	private AdvancedMemoryCache aClass113_2955 = new AdvancedMemoryCache(64);

	static final void method3425(int i, boolean bool, String string) {
		string = string.toLowerCase();
		short[] is = new short[i];
		int i_0_ = 0;
		int i_1_ = !bool ? 0 : 32768;
		int i_2_ = ((bool ? Class338_Sub3_Sub4_Sub1.aClass292_6659.anInt2660 : Class338_Sub3_Sub4_Sub1.aClass292_6659.anInt2662) + i_1_);
		for (int i_3_ = i_1_; i_2_ > i_3_; i_3_++) {
			Class296_Sub39_Sub10 class296_sub39_sub10 = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_3_, -126);
			if (class296_sub39_sub10.aBoolean6183 && class296_sub39_sub10.method2834(i ^ 0x10).toLowerCase().indexOf(string) != -1) {
				if (i_0_ >= 50) {
					StaticMethods.aShortArray5932 = null;
					Class342.anInt2989 = -1;
					return;
				}
				if (is.length <= i_0_) {
					short[] is_4_ = new short[is.length * 2];
					for (int i_5_ = 0; i_5_ < i_0_; i_5_++)
						is_4_[i_5_] = is[i_5_];
					is = is_4_;
				}
				is[i_0_++] = (short) i_3_;
			}
		}
		Class342.anInt2989 = i_0_;
		StaticMethods.aShortArray5932 = is;
		Class203.anInt3569 = 0;
		String[] strings = new String[Class342.anInt2989];
		for (int i_6_ = 0; i_6_ < Class342.anInt2989; i_6_++)
			strings[i_6_] = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(is[i_6_], Class294.method2420(i, -109)).method2834(0);
		Class304.method3272(true, strings, StaticMethods.aShortArray5932);
	}

	final void method3426(int i) {
		synchronized (aClass113_2955) {
			aClass113_2955.clear();
		}
		if (i >= -83)
			method3431((byte) 77, -86);
	}

	final Class153 method3427(byte i, int i_7_) {
		Class153 class153;
		synchronized (aClass113_2955) {
			class153 = (Class153) aClass113_2955.get((long) i_7_);
		}
		if (class153 != null)
			return class153;
		if (i < 72)
			aClass113_2955 = null;
		byte[] is;
		synchronized (aClass138_2952) {
			is = aClass138_2952.getFile(31, i_7_);
		}
		class153 = new Class153();
		if (is != null)
			class153.method1553(false, new Packet(is));
		synchronized (aClass113_2955) {
			aClass113_2955.put(class153, (long) i_7_);
		}
		return class153;
	}

	static final void method3428(byte i) {
		if (i <= -33) {
			Class368_Sub1.method3810(-48001, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub24_5016.method494(125) == 1), 2, 22050);
			Class274.aClass381_2524 = Class338_Sub3_Sub5_Sub1.method3585(Class230.aCanvas2209, 0, 22050, -14959, Class252.aClass398_2383);
			Class123_Sub2.method1071(Class211.method2016(-24345, null), true, true);
			Class325.aClass381_2869 = Class338_Sub3_Sub5_Sub1.method3585(Class230.aCanvas2209, 1, 2048, -14959, Class252.aClass398_2383);
			Class16_Sub3.aClass296_Sub45_Sub5_3734 = new Class296_Sub45_Sub5();
			Class325.aClass381_2869.method3996(-101, Class16_Sub3.aClass296_Sub45_Sub5_3734);
			Class264.aClass288_2474 = new Class288(22050, Class298.anInt2699);
			Class296_Sub34.method2734(255);
		}
	}

	final void method3429(byte i) {
		synchronized (aClass113_2955) {
			int i_8_ = 34 / ((57 - i) / 41);
			aClass113_2955.clearSoftReferences();
		}
	}

	public static void method3430(int i) {
		aClass260_2953 = null;
		aCharArray2951 = null;
		if (i != 42)
			method3425(-7, false, null);
	}

	final void method3431(byte i, int i_9_) {
		synchronized (aClass113_2955) {
			aClass113_2955.clean(i_9_);
		}
		if (i < 43)
			anInt2954 = 112;
	}

	Class335(GameType class35, int i, Js5 class138) {
		aClass138_2952 = class138;
		aClass138_2952.getLastFileId(31);
	}

	static {
		for (int i = 0; i < 26; i++)
			aCharArray2951[i] = (char) (i + 65);
		for (int i = 26; i < 52; i++)
			aCharArray2951[i] = (char) (i + 97 - 26);
		for (int i = 52; i < 62; i++)
			aCharArray2951[i] = (char) (i + 48 - 52);
		aCharArray2951[63] = '-';
		aCharArray2951[62] = '*';
		aClass260_2953 = new SubInPacket(6, 7);
	}
}
