package net.zaros.client;

/* Class31 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.lang.reflect.Constructor;

final class Class31 {
	char aChar323;
	static Class245 aClass245_324;
	static int[][] anIntArrayArray325;
	int anInt326 = 1;

	static final ha method333(int i, Canvas canvas, int i_0_, int i_1_, d var_d) {
		if (i != -1)
			return null;
		try {
			Class<?> clazz = Class.forName("oa");
			Constructor<?> constructor = clazz.getDeclaredConstructor(Canvas.class, d.class, int.class, int.class);
			return (ha) constructor.newInstance(canvas, var_d, i_1_, i_0_);
		} catch (Throwable e) {
			return null;
		}
	}

	private final void method334(Packet class296_sub17, int i, byte i_2_) {
		if (i != 1) {
			if (i == 2)
				anInt326 = 0;
		} else
			aChar323 = Mesh.method1387((byte) -45, class296_sub17.g1b());
		if (i_2_ != -119)
			aChar323 = '\035';
	}

	static final void method335(int i, int i_3_, int i_4_, int i_5_) {
		if (i_3_ != -10322)
			aClass245_324 = null;
		if (i_4_ == 1008)
			CS2Executor.method1520(Class296_Sub9.aClass81_4634, i_5_, i);
		else if (i_4_ == 1010)
			CS2Executor.method1520(Class296_Sub39_Sub1.aClass81_6120, i_5_, i);
		else if (i_4_ == 1007)
			CS2Executor.method1520(Class18.aClass81_6158, i_5_, i);
		else if (i_4_ == 1011)
			CS2Executor.method1520(Class105_Sub1.aClass81_5691, i_5_, i);
		else if (i_4_ == 1001)
			CS2Executor.method1520(Class296_Sub51_Sub18.aClass81_6435, i_5_, i);
	}

	final void method336(Packet class296_sub17, int i) {
		if (i != 1007)
			anIntArrayArray325 = null;
		for (;;) {
			int i_6_ = class296_sub17.g1();
			if (i_6_ == 0)
				break;
			method334(class296_sub17, i_6_, (byte) -119);
		}
	}

	public static void method337(int i) {
		aClass245_324 = null;
		anIntArrayArray325 = null;
		if (i >= -57)
			method338(-55, 47);
	}

	static final void method338(int i, int i_7_) {
		if (i_7_ != -2)
			method335(-77, -48, -88, -1);
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i, 21);
		class296_sub39_sub5.insertIntoQueue();
	}

	public Class31() {
		/* empty */
	}

	static final boolean method339(int i, byte i_8_, int i_9_) {
		if (i_8_ != 95)
			method333(50, null, 62, 125, null);
		if (Class205_Sub2.method1982(544, i, i_9_) | (i_9_ & 0x10000) != 0 || Class404.method4167((byte) 125, i, i_9_))
			return true;
		if ((i & 0x37) != 0 || !Class368_Sub8.method3836(i, i_9_, i_8_ - 96))
			return false;
		return true;
	}
}
