package net.zaros.client;

import java.awt.Insets;
import java.awt.Rectangle;

public abstract class s {
	public int[][] anIntArrayArray2831;
	public int anInt2832;
	public static Class338_Sub3_Sub1[] aClass338_Sub3_Sub1Array2833;
	public int anInt2834;
	public int anInt2835;
	public int anInt2836;
	public static Rectangle[] aRectangleArray2837 = new Rectangle[100];

	public abstract void method3348(int i, int i_0_, int i_1_, boolean[][] bools, boolean bool, int i_2_, int i_3_);

	public int method3349(int i, int i_4_, int i_5_) {
		int i_6_ = i_5_ >> anInt2835;
		int i_7_ = i_4_ >> anInt2835;
		if (i_6_ < i || i_7_ < 0 || i_6_ > anInt2832 - 1 || anInt2834 - 1 < i_7_)
			return 0;
		int i_8_ = i_5_ & anInt2836 - 1;
		int i_9_ = anInt2836 - 1 & i_4_;
		int i_10_ = (((anInt2836 - i_8_) * anIntArrayArray2831[i_6_][i_7_]
				+ anIntArrayArray2831[i_6_ + 1][i_7_] * i_8_) >> anInt2835);
		int i_11_ = ((anIntArrayArray2831[i_6_][i_7_ + 1] * (-i_8_ + anInt2836)
				+ anIntArrayArray2831[i_6_ + 1][i_7_ + 1] * i_8_) >> anInt2835);
		return i_9_ * i_11_ + (-i_9_ + anInt2836) * i_10_ >> anInt2835;
	}

	public abstract void method3350(int i, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_, int i_17_,
			boolean[][] bools);

	public abstract boolean method3351(r var_r, int i, int i_18_, int i_19_, int i_20_, boolean bool);

	public abstract r fa(int i, int i_21_, r var_r);

	public abstract void wa(r var_r, int i, int i_22_, int i_23_, int i_24_, boolean bool);

	public abstract void method3352(int i, int i_25_);

	public abstract void YA();

	static public void method3353(boolean bool, int i, int i_26_, int i_27_, int i_28_, int i_29_) {
		if (Animator.aFrame435 != null && (i_29_ != 3 || i_26_ != Applet_Sub1.anInt11 || i != Class221.anInt2155)) {
			Class41.method388(i_27_ + 39163, Animator.aFrame435, Class252.aClass398_2383);
			Animator.aFrame435 = null;
		}
		if (i_29_ == 3 && Animator.aFrame435 == null) {
			Animator.aFrame435 = Class284.method2362(i, 0, 0, Class252.aClass398_2383, 0, i_26_);
			if (Animator.aFrame435 != null) {
				Class221.anInt2155 = i;
				Applet_Sub1.anInt11 = i_26_;
				Class368_Sub4.method3820(1);
			}
		}
		if (i_29_ == 3 && Animator.aFrame435 == null)
			method3353(true, -1, -1, -22614, i_28_,
					Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_5003.method444(121));
		else {
			java.awt.Container container;
			if (Animator.aFrame435 == null) {
				if (Class340.aFrame3707 != null) {
					Insets insets = Class340.aFrame3707.getInsets();
					StaticMethods.anInt1838 = (Class340.aFrame3707.getSize().width - insets.left - insets.right);
					Class152.anInt1568 = (Class340.aFrame3707.getSize().height - insets.top - insets.bottom);
					container = Class340.aFrame3707;
				} else {
					if (CS2Script.anApplet6140 == null)
						container = Class55.anApplet_Sub1_656;
					else
						container = CS2Script.anApplet6140;
					StaticMethods.anInt1838 = container.getSize().width;
					Class152.anInt1568 = container.getSize().height;
				}
			} else {
				StaticMethods.anInt1838 = i_26_;
				Class152.anInt1568 = i;
				container = Animator.aFrame435;
			}
			if (i_29_ != 1)
				Class160.method1618(-8783);
			else {
				Class241.anInt2301 = Class368_Sub7.anInt5463;
				Class384.anInt3254 = Class296_Sub15_Sub1.anInt5996;
				CS2Stack.anInt2251 = 0;
				Class241_Sub2_Sub2.anInt5908 = (StaticMethods.anInt1838 - Class368_Sub7.anInt5463) / 2;
			}
			if (Class41_Sub29.modeWhere != BITConfigsLoader.liveModeWhere) {
				if (Class241.anInt2301 < 1024 && Class384.anInt3254 < 768) {
					/* empty */
				}
			}
			if (!bool) {
				Class230.aCanvas2209.setSize(Class241.anInt2301, Class384.anInt3254);
				if (Class368_Sub5_Sub2.aBoolean6597)
					Class303.method3268(Class230.aCanvas2209, -2);
				else
					Class41_Sub13.aHa3774.a(Class230.aCanvas2209, Class241.anInt2301, Class384.anInt3254);
				if (Class340.aFrame3707 != container)
					Class230.aCanvas2209.setLocation((Class241_Sub2_Sub2.anInt5908), CS2Stack.anInt2251);
				else {
					Insets insets = Class340.aFrame3707.getInsets();
					Class230.aCanvas2209.setLocation(Class241_Sub2_Sub2.anInt5908 + insets.left,
							insets.top + CS2Stack.anInt2251);
				}
			} else
				Class42_Sub1.method533(true);
			if (i_29_ < 2)
				Class236.aBoolean2231 = false;
			else
				Class236.aBoolean2231 = true;
			if (Class99.anInt1064 != -1)
				Animator.method569(true, (byte) 90);
			if (i_27_ != -22614)
				aClass338_Sub3_Sub1Array2833 = null;
			if (Class296_Sub45_Sub2.aClass204_6277.aClass154_2045 != null
					&& Class130.method1376(Class366_Sub6.anInt5392, i_27_ ^ 0x61d7))
				Class300.method3248((byte) 19);
			for (int i_30_ = 0; i_30_ < 100; i_30_++)
				Class360_Sub9.aBooleanArray5345[i_30_] = true;
			Class78.aBoolean3430 = true;
		}
	}

	public abstract void method3354(int i, int i_31_, int i_32_, boolean[][] bools, boolean bool, int i_33_);

	public abstract void U(int i, int i_34_, int[] is, int[] is_35_, int[] is_36_, int[] is_37_, int[] is_38_,
			int[] is_39_, int[] is_40_, int[] is_41_, int i_42_, int i_43_, int i_44_, boolean bool);

	public abstract void ka(int i, int i_45_, int i_46_);

	public s(int i, int i_47_, int i_48_, int[][] is) {
		anInt2832 = i;
		anInt2834 = i_47_;
		int i_49_ = 0;
		while (i_48_ > 1) {
			i_48_ >>= 1;
			i_49_++;
		}
		anInt2836 = 1 << i_49_;
		anIntArrayArray2831 = is;
		anInt2835 = i_49_;
	}

	public int method3355(int i, byte i_50_, int i_51_) {
		if (i_50_ >= -104)
			aRectangleArray2837 = null;
		return anIntArrayArray2831[i_51_][i];
	}

	public abstract void method3356(int i, int i_52_, int[] is, int[] is_53_, int[] is_54_, int[] is_55_, int[] is_56_,
			int[] is_57_, int[] is_58_, int[] is_59_, int[] is_60_, int[] is_61_, int[] is_62_, int i_63_, int i_64_,
			int i_65_, boolean bool);

	public abstract void CA(r var_r, int i, int i_66_, int i_67_, int i_68_, boolean bool);

	public abstract void method3357(Class296_Sub35 class296_sub35, int[] is);

	public static void method3358(int i) {
		aClass338_Sub3_Sub1Array2833 = null;
		if (i <= -61)
			aRectangleArray2837 = null;
	}

	static {
		for (int i = 0; i < 100; i++)
			aRectangleArray2837[i] = new Rectangle();
	}
}
