package net.zaros.client;

/* Class142 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class142 implements Runnable {
	private Class299 aClass299_1424;
	private int[] anIntArray1425 = new int[3];
	private long aLong1426;
	private volatile boolean aBoolean1427;
	private ha aHa1428;
	private int anInt1429;
	private volatile boolean aBoolean1430;
	private volatile boolean aBoolean1431 = true;
	private Class296_Sub35[] aClass296_Sub35Array1432;

	final void method1472(Class299 class299) {
		if (aClass299_1424 != null)
			aClass299_1424.method3242(null, 0);
		aClass299_1424 = class299;
		if (aClass299_1424 != null)
			aClass299_1424.method3242(this, 0);
	}

	private final void method1473() {
		aHa1428.i(anInt1429);
		while (!aBoolean1431) {
			if (!aBoolean1430)
				break;
			if (aClass299_1424 != null && !aClass299_1424.method3239(-91)) {
				aBoolean1427 = true;
				Class338 class338 = aClass299_1424.method3245((byte) -82);
				if (class338 instanceof Class338_Sub3) {
					Class338_Sub3 class338_sub3 = (Class338_Sub3) class338;
					if (class338_sub3.aBoolean5207)
						class338_sub3.method3460(26, Class16_Sub2.aHa3733);
					else {
						Class338_Sub3_Sub4_Sub1.method3572(class338_sub3, aClass296_Sub35Array1432);
						if (Class166_Sub1.aClass55_4303 != null)
							Class166_Sub1.aClass55_4303.a(-16777216, 1659, -256, class338_sub3.anInt5204, aClass299_1424.aString2701, class338_sub3.anInt5211);
					}
				} else {
					int i = ((Class338_Sub10) class338).anInt5271;
					if (i >= 1 && i <= 4) {
						s var_s = Class360_Sub2.aSArray5304[i - 1];
						for (int i_0_ = 0; i_0_ < (Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684); i_0_++) {
							for (int i_1_ = 0; i_1_ < (Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684); i_1_++) {
								if (Class121.aBooleanArrayArrayArray1268[i - 1][i_0_][i_1_]) {
									int i_2_ = (Class296_Sub45_Sub2.anInt6288 - Class379_Sub2.anInt5684 + i_0_);
									int i_3_ = (Class296_Sub39_Sub20_Sub2.anInt6728 - Class379_Sub2.anInt5684 + i_1_);
									if (i_2_ >= 0 && i_2_ < var_s.anInt2832 && i_3_ >= 0 && i_3_ < var_s.anInt2834) {
										Class16_Sub2.aHa3733.H(i_2_ << Class313.anInt2779, var_s.method3355(i_3_, (byte) -106, i_2_), i_3_ << Class313.anInt2779, anIntArray1425);
										if (ModeWhere.method2191(anIntArray1425[0]) == anInt1429 - 1)
											var_s.method3352(i_2_, i_3_);
									}
								}
							}
						}
					}
				}
			} else {
				aBoolean1427 = false;
				aLong1426 = BITConfigsLoader.aClass327_2205.method3390(-92);
				synchronized (this) {
					try {
						this.wait();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
			}
		}
		aHa1428.c(anInt1429);
		while (aBoolean1431) {
			if (!aBoolean1430)
				break;
			synchronized (this) {
				try {
					this.wait();
				} catch (InterruptedException interruptedexception) {
					/* empty */
				}
			}
		}
	}

	final void method1474() {
		aBoolean1431 = false;
		aBoolean1430 = false;
		synchronized (this) {
			this.notify();
		}
	}

	final void method1475() {
		aBoolean1431 = true;
		synchronized (this) {
			this.notify();
		}
	}

	final void method1476() {
		aBoolean1431 = false;
		synchronized (this) {
			this.notify();
		}
	}

	final boolean method1477() {
		if (aClass299_1424 != null && (aBoolean1427 || !aClass299_1424.method3239(-88)))
			return false;
		return true;
	}

	public final void run() {
		while (aBoolean1430)
			method1473();
	}

	final long method1478() {
		return aLong1426;
	}

	Class142(int i, ha var_ha) {
		aBoolean1430 = true;
		aBoolean1427 = false;
		aClass296_Sub35Array1432 = new Class296_Sub35[8];
		anInt1429 = i;
		aHa1428 = var_ha;
	}
}
