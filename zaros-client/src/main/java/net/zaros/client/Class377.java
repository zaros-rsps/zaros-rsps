package net.zaros.client;

/* Class377 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class377 {
	static AnimationsLoader aClass310_3188;
	static boolean aBoolean3189 = true;
	static int anInt3190;
	static Connection loginConnection;
	static int[] anIntArray3192;

	public static void method3959(boolean bool) {
		aClass310_3188 = null;
		if (bool)
			anIntArray3192 = null;
		loginConnection = null;
		anIntArray3192 = null;
	}

	static final void method3960(InterfaceComponent class51, int i) {
		if (Class154_Sub1.anInt4293 == class51.contentType) {
			if ((Class296_Sub51_Sub11.localPlayer.displayName) == null) {
				class51.modelID = 0;
				class51.anInt545 = 0;
				return;
			}
			class51.anInt612 = 150;
			class51.anInt515 = ((int) (Math.sin((double) Class29.anInt307 / 40.0) * 256.0) & 0x7ff);
			class51.modelID = Class362.myPlayerIndex;
			class51.modelType = 5;
			class51.anInt545 = Class226.method2086((Class296_Sub51_Sub11.localPlayer.displayName), i - 1662193786);
			Animator class44 = (Class296_Sub51_Sub11.localPlayer.aClass44_6777);
			if (class44 == null)
				class51.aClass44_554 = null;
			else {
				if (class51.aClass44_554 == null)
					class51.aClass44_554 = new Class44_Sub2();
				class51.anInt497 = class44.method557((byte) -88);
				class51.aClass44_554.method550(-1, class44);
			}
		}
		if (i != -25153)
			anInt3190 = -114;
	}
}
