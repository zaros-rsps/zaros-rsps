package net.zaros.client;

/* Class228 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class228 {
	String aString2188;
	int anInt2189;
	int anInt2190;
	static int anInt2191;
	int anInt2192;
	String aString2193;
	String aString2194;
	int anInt2195;
	String aString2196;
	static IncomingPacket aClass231_2197 = new IncomingPacket(20, 6);
	String aString2198;
	int anInt2199 = Class183.method1853(0);
	static String aString2200 = null;
	static int anInt2201;

	final void method2094(int i, String string, String string_0_, int i_1_, byte i_2_, String string_3_, String string_4_, int i_5_, String string_6_) {
		anInt2199 = Class183.method1853(0);
		anInt2189 = i_5_;
		aString2194 = string_6_;
		aString2198 = string_0_;
		anInt2195 = Class29.anInt307;
		aString2188 = string_4_;
		anInt2192 = i_1_;
		if (i_2_ >= -124) {
			aClass231_2197 = null;
		}
		aString2196 = string;
		aString2193 = string_3_;
		anInt2190 = i;
	}

	static final void method2095(Packet class296_sub17, byte i) {
		if (i == -123) {
			for (int i_7_ = 0; Class296_Sub15_Sub1.anInt5998 > i_7_; i_7_++) {
				int i_8_ = class296_sub17.readSmart();
				int i_9_ = class296_sub17.g2();
				if (i_9_ == 65535) {
					i_9_ = -1;
				}
				if (SubCache.aClass210_Sub1Array2713[i_8_] != null) {
					SubCache.aClass210_Sub1Array2713[i_8_].anInt2096 = i_9_;
				}
			}
			anInt2191++;
		}
	}

	public static void method2096(int i) {
		aClass231_2197 = null;
		if (i != -65536) {
			method2096(-53);
		}
		aString2200 = null;
	}

	Class228(int i, int i_10_, String string, String string_11_, String string_12_, String string_13_, int i_14_, String string_15_) {
		aString2194 = string;
		anInt2192 = i;
		aString2198 = string_13_;
		aString2188 = string_15_;
		aString2193 = string_12_;
		anInt2189 = i_14_;
		anInt2190 = i_10_;
		aString2196 = string_11_;
		anInt2195 = Class29.anInt307;
	}
}
