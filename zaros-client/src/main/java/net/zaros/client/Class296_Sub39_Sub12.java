package net.zaros.client;

/* Class296_Sub39_Sub12 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.File;

final class Class296_Sub39_Sub12 extends Queuable {
	String aString6193;
	char[] aCharArray6194;
	static int anInt6195;
	int[] anIntArray6196;
	static int anInt6197;
	static OutgoingPacket aClass311_6198;
	static int loginState = 0;
	static int anInt6200 = -2;
	int[] anIntArray6201;
	char[] aCharArray6202;

	final void method2843(int i) {
		if (anIntArray6196 != null) {
			for (int i_0_ = 0; i_0_ < anIntArray6196.length; i_0_++)
				anIntArray6196[i_0_] = Class48.bitOR(anIntArray6196[i_0_], 32768);
		}
		if (anIntArray6201 != null) {
			for (int i_1_ = 0; i_1_ < anIntArray6201.length; i_1_++)
				anIntArray6201[i_1_] = Class48.bitOR(anIntArray6201[i_1_], 32768);
		}
		int i_2_ = -6 % ((66 - i) / 44);
	}

	static final void method2844(int i) {
		Class349.anInt3036 = 0;
		Class57.anInt668 = Js5TextureLoader.anInt3440;
		Class29.aClass324Array305 = new Class324[500];
		Class360_Sub9.anInt5343 = i;
		Class190.aBoolean1943 = false;
		za.aClass324Array5088 = new Class324[500];
		Class77.anInt880 = 0;
		Class343_Sub1.aClass324Array5273 = new Class324[1000];
		Class116.aClass324Array3678 = new Class324[2000];
		Class22.anInt249 = 0;
		Class296_Sub36.anIntArrayArrayArray4870 = (new int[Class368_Sub9.anInt5477][Class228.anInt2201
				+ 1][Class368_Sub12.anInt5488 + 1]);
		EffectiveVertex.anInt2213 = Js5TextureLoader.anInt3440;
		try {
			if (!(Class.forName("oa").isInstance(Class16_Sub2.aHa3733)))
				Class103.aBoolean1086 = true;
			else
				Class103.aBoolean1086 = false;
		} catch (ClassNotFoundException e) {
			Class103.aBoolean1086 = false;
		}
	}

	final void method2845(int i, Packet class296_sub17) {
		for (;;) {
			int i_3_ = class296_sub17.g1();
			if (i_3_ == 0)
				break;
			method2849(i_3_, 89, class296_sub17);
		}
		if (i != 1)
			method2846(-90, '\uffda');
	}

	final int method2846(int i, char c) {
		try {
			if (anIntArray6196 == null)
				return -1;
			for (int i_4_ = 0; i_4_ < anIntArray6196.length; i_4_++) {
				if (aCharArray6202[i_4_] == c)
					return anIntArray6196[i_4_];
			}
			if (i != 15118)
				method2848(-68, 61, -98, 30, 8, 62, -105, 28, -56, -57);
			return -1;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "qj.K(" + i + ',' + c + ')');
		}
	}

	static final void method2847(byte[][] is, Class181_Sub1 class181_sub1, int i) {
		for (int i_5_ = 0; i_5_ < class181_sub1.anInt1870; i_5_++) {
			Class108.method948(-2060);
			for (int i_6_ = 0; Class198.currentMapSizeX >> 3 > i_6_; i_6_++) {
				for (int i_7_ = 0; i_7_ < Class296_Sub38.currentMapSizeY >> 3; i_7_++) {
					int i_8_ = Class181.buildedMapParts[i_5_][i_6_][i_7_];
					if (i_8_ != -1) {
						int i_9_ = i_8_ >> 24 & 0x3;
						if (!class181_sub1.aBoolean1860 || i_9_ == 0) {
							int i_10_ = i_8_ >> 1 & 0x3;
							int i_11_ = (i_8_ & 0xffdc9e) >> 14;
							int i_12_ = i_8_ >> 3 & 0x7ff;
							int i_13_ = i_12_ / 8 + (i_11_ / 8 << 8);
							for (int i_14_ = 0; i_14_ < Class296_Sub43.anIntArray4940.length; i_14_++) {
								if ((Class296_Sub43.anIntArray4940[i_14_] == i_13_) && is[i_14_] != null) {
									class181_sub1.method1833(i_7_ * 8, i_6_ * 8, is[i_14_], Class41_Sub13.aHa3774, i_9_,
											(i_12_ & 0x7) * 8, (byte) -9, i_10_, i_5_, (i_11_ & 0x7) * 8,
											BITConfigDefinition.mapClips);
									break;
								}
							}
						}
					}
				}
			}
		}
		if (i != -24660)
			method2852(16, null, null);
	}

	static final boolean method2848(int i, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_,
			int i_22_, int i_23_) {
		if (i_17_ > 2000 || i_19_ > 2000 || i_22_ > 2000 || i_18_ > 2000 || i_15_ > 2000 || i > 2000)
			return false;
		if (i_17_ < -2000 || i_19_ < -2000 || i_22_ < -2000 || i_18_ < -2000 || i_15_ < -2000 || i < -2000)
			return false;
		if (Class296_Sub51_Sub6.anInt6368 == 2) {
			int i_24_ = Class213.anInt2110 * i_17_ + i_18_;
			if (i_24_ >= 0 && i_24_ < Class365.anIntArray3115.length
					&& Class365.anIntArray3115[i_24_] > (i_20_ << 8) - 38400)
				return false;
			i_24_ = Class213.anInt2110 * i_19_ + i_15_;
			if (i_24_ >= 0 && i_24_ < Class365.anIntArray3115.length
					&& Class365.anIntArray3115[i_24_] > (i_21_ << 8) - 38400)
				return false;
			i_24_ = i_22_ * Class213.anInt2110 + i;
			if (i_24_ >= 0 && i_24_ < Class365.anIntArray3115.length
					&& (i_23_ << 8) - 38400 < Class365.anIntArray3115[i_24_])
				return false;
		}
		int i_25_ = -i_18_ + i_15_;
		int i_26_ = -i_17_ + i_19_;
		int i_27_ = -i_18_ + i;
		int i_28_ = i_22_ - i_17_;
		int i_29_ = i_21_ - i_20_;
		if (i_16_ != 30946)
			anInt6200 = -20;
		if (i_17_ < i_19_ && i_22_ > i_22_) {
			i_17_--;
			if (i_22_ < i_19_)
				i_19_++;
			else
				i_22_++;
		} else if (i_22_ > i_19_) {
			if (i_17_ <= i_22_)
				i_22_++;
			else
				i_17_++;
			i_19_--;
		} else {
			i_22_--;
			if (i_17_ > i_19_)
				i_17_++;
			else
				i_19_++;
		}
		int i_30_ = i_23_ - i_20_;
		int i_31_ = 0;
		if (i_19_ != i_17_)
			i_31_ = (-i_18_ + i_15_ << 12) / (-i_17_ + i_19_);
		int i_32_ = 0;
		if (i_22_ != i_19_)
			i_32_ = (-i_15_ + i << 12) / (i_22_ - i_19_);
		int i_33_ = 0;
		if (i_17_ != i_22_)
			i_33_ = (i_18_ - i << 12) / (i_17_ - i_22_);
		int i_34_ = -(i_27_ * i_26_) + i_28_ * i_25_;
		if (i_34_ == 0)
			return false;
		int i_35_ = (i_28_ * i_29_ - i_26_ * i_30_ << 8) / i_34_;
		int i_36_ = (i_25_ * i_30_ - i_29_ * i_27_ << 8) / i_34_;
		if (i_17_ <= i_19_ && i_22_ >= i_17_) {
			if (Class123_Sub1_Sub1.anInt5813 <= i_17_)
				return true;
			if (i_22_ > Class123_Sub1_Sub1.anInt5813)
				i_22_ = Class123_Sub1_Sub1.anInt5813;
			i_20_ = (i_20_ << 8) - i_35_ * i_18_ + i_35_;
			if (Class123_Sub1_Sub1.anInt5813 < i_19_)
				i_19_ = Class123_Sub1_Sub1.anInt5813;
			if (i_19_ < i_22_) {
				i = i_18_ <<= 12;
				i_15_ <<= 12;
				if (i_17_ < 0) {
					i_18_ -= i_31_ * i_17_;
					i -= i_33_ * i_17_;
					i_20_ -= i_17_ * i_36_;
					i_17_ = 0;
				}
				if (i_19_ < 0) {
					i_15_ -= i_32_ * i_19_;
					i_19_ = 0;
				}
				if ((i_17_ == i_19_ || i_33_ >= i_31_) && (i_19_ != i_17_ || i_32_ >= i_33_)) {
					i_22_ -= i_19_;
					i_19_ -= i_17_;
					i_17_ = Class213.anInt2110 * i_17_;
					while (--i_19_ >= 0) {
						if (!Class296_Sub51_Sub26.method3149(i_35_, i_20_, -111, (i >> 12) + 1, (i_18_ >> 12) - 1,
								i_17_, (Class365.anIntArray3115), 0))
							return false;
						i += i_33_;
						i_18_ += i_31_;
						i_17_ += Class213.anInt2110;
						i_20_ += i_36_;
					}
					while (--i_22_ >= 0) {
						if (!Class296_Sub51_Sub26.method3149(i_35_, i_20_, -63, (i >> 12) + 1, (i_15_ >> 12) - 1, i_17_,
								(Class365.anIntArray3115), 0))
							return false;
						i_15_ += i_32_;
						i_17_ += Class213.anInt2110;
						i_20_ += i_36_;
						i += i_33_;
					}
					return true;
				}
				i_22_ -= i_19_;
				i_19_ -= i_17_;
				i_17_ *= Class213.anInt2110;
				while (--i_19_ >= 0) {
					if (!Class296_Sub51_Sub26.method3149(i_35_, i_20_, -76, (i_18_ >> 12) + 1, (i >> 12) - 1, i_17_,
							(Class365.anIntArray3115), 0))
						return false;
					i_18_ += i_31_;
					i_17_ += Class213.anInt2110;
					i_20_ += i_36_;
					i += i_33_;
				}
				while (--i_22_ >= 0) {
					if (!Class296_Sub51_Sub26.method3149(i_35_, i_20_, 88, (i_15_ >> 12) + 1, (i >> 12) - 1, i_17_,
							(Class365.anIntArray3115), 0))
						return false;
					i_15_ += i_32_;
					i_20_ += i_36_;
					i_17_ += Class213.anInt2110;
					i += i_33_;
				}
				return true;
			}
			i_15_ = i_18_ <<= 12;
			if (i_17_ < 0) {
				i_18_ -= i_31_ * i_17_;
				i_20_ -= i_17_ * i_36_;
				i_15_ -= i_33_ * i_17_;
				i_17_ = 0;
			}
			i <<= 12;
			if (i_22_ < 0) {
				i -= i_22_ * i_32_;
				i_22_ = 0;
			}
			if (i_17_ != i_22_ && i_33_ < i_31_ || i_22_ == i_17_ && i_32_ > i_31_) {
				i_19_ -= i_22_;
				i_22_ -= i_17_;
				i_17_ *= Class213.anInt2110;
				while (--i_22_ >= 0) {
					if (!Class296_Sub51_Sub26.method3149(i_35_, i_20_, -41, (i_18_ >> 12) + 1, (i_15_ >> 12) - 1, i_17_,
							(Class365.anIntArray3115), 0))
						return false;
					i_17_ += Class213.anInt2110;
					i_20_ += i_36_;
					i_18_ += i_31_;
					i_15_ += i_33_;
				}
				while (--i_19_ >= 0) {
					if (!Class296_Sub51_Sub26.method3149(i_35_, i_20_, 77, (i_18_ >> 12) + 1, (i >> 12) - 1, i_17_,
							(Class365.anIntArray3115), 0))
						return false;
					i_18_ += i_31_;
					i_17_ += Class213.anInt2110;
					i_20_ += i_36_;
					i += i_32_;
				}
				return true;
			}
			i_19_ -= i_22_;
			i_22_ -= i_17_;
			i_17_ = Class213.anInt2110 * i_17_;
			while (--i_22_ >= 0) {
				if (!Class296_Sub51_Sub26.method3149(i_35_, i_20_, i_16_ - 30872, (i_15_ >> 12) + 1, (i_18_ >> 12) - 1,
						i_17_, Class365.anIntArray3115, 0))
					return false;
				i_18_ += i_31_;
				i_15_ += i_33_;
				i_17_ += Class213.anInt2110;
				i_20_ += i_36_;
			}
			while (--i_19_ >= 0) {
				if (!Class296_Sub51_Sub26.method3149(i_35_, i_20_, i_16_ - 31071, (i >> 12) + 1, (i_18_ >> 12) - 1,
						i_17_, Class365.anIntArray3115, 0))
					return false;
				i += i_32_;
				i_17_ += Class213.anInt2110;
				i_18_ += i_31_;
				i_20_ += i_36_;
			}
			return true;
		}
		if (i_22_ >= i_19_) {
			if (Class123_Sub1_Sub1.anInt5813 <= i_19_)
				return true;
			if (Class123_Sub1_Sub1.anInt5813 < i_17_)
				i_17_ = Class123_Sub1_Sub1.anInt5813;
			if (Class123_Sub1_Sub1.anInt5813 < i_22_)
				i_22_ = Class123_Sub1_Sub1.anInt5813;
			i_21_ = i_35_ + (-(i_35_ * i_15_) + (i_21_ << 8));
			if (i_22_ < i_17_) {
				i_18_ = i_15_ <<= 12;
				i <<= 12;
				if (i_19_ < 0) {
					i_15_ -= i_32_ * i_19_;
					i_21_ -= i_19_ * i_36_;
					i_18_ -= i_19_ * i_31_;
					i_19_ = 0;
				}
				if (i_22_ < 0) {
					i -= i_33_ * i_22_;
					i_22_ = 0;
				}
				if (i_19_ != i_22_ && i_31_ < i_32_ || i_22_ == i_19_ && i_33_ < i_31_) {
					i_17_ -= i_22_;
					i_22_ -= i_19_;
					i_19_ = Class213.anInt2110 * i_19_;
					while (--i_22_ >= 0) {
						if (!Class296_Sub51_Sub26.method3149(i_35_, i_21_, 107, (i_15_ >> 12) + 1, (i_18_ >> 12) - 1,
								i_19_, (Class365.anIntArray3115), 0))
							return false;
						i_18_ += i_31_;
						i_15_ += i_32_;
						i_19_ += Class213.anInt2110;
						i_21_ += i_36_;
					}
					while (--i_17_ >= 0) {
						if (!Class296_Sub51_Sub26.method3149(i_35_, i_21_, -93, (i >> 12) + 1, (i_18_ >> 12) - 1, i_19_,
								(Class365.anIntArray3115), 0))
							return false;
						i_21_ += i_36_;
						i_19_ += Class213.anInt2110;
						i_18_ += i_31_;
						i += i_33_;
					}
					return true;
				}
				i_17_ -= i_22_;
				i_22_ -= i_19_;
				i_19_ = Class213.anInt2110 * i_19_;
				while (--i_22_ >= 0) {
					if (!Class296_Sub51_Sub26.method3149(i_35_, i_21_, -113, (i_18_ >> 12) + 1, (i_15_ >> 12) - 1,
							i_19_, (Class365.anIntArray3115), 0))
						return false;
					i_21_ += i_36_;
					i_18_ += i_31_;
					i_15_ += i_32_;
					i_19_ += Class213.anInt2110;
				}
				while (--i_17_ >= 0) {
					if (!Class296_Sub51_Sub26.method3149(i_35_, i_21_, 90, (i_18_ >> 12) + 1, (i >> 12) - 1, i_19_,
							(Class365.anIntArray3115), 0))
						return false;
					i += i_33_;
					i_18_ += i_31_;
					i_21_ += i_36_;
					i_19_ += Class213.anInt2110;
				}
				return true;
			}
			i = i_15_ <<= 12;
			i_18_ <<= 12;
			if (i_19_ < 0) {
				i -= i_19_ * i_31_;
				i_15_ -= i_19_ * i_32_;
				i_21_ -= i_36_ * i_19_;
				i_19_ = 0;
			}
			if (i_17_ < 0) {
				i_18_ -= i_33_ * i_17_;
				i_17_ = 0;
			}
			if (i_32_ <= i_31_) {
				i_22_ -= i_17_;
				i_17_ -= i_19_;
				i_19_ = Class213.anInt2110 * i_19_;
				while (--i_17_ >= 0) {
					if (!Class296_Sub51_Sub26.method3149(i_35_, i_21_, 127, (i >> 12) + 1, (i_15_ >> 12) - 1, i_19_,
							(Class365.anIntArray3115), 0))
						return false;
					i += i_31_;
					i_19_ += Class213.anInt2110;
					i_21_ += i_36_;
					i_15_ += i_32_;
				}
				while (--i_22_ >= 0) {
					if (!Class296_Sub51_Sub26.method3149(i_35_, i_21_, 83, (i_18_ >> 12) + 1, (i_15_ >> 12) - 1, i_19_,
							(Class365.anIntArray3115), 0))
						return false;
					i_18_ += i_33_;
					i_15_ += i_32_;
					i_21_ += i_36_;
					i_19_ += Class213.anInt2110;
				}
				return true;
			}
			i_22_ -= i_17_;
			i_17_ -= i_19_;
			i_19_ = Class213.anInt2110 * i_19_;
			while (--i_17_ >= 0) {
				if (!Class296_Sub51_Sub26.method3149(i_35_, i_21_, -79, (i_15_ >> 12) + 1, (i >> 12) - 1, i_19_,
						Class365.anIntArray3115, 0))
					return false;
				i_21_ += i_36_;
				i_15_ += i_32_;
				i += i_31_;
				i_19_ += Class213.anInt2110;
			}
			while (--i_22_ >= 0) {
				if (!Class296_Sub51_Sub26.method3149(i_35_, i_21_, -126, (i_15_ >> 12) + 1, (i_18_ >> 12) - 1, i_19_,
						Class365.anIntArray3115, 0))
					return false;
				i_15_ += i_32_;
				i_18_ += i_33_;
				i_21_ += i_36_;
				i_19_ += Class213.anInt2110;
			}
			return true;
		}
		if (i_22_ >= Class123_Sub1_Sub1.anInt5813)
			return true;
		if (Class123_Sub1_Sub1.anInt5813 < i_19_)
			i_19_ = Class123_Sub1_Sub1.anInt5813;
		i_23_ = (i_23_ << 8) - (i_35_ * i - i_35_);
		if (i_17_ > Class123_Sub1_Sub1.anInt5813)
			i_17_ = Class123_Sub1_Sub1.anInt5813;
		if (i_17_ >= i_19_) {
			i_18_ = i <<= 12;
			if (i_22_ < 0) {
				i -= i_22_ * i_33_;
				i_23_ -= i_36_ * i_22_;
				i_18_ -= i_32_ * i_22_;
				i_22_ = 0;
			}
			i_15_ <<= 12;
			if (i_19_ < 0) {
				i_15_ -= i_19_ * i_31_;
				i_19_ = 0;
			}
			if (i_33_ > i_32_) {
				i_17_ -= i_19_;
				i_19_ -= i_22_;
				i_22_ = Class213.anInt2110 * i_22_;
				while (--i_19_ >= 0) {
					if (!Class296_Sub51_Sub26.method3149(i_35_, i_23_, 104, (i >> 12) + 1, (i_18_ >> 12) - 1, i_22_,
							(Class365.anIntArray3115), 0))
						return false;
					i_22_ += Class213.anInt2110;
					i_18_ += i_32_;
					i_23_ += i_36_;
					i += i_33_;
				}
				while (--i_17_ >= 0) {
					if (!Class296_Sub51_Sub26.method3149(i_35_, i_23_, i_16_ ^ ~0x78cf, (i >> 12) + 1,
							(i_15_ >> 12) - 1, i_22_, (Class365.anIntArray3115), 0))
						return false;
					i_22_ += Class213.anInt2110;
					i_23_ += i_36_;
					i_15_ += i_31_;
					i += i_33_;
				}
				return true;
			}
			i_17_ -= i_19_;
			i_19_ -= i_22_;
			i_22_ = Class213.anInt2110 * i_22_;
			while (--i_19_ >= 0) {
				if (!Class296_Sub51_Sub26.method3149(i_35_, i_23_, 103, (i_18_ >> 12) + 1, (i >> 12) - 1, i_22_,
						Class365.anIntArray3115, 0))
					return false;
				i_22_ += Class213.anInt2110;
				i += i_33_;
				i_23_ += i_36_;
				i_18_ += i_32_;
			}
			while (--i_17_ >= 0) {
				if (!Class296_Sub51_Sub26.method3149(i_35_, i_23_, 127, (i_15_ >> 12) + 1, (i >> 12) - 1, i_22_,
						Class365.anIntArray3115, 0))
					return false;
				i_23_ += i_36_;
				i_15_ += i_31_;
				i_22_ += Class213.anInt2110;
				i += i_33_;
			}
			return true;
		}
		i_15_ = i <<= 12;
		if (i_22_ < 0) {
			i -= i_22_ * i_33_;
			i_15_ -= i_22_ * i_32_;
			i_23_ -= i_36_ * i_22_;
			i_22_ = 0;
		}
		i_18_ <<= 12;
		if (i_17_ < 0) {
			i_18_ -= i_31_ * i_17_;
			i_17_ = 0;
		}
		if (i_33_ <= i_32_) {
			i_19_ -= i_17_;
			i_17_ -= i_22_;
			i_22_ = Class213.anInt2110 * i_22_;
			while (--i_17_ >= 0) {
				if (!Class296_Sub51_Sub26.method3149(i_35_, i_23_, i_16_ - 30859, (i_15_ >> 12) + 1, (i >> 12) - 1,
						i_22_, Class365.anIntArray3115, 0))
					return false;
				i_23_ += i_36_;
				i_22_ += Class213.anInt2110;
				i += i_33_;
				i_15_ += i_32_;
			}
			while (--i_19_ >= 0) {
				if (!Class296_Sub51_Sub26.method3149(i_35_, i_23_, -62, (i_15_ >> 12) + 1, (i_18_ >> 12) - 1, i_22_,
						Class365.anIntArray3115, 0))
					return false;
				i_23_ += i_36_;
				i_22_ += Class213.anInt2110;
				i_18_ += i_31_;
				i_15_ += i_32_;
			}
			return true;
		}
		i_19_ -= i_17_;
		i_17_ -= i_22_;
		i_22_ *= Class213.anInt2110;
		while (--i_17_ >= 0) {
			if (!Class296_Sub51_Sub26.method3149(i_35_, i_23_, 110, (i >> 12) + 1, (i_15_ >> 12) - 1, i_22_,
					Class365.anIntArray3115, 0))
				return false;
			i += i_33_;
			i_22_ += Class213.anInt2110;
			i_23_ += i_36_;
			i_15_ += i_32_;
		}
		while (--i_19_ >= 0) {
			if (!Class296_Sub51_Sub26.method3149(i_35_, i_23_, -41, (i_18_ >> 12) + 1, (i_15_ >> 12) - 1, i_22_,
					Class365.anIntArray3115, 0))
				return false;
			i_22_ += Class213.anInt2110;
			i_15_ += i_32_;
			i_23_ += i_36_;
			i_18_ += i_31_;
		}
		return true;
	}

	private final void method2849(int i, int i_37_, Packet class296_sub17) {
		if (i != 1) {
			if (i == 2) {
				int i_38_ = class296_sub17.g1();
				aCharArray6194 = new char[i_38_];
				anIntArray6201 = new int[i_38_];
				for (int i_39_ = 0; i_38_ > i_39_; i_39_++) {
					anIntArray6201[i_39_] = class296_sub17.g2();
					byte i_40_ = class296_sub17.g1b();
					aCharArray6194[i_39_] = i_40_ == 0 ? '\0' : Mesh.method1387((byte) -81, i_40_);
				}
			} else if (i == 3) {
				int i_41_ = class296_sub17.g1();
				anIntArray6196 = new int[i_41_];
				aCharArray6202 = new char[i_41_];
				for (int i_42_ = 0; i_42_ < i_41_; i_42_++) {
					anIntArray6196[i_42_] = class296_sub17.g2();
					byte i_43_ = class296_sub17.g1b();
					aCharArray6202[i_42_] = i_43_ == 0 ? '\0' : Mesh.method1387((byte) -90, i_43_);
				}
			}
		} else
			aString6193 = class296_sub17.gstr();
		if (i_37_ <= 2) {
			/* empty */
		}
	}

	public static void method2850(int i) {
		aClass311_6198 = null;
		if (i != -20134)
			method2847(null, null, 38);
	}

	final int method2851(byte i, char c) {
		try {
			int i_44_ = 77 / ((2 - i) / 43);
			if (anIntArray6201 == null)
				return -1;
			for (int i_45_ = 0; i_45_ < anIntArray6201.length; i_45_++) {
				if (c == aCharArray6194[i_45_])
					return anIntArray6201[i_45_];
			}
			return -1;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "qj.M(" + i + ',' + c + ')');
		}
	}

	public Class296_Sub39_Sub12() {
		/* empty */
	}

	static final void method2852(int i, File file, String string) {
		Class154.aHashtable1581.put(string, file);
		int i_46_ = 108 / ((i + 34) / 62);
	}

	static {
		aClass311_6198 = new OutgoingPacket(9, -1);
	}
}
