package net.zaros.client;

/* Class296_Sub20 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class296_Sub20 extends Node {
	short aShort4715;
	static IncomingPacket aClass231_4716 = new IncomingPacket(25, 17);
	static IncomingPacket aClass231_4717;
	static int[] anIntArray4718 = {36064, 36065, 36066, 36067, 36068, 36069, 36070, 36071, 36096};
	static Class296_Sub45_Sub4 aClass296_Sub45_Sub4_4719;

	static final void method2653(String string, byte i) {
		if (i == -42)
			System.out.println("Error: " + Class76.method784(string, "\n", "%0a", (byte) 104));
	}

	public static void method2654(int i) {
		anIntArray4718 = null;
		if (i != -24695)
			aClass296_Sub45_Sub4_4719 = null;
		aClass231_4716 = null;
		aClass231_4717 = null;
		aClass296_Sub45_Sub4_4719 = null;
	}

	static final void method2655(int i) {
		if (Class355_Sub1.method3701(true, Class366_Sub6.anInt5392) || BITConfigDefinition.method2353(-8, Class366_Sub6.anInt5392))
			StaticMethods.method310(Class219.camPosX >> 12, Class124.camPosZ >> 12, -119, Class296_Sub40.anInt4913);
		else {
			int i_0_ = ((Class296_Sub51_Sub11.localPlayer.waypointQueueX[0]) >> 3);
			int i_1_ = ((Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0]) >> 3);
			if (i_0_ >= 0 && Class198.currentMapSizeX >> 3 > i_0_ && i_1_ >= 0 && i_1_ < Class296_Sub38.currentMapSizeY >> 3)
				StaticMethods.method310(i_0_, i_1_, 99, Class296_Sub40.anInt4913);
			else
				StaticMethods.method310(Class198.currentMapSizeX >> 4, Class296_Sub38.currentMapSizeY >> 4, -57, 0);
		}
		int i_2_ = 83 % ((-49 - i) / 62);
		MaterialRaw.method1671((byte) -53);
		Class384.method4018((byte) -89);
		Class226.method2090(13044);
		Class103.method893(-2);
	}

	static final void method2656(boolean bool, String string) {
		System.exit(1);
		if (bool != true)
			method2655(35);
	}

	public Class296_Sub20() {
		/* empty */
	}

	Class296_Sub20(short i) {
		aShort4715 = i;
	}

	static {
		aClass231_4717 = new IncomingPacket(11, -2);
	}
}
