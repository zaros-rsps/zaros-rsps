package net.zaros.client;

/* Class156 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class156 implements Interface10 {
	int anInt3591;
	static IncomingPacket aClass231_3592 = new IncomingPacket(14, 6);
	static int anInt3593 = 0;
	static int anInt3594 = 0;
	static int anInt3595 = 0;

	static final void method1582(int i, boolean bool) {
		if (Class338_Sub3_Sub1_Sub1.method3481(116)) {
			if (Class296_Sub51_Sub14.anInt6423 != i)
				Class384.aString3252 = "";
			Class296_Sub51_Sub14.anInt6423 = i;
			if (bool)
				method1582(91, true);
			Class296_Sub45_Sub2.aClass204_6276.method1966(320);
			Class41_Sub8.method422(1, 5);
		}
	}

	public static void method1583(byte i) {
		if (i < 58)
			anInt3595 = -2;
		aClass231_3592 = null;
	}

	public final Class294 method45(byte i) {
		int i_0_ = -20 / ((i + 6) / 44);
		return Class296_Sub34_Sub2.aClass294_6098;
	}

	Class156(int i) {
		anInt3591 = i;
	}

	static final boolean method1584(byte i, ha var_ha, int i_1_) {
		int i_2_ = (Class198.currentMapSizeX - 104) / 2;
		int i_3_ = (Class296_Sub38.currentMapSizeY - 104) / 2;
		boolean bool = true;
		for (int i_4_ = i_2_; i_2_ + 104 > i_4_; i_4_++) {
			for (int i_5_ = i_3_; i_5_ < i_3_ + 104; i_5_++) {
				for (int i_6_ = i_1_; i_6_ <= 3; i_6_++) {
					if (Class296_Sub40.method2912(23266, i_5_, i_1_, i_4_, i_6_)) {
						int i_7_ = i_6_;
						if (r_Sub2.method2871(i_5_, i_4_, (byte) -47))
							i_7_--;
						if (i_7_ >= 0)
							bool &= Model.method1716(i_7_, true, i_4_, i_5_);
					}
				}
			}
		}
		if (i != 22)
			anInt3594 = -91;
		if (!bool)
			return false;
		int[] is = new int[262144];
		for (int i_8_ = 0; is.length > i_8_; i_8_++)
			is[i_8_] = -16777216;
		Class69_Sub3.aClass397_5722 = var_ha.method1096(120, 0, 512, 512, 512, is);
		Class130.method1377(23783);
		int i_9_ = (((238 + (int) (Math.random() * 20.0) - 10 << 16) + ((238 - (-(int) (Math.random() * 20.0) + 10) << 8) - 10) + ((int) (Math.random() * 20.0) + 238)) | ~0xffffff);
		int i_10_ = (int) (Math.random() * 20.0) + 238 - 10 << 16 | ~0xffffff;
		int i_11_ = ((int) (Math.random() * 8.0) | ((int) (Math.random() * 8.0) << 8 | (int) (Math.random() * 8.0) << 16));
		boolean[][] bools = new boolean[Graphic.anInt261 + 1 + 2][Graphic.anInt261 + 2 + 1];
		for (int i_12_ = i_2_; i_2_ + 104 > i_12_; i_12_ += Graphic.anInt261) {
			for (int i_13_ = i_3_; i_13_ < i_3_ + 104; i_13_ += Graphic.anInt261) {
				int i_14_ = 0;
				int i_15_ = 0;
				int i_16_ = i_12_;
				if (i_16_ > 0) {
					i_16_--;
					i_14_ += 4;
				}
				int i_17_ = i_13_;
				if (i_17_ > 0)
					i_17_--;
				int i_18_ = i_12_ + Graphic.anInt261;
				if (i_18_ < 104)
					i_18_++;
				int i_19_ = Graphic.anInt261 + i_13_;
				if (i_19_ < 104) {
					i_19_++;
					i_15_ += 4;
				}
				var_ha.KA(0, 0, Graphic.anInt261 * 4 + i_14_, i_15_ + Graphic.anInt261 * 4);
				var_ha.GA(-16777216);
				for (int i_20_ = i_1_; i_20_ <= 3; i_20_++) {
					for (int i_21_ = 0; i_21_ <= Graphic.anInt261; i_21_++) {
						for (int i_22_ = 0; Graphic.anInt261 >= i_22_; i_22_++)
							bools[i_21_][i_22_] = Class296_Sub40.method2912(23266, i_17_ + i_22_, i_1_, i_16_ + i_21_, i_20_);
					}
					Class244.aSArray2320[i_20_].method3350(0, 0, 1024, i_16_, i_17_, i_18_, i_19_, bools);
					if (!Class296_Sub39_Sub1.aBoolean6124) {
						for (int i_23_ = -4; Graphic.anInt261 > i_23_; i_23_++) {
							for (int i_24_ = -4; Graphic.anInt261 > i_24_; i_24_++) {
								int i_25_ = i_23_ + i_12_;
								int i_26_ = i_24_ + i_13_;
								if (i_25_ >= i_2_ && i_26_ >= i_3_ && Class296_Sub40.method2912(23266, i_26_, i_1_, i_25_, i_20_)) {
									int i_27_ = i_20_;
									if (r_Sub2.method2871(i_26_, i_25_, (byte) -47))
										i_27_--;
									if (i_27_ >= 0)
										Class296_Sub34_Sub2.method2739(i_25_, i_27_, i_26_, i_10_, (i_15_ + (Graphic.anInt261 - i_24_) * 4 - 4), i_23_ * 4 + i_14_, i_9_, (byte) -104, var_ha);
								}
							}
						}
					}
				}
				if (Class296_Sub39_Sub1.aBoolean6124) {
					ClipData class17 = BITConfigDefinition.mapClips[i_1_];
					for (int i_28_ = 0; Graphic.anInt261 > i_28_; i_28_++) {
						for (int i_29_ = 0; Graphic.anInt261 > i_29_; i_29_++) {
							int i_30_ = i_28_ + i_12_;
							int i_31_ = i_13_ + i_29_;
							int i_32_ = (class17.clip[-class17.offsetX + i_30_][-class17.offsetY + i_31_]);
							if ((i_32_ & 0x40240000) != 0)
								var_ha.method1088(i_14_ + i_28_ * 4, 4, -1713569622, 1, ((Graphic.anInt261 - i_29_) * 4) + i_15_ - 4, 4);
							else if ((i_32_ & 0x800000) != 0)
								var_ha.method1089(4, -1713569622, ((Graphic.anInt261 - i_29_) * 4) + i_15_ - 4, i_28_ * 4 + i_14_, -102);
							else if ((i_32_ & 0x2000000) == 0) {
								if ((i_32_ & 0x8000000) != 0)
									var_ha.method1089(4, -1713569622, ((Graphic.anInt261 - i_29_) * 4 - 4 + (i_15_ + 3)), i_14_ + i_28_ * 4, i - 150);
								else if ((i_32_ & 0x20000000) != 0)
									var_ha.method1095((i_15_ - 4 + ((-i_29_ + Graphic.anInt261) * 4)), i_14_ + i_28_ * 4, -1713569622, 4, i ^ 0x40);
							} else
								var_ha.method1095((i_15_ - 4 + (-i_29_ + Graphic.anInt261) * 4), i_14_ + i_28_ * 4 + 3, -1713569622, 4, i ^ 0x49);
						}
					}
				}
				var_ha.aa(i_14_, i_15_, Graphic.anInt261 * 4, Graphic.anInt261 * 4, i_11_, 2);
				Class69_Sub3.aClass397_5722.method4090((i_12_ - i_2_) * 4 + 48, -((i_13_ - i_3_) * 4) + 464 - Graphic.anInt261 * 4, Graphic.anInt261 * 4, Graphic.anInt261 * 4, i_14_, i_15_);
			}
		}
		var_ha.la();
		var_ha.GA(-16777215);
		Class366_Sub8.method3794(true);
		Class41_Sub1.anInt3739 = 0;
		Class290.aClass155_2655.method1581(i ^ 0x50016);
		if (!Class296_Sub39_Sub1.aBoolean6124) {
			for (int i_33_ = i_2_; i_2_ + 104 > i_33_; i_33_++) {
				for (int i_34_ = i_3_; i_34_ < i_3_ + 104; i_34_++) {
					for (int i_35_ = i_1_; i_35_ <= i_1_ + 1 && i_35_ <= 3; i_35_++) {
						if (Class296_Sub40.method2912(23266, i_34_, i_1_, i_33_, i_35_)) {
							Interface14 interface14 = ((Interface14) Class296_Sub15_Sub3.method2540(i_35_, i_33_, i_34_));
							if (interface14 == null)
								interface14 = ((Interface14) (Class123_Sub2.method1070(i_35_, i_33_, i_34_, Interface14.class)));
							if (interface14 == null)
								interface14 = ((Interface14) StaticMethods.method2730(i_35_, i_33_, i_34_));
							if (interface14 == null)
								interface14 = (Interface14) Class172.method1679(i_35_, i_33_, i_34_);
							if (interface14 != null) {
								ObjectDefinition class70 = (Class379.objectDefinitionLoader.getObjectDefinition(interface14.method57((byte) 105)));
								if (!class70.membersOnly || Class172.member) {
									int i_36_ = class70.anInt759;
									if (class70.transforms != null) {
										for (int i_37_ = 0; (class70.transforms.length > i_37_); i_37_++) {
											if (class70.transforms[i_37_] != -1) {
												ObjectDefinition class70_38_ = (Class379.objectDefinitionLoader.getObjectDefinition((class70.transforms[i_37_])));
												if (class70_38_.anInt759 >= 0)
													i_36_ = class70_38_.anInt759;
											}
										}
									}
									if (i_36_ >= 0) {
										boolean bool_39_ = false;
										if (i_36_ >= 0) {
											Class18 class18 = (Class31.aClass245_324.method2179(11702, i_36_));
											if (class18 != null && class18.aBoolean221)
												bool_39_ = true;
										}
										int i_40_ = i_33_;
										int i_41_ = i_34_;
										if (bool_39_) {
											int[][] is_42_ = (BITConfigDefinition.mapClips[i_35_].clip);
											int i_43_ = (BITConfigDefinition.mapClips[i_35_].offsetX);
											int i_44_ = (BITConfigDefinition.mapClips[i_35_].offsetY);
											for (int i_45_ = 0; i_45_ < 10; i_45_++) {
												int i_46_ = (int) (Math.random() * 4.0);
												if (i_46_ == 0 && i_40_ > i_2_ && i_33_ - 3 < i_40_ && ((is_42_[i_40_ - 1 - i_43_][-i_44_ + i_41_]) & 0x2c0108) == 0)
													i_40_--;
												if (i_46_ == 1 && i_2_ + 104 - 1 > i_40_ && i_40_ < i_33_ + 3 && ((is_42_[-i_43_ + 1 + i_40_][i_41_ - i_44_]) & 0x2c0180) == 0)
													i_40_++;
												if (i_46_ == 2 && i_41_ > i_3_ && i_34_ - 3 < i_41_ && ((is_42_[-i_43_ + i_40_][i_41_ - 1 - i_44_]) & 0x2c0102) == 0)
													i_41_--;
												if (i_46_ == 3 && i_3_ + 103 > i_41_ && i_41_ < i_34_ + 3 && ((is_42_[-i_43_ + i_40_][-i_44_ + (i_41_ + 1)]) & 0x2c0120) == 0)
													i_41_++;
											}
										}
										StaticMethods.anIntArray6089[Class41_Sub1.anInt3739] = class70.ID;
										Class106_Sub1.anIntArray3902[Class41_Sub1.anInt3739] = i_40_;
										Class366_Sub9.anIntArray5421[Class41_Sub1.anInt3739] = i_41_;
										Class41_Sub1.anInt3739++;
									}
								}
							}
						}
					}
				}
			}
			if (Class296_Sub51_Sub19.aClass259_6439 != null) {
				Class296_Sub39_Sub1.fs2.discardUnpacked_ = 1;
				Class31.aClass245_324.method2183(64, 128, 1024);
				for (int i_47_ = 0; Class296_Sub51_Sub19.aClass259_6439.anInt2417 > i_47_; i_47_++) {
					int i_48_ = (Class296_Sub51_Sub19.aClass259_6439.anIntArray2418[i_47_]);
					if (i_48_ >> 28 == (Class296_Sub51_Sub11.localPlayer.z)) {
						int i_49_ = -Class206.worldBaseX + (i_48_ >> 14 & 0x3fff);
						int i_50_ = (i_48_ & 0x3fff) - Class41_Sub26.worldBaseY;
						if (i_49_ < 0 || i_49_ >= Class198.currentMapSizeX || i_50_ < 0 || i_50_ >= Class296_Sub38.currentMapSizeY) {
							Class18 class18 = (Class31.aClass245_324.method2179(11702, (Class296_Sub51_Sub19.aClass259_6439.anIntArray2420[i_47_])));
							if (class18.anIntArray211 != null && class18.anInt210 + i_49_ >= 0 && (Class198.currentMapSizeX > class18.anInt219 + i_49_) && class18.anInt222 + i_50_ >= 0 && (class18.anInt223 + i_50_ < Class296_Sub38.currentMapSizeY))
								Class290.aClass155_2655.addLast((byte) 104, new IntegerNode(i_47_));
						} else
							Class290.aClass155_2655.addLast((byte) 100, new IntegerNode(i_47_));
					}
				}
				Class31.aClass245_324.method2183(64, 128, 128);
				Class296_Sub39_Sub1.fs2.discardUnpacked_ = 2;
				Class296_Sub39_Sub1.fs2.clearEntryBuffers((byte) -126);
			}
		}
		return true;
	}
}
