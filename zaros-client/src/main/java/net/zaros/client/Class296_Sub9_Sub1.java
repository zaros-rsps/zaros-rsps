package net.zaros.client;
import jaggl.OpenGL;

final class Class296_Sub9_Sub1 extends Class296_Sub9 {
	static Class200[] aClass200Array5978;
	private int anInt5979;
	private Class389 aClass389_5980;
	private Class392 aClass392_5981;
	static int anInt5982 = 0;
	private Class69_Sub1 aClass69_Sub1_5983;
	private Class389 aClass389_5984;
	private Class69_Sub1[] aClass69_Sub1Array5985;
	static Sprite[] aClass397Array5986;
	private int anInt5987;
	private Class389 aClass389_5988;
	private Class392 aClass392_5989;
	private int anInt5990;
	private int anInt5991;
	private Class389 aClass389_5992;
	private Class69_Sub1 aClass69_Sub1_5993;

	static final int method2491(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		i_3_ &= 0x3;
		if ((i_0_ & i_1_) == 1) {
			int i_6_ = i_2_;
			i_2_ = i_4_;
			i_4_ = i_6_;
		}
		if (i_3_ == 0)
			return i_5_;
		if (i_3_ == 1)
			return i;
		if (i_3_ == 2)
			return -i_2_ + 1 + (-i_5_ + 7);
		return -i_4_ + 1 + 7 - i;
	}

	final boolean method2492(int i) {
		if (i != 11684)
			method2482((byte) 109);
		if (!aHa_Sub3_4631.aBoolean4190 || !aHa_Sub3_4631.aBoolean4174 || !aHa_Sub3_4631.aBoolean4189)
			return false;
		return true;
	}

	public static void method2493(int i) {
		aClass200Array5978 = null;
		aClass397Array5986 = null;
		if (i != 0)
			method2493(-127);
	}

	final int method2485(int i) {
		if (i != 256)
			return -67;
		return 1;
	}

	final boolean method2489(byte i) {
		if (i != -61)
			return false;
		if (aHa_Sub3_4631.aBoolean4190 && aHa_Sub3_4631.aBoolean4174 && aHa_Sub3_4631.aBoolean4189) {
			aClass392_5981 = new Class392(aHa_Sub3_4631);
			aClass69_Sub1_5993 = new Class69_Sub1(aHa_Sub3_4631, 3553, 34842, 256, 256);
			aClass69_Sub1_5993.method733(false, false, true);
			aClass69_Sub1_5983 = new Class69_Sub1(aHa_Sub3_4631, 3553, 34842, 256, 256);
			aClass69_Sub1_5983.method733(false, false, true);
			aHa_Sub3_4631.method1351(aClass392_5981, (byte) -27);
			aClass392_5981.method4050(aClass69_Sub1_5993, 0, 0);
			aClass392_5981.method4050(aClass69_Sub1_5983, 1, 0);
			aClass392_5981.method4045(0, 0);
			if (!aClass392_5981.method4053((byte) 118)) {
				aHa_Sub3_4631.method1280(aClass392_5981, -1);
				return false;
			}
			aHa_Sub3_4631.method1280(aClass392_5981, -1);
			aClass389_5984 = (Class296_Sub51_Sub19.method3127(1, (new Class77[]{Class296_Sub51_Sub3.method3084(i ^ ~0x4bfb, aHa_Sub3_4631, "#extension GL_ARB_texture_rectangle : enable\nuniform vec3 params;\nuniform sampler2DRect sceneTex;\nconst vec3 lumCoef = vec3(0.2126, 0.7152, 0.0722);\nvoid main() {\n    vec4 col = texture2DRect(sceneTex, gl_TexCoord[0].xy);\n    gl_FragColor = col*step(params.x, dot(lumCoef, col.rgb));\n}\n", 35632)}), aHa_Sub3_4631));
			aClass389_5988 = (Class296_Sub51_Sub19.method3127(1, (new Class77[]{Class296_Sub51_Sub3.method3084(19399, aHa_Sub3_4631, "uniform vec3 params;\nuniform sampler2D sceneTex;\nconst vec3 lumCoef = vec3(0.2126, 0.7152, 0.0722);\nvoid main() {\n    vec4 col = texture2D(sceneTex, gl_TexCoord[0].xy);\n    gl_FragColor = col*step(params.x, dot(lumCoef, col.rgb));\n}\n", 35632)}), aHa_Sub3_4631));
			aClass389_5980 = (Class296_Sub51_Sub19.method3127(1, (new Class77[]{Class296_Sub51_Sub3.method3084(19399, aHa_Sub3_4631, "#extension GL_ARB_texture_rectangle : enable\nuniform vec3 params;\nuniform vec3 dimScale;\nuniform sampler2D bloomTex;\nuniform sampler2DRect sceneTex;\nconst vec3 lumCoef = vec3(0.2126, 0.7152, 0.0722);\nvoid main() {\n\t vec4 bloomCol = texture2D(bloomTex, gl_TexCoord[1].xy);\n\t vec4 sceneCol = texture2DRect(sceneTex, gl_TexCoord[0].xy);\n\t float preLum = 0.99*dot(lumCoef, sceneCol.rgb)+0.01;\n    float postLum = preLum*(1.0+(preLum/params.y))/(preLum+1.0);\n\t gl_FragColor = sceneCol*(postLum/preLum)+bloomCol*params.x;\n}\n", 35632)}), aHa_Sub3_4631));
			aClass389_5992 = (Class296_Sub51_Sub19.method3127(1, (new Class77[]{Class296_Sub51_Sub3.method3084(19399, aHa_Sub3_4631, "uniform vec3 step;\nuniform sampler2D baseTex;\nvoid main() {\n\tvec4 fragCol = texture2D(baseTex, gl_TexCoord[0].xy)*0.091396265;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+(-1.0*step.xy))*0.088584304;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+( 1.0*step.xy))*0.088584304;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+(-2.0*step.xy))*0.08065692;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+( 2.0*step.xy))*0.08065692;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+(-3.0*step.xy))*0.068989515;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+( 3.0*step.xy))*0.068989515;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+(-4.0*step.xy))*0.055434637;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+( 4.0*step.xy))*0.055434637;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+(-5.0*step.xy))*0.04184426;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+( 5.0*step.xy))*0.04184426;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+(-6.0*step.xy))*0.029672023;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+( 6.0*step.xy))*0.029672023;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+(-7.0*step.xy))*0.019765828;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+( 7.0*step.xy))*0.019765828;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+(-8.0*step.xy))*0.012369139;\n\tfragCol += texture2D(baseTex, gl_TexCoord[0].xy+( 8.0*step.xy))*0.012369139;\n\tgl_FragColor = fragCol;\n}\n", 35632)}), aHa_Sub3_4631));
			if (aClass389_5988 == null || aClass389_5984 == null || aClass389_5980 == null || aClass389_5992 == null)
				return false;
			return true;
		}
		return false;
	}

	final void method2484(int i, int i_7_) {
		OpenGL.glUseProgramObjectARB(0L);
		aHa_Sub3_4631.method1330(122, 1);
		aHa_Sub3_4631.method1316(null, (byte) -113);
		aHa_Sub3_4631.method1330(112, 0);
		if (i <= 70)
			method2489((byte) 103);
	}

	final boolean method2483(int i) {
		int i_8_ = -64 / ((-26 - i) / 33);
		if (aClass392_5981 == null)
			return false;
		return true;
	}

	final void method2480(byte i, Class69_Sub1 class69_sub1, int i_9_, Class69_Sub1 class69_sub1_10_) {
		OpenGL.glPushAttrib(2048);
		OpenGL.glMatrixMode(5889);
		OpenGL.glPushMatrix();
		OpenGL.glLoadIdentity();
		OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
		if (aClass69_Sub1Array5985 == null) {
			aHa_Sub3_4631.method1316(class69_sub1_10_, (byte) -127);
			aHa_Sub3_4631.method1351(aClass392_5981, (byte) -27);
			aClass392_5981.method4045(0, 0);
			OpenGL.glViewport(0, 0, 256, 256);
			long l = aClass389_5984.aLong3280;
			OpenGL.glUseProgramObjectARB(l);
			OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l, "sceneTex"), 0);
			OpenGL.glUniform3fARB(OpenGL.glGetUniformLocationARB(l, "params"), HardReferenceWrapper.aFloat6696, 0.0F, 0.0F);
			OpenGL.glBegin(7);
			OpenGL.glTexCoord2f(0.0F, 0.0F);
			OpenGL.glVertex2i(0, 0);
			OpenGL.glTexCoord2f((float) anInt5991, 0.0F);
			OpenGL.glVertex2i(1, 0);
			OpenGL.glTexCoord2f((float) anInt5991, (float) anInt5979);
			OpenGL.glVertex2i(1, 1);
			OpenGL.glTexCoord2f(0.0F, (float) anInt5979);
			OpenGL.glVertex2i(0, 1);
			OpenGL.glEnd();
		} else {
			aHa_Sub3_4631.method1351(aClass392_5989, (byte) -27);
			int i_11_ = Statics.method629(false, anInt5991);
			int i_12_ = Statics.method629(false, anInt5979);
			int i_13_ = 0;
			while (i_11_ > 256 || i_12_ > 256) {
				OpenGL.glViewport(0, 0, i_11_, i_12_);
				aClass392_5989.method4050(aClass69_Sub1Array5985[i_13_], 0, 0);
				if (i_12_ > 256)
					i_12_ >>= 1;
				if (i_11_ > 256)
					i_11_ >>= 1;
				if (i_13_ != 0) {
					aHa_Sub3_4631.method1316(aClass69_Sub1Array5985[i_13_ - 1], (byte) -128);
					OpenGL.glBegin(7);
					OpenGL.glTexCoord2f(0.0F, 0.0F);
					OpenGL.glVertex2i(0, 0);
					OpenGL.glTexCoord2f(1.0F, 0.0F);
					OpenGL.glVertex2i(1, 0);
					OpenGL.glTexCoord2f(1.0F, 1.0F);
					OpenGL.glVertex2i(1, 1);
					OpenGL.glTexCoord2f(0.0F, 1.0F);
					OpenGL.glVertex2i(0, 1);
					OpenGL.glEnd();
				} else {
					aHa_Sub3_4631.method1316(class69_sub1_10_, (byte) -123);
					OpenGL.glBegin(7);
					OpenGL.glTexCoord2f(0.0F, 0.0F);
					OpenGL.glVertex2i(0, 0);
					OpenGL.glTexCoord2f((float) anInt5991, 0.0F);
					OpenGL.glVertex2i(1, 0);
					OpenGL.glTexCoord2f((float) anInt5991, (float) anInt5979);
					OpenGL.glVertex2i(1, 1);
					OpenGL.glTexCoord2f(0.0F, (float) anInt5979);
					OpenGL.glVertex2i(0, 1);
					OpenGL.glEnd();
				}
				i_13_++;
			}
			aHa_Sub3_4631.method1280(aClass392_5989, -1);
			aHa_Sub3_4631.method1316(aClass69_Sub1Array5985[i_13_ - 1], (byte) -117);
			aHa_Sub3_4631.method1351(aClass392_5981, (byte) -27);
			aClass392_5981.method4045(0, 0);
			OpenGL.glViewport(0, 0, 256, 256);
			long l = aClass389_5988.aLong3280;
			OpenGL.glUseProgramObjectARB(l);
			OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l, "sceneTex"), 0);
			OpenGL.glUniform3fARB(OpenGL.glGetUniformLocationARB(l, "params"), HardReferenceWrapper.aFloat6696, 0.0F, 0.0F);
			OpenGL.glBegin(7);
			OpenGL.glTexCoord2f(0.0F, 0.0F);
			OpenGL.glVertex2i(0, 0);
			OpenGL.glTexCoord2f(1.0F, 0.0F);
			OpenGL.glVertex2i(1, 0);
			OpenGL.glTexCoord2f(1.0F, 1.0F);
			OpenGL.glVertex2i(1, 1);
			OpenGL.glTexCoord2f(0.0F, 1.0F);
			OpenGL.glVertex2i(0, 1);
			OpenGL.glEnd();
		}
		aClass392_5981.method4045(0, 1);
		aHa_Sub3_4631.method1316(aClass69_Sub1_5993, (byte) -124);
		long l = aClass389_5992.aLong3280;
		OpenGL.glUseProgramObjectARB(l);
		OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l, "baseTex"), 0);
		OpenGL.glUniform3fARB(OpenGL.glGetUniformLocationARB(l, "step"), 0.00390625F, 0.0F, 0.0F);
		OpenGL.glBegin(7);
		OpenGL.glTexCoord2f(0.0F, 0.0F);
		OpenGL.glVertex2i(0, 0);
		OpenGL.glTexCoord2f(1.0F, 0.0F);
		OpenGL.glVertex2i(1, 0);
		OpenGL.glTexCoord2f(1.0F, 1.0F);
		OpenGL.glVertex2i(1, 1);
		OpenGL.glTexCoord2f(0.0F, 1.0F);
		OpenGL.glVertex2i(0, 1);
		OpenGL.glEnd();
		aClass392_5981.method4045(0, 0);
		aHa_Sub3_4631.method1316(aClass69_Sub1_5983, (byte) -114);
		OpenGL.glUniform3fARB(OpenGL.glGetUniformLocationARB(l, "step"), 0.0F, 0.00390625F, 0.0F);
		OpenGL.glBegin(7);
		OpenGL.glTexCoord2f(0.0F, 0.0F);
		OpenGL.glVertex2i(0, 0);
		OpenGL.glTexCoord2f(1.0F, 0.0F);
		OpenGL.glVertex2i(1, 0);
		OpenGL.glTexCoord2f(1.0F, 1.0F);
		OpenGL.glVertex2i(1, 1);
		OpenGL.glTexCoord2f(0.0F, 1.0F);
		OpenGL.glVertex2i(0, 1);
		OpenGL.glEnd();
		OpenGL.glPopAttrib();
		OpenGL.glPopMatrix();
		OpenGL.glMatrixMode(5888);
		aHa_Sub3_4631.method1280(aClass392_5981, -1);
		long l_14_ = aClass389_5980.aLong3280;
		if (i < 7)
			aClass69_Sub1_5983 = null;
		OpenGL.glUseProgramObjectARB(l_14_);
		OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l_14_, "sceneTex"), 0);
		OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l_14_, "bloomTex"), 1);
		OpenGL.glUniform3fARB(OpenGL.glGetUniformLocationARB(l_14_, "params"), Class318.aFloat2810, GameType.aFloat342, 0.0F);
		aHa_Sub3_4631.method1330(108, 1);
		aHa_Sub3_4631.method1316(aClass69_Sub1_5993, (byte) -127);
		aHa_Sub3_4631.method1330(107, 0);
		aHa_Sub3_4631.method1316(class69_sub1_10_, (byte) -118);
	}

	final void method2482(byte i) {
		aClass69_Sub1_5983 = null;
		aClass389_5988 = null;
		if (i > -114)
			method2484(-124, 2);
		aClass389_5984 = null;
		aClass69_Sub1_5993 = null;
		aClass389_5992 = null;
		aClass392_5981 = null;
		aClass389_5980 = null;
		aClass69_Sub1Array5985 = null;
		aClass392_5989 = null;
	}

	final void method2487(int i, int i_15_, boolean bool) {
		anInt5991 = i;
		anInt5979 = i_15_;
		int i_16_ = Statics.method629(false, anInt5991);
		int i_17_ = Statics.method629(bool, anInt5979);
		if (anInt5990 != i_16_ || i_17_ != anInt5987) {
			if (aClass69_Sub1Array5985 != null) {
				for (int i_18_ = 0; aClass69_Sub1Array5985.length > i_18_; i_18_++)
					aClass69_Sub1Array5985[i_18_].method726(0);
				aClass69_Sub1Array5985 = null;
			}
			if (i_16_ <= 256 && i_17_ <= 256)
				aClass392_5989 = null;
			else {
				int i_19_ = i_16_;
				int i_20_ = i_17_;
				int i_21_ = 0;
				while (i_19_ > 256 || i_20_ > 256) {
					if (i_19_ > 256)
						i_19_ >>= 1;
					if (i_20_ > 256)
						i_20_ >>= 1;
					i_21_++;
				}
				if (aClass392_5989 == null)
					aClass392_5989 = new Class392(aHa_Sub3_4631);
				i_19_ = i_16_;
				aClass69_Sub1Array5985 = new Class69_Sub1[i_21_];
				i_20_ = i_17_;
				i_21_ = 0;
				while (i_19_ > 256 || i_20_ > 256) {
					aClass69_Sub1Array5985[i_21_++] = new Class69_Sub1(aHa_Sub3_4631, 3553, 34842, i_19_, i_20_);
					if (i_20_ > 256)
						i_20_ >>= 1;
					if (i_19_ > 256)
						i_19_ >>= 1;
				}
			}
			anInt5990 = i_16_;
			anInt5987 = i_17_;
		}
	}

	Class296_Sub9_Sub1(ha_Sub3 var_ha_Sub3) {
		super(var_ha_Sub3);
	}
}
