package net.zaros.client;

/* Class296_Sub51_Sub37 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub37 extends TextureOperation {
	static Class338_Sub3[] aClass338_Sub3Array6531;
	static String aString6532;
	static int anInt6533;
	private int anInt6534 = 4;
	private int anInt6535 = 4;
	static int[][] anIntArrayArray6536;

	public static void method3192(int i) {
		if (i != 0)
			method3193(12);
		aClass338_Sub3Array6531 = null;
		anIntArrayArray6536 = null;
		aString6532 = null;
	}

	final int[] get_monochrome_output(int i, int i_0_) {
		int[] is = aClass318_5035.method3335(i_0_, (byte) 28);
		if (i != 0)
			get_colour_output(-43, -86);
		if (aClass318_5035.aBoolean2819) {
			int i_1_ = Class41_Sub10.anInt3769 / anInt6534;
			int i_2_ = Class296_Sub35_Sub2.anInt6114 / anInt6535;
			int[] is_3_;
			if (i_2_ <= 0)
				is_3_ = this.method3064(0, 0, 0);
			else {
				int i_4_ = i_0_ % i_2_;
				is_3_ = this.method3064(0, i, (Class296_Sub35_Sub2.anInt6114 * i_4_ / i_2_));
			}
			for (int i_5_ = 0; Class41_Sub10.anInt3769 > i_5_; i_5_++) {
				if (i_1_ <= 0)
					is[i_5_] = is_3_[0];
				else {
					int i_6_ = i_5_ % i_1_;
					is[i_5_] = is_3_[i_6_ * Class41_Sub10.anInt3769 / i_1_];
				}
			}
		}
		return is;
	}

	static final Class296_Sub39_Sub14 method3193(int i) {
		if (i > -62)
			return null;
		return Class106.aClass296_Sub39_Sub14_1097;
	}

	public Class296_Sub51_Sub37() {
		super(1, false);
	}

	final void method3071(int i, Packet class296_sub17, int i_7_) {
		if (i > -84)
			aString6532 = null;
		int i_8_ = i_7_;
		do {
			if (i_8_ != 0) {
				if (i_8_ != 1)
					break;
			} else {
				anInt6534 = class296_sub17.g1();
				break;
			}
			anInt6535 = class296_sub17.g1();
			break;
		} while (false);
	}

	final int[][] get_colour_output(int i, int i_9_) {
		if (i_9_ != 17621)
			get_colour_output(-13, 23);
		int[][] is = aClass86_5034.method823((byte) 93, i);
		if (aClass86_5034.aBoolean939) {
			int i_10_ = Class41_Sub10.anInt3769 / anInt6534;
			int i_11_ = Class296_Sub35_Sub2.anInt6114 / anInt6535;
			int[][] is_12_;
			if (i_11_ > 0) {
				int i_13_ = i % i_11_;
				is_12_ = this.method3075((byte) 125, 0, (i_13_ * Class296_Sub35_Sub2.anInt6114 / i_11_));
			} else
				is_12_ = this.method3075((byte) -122, 0, 0);
			int[] is_14_ = is_12_[0];
			int[] is_15_ = is_12_[1];
			int[] is_16_ = is_12_[2];
			int[] is_17_ = is[0];
			int[] is_18_ = is[1];
			int[] is_19_ = is[2];
			for (int i_20_ = 0; i_20_ < Class41_Sub10.anInt3769; i_20_++) {
				int i_21_;
				if (i_10_ > 0) {
					int i_22_ = i_20_ % i_10_;
					i_21_ = Class41_Sub10.anInt3769 * i_22_ / i_10_;
				} else
					i_21_ = 0;
				is_17_[i_20_] = is_14_[i_21_];
				is_18_[i_20_] = is_15_[i_21_];
				is_19_[i_20_] = is_16_[i_21_];
			}
		}
		return is;
	}
}
