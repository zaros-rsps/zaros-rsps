package net.zaros.client;

/* Class296_Sub51_Sub18 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub18 extends TextureOperation {
	static double aDouble6434;
	static Class81 aClass81_6435 = new Class81("", 14);
	static int anInt6436 = 0;

	public static void method3125(int i) {
		aClass81_6435 = null;
		if (i != 0)
			method3126(33, (byte) -55, -49);
	}

	final int[][] get_colour_output(int i, int i_0_) {
		int[][] is = aClass86_5034.method823((byte) 24, i);
		if (i_0_ != 17621)
			return null;
		if (aClass86_5034.aBoolean939) {
			int[][] is_1_ = this.method3075((byte) 124, 0, i);
			int[] is_2_ = is_1_[0];
			int[] is_3_ = is_1_[1];
			int[] is_4_ = is_1_[2];
			int[] is_5_ = is[0];
			int[] is_6_ = is[1];
			int[] is_7_ = is[2];
			for (int i_8_ = 0; Class41_Sub10.anInt3769 > i_8_; i_8_++) {
				is_5_[i_8_] = -is_2_[i_8_] + 4096;
				is_6_[i_8_] = -is_3_[i_8_] + 4096;
				is_7_[i_8_] = -is_4_[i_8_] + 4096;
			}
		}
		return is;
	}

	static final byte method3126(int i, byte i_9_, int i_10_) {
		if (i != 9)
			return (byte) 0;
		if (i_9_ != 17)
			method3125(-58);
		if ((i_10_ & 0x1) == 0)
			return (byte) 1;
		return (byte) 2;
	}

	public Class296_Sub51_Sub18() {
		super(1, false);
	}

	final int[] get_monochrome_output(int i, int i_11_) {
		int[] is = aClass318_5035.method3335(i_11_, (byte) 28);
		if (i != 0)
			aClass81_6435 = null;
		if (aClass318_5035.aBoolean2819) {
			int[] is_12_ = this.method3064(0, 0, i_11_);
			for (int i_13_ = 0; i_13_ < Class41_Sub10.anInt3769; i_13_++)
				is[i_13_] = -is_12_[i_13_] + 4096;
		}
		return is;
	}

	final void method3071(int i, Packet class296_sub17, int i_14_) {
		if (i >= -84)
			get_colour_output(-109, 52);
		if (i_14_ == 0)
			monochromatic = class296_sub17.g1() == 1;
	}
}
