package net.zaros.client;

/* Class296_Sub51_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public  class Class296_Sub51_Sub4 extends TextureOperation {
	private boolean aBoolean6354 = true;
	private int anInt6355 = 4096;
	static IncomingPacket aClass231_6356 = new IncomingPacket(43, 2);

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		int i_1_ = i_0_;
		do {
			if (i_1_ != 0) {
				if (i_1_ != 1)
					break;
			} else {
				anInt6355 = class296_sub17.g2();
				break;
			}
			aBoolean6354 = class296_sub17.g1() == 1;
		} while (false);
		if (i > -84)
			method3088(-29);
	}

	final int[][] get_colour_output(int i, int i_2_) {
		if (i_2_ != 17621)
			return null;
		int[][] is = aClass86_5034.method823((byte) 72, i);
		if (aClass86_5034.aBoolean939) {
			int[] is_3_ = this.method3064(0, 0, i - 1 & Class67.anInt753);
			int[] is_4_ = this.method3064(0, i_2_ ^ 0x44d5, i);
			int[] is_5_ = this.method3064(0, 0, Class67.anInt753 & i + 1);
			int[] is_6_ = is[0];
			int[] is_7_ = is[1];
			int[] is_8_ = is[2];
			for (int i_9_ = 0; i_9_ < Class41_Sub10.anInt3769; i_9_++) {
				int i_10_ = (-is_3_[i_9_] + is_5_[i_9_]) * anInt6355;
				int i_11_ = (anInt6355 * (is_4_[i_9_ + 1 & Class41_Sub25.anInt3803] - is_4_[Class41_Sub25.anInt3803 & i_9_ - 1]));
				int i_12_ = i_11_ >> 12;
				int i_13_ = i_10_ >> 12;
				int i_14_ = i_12_ * i_12_ >> 12;
				int i_15_ = i_13_ * i_13_ >> 12;
				int i_16_ = (int) (Math.sqrt((double) ((float) (i_15_ + 4096 + i_14_) / 4096.0F)) * 4096.0);
				int i_17_;
				int i_18_;
				int i_19_;
				if (i_16_ == 0) {
					i_19_ = 0;
					i_17_ = 0;
					i_18_ = 0;
				} else {
					i_17_ = i_10_ / i_16_;
					i_18_ = i_11_ / i_16_;
					i_19_ = 16777216 / i_16_;
				}
				if (aBoolean6354) {
					i_18_ = (i_18_ >> 1) + 2048;
					i_17_ = (i_17_ >> 1) + 2048;
					i_19_ = (i_19_ >> 1) + 2048;
				}
				is_6_[i_9_] = i_18_;
				is_7_[i_9_] = i_17_;
				is_8_[i_9_] = i_19_;
			}
		}
		return is;
	}

	public static BillboardRaw method3086(byte i, int i_20_) {
		BillboardRaw class101 = ((BillboardRaw) Class368_Sub4.aClass113_5443.get((long) i_20_));
		if (class101 != null)
			return class101;
		int i_21_ = 59 / ((i + 41) / 49);
		byte[] is = StaticMethods.aClass138_5928.getFile(0, i_20_);
		class101 = new BillboardRaw();
		if (is != null)
			class101.method888((byte) -58, new Packet(is), i_20_);
		Class368_Sub4.aClass113_5443.put(class101, (long) i_20_);
		return class101;
	}

	static final void method3087(int i) {
		Class296_Sub39_Sub17.aClass397_6244 = null;
		Class130.aClass397_1334 = null;
		Class363.aClass397_3107 = null;
		Class227.aClass397_2187 = null;
		Class368_Sub15.aClass397_5521 = null;
		Class41_Sub27.aClass397_3811 = null;
		if (i == 21097) {
			Class49.aClass397_463 = null;
			Class395.aClass397_3316 = null;
			Class44_Sub1_Sub1.aClass397Array5811 = null;
		}
	}

	public Class296_Sub51_Sub4() {
		super(1, false);
	}

	public static void method3088(int i) {
		if (i == 32547)
			aClass231_6356 = null;
	}
}
