package net.zaros.client;

/* Class296_Sub39_Sub13 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub13 extends Queuable {
	private HashTable aClass263_6203;
	static OutgoingPacket aClass311_6204 = new OutgoingPacket(66, 8);
	static AdvancedMemoryCache aClass113_6205 = new AdvancedMemoryCache(3000000, 200);
	static Class230 aClass230_6206 = new Class230();

	final int method2853(int i, int i_0_, int i_1_) {
		if (aClass263_6203 == null)
			return i;
		IntegerNode class296_sub16 = (IntegerNode) aClass263_6203.get((long) i_0_);
		if (class296_sub16 == null)
			return i;
		if (i_1_ != 8)
			method2854((byte) 58);
		return class296_sub16.value;
	}

	public static void method2854(byte i) {
		aClass113_6205 = null;
		if (i > -35)
			method2854((byte) 19);
		aClass311_6204 = null;
		aClass230_6206 = null;
	}

	private final void method2855(int i, Packet class296_sub17, byte i_2_) {
		if (i_2_ < -116) {
			if (i == 249) {
				int i_3_ = class296_sub17.g1();
				if (aClass263_6203 == null) {
					int i_4_ = Class8.get_next_high_pow2(i_3_);
					aClass263_6203 = new HashTable(i_4_);
				}
				for (int i_5_ = 0; i_5_ < i_3_; i_5_++) {
					boolean bool = class296_sub17.g1() == 1;
					int i_6_ = class296_sub17.readUnsignedMedInt();
					Node class296;
					if (!bool)
						class296 = new IntegerNode(class296_sub17.g4());
					else
						class296 = new StringNode(class296_sub17.gstr());
					aClass263_6203.put((long) i_6_, class296);
				}
			}
		}
	}

	final String method2856(String string, int i, int i_7_) {
		if (aClass263_6203 == null)
			return string;
		if (i <= 95)
			aClass263_6203 = null;
		StringNode class296_sub5 = (StringNode) aClass263_6203.get((long) i_7_);
		if (class296_sub5 == null)
			return string;
		return class296_sub5.value;
	}

	final void method2857(Packet class296_sub17, byte i) {
		if (i != -51)
			aClass230_6206 = null;
		for (;;) {
			int i_8_ = class296_sub17.g1();
			if (i_8_ == 0)
				break;
			method2855(i_8_, class296_sub17, (byte) -123);
		}
	}

	public Class296_Sub39_Sub13() {
		/* empty */
	}
}
