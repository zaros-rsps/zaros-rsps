package net.zaros.client;

/* Class373_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class373_Sub3 extends Class373 {
	float aFloat5604;
	static int anInt5605;
	float aFloat5606;
	float aFloat5607;
	static int anInt5608;
	static Thread aThread5609;
	float aFloat5610;
	float aFloat5611;
	static Class373 aClass373_5612;
	float aFloat5613;
	float aFloat5614;
	float aFloat5615;
	float aFloat5616;
	float aFloat5617;
	float aFloat5618;
	float aFloat5619;

	@Override
	public final void method3905(int i, int i_0_, int i_1_, int[] is) {
		i_1_ -= aFloat5618;
		i -= aFloat5611;
		i_0_ -= aFloat5619;
		is[0] = (int) (aFloat5614 * i_1_ + (aFloat5607 * i_0_ + aFloat5606 * i));
		is[1] = (int) (aFloat5615 * i_0_ + aFloat5613 * i + i_1_ * aFloat5617);
		is[2] = (int) (i_0_ * aFloat5616 + i * aFloat5610 + i_1_ * aFloat5604);
	}

	@Override
	public final void method3910() {
		aFloat5606 = aFloat5615 = aFloat5604 = 1.0F;
		aFloat5607 = aFloat5614 = aFloat5613 = aFloat5617 = aFloat5610 = aFloat5616 = aFloat5611 = aFloat5619 = aFloat5618 = 0.0F;
	}

	@Override
	public final void method3903(int i, int i_2_, int i_3_, int[] is) {
		is[2] = (int) (aFloat5618 + (aFloat5604 * i_3_ + (i_2_ * aFloat5617 + i * aFloat5614)));
		is[1] = (int) (i_2_ * aFloat5615 + i * aFloat5607 + i_3_ * aFloat5616 + aFloat5619);
		is[0] = (int) (aFloat5611 + (i_3_ * aFloat5610 + (aFloat5613 * i_2_ + aFloat5606 * i)));
	}

	@Override
	public final void method3908(int[] is) {
		float f = -aFloat5611 + is[0];
		float f_4_ = is[1] - aFloat5619;
		float f_5_ = is[2] - aFloat5618;
		is[0] = (int) (aFloat5614 * f_5_ + (f_4_ * aFloat5607 + aFloat5606 * f));
		is[1] = (int) (aFloat5615 * f_4_ + aFloat5613 * f + f_5_ * aFloat5617);
		is[2] = (int) (aFloat5604 * f_5_ + (f * aFloat5610 + f_4_ * aFloat5616));
	}

	@Override
	public final void method3915(Class373 class373) {
		Class373_Sub3 class373_sub3_6_ = (Class373_Sub3) class373;
		aFloat5606 = class373_sub3_6_.aFloat5606;
		aFloat5618 = class373_sub3_6_.aFloat5618;
		aFloat5604 = class373_sub3_6_.aFloat5604;
		aFloat5619 = class373_sub3_6_.aFloat5619;
		aFloat5607 = class373_sub3_6_.aFloat5607;
		aFloat5616 = class373_sub3_6_.aFloat5616;
		aFloat5611 = class373_sub3_6_.aFloat5611;
		aFloat5610 = class373_sub3_6_.aFloat5610;
		aFloat5614 = class373_sub3_6_.aFloat5614;
		aFloat5617 = class373_sub3_6_.aFloat5617;
		aFloat5613 = class373_sub3_6_.aFloat5613;
		aFloat5615 = class373_sub3_6_.aFloat5615;
	}

	@Override
	public final void method3900(int i) {
		float f = Class5.aFloatArray78[i & 0x3fff];
		float f_7_ = Class5.aFloatArray76[i & 0x3fff];
		float f_8_ = aFloat5606;
		float f_9_ = aFloat5613;
		float f_10_ = aFloat5610;
		float f_11_ = aFloat5611;
		aFloat5606 = f_8_ * f - aFloat5607 * f_7_;
		aFloat5613 = -(aFloat5615 * f_7_) + f_9_ * f;
		aFloat5607 = f * aFloat5607 + f_7_ * f_8_;
		aFloat5610 = -(aFloat5616 * f_7_) + f_10_ * f;
		aFloat5615 = f_9_ * f_7_ + aFloat5615 * f;
		aFloat5611 = f_11_ * f - f_7_ * aFloat5619;
		aFloat5616 = f_7_ * f_10_ + aFloat5616 * f;
		aFloat5619 = f_7_ * f_11_ + f * aFloat5619;
	}

	@Override
	public final void method3906(int i) {
		aFloat5615 = 1.0F;
		aFloat5606 = aFloat5604 = Class5.aFloatArray78[i & 0x3fff];
		aFloat5610 = Class5.aFloatArray76[i & 0x3fff];
		aFloat5613 = aFloat5611 = aFloat5607 = aFloat5616 = aFloat5619 = aFloat5617 = aFloat5618 = 0.0F;
		aFloat5614 = -aFloat5610;
	}

	@Override
	public final Class373 method3916() {
		Class373_Sub3 class373_sub3_12_ = new Class373_Sub3();
		class373_sub3_12_.aFloat5619 = aFloat5619;
		class373_sub3_12_.aFloat5610 = aFloat5610;
		class373_sub3_12_.aFloat5617 = aFloat5617;
		class373_sub3_12_.aFloat5616 = aFloat5616;
		class373_sub3_12_.aFloat5611 = aFloat5611;
		class373_sub3_12_.aFloat5615 = aFloat5615;
		class373_sub3_12_.aFloat5606 = aFloat5606;
		class373_sub3_12_.aFloat5614 = aFloat5614;
		class373_sub3_12_.aFloat5604 = aFloat5604;
		class373_sub3_12_.aFloat5618 = aFloat5618;
		class373_sub3_12_.aFloat5607 = aFloat5607;
		class373_sub3_12_.aFloat5613 = aFloat5613;
		return class373_sub3_12_;
	}

	@Override
	public final void method3902(int i, int i_13_, int i_14_) {
		aFloat5618 = i_14_;
		aFloat5607 = aFloat5614 = aFloat5613 = aFloat5617 = aFloat5610 = aFloat5616 = 0.0F;
		aFloat5606 = aFloat5615 = aFloat5604 = 1.0F;
		aFloat5619 = i_13_;
		aFloat5611 = i;
	}

	public static void method3950(int i) {
		if (i != 16383) {
			anInt5605 = -116;
		}
		aThread5609 = null;
		aClass373_5612 = null;
	}

	@Override
	public final void method3907(int i, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_) {
		float f = Class5.aFloatArray78[i_17_ & 0x3fff];
		float f_20_ = Class5.aFloatArray76[i_17_ & 0x3fff];
		float f_21_ = Class5.aFloatArray78[i_18_ & 0x3fff];
		float f_22_ = Class5.aFloatArray76[i_18_ & 0x3fff];
		float f_23_ = Class5.aFloatArray78[i_19_ & 0x3fff];
		float f_24_ = Class5.aFloatArray76[i_19_ & 0x3fff];
		float f_25_ = f_23_ * f_20_;
		float f_26_ = f_20_ * f_24_;
		aFloat5617 = -f_20_;
		aFloat5616 = f_21_ * f_25_ + f_24_ * f_22_;
		aFloat5604 = f_21_ * f;
		aFloat5607 = f_25_ * f_22_ + f_24_ * -f_21_;
		aFloat5606 = f_23_ * f_21_ + f_22_ * f_26_;
		aFloat5614 = f * f_22_;
		aFloat5615 = f_23_ * f;
		aFloat5613 = f_24_ * f;
		aFloat5610 = -f_22_ * f_23_ + f_26_ * f_21_;
		aFloat5619 = -i * aFloat5607 - aFloat5615 * i_15_ - aFloat5616 * i_16_;
		aFloat5618 = -i * aFloat5614 - aFloat5617 * i_15_ - i_16_ * aFloat5604;
		aFloat5611 = -(aFloat5610 * i_16_) + (aFloat5606 * -i - i_15_ * aFloat5613);
	}

	@Override
	public final void method3914(int i) {
		float f = Class5.aFloatArray78[i & 0x3fff];
		float f_27_ = Class5.aFloatArray76[i & 0x3fff];
		float f_28_ = aFloat5607;
		float f_29_ = aFloat5615;
		float f_30_ = aFloat5616;
		float f_31_ = aFloat5619;
		aFloat5607 = -(aFloat5614 * f_27_) + f_28_ * f;
		aFloat5615 = -(f_27_ * aFloat5617) + f_29_ * f;
		aFloat5614 = aFloat5614 * f + f_27_ * f_28_;
		aFloat5616 = -(aFloat5604 * f_27_) + f_30_ * f;
		aFloat5617 = aFloat5617 * f + f_29_ * f_27_;
		aFloat5619 = f * f_31_ - aFloat5618 * f_27_;
		aFloat5604 = f_27_ * f_30_ + aFloat5604 * f;
		aFloat5618 = f_31_ * f_27_ + f * aFloat5618;
	}

	@Override
	public final void method3904(int i, int i_32_, int i_33_) {
		aFloat5619 += i_32_;
		aFloat5618 += i_33_;
		aFloat5611 += i;
	}

	@Override
	public final void method3917(int i) {
		float f = Class5.aFloatArray78[i & 0x3fff];
		float f_34_ = Class5.aFloatArray76[i & 0x3fff];
		float f_35_ = aFloat5606;
		float f_36_ = aFloat5613;
		float f_37_ = aFloat5610;
		float f_38_ = aFloat5611;
		aFloat5606 = f_35_ * f + f_34_ * aFloat5614;
		aFloat5613 = aFloat5617 * f_34_ + f * f_36_;
		aFloat5614 = -(f_34_ * f_35_) + aFloat5614 * f;
		aFloat5617 = aFloat5617 * f - f_34_ * f_36_;
		aFloat5610 = f_37_ * f + aFloat5604 * f_34_;
		aFloat5611 = aFloat5618 * f_34_ + f_38_ * f;
		aFloat5604 = f * aFloat5604 - f_34_ * f_37_;
		aFloat5618 = -(f_38_ * f_34_) + f * aFloat5618;
	}

	@Override
	public final void method3901(int i, int i_39_, int i_40_, int[] is) {
		is[0] = (int) (i_40_ * aFloat5610 + (aFloat5613 * i_39_ + i * aFloat5606));
		is[1] = (int) (i_40_ * aFloat5616 + (aFloat5607 * i + i_39_ * aFloat5615));
		is[2] = (int) (aFloat5604 * i_40_ + (i_39_ * aFloat5617 + i * aFloat5614));
	}


	@Override
	public final void method3911(int i) {
		aFloat5604 = 1.0F;
		aFloat5606 = aFloat5615 = Class5.aFloatArray78[i & 0x3fff];
		aFloat5607 = Class5.aFloatArray76[i & 0x3fff];
		aFloat5610 = aFloat5611 = aFloat5616 = aFloat5619 = aFloat5614 = aFloat5617 = aFloat5618 = 0.0F;
		aFloat5613 = -aFloat5607;
	}

	@Override
	public final void method3909(int i) {
		aFloat5606 = 1.0F;
		aFloat5615 = aFloat5604 = Class5.aFloatArray78[i & 0x3fff];
		aFloat5617 = Class5.aFloatArray76[i & 0x3fff];
		aFloat5613 = aFloat5610 = aFloat5611 = aFloat5607 = aFloat5619 = aFloat5614 = aFloat5618 = 0.0F;
		aFloat5616 = -aFloat5617;
	}

	public Class373_Sub3() {
		method3910();
	}
}
