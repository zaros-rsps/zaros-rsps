package net.zaros.client;

public class EffectiveVertex {
	public static int anInt2213;
	public int anInt2214;
	public static int[] anIntArray2215;
	public int anInt2216;
	public int anInt2217;
	public static boolean aBoolean2218 = false;
	public int anInt2219;
	public int anInt2220;
	public static boolean aBoolean2221 = false;
	public Class373 aClass373_2222;
	public EffectiveVertex aClass232_2223;

	public EffectiveVertex method2113(int i, byte i_0_) {
		if (i_0_ != 125)
			method2119(-69);
		return new EffectiveVertex(anInt2216, i);
	}

	public static final void method2114(double d, byte i) {
		Class338_Sub9.aClass373_5269.method3915(Class373_Sub3.aClass373_5612);
		Class338_Sub9.aClass373_5269.method3904(0, 0, (int) d);
		if (i == 2)
			Class296_Sub51_Sub36.aHa6529.a(Class338_Sub9.aClass373_5269);
	}

	public static void method2115(int i) {
		if (i != 0)
			aBoolean2221 = false;
		anIntArray2215 = null;
	}

	public static final int method2116(int i, InterfaceComponent class51, byte i_1_) {
		if (i_1_ < 8)
			aBoolean2221 = true;
		if (!GameClient.method115(class51).method2709(i, (byte) 107) && class51.anObjectArray585 == null)
			return -1;
		if (class51.cursors != null && class51.cursors.length > i)
			return class51.cursors[i];
		return -1;
	}

	static final boolean method2117(int i, int i_2_, int i_3_, byte[] is, int i_4_, int i_5_, boolean bool) {
		int i_6_ = i_3_ % i_4_;
		int i_7_;
		if (i_6_ != 0)
			i_7_ = -i_6_ + i_4_;
		else
			i_7_ = 0;
		if (bool != true)
			return true;
		int i_8_ = -((i_4_ + (i_5_ - 1)) / i_4_);
		int i_9_ = -((i_3_ + i_4_ - 1) / i_4_);
		for (int i_10_ = i_8_; i_10_ < 0; i_10_++) {
			for (int i_11_ = i_9_; i_11_ < 0; i_11_++) {
				if (is[i] == 0)
					return true;
				i += i_4_;
			}
			i -= i_7_;
			if (is[i - 1] == 0)
				return true;
			i += i_2_;
		}
		return false;
	}

	static final int method2118(byte i, int i_12_) {
		if (i != 117)
			method2115(71);
		return i_12_ & 0xff;
	}

	final Class95 method2119(int i) {
		if (i != 255)
			return null;
		return Class355.method3697(i ^ 0x881b, anInt2216);
	}

	EffectiveVertex(int i, int i_13_) {
		anInt2220 = i_13_;
		anInt2216 = i;
	}

	static {
		anIntArray2215 = new int[25];
	}
}
