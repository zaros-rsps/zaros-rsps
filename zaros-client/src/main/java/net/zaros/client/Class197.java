package net.zaros.client;

/* Class197 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class197 {
	private Class366[] aClass366Array1992;
	private int anInt1993 = 0;
	Class366_Sub7 aClass366_Sub7_1994;
	private int anInt1995 = 0;
	private Class361 aClass361_1996;
	private ha_Sub3 aHa_Sub3_1997;
	private int anInt1998 = 0;

	final void method1941(boolean bool, boolean bool_0_, int i, int i_1_, byte i_2_, int i_3_) {
		bool &= aHa_Sub3_1997.l();
		if (!bool && (i_1_ == 4 || i_1_ == 8 || i_1_ == 9)) {
			if (i_1_ == 4)
				i = i_3_;
			i_1_ = 2;
		}
		if (i_2_ >= 95) {
			if (i_1_ != 0 && bool_0_)
				i_1_ |= ~0x7fffffff;
			if (anInt1993 != i_1_) {
				if (anInt1993 != 0)
					aClass366Array1992[anInt1993 & 0x7fffffff].method3766(58);
				if (i_1_ != 0) {
					aClass366Array1992[i_1_ & 0x7fffffff].method3768((byte) -88, bool_0_);
					aClass366Array1992[i_1_ & 0x7fffffff].method3770((byte) 33, bool_0_);
					aClass366Array1992[i_1_ & 0x7fffffff].method3769(i_3_, (byte) -81, i);
				}
				anInt1993 = i_1_;
				anInt1998 = i_3_;
				anInt1995 = i;
			} else if (anInt1993 != 0) {
				aClass366Array1992[anInt1993 & 0x7fffffff].method3770((byte) 33, bool_0_);
				if (i_3_ != anInt1998 || anInt1995 != i) {
					aClass366Array1992[anInt1993 & 0x7fffffff].method3769(i_3_, (byte) -81, i);
					anInt1995 = i;
					anInt1998 = i_3_;
				}
			}
		}
	}

	final boolean method1942(int i, int i_4_) {
		if (i != 9)
			anInt1995 = 49;
		return aClass366Array1992[i_4_].method3763(i ^ 0x75);
	}

	final boolean method1943(int i, boolean bool, Class69 class69) {
		if (anInt1993 == 0)
			return false;
		aClass366Array1992[anInt1993 & 0x7fffffff].method3764(bool, i, class69);
		if (bool)
			method1941(false, false, -108, -125, (byte) -19, 52);
		return true;
	}

	Class197(ha_Sub3 var_ha_Sub3) {
		aHa_Sub3_1997 = var_ha_Sub3;
		aClass361_1996 = new Class361(var_ha_Sub3);
		aClass366Array1992 = new Class366[10];
		aClass366Array1992[1] = new Class366_Sub2(var_ha_Sub3);
		aClass366Array1992[2] = new Class366_Sub5(var_ha_Sub3, aClass361_1996);
		aClass366Array1992[4] = new Class366_Sub9(var_ha_Sub3, aClass361_1996);
		aClass366Array1992[5] = new Class366_Sub4(var_ha_Sub3, aClass361_1996);
		aClass366Array1992[6] = new Class366_Sub3(var_ha_Sub3);
		aClass366Array1992[7] = new Class366_Sub8(var_ha_Sub3);
		aClass366Array1992[3] = aClass366_Sub7_1994 = new Class366_Sub7(var_ha_Sub3);
		aClass366Array1992[8] = new Class366_Sub1(var_ha_Sub3, aClass361_1996);
		aClass366Array1992[9] = new Class366_Sub6(var_ha_Sub3, aClass361_1996);
		if (!aClass366Array1992[8].method3763(-39))
			aClass366Array1992[8] = aClass366Array1992[4];
		if (!aClass366Array1992[9].method3763(-49))
			aClass366Array1992[9] = aClass366Array1992[8];
	}
}
