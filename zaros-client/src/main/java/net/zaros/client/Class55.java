package net.zaros.client;

/* Class55 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Random;

public abstract class Class55 {
	public static Class202 aClass202_654;
	private ha aHa655;
	public static Applet_Sub1 anApplet_Sub1_656 = null;
	public static String aString657 = null;
	private Class92 aClass92_658;

	public int a(int i, String string, byte i_0_, int[] is, int i_1_, int i_2_, Random random, Sprite[] class397s,
			int i_3_, int i_4_) {
		if (string == null)
			return 0;
		random.setSeed((long) i_1_);
		if (i_0_ >= -63)
			aString657 = null;
		int i_5_ = (random.nextInt() & 0x1f) + 192;
		a(i & 0xffffff | i_5_ << 24, 0, i_5_ << 24 | i_2_ & 0xffffff);
		int i_6_ = string.length();
		int[] is_7_ = new int[i_6_];
		int i_8_ = 0;
		for (int i_9_ = 0; i_9_ < i_6_; i_9_++) {
			is_7_[i_9_] = i_8_;
			if ((random.nextInt() & 0x3) == 0)
				i_8_++;
		}
		a(class397s, null, i_3_, -128, is_7_, is, i_4_, string);
		return i_8_;
	}

	public void a(byte i, int i_10_, int i_11_, int i_12_, int i_13_, String string, int i_14_) {
		if (string != null) {
			a(i_11_, i ^ 0x51, i_13_);
			int i_15_ = string.length();
			int[] is = new int[i_15_];
			int[] is_16_ = new int[i_15_];
			if (i != 81)
				anApplet_Sub1_656 = null;
			for (int i_17_ = 0; i_17_ < i_15_; i_17_++) {
				is[i_17_] = (int) (Math.sin((double) i_14_ / 5.0 + (double) i_17_ / 5.0) * 5.0);
				is_16_[i_17_] = (int) (Math.sin((double) i_17_ / 3.0 + (double) i_14_ / 5.0) * 5.0);
			}
			a(null, is_16_, i_12_ - aClass92_658.method851(i - 150, string) / 2, -128, is, null, i_10_, string);
		}
	}

	public void a(int i, String string, int i_18_, int i_19_, int i_20_, int i_21_) {
		if (string != null) {
			a(i_19_, 0, i_18_);
			if (i != 15897)
				aClass202_654 = null;
			a(0, 0, null, -(aClass92_658.method851(-125, string) / 2) + i_20_, null, null, 0, i_21_, string);
		}
	}

	public static final int a(int i, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_, int i_27_) {
		int i_28_ = -87 / ((-7 - i_26_) / 57);
		i_23_ &= 0x3;
		if ((i_25_ & 0x1) == 1) {
			int i_29_ = i;
			i = i_27_;
			i_27_ = i_29_;
		}
		if (i_23_ == 0)
			return i_22_;
		if (i_23_ == 1)
			return -i + (-i_24_ + 1 + 7);
		if (i_23_ == 2)
			return -i_22_ + (7 - (i_27_ - 1));
		return i_24_;
	}

	public void a(int i, int i_30_, String string, int i_31_, int i_32_, int i_33_, int i_34_) {
		if (string != null) {
			a(i_31_, 0, i_30_);
			int i_35_ = string.length();
			int[] is = new int[i_35_];
			for (int i_36_ = i_33_; i_36_ < i_35_; i_36_++)
				is[i_36_] = (int) (Math.sin((double) i_34_ / 5.0 + (double) i_36_ / 2.0) * 5.0);
			a(null, is, -(aClass92_658.method851(-76, string) / 2) + i, i_33_ - 127, null, null, i_32_, string);
		}
	}

	public int a(int i, int[] is, int[] is_37_, int i_38_, int i_39_, int i_40_, String string, Sprite[] class397s,
			int i_41_, int i_42_, int i_43_, int i_44_, Random random, int i_45_, int i_46_) {
		if (i != -28329)
			aString657 = null;
		if (string == null)
			return 0;
		random.setSeed((long) i_45_);
		int i_47_ = (random.nextInt() & 0x1f) + 192;
		a(i_47_ << 24 | i_43_ & 0xffffff, 0, i_47_ << 24 | i_39_ & 0xffffff);
		int i_48_ = string.length();
		int[] is_49_ = new int[i_48_];
		int i_50_ = 0;
		for (int i_51_ = 0; i_48_ > i_51_; i_51_++) {
			is_49_[i_51_] = i_50_;
			if ((random.nextInt() & 0x3) == 0)
				i_50_++;
		}
		int i_52_ = i_42_;
		int i_53_ = i_38_ + aClass92_658.anInt989;
		int i_54_ = -1;
		if (i_46_ != 1) {
			if (i_46_ == 2)
				i_53_ = -aClass92_658.anInt992 + i_40_ + i_38_;
		} else
			i_53_ += (-aClass92_658.anInt992 + (-aClass92_658.anInt989 + i_40_)) / 2;
		if (i_44_ == 1) {
			i_54_ = i_50_ + aClass92_658.method851(-111, string);
			i_52_ += (-i_54_ + i_41_) / 2;
		} else if (i_44_ == 2) {
			i_54_ = aClass92_658.method851(-96, string) + i_50_;
			i_52_ += -i_54_ + i_41_;
		}
		a(class397s, null, i_52_, i ^ 0x6ed5, is_49_, is_37_, i_53_, string);
		if (is != null) {
			if (i_54_ == -1)
				i_54_ = i_50_ + aClass92_658.method851(-113, string);
			is[0] = i_52_;
			is[1] = -aClass92_658.anInt989 + i_53_;
			is[2] = i_54_;
			is[3] = aClass92_658.anInt992 + aClass92_658.anInt989;
		}
		return i_50_;
	}

	public static void a(byte i) {
		aClass202_654 = null;
		if (i >= -30)
			a(53, -6, 29, -15, 29, 118, -2);
		aString657 = null;
	}

	public void a(int i, int i_55_, int i_56_, int i_57_, String string, int i_58_) {
		if (string != null) {
			a(i_56_, 0, i);
			a(i_55_ - 1659, 0, null, i_57_, null, null, 0, i_58_, string);
			if (i_55_ != 1659)
				aClass92_658 = null;
		}
	}

	public abstract void fa(char c, int i, int i_59_, int i_60_, boolean bool);

	public void a(int i, int i_61_, int i_62_, int[] is, String string, int i_63_, int i_64_, Sprite[] class397s) {
		if (string != null) {
			a(i, 0, i_63_);
			a(i_61_ ^ i_61_, 0, class397s, i_62_, is, null, 0, i_64_, string);
		}
	}

	public void a(int i, int i_65_, int i_66_, int i_67_, int i_68_, String string) {
		if (i < 126)
			aClass92_658 = null;
		if (string != null) {
			a(i_67_, 0, i_68_);
			a(0, 0, null, i_66_ - aClass92_658.method851(-102, string), null, null, 0, i_65_, string);
		}
	}

	final int a(int i, int i_69_, int i_70_, int i_71_, int i_72_, int i_73_, String string, int i_74_, int[] is,
			int i_75_, int i_76_, aa var_aa, Sprite[] class397s, int i_77_, int i_78_, int i_79_, int i_80_) {
		if (string == null)
			return 0;
		a(i_72_, 0, i_78_);
		if (i_69_ == 0)
			i_69_ = aClass92_658.anInt990;
		int[] is_81_;
		if (i_69_ + aClass92_658.anInt989 + aClass92_658.anInt992 <= i_74_ || i_69_ + i_69_ <= i_74_)
			is_81_ = new int[] { i_77_ };
		else
			is_81_ = null;
		if (i_75_ == -1) {
			i_75_ = i_74_ / i_69_;
			if (i_75_ <= 0)
				i_75_ = 1;
		}
		int i_82_ = aClass92_658.method846(class397s, ModeWhat.aStringArray1191, (byte) -75, string, is_81_);
		if (i_75_ > 0 && i_75_ <= i_82_) {
			i_82_ = i_75_;
			ModeWhat.aStringArray1191[i_75_ - 1] = aClass92_658.method853(i_77_, class397s,
					ModeWhat.aStringArray1191[i_75_ - 1], 110);
		}
		if (i_71_ == 3 && i_82_ == 1)
			i_71_ = 1;
		int i_83_;
		if (i_71_ != 0) {
			if (i_71_ == 1)
				i_83_ = (-(i_69_ * (i_82_ - 1)) + (-aClass92_658.anInt992 - aClass92_658.anInt989) + i_74_) / 2 + i_79_
						+ aClass92_658.anInt989;
			else if (i_71_ != 2) {
				int i_84_ = ((i_74_ - aClass92_658.anInt989 + (-aClass92_658.anInt992 - (i_82_ - 1) * i_69_))
						/ (i_82_ + 1));
				if (i_84_ < 0)
					i_84_ = 0;
				i_69_ += i_84_;
				i_83_ = i_84_ + (i_79_ + aClass92_658.anInt989);
			} else
				i_83_ = -aClass92_658.anInt992 + i_79_ - (-i_74_ + (i_82_ - 1) * i_69_);
		} else
			i_83_ = i_79_ + aClass92_658.anInt989;
		if (i_80_ > -126)
			a((int) -7);
		for (int i_85_ = 0; i_85_ < i_82_; i_85_++) {
			if (i_70_ == 0)
				a(0, i_73_, class397s, i_76_, is, var_aa, i, i_83_, ModeWhat.aStringArray1191[i_85_]);
			else if (i_70_ != 1) {
				if (i_70_ == 2)
					a(0, i_73_, class397s,
							(-aClass92_658.method851(-93, ModeWhat.aStringArray1191[i_85_]) + (i_77_ + i_76_)), is,
							var_aa, i, i_83_, ModeWhat.aStringArray1191[i_85_]);
				else if (i_85_ == i_82_ - 1)
					a(0, i_73_, class397s, i_76_, is, var_aa, i, i_83_, ModeWhat.aStringArray1191[i_85_]);
				else {
					a(-124, ModeWhat.aStringArray1191[i_85_], i_77_);
					a(0, i_73_, class397s, i_76_, is, var_aa, i, i_83_, ModeWhat.aStringArray1191[i_85_]);
					Class296_Sub51_Sub39.anInt6551 = 0;
				}
			} else
				a(0, i_73_, class397s,
						(i_76_ + (-aClass92_658.method851(-73, ModeWhat.aStringArray1191[i_85_]) + i_77_) / 2), is,
						var_aa, i, i_83_, ModeWhat.aStringArray1191[i_85_]);
			i_83_ += i_69_;
		}
		return i_82_;
	}

	private final void a(boolean bool, String string) {
		try {
			if (!string.startsWith("col=")) {
				if (string.equals("/col"))
					Class209.anInt2093 = (Class296_Sub51_Sub12.anInt6411 & 0xffffff | Class209.anInt2093 & ~0xffffff);
			} else
				Class209.anInt2093 = (Class209.anInt2093 & ~0xffffff
						| (Class32.method343(-1, 16, string.substring(4)) & 0xffffff));
			if (string.startsWith("argb="))
				Class209.anInt2093 = Class32.method343(-1, 16, string.substring(5));
			else if (!string.equals("/argb")) {
				if (!string.startsWith("str=")) {
					if (string.equals("str"))
						Class393.anInt3302 = Class209.anInt2093 & ~0xffffff | 0x800000;
					else if (string.equals("/str"))
						Class393.anInt3302 = -1;
					else if (string.startsWith("u="))
						ClotchesLoader.anInt277 = (Class209.anInt2093 & ~0xffffff
								| Class32.method343(-1, 16, string.substring(2)));
					else if (!string.equals("u")) {
						if (string.equals("/u"))
							ClotchesLoader.anInt277 = -1;
						else if (string.equalsIgnoreCase("shad=-1"))
							Class69_Sub1.anInt5713 = 0;
						else if (!string.startsWith("shad=")) {
							if (string.equals("shad"))
								Class69_Sub1.anInt5713 = Class209.anInt2093 & ~0xffffff;
							else if (!string.equals("/shad")) {
								if (string.equals("br"))
									a(Class296_Sub51_Sub12.anInt6411, 0, Class296_Sub39_Sub1.anInt6122);
							} else
								Class69_Sub1.anInt5713 = Class296_Sub39_Sub1.anInt6122;
						} else
							Class69_Sub1.anInt5713 = (Class209.anInt2093 & ~0xffffff
									| Class32.method343(-1, 16, string.substring(5)));
					} else
						ClotchesLoader.anInt277 = Class209.anInt2093 & ~0xffffff;
				} else
					Class393.anInt3302 = (Class209.anInt2093 & ~0xffffff
							| Class32.method343(-1, 16, string.substring(4)));
			} else
				Class209.anInt2093 = Class296_Sub51_Sub12.anInt6411;
		} catch (Exception exception) {
			/* empty */
		}
		if (bool)
			a((byte) 30, null, 45, null, 36, -78, 49, 117, null, 33, 51, -97, 101, -87, null, 107);
	}

	private final void a(int i, String string, int i_86_) {
		int i_87_ = 0;
		boolean bool = false;
		for (int i_88_ = 0; i_88_ < string.length(); i_88_++) {
			char c = string.charAt(i_88_);
			if (c != '<') {
				if (c != '>') {
					if (!bool && c == ' ')
						i_87_++;
				} else
					bool = false;
			} else
				bool = true;
		}
		if (i_87_ > 0)
			Class296_Sub51_Sub39.anInt6551 = (-aClass92_658.method851(-92, string) + i_86_ << 8) / i_87_;
		int i_89_ = -118 / ((-61 - i) / 59);
	}

	public 	abstract void a(char c, int i, int i_90_, int i_91_, boolean bool, aa var_aa, int i_92_, int i_93_);

	private final void a(int i, int i_94_, int i_95_) {
		ClotchesLoader.anInt277 = -1;
		Class393.anInt3302 = -1;
		Class209.anInt2093 = Class296_Sub51_Sub12.anInt6411 = i;
		Class296_Sub51_Sub11.anInt6402 = i_94_;
		Class296_Sub51_Sub39.anInt6551 = 0;
		if (i_95_ == -1)
			i_95_ = 0;
		Class69_Sub1.anInt5713 = Class296_Sub39_Sub1.anInt6122 = i_95_;
	}

	static final void a(int i) {
		Class249.aClass113_2355.clear();
		if (i != 8364)
			anApplet_Sub1_656 = null;
	}

	private final void a(int i, int i_96_, Sprite[] class397s, int i_97_, int[] is, aa var_aa, int i_98_, int i_99_,
			String string) {
		i_99_ -= aClass92_658.anInt990;
		int i_100_ = -1;
		int i_101_ = -1;
		int i_102_ = string.length();
		for (int i_103_ = i; i_103_ < i_102_; i_103_++) {
			char c = (char) (Class296_Sub39_Sub9.method2827(string.charAt(i_103_), (byte) -88) & 0xff);
			if (c == 60)
				i_100_ = i_103_;
			else {
				if (c == 62 && i_100_ != -1) {
					String string_104_ = string.substring(i_100_ + 1, i_103_);
					i_100_ = -1;
					if (string_104_.equals("lt"))
						c = '<';
					else if (!string_104_.equals("gt")) {
						if (string_104_.equals("nbsp"))
							c = '\u00a0';
						else if (string_104_.equals("shy"))
							c = '\u00ad';
						else if (string_104_.equals("times"))
							c = '\u00d7';
						else if (!string_104_.equals("euro")) {
							if (!string_104_.equals("copy")) {
								if (!string_104_.equals("reg")) {
									if (!string_104_.startsWith("img="))
										a(false, string_104_);
									else {
										try {
											int i_105_ = (Class366_Sub2.method3774(-37, string_104_.substring(4)));
											Sprite class397 = class397s[i_105_];
											int i_106_ = (is == null ? class397.method4088() : is[i_105_]);
											if ((Class209.anInt2093 & ~0xffffff) != -16777216)
												class397.method4079(i_97_, (aClass92_658.anInt990 + (i_99_ - i_106_)),
														0, (Class209.anInt2093 & ~0xffffff) | 0xffffff, 1);
											else
												class397.method4079(i_97_, (aClass92_658.anInt990 + (i_99_ - i_106_)),
														1, 0, 1);
											i_97_ += class397s[i_105_].method4099();
											i_101_ = -1;
										} catch (Exception exception) {
											/* empty */
										}
									}
									continue;
								}
								c = '\u00ae';
							} else
								c = '\u00a9';
						} else
							c = '\u20ac';
					} else
						c = '>';
				}
				if (i_100_ == -1) {
					if (i_101_ != -1)
						i_97_ += aClass92_658.method844(c, i_101_, 91);
					if (c == 32) {
						if (Class296_Sub51_Sub39.anInt6551 > 0) {
							Class296_Sub51_Sub11.anInt6402 += Class296_Sub51_Sub39.anInt6551;
							i_97_ += Class296_Sub51_Sub11.anInt6402 >> 8;
							Class296_Sub51_Sub11.anInt6402 &= 0xff;
						}
					} else if (var_aa != null) {
						if ((Class69_Sub1.anInt5713 & ~0xffffff) != 0)
							a(c, i_97_ + 1, i_99_ + 1, Class69_Sub1.anInt5713, true, var_aa, i_96_, i_98_);
						a(c, i_97_, i_99_, Class209.anInt2093, false, var_aa, i_96_, i_98_);
					} else {
						if ((Class69_Sub1.anInt5713 & ~0xffffff) != 0)
							fa(c, i_97_ + 1, i_99_ + 1, Class69_Sub1.anInt5713, true);
						fa(c, i_97_, i_99_, Class209.anInt2093, false);
					}
					int i_107_ = aClass92_658.method842(true, c);
					if (Class393.anInt3302 != -1)
						aHa655.method1089(i_107_, Class393.anInt3302,
								(int) ((double) aClass92_658.anInt990 * 0.7) + i_99_, i_97_, -125);
					if (ClotchesLoader.anInt277 != -1)
						aHa655.method1089(i_107_, ClotchesLoader.anInt277, aClass92_658.anInt990 + (i_99_ + 1), i_97_,
								-105);
					i_97_ += i_107_;
					i_101_ = c;
				}
			}
		}
	}

	private final void a(Sprite[] class397s, int[] is, int i, int i_108_, int[] is_109_, int[] is_110_, int i_111_,
			String string) {
		i_111_ -= aClass92_658.anInt990;
		int i_112_ = -1;
		if (i_108_ >= -125)
			a(-41, 91, -48, 53, 119, -101, null, 70, null, 86, -39, null, null, 20, -74, 38, 112);
		int i_113_ = -1;
		int i_114_ = 0;
		int i_115_ = string.length();
		for (int i_116_ = 0; i_115_ > i_116_; i_116_++) {
			char c = (char) (Class296_Sub39_Sub9.method2827(string.charAt(i_116_), (byte) -76) & 0xff);
			if (c == 60)
				i_112_ = i_116_;
			else {
				if (c == 62 && i_112_ != -1) {
					String string_117_ = string.substring(i_112_ + 1, i_116_);
					i_112_ = -1;
					if (!string_117_.equals("lt")) {
						if (string_117_.equals("gt"))
							c = '>';
						else if (!string_117_.equals("nbsp")) {
							if (string_117_.equals("shy"))
								c = '\u00ad';
							else if (!string_117_.equals("times")) {
								if (string_117_.equals("euro"))
									c = '\u20ac';
								else if (!string_117_.equals("copy")) {
									if (!string_117_.equals("reg")) {
										if (string_117_.startsWith("img=")) {
											try {
												int i_118_;
												if (is_109_ != null)
													i_118_ = is_109_[i_114_];
												else
													i_118_ = 0;
												int i_119_;
												if (is == null)
													i_119_ = 0;
												else
													i_119_ = is[i_114_];
												i_114_++;
												int i_120_ = (Class366_Sub2.method3774(-40, string_117_.substring(4)));
												Sprite class397 = class397s[i_120_];
												int i_121_ = (is_110_ != null ? is_110_[i_120_]
														: class397.method4088());
												class397.method4079(i_118_ + i,
														(-i_121_ + aClass92_658.anInt990 + (i_111_ + i_119_)), 1, 0, 1);
												i += class397s[i_120_].method4099();
												i_113_ = -1;
											} catch (Exception exception) {
												/* empty */
											}
										} else
											a(false, string_117_);
										continue;
									}
									c = '\u00ae';
								} else
									c = '\u00a9';
							} else
								c = '\u00d7';
						} else
							c = '\u00a0';
					} else
						c = '<';
				}
				if (i_112_ == -1) {
					if (i_113_ != -1)
						i += aClass92_658.method844(c, i_113_, 70);
					int i_122_;
					if (is_109_ != null)
						i_122_ = is_109_[i_114_];
					else
						i_122_ = 0;
					int i_123_;
					if (is == null)
						i_123_ = 0;
					else
						i_123_ = is[i_114_];
					i_114_++;
					if (c == 32) {
						if (Class296_Sub51_Sub39.anInt6551 > 0) {
							Class296_Sub51_Sub11.anInt6402 += Class296_Sub51_Sub39.anInt6551;
							i += Class296_Sub51_Sub11.anInt6402 >> 8;
							Class296_Sub51_Sub11.anInt6402 &= 0xff;
						}
					} else {
						if ((Class69_Sub1.anInt5713 & ~0xffffff) != 0)
							fa(c, i + i_122_ + 1, i_111_ + i_123_ + 1, Class69_Sub1.anInt5713, true);
						fa(c, i + i_122_, i_111_ + i_123_, Class209.anInt2093, false);
					}
					int i_124_ = aClass92_658.method842(true, c);
					if (Class393.anInt3302 != -1)
						aHa655.method1089(i_124_, Class393.anInt3302,
								(i_111_ + (int) ((double) (aClass92_658.anInt990) * 0.7)), i, -96);
					if (ClotchesLoader.anInt277 != -1)
						aHa655.method1089(i_124_, ClotchesLoader.anInt277, i_111_ + aClass92_658.anInt990, i, -106);
					i_113_ = c;
					i += i_124_;
				}
			}
		}
	}

	public Class55(ha var_ha, Class92 class92) {
		aHa655 = var_ha;
		aClass92_658 = class92;
	}

	public void a(int i, int i_125_, int i_126_, String string, byte i_127_, int i_128_, int i_129_, int i_130_) {
		if (string != null) {
			a(i_129_, 0, i_130_);
			if (i_127_ >= 0)
				a((byte) -114, null, 88, null, -41, -2, 117, 87, null, 92, 40, -17, 67, 52, null, 4);
			double d = -((double) i_128_ / 8.0) + 7.0;
			if (d < 0.0)
				d = 0.0;
			int i_131_ = string.length();
			int[] is = new int[i_131_];
			for (int i_132_ = 0; i_132_ < i_131_; i_132_++)
				is[i_132_] = (int) (Math.sin((double) i_132_ / 1.5 + (double) i_126_) * d);
			a(null, is, i - aClass92_658.method851(-71, string) / 2, -128, null, null, i_125_, string);
		}
	}

	public int a(byte i, Sprite[] class397s, int i_133_, aa var_aa, int i_134_, int i_135_, int i_136_, int i_137_,
			String string, int i_138_, int i_139_, int i_140_, int i_141_, int i_142_, int[] is, int i_143_) {
		int i_144_ = 95 / ((29 - i) / 60);
		return a(i_142_, i_143_, i_138_, i_133_, i_139_, i_141_, string, i_140_, is, 0, i_137_, var_aa, class397s,
				i_135_, i_134_, i_136_, -128);
	}

	static {
		aClass202_654 = new Class202(1);
	}
}
