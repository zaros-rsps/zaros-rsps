package net.zaros.client;

/* Class381_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;

final class Class381_Sub2 extends Class381 {
	private int anInt5622;
	private boolean aBoolean5623 = false;
	private SourceDataLine aSourceDataLine5624;
	private AudioFormat anAudioFormat5625;
	private byte[] aByteArray5626;

	final void method4003(Component component) {
		Mixer.Info[] infos = AudioSystem.getMixerInfo();
		if (infos != null) {
			Mixer.Info[] infos_0_ = infos;
			for (int i = 0; infos_0_.length > i; i++) {
				Mixer.Info info = infos_0_[i];
				if (info != null) {
					String string = info.getName();
					if (string != null && string.toLowerCase().indexOf("soundmax") >= 0)
						aBoolean5623 = true;
				}
			}
		}
		anAudioFormat5625 = new AudioFormat((float) Class298.anInt2699, 16, HardReferenceWrapper.aBoolean6695 ? 2 : 1, true, false);
		aByteArray5626 = new byte[256 << (!HardReferenceWrapper.aBoolean6695 ? 1 : 2)];
	}

	final void method3997() throws LineUnavailableException {
		aSourceDataLine5624.flush();
		if (aBoolean5623) {
			aSourceDataLine5624.close();
			aSourceDataLine5624 = null;
			DataLine.Info info = (new DataLine.Info(SourceDataLine.class, anAudioFormat5625, anInt5622 << (!HardReferenceWrapper.aBoolean6695 ? 1 : 2)));
			aSourceDataLine5624 = (SourceDataLine) AudioSystem.getLine(info);
			aSourceDataLine5624.open();
			aSourceDataLine5624.start();
		}
	}

	final void method4000() {
		int i = 256;
		if (HardReferenceWrapper.aBoolean6695)
			i <<= 1;
		for (int i_1_ = 0; i_1_ < i; i_1_++) {
			int i_2_ = anIntArray3222[i_1_];
			if ((i_2_ + 8388608 & ~0xffffff) != 0)
				i_2_ = 0x7fffff ^ i_2_ >> 31;
			aByteArray5626[i_1_ * 2] = (byte) (i_2_ >> 8);
			aByteArray5626[i_1_ * 2 + 1] = (byte) (i_2_ >> 16);
		}
		aSourceDataLine5624.write(aByteArray5626, 0, i << 1);
	}

	final int method3995() {
		return (anInt5622 - (aSourceDataLine5624.available() >> (HardReferenceWrapper.aBoolean6695 ? 2 : 1)));
	}

	final void method4001() {
		if (aSourceDataLine5624 != null) {
			aSourceDataLine5624.close();
			aSourceDataLine5624 = null;
		}
	}

	final void method3999(int i) throws LineUnavailableException {
		try {
			DataLine.Info info = (new DataLine.Info(SourceDataLine.class, anAudioFormat5625, i << (HardReferenceWrapper.aBoolean6695 ? 2 : 1)));
			aSourceDataLine5624 = (SourceDataLine) AudioSystem.getLine(info);
			aSourceDataLine5624.open();
			aSourceDataLine5624.start();
			anInt5622 = i;
		} catch (LineUnavailableException lineunavailableexception) {
			if (Class403.method4155(i, -111) != 1)
				method3999(Class8.get_next_high_pow2(i));
			else {
				aSourceDataLine5624 = null;
				throw lineunavailableexception;
			}
		}
	}
}
