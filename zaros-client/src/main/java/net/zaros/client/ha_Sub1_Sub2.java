package net.zaros.client;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Rectangle;

import jaclib.peer.vba;

import jagdx.D3DADAPTER_IDENTIFIER;
import jagdx.D3DCAPS;
import jagdx.D3DDISPLAYMODE;
import jagdx.D3DLIGHT;
import jagdx.D3DPRESENT_PARAMETERS;
import jagdx.GeometryBuffer;
import jagdx.IDirect3D;
import jagdx.IDirect3DDevice;
import jagdx.IDirect3DEventQuery;
import jagdx.IDirect3DPixelShader;
import jagdx.IDirect3DSurface;
import jagdx.IDirect3DVertexShader;
import jagdx.PixelBuffer;
import jagdx.aj;
import jagdx.tn;

public class ha_Sub1_Sub2 extends ha_Sub1 {
	private static float[] aFloatArray5855;
	private int[] anIntArray5856;
	private static int[] anIntArray5857;
	D3DCAPS aD3DCAPS5858;
	private boolean[] aBooleanArray5859;
	private D3DLIGHT aD3DLIGHT5860;
	private IDirect3DVertexShader anIDirect3DVertexShader5861;
	private static int[] anIntArray5862 = { 77, 80 };
	GeometryBuffer aGeometryBuffer5863;
	private D3DLIGHT aD3DLIGHT5864;
	IDirect3DDevice anIDirect3DDevice5865;
	private boolean aBoolean5866;
	PixelBuffer aPixelBuffer5867;
	private int anInt5868 = 0;
	boolean aBoolean5869;
	vba aVba5870;
	private IDirect3D anIDirect3D5871;
	private boolean[] aBooleanArray5872;
	private int anInt5873;
	private D3DPRESENT_PARAMETERS aD3DPRESENT_PARAMETERS5874;
	private Class102 aClass102_5875;
	private D3DLIGHT aD3DLIGHT5876;
	private boolean[] aBooleanArray5877;
	private Class131[] aClass131Array5878;
	boolean aBoolean5879;
	boolean aBoolean5880;
	private int anInt5881;
	private boolean[] aBooleanArray5882;

	public void a(float f, float f_0_, float f_1_) {
		/* empty */
	}

	public float method1144(int i) {
		if (i != 1)
			anInt5868 = 34;
		return -0.5F;
	}

	public void method1152(int i) {
		if (i >= 106) {
			if (!aBoolean3940)
				aClass373_Sub2_3939.method3932(0, aFloatArray5855);
			else {
				aFloatArray5855[8] = 0.0F;
				aFloatArray5855[12] = 0.0F;
				aFloatArray5855[1] = 0.0F;
				aFloatArray5855[0] = 1.0F;
				aFloatArray5855[13] = 0.0F;
				aFloatArray5855[9] = 0.0F;
				aFloatArray5855[15] = 1.0F;
				aFloatArray5855[6] = 0.0F;
				aFloatArray5855[11] = 0.0F;
				aFloatArray5855[10] = 1.0F;
				aFloatArray5855[5] = 1.0F;
				aFloatArray5855[3] = 0.0F;
				aFloatArray5855[2] = 0.0F;
				aFloatArray5855[4] = 0.0F;
				aFloatArray5855[7] = 0.0F;
				aFloatArray5855[14] = 0.0F;
			}
			anIDirect3DDevice5865.SetTransform(256, aFloatArray5855);
		}
	}

	public void method1211(byte i) {
		anIDirect3DDevice5865.a(137, aBoolean3973 && !aBoolean4032);
		if (i != 85)
			method1162(false);
	}

	public Interface6_Impl1 method1107(int i, int i_2_, int i_3_, Class67 class67, Class202 class202) {
		if (i_2_ >= -58)
			aD3DPRESENT_PARAMETERS5874 = null;
		return new Class64_Sub3(this, class202, class67, i_3_, i);
	}

	public void method1148(int i) {
		if (i >= 4)
			anIDirect3DDevice5865.a(28, aBoolean3952 && aBoolean3963 && anInt3990 >= 0);
	}

	public static ha createToolkit(Canvas canvas, d var_d, Js5 class138, Integer integer) {
		ha_Sub1_Sub2 var_ha_Sub1_Sub2 = null;
		ha_Sub1_Sub2 var_ha_Sub1_Sub2_4_;
		try {
			int i = 0;
			int i_5_ = 1;
			vba var_vba = new vba();
			IDirect3D idirect3d = IDirect3D.a(-2147483616, var_vba);
			D3DCAPS d3dcaps = idirect3d.a(i, i_5_);
			if ((d3dcaps.RasterCaps & 0x1000000) == 0)
				throw new RuntimeException("");
			if (d3dcaps.MaxSimultaneousTextures < 2)
				throw new RuntimeException("");
			if ((d3dcaps.TextureOpCaps & 0x2) == 0)
				throw new RuntimeException("");
			if ((d3dcaps.TextureOpCaps & 0x8) == 0)
				throw new RuntimeException("");
			if ((d3dcaps.TextureOpCaps & 0x40) == 0)
				throw new RuntimeException("");
			if ((d3dcaps.TextureOpCaps & 0x200) == 0)
				throw new RuntimeException("");
			if ((d3dcaps.TextureOpCaps & 0x2000000) == 0)
				throw new RuntimeException("");
			if ((d3dcaps.DestBlendCaps & d3dcaps.SrcBlendCaps & 0x10) == 0)
				throw new RuntimeException("");
			if ((d3dcaps.SrcBlendCaps & d3dcaps.DestBlendCaps & 0x20) == 0)
				throw new RuntimeException("");
			if ((d3dcaps.SrcBlendCaps & d3dcaps.DestBlendCaps & 0x2) == 0)
				throw new RuntimeException("");
			if (d3dcaps.MaxActiveLights > 0 && d3dcaps.MaxActiveLights < 2)
				throw new RuntimeException("");
			if (d3dcaps.MaxStreams < 5)
				throw new RuntimeException("");
			D3DPRESENT_PARAMETERS d3dpresent_parameters = new D3DPRESENT_PARAMETERS(canvas);
			if (!method1243(integer.intValue(), false, idirect3d, d3dpresent_parameters, i, i_5_))
				throw new RuntimeException("");
			d3dpresent_parameters.Windowed = true;
			d3dpresent_parameters.EnableAutoDepthStencil = true;
			d3dpresent_parameters.PresentationInterval = -2147483648;
			int i_6_ = 2;
			if ((d3dcaps.DevCaps & 0x100000) != 0)
				i_6_ |= 0x10;
			Object object = null;
			IDirect3DDevice idirect3ddevice;
			try {
				idirect3ddevice = idirect3d.a(i, i_5_, canvas, i_6_ | 0x40, d3dpresent_parameters);
			} catch (tn var_tn) {
				idirect3ddevice = idirect3d.a(i, i_5_, canvas, i_6_ | 0x20, d3dpresent_parameters);
			}
			Class102 class102 = new Class102(idirect3ddevice.b((int) 0), idirect3ddevice.b());
			var_ha_Sub1_Sub2 = new ha_Sub1_Sub2(i, i_5_, canvas, var_vba, idirect3d, idirect3ddevice, class102,
					d3dpresent_parameters, d3dcaps, var_d, class138, integer.intValue());
			var_ha_Sub1_Sub2.method1193(-16649);
			var_ha_Sub1_Sub2_4_ = var_ha_Sub1_Sub2;
		} catch (RuntimeException runtimeexception) {
			if (var_ha_Sub1_Sub2 != null)
				var_ha_Sub1_Sub2.t();
			throw runtimeexception;
		}
		return var_ha_Sub1_Sub2_4_;
	}

	public void method1241(byte i, IDirect3DPixelShader idirect3dpixelshader) {
		anIDirect3DDevice5865.SetPixelShader(idirect3dpixelshader);
		if (i != 79) {
			/* empty */
		}
	}

	public void method1242(Class64_Sub1 class64_sub1, byte i) {
		method1249(class64_sub1, (byte) -127);
		if (!aBooleanArray5877[anInt3991]) {
			anIDirect3DDevice5865.SetSamplerState(anInt3991, 1, 1);
			aBooleanArray5877[anInt3991] = true;
		}
		int i_7_ = -68 % ((-41 - i) / 46);
		if (!aBooleanArray5882[anInt3991]) {
			anIDirect3DDevice5865.SetSamplerState(anInt3991, 2, 1);
			aBooleanArray5882[anInt3991] = true;
		}
	}

	public void method1186(int i) {
		if (i == 0)
			anIDirect3DDevice5865.a(14, aBoolean3946 && aBoolean3997);
	}

	public void A() {
		IDirect3DEventQuery idirect3deventquery = anIDirect3DDevice5865.c();
		if (aj.a(idirect3deventquery.Issue(), (int) 65)) {
			for (;;) {
				int i = idirect3deventquery.IsSignaled();
				if (i != 1)
					break;
				Thread.yield();
			}
		}
		idirect3deventquery.b((byte) 83);
	}

	public void method1149(int i) {
		if (i == 25154)
			anIDirect3DDevice5865.a(174, aBoolean4062);
	}

	public Interface6_Impl1 method1187(int i, byte i_8_, int[] is, int i_9_, int i_10_, int i_11_, boolean bool) {
		if (i_8_ > -121)
			return null;
		return new Class64_Sub3(this, i_11_, i, bool, is, i_10_, i_9_);
	}

	public void method1150(boolean bool) {
		int i = (!aBooleanArray5872[anInt3991] ? 1 : method1244(10, aClass125Array4003[anInt3991]));
		anIDirect3DDevice5865.SetTextureStageState(anInt3991, 1, i);
		if (bool != true)
			aClass131Array5878 = null;
	}

	private ha_Sub1_Sub2(int i, int i_12_, Canvas canvas, vba var_vba, IDirect3D idirect3d,
			IDirect3DDevice idirect3ddevice, Class102 class102, D3DPRESENT_PARAMETERS d3dpresent_parameters,
			D3DCAPS d3dcaps, d var_d, Js5 class138, int i_13_) {
		super(canvas, class102, var_d, class138, i_13_, 0);
		aBoolean5866 = false;
		try {
			aVba5870 = var_vba;
			aD3DPRESENT_PARAMETERS5874 = d3dpresent_parameters;
			anIDirect3D5871 = idirect3d;
			anInt5881 = i_12_;
			anIDirect3DDevice5865 = idirect3ddevice;
			aD3DCAPS5858 = d3dcaps;
			aClass102_5875 = class102;
			anInt5873 = i;
			aD3DLIGHT5864 = new D3DLIGHT(aVba5870);
			aD3DLIGHT5860 = new D3DLIGHT(aVba5870);
			aD3DLIGHT5876 = new D3DLIGHT(aVba5870);
			aPixelBuffer5867 = new PixelBuffer(aVba5870);
			aGeometryBuffer5863 = new GeometryBuffer(aVba5870);
			new GeometryBuffer(aVba5870);
			anInt4031 = aD3DCAPS5858.MaxSimultaneousTextures;
			aBoolean5869 = (aD3DCAPS5858.TextureCaps & 0x4000) != 0;
			anInt3950 = (aD3DCAPS5858.MaxActiveLights <= 0 ? 8 : aD3DCAPS5858.MaxActiveLights);
			aBoolean3968 = (aD3DCAPS5858.TextureCaps & 0x2000) != 0;
			aBoolean5879 = (aD3DCAPS5858.TextureCaps & 0x2) == 0;
			aBoolean3977 = (aD3DCAPS5858.TextureCaps & 0x800) != 0;
			aBoolean5880 = (aD3DCAPS5858.TextureCaps & 0x10000) != 0;
			aBoolean3974 = (anInt3978 > 0 || (anIDirect3D5871.CheckDeviceMultiSampleType(anInt5873, anInt5881,
					aD3DPRESENT_PARAMETERS5874.BackBufferFormat, true, 2)) == 0);
			anIntArray5856 = new int[anInt4031];
			aBooleanArray5882 = new boolean[anInt4031];
			aBooleanArray5877 = new boolean[anInt4031];
			aClass131Array5878 = new Class131[anInt4031];
			aBooleanArray5859 = new boolean[anInt4031];
			aBooleanArray5872 = new boolean[anInt4031];
			anIDirect3DDevice5865.BeginScene();
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			method1091((byte) -115);
			throw new RuntimeException("");
		}
	}

	public Interface15_Impl1 method1154(boolean bool, int i) {
		int i_14_ = -4 / ((i + 30) / 37);
		return new Class374(this, Class67.aClass67_746, bool);
	}

	public void F(int i, int i_15_) {
		/* empty */
	}

	private static boolean method1243(int i, boolean bool, IDirect3D idirect3d,
			D3DPRESENT_PARAMETERS d3dpresent_parameters, int i_16_, int i_17_) {
		int i_18_ = 0;
		int i_19_ = 0;
		int i_20_ = 0;
		boolean bool_21_;
		try {
			D3DDISPLAYMODE d3ddisplaymode = new D3DDISPLAYMODE();
			if (aj.a(idirect3d.a(i_16_, d3ddisplaymode), (byte) -91))
				return false;
			while_109_: for (/**/; i >= 0; i--) {
				if (i != 1) {
					i_20_ = i + 0;
					for (int i_22_ = 0; anIntArray5857.length > i_22_; i_22_++) {
						if (idirect3d.CheckDeviceType(i_16_, i_17_, d3ddisplaymode.Format, anIntArray5857[i_22_],
								true) == 0
								&& idirect3d.CheckDeviceFormat(i_16_, i_17_, (d3ddisplaymode.Format), 1, 1,
										(anIntArray5857[i_22_])) == 0
								&& (i == 0 || (idirect3d.CheckDeviceMultiSampleType(i_16_, i_17_, anIntArray5857[i_22_],
										true, i_20_)) == 0)) {
							for (int i_23_ = 0; anIntArray5862.length > i_23_; i_23_++) {
								if (idirect3d.CheckDeviceFormat(i_16_, i_17_, (d3ddisplaymode.Format), 2, 1,
										(anIntArray5862[i_23_])) == 0
										&& (idirect3d.CheckDepthStencilMatch(i_16_, i_17_, d3ddisplaymode.Format,
												anIntArray5857[i_22_], anIntArray5862[i_23_])) == 0
										&& (i == 0 || (idirect3d.CheckDeviceMultiSampleType(i_16_, i_17_,
												anIntArray5862[i_22_], true, i_20_)) == 0)) {
									i_18_ = anIntArray5862[i_23_];
									i_19_ = anIntArray5857[i_22_];
									break while_109_;
								}
							}
						}
					}
				}
			}
			if (i < 0 || i_19_ == 0 || i_18_ == 0)
				return false;
			if (bool)
				method1252(null, (byte) -105);
			d3dpresent_parameters.MultiSampleType = i_20_;
			d3dpresent_parameters.MultiSampleQuality = 0;
			d3dpresent_parameters.BackBufferFormat = i_19_;
			d3dpresent_parameters.AutoDepthStencilFormat = i_18_;
			bool_21_ = true;
		} catch (Throwable throwable) {
			return false;
		}
		return bool_21_;
	}

	public void w() {
		/* empty */
	}

	private static int method1244(int i, Class125 class125) {
		if (class125 != Class41_Sub4.aClass125_3745) {
			if (Js5.aClass125_1411 != class125) {
				if (Class280.aClass125_2589 != class125) {
					if (Class41_Sub1.aClass125_3735 == class125)
						return 7;
					if (class125 == Class390.aClass125_3283)
						return 10;
				} else
					return 26;
			} else
				return 4;
		} else
			return 2;
		if (i != 10)
			return 21;
		throw new IllegalArgumentException();
	}

	public Interface6_Impl2 method1159(boolean bool, int i, Class202 class202, int i_24_, int i_25_, byte[] is) {
		if (bool != true)
			return null;
		return new Class64_Sub1(this, class202, i_25_, i, i_24_, is);
	}

	public void method1227(int i) {
		if (i == -1) {
			if (anIDirect3DVertexShader5861 != null || Class67.aClass230_754 == aClass230Array3966[anInt3991]) {
				anIDirect3DDevice5865.SetTextureStageState(anInt3991, 24, 0);
				anIntArray5856[anInt3991] = 0;
			} else {
				if (aClass230Array3966[anInt3991] == Class360_Sub5.aClass230_5325)
					anIDirect3DDevice5865.SetTransform(anInt3991 + 16,
							aClass373_Sub2Array3965[anInt3991].method3939(aFloatArray5855, 14));
				else
					anIDirect3DDevice5865.SetTransform(anInt3991 + 16,
							aClass373_Sub2Array3965[anInt3991].method3932(0, aFloatArray5855));
				int i_26_ = method1252(aClass230Array3966[anInt3991], (byte) 112);
				if (anIntArray5856[anInt3991] != i_26_) {
					anIDirect3DDevice5865.SetTextureStageState(anInt3991, 24, i_26_);
					anIntArray5856[anInt3991] = i_26_;
				}
			}
		}
	}

	public void method1189(byte i) {
		if (i != 75)
			anIDirect3DVertexShader5861 = null;
		float f = !aBoolean4041 ? 0.0F : aFloat4022;
		float f_27_ = aBoolean4041 ? -aFloat4007 : 0.0F;
		aD3DLIGHT5864.SetDiffuse(aFloat4000 * f, f * aFloat4019, aFloat4043 * f, 0.0F);
		aD3DLIGHT5860.SetDiffuse(aFloat4000 * f_27_, f_27_ * aFloat4019, aFloat4043 * f_27_, 0.0F);
		aBoolean5866 = false;
	}

	public void method1232(Class289 class289, Interface15_Impl1 interface15_impl1, int i, int i_28_, int i_29_,
			int i_30_, int i_31_) {
		if (i_30_ >= 23) {
			anIDirect3DDevice5865.SetIndices(((Class374) interface15_impl1).anIDirect3DIndexBuffer5699);
			anIDirect3DDevice5865.DrawIndexedPrimitive(method1250((byte) 100, class289), 0, i_31_, i, i_29_, i_28_);
		}
	}

	public Interface6_Impl1 method1195(boolean bool, int i, byte[] is, Class202 class202, int i_32_, boolean bool_33_,
			int i_34_, int i_35_) {
		if (bool != true)
			method1200(null, null, (byte) 111);
		return new Class64_Sub3(this, class202, i, i_32_, bool_33_, is, i_35_, i_34_);
	}

	public void method1209(Class127 class127, byte i) {
		dxVertexLayout var_dxVertexLayout = (dxVertexLayout) class127;
		anIDirect3DDevice5865.SetVertexDeclaration(var_dxVertexLayout.anIDirect3DVertexDeclaration4281);
		if (i > -92)
			method1217(-113, null, null);
	}

	public boolean b() {
		return false;
	}

	private static int method1245(Class131 class131, boolean bool) {
		if (class131 == ISAACCipher.aClass131_1892)
			return 2;
		if (aa_Sub2.aClass131_3724 == class131)
			return 1;
		if (bool != true)
			return -63;
		throw new IllegalArgumentException();
	}

	public void c(int i, int i_36_) throws Exception_Sub1 {
		anIDirect3DDevice5865.EndScene();
		if (!aClass102_5875.method889(0)) {
			if (++anInt5868 > 50)
				throw new Exception_Sub1();
			method1247(0);
		} else {
			anInt5868 = 0;
			if (aj.a(aClass102_5875.method890(0, -15265), (byte) -91))
				method1247(0);
		}
		anIDirect3DDevice5865.BeginScene();
	}

	public void t() {
		aVba5870.c(-1946);
		super.t();
	}

	public Class360 method1119(int i, int i_37_) {
		if (i != 22940)
			return null;
		int i_38_ = i_37_;
		while_110_: do {
			do {
				if (i_38_ != 3) {
					if (i_38_ != 4) {
						if (i_38_ == 8)
							break;
						break while_110_;
					}
				} else
					return new Class360_Sub4(this, aClass138_3928);
				return new Class360_Sub7(this, aClass138_3928, aClass184_3929);
			} while (false);
			return new Class360_Sub1(this, aClass138_3928, aClass184_3929);
		} while (false);
		return super.method1119(22940, i_37_);
	}

	public void a(int i, int i_39_, int i_40_, int i_41_) {
		/* empty */
	}

	public void method1190(Object object, byte i, Canvas canvas) {
		if (i > -52)
			anIDirect3DVertexShader5861 = null;
		aClass102_5875 = (Class102) object;
	}

	public void b(int i, int i_42_, int i_43_, int i_44_, double d) {
		/* empty */
	}

	public boolean method1147(Class67 class67, Class202 class202, byte i) {
		D3DDISPLAYMODE d3ddisplaymode = new D3DDISPLAYMODE();
		if (i != -14)
			method1245(null, true);
		return (aj.a(anIDirect3D5871.a(anInt5873, d3ddisplaymode), (int) 47)
				&& aj.a((anIDirect3D5871.CheckDeviceFormat(anInt5873, anInt5881, d3ddisplaymode.Format, 0, 4,
						method1253(0, class67, class202))), (int) 81));
	}

	public void method1157(boolean bool, Class246 class246) {
		if (bool)
			aBooleanArray5872 = null;
		int i = 0;
		if (Class99.aClass246_1060 == class246)
			i = 65536;
		else if (Class96.aClass246_1048 != class246) {
			if (class246 == Class13.aClass246_3515)
				i = 196608;
		} else
			i = 131072;
		anIDirect3DDevice5865.SetTextureStageState(anInt3991, 11, i | anInt3991);
	}

	private static int method1246(int i, Class287 class287) {
		if (i != 1)
			return -54;
		if (class287 != Class199.aClass287_2007) {
			if (Class347.aClass287_3025 != class287) {
				if (Class153.aClass287_1578 == class287)
					return 1;
				if (class287 == Class151.aClass287_1553)
					return 3;
			} else
				return 0;
		} else
			return 2;
		throw new IllegalArgumentException();
	}

	public void method1224(boolean bool) {
		aFloat3964 = (float) (anInt3981 - anInt3960);
		if (bool == true) {
			aFloat3989 = (float) -anInt3990 + aFloat3964;
			if ((float) anInt4035 > aFloat3989)
				aFloat3989 = (float) anInt4035;
			anIDirect3DDevice5865.a(36, aFloat3989);
			anIDirect3DDevice5865.a(37, aFloat3964);
			anIDirect3DDevice5865.SetRenderState(34, anInt3947);
		}
	}

	public Class241 a(Class241 class241, Class241 class241_45_, float f, Class241 class241_46_) {
		return 0.5F > f ? class241 : class241_45_;
	}

	public Interface6_Impl1 method1160(Class202 class202, int i, boolean bool, int i_47_, float[] fs, int i_48_,
			int i_49_, int i_50_) {
		if (i_50_ != 28426)
			return null;
		return null;
	}

	public void method1204(boolean bool) {
		if (bool != true)
			method1227(-106);
		anIDirect3DDevice5865.SetViewport(anInt4011, anInt4042, anInt3921, anInt3920, 0.0F, 1.0F);
	}

	public Interface6_Impl3 method1104(boolean bool, int i, int i_51_, int[][] is) {
		if (i != 0)
			anIntArray5856 = null;
		return new Class64_Sub2(this, i_51_, bool, is);
	}

	public void method1162(boolean bool) {
		if (bool)
			aGeometryBuffer5863 = null;
		if (!aBoolean5866) {
			anIDirect3DDevice5865.LightEnable(0, false);
			anIDirect3DDevice5865.LightEnable(1, false);
			anIDirect3DDevice5865.SetLight(0, aD3DLIGHT5864);
			anIDirect3DDevice5865.SetLight(1, aD3DLIGHT5860);
			anIDirect3DDevice5865.LightEnable(0, true);
			anIDirect3DDevice5865.LightEnable(1, true);
			aBoolean5866 = true;
		}
	}

	public void ya() {
		method1106(-55, true);
		anIDirect3DDevice5865.Clear(2, 0, 1.0F, 0);
	}

	public void method1108(boolean bool) {
		anIDirect3DDevice5865.a(15, aBoolean3979);
		if (bool != true)
			aBoolean5879 = false;
	}

	public void method1228(int i) {
		if (i != -25685)
			c();
		anIDirect3DDevice5865.SetRenderState(60, anInt4033);
	}

	private boolean method1247(int i) {
		int i_52_ = anIDirect3DDevice5865.TestCooperativeLevel();
		if (i_52_ == i || i_52_ == -2005530519) {
			Class102 class102 = (Class102) anObject3927;
			method1199(697);
			class102.method892(0);
			aD3DPRESENT_PARAMETERS5874.BackBufferHeight = 0;
			aD3DPRESENT_PARAMETERS5874.BackBufferWidth = 0;
			if (method1243(anInt3978, false, anIDirect3D5871, aD3DPRESENT_PARAMETERS5874, anInt5873, anInt5881)) {
				int i_53_ = anIDirect3DDevice5865.Reset(aD3DPRESENT_PARAMETERS5874);
				if (aj.a(i_53_, (int) 101)) {
					class102.method891(anIDirect3DDevice5865.b((int) 0), true, anIDirect3DDevice5865.b());
					method1213((byte) 74);
					method1146((byte) 62);
					return true;
				}
			}
		}
		return false;
	}

	public void method1218(int i, int i_54_) {
		anIDirect3DDevice5865.SetTextureStageState(anInt3991, 11, i_54_);
		if (i != 11688)
			method1107(51, 39, -65, null, null);
	}

	public void method1134(int i) {
		if (aBooleanArray5872[anInt3991]) {
			aBooleanArray5872[anInt3991] = false;
			anIDirect3DDevice5865.SetTexture(anInt3991, null);
			method1165(2147483647);
			method1150(true);
		}
		if (i != 25993)
			aBoolean5869 = false;
	}

	public void a(boolean bool) {
		/* empty */
	}

	public void a(Interface13 interface13) {
		/* empty */
	}

	public void method1165(int i) {
		int i_55_ = (!aBooleanArray5872[anInt3991] ? 1 : method1244(i ^ 0x7ffffff5, aClass125Array3995[anInt3991]));
		if (i != 2147483647)
			method1210(115);
		anIDirect3DDevice5865.SetTextureStageState(anInt3991, 4, i_55_);
	}

	public void method1173(int i) {
		if (i != -3394)
			method1100(-64, null);
		method1189((byte) 75);
		method1162(false);
	}

	public Class127 method1233(byte i, Class211[] class211s) {
		if (i != -46)
			anIDirect3DDevice5865 = null;
		return new dxVertexLayout(this, class211s);
	}

	public void method1194(byte i) {
		anIDirect3DDevice5865.SetTransform(3, aFloatArray3949);
		if (i != -57)
			anIDirect3DDevice5865 = null;
	}

	public void h() {
		/* empty */
	}

	public void method1146(byte i) {
		for (int i_56_ = 0; anInt4031 > i_56_; i_56_++) {
			anIDirect3DDevice5865.SetSamplerState(i_56_, 7, 0);
			anIDirect3DDevice5865.SetSamplerState(i_56_, 6, 2);
			anIDirect3DDevice5865.SetSamplerState(i_56_, 5, 2);
			anIDirect3DDevice5865.SetSamplerState(i_56_, 1, 1);
			anIDirect3DDevice5865.SetSamplerState(i_56_, 2, 1);
			aClass131Array5878[i_56_] = ISAACCipher.aClass131_1892;
			aBooleanArray5877[i_56_] = aBooleanArray5882[i_56_] = true;
			aBooleanArray5859[i_56_] = false;
			anIntArray5856[i_56_] = 0;
		}
		anIDirect3DDevice5865.SetTextureStageState(0, 6, 1);
		anIDirect3DDevice5865.SetRenderState(9, 2);
		anIDirect3DDevice5865.SetRenderState(23, 4);
		anIDirect3DDevice5865.SetRenderState(25, 5);
		anIDirect3DDevice5865.SetRenderState(24, 0);
		anIDirect3DDevice5865.SetRenderState(22, 2);
		anIDirect3DDevice5865.SetRenderState(147, 1);
		anIDirect3DDevice5865.SetRenderState(145, 1);
		anIDirect3DDevice5865.a(38, 0.95F);
		anIDirect3DDevice5865.SetRenderState(140, 3);
		aD3DLIGHT5864.SetType(3);
		aD3DLIGHT5860.SetType(3);
		aD3DLIGHT5876.SetType(1);
		aBoolean5866 = false;
		super.method1146(i);
	}

	public void method1248(int i, IDirect3DVertexShader idirect3dvertexshader) {
		anIDirect3DVertexShader5861 = idirect3dvertexshader;
		anIDirect3DDevice5865.SetVertexShader(idirect3dvertexshader);
		method1227(-1);
		if (i != 1326468944)
			aBoolean5866 = false;
	}

	public void a(Rectangle[] rectangles, int i, int i_57_, int i_58_) throws Exception_Sub1 {
		c(i_57_, i_58_);
	}

	public void GA(int i) {
		anIDirect3DDevice5865.Clear(1, i, 0.0F, 0);
	}

	public void method1249(Class64 class64, byte i) {
		if (i > -126)
			method1162(true);
		anIDirect3DDevice5865.SetTexture(anInt3991, class64.method706(12931));
		if (aClass131Array5878[anInt3991] == class64.aClass131_728) {
			if (!class64.aBoolean732 != !aBooleanArray5859[anInt3991]) {
				anIDirect3DDevice5865.SetSamplerState(anInt3991, 7,
						(class64.aBoolean732 ? method1245(class64.aClass131_728, true) : 0));
				aBooleanArray5859[anInt3991] = class64.aBoolean732;
			}
		} else {
			int i_59_ = method1245(class64.aClass131_728, true);
			anIDirect3DDevice5865.SetSamplerState(anInt3991, 6, i_59_);
			anIDirect3DDevice5865.SetSamplerState(anInt3991, 5, i_59_);
			aClass131Array5878[anInt3991] = class64.aClass131_728;
			if (aBooleanArray5859[anInt3991] == !class64.aBoolean732) {
				anIDirect3DDevice5865.SetSamplerState(anInt3991, 7,
						(!class64.aBoolean732 ? 0 : method1245(class64.aClass131_728, true)));
				aBooleanArray5859[anInt3991] = class64.aBoolean732;
			}
		}
		if (!aBooleanArray5872[anInt3991]) {
			aBooleanArray5872[anInt3991] = true;
			method1165(2147483647);
			method1150(true);
		}
	}

	public boolean method1200(Class67 class67, Class202 class202, byte i) {
		int i_60_ = -120 / ((i + 54) / 42);
		D3DDISPLAYMODE d3ddisplaymode = new D3DDISPLAYMODE();
		return (aj.a(anIDirect3D5871.a(anInt5873, d3ddisplaymode), (int) 96)
				&& aj.a((anIDirect3D5871.CheckDeviceFormat(anInt5873, anInt5881, d3ddisplaymode.Format, 0, 3,
						method1253(0, class67, class202))), (int) 17));
	}

	public void method1171(byte i, boolean bool, int i_61_, Class287 class287) {
		int i_62_ = 94 / ((-37 - i) / 38);
		int i_63_ = i_61_;
		int i_64_;
		while_111_: do {
			do {
				if (i_63_ != 1) {
					if (i_63_ != 2)
						break;
				} else {
					i_64_ = 6;
					break while_111_;
				}
				i_64_ = 27;
				break while_111_;
			} while (false);
			i_64_ = 5;
		} while (false);
		int i_65_ = 0;
		if (bool)
			i_65_ |= 0x10;
		anIDirect3DDevice5865.SetTextureStageState(anInt3991, i_64_, (i_65_ | method1246(1, class287)));
	}

	public void method1115(int i) {
		anIDirect3DDevice5865.SetScissorRect(anInt3987 + anInt4011, anInt4042 + anInt4039, anInt3982, anInt4040);
		if (i != 13)
			method1162(false);
	}

	public void c() {
		/* empty */
	}

	private static int method1250(byte i, Class289 class289) {
		if (i < 91)
			method1243(14, false, null, null, -24, 71);
		if (class289 == ISAACCipher.aClass289_1896)
			return 2;
		if (Class287.aClass289_2645 != class289) {
			if (Class264.aClass289_2467 != class289) {
				if (class289 != Class166_Sub1.aClass289_4298) {
					if (class289 != aa_Sub2.aClass289_3730) {
						if (class289 == Class121.aClass289_1267)
							return 5;
					} else
						return 6;
				} else
					return 4;
			} else
				return 1;
		} else
			return 3;
		throw new IllegalArgumentException("");
	}

	public void method1178(Interface15_Impl2 interface15_impl2, byte i, int i_66_) {
		if (i > -116)
			method1200(null, null, (byte) 96);
		Class133 class133 = (Class133) interface15_impl2;
		anIDirect3DDevice5865.SetStreamSource(i_66_, (class133.anIDirect3DVertexBuffer5708), 0,
				class133.method1400((byte) 126));
	}

	public Interface19 b(int i, int i_67_) {
		return null;
	}

	public Object method1100(int i, Canvas canvas) {
		int i_68_ = -17 % ((9 - i) / 53);
		return null;
	}

	public void method1216(int i) {
		anIDirect3DDevice5865.a(27, aBoolean4015);
		if (i >= -56)
			aBooleanArray5872 = null;
	}

	public void method1217(int i, Object object, Canvas canvas) {
		if (i != 26104)
			aD3DLIGHT5864 = null;
	}

	public void method1225(int i) {
		int i_69_;
		for (i_69_ = 0; anInt4046 > i_69_; i_69_++) {
			Class296_Sub35 class296_sub35 = aClass296_Sub35Array4005[i_69_];
			int i_70_ = i_69_ + 2;
			int i_71_ = class296_sub35.method2746(-24996);
			float f = class296_sub35.method2745((byte) 100) / 255.0F;
			aD3DLIGHT5876.SetPosition((float) class296_sub35.method2749(true),
					(float) class296_sub35.method2751(-28925), (float) class296_sub35.method2750(-4444));
			aD3DLIGHT5876.SetDiffuse(f * (float) (i_71_ >> 16 & 0xff), (float) ((i_71_ & 0xffca) >> 8) * f,
					f * (float) (i_71_ & 0xff), 0.0F);
			aD3DLIGHT5876.SetAttenuation(0.0F, 0.0F,
					1.0F / (float) (class296_sub35.method2748(91) * class296_sub35.method2748(100)));
			aD3DLIGHT5876.SetRange((float) class296_sub35.method2748(98));
			anIDirect3DDevice5865.SetLight(i_70_, aD3DLIGHT5876);
			anIDirect3DDevice5865.LightEnable(i_70_, true);
		}
		int i_72_ = 92 % ((i - 33) / 61);
		for (/**/; i_69_ < anInt3983; i_69_++)
			anIDirect3DDevice5865.LightEnable(i_69_ + 2, false);
		super.method1225(-82);
	}

	public void method1170(int i, Class287 class287, boolean bool, boolean bool_73_, byte i_74_) {
		if (i_74_ < -29) {
			int i_75_ = 0;
			int i_76_ = i;
			int i_77_;
			while_112_: do {
				do {
					if (i_76_ != 1) {
						if (i_76_ != 2)
							break;
					} else {
						i_77_ = 3;
						break while_112_;
					}
					i_77_ = 26;
					break while_112_;
				} while (false);
				i_77_ = 2;
			} while (false);
			if (bool_73_)
				i_75_ |= 0x20;
			if (bool)
				i_75_ |= 0x10;
			anIDirect3DDevice5865.SetTextureStageState(anInt3991, i_77_, i_75_ | method1246(1, class287));
		}
	}

	public synchronized void g(int i) {
		aVba5870.a(-18685);
		super.g(i);
	}

	public void method1210(int i) {
		if (aClass108_3954.method947(27229))
			aClass373_Sub2_3943.method3932(0, aFloatArray5855);
		else {
			aFloatArray5855[0] = 1.0F;
			aFloatArray5855[11] = 0.0F;
			aFloatArray5855[13] = 0.0F;
			aFloatArray5855[8] = 0.0F;
			aFloatArray5855[12] = 0.0F;
			aFloatArray5855[9] = 0.0F;
			aFloatArray5855[5] = 1.0F;
			aFloatArray5855[14] = 0.0F;
			aFloatArray5855[10] = 1.0F;
			aFloatArray5855[4] = 0.0F;
			aFloatArray5855[7] = 0.0F;
			aFloatArray5855[15] = 1.0F;
			aFloatArray5855[6] = 0.0F;
			aFloatArray5855[3] = 0.0F;
			aFloatArray5855[2] = 0.0F;
			aFloatArray5855[1] = 0.0F;
		}
		anIDirect3DDevice5865.SetTransform(2, aFloatArray5855);
		if (i != -10667)
			method1210(-109);
	}

	public Class33 s() {
		D3DADAPTER_IDENTIFIER d3dadapter_identifier = anIDirect3D5871.b(anInt5873, 0);
		return new Class33(d3dadapter_identifier.VendorID, "Direct3D", 9, d3dadapter_identifier.Description,
				d3dadapter_identifier.DriverVersion);
	}

	public boolean k() {
		return false;
	}

	public void method1138(int i) {
		aD3DLIGHT5864.SetDirection(-aFloatArray4030[i], -aFloatArray4030[1], -aFloatArray4030[2]);
		aD3DLIGHT5860.SetDirection(-aFloatArray4028[0], -aFloatArray4028[1], -aFloatArray4028[2]);
		aBoolean5866 = false;
	}

	public void method1128(boolean bool) {
		if (bool)
			method1134(-90);
		anIDirect3DDevice5865.a(7, aBoolean3948);
	}

	public void method1112(boolean bool) {
		aD3DLIGHT5864.SetAmbient(aFloat4000 * aFloat4010, aFloat4010 * aFloat4019, aFloat4043 * aFloat4010, 0.0F);
		aBoolean5866 = bool;
	}

	public Interface13 a(Interface19 interface19, Interface11 interface11) {
		return null;
	}

	public void method1251(Class64_Sub3 class64_sub3, boolean bool) {
		if (bool)
			method1134(-119);
		method1249(class64_sub3, (byte) -127);
		if (class64_sub3.aBoolean3897 == !aBooleanArray5877[anInt3991]) {
			anIDirect3DDevice5865.SetSamplerState(anInt3991, 1, (!class64_sub3.aBoolean3897 ? 3 : 1));
			aBooleanArray5877[anInt3991] = class64_sub3.aBoolean3897;
		}
		if (!aBooleanArray5882[anInt3991] != !class64_sub3.aBoolean3900) {
			anIDirect3DDevice5865.SetSamplerState(anInt3991, 2, (!class64_sub3.aBoolean3900 ? 3 : 1));
			aBooleanArray5882[anInt3991] = class64_sub3.aBoolean3900;
		}
	}

	private static int method1252(Class230 class230, byte i) {
		if (Class316.aClass230_2797 == class230)
			return 1;
		if (class230 == Class360_Sub5.aClass230_5325)
			return 2;
		if (class230 != Class2.aClass230_62) {
			if (class230 == Class296_Sub39_Sub13.aClass230_6206)
				return 4;
			if (class230 == Class253.aClass230_2387)
				return 256;
		} else
			return 3;
		if (i != 112)
			method1243(120, true, null, null, -109, 52);
		return 0;
	}

	public Interface15_Impl2 method1205(boolean bool, int i) {
		int i_78_ = 101 % ((i + 15) / 62);
		return new Class133(this, bool);
	}

	public Interface11 a(int i, int i_79_) {
		return null;
	}

	public void method1185(int i, int i_80_, Class289 class289, int i_81_) {
		if (i == 0)
			anIDirect3DDevice5865.DrawPrimitive(method1250((byte) 100, class289), i_80_, i_81_);
	}

	public void method1101(boolean bool, byte i) {
		int i_82_ = -4 / ((15 - i) / 49);
		anIDirect3DDevice5865.a(161, bool);
	}

	public void method1184(byte i) {
		if (aClass258_4029 != StaticMethods.aClass258_5957) {
			if (HardReferenceWrapper.aClass258_6699 == aClass258_4029) {
				anIDirect3DDevice5865.SetRenderState(19, 2);
				anIDirect3DDevice5865.SetRenderState(20, 2);
			} else if (ConfigurationsLoader.aClass258_89 == aClass258_4029) {
				anIDirect3DDevice5865.SetRenderState(19, 9);
				anIDirect3DDevice5865.SetRenderState(20, 2);
			}
		} else {
			anIDirect3DDevice5865.SetRenderState(19, 5);
			anIDirect3DDevice5865.SetRenderState(20, 6);
		}
		if (i != -109)
			aBoolean5869 = false;
	}

	public void method1188(int i) {
		int i_83_ = 5 / ((-15 - i) / 44);
	}

	public static int method1253(int i, Class67 class67, Class202 class202) {
		if (i != 0)
			return -45;
		if (class67 == Class67.aClass67_745) {
			if (Class122_Sub1_Sub1.aClass202_6602 == class202)
				return 22;
			if (za_Sub2.aClass202_6555 != class202) {
				if (Class13.aClass202_3516 == class202)
					return 28;
				if (StaticMethods.aClass202_6068 != class202) {
					if (class202 != Class125.aClass202_1285) {
						if (class202 == Class55.aClass202_654)
							return 77;
					} else
						return 51;
				} else
					return 50;
			} else
				return 21;
		}
		throw new IllegalArgumentException("");
	}

	public void y() {
		/* empty */
	}

	public int[] na(int i, int i_84_, int i_85_, int i_86_) {
		int[] is = null;
		IDirect3DSurface idirect3dsurface = anIDirect3DDevice5865.a((int) 0);
		IDirect3DSurface idirect3dsurface_87_ = anIDirect3DDevice5865.a(i_85_, i_86_, 21, 0, 0, true);
		if (aj.a(anIDirect3DDevice5865.StretchRect(idirect3dsurface, i, i_84_, i_85_, i_86_, idirect3dsurface_87_, 0, 0,
				i_85_, i_86_, 0), (int) 101)
				&& aj.a(idirect3dsurface_87_.LockRect(0, 0, i_85_, i_86_, 16, aPixelBuffer5867), (int) 49)) {
			is = new int[i_86_ * i_85_];
			int i_88_ = aPixelBuffer5867.getRowPitch();
			if (i_85_ * 4 != i_88_) {
				for (int i_89_ = 0; i_86_ > i_89_; i_89_++)
					aPixelBuffer5867.a(is, i_89_ * i_85_, i_89_ * i_88_, i_85_);
			} else
				aPixelBuffer5867.a(is, 0, 0, i_86_ * i_85_);
			idirect3dsurface_87_.UnlockRect();
		}
		idirect3dsurface.b((byte) 83);
		idirect3dsurface_87_.b((byte) 83);
		return is;
	}

	public int I() {
		return 0;
	}

	public void method1208(Object object, Canvas canvas, byte i) {
		if (aCanvas3926 == canvas) {
			Dimension dimension = canvas.getSize();
			if (dimension.width > 0 && dimension.height > 0) {
				anIDirect3DDevice5865.EndScene();
				method1247(0);
				anIDirect3DDevice5865.BeginScene();
			}
		}
		if (i <= 118)
			aD3DLIGHT5876 = null;
	}

	static {
		aFloatArray5855 = new float[16];
		anIntArray5857 = new int[] { 22, 23 };
	}
}
