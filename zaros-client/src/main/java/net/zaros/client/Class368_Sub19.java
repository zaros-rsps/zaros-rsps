package net.zaros.client;


/* Class368_Sub19 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub19 extends Class368 {
	private int anInt5537;
	private int anInt5538;
	private int anInt5539;
	private int anInt5540;
	private int anInt5541;
	static Class404 aClass404_5542;

	Class368_Sub19(Packet class296_sub17) {
		super(class296_sub17);
		anInt5539 = class296_sub17.g2();
		int i = class296_sub17.g4();
		anInt5538 = i >>> 16;
		anInt5537 = i & 0xffff;
		anInt5540 = class296_sub17.g1();
		anInt5541 = class296_sub17.g1();
	}

	final void method3807(byte i) {
		Class41_Sub14.aClass45Array3777[anInt5539].method581(anInt5538, anInt5541, anInt5537, (byte) -23, anInt5540);
		int i_0_ = 110 % ((i - 52) / 52);
	}

	public static void method3855(int i) {
		aClass404_5542 = null;
		if (i >= -43)
			aClass404_5542 = null;
	}

	static final String method3856(InterfaceComponent class51, int i) {
		if (GameClient.method115(class51).method2707(-91) == 0)
			return null;
		int i_1_ = -40 / ((i - 67) / 36);
		if (class51.selectedAction == null || class51.selectedAction.trim().length() == 0) {
			if (BillboardRaw.aBoolean1071)
				return "Hidden-use";
			return null;
		}
		return class51.selectedAction;
	}
}
