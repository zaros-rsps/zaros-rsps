package net.zaros.client;

/* Class277 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class277 {
	static int anInt2530;
	private Object anObject2531;
	private String name = "null";
	char aChar2533;
	char aChar2534;
	static int anInt2535;
	private HashTable aClass263_2536;
	private int anInt2537;
	private int anInt2538;

	final Class296_Sub43 method2323(int i, int i_0_) {
		if (anObject2531 == null)
			return null;
		if (aClass263_2536 == null)
			method2333(0);
		if (i_0_ >= -116)
			method2329(null, (byte) -96, -81);
		return (Class296_Sub43) aClass263_2536.get((long) i);
	}

	static final boolean method2324(byte i, byte i_1_) {
		int i_2_ = i & 0xff;
		if (i_2_ == 0)
			return false;
		int i_3_ = -64 / ((-35 - i_1_) / 53);
		if (i_2_ >= 128 && i_2_ < 160 && r.aCharArray6207[i_2_ - 128] == 0)
			return false;
		return true;
	}

	final String method2325(byte i, int i_4_) {
		if (i < 119)
			aChar2534 = '\032';
		if (anObject2531 == null)
			return name;
		if (!(anObject2531 instanceof HashTable)) {
			String[] strings = (String[]) anObject2531;
			if (i_4_ < 0 || i_4_ >= strings.length)
				return name;
			String string = strings[i_4_];
			if (string != null)
				return string;
			return name;
		}
		StringNode v = ((StringNode) ((HashTable) anObject2531).get((long) i_4_));
		if (v != null)
			return v.value;
		return name;
	}

	final int method2326(boolean bool, int i) {
		if (bool)
			method2328(65, null, -87);
		if (anObject2531 == null)
			return anInt2538;
		if (!(anObject2531 instanceof HashTable)) {
			Integer[] integers = (Integer[]) anObject2531;
			if (i < 0 || i >= integers.length)
				return anInt2538;
			Integer integer = integers[i];
			if (integer != null)
				return integer.intValue();
			return anInt2538;
		}
		IntegerNode class296_sub16 = ((IntegerNode) ((HashTable) anObject2531).get((long) i));
		if (class296_sub16 != null)
			return class296_sub16.value;
		return anInt2538;
	}

	final void method2327(Packet class296_sub17, int i) {
		if (i != 255)
			name = null;
		for (;;) {
			int i_5_ = class296_sub17.g1();
			if (i_5_ == 0)
				break;
			method2329(class296_sub17, (byte) 73, i_5_);
		}
	}

	static final int method2328(int i, byte[] is, int i_6_) {
		if (i >= -77)
			anInt2535 = -104;
		return ClotchesLoader.method303(24443, i_6_, is, 0);
	}

	private final void method2329(Packet in, byte i, int i_7_) {
		if (i_7_ != 1) {
			if (i_7_ == 2)
				aChar2534 = Mesh.method1387((byte) -117, in.g1b());
			else if (i_7_ != 3) {
				if (i_7_ != 4) {
					if (i_7_ != 5 && i_7_ != 6) {
						if (i_7_ == 7) {
							int i_8_ = in.g2();
							anInt2537 = in.g2();
							String[] strings = new String[i_8_];
							for (int i_9_ = 0; anInt2537 > i_9_; i_9_++) {
								int i_10_ = in.g2();
								strings[i_10_] = in.gstr();
							}
							anObject2531 = strings;
						} else if (i_7_ == 8) {
							int i_11_ = in.g2();
							anInt2537 = in.g2();
							Integer[] integers = new Integer[i_11_];
							for (int i_12_ = 0; i_12_ < anInt2537; i_12_++) {
								int i_13_ = in.g2();
								integers[i_13_] = new Integer(in.g4());
							}
							anObject2531 = integers;
						}
					} else {
						anInt2537 = in.g2();
						HashTable extraData = new HashTable(Class8.get_next_high_pow2(anInt2537));
						for (int i_14_ = 0; i_14_ < anInt2537; i_14_++) {
							int i_15_ = in.g4();
							Node n;
							if (i_7_ != 5)
								n = (new IntegerNode(in.g4()));
							else {
								n = new StringNode(in.gstr());
							}
							extraData.put((long) i_15_, n);
						}
						anObject2531 = extraData;
					}
				} else
					anInt2538 = in.g4();
			} else {
				name = in.gstr();
			}
		} else
			aChar2533 = Mesh.method1387((byte) -58, in.g1b());
		int i_16_ = 47 / ((-66 - i) / 58);
	}

	final boolean method2330(int i, int i_17_) {
		if (anObject2531 == null)
			return false;
		if (i != 28341)
			anInt2535 = -18;
		if (aClass263_2536 == null)
			method2333(0);
		if (aClass263_2536.get((long) i_17_) == null)
			return false;
		return true;
	}

	final boolean method2331(int i, String string) {
		if (anObject2531 == null)
			return false;
		if (aClass263_2536 == null)
			method2335(107);
		Class296_Sub40 class296_sub40 = ((Class296_Sub40) aClass263_2536.get(Class239.method2142(string, -681688315)));
		if (i != 0)
			method2328(-118, null, 113);
		for (/**/; class296_sub40 != null; class296_sub40 = (Class296_Sub40) aClass263_2536.getUnknown(false)) {
			if (class296_sub40.aString4907.equals(string))
				return true;
		}
		return false;
	}

	static final boolean method2332(byte i) {
		Class296_Sub24 class296_sub24 = (Class296_Sub24) Class328.aClass155_2912.removeFirst((byte) 116);
		if (class296_sub24 == null)
			return false;
		for (int i_18_ = 0; i_18_ < class296_sub24.anInt4756; i_18_++) {
			if (class296_sub24.aClass278Array4749[i_18_] != null && class296_sub24.aClass278Array4749[i_18_].anInt2540 == 0)
				return false;
			if (class296_sub24.aClass278Array4755[i_18_] != null && class296_sub24.aClass278Array4755[i_18_].anInt2540 == 0)
				return false;
		}
		if (i < 25)
			method2324((byte) 89, (byte) -31);
		return true;
	}

	private final void method2333(int i) {
		if (!(anObject2531 instanceof HashTable)) {
			Integer[] integers = (Integer[]) anObject2531;
			int i_19_ = Class8.get_next_high_pow2(integers.length);
			aClass263_2536 = new HashTable(i_19_);
			HashTable class263 = new HashTable(i_19_);
			for (int i_20_ = 0; i_20_ < integers.length; i_20_++) {
				if (integers[i_20_] != null) {
					int i_21_ = integers[i_20_].intValue();
					IntegerNode class296_sub16 = ((IntegerNode) class263.get((long) i_21_));
					if (class296_sub16 == null) {
						class296_sub16 = new IntegerNode(0);
						class263.put((long) i_21_, class296_sub16);
					}
					class296_sub16.value++;
				}
			}
			for (int i_22_ = 0; i_22_ < integers.length; i_22_++) {
				if (integers[i_22_] != null) {
					int i_23_ = integers[i_22_].intValue();
					Class296_Sub43 class296_sub43 = ((Class296_Sub43) aClass263_2536.get((long) i_23_));
					int i_24_ = ((IntegerNode) class263.get((long) i_23_)).value--;
					if (class296_sub43 == null) {
						class296_sub43 = new Class296_Sub43(i_24_);
						aClass263_2536.put((long) i_23_, class296_sub43);
					}
					class296_sub43.anIntArray4938[-i_24_ + class296_sub43.anIntArray4938.length] = i_22_;
				}
			}
		} else {
			HashTable class263 = (HashTable) anObject2531;
			aClass263_2536 = new HashTable(class263.getBucketCount((byte) 127));
			HashTable class263_25_ = new HashTable(class263.getBucketCount((byte) 126));
			for (IntegerNode class296_sub16 = (IntegerNode) class263.getFirst(true); class296_sub16 != null; class296_sub16 = (IntegerNode) class263.getNext(0)) {
				IntegerNode class296_sub16_26_ = ((IntegerNode) class263_25_.get((long) class296_sub16.value));
				if (class296_sub16_26_ == null) {
					class296_sub16_26_ = new IntegerNode(0);
					class263_25_.put((long) class296_sub16.value, class296_sub16_26_);
				}
				class296_sub16_26_.value++;
			}
			for (IntegerNode class296_sub16 = (IntegerNode) class263.getFirst(true); class296_sub16 != null; class296_sub16 = (IntegerNode) class263.getNext(i)) {
				Class296_Sub43 class296_sub43 = ((Class296_Sub43) aClass263_2536.get((long) (class296_sub16.value)));
				int i_27_ = ((IntegerNode) class263_25_.get((long) class296_sub16.value)).value--;
				if (class296_sub43 == null) {
					class296_sub43 = new Class296_Sub43(i_27_);
					aClass263_2536.put((long) class296_sub16.value, class296_sub43);
				}
				class296_sub43.anIntArray4938[class296_sub43.anIntArray4938.length - i_27_] = (int) class296_sub16.uid;
			}
		}
		if (i != 0)
			anObject2531 = null;
	}

	final int method2334(int i) {
		if (i != -1)
			method2330(-41, -24);
		return anInt2537;
	}

	private final void method2335(int i) {
		if (i <= 94)
			aChar2533 = '\022';
		if (!(anObject2531 instanceof HashTable)) {
			String[] strings = (String[]) anObject2531;
			int i_28_ = Class8.get_next_high_pow2(strings.length);
			aClass263_2536 = new HashTable(i_28_);
			HashTable class263 = new HashTable(i_28_);
			for (int i_29_ = 0; strings.length > i_29_; i_29_++) {
				if (strings[i_29_] != null) {
					String string = strings[i_29_];
					long l = Class239.method2142(string, -681688315);
					Class296_Sub40 class296_sub40;
					for (class296_sub40 = (Class296_Sub40) class263.get(l); class296_sub40 != null; class296_sub40 = (Class296_Sub40) class263.getUnknown(false)) {
						if (class296_sub40.aString4907.equals(string))
							break;
					}
					if (class296_sub40 == null) {
						class296_sub40 = new Class296_Sub40(string, 0);
						class263.put(l, class296_sub40);
					}
					class296_sub40.anInt4908++;
				}
			}
			for (int i_30_ = 0; strings.length > i_30_; i_30_++) {
				if (strings[i_30_] != null) {
					String string = strings[i_30_];
					long l = Class239.method2142(string, -681688315);
					Class296_Sub30 class296_sub30;
					for (class296_sub30 = (Class296_Sub30) aClass263_2536.get(l); class296_sub30 != null; class296_sub30 = ((Class296_Sub30) aClass263_2536.getUnknown(false))) {
						if (class296_sub30.aString4817.equals(string))
							break;
					}
					Class296_Sub40 class296_sub40;
					for (class296_sub40 = (Class296_Sub40) class263.get(l); class296_sub40 != null; class296_sub40 = (Class296_Sub40) class263.getUnknown(false)) {
						if (class296_sub40.aString4907.equals(string))
							break;
					}
					int i_31_ = class296_sub40.anInt4908--;
					if (class296_sub30 == null) {
						class296_sub30 = new Class296_Sub30(string, i_31_);
						aClass263_2536.put(l, class296_sub30);
					}
					class296_sub30.anIntArray4819[class296_sub30.anIntArray4819.length - i_31_] = i_30_;
				}
			}
		} else {
			HashTable class263 = (HashTable) anObject2531;
			aClass263_2536 = new HashTable(class263.getBucketCount((byte) 127));
			HashTable class263_32_ = new HashTable(class263.getBucketCount((byte) 126));
			for (StringNode class296_sub5 = (StringNode) class263.getFirst(true); class296_sub5 != null; class296_sub5 = (StringNode) class263.getNext(0)) {
				long l = Class239.method2142(class296_sub5.value, -681688315);
				Class296_Sub40 class296_sub40;
				for (class296_sub40 = (Class296_Sub40) class263_32_.get(l); class296_sub40 != null; class296_sub40 = (Class296_Sub40) class263_32_.getUnknown(false)) {
					if (class296_sub40.aString4907.equals(class296_sub5.value))
						break;
				}
				if (class296_sub40 == null) {
					class296_sub40 = new Class296_Sub40(class296_sub5.value, 0);
					class263_32_.put(l, class296_sub40);
				}
				class296_sub40.anInt4908++;
			}
			for (StringNode class296_sub5 = (StringNode) class263.getFirst(true); class296_sub5 != null; class296_sub5 = (StringNode) class263.getNext(0)) {
				long l = Class239.method2142(class296_sub5.value, -681688315);
				Class296_Sub30 class296_sub30;
				for (class296_sub30 = (Class296_Sub30) aClass263_2536.get(l); class296_sub30 != null; class296_sub30 = (Class296_Sub30) aClass263_2536.getUnknown(false)) {
					if (class296_sub30.aString4817.equals(class296_sub5.value))
						break;
				}
				Class296_Sub40 class296_sub40;
				for (class296_sub40 = (Class296_Sub40) class263_32_.get(l); class296_sub40 != null; class296_sub40 = (Class296_Sub40) class263_32_.getUnknown(false)) {
					if (class296_sub40.aString4907.equals(class296_sub5.value))
						break;
				}
				int i_33_ = class296_sub40.anInt4908--;
				if (class296_sub30 == null) {
					class296_sub30 = new Class296_Sub30(class296_sub5.value, i_33_);
					aClass263_2536.put(l, class296_sub30);
				}
				class296_sub30.anIntArray4819[class296_sub30.anIntArray4819.length - i_33_] = (int) class296_sub5.uid;
			}
		}
	}

	final Class296_Sub30 method2336(String string, int i) {
		if (anObject2531 == null)
			return null;
		if (aClass263_2536 == null)
			method2335(105);
		Class296_Sub30 class296_sub30 = ((Class296_Sub30) aClass263_2536.get(Class239.method2142(string, -681688315)));
		if (i != -8470)
			anInt2535 = 79;
		for (/**/; class296_sub30 != null; class296_sub30 = (Class296_Sub30) aClass263_2536.getUnknown(false)) {
			if (class296_sub30.aString4817.equals(string))
				break;
		}
		return class296_sub30;
	}

	public Class277() {
		/* empty */
	}
}
