package net.zaros.client;

public abstract class Model {
	static int anInt1848 = 0;
	public boolean aBoolean1849 = false;
	public static Class338_Sub3[] aClass338_Sub3Array1850;

	public abstract void method1715(Class373 class373, Class338_Sub5 class338_sub5, int i);

	static final boolean method1716(int i, boolean bool, int i_0_, int i_1_) {
		Interface14 interface14 = (Interface14) StaticMethods.method2730(i, i_0_, i_1_);
		boolean bool_2_ = bool;
		if (interface14 != null)
			bool_2_ &= Class296_Sub39_Sub14.method2876((byte) -86, interface14);
		interface14 = ((Interface14) Class123_Sub2.method1070(i, i_0_, i_1_, Interface14.class));
		if (interface14 != null)
			bool_2_ &= Class296_Sub39_Sub14.method2876((byte) -86, interface14);
		interface14 = (Interface14) Class296_Sub15_Sub3.method2540(i, i_0_, i_1_);
		if (interface14 != null)
			bool_2_ &= Class296_Sub39_Sub14.method2876((byte) -86, interface14);
		return bool_2_;
	}

	public abstract void method1717(int i, int i_3_, int i_4_, int i_5_);

	public abstract int HA();

	public abstract void k(int i);

	public abstract void s(int i);

	public abstract int da();

	public abstract void method1718(Class373 class373);

	public abstract void method1719(Class373 class373, Class338_Sub5 class338_sub5, int i, int i_6_);

	public abstract boolean NA();

	public abstract boolean F();

	public abstract void FA(int i);

	public abstract void method1720();

	final void method1721(boolean[] bools, int i, boolean bool, Class296_Sub39_Sub6 class296_sub39_sub6, int i_7_,
			Class296_Sub39_Sub6 class296_sub39_sub6_8_, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_,
			boolean bool_14_, int i_15_, Class296_Sub39_Sub6 class296_sub39_sub6_16_,
			Class296_Sub39_Sub6 class296_sub39_sub6_17_) {
		if (i_11_ != -1) {
			if (bools == null || i == -1)
				method1741(bool_14_, i_15_, i_11_, 0, 0, i_9_, class296_sub39_sub6_16_, i_13_, class296_sub39_sub6_17_);
			else {
				method1720();
				if (!NA())
					method1730();
				else {
					AnimFrame class60 = class296_sub39_sub6_17_.aClass60Array6151[i_11_];
					AnimBase class296_sub26 = class60.aClass296_Sub26_694;
					AnimFrame class60_18_ = null;
					if (class296_sub39_sub6_16_ != null) {
						class60_18_ = class296_sub39_sub6_16_.aClass60Array6151[i_9_];
						if (class60_18_.aClass296_Sub26_694 != class296_sub26)
							class60_18_ = null;
					}
					method1723(bool_14_, bools, null, class296_sub26, class60_18_, i_13_, bool, 65535, i_15_, 0,
							class60, -13);
					AnimFrame class60_19_ = class296_sub39_sub6_8_.aClass60Array6151[i];
					AnimFrame class60_20_ = null;
					if (class296_sub39_sub6 != null) {
						class60_20_ = class296_sub39_sub6.aClass60Array6151[i_12_];
						if (class296_sub26 != class60_20_.aClass296_Sub26_694)
							class60_20_ = null;
					}
					method1725(0, new int[0], 0, 0, 0, 0, bool_14_);
					method1723(bool_14_, bools, null, class60_19_.aClass296_Sub26_694, class60_20_, i_7_, true, 65535,
							i_10_, 0, class60_19_, 111);
					wa();
					method1730();
				}
			}
		}
	}

	public abstract int fa();

	public abstract int ma();

	public abstract int ua();

	public abstract void ia(short i, short i_21_);

	public abstract void p(int i, int i_22_, s var_s, s var_s_23_, int i_24_, int i_25_, int i_26_);

	public abstract int na();

	public abstract int RA();

	public abstract void LA(int i);

	public abstract void method1722();

	public abstract void wa();

	private final void method1723(boolean bool, boolean[] bools, int[] is, AnimBase class296_sub26, AnimFrame class60,
			int i, boolean bool_27_, int i_28_, int i_29_, int i_30_, AnimFrame class60_31_, int i_32_) {
		if (class60 == null || i == 0) {
			for (int i_33_ = 0; i_33_ < class60_31_.anInt691; i_33_++) {
				short i_34_ = class60_31_.aShortArray685[i_33_];
				if (bools == null || bools[i_34_] == bool_27_ || class296_sub26.anIntArray4768[i_34_] == 0) {
					short i_35_ = class60_31_.aShortArray687[i_33_];
					if (i_35_ != -1)
						method1740(0, bool, 0, class296_sub26.anIntArrayArray4771[i_35_],
								(class296_sub26.anIntArray4770[i_35_] & i_28_), i_30_, 0, false, is, 0);
					method1740(class60_31_.aShortArray682[i_33_], bool, class60_31_.aShortArray679[i_33_],
							class296_sub26.anIntArrayArray4771[i_34_], i_28_ & class296_sub26.anIntArray4770[i_34_],
							i_30_, class296_sub26.anIntArray4768[i_34_], false, is, class60_31_.aShortArray693[i_33_]);
				}
			}
		} else {
			int i_36_ = -43 % ((i_32_ - 62) / 49);
			int i_37_ = 0;
			int i_38_ = 0;
			for (int i_39_ = 0; i_39_ < class296_sub26.anInt4766; i_39_++) {
				boolean bool_40_ = false;
				if (i_37_ < class60_31_.anInt691 && class60_31_.aShortArray685[i_37_] == i_39_)
					bool_40_ = true;
				boolean bool_41_ = false;
				if (i_38_ < class60.anInt691 && i_39_ == class60.aShortArray685[i_38_])
					bool_41_ = true;
				if (bool_40_ || bool_41_) {
					if (bools != null && bool_27_ == !bools[i_39_] && class296_sub26.anIntArray4768[i_39_] != 0) {
						if (bool_41_)
							i_38_++;
						if (bool_40_)
							i_37_++;
					} else {
						int i_42_ = 0;
						int i_43_ = class296_sub26.anIntArray4768[i_39_];
						if (i_43_ == 3 || i_43_ == 10)
							i_42_ = 128;
						byte i_44_;
						int i_45_;
						short i_46_;
						int i_47_;
						int i_48_;
						if (bool_40_) {
							i_46_ = class60_31_.aShortArray687[i_37_];
							i_44_ = class60_31_.aByteArray686[i_37_];
							i_47_ = class60_31_.aShortArray682[i_37_];
							i_48_ = class60_31_.aShortArray679[i_37_];
							i_45_ = class60_31_.aShortArray693[i_37_];
							i_37_++;
						} else {
							i_44_ = (byte) 0;
							i_45_ = i_42_;
							i_46_ = (short) -1;
							i_47_ = i_42_;
							i_48_ = i_42_;
						}
						int i_49_;
						int i_50_;
						byte i_51_;
						short i_52_;
						int i_53_;
						if (bool_41_) {
							i_51_ = class60.aByteArray686[i_38_];
							i_50_ = class60.aShortArray679[i_38_];
							i_49_ = class60.aShortArray682[i_38_];
							i_53_ = class60.aShortArray693[i_38_];
							i_52_ = class60.aShortArray687[i_38_];
							i_38_++;
						} else {
							i_49_ = i_42_;
							i_50_ = i_42_;
							i_51_ = (byte) 0;
							i_52_ = (short) -1;
							i_53_ = i_42_;
						}
						int i_54_;
						int i_55_;
						int i_56_;
						if ((i_44_ & 0x2) != 0 || (i_51_ & 0x1) != 0) {
							i_56_ = i_48_;
							i_54_ = i_47_;
							i_55_ = i_45_;
						} else if (i_43_ == 2) {
							int i_57_ = i_49_ - i_47_ & 0x3fff;
							int i_58_ = -i_45_ + i_53_ & 0x3fff;
							if (i_57_ >= 8192)
								i_57_ -= 16384;
							int i_59_ = i_50_ - i_48_ & 0x3fff;
							if (i_58_ >= 8192)
								i_58_ -= 16384;
							if (i_59_ >= 8192)
								i_59_ -= 16384;
							i_54_ = i_57_ * i / i_29_ + i_47_ & 0x3fff;
							i_55_ = i_45_ + i_58_ * i / i_29_ & 0x3fff;
							i_56_ = i_59_ * i / i_29_ + i_48_ & 0x3fff;
						} else if (i_43_ != 9) {
							if (i_43_ == 7) {
								int i_60_ = i_49_ - i_47_ & 0x3f;
								if (i_60_ >= 32)
									i_60_ -= 64;
								i_55_ = i_45_ + (i_53_ - i_45_) * i / i_29_;
								i_54_ = i_47_ + i_60_ * i / i_29_ & 0x3f;
								i_56_ = i * (-i_48_ + i_50_) / i_29_ + i_48_;
							} else {
								i_54_ = i * (i_49_ - i_47_) / i_29_ + i_47_;
								i_56_ = i_48_ + (-i_48_ + i_50_) * i / i_29_;
								i_55_ = i * (-i_45_ + i_53_) / i_29_ + i_45_;
							}
						} else {
							int i_61_ = -i_47_ + i_49_ & 0x3fff;
							if (i_61_ >= 8192)
								i_61_ -= 16384;
							i_54_ = i_47_ + i_61_ * i / i_29_ & 0x3fff;
							i_55_ = i_56_ = 0;
						}
						if (i_46_ != -1)
							method1740(0, bool, 0, (class296_sub26.anIntArrayArray4771[i_46_]),
									(class296_sub26.anIntArray4770[i_46_] & i_28_), i_30_, 0, false, is, 0);
						else if (i_52_ != -1)
							method1740(0, bool, 0, (class296_sub26.anIntArrayArray4771[i_52_]),
									i_28_ & (class296_sub26.anIntArray4770[i_52_]), i_30_, 0, false, is, 0);
						method1740(i_54_, bool, i_56_, class296_sub26.anIntArrayArray4771[i_39_],
								(i_28_ & class296_sub26.anIntArray4770[i_39_]), i_30_, i_43_, false, is, i_55_);
					}
				}
			}
		}
	}

	public abstract void aa(short i, short i_62_);

	public abstract void method1724(byte i, byte[] is);

	public abstract void method1725(int i, int[] is, int i_63_, int i_64_, int i_65_, int i_66_, boolean bool);

	public abstract int EA();

	public abstract void method1726(Class373 class373, int i, boolean bool);

	public abstract boolean r();

	final void method1727(Class296_Sub39_Sub6 class296_sub39_sub6, int i, int i_67_,
			Class296_Sub39_Sub6 class296_sub39_sub6_68_, int[] is, int i_69_, int i_70_, int i_71_, int i_72_,
			int i_73_, boolean bool) {
		if (i_73_ != -1) {
			method1720();
			if (i_71_ < 119)
				method1731(-97, 11, null, false, -62, -60);
			if (!NA())
				method1730();
			else {
				AnimFrame class60 = class296_sub39_sub6.aClass60Array6151[i_73_];
				AnimBase class296_sub26 = class60.aClass296_Sub26_694;
				AnimFrame class60_74_ = null;
				if (class296_sub39_sub6_68_ != null) {
					class60_74_ = class296_sub39_sub6_68_.aClass60Array6151[i];
					if (class60_74_.aClass296_Sub26_694 != class296_sub26)
						class60_74_ = null;
				}
				method1723(bool, null, is, class296_sub26, class60_74_, i_70_, false, i_67_, i_72_, i_69_, class60,
						122);
				wa();
				method1730();
			}
		}
	}

	public abstract Model method1728(byte i, int i_75_, boolean bool);

	public abstract void VA(int i);

	public abstract void v();

	public abstract EffectiveVertex[] method1729();

	public abstract int G();

	public abstract void method1730();

	public abstract int WA();

	public abstract void H(int i, int i_76_, int i_77_);

	public abstract boolean method1731(int i, int i_78_, Class373 class373, boolean bool, int i_79_, int i_80_);

	public abstract boolean method1732(int i, int i_81_, Class373 class373, boolean bool, int i_82_);

	public abstract byte[] method1733();

	public abstract r ba(r var_r);

	final void method1734(int i, s var_s, int i_83_, int i_84_, int i_85_, int i_86_, int i_87_, int i_88_, int i_89_) {
		if (i_89_ == 65535) {
			boolean bool = false;
			boolean bool_90_ = false;
			boolean bool_91_ = false;
			int i_92_ = -i / 2;
			int i_93_ = -i_88_ / 2;
			int i_94_ = var_s.method3349(0, i_93_ + i_87_, i_85_ + i_92_);
			int i_95_ = i / 2;
			int i_96_ = -i_88_ / 2;
			int i_97_ = var_s.method3349(i_89_ - 65535, i_96_ + i_87_, i_95_ + i_85_);
			int i_98_ = -i / 2;
			int i_99_ = i_88_ / 2;
			int i_100_ = var_s.method3349(0, i_87_ + i_99_, i_98_ + i_85_);
			int i_101_ = i / 2;
			int i_102_ = i_88_ / 2;
			int i_103_ = var_s.method3349(0, i_87_ + i_102_, i_85_ + i_101_);
			int i_104_ = i_94_ < i_97_ ? i_94_ : i_97_;
			int i_105_ = i_100_ >= i_103_ ? i_103_ : i_100_;
			int i_106_ = i_103_ > i_97_ ? i_97_ : i_103_;
			int i_107_ = i_100_ <= i_94_ ? i_100_ : i_94_;
			if (i_88_ != 0) {
				int i_108_ = ((int) (Math.atan2((double) (i_104_ - i_105_), (double) i_88_) * 2607.5945876176133)
						& 0x3fff);
				if (i_108_ != 0) {
					if (i_84_ != 0) {
						if (i_108_ > 8192) {
							int i_109_ = 16384 - i_84_;
							if (i_109_ > i_108_)
								i_108_ = i_109_;
						} else if (i_108_ > i_84_)
							i_108_ = i_84_;
					}
					FA(i_108_);
				}
			}
			int i_110_ = i_94_ + i_103_;
			if (i != 0) {
				int i_111_ = (int) (Math.atan2((double) (i_107_ - i_106_), (double) i) * 2607.5945876176133) & 0x3fff;
				if (i_111_ != 0) {
					if (i_86_ != 0) {
						if (i_111_ > 8192) {
							int i_112_ = -i_86_ + 16384;
							if (i_112_ > i_111_)
								i_111_ = i_112_;
						} else if (i_86_ < i_111_)
							i_111_ = i_86_;
					}
					VA(i_111_);
				}
			}
			if (i_100_ + i_97_ < i_110_)
				i_110_ = i_100_ + i_97_;
			i_110_ = -i_83_ + (i_110_ >> 1);
			if (i_110_ != 0)
				H(0, i_110_, 0);
		}
	}

	public abstract void P(int i, int i_113_, int i_114_, int i_115_);

	public abstract EmissiveTriangle[] method1735();

	public abstract void I(int i, int[] is, int i_116_, int i_117_, int i_118_, boolean bool, int i_119_,
			int[] is_120_);

	public abstract void method1736(Model class178_121_, int i, int i_122_, int i_123_, boolean bool);

	public abstract boolean method1737();

	public static void method1738(int i) {
		aClass338_Sub3Array1850 = null;
		if (i != 0)
			method1716(4, true, 39, -87);
	}

	public abstract int V();

	public abstract void a(int i);

	public abstract void C(int i);

	public Model() {
		/* empty */
	}

	static final void method1739(int i) {
		Class210_Sub1.foundX = -1;
		Class205.foundY = -1;
		if (i < 42)
			anInt1848 = 67;
		Class338_Sub3_Sub5_Sub1.anInt6645 = 0;
	}

	private final void method1740(int i, boolean bool, int i_124_, int[] is, int i_125_, int i_126_, int i_127_,
			boolean bool_128_, int[] is_129_, int i_130_) {
		if (i_126_ == 1) {
			if (i_127_ == 0 || i_127_ == 1) {
				int i_131_ = -i;
				i = i_124_;
				i_124_ = i_131_;
			} else if (i_127_ != 3) {
				if (i_127_ == 2) {
					int i_132_ = i;
					i = -i_124_ & 0x3fff;
					i_124_ = i_132_ & 0x3fff;
				}
			} else {
				int i_133_ = i;
				i = i_124_;
				i_124_ = i_133_;
			}
		} else if (i_126_ != 2) {
			if (i_126_ == 3) {
				if (i_127_ != 0 && i_127_ != 1) {
					if (i_127_ == 3) {
						int i_134_ = i;
						i = i_124_;
						i_124_ = i_134_;
					} else if (i_127_ == 2) {
						int i_135_ = i;
						i = i_124_ & 0x3fff;
						i_124_ = -i_135_ & 0x3fff;
					}
				} else {
					int i_136_ = i;
					i = -i_124_;
					i_124_ = i_136_;
				}
			}
		} else if (i_127_ != 0 && i_127_ != 1) {
			if (i_127_ == 2) {
				i = -i & 0x3fff;
				i_124_ = -i_124_ & 0x3fff;
			}
		} else {
			i = -i;
			i_124_ = -i_124_;
		}
		if (bool_128_)
			na();
		if (i_125_ != 65535)
			I(i_127_, is, i, i_130_, i_124_, bool, i_125_, is_129_);
		else
			method1725(i_127_, is, i, i_130_, i_124_, i_126_, bool);
	}

	final void method1741(boolean bool, int i, int i_137_, int i_138_, int i_139_, int i_140_,
			Class296_Sub39_Sub6 class296_sub39_sub6, int i_141_, Class296_Sub39_Sub6 class296_sub39_sub6_142_) {
		if (i_137_ != -1) {
			method1720();
			if (!NA())
				method1730();
			else {
				AnimFrame class60 = class296_sub39_sub6_142_.aClass60Array6151[i_137_];
				AnimBase class296_sub26 = class60.aClass296_Sub26_694;
				AnimFrame class60_143_ = null;
				if (class296_sub39_sub6 != null) {
					class60_143_ = class296_sub39_sub6.aClass60Array6151[i_140_];
					if (class60_143_.aClass296_Sub26_694 != class296_sub26)
						class60_143_ = null;
				}
				method1723(bool, null, null, class296_sub26, class60_143_, i_141_, false, 65535, i, i_139_, class60,
						-128);
				wa();
				method1730();
				if (i_138_ != 0)
					ua();
			}
		}
	}

	public abstract void O(int i, int i_144_, int i_145_);

	final void method1742(Class296_Sub39_Sub6 class296_sub39_sub6, int i, int i_146_) {
		if (i_146_ != -1) {
			method1720();
			if (!NA())
				method1730();
			else {
				AnimFrame class60 = class296_sub39_sub6.aClass60Array6151[i_146_];
				AnimBase class296_sub26 = class60.aClass296_Sub26_694;
				int i_147_ = 0;
				if (i != -31633)
					s(-38);
				for (/**/; class60.anInt691 > i_147_; i_147_++) {
					short i_148_ = class60.aShortArray685[i_147_];
					if (class296_sub26.aBooleanArray4764[i_148_]) {
						if (class60.aShortArray687[i_147_] != -1)
							P(0, 0, 0, 0);
						P(class296_sub26.anIntArray4768[i_148_], class60.aShortArray682[i_147_],
								class60.aShortArray693[i_147_], class60.aShortArray679[i_147_]);
					}
				}
				wa();
				method1730();
			}
		}
	}
}
