package net.zaros.client;

/* Class368_Sub9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub9 extends Class368 {
	static IncomingPacket aClass231_5471;
	private int anInt5472;
	static int[] anIntArray5473 = new int[2];
	private int anInt5474;
	private int anInt5475;
	static int anInt5476;
	static int anInt5477;

	static final void method3839(int i) {
		if (i != 2)
			method3839(-62);
		Class333.method3417(81);
		Class368_Sub5_Sub2.aBoolean6597 = false;
	}

	Class368_Sub9(Packet class296_sub17) {
		super(class296_sub17);
		anInt5472 = class296_sub17.g2();
		anInt5474 = class296_sub17.g2();
		anInt5475 = class296_sub17.g1();
	}

	public static void method3840(int i) {
		anIntArray5473 = null;
		if (i != -1)
			method3840(2);
		aClass231_5471 = null;
	}

	static final void method3841() {
		for (int i = 0; i < Class127.anInt1303; i++) {
			Class338_Sub3_Sub1 class338_sub3_sub1 = s.aClass338_Sub3_Sub1Array2833[i];
			Class159.method1616(class338_sub3_sub1, true);
			s.aClass338_Sub3_Sub1Array2833[i] = null;
		}
		Class127.anInt1303 = 0;
	}

	final void method3807(byte i) {
		int i_0_ = -79 % ((52 - i) / 52);
		Class302 class302 = Class379.aClass302Array3624[anInt5472];
		Class218 class218 = InputStream_Sub1.aClass218Array34[anInt5474];
		class218.method2040(class302, anInt5475, (byte) 87);
	}

	static {
		aClass231_5471 = new IncomingPacket(21, 3);
		anInt5476 = -1;
	}
}
