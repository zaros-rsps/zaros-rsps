package net.zaros.client;

public class HashTable {
	private long cached_key;
	int bucketCount;
	static Class149 aClass149_2456;
	Node[] buckets;
	static int anInt2458;
	static int anInt2459;
	private Node cached_node;
	static OutgoingPacket aClass311_2461 = new OutgoingPacket(17, 8);
	private int currentBucket = 0;
	static Class130 aClass130_2463;
	private Node lastIterated;
	static SubCache aClass301_2465 = new SubCache(16);
	static int anInt2466 = -50;

	public HashTable(int size) {
		buckets = new Node[size];
		bucketCount = size;
		for (int i_13_ = 0; i_13_ < size; i_13_++) {
			Node n = buckets[i_13_] = new Node();
			n.prev = n;
			n.next = n;
		}
	}

	static final int method2264(int i, Class67 class67) {
		if (class67 == Class67.aClass67_742) {
			return 5120;
		}
		if (Class67.aClass67_743 != class67) {
			if (Class67.aClass67_744 == class67) {
				return 5124;
			}
			if (class67 == Class67.aClass67_745) {
				return 5121;
			}
			if (Class67.aClass67_746 == class67) {
				return 5123;
			}
			if (Class67.aClass67_747 == class67) {
				return 5125;
			}
			if (Class67.aClass67_748 != class67) {
				if (class67 == Class67.aClass67_749) {
					return 5126;
				}
			} else {
				return 5131;
			}
		} else {
			return 5122;
		}
		if (i > -96) {
			return -33;
		}
		throw new IllegalArgumentException("");
	}

	public Node getUnknown(boolean bool) {
		if (cached_node == null) {
			return null;
		}
		if (bool) {
			return null;
		}
		for (Node class296 = buckets[(int) (cached_key
				& bucketCount - 1)]; cached_node != class296; cached_node = cached_node.next) {
			if (cached_node.uid == cached_key) {
				Node class296_0_ = cached_node;
				cached_node = cached_node.next;
				return class296_0_;
			}
		}
		cached_node = null;
		return null;
	}

	public Node getFirst(boolean bool) {
		currentBucket = 0;
		if (bool != true) {
			return null;
		}
		return getNext(0);
	}

	public static void method2267(int i) {
		aClass311_2461 = null;
		aClass301_2465 = null;
		aClass130_2463 = null;
		if (i != -22705) {
			anInt2466 = -47;
		}
		aClass149_2456 = null;
	}

	public int size(byte i) {
		int sum = 0;
		if (i != -4) {
			return -35;
		}
		for (int i_2_ = 0; bucketCount > i_2_; i_2_++) {
			Node n0 = buckets[i_2_];
			Node n1 = n0.next;
			while (n0 != n1) {
				n1 = n1.next;
				sum++;
			}
		}
		return sum;
	}

	public int toArray(int i, Node[] class296s) {
		int count = 0;
		int i_5_ = 0;
		if (i != 5125) {
			bucketCount = -76;
		}
		for (/**/; bucketCount > i_5_; i_5_++) {
			Node class296 = buckets[i_5_];
			for (Node class296_6_ = class296.next; class296 != class296_6_; class296_6_ = class296_6_.next) {
				class296s[count++] = class296_6_;
			}
		}
		return count;
	}

	public Node getNext(int i) {
		if (currentBucket > 0 && buckets[currentBucket - 1] != lastIterated) {
			Node class296 = lastIterated;
			lastIterated = class296.next;
			return class296;
		}
		if (i != 0) {
			return null;
		}
		while (currentBucket < bucketCount) {
			Node class296 = buckets[currentBucket++].next;
			if (class296 != buckets[currentBucket - 1]) {
				lastIterated = class296.next;
				return class296;
			}
		}
		return null;
	}

	static final boolean method2271(byte i, int i_7_) {
		if (i != -72) {
			return false;
		}
		if (i_7_ != 9 && i_7_ != 10) {
			return false;
		}
		return true;
	}

	public Node get(long uid) {
		cached_key = uid;
		Node bucket = buckets[(int) (uid & bucketCount - 1)];
		for (cached_node = bucket.next; cached_node != bucket; cached_node = cached_node.next) {
			if (cached_node.uid == uid) {
				Node class296_9_ = cached_node;
				cached_node = cached_node.next;
				return class296_9_;
			}
		}
		cached_node = null;
		return null;
	}

	public void clear() {
		for (int i_10_ = 0; bucketCount > i_10_; i_10_++) {
			Node class296 = buckets[i_10_];
			for (;;) {
				Node class296_11_ = class296.next;
				if (class296_11_ == class296) {
					break;
				}
				class296_11_.unlink();
			}
		}
		cached_node = null;
		lastIterated = null;
	}

	public void put(long key, Node val) {
		if (val.prev != null) {
			val.unlink();
		}
		Node front = buckets[(int) (key & bucketCount - 1)];
		val.next = front;
		val.prev = front.prev;
		val.prev.next = val;
		val.uid = key;
		val.next.prev = val;
	}

	public int getBucketCount(byte i) {
		if (i < 125) {
			return 83;
		}
		return bucketCount;
	}
}
