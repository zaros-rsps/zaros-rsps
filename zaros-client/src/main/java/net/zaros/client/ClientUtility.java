package net.zaros.client;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URI;

import javax.swing.ImageIcon;

/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 8/15/2017
 */
public final class ClientUtility {
	
	/**
	 * The name
	 */
	public static final String NAME = "Zaros";
	
	/**
	 * The replaced login text
	 */
	public static final String[][] REPLACED_LOGINS = new String[][] {
			// account must be made
			{ "You are standing in a members-only area. To play on this world, move to a free area first.", "There was no account registered by that name. Please click back and press <col=FFDD06>Create Account Now</col> to play." } };
	
	/**
	 * The page that will popup when user clicks forgot password
	 */
	public static final String FORGOT_PASSWORD_URL = "http://redrune.org/lostpassword/";
	
	/**
	 * Opens a url
	 *
	 * @param url
	 * 		The url to open
	 */
	public static void openURL(String url) {
		try {
			Desktop.getDesktop().browse(new URI(url));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
