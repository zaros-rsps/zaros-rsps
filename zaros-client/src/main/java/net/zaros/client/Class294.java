package net.zaros.client;

/* Class294 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class294 {
	static ModeWhere office4ModeWhere = new ModeWhere("WTI", "office", "_wti", 5);
	static int[] anIntArray2686;
	static int anInt2687;
	static byte[] sessionKey = null;
	int anInt2689;
	static boolean[] aBooleanArray2690;

	static int method2420(int i, int i_0_) {
		return i ^ i_0_;
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	static Class294[] method2478(int i) {
		if (i != -4652) {
			StaticMethods.lookatSpeed = -70;
		}
		return new Class294[] { Class296_Sub34_Sub2.aClass294_6098, Class122_Sub3.aClass294_5663, Class391.aClass294_3291, Class296_Sub51_Sub20.aClass294_6441, Animation.aClass294_422, Class397_Sub3.aClass294_5798, Class110.aClass294_1140, Class227.aClass294_2186, Class296_Sub39_Sub14.aClass294_6218, Class83.aClass294_917 };
	}

	public static void method2421(boolean bool) {
		aBooleanArray2690 = null;
		anIntArray2686 = null;
		if (!bool) {
			office4ModeWhere = null;
			sessionKey = null;
		}
	}

	Class294(int i, int i_1_) {
		anInt2689 = i_1_;
	}

	static {
		anInt2687 = 1;
		aBooleanArray2690 = new boolean[8];
	}
}
