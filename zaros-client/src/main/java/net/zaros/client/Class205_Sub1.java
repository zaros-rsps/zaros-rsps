package net.zaros.client;
import jaggl.OpenGL;

final class Class205_Sub1 extends Class205 implements Interface6_Impl1 {
	private int anInt5637;
	static IncomingPacket aClass231_5638 = new IncomingPacket(101, 4);
	private int anInt5639;
	static IConfigsRegister anInterface17_5640 = new Class116();
	static Js5 aClass138_5641;
	static Class55 aClass55_5642;

	public final void method64(byte i, boolean bool, boolean bool_0_) {
		if (i != 49)
			aClass55_5642 = null;
	}

	public final float method69(int i, float f) {
		if (i >= -86)
			method67(true, -96, 62, 14, null, 38, -69, 91, null);
		return f;
	}

	public final void method65(int i, int[] is, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		int[] is_6_ = new int[anInt5639 * anInt5637];
		if (i_4_ != 16438)
			anInt5637 = 26;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glGetTexImagei(anInt3507, 0, 32993, 5121, is_6_, 0);
		for (int i_7_ = 0; i_3_ > i_7_; i_7_++)
			ArrayTools.removeElements(is_6_, (i_5_ + i_3_ - (1 + i_7_)) * anInt5639, is, i_1_ + i_7_ * i_2_, i_2_);
	}

	public final boolean method72(int i) {
		int i_8_ = -31 / ((i - 60) / 37);
		return false;
	}

	public final int method70(int i) {
		int i_9_ = 80 % ((-33 - i) / 37);
		return anInt5639;
	}

	Class205_Sub1(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Class202 class202, Class67 class67, int i, int i_10_) {
		super(var_ha_Sub1_Sub1, 34037, class202, class67, i * i_10_, false);
		anInt5637 = i_10_;
		anInt5639 = i;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glTexImage2Dub(anInt3507, 0, this.method1971(-66), i, i_10_, 0, BITConfigDefinition.method2350(6409, aClass202_3514), HashTable.method2264(-107, aClass67_3513), null, 0);
	}

	public final int method66(int i) {
		if (i != 3776)
			method72(-25);
		return anInt5637;
	}

	Class205_Sub1(ha_Sub1_Sub1 var_ha_Sub1_Sub1, int i, int i_11_, int[] is, int i_12_, int i_13_) {
		super(var_ha_Sub1_Sub1, 34037, za_Sub2.aClass202_6555, Class67.aClass67_745, i * i_11_, false);
		anInt5639 = i;
		anInt5637 = i_11_;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glPixelStorei(3314, i_13_);
		OpenGL.glTexImage2Di(anInt3507, 0, 6408, anInt5639, anInt5637, 0, 32993, aHa_Sub1_Sub1_3508.anInt5852, is, i_12_ * 4);
		OpenGL.glPixelStorei(3314, 0);
	}

	public final void method68(int i, int i_14_, int i_15_, int[] is, int i_16_, int i_17_, int i_18_, int i_19_) {
		aHa_Sub1_Sub1_3508.method1140(this, false);
		if (i_17_ == 0)
			i_17_ = i_14_;
		if (i_17_ != i_14_)
			OpenGL.glPixelStorei(3314, i_17_);
		OpenGL.glTexSubImage2Di(anInt3507, 0, i, i_19_, i_14_, i_18_, 32993, aHa_Sub1_Sub1_3508.anInt5852, is, i_15_);
		if (i_14_ != i_17_)
			OpenGL.glPixelStorei(3314, 0);
		if (i_16_ != -25989)
			aClass231_5638 = null;
	}

	Class205_Sub1(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Class202 class202, int i, int i_20_, byte[] is, int i_21_, int i_22_) {
		super(var_ha_Sub1_Sub1, 34037, class202, Class67.aClass67_745, i * i_20_, false);
		anInt5639 = i;
		anInt5637 = i_20_;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glPixelStorei(3317, 1);
		OpenGL.glPixelStorei(3314, i_22_);
		OpenGL.glTexImage2Dub(anInt3507, 0, this.method1971(-56), i, i_20_, 0, BITConfigDefinition.method2350(6409, aClass202_3514), 5121, is, i_21_);
		OpenGL.glPixelStorei(3314, 0);
		OpenGL.glPixelStorei(3317, 4);
	}

	public final float method71(float f, byte i) {
		int i_23_ = -52 / ((52 - i) / 54);
		return f;
	}

	public static void method1980(int i) {
		aClass231_5638 = null;
		if (i == -6790) {
			anInterface17_5640 = null;
			aClass138_5641 = null;
			aClass55_5642 = null;
		}
	}

	Class205_Sub1(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Class202 class202, int i, int i_24_, float[] fs, int i_25_, int i_26_) {
		super(var_ha_Sub1_Sub1, 34037, class202, Class67.aClass67_749, i_24_ * i, false);
		anInt5639 = i;
		anInt5637 = i_24_;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glPixelStorei(3314, i_26_);
		OpenGL.glTexImage2Df(anInt3507, 0, this.method1971(119), i, i_24_, 0, BITConfigDefinition.method2350(6409, aClass202_3514), 5126, fs, i_25_ * 4);
		OpenGL.glPixelStorei(3314, 0);
	}

	public final void method67(boolean bool, int i, int i_27_, int i_28_, byte[] is, int i_29_, int i_30_, int i_31_, Class202 class202) {
		if (i == 0)
			i = i_27_;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glPixelStorei(3317, 1);
		if (i != i_27_)
			OpenGL.glPixelStorei(3314, i);
		OpenGL.glTexSubImage2Dub(anInt3507, 0, i_29_, i_28_, i_27_, i_31_, BITConfigDefinition.method2350(6409, class202), 5121, is, i_30_);
		if (bool != true)
			aClass231_5638 = null;
		if (i != i_27_)
			OpenGL.glPixelStorei(3314, 0);
		OpenGL.glPixelStorei(3317, 4);
	}
}
