package net.zaros.client;

/* Class11 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class11 {
	int anInt95;
	private ha_Sub2 aHa_Sub2_96;
	int[] anIntArray97 = new int[4096];
	private int anInt98;
	private int[] anIntArray99;
	private boolean aBoolean100 = false;
	int anInt101;
	boolean aBoolean102 = true;
	boolean aBoolean103;
	private Class240 aClass240_104;
	private float[] aFloatArray105;
	boolean aBoolean106 = false;
	int anInt107;
	int anInt108;
	int anInt109;
	private boolean aBoolean110;
	private int[] anIntArray111;
	private int anInt112;
	private int anInt113;
	private float aFloat114;
	private int anInt115;
	private int[] anIntArray116;
	private int anInt117;
	private int anInt118;
	private boolean aBoolean119;
	private int anInt120;
	private int anInt121;
	private float aFloat122;
	private int[] anIntArray123;
	private int anInt124;
	private int anInt125;
	private int anInt126;
	private int anInt127;
	private float aFloat128;

	final void method198(float f, float f_0_, float f_1_, float f_2_, float f_3_, float f_4_, float f_5_, float f_6_, float f_7_, int i, int i_8_, int i_9_) {
		if (aBoolean100) {
			aHa_Sub2_96.method1094((int) f_0_, (int) f, (int) f_3_, (int) f_2_, 127, i | ~0xffffff);
			aHa_Sub2_96.method1094((int) f_1_, (int) f_0_, (int) f_4_, (int) f_3_, 123, i | ~0xffffff);
			aHa_Sub2_96.method1094((int) f, (int) f_1_, (int) f_2_, (int) f_4_, 123, i | ~0xffffff);
		} else {
			float f_10_ = f_3_ - f_2_;
			float f_11_ = f_0_ - f;
			float f_12_ = f_4_ - f_2_;
			float f_13_ = f_1_ - f;
			float f_14_ = f_6_ - f_5_;
			float f_15_ = f_7_ - f_5_;
			float f_16_ = (float) ((i_8_ & 0xff0000) - (i & 0xff0000));
			float f_17_ = (float) ((i_9_ & 0xff0000) - (i & 0xff0000));
			float f_18_ = (float) ((i_8_ & 0xff00) - (i & 0xff00));
			float f_19_ = (float) ((i_9_ & 0xff00) - (i & 0xff00));
			float f_20_ = (float) ((i_8_ & 0xff) - (i & 0xff));
			float f_21_ = (float) ((i_9_ & 0xff) - (i & 0xff));
			float f_22_;
			if (f_1_ != f_0_)
				f_22_ = (f_4_ - f_3_) / (f_1_ - f_0_);
			else
				f_22_ = 0.0F;
			float f_23_;
			if (f_0_ != f)
				f_23_ = f_10_ / f_11_;
			else
				f_23_ = 0.0F;
			float f_24_;
			if (f_1_ != f)
				f_24_ = f_12_ / f_13_;
			else
				f_24_ = 0.0F;
			float f_25_ = f_10_ * f_13_ - f_12_ * f_11_;
			if (f_25_ != 0.0F) {
				float f_26_ = (f_14_ * f_13_ - f_15_ * f_11_) / f_25_;
				float f_27_ = (f_15_ * f_10_ - f_14_ * f_12_) / f_25_;
				float f_28_ = (f_16_ * f_13_ - f_17_ * f_11_) / f_25_;
				float f_29_ = (f_17_ * f_10_ - f_16_ * f_12_) / f_25_;
				float f_30_ = (f_18_ * f_13_ - f_19_ * f_11_) / f_25_;
				float f_31_ = (f_19_ * f_10_ - f_18_ * f_12_) / f_25_;
				float f_32_ = (f_20_ * f_13_ - f_21_ * f_11_) / f_25_;
				float f_33_ = (f_21_ * f_10_ - f_20_ * f_12_) / f_25_;
				if (f <= f_0_ && f <= f_1_) {
					if (!(f >= (float) anInt101)) {
						if (f_0_ > (float) anInt101)
							f_0_ = (float) anInt101;
						if (f_1_ > (float) anInt101)
							f_1_ = (float) anInt101;
						f_5_ = f_5_ - f_26_ * f_2_ + f_26_;
						float f_34_ = (float) (i & 0xff0000) - f_28_ * f_2_ + f_28_;
						float f_35_ = (float) (i & 0xff00) - f_30_ * f_2_ + f_30_;
						float f_36_ = (float) (i & 0xff) - f_32_ * f_2_ + f_32_;
						if (f_0_ < f_1_) {
							f_4_ = f_2_;
							if (f < 0.0F) {
								f_4_ -= f_24_ * f;
								f_2_ -= f_23_ * f;
								f_5_ -= f_27_ * f;
								f_34_ -= f_29_ * f;
								f_35_ -= f_31_ * f;
								f_36_ -= f_33_ * f;
								f = 0.0F;
							}
							if (f_0_ < 0.0F) {
								f_3_ -= f_22_ * f_0_;
								f_0_ = 0.0F;
							}
							if (f != f_0_ && f_24_ < f_23_ || f == f_0_ && f_24_ > f_22_) {
								f_1_ -= f_0_;
								f_0_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_0_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_4_, (int) f_2_, f_5_, f_26_, f_34_, f_28_, f_35_, f_30_, f_36_, f_32_);
									f_4_ += f_24_;
									f_2_ += f_23_;
									f_5_ += f_27_;
									f_34_ += f_29_;
									f_35_ += f_31_;
									f_36_ += f_33_;
									f += (float) anInt98;
								}
								while (--f_1_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_4_, (int) f_3_, f_5_, f_26_, f_34_, f_28_, f_35_, f_30_, f_36_, f_32_);
									f_4_ += f_24_;
									f_3_ += f_22_;
									f_5_ += f_27_;
									f_34_ += f_29_;
									f_35_ += f_31_;
									f_36_ += f_33_;
									f += (float) anInt98;
								}
							} else {
								f_1_ -= f_0_;
								f_0_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_0_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_2_, (int) f_4_, f_5_, f_26_, f_34_, f_28_, f_35_, f_30_, f_36_, f_32_);
									f_4_ += f_24_;
									f_2_ += f_23_;
									f_5_ += f_27_;
									f_34_ += f_29_;
									f_35_ += f_31_;
									f_36_ += f_33_;
									f += (float) anInt98;
								}
								while (--f_1_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_3_, (int) f_4_, f_5_, f_26_, f_34_, f_28_, f_35_, f_30_, f_36_, f_32_);
									f_4_ += f_24_;
									f_3_ += f_22_;
									f_5_ += f_27_;
									f_34_ += f_29_;
									f_35_ += f_31_;
									f_36_ += f_33_;
									f += (float) anInt98;
								}
							}
						} else {
							f_3_ = f_2_;
							if (f < 0.0F) {
								f_3_ -= f_24_ * f;
								f_2_ -= f_23_ * f;
								f_5_ -= f_27_ * f;
								f_34_ -= f_29_ * f;
								f_35_ -= f_31_ * f;
								f_36_ -= f_33_ * f;
								f = 0.0F;
							}
							if (f_1_ < 0.0F) {
								f_4_ -= f_22_ * f_1_;
								f_1_ = 0.0F;
							}
							if (f != f_1_ && f_24_ < f_23_ || f == f_1_ && f_22_ > f_23_) {
								f_0_ -= f_1_;
								f_1_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_1_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_3_, (int) f_2_, f_5_, f_26_, f_34_, f_28_, f_35_, f_30_, f_36_, f_32_);
									f_3_ += f_24_;
									f_2_ += f_23_;
									f_5_ += f_27_;
									f_34_ += f_29_;
									f_35_ += f_31_;
									f_36_ += f_33_;
									f += (float) anInt98;
								}
								while (--f_0_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_4_, (int) f_2_, f_5_, f_26_, f_34_, f_28_, f_35_, f_30_, f_36_, f_32_);
									f_4_ += f_22_;
									f_2_ += f_23_;
									f_5_ += f_27_;
									f_34_ += f_29_;
									f_35_ += f_31_;
									f_36_ += f_33_;
									f += (float) anInt98;
								}
							} else {
								f_0_ -= f_1_;
								f_1_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_1_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_2_, (int) f_3_, f_5_, f_26_, f_34_, f_28_, f_35_, f_30_, f_36_, f_32_);
									f_3_ += f_24_;
									f_2_ += f_23_;
									f_5_ += f_27_;
									f_34_ += f_29_;
									f_35_ += f_31_;
									f_36_ += f_33_;
									f += (float) anInt98;
								}
								while (--f_0_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_2_, (int) f_4_, f_5_, f_26_, f_34_, f_28_, f_35_, f_30_, f_36_, f_32_);
									f_4_ += f_22_;
									f_2_ += f_23_;
									f_5_ += f_27_;
									f_34_ += f_29_;
									f_35_ += f_31_;
									f_36_ += f_33_;
									f += (float) anInt98;
								}
							}
						}
					}
				} else if (f_0_ <= f_1_) {
					if (!(f_0_ >= (float) anInt101)) {
						if (f_1_ > (float) anInt101)
							f_1_ = (float) anInt101;
						if (f > (float) anInt101)
							f = (float) anInt101;
						f_6_ = f_6_ - f_26_ * f_3_ + f_26_;
						float f_37_ = (float) (i_8_ & 0xff0000) - f_28_ * f_3_ + f_28_;
						float f_38_ = (float) (i_8_ & 0xff00) - f_30_ * f_3_ + f_30_;
						float f_39_ = (float) (i_8_ & 0xff) - f_32_ * f_3_ + f_32_;
						if (f_1_ < f) {
							f_2_ = f_3_;
							if (f_0_ < 0.0F) {
								f_2_ -= f_23_ * f_0_;
								f_3_ -= f_22_ * f_0_;
								f_6_ -= f_27_ * f_0_;
								f_37_ -= f_29_ * f_0_;
								f_38_ -= f_31_ * f_0_;
								f_39_ -= f_33_ * f_0_;
								f_0_ = 0.0F;
							}
							if (f_1_ < 0.0F) {
								f_4_ -= f_24_ * f_1_;
								f_1_ = 0.0F;
							}
							if (f_0_ != f_1_ && f_23_ < f_22_ || f_0_ == f_1_ && f_23_ > f_24_) {
								f -= f_1_;
								f_1_ -= f_0_;
								f_0_ = (float) anIntArray97[(int) f_0_];
								while (--f_1_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f_0_, 0, 0, (int) f_2_, (int) f_3_, f_6_, f_26_, f_37_, f_28_, f_38_, f_30_, f_39_, f_32_);
									f_2_ += f_23_;
									f_3_ += f_22_;
									f_6_ += f_27_;
									f_37_ += f_29_;
									f_38_ += f_31_;
									f_39_ += f_33_;
									f_0_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f_0_, 0, 0, (int) f_2_, (int) f_4_, f_6_, f_26_, f_37_, f_28_, f_38_, f_30_, f_39_, f_32_);
									f_2_ += f_23_;
									f_4_ += f_24_;
									f_6_ += f_27_;
									f_37_ += f_29_;
									f_38_ += f_31_;
									f_39_ += f_33_;
									f_0_ += (float) anInt98;
								}
							} else {
								f -= f_1_;
								f_1_ -= f_0_;
								f_0_ = (float) anIntArray97[(int) f_0_];
								while (--f_1_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f_0_, 0, 0, (int) f_3_, (int) f_2_, f_6_, f_26_, f_37_, f_28_, f_38_, f_30_, f_39_, f_32_);
									f_2_ += f_23_;
									f_3_ += f_22_;
									f_6_ += f_27_;
									f_37_ += f_29_;
									f_38_ += f_31_;
									f_39_ += f_33_;
									f_0_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f_0_, 0, 0, (int) f_4_, (int) f_2_, f_6_, f_26_, f_37_, f_28_, f_38_, f_30_, f_39_, f_32_);
									f_2_ += f_23_;
									f_4_ += f_24_;
									f_6_ += f_27_;
									f_37_ += f_29_;
									f_38_ += f_31_;
									f_39_ += f_33_;
									f_0_ += (float) anInt98;
								}
							}
						} else {
							f_4_ = f_3_;
							if (f_0_ < 0.0F) {
								f_4_ -= f_23_ * f_0_;
								f_3_ -= f_22_ * f_0_;
								f_6_ -= f_27_ * f_0_;
								f_37_ -= f_29_ * f_0_;
								f_38_ -= f_31_ * f_0_;
								f_39_ -= f_33_ * f_0_;
								f_0_ = 0.0F;
							}
							if (f < 0.0F) {
								f_2_ -= f_24_ * f;
								f = 0.0F;
							}
							if (f_23_ < f_22_) {
								f_1_ -= f;
								f -= f_0_;
								f_0_ = (float) anIntArray97[(int) f_0_];
								while (--f >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f_0_, 0, 0, (int) f_4_, (int) f_3_, f_6_, f_26_, f_37_, f_28_, f_38_, f_30_, f_39_, f_32_);
									f_4_ += f_23_;
									f_3_ += f_22_;
									f_6_ += f_27_;
									f_37_ += f_29_;
									f_38_ += f_31_;
									f_39_ += f_33_;
									f_0_ += (float) anInt98;
								}
								while (--f_1_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f_0_, 0, 0, (int) f_2_, (int) f_3_, f_6_, f_26_, f_37_, f_28_, f_38_, f_30_, f_39_, f_32_);
									f_2_ += f_24_;
									f_3_ += f_22_;
									f_6_ += f_27_;
									f_37_ += f_29_;
									f_38_ += f_31_;
									f_39_ += f_33_;
									f_0_ += (float) anInt98;
								}
							} else {
								f_1_ -= f;
								f -= f_0_;
								f_0_ = (float) anIntArray97[(int) f_0_];
								while (--f >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f_0_, 0, 0, (int) f_3_, (int) f_4_, f_6_, f_26_, f_37_, f_28_, f_38_, f_30_, f_39_, f_32_);
									f_4_ += f_23_;
									f_3_ += f_22_;
									f_6_ += f_27_;
									f_37_ += f_29_;
									f_38_ += f_31_;
									f_39_ += f_33_;
									f_0_ += (float) anInt98;
								}
								while (--f_1_ >= 0.0F) {
									method200(anIntArray99, aFloatArray105, (int) f_0_, 0, 0, (int) f_3_, (int) f_2_, f_6_, f_26_, f_37_, f_28_, f_38_, f_30_, f_39_, f_32_);
									f_2_ += f_24_;
									f_3_ += f_22_;
									f_6_ += f_27_;
									f_37_ += f_29_;
									f_38_ += f_31_;
									f_39_ += f_33_;
									f_0_ += (float) anInt98;
								}
							}
						}
					}
				} else if (!(f_1_ >= (float) anInt101)) {
					if (f > (float) anInt101)
						f = (float) anInt101;
					if (f_0_ > (float) anInt101)
						f_0_ = (float) anInt101;
					f_7_ = f_7_ - f_26_ * f_4_ + f_26_;
					float f_40_ = (float) (i_9_ & 0xff0000) - f_28_ * f_4_ + f_28_;
					float f_41_ = (float) (i_9_ & 0xff00) - f_30_ * f_4_ + f_30_;
					float f_42_ = (float) (i_9_ & 0xff) - f_32_ * f_4_ + f_32_;
					if (f < f_0_) {
						f_3_ = f_4_;
						if (f_1_ < 0.0F) {
							f_3_ -= f_22_ * f_1_;
							f_4_ -= f_24_ * f_1_;
							f_7_ -= f_27_ * f_1_;
							f_40_ -= f_29_ * f_1_;
							f_41_ -= f_31_ * f_1_;
							f_42_ -= f_33_ * f_1_;
							f_1_ = 0.0F;
						}
						if (f < 0.0F) {
							f_2_ -= f_23_ * f;
							f = 0.0F;
						}
						if (f_22_ < f_24_) {
							f_0_ -= f;
							f -= f_1_;
							f_1_ = (float) anIntArray97[(int) f_1_];
							while (--f >= 0.0F) {
								method200(anIntArray99, aFloatArray105, (int) f_1_, 0, 0, (int) f_3_, (int) f_4_, f_7_, f_26_, f_40_, f_28_, f_41_, f_30_, f_42_, f_32_);
								f_3_ += f_22_;
								f_4_ += f_24_;
								f_7_ += f_27_;
								f_40_ += f_29_;
								f_41_ += f_31_;
								f_42_ += f_33_;
								f_1_ += (float) anInt98;
							}
							while (--f_0_ >= 0.0F) {
								method200(anIntArray99, aFloatArray105, (int) f_1_, 0, 0, (int) f_3_, (int) f_2_, f_7_, f_26_, f_40_, f_28_, f_41_, f_30_, f_42_, f_32_);
								f_3_ += f_22_;
								f_2_ += f_23_;
								f_7_ += f_27_;
								f_40_ += f_29_;
								f_41_ += f_31_;
								f_42_ += f_33_;
								f_1_ += (float) anInt98;
							}
						} else {
							f_0_ -= f;
							f -= f_1_;
							f_1_ = (float) anIntArray97[(int) f_1_];
							while (--f >= 0.0F) {
								method200(anIntArray99, aFloatArray105, (int) f_1_, 0, 0, (int) f_4_, (int) f_3_, f_7_, f_26_, f_40_, f_28_, f_41_, f_30_, f_42_, f_32_);
								f_3_ += f_22_;
								f_4_ += f_24_;
								f_7_ += f_27_;
								f_40_ += f_29_;
								f_41_ += f_31_;
								f_42_ += f_33_;
								f_1_ += (float) anInt98;
							}
							while (--f_0_ >= 0.0F) {
								method200(anIntArray99, aFloatArray105, (int) f_1_, 0, 0, (int) f_2_, (int) f_3_, f_7_, f_26_, f_40_, f_28_, f_41_, f_30_, f_42_, f_32_);
								f_3_ += f_22_;
								f_2_ += f_23_;
								f_7_ += f_27_;
								f_40_ += f_29_;
								f_41_ += f_31_;
								f_42_ += f_33_;
								f_1_ += (float) anInt98;
							}
						}
					} else {
						f_2_ = f_4_;
						if (f_1_ < 0.0F) {
							f_2_ -= f_22_ * f_1_;
							f_4_ -= f_24_ * f_1_;
							f_7_ -= f_27_ * f_1_;
							f_40_ -= f_29_ * f_1_;
							f_41_ -= f_31_ * f_1_;
							f_42_ -= f_33_ * f_1_;
							f_1_ = 0.0F;
						}
						if (f_0_ < 0.0F) {
							f_3_ -= f_23_ * f_0_;
							f_0_ = 0.0F;
						}
						if (f_22_ < f_24_) {
							f -= f_0_;
							f_0_ -= f_1_;
							f_1_ = (float) anIntArray97[(int) f_1_];
							while (--f_0_ >= 0.0F) {
								method200(anIntArray99, aFloatArray105, (int) f_1_, 0, 0, (int) f_2_, (int) f_4_, f_7_, f_26_, f_40_, f_28_, f_41_, f_30_, f_42_, f_32_);
								f_2_ += f_22_;
								f_4_ += f_24_;
								f_7_ += f_27_;
								f_40_ += f_29_;
								f_41_ += f_31_;
								f_42_ += f_33_;
								f_1_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method200(anIntArray99, aFloatArray105, (int) f_1_, 0, 0, (int) f_3_, (int) f_4_, f_7_, f_26_, f_40_, f_28_, f_41_, f_30_, f_42_, f_32_);
								f_3_ += f_23_;
								f_4_ += f_24_;
								f_7_ += f_27_;
								f_40_ += f_29_;
								f_41_ += f_31_;
								f_42_ += f_33_;
								f_1_ += (float) anInt98;
							}
						} else {
							f -= f_0_;
							f_0_ -= f_1_;
							f_1_ = (float) anIntArray97[(int) f_1_];
							while (--f_0_ >= 0.0F) {
								method200(anIntArray99, aFloatArray105, (int) f_1_, 0, 0, (int) f_4_, (int) f_2_, f_7_, f_26_, f_40_, f_28_, f_41_, f_30_, f_42_, f_32_);
								f_2_ += f_22_;
								f_4_ += f_24_;
								f_7_ += f_27_;
								f_40_ += f_29_;
								f_41_ += f_31_;
								f_42_ += f_33_;
								f_1_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method200(anIntArray99, aFloatArray105, (int) f_1_, 0, 0, (int) f_4_, (int) f_3_, f_7_, f_26_, f_40_, f_28_, f_41_, f_30_, f_42_, f_32_);
								f_3_ += f_23_;
								f_4_ += f_24_;
								f_7_ += f_27_;
								f_40_ += f_29_;
								f_41_ += f_31_;
								f_42_ += f_33_;
								f_1_ += (float) anInt98;
							}
						}
					}
				}
			}
		}
	}

	final void method199(float f, float f_43_, float f_44_, float f_45_, float f_46_, float f_47_, float f_48_, float f_49_, float f_50_, int i, int i_51_, int i_52_) {
		if (aBoolean100) {
			aHa_Sub2_96.method1094((int) f_43_, (int) f, (int) f_46_, (int) f_45_, 124, i | ~0xffffff);
			aHa_Sub2_96.method1094((int) f_44_, (int) f_43_, (int) f_47_, (int) f_46_, 125, i | ~0xffffff);
			aHa_Sub2_96.method1094((int) f, (int) f_44_, (int) f_45_, (int) f_47_, 126, i | ~0xffffff);
		} else {
			float f_53_ = f_46_ - f_45_;
			float f_54_ = f_43_ - f;
			float f_55_ = f_47_ - f_45_;
			float f_56_ = f_44_ - f;
			float f_57_ = f_49_ - f_48_;
			float f_58_ = f_50_ - f_48_;
			float f_59_ = (float) ((i_51_ & 0xff0000) - (i & 0xff0000));
			float f_60_ = (float) ((i_52_ & 0xff0000) - (i & 0xff0000));
			float f_61_ = (float) ((i_51_ & 0xff00) - (i & 0xff00));
			float f_62_ = (float) ((i_52_ & 0xff00) - (i & 0xff00));
			float f_63_ = (float) ((i_51_ & 0xff) - (i & 0xff));
			float f_64_ = (float) ((i_52_ & 0xff) - (i & 0xff));
			float f_65_;
			if (f_44_ != f_43_)
				f_65_ = (f_47_ - f_46_) / (f_44_ - f_43_);
			else
				f_65_ = 0.0F;
			float f_66_;
			if (f_43_ != f)
				f_66_ = f_53_ / f_54_;
			else
				f_66_ = 0.0F;
			float f_67_;
			if (f_44_ != f)
				f_67_ = f_55_ / f_56_;
			else
				f_67_ = 0.0F;
			float f_68_ = f_53_ * f_56_ - f_55_ * f_54_;
			if (f_68_ != 0.0F) {
				float f_69_ = (f_57_ * f_56_ - f_58_ * f_54_) / f_68_;
				float f_70_ = (f_58_ * f_53_ - f_57_ * f_55_) / f_68_;
				float f_71_ = (f_59_ * f_56_ - f_60_ * f_54_) / f_68_;
				float f_72_ = (f_60_ * f_53_ - f_59_ * f_55_) / f_68_;
				float f_73_ = (f_61_ * f_56_ - f_62_ * f_54_) / f_68_;
				float f_74_ = (f_62_ * f_53_ - f_61_ * f_55_) / f_68_;
				float f_75_ = (f_63_ * f_56_ - f_64_ * f_54_) / f_68_;
				float f_76_ = (f_64_ * f_53_ - f_63_ * f_55_) / f_68_;
				if (f <= f_43_ && f <= f_44_) {
					if (!(f >= (float) anInt101)) {
						if (f_43_ > (float) anInt101)
							f_43_ = (float) anInt101;
						if (f_44_ > (float) anInt101)
							f_44_ = (float) anInt101;
						f_48_ = f_48_ - f_69_ * f_45_ + f_69_;
						float f_77_ = (float) (i & 0xff0000) - f_71_ * f_45_ + f_71_;
						float f_78_ = (float) (i & 0xff00) - f_73_ * f_45_ + f_73_;
						float f_79_ = (float) (i & 0xff) - f_75_ * f_45_ + f_75_;
						if (f_43_ < f_44_) {
							f_47_ = f_45_;
							if (f < 0.0F) {
								f_47_ -= f_67_ * f;
								f_45_ -= f_66_ * f;
								f_48_ -= f_70_ * f;
								f_77_ -= f_72_ * f;
								f_78_ -= f_74_ * f;
								f_79_ -= f_76_ * f;
								f = 0.0F;
							}
							if (f_43_ < 0.0F) {
								f_46_ -= f_65_ * f_43_;
								f_43_ = 0.0F;
							}
							if (f != f_43_ && f_67_ < f_66_ || f == f_43_ && f_67_ > f_65_) {
								f_44_ -= f_43_;
								f_43_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_43_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_47_, (int) f_45_, f_48_, f_69_, f_77_, f_71_, f_78_, f_73_, f_79_, f_75_);
									f_47_ += f_67_;
									f_45_ += f_66_;
									f_48_ += f_70_;
									f_77_ += f_72_;
									f_78_ += f_74_;
									f_79_ += f_76_;
									f += (float) anInt98;
								}
								while (--f_44_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_47_, (int) f_46_, f_48_, f_69_, f_77_, f_71_, f_78_, f_73_, f_79_, f_75_);
									f_47_ += f_67_;
									f_46_ += f_65_;
									f_48_ += f_70_;
									f_77_ += f_72_;
									f_78_ += f_74_;
									f_79_ += f_76_;
									f += (float) anInt98;
								}
							} else {
								f_44_ -= f_43_;
								f_43_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_43_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_45_, (int) f_47_, f_48_, f_69_, f_77_, f_71_, f_78_, f_73_, f_79_, f_75_);
									f_47_ += f_67_;
									f_45_ += f_66_;
									f_48_ += f_70_;
									f_77_ += f_72_;
									f_78_ += f_74_;
									f_79_ += f_76_;
									f += (float) anInt98;
								}
								while (--f_44_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_46_, (int) f_47_, f_48_, f_69_, f_77_, f_71_, f_78_, f_73_, f_79_, f_75_);
									f_47_ += f_67_;
									f_46_ += f_65_;
									f_48_ += f_70_;
									f_77_ += f_72_;
									f_78_ += f_74_;
									f_79_ += f_76_;
									f += (float) anInt98;
								}
							}
						} else {
							f_46_ = f_45_;
							if (f < 0.0F) {
								f_46_ -= f_67_ * f;
								f_45_ -= f_66_ * f;
								f_48_ -= f_70_ * f;
								f_77_ -= f_72_ * f;
								f_78_ -= f_74_ * f;
								f_79_ -= f_76_ * f;
								f = 0.0F;
							}
							if (f_44_ < 0.0F) {
								f_47_ -= f_65_ * f_44_;
								f_44_ = 0.0F;
							}
							if (f != f_44_ && f_67_ < f_66_ || f == f_44_ && f_65_ > f_66_) {
								f_43_ -= f_44_;
								f_44_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_44_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_46_, (int) f_45_, f_48_, f_69_, f_77_, f_71_, f_78_, f_73_, f_79_, f_75_);
									f_46_ += f_67_;
									f_45_ += f_66_;
									f_48_ += f_70_;
									f_77_ += f_72_;
									f_78_ += f_74_;
									f_79_ += f_76_;
									f += (float) anInt98;
								}
								while (--f_43_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_47_, (int) f_45_, f_48_, f_69_, f_77_, f_71_, f_78_, f_73_, f_79_, f_75_);
									f_47_ += f_65_;
									f_45_ += f_66_;
									f_48_ += f_70_;
									f_77_ += f_72_;
									f_78_ += f_74_;
									f_79_ += f_76_;
									f += (float) anInt98;
								}
							} else {
								f_43_ -= f_44_;
								f_44_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_44_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_45_, (int) f_46_, f_48_, f_69_, f_77_, f_71_, f_78_, f_73_, f_79_, f_75_);
									f_46_ += f_67_;
									f_45_ += f_66_;
									f_48_ += f_70_;
									f_77_ += f_72_;
									f_78_ += f_74_;
									f_79_ += f_76_;
									f += (float) anInt98;
								}
								while (--f_43_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_45_, (int) f_47_, f_48_, f_69_, f_77_, f_71_, f_78_, f_73_, f_79_, f_75_);
									f_47_ += f_65_;
									f_45_ += f_66_;
									f_48_ += f_70_;
									f_77_ += f_72_;
									f_78_ += f_74_;
									f_79_ += f_76_;
									f += (float) anInt98;
								}
							}
						}
					}
				} else if (f_43_ <= f_44_) {
					if (!(f_43_ >= (float) anInt101)) {
						if (f_44_ > (float) anInt101)
							f_44_ = (float) anInt101;
						if (f > (float) anInt101)
							f = (float) anInt101;
						f_49_ = f_49_ - f_69_ * f_46_ + f_69_;
						float f_80_ = ((float) (i_51_ & 0xff0000) - f_71_ * f_46_ + f_71_);
						float f_81_ = (float) (i_51_ & 0xff00) - f_73_ * f_46_ + f_73_;
						float f_82_ = (float) (i_51_ & 0xff) - f_75_ * f_46_ + f_75_;
						if (f_44_ < f) {
							f_45_ = f_46_;
							if (f_43_ < 0.0F) {
								f_45_ -= f_66_ * f_43_;
								f_46_ -= f_65_ * f_43_;
								f_49_ -= f_70_ * f_43_;
								f_80_ -= f_72_ * f_43_;
								f_81_ -= f_74_ * f_43_;
								f_82_ -= f_76_ * f_43_;
								f_43_ = 0.0F;
							}
							if (f_44_ < 0.0F) {
								f_47_ -= f_67_ * f_44_;
								f_44_ = 0.0F;
							}
							if (f_43_ != f_44_ && f_66_ < f_65_ || f_43_ == f_44_ && f_66_ > f_67_) {
								f -= f_44_;
								f_44_ -= f_43_;
								f_43_ = (float) anIntArray97[(int) f_43_];
								while (--f_44_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f_43_, 0, 0, (int) f_45_, (int) f_46_, f_49_, f_69_, f_80_, f_71_, f_81_, f_73_, f_82_, f_75_);
									f_45_ += f_66_;
									f_46_ += f_65_;
									f_49_ += f_70_;
									f_80_ += f_72_;
									f_81_ += f_74_;
									f_82_ += f_76_;
									f_43_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f_43_, 0, 0, (int) f_45_, (int) f_47_, f_49_, f_69_, f_80_, f_71_, f_81_, f_73_, f_82_, f_75_);
									f_45_ += f_66_;
									f_47_ += f_67_;
									f_49_ += f_70_;
									f_80_ += f_72_;
									f_81_ += f_74_;
									f_82_ += f_76_;
									f_43_ += (float) anInt98;
								}
							} else {
								f -= f_44_;
								f_44_ -= f_43_;
								f_43_ = (float) anIntArray97[(int) f_43_];
								while (--f_44_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f_43_, 0, 0, (int) f_46_, (int) f_45_, f_49_, f_69_, f_80_, f_71_, f_81_, f_73_, f_82_, f_75_);
									f_45_ += f_66_;
									f_46_ += f_65_;
									f_49_ += f_70_;
									f_80_ += f_72_;
									f_81_ += f_74_;
									f_82_ += f_76_;
									f_43_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f_43_, 0, 0, (int) f_47_, (int) f_45_, f_49_, f_69_, f_80_, f_71_, f_81_, f_73_, f_82_, f_75_);
									f_45_ += f_66_;
									f_47_ += f_67_;
									f_49_ += f_70_;
									f_80_ += f_72_;
									f_81_ += f_74_;
									f_82_ += f_76_;
									f_43_ += (float) anInt98;
								}
							}
						} else {
							f_47_ = f_46_;
							if (f_43_ < 0.0F) {
								f_47_ -= f_66_ * f_43_;
								f_46_ -= f_65_ * f_43_;
								f_49_ -= f_70_ * f_43_;
								f_80_ -= f_72_ * f_43_;
								f_81_ -= f_74_ * f_43_;
								f_82_ -= f_76_ * f_43_;
								f_43_ = 0.0F;
							}
							if (f < 0.0F) {
								f_45_ -= f_67_ * f;
								f = 0.0F;
							}
							if (f_66_ < f_65_) {
								f_44_ -= f;
								f -= f_43_;
								f_43_ = (float) anIntArray97[(int) f_43_];
								while (--f >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f_43_, 0, 0, (int) f_47_, (int) f_46_, f_49_, f_69_, f_80_, f_71_, f_81_, f_73_, f_82_, f_75_);
									f_47_ += f_66_;
									f_46_ += f_65_;
									f_49_ += f_70_;
									f_80_ += f_72_;
									f_81_ += f_74_;
									f_82_ += f_76_;
									f_43_ += (float) anInt98;
								}
								while (--f_44_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f_43_, 0, 0, (int) f_45_, (int) f_46_, f_49_, f_69_, f_80_, f_71_, f_81_, f_73_, f_82_, f_75_);
									f_45_ += f_67_;
									f_46_ += f_65_;
									f_49_ += f_70_;
									f_80_ += f_72_;
									f_81_ += f_74_;
									f_82_ += f_76_;
									f_43_ += (float) anInt98;
								}
							} else {
								f_44_ -= f;
								f -= f_43_;
								f_43_ = (float) anIntArray97[(int) f_43_];
								while (--f >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f_43_, 0, 0, (int) f_46_, (int) f_47_, f_49_, f_69_, f_80_, f_71_, f_81_, f_73_, f_82_, f_75_);
									f_47_ += f_66_;
									f_46_ += f_65_;
									f_49_ += f_70_;
									f_80_ += f_72_;
									f_81_ += f_74_;
									f_82_ += f_76_;
									f_43_ += (float) anInt98;
								}
								while (--f_44_ >= 0.0F) {
									method218(anIntArray99, aFloatArray105, (int) f_43_, 0, 0, (int) f_46_, (int) f_45_, f_49_, f_69_, f_80_, f_71_, f_81_, f_73_, f_82_, f_75_);
									f_45_ += f_67_;
									f_46_ += f_65_;
									f_49_ += f_70_;
									f_80_ += f_72_;
									f_81_ += f_74_;
									f_82_ += f_76_;
									f_43_ += (float) anInt98;
								}
							}
						}
					}
				} else if (!(f_44_ >= (float) anInt101)) {
					if (f > (float) anInt101)
						f = (float) anInt101;
					if (f_43_ > (float) anInt101)
						f_43_ = (float) anInt101;
					f_50_ = f_50_ - f_69_ * f_47_ + f_69_;
					float f_83_ = (float) (i_52_ & 0xff0000) - f_71_ * f_47_ + f_71_;
					float f_84_ = (float) (i_52_ & 0xff00) - f_73_ * f_47_ + f_73_;
					float f_85_ = (float) (i_52_ & 0xff) - f_75_ * f_47_ + f_75_;
					if (f < f_43_) {
						f_46_ = f_47_;
						if (f_44_ < 0.0F) {
							f_46_ -= f_65_ * f_44_;
							f_47_ -= f_67_ * f_44_;
							f_50_ -= f_70_ * f_44_;
							f_83_ -= f_72_ * f_44_;
							f_84_ -= f_74_ * f_44_;
							f_85_ -= f_76_ * f_44_;
							f_44_ = 0.0F;
						}
						if (f < 0.0F) {
							f_45_ -= f_66_ * f;
							f = 0.0F;
						}
						if (f_65_ < f_67_) {
							f_43_ -= f;
							f -= f_44_;
							f_44_ = (float) anIntArray97[(int) f_44_];
							while (--f >= 0.0F) {
								method218(anIntArray99, aFloatArray105, (int) f_44_, 0, 0, (int) f_46_, (int) f_47_, f_50_, f_69_, f_83_, f_71_, f_84_, f_73_, f_85_, f_75_);
								f_46_ += f_65_;
								f_47_ += f_67_;
								f_50_ += f_70_;
								f_83_ += f_72_;
								f_84_ += f_74_;
								f_85_ += f_76_;
								f_44_ += (float) anInt98;
							}
							while (--f_43_ >= 0.0F) {
								method218(anIntArray99, aFloatArray105, (int) f_44_, 0, 0, (int) f_46_, (int) f_45_, f_50_, f_69_, f_83_, f_71_, f_84_, f_73_, f_85_, f_75_);
								f_46_ += f_65_;
								f_45_ += f_66_;
								f_50_ += f_70_;
								f_83_ += f_72_;
								f_84_ += f_74_;
								f_85_ += f_76_;
								f_44_ += (float) anInt98;
							}
						} else {
							f_43_ -= f;
							f -= f_44_;
							f_44_ = (float) anIntArray97[(int) f_44_];
							while (--f >= 0.0F) {
								method218(anIntArray99, aFloatArray105, (int) f_44_, 0, 0, (int) f_47_, (int) f_46_, f_50_, f_69_, f_83_, f_71_, f_84_, f_73_, f_85_, f_75_);
								f_46_ += f_65_;
								f_47_ += f_67_;
								f_50_ += f_70_;
								f_83_ += f_72_;
								f_84_ += f_74_;
								f_85_ += f_76_;
								f_44_ += (float) anInt98;
							}
							while (--f_43_ >= 0.0F) {
								method218(anIntArray99, aFloatArray105, (int) f_44_, 0, 0, (int) f_45_, (int) f_46_, f_50_, f_69_, f_83_, f_71_, f_84_, f_73_, f_85_, f_75_);
								f_46_ += f_65_;
								f_45_ += f_66_;
								f_50_ += f_70_;
								f_83_ += f_72_;
								f_84_ += f_74_;
								f_85_ += f_76_;
								f_44_ += (float) anInt98;
							}
						}
					} else {
						f_45_ = f_47_;
						if (f_44_ < 0.0F) {
							f_45_ -= f_65_ * f_44_;
							f_47_ -= f_67_ * f_44_;
							f_50_ -= f_70_ * f_44_;
							f_83_ -= f_72_ * f_44_;
							f_84_ -= f_74_ * f_44_;
							f_85_ -= f_76_ * f_44_;
							f_44_ = 0.0F;
						}
						if (f_43_ < 0.0F) {
							f_46_ -= f_66_ * f_43_;
							f_43_ = 0.0F;
						}
						if (f_65_ < f_67_) {
							f -= f_43_;
							f_43_ -= f_44_;
							f_44_ = (float) anIntArray97[(int) f_44_];
							while (--f_43_ >= 0.0F) {
								method218(anIntArray99, aFloatArray105, (int) f_44_, 0, 0, (int) f_45_, (int) f_47_, f_50_, f_69_, f_83_, f_71_, f_84_, f_73_, f_85_, f_75_);
								f_45_ += f_65_;
								f_47_ += f_67_;
								f_50_ += f_70_;
								f_83_ += f_72_;
								f_84_ += f_74_;
								f_85_ += f_76_;
								f_44_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method218(anIntArray99, aFloatArray105, (int) f_44_, 0, 0, (int) f_46_, (int) f_47_, f_50_, f_69_, f_83_, f_71_, f_84_, f_73_, f_85_, f_75_);
								f_46_ += f_66_;
								f_47_ += f_67_;
								f_50_ += f_70_;
								f_83_ += f_72_;
								f_84_ += f_74_;
								f_85_ += f_76_;
								f_44_ += (float) anInt98;
							}
						} else {
							f -= f_43_;
							f_43_ -= f_44_;
							f_44_ = (float) anIntArray97[(int) f_44_];
							while (--f_43_ >= 0.0F) {
								method218(anIntArray99, aFloatArray105, (int) f_44_, 0, 0, (int) f_47_, (int) f_45_, f_50_, f_69_, f_83_, f_71_, f_84_, f_73_, f_85_, f_75_);
								f_45_ += f_65_;
								f_47_ += f_67_;
								f_50_ += f_70_;
								f_83_ += f_72_;
								f_84_ += f_74_;
								f_85_ += f_76_;
								f_44_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method218(anIntArray99, aFloatArray105, (int) f_44_, 0, 0, (int) f_47_, (int) f_46_, f_50_, f_69_, f_83_, f_71_, f_84_, f_73_, f_85_, f_75_);
								f_46_ += f_66_;
								f_47_ += f_67_;
								f_50_ += f_70_;
								f_83_ += f_72_;
								f_84_ += f_74_;
								f_85_ += f_76_;
								f_44_ += (float) anInt98;
							}
						}
					}
				}
			}
		}
	}

	private final void method200(int[] is, float[] fs, int i, int i_86_, int i_87_, int i_88_, int i_89_, float f, float f_90_, float f_91_, float f_92_, float f_93_, float f_94_, float f_95_, float f_96_) {
		if (aBoolean103) {
			if (i_89_ > anInt107)
				i_89_ = anInt107;
			if (i_88_ < 0)
				i_88_ = 0;
		}
		if (i_88_ < i_89_) {
			if (aBoolean110) {
				i += i_88_;
				f_91_ += f_92_ * (float) i_88_;
				f_93_ += f_94_ * (float) i_88_;
				f_95_ += f_96_ * (float) i_88_;
				if (aBoolean102) {
					i_87_ = i_89_ - i_88_ >> 2;
					f_92_ *= 4.0F;
					f_94_ *= 4.0F;
					f_96_ *= 4.0F;
					if (anInt108 == 0) {
						if (i_87_ > 0) {
							do {
								i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
								f_91_ += f_92_;
								f_93_ += f_94_;
								f_95_ += f_96_;
								is[i++] = i_86_;
								is[i++] = i_86_;
								is[i++] = i_86_;
								is[i++] = i_86_;
							} while (--i_87_ > 0);
						}
						i_87_ = i_89_ - i_88_ & 0x3;
						if (i_87_ > 0) {
							i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
							do
								is[i++] = i_86_;
							while (--i_87_ > 0);
						}
					} else if (!aBoolean106) {
						int i_97_ = anInt108;
						int i_98_ = 256 - anInt108;
						if (i_87_ > 0) {
							do {
								i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
								f_91_ += f_92_;
								f_93_ += f_94_;
								f_95_ += f_96_;
								i_86_ = (((i_86_ & 0xff00ff) * i_98_ >> 8 & 0xff00ff) + ((i_86_ & 0xff00) * i_98_ >> 8 & 0xff00));
								int i_99_ = is[i];
								is[i++] = ((i_98_ | i_99_ >> 24) << 24 | (i_86_ + ((i_99_ & 0xff00ff) * i_97_ >> 8 & 0xff00ff) + ((i_99_ & 0xff00) * i_97_ >> 8 & 0xff00)));
								i_99_ = is[i];
								is[i++] = ((i_98_ | i_99_ >> 24) << 24 | (i_86_ + ((i_99_ & 0xff00ff) * i_97_ >> 8 & 0xff00ff) + ((i_99_ & 0xff00) * i_97_ >> 8 & 0xff00)));
								i_99_ = is[i];
								is[i++] = ((i_98_ | i_99_ >> 24) << 24 | (i_86_ + ((i_99_ & 0xff00ff) * i_97_ >> 8 & 0xff00ff) + ((i_99_ & 0xff00) * i_97_ >> 8 & 0xff00)));
								i_99_ = is[i];
								is[i++] = ((i_98_ | i_99_ >> 24) << 24 | (i_86_ + ((i_99_ & 0xff00ff) * i_97_ >> 8 & 0xff00ff) + ((i_99_ & 0xff00) * i_97_ >> 8 & 0xff00)));
							} while (--i_87_ > 0);
						}
						i_87_ = i_89_ - i_88_ & 0x3;
						if (i_87_ > 0) {
							i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
							i_86_ = (((i_86_ & 0xff00ff) * i_98_ >> 8 & 0xff00ff) + ((i_86_ & 0xff00) * i_98_ >> 8 & 0xff00));
							do {
								int i_100_ = is[i];
								is[i++] = ((i_98_ | i_100_ >> 24) << 24 | (i_86_ + ((i_100_ & 0xff00ff) * i_97_ >> 8 & 0xff00ff) + ((i_100_ & 0xff00) * i_97_ >> 8 & 0xff00)));
							} while (--i_87_ > 0);
						}
					} else {
						if (i_87_ > 0) {
							do {
								i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff);
								f_91_ += f_92_;
								f_93_ += f_94_;
								f_95_ += f_96_;
								int[] is_101_ = is;
								int i_102_ = i++;
								int i_103_ = i_86_;
								int i_104_ = is_101_[i_102_];
								int i_105_ = i_103_ + i_104_;
								int i_106_ = ((i_103_ & 0xff00ff) + (i_104_ & 0xff00ff));
								i_104_ = (i_106_ & 0x1000100) + (i_105_ - i_106_ & 0x10000);
								is_101_[i_102_] = (i_105_ - i_104_ | ~0xffffff | i_104_ - (i_104_ >>> 8));
								int[] is_107_ = is;
								int i_108_ = i++;
								int i_109_ = i_86_;
								int i_110_ = is_107_[i_108_];
								int i_111_ = i_109_ + i_110_;
								int i_112_ = ((i_109_ & 0xff00ff) + (i_110_ & 0xff00ff));
								i_110_ = (i_112_ & 0x1000100) + (i_111_ - i_112_ & 0x10000);
								is_107_[i_108_] = (i_111_ - i_110_ | ~0xffffff | i_110_ - (i_110_ >>> 8));
								int[] is_113_ = is;
								int i_114_ = i++;
								int i_115_ = i_86_;
								int i_116_ = is_113_[i_114_];
								int i_117_ = i_115_ + i_116_;
								int i_118_ = ((i_115_ & 0xff00ff) + (i_116_ & 0xff00ff));
								i_116_ = (i_118_ & 0x1000100) + (i_117_ - i_118_ & 0x10000);
								is_113_[i_114_] = (i_117_ - i_116_ | ~0xffffff | i_116_ - (i_116_ >>> 8));
								int[] is_119_ = is;
								int i_120_ = i++;
								int i_121_ = i_86_;
								int i_122_ = is_119_[i_120_];
								int i_123_ = i_121_ + i_122_;
								int i_124_ = ((i_121_ & 0xff00ff) + (i_122_ & 0xff00ff));
								i_122_ = (i_124_ & 0x1000100) + (i_123_ - i_124_ & 0x10000);
								is_119_[i_120_] = (i_123_ - i_122_ | ~0xffffff | i_122_ - (i_122_ >>> 8));
							} while (--i_87_ > 0);
						}
						i_87_ = i_89_ - i_88_ & 0x3;
						if (i_87_ > 0) {
							i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff);
							do {
								int[] is_125_ = is;
								int i_126_ = i++;
								int i_127_ = i_86_;
								int i_128_ = is_125_[i_126_];
								int i_129_ = i_127_ + i_128_;
								int i_130_ = ((i_127_ & 0xff00ff) + (i_128_ & 0xff00ff));
								i_128_ = (i_130_ & 0x1000100) + (i_129_ - i_130_ & 0x10000);
								is_125_[i_126_] = (i_129_ - i_128_ | ~0xffffff | i_128_ - (i_128_ >>> 8));
							} while (--i_87_ > 0);
						}
					}
				} else {
					i_87_ = i_89_ - i_88_;
					if (anInt108 == 0) {
						do {
							is[i++] = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
							f_91_ += f_92_;
							f_93_ += f_94_;
							f_95_ += f_96_;
						} while (--i_87_ > 0);
					} else if (!aBoolean106) {
						int i_131_ = anInt108;
						int i_132_ = 256 - anInt108;
						do {
							i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
							f_91_ += f_92_;
							f_93_ += f_94_;
							f_95_ += f_96_;
							i_86_ = (((i_86_ & 0xff00ff) * i_132_ >> 8 & 0xff00ff) + ((i_86_ & 0xff00) * i_132_ >> 8 & 0xff00));
							int i_133_ = is[i];
							is[i++] = ((i_132_ | i_133_ >> 24) << 24 | (i_86_ + ((i_133_ & 0xff00ff) * i_131_ >> 8 & 0xff00ff) + ((i_133_ & 0xff00) * i_131_ >> 8 & 0xff00)));
						} while (--i_87_ > 0);
					} else {
						do {
							int[] is_134_ = is;
							int i_135_ = i++;
							int i_136_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff);
							int i_137_ = is_134_[i_135_];
							int i_138_ = i_136_ + i_137_;
							int i_139_ = (i_136_ & 0xff00ff) + (i_137_ & 0xff00ff);
							i_137_ = (i_139_ & 0x1000100) + (i_138_ - i_139_ & 0x10000);
							is_134_[i_135_] = (i_138_ - i_137_ | ~0xffffff | i_137_ - (i_137_ >>> 8));
							f_91_ += f_92_;
							f_93_ += f_94_;
							f_95_ += f_96_;
						} while (--i_87_ > 0);
					}
				}
			} else {
				i += i_88_ - 1;
				f += f_90_ * (float) i_88_;
				f_91_ += f_92_ * (float) i_88_;
				f_93_ += f_94_ * (float) i_88_;
				f_95_ += f_96_ * (float) i_88_;
				if (aClass240_104.aBoolean2262) {
					if (aBoolean102) {
						i_87_ = i_89_ - i_88_ >> 2;
						f_92_ *= 4.0F;
						f_94_ *= 4.0F;
						f_96_ *= 4.0F;
						if (anInt108 == 0) {
							if (i_87_ > 0) {
								do {
									i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
									f_91_ += f_92_;
									f_93_ += f_94_;
									f_95_ += f_96_;
									if (f < fs[++i]) {
										is[i] = i_86_;
										fs[i] = f;
									}
									f += f_90_;
									if (f < fs[++i]) {
										is[i] = i_86_;
										fs[i] = f;
									}
									f += f_90_;
									if (f < fs[++i]) {
										is[i] = i_86_;
										fs[i] = f;
									}
									f += f_90_;
									if (f < fs[++i]) {
										is[i] = i_86_;
										fs[i] = f;
									}
									f += f_90_;
								} while (--i_87_ > 0);
							}
							i_87_ = i_89_ - i_88_ & 0x3;
							if (i_87_ > 0) {
								i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
								do {
									if (f < fs[++i]) {
										is[i] = i_86_;
										fs[i] = f;
									}
									f += f_90_;
								} while (--i_87_ > 0);
							}
						} else if (!aBoolean106) {
							int i_140_ = anInt108;
							int i_141_ = 256 - anInt108;
							if (i_87_ > 0) {
								do {
									i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
									f_91_ += f_92_;
									f_93_ += f_94_;
									f_95_ += f_96_;
									i_86_ = (((i_86_ & 0xff00ff) * i_141_ >> 8 & 0xff00ff) + ((i_86_ & 0xff00) * i_141_ >> 8 & 0xff00));
									if (f < fs[++i]) {
										int i_142_ = is[i];
										is[i] = ((i_141_ | i_142_ >> 24) << 24 | (i_86_ + (((i_142_ & 0xff00ff) * i_140_) >> 8 & 0xff00ff) + (((i_142_ & 0xff00) * i_140_) >> 8 & 0xff00)));
										fs[i] = f;
									}
									f += f_90_;
									if (f < fs[++i]) {
										int i_143_ = is[i];
										is[i] = ((i_141_ | i_143_ >> 24) << 24 | (i_86_ + (((i_143_ & 0xff00ff) * i_140_) >> 8 & 0xff00ff) + (((i_143_ & 0xff00) * i_140_) >> 8 & 0xff00)));
										fs[i] = f;
									}
									f += f_90_;
									if (f < fs[++i]) {
										int i_144_ = is[i];
										is[i] = ((i_141_ | i_144_ >> 24) << 24 | (i_86_ + (((i_144_ & 0xff00ff) * i_140_) >> 8 & 0xff00ff) + (((i_144_ & 0xff00) * i_140_) >> 8 & 0xff00)));
										fs[i] = f;
									}
									f += f_90_;
									if (f < fs[++i]) {
										int i_145_ = is[i];
										is[i] = ((i_141_ | i_145_ >> 24) << 24 | (i_86_ + (((i_145_ & 0xff00ff) * i_140_) >> 8 & 0xff00ff) + (((i_145_ & 0xff00) * i_140_) >> 8 & 0xff00)));
										fs[i] = f;
									}
									f += f_90_;
								} while (--i_87_ > 0);
							}
							i_87_ = i_89_ - i_88_ & 0x3;
							if (i_87_ > 0) {
								i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
								i_86_ = (((i_86_ & 0xff00ff) * i_141_ >> 8 & 0xff00ff) + ((i_86_ & 0xff00) * i_141_ >> 8 & 0xff00));
								do {
									if (f < fs[++i]) {
										int i_146_ = is[i];
										is[i] = ((i_141_ | i_146_ >> 24) << 24 | (i_86_ + (((i_146_ & 0xff00ff) * i_140_) >> 8 & 0xff00ff) + (((i_146_ & 0xff00) * i_140_) >> 8 & 0xff00)));
										fs[i] = f;
									}
									f += f_90_;
								} while (--i_87_ > 0);
							}
						} else {
							if (i_87_ > 0) {
								do {
									i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff);
									f_91_ += f_92_;
									f_93_ += f_94_;
									f_95_ += f_96_;
									if (f < fs[++i]) {
										int[] is_147_ = is;
										int i_148_ = i;
										int i_149_ = i_86_;
										int i_150_ = is_147_[i_148_];
										int i_151_ = i_149_ + i_150_;
										int i_152_ = ((i_149_ & 0xff00ff) + (i_150_ & 0xff00ff));
										i_150_ = ((i_152_ & 0x1000100) + (i_151_ - i_152_ & 0x10000));
										is_147_[i_148_] = (i_151_ - i_150_ | ~0xffffff | i_150_ - (i_150_ >>> 8));
										fs[i] = f;
									}
									f += f_90_;
									if (f < fs[++i]) {
										int[] is_153_ = is;
										int i_154_ = i;
										int i_155_ = i_86_;
										int i_156_ = is_153_[i_154_];
										int i_157_ = i_155_ + i_156_;
										int i_158_ = ((i_155_ & 0xff00ff) + (i_156_ & 0xff00ff));
										i_156_ = ((i_158_ & 0x1000100) + (i_157_ - i_158_ & 0x10000));
										is_153_[i_154_] = (i_157_ - i_156_ | ~0xffffff | i_156_ - (i_156_ >>> 8));
										fs[i] = f;
									}
									f += f_90_;
									if (f < fs[++i]) {
										int[] is_159_ = is;
										int i_160_ = i;
										int i_161_ = i_86_;
										int i_162_ = is_159_[i_160_];
										int i_163_ = i_161_ + i_162_;
										int i_164_ = ((i_161_ & 0xff00ff) + (i_162_ & 0xff00ff));
										i_162_ = ((i_164_ & 0x1000100) + (i_163_ - i_164_ & 0x10000));
										is_159_[i_160_] = (i_163_ - i_162_ | ~0xffffff | i_162_ - (i_162_ >>> 8));
										fs[i] = f;
									}
									f += f_90_;
									if (f < fs[++i]) {
										int[] is_165_ = is;
										int i_166_ = i;
										int i_167_ = i_86_;
										int i_168_ = is_165_[i_166_];
										int i_169_ = i_167_ + i_168_;
										int i_170_ = ((i_167_ & 0xff00ff) + (i_168_ & 0xff00ff));
										i_168_ = ((i_170_ & 0x1000100) + (i_169_ - i_170_ & 0x10000));
										is_165_[i_166_] = (i_169_ - i_168_ | ~0xffffff | i_168_ - (i_168_ >>> 8));
										fs[i] = f;
									}
									f += f_90_;
								} while (--i_87_ > 0);
							}
							i_87_ = i_89_ - i_88_ & 0x3;
							if (i_87_ > 0) {
								i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff);
								do {
									if (f < fs[++i]) {
										int[] is_171_ = is;
										int i_172_ = i;
										int i_173_ = i_86_;
										int i_174_ = is_171_[i_172_];
										int i_175_ = i_173_ + i_174_;
										int i_176_ = ((i_173_ & 0xff00ff) + (i_174_ & 0xff00ff));
										i_174_ = ((i_176_ & 0x1000100) + (i_175_ - i_176_ & 0x10000));
										is_171_[i_172_] = (i_175_ - i_174_ | ~0xffffff | i_174_ - (i_174_ >>> 8));
										fs[i] = f;
									}
									f += f_90_;
								} while (--i_87_ > 0);
							}
						}
					} else {
						i_87_ = i_89_ - i_88_;
						if (anInt108 == 0) {
							do {
								if (f < fs[++i]) {
									is[i] = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
									fs[i] = f;
								}
								f += f_90_;
								f_91_ += f_92_;
								f_93_ += f_94_;
								f_95_ += f_96_;
							} while (--i_87_ > 0);
						} else if (!aBoolean106) {
							int i_177_ = anInt108;
							int i_178_ = 256 - anInt108;
							do {
								if (f < fs[++i]) {
									i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
									i_86_ = (((i_86_ & 0xff00ff) * i_178_ >> 8 & 0xff00ff) + ((i_86_ & 0xff00) * i_178_ >> 8 & 0xff00));
									int i_179_ = is[i];
									is[i] = ((i_178_ | i_179_ >> 24) << 24 | (i_86_ + (((i_179_ & 0xff00ff) * i_177_ >> 8) & 0xff00ff) + (((i_179_ & 0xff00) * i_177_ >> 8) & 0xff00)));
									fs[i] = f;
								}
								f += f_90_;
								f_91_ += f_92_;
								f_93_ += f_94_;
								f_95_ += f_96_;
							} while (--i_87_ > 0);
						} else {
							do {
								if (f < fs[++i]) {
									int[] is_180_ = is;
									int i_181_ = i;
									int i_182_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff);
									int i_183_ = is_180_[i_181_];
									int i_184_ = i_182_ + i_183_;
									int i_185_ = ((i_182_ & 0xff00ff) + (i_183_ & 0xff00ff));
									i_183_ = ((i_185_ & 0x1000100) + (i_184_ - i_185_ & 0x10000));
									is_180_[i_181_] = (i_184_ - i_183_ | ~0xffffff | i_183_ - (i_183_ >>> 8));
									fs[i] = f;
								}
								f += f_90_;
								f_91_ += f_92_;
								f_93_ += f_94_;
								f_95_ += f_96_;
							} while (--i_87_ > 0);
						}
					}
				} else if (aBoolean102) {
					i_87_ = i_89_ - i_88_ >> 2;
					f_92_ *= 4.0F;
					f_94_ *= 4.0F;
					f_96_ *= 4.0F;
					if (anInt108 == 0) {
						if (i_87_ > 0) {
							do {
								i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
								f_91_ += f_92_;
								f_93_ += f_94_;
								f_95_ += f_96_;
								if (f < fs[++i])
									is[i] = i_86_;
								f += f_90_;
								if (f < fs[++i])
									is[i] = i_86_;
								f += f_90_;
								if (f < fs[++i])
									is[i] = i_86_;
								f += f_90_;
								if (f < fs[++i])
									is[i] = i_86_;
								f += f_90_;
							} while (--i_87_ > 0);
						}
						i_87_ = i_89_ - i_88_ & 0x3;
						if (i_87_ > 0) {
							i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
							do {
								if (f < fs[++i])
									is[i] = i_86_;
								f += f_90_;
							} while (--i_87_ > 0);
						}
					} else if (!aBoolean106) {
						int i_186_ = anInt108;
						int i_187_ = 256 - anInt108;
						if (i_87_ > 0) {
							do {
								i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
								f_91_ += f_92_;
								f_93_ += f_94_;
								f_95_ += f_96_;
								i_86_ = (((i_86_ & 0xff00ff) * i_187_ >> 8 & 0xff00ff) + ((i_86_ & 0xff00) * i_187_ >> 8 & 0xff00));
								if (f < fs[++i]) {
									int i_188_ = is[i];
									is[i] = ((i_187_ | i_188_ >> 24) << 24 | (i_86_ + (((i_188_ & 0xff00ff) * i_186_ >> 8) & 0xff00ff) + (((i_188_ & 0xff00) * i_186_ >> 8) & 0xff00)));
								}
								f += f_90_;
								if (f < fs[++i]) {
									int i_189_ = is[i];
									is[i] = ((i_187_ | i_189_ >> 24) << 24 | (i_86_ + (((i_189_ & 0xff00ff) * i_186_ >> 8) & 0xff00ff) + (((i_189_ & 0xff00) * i_186_ >> 8) & 0xff00)));
								}
								f += f_90_;
								if (f < fs[++i]) {
									int i_190_ = is[i];
									is[i] = ((i_187_ | i_190_ >> 24) << 24 | (i_86_ + (((i_190_ & 0xff00ff) * i_186_ >> 8) & 0xff00ff) + (((i_190_ & 0xff00) * i_186_ >> 8) & 0xff00)));
								}
								f += f_90_;
								if (f < fs[++i]) {
									int i_191_ = is[i];
									is[i] = ((i_187_ | i_191_ >> 24) << 24 | (i_86_ + (((i_191_ & 0xff00ff) * i_186_ >> 8) & 0xff00ff) + (((i_191_ & 0xff00) * i_186_ >> 8) & 0xff00)));
								}
								f += f_90_;
							} while (--i_87_ > 0);
						}
						i_87_ = i_89_ - i_88_ & 0x3;
						if (i_87_ > 0) {
							i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
							i_86_ = (((i_86_ & 0xff00ff) * i_187_ >> 8 & 0xff00ff) + ((i_86_ & 0xff00) * i_187_ >> 8 & 0xff00));
							do {
								if (f < fs[++i]) {
									int i_192_ = is[i];
									is[i] = ((i_187_ | i_192_ >> 24) << 24 | (i_86_ + (((i_192_ & 0xff00ff) * i_186_ >> 8) & 0xff00ff) + (((i_192_ & 0xff00) * i_186_ >> 8) & 0xff00)));
								}
								f += f_90_;
							} while (--i_87_ > 0);
						}
					} else {
						if (i_87_ > 0) {
							do {
								i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff);
								f_91_ += f_92_;
								f_93_ += f_94_;
								f_95_ += f_96_;
								if (f < fs[++i]) {
									int[] is_193_ = is;
									int i_194_ = i;
									int i_195_ = i_86_;
									int i_196_ = is_193_[i_194_];
									int i_197_ = i_195_ + i_196_;
									int i_198_ = ((i_195_ & 0xff00ff) + (i_196_ & 0xff00ff));
									i_196_ = ((i_198_ & 0x1000100) + (i_197_ - i_198_ & 0x10000));
									is_193_[i_194_] = (i_197_ - i_196_ | ~0xffffff | i_196_ - (i_196_ >>> 8));
								}
								f += f_90_;
								if (f < fs[++i]) {
									int[] is_199_ = is;
									int i_200_ = i;
									int i_201_ = i_86_;
									int i_202_ = is_199_[i_200_];
									int i_203_ = i_201_ + i_202_;
									int i_204_ = ((i_201_ & 0xff00ff) + (i_202_ & 0xff00ff));
									i_202_ = ((i_204_ & 0x1000100) + (i_203_ - i_204_ & 0x10000));
									is_199_[i_200_] = (i_203_ - i_202_ | ~0xffffff | i_202_ - (i_202_ >>> 8));
								}
								f += f_90_;
								if (f < fs[++i]) {
									int[] is_205_ = is;
									int i_206_ = i;
									int i_207_ = i_86_;
									int i_208_ = is_205_[i_206_];
									int i_209_ = i_207_ + i_208_;
									int i_210_ = ((i_207_ & 0xff00ff) + (i_208_ & 0xff00ff));
									i_208_ = ((i_210_ & 0x1000100) + (i_209_ - i_210_ & 0x10000));
									is_205_[i_206_] = (i_209_ - i_208_ | ~0xffffff | i_208_ - (i_208_ >>> 8));
								}
								f += f_90_;
								if (f < fs[++i]) {
									int[] is_211_ = is;
									int i_212_ = i;
									int i_213_ = i_86_;
									int i_214_ = is_211_[i_212_];
									int i_215_ = i_213_ + i_214_;
									int i_216_ = ((i_213_ & 0xff00ff) + (i_214_ & 0xff00ff));
									i_214_ = ((i_216_ & 0x1000100) + (i_215_ - i_216_ & 0x10000));
									is_211_[i_212_] = (i_215_ - i_214_ | ~0xffffff | i_214_ - (i_214_ >>> 8));
								}
								f += f_90_;
							} while (--i_87_ > 0);
						}
						i_87_ = i_89_ - i_88_ & 0x3;
						if (i_87_ > 0) {
							i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff);
							do {
								if (f < fs[++i]) {
									int[] is_217_ = is;
									int i_218_ = i;
									int i_219_ = i_86_;
									int i_220_ = is_217_[i_218_];
									int i_221_ = i_219_ + i_220_;
									int i_222_ = ((i_219_ & 0xff00ff) + (i_220_ & 0xff00ff));
									i_220_ = ((i_222_ & 0x1000100) + (i_221_ - i_222_ & 0x10000));
									is_217_[i_218_] = (i_221_ - i_220_ | ~0xffffff | i_220_ - (i_220_ >>> 8));
								}
								f += f_90_;
							} while (--i_87_ > 0);
						}
					}
				} else {
					i_87_ = i_89_ - i_88_;
					if (anInt108 == 0) {
						do {
							if (f < fs[++i])
								is[i] = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
							f += f_90_;
							f_91_ += f_92_;
							f_93_ += f_94_;
							f_95_ += f_96_;
						} while (--i_87_ > 0);
					} else if (!aBoolean106) {
						int i_223_ = anInt108;
						int i_224_ = 256 - anInt108;
						do {
							if (f < fs[++i]) {
								i_86_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff | ~0xffffff);
								i_86_ = (((i_86_ & 0xff00ff) * i_224_ >> 8 & 0xff00ff) + ((i_86_ & 0xff00) * i_224_ >> 8 & 0xff00));
								int i_225_ = is[i];
								is[i] = ((i_224_ | i_225_ >> 24) << 24 | (i_86_ + ((i_225_ & 0xff00ff) * i_223_ >> 8 & 0xff00ff) + ((i_225_ & 0xff00) * i_223_ >> 8 & 0xff00)));
							}
							f += f_90_;
							f_91_ += f_92_;
							f_93_ += f_94_;
							f_95_ += f_96_;
						} while (--i_87_ > 0);
					} else {
						do {
							if (f < fs[++i]) {
								int[] is_226_ = is;
								int i_227_ = i;
								int i_228_ = ((int) f_91_ & 0xff0000 | (int) f_93_ & 0xff00 | (int) f_95_ & 0xff);
								int i_229_ = is_226_[i_227_];
								int i_230_ = i_228_ + i_229_;
								int i_231_ = ((i_228_ & 0xff00ff) + (i_229_ & 0xff00ff));
								i_229_ = (i_231_ & 0x1000100) + (i_230_ - i_231_ & 0x10000);
								is_226_[i_227_] = (i_230_ - i_229_ | ~0xffffff | i_229_ - (i_229_ >>> 8));
							}
							f += f_90_;
							f_91_ += f_92_;
							f_93_ += f_94_;
							f_95_ += f_96_;
						} while (--i_87_ > 0);
					}
				}
			}
		}
	}

	private final void method201(int[] is, int[] is_232_, int i, int i_233_, int i_234_, float f, float f_235_, float f_236_, float f_237_, float f_238_, float f_239_, float f_240_, float f_241_, float f_242_, float f_243_, float f_244_, float f_245_, float f_246_, float f_247_, float f_248_, float f_249_) {
		int i_250_ = i_234_ - i_233_;
		float f_251_ = 1.0F / (float) i_250_;
		float f_252_ = (f_235_ - f) * f_251_;
		float f_253_ = (f_237_ - f_236_) * f_251_;
		float f_254_ = (f_239_ - f_238_) * f_251_;
		float f_255_ = (f_241_ - f_240_) * f_251_;
		float f_256_ = (f_243_ - f_242_) * f_251_;
		float f_257_ = (f_245_ - f_244_) * f_251_;
		float f_258_ = (f_247_ - f_246_) * f_251_;
		float f_259_ = (f_249_ - f_248_) * f_251_;
		if (aBoolean103) {
			if (i_234_ > anInt107)
				i_234_ = anInt107;
			if (i_233_ < 0) {
				f -= f_252_ * (float) i_233_;
				f_236_ -= f_253_ * (float) i_233_;
				f_238_ -= f_254_ * (float) i_233_;
				f_240_ -= f_255_ * (float) i_233_;
				f_242_ -= f_256_ * (float) i_233_;
				f_244_ -= f_257_ * (float) i_233_;
				f_246_ -= f_258_ * (float) i_233_;
				f_248_ -= f_259_ * (float) i_233_;
				i_233_ = 0;
			}
		}
		if (i_233_ < i_234_) {
			i_250_ = i_234_ - i_233_;
			i += i_233_;
			while (i_250_-- > 0) {
				float f_260_ = 1.0F / f;
				if (f_260_ < aFloatArray105[i]) {
					int i_261_ = (int) (f_236_ * f_260_ * (float) anInt113);
					if (aBoolean119)
						i_261_ &= anInt112;
					else if (i_261_ < 0)
						i_261_ = 0;
					else if (i_261_ > anInt112)
						i_261_ = anInt112;
					int i_262_ = (int) (f_238_ * f_260_ * (float) anInt113);
					if (aBoolean119)
						i_262_ &= anInt112;
					else if (i_262_ < 0)
						i_262_ = 0;
					else if (i_262_ > anInt112)
						i_262_ = anInt112;
					int i_263_ = anIntArray123[i_262_ * anInt113 + i_261_];
					int i_264_ = 255;
					if (anInt115 == 2)
						i_264_ = i_263_ >> 24 & 0xff;
					else if (anInt115 == 1)
						i_264_ = i_263_ == 0 ? 0 : 255;
					else
						i_264_ = (int) f_242_;
					if (i_264_ != 0) {
						if (i_264_ != 255) {
							int i_265_ = (((int) (f_244_ * (float) (i_263_ >> 16 & 0xff)) << 8 & 0xff0000) | ~0xffffff | (int) (f_246_ * (float) (i_263_ >> 8 & 0xff)) & 0xff00 | ((int) (f_248_ * (float) (i_263_ & 0xff)) >> 8));
							if (f_240_ != 0.0F) {
								int i_266_ = (int) (255.0F - f_240_);
								int i_267_ = ((((anInt125 & 0xff00ff) * (int) f_240_ & ~0xff00ff) | ((anInt125 & 0xff00) * (int) f_240_ & 0xff0000)) >>> 8);
								i_265_ = ((((i_265_ & 0xff00ff) * i_266_ & ~0xff00ff) | ((i_265_ & 0xff00) * i_266_ & 0xff0000)) >>> 8) + i_267_;
							}
							int i_268_ = is[i];
							int i_269_ = 255 - i_264_;
							i_265_ = ((((i_268_ & 0xff00ff) * i_269_ + (i_265_ & 0xff00ff) * i_264_) & ~0xff00ff) + (((i_268_ & 0xff00) * i_269_ + (i_265_ & 0xff00) * i_264_) & 0xff0000)) >> 8;
							is[i] = (i_264_ | is[i] >> 24) << 24 | i_265_;
							aFloatArray105[i] = f_260_;
						} else {
							int i_270_ = (((int) (f_244_ * (float) (i_263_ >> 16 & 0xff)) << 8 & 0xff0000) | ~0xffffff | (int) (f_246_ * (float) (i_263_ >> 8 & 0xff)) & 0xff00 | ((int) (f_248_ * (float) (i_263_ & 0xff)) >> 8));
							if (f_240_ != 0.0F) {
								int i_271_ = (int) (255.0F - f_240_);
								int i_272_ = ((((anInt125 & 0xff00ff) * (int) f_240_ & ~0xff00ff) | ((anInt125 & 0xff00) * (int) f_240_ & 0xff0000)) >>> 8);
								i_270_ = ((((i_270_ & 0xff00ff) * i_271_ & ~0xff00ff) | ((i_270_ & 0xff00) * i_271_ & 0xff0000)) >>> 8) + i_272_;
							}
							is[i] = i_264_ << 24 | i_270_;
							aFloatArray105[i] = f_260_;
						}
					}
				}
				i++;
				f += f_252_;
				f_236_ += f_253_;
				f_238_ += f_254_;
				f_240_ += f_255_;
				f_242_ += f_256_;
				f_244_ += f_257_;
				f_246_ += f_258_;
				f_248_ += f_259_;
			}
		}
	}

	private final void method202(int[] is, float[] fs, int i, int i_273_, int i_274_, int i_275_, int i_276_, float f, float f_277_) {
		if (aBoolean103) {
			if (i_276_ > anInt107)
				i_276_ = anInt107;
			if (i_275_ < 0)
				i_275_ = 0;
		}
		if (i_275_ < i_276_) {
			i += i_275_ - 1;
			i_274_ = i_276_ - i_275_ >> 2;
			f += f_277_ * (float) i_275_;
			if (aClass240_104.aBoolean2262) {
				if (anInt108 == 0) {
					while (--i_274_ >= 0) {
						if (f < fs[++i]) {
							is[i] = i_273_;
							fs[i] = f;
						}
						f += f_277_;
						if (f < fs[++i]) {
							is[i] = i_273_;
							fs[i] = f;
						}
						f += f_277_;
						if (f < fs[++i]) {
							is[i] = i_273_;
							fs[i] = f;
						}
						f += f_277_;
						if (f < fs[++i]) {
							is[i] = i_273_;
							fs[i] = f;
						}
						f += f_277_;
					}
					i_274_ = i_276_ - i_275_ & 0x3;
					while (--i_274_ >= 0) {
						if (f < fs[++i]) {
							is[i] = i_273_;
							fs[i] = f;
						}
						f += f_277_;
					}
				} else if (anInt108 == 254) {
					if (i_275_ != 0 && i_276_ <= anInt107 - 1) {
						while (--i_274_ >= 0) {
							if (f < fs[++i])
								is[i - 1] = is[i];
							f += f_277_;
							if (f < fs[++i])
								is[i - 1] = is[i];
							f += f_277_;
							if (f < fs[++i])
								is[i - 1] = is[i];
							f += f_277_;
							if (f < fs[++i])
								is[i - 1] = is[i];
							f += f_277_;
						}
						i_274_ = i_276_ - i_275_ & 0x3;
						while (--i_274_ >= 0) {
							if (f < fs[++i])
								is[i - 1] = is[i];
							f += f_277_;
						}
					}
				} else {
					int i_278_ = anInt108;
					int i_279_ = 256 - anInt108;
					i_273_ = (((i_273_ & 0xff00ff) * i_279_ >> 8 & 0xff00ff) + ((i_273_ & 0xff00) * i_279_ >> 8 & 0xff00));
					while (--i_274_ >= 0) {
						if (f < fs[++i]) {
							int i_280_ = is[i];
							is[i] = ((i_279_ | i_280_ >> 24) << 24 | (i_273_ + ((i_280_ & 0xff00ff) * i_278_ >> 8 & 0xff00ff) + ((i_280_ & 0xff00) * i_278_ >> 8 & 0xff00)));
							fs[i] = f;
						}
						f += f_277_;
						if (f < fs[++i]) {
							int i_281_ = is[i];
							is[i] = ((i_279_ | i_281_ >> 24) << 24 | (i_273_ + ((i_281_ & 0xff00ff) * i_278_ >> 8 & 0xff00ff) + ((i_281_ & 0xff00) * i_278_ >> 8 & 0xff00)));
							fs[i] = f;
						}
						f += f_277_;
						if (f < fs[++i]) {
							int i_282_ = is[i];
							is[i] = ((i_279_ | i_282_ >> 24) << 24 | (i_273_ + ((i_282_ & 0xff00ff) * i_278_ >> 8 & 0xff00ff) + ((i_282_ & 0xff00) * i_278_ >> 8 & 0xff00)));
							fs[i] = f;
						}
						f += f_277_;
						if (f < fs[++i]) {
							int i_283_ = is[i];
							is[i] = ((i_279_ | i_283_ >> 24) << 24 | (i_273_ + ((i_283_ & 0xff00ff) * i_278_ >> 8 & 0xff00ff) + ((i_283_ & 0xff00) * i_278_ >> 8 & 0xff00)));
							fs[i] = f;
						}
						f += f_277_;
					}
					i_274_ = i_276_ - i_275_ & 0x3;
					while (--i_274_ >= 0) {
						if (f < fs[++i]) {
							int i_284_ = is[i];
							is[i] = ((i_279_ | i_284_ >> 24) << 24 | (i_273_ + ((i_284_ & 0xff00ff) * i_278_ >> 8 & 0xff00ff) + ((i_284_ & 0xff00) * i_278_ >> 8 & 0xff00)));
							fs[i] = f;
						}
						f += f_277_;
					}
				}
			} else if (anInt108 == 0) {
				while (--i_274_ >= 0) {
					if (f < fs[++i])
						is[i] = i_273_;
					f += f_277_;
					if (f < fs[++i])
						is[i] = i_273_;
					f += f_277_;
					if (f < fs[++i])
						is[i] = i_273_;
					f += f_277_;
					if (f < fs[++i])
						is[i] = i_273_;
					f += f_277_;
				}
				i_274_ = i_276_ - i_275_ & 0x3;
				while (--i_274_ >= 0) {
					if (f < fs[++i])
						is[i] = i_273_;
					f += f_277_;
				}
			} else if (anInt108 == 254) {
				if (i_275_ != 0 && i_276_ <= anInt107 - 1) {
					while (--i_274_ >= 0) {
						if (f < fs[++i])
							is[i - 1] = is[i];
						f += f_277_;
						if (f < fs[++i])
							is[i - 1] = is[i];
						f += f_277_;
						if (f < fs[++i])
							is[i - 1] = is[i];
						f += f_277_;
						if (f < fs[++i])
							is[i - 1] = is[i];
						f += f_277_;
					}
					i_274_ = i_276_ - i_275_ & 0x3;
					while (--i_274_ >= 0) {
						if (f < fs[++i])
							is[i - 1] = is[i];
						f += f_277_;
					}
				}
			} else {
				int i_285_ = anInt108;
				int i_286_ = 256 - anInt108;
				i_273_ = (((i_273_ & 0xff00ff) * i_286_ >> 8 & 0xff00ff) + ((i_273_ & 0xff00) * i_286_ >> 8 & 0xff00));
				while (--i_274_ >= 0) {
					if (f < fs[++i]) {
						int i_287_ = is[i];
						is[i] = ((i_286_ | i_287_ >> 24) << 24 | (i_273_ + ((i_287_ & 0xff00ff) * i_285_ >> 8 & 0xff00ff) + ((i_287_ & 0xff00) * i_285_ >> 8 & 0xff00)));
					}
					f += f_277_;
					if (f < fs[++i]) {
						int i_288_ = is[i];
						is[i] = ((i_286_ | i_288_ >> 24) << 24 | (i_273_ + ((i_288_ & 0xff00ff) * i_285_ >> 8 & 0xff00ff) + ((i_288_ & 0xff00) * i_285_ >> 8 & 0xff00)));
					}
					f += f_277_;
					if (f < fs[++i]) {
						int i_289_ = is[i];
						is[i] = ((i_286_ | i_289_ >> 24) << 24 | (i_273_ + ((i_289_ & 0xff00ff) * i_285_ >> 8 & 0xff00ff) + ((i_289_ & 0xff00) * i_285_ >> 8 & 0xff00)));
					}
					f += f_277_;
					if (f < fs[++i]) {
						int i_290_ = is[i];
						is[i] = ((i_286_ | i_290_ >> 24) << 24 | (i_273_ + ((i_290_ & 0xff00ff) * i_285_ >> 8 & 0xff00ff) + ((i_290_ & 0xff00) * i_285_ >> 8 & 0xff00)));
					}
					f += f_277_;
				}
				i_274_ = i_276_ - i_275_ & 0x3;
				while (--i_274_ >= 0) {
					if (f < fs[++i]) {
						int i_291_ = is[i];
						is[i] = ((i_286_ | i_291_ >> 24) << 24 | (i_273_ + ((i_291_ & 0xff00ff) * i_285_ >> 8 & 0xff00ff) + ((i_291_ & 0xff00) * i_285_ >> 8 & 0xff00)));
					}
					f += f_277_;
				}
			}
		}
	}

	private final void method203(int[] is, float[] fs, int i, int i_292_, int i_293_, int i_294_, int i_295_, float f, float f_296_) {
		if (aBoolean103) {
			if (i_295_ > anInt107)
				i_295_ = anInt107;
			if (i_294_ < 0)
				i_294_ = 0;
		}
		if (i_294_ < i_295_) {
			i += i_294_ - 1;
			i_293_ = i_295_ - i_294_ >> 2;
			f += f_296_ * (float) i_294_;
			if (aClass240_104.aBoolean2262) {
				if (anInt108 == 0) {
					while (--i_293_ >= 0) {
						if (f < fs[++i]) {
							is[i] = i_292_;
							fs[i] = f;
						}
						f += f_296_;
						if (f < fs[++i]) {
							is[i] = i_292_;
							fs[i] = f;
						}
						f += f_296_;
						if (f < fs[++i]) {
							is[i] = i_292_;
							fs[i] = f;
						}
						f += f_296_;
						if (f < fs[++i]) {
							is[i] = i_292_;
							fs[i] = f;
						}
						f += f_296_;
					}
					i_293_ = i_295_ - i_294_ & 0x3;
					while (--i_293_ >= 0) {
						if (f < fs[++i]) {
							is[i] = i_292_;
							fs[i] = f;
						}
						f += f_296_;
					}
				} else if (anInt108 == 254) {
					if (i_294_ != 0 && i_295_ <= anInt107 - 1) {
						while (--i_293_ >= 0) {
							if (f < fs[++i])
								is[i - 1] = is[i];
							f += f_296_;
							if (f < fs[++i])
								is[i - 1] = is[i];
							f += f_296_;
							if (f < fs[++i])
								is[i - 1] = is[i];
							f += f_296_;
							if (f < fs[++i])
								is[i - 1] = is[i];
							f += f_296_;
						}
						i_293_ = i_295_ - i_294_ & 0x3;
						while (--i_293_ >= 0) {
							if (f < fs[++i])
								is[i - 1] = is[i];
							f += f_296_;
						}
					}
				} else {
					int i_297_ = anInt108;
					int i_298_ = 256 - anInt108;
					i_292_ = (((i_292_ & 0xff00ff) * i_298_ >> 8 & 0xff00ff) + ((i_292_ & 0xff00) * i_298_ >> 8 & 0xff00));
					while (--i_293_ >= 0) {
						if (f < fs[++i]) {
							int i_299_ = is[i];
							is[i] = (i_292_ + ((i_299_ & 0xff00ff) * i_297_ >> 8 & 0xff00ff) + ((i_299_ & 0xff00) * i_297_ >> 8 & 0xff00));
							fs[i] = f;
						}
						f += f_296_;
						if (f < fs[++i]) {
							int i_300_ = is[i];
							is[i] = (i_292_ + ((i_300_ & 0xff00ff) * i_297_ >> 8 & 0xff00ff) + ((i_300_ & 0xff00) * i_297_ >> 8 & 0xff00));
							fs[i] = f;
						}
						f += f_296_;
						if (f < fs[++i]) {
							int i_301_ = is[i];
							is[i] = (i_292_ + ((i_301_ & 0xff00ff) * i_297_ >> 8 & 0xff00ff) + ((i_301_ & 0xff00) * i_297_ >> 8 & 0xff00));
							fs[i] = f;
						}
						f += f_296_;
						if (f < fs[++i]) {
							int i_302_ = is[i];
							is[i] = (i_292_ + ((i_302_ & 0xff00ff) * i_297_ >> 8 & 0xff00ff) + ((i_302_ & 0xff00) * i_297_ >> 8 & 0xff00));
							fs[i] = f;
						}
						f += f_296_;
					}
					i_293_ = i_295_ - i_294_ & 0x3;
					while (--i_293_ >= 0) {
						if (f < fs[++i]) {
							int i_303_ = is[i];
							is[i] = (i_292_ + ((i_303_ & 0xff00ff) * i_297_ >> 8 & 0xff00ff) + ((i_303_ & 0xff00) * i_297_ >> 8 & 0xff00));
							fs[i] = f;
						}
						f += f_296_;
					}
				}
			} else if (anInt108 == 0) {
				while (--i_293_ >= 0) {
					if (f < fs[++i])
						is[i] = i_292_;
					f += f_296_;
					if (f < fs[++i])
						is[i] = i_292_;
					f += f_296_;
					if (f < fs[++i])
						is[i] = i_292_;
					f += f_296_;
					if (f < fs[++i])
						is[i] = i_292_;
					f += f_296_;
				}
				i_293_ = i_295_ - i_294_ & 0x3;
				while (--i_293_ >= 0) {
					if (f < fs[++i])
						is[i] = i_292_;
					f += f_296_;
				}
			} else if (anInt108 == 254) {
				if (i_294_ != 0 && i_295_ <= anInt107 - 1) {
					while (--i_293_ >= 0) {
						if (f < fs[++i])
							is[i - 1] = is[i];
						f += f_296_;
						if (f < fs[++i])
							is[i - 1] = is[i];
						f += f_296_;
						if (f < fs[++i])
							is[i - 1] = is[i];
						f += f_296_;
						if (f < fs[++i])
							is[i - 1] = is[i];
						f += f_296_;
					}
					i_293_ = i_295_ - i_294_ & 0x3;
					while (--i_293_ >= 0) {
						if (f < fs[++i])
							is[i - 1] = is[i];
						f += f_296_;
					}
				}
			} else {
				int i_304_ = anInt108;
				int i_305_ = 256 - anInt108;
				i_292_ = (((i_292_ & 0xff00ff) * i_305_ >> 8 & 0xff00ff) + ((i_292_ & 0xff00) * i_305_ >> 8 & 0xff00));
				while (--i_293_ >= 0) {
					if (f < fs[++i]) {
						int i_306_ = is[i];
						is[i] = (i_292_ + ((i_306_ & 0xff00ff) * i_304_ >> 8 & 0xff00ff) + ((i_306_ & 0xff00) * i_304_ >> 8 & 0xff00));
					}
					f += f_296_;
					if (f < fs[++i]) {
						int i_307_ = is[i];
						is[i] = (i_292_ + ((i_307_ & 0xff00ff) * i_304_ >> 8 & 0xff00ff) + ((i_307_ & 0xff00) * i_304_ >> 8 & 0xff00));
					}
					f += f_296_;
					if (f < fs[++i]) {
						int i_308_ = is[i];
						is[i] = (i_292_ + ((i_308_ & 0xff00ff) * i_304_ >> 8 & 0xff00ff) + ((i_308_ & 0xff00) * i_304_ >> 8 & 0xff00));
					}
					f += f_296_;
					if (f < fs[++i]) {
						int i_309_ = is[i];
						is[i] = (i_292_ + ((i_309_ & 0xff00ff) * i_304_ >> 8 & 0xff00ff) + ((i_309_ & 0xff00) * i_304_ >> 8 & 0xff00));
					}
					f += f_296_;
				}
				i_293_ = i_295_ - i_294_ & 0x3;
				while (--i_293_ >= 0) {
					if (f < fs[++i]) {
						int i_310_ = is[i];
						is[i] = (i_292_ + ((i_310_ & 0xff00ff) * i_304_ >> 8 & 0xff00ff) + ((i_310_ & 0xff00) * i_304_ >> 8 & 0xff00));
					}
					f += f_296_;
				}
			}
		}
	}

	private final void method204(int[] is, int[] is_311_, int i, int i_312_, int i_313_, float f, float f_314_, float f_315_, float f_316_, float f_317_, float f_318_, float f_319_, float f_320_, float f_321_, float f_322_, float f_323_, float f_324_, float f_325_, float f_326_, float f_327_, float f_328_, float f_329_, float f_330_, float f_331_, float f_332_) {
		int i_333_ = i_313_ - i_312_;
		float f_334_ = 1.0F / (float) i_333_;
		float f_335_ = (f_314_ - f) * f_334_;
		float f_336_ = (f_316_ - f_315_) * f_334_;
		float f_337_ = (f_318_ - f_317_) * f_334_;
		float f_338_ = (f_320_ - f_319_) * f_334_;
		float f_339_ = (f_324_ - f_323_) * f_334_;
		float f_340_ = (f_326_ - f_325_) * f_334_;
		float f_341_ = (f_328_ - f_327_) * f_334_;
		float f_342_ = (f_330_ - f_329_) * f_334_;
		float f_343_ = (f_332_ - f_331_) * f_334_;
		if (aBoolean103) {
			if (i_313_ > anInt107)
				i_313_ = anInt107;
			if (i_312_ < 0) {
				f -= f_335_ * (float) i_312_;
				f_315_ -= f_336_ * (float) i_312_;
				f_317_ -= f_337_ * (float) i_312_;
				f_319_ -= f_338_ * (float) i_312_;
				f_323_ -= f_339_ * (float) i_312_;
				f_325_ -= f_340_ * (float) i_312_;
				f_327_ -= f_341_ * (float) i_312_;
				f_329_ -= f_342_ * (float) i_312_;
				f_331_ -= f_343_ * (float) i_312_;
				i_312_ = 0;
			}
		}
		if (i_312_ < i_313_) {
			i_333_ = i_313_ - i_312_;
			i += i_312_;
			while (i_333_-- > 0) {
				float f_344_ = 1.0F / f;
				if (f_344_ < aFloatArray105[i]) {
					float f_345_ = f_315_ * f_344_;
					float f_346_ = f_317_ * f_344_;
					int i_347_ = ((int) (f_345_ * (float) anInt113 * aFloat114) & anInt112);
					int i_348_ = ((int) (f_346_ * (float) anInt113 * aFloat114) & anInt112);
					int i_349_ = anIntArray123[i_348_ * anInt113 + i_347_];
					i_347_ = ((int) (f_345_ * (float) anInt117 * aFloat128) & anInt127);
					i_348_ = ((int) (f_346_ * (float) anInt117 * aFloat128) & anInt127);
					int i_350_ = anIntArray116[i_348_ * anInt117 + i_347_];
					i_347_ = ((int) (f_345_ * (float) anInt120 * aFloat122) & anInt118);
					i_348_ = ((int) (f_346_ * (float) anInt120 * aFloat122) & anInt118);
					int i_351_ = anIntArray111[i_348_ * anInt120 + i_347_];
					float f_352_ = 1.0F - (f_329_ + f_331_);
					i_349_ = ((int) (f_329_ * (float) (i_349_ >> 16 & 0xff)) << 16 | ~0xffffff | (int) (f_329_ * (float) (i_349_ >> 8 & 0xff)) << 8 | (int) (f_329_ * (float) (i_349_ & 0xff)));
					i_350_ = ((int) (f_331_ * (float) (i_350_ >> 16 & 0xff)) << 16 | ~0xffffff | (int) (f_331_ * (float) (i_350_ >> 8 & 0xff)) << 8 | (int) (f_331_ * (float) (i_350_ & 0xff)));
					i_351_ = ((int) (f_352_ * (float) (i_351_ >> 16 & 0xff)) << 16 | ~0xffffff | (int) (f_352_ * (float) (i_351_ >> 8 & 0xff)) << 8 | (int) (f_352_ * (float) (i_351_ & 0xff)));
					int i_353_ = i_349_ + i_350_ + i_351_;
					int i_354_ = (((int) (f_323_ * (float) (i_353_ >> 16 & 0xff)) << 8 & 0xff0000) | ~0xffffff | ((int) (f_325_ * (float) (i_353_ >> 8 & 0xff)) & 0xff00) | (int) (f_327_ * (float) (i_353_ & 0xff)) >> 8);
					if (f_319_ != 0.0F) {
						int i_355_ = (int) (255.0F - f_319_);
						int i_356_ = ((anInt125 & 0xff00ff) * (int) f_319_ & ~0xff00ff | ((anInt125 & 0xff00) * (int) f_319_ & 0xff0000)) >>> 8;
						i_354_ = (((i_354_ & 0xff00ff) * i_355_ & ~0xff00ff | (i_354_ & 0xff00) * i_355_ & 0xff0000) >>> 8) + i_356_;
					}
					is[i] = i_354_;
					aFloatArray105[i] = f_344_;
				}
				i++;
				f += f_335_;
				f_315_ += f_336_;
				f_317_ += f_337_;
				f_319_ += f_338_;
				f_323_ += f_339_;
				f_325_ += f_340_;
				f_327_ += f_341_;
				f_329_ += f_342_;
				f_331_ += f_343_;
			}
		}
	}

	final void method205(float f, float f_357_, float f_358_, float f_359_, float f_360_, float f_361_, float f_362_, float f_363_, float f_364_, float f_365_, float f_366_, float f_367_) {
		if (aBoolean100) {
			aHa_Sub2_96.method1094((int) f_357_, (int) f, (int) f_360_, (int) f_359_, 125, Class295_Sub1.anIntArray3691[(int) f_365_]);
			aHa_Sub2_96.method1094((int) f_358_, (int) f_357_, (int) f_361_, (int) f_360_, 124, Class295_Sub1.anIntArray3691[(int) f_365_]);
			aHa_Sub2_96.method1094((int) f, (int) f_358_, (int) f_359_, (int) f_361_, 126, Class295_Sub1.anIntArray3691[(int) f_365_]);
		} else {
			float f_368_ = f_360_ - f_359_;
			float f_369_ = f_357_ - f;
			float f_370_ = f_361_ - f_359_;
			float f_371_ = f_358_ - f;
			float f_372_ = f_366_ - f_365_;
			float f_373_ = f_367_ - f_365_;
			float f_374_ = f_363_ - f_362_;
			float f_375_ = f_364_ - f_362_;
			float f_376_;
			if (f_358_ != f_357_)
				f_376_ = (f_361_ - f_360_) / (f_358_ - f_357_);
			else
				f_376_ = 0.0F;
			float f_377_;
			if (f_357_ != f)
				f_377_ = f_368_ / f_369_;
			else
				f_377_ = 0.0F;
			float f_378_;
			if (f_358_ != f)
				f_378_ = f_370_ / f_371_;
			else
				f_378_ = 0.0F;
			float f_379_ = f_368_ * f_371_ - f_370_ * f_369_;
			if (f_379_ != 0.0F) {
				float f_380_ = (f_372_ * f_371_ - f_373_ * f_369_) / f_379_;
				float f_381_ = (f_373_ * f_368_ - f_372_ * f_370_) / f_379_;
				float f_382_ = (f_374_ * f_371_ - f_375_ * f_369_) / f_379_;
				float f_383_ = (f_375_ * f_368_ - f_374_ * f_370_) / f_379_;
				if (f <= f_357_ && f <= f_358_) {
					if (!(f >= (float) anInt101)) {
						if (f_357_ > (float) anInt101)
							f_357_ = (float) anInt101;
						if (f_358_ > (float) anInt101)
							f_358_ = (float) anInt101;
						f_365_ = f_365_ - f_380_ * f_359_ + f_380_;
						f_362_ = f_362_ - f_382_ * f_359_ + f_382_;
						if (f_357_ < f_358_) {
							f_361_ = f_359_;
							if (f < 0.0F) {
								f_361_ -= f_378_ * f;
								f_359_ -= f_377_ * f;
								f_365_ -= f_381_ * f;
								f_362_ -= f_383_ * f;
								f = 0.0F;
							}
							if (f_357_ < 0.0F) {
								f_360_ -= f_376_ * f_357_;
								f_357_ = 0.0F;
							}
							if (f != f_357_ && f_378_ < f_377_ || f == f_357_ && f_378_ > f_376_) {
								f_358_ -= f_357_;
								f_357_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_357_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_361_, (int) f_359_, f_365_, f_380_, f_362_, f_382_);
									f_361_ += f_378_;
									f_359_ += f_377_;
									f_365_ += f_381_;
									f_362_ += f_383_;
									f += (float) anInt98;
								}
								while (--f_358_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_361_, (int) f_360_, f_365_, f_380_, f_362_, f_382_);
									f_361_ += f_378_;
									f_360_ += f_376_;
									f_365_ += f_381_;
									f_362_ += f_383_;
									f += (float) anInt98;
								}
							} else {
								f_358_ -= f_357_;
								f_357_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_357_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_359_, (int) f_361_, f_365_, f_380_, f_362_, f_382_);
									f_361_ += f_378_;
									f_359_ += f_377_;
									f_365_ += f_381_;
									f_362_ += f_383_;
									f += (float) anInt98;
								}
								while (--f_358_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_360_, (int) f_361_, f_365_, f_380_, f_362_, f_382_);
									f_361_ += f_378_;
									f_360_ += f_376_;
									f_365_ += f_381_;
									f_362_ += f_383_;
									f += (float) anInt98;
								}
							}
						} else {
							f_360_ = f_359_;
							if (f < 0.0F) {
								f_360_ -= f_378_ * f;
								f_359_ -= f_377_ * f;
								f_365_ -= f_381_ * f;
								f_362_ -= f_383_ * f;
								f = 0.0F;
							}
							if (f_358_ < 0.0F) {
								f_361_ -= f_376_ * f_358_;
								f_358_ = 0.0F;
							}
							if (f != f_358_ && f_378_ < f_377_ || f == f_358_ && f_376_ > f_377_) {
								f_357_ -= f_358_;
								f_358_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_358_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_360_, (int) f_359_, f_365_, f_380_, f_362_, f_382_);
									f_360_ += f_378_;
									f_359_ += f_377_;
									f_365_ += f_381_;
									f_362_ += f_383_;
									f += (float) anInt98;
								}
								while (--f_357_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_361_, (int) f_359_, f_365_, f_380_, f_362_, f_382_);
									f_361_ += f_376_;
									f_359_ += f_377_;
									f_365_ += f_381_;
									f_362_ += f_383_;
									f += (float) anInt98;
								}
							} else {
								f_357_ -= f_358_;
								f_358_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_358_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_359_, (int) f_360_, f_365_, f_380_, f_362_, f_382_);
									f_360_ += f_378_;
									f_359_ += f_377_;
									f_365_ += f_381_;
									f_362_ += f_383_;
									f += (float) anInt98;
								}
								while (--f_357_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_359_, (int) f_361_, f_365_, f_380_, f_362_, f_382_);
									f_361_ += f_376_;
									f_359_ += f_377_;
									f_365_ += f_381_;
									f_362_ += f_383_;
									f += (float) anInt98;
								}
							}
						}
					}
				} else if (f_357_ <= f_358_) {
					if (!(f_357_ >= (float) anInt101)) {
						if (f_358_ > (float) anInt101)
							f_358_ = (float) anInt101;
						if (f > (float) anInt101)
							f = (float) anInt101;
						f_366_ = f_366_ - f_380_ * f_360_ + f_380_;
						f_363_ = f_363_ - f_382_ * f_360_ + f_382_;
						if (f_358_ < f) {
							f_359_ = f_360_;
							if (f_357_ < 0.0F) {
								f_359_ -= f_377_ * f_357_;
								f_360_ -= f_376_ * f_357_;
								f_366_ -= f_381_ * f_357_;
								f_363_ -= f_383_ * f_357_;
								f_357_ = 0.0F;
							}
							if (f_358_ < 0.0F) {
								f_361_ -= f_378_ * f_358_;
								f_358_ = 0.0F;
							}
							if (f_357_ != f_358_ && f_377_ < f_376_ || f_357_ == f_358_ && f_377_ > f_378_) {
								f -= f_358_;
								f_358_ -= f_357_;
								f_357_ = (float) anIntArray97[(int) f_357_];
								while (--f_358_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f_357_, 0, 0, (int) f_359_, (int) f_360_, f_366_, f_380_, f_363_, f_382_);
									f_359_ += f_377_;
									f_360_ += f_376_;
									f_366_ += f_381_;
									f_363_ += f_383_;
									f_357_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f_357_, 0, 0, (int) f_359_, (int) f_361_, f_366_, f_380_, f_363_, f_382_);
									f_359_ += f_377_;
									f_361_ += f_378_;
									f_366_ += f_381_;
									f_363_ += f_383_;
									f_357_ += (float) anInt98;
								}
							} else {
								f -= f_358_;
								f_358_ -= f_357_;
								f_357_ = (float) anIntArray97[(int) f_357_];
								while (--f_358_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f_357_, 0, 0, (int) f_360_, (int) f_359_, f_366_, f_380_, f_363_, f_382_);
									f_359_ += f_377_;
									f_360_ += f_376_;
									f_366_ += f_381_;
									f_363_ += f_383_;
									f_357_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f_357_, 0, 0, (int) f_361_, (int) f_359_, f_366_, f_380_, f_363_, f_382_);
									f_359_ += f_377_;
									f_361_ += f_378_;
									f_366_ += f_381_;
									f_363_ += f_383_;
									f_357_ += (float) anInt98;
								}
							}
						} else {
							f_361_ = f_360_;
							if (f_357_ < 0.0F) {
								f_361_ -= f_377_ * f_357_;
								f_360_ -= f_376_ * f_357_;
								f_366_ -= f_381_ * f_357_;
								f_363_ -= f_383_ * f_357_;
								f_357_ = 0.0F;
							}
							if (f < 0.0F) {
								f_359_ -= f_378_ * f;
								f = 0.0F;
							}
							if (f_377_ < f_376_) {
								f_358_ -= f;
								f -= f_357_;
								f_357_ = (float) anIntArray97[(int) f_357_];
								while (--f >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f_357_, 0, 0, (int) f_361_, (int) f_360_, f_366_, f_380_, f_363_, f_382_);
									f_361_ += f_377_;
									f_360_ += f_376_;
									f_366_ += f_381_;
									f_363_ += f_383_;
									f_357_ += (float) anInt98;
								}
								while (--f_358_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f_357_, 0, 0, (int) f_359_, (int) f_360_, f_366_, f_380_, f_363_, f_382_);
									f_359_ += f_378_;
									f_360_ += f_376_;
									f_366_ += f_381_;
									f_363_ += f_383_;
									f_357_ += (float) anInt98;
								}
							} else {
								f_358_ -= f;
								f -= f_357_;
								f_357_ = (float) anIntArray97[(int) f_357_];
								while (--f >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f_357_, 0, 0, (int) f_360_, (int) f_361_, f_366_, f_380_, f_363_, f_382_);
									f_361_ += f_377_;
									f_360_ += f_376_;
									f_366_ += f_381_;
									f_363_ += f_383_;
									f_357_ += (float) anInt98;
								}
								while (--f_358_ >= 0.0F) {
									method211(anIntArray99, aFloatArray105, (int) f_357_, 0, 0, (int) f_360_, (int) f_359_, f_366_, f_380_, f_363_, f_382_);
									f_359_ += f_378_;
									f_360_ += f_376_;
									f_366_ += f_381_;
									f_363_ += f_383_;
									f_357_ += (float) anInt98;
								}
							}
						}
					}
				} else if (!(f_358_ >= (float) anInt101)) {
					if (f > (float) anInt101)
						f = (float) anInt101;
					if (f_357_ > (float) anInt101)
						f_357_ = (float) anInt101;
					f_367_ = f_367_ - f_380_ * f_361_ + f_380_;
					f_364_ = f_364_ - f_382_ * f_361_ + f_382_;
					if (f < f_357_) {
						f_360_ = f_361_;
						if (f_358_ < 0.0F) {
							f_360_ -= f_376_ * f_358_;
							f_361_ -= f_378_ * f_358_;
							f_367_ -= f_381_ * f_358_;
							f_364_ -= f_383_ * f_358_;
							f_358_ = 0.0F;
						}
						if (f < 0.0F) {
							f_359_ -= f_377_ * f;
							f = 0.0F;
						}
						if (f_376_ < f_378_) {
							f_357_ -= f;
							f -= f_358_;
							f_358_ = (float) anIntArray97[(int) f_358_];
							while (--f >= 0.0F) {
								method211(anIntArray99, aFloatArray105, (int) f_358_, 0, 0, (int) f_360_, (int) f_361_, f_367_, f_380_, f_364_, f_382_);
								f_360_ += f_376_;
								f_361_ += f_378_;
								f_367_ += f_381_;
								f_364_ += f_383_;
								f_358_ += (float) anInt98;
							}
							while (--f_357_ >= 0.0F) {
								method211(anIntArray99, aFloatArray105, (int) f_358_, 0, 0, (int) f_360_, (int) f_359_, f_367_, f_380_, f_364_, f_382_);
								f_360_ += f_376_;
								f_359_ += f_377_;
								f_367_ += f_381_;
								f_364_ += f_383_;
								f_358_ += (float) anInt98;
							}
						} else {
							f_357_ -= f;
							f -= f_358_;
							f_358_ = (float) anIntArray97[(int) f_358_];
							while (--f >= 0.0F) {
								method211(anIntArray99, aFloatArray105, (int) f_358_, 0, 0, (int) f_361_, (int) f_360_, f_367_, f_380_, f_364_, f_382_);
								f_360_ += f_376_;
								f_361_ += f_378_;
								f_367_ += f_381_;
								f_364_ += f_383_;
								f_358_ += (float) anInt98;
							}
							while (--f_357_ >= 0.0F) {
								method211(anIntArray99, aFloatArray105, (int) f_358_, 0, 0, (int) f_359_, (int) f_360_, f_367_, f_380_, f_364_, f_382_);
								f_360_ += f_376_;
								f_359_ += f_377_;
								f_367_ += f_381_;
								f_364_ += f_383_;
								f_358_ += (float) anInt98;
							}
						}
					} else {
						f_359_ = f_361_;
						if (f_358_ < 0.0F) {
							f_359_ -= f_376_ * f_358_;
							f_361_ -= f_378_ * f_358_;
							f_367_ -= f_381_ * f_358_;
							f_364_ -= f_383_ * f_358_;
							f_358_ = 0.0F;
						}
						if (f_357_ < 0.0F) {
							f_360_ -= f_377_ * f_357_;
							f_357_ = 0.0F;
						}
						if (f_376_ < f_378_) {
							f -= f_357_;
							f_357_ -= f_358_;
							f_358_ = (float) anIntArray97[(int) f_358_];
							while (--f_357_ >= 0.0F) {
								method211(anIntArray99, aFloatArray105, (int) f_358_, 0, 0, (int) f_359_, (int) f_361_, f_367_, f_380_, f_364_, f_382_);
								f_359_ += f_376_;
								f_361_ += f_378_;
								f_367_ += f_381_;
								f_364_ += f_383_;
								f_358_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method211(anIntArray99, aFloatArray105, (int) f_358_, 0, 0, (int) f_360_, (int) f_361_, f_367_, f_380_, f_364_, f_382_);
								f_360_ += f_377_;
								f_361_ += f_378_;
								f_367_ += f_381_;
								f_364_ += f_383_;
								f_358_ += (float) anInt98;
							}
						} else {
							f -= f_357_;
							f_357_ -= f_358_;
							f_358_ = (float) anIntArray97[(int) f_358_];
							while (--f_357_ >= 0.0F) {
								method211(anIntArray99, aFloatArray105, (int) f_358_, 0, 0, (int) f_361_, (int) f_359_, f_367_, f_380_, f_364_, f_382_);
								f_359_ += f_376_;
								f_361_ += f_378_;
								f_367_ += f_381_;
								f_364_ += f_383_;
								f_358_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method211(anIntArray99, aFloatArray105, (int) f_358_, 0, 0, (int) f_361_, (int) f_360_, f_367_, f_380_, f_364_, f_382_);
								f_360_ += f_377_;
								f_361_ += f_378_;
								f_367_ += f_381_;
								f_364_ += f_383_;
								f_358_ += (float) anInt98;
							}
						}
					}
				}
			}
		}
	}

	final void method206(float f, float f_384_, float f_385_, float f_386_, float f_387_, float f_388_, float f_389_, float f_390_, float f_391_, int i) {
		if (aBoolean100) {
			aHa_Sub2_96.method1094((int) f_384_, (int) f, (int) f_387_, (int) f_386_, 124, i);
			aHa_Sub2_96.method1094((int) f_385_, (int) f_384_, (int) f_388_, (int) f_387_, 123, i);
			aHa_Sub2_96.method1094((int) f, (int) f_385_, (int) f_386_, (int) f_388_, 127, i);
		} else {
			float f_392_ = f_387_ - f_386_;
			float f_393_ = f_384_ - f;
			float f_394_ = f_388_ - f_386_;
			float f_395_ = f_385_ - f;
			float f_396_ = f_390_ - f_389_;
			float f_397_ = f_391_ - f_389_;
			float f_398_ = 0.0F;
			if (f_384_ != f)
				f_398_ = (f_387_ - f_386_) / (f_384_ - f);
			float f_399_ = 0.0F;
			if (f_385_ != f_384_)
				f_399_ = (f_388_ - f_387_) / (f_385_ - f_384_);
			float f_400_ = 0.0F;
			if (f_385_ != f)
				f_400_ = (f_386_ - f_388_) / (f - f_385_);
			float f_401_ = f_392_ * f_395_ - f_394_ * f_393_;
			if (f_401_ != 0.0F) {
				float f_402_ = (f_396_ * f_395_ - f_397_ * f_393_) / f_401_;
				float f_403_ = (f_397_ * f_392_ - f_396_ * f_394_) / f_401_;
				if (f <= f_384_ && f <= f_385_) {
					if (!(f >= (float) anInt101)) {
						if (f_384_ > (float) anInt101)
							f_384_ = (float) anInt101;
						if (f_385_ > (float) anInt101)
							f_385_ = (float) anInt101;
						f_389_ = f_389_ - f_402_ * f_386_ + f_402_;
						if (f_384_ < f_385_) {
							f_388_ = f_386_;
							if (f < 0.0F) {
								f_388_ -= f_400_ * f;
								f_386_ -= f_398_ * f;
								f_389_ -= f_403_ * f;
								f = 0.0F;
							}
							if (f_384_ < 0.0F) {
								f_387_ -= f_399_ * f_384_;
								f_384_ = 0.0F;
							}
							if (f != f_384_ && f_400_ < f_398_ || f == f_384_ && f_400_ > f_399_) {
								f_385_ -= f_384_;
								f_384_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_384_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_388_, (int) f_386_, f_389_, f_402_);
									f_388_ += f_400_;
									f_386_ += f_398_;
									f_389_ += f_403_;
									f += (float) anInt98;
								}
								while (--f_385_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_388_, (int) f_387_, f_389_, f_402_);
									f_388_ += f_400_;
									f_387_ += f_399_;
									f_389_ += f_403_;
									f += (float) anInt98;
								}
							} else {
								f_385_ -= f_384_;
								f_384_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_384_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_386_, (int) f_388_, f_389_, f_402_);
									f_388_ += f_400_;
									f_386_ += f_398_;
									f_389_ += f_403_;
									f += (float) anInt98;
								}
								while (--f_385_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_387_, (int) f_388_, f_389_, f_402_);
									f_388_ += f_400_;
									f_387_ += f_399_;
									f_389_ += f_403_;
									f += (float) anInt98;
								}
							}
						} else {
							f_387_ = f_386_;
							if (f < 0.0F) {
								f_387_ -= f_400_ * f;
								f_386_ -= f_398_ * f;
								f_389_ -= f_403_ * f;
								f = 0.0F;
							}
							if (f_385_ < 0.0F) {
								f_388_ -= f_399_ * f_385_;
								f_385_ = 0.0F;
							}
							if (f != f_385_ && f_400_ < f_398_ || f == f_385_ && f_399_ > f_398_) {
								f_384_ -= f_385_;
								f_385_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_385_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_387_, (int) f_386_, f_389_, f_402_);
									f_387_ += f_400_;
									f_386_ += f_398_;
									f_389_ += f_403_;
									f += (float) anInt98;
								}
								while (--f_384_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_388_, (int) f_386_, f_389_, f_402_);
									f_388_ += f_399_;
									f_386_ += f_398_;
									f_389_ += f_403_;
									f += (float) anInt98;
								}
							} else {
								f_384_ -= f_385_;
								f_385_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_385_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_386_, (int) f_387_, f_389_, f_402_);
									f_387_ += f_400_;
									f_386_ += f_398_;
									f_389_ += f_403_;
									f += (float) anInt98;
								}
								while (--f_384_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_386_, (int) f_388_, f_389_, f_402_);
									f_388_ += f_399_;
									f_386_ += f_398_;
									f_389_ += f_403_;
									f += (float) anInt98;
								}
							}
						}
					}
				} else if (f_384_ <= f_385_) {
					if (!(f_384_ >= (float) anInt101)) {
						if (f_385_ > (float) anInt101)
							f_385_ = (float) anInt101;
						if (f > (float) anInt101)
							f = (float) anInt101;
						f_390_ = f_390_ - f_402_ * f_387_ + f_402_;
						if (f_385_ < f) {
							f_386_ = f_387_;
							if (f_384_ < 0.0F) {
								f_386_ -= f_398_ * f_384_;
								f_387_ -= f_399_ * f_384_;
								f_390_ -= f_403_ * f_384_;
								f_384_ = 0.0F;
							}
							if (f_385_ < 0.0F) {
								f_388_ -= f_400_ * f_385_;
								f_385_ = 0.0F;
							}
							if (f_384_ != f_385_ && f_398_ < f_399_ || f_384_ == f_385_ && f_398_ > f_400_) {
								f -= f_385_;
								f_385_ -= f_384_;
								f_384_ = (float) anIntArray97[(int) f_384_];
								while (--f_385_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f_384_, i, 0, (int) f_386_, (int) f_387_, f_390_, f_402_);
									f_386_ += f_398_;
									f_387_ += f_399_;
									f_390_ += f_403_;
									f_384_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f_384_, i, 0, (int) f_386_, (int) f_388_, f_390_, f_402_);
									f_386_ += f_398_;
									f_388_ += f_400_;
									f_390_ += f_403_;
									f_384_ += (float) anInt98;
								}
							} else {
								f -= f_385_;
								f_385_ -= f_384_;
								f_384_ = (float) anIntArray97[(int) f_384_];
								while (--f_385_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f_384_, i, 0, (int) f_387_, (int) f_386_, f_390_, f_402_);
									f_386_ += f_398_;
									f_387_ += f_399_;
									f_390_ += f_403_;
									f_384_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f_384_, i, 0, (int) f_388_, (int) f_386_, f_390_, f_402_);
									f_386_ += f_398_;
									f_388_ += f_400_;
									f_390_ += f_403_;
									f_384_ += (float) anInt98;
								}
							}
						} else {
							f_388_ = f_387_;
							if (f_384_ < 0.0F) {
								f_388_ -= f_398_ * f_384_;
								f_387_ -= f_399_ * f_384_;
								f_390_ -= f_403_ * f_384_;
								f_384_ = 0.0F;
							}
							if (f < 0.0F) {
								f_386_ -= f_400_ * f;
								f = 0.0F;
							}
							if (f_398_ < f_399_) {
								f_385_ -= f;
								f -= f_384_;
								f_384_ = (float) anIntArray97[(int) f_384_];
								while (--f >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f_384_, i, 0, (int) f_388_, (int) f_387_, f_390_, f_402_);
									f_388_ += f_398_;
									f_387_ += f_399_;
									f_390_ += f_403_;
									f_384_ += (float) anInt98;
								}
								while (--f_385_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f_384_, i, 0, (int) f_386_, (int) f_387_, f_390_, f_402_);
									f_386_ += f_400_;
									f_387_ += f_399_;
									f_390_ += f_403_;
									f_384_ += (float) anInt98;
								}
							} else {
								f_385_ -= f;
								f -= f_384_;
								f_384_ = (float) anIntArray97[(int) f_384_];
								while (--f >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f_384_, i, 0, (int) f_387_, (int) f_388_, f_390_, f_402_);
									f_388_ += f_398_;
									f_387_ += f_399_;
									f_390_ += f_403_;
									f_384_ += (float) anInt98;
								}
								while (--f_385_ >= 0.0F) {
									method203(anIntArray99, aFloatArray105, (int) f_384_, i, 0, (int) f_387_, (int) f_386_, f_390_, f_402_);
									f_386_ += f_400_;
									f_387_ += f_399_;
									f_390_ += f_403_;
									f_384_ += (float) anInt98;
								}
							}
						}
					}
				} else if (!(f_385_ >= (float) anInt101)) {
					if (f > (float) anInt101)
						f = (float) anInt101;
					if (f_384_ > (float) anInt101)
						f_384_ = (float) anInt101;
					f_391_ = f_391_ - f_402_ * f_388_ + f_402_;
					if (f < f_384_) {
						f_387_ = f_388_;
						if (f_385_ < 0.0F) {
							f_387_ -= f_399_ * f_385_;
							f_388_ -= f_400_ * f_385_;
							f_391_ -= f_403_ * f_385_;
							f_385_ = 0.0F;
						}
						if (f < 0.0F) {
							f_386_ -= f_398_ * f;
							f = 0.0F;
						}
						if (f_399_ < f_400_) {
							f_384_ -= f;
							f -= f_385_;
							f_385_ = (float) anIntArray97[(int) f_385_];
							while (--f >= 0.0F) {
								method203(anIntArray99, aFloatArray105, (int) f_385_, i, 0, (int) f_387_, (int) f_388_, f_391_, f_402_);
								f_387_ += f_399_;
								f_388_ += f_400_;
								f_391_ += f_403_;
								f_385_ += (float) anInt98;
							}
							while (--f_384_ >= 0.0F) {
								method203(anIntArray99, aFloatArray105, (int) f_385_, i, 0, (int) f_387_, (int) f_386_, f_391_, f_402_);
								f_387_ += f_399_;
								f_386_ += f_398_;
								f_391_ += f_403_;
								f_385_ += (float) anInt98;
							}
						} else {
							f_384_ -= f;
							f -= f_385_;
							f_385_ = (float) anIntArray97[(int) f_385_];
							while (--f >= 0.0F) {
								method203(anIntArray99, aFloatArray105, (int) f_385_, i, 0, (int) f_388_, (int) f_387_, f_391_, f_402_);
								f_387_ += f_399_;
								f_388_ += f_400_;
								f_391_ += f_403_;
								f_385_ += (float) anInt98;
							}
							while (--f_384_ >= 0.0F) {
								method203(anIntArray99, aFloatArray105, (int) f_385_, i, 0, (int) f_386_, (int) f_387_, f_391_, f_402_);
								f_387_ += f_399_;
								f_386_ += f_398_;
								f_391_ += f_403_;
								f_385_ += (float) anInt98;
							}
						}
					} else {
						f_386_ = f_388_;
						if (f_385_ < 0.0F) {
							f_386_ -= f_399_ * f_385_;
							f_388_ -= f_400_ * f_385_;
							f_391_ -= f_403_ * f_385_;
							f_385_ = 0.0F;
						}
						if (f_384_ < 0.0F) {
							f_387_ -= f_398_ * f_384_;
							f_384_ = 0.0F;
						}
						if (f_399_ < f_400_) {
							f -= f_384_;
							f_384_ -= f_385_;
							f_385_ = (float) anIntArray97[(int) f_385_];
							while (--f_384_ >= 0.0F) {
								method203(anIntArray99, aFloatArray105, (int) f_385_, i, 0, (int) f_386_, (int) f_388_, f_391_, f_402_);
								f_386_ += f_399_;
								f_388_ += f_400_;
								f_391_ += f_403_;
								f_385_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method203(anIntArray99, aFloatArray105, (int) f_385_, i, 0, (int) f_387_, (int) f_388_, f_391_, f_402_);
								f_387_ += f_398_;
								f_388_ += f_400_;
								f_391_ += f_403_;
								f_385_ += (float) anInt98;
							}
						} else {
							f -= f_384_;
							f_384_ -= f_385_;
							f_385_ = (float) anIntArray97[(int) f_385_];
							while (--f_384_ >= 0.0F) {
								method203(anIntArray99, aFloatArray105, (int) f_385_, i, 0, (int) f_388_, (int) f_386_, f_391_, f_402_);
								f_386_ += f_399_;
								f_388_ += f_400_;
								f_391_ += f_403_;
								f_385_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method203(anIntArray99, aFloatArray105, (int) f_385_, i, 0, (int) f_388_, (int) f_387_, f_391_, f_402_);
								f_387_ += f_398_;
								f_388_ += f_400_;
								f_391_ += f_403_;
								f_385_ += (float) anInt98;
							}
						}
					}
				}
			}
		}
	}

	private final void method207(int[] is, float[] fs, int i, int i_404_, int i_405_, int i_406_, int i_407_, float f, float f_408_, float f_409_, float f_410_) {
		if (aBoolean103) {
			if (i_407_ > anInt107)
				i_407_ = anInt107;
			if (i_406_ < 0)
				i_406_ = 0;
		}
		if (i_406_ < i_407_) {
			i += i_406_ - 1;
			f += f_408_ * (float) i_406_;
			f_409_ += f_410_ * (float) i_406_;
			if (aClass240_104.aBoolean2262) {
				do {
					if (aBoolean102) {
						i_405_ = i_407_ - i_406_ >> 2;
						f_408_ *= 4.0F;
						if (anInt108 == 0) {
							if (i_405_ > 0) {
								do {
									i_404_ = (Class295_Sub1.anIntArray3691[(int) f]);
									f += f_408_;
									if (f_409_ < fs[++i]) {
										is[i] = i_404_;
										fs[i] = f_409_;
									}
									f_409_ += f_410_;
									if (f_409_ < fs[++i]) {
										is[i] = i_404_;
										fs[i] = f_409_;
									}
									f_409_ += f_410_;
									if (f_409_ < fs[++i]) {
										is[i] = i_404_;
										fs[i] = f_409_;
									}
									f_409_ += f_410_;
									if (f_409_ < fs[++i]) {
										is[i] = i_404_;
										fs[i] = f_409_;
									}
									f_409_ += f_410_;
								} while (--i_405_ > 0);
							}
							i_405_ = i_407_ - i_406_ & 0x3;
							if (i_405_ > 0) {
								i_404_ = Class295_Sub1.anIntArray3691[(int) f];
								do {
									if (f_409_ < fs[++i]) {
										is[i] = i_404_;
										fs[i] = f_409_;
									}
									f_409_ += f_410_;
								} while (--i_405_ > 0);
							}
						} else {
							int i_411_ = anInt108;
							int i_412_ = 256 - anInt108;
							if (i_405_ > 0) {
								do {
									i_404_ = (Class295_Sub1.anIntArray3691[(int) f]);
									f += f_408_;
									i_404_ = (((i_404_ & 0xff00ff) * i_412_ >> 8 & 0xff00ff) + ((i_404_ & 0xff00) * i_412_ >> 8 & 0xff00));
									if (f_409_ < fs[++i]) {
										int i_413_ = is[i];
										is[i] = ((i_412_ | i_413_ >> 24) << 24 | (i_404_ + (((i_413_ & 0xff00ff) * i_411_) >> 8 & 0xff00ff) + (((i_413_ & 0xff00) * i_411_) >> 8 & 0xff00)));
										fs[i] = f_409_;
									}
									f_409_ += f_410_;
									if (f_409_ < fs[++i]) {
										int i_414_ = is[i];
										is[i] = ((i_412_ | i_414_ >> 24) << 24 | (i_404_ + (((i_414_ & 0xff00ff) * i_411_) >> 8 & 0xff00ff) + (((i_414_ & 0xff00) * i_411_) >> 8 & 0xff00)));
										fs[i] = f_409_;
									}
									f_409_ += f_410_;
									if (f_409_ < fs[++i]) {
										int i_415_ = is[i];
										is[i] = ((i_412_ | i_415_ >> 24) << 24 | (i_404_ + (((i_415_ & 0xff00ff) * i_411_) >> 8 & 0xff00ff) + (((i_415_ & 0xff00) * i_411_) >> 8 & 0xff00)));
										fs[i] = f_409_;
									}
									f_409_ += f_410_;
									if (f_409_ < fs[++i]) {
										int i_416_ = is[i];
										is[i] = ((i_412_ | i_416_ >> 24) << 24 | (i_404_ + (((i_416_ & 0xff00ff) * i_411_) >> 8 & 0xff00ff) + (((i_416_ & 0xff00) * i_411_) >> 8 & 0xff00)));
										fs[i] = f_409_;
									}
									f_409_ += f_410_;
								} while (--i_405_ > 0);
							}
							i_405_ = i_407_ - i_406_ & 0x3;
							if (i_405_ <= 0)
								break;
							i_404_ = Class295_Sub1.anIntArray3691[(int) f];
							i_404_ = (((i_404_ & 0xff00ff) * i_412_ >> 8 & 0xff00ff) + ((i_404_ & 0xff00) * i_412_ >> 8 & 0xff00));
							do {
								if (f_409_ < fs[++i]) {
									int i_417_ = is[i];
									is[i] = ((i_412_ | i_417_ >> 24) << 24 | (i_404_ + (((i_417_ & 0xff00ff) * i_411_ >> 8) & 0xff00ff) + (((i_417_ & 0xff00) * i_411_ >> 8) & 0xff00)));
									fs[i] = f_409_;
								}
								f_409_ += f_410_;
							} while (--i_405_ > 0);
						}
						break;
					}
					i_405_ = i_407_ - i_406_;
					if (anInt108 == 0) {
						do {
							if (f_409_ < fs[++i]) {
								is[i] = Class295_Sub1.anIntArray3691[(int) f];
								fs[i] = f_409_;
							}
							f_409_ += f_410_;
							f += f_408_;
						} while (--i_405_ > 0);
						break;
					}
					int i_418_ = anInt108;
					int i_419_ = 256 - anInt108;
					do {
						if (f_409_ < fs[++i]) {
							i_404_ = Class295_Sub1.anIntArray3691[(int) f];
							i_404_ = (((i_404_ & 0xff00ff) * i_419_ >> 8 & 0xff00ff) + ((i_404_ & 0xff00) * i_419_ >> 8 & 0xff00));
							int i_420_ = is[i];
							is[i] = ((i_419_ | i_420_ >> 24) << 24 | (i_404_ + ((i_420_ & 0xff00ff) * i_418_ >> 8 & 0xff00ff) + ((i_420_ & 0xff00) * i_418_ >> 8 & 0xff00)));
							fs[i] = f_409_;
						}
						f += f_408_;
						f_409_ += f_410_;
					} while (--i_405_ > 0);
				} while (false);
			} else {
				do {
					if (aBoolean102) {
						i_405_ = i_407_ - i_406_ >> 2;
						f_408_ *= 4.0F;
						if (anInt108 == 0) {
							if (i_405_ > 0) {
								do {
									i_404_ = (Class295_Sub1.anIntArray3691[(int) f]);
									f += f_408_;
									if (f_409_ < fs[++i])
										is[i] = i_404_;
									f_409_ += f_410_;
									if (f_409_ < fs[++i])
										is[i] = i_404_;
									f_409_ += f_410_;
									if (f_409_ < fs[++i])
										is[i] = i_404_;
									f_409_ += f_410_;
									if (f_409_ < fs[++i])
										is[i] = i_404_;
									f_409_ += f_410_;
								} while (--i_405_ > 0);
							}
							i_405_ = i_407_ - i_406_ & 0x3;
							if (i_405_ > 0) {
								i_404_ = Class295_Sub1.anIntArray3691[(int) f];
								do {
									if (f_409_ < fs[++i])
										is[i] = i_404_;
									f_409_ += f_410_;
								} while (--i_405_ > 0);
							}
						} else {
							int i_421_ = anInt108;
							int i_422_ = 256 - anInt108;
							if (i_405_ > 0) {
								do {
									i_404_ = (Class295_Sub1.anIntArray3691[(int) f]);
									f += f_408_;
									i_404_ = (((i_404_ & 0xff00ff) * i_422_ >> 8 & 0xff00ff) + ((i_404_ & 0xff00) * i_422_ >> 8 & 0xff00));
									if (f_409_ < fs[++i]) {
										int i_423_ = is[i];
										is[i] = ((i_422_ | i_423_ >> 24) << 24 | (i_404_ + (((i_423_ & 0xff00ff) * i_421_) >> 8 & 0xff00ff) + (((i_423_ & 0xff00) * i_421_) >> 8 & 0xff00)));
									}
									f_409_ += f_410_;
									if (f_409_ < fs[++i]) {
										int i_424_ = is[i];
										is[i] = ((i_422_ | i_424_ >> 24) << 24 | (i_404_ + (((i_424_ & 0xff00ff) * i_421_) >> 8 & 0xff00ff) + (((i_424_ & 0xff00) * i_421_) >> 8 & 0xff00)));
									}
									f_409_ += f_410_;
									if (f_409_ < fs[++i]) {
										int i_425_ = is[i];
										is[i] = ((i_422_ | i_425_ >> 24) << 24 | (i_404_ + (((i_425_ & 0xff00ff) * i_421_) >> 8 & 0xff00ff) + (((i_425_ & 0xff00) * i_421_) >> 8 & 0xff00)));
									}
									f_409_ += f_410_;
									if (f_409_ < fs[++i]) {
										int i_426_ = is[i];
										is[i] = ((i_422_ | i_426_ >> 24) << 24 | (i_404_ + (((i_426_ & 0xff00ff) * i_421_) >> 8 & 0xff00ff) + (((i_426_ & 0xff00) * i_421_) >> 8 & 0xff00)));
									}
									f_409_ += f_410_;
								} while (--i_405_ > 0);
							}
							i_405_ = i_407_ - i_406_ & 0x3;
							if (i_405_ <= 0)
								break;
							i_404_ = Class295_Sub1.anIntArray3691[(int) f];
							i_404_ = (((i_404_ & 0xff00ff) * i_422_ >> 8 & 0xff00ff) + ((i_404_ & 0xff00) * i_422_ >> 8 & 0xff00));
							do {
								if (f_409_ < fs[++i]) {
									int i_427_ = is[i];
									is[i] = ((i_422_ | i_427_ >> 24) << 24 | (i_404_ + (((i_427_ & 0xff00ff) * i_421_ >> 8) & 0xff00ff) + (((i_427_ & 0xff00) * i_421_ >> 8) & 0xff00)));
								}
								f_409_ += f_410_;
							} while (--i_405_ > 0);
						}
						break;
					}
					i_405_ = i_407_ - i_406_;
					if (anInt108 == 0) {
						do {
							if (f_409_ < fs[++i])
								is[i] = Class295_Sub1.anIntArray3691[(int) f];
							f_409_ += f_410_;
							f += f_408_;
						} while (--i_405_ > 0);
						break;
					}
					int i_428_ = anInt108;
					int i_429_ = 256 - anInt108;
					do {
						if (f_409_ < fs[++i]) {
							i_404_ = Class295_Sub1.anIntArray3691[(int) f];
							i_404_ = (((i_404_ & 0xff00ff) * i_429_ >> 8 & 0xff00ff) + ((i_404_ & 0xff00) * i_429_ >> 8 & 0xff00));
							int i_430_ = is[i];
							is[i] = ((i_429_ | i_430_ >> 24) << 24 | (i_404_ + ((i_430_ & 0xff00ff) * i_428_ >> 8 & 0xff00ff) + ((i_430_ & 0xff00) * i_428_ >> 8 & 0xff00)));
						}
						f += f_408_;
						f_409_ += f_410_;
					} while (--i_405_ > 0);
				} while (false);
			}
		}
	}

	final void method208(boolean bool) {
		aBoolean100 = bool;
	}

	final void method209(float f, float f_431_, float f_432_, float f_433_, float f_434_, float f_435_, float f_436_, float f_437_, float f_438_, float f_439_, float f_440_, float f_441_, float f_442_, float f_443_, float f_444_, int i, int i_445_, int i_446_, int i_447_, int i_448_, int i_449_, int i_450_, int i_451_) {
		if (i_451_ != anInt124) {
			anIntArray123 = aHa_Sub2_96.method1260(i_451_);
			if (anIntArray123 == null) {
				method198((float) (int) f, (float) (int) f_431_, (float) (int) f_432_, (float) (int) f_433_, (float) (int) f_434_, (float) (int) f_435_, (float) (int) f_436_, (float) (int) f_437_, (float) (int) f_438_, Class69_Sub4.method745(i, 16711680, i_447_ | i_448_ << 24), Class69_Sub4.method745(i_445_, 16711680, i_447_ | i_449_ << 24), Class69_Sub4.method745(i_446_, 16711680, i_447_ | i_450_ << 24));
				return;
			}
			anInt113 = aHa_Sub2_96.method1262(i_451_) ? 64 : aHa_Sub2_96.anInt4081;
			anInt112 = anInt113 - 1;
			anInt115 = aHa_Sub2_96.method1257(i_451_);
			aBoolean119 = aHa_Sub2_96.method1268(i_451_);
		}
		anInt125 = i_447_;
		float f_452_ = (float) (i >> 24 & 0xff);
		float f_453_ = (float) (i_445_ >> 24 & 0xff);
		float f_454_ = (float) (i_446_ >> 24 & 0xff);
		float f_455_ = (float) (i >> 16 & 0xff);
		float f_456_ = (float) (i_445_ >> 16 & 0xff);
		float f_457_ = (float) (i_446_ >> 16 & 0xff);
		float f_458_ = (float) (i >> 8 & 0xff);
		float f_459_ = (float) (i_445_ >> 8 & 0xff);
		float f_460_ = (float) (i_446_ >> 8 & 0xff);
		float f_461_ = (float) (i & 0xff);
		float f_462_ = (float) (i_445_ & 0xff);
		float f_463_ = (float) (i_446_ & 0xff);
		f_439_ /= f_436_;
		f_440_ /= f_437_;
		f_441_ /= f_438_;
		f_442_ /= f_436_;
		f_443_ /= f_437_;
		f_444_ /= f_438_;
		f_436_ = 1.0F / f_436_;
		f_437_ = 1.0F / f_437_;
		f_438_ = 1.0F / f_438_;
		float f_464_ = 0.0F;
		float f_465_ = 0.0F;
		float f_466_ = 0.0F;
		float f_467_ = 0.0F;
		float f_468_ = 0.0F;
		float f_469_ = 0.0F;
		float f_470_ = 0.0F;
		float f_471_ = 0.0F;
		float f_472_ = 0.0F;
		if (f_431_ != f) {
			float f_473_ = f_431_ - f;
			f_464_ = (f_434_ - f_433_) / f_473_;
			f_465_ = (f_437_ - f_436_) / f_473_;
			f_466_ = (f_440_ - f_439_) / f_473_;
			f_467_ = (f_443_ - f_442_) / f_473_;
			f_468_ = (float) (i_449_ - i_448_) / f_473_;
			f_469_ = (f_453_ - f_452_) / f_473_;
			f_470_ = (f_456_ - f_455_) / f_473_;
			f_471_ = (f_459_ - f_458_) / f_473_;
			f_472_ = (f_462_ - f_461_) / f_473_;
		}
		float f_474_ = 0.0F;
		float f_475_ = 0.0F;
		float f_476_ = 0.0F;
		float f_477_ = 0.0F;
		float f_478_ = 0.0F;
		float f_479_ = 0.0F;
		float f_480_ = 0.0F;
		float f_481_ = 0.0F;
		float f_482_ = 0.0F;
		if (f_432_ != f_431_) {
			float f_483_ = f_432_ - f_431_;
			f_474_ = (f_435_ - f_434_) / f_483_;
			f_475_ = (f_438_ - f_437_) / f_483_;
			f_476_ = (f_441_ - f_440_) / f_483_;
			f_477_ = (f_444_ - f_443_) / f_483_;
			f_478_ = (float) (i_450_ - i_449_) / f_483_;
			f_479_ = (f_454_ - f_453_) / f_483_;
			f_480_ = (f_457_ - f_456_) / f_483_;
			f_481_ = (f_460_ - f_459_) / f_483_;
			f_482_ = (f_463_ - f_462_) / f_483_;
		}
		float f_484_ = 0.0F;
		float f_485_ = 0.0F;
		float f_486_ = 0.0F;
		float f_487_ = 0.0F;
		float f_488_ = 0.0F;
		float f_489_ = 0.0F;
		float f_490_ = 0.0F;
		float f_491_ = 0.0F;
		float f_492_ = 0.0F;
		if (f != f_432_) {
			float f_493_ = f - f_432_;
			f_484_ = (f_433_ - f_435_) / f_493_;
			f_485_ = (f_436_ - f_438_) / f_493_;
			f_486_ = (f_439_ - f_441_) / f_493_;
			f_487_ = (f_442_ - f_444_) / f_493_;
			f_488_ = (float) (i_448_ - i_450_) / f_493_;
			f_489_ = (f_452_ - f_454_) / f_493_;
			f_490_ = (f_455_ - f_457_) / f_493_;
			f_491_ = (f_458_ - f_460_) / f_493_;
			f_492_ = (f_461_ - f_463_) / f_493_;
		}
		if (f <= f_431_ && f <= f_432_) {
			if (!(f >= (float) anInt101)) {
				if (f_431_ > (float) anInt101)
					f_431_ = (float) anInt101;
				if (f_432_ > (float) anInt101)
					f_432_ = (float) anInt101;
				if (f_431_ < f_432_) {
					f_435_ = f_433_;
					f_438_ = f_436_;
					f_441_ = f_439_;
					f_444_ = f_442_;
					i_450_ = i_448_;
					f_454_ = f_452_;
					f_457_ = f_455_;
					f_460_ = f_458_;
					f_463_ = f_461_;
					if (f < 0.0F) {
						f_433_ -= f_464_ * f;
						f_435_ -= f_484_ * f;
						f_436_ -= f_465_ * f;
						f_438_ -= f_485_ * f;
						f_439_ -= f_466_ * f;
						f_441_ -= f_486_ * f;
						f_442_ -= f_467_ * f;
						f_444_ -= f_487_ * f;
						i_448_ -= f_468_ * f;
						i_450_ -= f_488_ * f;
						f_452_ -= f_469_ * f;
						f_454_ -= f_489_ * f;
						f_455_ -= f_469_ * f;
						f_457_ -= f_489_ * f;
						f_458_ -= f_469_ * f;
						f_460_ -= f_489_ * f;
						f_461_ -= f_469_ * f;
						f_463_ -= f_489_ * f;
						f = 0.0F;
					}
					if (f_431_ < 0.0F) {
						f_434_ -= f_474_ * f_431_;
						f_437_ -= f_475_ * f_431_;
						f_440_ -= f_476_ * f_431_;
						f_443_ -= f_477_ * f_431_;
						i_449_ -= f_478_ * f_431_;
						f_453_ -= f_479_ * f_431_;
						f_456_ -= f_480_ * f_431_;
						f_459_ -= f_481_ * f_431_;
						f_462_ -= f_482_ * f_431_;
						f_431_ = 0.0F;
					}
					if (f != f_431_ && f_484_ < f_464_ || f == f_431_ && f_484_ > f_474_) {
						f_432_ -= f_431_;
						f_431_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_431_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f, (int) f_435_, (int) f_433_, f_438_, f_436_, f_441_, f_439_, f_444_, f_442_, (float) i_450_, (float) i_448_, f_454_, f_452_, f_457_, f_455_, f_460_, f_458_, f_463_, f_461_);
							f_433_ += f_464_;
							f_435_ += f_484_;
							f_436_ += f_465_;
							f_438_ += f_485_;
							f_439_ += f_466_;
							f_441_ += f_486_;
							f_442_ += f_467_;
							f_444_ += f_487_;
							i_448_ += f_468_;
							i_450_ += f_488_;
							f_452_ += f_469_;
							f_454_ += f_489_;
							f_455_ += f_470_;
							f_457_ += f_490_;
							f_458_ += f_471_;
							f_460_ += f_491_;
							f_461_ += f_472_;
							f_463_ += f_492_;
							f += (float) anInt98;
						}
						while (--f_432_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f, (int) f_435_, (int) f_434_, f_438_, f_437_, f_441_, f_440_, f_444_, f_443_, (float) i_450_, (float) i_449_, f_454_, f_453_, f_457_, f_456_, f_460_, f_459_, f_463_, f_462_);
							f_434_ += f_474_;
							f_435_ += f_484_;
							f_437_ += f_475_;
							f_438_ += f_485_;
							f_440_ += f_476_;
							f_441_ += f_486_;
							f_443_ += f_477_;
							f_444_ += f_487_;
							i_449_ += f_478_;
							i_450_ += f_488_;
							f_453_ += f_479_;
							f_454_ += f_489_;
							f_456_ += f_480_;
							f_457_ += f_490_;
							f_459_ += f_481_;
							f_460_ += f_491_;
							f_462_ += f_482_;
							f_463_ += f_492_;
							f += (float) anInt98;
						}
					} else {
						f_432_ -= f_431_;
						f_431_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_431_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f, (int) f_433_, (int) f_435_, f_436_, f_438_, f_439_, f_441_, f_442_, f_444_, (float) i_448_, (float) i_450_, f_452_, f_454_, f_455_, f_457_, f_458_, f_460_, f_461_, f_463_);
							f_433_ += f_464_;
							f_435_ += f_484_;
							f_436_ += f_465_;
							f_438_ += f_485_;
							f_439_ += f_466_;
							f_441_ += f_486_;
							f_442_ += f_467_;
							f_444_ += f_487_;
							i_448_ += f_468_;
							i_450_ += f_488_;
							f_452_ += f_469_;
							f_454_ += f_489_;
							f_455_ += f_470_;
							f_457_ += f_490_;
							f_458_ += f_471_;
							f_460_ += f_491_;
							f_461_ += f_472_;
							f_463_ += f_492_;
							f += (float) anInt98;
						}
						while (--f_432_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f, (int) f_434_, (int) f_435_, f_437_, f_438_, f_440_, f_441_, f_443_, f_444_, (float) i_449_, (float) i_450_, f_453_, f_454_, f_456_, f_457_, f_459_, f_460_, f_462_, f_463_);
							f_434_ += f_474_;
							f_435_ += f_484_;
							f_437_ += f_475_;
							f_438_ += f_485_;
							f_440_ += f_476_;
							f_441_ += f_486_;
							f_443_ += f_477_;
							f_444_ += f_487_;
							i_449_ += f_478_;
							i_450_ += f_488_;
							f_453_ += f_479_;
							f_454_ += f_489_;
							f_456_ += f_480_;
							f_457_ += f_490_;
							f_459_ += f_481_;
							f_460_ += f_491_;
							f_462_ += f_482_;
							f_463_ += f_492_;
							f += (float) anInt98;
						}
					}
				} else {
					f_434_ = f_433_;
					f_437_ = f_436_;
					f_440_ = f_439_;
					f_443_ = f_442_;
					i_449_ = i_448_;
					f_453_ = f_452_;
					f_456_ = f_455_;
					f_459_ = f_458_;
					f_462_ = f_461_;
					if (f < 0.0F) {
						f_433_ -= f_464_ * f;
						f_434_ -= f_484_ * f;
						f_436_ -= f_465_ * f;
						f_437_ -= f_485_ * f;
						f_439_ -= f_466_ * f;
						f_440_ -= f_486_ * f;
						f_442_ -= f_467_ * f;
						f_443_ -= f_487_ * f;
						i_448_ -= f_468_ * f;
						i_449_ -= f_488_ * f;
						f_452_ -= f_469_ * f;
						f_453_ -= f_489_ * f;
						f_455_ -= f_469_ * f;
						f_456_ -= f_489_ * f;
						f_458_ -= f_469_ * f;
						f_459_ -= f_489_ * f;
						f_461_ -= f_469_ * f;
						f_462_ -= f_489_ * f;
						f = 0.0F;
					}
					if (f_432_ < 0.0F) {
						f_435_ -= f_474_ * f_432_;
						f_438_ -= f_475_ * f_432_;
						f_441_ -= f_476_ * f_432_;
						f_444_ -= f_477_ * f_432_;
						i_450_ -= f_478_ * f_432_;
						f_454_ -= f_479_ * f_432_;
						f_457_ -= f_480_ * f_432_;
						f_460_ -= f_481_ * f_432_;
						f_463_ -= f_482_ * f_432_;
						f_432_ = 0.0F;
					}
					if (f != f_432_ && f_484_ < f_464_ || f == f_432_ && f_474_ > f_464_) {
						f_431_ -= f_432_;
						f_432_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_432_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f, (int) f_434_, (int) f_433_, f_437_, f_436_, f_440_, f_439_, f_443_, f_442_, (float) i_449_, (float) i_448_, f_453_, f_452_, f_456_, f_455_, f_459_, f_458_, f_462_, f_461_);
							f_433_ += f_464_;
							f_434_ += f_484_;
							f_436_ += f_465_;
							f_437_ += f_485_;
							f_439_ += f_466_;
							f_440_ += f_486_;
							f_442_ += f_467_;
							f_443_ += f_487_;
							i_448_ += f_468_;
							i_449_ += f_488_;
							f_452_ += f_469_;
							f_453_ += f_489_;
							f_455_ += f_470_;
							f_456_ += f_490_;
							f_458_ += f_471_;
							f_459_ += f_491_;
							f_461_ += f_472_;
							f_462_ += f_492_;
							f += (float) anInt98;
						}
						while (--f_431_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f, (int) f_435_, (int) f_433_, f_438_, f_436_, f_441_, f_439_, f_444_, f_442_, (float) i_450_, (float) i_448_, f_454_, f_452_, f_457_, f_455_, f_460_, f_458_, f_463_, f_461_);
							f_435_ += f_474_;
							f_433_ += f_464_;
							f_438_ += f_475_;
							f_436_ += f_465_;
							f_441_ += f_476_;
							f_439_ += f_466_;
							f_444_ += f_477_;
							f_442_ += f_467_;
							i_450_ += f_478_;
							i_448_ += f_468_;
							f_454_ += f_479_;
							f_452_ += f_469_;
							f_457_ += f_480_;
							f_455_ += f_470_;
							f_460_ += f_481_;
							f_458_ += f_471_;
							f_463_ += f_482_;
							f_461_ += f_472_;
							f += (float) anInt98;
						}
					} else {
						f_431_ -= f_432_;
						f_432_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_432_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f, (int) f_433_, (int) f_434_, f_436_, f_437_, f_439_, f_440_, f_442_, f_443_, (float) i_448_, (float) i_449_, f_452_, f_453_, f_455_, f_456_, f_458_, f_459_, f_461_, f_462_);
							f_434_ += f_484_;
							f_433_ += f_464_;
							f_437_ += f_485_;
							f_436_ += f_465_;
							f_440_ += f_486_;
							f_439_ += f_466_;
							f_443_ += f_487_;
							f_442_ += f_467_;
							i_449_ += f_488_;
							i_448_ += f_468_;
							f_453_ += f_489_;
							f_452_ += f_469_;
							f_456_ += f_490_;
							f_455_ += f_470_;
							f_459_ += f_491_;
							f_458_ += f_471_;
							f_462_ += f_492_;
							f_461_ += f_472_;
							f += (float) anInt98;
						}
						while (--f_431_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f, (int) f_433_, (int) f_435_, f_436_, f_438_, f_439_, f_441_, f_442_, f_444_, (float) i_448_, (float) i_450_, f_452_, f_454_, f_455_, f_457_, f_458_, f_460_, f_461_, f_463_);
							f_433_ += f_464_;
							f_435_ += f_474_;
							f_436_ += f_465_;
							f_438_ += f_475_;
							f_439_ += f_466_;
							f_441_ += f_476_;
							f_442_ += f_467_;
							f_444_ += f_477_;
							i_448_ += f_468_;
							i_450_ += f_478_;
							f_452_ += f_469_;
							f_454_ += f_479_;
							f_455_ += f_470_;
							f_457_ += f_480_;
							f_458_ += f_471_;
							f_460_ += f_481_;
							f_461_ += f_472_;
							f_463_ += f_482_;
							f += (float) anInt98;
						}
					}
				}
			}
		} else if (f_431_ <= f_432_) {
			if (!(f_431_ >= (float) anInt101)) {
				if (f_432_ > (float) anInt101)
					f_432_ = (float) anInt101;
				if (f > (float) anInt101)
					f = (float) anInt101;
				if (f_432_ < f) {
					f_433_ = f_434_;
					f_436_ = f_437_;
					f_439_ = f_440_;
					f_442_ = f_443_;
					i_448_ = i_449_;
					f_452_ = f_453_;
					f_455_ = f_456_;
					f_458_ = f_459_;
					f_461_ = f_462_;
					if (f_431_ < 0.0F) {
						f_433_ -= f_464_ * f_431_;
						f_434_ -= f_474_ * f_431_;
						f_436_ -= f_465_ * f_431_;
						f_437_ -= f_475_ * f_431_;
						f_439_ -= f_466_ * f_431_;
						f_440_ -= f_476_ * f_431_;
						f_442_ -= f_467_ * f_431_;
						f_443_ -= f_477_ * f_431_;
						i_448_ -= f_468_ * f_431_;
						i_449_ -= f_478_ * f_431_;
						f_452_ -= f_469_ * f_431_;
						f_453_ -= f_479_ * f_431_;
						f_455_ -= f_470_ * f_431_;
						f_456_ -= f_480_ * f_431_;
						f_458_ -= f_471_ * f_431_;
						f_459_ -= f_481_ * f_431_;
						f_461_ -= f_472_ * f_431_;
						f_462_ -= f_482_ * f_431_;
						f_431_ = 0.0F;
					}
					if (f_432_ < 0.0F) {
						f_435_ -= f_484_ * f_432_;
						f_438_ -= f_485_ * f_432_;
						f_441_ -= f_486_ * f_432_;
						f_444_ -= f_487_ * f_432_;
						i_450_ -= f_488_ * f_432_;
						f_454_ -= f_489_ * f_432_;
						f_457_ -= f_490_ * f_432_;
						f_460_ -= f_491_ * f_432_;
						f_463_ -= f_492_ * f_432_;
						f_432_ = 0.0F;
					}
					if (f_431_ != f_432_ && f_464_ < f_474_ || f_431_ == f_432_ && f_464_ > f_484_) {
						f -= f_432_;
						f_432_ -= f_431_;
						f_431_ = (float) anIntArray97[(int) f_431_];
						while (--f_432_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f_431_, (int) f_433_, (int) f_434_, f_436_, f_437_, f_439_, f_440_, f_442_, f_443_, (float) i_448_, (float) i_449_, f_452_, f_453_, f_455_, f_456_, f_458_, f_459_, f_461_, f_462_);
							f_433_ += f_464_;
							f_434_ += f_474_;
							f_436_ += f_465_;
							f_437_ += f_475_;
							f_439_ += f_466_;
							f_440_ += f_476_;
							f_442_ += f_467_;
							f_443_ += f_477_;
							i_448_ += f_468_;
							i_449_ += f_478_;
							f_452_ += f_469_;
							f_453_ += f_479_;
							f_455_ += f_470_;
							f_456_ += f_480_;
							f_458_ += f_471_;
							f_459_ += f_481_;
							f_461_ += f_472_;
							f_462_ += f_482_;
							f_431_ += (float) anInt98;
						}
						while (--f >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f_431_, (int) f_433_, (int) f_435_, f_436_, f_438_, f_439_, f_441_, f_442_, f_444_, (float) i_448_, (float) i_450_, f_452_, f_454_, f_455_, f_457_, f_458_, f_460_, f_461_, f_463_);
							f_433_ += f_464_;
							f_435_ += f_484_;
							f_436_ += f_465_;
							f_438_ += f_485_;
							f_439_ += f_466_;
							f_441_ += f_486_;
							f_442_ += f_467_;
							f_444_ += f_487_;
							i_448_ += f_468_;
							i_450_ += f_488_;
							f_452_ += f_469_;
							f_454_ += f_489_;
							f_455_ += f_470_;
							f_457_ += f_490_;
							f_458_ += f_471_;
							f_460_ += f_491_;
							f_461_ += f_472_;
							f_463_ += f_492_;
							f_431_ += (float) anInt98;
						}
					} else {
						f -= f_432_;
						f_432_ -= f_431_;
						f_431_ = (float) anIntArray97[(int) f_431_];
						while (--f_432_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f_431_, (int) f_434_, (int) f_433_, f_437_, f_436_, f_440_, f_439_, f_443_, f_442_, (float) i_449_, (float) i_448_, f_453_, f_452_, f_456_, f_455_, f_459_, f_458_, f_462_, f_461_);
							f_434_ += f_474_;
							f_433_ += f_464_;
							f_437_ += f_475_;
							f_436_ += f_465_;
							f_440_ += f_476_;
							f_439_ += f_466_;
							f_443_ += f_477_;
							f_442_ += f_467_;
							i_449_ += f_478_;
							i_448_ += f_468_;
							f_453_ += f_479_;
							f_452_ += f_469_;
							f_456_ += f_480_;
							f_455_ += f_470_;
							f_459_ += f_481_;
							f_458_ += f_471_;
							f_462_ += f_482_;
							f_461_ += f_472_;
							f_431_ += (float) anInt98;
						}
						while (--f >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f_431_, (int) f_435_, (int) f_433_, f_438_, f_436_, f_441_, f_439_, f_444_, f_442_, (float) i_450_, (float) i_448_, f_454_, f_452_, f_457_, f_455_, f_460_, f_458_, f_463_, f_461_);
							f_435_ += f_484_;
							f_433_ += f_464_;
							f_438_ += f_485_;
							f_436_ += f_465_;
							f_441_ += f_486_;
							f_439_ += f_466_;
							f_444_ += f_487_;
							f_442_ += f_467_;
							i_450_ += f_488_;
							i_448_ += f_468_;
							f_454_ += f_489_;
							f_452_ += f_469_;
							f_457_ += f_490_;
							f_455_ += f_470_;
							f_460_ += f_491_;
							f_458_ += f_471_;
							f_463_ += f_492_;
							f_461_ += f_472_;
							f_431_ += (float) anInt98;
						}
					}
				} else {
					f_435_ = f_434_;
					f_438_ = f_437_;
					f_441_ = f_440_;
					f_444_ = f_443_;
					i_450_ = i_449_;
					f_454_ = f_453_;
					f_457_ = f_456_;
					f_460_ = f_459_;
					f_463_ = f_462_;
					if (f_431_ < 0.0F) {
						f_435_ -= f_464_ * f_431_;
						f_434_ -= f_474_ * f_431_;
						f_438_ -= f_465_ * f_431_;
						f_437_ -= f_475_ * f_431_;
						f_441_ -= f_466_ * f_431_;
						f_440_ -= f_476_ * f_431_;
						f_444_ -= f_467_ * f_431_;
						f_443_ -= f_477_ * f_431_;
						i_450_ -= f_468_ * f_431_;
						i_449_ -= f_478_ * f_431_;
						f_454_ -= f_469_ * f_431_;
						f_453_ -= f_479_ * f_431_;
						f_457_ -= f_470_ * f_431_;
						f_456_ -= f_480_ * f_431_;
						f_460_ -= f_471_ * f_431_;
						f_459_ -= f_481_ * f_431_;
						f_463_ -= f_472_ * f_431_;
						f_462_ -= f_482_ * f_431_;
						f_431_ = 0.0F;
					}
					if (f < 0.0F) {
						f_433_ -= f_484_ * f;
						f_436_ -= f_485_ * f;
						f_439_ -= f_486_ * f;
						f_442_ -= f_487_ * f;
						i_448_ -= f_488_ * f;
						f_452_ -= f_489_ * f;
						f_455_ -= f_490_ * f;
						f_458_ -= f_491_ * f;
						f_461_ -= f_492_ * f;
						f = 0.0F;
					}
					f_432_ -= f;
					f -= f_431_;
					f_431_ = (float) anIntArray97[(int) f_431_];
					if (f_464_ < f_474_) {
						while (--f >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f_431_, (int) f_435_, (int) f_434_, f_438_, f_437_, f_441_, f_440_, f_444_, f_443_, (float) i_450_, (float) i_449_, f_454_, f_453_, f_457_, f_456_, f_460_, f_459_, f_463_, f_462_);
							f_435_ += f_464_;
							f_434_ += f_474_;
							f_438_ += f_465_;
							f_437_ += f_475_;
							f_441_ += f_466_;
							f_440_ += f_476_;
							f_444_ += f_467_;
							f_443_ += f_477_;
							i_450_ += f_468_;
							i_449_ += f_478_;
							f_454_ += f_469_;
							f_453_ += f_479_;
							f_457_ += f_470_;
							f_456_ += f_480_;
							f_460_ += f_471_;
							f_459_ += f_481_;
							f_463_ += f_472_;
							f_462_ += f_482_;
							f_431_ += (float) anInt98;
						}
						while (--f_432_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f_431_, (int) f_433_, (int) f_434_, f_436_, f_437_, f_439_, f_440_, f_442_, f_443_, (float) i_448_, (float) i_449_, f_452_, f_453_, f_455_, f_456_, f_458_, f_459_, f_461_, f_462_);
							f_433_ += f_484_;
							f_434_ += f_474_;
							f_436_ += f_485_;
							f_437_ += f_475_;
							f_439_ += f_486_;
							f_440_ += f_476_;
							f_442_ += f_487_;
							f_443_ += f_477_;
							i_448_ += f_488_;
							i_449_ += f_478_;
							f_452_ += f_489_;
							f_453_ += f_479_;
							f_455_ += f_490_;
							f_456_ += f_480_;
							f_458_ += f_491_;
							f_459_ += f_481_;
							f_461_ += f_492_;
							f_462_ += f_482_;
							f_431_ += (float) anInt98;
						}
					} else {
						while (--f >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f_431_, (int) f_434_, (int) f_435_, f_437_, f_438_, f_440_, f_441_, f_443_, f_444_, (float) i_449_, (float) i_450_, f_453_, f_454_, f_456_, f_457_, f_459_, f_460_, f_462_, f_463_);
							f_434_ += f_474_;
							f_435_ += f_464_;
							f_437_ += f_475_;
							f_438_ += f_465_;
							f_440_ += f_476_;
							f_441_ += f_466_;
							f_443_ += f_477_;
							f_444_ += f_467_;
							i_449_ += f_478_;
							i_450_ += f_468_;
							f_453_ += f_479_;
							f_454_ += f_469_;
							f_456_ += f_480_;
							f_457_ += f_470_;
							f_459_ += f_481_;
							f_460_ += f_471_;
							f_462_ += f_482_;
							f_463_ += f_472_;
							f_431_ += (float) anInt98;
						}
						while (--f_432_ >= 0.0F) {
							method201(anIntArray99, anIntArray123, (int) f_431_, (int) f_434_, (int) f_433_, f_437_, f_436_, f_440_, f_439_, f_443_, f_442_, (float) i_449_, (float) i_448_, f_453_, f_452_, f_456_, f_455_, f_459_, f_458_, f_462_, f_461_);
							f_434_ += f_474_;
							f_433_ += f_484_;
							f_437_ += f_475_;
							f_436_ += f_485_;
							f_440_ += f_476_;
							f_439_ += f_486_;
							f_443_ += f_477_;
							f_442_ += f_487_;
							i_449_ += f_478_;
							i_448_ += f_488_;
							f_453_ += f_479_;
							f_452_ += f_489_;
							f_456_ += f_480_;
							f_455_ += f_490_;
							f_459_ += f_481_;
							f_458_ += f_491_;
							f_462_ += f_482_;
							f_461_ += f_492_;
							f_431_ += (float) anInt98;
						}
					}
				}
			}
		} else if (!(f_432_ >= (float) anInt101)) {
			if (f > (float) anInt101)
				f = (float) anInt101;
			if (f_431_ > (float) anInt101)
				f_431_ = (float) anInt101;
			if (f < f_431_) {
				f_434_ = f_435_;
				f_437_ = f_438_;
				f_440_ = f_441_;
				f_443_ = f_444_;
				i_449_ = i_450_;
				f_453_ = f_454_;
				f_456_ = f_457_;
				f_459_ = f_460_;
				f_462_ = f_463_;
				if (f_432_ < 0.0F) {
					f_435_ -= f_484_ * f_432_;
					f_434_ -= f_474_ * f_432_;
					f_438_ -= f_485_ * f_432_;
					f_437_ -= f_475_ * f_432_;
					f_441_ -= f_486_ * f_432_;
					f_440_ -= f_476_ * f_432_;
					f_444_ -= f_487_ * f_432_;
					f_443_ -= f_477_ * f_432_;
					i_450_ -= f_488_ * 3.0F;
					i_449_ -= f_478_ * f_432_;
					f_454_ -= f_489_ * f_432_;
					f_453_ -= f_479_ * f_432_;
					f_457_ -= f_490_ * f_432_;
					f_456_ -= f_480_ * f_432_;
					f_460_ -= f_491_ * f_432_;
					f_459_ -= f_481_ * f_432_;
					f_463_ -= f_492_ * f_432_;
					f_462_ -= f_482_ * f_432_;
					f_432_ = 0.0F;
				}
				if (f < 0.0F) {
					f_433_ -= f_464_ * f;
					f_436_ -= f_465_ * f;
					f_439_ -= f_466_ * f;
					f_442_ -= f_467_ * f;
					i_448_ -= f_468_ * f;
					f_452_ -= f_469_ * f;
					f_455_ -= f_470_ * f;
					f_458_ -= f_471_ * f;
					f_461_ -= f_472_ * f;
					f = 0.0F;
				}
				if (f_474_ < f_484_) {
					f_431_ -= f;
					f -= f_432_;
					f_432_ = (float) anIntArray97[(int) f_432_];
					while (--f >= 0.0F) {
						method201(anIntArray99, anIntArray123, (int) f_432_, (int) f_434_, (int) f_435_, f_437_, f_438_, f_440_, f_441_, f_443_, f_444_, (float) i_449_, (float) i_450_, f_453_, f_454_, f_456_, f_457_, f_459_, f_460_, f_462_, f_463_);
						f_434_ += f_474_;
						f_435_ += f_484_;
						f_437_ += f_475_;
						f_438_ += f_485_;
						f_440_ += f_476_;
						f_441_ += f_486_;
						f_443_ += f_477_;
						f_444_ += f_487_;
						i_449_ += f_478_;
						i_450_ += f_488_;
						f_453_ += f_479_;
						f_454_ += f_489_;
						f_456_ += f_480_;
						f_457_ += f_490_;
						f_459_ += f_481_;
						f_460_ += f_491_;
						f_462_ += f_482_;
						f_463_ += f_492_;
						f_432_ += (float) anInt98;
					}
					while (--f_431_ >= 0.0F) {
						method201(anIntArray99, anIntArray123, (int) f_432_, (int) f_434_, (int) f_433_, f_437_, f_436_, f_440_, f_439_, f_443_, f_442_, (float) i_449_, (float) i_448_, f_453_, f_452_, f_456_, f_455_, f_459_, f_458_, f_462_, f_461_);
						f_434_ += f_474_;
						f_433_ += f_464_;
						f_437_ += f_475_;
						f_436_ += f_465_;
						f_440_ += f_476_;
						f_439_ += f_466_;
						f_443_ += f_477_;
						f_442_ += f_467_;
						i_449_ += f_478_;
						i_448_ += f_468_;
						f_453_ += f_479_;
						f_452_ += f_469_;
						f_456_ += f_480_;
						f_455_ += f_470_;
						f_459_ += f_481_;
						f_458_ += f_471_;
						f_462_ += f_482_;
						f_461_ += f_472_;
						f_432_ += (float) anInt98;
					}
				} else {
					f_431_ -= f;
					f -= f_432_;
					f_432_ = (float) anIntArray97[(int) f_432_];
					while (--f >= 0.0F) {
						method201(anIntArray99, anIntArray123, (int) f_432_, (int) f_435_, (int) f_434_, f_438_, f_437_, f_441_, f_440_, f_444_, f_443_, (float) i_450_, (float) i_449_, f_454_, f_453_, f_457_, f_456_, f_460_, f_459_, f_463_, f_462_);
						f_435_ += f_484_;
						f_434_ += f_474_;
						f_438_ += f_485_;
						f_437_ += f_475_;
						f_441_ += f_486_;
						f_440_ += f_476_;
						f_444_ += f_487_;
						f_443_ += f_477_;
						i_450_ += f_488_;
						i_449_ += f_478_;
						f_454_ += f_489_;
						f_453_ += f_479_;
						f_457_ += f_490_;
						f_456_ += f_480_;
						f_460_ += f_491_;
						f_459_ += f_481_;
						f_463_ += f_492_;
						f_462_ += f_482_;
						f_432_ += (float) anInt98;
					}
					while (--f_431_ >= 0.0F) {
						method201(anIntArray99, anIntArray123, (int) f_432_, (int) f_433_, (int) f_434_, f_436_, f_437_, f_439_, f_440_, f_442_, f_443_, (float) i_448_, (float) i_449_, f_452_, f_453_, f_455_, f_456_, f_458_, f_459_, f_461_, f_462_);
						f_433_ += f_464_;
						f_434_ += f_474_;
						f_436_ += f_465_;
						f_437_ += f_475_;
						f_439_ += f_466_;
						f_440_ += f_476_;
						f_442_ += f_467_;
						f_443_ += f_477_;
						i_448_ += f_468_;
						i_449_ += f_478_;
						f_452_ += f_469_;
						f_453_ += f_479_;
						f_455_ += f_470_;
						f_456_ += f_480_;
						f_458_ += f_471_;
						f_459_ += f_481_;
						f_461_ += f_472_;
						f_462_ += f_482_;
						f_432_ += (float) anInt98;
					}
				}
			} else {
				f_433_ = f_435_;
				f_436_ = f_438_;
				f_439_ = f_441_;
				f_442_ = f_444_;
				i_448_ = i_450_;
				f_452_ = f_454_;
				f_455_ = f_457_;
				f_458_ = f_460_;
				f_461_ = f_463_;
				if (f_432_ < 0.0F) {
					f_435_ -= f_484_ * f_432_;
					f_433_ -= f_474_ * f_432_;
					f_438_ -= f_485_ * f_432_;
					f_436_ -= f_475_ * f_432_;
					f_441_ -= f_486_ * f_432_;
					f_439_ -= f_476_ * f_432_;
					f_444_ -= f_487_ * f_432_;
					f_442_ -= f_477_ * f_432_;
					i_450_ -= f_488_ * 3.0F;
					i_448_ -= f_478_ * f_432_;
					f_454_ -= f_489_ * f_432_;
					f_452_ -= f_479_ * f_432_;
					f_457_ -= f_490_ * f_432_;
					f_455_ -= f_480_ * f_432_;
					f_460_ -= f_491_ * f_432_;
					f_458_ -= f_481_ * f_432_;
					f_463_ -= f_492_ * f_432_;
					f_461_ -= f_482_ * f_432_;
					f_432_ = 0.0F;
				}
				if (f_431_ < 0.0F) {
					f_434_ -= f_464_ * f_431_;
					f_437_ -= f_465_ * f_431_;
					f_440_ -= f_466_ * f_431_;
					f_443_ -= f_467_ * f_431_;
					i_449_ -= f_468_ * f_431_;
					f_453_ -= f_469_ * f_431_;
					f_456_ -= f_470_ * f_431_;
					f_459_ -= f_471_ * f_431_;
					f_462_ -= f_472_ * f_431_;
					f_431_ = 0.0F;
				}
				if (f_474_ < f_484_) {
					f -= f_431_;
					f_431_ -= f_432_;
					f_432_ = (float) anIntArray97[(int) f_432_];
					while (--f_431_ >= 0.0F) {
						method201(anIntArray99, anIntArray123, (int) f_432_, (int) f_433_, (int) f_435_, f_436_, f_438_, f_439_, f_441_, f_442_, f_444_, (float) i_448_, (float) i_450_, f_452_, f_454_, f_455_, f_457_, f_458_, f_460_, f_461_, f_463_);
						f_433_ += f_474_;
						f_435_ += f_484_;
						f_436_ += f_475_;
						f_438_ += f_485_;
						f_439_ += f_476_;
						f_441_ += f_486_;
						f_442_ += f_477_;
						f_444_ += f_487_;
						i_448_ += f_478_;
						i_450_ += f_488_;
						f_452_ += f_479_;
						f_454_ += f_489_;
						f_455_ += f_480_;
						f_457_ += f_490_;
						f_458_ += f_481_;
						f_460_ += f_491_;
						f_461_ += f_482_;
						f_463_ += f_492_;
						f_432_ += (float) anInt98;
					}
					while (--f >= 0.0F) {
						method201(anIntArray99, anIntArray123, (int) f_432_, (int) f_434_, (int) f_435_, f_437_, f_438_, f_440_, f_441_, f_443_, f_444_, (float) i_449_, (float) i_450_, f_453_, f_454_, f_456_, f_457_, f_459_, f_460_, f_462_, f_463_);
						f_434_ += f_464_;
						f_435_ += f_484_;
						f_437_ += f_465_;
						f_438_ += f_485_;
						f_440_ += f_466_;
						f_441_ += f_486_;
						f_443_ += f_467_;
						f_444_ += f_487_;
						i_449_ += f_468_;
						i_450_ += f_488_;
						f_453_ += f_469_;
						f_454_ += f_489_;
						f_456_ += f_470_;
						f_457_ += f_490_;
						f_459_ += f_471_;
						f_460_ += f_491_;
						f_462_ += f_472_;
						f_463_ += f_492_;
						f_432_ += (float) anInt98;
					}
				} else {
					f -= f_431_;
					f_431_ -= f_432_;
					f_432_ = (float) anIntArray97[(int) f_432_];
					while (--f_431_ >= 0.0F) {
						method201(anIntArray99, anIntArray123, (int) f_432_, (int) f_435_, (int) f_433_, f_438_, f_436_, f_441_, f_439_, f_444_, f_442_, (float) i_450_, (float) i_448_, f_454_, f_452_, f_457_, f_455_, f_460_, f_458_, f_463_, f_461_);
						f_435_ += f_484_;
						f_433_ += f_474_;
						f_438_ += f_485_;
						f_436_ += f_475_;
						f_441_ += f_486_;
						f_439_ += f_476_;
						f_444_ += f_487_;
						f_442_ += f_477_;
						i_450_ += f_488_;
						i_448_ += f_478_;
						f_454_ += f_489_;
						f_452_ += f_479_;
						f_457_ += f_490_;
						f_455_ += f_480_;
						f_460_ += f_491_;
						f_458_ += f_481_;
						f_463_ += f_492_;
						f_461_ += f_482_;
						f_432_ += (float) anInt98;
					}
					while (--f >= 0.0F) {
						method201(anIntArray99, anIntArray123, (int) f_432_, (int) f_435_, (int) f_434_, f_438_, f_437_, f_441_, f_440_, f_444_, f_443_, (float) i_450_, (float) i_449_, f_454_, f_453_, f_457_, f_456_, f_460_, f_459_, f_463_, f_462_);
						f_435_ += f_484_;
						f_434_ += f_464_;
						f_438_ += f_485_;
						f_437_ += f_465_;
						f_441_ += f_486_;
						f_440_ += f_466_;
						f_444_ += f_487_;
						f_443_ += f_467_;
						i_450_ += f_488_;
						i_449_ += f_468_;
						f_454_ += f_489_;
						f_453_ += f_469_;
						f_457_ += f_490_;
						f_456_ += f_470_;
						f_460_ += f_491_;
						f_459_ += f_471_;
						f_463_ += f_492_;
						f_462_ += f_472_;
						f_432_ += (float) anInt98;
					}
				}
			}
		}
	}

	final int method210() {
		return anIntArray97[0] % anInt98;
	}

	private final void method211(int[] is, float[] fs, int i, int i_494_, int i_495_, int i_496_, int i_497_, float f, float f_498_, float f_499_, float f_500_) {
		if (aBoolean103) {
			if (i_497_ > anInt107)
				i_497_ = anInt107;
			if (i_496_ < 0)
				i_496_ = 0;
		}
		if (i_496_ < i_497_) {
			i += i_496_ - 1;
			f += f_498_ * (float) i_496_;
			f_499_ += f_500_ * (float) i_496_;
			if (aClass240_104.aBoolean2262) {
				do {
					if (aBoolean102) {
						i_495_ = i_497_ - i_496_ >> 2;
						f_498_ *= 4.0F;
						if (anInt108 == 0) {
							if (i_495_ > 0) {
								do {
									i_494_ = (Class295_Sub1.anIntArray3691[(int) f]);
									f += f_498_;
									if (f_499_ < fs[++i]) {
										is[i] = i_494_;
										fs[i] = f_499_;
									}
									f_499_ += f_500_;
									if (f_499_ < fs[++i]) {
										is[i] = i_494_;
										fs[i] = f_499_;
									}
									f_499_ += f_500_;
									if (f_499_ < fs[++i]) {
										is[i] = i_494_;
										fs[i] = f_499_;
									}
									f_499_ += f_500_;
									if (f_499_ < fs[++i]) {
										is[i] = i_494_;
										fs[i] = f_499_;
									}
									f_499_ += f_500_;
								} while (--i_495_ > 0);
							}
							i_495_ = i_497_ - i_496_ & 0x3;
							if (i_495_ > 0) {
								i_494_ = Class295_Sub1.anIntArray3691[(int) f];
								do {
									if (f_499_ < fs[++i]) {
										is[i] = i_494_;
										fs[i] = f_499_;
									}
									f_499_ += f_500_;
								} while (--i_495_ > 0);
							}
						} else {
							int i_501_ = anInt108;
							int i_502_ = 256 - anInt108;
							if (i_495_ > 0) {
								do {
									i_494_ = (Class295_Sub1.anIntArray3691[(int) f]);
									f += f_498_;
									i_494_ = (((i_494_ & 0xff00ff) * i_502_ >> 8 & 0xff00ff) + ((i_494_ & 0xff00) * i_502_ >> 8 & 0xff00));
									if (f_499_ < fs[++i]) {
										int i_503_ = is[i];
										is[i] = (i_494_ + (((i_503_ & 0xff00ff) * i_501_ >> 8) & 0xff00ff) + (((i_503_ & 0xff00) * i_501_ >> 8) & 0xff00));
										fs[i] = f_499_;
									}
									f_499_ += f_500_;
									if (f_499_ < fs[++i]) {
										int i_504_ = is[i];
										is[i] = (i_494_ + (((i_504_ & 0xff00ff) * i_501_ >> 8) & 0xff00ff) + (((i_504_ & 0xff00) * i_501_ >> 8) & 0xff00));
										fs[i] = f_499_;
									}
									f_499_ += f_500_;
									if (f_499_ < fs[++i]) {
										int i_505_ = is[i];
										is[i] = (i_494_ + (((i_505_ & 0xff00ff) * i_501_ >> 8) & 0xff00ff) + (((i_505_ & 0xff00) * i_501_ >> 8) & 0xff00));
										fs[i] = f_499_;
									}
									f_499_ += f_500_;
									if (f_499_ < fs[++i]) {
										int i_506_ = is[i];
										is[i] = (i_494_ + (((i_506_ & 0xff00ff) * i_501_ >> 8) & 0xff00ff) + (((i_506_ & 0xff00) * i_501_ >> 8) & 0xff00));
										fs[i] = f_499_;
									}
									f_499_ += f_500_;
								} while (--i_495_ > 0);
							}
							i_495_ = i_497_ - i_496_ & 0x3;
							if (i_495_ <= 0)
								break;
							i_494_ = Class295_Sub1.anIntArray3691[(int) f];
							i_494_ = (((i_494_ & 0xff00ff) * i_502_ >> 8 & 0xff00ff) + ((i_494_ & 0xff00) * i_502_ >> 8 & 0xff00));
							do {
								if (f_499_ < fs[++i]) {
									int i_507_ = is[i];
									is[i] = (i_494_ + ((i_507_ & 0xff00ff) * i_501_ >> 8 & 0xff00ff) + ((i_507_ & 0xff00) * i_501_ >> 8 & 0xff00));
									fs[i] = f_499_;
								}
								f_499_ += f_500_;
							} while (--i_495_ > 0);
						}
						break;
					}
					i_495_ = i_497_ - i_496_;
					if (anInt108 == 0) {
						do {
							if (f_499_ < fs[++i]) {
								is[i] = Class295_Sub1.anIntArray3691[(int) f];
								fs[i] = f_499_;
							}
							f_499_ += f_500_;
							f += f_498_;
						} while (--i_495_ > 0);
						break;
					}
					int i_508_ = anInt108;
					int i_509_ = 256 - anInt108;
					do {
						if (f_499_ < fs[++i]) {
							i_494_ = Class295_Sub1.anIntArray3691[(int) f];
							i_494_ = (((i_494_ & 0xff00ff) * i_509_ >> 8 & 0xff00ff) + ((i_494_ & 0xff00) * i_509_ >> 8 & 0xff00));
							int i_510_ = is[i];
							is[i] = (i_494_ + ((i_510_ & 0xff00ff) * i_508_ >> 8 & 0xff00ff) + ((i_510_ & 0xff00) * i_508_ >> 8 & 0xff00));
							fs[i] = f_499_;
						}
						f += f_498_;
						f_499_ += f_500_;
					} while (--i_495_ > 0);
				} while (false);
			} else {
				do {
					if (aBoolean102) {
						i_495_ = i_497_ - i_496_ >> 2;
						f_498_ *= 4.0F;
						if (anInt108 == 0) {
							if (i_495_ > 0) {
								do {
									i_494_ = (Class295_Sub1.anIntArray3691[(int) f]);
									f += f_498_;
									if (f_499_ < fs[++i])
										is[i] = i_494_;
									f_499_ += f_500_;
									if (f_499_ < fs[++i])
										is[i] = i_494_;
									f_499_ += f_500_;
									if (f_499_ < fs[++i])
										is[i] = i_494_;
									f_499_ += f_500_;
									if (f_499_ < fs[++i])
										is[i] = i_494_;
									f_499_ += f_500_;
								} while (--i_495_ > 0);
							}
							i_495_ = i_497_ - i_496_ & 0x3;
							if (i_495_ > 0) {
								i_494_ = Class295_Sub1.anIntArray3691[(int) f];
								do {
									if (f_499_ < fs[++i])
										is[i] = i_494_;
									f_499_ += f_500_;
								} while (--i_495_ > 0);
							}
						} else {
							int i_511_ = anInt108;
							int i_512_ = 256 - anInt108;
							if (i_495_ > 0) {
								do {
									i_494_ = (Class295_Sub1.anIntArray3691[(int) f]);
									f += f_498_;
									i_494_ = (((i_494_ & 0xff00ff) * i_512_ >> 8 & 0xff00ff) + ((i_494_ & 0xff00) * i_512_ >> 8 & 0xff00));
									if (f_499_ < fs[++i]) {
										int i_513_ = is[i];
										is[i] = (i_494_ + (((i_513_ & 0xff00ff) * i_511_ >> 8) & 0xff00ff) + (((i_513_ & 0xff00) * i_511_ >> 8) & 0xff00));
									}
									f_499_ += f_500_;
									if (f_499_ < fs[++i]) {
										int i_514_ = is[i];
										is[i] = (i_494_ + (((i_514_ & 0xff00ff) * i_511_ >> 8) & 0xff00ff) + (((i_514_ & 0xff00) * i_511_ >> 8) & 0xff00));
									}
									f_499_ += f_500_;
									if (f_499_ < fs[++i]) {
										int i_515_ = is[i];
										is[i] = (i_494_ + (((i_515_ & 0xff00ff) * i_511_ >> 8) & 0xff00ff) + (((i_515_ & 0xff00) * i_511_ >> 8) & 0xff00));
									}
									f_499_ += f_500_;
									if (f_499_ < fs[++i]) {
										int i_516_ = is[i];
										is[i] = (i_494_ + (((i_516_ & 0xff00ff) * i_511_ >> 8) & 0xff00ff) + (((i_516_ & 0xff00) * i_511_ >> 8) & 0xff00));
									}
									f_499_ += f_500_;
								} while (--i_495_ > 0);
							}
							i_495_ = i_497_ - i_496_ & 0x3;
							if (i_495_ <= 0)
								break;
							i_494_ = Class295_Sub1.anIntArray3691[(int) f];
							i_494_ = (((i_494_ & 0xff00ff) * i_512_ >> 8 & 0xff00ff) + ((i_494_ & 0xff00) * i_512_ >> 8 & 0xff00));
							do {
								if (f_499_ < fs[++i]) {
									int i_517_ = is[i];
									is[i] = (i_494_ + ((i_517_ & 0xff00ff) * i_511_ >> 8 & 0xff00ff) + ((i_517_ & 0xff00) * i_511_ >> 8 & 0xff00));
								}
								f_499_ += f_500_;
							} while (--i_495_ > 0);
						}
						break;
					}
					i_495_ = i_497_ - i_496_;
					if (anInt108 == 0) {
						do {
							if (f_499_ < fs[++i])
								is[i] = Class295_Sub1.anIntArray3691[(int) f];
							f_499_ += f_500_;
							f += f_498_;
						} while (--i_495_ > 0);
						break;
					}
					int i_518_ = anInt108;
					int i_519_ = 256 - anInt108;
					do {
						if (f_499_ < fs[++i]) {
							i_494_ = Class295_Sub1.anIntArray3691[(int) f];
							i_494_ = (((i_494_ & 0xff00ff) * i_519_ >> 8 & 0xff00ff) + ((i_494_ & 0xff00) * i_519_ >> 8 & 0xff00));
							int i_520_ = is[i];
							is[i] = (i_494_ + ((i_520_ & 0xff00ff) * i_518_ >> 8 & 0xff00ff) + ((i_520_ & 0xff00) * i_518_ >> 8 & 0xff00));
						}
						f += f_498_;
						f_499_ += f_500_;
					} while (--i_495_ > 0);
				} while (false);
			}
		}
	}

	final void method212(float f, float f_521_, float f_522_, float f_523_, float f_524_, float f_525_, float f_526_, float f_527_, float f_528_, int i) {
		if (aBoolean100) {
			aHa_Sub2_96.method1094((int) f_521_, (int) f, (int) f_524_, (int) f_523_, 123, i);
			aHa_Sub2_96.method1094((int) f_522_, (int) f_521_, (int) f_525_, (int) f_524_, 125, i);
			aHa_Sub2_96.method1094((int) f, (int) f_522_, (int) f_523_, (int) f_525_, 125, i);
		} else {
			float f_529_ = f_524_ - f_523_;
			float f_530_ = f_521_ - f;
			float f_531_ = f_525_ - f_523_;
			float f_532_ = f_522_ - f;
			float f_533_ = f_527_ - f_526_;
			float f_534_ = f_528_ - f_526_;
			float f_535_ = 0.0F;
			if (f_521_ != f)
				f_535_ = (f_524_ - f_523_) / (f_521_ - f);
			float f_536_ = 0.0F;
			if (f_522_ != f_521_)
				f_536_ = (f_525_ - f_524_) / (f_522_ - f_521_);
			float f_537_ = 0.0F;
			if (f_522_ != f)
				f_537_ = (f_523_ - f_525_) / (f - f_522_);
			float f_538_ = f_529_ * f_532_ - f_531_ * f_530_;
			if (f_538_ != 0.0F) {
				float f_539_ = (f_533_ * f_532_ - f_534_ * f_530_) / f_538_;
				float f_540_ = (f_534_ * f_529_ - f_533_ * f_531_) / f_538_;
				if (f <= f_521_ && f <= f_522_) {
					if (!(f >= (float) anInt101)) {
						if (f_521_ > (float) anInt101)
							f_521_ = (float) anInt101;
						if (f_522_ > (float) anInt101)
							f_522_ = (float) anInt101;
						f_526_ = f_526_ - f_539_ * f_523_ + f_539_;
						if (f_521_ < f_522_) {
							f_525_ = f_523_;
							if (f < 0.0F) {
								f_525_ -= f_537_ * f;
								f_523_ -= f_535_ * f;
								f_526_ -= f_540_ * f;
								f = 0.0F;
							}
							if (f_521_ < 0.0F) {
								f_524_ -= f_536_ * f_521_;
								f_521_ = 0.0F;
							}
							if (f != f_521_ && f_537_ < f_535_ || f == f_521_ && f_537_ > f_536_) {
								f_522_ -= f_521_;
								f_521_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_521_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_525_, (int) f_523_, f_526_, f_539_);
									f_525_ += f_537_;
									f_523_ += f_535_;
									f_526_ += f_540_;
									f += (float) anInt98;
								}
								while (--f_522_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_525_, (int) f_524_, f_526_, f_539_);
									f_525_ += f_537_;
									f_524_ += f_536_;
									f_526_ += f_540_;
									f += (float) anInt98;
								}
							} else {
								f_522_ -= f_521_;
								f_521_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_521_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_523_, (int) f_525_, f_526_, f_539_);
									f_525_ += f_537_;
									f_523_ += f_535_;
									f_526_ += f_540_;
									f += (float) anInt98;
								}
								while (--f_522_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_524_, (int) f_525_, f_526_, f_539_);
									f_525_ += f_537_;
									f_524_ += f_536_;
									f_526_ += f_540_;
									f += (float) anInt98;
								}
							}
						} else {
							f_524_ = f_523_;
							if (f < 0.0F) {
								f_524_ -= f_537_ * f;
								f_523_ -= f_535_ * f;
								f_526_ -= f_540_ * f;
								f = 0.0F;
							}
							if (f_522_ < 0.0F) {
								f_525_ -= f_536_ * f_522_;
								f_522_ = 0.0F;
							}
							if (f != f_522_ && f_537_ < f_535_ || f == f_522_ && f_536_ > f_535_) {
								f_521_ -= f_522_;
								f_522_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_522_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_524_, (int) f_523_, f_526_, f_539_);
									f_524_ += f_537_;
									f_523_ += f_535_;
									f_526_ += f_540_;
									f += (float) anInt98;
								}
								while (--f_521_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_525_, (int) f_523_, f_526_, f_539_);
									f_525_ += f_536_;
									f_523_ += f_535_;
									f_526_ += f_540_;
									f += (float) anInt98;
								}
							} else {
								f_521_ -= f_522_;
								f_522_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_522_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_523_, (int) f_524_, f_526_, f_539_);
									f_524_ += f_537_;
									f_523_ += f_535_;
									f_526_ += f_540_;
									f += (float) anInt98;
								}
								while (--f_521_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f, i, 0, (int) f_523_, (int) f_525_, f_526_, f_539_);
									f_525_ += f_536_;
									f_523_ += f_535_;
									f_526_ += f_540_;
									f += (float) anInt98;
								}
							}
						}
					}
				} else if (f_521_ <= f_522_) {
					if (!(f_521_ >= (float) anInt101)) {
						if (f_522_ > (float) anInt101)
							f_522_ = (float) anInt101;
						if (f > (float) anInt101)
							f = (float) anInt101;
						f_527_ = f_527_ - f_539_ * f_524_ + f_539_;
						if (f_522_ < f) {
							f_523_ = f_524_;
							if (f_521_ < 0.0F) {
								f_523_ -= f_535_ * f_521_;
								f_524_ -= f_536_ * f_521_;
								f_527_ -= f_540_ * f_521_;
								f_521_ = 0.0F;
							}
							if (f_522_ < 0.0F) {
								f_525_ -= f_537_ * f_522_;
								f_522_ = 0.0F;
							}
							if (f_521_ != f_522_ && f_535_ < f_536_ || f_521_ == f_522_ && f_535_ > f_537_) {
								f -= f_522_;
								f_522_ -= f_521_;
								f_521_ = (float) anIntArray97[(int) f_521_];
								while (--f_522_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f_521_, i, 0, (int) f_523_, (int) f_524_, f_527_, f_539_);
									f_523_ += f_535_;
									f_524_ += f_536_;
									f_527_ += f_540_;
									f_521_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f_521_, i, 0, (int) f_523_, (int) f_525_, f_527_, f_539_);
									f_523_ += f_535_;
									f_525_ += f_537_;
									f_527_ += f_540_;
									f_521_ += (float) anInt98;
								}
							} else {
								f -= f_522_;
								f_522_ -= f_521_;
								f_521_ = (float) anIntArray97[(int) f_521_];
								while (--f_522_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f_521_, i, 0, (int) f_524_, (int) f_523_, f_527_, f_539_);
									f_523_ += f_535_;
									f_524_ += f_536_;
									f_527_ += f_540_;
									f_521_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f_521_, i, 0, (int) f_525_, (int) f_523_, f_527_, f_539_);
									f_523_ += f_535_;
									f_525_ += f_537_;
									f_527_ += f_540_;
									f_521_ += (float) anInt98;
								}
							}
						} else {
							f_525_ = f_524_;
							if (f_521_ < 0.0F) {
								f_525_ -= f_535_ * f_521_;
								f_524_ -= f_536_ * f_521_;
								f_527_ -= f_540_ * f_521_;
								f_521_ = 0.0F;
							}
							if (f < 0.0F) {
								f_523_ -= f_537_ * f;
								f = 0.0F;
							}
							if (f_535_ < f_536_) {
								f_522_ -= f;
								f -= f_521_;
								f_521_ = (float) anIntArray97[(int) f_521_];
								while (--f >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f_521_, i, 0, (int) f_525_, (int) f_524_, f_527_, f_539_);
									f_525_ += f_535_;
									f_524_ += f_536_;
									f_527_ += f_540_;
									f_521_ += (float) anInt98;
								}
								while (--f_522_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f_521_, i, 0, (int) f_523_, (int) f_524_, f_527_, f_539_);
									f_523_ += f_537_;
									f_524_ += f_536_;
									f_527_ += f_540_;
									f_521_ += (float) anInt98;
								}
							} else {
								f_522_ -= f;
								f -= f_521_;
								f_521_ = (float) anIntArray97[(int) f_521_];
								while (--f >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f_521_, i, 0, (int) f_524_, (int) f_525_, f_527_, f_539_);
									f_525_ += f_535_;
									f_524_ += f_536_;
									f_527_ += f_540_;
									f_521_ += (float) anInt98;
								}
								while (--f_522_ >= 0.0F) {
									method202(anIntArray99, aFloatArray105, (int) f_521_, i, 0, (int) f_524_, (int) f_523_, f_527_, f_539_);
									f_523_ += f_537_;
									f_524_ += f_536_;
									f_527_ += f_540_;
									f_521_ += (float) anInt98;
								}
							}
						}
					}
				} else if (!(f_522_ >= (float) anInt101)) {
					if (f > (float) anInt101)
						f = (float) anInt101;
					if (f_521_ > (float) anInt101)
						f_521_ = (float) anInt101;
					f_528_ = f_528_ - f_539_ * f_525_ + f_539_;
					if (f < f_521_) {
						f_524_ = f_525_;
						if (f_522_ < 0.0F) {
							f_524_ -= f_536_ * f_522_;
							f_525_ -= f_537_ * f_522_;
							f_528_ -= f_540_ * f_522_;
							f_522_ = 0.0F;
						}
						if (f < 0.0F) {
							f_523_ -= f_535_ * f;
							f = 0.0F;
						}
						if (f_536_ < f_537_) {
							f_521_ -= f;
							f -= f_522_;
							f_522_ = (float) anIntArray97[(int) f_522_];
							while (--f >= 0.0F) {
								method202(anIntArray99, aFloatArray105, (int) f_522_, i, 0, (int) f_524_, (int) f_525_, f_528_, f_539_);
								f_524_ += f_536_;
								f_525_ += f_537_;
								f_528_ += f_540_;
								f_522_ += (float) anInt98;
							}
							while (--f_521_ >= 0.0F) {
								method202(anIntArray99, aFloatArray105, (int) f_522_, i, 0, (int) f_524_, (int) f_523_, f_528_, f_539_);
								f_524_ += f_536_;
								f_523_ += f_535_;
								f_528_ += f_540_;
								f_522_ += (float) anInt98;
							}
						} else {
							f_521_ -= f;
							f -= f_522_;
							f_522_ = (float) anIntArray97[(int) f_522_];
							while (--f >= 0.0F) {
								method202(anIntArray99, aFloatArray105, (int) f_522_, i, 0, (int) f_525_, (int) f_524_, f_528_, f_539_);
								f_524_ += f_536_;
								f_525_ += f_537_;
								f_528_ += f_540_;
								f_522_ += (float) anInt98;
							}
							while (--f_521_ >= 0.0F) {
								method202(anIntArray99, aFloatArray105, (int) f_522_, i, 0, (int) f_523_, (int) f_524_, f_528_, f_539_);
								f_524_ += f_536_;
								f_523_ += f_535_;
								f_528_ += f_540_;
								f_522_ += (float) anInt98;
							}
						}
					} else {
						f_523_ = f_525_;
						if (f_522_ < 0.0F) {
							f_523_ -= f_536_ * f_522_;
							f_525_ -= f_537_ * f_522_;
							f_528_ -= f_540_ * f_522_;
							f_522_ = 0.0F;
						}
						if (f_521_ < 0.0F) {
							f_524_ -= f_535_ * f_521_;
							f_521_ = 0.0F;
						}
						if (f_536_ < f_537_) {
							f -= f_521_;
							f_521_ -= f_522_;
							f_522_ = (float) anIntArray97[(int) f_522_];
							while (--f_521_ >= 0.0F) {
								method202(anIntArray99, aFloatArray105, (int) f_522_, i, 0, (int) f_523_, (int) f_525_, f_528_, f_539_);
								f_523_ += f_536_;
								f_525_ += f_537_;
								f_528_ += f_540_;
								f_522_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method202(anIntArray99, aFloatArray105, (int) f_522_, i, 0, (int) f_524_, (int) f_525_, f_528_, f_539_);
								f_524_ += f_535_;
								f_525_ += f_537_;
								f_528_ += f_540_;
								f_522_ += (float) anInt98;
							}
						} else {
							f -= f_521_;
							f_521_ -= f_522_;
							f_522_ = (float) anIntArray97[(int) f_522_];
							while (--f_521_ >= 0.0F) {
								method202(anIntArray99, aFloatArray105, (int) f_522_, i, 0, (int) f_525_, (int) f_523_, f_528_, f_539_);
								f_523_ += f_536_;
								f_525_ += f_537_;
								f_528_ += f_540_;
								f_522_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method202(anIntArray99, aFloatArray105, (int) f_522_, i, 0, (int) f_525_, (int) f_524_, f_528_, f_539_);
								f_524_ += f_535_;
								f_525_ += f_537_;
								f_528_ += f_540_;
								f_522_ += (float) anInt98;
							}
						}
					}
				}
			}
		}
	}

	final int method213() {
		return anIntArray97[0] / anInt98;
	}

	final void method214(float f, float f_541_, float f_542_, float f_543_, float f_544_, float f_545_, float f_546_, float f_547_, float f_548_, float f_549_, float f_550_, float f_551_, float f_552_, float f_553_, float f_554_, int i, int i_555_, int i_556_, int i_557_, int i_558_, int i_559_, int i_560_, int i_561_) {
		if (i_561_ != anInt124) {
			anIntArray123 = aHa_Sub2_96.method1260(i_561_);
			if (anIntArray123 == null) {
				method199((float) (int) f, (float) (int) f_541_, (float) (int) f_542_, (float) (int) f_543_, (float) (int) f_544_, (float) (int) f_545_, (float) (int) f_546_, (float) (int) f_547_, (float) (int) f_548_, Class69_Sub4.method745(i, 16711680, i_557_ | i_558_ << 24), Class69_Sub4.method745(i_555_, 16711680, i_557_ | i_559_ << 24), Class69_Sub4.method745(i_556_, 16711680, i_557_ | i_560_ << 24));
				return;
			}
			anInt113 = aHa_Sub2_96.method1262(i_561_) ? 64 : aHa_Sub2_96.anInt4081;
			anInt112 = anInt113 - 1;
			anInt115 = aHa_Sub2_96.method1257(i_561_);
			aBoolean119 = aHa_Sub2_96.method1268(i_561_);
		}
		anInt125 = i_557_;
		float f_562_ = (float) (i >> 24 & 0xff);
		float f_563_ = (float) (i_555_ >> 24 & 0xff);
		float f_564_ = (float) (i_556_ >> 24 & 0xff);
		float f_565_ = (float) (i >> 16 & 0xff);
		float f_566_ = (float) (i_555_ >> 16 & 0xff);
		float f_567_ = (float) (i_556_ >> 16 & 0xff);
		float f_568_ = (float) (i >> 8 & 0xff);
		float f_569_ = (float) (i_555_ >> 8 & 0xff);
		float f_570_ = (float) (i_556_ >> 8 & 0xff);
		float f_571_ = (float) (i & 0xff);
		float f_572_ = (float) (i_555_ & 0xff);
		float f_573_ = (float) (i_556_ & 0xff);
		f_549_ /= f_546_;
		f_550_ /= f_547_;
		f_551_ /= f_548_;
		f_552_ /= f_546_;
		f_553_ /= f_547_;
		f_554_ /= f_548_;
		f_546_ = 1.0F / f_546_;
		f_547_ = 1.0F / f_547_;
		f_548_ = 1.0F / f_548_;
		float f_574_ = 0.0F;
		float f_575_ = 0.0F;
		float f_576_ = 0.0F;
		float f_577_ = 0.0F;
		float f_578_ = 0.0F;
		float f_579_ = 0.0F;
		float f_580_ = 0.0F;
		float f_581_ = 0.0F;
		float f_582_ = 0.0F;
		if (f_541_ != f) {
			float f_583_ = f_541_ - f;
			f_574_ = (f_544_ - f_543_) / f_583_;
			f_575_ = (f_547_ - f_546_) / f_583_;
			f_576_ = (f_550_ - f_549_) / f_583_;
			f_577_ = (f_553_ - f_552_) / f_583_;
			f_578_ = (float) (i_559_ - i_558_) / f_583_;
			f_579_ = (f_563_ - f_562_) / f_583_;
			f_580_ = (f_566_ - f_565_) / f_583_;
			f_581_ = (f_569_ - f_568_) / f_583_;
			f_582_ = (f_572_ - f_571_) / f_583_;
		}
		float f_584_ = 0.0F;
		float f_585_ = 0.0F;
		float f_586_ = 0.0F;
		float f_587_ = 0.0F;
		float f_588_ = 0.0F;
		float f_589_ = 0.0F;
		float f_590_ = 0.0F;
		float f_591_ = 0.0F;
		float f_592_ = 0.0F;
		if (f_542_ != f_541_) {
			float f_593_ = f_542_ - f_541_;
			f_584_ = (f_545_ - f_544_) / f_593_;
			f_585_ = (f_548_ - f_547_) / f_593_;
			f_586_ = (f_551_ - f_550_) / f_593_;
			f_587_ = (f_554_ - f_553_) / f_593_;
			f_588_ = (float) (i_560_ - i_559_) / f_593_;
			f_589_ = (f_564_ - f_563_) / f_593_;
			f_590_ = (f_567_ - f_566_) / f_593_;
			f_591_ = (f_570_ - f_569_) / f_593_;
			f_592_ = (f_573_ - f_572_) / f_593_;
		}
		float f_594_ = 0.0F;
		float f_595_ = 0.0F;
		float f_596_ = 0.0F;
		float f_597_ = 0.0F;
		float f_598_ = 0.0F;
		float f_599_ = 0.0F;
		float f_600_ = 0.0F;
		float f_601_ = 0.0F;
		float f_602_ = 0.0F;
		if (f != f_542_) {
			float f_603_ = f - f_542_;
			f_594_ = (f_543_ - f_545_) / f_603_;
			f_595_ = (f_546_ - f_548_) / f_603_;
			f_596_ = (f_549_ - f_551_) / f_603_;
			f_597_ = (f_552_ - f_554_) / f_603_;
			f_598_ = (float) (i_558_ - i_560_) / f_603_;
			f_599_ = (f_562_ - f_564_) / f_603_;
			f_600_ = (f_565_ - f_567_) / f_603_;
			f_601_ = (f_568_ - f_570_) / f_603_;
			f_602_ = (f_571_ - f_573_) / f_603_;
		}
		if (f <= f_541_ && f <= f_542_) {
			if (!(f >= (float) anInt101)) {
				if (f_541_ > (float) anInt101)
					f_541_ = (float) anInt101;
				if (f_542_ > (float) anInt101)
					f_542_ = (float) anInt101;
				if (f_541_ < f_542_) {
					f_545_ = f_543_;
					f_548_ = f_546_;
					f_551_ = f_549_;
					f_554_ = f_552_;
					i_560_ = i_558_;
					f_564_ = f_562_;
					f_567_ = f_565_;
					f_570_ = f_568_;
					f_573_ = f_571_;
					if (f < 0.0F) {
						f_543_ -= f_574_ * f;
						f_545_ -= f_594_ * f;
						f_546_ -= f_575_ * f;
						f_548_ -= f_595_ * f;
						f_549_ -= f_576_ * f;
						f_551_ -= f_596_ * f;
						f_552_ -= f_577_ * f;
						f_554_ -= f_597_ * f;
						i_558_ -= f_578_ * f;
						i_560_ -= f_598_ * f;
						f_562_ -= f_579_ * f;
						f_564_ -= f_599_ * f;
						f_565_ -= f_579_ * f;
						f_567_ -= f_599_ * f;
						f_568_ -= f_579_ * f;
						f_570_ -= f_599_ * f;
						f_571_ -= f_579_ * f;
						f_573_ -= f_599_ * f;
						f = 0.0F;
					}
					if (f_541_ < 0.0F) {
						f_544_ -= f_584_ * f_541_;
						f_547_ -= f_585_ * f_541_;
						f_550_ -= f_586_ * f_541_;
						f_553_ -= f_587_ * f_541_;
						i_559_ -= f_588_ * f_541_;
						f_563_ -= f_589_ * f_541_;
						f_566_ -= f_590_ * f_541_;
						f_569_ -= f_591_ * f_541_;
						f_572_ -= f_592_ * f_541_;
						f_541_ = 0.0F;
					}
					if (f != f_541_ && f_594_ < f_574_ || f == f_541_ && f_594_ > f_584_) {
						f_542_ -= f_541_;
						f_541_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_541_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f, (int) f_545_, (int) f_543_, f_548_, f_546_, f_551_, f_549_, f_554_, f_552_, (float) i_560_, (float) i_558_, f_564_, f_562_, f_567_, f_565_, f_570_, f_568_, f_573_, f_571_);
							f_543_ += f_574_;
							f_545_ += f_594_;
							f_546_ += f_575_;
							f_548_ += f_595_;
							f_549_ += f_576_;
							f_551_ += f_596_;
							f_552_ += f_577_;
							f_554_ += f_597_;
							i_558_ += f_578_;
							i_560_ += f_598_;
							f_562_ += f_579_;
							f_564_ += f_599_;
							f_565_ += f_580_;
							f_567_ += f_600_;
							f_568_ += f_581_;
							f_570_ += f_601_;
							f_571_ += f_582_;
							f_573_ += f_602_;
							f += (float) anInt98;
						}
						while (--f_542_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f, (int) f_545_, (int) f_544_, f_548_, f_547_, f_551_, f_550_, f_554_, f_553_, (float) i_560_, (float) i_559_, f_564_, f_563_, f_567_, f_566_, f_570_, f_569_, f_573_, f_572_);
							f_544_ += f_584_;
							f_545_ += f_594_;
							f_547_ += f_585_;
							f_548_ += f_595_;
							f_550_ += f_586_;
							f_551_ += f_596_;
							f_553_ += f_587_;
							f_554_ += f_597_;
							i_559_ += f_588_;
							i_560_ += f_598_;
							f_563_ += f_589_;
							f_564_ += f_599_;
							f_566_ += f_590_;
							f_567_ += f_600_;
							f_569_ += f_591_;
							f_570_ += f_601_;
							f_572_ += f_592_;
							f_573_ += f_602_;
							f += (float) anInt98;
						}
					} else {
						f_542_ -= f_541_;
						f_541_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_541_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f, (int) f_543_, (int) f_545_, f_546_, f_548_, f_549_, f_551_, f_552_, f_554_, (float) i_558_, (float) i_560_, f_562_, f_564_, f_565_, f_567_, f_568_, f_570_, f_571_, f_573_);
							f_543_ += f_574_;
							f_545_ += f_594_;
							f_546_ += f_575_;
							f_548_ += f_595_;
							f_549_ += f_576_;
							f_551_ += f_596_;
							f_552_ += f_577_;
							f_554_ += f_597_;
							i_558_ += f_578_;
							i_560_ += f_598_;
							f_562_ += f_579_;
							f_564_ += f_599_;
							f_565_ += f_580_;
							f_567_ += f_600_;
							f_568_ += f_581_;
							f_570_ += f_601_;
							f_571_ += f_582_;
							f_573_ += f_602_;
							f += (float) anInt98;
						}
						while (--f_542_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f, (int) f_544_, (int) f_545_, f_547_, f_548_, f_550_, f_551_, f_553_, f_554_, (float) i_559_, (float) i_560_, f_563_, f_564_, f_566_, f_567_, f_569_, f_570_, f_572_, f_573_);
							f_544_ += f_584_;
							f_545_ += f_594_;
							f_547_ += f_585_;
							f_548_ += f_595_;
							f_550_ += f_586_;
							f_551_ += f_596_;
							f_553_ += f_587_;
							f_554_ += f_597_;
							i_559_ += f_588_;
							i_560_ += f_598_;
							f_563_ += f_589_;
							f_564_ += f_599_;
							f_566_ += f_590_;
							f_567_ += f_600_;
							f_569_ += f_591_;
							f_570_ += f_601_;
							f_572_ += f_592_;
							f_573_ += f_602_;
							f += (float) anInt98;
						}
					}
				} else {
					f_544_ = f_543_;
					f_547_ = f_546_;
					f_550_ = f_549_;
					f_553_ = f_552_;
					i_559_ = i_558_;
					f_563_ = f_562_;
					f_566_ = f_565_;
					f_569_ = f_568_;
					f_572_ = f_571_;
					if (f < 0.0F) {
						f_543_ -= f_574_ * f;
						f_544_ -= f_594_ * f;
						f_546_ -= f_575_ * f;
						f_547_ -= f_595_ * f;
						f_549_ -= f_576_ * f;
						f_550_ -= f_596_ * f;
						f_552_ -= f_577_ * f;
						f_553_ -= f_597_ * f;
						i_558_ -= f_578_ * f;
						i_559_ -= f_598_ * f;
						f_562_ -= f_579_ * f;
						f_563_ -= f_599_ * f;
						f_565_ -= f_579_ * f;
						f_566_ -= f_599_ * f;
						f_568_ -= f_579_ * f;
						f_569_ -= f_599_ * f;
						f_571_ -= f_579_ * f;
						f_572_ -= f_599_ * f;
						f = 0.0F;
					}
					if (f_542_ < 0.0F) {
						f_545_ -= f_584_ * f_542_;
						f_548_ -= f_585_ * f_542_;
						f_551_ -= f_586_ * f_542_;
						f_554_ -= f_587_ * f_542_;
						i_560_ -= f_588_ * f_542_;
						f_564_ -= f_589_ * f_542_;
						f_567_ -= f_590_ * f_542_;
						f_570_ -= f_591_ * f_542_;
						f_573_ -= f_592_ * f_542_;
						f_542_ = 0.0F;
					}
					if (f != f_542_ && f_594_ < f_574_ || f == f_542_ && f_584_ > f_574_) {
						f_541_ -= f_542_;
						f_542_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_542_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f, (int) f_544_, (int) f_543_, f_547_, f_546_, f_550_, f_549_, f_553_, f_552_, (float) i_559_, (float) i_558_, f_563_, f_562_, f_566_, f_565_, f_569_, f_568_, f_572_, f_571_);
							f_543_ += f_574_;
							f_544_ += f_594_;
							f_546_ += f_575_;
							f_547_ += f_595_;
							f_549_ += f_576_;
							f_550_ += f_596_;
							f_552_ += f_577_;
							f_553_ += f_597_;
							i_558_ += f_578_;
							i_559_ += f_598_;
							f_562_ += f_579_;
							f_563_ += f_599_;
							f_565_ += f_580_;
							f_566_ += f_600_;
							f_568_ += f_581_;
							f_569_ += f_601_;
							f_571_ += f_582_;
							f_572_ += f_602_;
							f += (float) anInt98;
						}
						while (--f_541_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f, (int) f_545_, (int) f_543_, f_548_, f_546_, f_551_, f_549_, f_554_, f_552_, (float) i_560_, (float) i_558_, f_564_, f_562_, f_567_, f_565_, f_570_, f_568_, f_573_, f_571_);
							f_545_ += f_584_;
							f_543_ += f_574_;
							f_548_ += f_585_;
							f_546_ += f_575_;
							f_551_ += f_586_;
							f_549_ += f_576_;
							f_554_ += f_587_;
							f_552_ += f_577_;
							i_560_ += f_588_;
							i_558_ += f_578_;
							f_564_ += f_589_;
							f_562_ += f_579_;
							f_567_ += f_590_;
							f_565_ += f_580_;
							f_570_ += f_591_;
							f_568_ += f_581_;
							f_573_ += f_592_;
							f_571_ += f_582_;
							f += (float) anInt98;
						}
					} else {
						f_541_ -= f_542_;
						f_542_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_542_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f, (int) f_543_, (int) f_544_, f_546_, f_547_, f_549_, f_550_, f_552_, f_553_, (float) i_558_, (float) i_559_, f_562_, f_563_, f_565_, f_566_, f_568_, f_569_, f_571_, f_572_);
							f_544_ += f_594_;
							f_543_ += f_574_;
							f_547_ += f_595_;
							f_546_ += f_575_;
							f_550_ += f_596_;
							f_549_ += f_576_;
							f_553_ += f_597_;
							f_552_ += f_577_;
							i_559_ += f_598_;
							i_558_ += f_578_;
							f_563_ += f_599_;
							f_562_ += f_579_;
							f_566_ += f_600_;
							f_565_ += f_580_;
							f_569_ += f_601_;
							f_568_ += f_581_;
							f_572_ += f_602_;
							f_571_ += f_582_;
							f += (float) anInt98;
						}
						while (--f_541_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f, (int) f_543_, (int) f_545_, f_546_, f_548_, f_549_, f_551_, f_552_, f_554_, (float) i_558_, (float) i_560_, f_562_, f_564_, f_565_, f_567_, f_568_, f_570_, f_571_, f_573_);
							f_543_ += f_574_;
							f_545_ += f_584_;
							f_546_ += f_575_;
							f_548_ += f_585_;
							f_549_ += f_576_;
							f_551_ += f_586_;
							f_552_ += f_577_;
							f_554_ += f_587_;
							i_558_ += f_578_;
							i_560_ += f_588_;
							f_562_ += f_579_;
							f_564_ += f_589_;
							f_565_ += f_580_;
							f_567_ += f_590_;
							f_568_ += f_581_;
							f_570_ += f_591_;
							f_571_ += f_582_;
							f_573_ += f_592_;
							f += (float) anInt98;
						}
					}
				}
			}
		} else if (f_541_ <= f_542_) {
			if (!(f_541_ >= (float) anInt101)) {
				if (f_542_ > (float) anInt101)
					f_542_ = (float) anInt101;
				if (f > (float) anInt101)
					f = (float) anInt101;
				if (f_542_ < f) {
					f_543_ = f_544_;
					f_546_ = f_547_;
					f_549_ = f_550_;
					f_552_ = f_553_;
					i_558_ = i_559_;
					f_562_ = f_563_;
					f_565_ = f_566_;
					f_568_ = f_569_;
					f_571_ = f_572_;
					if (f_541_ < 0.0F) {
						f_543_ -= f_574_ * f_541_;
						f_544_ -= f_584_ * f_541_;
						f_546_ -= f_575_ * f_541_;
						f_547_ -= f_585_ * f_541_;
						f_549_ -= f_576_ * f_541_;
						f_550_ -= f_586_ * f_541_;
						f_552_ -= f_577_ * f_541_;
						f_553_ -= f_587_ * f_541_;
						i_558_ -= f_578_ * f_541_;
						i_559_ -= f_588_ * f_541_;
						f_562_ -= f_579_ * f_541_;
						f_563_ -= f_589_ * f_541_;
						f_565_ -= f_580_ * f_541_;
						f_566_ -= f_590_ * f_541_;
						f_568_ -= f_581_ * f_541_;
						f_569_ -= f_591_ * f_541_;
						f_571_ -= f_582_ * f_541_;
						f_572_ -= f_592_ * f_541_;
						f_541_ = 0.0F;
					}
					if (f_542_ < 0.0F) {
						f_545_ -= f_594_ * f_542_;
						f_548_ -= f_595_ * f_542_;
						f_551_ -= f_596_ * f_542_;
						f_554_ -= f_597_ * f_542_;
						i_560_ -= f_598_ * f_542_;
						f_564_ -= f_599_ * f_542_;
						f_567_ -= f_600_ * f_542_;
						f_570_ -= f_601_ * f_542_;
						f_573_ -= f_602_ * f_542_;
						f_542_ = 0.0F;
					}
					if (f_541_ != f_542_ && f_574_ < f_584_ || f_541_ == f_542_ && f_574_ > f_594_) {
						f -= f_542_;
						f_542_ -= f_541_;
						f_541_ = (float) anIntArray97[(int) f_541_];
						while (--f_542_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f_541_, (int) f_543_, (int) f_544_, f_546_, f_547_, f_549_, f_550_, f_552_, f_553_, (float) i_558_, (float) i_559_, f_562_, f_563_, f_565_, f_566_, f_568_, f_569_, f_571_, f_572_);
							f_543_ += f_574_;
							f_544_ += f_584_;
							f_546_ += f_575_;
							f_547_ += f_585_;
							f_549_ += f_576_;
							f_550_ += f_586_;
							f_552_ += f_577_;
							f_553_ += f_587_;
							i_558_ += f_578_;
							i_559_ += f_588_;
							f_562_ += f_579_;
							f_563_ += f_589_;
							f_565_ += f_580_;
							f_566_ += f_590_;
							f_568_ += f_581_;
							f_569_ += f_591_;
							f_571_ += f_582_;
							f_572_ += f_592_;
							f_541_ += (float) anInt98;
						}
						while (--f >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f_541_, (int) f_543_, (int) f_545_, f_546_, f_548_, f_549_, f_551_, f_552_, f_554_, (float) i_558_, (float) i_560_, f_562_, f_564_, f_565_, f_567_, f_568_, f_570_, f_571_, f_573_);
							f_543_ += f_574_;
							f_545_ += f_594_;
							f_546_ += f_575_;
							f_548_ += f_595_;
							f_549_ += f_576_;
							f_551_ += f_596_;
							f_552_ += f_577_;
							f_554_ += f_597_;
							i_558_ += f_578_;
							i_560_ += f_598_;
							f_562_ += f_579_;
							f_564_ += f_599_;
							f_565_ += f_580_;
							f_567_ += f_600_;
							f_568_ += f_581_;
							f_570_ += f_601_;
							f_571_ += f_582_;
							f_573_ += f_602_;
							f_541_ += (float) anInt98;
						}
					} else {
						f -= f_542_;
						f_542_ -= f_541_;
						f_541_ = (float) anIntArray97[(int) f_541_];
						while (--f_542_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f_541_, (int) f_544_, (int) f_543_, f_547_, f_546_, f_550_, f_549_, f_553_, f_552_, (float) i_559_, (float) i_558_, f_563_, f_562_, f_566_, f_565_, f_569_, f_568_, f_572_, f_571_);
							f_544_ += f_584_;
							f_543_ += f_574_;
							f_547_ += f_585_;
							f_546_ += f_575_;
							f_550_ += f_586_;
							f_549_ += f_576_;
							f_553_ += f_587_;
							f_552_ += f_577_;
							i_559_ += f_588_;
							i_558_ += f_578_;
							f_563_ += f_589_;
							f_562_ += f_579_;
							f_566_ += f_590_;
							f_565_ += f_580_;
							f_569_ += f_591_;
							f_568_ += f_581_;
							f_572_ += f_592_;
							f_571_ += f_582_;
							f_541_ += (float) anInt98;
						}
						while (--f >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f_541_, (int) f_545_, (int) f_543_, f_548_, f_546_, f_551_, f_549_, f_554_, f_552_, (float) i_560_, (float) i_558_, f_564_, f_562_, f_567_, f_565_, f_570_, f_568_, f_573_, f_571_);
							f_545_ += f_594_;
							f_543_ += f_574_;
							f_548_ += f_595_;
							f_546_ += f_575_;
							f_551_ += f_596_;
							f_549_ += f_576_;
							f_554_ += f_597_;
							f_552_ += f_577_;
							i_560_ += f_598_;
							i_558_ += f_578_;
							f_564_ += f_599_;
							f_562_ += f_579_;
							f_567_ += f_600_;
							f_565_ += f_580_;
							f_570_ += f_601_;
							f_568_ += f_581_;
							f_573_ += f_602_;
							f_571_ += f_582_;
							f_541_ += (float) anInt98;
						}
					}
				} else {
					f_545_ = f_544_;
					f_548_ = f_547_;
					f_551_ = f_550_;
					f_554_ = f_553_;
					i_560_ = i_559_;
					f_564_ = f_563_;
					f_567_ = f_566_;
					f_570_ = f_569_;
					f_573_ = f_572_;
					if (f_541_ < 0.0F) {
						f_545_ -= f_574_ * f_541_;
						f_544_ -= f_584_ * f_541_;
						f_548_ -= f_575_ * f_541_;
						f_547_ -= f_585_ * f_541_;
						f_551_ -= f_576_ * f_541_;
						f_550_ -= f_586_ * f_541_;
						f_554_ -= f_577_ * f_541_;
						f_553_ -= f_587_ * f_541_;
						i_560_ -= f_578_ * f_541_;
						i_559_ -= f_588_ * f_541_;
						f_564_ -= f_579_ * f_541_;
						f_563_ -= f_589_ * f_541_;
						f_567_ -= f_580_ * f_541_;
						f_566_ -= f_590_ * f_541_;
						f_570_ -= f_581_ * f_541_;
						f_569_ -= f_591_ * f_541_;
						f_573_ -= f_582_ * f_541_;
						f_572_ -= f_592_ * f_541_;
						f_541_ = 0.0F;
					}
					if (f < 0.0F) {
						f_543_ -= f_594_ * f;
						f_546_ -= f_595_ * f;
						f_549_ -= f_596_ * f;
						f_552_ -= f_597_ * f;
						i_558_ -= f_598_ * f;
						f_562_ -= f_599_ * f;
						f_565_ -= f_600_ * f;
						f_568_ -= f_601_ * f;
						f_571_ -= f_602_ * f;
						f = 0.0F;
					}
					f_542_ -= f;
					f -= f_541_;
					f_541_ = (float) anIntArray97[(int) f_541_];
					if (f_574_ < f_584_) {
						while (--f >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f_541_, (int) f_545_, (int) f_544_, f_548_, f_547_, f_551_, f_550_, f_554_, f_553_, (float) i_560_, (float) i_559_, f_564_, f_563_, f_567_, f_566_, f_570_, f_569_, f_573_, f_572_);
							f_545_ += f_574_;
							f_544_ += f_584_;
							f_548_ += f_575_;
							f_547_ += f_585_;
							f_551_ += f_576_;
							f_550_ += f_586_;
							f_554_ += f_577_;
							f_553_ += f_587_;
							i_560_ += f_578_;
							i_559_ += f_588_;
							f_564_ += f_579_;
							f_563_ += f_589_;
							f_567_ += f_580_;
							f_566_ += f_590_;
							f_570_ += f_581_;
							f_569_ += f_591_;
							f_573_ += f_582_;
							f_572_ += f_592_;
							f_541_ += (float) anInt98;
						}
						while (--f_542_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f_541_, (int) f_543_, (int) f_544_, f_546_, f_547_, f_549_, f_550_, f_552_, f_553_, (float) i_558_, (float) i_559_, f_562_, f_563_, f_565_, f_566_, f_568_, f_569_, f_571_, f_572_);
							f_543_ += f_594_;
							f_544_ += f_584_;
							f_546_ += f_595_;
							f_547_ += f_585_;
							f_549_ += f_596_;
							f_550_ += f_586_;
							f_552_ += f_597_;
							f_553_ += f_587_;
							i_558_ += f_598_;
							i_559_ += f_588_;
							f_562_ += f_599_;
							f_563_ += f_589_;
							f_565_ += f_600_;
							f_566_ += f_590_;
							f_568_ += f_601_;
							f_569_ += f_591_;
							f_571_ += f_602_;
							f_572_ += f_592_;
							f_541_ += (float) anInt98;
						}
					} else {
						while (--f >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f_541_, (int) f_544_, (int) f_545_, f_547_, f_548_, f_550_, f_551_, f_553_, f_554_, (float) i_559_, (float) i_560_, f_563_, f_564_, f_566_, f_567_, f_569_, f_570_, f_572_, f_573_);
							f_544_ += f_584_;
							f_545_ += f_574_;
							f_547_ += f_585_;
							f_548_ += f_575_;
							f_550_ += f_586_;
							f_551_ += f_576_;
							f_553_ += f_587_;
							f_554_ += f_577_;
							i_559_ += f_588_;
							i_560_ += f_578_;
							f_563_ += f_589_;
							f_564_ += f_579_;
							f_566_ += f_590_;
							f_567_ += f_580_;
							f_569_ += f_591_;
							f_570_ += f_581_;
							f_572_ += f_592_;
							f_573_ += f_582_;
							f_541_ += (float) anInt98;
						}
						while (--f_542_ >= 0.0F) {
							method215(anIntArray99, anIntArray123, (int) f_541_, (int) f_544_, (int) f_543_, f_547_, f_546_, f_550_, f_549_, f_553_, f_552_, (float) i_559_, (float) i_558_, f_563_, f_562_, f_566_, f_565_, f_569_, f_568_, f_572_, f_571_);
							f_544_ += f_584_;
							f_543_ += f_594_;
							f_547_ += f_585_;
							f_546_ += f_595_;
							f_550_ += f_586_;
							f_549_ += f_596_;
							f_553_ += f_587_;
							f_552_ += f_597_;
							i_559_ += f_588_;
							i_558_ += f_598_;
							f_563_ += f_589_;
							f_562_ += f_599_;
							f_566_ += f_590_;
							f_565_ += f_600_;
							f_569_ += f_591_;
							f_568_ += f_601_;
							f_572_ += f_592_;
							f_571_ += f_602_;
							f_541_ += (float) anInt98;
						}
					}
				}
			}
		} else if (!(f_542_ >= (float) anInt101)) {
			if (f > (float) anInt101)
				f = (float) anInt101;
			if (f_541_ > (float) anInt101)
				f_541_ = (float) anInt101;
			if (f < f_541_) {
				f_544_ = f_545_;
				f_547_ = f_548_;
				f_550_ = f_551_;
				f_553_ = f_554_;
				i_559_ = i_560_;
				f_563_ = f_564_;
				f_566_ = f_567_;
				f_569_ = f_570_;
				f_572_ = f_573_;
				if (f_542_ < 0.0F) {
					f_545_ -= f_594_ * f_542_;
					f_544_ -= f_584_ * f_542_;
					f_548_ -= f_595_ * f_542_;
					f_547_ -= f_585_ * f_542_;
					f_551_ -= f_596_ * f_542_;
					f_550_ -= f_586_ * f_542_;
					f_554_ -= f_597_ * f_542_;
					f_553_ -= f_587_ * f_542_;
					i_560_ -= f_598_ * 3.0F;
					i_559_ -= f_588_ * f_542_;
					f_564_ -= f_599_ * f_542_;
					f_563_ -= f_589_ * f_542_;
					f_567_ -= f_600_ * f_542_;
					f_566_ -= f_590_ * f_542_;
					f_570_ -= f_601_ * f_542_;
					f_569_ -= f_591_ * f_542_;
					f_573_ -= f_602_ * f_542_;
					f_572_ -= f_592_ * f_542_;
					f_542_ = 0.0F;
				}
				if (f < 0.0F) {
					f_543_ -= f_574_ * f;
					f_546_ -= f_575_ * f;
					f_549_ -= f_576_ * f;
					f_552_ -= f_577_ * f;
					i_558_ -= f_578_ * f;
					f_562_ -= f_579_ * f;
					f_565_ -= f_580_ * f;
					f_568_ -= f_581_ * f;
					f_571_ -= f_582_ * f;
					f = 0.0F;
				}
				if (f_584_ < f_594_) {
					f_541_ -= f;
					f -= f_542_;
					f_542_ = (float) anIntArray97[(int) f_542_];
					while (--f >= 0.0F) {
						method215(anIntArray99, anIntArray123, (int) f_542_, (int) f_544_, (int) f_545_, f_547_, f_548_, f_550_, f_551_, f_553_, f_554_, (float) i_559_, (float) i_560_, f_563_, f_564_, f_566_, f_567_, f_569_, f_570_, f_572_, f_573_);
						f_544_ += f_584_;
						f_545_ += f_594_;
						f_547_ += f_585_;
						f_548_ += f_595_;
						f_550_ += f_586_;
						f_551_ += f_596_;
						f_553_ += f_587_;
						f_554_ += f_597_;
						i_559_ += f_588_;
						i_560_ += f_598_;
						f_563_ += f_589_;
						f_564_ += f_599_;
						f_566_ += f_590_;
						f_567_ += f_600_;
						f_569_ += f_591_;
						f_570_ += f_601_;
						f_572_ += f_592_;
						f_573_ += f_602_;
						f_542_ += (float) anInt98;
					}
					while (--f_541_ >= 0.0F) {
						method215(anIntArray99, anIntArray123, (int) f_542_, (int) f_544_, (int) f_543_, f_547_, f_546_, f_550_, f_549_, f_553_, f_552_, (float) i_559_, (float) i_558_, f_563_, f_562_, f_566_, f_565_, f_569_, f_568_, f_572_, f_571_);
						f_544_ += f_584_;
						f_543_ += f_574_;
						f_547_ += f_585_;
						f_546_ += f_575_;
						f_550_ += f_586_;
						f_549_ += f_576_;
						f_553_ += f_587_;
						f_552_ += f_577_;
						i_559_ += f_588_;
						i_558_ += f_578_;
						f_563_ += f_589_;
						f_562_ += f_579_;
						f_566_ += f_590_;
						f_565_ += f_580_;
						f_569_ += f_591_;
						f_568_ += f_581_;
						f_572_ += f_592_;
						f_571_ += f_582_;
						f_542_ += (float) anInt98;
					}
				} else {
					f_541_ -= f;
					f -= f_542_;
					f_542_ = (float) anIntArray97[(int) f_542_];
					while (--f >= 0.0F) {
						method215(anIntArray99, anIntArray123, (int) f_542_, (int) f_545_, (int) f_544_, f_548_, f_547_, f_551_, f_550_, f_554_, f_553_, (float) i_560_, (float) i_559_, f_564_, f_563_, f_567_, f_566_, f_570_, f_569_, f_573_, f_572_);
						f_545_ += f_594_;
						f_544_ += f_584_;
						f_548_ += f_595_;
						f_547_ += f_585_;
						f_551_ += f_596_;
						f_550_ += f_586_;
						f_554_ += f_597_;
						f_553_ += f_587_;
						i_560_ += f_598_;
						i_559_ += f_588_;
						f_564_ += f_599_;
						f_563_ += f_589_;
						f_567_ += f_600_;
						f_566_ += f_590_;
						f_570_ += f_601_;
						f_569_ += f_591_;
						f_573_ += f_602_;
						f_572_ += f_592_;
						f_542_ += (float) anInt98;
					}
					while (--f_541_ >= 0.0F) {
						method215(anIntArray99, anIntArray123, (int) f_542_, (int) f_543_, (int) f_544_, f_546_, f_547_, f_549_, f_550_, f_552_, f_553_, (float) i_558_, (float) i_559_, f_562_, f_563_, f_565_, f_566_, f_568_, f_569_, f_571_, f_572_);
						f_543_ += f_574_;
						f_544_ += f_584_;
						f_546_ += f_575_;
						f_547_ += f_585_;
						f_549_ += f_576_;
						f_550_ += f_586_;
						f_552_ += f_577_;
						f_553_ += f_587_;
						i_558_ += f_578_;
						i_559_ += f_588_;
						f_562_ += f_579_;
						f_563_ += f_589_;
						f_565_ += f_580_;
						f_566_ += f_590_;
						f_568_ += f_581_;
						f_569_ += f_591_;
						f_571_ += f_582_;
						f_572_ += f_592_;
						f_542_ += (float) anInt98;
					}
				}
			} else {
				f_543_ = f_545_;
				f_546_ = f_548_;
				f_549_ = f_551_;
				f_552_ = f_554_;
				i_558_ = i_560_;
				f_562_ = f_564_;
				f_565_ = f_567_;
				f_568_ = f_570_;
				f_571_ = f_573_;
				if (f_542_ < 0.0F) {
					f_545_ -= f_594_ * f_542_;
					f_543_ -= f_584_ * f_542_;
					f_548_ -= f_595_ * f_542_;
					f_546_ -= f_585_ * f_542_;
					f_551_ -= f_596_ * f_542_;
					f_549_ -= f_586_ * f_542_;
					f_554_ -= f_597_ * f_542_;
					f_552_ -= f_587_ * f_542_;
					i_560_ -= f_598_ * 3.0F;
					i_558_ -= f_588_ * f_542_;
					f_564_ -= f_599_ * f_542_;
					f_562_ -= f_589_ * f_542_;
					f_567_ -= f_600_ * f_542_;
					f_565_ -= f_590_ * f_542_;
					f_570_ -= f_601_ * f_542_;
					f_568_ -= f_591_ * f_542_;
					f_573_ -= f_602_ * f_542_;
					f_571_ -= f_592_ * f_542_;
					f_542_ = 0.0F;
				}
				if (f_541_ < 0.0F) {
					f_544_ -= f_574_ * f_541_;
					f_547_ -= f_575_ * f_541_;
					f_550_ -= f_576_ * f_541_;
					f_553_ -= f_577_ * f_541_;
					i_559_ -= f_578_ * f_541_;
					f_563_ -= f_579_ * f_541_;
					f_566_ -= f_580_ * f_541_;
					f_569_ -= f_581_ * f_541_;
					f_572_ -= f_582_ * f_541_;
					f_541_ = 0.0F;
				}
				if (f_584_ < f_594_) {
					f -= f_541_;
					f_541_ -= f_542_;
					f_542_ = (float) anIntArray97[(int) f_542_];
					while (--f_541_ >= 0.0F) {
						method215(anIntArray99, anIntArray123, (int) f_542_, (int) f_543_, (int) f_545_, f_546_, f_548_, f_549_, f_551_, f_552_, f_554_, (float) i_558_, (float) i_560_, f_562_, f_564_, f_565_, f_567_, f_568_, f_570_, f_571_, f_573_);
						f_543_ += f_584_;
						f_545_ += f_594_;
						f_546_ += f_585_;
						f_548_ += f_595_;
						f_549_ += f_586_;
						f_551_ += f_596_;
						f_552_ += f_587_;
						f_554_ += f_597_;
						i_558_ += f_588_;
						i_560_ += f_598_;
						f_562_ += f_589_;
						f_564_ += f_599_;
						f_565_ += f_590_;
						f_567_ += f_600_;
						f_568_ += f_591_;
						f_570_ += f_601_;
						f_571_ += f_592_;
						f_573_ += f_602_;
						f_542_ += (float) anInt98;
					}
					while (--f >= 0.0F) {
						method215(anIntArray99, anIntArray123, (int) f_542_, (int) f_544_, (int) f_545_, f_547_, f_548_, f_550_, f_551_, f_553_, f_554_, (float) i_559_, (float) i_560_, f_563_, f_564_, f_566_, f_567_, f_569_, f_570_, f_572_, f_573_);
						f_544_ += f_574_;
						f_545_ += f_594_;
						f_547_ += f_575_;
						f_548_ += f_595_;
						f_550_ += f_576_;
						f_551_ += f_596_;
						f_553_ += f_577_;
						f_554_ += f_597_;
						i_559_ += f_578_;
						i_560_ += f_598_;
						f_563_ += f_579_;
						f_564_ += f_599_;
						f_566_ += f_580_;
						f_567_ += f_600_;
						f_569_ += f_581_;
						f_570_ += f_601_;
						f_572_ += f_582_;
						f_573_ += f_602_;
						f_542_ += (float) anInt98;
					}
				} else {
					f -= f_541_;
					f_541_ -= f_542_;
					f_542_ = (float) anIntArray97[(int) f_542_];
					while (--f_541_ >= 0.0F) {
						method215(anIntArray99, anIntArray123, (int) f_542_, (int) f_545_, (int) f_543_, f_548_, f_546_, f_551_, f_549_, f_554_, f_552_, (float) i_560_, (float) i_558_, f_564_, f_562_, f_567_, f_565_, f_570_, f_568_, f_573_, f_571_);
						f_545_ += f_594_;
						f_543_ += f_584_;
						f_548_ += f_595_;
						f_546_ += f_585_;
						f_551_ += f_596_;
						f_549_ += f_586_;
						f_554_ += f_597_;
						f_552_ += f_587_;
						i_560_ += f_598_;
						i_558_ += f_588_;
						f_564_ += f_599_;
						f_562_ += f_589_;
						f_567_ += f_600_;
						f_565_ += f_590_;
						f_570_ += f_601_;
						f_568_ += f_591_;
						f_573_ += f_602_;
						f_571_ += f_592_;
						f_542_ += (float) anInt98;
					}
					while (--f >= 0.0F) {
						method215(anIntArray99, anIntArray123, (int) f_542_, (int) f_545_, (int) f_544_, f_548_, f_547_, f_551_, f_550_, f_554_, f_553_, (float) i_560_, (float) i_559_, f_564_, f_563_, f_567_, f_566_, f_570_, f_569_, f_573_, f_572_);
						f_545_ += f_594_;
						f_544_ += f_574_;
						f_548_ += f_595_;
						f_547_ += f_575_;
						f_551_ += f_596_;
						f_550_ += f_576_;
						f_554_ += f_597_;
						f_553_ += f_577_;
						i_560_ += f_598_;
						i_559_ += f_578_;
						f_564_ += f_599_;
						f_563_ += f_579_;
						f_567_ += f_600_;
						f_566_ += f_580_;
						f_570_ += f_601_;
						f_569_ += f_581_;
						f_573_ += f_602_;
						f_572_ += f_582_;
						f_542_ += (float) anInt98;
					}
				}
			}
		}
	}

	private final void method215(int[] is, int[] is_604_, int i, int i_605_, int i_606_, float f, float f_607_, float f_608_, float f_609_, float f_610_, float f_611_, float f_612_, float f_613_, float f_614_, float f_615_, float f_616_, float f_617_, float f_618_, float f_619_, float f_620_, float f_621_) {
		int i_622_ = i_606_ - i_605_;
		float f_623_ = 1.0F / (float) i_622_;
		float f_624_ = (f_607_ - f) * f_623_;
		float f_625_ = (f_609_ - f_608_) * f_623_;
		float f_626_ = (f_611_ - f_610_) * f_623_;
		float f_627_ = (f_613_ - f_612_) * f_623_;
		float f_628_ = (f_615_ - f_614_) * f_623_;
		float f_629_ = (f_617_ - f_616_) * f_623_;
		float f_630_ = (f_619_ - f_618_) * f_623_;
		float f_631_ = (f_621_ - f_620_) * f_623_;
		if (aBoolean103) {
			if (i_606_ > anInt107)
				i_606_ = anInt107;
			if (i_605_ < 0) {
				f -= f_624_ * (float) i_605_;
				f_608_ -= f_625_ * (float) i_605_;
				f_610_ -= f_626_ * (float) i_605_;
				f_612_ -= f_627_ * (float) i_605_;
				f_614_ -= f_628_ * (float) i_605_;
				f_616_ -= f_629_ * (float) i_605_;
				f_618_ -= f_630_ * (float) i_605_;
				f_620_ -= f_631_ * (float) i_605_;
				i_605_ = 0;
			}
		}
		if (i_605_ < i_606_) {
			i_622_ = i_606_ - i_605_;
			i += i_605_;
			while (i_622_-- > 0) {
				float f_632_ = 1.0F / f;
				if (f_632_ < aFloatArray105[i]) {
					int i_633_ = (int) (f_608_ * f_632_ * (float) anInt113);
					if (aBoolean119)
						i_633_ &= anInt112;
					else if (i_633_ < 0)
						i_633_ = 0;
					else if (i_633_ > anInt112)
						i_633_ = anInt112;
					int i_634_ = (int) (f_610_ * f_632_ * (float) anInt113);
					if (aBoolean119)
						i_634_ &= anInt112;
					else if (i_634_ < 0)
						i_634_ = 0;
					else if (i_634_ > anInt112)
						i_634_ = anInt112;
					int i_635_ = anIntArray123[i_634_ * anInt113 + i_633_];
					int i_636_ = 255;
					if (anInt115 == 2)
						i_636_ = i_635_ >> 24 & 0xff;
					else if (anInt115 == 1)
						i_636_ = i_635_ == 0 ? 0 : 255;
					else
						i_636_ = (int) f_614_;
					if (i_636_ != 0) {
						if (i_636_ != 255) {
							int i_637_ = (((int) (f_616_ * (float) (i_635_ >> 16 & 0xff)) << 8 & 0xff0000) | ~0xffffff | (int) (f_618_ * (float) (i_635_ >> 8 & 0xff)) & 0xff00 | ((int) (f_620_ * (float) (i_635_ & 0xff)) >> 8));
							if (f_612_ != 0.0F) {
								int i_638_ = (int) (255.0F - f_612_);
								int i_639_ = ((((anInt125 & 0xff00ff) * (int) f_612_ & ~0xff00ff) | ((anInt125 & 0xff00) * (int) f_612_ & 0xff0000)) >>> 8);
								i_637_ = ((((i_637_ & 0xff00ff) * i_638_ & ~0xff00ff) | ((i_637_ & 0xff00) * i_638_ & 0xff0000)) >>> 8) + i_639_;
							}
							int i_640_ = is[i];
							int i_641_ = 255 - i_636_;
							i_637_ = ((((i_640_ & 0xff00ff) * i_641_ + (i_637_ & 0xff00ff) * i_636_) & ~0xff00ff) + (((i_640_ & 0xff00) * i_641_ + (i_637_ & 0xff00) * i_636_) & 0xff0000)) >> 8;
							is[i] = i_637_;
							aFloatArray105[i] = f_632_;
						} else {
							int i_642_ = (((int) (f_616_ * (float) (i_635_ >> 16 & 0xff)) << 8 & 0xff0000) | ~0xffffff | (int) (f_618_ * (float) (i_635_ >> 8 & 0xff)) & 0xff00 | ((int) (f_620_ * (float) (i_635_ & 0xff)) >> 8));
							if (f_612_ != 0.0F) {
								int i_643_ = (int) (255.0F - f_612_);
								int i_644_ = ((((anInt125 & 0xff00ff) * (int) f_612_ & ~0xff00ff) | ((anInt125 & 0xff00) * (int) f_612_ & 0xff0000)) >>> 8);
								i_642_ = ((((i_642_ & 0xff00ff) * i_643_ & ~0xff00ff) | ((i_642_ & 0xff00) * i_643_ & 0xff0000)) >>> 8) + i_644_;
							}
							is[i] = i_642_;
							aFloatArray105[i] = f_632_;
						}
					}
				}
				i++;
				f += f_624_;
				f_608_ += f_625_;
				f_610_ += f_626_;
				f_612_ += f_627_;
				f_614_ += f_628_;
				f_616_ += f_629_;
				f_618_ += f_630_;
				f_620_ += f_631_;
			}
		}
	}

	final void method216(float f, float f_645_, float f_646_, float f_647_, float f_648_, float f_649_, float f_650_, float f_651_, float f_652_, float f_653_, float f_654_, float f_655_) {
		if (aBoolean100) {
			aHa_Sub2_96.method1094((int) f_645_, (int) f, (int) f_648_, (int) f_647_, 126, Class295_Sub1.anIntArray3691[(int) f_653_]);
			aHa_Sub2_96.method1094((int) f_646_, (int) f_645_, (int) f_649_, (int) f_648_, 124, Class295_Sub1.anIntArray3691[(int) f_653_]);
			aHa_Sub2_96.method1094((int) f, (int) f_646_, (int) f_647_, (int) f_649_, 125, Class295_Sub1.anIntArray3691[(int) f_653_]);
		} else {
			float f_656_ = f_648_ - f_647_;
			float f_657_ = f_645_ - f;
			float f_658_ = f_649_ - f_647_;
			float f_659_ = f_646_ - f;
			float f_660_ = f_654_ - f_653_;
			float f_661_ = f_655_ - f_653_;
			float f_662_ = f_651_ - f_650_;
			float f_663_ = f_652_ - f_650_;
			float f_664_;
			if (f_646_ != f_645_)
				f_664_ = (f_649_ - f_648_) / (f_646_ - f_645_);
			else
				f_664_ = 0.0F;
			float f_665_;
			if (f_645_ != f)
				f_665_ = f_656_ / f_657_;
			else
				f_665_ = 0.0F;
			float f_666_;
			if (f_646_ != f)
				f_666_ = f_658_ / f_659_;
			else
				f_666_ = 0.0F;
			float f_667_ = f_656_ * f_659_ - f_658_ * f_657_;
			if (f_667_ != 0.0F) {
				float f_668_ = (f_660_ * f_659_ - f_661_ * f_657_) / f_667_;
				float f_669_ = (f_661_ * f_656_ - f_660_ * f_658_) / f_667_;
				float f_670_ = (f_662_ * f_659_ - f_663_ * f_657_) / f_667_;
				float f_671_ = (f_663_ * f_656_ - f_662_ * f_658_) / f_667_;
				if (f <= f_645_ && f <= f_646_) {
					if (!(f >= (float) anInt101)) {
						if (f_645_ > (float) anInt101)
							f_645_ = (float) anInt101;
						if (f_646_ > (float) anInt101)
							f_646_ = (float) anInt101;
						f_653_ = f_653_ - f_668_ * f_647_ + f_668_;
						f_650_ = f_650_ - f_670_ * f_647_ + f_670_;
						if (f_645_ < f_646_) {
							f_649_ = f_647_;
							if (f < 0.0F) {
								f_649_ -= f_666_ * f;
								f_647_ -= f_665_ * f;
								f_653_ -= f_669_ * f;
								f_650_ -= f_671_ * f;
								f = 0.0F;
							}
							if (f_645_ < 0.0F) {
								f_648_ -= f_664_ * f_645_;
								f_645_ = 0.0F;
							}
							if (f != f_645_ && f_666_ < f_665_ || f == f_645_ && f_666_ > f_664_) {
								f_646_ -= f_645_;
								f_645_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_645_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_649_, (int) f_647_, f_653_, f_668_, f_650_, f_670_);
									f_649_ += f_666_;
									f_647_ += f_665_;
									f_653_ += f_669_;
									f_650_ += f_671_;
									f += (float) anInt98;
								}
								while (--f_646_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_649_, (int) f_648_, f_653_, f_668_, f_650_, f_670_);
									f_649_ += f_666_;
									f_648_ += f_664_;
									f_653_ += f_669_;
									f_650_ += f_671_;
									f += (float) anInt98;
								}
							} else {
								f_646_ -= f_645_;
								f_645_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_645_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_647_, (int) f_649_, f_653_, f_668_, f_650_, f_670_);
									f_649_ += f_666_;
									f_647_ += f_665_;
									f_653_ += f_669_;
									f_650_ += f_671_;
									f += (float) anInt98;
								}
								while (--f_646_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_648_, (int) f_649_, f_653_, f_668_, f_650_, f_670_);
									f_649_ += f_666_;
									f_648_ += f_664_;
									f_653_ += f_669_;
									f_650_ += f_671_;
									f += (float) anInt98;
								}
							}
						} else {
							f_648_ = f_647_;
							if (f < 0.0F) {
								f_648_ -= f_666_ * f;
								f_647_ -= f_665_ * f;
								f_653_ -= f_669_ * f;
								f_650_ -= f_671_ * f;
								f = 0.0F;
							}
							if (f_646_ < 0.0F) {
								f_649_ -= f_664_ * f_646_;
								f_646_ = 0.0F;
							}
							if (f != f_646_ && f_666_ < f_665_ || f == f_646_ && f_664_ > f_665_) {
								f_645_ -= f_646_;
								f_646_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_646_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_648_, (int) f_647_, f_653_, f_668_, f_650_, f_670_);
									f_648_ += f_666_;
									f_647_ += f_665_;
									f_653_ += f_669_;
									f_650_ += f_671_;
									f += (float) anInt98;
								}
								while (--f_645_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_649_, (int) f_647_, f_653_, f_668_, f_650_, f_670_);
									f_649_ += f_664_;
									f_647_ += f_665_;
									f_653_ += f_669_;
									f_650_ += f_671_;
									f += (float) anInt98;
								}
							} else {
								f_645_ -= f_646_;
								f_646_ -= f;
								f = (float) anIntArray97[(int) f];
								while (--f_646_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_647_, (int) f_648_, f_653_, f_668_, f_650_, f_670_);
									f_648_ += f_666_;
									f_647_ += f_665_;
									f_653_ += f_669_;
									f_650_ += f_671_;
									f += (float) anInt98;
								}
								while (--f_645_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f, 0, 0, (int) f_647_, (int) f_649_, f_653_, f_668_, f_650_, f_670_);
									f_649_ += f_664_;
									f_647_ += f_665_;
									f_653_ += f_669_;
									f_650_ += f_671_;
									f += (float) anInt98;
								}
							}
						}
					}
				} else if (f_645_ <= f_646_) {
					if (!(f_645_ >= (float) anInt101)) {
						if (f_646_ > (float) anInt101)
							f_646_ = (float) anInt101;
						if (f > (float) anInt101)
							f = (float) anInt101;
						f_654_ = f_654_ - f_668_ * f_648_ + f_668_;
						f_651_ = f_651_ - f_670_ * f_648_ + f_670_;
						if (f_646_ < f) {
							f_647_ = f_648_;
							if (f_645_ < 0.0F) {
								f_647_ -= f_665_ * f_645_;
								f_648_ -= f_664_ * f_645_;
								f_654_ -= f_669_ * f_645_;
								f_651_ -= f_671_ * f_645_;
								f_645_ = 0.0F;
							}
							if (f_646_ < 0.0F) {
								f_649_ -= f_666_ * f_646_;
								f_646_ = 0.0F;
							}
							if (f_645_ != f_646_ && f_665_ < f_664_ || f_645_ == f_646_ && f_665_ > f_666_) {
								f -= f_646_;
								f_646_ -= f_645_;
								f_645_ = (float) anIntArray97[(int) f_645_];
								while (--f_646_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f_645_, 0, 0, (int) f_647_, (int) f_648_, f_654_, f_668_, f_651_, f_670_);
									f_647_ += f_665_;
									f_648_ += f_664_;
									f_654_ += f_669_;
									f_651_ += f_671_;
									f_645_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f_645_, 0, 0, (int) f_647_, (int) f_649_, f_654_, f_668_, f_651_, f_670_);
									f_647_ += f_665_;
									f_649_ += f_666_;
									f_654_ += f_669_;
									f_651_ += f_671_;
									f_645_ += (float) anInt98;
								}
							} else {
								f -= f_646_;
								f_646_ -= f_645_;
								f_645_ = (float) anIntArray97[(int) f_645_];
								while (--f_646_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f_645_, 0, 0, (int) f_648_, (int) f_647_, f_654_, f_668_, f_651_, f_670_);
									f_647_ += f_665_;
									f_648_ += f_664_;
									f_654_ += f_669_;
									f_651_ += f_671_;
									f_645_ += (float) anInt98;
								}
								while (--f >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f_645_, 0, 0, (int) f_649_, (int) f_647_, f_654_, f_668_, f_651_, f_670_);
									f_647_ += f_665_;
									f_649_ += f_666_;
									f_654_ += f_669_;
									f_651_ += f_671_;
									f_645_ += (float) anInt98;
								}
							}
						} else {
							f_649_ = f_648_;
							if (f_645_ < 0.0F) {
								f_649_ -= f_665_ * f_645_;
								f_648_ -= f_664_ * f_645_;
								f_654_ -= f_669_ * f_645_;
								f_651_ -= f_671_ * f_645_;
								f_645_ = 0.0F;
							}
							if (f < 0.0F) {
								f_647_ -= f_666_ * f;
								f = 0.0F;
							}
							if (f_665_ < f_664_) {
								f_646_ -= f;
								f -= f_645_;
								f_645_ = (float) anIntArray97[(int) f_645_];
								while (--f >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f_645_, 0, 0, (int) f_649_, (int) f_648_, f_654_, f_668_, f_651_, f_670_);
									f_649_ += f_665_;
									f_648_ += f_664_;
									f_654_ += f_669_;
									f_651_ += f_671_;
									f_645_ += (float) anInt98;
								}
								while (--f_646_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f_645_, 0, 0, (int) f_647_, (int) f_648_, f_654_, f_668_, f_651_, f_670_);
									f_647_ += f_666_;
									f_648_ += f_664_;
									f_654_ += f_669_;
									f_651_ += f_671_;
									f_645_ += (float) anInt98;
								}
							} else {
								f_646_ -= f;
								f -= f_645_;
								f_645_ = (float) anIntArray97[(int) f_645_];
								while (--f >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f_645_, 0, 0, (int) f_648_, (int) f_649_, f_654_, f_668_, f_651_, f_670_);
									f_649_ += f_665_;
									f_648_ += f_664_;
									f_654_ += f_669_;
									f_651_ += f_671_;
									f_645_ += (float) anInt98;
								}
								while (--f_646_ >= 0.0F) {
									method207(anIntArray99, aFloatArray105, (int) f_645_, 0, 0, (int) f_648_, (int) f_647_, f_654_, f_668_, f_651_, f_670_);
									f_647_ += f_666_;
									f_648_ += f_664_;
									f_654_ += f_669_;
									f_651_ += f_671_;
									f_645_ += (float) anInt98;
								}
							}
						}
					}
				} else if (!(f_646_ >= (float) anInt101)) {
					if (f > (float) anInt101)
						f = (float) anInt101;
					if (f_645_ > (float) anInt101)
						f_645_ = (float) anInt101;
					f_655_ = f_655_ - f_668_ * f_649_ + f_668_;
					f_652_ = f_652_ - f_670_ * f_649_ + f_670_;
					if (f < f_645_) {
						f_648_ = f_649_;
						if (f_646_ < 0.0F) {
							f_648_ -= f_664_ * f_646_;
							f_649_ -= f_666_ * f_646_;
							f_655_ -= f_669_ * f_646_;
							f_652_ -= f_671_ * f_646_;
							f_646_ = 0.0F;
						}
						if (f < 0.0F) {
							f_647_ -= f_665_ * f;
							f = 0.0F;
						}
						if (f_664_ < f_666_) {
							f_645_ -= f;
							f -= f_646_;
							f_646_ = (float) anIntArray97[(int) f_646_];
							while (--f >= 0.0F) {
								method207(anIntArray99, aFloatArray105, (int) f_646_, 0, 0, (int) f_648_, (int) f_649_, f_655_, f_668_, f_652_, f_670_);
								f_648_ += f_664_;
								f_649_ += f_666_;
								f_655_ += f_669_;
								f_652_ += f_671_;
								f_646_ += (float) anInt98;
							}
							while (--f_645_ >= 0.0F) {
								method207(anIntArray99, aFloatArray105, (int) f_646_, 0, 0, (int) f_648_, (int) f_647_, f_655_, f_668_, f_652_, f_670_);
								f_648_ += f_664_;
								f_647_ += f_665_;
								f_655_ += f_669_;
								f_652_ += f_671_;
								f_646_ += (float) anInt98;
							}
						} else {
							f_645_ -= f;
							f -= f_646_;
							f_646_ = (float) anIntArray97[(int) f_646_];
							while (--f >= 0.0F) {
								method207(anIntArray99, aFloatArray105, (int) f_646_, 0, 0, (int) f_649_, (int) f_648_, f_655_, f_668_, f_652_, f_670_);
								f_648_ += f_664_;
								f_649_ += f_666_;
								f_655_ += f_669_;
								f_652_ += f_671_;
								f_646_ += (float) anInt98;
							}
							while (--f_645_ >= 0.0F) {
								method207(anIntArray99, aFloatArray105, (int) f_646_, 0, 0, (int) f_647_, (int) f_648_, f_655_, f_668_, f_652_, f_670_);
								f_648_ += f_664_;
								f_647_ += f_665_;
								f_655_ += f_669_;
								f_652_ += f_671_;
								f_646_ += (float) anInt98;
							}
						}
					} else {
						f_647_ = f_649_;
						if (f_646_ < 0.0F) {
							f_647_ -= f_664_ * f_646_;
							f_649_ -= f_666_ * f_646_;
							f_655_ -= f_669_ * f_646_;
							f_652_ -= f_671_ * f_646_;
							f_646_ = 0.0F;
						}
						if (f_645_ < 0.0F) {
							f_648_ -= f_665_ * f_645_;
							f_645_ = 0.0F;
						}
						if (f_664_ < f_666_) {
							f -= f_645_;
							f_645_ -= f_646_;
							f_646_ = (float) anIntArray97[(int) f_646_];
							while (--f_645_ >= 0.0F) {
								method207(anIntArray99, aFloatArray105, (int) f_646_, 0, 0, (int) f_647_, (int) f_649_, f_655_, f_668_, f_652_, f_670_);
								f_647_ += f_664_;
								f_649_ += f_666_;
								f_655_ += f_669_;
								f_652_ += f_671_;
								f_646_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method207(anIntArray99, aFloatArray105, (int) f_646_, 0, 0, (int) f_648_, (int) f_649_, f_655_, f_668_, f_652_, f_670_);
								f_648_ += f_665_;
								f_649_ += f_666_;
								f_655_ += f_669_;
								f_652_ += f_671_;
								f_646_ += (float) anInt98;
							}
						} else {
							f -= f_645_;
							f_645_ -= f_646_;
							f_646_ = (float) anIntArray97[(int) f_646_];
							while (--f_645_ >= 0.0F) {
								method207(anIntArray99, aFloatArray105, (int) f_646_, 0, 0, (int) f_649_, (int) f_647_, f_655_, f_668_, f_652_, f_670_);
								f_647_ += f_664_;
								f_649_ += f_666_;
								f_655_ += f_669_;
								f_652_ += f_671_;
								f_646_ += (float) anInt98;
							}
							while (--f >= 0.0F) {
								method207(anIntArray99, aFloatArray105, (int) f_646_, 0, 0, (int) f_649_, (int) f_648_, f_655_, f_668_, f_652_, f_670_);
								f_648_ += f_665_;
								f_649_ += f_666_;
								f_655_ += f_669_;
								f_652_ += f_671_;
								f_646_ += (float) anInt98;
							}
						}
					}
				}
			}
		}
	}

	final void method217(float f, float f_672_, float f_673_, float f_674_, float f_675_, float f_676_, float f_677_, float f_678_, float f_679_, float f_680_, float f_681_, float f_682_, float f_683_, float f_684_, float f_685_, int i, int i_686_, int i_687_, int i_688_, int i_689_, int i_690_, int i_691_, int i_692_, float f_693_, int i_694_, float f_695_, int i_696_, float f_697_) {
		if (i_692_ != anInt124) {
			anIntArray123 = aHa_Sub2_96.method1259(i_692_);
			if (anIntArray123 == null) {
				method199((float) (int) f, (float) (int) f_672_, (float) (int) f_673_, (float) (int) f_674_, (float) (int) f_675_, (float) (int) f_676_, (float) (int) f_677_, (float) (int) f_678_, (float) (int) f_679_, Class69_Sub4.method745(i, 16711680, i_688_ | i_689_ << 24), Class69_Sub4.method745(i_686_, 16711680, i_688_ | i_690_ << 24), Class69_Sub4.method745(i_687_, 16711680, i_688_ | i_691_ << 24));
				return;
			}
			anInt113 = aHa_Sub2_96.method1262(i_692_) ? 64 : aHa_Sub2_96.anInt4081;
			anInt112 = anInt113 - 1;
			anInt115 = aHa_Sub2_96.method1257(i_692_);
		}
		aFloat114 = f_693_;
		if (i_694_ != anInt126) {
			anIntArray116 = aHa_Sub2_96.method1259(i_694_);
			if (anIntArray116 == null) {
				method199((float) (int) f, (float) (int) f_672_, (float) (int) f_673_, (float) (int) f_674_, (float) (int) f_675_, (float) (int) f_676_, (float) (int) f_677_, (float) (int) f_678_, (float) (int) f_679_, Class69_Sub4.method745(i, 16711680, i_688_ | i_689_ << 24), Class69_Sub4.method745(i_686_, 16711680, i_688_ | i_690_ << 24), Class69_Sub4.method745(i_687_, 16711680, i_688_ | i_691_ << 24));
				return;
			}
			anInt117 = aHa_Sub2_96.method1262(i_694_) ? 64 : aHa_Sub2_96.anInt4081;
			anInt127 = anInt117 - 1;
		}
		aFloat128 = f_695_;
		if (i_696_ != anInt121) {
			anIntArray111 = aHa_Sub2_96.method1259(i_696_);
			if (anIntArray111 == null) {
				method199((float) (int) f, (float) (int) f_672_, (float) (int) f_673_, (float) (int) f_674_, (float) (int) f_675_, (float) (int) f_676_, (float) (int) f_677_, (float) (int) f_678_, (float) (int) f_679_, Class69_Sub4.method745(i, 16711680, i_688_ | i_689_ << 24), Class69_Sub4.method745(i_686_, 16711680, i_688_ | i_690_ << 24), Class69_Sub4.method745(i_687_, 16711680, i_688_ | i_691_ << 24));
				return;
			}
			anInt120 = aHa_Sub2_96.method1262(i_696_) ? 64 : aHa_Sub2_96.anInt4081;
			anInt118 = anInt120 - 1;
		}
		aFloat122 = f_697_;
		anInt125 = i_688_;
		float f_698_ = (float) (i >> 24 & 0xff);
		float f_699_ = (float) (i_686_ >> 24 & 0xff);
		float f_700_ = (float) (i_687_ >> 24 & 0xff);
		float f_701_ = (float) (i >> 16 & 0xff);
		float f_702_ = (float) (i_686_ >> 16 & 0xff);
		float f_703_ = (float) (i_687_ >> 16 & 0xff);
		float f_704_ = (float) (i >> 8 & 0xff);
		float f_705_ = (float) (i_686_ >> 8 & 0xff);
		float f_706_ = (float) (i_687_ >> 8 & 0xff);
		float f_707_ = (float) (i & 0xff);
		float f_708_ = (float) (i_686_ & 0xff);
		float f_709_ = (float) (i_687_ & 0xff);
		f_680_ /= f_677_;
		f_681_ /= f_678_;
		f_682_ /= f_679_;
		f_683_ /= f_677_;
		f_684_ /= f_678_;
		f_685_ /= f_679_;
		f_677_ = 1.0F / f_677_;
		f_678_ = 1.0F / f_678_;
		f_679_ = 1.0F / f_679_;
		float f_710_ = 1.0F;
		float f_711_ = 0.0F;
		float f_712_ = 0.0F;
		float f_713_ = 0.0F;
		float f_714_ = 1.0F;
		float f_715_ = 0.0F;
		float f_716_ = 0.0F;
		float f_717_ = 0.0F;
		float f_718_ = 0.0F;
		float f_719_ = 0.0F;
		float f_720_ = 0.0F;
		float f_721_ = 0.0F;
		float f_722_ = 0.0F;
		float f_723_ = 0.0F;
		float f_724_ = 0.0F;
		float f_725_ = 0.0F;
		float f_726_ = 0.0F;
		if (f_672_ != f) {
			float f_727_ = f_672_ - f;
			f_716_ = (f_675_ - f_674_) / f_727_;
			f_717_ = (f_678_ - f_677_) / f_727_;
			f_718_ = (f_681_ - f_680_) / f_727_;
			f_719_ = (f_684_ - f_683_) / f_727_;
			f_720_ = (float) (i_690_ - i_689_) / f_727_;
			f_721_ = (f_699_ - f_698_) / f_727_;
			f_722_ = (f_702_ - f_701_) / f_727_;
			f_723_ = (f_705_ - f_704_) / f_727_;
			f_724_ = (f_708_ - f_707_) / f_727_;
			f_725_ = (f_711_ - f_710_) / f_727_;
			f_726_ = (f_714_ - f_713_) / f_727_;
		}
		float f_728_ = 0.0F;
		float f_729_ = 0.0F;
		float f_730_ = 0.0F;
		float f_731_ = 0.0F;
		float f_732_ = 0.0F;
		float f_733_ = 0.0F;
		float f_734_ = 0.0F;
		float f_735_ = 0.0F;
		float f_736_ = 0.0F;
		float f_737_ = 0.0F;
		float f_738_ = 0.0F;
		if (f_673_ != f_672_) {
			float f_739_ = f_673_ - f_672_;
			f_728_ = (f_676_ - f_675_) / f_739_;
			f_729_ = (f_679_ - f_678_) / f_739_;
			f_730_ = (f_682_ - f_681_) / f_739_;
			f_731_ = (f_685_ - f_684_) / f_739_;
			f_732_ = (float) (i_691_ - i_690_) / f_739_;
			f_733_ = (f_700_ - f_699_) / f_739_;
			f_734_ = (f_703_ - f_702_) / f_739_;
			f_735_ = (f_706_ - f_705_) / f_739_;
			f_736_ = (f_709_ - f_708_) / f_739_;
			f_737_ = (f_712_ - f_711_) / f_739_;
			f_738_ = (f_715_ - f_714_) / f_739_;
		}
		float f_740_ = 0.0F;
		float f_741_ = 0.0F;
		float f_742_ = 0.0F;
		float f_743_ = 0.0F;
		float f_744_ = 0.0F;
		float f_745_ = 0.0F;
		float f_746_ = 0.0F;
		float f_747_ = 0.0F;
		float f_748_ = 0.0F;
		float f_749_ = 0.0F;
		float f_750_ = 0.0F;
		if (f != f_673_) {
			float f_751_ = f - f_673_;
			f_740_ = (f_674_ - f_676_) / f_751_;
			f_741_ = (f_677_ - f_679_) / f_751_;
			f_742_ = (f_680_ - f_682_) / f_751_;
			f_743_ = (f_683_ - f_685_) / f_751_;
			f_744_ = (float) (i_689_ - i_691_) / f_751_;
			f_745_ = (f_698_ - f_700_) / f_751_;
			f_746_ = (f_701_ - f_703_) / f_751_;
			f_747_ = (f_704_ - f_706_) / f_751_;
			f_748_ = (f_707_ - f_709_) / f_751_;
			f_749_ = (f_710_ - f_712_) / f_751_;
			f_750_ = (f_713_ - f_715_) / f_751_;
		}
		if (f <= f_672_ && f <= f_673_) {
			if (!(f >= (float) anInt101)) {
				if (f_672_ > (float) anInt101)
					f_672_ = (float) anInt101;
				if (f_673_ > (float) anInt101)
					f_673_ = (float) anInt101;
				if (f_672_ < f_673_) {
					f_676_ = f_674_;
					f_679_ = f_677_;
					f_682_ = f_680_;
					f_685_ = f_683_;
					i_691_ = i_689_;
					f_700_ = f_698_;
					f_703_ = f_701_;
					f_706_ = f_704_;
					f_709_ = f_707_;
					f_712_ = f_710_;
					f_715_ = f_713_;
					if (f < 0.0F) {
						f_674_ -= f_716_ * f;
						f_676_ -= f_740_ * f;
						f_677_ -= f_717_ * f;
						f_679_ -= f_741_ * f;
						f_680_ -= f_718_ * f;
						f_682_ -= f_742_ * f;
						f_683_ -= f_719_ * f;
						f_685_ -= f_743_ * f;
						i_689_ -= f_720_ * f;
						i_691_ -= f_744_ * f;
						f_698_ -= f_721_ * f;
						f_700_ -= f_745_ * f;
						f_701_ -= f_722_ * f;
						f_703_ -= f_746_ * f;
						f_704_ -= f_723_ * f;
						f_706_ -= f_747_ * f;
						f_707_ -= f_724_ * f;
						f_709_ -= f_748_ * f;
						f_710_ -= f_725_ * f;
						f_712_ -= f_749_ * f;
						f_713_ -= f_726_ * f;
						f_715_ -= f_750_ * f;
						f = 0.0F;
					}
					if (f_672_ < 0.0F) {
						f_675_ -= f_728_ * f_672_;
						f_678_ -= f_729_ * f_672_;
						f_681_ -= f_730_ * f_672_;
						f_684_ -= f_731_ * f_672_;
						i_690_ -= f_732_ * f_672_;
						f_699_ -= f_733_ * f_672_;
						f_702_ -= f_734_ * f_672_;
						f_705_ -= f_735_ * f_672_;
						f_708_ -= f_736_ * f_672_;
						f_711_ -= f_737_ * f_672_;
						f_714_ -= f_738_ * f_672_;
						f_672_ = 0.0F;
					}
					if (f != f_672_ && f_740_ < f_716_ || f == f_672_ && f_740_ > f_728_) {
						f_673_ -= f_672_;
						f_672_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_672_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f, (int) f_676_, (int) f_674_, f_679_, f_677_, f_682_, f_680_, f_685_, f_683_, (float) i_691_, (float) i_689_, f_700_, f_698_, f_703_, f_701_, f_706_, f_704_, f_709_, f_707_, f_712_, f_710_, f_715_, f_713_);
							f_674_ += f_716_;
							f_676_ += f_740_;
							f_677_ += f_717_;
							f_679_ += f_741_;
							f_680_ += f_718_;
							f_682_ += f_742_;
							f_683_ += f_719_;
							f_685_ += f_743_;
							i_689_ += f_720_;
							i_691_ += f_744_;
							f_698_ += f_721_;
							f_700_ += f_745_;
							f_701_ += f_722_;
							f_703_ += f_746_;
							f_704_ += f_723_;
							f_706_ += f_747_;
							f_707_ += f_724_;
							f_709_ += f_748_;
							f_710_ += f_725_;
							f_712_ += f_749_;
							f_713_ += f_726_;
							f_715_ += f_726_;
							f += (float) anInt98;
						}
						while (--f_673_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f, (int) f_676_, (int) f_675_, f_679_, f_678_, f_682_, f_681_, f_685_, f_684_, (float) i_691_, (float) i_690_, f_700_, f_699_, f_703_, f_702_, f_706_, f_705_, f_709_, f_708_, f_712_, f_711_, f_715_, f_714_);
							f_675_ += f_728_;
							f_676_ += f_740_;
							f_678_ += f_729_;
							f_679_ += f_741_;
							f_681_ += f_730_;
							f_682_ += f_742_;
							f_684_ += f_731_;
							f_685_ += f_743_;
							i_690_ += f_732_;
							i_691_ += f_744_;
							f_699_ += f_733_;
							f_700_ += f_745_;
							f_702_ += f_734_;
							f_703_ += f_746_;
							f_705_ += f_735_;
							f_706_ += f_747_;
							f_708_ += f_736_;
							f_709_ += f_748_;
							f_711_ += f_737_;
							f_712_ += f_749_;
							f_714_ += f_738_;
							f_715_ += f_750_;
							f += (float) anInt98;
						}
					} else {
						f_673_ -= f_672_;
						f_672_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_672_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f, (int) f_674_, (int) f_676_, f_677_, f_679_, f_680_, f_682_, f_683_, f_685_, (float) i_689_, (float) i_691_, f_698_, f_700_, f_701_, f_703_, f_704_, f_706_, f_707_, f_709_, f_710_, f_712_, f_713_, f_715_);
							f_674_ += f_716_;
							f_676_ += f_740_;
							f_677_ += f_717_;
							f_679_ += f_741_;
							f_680_ += f_718_;
							f_682_ += f_742_;
							f_683_ += f_719_;
							f_685_ += f_743_;
							i_689_ += f_720_;
							i_691_ += f_744_;
							f_698_ += f_721_;
							f_700_ += f_745_;
							f_701_ += f_722_;
							f_703_ += f_746_;
							f_704_ += f_723_;
							f_706_ += f_747_;
							f_707_ += f_724_;
							f_709_ += f_748_;
							f_710_ += f_725_;
							f_712_ += f_749_;
							f_713_ += f_726_;
							f_715_ += f_750_;
							f += (float) anInt98;
						}
						while (--f_673_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f, (int) f_675_, (int) f_676_, f_678_, f_679_, f_681_, f_682_, f_684_, f_685_, (float) i_690_, (float) i_691_, f_699_, f_700_, f_702_, f_703_, f_705_, f_706_, f_708_, f_709_, f_711_, f_712_, f_714_, f_715_);
							f_675_ += f_728_;
							f_676_ += f_740_;
							f_678_ += f_729_;
							f_679_ += f_741_;
							f_681_ += f_730_;
							f_682_ += f_742_;
							f_684_ += f_731_;
							f_685_ += f_743_;
							i_690_ += f_732_;
							i_691_ += f_744_;
							f_699_ += f_733_;
							f_700_ += f_745_;
							f_702_ += f_734_;
							f_703_ += f_746_;
							f_705_ += f_735_;
							f_706_ += f_747_;
							f_708_ += f_736_;
							f_709_ += f_748_;
							f_711_ += f_737_;
							f_712_ += f_749_;
							f_714_ += f_738_;
							f_715_ += f_750_;
							f += (float) anInt98;
						}
					}
				} else {
					f_675_ = f_674_;
					f_678_ = f_677_;
					f_681_ = f_680_;
					f_684_ = f_683_;
					i_690_ = i_689_;
					f_699_ = f_698_;
					f_702_ = f_701_;
					f_705_ = f_704_;
					f_708_ = f_707_;
					f_711_ = f_710_;
					f_714_ = f_713_;
					if (f < 0.0F) {
						f_674_ -= f_716_ * f;
						f_675_ -= f_740_ * f;
						f_677_ -= f_717_ * f;
						f_678_ -= f_741_ * f;
						f_680_ -= f_718_ * f;
						f_681_ -= f_742_ * f;
						f_683_ -= f_719_ * f;
						f_684_ -= f_743_ * f;
						i_689_ -= f_720_ * f;
						i_690_ -= f_744_ * f;
						f_698_ -= f_721_ * f;
						f_699_ -= f_745_ * f;
						f_701_ -= f_722_ * f;
						f_702_ -= f_746_ * f;
						f_704_ -= f_723_ * f;
						f_705_ -= f_747_ * f;
						f_707_ -= f_724_ * f;
						f_708_ -= f_748_ * f;
						f_710_ -= f_725_ * f;
						f_711_ -= f_749_ * f;
						f_713_ -= f_726_ * f;
						f_714_ -= f_750_ * f;
						f = 0.0F;
					}
					if (f_673_ < 0.0F) {
						f_676_ -= f_728_ * f_673_;
						f_679_ -= f_729_ * f_673_;
						f_682_ -= f_730_ * f_673_;
						f_685_ -= f_731_ * f_673_;
						i_691_ -= f_732_ * f_673_;
						f_700_ -= f_733_ * f_673_;
						f_703_ -= f_734_ * f_673_;
						f_706_ -= f_735_ * f_673_;
						f_709_ -= f_736_ * f_673_;
						f_712_ -= f_737_ * f_673_;
						f_715_ -= f_738_ * f_673_;
						f_673_ = 0.0F;
					}
					if (f != f_673_ && f_740_ < f_716_ || f == f_673_ && f_728_ > f_716_) {
						f_672_ -= f_673_;
						f_673_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_673_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f, (int) f_675_, (int) f_674_, f_678_, f_677_, f_681_, f_680_, f_684_, f_683_, (float) i_690_, (float) i_689_, f_699_, f_698_, f_702_, f_701_, f_705_, f_704_, f_708_, f_707_, f_711_, f_710_, f_714_, f_713_);
							f_674_ += f_716_;
							f_675_ += f_740_;
							f_677_ += f_717_;
							f_678_ += f_741_;
							f_680_ += f_718_;
							f_681_ += f_742_;
							f_683_ += f_719_;
							f_684_ += f_743_;
							i_689_ += f_720_;
							i_690_ += f_744_;
							f_698_ += f_721_;
							f_699_ += f_745_;
							f_701_ += f_722_;
							f_702_ += f_746_;
							f_704_ += f_723_;
							f_705_ += f_747_;
							f_707_ += f_724_;
							f_708_ += f_748_;
							f_710_ += f_725_;
							f_711_ += f_749_;
							f_713_ += f_726_;
							f_714_ += f_750_;
							f += (float) anInt98;
						}
						while (--f_672_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f, (int) f_676_, (int) f_674_, f_679_, f_677_, f_682_, f_680_, f_685_, f_683_, (float) i_691_, (float) i_689_, f_700_, f_698_, f_703_, f_701_, f_706_, f_704_, f_709_, f_707_, f_712_, f_710_, f_715_, f_713_);
							f_676_ += f_728_;
							f_674_ += f_716_;
							f_679_ += f_729_;
							f_677_ += f_717_;
							f_682_ += f_730_;
							f_680_ += f_718_;
							f_685_ += f_731_;
							f_683_ += f_719_;
							i_691_ += f_732_;
							i_689_ += f_720_;
							f_700_ += f_733_;
							f_698_ += f_721_;
							f_703_ += f_734_;
							f_701_ += f_722_;
							f_706_ += f_735_;
							f_704_ += f_723_;
							f_709_ += f_736_;
							f_707_ += f_724_;
							f_712_ += f_737_;
							f_710_ += f_725_;
							f_715_ += f_738_;
							f_713_ += f_726_;
							f += (float) anInt98;
						}
					} else {
						f_672_ -= f_673_;
						f_673_ -= f;
						f = (float) anIntArray97[(int) f];
						while (--f_673_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f, (int) f_674_, (int) f_675_, f_677_, f_678_, f_680_, f_681_, f_683_, f_684_, (float) i_689_, (float) i_690_, f_698_, f_699_, f_701_, f_702_, f_704_, f_705_, f_707_, f_708_, f_710_, f_711_, f_713_, f_714_);
							f_675_ += f_740_;
							f_674_ += f_716_;
							f_678_ += f_741_;
							f_677_ += f_717_;
							f_681_ += f_742_;
							f_680_ += f_718_;
							f_684_ += f_743_;
							f_683_ += f_719_;
							i_690_ += f_744_;
							i_689_ += f_720_;
							f_699_ += f_745_;
							f_698_ += f_721_;
							f_702_ += f_746_;
							f_701_ += f_722_;
							f_705_ += f_747_;
							f_704_ += f_723_;
							f_708_ += f_748_;
							f_707_ += f_724_;
							f_711_ += f_749_;
							f_710_ += f_725_;
							f_714_ += f_750_;
							f_713_ += f_726_;
							f += (float) anInt98;
						}
						while (--f_672_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f, (int) f_674_, (int) f_676_, f_677_, f_679_, f_680_, f_682_, f_683_, f_685_, (float) i_689_, (float) i_691_, f_698_, f_700_, f_701_, f_703_, f_704_, f_706_, f_707_, f_709_, f_710_, f_712_, f_713_, f_715_);
							f_674_ += f_716_;
							f_676_ += f_728_;
							f_677_ += f_717_;
							f_679_ += f_729_;
							f_680_ += f_718_;
							f_682_ += f_730_;
							f_683_ += f_719_;
							f_685_ += f_731_;
							i_689_ += f_720_;
							i_691_ += f_732_;
							f_698_ += f_721_;
							f_700_ += f_733_;
							f_701_ += f_722_;
							f_703_ += f_734_;
							f_704_ += f_723_;
							f_706_ += f_735_;
							f_707_ += f_724_;
							f_709_ += f_736_;
							f_710_ += f_725_;
							f_712_ += f_737_;
							f_713_ += f_726_;
							f_715_ += f_738_;
							f += (float) anInt98;
						}
					}
				}
			}
		} else if (f_672_ <= f_673_) {
			if (!(f_672_ >= (float) anInt101)) {
				if (f_673_ > (float) anInt101)
					f_673_ = (float) anInt101;
				if (f > (float) anInt101)
					f = (float) anInt101;
				if (f_673_ < f) {
					f_674_ = f_675_;
					f_677_ = f_678_;
					f_680_ = f_681_;
					f_683_ = f_684_;
					i_689_ = i_690_;
					f_698_ = f_699_;
					f_701_ = f_702_;
					f_704_ = f_705_;
					f_707_ = f_708_;
					f_710_ = f_711_;
					f_713_ = f_714_;
					if (f_672_ < 0.0F) {
						f_674_ -= f_716_ * f_672_;
						f_675_ -= f_728_ * f_672_;
						f_677_ -= f_717_ * f_672_;
						f_678_ -= f_729_ * f_672_;
						f_680_ -= f_718_ * f_672_;
						f_681_ -= f_730_ * f_672_;
						f_683_ -= f_719_ * f_672_;
						f_684_ -= f_731_ * f_672_;
						i_689_ -= f_720_ * f_672_;
						i_690_ -= f_732_ * f_672_;
						f_698_ -= f_721_ * f_672_;
						f_699_ -= f_733_ * f_672_;
						f_701_ -= f_722_ * f_672_;
						f_702_ -= f_734_ * f_672_;
						f_704_ -= f_723_ * f_672_;
						f_705_ -= f_735_ * f_672_;
						f_707_ -= f_724_ * f_672_;
						f_708_ -= f_736_ * f_672_;
						f_710_ -= f_725_ * f_672_;
						f_711_ -= f_737_ * f_672_;
						f_713_ -= f_726_ * f_672_;
						f_714_ -= f_738_ * f_672_;
						f_672_ = 0.0F;
					}
					if (f_673_ < 0.0F) {
						f_676_ -= f_740_ * f_673_;
						f_679_ -= f_741_ * f_673_;
						f_682_ -= f_742_ * f_673_;
						f_685_ -= f_743_ * f_673_;
						i_691_ -= f_744_ * f_673_;
						f_700_ -= f_745_ * f_673_;
						f_703_ -= f_746_ * f_673_;
						f_706_ -= f_747_ * f_673_;
						f_709_ -= f_748_ * f_673_;
						f_712_ -= f_749_ * f_673_;
						f_715_ -= f_750_ * f_673_;
						f_673_ = 0.0F;
					}
					if (f_672_ != f_673_ && f_716_ < f_728_ || f_672_ == f_673_ && f_716_ > f_740_) {
						f -= f_673_;
						f_673_ -= f_672_;
						f_672_ = (float) anIntArray97[(int) f_672_];
						while (--f_673_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f_672_, (int) f_674_, (int) f_675_, f_677_, f_678_, f_680_, f_681_, f_683_, f_684_, (float) i_689_, (float) i_690_, f_698_, f_699_, f_701_, f_702_, f_704_, f_705_, f_707_, f_708_, f_710_, f_711_, f_713_, f_714_);
							f_674_ += f_716_;
							f_675_ += f_728_;
							f_677_ += f_717_;
							f_678_ += f_729_;
							f_680_ += f_718_;
							f_681_ += f_730_;
							f_683_ += f_719_;
							f_684_ += f_731_;
							i_689_ += f_720_;
							i_690_ += f_732_;
							f_698_ += f_721_;
							f_699_ += f_733_;
							f_701_ += f_722_;
							f_702_ += f_734_;
							f_704_ += f_723_;
							f_705_ += f_735_;
							f_707_ += f_724_;
							f_708_ += f_736_;
							f_710_ += f_725_;
							f_711_ += f_737_;
							f_713_ += f_726_;
							f_714_ += f_738_;
							f_672_ += (float) anInt98;
						}
						while (--f >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f_672_, (int) f_674_, (int) f_676_, f_677_, f_679_, f_680_, f_682_, f_683_, f_685_, (float) i_689_, (float) i_691_, f_698_, f_700_, f_701_, f_703_, f_704_, f_706_, f_707_, f_709_, f_710_, f_712_, f_713_, f_715_);
							f_674_ += f_716_;
							f_676_ += f_740_;
							f_677_ += f_717_;
							f_679_ += f_741_;
							f_680_ += f_718_;
							f_682_ += f_742_;
							f_683_ += f_719_;
							f_685_ += f_743_;
							i_689_ += f_720_;
							i_691_ += f_744_;
							f_698_ += f_721_;
							f_700_ += f_745_;
							f_701_ += f_722_;
							f_703_ += f_746_;
							f_704_ += f_723_;
							f_706_ += f_747_;
							f_707_ += f_724_;
							f_709_ += f_748_;
							f_710_ += f_725_;
							f_712_ += f_749_;
							f_713_ += f_726_;
							f_715_ += f_750_;
							f_672_ += (float) anInt98;
						}
					} else {
						f -= f_673_;
						f_673_ -= f_672_;
						f_672_ = (float) anIntArray97[(int) f_672_];
						while (--f_673_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f_672_, (int) f_675_, (int) f_674_, f_678_, f_677_, f_681_, f_680_, f_684_, f_683_, (float) i_690_, (float) i_689_, f_699_, f_698_, f_702_, f_701_, f_705_, f_704_, f_708_, f_707_, f_711_, f_710_, f_714_, f_713_);
							f_675_ += f_728_;
							f_674_ += f_716_;
							f_678_ += f_729_;
							f_677_ += f_717_;
							f_681_ += f_730_;
							f_680_ += f_718_;
							f_684_ += f_731_;
							f_683_ += f_719_;
							i_690_ += f_732_;
							i_689_ += f_720_;
							f_699_ += f_733_;
							f_698_ += f_721_;
							f_702_ += f_734_;
							f_701_ += f_722_;
							f_705_ += f_735_;
							f_704_ += f_723_;
							f_708_ += f_736_;
							f_707_ += f_724_;
							f_712_ += f_737_;
							f_710_ += f_725_;
							f_714_ += f_738_;
							f_713_ += f_726_;
							f_672_ += (float) anInt98;
						}
						while (--f >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f_672_, (int) f_676_, (int) f_674_, f_679_, f_677_, f_682_, f_680_, f_685_, f_683_, (float) i_691_, (float) i_689_, f_700_, f_698_, f_703_, f_701_, f_706_, f_704_, f_709_, f_707_, f_712_, f_710_, f_715_, f_713_);
							f_676_ += f_740_;
							f_674_ += f_716_;
							f_679_ += f_741_;
							f_677_ += f_717_;
							f_682_ += f_742_;
							f_680_ += f_718_;
							f_685_ += f_743_;
							f_683_ += f_719_;
							i_691_ += f_744_;
							i_689_ += f_720_;
							f_700_ += f_745_;
							f_698_ += f_721_;
							f_703_ += f_746_;
							f_701_ += f_722_;
							f_706_ += f_747_;
							f_704_ += f_723_;
							f_709_ += f_748_;
							f_707_ += f_724_;
							f_712_ += f_749_;
							f_710_ += f_725_;
							f_715_ += f_750_;
							f_713_ += f_726_;
							f_672_ += (float) anInt98;
						}
					}
				} else {
					f_676_ = f_675_;
					f_679_ = f_678_;
					f_682_ = f_681_;
					f_685_ = f_684_;
					i_691_ = i_690_;
					f_700_ = f_699_;
					f_703_ = f_702_;
					f_706_ = f_705_;
					f_709_ = f_708_;
					f_712_ = f_711_;
					f_715_ = f_714_;
					if (f_672_ < 0.0F) {
						f_676_ -= f_716_ * f_672_;
						f_675_ -= f_728_ * f_672_;
						f_679_ -= f_717_ * f_672_;
						f_678_ -= f_729_ * f_672_;
						f_682_ -= f_718_ * f_672_;
						f_681_ -= f_730_ * f_672_;
						f_685_ -= f_719_ * f_672_;
						f_684_ -= f_731_ * f_672_;
						i_691_ -= f_720_ * f_672_;
						i_690_ -= f_732_ * f_672_;
						f_700_ -= f_721_ * f_672_;
						f_699_ -= f_733_ * f_672_;
						f_703_ -= f_722_ * f_672_;
						f_702_ -= f_734_ * f_672_;
						f_706_ -= f_723_ * f_672_;
						f_705_ -= f_735_ * f_672_;
						f_709_ -= f_724_ * f_672_;
						f_708_ -= f_736_ * f_672_;
						f_712_ -= f_725_ * f_672_;
						f_711_ -= f_737_ * f_672_;
						f_715_ -= f_726_ * f_672_;
						f_714_ -= f_738_ * f_672_;
						f_672_ = 0.0F;
					}
					if (f < 0.0F) {
						f_674_ -= f_740_ * f;
						f_677_ -= f_741_ * f;
						f_680_ -= f_742_ * f;
						f_683_ -= f_743_ * f;
						i_689_ -= f_744_ * f;
						f_698_ -= f_745_ * f;
						f_701_ -= f_746_ * f;
						f_704_ -= f_747_ * f;
						f_707_ -= f_748_ * f;
						f_710_ -= f_749_ * f;
						f_713_ -= f_750_ * f;
						f = 0.0F;
					}
					f_673_ -= f;
					f -= f_672_;
					f_672_ = (float) anIntArray97[(int) f_672_];
					if (f_716_ < f_728_) {
						while (--f >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f_672_, (int) f_676_, (int) f_675_, f_679_, f_678_, f_682_, f_681_, f_685_, f_684_, (float) i_691_, (float) i_690_, f_700_, f_699_, f_703_, f_702_, f_706_, f_705_, f_709_, f_708_, f_712_, f_711_, f_715_, f_714_);
							f_676_ += f_716_;
							f_675_ += f_728_;
							f_679_ += f_717_;
							f_678_ += f_729_;
							f_682_ += f_718_;
							f_681_ += f_730_;
							f_685_ += f_719_;
							f_684_ += f_731_;
							i_691_ += f_720_;
							i_690_ += f_732_;
							f_700_ += f_721_;
							f_699_ += f_733_;
							f_703_ += f_722_;
							f_702_ += f_734_;
							f_706_ += f_723_;
							f_705_ += f_735_;
							f_709_ += f_724_;
							f_708_ += f_736_;
							f_712_ += f_725_;
							f_711_ += f_737_;
							f_715_ += f_726_;
							f_714_ += f_738_;
							f_672_ += (float) anInt98;
						}
						while (--f_673_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f_672_, (int) f_674_, (int) f_675_, f_677_, f_678_, f_680_, f_681_, f_683_, f_684_, (float) i_689_, (float) i_690_, f_698_, f_699_, f_701_, f_702_, f_704_, f_705_, f_707_, f_708_, f_710_, f_711_, f_713_, f_714_);
							f_674_ += f_740_;
							f_675_ += f_728_;
							f_677_ += f_741_;
							f_678_ += f_729_;
							f_680_ += f_742_;
							f_681_ += f_730_;
							f_683_ += f_743_;
							f_684_ += f_731_;
							i_689_ += f_744_;
							i_690_ += f_732_;
							f_698_ += f_745_;
							f_699_ += f_733_;
							f_701_ += f_746_;
							f_702_ += f_734_;
							f_704_ += f_747_;
							f_705_ += f_735_;
							f_707_ += f_748_;
							f_708_ += f_736_;
							f_710_ += f_749_;
							f_711_ += f_737_;
							f_713_ += f_750_;
							f_714_ += f_738_;
							f_672_ += (float) anInt98;
						}
					} else {
						while (--f >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f_672_, (int) f_675_, (int) f_676_, f_678_, f_679_, f_681_, f_682_, f_684_, f_685_, (float) i_690_, (float) i_691_, f_699_, f_700_, f_702_, f_703_, f_705_, f_706_, f_708_, f_709_, f_711_, f_712_, f_714_, f_715_);
							f_675_ += f_728_;
							f_676_ += f_716_;
							f_678_ += f_729_;
							f_679_ += f_717_;
							f_681_ += f_730_;
							f_682_ += f_718_;
							f_684_ += f_731_;
							f_685_ += f_719_;
							i_690_ += f_732_;
							i_691_ += f_720_;
							f_699_ += f_733_;
							f_700_ += f_721_;
							f_702_ += f_734_;
							f_703_ += f_722_;
							f_705_ += f_735_;
							f_706_ += f_723_;
							f_708_ += f_736_;
							f_709_ += f_724_;
							f_711_ += f_737_;
							f_712_ += f_725_;
							f_714_ += f_738_;
							f_715_ += f_726_;
							f_672_ += (float) anInt98;
						}
						while (--f_673_ >= 0.0F) {
							method204(anIntArray99, anIntArray123, (int) f_672_, (int) f_675_, (int) f_674_, f_678_, f_677_, f_681_, f_680_, f_684_, f_683_, (float) i_690_, (float) i_689_, f_699_, f_698_, f_702_, f_701_, f_705_, f_704_, f_708_, f_707_, f_711_, f_710_, f_714_, f_713_);
							f_675_ += f_728_;
							f_674_ += f_740_;
							f_678_ += f_729_;
							f_677_ += f_741_;
							f_681_ += f_730_;
							f_680_ += f_742_;
							f_684_ += f_731_;
							f_683_ += f_743_;
							i_690_ += f_732_;
							i_689_ += f_744_;
							f_699_ += f_733_;
							f_698_ += f_745_;
							f_702_ += f_734_;
							f_701_ += f_746_;
							f_705_ += f_735_;
							f_704_ += f_747_;
							f_708_ += f_736_;
							f_707_ += f_748_;
							f_711_ += f_737_;
							f_710_ += f_749_;
							f_714_ += f_738_;
							f_713_ += f_750_;
							f_672_ += (float) anInt98;
						}
					}
				}
			}
		} else if (!(f_673_ >= (float) anInt101)) {
			if (f > (float) anInt101)
				f = (float) anInt101;
			if (f_672_ > (float) anInt101)
				f_672_ = (float) anInt101;
			if (f < f_672_) {
				f_675_ = f_676_;
				f_678_ = f_679_;
				f_681_ = f_682_;
				f_684_ = f_685_;
				i_690_ = i_691_;
				f_699_ = f_700_;
				f_702_ = f_703_;
				f_705_ = f_706_;
				f_708_ = f_709_;
				f_711_ = f_712_;
				f_714_ = f_715_;
				if (f_673_ < 0.0F) {
					f_676_ -= f_740_ * f_673_;
					f_675_ -= f_728_ * f_673_;
					f_679_ -= f_741_ * f_673_;
					f_678_ -= f_729_ * f_673_;
					f_682_ -= f_742_ * f_673_;
					f_681_ -= f_730_ * f_673_;
					f_685_ -= f_743_ * f_673_;
					f_684_ -= f_731_ * f_673_;
					i_691_ -= f_744_ * 3.0F;
					i_690_ -= f_732_ * f_673_;
					f_700_ -= f_745_ * f_673_;
					f_699_ -= f_733_ * f_673_;
					f_703_ -= f_746_ * f_673_;
					f_702_ -= f_734_ * f_673_;
					f_706_ -= f_747_ * f_673_;
					f_705_ -= f_735_ * f_673_;
					f_709_ -= f_748_ * f_673_;
					f_708_ -= f_736_ * f_673_;
					f_712_ -= f_749_ * f_673_;
					f_711_ -= f_737_ * f_673_;
					f_715_ -= f_750_ * f_673_;
					f_714_ -= f_738_ * f_673_;
					f_673_ = 0.0F;
				}
				if (f < 0.0F) {
					f_674_ -= f_716_ * f;
					f_677_ -= f_717_ * f;
					f_680_ -= f_718_ * f;
					f_683_ -= f_719_ * f;
					i_689_ -= f_720_ * f;
					f_698_ -= f_721_ * f;
					f_701_ -= f_722_ * f;
					f_704_ -= f_723_ * f;
					f_707_ -= f_724_ * f;
					f_710_ -= f_725_ * f;
					f_713_ -= f_726_ * f;
					f = 0.0F;
				}
				if (f_728_ < f_740_) {
					f_672_ -= f;
					f -= f_673_;
					f_673_ = (float) anIntArray97[(int) f_673_];
					while (--f >= 0.0F) {
						method204(anIntArray99, anIntArray123, (int) f_673_, (int) f_675_, (int) f_676_, f_678_, f_679_, f_681_, f_682_, f_684_, f_685_, (float) i_690_, (float) i_691_, f_699_, f_700_, f_702_, f_703_, f_705_, f_706_, f_708_, f_709_, f_711_, f_712_, f_714_, f_715_);
						f_675_ += f_728_;
						f_676_ += f_740_;
						f_678_ += f_729_;
						f_679_ += f_741_;
						f_681_ += f_730_;
						f_682_ += f_742_;
						f_684_ += f_731_;
						f_685_ += f_743_;
						i_690_ += f_732_;
						i_691_ += f_744_;
						f_699_ += f_733_;
						f_700_ += f_745_;
						f_702_ += f_734_;
						f_703_ += f_746_;
						f_705_ += f_735_;
						f_706_ += f_747_;
						f_708_ += f_736_;
						f_709_ += f_748_;
						f_711_ += f_737_;
						f_712_ += f_749_;
						f_714_ += f_738_;
						f_715_ += f_750_;
						f_673_ += (float) anInt98;
					}
					while (--f_672_ >= 0.0F) {
						method204(anIntArray99, anIntArray123, (int) f_673_, (int) f_675_, (int) f_674_, f_678_, f_677_, f_681_, f_680_, f_684_, f_683_, (float) i_690_, (float) i_689_, f_699_, f_698_, f_702_, f_701_, f_705_, f_704_, f_708_, f_707_, f_711_, f_710_, f_714_, f_713_);
						f_675_ += f_728_;
						f_674_ += f_716_;
						f_678_ += f_729_;
						f_677_ += f_717_;
						f_681_ += f_730_;
						f_680_ += f_718_;
						f_684_ += f_731_;
						f_683_ += f_719_;
						i_690_ += f_732_;
						i_689_ += f_720_;
						f_699_ += f_733_;
						f_698_ += f_721_;
						f_702_ += f_734_;
						f_701_ += f_722_;
						f_705_ += f_735_;
						f_704_ += f_723_;
						f_708_ += f_736_;
						f_707_ += f_724_;
						f_711_ += f_737_;
						f_710_ += f_725_;
						f_714_ += f_738_;
						f_713_ += f_726_;
						f_673_ += (float) anInt98;
					}
				} else {
					f_672_ -= f;
					f -= f_673_;
					f_673_ = (float) anIntArray97[(int) f_673_];
					while (--f >= 0.0F) {
						method204(anIntArray99, anIntArray123, (int) f_673_, (int) f_676_, (int) f_675_, f_679_, f_678_, f_682_, f_681_, f_685_, f_684_, (float) i_691_, (float) i_690_, f_700_, f_699_, f_703_, f_702_, f_706_, f_705_, f_709_, f_708_, f_712_, f_711_, f_715_, f_714_);
						f_676_ += f_740_;
						f_675_ += f_728_;
						f_679_ += f_741_;
						f_678_ += f_729_;
						f_682_ += f_742_;
						f_681_ += f_730_;
						f_685_ += f_743_;
						f_684_ += f_731_;
						i_691_ += f_744_;
						i_690_ += f_732_;
						f_700_ += f_745_;
						f_699_ += f_733_;
						f_703_ += f_746_;
						f_702_ += f_734_;
						f_706_ += f_747_;
						f_705_ += f_735_;
						f_709_ += f_748_;
						f_708_ += f_736_;
						f_712_ += f_749_;
						f_711_ += f_737_;
						f_715_ += f_750_;
						f_714_ += f_738_;
						f_673_ += (float) anInt98;
					}
					while (--f_672_ >= 0.0F) {
						method204(anIntArray99, anIntArray123, (int) f_673_, (int) f_674_, (int) f_675_, f_677_, f_678_, f_680_, f_681_, f_683_, f_684_, (float) i_689_, (float) i_690_, f_698_, f_699_, f_701_, f_702_, f_704_, f_705_, f_707_, f_708_, f_710_, f_711_, f_713_, f_714_);
						f_674_ += f_716_;
						f_675_ += f_728_;
						f_677_ += f_717_;
						f_678_ += f_729_;
						f_680_ += f_718_;
						f_681_ += f_730_;
						f_683_ += f_719_;
						f_684_ += f_731_;
						i_689_ += f_720_;
						i_690_ += f_732_;
						f_698_ += f_721_;
						f_699_ += f_733_;
						f_701_ += f_722_;
						f_702_ += f_734_;
						f_704_ += f_723_;
						f_705_ += f_735_;
						f_707_ += f_724_;
						f_708_ += f_736_;
						f_710_ += f_725_;
						f_711_ += f_737_;
						f_713_ += f_726_;
						f_714_ += f_738_;
						f_673_ += (float) anInt98;
					}
				}
			} else {
				f_674_ = f_676_;
				f_677_ = f_679_;
				f_680_ = f_682_;
				f_683_ = f_685_;
				i_689_ = i_691_;
				f_698_ = f_700_;
				f_701_ = f_703_;
				f_704_ = f_706_;
				f_707_ = f_709_;
				f_710_ = f_712_;
				f_713_ = f_715_;
				if (f_673_ < 0.0F) {
					f_676_ -= f_740_ * f_673_;
					f_674_ -= f_728_ * f_673_;
					f_679_ -= f_741_ * f_673_;
					f_677_ -= f_729_ * f_673_;
					f_682_ -= f_742_ * f_673_;
					f_680_ -= f_730_ * f_673_;
					f_685_ -= f_743_ * f_673_;
					f_683_ -= f_731_ * f_673_;
					i_691_ -= f_744_ * 3.0F;
					i_689_ -= f_732_ * f_673_;
					f_700_ -= f_745_ * f_673_;
					f_698_ -= f_733_ * f_673_;
					f_703_ -= f_746_ * f_673_;
					f_701_ -= f_734_ * f_673_;
					f_706_ -= f_747_ * f_673_;
					f_704_ -= f_735_ * f_673_;
					f_709_ -= f_748_ * f_673_;
					f_707_ -= f_736_ * f_673_;
					f_712_ -= f_749_ * f_673_;
					f_710_ -= f_737_ * f_673_;
					f_715_ -= f_750_ * f_673_;
					f_713_ -= f_738_ * f_673_;
					f_673_ = 0.0F;
				}
				if (f_672_ < 0.0F) {
					f_675_ -= f_716_ * f_672_;
					f_678_ -= f_717_ * f_672_;
					f_681_ -= f_718_ * f_672_;
					f_684_ -= f_719_ * f_672_;
					i_690_ -= f_720_ * f_672_;
					f_699_ -= f_721_ * f_672_;
					f_702_ -= f_722_ * f_672_;
					f_705_ -= f_723_ * f_672_;
					f_708_ -= f_724_ * f_672_;
					f_711_ -= f_725_ * f_672_;
					f_714_ -= f_726_ * f_672_;
					f_672_ = 0.0F;
				}
				if (f_728_ < f_740_) {
					f -= f_672_;
					f_672_ -= f_673_;
					f_673_ = (float) anIntArray97[(int) f_673_];
					while (--f_672_ >= 0.0F) {
						method204(anIntArray99, anIntArray123, (int) f_673_, (int) f_674_, (int) f_676_, f_677_, f_679_, f_680_, f_682_, f_683_, f_685_, (float) i_689_, (float) i_691_, f_698_, f_700_, f_701_, f_703_, f_704_, f_706_, f_707_, f_709_, f_710_, f_712_, f_713_, f_715_);
						f_674_ += f_728_;
						f_676_ += f_740_;
						f_677_ += f_729_;
						f_679_ += f_741_;
						f_680_ += f_730_;
						f_682_ += f_742_;
						f_683_ += f_731_;
						f_685_ += f_743_;
						i_689_ += f_732_;
						i_691_ += f_744_;
						f_698_ += f_733_;
						f_700_ += f_745_;
						f_701_ += f_734_;
						f_703_ += f_746_;
						f_704_ += f_735_;
						f_706_ += f_747_;
						f_707_ += f_736_;
						f_709_ += f_748_;
						f_710_ += f_737_;
						f_712_ += f_749_;
						f_713_ += f_738_;
						f_715_ += f_750_;
						f_673_ += (float) anInt98;
					}
					while (--f >= 0.0F) {
						method204(anIntArray99, anIntArray123, (int) f_673_, (int) f_675_, (int) f_676_, f_678_, f_679_, f_681_, f_682_, f_684_, f_685_, (float) i_690_, (float) i_691_, f_699_, f_700_, f_702_, f_703_, f_705_, f_706_, f_708_, f_709_, f_711_, f_712_, f_714_, f_715_);
						f_675_ += f_716_;
						f_676_ += f_740_;
						f_678_ += f_717_;
						f_679_ += f_741_;
						f_681_ += f_718_;
						f_682_ += f_742_;
						f_684_ += f_719_;
						f_685_ += f_743_;
						i_690_ += f_720_;
						i_691_ += f_744_;
						f_699_ += f_721_;
						f_700_ += f_745_;
						f_702_ += f_722_;
						f_703_ += f_746_;
						f_705_ += f_723_;
						f_706_ += f_747_;
						f_708_ += f_724_;
						f_709_ += f_748_;
						f_711_ += f_725_;
						f_712_ += f_749_;
						f_714_ += f_726_;
						f_715_ += f_750_;
						f_673_ += (float) anInt98;
					}
				} else {
					f -= f_672_;
					f_672_ -= f_673_;
					f_673_ = (float) anIntArray97[(int) f_673_];
					while (--f_672_ >= 0.0F) {
						method204(anIntArray99, anIntArray123, (int) f_673_, (int) f_676_, (int) f_674_, f_679_, f_677_, f_682_, f_680_, f_685_, f_683_, (float) i_691_, (float) i_689_, f_700_, f_698_, f_703_, f_701_, f_706_, f_704_, f_709_, f_707_, f_712_, f_710_, f_715_, f_713_);
						f_676_ += f_740_;
						f_674_ += f_728_;
						f_679_ += f_741_;
						f_677_ += f_729_;
						f_682_ += f_742_;
						f_680_ += f_730_;
						f_685_ += f_743_;
						f_683_ += f_731_;
						i_691_ += f_744_;
						i_689_ += f_732_;
						f_700_ += f_745_;
						f_698_ += f_733_;
						f_703_ += f_746_;
						f_701_ += f_734_;
						f_706_ += f_747_;
						f_704_ += f_735_;
						f_709_ += f_748_;
						f_707_ += f_736_;
						f_712_ += f_749_;
						f_710_ += f_737_;
						f_715_ += f_750_;
						f_713_ += f_738_;
						f_673_ += (float) anInt98;
					}
					while (--f >= 0.0F) {
						method204(anIntArray99, anIntArray123, (int) f_673_, (int) f_676_, (int) f_675_, f_679_, f_678_, f_682_, f_681_, f_685_, f_684_, (float) i_691_, (float) i_690_, f_700_, f_699_, f_703_, f_702_, f_706_, f_705_, f_709_, f_708_, f_712_, f_711_, f_715_, f_714_);
						f_676_ += f_740_;
						f_675_ += f_716_;
						f_679_ += f_741_;
						f_678_ += f_717_;
						f_682_ += f_742_;
						f_681_ += f_718_;
						f_685_ += f_743_;
						f_684_ += f_719_;
						i_691_ += f_744_;
						i_690_ += f_720_;
						f_700_ += f_745_;
						f_699_ += f_721_;
						f_703_ += f_746_;
						f_702_ += f_722_;
						f_706_ += f_747_;
						f_705_ += f_723_;
						f_709_ += f_748_;
						f_708_ += f_724_;
						f_712_ += f_749_;
						f_711_ += f_725_;
						f_715_ += f_750_;
						f_714_ += f_726_;
						f_673_ += (float) anInt98;
					}
				}
			}
		}
	}

	private final void method218(int[] is, float[] fs, int i, int i_752_, int i_753_, int i_754_, int i_755_, float f, float f_756_, float f_757_, float f_758_, float f_759_, float f_760_, float f_761_, float f_762_) {
		if (aBoolean103) {
			if (i_755_ > anInt107)
				i_755_ = anInt107;
			if (i_754_ < 0)
				i_754_ = 0;
		}
		if (i_754_ < i_755_) {
			if (aBoolean110) {
				i += i_754_;
				f_757_ += f_758_ * (float) i_754_;
				f_759_ += f_760_ * (float) i_754_;
				f_761_ += f_762_ * (float) i_754_;
				if (aBoolean102) {
					i_753_ = i_755_ - i_754_ >> 2;
					f_758_ *= 4.0F;
					f_760_ *= 4.0F;
					f_762_ *= 4.0F;
					if (anInt108 == 0) {
						if (i_753_ > 0) {
							do {
								i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
								f_757_ += f_758_;
								f_759_ += f_760_;
								f_761_ += f_762_;
								is[i++] = i_752_;
								is[i++] = i_752_;
								is[i++] = i_752_;
								is[i++] = i_752_;
							} while (--i_753_ > 0);
						}
						i_753_ = i_755_ - i_754_ & 0x3;
						if (i_753_ > 0) {
							i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
							do
								is[i++] = i_752_;
							while (--i_753_ > 0);
						}
					} else if (!aBoolean106) {
						int i_763_ = anInt108;
						int i_764_ = 256 - anInt108;
						if (i_753_ > 0) {
							do {
								i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
								f_757_ += f_758_;
								f_759_ += f_760_;
								f_761_ += f_762_;
								i_752_ = (((i_752_ & 0xff00ff) * i_764_ >> 8 & 0xff00ff) + ((i_752_ & 0xff00) * i_764_ >> 8 & 0xff00));
								int i_765_ = is[i];
								is[i++] = (i_752_ + ((i_765_ & 0xff00ff) * i_763_ >> 8 & 0xff00ff) + ((i_765_ & 0xff00) * i_763_ >> 8 & 0xff00));
								i_765_ = is[i];
								is[i++] = (i_752_ + ((i_765_ & 0xff00ff) * i_763_ >> 8 & 0xff00ff) + ((i_765_ & 0xff00) * i_763_ >> 8 & 0xff00));
								i_765_ = is[i];
								is[i++] = (i_752_ + ((i_765_ & 0xff00ff) * i_763_ >> 8 & 0xff00ff) + ((i_765_ & 0xff00) * i_763_ >> 8 & 0xff00));
								i_765_ = is[i];
								is[i++] = (i_752_ + ((i_765_ & 0xff00ff) * i_763_ >> 8 & 0xff00ff) + ((i_765_ & 0xff00) * i_763_ >> 8 & 0xff00));
							} while (--i_753_ > 0);
						}
						i_753_ = i_755_ - i_754_ & 0x3;
						if (i_753_ > 0) {
							i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
							i_752_ = (((i_752_ & 0xff00ff) * i_764_ >> 8 & 0xff00ff) + ((i_752_ & 0xff00) * i_764_ >> 8 & 0xff00));
							do {
								int i_766_ = is[i];
								is[i++] = (i_752_ + ((i_766_ & 0xff00ff) * i_763_ >> 8 & 0xff00ff) + ((i_766_ & 0xff00) * i_763_ >> 8 & 0xff00));
							} while (--i_753_ > 0);
						}
					} else {
						if (i_753_ > 0) {
							do {
								i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff);
								f_757_ += f_758_;
								f_759_ += f_760_;
								f_761_ += f_762_;
								int[] is_767_ = is;
								int i_768_ = i++;
								int i_769_ = i_752_;
								int i_770_ = is_767_[i_768_];
								int i_771_ = i_769_ + i_770_;
								int i_772_ = ((i_769_ & 0xff00ff) + (i_770_ & 0xff00ff));
								i_770_ = (i_772_ & 0x1000100) + (i_771_ - i_772_ & 0x10000);
								is_767_[i_768_] = (i_771_ - i_770_ | ~0xffffff | i_770_ - (i_770_ >>> 8));
								int[] is_773_ = is;
								int i_774_ = i++;
								int i_775_ = i_752_;
								int i_776_ = is_773_[i_774_];
								int i_777_ = i_775_ + i_776_;
								int i_778_ = ((i_775_ & 0xff00ff) + (i_776_ & 0xff00ff));
								i_776_ = (i_778_ & 0x1000100) + (i_777_ - i_778_ & 0x10000);
								is_773_[i_774_] = (i_777_ - i_776_ | ~0xffffff | i_776_ - (i_776_ >>> 8));
								int[] is_779_ = is;
								int i_780_ = i++;
								int i_781_ = i_752_;
								int i_782_ = is_779_[i_780_];
								int i_783_ = i_781_ + i_782_;
								int i_784_ = ((i_781_ & 0xff00ff) + (i_782_ & 0xff00ff));
								i_782_ = (i_784_ & 0x1000100) + (i_783_ - i_784_ & 0x10000);
								is_779_[i_780_] = (i_783_ - i_782_ | ~0xffffff | i_782_ - (i_782_ >>> 8));
								int[] is_785_ = is;
								int i_786_ = i++;
								int i_787_ = i_752_;
								int i_788_ = is_785_[i_786_];
								int i_789_ = i_787_ + i_788_;
								int i_790_ = ((i_787_ & 0xff00ff) + (i_788_ & 0xff00ff));
								i_788_ = (i_790_ & 0x1000100) + (i_789_ - i_790_ & 0x10000);
								is_785_[i_786_] = (i_789_ - i_788_ | ~0xffffff | i_788_ - (i_788_ >>> 8));
							} while (--i_753_ > 0);
						}
						i_753_ = i_755_ - i_754_ & 0x3;
						if (i_753_ > 0) {
							i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff);
							do {
								int[] is_791_ = is;
								int i_792_ = i++;
								int i_793_ = i_752_;
								int i_794_ = is_791_[i_792_];
								int i_795_ = i_793_ + i_794_;
								int i_796_ = ((i_793_ & 0xff00ff) + (i_794_ & 0xff00ff));
								i_794_ = (i_796_ & 0x1000100) + (i_795_ - i_796_ & 0x10000);
								is_791_[i_792_] = (i_795_ - i_794_ | ~0xffffff | i_794_ - (i_794_ >>> 8));
							} while (--i_753_ > 0);
						}
					}
				} else {
					i_753_ = i_755_ - i_754_;
					if (anInt108 == 0) {
						do {
							is[i++] = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
							f_757_ += f_758_;
							f_759_ += f_760_;
							f_761_ += f_762_;
						} while (--i_753_ > 0);
					} else if (!aBoolean106) {
						int i_797_ = anInt108;
						int i_798_ = 256 - anInt108;
						do {
							i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
							f_757_ += f_758_;
							f_759_ += f_760_;
							f_761_ += f_762_;
							i_752_ = (((i_752_ & 0xff00ff) * i_798_ >> 8 & 0xff00ff) + ((i_752_ & 0xff00) * i_798_ >> 8 & 0xff00));
							int i_799_ = is[i];
							is[i++] = (i_752_ + ((i_799_ & 0xff00ff) * i_797_ >> 8 & 0xff00ff) + ((i_799_ & 0xff00) * i_797_ >> 8 & 0xff00));
						} while (--i_753_ > 0);
					} else {
						do {
							int[] is_800_ = is;
							int i_801_ = i++;
							int i_802_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff);
							int i_803_ = is_800_[i_801_];
							int i_804_ = i_802_ + i_803_;
							int i_805_ = (i_802_ & 0xff00ff) + (i_803_ & 0xff00ff);
							i_803_ = (i_805_ & 0x1000100) + (i_804_ - i_805_ & 0x10000);
							is_800_[i_801_] = (i_804_ - i_803_ | ~0xffffff | i_803_ - (i_803_ >>> 8));
							f_757_ += f_758_;
							f_759_ += f_760_;
							f_761_ += f_762_;
						} while (--i_753_ > 0);
					}
				}
			} else {
				i += i_754_ - 1;
				f += f_756_ * (float) i_754_;
				f_757_ += f_758_ * (float) i_754_;
				f_759_ += f_760_ * (float) i_754_;
				f_761_ += f_762_ * (float) i_754_;
				if (aClass240_104.aBoolean2262) {
					if (aBoolean102) {
						i_753_ = i_755_ - i_754_ >> 2;
						f_758_ *= 4.0F;
						f_760_ *= 4.0F;
						f_762_ *= 4.0F;
						if (anInt108 == 0) {
							if (i_753_ > 0) {
								do {
									i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
									f_757_ += f_758_;
									f_759_ += f_760_;
									f_761_ += f_762_;
									if (f < fs[++i]) {
										is[i] = i_752_;
										fs[i] = f;
									}
									f += f_756_;
									if (f < fs[++i]) {
										is[i] = i_752_;
										fs[i] = f;
									}
									f += f_756_;
									if (f < fs[++i]) {
										is[i] = i_752_;
										fs[i] = f;
									}
									f += f_756_;
									if (f < fs[++i]) {
										is[i] = i_752_;
										fs[i] = f;
									}
									f += f_756_;
								} while (--i_753_ > 0);
							}
							i_753_ = i_755_ - i_754_ & 0x3;
							if (i_753_ > 0) {
								i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
								do {
									if (f < fs[++i]) {
										is[i] = i_752_;
										fs[i] = f;
									}
									f += f_756_;
								} while (--i_753_ > 0);
							}
						} else if (!aBoolean106) {
							int i_806_ = anInt108;
							int i_807_ = 256 - anInt108;
							if (i_753_ > 0) {
								do {
									i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
									f_757_ += f_758_;
									f_759_ += f_760_;
									f_761_ += f_762_;
									i_752_ = (((i_752_ & 0xff00ff) * i_807_ >> 8 & 0xff00ff) + ((i_752_ & 0xff00) * i_807_ >> 8 & 0xff00));
									if (f < fs[++i]) {
										int i_808_ = is[i];
										is[i] = (i_752_ + (((i_808_ & 0xff00ff) * i_806_ >> 8) & 0xff00ff) + (((i_808_ & 0xff00) * i_806_ >> 8) & 0xff00));
										fs[i] = f;
									}
									f += f_756_;
									if (f < fs[++i]) {
										int i_809_ = is[i];
										is[i] = (i_752_ + (((i_809_ & 0xff00ff) * i_806_ >> 8) & 0xff00ff) + (((i_809_ & 0xff00) * i_806_ >> 8) & 0xff00));
										fs[i] = f;
									}
									f += f_756_;
									if (f < fs[++i]) {
										int i_810_ = is[i];
										is[i] = (i_752_ + (((i_810_ & 0xff00ff) * i_806_ >> 8) & 0xff00ff) + (((i_810_ & 0xff00) * i_806_ >> 8) & 0xff00));
										fs[i] = f;
									}
									f += f_756_;
									if (f < fs[++i]) {
										int i_811_ = is[i];
										is[i] = (i_752_ + (((i_811_ & 0xff00ff) * i_806_ >> 8) & 0xff00ff) + (((i_811_ & 0xff00) * i_806_ >> 8) & 0xff00));
										fs[i] = f;
									}
									f += f_756_;
								} while (--i_753_ > 0);
							}
							i_753_ = i_755_ - i_754_ & 0x3;
							if (i_753_ > 0) {
								i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
								i_752_ = (((i_752_ & 0xff00ff) * i_807_ >> 8 & 0xff00ff) + ((i_752_ & 0xff00) * i_807_ >> 8 & 0xff00));
								do {
									if (f < fs[++i]) {
										int i_812_ = is[i];
										is[i] = (i_752_ + (((i_812_ & 0xff00ff) * i_806_ >> 8) & 0xff00ff) + (((i_812_ & 0xff00) * i_806_ >> 8) & 0xff00));
										fs[i] = f;
									}
									f += f_756_;
								} while (--i_753_ > 0);
							}
						} else {
							if (i_753_ > 0) {
								do {
									i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff);
									f_757_ += f_758_;
									f_759_ += f_760_;
									f_761_ += f_762_;
									if (f < fs[++i]) {
										int[] is_813_ = is;
										int i_814_ = i;
										int i_815_ = i_752_;
										int i_816_ = is_813_[i_814_];
										int i_817_ = i_815_ + i_816_;
										int i_818_ = ((i_815_ & 0xff00ff) + (i_816_ & 0xff00ff));
										i_816_ = ((i_818_ & 0x1000100) + (i_817_ - i_818_ & 0x10000));
										is_813_[i_814_] = (i_817_ - i_816_ | ~0xffffff | i_816_ - (i_816_ >>> 8));
										fs[i] = f;
									}
									f += f_756_;
									if (f < fs[++i]) {
										int[] is_819_ = is;
										int i_820_ = i;
										int i_821_ = i_752_;
										int i_822_ = is_819_[i_820_];
										int i_823_ = i_821_ + i_822_;
										int i_824_ = ((i_821_ & 0xff00ff) + (i_822_ & 0xff00ff));
										i_822_ = ((i_824_ & 0x1000100) + (i_823_ - i_824_ & 0x10000));
										is_819_[i_820_] = (i_823_ - i_822_ | ~0xffffff | i_822_ - (i_822_ >>> 8));
										fs[i] = f;
									}
									f += f_756_;
									if (f < fs[++i]) {
										int[] is_825_ = is;
										int i_826_ = i;
										int i_827_ = i_752_;
										int i_828_ = is_825_[i_826_];
										int i_829_ = i_827_ + i_828_;
										int i_830_ = ((i_827_ & 0xff00ff) + (i_828_ & 0xff00ff));
										i_828_ = ((i_830_ & 0x1000100) + (i_829_ - i_830_ & 0x10000));
										is_825_[i_826_] = (i_829_ - i_828_ | ~0xffffff | i_828_ - (i_828_ >>> 8));
										fs[i] = f;
									}
									f += f_756_;
									if (f < fs[++i]) {
										int[] is_831_ = is;
										int i_832_ = i;
										int i_833_ = i_752_;
										int i_834_ = is_831_[i_832_];
										int i_835_ = i_833_ + i_834_;
										int i_836_ = ((i_833_ & 0xff00ff) + (i_834_ & 0xff00ff));
										i_834_ = ((i_836_ & 0x1000100) + (i_835_ - i_836_ & 0x10000));
										is_831_[i_832_] = (i_835_ - i_834_ | ~0xffffff | i_834_ - (i_834_ >>> 8));
										fs[i] = f;
									}
									f += f_756_;
								} while (--i_753_ > 0);
							}
							i_753_ = i_755_ - i_754_ & 0x3;
							if (i_753_ > 0) {
								i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff);
								do {
									if (f < fs[++i]) {
										int[] is_837_ = is;
										int i_838_ = i;
										int i_839_ = i_752_;
										int i_840_ = is_837_[i_838_];
										int i_841_ = i_839_ + i_840_;
										int i_842_ = ((i_839_ & 0xff00ff) + (i_840_ & 0xff00ff));
										i_840_ = ((i_842_ & 0x1000100) + (i_841_ - i_842_ & 0x10000));
										is_837_[i_838_] = (i_841_ - i_840_ | ~0xffffff | i_840_ - (i_840_ >>> 8));
										fs[i] = f;
									}
									f += f_756_;
								} while (--i_753_ > 0);
							}
						}
					} else {
						i_753_ = i_755_ - i_754_;
						if (anInt108 == 0) {
							do {
								if (f < fs[++i]) {
									is[i] = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
									fs[i] = f;
								}
								f += f_756_;
								f_757_ += f_758_;
								f_759_ += f_760_;
								f_761_ += f_762_;
							} while (--i_753_ > 0);
						} else if (!aBoolean106) {
							int i_843_ = anInt108;
							int i_844_ = 256 - anInt108;
							do {
								if (f < fs[++i]) {
									i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
									i_752_ = (((i_752_ & 0xff00ff) * i_844_ >> 8 & 0xff00ff) + ((i_752_ & 0xff00) * i_844_ >> 8 & 0xff00));
									int i_845_ = is[i];
									is[i] = (i_752_ + ((i_845_ & 0xff00ff) * i_843_ >> 8 & 0xff00ff) + ((i_845_ & 0xff00) * i_843_ >> 8 & 0xff00));
									fs[i] = f;
								}
								f += f_756_;
								f_757_ += f_758_;
								f_759_ += f_760_;
								f_761_ += f_762_;
							} while (--i_753_ > 0);
						} else {
							do {
								if (f < fs[++i]) {
									int[] is_846_ = is;
									int i_847_ = i;
									int i_848_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff);
									int i_849_ = is_846_[i_847_];
									int i_850_ = i_848_ + i_849_;
									int i_851_ = ((i_848_ & 0xff00ff) + (i_849_ & 0xff00ff));
									i_849_ = ((i_851_ & 0x1000100) + (i_850_ - i_851_ & 0x10000));
									is_846_[i_847_] = (i_850_ - i_849_ | ~0xffffff | i_849_ - (i_849_ >>> 8));
									fs[i] = f;
								}
								f += f_756_;
								f_757_ += f_758_;
								f_759_ += f_760_;
								f_761_ += f_762_;
							} while (--i_753_ > 0);
						}
					}
				} else if (aBoolean102) {
					i_753_ = i_755_ - i_754_ >> 2;
					f_758_ *= 4.0F;
					f_760_ *= 4.0F;
					f_762_ *= 4.0F;
					if (anInt108 == 0) {
						if (i_753_ > 0) {
							do {
								i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
								f_757_ += f_758_;
								f_759_ += f_760_;
								f_761_ += f_762_;
								if (f < fs[++i])
									is[i] = i_752_;
								f += f_756_;
								if (f < fs[++i])
									is[i] = i_752_;
								f += f_756_;
								if (f < fs[++i])
									is[i] = i_752_;
								f += f_756_;
								if (f < fs[++i])
									is[i] = i_752_;
								f += f_756_;
							} while (--i_753_ > 0);
						}
						i_753_ = i_755_ - i_754_ & 0x3;
						if (i_753_ > 0) {
							i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
							do {
								if (f < fs[++i])
									is[i] = i_752_;
								f += f_756_;
							} while (--i_753_ > 0);
						}
					} else if (!aBoolean106) {
						int i_852_ = anInt108;
						int i_853_ = 256 - anInt108;
						if (i_753_ > 0) {
							do {
								i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
								f_757_ += f_758_;
								f_759_ += f_760_;
								f_761_ += f_762_;
								i_752_ = (((i_752_ & 0xff00ff) * i_853_ >> 8 & 0xff00ff) + ((i_752_ & 0xff00) * i_853_ >> 8 & 0xff00));
								if (f < fs[++i]) {
									int i_854_ = is[i];
									is[i] = (i_752_ + ((i_854_ & 0xff00ff) * i_852_ >> 8 & 0xff00ff) + ((i_854_ & 0xff00) * i_852_ >> 8 & 0xff00));
								}
								f += f_756_;
								if (f < fs[++i]) {
									int i_855_ = is[i];
									is[i] = (i_752_ + ((i_855_ & 0xff00ff) * i_852_ >> 8 & 0xff00ff) + ((i_855_ & 0xff00) * i_852_ >> 8 & 0xff00));
								}
								f += f_756_;
								if (f < fs[++i]) {
									int i_856_ = is[i];
									is[i] = (i_752_ + ((i_856_ & 0xff00ff) * i_852_ >> 8 & 0xff00ff) + ((i_856_ & 0xff00) * i_852_ >> 8 & 0xff00));
								}
								f += f_756_;
								if (f < fs[++i]) {
									int i_857_ = is[i];
									is[i] = (i_752_ + ((i_857_ & 0xff00ff) * i_852_ >> 8 & 0xff00ff) + ((i_857_ & 0xff00) * i_852_ >> 8 & 0xff00));
								}
								f += f_756_;
							} while (--i_753_ > 0);
						}
						i_753_ = i_755_ - i_754_ & 0x3;
						if (i_753_ > 0) {
							i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
							i_752_ = (((i_752_ & 0xff00ff) * i_853_ >> 8 & 0xff00ff) + ((i_752_ & 0xff00) * i_853_ >> 8 & 0xff00));
							do {
								if (f < fs[++i]) {
									int i_858_ = is[i];
									is[i] = (i_752_ + ((i_858_ & 0xff00ff) * i_852_ >> 8 & 0xff00ff) + ((i_858_ & 0xff00) * i_852_ >> 8 & 0xff00));
								}
								f += f_756_;
							} while (--i_753_ > 0);
						}
					} else {
						if (i_753_ > 0) {
							do {
								i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff);
								f_757_ += f_758_;
								f_759_ += f_760_;
								f_761_ += f_762_;
								if (f < fs[++i]) {
									int[] is_859_ = is;
									int i_860_ = i;
									int i_861_ = i_752_;
									int i_862_ = is_859_[i_860_];
									int i_863_ = i_861_ + i_862_;
									int i_864_ = ((i_861_ & 0xff00ff) + (i_862_ & 0xff00ff));
									i_862_ = ((i_864_ & 0x1000100) + (i_863_ - i_864_ & 0x10000));
									is_859_[i_860_] = (i_863_ - i_862_ | ~0xffffff | i_862_ - (i_862_ >>> 8));
								}
								f += f_756_;
								if (f < fs[++i]) {
									int[] is_865_ = is;
									int i_866_ = i;
									int i_867_ = i_752_;
									int i_868_ = is_865_[i_866_];
									int i_869_ = i_867_ + i_868_;
									int i_870_ = ((i_867_ & 0xff00ff) + (i_868_ & 0xff00ff));
									i_868_ = ((i_870_ & 0x1000100) + (i_869_ - i_870_ & 0x10000));
									is_865_[i_866_] = (i_869_ - i_868_ | ~0xffffff | i_868_ - (i_868_ >>> 8));
								}
								f += f_756_;
								if (f < fs[++i]) {
									int[] is_871_ = is;
									int i_872_ = i;
									int i_873_ = i_752_;
									int i_874_ = is_871_[i_872_];
									int i_875_ = i_873_ + i_874_;
									int i_876_ = ((i_873_ & 0xff00ff) + (i_874_ & 0xff00ff));
									i_874_ = ((i_876_ & 0x1000100) + (i_875_ - i_876_ & 0x10000));
									is_871_[i_872_] = (i_875_ - i_874_ | ~0xffffff | i_874_ - (i_874_ >>> 8));
								}
								f += f_756_;
								if (f < fs[++i]) {
									int[] is_877_ = is;
									int i_878_ = i;
									int i_879_ = i_752_;
									int i_880_ = is_877_[i_878_];
									int i_881_ = i_879_ + i_880_;
									int i_882_ = ((i_879_ & 0xff00ff) + (i_880_ & 0xff00ff));
									i_880_ = ((i_882_ & 0x1000100) + (i_881_ - i_882_ & 0x10000));
									is_877_[i_878_] = (i_881_ - i_880_ | ~0xffffff | i_880_ - (i_880_ >>> 8));
								}
								f += f_756_;
							} while (--i_753_ > 0);
						}
						i_753_ = i_755_ - i_754_ & 0x3;
						if (i_753_ > 0) {
							i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff);
							do {
								if (f < fs[++i]) {
									int[] is_883_ = is;
									int i_884_ = i;
									int i_885_ = i_752_;
									int i_886_ = is_883_[i_884_];
									int i_887_ = i_885_ + i_886_;
									int i_888_ = ((i_885_ & 0xff00ff) + (i_886_ & 0xff00ff));
									i_886_ = ((i_888_ & 0x1000100) + (i_887_ - i_888_ & 0x10000));
									is_883_[i_884_] = (i_887_ - i_886_ | ~0xffffff | i_886_ - (i_886_ >>> 8));
								}
								f += f_756_;
							} while (--i_753_ > 0);
						}
					}
				} else {
					i_753_ = i_755_ - i_754_;
					if (anInt108 == 0) {
						do {
							if (f < fs[++i])
								is[i] = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
							f += f_756_;
							f_757_ += f_758_;
							f_759_ += f_760_;
							f_761_ += f_762_;
						} while (--i_753_ > 0);
					} else if (!aBoolean106) {
						int i_889_ = anInt108;
						int i_890_ = 256 - anInt108;
						do {
							if (f < fs[++i]) {
								i_752_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff | ~0xffffff);
								i_752_ = (((i_752_ & 0xff00ff) * i_890_ >> 8 & 0xff00ff) + ((i_752_ & 0xff00) * i_890_ >> 8 & 0xff00));
								int i_891_ = is[i];
								is[i] = (i_752_ + ((i_891_ & 0xff00ff) * i_889_ >> 8 & 0xff00ff) + ((i_891_ & 0xff00) * i_889_ >> 8 & 0xff00));
							}
							f += f_756_;
							f_757_ += f_758_;
							f_759_ += f_760_;
							f_761_ += f_762_;
						} while (--i_753_ > 0);
					} else {
						do {
							if (f < fs[++i]) {
								int[] is_892_ = is;
								int i_893_ = i;
								int i_894_ = ((int) f_757_ & 0xff0000 | (int) f_759_ & 0xff00 | (int) f_761_ & 0xff);
								int i_895_ = is_892_[i_893_];
								int i_896_ = i_894_ + i_895_;
								int i_897_ = ((i_894_ & 0xff00ff) + (i_895_ & 0xff00ff));
								i_895_ = (i_897_ & 0x1000100) + (i_896_ - i_897_ & 0x10000);
								is_892_[i_893_] = (i_896_ - i_895_ | ~0xffffff | i_895_ - (i_895_ >>> 8));
							}
							f += f_756_;
							f_757_ += f_758_;
							f_759_ += f_760_;
							f_761_ += f_762_;
						} while (--i_753_ > 0);
					}
				}
			}
		}
	}

	Class11(ha_Sub2 var_ha_Sub2, Class240 class240) {
		aBoolean103 = false;
		anInt108 = 0;
		aBoolean110 = false;
		anIntArray111 = null;
		anInt113 = 0;
		aFloat114 = 0.0F;
		anInt115 = 0;
		anIntArray116 = null;
		aBoolean119 = true;
		anInt117 = 0;
		anInt112 = 0;
		aFloat122 = 0.0F;
		anInt126 = -1;
		anInt120 = 0;
		anInt124 = -1;
		anInt121 = -1;
		anIntArray123 = null;
		anInt127 = 0;
		anInt118 = 0;
		aFloat128 = 0.0F;
		aHa_Sub2_96 = var_ha_Sub2;
		aClass240_104 = class240;
		anInt98 = aHa_Sub2_96.anInt4084;
		anIntArray99 = aHa_Sub2_96.anIntArray4108;
		aFloatArray105 = aHa_Sub2_96.aFloatArray4074;
	}
}
