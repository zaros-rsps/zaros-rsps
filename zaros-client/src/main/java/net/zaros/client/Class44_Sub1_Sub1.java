package net.zaros.client;

/* Class44_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class44_Sub1_Sub1 extends Class44_Sub1 {
	int anInt5808;
	static Class404 aClass404_5809 = new Class404();
	static OutgoingPacket aClass311_5810 = new OutgoingPacket(49, 7);
	static Sprite[] aClass397Array5811;

	static final void method576(byte i, MidiDecoder class296_sub47) {
		Class235.aClass296_Sub45_Sub4_2229.method3014(class296_sub47, false, true);
		if (i >= -77)
			aClass397Array5811 = null;
		if (Class296_Sub39_Sub9.aClass381_6163 != null)
			Class296_Sub39_Sub9.aClass381_6163.method3996(115, Class235.aClass296_Sub45_Sub4_2229);
		SubInPacket.aClass296_Sub47_2421 = null;
		Class249.aClass296_Sub45_Sub4_2357 = null;
		TextureOperation.aClass138_5040 = null;
		za.aClass25_5085 = null;
		ReferenceWrapper.anInt6128 = 0;
	}

	public static void method577(byte i) {
		if (i != 35)
			aClass397Array5811 = null;
		aClass311_5810 = null;
		aClass404_5809 = null;
		aClass397Array5811 = null;
	}

	Class44_Sub1_Sub1(Class338_Sub3 class338_sub3) {
		super(class338_sub3, false);
	}
}
