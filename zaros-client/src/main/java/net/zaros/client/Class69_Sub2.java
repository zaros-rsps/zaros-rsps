package net.zaros.client;
import jaggl.OpenGL;

final class Class69_Sub2 extends Class69 {
	private int anInt5716;
	int anInt5717;
	int anInt5718;
	static Class252 aClass252_5719;
	private int anInt5720 = -1;
	int anInt5721;

	Class69_Sub2(ha_Sub3 var_ha_Sub3, int i, int i_0_, int i_1_, int i_2_, byte[] is, int i_3_) {
		super(var_ha_Sub3, 32879, i, i_0_ * (i_1_ * i_2_), false);
		anInt5716 = -1;
		anInt5721 = i_2_;
		anInt5718 = i_0_;
		anInt5717 = i_1_;
		aHa_Sub3_3681.method1316(this, (byte) -112);
		OpenGL.glPixelStorei(3317, 1);
		OpenGL.glTexImage3Dub(anInt3682, 0, anInt3685, anInt5718, anInt5717, anInt5721, 0, i_3_, 5121, is, 0);
		OpenGL.glPixelStorei(3317, 4);
		this.method723(91, true);
	}

	static final void method737(int i, Class130 class130) {
		if (i <= 39)
			aClass252_5719 = null;
		HashTable.aClass130_2463 = class130;
	}

	final void method738(int i, int i_4_, int i_5_, int i_6_, boolean bool, int i_7_, int i_8_, int i_9_) {
		aHa_Sub3_3681.method1316(this, (byte) -124);
		if (bool)
			method737(81, null);
		OpenGL.glCopyTexSubImage3D(anInt3682, 0, i_7_, i, i_5_, i_9_, i_4_, i_6_, i_8_);
		OpenGL.glFlush();
	}

	Class69_Sub2(ha_Sub3 var_ha_Sub3, int i, int i_10_, int i_11_, int i_12_) {
		super(var_ha_Sub3, 32879, i, i_12_ * (i_10_ * i_11_), false);
		anInt5716 = -1;
		anInt5718 = i_10_;
		anInt5721 = i_12_;
		anInt5717 = i_11_;
		aHa_Sub3_3681.method1316(this, (byte) -119);
		OpenGL.glTexImage3Dub(anInt3682, 0, anInt3685, anInt5718, anInt5717, anInt5721, 0, Class296_Sub39_Sub18.method2897((byte) 61, anInt3685), 5121, null, 0);
		this.method723(120, true);
	}

	public static void method739(int i) {
		if (i != 0)
			method739(-83);
		aClass252_5719 = null;
	}

	public final void method73(boolean bool) {
		OpenGL.glFramebufferTexture3DEXT(anInt5716, anInt5720, anInt3682, 0, 0, 0);
		anInt5716 = -1;
		anInt5720 = -1;
		if (bool != true)
			method740(-54);
	}

	static final void method740(int i) {
		Class41_Sub28.setMapSize(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987.method402(i + 111));
		int i_13_ = (Class206.worldBaseX >> 3) + (Class219.camPosX >> 12);
		FileWorker.anInt3005 = Class296_Sub51_Sub11.localPlayer.z = (byte) 0;
		int i_14_ = (Class124.camPosZ >> 12) + (Class41_Sub26.worldBaseY >> 3);
		Class296_Sub51_Sub11.localPlayer.setPosition(8, 8);
		int i_15_ = 18;
		Class296_Sub43.anIntArray4940 = new int[i_15_];
		Class296_Sub51_Sub31.aByteArrayArray6509 = new byte[i_15_][];
		Class355_Sub2.xteaKeys = new int[i_15_][i];
		Class188.anIntArray1924 = new int[i_15_];
		StaticMethods.aByteArrayArray3167 = new byte[i_15_][];
		Class243.anIntArray2317 = new int[i_15_];
		Class166.aByteArrayArray1691 = new byte[i_15_][];
		Class107_Sub1.aByteArrayArray5667 = new byte[i_15_][];
		StaticMethods.anIntArray1844 = new int[i_15_];
		ParticleEmitterRaw.aByteArrayArray1772 = new byte[i_15_][];
		Class4.anIntArray70 = new int[i_15_];
		Class56.anIntArray659 = new int[i_15_];
		i_15_ = 0;
		for (int i_16_ = (-(Class198.currentMapSizeX >> 4) + i_13_) / 8; i_16_ <= (i_13_ + (Class198.currentMapSizeX >> 4)) / 8; i_16_++) {
			for (int i_17_ = (-(Class296_Sub38.currentMapSizeY >> 4) + i_14_) / 8; i_17_ <= (i_14_ + (Class296_Sub38.currentMapSizeY >> 4)) / 8; i_17_++) {
				int i_18_ = i_17_ + (i_16_ << 8);
				Class296_Sub43.anIntArray4940[i_15_] = i_18_;
				Class56.anIntArray659[i_15_] = Class324.fs5.getFileIndex("m" + i_16_ + "_" + i_17_);
				Class4.anIntArray70[i_15_] = Class324.fs5.getFileIndex("l" + i_16_ + "_" + i_17_);
				Class243.anIntArray2317[i_15_] = (Class324.fs5.getFileIndex("n" + i_16_ + "_" + i_17_));
				Class188.anIntArray1924[i_15_] = (Class324.fs5.getFileIndex("um" + i_16_ + "_" + i_17_));
				StaticMethods.anIntArray1844[i_15_] = Class324.fs5.getFileIndex("ul" + i_16_ + "_" + i_17_);
				if (Class243.anIntArray2317[i_15_] == -1) {
					Class56.anIntArray659[i_15_] = -1;
					Class4.anIntArray70[i_15_] = -1;
					Class188.anIntArray1924[i_15_] = -1;
					StaticMethods.anIntArray1844[i_15_] = -1;
				}
				i_15_++;
			}
		}
		for (int i_19_ = i_15_; Class243.anIntArray2317.length > i_19_; i_19_++) {
			Class243.anIntArray2317[i_19_] = -1;
			Class56.anIntArray659[i_19_] = -1;
			Class4.anIntArray70[i_19_] = -1;
			Class188.anIntArray1924[i_19_] = -1;
			StaticMethods.anIntArray1844[i_19_] = -1;
		}
		int i_20_;
		if (Class366_Sub6.anInt5392 == 3)
			i_20_ = 4;
		else if (Class366_Sub6.anInt5392 == 9)
			i_20_ = 10;
		else {
			if (Class366_Sub6.anInt5392 != 7)
				throw new RuntimeException(String.valueOf(Class366_Sub6.anInt5392));
			i_20_ = 8;
		}
		StaticMethods.method1010(i ^ 0x4, i_20_, i_14_, false, i_13_);
	}
}
