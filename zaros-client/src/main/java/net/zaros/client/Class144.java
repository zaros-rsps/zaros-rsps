package net.zaros.client;

/* Class144 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class144 {
	static IncomingPacket aClass231_1433 = new IncomingPacket(141, -2);
	static Class209 aClass209_1434 = new Class209(30);
	static Class95[] aClass95Array1435 = new Class95[16];

	static final Class338_Sub2 method1481(int i, boolean bool, boolean bool_0_) {
		synchronized (Class368.aClass404Array3129) {
			Class338_Sub2 class338_sub2;
			if (i >= Class368.aClass404Array3129.length || Class368.aClass404Array3129[i].method4166((byte) -90)) {
				class338_sub2 = new Class338_Sub2();
				class338_sub2.aClass338_Sub5Array5194 = new Class338_Sub5[i];
				for (int i_1_ = 0; i > i_1_; i_1_++)
					class338_sub2.aClass338_Sub5Array5194[i_1_] = new Class338_Sub5();
			} else {
				class338_sub2 = (Class338_Sub2) Class368.aClass404Array3129[i].method4169((byte) -70);
				class338_sub2.method3438(!bool);
				Class69_Sub1_Sub1.anIntArray6686[i]--;
			}
			if (bool != true)
				method1483(true, 76);
			class338_sub2.aBoolean5196 = bool_0_;
			return class338_sub2;
		}
	}

	public static void method1482(int i) {
		aClass209_1434 = null;
		if (i <= -111) {
			aClass231_1433 = null;
			aClass95Array1435 = null;
		}
	}

	static final void method1483(boolean bool, int i) {
		if (Class355_Sub2.aString3503.length() != 0) {
			if (i > -29)
				aClass95Array1435 = null;
			Class41_Sub18.writeConsoleMessage("--> " + Class355_Sub2.aString3503);
			Class41_Sub28.method513(Class355_Sub2.aString3503, false, bool, -86);
			if (!bool) {
				OutputStream_Sub2.anInt44 = 0;
				Class355_Sub2.aString3503 = "";
			}
			Class296_Sub51_Sub9.anInt6389 = 0;
		}
	}

	abstract Class296_Sub39_Sub15 method1484(Class296_Sub39_Sub15 class296_sub39_sub15, int i);
}
