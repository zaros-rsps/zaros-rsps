package net.zaros.client;

/* Class313 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class313 {
	private Queue aClass145_2774 = new Queue();
	private int anInt2775;
	static Class209 aClass209_2776 = new Class209(0);
	private int anInt2777;
	private HashTable aClass263_2778;
	static int anInt2779;
	static int anInt2780 = 0;

	public int method3307(byte i) {
		if (i > -58) {
			return 87;
		}
		return anInt2777;
	}

	public static void method3308(byte i) {
		aClass209_2776 = null;
		if (i < 64) {
			anInt2779 = -64;
		}
	}

	public void clear() {
		aClass145_2774.clear();
		aClass263_2778.clear();
		anInt2777 = anInt2775;
	}

	private void method3310(int i, Interface12 interface12) {
		long l = interface12.method51((byte) 77);
		for (Class296_Sub39_Sub15 class296_sub39_sub15 = (Class296_Sub39_Sub15) aClass263_2778.get(l); class296_sub39_sub15 != null; class296_sub39_sub15 = (Class296_Sub39_Sub15) aClass263_2778.getUnknown(false)) {
			if (class296_sub39_sub15.anInterface12_6225.method50((byte) -34, interface12)) {
				method3318(class296_sub39_sub15, i ^ 0x1);
				break;
			}
		}
		if (i != 0) {
			method3315(67, null, 6, null);
		}
	}

	public Object method3312(Interface12 interface12, int i) {
		long l = interface12.method51((byte) 105);
		for (Class296_Sub39_Sub15 class296_sub39_sub15 = (Class296_Sub39_Sub15) aClass263_2778.get(l); class296_sub39_sub15 != null; class296_sub39_sub15 = (Class296_Sub39_Sub15) aClass263_2778.getUnknown(false)) {
			if (class296_sub39_sub15.anInterface12_6225.method50((byte) 111, interface12)) {
				Object object = class296_sub39_sub15.method2883((byte) -117);
				if (object == null) {
					class296_sub39_sub15.unlink();
					class296_sub39_sub15.queue_unlink();
					anInt2777 += class296_sub39_sub15.anInt6226;
				} else {
					if (class296_sub39_sub15.method2882(-3913)) {
						Class296_Sub39_Sub15_Sub1 class296_sub39_sub15_sub1 = new Class296_Sub39_Sub15_Sub1(interface12, object, class296_sub39_sub15.anInt6226);
						aClass263_2778.put(class296_sub39_sub15.uid, class296_sub39_sub15_sub1);
						aClass145_2774.insert(class296_sub39_sub15_sub1, -2);
						class296_sub39_sub15_sub1.queue_uid = 0L;
						class296_sub39_sub15.unlink();
						class296_sub39_sub15.queue_unlink();
					} else {
						aClass145_2774.insert(class296_sub39_sub15, i ^ ~0x1);
						class296_sub39_sub15.queue_uid = 0L;
					}
					return object;
				}
			}
		}
		if (i != 0) {
			method3318(null, 97);
		}
		return null;
	}

	public void clean(int i) {
		if (Class2.aClass144_65 != null) {
			for (Class296_Sub39_Sub15 class296_sub39_sub15 = (Class296_Sub39_Sub15) aClass145_2774.getFront(); class296_sub39_sub15 != null; class296_sub39_sub15 = (Class296_Sub39_Sub15) aClass145_2774.getNext()) {
				if (!class296_sub39_sub15.method2882(-3913)) {
					if (++class296_sub39_sub15.queue_uid > i) {
						Class296_Sub39_Sub15 class296_sub39_sub15_5_ = Class2.aClass144_65.method1484(class296_sub39_sub15, 0);
						aClass263_2778.put(class296_sub39_sub15.uid, class296_sub39_sub15_5_);
						Class296_Sub15_Sub3.insertNode(class296_sub39_sub15_5_, 825, class296_sub39_sub15);
						class296_sub39_sub15.unlink();
						class296_sub39_sub15.queue_unlink();
					}
				} else if (class296_sub39_sub15.method2883((byte) -90) == null) {
					class296_sub39_sub15.unlink();
					class296_sub39_sub15.queue_unlink();
					anInt2777 += class296_sub39_sub15.anInt6226;
				}
			}
		}
	}

	public int method3314(int i) {
		if (i != -1) {
			anInt2780 = 123;
		}
		return anInt2775;
	}

	private void method3315(int i, Interface12 interface12, int i_6_, Object object) {
		if (anInt2775 < i) {
			throw new IllegalStateException("s>cs");
		}
		method3310(0, interface12);
		anInt2777 -= i;
		while (anInt2777 < 0) {
			Class296_Sub39_Sub15 class296_sub39_sub15 = (Class296_Sub39_Sub15) aClass145_2774.remove();
			method3318(class296_sub39_sub15, 1);
		}
		if (i_6_ != 27891) {
			aClass263_2778 = null;
		}
		Class296_Sub39_Sub15_Sub1 class296_sub39_sub15_sub1 = new Class296_Sub39_Sub15_Sub1(interface12, object, i);
		aClass263_2778.put(interface12.method51((byte) -122), class296_sub39_sub15_sub1);
		aClass145_2774.insert(class296_sub39_sub15_sub1, -2);
		class296_sub39_sub15_sub1.queue_uid = 0L;
	}

	public void method3316(boolean bool) {
		for (Class296_Sub39_Sub15 class296_sub39_sub15 = (Class296_Sub39_Sub15) aClass145_2774.getFront(); class296_sub39_sub15 != null; class296_sub39_sub15 = (Class296_Sub39_Sub15) aClass145_2774.getNext()) {
			if (class296_sub39_sub15.method2882(-3913)) {
				class296_sub39_sub15.unlink();
				class296_sub39_sub15.queue_unlink();
				anInt2777 += class296_sub39_sub15.anInt6226;
			}
		}
		if (bool) {
			anInt2780 = -39;
		}
	}

	public void method3317(Object object, int i, Interface12 interface12) {
		method3315(1, interface12, 27891, object);
		int i_7_ = 104 % ((i - 31) / 55);
	}

	private void method3318(Class296_Sub39_Sub15 class296_sub39_sub15, int i) {
		if (i != 1) {
			anInt2777 = 9;
		}
		if (class296_sub39_sub15 != null) {
			class296_sub39_sub15.unlink();
			class296_sub39_sub15.queue_unlink();
			anInt2777 += class296_sub39_sub15.anInt6226;
		}
	}

	public Class313(int i) {
		anInt2775 = i;
		anInt2777 = i;
		int i_8_;
		for (i_8_ = 1; i > i_8_ + i_8_; i_8_ += i_8_) {
			/* empty */
		}
		aClass263_2778 = new HashTable(i_8_);
	}
}
