package net.zaros.client;

/* Class368_Sub8 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub8 extends Class368 {
	static byte[] aByteArray5465;
	private int anInt5466;
	private int anInt5467;
	private int anInt5468;
	private int anInt5469;
	static int[] npcsToRemoveIndexes = new int[1000];

	static final boolean method3836(int i, int i_0_, int i_1_) {
		if (i_1_ != -1)
			return false;
		if (!Class30.method328(i_0_, i, i_1_))
			return false;
		if (Class315.method3325((byte) 113, i, i_0_) | (i_0_ & 0xb000) != 0 | Class296_Sub51_Sub5.method3092(i_0_, -24276, i))
			return true;
		return ((Class205_Sub2.method1982(i_1_ + 545, i, i_0_) | ParamTypeList.method1657((byte) -47, i, i_0_)) & (i & 0x37) == 0);
	}

	public static void method3837(byte i) {
		aByteArray5465 = null;
		int i_2_ = 54 / ((i + 22) / 60);
		npcsToRemoveIndexes = null;
	}

	final void method3807(byte i) {
		int i_3_ = -68 / ((52 - i) / 52);
		Class296_Sub51_Sub38.method3194(-91, anInt5467, anInt5466, anInt5468, anInt5469, 0);
	}

	Class368_Sub8(Packet class296_sub17) {
		super(class296_sub17);
		anInt5467 = class296_sub17.g2();
		anInt5468 = class296_sub17.g1();
		anInt5466 = class296_sub17.g1();
		anInt5469 = class296_sub17.g1();
	}

	static final void method3838(boolean bool, int i, int i_4_, int i_5_, int i_6_) {
		if (ConfigsRegister.anInt3674 == 1) {
			int i_7_ = i / Class290.anInt2656;
			int i_8_ = i_6_ / Class290.anInt2656;
			int i_9_ = i_4_ / Class395.anInt3317;
			int i_10_ = i_5_ / Class395.anInt3317;
			if (i_7_ < Class317.anInt3705 && i_8_ >= 0 && Applet_Sub1.anInt7 > i_9_ && i_10_ >= 0) {
				if (i_7_ < 0)
					i_7_ = 0;
				if (Class317.anInt3705 <= i_8_)
					i_8_ = Class317.anInt3705 - 1;
				if (i_9_ < 0)
					i_9_ = 0;
				if (i_10_ >= Applet_Sub1.anInt7)
					i_10_ = Applet_Sub1.anInt7 - 1;
				for (int i_11_ = i_9_; i_11_ <= i_10_; i_11_++) {
					int i_12_ = (Class296_Sub28.method2683((byte) 68, (i_11_ + Class404.anInt3385), Applet_Sub1.anInt7) * Class317.anInt3705);
					for (int i_13_ = i_7_; i_13_ <= i_8_; i_13_++) {
						int i_14_ = (Class296_Sub28.method2683((byte) 80, (Class292.anInt2665 + i_13_), Class317.anInt3705) + i_12_);
						BillboardRaw.anIntArray1080[i_14_] = Class41_Sub20.anInt3795;
					}
				}
				if (bool)
					aByteArray5465 = null;
			}
		}
	}

	static {
		int i = 0;
		aByteArray5465 = new byte[32896];
		for (int i_15_ = 0; i_15_ < 256; i_15_++) {
			for (int i_16_ = 0; i_16_ <= i_15_; i_16_++)
				aByteArray5465[i++] = (byte) (int) (255.0 / Math.sqrt((double) ((float) ((i_16_ * i_16_) + ((i_15_ * i_15_) + 65535)) / 65535.0F)));
		}
	}
}
