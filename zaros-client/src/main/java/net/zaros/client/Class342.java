package net.zaros.client;
import jaggl.OpenGL;

final class Class342 {
	private Class392 aClass392_2980;
	private int anInt2981;
	private int anInt2982 = 0;
	private boolean aBoolean2983;
	private NodeDeque aClass155_2984;
	private int anInt2985;
	static IncomingPacket aClass231_2986;
	private Class392 aClass392_2987;
	private ha_Sub3 aHa_Sub3_2988;
	static int anInt2989;
	static boolean aBoolean2990 = false;
	private Class392 aClass392_2991;
	private int anInt2992;
	private boolean aBoolean2993;
	private Class69_Sub1 aClass69_Sub1_2994;
	private int anInt2995;
	private boolean aBoolean2996;
	private boolean aBoolean2997;
	private boolean aBoolean2998;
	private Class296_Sub39_Sub4 aClass296_Sub39_Sub4_2999;
	private Class296_Sub39_Sub4 aClass296_Sub39_Sub4_3000;
	private boolean aBoolean3001;
	private int anInt3002;
	private Class69_Sub1[] aClass69_Sub1Array3003;

	public static void method3628(int i) {
		aClass231_2986 = null;
		if (i > -10)
			anInt2989 = 42;
	}

	final void method3629(int i) {
		if (aBoolean2983) {
			if (aClass392_2987 != null) {
				int i_0_ = 16384;
				aHa_Sub3_2988.method1335(0, aClass392_2987);
				aHa_Sub3_2988.method1307((byte) -39, aClass392_2991);
				aClass392_2987.method4051(0, (byte) 64);
				aClass392_2991.method4045(0, 0);
				if (aBoolean2997)
					i_0_ |= 0x100;
				OpenGL.glBlitFramebufferEXT(0, 0, anInt2992, anInt2985, 0, 0, anInt2992, anInt2985, i_0_, 9728);
				aHa_Sub3_2988.method1353(-1, aClass392_2987);
				aHa_Sub3_2988.method1333(aClass392_2991, (byte) -12);
			}
			aHa_Sub3_2988.method1305((byte) 112);
			aHa_Sub3_2988.method1336((byte) -104, 0);
			aHa_Sub3_2988.method1272((byte) -107, 1);
			aHa_Sub3_2988.la();
			int i_1_ = 0;
			int i_2_ = 1;
			int i_3_ = -128 / ((i - 36) / 56);
			Class296_Sub9 class296_sub9;
			for (Class296_Sub9 class296_sub9_4_ = (Class296_Sub9) aClass155_2984.removeFirst((byte) 124); class296_sub9_4_ != null; class296_sub9_4_ = class296_sub9) {
				class296_sub9 = (Class296_Sub9) aClass155_2984.removeNext(1001);
				int i_5_ = class296_sub9_4_.method2486(37);
				for (int i_6_ = 0; i_6_ < i_5_; i_6_++) {
					class296_sub9_4_.method2480((byte) 125, aClass69_Sub1_2994, i_6_, aClass69_Sub1Array3003[i_1_]);
					if (class296_sub9 == null && i_5_ - 1 == i_6_) {
						aHa_Sub3_2988.method1280(aClass392_2991, -1);
						aHa_Sub3_2988.method1314(0, 1, 0);
						OpenGL.glBegin(7);
						OpenGL.glTexCoord2f(0.0F, (float) anInt2985);
						OpenGL.glMultiTexCoord2f(33985, 0.0F, 1.0F);
						OpenGL.glVertex2i(anInt2981, anInt2982);
						OpenGL.glTexCoord2f(0.0F, 0.0F);
						OpenGL.glMultiTexCoord2f(33985, 0.0F, 0.0F);
						OpenGL.glVertex2i(anInt2981, anInt2982 + anInt2985);
						OpenGL.glTexCoord2f((float) anInt2992, 0.0F);
						OpenGL.glMultiTexCoord2f(33985, 1.0F, 0.0F);
						OpenGL.glVertex2i(anInt2981 + anInt2992, anInt2985 + anInt2982);
						OpenGL.glTexCoord2f((float) anInt2992, (float) anInt2985);
						OpenGL.glMultiTexCoord2f(33985, 1.0F, 1.0F);
						OpenGL.glVertex2i(anInt2981 + anInt2992, anInt2982);
						OpenGL.glEnd();
					} else {
						aClass392_2991.method4045(0, i_2_);
						OpenGL.glBegin(7);
						OpenGL.glTexCoord2f(0.0F, (float) anInt2985);
						OpenGL.glMultiTexCoord2f(33985, 0.0F, 1.0F);
						OpenGL.glVertex2i(0, 0);
						OpenGL.glTexCoord2f(0.0F, 0.0F);
						OpenGL.glMultiTexCoord2f(33985, 0.0F, 0.0F);
						OpenGL.glVertex2i(0, anInt2985);
						OpenGL.glTexCoord2f((float) anInt2992, 0.0F);
						OpenGL.glMultiTexCoord2f(33985, 1.0F, 0.0F);
						OpenGL.glVertex2i(anInt2992, anInt2985);
						OpenGL.glTexCoord2f((float) anInt2992, (float) anInt2985);
						OpenGL.glMultiTexCoord2f(33985, 1.0F, 1.0F);
						OpenGL.glVertex2i(anInt2992, 0);
						OpenGL.glEnd();
					}
					i_2_ = i_2_ + 1 & 0x1;
					class296_sub9_4_.method2484(101, i_6_);
					i_1_ = i_1_ + 1 & 0x1;
				}
			}
			aBoolean2983 = false;
		}
	}

	final void method3630(Class296_Sub9 class296_sub9, byte i) {
		class296_sub9.aBoolean4633 = false;
		int i_7_ = 17 % ((i - 62) / 43);
		class296_sub9.method2482((byte) -115);
		class296_sub9.unlink();
		method3631(-114);
	}

	private final void method3631(int i) {
		boolean bool = false;
		int i_8_ = 0;
		int i_9_ = 0;
		Class296_Sub9 class296_sub9 = (Class296_Sub9) aClass155_2984.removeFirst((byte) 125);
		int i_10_ = 40 % ((-54 - i) / 36);
		for (/**/; class296_sub9 != null; class296_sub9 = (Class296_Sub9) aClass155_2984.removeNext(1001)) {
			int i_11_ = class296_sub9.method2485(256);
			if (i_8_ < i_11_)
				i_8_ = i_11_;
			i_9_ += class296_sub9.method2486(-120);
			bool |= class296_sub9.method2490(63);
		}
		int i_12_;
		if (i_8_ == 2)
			i_12_ = 34836;
		else if (i_8_ == 1)
			i_12_ = 34842;
		else
			i_12_ = 6408;
		if (i_12_ != anInt2995) {
			aBoolean3001 = true;
			anInt2995 = i_12_;
		}
		int i_13_ = anInt3002 > 2 ? 2 : anInt3002;
		int i_14_ = i_9_ > 2 ? 2 : i_9_;
		if (i_13_ != i_14_)
			aBoolean2996 = aBoolean3001 = true;
		if (aBoolean2997 != bool) {
			aBoolean2993 = true;
			aBoolean2997 = bool;
		}
		anInt3002 = i_9_;
	}

	final boolean method3632(byte i) {
		if (i >= -75)
			method3630(null, (byte) 61);
		if (aClass392_2980 == null)
			return false;
		return true;
	}

	private final boolean method3633(boolean bool) {
		if (aBoolean2993) {
			if (aClass296_Sub39_Sub4_2999 != null) {
				aClass296_Sub39_Sub4_2999.method2801(0);
				aClass296_Sub39_Sub4_2999 = null;
			}
			if (aClass69_Sub1_2994 != null) {
				aClass69_Sub1_2994.method726(0);
				aClass69_Sub1_2994 = null;
			}
			if (aClass392_2987 != null)
				aClass296_Sub39_Sub4_2999 = new Class296_Sub39_Sub4(aHa_Sub3_2988, 6402, anInt2992, anInt2985, aHa_Sub3_2988.anInt4128);
			if (aBoolean2997)
				aClass69_Sub1_2994 = new Class69_Sub1(aHa_Sub3_2988, 34037, 6402, anInt2992, anInt2985);
			else if (aClass296_Sub39_Sub4_2999 == null)
				aClass296_Sub39_Sub4_2999 = new Class296_Sub39_Sub4(aHa_Sub3_2988, 6402, anInt2992, anInt2985);
			aBoolean2993 = false;
			aBoolean2998 = true;
			aBoolean2996 = true;
		}
		if (aBoolean3001) {
			if (aClass296_Sub39_Sub4_3000 != null) {
				aClass296_Sub39_Sub4_3000.method2801(0);
				aClass296_Sub39_Sub4_3000 = null;
			}
			if (aClass69_Sub1Array3003[0] != null) {
				aClass69_Sub1Array3003[0].method726(0);
				aClass69_Sub1Array3003[0] = null;
			}
			if (aClass69_Sub1Array3003[1] != null) {
				aClass69_Sub1Array3003[1].method726(0);
				aClass69_Sub1Array3003[1] = null;
			}
			if (aClass392_2987 != null)
				aClass296_Sub39_Sub4_3000 = new Class296_Sub39_Sub4(aHa_Sub3_2988, anInt2995, anInt2992, anInt2985, aHa_Sub3_2988.anInt4128);
			aClass69_Sub1Array3003[0] = new Class69_Sub1(aHa_Sub3_2988, 34037, anInt2995, anInt2992, anInt2985);
			aClass69_Sub1Array3003[1] = anInt3002 > 1 ? new Class69_Sub1(aHa_Sub3_2988, 34037, anInt2995, anInt2992, anInt2985) : null;
			aBoolean3001 = false;
			aBoolean2998 = true;
			aBoolean2996 = true;
		}
		if (aBoolean2996) {
			if (aClass392_2987 != null) {
				aHa_Sub3_2988.method1351(aClass392_2991, (byte) -27);
				aClass392_2991.method4047((byte) -56, 0);
				aClass392_2991.method4047((byte) -127, 1);
				aClass392_2991.method4047((byte) -106, 8);
				aClass392_2991.method4050(aClass69_Sub1Array3003[0], 0, 0);
				if (anInt3002 > 1)
					aClass392_2991.method4050(aClass69_Sub1Array3003[1], 1, 0);
				if (aBoolean2997)
					aClass392_2991.method4050(aClass69_Sub1_2994, 8, 0);
				aHa_Sub3_2988.method1280(aClass392_2991, -1);
				aHa_Sub3_2988.method1351(aClass392_2987, (byte) -27);
				aClass392_2987.method4047((byte) -51, 0);
				aClass392_2987.method4047((byte) -60, 8);
				aClass392_2987.method4046((byte) 104, 0, aClass296_Sub39_Sub4_3000);
				aClass392_2987.method4046((byte) 105, 8, aClass296_Sub39_Sub4_2999);
				aHa_Sub3_2988.method1280(aClass392_2987, -1);
			} else {
				aHa_Sub3_2988.method1351(aClass392_2991, (byte) -27);
				aClass392_2991.method4047((byte) -118, 0);
				aClass392_2991.method4047((byte) -106, 1);
				aClass392_2991.method4047((byte) -71, 8);
				aClass392_2991.method4050(aClass69_Sub1Array3003[0], 0, 0);
				if (anInt3002 > 1)
					aClass392_2991.method4050(aClass69_Sub1Array3003[1], 1, 0);
				if (!aBoolean2997)
					aClass392_2991.method4046((byte) 105, 8, aClass296_Sub39_Sub4_2999);
				else
					aClass392_2991.method4050(aClass69_Sub1_2994, 8, 0);
				aHa_Sub3_2988.method1280(aClass392_2991, -1);
			}
			aBoolean2998 = true;
			aBoolean2996 = false;
		}
		if (aBoolean2998) {
			aHa_Sub3_2988.method1351(aClass392_2980, (byte) -27);
			aBoolean2998 = !aClass392_2980.method4053((byte) 118);
			aHa_Sub3_2988.method1280(aClass392_2980, -1);
		}
		if (bool != true)
			anInt2981 = 126;
		if (aBoolean2998)
			return false;
		return true;
	}

	final boolean method3634(int i, Class296_Sub9 class296_sub9) {
		if (aClass392_2980 != null) {
			if (class296_sub9.method2483(i ^ 0x5a) || class296_sub9.method2489((byte) -61)) {
				aClass155_2984.addLast((byte) 113, class296_sub9);
				method3631(49);
				if (method3633(true)) {
					if (anInt2992 != -1 && anInt2985 != -1)
						class296_sub9.method2487(anInt2992, anInt2985, false);
					class296_sub9.aBoolean4633 = true;
					return true;
				}
			}
			method3630(class296_sub9, (byte) -49);
		}
		if (i != 1)
			aHa_Sub3_2988 = null;
		return false;
	}

	final boolean method3635(int i, int i_15_, int i_16_, int i_17_, boolean bool) {
		if (aClass392_2980 == null || aClass155_2984.method1577((byte) -77))
			return false;
		if (bool)
			return false;
		if (anInt2992 != i_15_ || i_17_ != anInt2985) {
			anInt2985 = i_17_;
			anInt2992 = i_15_;
			for (Node class296 = aClass155_2984.removeFirst((byte) 116); aClass155_2984.aClass296_1586 != class296; class296 = class296.next)
				((Class296_Sub9) class296).method2487(anInt2992, anInt2985, false);
			aBoolean2996 = true;
			aBoolean2993 = true;
			aBoolean3001 = true;
		}
		if (method3633(true)) {
			aBoolean2983 = true;
			anInt2982 = i;
			anInt2981 = i_16_;
			aHa_Sub3_2988.method1351(aClass392_2980, (byte) -27);
			aClass392_2980.method4045(0, 0);
			aHa_Sub3_2988.method1314(-aHa_Sub3_2988.anInt4113 + (anInt2982 + anInt2985), 1, -anInt2981);
			return true;
		}
		return false;
	}

	final void method3636(byte i) {
		aClass69_Sub1Array3003 = null;
		aClass392_2980 = aClass392_2987 = aClass392_2991 = null;
		aClass296_Sub39_Sub4_2999 = null;
		aClass296_Sub39_Sub4_3000 = null;
		aClass69_Sub1_2994 = null;
		if (i == -112) {
			if (!aClass155_2984.method1577((byte) -77)) {
				for (Node class296 = aClass155_2984.removeFirst((byte) 123); class296 != aClass155_2984.aClass296_1586; class296 = class296.next)
					((Class296_Sub9) class296).method2482((byte) -122);
			}
			anInt2992 = anInt2985 = 1;
		}
	}

	Class342(ha_Sub3 var_ha_Sub3) {
		anInt2981 = 0;
		anInt2985 = 1;
		anInt2992 = 1;
		aClass155_2984 = new NodeDeque();
		anInt2995 = -1;
		aBoolean2993 = true;
		aBoolean2997 = false;
		aBoolean2998 = true;
		aBoolean3001 = true;
		anInt3002 = 0;
		aBoolean2996 = true;
		aClass69_Sub1Array3003 = new Class69_Sub1[2];
		aHa_Sub3_2988 = var_ha_Sub3;
		if (aHa_Sub3_2988.aBoolean4190 && aHa_Sub3_2988.aBoolean4257) {
			aClass392_2980 = aClass392_2991 = new Class392(aHa_Sub3_2988);
			if (aHa_Sub3_2988.anInt4128 > 1 && aHa_Sub3_2988.aBoolean4268 && aHa_Sub3_2988.aBoolean4240)
				aClass392_2980 = aClass392_2987 = new Class392(aHa_Sub3_2988);
		}
	}

	static {
		aClass231_2986 = new IncomingPacket(128, 0);
	}
}
