package net.zaros.client;

/* Class236 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class236 {
	int anInt2230;
	static boolean aBoolean2231 = true;
	private int anInt2232;
	boolean aBoolean2233 = true;
	int anInt2234;
	static int anInt2235 = 0;
	int anInt2236;
	int anInt2237;
	int anInt2238;
	boolean aBoolean2239;
	static IncomingPacket aClass231_2240 = new IncomingPacket(126, 0);
	int anInt2241;

	final void method2130(byte i, Packet class296_sub17) {
		for (;;) {
			int i_0_ = class296_sub17.g1();
			if (i_0_ == 0)
				break;
			method2133(class296_sub17, i_0_, 123);
		}
		if (i != 121) {
			/* empty */
		}
	}

	public static void method2131(int i) {
		int i_1_ = 112 / ((i + 9) / 57);
		aClass231_2240 = null;
	}

	static final boolean method2132(int i, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		int i_8_ = i_7_ + i_5_;
		int i_9_ = i_4_ + i_3_;
		int i_10_ = i + i_2_;
		if (!Class389.method4037(i_9_, i_5_, 2, i_9_, i, i_8_, i_9_, i_5_, i_10_, i_10_))
			return false;
		if (i_6_ != 0)
			return false;
		if (!Class389.method4037(i_9_, i_5_, 2, i_9_, i, i_8_, i_9_, i_8_, i, i_10_))
			return false;
		if (Class395.anInt3315 <= i_5_) {
			if (!Class389.method4037(i_9_, i_8_, 2, i_3_, i_10_, i_8_, i_9_, i_8_, i, i_10_))
				return false;
			if (!Class389.method4037(i_3_, i_8_, 2, i_3_, i_10_, i_8_, i_9_, i_8_, i, i))
				return false;
		} else {
			if (!Class389.method4037(i_9_, i_5_, 2, i_3_, i_10_, i_5_, i_9_, i_5_, i, i_10_))
				return false;
			if (!Class389.method4037(i_3_, i_5_, 2, i_3_, i_10_, i_5_, i_9_, i_5_, i, i))
				return false;
		}
		if (StaticMethods.anInt5942 <= i) {
			if (!Class389.method4037(i_9_, i_5_, 2, i_3_, i_10_, i_8_, i_9_, i_5_, i_10_, i_10_))
				return false;
			if (!Class389.method4037(i_3_, i_5_, 2, i_3_, i_10_, i_8_, i_9_, i_8_, i_10_, i_10_))
				return false;
		} else {
			if (!Class389.method4037(i_9_, i_5_, i_6_ ^ 0x2, i_3_, i, i_8_, i_9_, i_5_, i, i))
				return false;
			if (!Class389.method4037(i_3_, i_5_, 2, i_3_, i, i_8_, i_9_, i_8_, i, i))
				return false;
		}
		return true;
	}

	private final void method2133(Packet class296_sub17, int i, int i_11_) {
		if (i == 1) {
			anInt2232 = class296_sub17.readUnsignedMedInt();
			method2135(anInt2232, -12575);
		} else if (i != 2) {
			if (i == 3)
				anInt2241 = class296_sub17.g2() << 2;
			else if (i == 4)
				aBoolean2233 = false;
			else if (i == 5)
				aBoolean2239 = false;
		} else {
			anInt2230 = class296_sub17.g2();
			if (anInt2230 == 65535)
				anInt2230 = -1;
		}
		if (i_11_ <= 17)
			method2133(null, -89, -13);
	}

	static final boolean method2134(int i, int i_12_, boolean bool) {
		if (bool)
			return false;
		if ((i & 0x8000) == 0)
			return false;
		return true;
	}

	private final void method2135(int i, int i_13_) {
		double d = (double) (i >> 16 & 0xff) / 256.0;
		double d_14_ = (double) ((i & 0xfff5) >> 8) / 256.0;
		if (i_13_ != -12575)
			method2134(58, -94, true);
		double d_15_ = (double) (i & 0xff) / 256.0;
		double d_16_ = d;
		if (d_14_ < d_16_)
			d_16_ = d_14_;
		if (d_15_ < d_16_)
			d_16_ = d_15_;
		double d_17_ = d;
		if (d_14_ > d_17_)
			d_17_ = d_14_;
		if (d_15_ > d_17_)
			d_17_ = d_15_;
		double d_18_ = 0.0;
		double d_19_ = 0.0;
		double d_20_ = (d_16_ + d_17_) / 2.0;
		if (d_16_ != d_17_) {
			if (d_20_ < 0.5)
				d_19_ = (d_17_ - d_16_) / (d_16_ + d_17_);
			if (d != d_17_) {
				if (d_14_ != d_17_) {
					if (d_15_ == d_17_)
						d_18_ = (-d_14_ + d) / (d_17_ - d_16_) + 4.0;
				} else
					d_18_ = (d_15_ - d) / (-d_16_ + d_17_) + 2.0;
			} else
				d_18_ = (-d_15_ + d_14_) / (-d_16_ + d_17_);
			if (d_20_ >= 0.5)
				d_19_ = (-d_16_ + d_17_) / (-d_17_ + 2.0 - d_16_);
		}
		d_18_ /= 6.0;
		anInt2237 = (int) (d_20_ * 256.0);
		anInt2236 = (int) (d_19_ * 256.0);
		if (!(d_20_ > 0.5))
			anInt2238 = (int) (d_20_ * d_19_ * 512.0);
		else
			anInt2238 = (int) (d_19_ * (1.0 - d_20_) * 512.0);
		if (anInt2236 < 0)
			anInt2236 = 0;
		else if (anInt2236 > 255)
			anInt2236 = 255;
		if (anInt2237 < 0)
			anInt2237 = 0;
		else if (anInt2237 > 255)
			anInt2237 = 255;
		if (anInt2238 < 1)
			anInt2238 = 1;
		anInt2234 = (int) (d_18_ * (double) anInt2238);
	}

	public Class236() {
		anInt2230 = -1;
		anInt2232 = 0;
		aBoolean2239 = true;
		anInt2241 = 512;
	}
}
