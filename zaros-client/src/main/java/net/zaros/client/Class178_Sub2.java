package net.zaros.client;

import jaclib.memory.Stream;

import jaggl.OpenGL;

public class Class178_Sub2 extends Model {
	private short[] aShortArray4390;
	static int anInt4391;
	private Class331[] aClass331Array4392;
	private short[] aShortArray4393;
	private short[] aShortArray4394;
	private int[] anIntArray4395;
	private short[] aShortArray4396;
	private EffectiveVertex[] aClass232Array4397;
	private float[] aFloatArray4398;
	private short aShort4399;
	private short aShort4400;
	private short aShort4401;
	private short aShort4402;
	private int[][] anIntArrayArray4403;
	private short[] aShortArray4404;
	private Class354[] aClass354Array4405;
	private int[] anIntArray4406;
	private boolean aBoolean4407;
	private boolean aBoolean4408 = false;
	private short aShort4409;
	private float[] aFloatArray4410;
	private short aShort4411;
	private int anInt4412;
	private short[] aShortArray4413;
	private short[] aShortArray4414;
	private ha_Sub3 aHa_Sub3_4415;
	private short[] aShortArray4416;
	private Interface20 anInterface20_4417;
	private int anInt4418;
	private short aShort4419;
	private short[] aShortArray4420;
	private int[] anIntArray4421;
	private int[] anIntArray4422;
	private Class47 aClass47_4423;
	private Interface5 anInterface5_4424;
	private byte[] aByteArray4425;
	private byte[] aByteArray4426;
	private EmissiveTriangle[] aClass89Array4427;
	private Class272 aClass272_4428;
	private int[][] anIntArrayArray4429;
	private boolean aBoolean4430;
	private Class119 aClass119_4431;
	private Class272 aClass272_4432;
	private int anInt4433;
	private short aShort4434;
	private short aShort4435;
	static SubInPacket aClass260_4436 = new SubInPacket(14, -1);
	private int anInt4437;
	private Class272 aClass272_4438;
	private int anInt4439;
	private boolean aBoolean4440 = false;
	private int anInt4441;
	private short[] aShortArray4442;
	private int[][] anIntArrayArray4443;
	private short aShort4444;
	private byte aByte4445;
	private Class272 aClass272_4446;
	private int anInt4447;
	private short[] aShortArray4448;
	private int anInt4449;
	private int[] anIntArray4450;

	public void method1724(byte i, byte[] is) {
		if (is == null) {
			for (int i_0_ = 0; i_0_ < anInt4433; i_0_++)
				aByteArray4426[i_0_] = i;
		} else {
			for (int i_1_ = 0; anInt4433 > i_1_; i_1_++) {
				int i_2_ = (-((-(is[i_1_] & 0xff) + 255) * (-(i & 0xff) + 255) / 255) + 255);
				aByteArray4426[i_1_] = (byte) i_2_;
			}
		}
		if (aClass272_4446 != null)
			aClass272_4446.anInterface5_2521 = null;
	}

	public int HA() {
		if (!aBoolean4440)
			method1793((byte) 119);
		return aShort4401;
	}

	public void p(int i, int i_3_, s var_s, s var_s_4_, int i_5_, int i_6_, int i_7_) {
		if (!aBoolean4440)
			method1793((byte) -33);
		int i_8_ = i_5_ + aShort4400;
		int i_9_ = i_5_ + aShort4409;
		int i_10_ = i_7_ + aShort4401;
		int i_11_ = aShort4435 + i_7_;
		if (i != 1 && i != 2 && i != 3 && i != 5
				|| (i_8_ >= 0 && var_s.anInt2832 > i_9_ + var_s.anInt2836 >> var_s.anInt2835 && i_10_ >= 0
						&& (var_s.anInt2834 > i_11_ + var_s.anInt2836 >> var_s.anInt2835))) {
			if (i != 4 && i != 5) {
				i_8_ >>= var_s.anInt2835;
				i_9_ = i_9_ - (-var_s.anInt2836 + 1) >> var_s.anInt2835;
				i_10_ >>= var_s.anInt2835;
				i_11_ = var_s.anInt2836 - 1 + i_11_ >> var_s.anInt2835;
				if (var_s.method3355(i_10_, (byte) -108, i_8_) == i_6_
						&& var_s.method3355(i_10_, (byte) -107, i_9_) == i_6_
						&& i_6_ == var_s.method3355(i_11_, (byte) -123, i_8_)
						&& var_s.method3355(i_11_, (byte) -127, i_9_) == i_6_)
					return;
			} else if (var_s_4_ == null
					|| (i_8_ < 0 || var_s_4_.anInt2832 <= (var_s_4_.anInt2836 + i_9_ >> var_s_4_.anInt2835) || i_10_ < 0
							|| (i_11_ + var_s_4_.anInt2836 >> var_s_4_.anInt2835 >= var_s_4_.anInt2834)))
				return;
			if (i == 1) {
				for (int i_12_ = 0; anInt4437 > i_12_; i_12_++)
					anIntArray4450[i_12_] = (anIntArray4450[i_12_]
							- (-var_s.method3349(0, anIntArray4422[i_12_] + i_7_, anIntArray4406[i_12_] + i_5_)
									+ i_6_));
			} else if (i != 2) {
				if (i == 3) {
					int i_13_ = (i_3_ & 0xff) * 4;
					int i_14_ = ((i_3_ & 0xff1d) >> 8) * 4;
					int i_15_ = i_3_ >> 16 << 6 & 0x3fc0;
					int i_16_ = (i_3_ >> 24 & 0xff) << 6;
					if (i_5_ - (i_13_ >> 1) < 0
							|| (var_s.anInt2832 << var_s.anInt2835 <= i_5_ - (-(i_13_ >> 1) - var_s.anInt2836))
							|| -(i_14_ >> 1) + i_7_ < 0
							|| ((i_14_ >> 1) + (i_7_ + var_s.anInt2836) >= var_s.anInt2834 << var_s.anInt2835))
						return;
					this.method1734(i_13_, var_s, i_6_, i_15_, i_5_, i_16_, i_7_, i_14_, 65535);
				} else if (i != 4) {
					if (i == 5) {
						int i_17_ = aShort4399 - aShort4402;
						for (int i_18_ = 0; anInt4437 > i_18_; i_18_++) {
							int i_19_ = anIntArray4406[i_18_] + i_5_;
							int i_20_ = anIntArray4422[i_18_] + i_7_;
							int i_21_ = var_s.method3349(0, i_20_, i_19_);
							int i_22_ = var_s_4_.method3349(0, i_20_, i_19_);
							int i_23_ = -i_3_ - i_22_ + i_21_;
							anIntArray4450[i_18_] = (i_21_ - i_6_
									+ (i_23_ * ((anIntArray4450[i_18_] << 8) / i_17_) >> 8));
						}
					}
				} else {
					int i_24_ = -aShort4402 + aShort4399;
					for (int i_25_ = 0; anInt4437 > i_25_; i_25_++)
						anIntArray4450[i_25_] = (i_24_ + anIntArray4450[i_25_] - (i_6_ - var_s_4_.method3349(0,
								i_7_ + (anIntArray4422[i_25_]), (anIntArray4406[i_25_] + i_5_))));
				}
			} else {
				int i_26_ = aShort4402;
				if (i_26_ == 0)
					return;
				for (int i_27_ = 0; i_27_ < anInt4437; i_27_++) {
					int i_28_ = (anIntArray4450[i_27_] << 16) / i_26_;
					if (i_28_ < i_3_)
						anIntArray4450[i_27_] = (anIntArray4450[i_27_] + ((-i_6_
								+ var_s.method3349(0, i_7_ + (anIntArray4422[i_27_]), (anIntArray4406[i_27_] + i_5_)))
								* (i_3_ - i_28_) / i_3_));
				}
			}
			aBoolean4440 = false;
			if (aClass272_4438 != null)
				aClass272_4438.anInterface5_2521 = null;
		}
	}

	private void method1780(r_Sub1 var_r_Sub1, byte i) {
		if (aHa_Sub3_4415.anIntArray4277.length < anInt4439) {
			aHa_Sub3_4415.anIntArray4275 = new int[anInt4439];
			aHa_Sub3_4415.anIntArray4277 = new int[anInt4439];
		}
		int[] is = aHa_Sub3_4415.anIntArray4277;
		int[] is_29_ = aHa_Sub3_4415.anIntArray4275;
		int i_30_ = 0;
		int i_31_ = 67 / ((-28 - i) / 41);
		for (/**/; anInt4437 > i_30_; i_30_++) {
			int i_32_ = (((anIntArray4406[i_30_]
					- (anIntArray4450[i_30_] * aHa_Sub3_4415.anInt4212 >> 8)) >> aHa_Sub3_4415.anInt4139)
					- var_r_Sub1.anInt6704);
			int i_33_ = (-var_r_Sub1.anInt6702 + (-(anIntArray4450[i_30_] * aHa_Sub3_4415.anInt4179 >> 8)
					+ anIntArray4422[i_30_] >> aHa_Sub3_4415.anInt4139));
			int i_34_ = anIntArray4395[i_30_];
			int i_35_ = anIntArray4395[i_30_ + 1];
			for (int i_36_ = i_34_; i_35_ > i_36_; i_36_++) {
				int i_37_ = aShortArray4414[i_36_] - 1;
				if (i_37_ == -1)
					break;
				is[i_37_] = i_32_;
				is_29_[i_37_] = i_33_;
			}
		}
		for (int i_38_ = 0; i_38_ < anInt4447; i_38_++) {
			if (aByteArray4426 == null || aByteArray4426[i_38_] <= 128) {
				short i_39_ = aShortArray4442[i_38_];
				short i_40_ = aShortArray4393[i_38_];
				short i_41_ = aShortArray4390[i_38_];
				int i_42_ = is[i_39_];
				int i_43_ = is[i_40_];
				int i_44_ = is[i_41_];
				int i_45_ = is_29_[i_39_];
				int i_46_ = is_29_[i_40_];
				int i_47_ = is_29_[i_41_];
				if ((-((-i_45_ + i_46_) * (-i_43_ + i_44_)) + (i_46_ - i_47_) * (-i_43_ + i_42_)) > 0)
					var_r_Sub1.method2864(i_42_, i_44_, i_43_, i_47_, i_45_, (byte) 0, i_46_);
			}
		}
	}

	private void method1781(byte i, boolean bool) {
		if (anInt4447 * 6 > aHa_Sub3_4415.aClass296_Sub17_Sub2_4194.data.length)
			aHa_Sub3_4415.aClass296_Sub17_Sub2_4194 = new Class296_Sub17_Sub2((anInt4447 + 100) * 6);
		else
			aHa_Sub3_4415.aClass296_Sub17_Sub2_4194.pos = 0;
		Class296_Sub17_Sub2 class296_sub17_sub2 = aHa_Sub3_4415.aClass296_Sub17_Sub2_4194;
		if (aHa_Sub3_4415.aBoolean4184) {
			for (int i_48_ = 0; anInt4447 > i_48_; i_48_++) {
				class296_sub17_sub2.p2(aShortArray4442[i_48_]);
				class296_sub17_sub2.p2(aShortArray4393[i_48_]);
				class296_sub17_sub2.p2(aShortArray4390[i_48_]);
			}
		} else {
			for (int i_49_ = 0; anInt4447 > i_49_; i_49_++) {
				class296_sub17_sub2.method2590(aShortArray4442[i_49_]);
				class296_sub17_sub2.method2590(aShortArray4393[i_49_]);
				class296_sub17_sub2.method2590(aShortArray4390[i_49_]);
			}
		}
		if (i <= 105)
			aByte4445 = (byte) -26;
		if (class296_sub17_sub2.pos != 0) {
			if (!bool)
				aClass119_4431.anInterface20_1194 = aHa_Sub3_4415.method1310(65280, class296_sub17_sub2.pos, 5123,
						(class296_sub17_sub2.data), false);
			else {
				if (anInterface20_4417 != null)
					anInterface20_4417.method75(class296_sub17_sub2.pos, (class296_sub17_sub2.data), 5123, false);
				else
					anInterface20_4417 = (aHa_Sub3_4415.method1310(65280, class296_sub17_sub2.pos, 5123,
							class296_sub17_sub2.data, true));
				aClass119_4431.anInterface20_1194 = anInterface20_4417;
			}
			if (!bool)
				aBoolean4430 = true;
		}
	}

	private void method1782(int i) {
		if (i != 4)
			aShortArray4413 = null;
		if (aClass354Array4405 != null) {
			Class373_Sub1 class373_sub1 = aHa_Sub3_4415.aClass373_Sub1_4133;
			aHa_Sub3_4415.method1286(-98);
			aHa_Sub3_4415.C(!aBoolean4407);
			aHa_Sub3_4415.method1278(1553921480, false);
			aHa_Sub3_4415.method1323(null, null, aHa_Sub3_4415.aClass272_4270, false, aHa_Sub3_4415.aClass272_4233);
			for (int i_50_ = 0; anInt4418 > i_50_; i_50_++) {
				Class354 class354 = aClass354Array4405[i_50_];
				Class331 class331 = aClass331Array4392[i_50_];
				if (!class354.aBoolean3060 || !aHa_Sub3_4415.b()) {
					float f = ((float) (anIntArray4406[class354.anInt3056]
							+ (anIntArray4406[class354.anInt3053] + anIntArray4406[class354.anInt3055])) * 0.3333333F);
					float f_51_ = ((float) (anIntArray4450[class354.anInt3056]
							+ (anIntArray4450[class354.anInt3055] + anIntArray4450[class354.anInt3053])) * 0.3333333F);
					float f_52_ = ((float) (anIntArray4422[class354.anInt3056]
							+ (anIntArray4422[class354.anInt3055] + anIntArray4422[class354.anInt3053])) * 0.3333333F);
					float f_53_ = (InvisiblePlayer.aFloat1976 + (f_51_ * Class284.aFloat2620
							+ Class296_Sub29.aFloat4811 * f + Class32_Sub1.aFloat5650 * f_52_));
					float f_54_ = (Class296_Sub55.aFloat5073
							+ (Class125.aFloat1287 * f_52_ + (f_51_ * Class126.aFloat1292 + f * Class52.aFloat637)));
					float f_55_ = (Class296_Sub27.aFloat4787 * f_52_
							+ (Class355_Sub1.aFloat3695 * f + f_51_ * Class211.aFloat2102) + FileOnDisk.aFloat674);
					float f_56_ = ((float) (1.0
							/ Math.sqrt((double) (f_55_ * f_55_ + (f_54_ * f_54_ + (f_53_ * f_53_)))))
							* (float) class354.anInt3051);
					class373_sub1.method3922(f_56_ * f_55_ + -f_55_,
							-f_54_ + (float) class331.anInt2937 + f_56_ * f_54_, 6173,
							class354.aShort3059 * class331.anInt2936 >> 7,
							-(f_56_ * f_53_) + ((float) class331.anInt2934 + f_53_),
							class354.aShort3058 * class331.anInt2935 >> 7, class331.anInt2933);
					aHa_Sub3_4415.method1313(-104, class373_sub1);
					int i_57_ = class331.anInt2938;
					OpenGL.glColor4ub((byte) (i_57_ >> 16), (byte) (i_57_ >> 8), (byte) i_57_, (byte) (i_57_ >> 24));
					aHa_Sub3_4415.method1340(class354.aShort3052, (byte) 113);
					aHa_Sub3_4415.method1336((byte) 49, class354.aByte3057);
					aHa_Sub3_4415.method1281(0, 4, 0, 7);
				}
			}
			aHa_Sub3_4415.C(true);
			aHa_Sub3_4415.method1288(7330);
		}
	}

	private void method1783(int i) {
		if (i != anInt4447) {
			if (aByte4445 != 0)
				method1786(false, true);
			method1786(false, false);
			if (aClass119_4431 != null) {
				if (aClass119_4431.anInterface20_1194 == null)
					method1781((byte) 110, (aByte4445 & 0x10) != 0);
				if (aClass119_4431.anInterface20_1194 != null) {
					aHa_Sub3_4415.method1278(1553921480, aClass272_4428 != null);
					aHa_Sub3_4415.method1323(aClass272_4428, aClass272_4446, aClass272_4432, false, aClass272_4438);
					int i_58_ = anIntArray4421.length - 1;
					for (int i_59_ = 0; i_59_ < i_58_; i_59_++) {
						int i_60_ = anIntArray4421[i_59_];
						int i_61_ = anIntArray4421[i_59_ + 1];
						int i_62_ = aShortArray4416[i_60_] & 0xffff;
						if (i_62_ == 65535)
							i_62_ = -1;
						aHa_Sub3_4415.method1350(aClass272_4428 != null, (byte) 107, i_62_);
						aHa_Sub3_4415.method1348((-i_60_ + i_61_) * 3, (aClass119_4431.anInterface20_1194), 4,
								(byte) -17, i_60_ * 3);
					}
				}
			}
			method1791((byte) 99);
		}
	}

	public int WA() {
		return aShort4444;
	}

	private int method1784(int i, int i_63_, short i_64_, byte i_65_, byte i_66_) {
		int i_67_ = (Class166_Sub1.anIntArray4300[Class41_Sub22.method480(i, i_63_, -2145109337)]);
		if (i_64_ != -1) {
			MaterialRaw class170 = aHa_Sub3_4415.aD1299.method14(i_64_ & 0xffff, -9412);
			int i_68_ = class170.aByte1773 & 0xff;
			if (i_68_ != 0) {
				int i_69_;
				if (i_63_ < 0)
					i_69_ = 0;
				else if (i_63_ <= 127)
					i_69_ = i_63_ * 131586;
				else
					i_69_ = 16777215;
				if (i_68_ == 256)
					i_67_ = i_69_;
				else {
					int i_70_ = i_68_;
					int i_71_ = -i_68_ + 256;
					i_67_ = ((i_70_ * (i_69_ & 0xff00) + i_71_ * (i_67_ & 0xff00) & 0xff0000)
							+ ((i_70_ * (i_69_ & 0xff00ff) + (i_67_ & 0xff00ff) * i_71_) & ~0xff00ff)) >> 8;
				}
			}
			int i_72_ = class170.aByte1793 & 0xff;
			if (i_72_ != 0) {
				i_72_ += 256;
				int i_73_ = i_72_ * ((i_67_ & 0xff0000) >> 16);
				if (i_73_ > 65535)
					i_73_ = 65535;
				int i_74_ = ((i_67_ & 0xff00) >> 8) * i_72_;
				if (i_74_ > 65535)
					i_74_ = 65535;
				int i_75_ = (i_67_ & 0xff) * i_72_;
				if (i_75_ > 65535)
					i_75_ = 65535;
				i_67_ = (i_74_ & 0xff00) + ((i_73_ & 0x5000ff00) << 8) + (i_75_ >> 8);
			}
		}
		if (i_66_ != 94)
			method1737();
		return -(i_65_ & 0xff) + 255 + (i_67_ << 8);
	}

	public void method1718(Class373 class373) {
		Class373_Sub1 class373_sub1 = (Class373_Sub1) class373;
		if (aClass89Array4427 != null) {
			for (int i = 0; i < aClass89Array4427.length; i++) {
				EmissiveTriangle class89 = aClass89Array4427[i];
				EmissiveTriangle class89_76_ = class89;
				if (class89.aClass89_956 != null)
					class89_76_ = class89.aClass89_956;
				class89_76_.anInt951 = (int) (class373_sub1.aFloat5576
						+ ((class373_sub1.aFloat5581 * (float) anIntArray4422[class89.anInt954])
								+ (((float) anIntArray4406[class89.anInt954] * class373_sub1.aFloat5577)
										+ (class373_sub1.aFloat5585 * (float) (anIntArray4450[class89.anInt954])))));
				class89_76_.anInt950 = (int) (((float) anIntArray4422[class89.anInt954] * class373_sub1.aFloat5586)
						+ (((float) anIntArray4406[class89.anInt954] * class373_sub1.aFloat5584)
								+ (class373_sub1.aFloat5583 * (float) anIntArray4450[class89.anInt954]))
						+ class373_sub1.aFloat5582);
				class89_76_.anInt957 = (int) (((float) anIntArray4450[class89.anInt954] * class373_sub1.aFloat5579)
						+ ((float) anIntArray4406[class89.anInt954] * class373_sub1.aFloat5587)
						+ ((float) anIntArray4422[class89.anInt954] * class373_sub1.aFloat5589)
						+ class373_sub1.aFloat5588);
				class89_76_.anInt961 = (int) (((float) anIntArray4422[class89.anInt953] * class373_sub1.aFloat5581)
						+ (((float) anIntArray4450[class89.anInt953] * class373_sub1.aFloat5585)
								+ (class373_sub1.aFloat5577 * (float) anIntArray4406[class89.anInt953]))
						+ class373_sub1.aFloat5576);
				class89_76_.anInt963 = (int) (class373_sub1.aFloat5582
						+ ((class373_sub1.aFloat5586 * (float) anIntArray4422[class89.anInt953])
								+ ((class373_sub1.aFloat5583 * (float) anIntArray4450[class89.anInt953])
										+ ((float) anIntArray4406[class89.anInt953] * class373_sub1.aFloat5584))));
				class89_76_.anInt960 = (int) (((float) anIntArray4450[class89.anInt953] * class373_sub1.aFloat5579)
						+ (class373_sub1.aFloat5587 * (float) anIntArray4406[class89.anInt953])
						+ ((float) anIntArray4422[class89.anInt953] * class373_sub1.aFloat5589)
						+ class373_sub1.aFloat5588);
				class89_76_.anInt965 = (int) ((class373_sub1.aFloat5581 * (float) anIntArray4422[class89.anInt964])
						+ (((float) anIntArray4450[class89.anInt964] * class373_sub1.aFloat5585)
								+ (class373_sub1.aFloat5577 * (float) anIntArray4406[class89.anInt964]))
						+ class373_sub1.aFloat5576);
				class89_76_.anInt948 = (int) (class373_sub1.aFloat5582
						+ (((float) anIntArray4422[class89.anInt964] * class373_sub1.aFloat5586)
								+ ((class373_sub1.aFloat5584 * (float) anIntArray4406[class89.anInt964])
										+ (class373_sub1.aFloat5583 * (float) (anIntArray4450[class89.anInt964])))));
				class89_76_.anInt949 = (int) ((class373_sub1.aFloat5587 * (float) anIntArray4406[class89.anInt964])
						+ ((float) anIntArray4450[class89.anInt964] * class373_sub1.aFloat5579)
						+ ((float) anIntArray4422[class89.anInt964] * class373_sub1.aFloat5589)
						+ class373_sub1.aFloat5588);
			}
		}
		if (aClass232Array4397 != null) {
			for (int i = 0; aClass232Array4397.length > i; i++) {
				EffectiveVertex class232 = aClass232Array4397[i];
				EffectiveVertex class232_77_ = class232;
				if (class232.aClass232_2223 != null)
					class232_77_ = class232.aClass232_2223;
				if (class232.aClass373_2222 != null)
					class232.aClass373_2222.method3915(class373_sub1);
				else
					class232.aClass373_2222 = class373_sub1.method3916();
				class232_77_.anInt2219 = (int) (((float) anIntArray4450[class232.anInt2220] * class373_sub1.aFloat5585)
						+ ((float) anIntArray4406[class232.anInt2220] * class373_sub1.aFloat5577)
						+ ((float) anIntArray4422[class232.anInt2220] * class373_sub1.aFloat5581)
						+ class373_sub1.aFloat5576);
				class232_77_.anInt2217 = (int) ((class373_sub1.aFloat5586 * (float) anIntArray4422[class232.anInt2220])
						+ (((float) anIntArray4406[class232.anInt2220] * class373_sub1.aFloat5584)
								+ (class373_sub1.aFloat5583 * (float) (anIntArray4450[class232.anInt2220])))
						+ class373_sub1.aFloat5582);
				class232_77_.anInt2214 = (int) ((class373_sub1.aFloat5579 * (float) anIntArray4450[class232.anInt2220])
						+ (class373_sub1.aFloat5587 * (float) anIntArray4406[class232.anInt2220])
						+ (class373_sub1.aFloat5589 * (float) anIntArray4422[class232.anInt2220])
						+ class373_sub1.aFloat5588);
			}
		}
	}

	public void aa(short i, short i_78_) {
		d var_d = aHa_Sub3_4415.aD1299;
		for (int i_79_ = 0; anInt4433 > i_79_; i_79_++) {
			if (i == aShortArray4416[i_79_])
				aShortArray4416[i_79_] = i_78_;
		}
		byte i_80_ = 0;
		byte i_81_ = 0;
		if (i != -1) {
			MaterialRaw class170 = var_d.method14(i & 0xffff, -9412);
			i_80_ = class170.aByte1773;
			i_81_ = class170.aByte1793;
		}
		byte i_82_ = 0;
		byte i_83_ = 0;
		if (i_78_ != -1) {
			MaterialRaw class170 = var_d.method14(i_78_ & 0xffff, -9412);
			i_82_ = class170.aByte1773;
			if (class170.speed_u != 0 || class170.speed_v != 0)
				aBoolean4408 = true;
			i_83_ = class170.aByte1793;
		}
		if (i_83_ != i_81_ | i_80_ != i_82_) {
			if (aClass354Array4405 != null) {
				for (int i_84_ = 0; i_84_ < anInt4418; i_84_++) {
					Class354 class354 = aClass354Array4405[i_84_];
					Class331 class331 = aClass331Array4392[i_84_];
					class331.anInt2938 = (class331.anInt2938 & ~0xffffff
							| ((Class166_Sub1.anIntArray4300[aShortArray4394[class354.anInt3054] & 0xffff])
									& 0xffffff));
				}
			}
			if (aClass272_4446 != null)
				aClass272_4446.anInterface5_2521 = null;
		}
	}

	public void I(int i, int[] is, int i_85_, int i_86_, int i_87_, boolean bool, int i_88_, int[] is_89_) {
		int i_90_ = is.length;
		if (i == 0) {
			i_85_ <<= 4;
			i_86_ <<= 4;
			i_87_ <<= 4;
			Class368.anInt3132 = 0;
			Class41_Sub20.anInt3796 = 0;
			Class288.anInt2647 = 0;
			int i_91_ = 0;
			for (int i_92_ = 0; i_92_ < i_90_; i_92_++) {
				int i_93_ = is[i_92_];
				if (i_93_ < anIntArrayArray4429.length) {
					int[] is_94_ = anIntArrayArray4429[i_93_];
					for (int i_95_ = 0; is_94_.length > i_95_; i_95_++) {
						int i_96_ = is_94_[i_95_];
						if (aShortArray4404 == null || (i_88_ & aShortArray4404[i_96_]) != 0) {
							Class288.anInt2647 += anIntArray4406[i_96_];
							Class41_Sub20.anInt3796 += anIntArray4450[i_96_];
							Class368.anInt3132 += anIntArray4422[i_96_];
							i_91_++;
						}
					}
				}
			}
			if (i_91_ > 0) {
				Class41_Sub20.anInt3796 = i_86_ + Class41_Sub20.anInt3796 / i_91_;
				Class288.anInt2647 = Class288.anInt2647 / i_91_ + i_85_;
				StaticMethods.aBoolean5963 = true;
				Class368.anInt3132 = Class368.anInt3132 / i_91_ + i_87_;
			} else {
				Class41_Sub20.anInt3796 = i_86_;
				Class288.anInt2647 = i_85_;
				Class368.anInt3132 = i_87_;
			}
		} else if (i == 1) {
			if (is_89_ != null) {
				int i_97_ = ((is_89_[1] * i_86_ + i_85_ * is_89_[0] + is_89_[2] * i_87_ + 8192) >> 14);
				int i_98_ = (i_87_ * is_89_[5] + 8192 + (is_89_[3] * i_85_ + is_89_[4] * i_86_) >> 14);
				int i_99_ = ((i_87_ * is_89_[8] + i_86_ * is_89_[7] + (is_89_[6] * i_85_ + 8192)) >> 14);
				i_87_ = i_99_;
				i_85_ = i_97_;
				i_86_ = i_98_;
			}
			i_86_ <<= 4;
			i_87_ <<= 4;
			i_85_ <<= 4;
			for (int i_100_ = 0; i_90_ > i_100_; i_100_++) {
				int i_101_ = is[i_100_];
				if (anIntArrayArray4429.length > i_101_) {
					int[] is_102_ = anIntArrayArray4429[i_101_];
					for (int i_103_ = 0; is_102_.length > i_103_; i_103_++) {
						int i_104_ = is_102_[i_103_];
						if (aShortArray4404 == null || (i_88_ & aShortArray4404[i_104_]) != 0) {
							anIntArray4406[i_104_] += i_85_;
							anIntArray4450[i_104_] += i_86_;
							anIntArray4422[i_104_] += i_87_;
						}
					}
				}
			}
		} else if (i == 2) {
			if (is_89_ == null) {
				for (int i_105_ = 0; i_105_ < i_90_; i_105_++) {
					int i_106_ = is[i_105_];
					if (i_106_ < anIntArrayArray4429.length) {
						int[] is_107_ = anIntArrayArray4429[i_106_];
						for (int i_108_ = 0; i_108_ < is_107_.length; i_108_++) {
							int i_109_ = is_107_[i_108_];
							if (aShortArray4404 == null || (aShortArray4404[i_109_] & i_88_) != 0) {
								anIntArray4406[i_109_] -= Class288.anInt2647;
								anIntArray4450[i_109_] -= Class41_Sub20.anInt3796;
								anIntArray4422[i_109_] -= Class368.anInt3132;
								if (i_87_ != 0) {
									int i_110_ = Class296_Sub4.anIntArray4598[i_87_];
									int i_111_ = Class296_Sub4.anIntArray4618[i_87_];
									int i_112_ = ((i_110_ * anIntArray4450[i_109_]
											- (-(anIntArray4406[i_109_] * i_111_) - 16383)) >> 14);
									anIntArray4450[i_109_] = (anIntArray4450[i_109_] * i_111_
											- (i_110_ * anIntArray4406[i_109_] - 16383)) >> 14;
									anIntArray4406[i_109_] = i_112_;
								}
								if (i_85_ != 0) {
									int i_113_ = Class296_Sub4.anIntArray4598[i_85_];
									int i_114_ = Class296_Sub4.anIntArray4618[i_85_];
									int i_115_ = ((-(i_113_ * anIntArray4422[i_109_])
											+ (anIntArray4450[i_109_] * i_114_ + 16383)) >> 14);
									anIntArray4422[i_109_] = (i_113_ * anIntArray4450[i_109_]
											+ i_114_ * anIntArray4422[i_109_] + 16383) >> 14;
									anIntArray4450[i_109_] = i_115_;
								}
								if (i_86_ != 0) {
									int i_116_ = Class296_Sub4.anIntArray4598[i_86_];
									int i_117_ = Class296_Sub4.anIntArray4618[i_86_];
									int i_118_ = ((anIntArray4406[i_109_] * i_117_ + 16383
											+ i_116_ * anIntArray4422[i_109_]) >> 14);
									anIntArray4422[i_109_] = (-(i_116_ * anIntArray4406[i_109_])
											+ i_117_ * anIntArray4422[i_109_] + 16383) >> 14;
									anIntArray4406[i_109_] = i_118_;
								}
								anIntArray4406[i_109_] += Class288.anInt2647;
								anIntArray4450[i_109_] += Class41_Sub20.anInt3796;
								anIntArray4422[i_109_] += Class368.anInt3132;
							}
						}
					}
				}
				if (bool) {
					for (int i_119_ = 0; i_90_ > i_119_; i_119_++) {
						int i_120_ = is[i_119_];
						if (anIntArrayArray4429.length > i_120_) {
							int[] is_121_ = anIntArrayArray4429[i_120_];
							for (int i_122_ = 0; i_122_ < is_121_.length; i_122_++) {
								int i_123_ = is_121_[i_122_];
								if (aShortArray4404 == null || ((i_88_ & aShortArray4404[i_123_]) != 0)) {
									int i_124_ = anIntArray4395[i_123_];
									int i_125_ = anIntArray4395[i_123_ + 1];
									for (int i_126_ = i_124_; i_126_ < i_125_; i_126_++) {
										int i_127_ = aShortArray4414[i_126_] - 1;
										if (i_127_ == -1)
											break;
										if (i_87_ != 0) {
											int i_128_ = (Class296_Sub4.anIntArray4598[i_87_]);
											int i_129_ = (Class296_Sub4.anIntArray4618[i_87_]);
											int i_130_ = (((aShortArray4448[i_127_] * i_128_)
													+ (aShortArray4420[i_127_] * i_129_) + 16383) >> 14);
											aShortArray4448[i_127_] = (short) ((-((aShortArray4420[i_127_]) * i_128_)
													+ ((aShortArray4448[i_127_]) * i_129_) + 16383) >> 14);
											aShortArray4420[i_127_] = (short) i_130_;
										}
										if (i_85_ != 0) {
											int i_131_ = (Class296_Sub4.anIntArray4598[i_85_]);
											int i_132_ = (Class296_Sub4.anIntArray4618[i_85_]);
											int i_133_ = (((i_132_ * aShortArray4448[i_127_])
													- i_131_ * (aShortArray4413[i_127_]) + 16383) >> 14);
											aShortArray4413[i_127_] = (short) (((i_132_ * (aShortArray4413[i_127_]))
													+ 16383 + (i_131_ * (aShortArray4448[i_127_]))) >> 14);
											aShortArray4448[i_127_] = (short) i_133_;
										}
										if (i_86_ != 0) {
											int i_134_ = (Class296_Sub4.anIntArray4598[i_86_]);
											int i_135_ = (Class296_Sub4.anIntArray4618[i_86_]);
											int i_136_ = (((aShortArray4413[i_127_] * i_134_)
													+ (aShortArray4420[i_127_] * i_135_) + 16383) >> 14);
											aShortArray4413[i_127_] = (short) ((-((aShortArray4420[i_127_]) * i_134_)
													+ (((aShortArray4413[i_127_]) * i_135_) + 16383)) >> 14);
											aShortArray4420[i_127_] = (short) i_136_;
										}
									}
								}
							}
						}
					}
					if (aClass272_4428 == null && aClass272_4446 != null)
						aClass272_4446.anInterface5_2521 = null;
					if (aClass272_4428 != null)
						aClass272_4428.anInterface5_2521 = null;
				}
			} else {
				int i_137_ = is_89_[9] << 4;
				int i_138_ = is_89_[10] << 4;
				int i_139_ = is_89_[11] << 4;
				int i_140_ = is_89_[12] << 4;
				int i_141_ = is_89_[13] << 4;
				int i_142_ = is_89_[14] << 4;
				if (StaticMethods.aBoolean5963) {
					int i_143_ = ((Class41_Sub20.anInt3796 * is_89_[3]
							+ (Class288.anInt2647 * is_89_[0] - (-(is_89_[6] * Class368.anInt3132) - 8192))) >> 14);
					int i_144_ = ((Class41_Sub20.anInt3796 * is_89_[4]
							+ (Class288.anInt2647 * is_89_[1] + is_89_[7] * Class368.anInt3132) + 8192) >> 14);
					int i_145_ = ((Class368.anInt3132 * is_89_[8]
							+ (is_89_[2] * Class288.anInt2647 + (Class41_Sub20.anInt3796 * is_89_[5] + 8192))) >> 14);
					i_143_ += i_140_;
					i_144_ += i_141_;
					Class41_Sub20.anInt3796 = i_144_;
					Class288.anInt2647 = i_143_;
					i_145_ += i_142_;
					Class368.anInt3132 = i_145_;
					StaticMethods.aBoolean5963 = false;
				}
				int[] is_146_ = new int[9];
				int i_147_ = Class296_Sub4.anIntArray4618[i_85_];
				int i_148_ = Class296_Sub4.anIntArray4598[i_85_];
				int i_149_ = Class296_Sub4.anIntArray4618[i_86_];
				int i_150_ = Class296_Sub4.anIntArray4598[i_86_];
				int i_151_ = Class296_Sub4.anIntArray4618[i_87_];
				int i_152_ = Class296_Sub4.anIntArray4598[i_87_];
				int i_153_ = i_151_ * i_148_ + 8192 >> 14;
				int i_154_ = i_152_ * i_148_ + 8192 >> 14;
				is_146_[3] = i_147_ * i_152_ + 8192 >> 14;
				is_146_[7] = i_149_ * i_153_ + (i_152_ * i_150_ + 8192) >> 14;
				is_146_[5] = -i_148_;
				is_146_[6] = i_151_ * -i_150_ + i_149_ * i_154_ + 8192 >> 14;
				is_146_[4] = i_151_ * i_147_ + 8192 >> 14;
				is_146_[2] = i_147_ * i_150_ + 8192 >> 14;
				is_146_[0] = i_151_ * i_149_ + 8192 + i_154_ * i_150_ >> 14;
				is_146_[8] = i_149_ * i_147_ + 8192 >> 14;
				is_146_[1] = i_153_ * i_150_ + (i_152_ * -i_149_ + 8192) >> 14;
				int i_155_ = ((is_146_[2] * -Class368.anInt3132 + -Class288.anInt2647 * is_146_[0]
						+ -Class41_Sub20.anInt3796 * is_146_[1] + 8192) >> 14);
				int i_156_ = ((is_146_[5] * -Class368.anInt3132 + is_146_[3] * -Class288.anInt2647
						+ (-Class41_Sub20.anInt3796 * is_146_[4] + 8192)) >> 14);
				int i_157_ = ((-Class368.anInt3132 * is_146_[8]
						+ (is_146_[7] * -Class41_Sub20.anInt3796 + -Class288.anInt2647 * is_146_[6]) + 8192) >> 14);
				int i_158_ = Class288.anInt2647 + i_155_;
				int i_159_ = i_156_ + Class41_Sub20.anInt3796;
				int i_160_ = i_157_ + Class368.anInt3132;
				int[] is_161_ = new int[9];
				for (int i_162_ = 0; i_162_ < 3; i_162_++) {
					for (int i_163_ = 0; i_163_ < 3; i_163_++) {
						int i_164_ = 0;
						for (int i_165_ = 0; i_165_ < 3; i_165_++)
							i_164_ += (is_89_[i_163_ * 3 + i_165_] * is_146_[i_162_ * 3 + i_165_]);
						is_161_[i_163_ + i_162_ * 3] = i_164_ + 8192 >> 14;
					}
				}
				int i_166_ = (is_146_[0] * i_140_ + (is_146_[1] * i_141_ + is_146_[2] * i_142_ + 8192) >> 14);
				int i_167_ = ((is_146_[5] * i_142_ + i_140_ * is_146_[3] + 8192 + is_146_[4] * i_141_) >> 14);
				i_166_ += i_158_;
				int i_168_ = (i_141_ * is_146_[7] + (is_146_[6] * i_140_ - (-(is_146_[8] * i_142_) - 8192)) >> 14);
				i_167_ += i_159_;
				i_168_ += i_160_;
				int[] is_169_ = new int[9];
				for (int i_170_ = 0; i_170_ < 3; i_170_++) {
					for (int i_171_ = 0; i_171_ < 3; i_171_++) {
						int i_172_ = 0;
						for (int i_173_ = 0; i_173_ < 3; i_173_++)
							i_172_ += (is_89_[i_170_ * 3 + i_173_] * is_161_[i_171_ + i_173_ * 3]);
						is_169_[i_170_ * 3 + i_171_] = i_172_ + 8192 >> 14;
					}
				}
				int i_174_ = ((i_168_ * is_89_[2] + (i_167_ * is_89_[1] + i_166_ * is_89_[0]) + 8192) >> 14);
				int i_175_ = ((i_168_ * is_89_[5] + (is_89_[4] * i_167_ + is_89_[3] * i_166_) + 8192) >> 14);
				i_175_ += i_138_;
				i_174_ += i_137_;
				int i_176_ = ((i_168_ * is_89_[8] + (is_89_[7] * i_167_ + i_166_ * is_89_[6]) + 8192) >> 14);
				i_176_ += i_139_;
				for (int i_177_ = 0; i_177_ < i_90_; i_177_++) {
					int i_178_ = is[i_177_];
					if (i_178_ < anIntArrayArray4429.length) {
						int[] is_179_ = anIntArrayArray4429[i_178_];
						for (int i_180_ = 0; i_180_ < is_179_.length; i_180_++) {
							int i_181_ = is_179_[i_180_];
							if (aShortArray4404 == null || (i_88_ & aShortArray4404[i_181_]) != 0) {
								int i_182_ = ((anIntArray4422[i_181_] * is_169_[2] + 8192
										+ is_169_[1] * anIntArray4450[i_181_]
										+ anIntArray4406[i_181_] * is_169_[0]) >> 14);
								int i_183_ = ((anIntArray4406[i_181_] * is_169_[3] + anIntArray4450[i_181_] * is_169_[4]
										- (-(is_169_[5] * anIntArray4422[i_181_]) - 8192)) >> 14);
								i_183_ += i_175_;
								i_182_ += i_174_;
								int i_184_ = ((anIntArray4422[i_181_] * is_169_[8] + 8192
										+ (anIntArray4450[i_181_] * is_169_[7]
												+ (is_169_[6] * anIntArray4406[i_181_]))) >> 14);
								i_184_ += i_176_;
								anIntArray4406[i_181_] = i_182_;
								anIntArray4450[i_181_] = i_183_;
								anIntArray4422[i_181_] = i_184_;
							}
						}
					}
				}
			}
		} else if (i == 3) {
			if (is_89_ != null) {
				int i_185_ = is_89_[9] << 4;
				int i_186_ = is_89_[10] << 4;
				int i_187_ = is_89_[11] << 4;
				int i_188_ = is_89_[12] << 4;
				int i_189_ = is_89_[13] << 4;
				int i_190_ = is_89_[14] << 4;
				if (StaticMethods.aBoolean5963) {
					int i_191_ = ((is_89_[6] * Class368.anInt3132
							+ (is_89_[0] * Class288.anInt2647 + Class41_Sub20.anInt3796 * is_89_[3]) + 8192) >> 14);
					int i_192_ = ((Class368.anInt3132 * is_89_[7]
							+ (Class41_Sub20.anInt3796 * is_89_[4] + Class288.anInt2647 * is_89_[1]) + 8192) >> 14);
					i_191_ += i_188_;
					int i_193_ = ((is_89_[5] * Class41_Sub20.anInt3796
							+ (Class288.anInt2647 * is_89_[2] - (-(Class368.anInt3132 * is_89_[8]) - 8192))) >> 14);
					i_192_ += i_189_;
					Class41_Sub20.anInt3796 = i_192_;
					Class288.anInt2647 = i_191_;
					i_193_ += i_190_;
					StaticMethods.aBoolean5963 = false;
					Class368.anInt3132 = i_193_;
				}
				int i_194_ = i_85_ << 15 >> 7;
				int i_195_ = i_86_ << 15 >> 7;
				int i_196_ = i_87_ << 15 >> 7;
				int i_197_ = i_194_ * -Class288.anInt2647 + 8192 >> 14;
				int i_198_ = -Class41_Sub20.anInt3796 * i_195_ + 8192 >> 14;
				int i_199_ = i_196_ * -Class368.anInt3132 + 8192 >> 14;
				int i_200_ = Class288.anInt2647 + i_197_;
				int i_201_ = Class41_Sub20.anInt3796 + i_198_;
				int i_202_ = Class368.anInt3132 + i_199_;
				int[] is_203_ = new int[9];
				is_203_[0] = i_194_ * is_89_[0] + 8192 >> 14;
				is_203_[1] = is_89_[3] * i_194_ + 8192 >> 14;
				is_203_[2] = i_194_ * is_89_[6] + 8192 >> 14;
				is_203_[5] = i_195_ * is_89_[7] + 8192 >> 14;
				is_203_[3] = is_89_[1] * i_195_ + 8192 >> 14;
				is_203_[4] = is_89_[4] * i_195_ + 8192 >> 14;
				is_203_[6] = i_196_ * is_89_[2] + 8192 >> 14;
				is_203_[7] = is_89_[5] * i_196_ + 8192 >> 14;
				is_203_[8] = is_89_[8] * i_196_ + 8192 >> 14;
				int i_204_ = i_188_ * i_194_ + 8192 >> 14;
				int i_205_ = i_195_ * i_189_ + 8192 >> 14;
				i_204_ += i_200_;
				int i_206_ = i_190_ * i_196_ + 8192 >> 14;
				i_205_ += i_201_;
				i_206_ += i_202_;
				int[] is_207_ = new int[9];
				for (int i_208_ = 0; i_208_ < 3; i_208_++) {
					for (int i_209_ = 0; i_209_ < 3; i_209_++) {
						int i_210_ = 0;
						for (int i_211_ = 0; i_211_ < 3; i_211_++)
							i_210_ += (is_89_[i_211_ + i_208_ * 3] * is_203_[i_211_ * 3 + i_209_]);
						is_207_[i_209_ + i_208_ * 3] = i_210_ + 8192 >> 14;
					}
				}
				int i_212_ = ((is_89_[2] * i_206_ + 8192 + i_204_ * is_89_[0] + i_205_ * is_89_[1]) >> 14);
				int i_213_ = ((is_89_[5] * i_206_ + i_205_ * is_89_[4] + (is_89_[3] * i_204_ + 8192)) >> 14);
				int i_214_ = ((is_89_[7] * i_205_ + (i_204_ * is_89_[6] + i_206_ * is_89_[8]) + 8192) >> 14);
				i_212_ += i_185_;
				i_213_ += i_186_;
				i_214_ += i_187_;
				for (int i_215_ = 0; i_215_ < i_90_; i_215_++) {
					int i_216_ = is[i_215_];
					if (anIntArrayArray4429.length > i_216_) {
						int[] is_217_ = anIntArrayArray4429[i_216_];
						for (int i_218_ = 0; i_218_ < is_217_.length; i_218_++) {
							int i_219_ = is_217_[i_218_];
							if (aShortArray4404 == null || (aShortArray4404[i_219_] & i_88_) != 0) {
								int i_220_ = ((anIntArray4450[i_219_] * is_207_[1] + 8192
										+ anIntArray4406[i_219_] * is_207_[0]
										+ is_207_[2] * anIntArray4422[i_219_]) >> 14);
								int i_221_ = ((anIntArray4422[i_219_] * is_207_[5] + is_207_[3] * anIntArray4406[i_219_]
										- (-(anIntArray4450[i_219_] * is_207_[4]) - 8192)) >> 14);
								i_221_ += i_213_;
								int i_222_ = ((is_207_[7] * anIntArray4450[i_219_] + anIntArray4406[i_219_] * is_207_[6]
										- (-(anIntArray4422[i_219_] * is_207_[8]) - 8192)) >> 14);
								i_220_ += i_212_;
								i_222_ += i_214_;
								anIntArray4406[i_219_] = i_220_;
								anIntArray4450[i_219_] = i_221_;
								anIntArray4422[i_219_] = i_222_;
							}
						}
					}
				}
			} else {
				for (int i_223_ = 0; i_90_ > i_223_; i_223_++) {
					int i_224_ = is[i_223_];
					if (anIntArrayArray4429.length > i_224_) {
						int[] is_225_ = anIntArrayArray4429[i_224_];
						for (int i_226_ = 0; is_225_.length > i_226_; i_226_++) {
							int i_227_ = is_225_[i_226_];
							if (aShortArray4404 == null || (i_88_ & aShortArray4404[i_227_]) != 0) {
								anIntArray4406[i_227_] -= Class288.anInt2647;
								anIntArray4450[i_227_] -= Class41_Sub20.anInt3796;
								anIntArray4422[i_227_] -= Class368.anInt3132;
								anIntArray4406[i_227_] = anIntArray4406[i_227_] * i_85_ >> 7;
								anIntArray4450[i_227_] = i_86_ * anIntArray4450[i_227_] >> 7;
								anIntArray4422[i_227_] = i_87_ * anIntArray4422[i_227_] >> 7;
								anIntArray4406[i_227_] += Class288.anInt2647;
								anIntArray4450[i_227_] += Class41_Sub20.anInt3796;
								anIntArray4422[i_227_] += Class368.anInt3132;
							}
						}
					}
				}
			}
		} else if (i == 5) {
			if (anIntArrayArray4403 != null) {
				for (int i_228_ = 0; i_90_ > i_228_; i_228_++) {
					int i_229_ = is[i_228_];
					if (anIntArrayArray4403.length > i_229_) {
						int[] is_230_ = anIntArrayArray4403[i_229_];
						for (int i_231_ = 0; i_231_ < is_230_.length; i_231_++) {
							int i_232_ = is_230_[i_231_];
							if (aShortArray4396 == null || (aShortArray4396[i_232_] & i_88_) != 0) {
								int i_233_ = ((aByteArray4426[i_232_] & 0xff) + i_85_ * 8);
								if (i_233_ >= 0) {
									if (i_233_ > 255)
										i_233_ = 255;
								} else
									i_233_ = 0;
								aByteArray4426[i_232_] = (byte) i_233_;
								if (aClass272_4446 != null)
									aClass272_4446.anInterface5_2521 = null;
							}
						}
					}
				}
				if (aClass354Array4405 != null) {
					for (int i_234_ = 0; i_234_ < anInt4418; i_234_++) {
						Class354 class354 = aClass354Array4405[i_234_];
						Class331 class331 = aClass331Array4392[i_234_];
						class331.anInt2938 = (255 - (aByteArray4426[class354.anInt3054] & 0xff) << 24
								| class331.anInt2938 & 0xffffff);
					}
				}
			}
		} else if (i == 7) {
			if (anIntArrayArray4403 != null) {
				for (int i_235_ = 0; i_90_ > i_235_; i_235_++) {
					int i_236_ = is[i_235_];
					if (i_236_ < anIntArrayArray4403.length) {
						int[] is_237_ = anIntArrayArray4403[i_236_];
						for (int i_238_ = 0; is_237_.length > i_238_; i_238_++) {
							int i_239_ = is_237_[i_238_];
							if (aShortArray4396 == null || (aShortArray4396[i_239_] & i_88_) != 0) {
								int i_240_ = aShortArray4394[i_239_] & 0xffff;
								int i_241_ = i_240_ >> 10 & 0x3f;
								int i_242_ = i_240_ >> 7 & 0x7;
								i_242_ += i_86_ / 4;
								int i_243_ = i_240_ & 0x7f;
								i_241_ = i_241_ + i_85_ & 0x3f;
								if (i_242_ < 0)
									i_242_ = 0;
								else if (i_242_ > 7)
									i_242_ = 7;
								i_243_ += i_87_;
								if (i_243_ < 0)
									i_243_ = 0;
								else if (i_243_ > 127)
									i_243_ = 127;
								aShortArray4394[i_239_] = (short) (Class48.bitOR(i_243_,
										Class48.bitOR(i_242_ << 7, (i_241_ << 10))));
								if (aClass272_4446 != null)
									aClass272_4446.anInterface5_2521 = null;
							}
						}
					}
				}
				if (aClass354Array4405 != null) {
					for (int i_244_ = 0; i_244_ < anInt4418; i_244_++) {
						Class354 class354 = aClass354Array4405[i_244_];
						Class331 class331 = aClass331Array4392[i_244_];
						class331.anInt2938 = (class331.anInt2938 & ~0xffffff
								| (Class166_Sub1.anIntArray4300[(aShortArray4394[class354.anInt3054] & 0xffff)])
										& 0xffffff);
					}
				}
			}
		} else if (i == 8) {
			if (anIntArrayArray4443 != null) {
				for (int i_245_ = 0; i_245_ < i_90_; i_245_++) {
					int i_246_ = is[i_245_];
					if (i_246_ < anIntArrayArray4443.length) {
						int[] is_247_ = anIntArrayArray4443[i_246_];
						for (int i_248_ = 0; is_247_.length > i_248_; i_248_++) {
							Class331 class331 = aClass331Array4392[is_247_[i_248_]];
							class331.anInt2937 += i_86_;
							class331.anInt2934 += i_85_;
						}
					}
				}
			}
		} else if (i == 10) {
			if (anIntArrayArray4443 != null) {
				for (int i_249_ = 0; i_249_ < i_90_; i_249_++) {
					int i_250_ = is[i_249_];
					if (i_250_ < anIntArrayArray4443.length) {
						int[] is_251_ = anIntArrayArray4443[i_250_];
						for (int i_252_ = 0; i_252_ < is_251_.length; i_252_++) {
							Class331 class331 = aClass331Array4392[is_251_[i_252_]];
							class331.anInt2935 = class331.anInt2935 * i_85_ >> 7;
							class331.anInt2936 = i_86_ * class331.anInt2936 >> 7;
						}
					}
				}
			}
		} else if (i == 9) {
			if (anIntArrayArray4443 != null) {
				for (int i_253_ = 0; i_90_ > i_253_; i_253_++) {
					int i_254_ = is[i_253_];
					if (i_254_ < anIntArrayArray4443.length) {
						int[] is_255_ = anIntArrayArray4443[i_254_];
						for (int i_256_ = 0; is_255_.length > i_256_; i_256_++) {
							Class331 class331 = aClass331Array4392[is_255_[i_256_]];
							class331.anInt2933 = class331.anInt2933 + i_85_ & 0x3fff;
						}
					}
				}
			}
		}
	}

	public void method1719(Class373 class373, Class338_Sub5 class338_sub5, int i, int i_257_) {
		if (anInt4439 != 0) {
			Class373_Sub1 class373_sub1 = aHa_Sub3_4415.aClass373_Sub1_4168;
			Class373_Sub1 class373_sub1_258_ = (Class373_Sub1) class373;
			if (!aBoolean4440)
				method1793((byte) -84);
			Class211.aFloat2102 = (class373_sub1_258_.aFloat5579 * class373_sub1.aFloat5589
					+ (class373_sub1_258_.aFloat5583 * class373_sub1.aFloat5579
							+ (class373_sub1.aFloat5587 * class373_sub1_258_.aFloat5585)));
			FileOnDisk.aFloat674 = (class373_sub1.aFloat5588 + (class373_sub1_258_.aFloat5576 * class373_sub1.aFloat5587
					+ (class373_sub1.aFloat5579 * class373_sub1_258_.aFloat5582)
					+ (class373_sub1_258_.aFloat5588 * class373_sub1.aFloat5589)));
			float f = FileOnDisk.aFloat674 + (float) aShort4402 * Class211.aFloat2102;
			float f_259_ = FileOnDisk.aFloat674 + Class211.aFloat2102 * (float) aShort4399;
			float f_260_;
			float f_261_;
			if (f > f_259_) {
				f_261_ = (float) aShort4434 + f;
				f_260_ = f_259_ - (float) aShort4434;
			} else {
				f_260_ = f - (float) aShort4434;
				f_261_ = f_259_ + (float) aShort4434;
			}
			if (!(aHa_Sub3_4415.aFloat4226 <= f_260_) && !(f_261_ <= (float) aHa_Sub3_4415.anInt4249)) {
				InvisiblePlayer.aFloat1976 = (class373_sub1.aFloat5576
						+ ((class373_sub1.aFloat5581 * class373_sub1_258_.aFloat5588)
								+ ((class373_sub1_258_.aFloat5582 * class373_sub1.aFloat5585)
										+ (class373_sub1.aFloat5577 * class373_sub1_258_.aFloat5576))));
				Class284.aFloat2620 = (class373_sub1.aFloat5581 * class373_sub1_258_.aFloat5579
						+ ((class373_sub1_258_.aFloat5585 * class373_sub1.aFloat5577)
								+ (class373_sub1.aFloat5585 * class373_sub1_258_.aFloat5583)));
				float f_262_ = (Class284.aFloat2620 * (float) aShort4402 + InvisiblePlayer.aFloat1976);
				float f_263_ = (InvisiblePlayer.aFloat1976 + Class284.aFloat2620 * (float) aShort4399);
				float f_264_;
				float f_265_;
				if (f_262_ > f_263_) {
					f_265_ = (((float) aShort4434 + f_262_) * (float) aHa_Sub3_4415.anInt4263);
					f_264_ = ((f_263_ - (float) aShort4434) * (float) aHa_Sub3_4415.anInt4263);
				} else {
					f_264_ = ((f_262_ - (float) aShort4434) * (float) aHa_Sub3_4415.anInt4263);
					f_265_ = (((float) aShort4434 + f_263_) * (float) aHa_Sub3_4415.anInt4263);
				}
				if (!(f_264_ / (float) i >= aHa_Sub3_4415.aFloat4198)
						&& !(f_265_ / (float) i <= aHa_Sub3_4415.aFloat4216)) {
					Class126.aFloat1292 = ((class373_sub1_258_.aFloat5585 * class373_sub1.aFloat5584)
							+ (class373_sub1.aFloat5583 * class373_sub1_258_.aFloat5583)
							+ (class373_sub1_258_.aFloat5579 * class373_sub1.aFloat5586));
					Class296_Sub55.aFloat5073 = ((class373_sub1_258_.aFloat5582 * class373_sub1.aFloat5583)
							+ (class373_sub1.aFloat5584 * class373_sub1_258_.aFloat5576)
							+ (class373_sub1_258_.aFloat5588 * class373_sub1.aFloat5586) + class373_sub1.aFloat5582);
					float f_266_ = ((float) aShort4402 * Class126.aFloat1292 + Class296_Sub55.aFloat5073);
					float f_267_ = ((float) aShort4399 * Class126.aFloat1292 + Class296_Sub55.aFloat5073);
					float f_268_;
					float f_269_;
					if (f_267_ < f_266_) {
						f_269_ = (((float) aShort4434 + f_266_) * (float) aHa_Sub3_4415.anInt4227);
						f_268_ = ((float) aHa_Sub3_4415.anInt4227 * (f_267_ - (float) aShort4434));
					} else {
						f_268_ = ((float) aHa_Sub3_4415.anInt4227 * (f_266_ - (float) aShort4434));
						f_269_ = (((float) aShort4434 + f_267_) * (float) aHa_Sub3_4415.anInt4227);
					}
					if (!(f_268_ / (float) i >= aHa_Sub3_4415.aFloat4256)
							&& !(f_269_ / (float) i <= aHa_Sub3_4415.aFloat4242)) {
						if (class338_sub5 != null || aClass354Array4405 != null) {
							Class52.aFloat637 = ((class373_sub1.aFloat5584 * class373_sub1_258_.aFloat5577)
									+ (class373_sub1.aFloat5583 * class373_sub1_258_.aFloat5584)
									+ (class373_sub1_258_.aFloat5587 * class373_sub1.aFloat5586));
							Class296_Sub27.aFloat4787 = ((class373_sub1.aFloat5587 * class373_sub1_258_.aFloat5581)
									+ (class373_sub1.aFloat5579 * class373_sub1_258_.aFloat5586)
									+ (class373_sub1_258_.aFloat5589 * class373_sub1.aFloat5589));
							Class125.aFloat1287 = ((class373_sub1.aFloat5586 * class373_sub1_258_.aFloat5589)
									+ ((class373_sub1_258_.aFloat5581 * class373_sub1.aFloat5584)
											+ (class373_sub1_258_.aFloat5586 * class373_sub1.aFloat5583)));
							Class296_Sub29.aFloat4811 = ((class373_sub1_258_.aFloat5587 * class373_sub1.aFloat5581)
									+ ((class373_sub1_258_.aFloat5577 * class373_sub1.aFloat5577)
											+ (class373_sub1_258_.aFloat5584 * class373_sub1.aFloat5585)));
							Class355_Sub1.aFloat3695 = ((class373_sub1_258_.aFloat5577 * class373_sub1.aFloat5587)
									+ (class373_sub1.aFloat5579 * class373_sub1_258_.aFloat5584)
									+ (class373_sub1_258_.aFloat5587 * class373_sub1.aFloat5589));
							Class32_Sub1.aFloat5650 = ((class373_sub1_258_.aFloat5589 * class373_sub1.aFloat5581)
									+ ((class373_sub1.aFloat5585 * class373_sub1_258_.aFloat5586)
											+ (class373_sub1.aFloat5577 * class373_sub1_258_.aFloat5581)));
						}
						if (class338_sub5 != null) {
							int i_270_ = aShort4400 + aShort4409 >> 1;
							int i_271_ = aShort4401 + aShort4435 >> 1;
							int i_272_ = (int) (((float) aShort4402 * Class284.aFloat2620)
									+ (InvisiblePlayer.aFloat1976 + ((float) i_270_ * Class296_Sub29.aFloat4811))
									+ (Class32_Sub1.aFloat5650 * (float) i_271_));
							int i_273_ = (int) (Class125.aFloat1287 * (float) i_271_
									+ (((float) aShort4402 * Class126.aFloat1292)
											+ (Class296_Sub55.aFloat5073 + ((float) i_270_ * Class52.aFloat637))));
							int i_274_ = (int) ((Class296_Sub27.aFloat4787 * (float) i_271_)
									+ (((float) aShort4402 * Class211.aFloat2102)
											+ (FileOnDisk.aFloat674 + (Class355_Sub1.aFloat3695 * (float) i_270_))));
							int i_275_ = (int) ((Class284.aFloat2620 * (float) aShort4399)
									+ (InvisiblePlayer.aFloat1976 + (Class296_Sub29.aFloat4811 * (float) i_270_))
									+ (Class32_Sub1.aFloat5650 * (float) i_271_));
							int i_276_ = (int) ((Class126.aFloat1292 * (float) aShort4399)
									+ ((float) i_270_ * Class52.aFloat637 + Class296_Sub55.aFloat5073)
									+ ((float) i_271_ * Class125.aFloat1287));
							class338_sub5.anInt5225 = (aHa_Sub3_4415.anInt4263 * i_272_ / i + aHa_Sub3_4415.anInt4215);
							class338_sub5.anInt5221 = (aHa_Sub3_4415.anInt4227 * i_276_ / i + aHa_Sub3_4415.anInt4246);
							class338_sub5.anInt5223 = (i_273_ * aHa_Sub3_4415.anInt4227 / i + aHa_Sub3_4415.anInt4246);
							int i_277_ = (int) (((float) aShort4399 * Class211.aFloat2102)
									+ (FileOnDisk.aFloat674 + (Class355_Sub1.aFloat3695 * (float) i_270_))
									+ (Class296_Sub27.aFloat4787 * (float) i_271_));
							class338_sub5.anInt5222 = (aHa_Sub3_4415.anInt4215 + i_275_ * aHa_Sub3_4415.anInt4263 / i);
							if (aHa_Sub3_4415.anInt4249 <= i_274_ || i_277_ >= aHa_Sub3_4415.anInt4249) {
								class338_sub5.anInt5226 = (-class338_sub5.anInt5225
										+ (((aShort4434 + i_272_) * aHa_Sub3_4415.anInt4263 / i)
												+ aHa_Sub3_4415.anInt4215));
								class338_sub5.aBoolean5224 = true;
							}
						}
						aHa_Sub3_4415.method1308(-106, (float) i);
						aHa_Sub3_4415.method1302(8448);
						aHa_Sub3_4415.method1276(class373_sub1_258_, (byte) -114);
						method1783(0);
						aHa_Sub3_4415.method1288(7330);
						method1782(4);
					}
				}
			}
		}
	}

	public void a(int i) {
		int i_278_ = Class296_Sub4.anIntArray4598[i];
		int i_279_ = Class296_Sub4.anIntArray4618[i];
		for (int i_280_ = 0; i_280_ < anInt4437; i_280_++) {
			int i_281_ = ((anIntArray4406[i_280_] * i_279_ + i_278_ * anIntArray4422[i_280_]) >> 14);
			anIntArray4422[i_280_] = (i_279_ * anIntArray4422[i_280_] - i_278_ * anIntArray4406[i_280_]) >> 14;
			anIntArray4406[i_280_] = i_281_;
		}
		aBoolean4440 = false;
		if (aClass272_4438 != null)
			aClass272_4438.anInterface5_2521 = null;
	}

	static public void method1785(int i, int i_282_) {
		Class296_Sub51_Sub5.anInt6365 = -1;
		if (i < 82)
			method1785(-114, 25);
		BITConfigDefinition.anInt2604 = -1;
		Class69.anInt3688 = i_282_;
		Class296_Sub51_Sub25.method3146((byte) -10);
	}

	public boolean r() {
		return aBoolean4408;
	}

	public void C(int i) {
		aShort4444 = (short) i;
		if (aClass272_4446 != null)
			aClass272_4446.anInterface5_2521 = null;
	}

	public void O(int i, int i_283_, int i_284_) {
		for (int i_285_ = 0; anInt4437 > i_285_; i_285_++) {
			if (i != 128)
				anIntArray4406[i_285_] = anIntArray4406[i_285_] * i >> 7;
			if (i_283_ != 128)
				anIntArray4450[i_285_] = i_283_ * anIntArray4450[i_285_] >> 7;
			if (i_284_ != 128)
				anIntArray4422[i_285_] = i_284_ * anIntArray4422[i_285_] >> 7;
		}
		aBoolean4440 = false;
		if (aClass272_4438 != null)
			aClass272_4438.anInterface5_2521 = null;
	}

	public void P(int i, int i_286_, int i_287_, int i_288_) {
		if (i == 0) {
			Class41_Sub20.anInt3796 = 0;
			int i_289_ = 0;
			Class288.anInt2647 = 0;
			Class368.anInt3132 = 0;
			for (int i_290_ = 0; i_290_ < anInt4437; i_290_++) {
				Class288.anInt2647 += anIntArray4406[i_290_];
				Class41_Sub20.anInt3796 += anIntArray4450[i_290_];
				i_289_++;
				Class368.anInt3132 = Class368.anInt3132 + anIntArray4422[i_290_];
			}
			if (i_289_ <= 0) {
				Class368.anInt3132 = i_288_;
				Class288.anInt2647 = i_286_;
				Class41_Sub20.anInt3796 = i_287_;
			} else {
				Class41_Sub20.anInt3796 = i_287_ + Class41_Sub20.anInt3796 / i_289_;
				Class288.anInt2647 = i_286_ + Class288.anInt2647 / i_289_;
				Class368.anInt3132 = Class368.anInt3132 / i_289_ + i_288_;
			}
		} else if (i == 1) {
			for (int i_291_ = 0; i_291_ < anInt4437; i_291_++) {
				anIntArray4406[i_291_] += i_286_;
				anIntArray4450[i_291_] += i_287_;
				anIntArray4422[i_291_] += i_288_;
			}
		} else if (i == 2) {
			for (int i_292_ = 0; i_292_ < anInt4437; i_292_++) {
				anIntArray4406[i_292_] -= Class288.anInt2647;
				anIntArray4450[i_292_] -= Class41_Sub20.anInt3796;
				anIntArray4422[i_292_] -= Class368.anInt3132;
				if (i_288_ != 0) {
					int i_293_ = Class296_Sub4.anIntArray4598[i_288_];
					int i_294_ = Class296_Sub4.anIntArray4618[i_288_];
					int i_295_ = ((i_293_ * anIntArray4450[i_292_] + i_294_ * anIntArray4406[i_292_] + 16383) >> 14);
					anIntArray4450[i_292_] = ((i_294_ * anIntArray4450[i_292_]
							+ (-(anIntArray4406[i_292_] * i_293_) + 16383)) >> 14);
					anIntArray4406[i_292_] = i_295_;
				}
				if (i_286_ != 0) {
					int i_296_ = Class296_Sub4.anIntArray4598[i_286_];
					int i_297_ = Class296_Sub4.anIntArray4618[i_286_];
					int i_298_ = ((-(i_296_ * anIntArray4422[i_292_])
							+ (anIntArray4450[i_292_] * i_297_ + 16383)) >> 14);
					anIntArray4422[i_292_] = (i_296_ * anIntArray4450[i_292_]
							+ (anIntArray4422[i_292_] * i_297_ + 16383)) >> 14;
					anIntArray4450[i_292_] = i_298_;
				}
				if (i_287_ != 0) {
					int i_299_ = Class296_Sub4.anIntArray4598[i_287_];
					int i_300_ = Class296_Sub4.anIntArray4618[i_287_];
					int i_301_ = ((anIntArray4422[i_292_] * i_299_
							- (-(i_300_ * anIntArray4406[i_292_]) - 16383)) >> 14);
					anIntArray4422[i_292_] = (-(i_299_ * anIntArray4406[i_292_])
							+ (anIntArray4422[i_292_] * i_300_ + 16383)) >> 14;
					anIntArray4406[i_292_] = i_301_;
				}
				anIntArray4406[i_292_] += Class288.anInt2647;
				anIntArray4450[i_292_] += Class41_Sub20.anInt3796;
				anIntArray4422[i_292_] += Class368.anInt3132;
			}
		} else if (i == 3) {
			for (int i_302_ = 0; anInt4437 > i_302_; i_302_++) {
				anIntArray4406[i_302_] -= Class288.anInt2647;
				anIntArray4450[i_302_] -= Class41_Sub20.anInt3796;
				anIntArray4422[i_302_] -= Class368.anInt3132;
				anIntArray4406[i_302_] = i_286_ * anIntArray4406[i_302_] / 128;
				anIntArray4450[i_302_] = anIntArray4450[i_302_] * i_287_ / 128;
				anIntArray4422[i_302_] = i_288_ * anIntArray4422[i_302_] / 128;
				anIntArray4406[i_302_] += Class288.anInt2647;
				anIntArray4450[i_302_] += Class41_Sub20.anInt3796;
				anIntArray4422[i_302_] += Class368.anInt3132;
			}
		} else if (i == 5) {
			for (int i_303_ = 0; anInt4433 > i_303_; i_303_++) {
				int i_304_ = (aByteArray4426[i_303_] & 0xff) + i_286_ * 8;
				if (i_304_ >= 0) {
					if (i_304_ > 255)
						i_304_ = 255;
				} else
					i_304_ = 0;
				aByteArray4426[i_303_] = (byte) i_304_;
			}
			if (aClass272_4446 != null)
				aClass272_4446.anInterface5_2521 = null;
			if (aClass354Array4405 != null) {
				for (int i_305_ = 0; i_305_ < anInt4418; i_305_++) {
					Class354 class354 = aClass354Array4405[i_305_];
					Class331 class331 = aClass331Array4392[i_305_];
					class331.anInt2938 = (class331.anInt2938 & 0xffffff
							| (-(aByteArray4426[class354.anInt3054] & 0xff) + 255) << 24);
				}
			}
		} else if (i == 7) {
			for (int i_306_ = 0; anInt4433 > i_306_; i_306_++) {
				int i_307_ = aShortArray4394[i_306_] & 0xffff;
				int i_308_ = i_307_ >> 10 & 0x3f;
				int i_309_ = i_307_ >> 7 & 0x7;
				i_308_ = i_308_ + i_286_ & 0x3f;
				i_309_ += i_287_ / 4;
				int i_310_ = i_307_ & 0x7f;
				if (i_309_ >= 0) {
					if (i_309_ > 7)
						i_309_ = 7;
				} else
					i_309_ = 0;
				i_310_ += i_288_;
				if (i_310_ >= 0) {
					if (i_310_ > 127)
						i_310_ = 127;
				} else
					i_310_ = 0;
				aShortArray4394[i_306_] = (short) Class48.bitOR(Class48.bitOR(i_308_ << 10, i_309_ << 7), i_310_);
			}
			if (aClass272_4446 != null)
				aClass272_4446.anInterface5_2521 = null;
			if (aClass354Array4405 != null) {
				for (int i_311_ = 0; i_311_ < anInt4418; i_311_++) {
					Class354 class354 = aClass354Array4405[i_311_];
					Class331 class331 = aClass331Array4392[i_311_];
					class331.anInt2938 = (class331.anInt2938 & ~0xffffff
							| ((Class166_Sub1.anIntArray4300[aShortArray4394[class354.anInt3054] & 0xffff])
									& 0xffffff));
				}
			}
		} else if (i == 8) {
			for (int i_312_ = 0; anInt4418 > i_312_; i_312_++) {
				Class331 class331 = aClass331Array4392[i_312_];
				class331.anInt2937 += i_287_;
				class331.anInt2934 += i_286_;
			}
		} else if (i == 10) {
			for (int i_313_ = 0; anInt4418 > i_313_; i_313_++) {
				Class331 class331 = aClass331Array4392[i_313_];
				class331.anInt2936 = i_287_ * class331.anInt2936 >> 7;
				class331.anInt2935 = i_286_ * class331.anInt2935 >> 7;
			}
		} else if (i == 9) {
			for (int i_314_ = 0; anInt4418 > i_314_; i_314_++) {
				Class331 class331 = aClass331Array4392[i_314_];
				class331.anInt2933 = i_286_ + class331.anInt2933 & 0x3fff;
			}
		}
	}

	public int ua() {
		return anInt4412;
	}

	public void method1726(Class373 class373, int i, boolean bool) {
		if (aShortArray4404 != null) {
			int[] is = new int[3];
			for (int i_315_ = 0; anInt4437 > i_315_; i_315_++) {
				if ((aShortArray4404[i_315_] & i) != 0) {
					if (bool)
						class373.method3905(anIntArray4406[i_315_], anIntArray4450[i_315_], anIntArray4422[i_315_], is);
					else
						class373.method3903(anIntArray4406[i_315_], anIntArray4450[i_315_], anIntArray4422[i_315_], is);
					anIntArray4406[i_315_] = is[0];
					anIntArray4450[i_315_] = is[1];
					anIntArray4422[i_315_] = is[2];
				}
			}
		}
	}

	public r ba(r var_r) {
		if (anInt4439 == 0)
			return null;
		if (!aBoolean4440)
			method1793((byte) 107);
		int i;
		int i_316_;
		if (aHa_Sub3_4415.anInt4212 > 0) {
			i = (aShort4400 - (aShort4399 * aHa_Sub3_4415.anInt4212 >> 8) >> aHa_Sub3_4415.anInt4139);
			i_316_ = (aShort4409 - (aShort4402 * aHa_Sub3_4415.anInt4212 >> 8) >> aHa_Sub3_4415.anInt4139);
		} else {
			i = (aShort4400 - (aShort4402 * aHa_Sub3_4415.anInt4212 >> 8) >> aHa_Sub3_4415.anInt4139);
			i_316_ = (aShort4409 - (aHa_Sub3_4415.anInt4212 * aShort4399 >> 8) >> aHa_Sub3_4415.anInt4139);
		}
		int i_317_;
		int i_318_;
		if (aHa_Sub3_4415.anInt4179 > 0) {
			i_317_ = (aShort4401 - (aHa_Sub3_4415.anInt4179 * aShort4399 >> 8) >> aHa_Sub3_4415.anInt4139);
			i_318_ = (aShort4435 - (aShort4402 * aHa_Sub3_4415.anInt4179 >> 8) >> aHa_Sub3_4415.anInt4139);
		} else {
			i_317_ = (-(aHa_Sub3_4415.anInt4179 * aShort4402 >> 8) + aShort4401 >> aHa_Sub3_4415.anInt4139);
			i_318_ = (-(aShort4399 * aHa_Sub3_4415.anInt4179 >> 8) + aShort4435 >> aHa_Sub3_4415.anInt4139);
		}
		int i_319_ = -i + i_316_ + 1;
		int i_320_ = i_318_ - i_317_ + 1;
		r_Sub1 var_r_Sub1 = (r_Sub1) var_r;
		r_Sub1 var_r_Sub1_321_;
		if (var_r_Sub1 != null && var_r_Sub1.method2861(i_320_, i_319_, (byte) 68)) {
			var_r_Sub1_321_ = var_r_Sub1;
			var_r_Sub1_321_.method2862((byte) 71);
		} else
			var_r_Sub1_321_ = new r_Sub1(aHa_Sub3_4415, i_319_, i_320_);
		var_r_Sub1_321_.method2865(i, i_317_, i_316_, i_318_, -111);
		method1780(var_r_Sub1_321_, (byte) 74);
		return var_r_Sub1_321_;
	}

	private void method1786(boolean bool, boolean bool_322_) {
		boolean bool_323_ = (aClass272_4446 != null && aClass272_4446.anInterface5_2521 == null);
		boolean bool_324_ = (aClass272_4428 != null && aClass272_4428.anInterface5_2521 == null);
		boolean bool_325_ = (aClass272_4438 != null && aClass272_4438.anInterface5_2521 == null);
		boolean bool_326_ = (aClass272_4432 != null && aClass272_4432.anInterface5_2521 == null);
		if (bool_322_) {
			bool_323_ = bool_323_ & (aByte4445 & 0x2) != 0;
			bool_325_ = bool_325_ & (aByte4445 & 0x1) != 0;
			bool_326_ = bool_326_ & (aByte4445 & 0x8) != 0;
			bool_324_ = bool_324_ & (aByte4445 & 0x4) != 0;
		}
		byte i = 0;
		byte i_327_ = 0;
		byte i_328_ = 0;
		byte i_329_ = 0;
		byte i_330_ = 0;
		if (bool_325_) {
			i_327_ = i;
			i += 12;
		}
		if (bool_323_) {
			i_328_ = i;
			i += 4;
		}
		if (bool_324_) {
			i_329_ = i;
			i += 12;
		}
		if (bool_326_) {
			i_330_ = i;
			i += 8;
		}
		if (i != 0) {
			if (i * anInt4439 <= (aHa_Sub3_4415.aClass296_Sub17_Sub2_4194.data).length)
				aHa_Sub3_4415.aClass296_Sub17_Sub2_4194.pos = 0;
			else
				aHa_Sub3_4415.aClass296_Sub17_Sub2_4194 = new Class296_Sub17_Sub2(i * (anInt4439 + 100));
			Class296_Sub17_Sub2 class296_sub17_sub2 = aHa_Sub3_4415.aClass296_Sub17_Sub2_4194;
			if (bool_325_) {
				if (!aHa_Sub3_4415.aBoolean4184) {
					for (int i_331_ = 0; i_331_ < anInt4437; i_331_++) {
						int i_332_ = Stream.floatToRawIntBits((float) anIntArray4406[i_331_]);
						int i_333_ = Stream.floatToRawIntBits((float) anIntArray4450[i_331_]);
						int i_334_ = Stream.floatToRawIntBits((float) anIntArray4422[i_331_]);
						int i_335_ = anIntArray4395[i_331_];
						int i_336_ = anIntArray4395[i_331_ + 1];
						for (int i_337_ = i_335_; i_336_ > i_337_; i_337_++) {
							int i_338_ = aShortArray4414[i_337_] - 1;
							if (i_338_ == -1)
								break;
							class296_sub17_sub2.pos = i_338_ * i;
							class296_sub17_sub2.method2580(i_332_);
							class296_sub17_sub2.method2580(i_333_);
							class296_sub17_sub2.method2580(i_334_);
						}
					}
				} else {
					for (int i_339_ = 0; i_339_ < anInt4437; i_339_++) {
						int i_340_ = Stream.floatToRawIntBits((float) anIntArray4406[i_339_]);
						int i_341_ = Stream.floatToRawIntBits((float) anIntArray4450[i_339_]);
						int i_342_ = Stream.floatToRawIntBits((float) anIntArray4422[i_339_]);
						int i_343_ = anIntArray4395[i_339_];
						int i_344_ = anIntArray4395[i_339_ + 1];
						for (int i_345_ = i_343_; i_344_ > i_345_; i_345_++) {
							int i_346_ = aShortArray4414[i_345_] - 1;
							if (i_346_ == -1)
								break;
							class296_sub17_sub2.pos = i * i_346_;
							class296_sub17_sub2.p4(i_340_);
							class296_sub17_sub2.p4(i_341_);
							class296_sub17_sub2.p4(i_342_);
						}
					}
				}
			}
			if (bool_323_) {
				if (aClass272_4428 == null) {
					short[] is;
					short[] is_347_;
					byte[] is_348_;
					short[] is_349_;
					if (aClass47_4423 != null) {
						is_348_ = aClass47_4423.aByteArray450;
						is_347_ = aClass47_4423.aShortArray453;
						is = aClass47_4423.aShortArray445;
						is_349_ = aClass47_4423.aShortArray446;
					} else {
						is = aShortArray4420;
						is_347_ = aShortArray4448;
						is_348_ = aByteArray4425;
						is_349_ = aShortArray4413;
					}
					float f = aHa_Sub3_4415.aFloatArray4202[0];
					float f_350_ = aHa_Sub3_4415.aFloatArray4202[1];
					float f_351_ = aHa_Sub3_4415.aFloatArray4202[2];
					float f_352_ = aHa_Sub3_4415.aFloat4217;
					float f_353_ = (aHa_Sub3_4415.aFloat4272 * 768.0F / (float) aShort4411);
					float f_354_ = (aHa_Sub3_4415.aFloat4248 * 768.0F / (float) aShort4411);
					for (int i_355_ = 0; anInt4433 > i_355_; i_355_++) {
						int i_356_ = method1784(aShortArray4394[i_355_], aShort4444, aShortArray4416[i_355_],
								aByteArray4426[i_355_], (byte) 94);
						int i_357_ = aShortArray4442[i_355_];
						float f_358_ = ((float) (i_356_ >>> 24) * aHa_Sub3_4415.aFloat4261);
						float f_359_ = ((float) ((i_356_ & 0xffe5) >> 8) * aHa_Sub3_4415.aFloat4193);
						float f_360_ = ((float) ((i_356_ & 0xffaecd) >> 16) * aHa_Sub3_4415.aFloat4186);
						short i_361_ = (short) is_348_[i_357_];
						float f_362_;
						if (i_361_ != 0)
							f_362_ = (((float) is_349_[i_357_] * f_351_
									+ ((float) is_347_[i_357_] * f_350_ + f * (float) is[i_357_]))
									/ (float) (i_361_ * 256));
						else
							f_362_ = ((f_351_ * (float) is_349_[i_357_]
									+ ((float) is_347_[i_357_] * f_350_ + (float) is[i_357_] * f)) * 0.0026041667F);
						float f_363_ = (f_362_ * (f_362_ < 0.0F ? f_354_ : f_353_) + f_352_);
						int i_364_ = (int) (f_363_ * f_358_);
						int i_365_ = (int) (f_363_ * f_360_);
						if (i_364_ < 0)
							i_364_ = 0;
						else if (i_364_ > 255)
							i_364_ = 255;
						int i_366_ = (int) (f_363_ * f_359_);
						if (i_365_ >= 0) {
							if (i_365_ > 255)
								i_365_ = 255;
						} else
							i_365_ = 0;
						if (i_366_ < 0)
							i_366_ = 0;
						else if (i_366_ > 255)
							i_366_ = 255;
						class296_sub17_sub2.pos = i_328_ + i_357_ * i;
						class296_sub17_sub2.p1(i_364_);
						class296_sub17_sub2.p1(i_365_);
						class296_sub17_sub2.p1(i_366_);
						class296_sub17_sub2.p1(255 - ((aByteArray4426[i_355_]) & 0xff));
						i_357_ = aShortArray4393[i_355_];
						i_361_ = (short) is_348_[i_357_];
						if (i_361_ == 0)
							f_362_ = ((f_351_ * (float) is_349_[i_357_]
									+ ((float) is_347_[i_357_] * f_350_ + f * (float) is[i_357_])) * 0.0026041667F);
						else
							f_362_ = (((float) is_347_[i_357_] * f_350_ + f * (float) is[i_357_]
									+ (float) is_349_[i_357_] * f_351_) / (float) (i_361_ * 256));
						f_363_ = f_352_ + ((f_362_ < 0.0F ? f_354_ : f_353_) * f_362_);
						i_364_ = (int) (f_358_ * f_363_);
						if (i_364_ >= 0) {
							if (i_364_ > 255)
								i_364_ = 255;
						} else
							i_364_ = 0;
						i_365_ = (int) (f_360_ * f_363_);
						if (i_365_ < 0)
							i_365_ = 0;
						else if (i_365_ > 255)
							i_365_ = 255;
						i_366_ = (int) (f_363_ * f_359_);
						if (i_366_ < 0)
							i_366_ = 0;
						else if (i_366_ > 255)
							i_366_ = 255;
						class296_sub17_sub2.pos = i_357_ * i + i_328_;
						class296_sub17_sub2.p1(i_364_);
						class296_sub17_sub2.p1(i_365_);
						class296_sub17_sub2.p1(i_366_);
						class296_sub17_sub2.p1(-(aByteArray4426[i_355_] & 0xff) + 255);
						i_357_ = aShortArray4390[i_355_];
						i_361_ = (short) is_348_[i_357_];
						if (i_361_ == 0)
							f_362_ = ((f_351_ * (float) is_349_[i_357_]
									+ ((float) is[i_357_] * f + (float) is_347_[i_357_] * f_350_)) * 0.0026041667F);
						else
							f_362_ = (((float) is_349_[i_357_] * f_351_
									+ (f * (float) is[i_357_] + (float) is_347_[i_357_] * f_350_))
									/ (float) (i_361_ * 256));
						f_363_ = f_352_ + ((!(f_362_ < 0.0F) ? f_353_ : f_354_) * f_362_);
						i_364_ = (int) (f_363_ * f_358_);
						i_365_ = (int) (f_363_ * f_360_);
						if (i_364_ < 0)
							i_364_ = 0;
						else if (i_364_ > 255)
							i_364_ = 255;
						i_366_ = (int) (f_359_ * f_363_);
						if (i_365_ < 0)
							i_365_ = 0;
						else if (i_365_ > 255)
							i_365_ = 255;
						class296_sub17_sub2.pos = i_328_ + i_357_ * i;
						if (i_366_ >= 0) {
							if (i_366_ > 255)
								i_366_ = 255;
						} else
							i_366_ = 0;
						class296_sub17_sub2.p1(i_364_);
						class296_sub17_sub2.p1(i_365_);
						class296_sub17_sub2.p1(i_366_);
						class296_sub17_sub2.p1(-(aByteArray4426[i_355_] & 0xff) + 255);
					}
				} else {
					for (int i_367_ = 0; i_367_ < anInt4433; i_367_++) {
						int i_368_ = method1784(aShortArray4394[i_367_], aShort4444, aShortArray4416[i_367_],
								aByteArray4426[i_367_], (byte) 94);
						class296_sub17_sub2.pos = i * aShortArray4442[i_367_] + i_328_;
						class296_sub17_sub2.p4(i_368_);
						class296_sub17_sub2.pos = i * aShortArray4393[i_367_] + i_328_;
						class296_sub17_sub2.p4(i_368_);
						class296_sub17_sub2.pos = i * aShortArray4390[i_367_] + i_328_;
						class296_sub17_sub2.p4(i_368_);
					}
				}
			}
			if (bool_324_) {
				short[] is;
				short[] is_369_;
				short[] is_370_;
				byte[] is_371_;
				if (aClass47_4423 == null) {
					is_371_ = aByteArray4425;
					is_369_ = aShortArray4420;
					is = aShortArray4448;
					is_370_ = aShortArray4413;
				} else {
					is = aClass47_4423.aShortArray453;
					is_369_ = aClass47_4423.aShortArray445;
					is_370_ = aClass47_4423.aShortArray446;
					is_371_ = aClass47_4423.aByteArray450;
				}
				float f = 3.0F / (float) aShort4411;
				class296_sub17_sub2.pos = i_329_;
				float f_372_ = 3.0F / (float) (aShort4411 + aShort4411 / 2);
				if (aHa_Sub3_4415.aBoolean4184) {
					for (int i_373_ = 0; i_373_ < anInt4439; i_373_++) {
						int i_374_ = is_371_[i_373_] & 0xff;
						if (i_374_ == 0) {
							class296_sub17_sub2.method2636((byte) 117, ((float) (is_369_[i_373_]) * f_372_));
							class296_sub17_sub2.method2636((byte) 104, f_372_ * (float) is[i_373_]);
							class296_sub17_sub2.method2636((byte) 119, ((float) (is_370_[i_373_]) * f_372_));
						} else {
							float f_375_ = f / (float) i_374_;
							class296_sub17_sub2.method2636((byte) 57, ((float) (is_369_[i_373_]) * f_375_));
							class296_sub17_sub2.method2636((byte) 65, ((float) is[i_373_] * f_375_));
							class296_sub17_sub2.method2636((byte) 51, f_375_ * (float) is_370_[i_373_]);
						}
						class296_sub17_sub2.pos += i - 12;
					}
				} else {
					for (int i_376_ = 0; anInt4439 > i_376_; i_376_++) {
						int i_377_ = is_371_[i_376_] & 0xff;
						if (i_377_ != 0) {
							float f_378_ = f / (float) i_377_;
							class296_sub17_sub2.method2637((byte) -124, ((float) (is_369_[i_376_]) * f_378_));
							class296_sub17_sub2.method2637((byte) -119, ((float) is[i_376_] * f_378_));
							class296_sub17_sub2.method2637((byte) -118, ((float) (is_370_[i_376_]) * f_378_));
						} else {
							class296_sub17_sub2.method2637((byte) -85, ((float) (is_369_[i_376_]) * f_372_));
							class296_sub17_sub2.method2637((byte) -90, ((float) is[i_376_] * f_372_));
							class296_sub17_sub2.method2637((byte) -105, f_372_ * (float) is_370_[i_376_]);
						}
						class296_sub17_sub2.pos += i - 12;
					}
				}
			}
			if (bool_326_) {
				class296_sub17_sub2.pos = i_330_;
				if (aHa_Sub3_4415.aBoolean4184) {
					for (int i_379_ = 0; i_379_ < anInt4439; i_379_++) {
						class296_sub17_sub2.method2636((byte) 104, aFloatArray4410[i_379_]);
						class296_sub17_sub2.method2636((byte) 73, aFloatArray4398[i_379_]);
						class296_sub17_sub2.pos += i - 8;
					}
				} else {
					for (int i_380_ = 0; i_380_ < anInt4439; i_380_++) {
						class296_sub17_sub2.method2637((byte) -78, aFloatArray4410[i_380_]);
						class296_sub17_sub2.method2637((byte) -119, aFloatArray4398[i_380_]);
						class296_sub17_sub2.pos += i - 8;
					}
				}
			}
			class296_sub17_sub2.pos = anInt4439 * i;
			Interface5 interface5;
			if (!bool_322_) {
				interface5 = aHa_Sub3_4415.method1321(class296_sub17_sub2.pos, false, i, (class296_sub17_sub2.data),
						5888);
				aBoolean4430 = true;
			} else {
				if (anInterface5_4424 != null)
					anInterface5_4424.method25((class296_sub17_sub2.data), (byte) -104, i, class296_sub17_sub2.pos);
				else
					anInterface5_4424 = aHa_Sub3_4415.method1321((class296_sub17_sub2.pos), true, i,
							(class296_sub17_sub2.data), 5888);
				aByte4445 = (byte) 0;
				interface5 = anInterface5_4424;
			}
			if (bool_325_) {
				aClass272_4438.anInterface5_2521 = interface5;
				aClass272_4438.aByte2520 = i_327_;
			}
			if (bool_326_) {
				aClass272_4432.aByte2520 = i_330_;
				aClass272_4432.anInterface5_2521 = interface5;
			}
			if (bool_323_) {
				aClass272_4446.aByte2520 = i_328_;
				aClass272_4446.anInterface5_2521 = interface5;
			}
			if (bool_324_) {
				aClass272_4428.anInterface5_2521 = interface5;
				aClass272_4428.aByte2520 = i_329_;
			}
			if (bool)
				method1791((byte) -9);
		}
	}

	private boolean method1787(int i, int i_381_, int i_382_, int i_383_, int i_384_, int i_385_, int i_386_,
			int i_387_, int i_388_) {
		if (i_385_ > i && i_381_ > i && i_382_ > i)
			return false;
		if (i > i_385_ && i_381_ < i && i > i_382_)
			return false;
		if (i_383_ < i_388_ && i_383_ < i_386_ && i_383_ < i_387_)
			return false;
		if (i_384_ != 3)
			aShort4419 = (short) 41;
		if (i_383_ > i_388_ && i_386_ < i_383_ && i_383_ > i_387_)
			return false;
		return true;
	}

	public int V() {
		if (!aBoolean4440)
			method1793((byte) -14);
		return aShort4400;
	}

	public void method1725(int i, int[] is, int i_389_, int i_390_, int i_391_, int i_392_, boolean bool) {
		int i_393_ = is.length;
		if (i == 0) {
			i_390_ <<= 4;
			i_389_ <<= 4;
			i_391_ <<= 4;
			Class288.anInt2647 = 0;
			Class41_Sub20.anInt3796 = 0;
			int i_394_ = 0;
			Class368.anInt3132 = 0;
			for (int i_395_ = 0; i_395_ < i_393_; i_395_++) {
				int i_396_ = is[i_395_];
				if (i_396_ < anIntArrayArray4429.length) {
					int[] is_397_ = anIntArrayArray4429[i_396_];
					for (int i_398_ = 0; is_397_.length > i_398_; i_398_++) {
						int i_399_ = is_397_[i_398_];
						Class288.anInt2647 += anIntArray4406[i_399_];
						Class41_Sub20.anInt3796 += anIntArray4450[i_399_];
						Class368.anInt3132 += anIntArray4422[i_399_];
						i_394_++;
					}
				}
			}
			if (i_394_ > 0) {
				Class288.anInt2647 = Class288.anInt2647 / i_394_ + i_389_;
				Class41_Sub20.anInt3796 = i_390_ + Class41_Sub20.anInt3796 / i_394_;
				Class368.anInt3132 = Class368.anInt3132 / i_394_ + i_391_;
			} else {
				Class41_Sub20.anInt3796 = i_390_;
				Class368.anInt3132 = i_391_;
				Class288.anInt2647 = i_389_;
			}
		} else if (i == 1) {
			i_391_ <<= 4;
			i_389_ <<= 4;
			i_390_ <<= 4;
			for (int i_400_ = 0; i_393_ > i_400_; i_400_++) {
				int i_401_ = is[i_400_];
				if (anIntArrayArray4429.length > i_401_) {
					int[] is_402_ = anIntArrayArray4429[i_401_];
					for (int i_403_ = 0; i_403_ < is_402_.length; i_403_++) {
						int i_404_ = is_402_[i_403_];
						anIntArray4406[i_404_] += i_389_;
						anIntArray4450[i_404_] += i_390_;
						anIntArray4422[i_404_] += i_391_;
					}
				}
			}
		} else if (i == 2) {
			for (int i_405_ = 0; i_405_ < i_393_; i_405_++) {
				int i_406_ = is[i_405_];
				if (anIntArrayArray4429.length > i_406_) {
					int[] is_407_ = anIntArrayArray4429[i_406_];
					if ((i_392_ & 0x1) == 0) {
						for (int i_408_ = 0; is_407_.length > i_408_; i_408_++) {
							int i_409_ = is_407_[i_408_];
							anIntArray4406[i_409_] -= Class288.anInt2647;
							anIntArray4450[i_409_] -= Class41_Sub20.anInt3796;
							anIntArray4422[i_409_] -= Class368.anInt3132;
							if (i_391_ != 0) {
								int i_410_ = Class296_Sub4.anIntArray4598[i_391_];
								int i_411_ = Class296_Sub4.anIntArray4618[i_391_];
								int i_412_ = ((i_411_ * anIntArray4406[i_409_] + 16383
										+ i_410_ * anIntArray4450[i_409_]) >> 14);
								anIntArray4450[i_409_] = (i_411_ * anIntArray4450[i_409_]
										- i_410_ * anIntArray4406[i_409_] + 16383) >> 14;
								anIntArray4406[i_409_] = i_412_;
							}
							if (i_389_ != 0) {
								int i_413_ = Class296_Sub4.anIntArray4598[i_389_];
								int i_414_ = Class296_Sub4.anIntArray4618[i_389_];
								int i_415_ = ((-(i_413_ * anIntArray4422[i_409_])
										+ (anIntArray4450[i_409_] * i_414_ + 16383)) >> 14);
								anIntArray4422[i_409_] = (i_413_ * anIntArray4450[i_409_]
										+ anIntArray4422[i_409_] * i_414_ + 16383) >> 14;
								anIntArray4450[i_409_] = i_415_;
							}
							if (i_390_ != 0) {
								int i_416_ = Class296_Sub4.anIntArray4598[i_390_];
								int i_417_ = Class296_Sub4.anIntArray4618[i_390_];
								int i_418_ = (i_417_ * anIntArray4406[i_409_] + i_416_ * anIntArray4422[i_409_]
										+ 16383) >> 14;
								anIntArray4422[i_409_] = (i_417_ * anIntArray4422[i_409_]
										- anIntArray4406[i_409_] * i_416_ + 16383) >> 14;
								anIntArray4406[i_409_] = i_418_;
							}
							anIntArray4406[i_409_] += Class288.anInt2647;
							anIntArray4450[i_409_] += Class41_Sub20.anInt3796;
							anIntArray4422[i_409_] += Class368.anInt3132;
						}
					} else {
						for (int i_419_ = 0; i_419_ < is_407_.length; i_419_++) {
							int i_420_ = is_407_[i_419_];
							anIntArray4406[i_420_] -= Class288.anInt2647;
							anIntArray4450[i_420_] -= Class41_Sub20.anInt3796;
							anIntArray4422[i_420_] -= Class368.anInt3132;
							if (i_389_ != 0) {
								int i_421_ = Class296_Sub4.anIntArray4598[i_389_];
								int i_422_ = Class296_Sub4.anIntArray4618[i_389_];
								int i_423_ = (anIntArray4450[i_420_] * i_422_ - anIntArray4422[i_420_] * i_421_
										+ 16383) >> 14;
								anIntArray4422[i_420_] = (i_422_ * anIntArray4422[i_420_]
										+ anIntArray4450[i_420_] * i_421_ + 16383) >> 14;
								anIntArray4450[i_420_] = i_423_;
							}
							if (i_391_ != 0) {
								int i_424_ = Class296_Sub4.anIntArray4598[i_391_];
								int i_425_ = Class296_Sub4.anIntArray4618[i_391_];
								int i_426_ = (i_425_ * anIntArray4406[i_420_] + i_424_ * anIntArray4450[i_420_]
										+ 16383) >> 14;
								anIntArray4450[i_420_] = (anIntArray4450[i_420_] * i_425_
										+ (-(anIntArray4406[i_420_] * i_424_) + 16383)) >> 14;
								anIntArray4406[i_420_] = i_426_;
							}
							if (i_390_ != 0) {
								int i_427_ = Class296_Sub4.anIntArray4598[i_390_];
								int i_428_ = Class296_Sub4.anIntArray4618[i_390_];
								int i_429_ = (i_428_ * anIntArray4406[i_420_] + i_427_ * anIntArray4422[i_420_]
										+ 16383) >> 14;
								anIntArray4422[i_420_] = (-(anIntArray4406[i_420_] * i_427_)
										+ anIntArray4422[i_420_] * i_428_ + 16383) >> 14;
								anIntArray4406[i_420_] = i_429_;
							}
							anIntArray4406[i_420_] += Class288.anInt2647;
							anIntArray4450[i_420_] += Class41_Sub20.anInt3796;
							anIntArray4422[i_420_] += Class368.anInt3132;
						}
					}
				}
			}
			if (bool) {
				for (int i_430_ = 0; i_393_ > i_430_; i_430_++) {
					int i_431_ = is[i_430_];
					if (i_431_ < anIntArrayArray4429.length) {
						int[] is_432_ = anIntArrayArray4429[i_431_];
						for (int i_433_ = 0; i_433_ < is_432_.length; i_433_++) {
							int i_434_ = is_432_[i_433_];
							int i_435_ = anIntArray4395[i_434_];
							int i_436_ = anIntArray4395[i_434_ + 1];
							for (int i_437_ = i_435_; i_437_ < i_436_; i_437_++) {
								int i_438_ = aShortArray4414[i_437_] - 1;
								if (i_438_ == -1)
									break;
								if (i_391_ != 0) {
									int i_439_ = Class296_Sub4.anIntArray4598[i_391_];
									int i_440_ = Class296_Sub4.anIntArray4618[i_391_];
									int i_441_ = ((aShortArray4448[i_438_] * i_439_ + 16383
											+ aShortArray4420[i_438_] * i_440_) >> 14);
									aShortArray4448[i_438_] = (short) (((aShortArray4448[i_438_] * i_440_)
											- i_439_ * (aShortArray4420[i_438_]) + 16383) >> 14);
									aShortArray4420[i_438_] = (short) i_441_;
								}
								if (i_389_ != 0) {
									int i_442_ = Class296_Sub4.anIntArray4598[i_389_];
									int i_443_ = Class296_Sub4.anIntArray4618[i_389_];
									int i_444_ = ((-(i_442_ * aShortArray4413[i_438_])
											+ aShortArray4448[i_438_] * i_443_ + 16383) >> 14);
									aShortArray4413[i_438_] = (short) (((i_442_ * aShortArray4448[i_438_])
											+ ((i_443_ * (aShortArray4413[i_438_])) + 16383)) >> 14);
									aShortArray4448[i_438_] = (short) i_444_;
								}
								if (i_390_ != 0) {
									int i_445_ = Class296_Sub4.anIntArray4598[i_390_];
									int i_446_ = Class296_Sub4.anIntArray4618[i_390_];
									int i_447_ = ((i_446_ * aShortArray4420[i_438_] + i_445_ * aShortArray4413[i_438_]
											+ 16383) >> 14);
									aShortArray4413[i_438_] = (short) ((-(aShortArray4420[i_438_] * i_445_)
											+ i_446_ * (aShortArray4413[i_438_]) + 16383) >> 14);
									aShortArray4420[i_438_] = (short) i_447_;
								}
							}
						}
					}
				}
				if (aClass272_4428 == null && aClass272_4446 != null)
					aClass272_4446.anInterface5_2521 = null;
				if (aClass272_4428 != null)
					aClass272_4428.anInterface5_2521 = null;
			}
		} else if (i == 3) {
			for (int i_448_ = 0; i_448_ < i_393_; i_448_++) {
				int i_449_ = is[i_448_];
				if (i_449_ < anIntArrayArray4429.length) {
					int[] is_450_ = anIntArrayArray4429[i_449_];
					for (int i_451_ = 0; is_450_.length > i_451_; i_451_++) {
						int i_452_ = is_450_[i_451_];
						anIntArray4406[i_452_] -= Class288.anInt2647;
						anIntArray4450[i_452_] -= Class41_Sub20.anInt3796;
						anIntArray4422[i_452_] -= Class368.anInt3132;
						anIntArray4406[i_452_] = i_389_ * anIntArray4406[i_452_] >> 7;
						anIntArray4450[i_452_] = i_390_ * anIntArray4450[i_452_] >> 7;
						anIntArray4422[i_452_] = i_391_ * anIntArray4422[i_452_] >> 7;
						anIntArray4406[i_452_] += Class288.anInt2647;
						anIntArray4450[i_452_] += Class41_Sub20.anInt3796;
						anIntArray4422[i_452_] += Class368.anInt3132;
					}
				}
			}
		} else if (i == 5) {
			if (anIntArrayArray4403 != null) {
				for (int i_453_ = 0; i_453_ < i_393_; i_453_++) {
					int i_454_ = is[i_453_];
					if (anIntArrayArray4403.length > i_454_) {
						int[] is_455_ = anIntArrayArray4403[i_454_];
						for (int i_456_ = 0; i_456_ < is_455_.length; i_456_++) {
							int i_457_ = is_455_[i_456_];
							int i_458_ = i_389_ * 8 + (aByteArray4426[i_457_] & 0xff);
							if (i_458_ >= 0) {
								if (i_458_ > 255)
									i_458_ = 255;
							} else
								i_458_ = 0;
							aByteArray4426[i_457_] = (byte) i_458_;
						}
						if (is_455_.length > 0 && aClass272_4446 != null)
							aClass272_4446.anInterface5_2521 = null;
					}
				}
				if (aClass354Array4405 != null) {
					for (int i_459_ = 0; i_459_ < anInt4418; i_459_++) {
						Class354 class354 = aClass354Array4405[i_459_];
						Class331 class331 = aClass331Array4392[i_459_];
						class331.anInt2938 = (class331.anInt2938 & 0xffffff
								| (-(aByteArray4426[class354.anInt3054] & 0xff) + 255) << 24);
					}
				}
			}
		} else if (i == 7) {
			if (anIntArrayArray4403 != null) {
				for (int i_460_ = 0; i_460_ < i_393_; i_460_++) {
					int i_461_ = is[i_460_];
					if (i_461_ < anIntArrayArray4403.length) {
						int[] is_462_ = anIntArrayArray4403[i_461_];
						for (int i_463_ = 0; is_462_.length > i_463_; i_463_++) {
							int i_464_ = is_462_[i_463_];
							int i_465_ = aShortArray4394[i_464_] & 0xffff;
							int i_466_ = (i_465_ & 0xfd14) >> 10;
							int i_467_ = i_465_ >> 7 & 0x7;
							int i_468_ = i_465_ & 0x7f;
							i_466_ = i_389_ + i_466_ & 0x3f;
							i_467_ += i_390_ / 4;
							i_468_ += i_391_;
							if (i_467_ < 0)
								i_467_ = 0;
							else if (i_467_ > 7)
								i_467_ = 7;
							if (i_468_ >= 0) {
								if (i_468_ > 127)
									i_468_ = 127;
							} else
								i_468_ = 0;
							aShortArray4394[i_464_] = (short) (Class48.bitOR(Class48.bitOR(i_467_ << 7, i_466_ << 10),
									i_468_));
						}
						if (is_462_.length > 0 && aClass272_4446 != null)
							aClass272_4446.anInterface5_2521 = null;
					}
				}
				if (aClass354Array4405 != null) {
					for (int i_469_ = 0; i_469_ < anInt4418; i_469_++) {
						Class354 class354 = aClass354Array4405[i_469_];
						Class331 class331 = aClass331Array4392[i_469_];
						class331.anInt2938 = (class331.anInt2938 & ~0xffffff
								| (Class166_Sub1.anIntArray4300[(aShortArray4394[class354.anInt3054] & 0xffff)])
										& 0xffffff);
					}
				}
			}
		} else if (i == 8) {
			if (anIntArrayArray4443 != null) {
				for (int i_470_ = 0; i_470_ < i_393_; i_470_++) {
					int i_471_ = is[i_470_];
					if (anIntArrayArray4443.length > i_471_) {
						int[] is_472_ = anIntArrayArray4443[i_471_];
						for (int i_473_ = 0; is_472_.length > i_473_; i_473_++) {
							Class331 class331 = aClass331Array4392[is_472_[i_473_]];
							class331.anInt2937 += i_390_;
							class331.anInt2934 += i_389_;
						}
					}
				}
			}
		} else if (i == 10) {
			if (anIntArrayArray4443 != null) {
				for (int i_474_ = 0; i_474_ < i_393_; i_474_++) {
					int i_475_ = is[i_474_];
					if (anIntArrayArray4443.length > i_475_) {
						int[] is_476_ = anIntArrayArray4443[i_475_];
						for (int i_477_ = 0; is_476_.length > i_477_; i_477_++) {
							Class331 class331 = aClass331Array4392[is_476_[i_477_]];
							class331.anInt2935 = i_389_ * class331.anInt2935 >> 7;
							class331.anInt2936 = i_390_ * class331.anInt2936 >> 7;
						}
					}
				}
			}
		} else if (i == 9) {
			if (anIntArrayArray4443 != null) {
				for (int i_478_ = 0; i_393_ > i_478_; i_478_++) {
					int i_479_ = is[i_478_];
					if (anIntArrayArray4443.length > i_479_) {
						int[] is_480_ = anIntArrayArray4443[i_479_];
						for (int i_481_ = 0; i_481_ < is_480_.length; i_481_++) {
							Class331 class331 = aClass331Array4392[is_480_[i_481_]];
							class331.anInt2933 = class331.anInt2933 + i_389_ & 0x3fff;
						}
					}
				}
			}
		}
	}

	public void H(int i, int i_482_, int i_483_) {
		for (int i_484_ = 0; i_484_ < anInt4437; i_484_++) {
			if (i != 0)
				anIntArray4406[i_484_] += i;
			if (i_482_ != 0)
				anIntArray4450[i_484_] += i_482_;
			if (i_483_ != 0)
				anIntArray4422[i_484_] += i_483_;
		}
		if (aClass272_4438 != null)
			aClass272_4438.anInterface5_2521 = null;
		aBoolean4440 = false;
	}

	public int ma() {
		if (!aBoolean4440)
			method1793((byte) -125);
		return aShort4419;
	}

	private boolean method1788(int i, int i_485_, boolean bool, int i_486_, int i_487_, Class373 class373, int i_488_) {
		if (i_488_ != 16717)
			anInt4412 = -83;
		Class373_Sub1 class373_sub1 = (Class373_Sub1) class373;
		Class373_Sub1 class373_sub1_489_ = aHa_Sub3_4415.aClass373_Sub1_4168;
		float f = (class373_sub1.aFloat5576 * class373_sub1_489_.aFloat5577
				+ class373_sub1_489_.aFloat5585 * class373_sub1.aFloat5582
				+ class373_sub1.aFloat5588 * class373_sub1_489_.aFloat5581 + class373_sub1_489_.aFloat5576);
		float f_490_ = (class373_sub1_489_.aFloat5586 * class373_sub1.aFloat5588
				+ (class373_sub1_489_.aFloat5583 * class373_sub1.aFloat5582
						+ class373_sub1_489_.aFloat5584 * class373_sub1.aFloat5576)
				+ class373_sub1_489_.aFloat5582);
		Class211.aFloat2102 = (class373_sub1.aFloat5579 * class373_sub1_489_.aFloat5589
				+ (class373_sub1_489_.aFloat5579 * class373_sub1.aFloat5583
						+ class373_sub1.aFloat5585 * class373_sub1_489_.aFloat5587));
		Class126.aFloat1292 = (class373_sub1_489_.aFloat5586 * class373_sub1.aFloat5579
				+ (class373_sub1_489_.aFloat5584 * class373_sub1.aFloat5585
						+ class373_sub1_489_.aFloat5583 * class373_sub1.aFloat5583));
		float f_491_ = (class373_sub1_489_.aFloat5588 + (class373_sub1_489_.aFloat5589 * class373_sub1.aFloat5588
				+ (class373_sub1.aFloat5582 * class373_sub1_489_.aFloat5579
						+ (class373_sub1_489_.aFloat5587 * class373_sub1.aFloat5576))));
		Class32_Sub1.aFloat5650 = (class373_sub1.aFloat5586 * class373_sub1_489_.aFloat5585
				+ class373_sub1.aFloat5581 * class373_sub1_489_.aFloat5577
				+ class373_sub1.aFloat5589 * class373_sub1_489_.aFloat5581);
		Class125.aFloat1287 = (class373_sub1_489_.aFloat5583 * class373_sub1.aFloat5586
				+ class373_sub1.aFloat5581 * class373_sub1_489_.aFloat5584
				+ class373_sub1_489_.aFloat5586 * class373_sub1.aFloat5589);
		Class296_Sub27.aFloat4787 = (class373_sub1.aFloat5581 * class373_sub1_489_.aFloat5587
				+ class373_sub1_489_.aFloat5579 * class373_sub1.aFloat5586
				+ class373_sub1_489_.aFloat5589 * class373_sub1.aFloat5589);
		Class355_Sub1.aFloat3695 = (class373_sub1.aFloat5577 * class373_sub1_489_.aFloat5587
				+ class373_sub1_489_.aFloat5579 * class373_sub1.aFloat5584
				+ class373_sub1_489_.aFloat5589 * class373_sub1.aFloat5587);
		Class284.aFloat2620 = (class373_sub1_489_.aFloat5581 * class373_sub1.aFloat5579
				+ (class373_sub1.aFloat5585 * class373_sub1_489_.aFloat5577
						+ class373_sub1.aFloat5583 * class373_sub1_489_.aFloat5585));
		Class52.aFloat637 = (class373_sub1_489_.aFloat5583 * class373_sub1.aFloat5584
				+ class373_sub1.aFloat5577 * class373_sub1_489_.aFloat5584
				+ class373_sub1_489_.aFloat5586 * class373_sub1.aFloat5587);
		Class296_Sub29.aFloat4811 = (class373_sub1.aFloat5577 * class373_sub1_489_.aFloat5577
				+ class373_sub1.aFloat5584 * class373_sub1_489_.aFloat5585
				+ class373_sub1.aFloat5587 * class373_sub1_489_.aFloat5581);
		boolean bool_492_ = false;
		float f_493_ = 3.4028235E38F;
		float f_494_ = -3.4028235E38F;
		float f_495_ = 3.4028235E38F;
		float f_496_ = -3.4028235E38F;
		int i_497_ = aHa_Sub3_4415.anInt4263;
		int i_498_ = aHa_Sub3_4415.anInt4227;
		if (!aBoolean4440)
			method1793((byte) 108);
		int i_499_ = -aShort4400 + aShort4409 >> 1;
		int i_500_ = aShort4399 - aShort4402 >> 1;
		int i_501_ = -aShort4401 + aShort4435 >> 1;
		int i_502_ = i_499_ + aShort4400;
		int i_503_ = i_500_ + aShort4402;
		int i_504_ = aShort4401 + i_501_;
		int i_505_ = -(i_499_ << i_487_) + i_502_;
		int i_506_ = i_503_ - (i_500_ << i_487_);
		int i_507_ = i_504_ - (i_501_ << i_487_);
		int i_508_ = i_502_ + (i_499_ << i_487_);
		int i_509_ = (i_500_ << i_487_) + i_503_;
		int i_510_ = i_504_ + (i_501_ << i_487_);
		Class121.anIntArray1265[0] = i_505_;
		Class98.anIntArray1057[0] = i_506_;
		Class399.anIntArray3353[0] = i_507_;
		Class121.anIntArray1265[1] = i_508_;
		Class98.anIntArray1057[1] = i_506_;
		Class121.anIntArray1265[2] = i_505_;
		Class399.anIntArray3353[1] = i_507_;
		Class98.anIntArray1057[2] = i_509_;
		Class399.anIntArray3353[2] = i_507_;
		Class121.anIntArray1265[3] = i_508_;
		Class98.anIntArray1057[3] = i_509_;
		Class399.anIntArray3353[3] = i_507_;
		Class121.anIntArray1265[4] = i_505_;
		Class98.anIntArray1057[4] = i_506_;
		Class121.anIntArray1265[5] = i_508_;
		Class399.anIntArray3353[4] = i_510_;
		Class98.anIntArray1057[5] = i_506_;
		Class399.anIntArray3353[5] = i_510_;
		Class121.anIntArray1265[6] = i_505_;
		Class98.anIntArray1057[6] = i_509_;
		Class399.anIntArray3353[6] = i_510_;
		Class121.anIntArray1265[7] = i_508_;
		Class98.anIntArray1057[7] = i_509_;
		Class399.anIntArray3353[7] = i_510_;
		for (int i_511_ = 0; i_511_ < 8; i_511_++) {
			float f_512_ = (float) Class98.anIntArray1057[i_511_];
			float f_513_ = (float) Class399.anIntArray3353[i_511_];
			float f_514_ = (float) Class121.anIntArray1265[i_511_];
			float f_515_ = (f_513_ * Class125.aFloat1287 + (Class52.aFloat637 * f_514_ + Class126.aFloat1292 * f_512_)
					+ f_490_);
			float f_516_ = (Class296_Sub29.aFloat4811 * f_514_ + Class284.aFloat2620 * f_512_
					+ f_513_ * Class32_Sub1.aFloat5650 + f);
			float f_517_ = (f_512_ * Class211.aFloat2102 + f_514_ * Class355_Sub1.aFloat3695
					+ Class296_Sub27.aFloat4787 * f_513_ + f_491_);
			if (f_517_ >= (float) aHa_Sub3_4415.anInt4249) {
				if (i_486_ > 0)
					f_517_ = (float) i_486_;
				float f_518_ = ((float) aHa_Sub3_4415.anInt4215 + f_516_ * (float) i_497_ / f_517_);
				float f_519_ = ((float) aHa_Sub3_4415.anInt4246 + f_515_ * (float) i_498_ / f_517_);
				if (f_493_ > f_518_)
					f_493_ = f_518_;
				if (f_494_ < f_518_)
					f_494_ = f_518_;
				if (f_495_ > f_519_)
					f_495_ = f_519_;
				if (f_519_ > f_496_)
					f_496_ = f_519_;
				bool_492_ = true;
			}
		}
		if (bool_492_ && f_493_ < (float) i_485_ && f_494_ > (float) i_485_ && (float) i > f_495_
				&& f_496_ > (float) i) {
			if (bool)
				return true;
			if (anInt4439 > aHa_Sub3_4415.anIntArray4277.length) {
				aHa_Sub3_4415.anIntArray4275 = new int[anInt4439];
				aHa_Sub3_4415.anIntArray4277 = new int[anInt4439];
			}
			int[] is = aHa_Sub3_4415.anIntArray4277;
			int[] is_520_ = aHa_Sub3_4415.anIntArray4275;
			for (int i_521_ = 0; i_521_ < anInt4437; i_521_++) {
				float f_522_ = (float) anIntArray4450[i_521_];
				float f_523_ = (float) anIntArray4406[i_521_];
				float f_524_ = (float) anIntArray4422[i_521_];
				float f_525_ = (Class125.aFloat1287 * f_524_
						+ (Class126.aFloat1292 * f_522_ + f_523_ * Class52.aFloat637) + f_490_);
				float f_526_ = f + (Class32_Sub1.aFloat5650 * f_524_
						+ (Class284.aFloat2620 * f_522_ + f_523_ * Class296_Sub29.aFloat4811));
				float f_527_ = f_491_ + (f_522_ * Class211.aFloat2102 + f_523_ * Class355_Sub1.aFloat3695
						+ f_524_ * Class296_Sub27.aFloat4787);
				if (f_527_ >= (float) aHa_Sub3_4415.anInt4249) {
					if (i_486_ > 0)
						f_527_ = (float) i_486_;
					int i_528_ = (int) ((float) aHa_Sub3_4415.anInt4215 + (float) i_497_ * f_526_ / f_527_);
					int i_529_ = (int) ((float) aHa_Sub3_4415.anInt4246 + f_525_ * (float) i_498_ / f_527_);
					int i_530_ = anIntArray4395[i_521_];
					int i_531_ = anIntArray4395[i_521_ + 1];
					for (int i_532_ = i_530_; i_531_ > i_532_; i_532_++) {
						int i_533_ = aShortArray4414[i_532_] - 1;
						if (i_533_ == -1)
							break;
						is[i_533_] = i_528_;
						is_520_[i_533_] = i_529_;
					}
				} else {
					int i_534_ = anIntArray4395[i_521_];
					int i_535_ = anIntArray4395[i_521_ + 1];
					for (int i_536_ = i_534_; i_536_ < i_535_; i_536_++) {
						int i_537_ = aShortArray4414[i_536_] - 1;
						if (i_537_ == -1)
							break;
						is[aShortArray4414[i_536_] - 1] = -999999;
					}
				}
			}
			for (int i_538_ = 0; anInt4433 > i_538_; i_538_++) {
				if (is[aShortArray4442[i_538_]] != -999999 && is[aShortArray4393[i_538_]] != -999999
						&& is[aShortArray4390[i_538_]] != -999999
						&& method1787(i, is_520_[aShortArray4393[i_538_]], is_520_[aShortArray4390[i_538_]], i_485_, 3,
								is_520_[aShortArray4442[i_538_]], is[aShortArray4393[i_538_]],
								is[aShortArray4390[i_538_]], is[aShortArray4442[i_538_]]))
					return true;
			}
		}
		return false;
	}

	public void wa() {
		for (int i = 0; i < anInt4449; i++) {
			anIntArray4406[i] = anIntArray4406[i] + 7 >> 4;
			anIntArray4450[i] = anIntArray4450[i] + 7 >> 4;
			anIntArray4422[i] = anIntArray4422[i] + 7 >> 4;
		}
		aBoolean4440 = false;
		if (aClass272_4438 != null)
			aClass272_4438.anInterface5_2521 = null;
	}

	public void ia(short i, short i_539_) {
		for (int i_540_ = 0; i_540_ < anInt4433; i_540_++) {
			if (i == aShortArray4394[i_540_])
				aShortArray4394[i_540_] = i_539_;
		}
		if (aClass354Array4405 != null) {
			for (int i_541_ = 0; anInt4418 > i_541_; i_541_++) {
				Class354 class354 = aClass354Array4405[i_541_];
				Class331 class331 = aClass331Array4392[i_541_];
				class331.anInt2938 = ((Class166_Sub1.anIntArray4300[aShortArray4394[class354.anInt3054] & 0xffff])
						& 0xffffff) | class331.anInt2938 & ~0xffffff;
			}
		}
		if (aClass272_4446 != null)
			aClass272_4446.anInterface5_2521 = null;
	}

	public EffectiveVertex[] method1729() {
		return aClass232Array4397;
	}

	public void method1722() {
		if (anInt4439 > 0 && anInt4447 > 0) {
			method1786(false, false);
			if ((aByte4445 & 0x10) == 0 && aClass119_4431.anInterface20_1194 == null)
				method1781((byte) 122, false);
			method1791((byte) 67);
		}
	}

	public int EA() {
		if (!aBoolean4440)
			method1793((byte) 108);
		return aShort4399;
	}

	public int fa() {
		if (!aBoolean4440)
			method1793((byte) -109);
		return aShort4402;
	}

	public void method1715(Class373 class373, Class338_Sub5 class338_sub5, int i) {
		if (anInt4439 != 0) {
			Class373_Sub1 class373_sub1 = aHa_Sub3_4415.aClass373_Sub1_4168;
			Class373_Sub1 class373_sub1_542_ = (Class373_Sub1) class373;
			if (!aBoolean4440)
				method1793((byte) 70);
			Class211.aFloat2102 = (class373_sub1.aFloat5587 * class373_sub1_542_.aFloat5585
					+ class373_sub1.aFloat5579 * class373_sub1_542_.aFloat5583
					+ class373_sub1.aFloat5589 * class373_sub1_542_.aFloat5579);
			FileOnDisk.aFloat674 = (class373_sub1.aFloat5588 + (class373_sub1_542_.aFloat5576 * class373_sub1.aFloat5587
					+ (class373_sub1.aFloat5579 * class373_sub1_542_.aFloat5582)
					+ (class373_sub1_542_.aFloat5588 * class373_sub1.aFloat5589)));
			float f = FileOnDisk.aFloat674 + Class211.aFloat2102 * (float) aShort4402;
			float f_543_ = FileOnDisk.aFloat674 + (float) aShort4399 * Class211.aFloat2102;
			float f_544_;
			float f_545_;
			if (!(f_543_ < f)) {
				f_545_ = (float) -aShort4434 + f;
				f_544_ = (float) aShort4434 + f_543_;
			} else {
				f_544_ = (float) aShort4434 + f;
				f_545_ = f_543_ - (float) aShort4434;
			}
			if (!(aHa_Sub3_4415.aFloat4210 <= f_545_) && !(f_544_ <= (float) aHa_Sub3_4415.anInt4249)) {
				InvisiblePlayer.aFloat1976 = (class373_sub1_542_.aFloat5582 * class373_sub1.aFloat5585
						+ (class373_sub1.aFloat5577 * class373_sub1_542_.aFloat5576)
						+ (class373_sub1_542_.aFloat5588 * class373_sub1.aFloat5581) + class373_sub1.aFloat5576);
				Class284.aFloat2620 = (class373_sub1.aFloat5581 * class373_sub1_542_.aFloat5579
						+ ((class373_sub1_542_.aFloat5583 * class373_sub1.aFloat5585)
								+ (class373_sub1.aFloat5577 * class373_sub1_542_.aFloat5585)));
				float f_546_ = (InvisiblePlayer.aFloat1976 + Class284.aFloat2620 * (float) aShort4402);
				float f_547_ = (Class284.aFloat2620 * (float) aShort4399 + InvisiblePlayer.aFloat1976);
				float f_548_;
				float f_549_;
				if (!(f_546_ > f_547_)) {
					f_548_ = (((float) aShort4434 + f_547_) * (float) aHa_Sub3_4415.anInt4263);
					f_549_ = (((float) -aShort4434 + f_546_) * (float) aHa_Sub3_4415.anInt4263);
				} else {
					f_548_ = (((float) aShort4434 + f_546_) * (float) aHa_Sub3_4415.anInt4263);
					f_549_ = ((float) aHa_Sub3_4415.anInt4263 * ((float) -aShort4434 + f_547_));
				}
				if (!(f_549_ / f_544_ >= aHa_Sub3_4415.aFloat4198) && !(f_548_ / f_544_ <= aHa_Sub3_4415.aFloat4216)) {
					Class296_Sub55.aFloat5073 = ((class373_sub1.aFloat5586 * class373_sub1_542_.aFloat5588)
							+ ((class373_sub1.aFloat5583 * class373_sub1_542_.aFloat5582)
									+ (class373_sub1_542_.aFloat5576 * class373_sub1.aFloat5584))
							+ class373_sub1.aFloat5582);
					Class126.aFloat1292 = ((class373_sub1_542_.aFloat5579 * class373_sub1.aFloat5586)
							+ ((class373_sub1.aFloat5583 * class373_sub1_542_.aFloat5583)
									+ (class373_sub1_542_.aFloat5585 * class373_sub1.aFloat5584)));
					float f_550_ = (Class126.aFloat1292 * (float) aShort4402 + Class296_Sub55.aFloat5073);
					float f_551_ = (Class126.aFloat1292 * (float) aShort4399 + Class296_Sub55.aFloat5073);
					float f_552_;
					float f_553_;
					if (!(f_550_ > f_551_)) {
						f_552_ = (((float) aShort4434 + f_551_) * (float) aHa_Sub3_4415.anInt4227);
						f_553_ = ((float) aHa_Sub3_4415.anInt4227 * (f_550_ - (float) aShort4434));
					} else {
						f_552_ = ((float) aHa_Sub3_4415.anInt4227 * ((float) aShort4434 + f_550_));
						f_553_ = ((float) aHa_Sub3_4415.anInt4227 * (f_551_ - (float) aShort4434));
					}
					if (!(aHa_Sub3_4415.aFloat4256 <= f_553_ / f_544_)
							&& !(f_552_ / f_544_ <= aHa_Sub3_4415.aFloat4242)) {
						if (class338_sub5 != null || aClass354Array4405 != null) {
							Class125.aFloat1287 = ((class373_sub1_542_.aFloat5586 * class373_sub1.aFloat5583)
									+ (class373_sub1_542_.aFloat5581 * class373_sub1.aFloat5584)
									+ (class373_sub1_542_.aFloat5589 * class373_sub1.aFloat5586));
							Class296_Sub27.aFloat4787 = ((class373_sub1.aFloat5579 * class373_sub1_542_.aFloat5586)
									+ (class373_sub1.aFloat5587 * class373_sub1_542_.aFloat5581)
									+ (class373_sub1_542_.aFloat5589 * class373_sub1.aFloat5589));
							Class32_Sub1.aFloat5650 = ((class373_sub1_542_.aFloat5589 * class373_sub1.aFloat5581)
									+ ((class373_sub1.aFloat5585 * class373_sub1_542_.aFloat5586)
											+ (class373_sub1.aFloat5577 * class373_sub1_542_.aFloat5581)));
							Class355_Sub1.aFloat3695 = ((class373_sub1.aFloat5589 * class373_sub1_542_.aFloat5587)
									+ ((class373_sub1.aFloat5587 * class373_sub1_542_.aFloat5577)
											+ (class373_sub1.aFloat5579 * class373_sub1_542_.aFloat5584)));
							Class296_Sub29.aFloat4811 = ((class373_sub1_542_.aFloat5587 * class373_sub1.aFloat5581)
									+ ((class373_sub1_542_.aFloat5584 * class373_sub1.aFloat5585)
											+ (class373_sub1.aFloat5577 * class373_sub1_542_.aFloat5577)));
							Class52.aFloat637 = ((class373_sub1.aFloat5584 * class373_sub1_542_.aFloat5577)
									+ (class373_sub1.aFloat5583 * class373_sub1_542_.aFloat5584)
									+ (class373_sub1_542_.aFloat5587 * class373_sub1.aFloat5586));
						}
						if (class338_sub5 != null) {
							boolean bool = false;
							boolean bool_554_ = true;
							int i_555_ = aShort4400 + aShort4409 >> 1;
							int i_556_ = aShort4401 + aShort4435 >> 1;
							int i_557_ = (int) ((Class32_Sub1.aFloat5650 * (float) i_556_) + ((Class284.aFloat2620
									* (float) aShort4402)
									+ ((Class296_Sub29.aFloat4811 * (float) i_555_) + InvisiblePlayer.aFloat1976)));
							int i_558_ = (int) (((float) aShort4402 * Class126.aFloat1292)
									+ (Class296_Sub55.aFloat5073 + (Class52.aFloat637 * (float) i_555_))
									+ (Class125.aFloat1287 * (float) i_556_));
							int i_559_ = (int) (((float) i_556_ * Class296_Sub27.aFloat4787)
									+ (((float) aShort4402 * Class211.aFloat2102)
											+ (((float) i_555_ * Class355_Sub1.aFloat3695) + FileOnDisk.aFloat674)));
							if (i_559_ >= aHa_Sub3_4415.anInt4249) {
								class338_sub5.anInt5223 = (aHa_Sub3_4415.anInt4246
										+ (aHa_Sub3_4415.anInt4227 * i_558_ / i_559_));
								class338_sub5.anInt5225 = (aHa_Sub3_4415.anInt4215
										+ (aHa_Sub3_4415.anInt4263 * i_557_ / i_559_));
							} else
								bool = true;
							int i_560_ = (int) ((Class32_Sub1.aFloat5650 * (float) i_556_)
									+ ((Class284.aFloat2620 * (float) aShort4399) + (InvisiblePlayer.aFloat1976
											+ ((float) i_555_ * (Class296_Sub29.aFloat4811)))));
							int i_561_ = (int) (Class125.aFloat1287 * (float) i_556_
									+ (((float) aShort4399 * Class126.aFloat1292)
											+ (Class296_Sub55.aFloat5073 + ((float) i_555_ * Class52.aFloat637))));
							int i_562_ = (int) ((Class296_Sub27.aFloat4787 * (float) i_556_)
									+ (FileOnDisk.aFloat674 + ((float) i_555_ * Class355_Sub1.aFloat3695)
											+ (Class211.aFloat2102 * (float) aShort4399)));
							if (i_562_ >= aHa_Sub3_4415.anInt4249) {
								class338_sub5.anInt5221 = (aHa_Sub3_4415.anInt4246
										+ (i_561_ * aHa_Sub3_4415.anInt4227 / i_562_));
								class338_sub5.anInt5222 = (aHa_Sub3_4415.anInt4215
										+ (i_560_ * aHa_Sub3_4415.anInt4263 / i_562_));
							} else
								bool = true;
							if (bool) {
								if (i_559_ >= aHa_Sub3_4415.anInt4249 || i_562_ >= aHa_Sub3_4415.anInt4249) {
									if (aHa_Sub3_4415.anInt4249 > i_559_) {
										int i_563_ = (((-aHa_Sub3_4415.anInt4249 + i_562_) << 16) / (-i_559_ + i_562_));
										int i_564_ = (i_560_ + (i_563_ * (i_560_ - i_557_) >> 16));
										int i_565_ = (((i_561_ - i_558_) * i_563_ >> 16) + i_561_);
										class338_sub5.anInt5225 = ((aHa_Sub3_4415.anInt4263 * i_564_
												/ aHa_Sub3_4415.anInt4249) + aHa_Sub3_4415.anInt4215);
										class338_sub5.anInt5223 = (aHa_Sub3_4415.anInt4246
												+ (aHa_Sub3_4415.anInt4227 * i_565_ / aHa_Sub3_4415.anInt4249));
									} else if (aHa_Sub3_4415.anInt4249 > i_562_) {
										int i_566_ = (((-aHa_Sub3_4415.anInt4249 + i_559_) << 16) / (i_559_ - i_562_));
										int i_567_ = (i_557_ + ((-i_560_ + i_557_) * i_566_ >> 16));
										int i_568_ = (i_558_ + (i_566_ * (i_558_ - i_561_) >> 16));
										class338_sub5.anInt5225 = (aHa_Sub3_4415.anInt4215
												+ (aHa_Sub3_4415.anInt4263 * i_567_ / aHa_Sub3_4415.anInt4249));
										class338_sub5.anInt5223 = (aHa_Sub3_4415.anInt4246
												+ (i_568_ * aHa_Sub3_4415.anInt4227 / aHa_Sub3_4415.anInt4249));
									}
								} else
									bool_554_ = false;
							}
							if (bool_554_) {
								if (i_559_ > i_562_)
									class338_sub5.anInt5226 = (-class338_sub5.anInt5225
											+ ((aHa_Sub3_4415.anInt4263 * (i_557_ + aShort4434) / i_559_)
													+ aHa_Sub3_4415.anInt4215));
								else
									class338_sub5.anInt5226 = (-class338_sub5.anInt5222
											+ (aHa_Sub3_4415.anInt4263 * (aShort4434 + i_560_) / i_562_)
											+ aHa_Sub3_4415.anInt4215);
								class338_sub5.aBoolean5224 = true;
							}
						}
						aHa_Sub3_4415.method1315(1);
						aHa_Sub3_4415.method1276(class373_sub1_542_, (byte) -95);
						method1783(0);
						aHa_Sub3_4415.method1288(7330);
						method1782(4);
					}
				}
			}
		}
	}

	public EmissiveTriangle[] method1735() {
		return aClass89Array4427;
	}

	public boolean F() {
		return aBoolean4407;
	}

	public void s(int i) {
		anInt4412 = i;
		if (aClass47_4423 != null && (anInt4412 & 0x10000) == 0) {
			aShortArray4420 = aClass47_4423.aShortArray445;
			aShortArray4413 = aClass47_4423.aShortArray446;
			aByteArray4425 = aClass47_4423.aByteArray450;
			aShortArray4448 = aClass47_4423.aShortArray453;
			aClass47_4423 = null;
		}
		aBoolean4430 = true;
		method1791((byte) 27);
	}

	public void k(int i) {
		int i_569_ = Class296_Sub4.anIntArray4598[i];
		int i_570_ = Class296_Sub4.anIntArray4618[i];
		for (int i_571_ = 0; i_571_ < anInt4437; i_571_++) {
			int i_572_ = ((i_570_ * anIntArray4406[i_571_] + anIntArray4422[i_571_] * i_569_) >> 14);
			anIntArray4422[i_571_] = (-(anIntArray4406[i_571_] * i_569_) + i_570_ * anIntArray4422[i_571_]) >> 14;
			anIntArray4406[i_571_] = i_572_;
		}
		for (int i_573_ = 0; anInt4439 > i_573_; i_573_++) {
			int i_574_ = ((i_569_ * aShortArray4413[i_573_] + aShortArray4420[i_573_] * i_570_) >> 14);
			aShortArray4413[i_573_] = (short) ((-(aShortArray4420[i_573_] * i_569_)
					+ i_570_ * aShortArray4413[i_573_]) >> 14);
			aShortArray4420[i_573_] = (short) i_574_;
		}
		if (aClass272_4428 == null && aClass272_4446 != null)
			aClass272_4446.anInterface5_2521 = null;
		if (aClass272_4428 != null)
			aClass272_4428.anInterface5_2521 = null;
		if (aClass272_4438 != null)
			aClass272_4438.anInterface5_2521 = null;
		aBoolean4440 = false;
	}

	public static void method1789(int i) {
		if (i < 83)
			aClass260_4436 = null;
		aClass260_4436 = null;
	}

	private Model method1790(byte i, boolean bool, int i_575_, Class178_Sub2 class178_sub2_576_, boolean bool_577_,
			Class178_Sub2 class178_sub2_578_) {
		class178_sub2_578_.aShort4411 = aShort4411;
		class178_sub2_578_.anInt4418 = anInt4418;
		class178_sub2_578_.aShort4444 = aShort4444;
		class178_sub2_578_.anInt4441 = anInt4441;
		class178_sub2_578_.anInt4439 = anInt4439;
		class178_sub2_578_.anInt4412 = i_575_;
		class178_sub2_578_.anInt4449 = anInt4449;
		class178_sub2_578_.anInt4433 = anInt4433;
		if ((i_575_ & 0x100) == 0)
			class178_sub2_578_.aBoolean4407 = aBoolean4407;
		else
			class178_sub2_578_.aBoolean4407 = true;
		class178_sub2_578_.anInt4437 = anInt4437;
		class178_sub2_578_.anInt4447 = anInt4447;
		class178_sub2_578_.aByte4445 = (byte) 0;
		class178_sub2_578_.aBoolean4408 = aBoolean4408;
		boolean bool_579_ = Class296_Sub44.method2928(255, i_575_, anInt4441);
		boolean bool_580_ = MaterialRaw.method1673(i_575_, anInt4441, (byte) -5);
		boolean bool_581_ = Class41_Sub26.method500(anInt4441, 75, i_575_);
		boolean bool_582_ = bool_581_ | (bool_579_ | bool_580_);
		if (!bool_582_) {
			class178_sub2_578_.anIntArray4422 = anIntArray4422;
			class178_sub2_578_.anIntArray4406 = anIntArray4406;
			class178_sub2_578_.anIntArray4450 = anIntArray4450;
		} else {
			if (bool_579_) {
				if (class178_sub2_576_.anIntArray4406 != null && class178_sub2_576_.anIntArray4406.length >= anInt4449)
					class178_sub2_578_.anIntArray4406 = class178_sub2_576_.anIntArray4406;
				else
					class178_sub2_578_.anIntArray4406 = class178_sub2_576_.anIntArray4406 = new int[anInt4449];
			} else
				class178_sub2_578_.anIntArray4406 = anIntArray4406;
			if (!bool_580_)
				class178_sub2_578_.anIntArray4450 = anIntArray4450;
			else if (class178_sub2_576_.anIntArray4450 != null && class178_sub2_576_.anIntArray4450.length >= anInt4449)
				class178_sub2_578_.anIntArray4450 = class178_sub2_576_.anIntArray4450;
			else
				class178_sub2_578_.anIntArray4450 = class178_sub2_576_.anIntArray4450 = new int[anInt4449];
			if (bool_581_) {
				if (class178_sub2_576_.anIntArray4422 != null && class178_sub2_576_.anIntArray4422.length >= anInt4449)
					class178_sub2_578_.anIntArray4422 = class178_sub2_576_.anIntArray4422;
				else
					class178_sub2_578_.anIntArray4422 = class178_sub2_576_.anIntArray4422 = new int[anInt4449];
			} else
				class178_sub2_578_.anIntArray4422 = anIntArray4422;
			for (int i_583_ = 0; i_583_ < anInt4449; i_583_++) {
				if (bool_579_)
					class178_sub2_578_.anIntArray4406[i_583_] = anIntArray4406[i_583_];
				if (bool_580_)
					class178_sub2_578_.anIntArray4450[i_583_] = anIntArray4450[i_583_];
				if (bool_581_)
					class178_sub2_578_.anIntArray4422[i_583_] = anIntArray4422[i_583_];
			}
		}
		if (Class296_Sub34_Sub2.method2741(i_575_, anInt4441, (byte) -128)) {
			if (bool)
				class178_sub2_578_.aByte4445 |= 0x1;
			class178_sub2_578_.aClass272_4438 = class178_sub2_576_.aClass272_4438;
			class178_sub2_578_.aClass272_4438.anInterface5_2521 = aClass272_4438.anInterface5_2521;
			class178_sub2_578_.aClass272_4438.aByte2520 = aClass272_4438.aByte2520;
		} else if (!Class296_Sub39_Sub9.method2831(113, i_575_, anInt4441))
			class178_sub2_578_.aClass272_4438 = null;
		else
			class178_sub2_578_.aClass272_4438 = aClass272_4438;
		if (!Class315.method3325((byte) 116, anInt4441, i_575_))
			class178_sub2_578_.aShortArray4394 = aShortArray4394;
		else {
			if (class178_sub2_576_.aShortArray4394 != null && anInt4433 <= class178_sub2_576_.aShortArray4394.length)
				class178_sub2_578_.aShortArray4394 = class178_sub2_576_.aShortArray4394;
			else
				class178_sub2_578_.aShortArray4394 = class178_sub2_576_.aShortArray4394 = new short[anInt4433];
			for (int i_584_ = 0; i_584_ < anInt4433; i_584_++)
				class178_sub2_578_.aShortArray4394[i_584_] = aShortArray4394[i_584_];
		}
		if (Class296_Sub51_Sub5.method3092(i_575_, -24276, anInt4441)) {
			if (class178_sub2_576_.aByteArray4426 == null || class178_sub2_576_.aByteArray4426.length < anInt4433)
				class178_sub2_578_.aByteArray4426 = class178_sub2_576_.aByteArray4426 = new byte[anInt4433];
			else
				class178_sub2_578_.aByteArray4426 = class178_sub2_576_.aByteArray4426;
			for (int i_585_ = 0; anInt4433 > i_585_; i_585_++)
				class178_sub2_578_.aByteArray4426[i_585_] = aByteArray4426[i_585_];
		} else
			class178_sub2_578_.aByteArray4426 = aByteArray4426;
		if (Class368_Sub8.method3836(anInt4441, i_575_, -1)) {
			class178_sub2_578_.aClass272_4446 = class178_sub2_576_.aClass272_4446;
			if (bool)
				class178_sub2_578_.aByte4445 |= 0x2;
			class178_sub2_578_.aClass272_4446.anInterface5_2521 = aClass272_4446.anInterface5_2521;
			class178_sub2_578_.aClass272_4446.aByte2520 = aClass272_4446.aByte2520;
		} else if (!Class30.method328(i_575_, anInt4441, -1))
			class178_sub2_578_.aClass272_4446 = null;
		else
			class178_sub2_578_.aClass272_4446 = aClass272_4446;
		if (Class205_Sub2.method1982(544, anInt4441, i_575_)) {
			if (class178_sub2_576_.aShortArray4420 == null || class178_sub2_576_.aShortArray4420.length < anInt4439) {
				int i_586_ = anInt4439;
				class178_sub2_578_.aShortArray4448 = class178_sub2_576_.aShortArray4448 = new short[i_586_];
				class178_sub2_578_.aShortArray4420 = class178_sub2_576_.aShortArray4420 = new short[i_586_];
				class178_sub2_578_.aShortArray4413 = class178_sub2_576_.aShortArray4413 = new short[i_586_];
			} else {
				class178_sub2_578_.aShortArray4448 = class178_sub2_576_.aShortArray4448;
				class178_sub2_578_.aShortArray4420 = class178_sub2_576_.aShortArray4420;
				class178_sub2_578_.aShortArray4413 = class178_sub2_576_.aShortArray4413;
			}
			if (aClass47_4423 != null) {
				if (class178_sub2_576_.aClass47_4423 == null)
					class178_sub2_576_.aClass47_4423 = new Class47();
				Class47 class47 = (class178_sub2_578_.aClass47_4423 = class178_sub2_576_.aClass47_4423);
				if (class47.aShortArray445 == null || class47.aShortArray445.length < anInt4439) {
					int i_587_ = anInt4439;
					class47.aShortArray446 = new short[i_587_];
					class47.aShortArray445 = new short[i_587_];
					class47.aByteArray450 = new byte[i_587_];
					class47.aShortArray453 = new short[i_587_];
				}
				for (int i_588_ = 0; i_588_ < anInt4439; i_588_++) {
					class178_sub2_578_.aShortArray4420[i_588_] = aShortArray4420[i_588_];
					class178_sub2_578_.aShortArray4448[i_588_] = aShortArray4448[i_588_];
					class178_sub2_578_.aShortArray4413[i_588_] = aShortArray4413[i_588_];
					class47.aShortArray445[i_588_] = aClass47_4423.aShortArray445[i_588_];
					class47.aShortArray453[i_588_] = aClass47_4423.aShortArray453[i_588_];
					class47.aShortArray446[i_588_] = aClass47_4423.aShortArray446[i_588_];
					class47.aByteArray450[i_588_] = aClass47_4423.aByteArray450[i_588_];
				}
			} else {
				for (int i_589_ = 0; anInt4439 > i_589_; i_589_++) {
					class178_sub2_578_.aShortArray4420[i_589_] = aShortArray4420[i_589_];
					class178_sub2_578_.aShortArray4448[i_589_] = aShortArray4448[i_589_];
					class178_sub2_578_.aShortArray4413[i_589_] = aShortArray4413[i_589_];
				}
			}
			class178_sub2_578_.aByteArray4425 = aByteArray4425;
		} else {
			class178_sub2_578_.aShortArray4448 = aShortArray4448;
			class178_sub2_578_.aClass47_4423 = aClass47_4423;
			class178_sub2_578_.aShortArray4420 = aShortArray4420;
			class178_sub2_578_.aShortArray4413 = aShortArray4413;
			class178_sub2_578_.aByteArray4425 = aByteArray4425;
		}
		if (Class404.method4167((byte) 122, anInt4441, i_575_)) {
			class178_sub2_578_.aClass272_4428 = class178_sub2_576_.aClass272_4428;
			if (bool)
				class178_sub2_578_.aByte4445 |= 0x4;
			class178_sub2_578_.aClass272_4428.anInterface5_2521 = aClass272_4428.anInterface5_2521;
			class178_sub2_578_.aClass272_4428.aByte2520 = aClass272_4428.aByte2520;
		} else if (!ReferenceWrapper.method2789(-1, anInt4441, i_575_))
			class178_sub2_578_.aClass272_4428 = null;
		else
			class178_sub2_578_.aClass272_4428 = aClass272_4428;
		if (!Class215.method2025((byte) -20, anInt4441, i_575_)) {
			class178_sub2_578_.aFloatArray4410 = aFloatArray4410;
			class178_sub2_578_.aFloatArray4398 = aFloatArray4398;
		} else {
			if (class178_sub2_576_.aFloatArray4410 != null && anInt4433 <= class178_sub2_576_.aFloatArray4410.length) {
				class178_sub2_578_.aFloatArray4410 = class178_sub2_576_.aFloatArray4410;
				class178_sub2_578_.aFloatArray4398 = class178_sub2_576_.aFloatArray4398;
			} else {
				int i_590_ = anInt4439;
				class178_sub2_578_.aFloatArray4398 = class178_sub2_576_.aFloatArray4398 = new float[i_590_];
				class178_sub2_578_.aFloatArray4410 = class178_sub2_576_.aFloatArray4410 = new float[i_590_];
			}
			for (int i_591_ = 0; i_591_ < anInt4439; i_591_++) {
				class178_sub2_578_.aFloatArray4410[i_591_] = aFloatArray4410[i_591_];
				class178_sub2_578_.aFloatArray4398[i_591_] = aFloatArray4398[i_591_];
			}
		}
		if (!Class273.method2313(93, anInt4441, i_575_)) {
			if (!Class379_Sub1.method3967(anInt4441, true, i_575_))
				class178_sub2_578_.aClass272_4432 = null;
			else
				class178_sub2_578_.aClass272_4432 = aClass272_4432;
		} else {
			class178_sub2_578_.aClass272_4432 = class178_sub2_576_.aClass272_4432;
			if (bool)
				class178_sub2_578_.aByte4445 |= 0x8;
			class178_sub2_578_.aClass272_4432.aByte2520 = aClass272_4432.aByte2520;
			class178_sub2_578_.aClass272_4432.anInterface5_2521 = aClass272_4432.anInterface5_2521;
		}
		if (Class244.method2178(anInt4441, 0, i_575_)) {
			if (class178_sub2_576_.aShortArray4442 != null && anInt4433 <= class178_sub2_576_.aShortArray4442.length) {
				class178_sub2_578_.aShortArray4393 = class178_sub2_576_.aShortArray4393;
				class178_sub2_578_.aShortArray4442 = class178_sub2_576_.aShortArray4442;
				class178_sub2_578_.aShortArray4390 = class178_sub2_576_.aShortArray4390;
			} else {
				int i_592_ = anInt4433;
				class178_sub2_578_.aShortArray4390 = class178_sub2_576_.aShortArray4390 = new short[i_592_];
				class178_sub2_578_.aShortArray4393 = class178_sub2_576_.aShortArray4393 = new short[i_592_];
				class178_sub2_578_.aShortArray4442 = class178_sub2_576_.aShortArray4442 = new short[i_592_];
			}
			for (int i_593_ = 0; anInt4433 > i_593_; i_593_++) {
				class178_sub2_578_.aShortArray4442[i_593_] = aShortArray4442[i_593_];
				class178_sub2_578_.aShortArray4393[i_593_] = aShortArray4393[i_593_];
				class178_sub2_578_.aShortArray4390[i_593_] = aShortArray4390[i_593_];
			}
		} else {
			class178_sub2_578_.aShortArray4393 = aShortArray4393;
			class178_sub2_578_.aShortArray4390 = aShortArray4390;
			class178_sub2_578_.aShortArray4442 = aShortArray4442;
		}
		if (!Class338_Sub3_Sub4.method3569(i_575_, anInt4441, (byte) -22)) {
			if (!Class206.method1990(i_575_, anInt4441, (byte) 89))
				class178_sub2_578_.aClass119_4431 = null;
			else
				class178_sub2_578_.aClass119_4431 = aClass119_4431;
		} else {
			if (bool)
				class178_sub2_578_.aByte4445 |= 0x10;
			class178_sub2_578_.aClass119_4431 = class178_sub2_576_.aClass119_4431;
			class178_sub2_578_.aClass119_4431.anInterface20_1194 = aClass119_4431.anInterface20_1194;
		}
		if (Class236.method2134(i_575_, anInt4441, false)) {
			if (class178_sub2_576_.aShortArray4416 == null || class178_sub2_576_.aShortArray4416.length < anInt4433) {
				int i_594_ = anInt4433;
				class178_sub2_578_.aShortArray4416 = class178_sub2_576_.aShortArray4416 = new short[i_594_];
			} else
				class178_sub2_578_.aShortArray4416 = class178_sub2_576_.aShortArray4416;
			for (int i_595_ = 0; i_595_ < anInt4433; i_595_++)
				class178_sub2_578_.aShortArray4416[i_595_] = aShortArray4416[i_595_];
		} else
			class178_sub2_578_.aShortArray4416 = aShortArray4416;
		if (Class296_Sub39_Sub20_Sub1.method2906(anInt4441, i_575_, true)) {
			if (class178_sub2_576_.aClass331Array4392 == null
					|| class178_sub2_576_.aClass331Array4392.length < anInt4418) {
				int i_596_ = anInt4418;
				class178_sub2_578_.aClass331Array4392 = class178_sub2_576_.aClass331Array4392 = new Class331[i_596_];
				for (int i_597_ = 0; anInt4418 > i_597_; i_597_++)
					class178_sub2_578_.aClass331Array4392[i_597_] = aClass331Array4392[i_597_].method3406(-105);
			} else {
				class178_sub2_578_.aClass331Array4392 = class178_sub2_576_.aClass331Array4392;
				for (int i_598_ = 0; i_598_ < anInt4418; i_598_++)
					class178_sub2_578_.aClass331Array4392[i_598_].method3408(aClass331Array4392[i_598_], 16383);
			}
		} else
			class178_sub2_578_.aClass331Array4392 = aClass331Array4392;
		class178_sub2_578_.anIntArrayArray4443 = anIntArrayArray4443;
		class178_sub2_578_.aShortArray4396 = aShortArray4396;
		class178_sub2_578_.aShortArray4414 = aShortArray4414;
		class178_sub2_578_.anIntArray4395 = anIntArray4395;
		if (!aBoolean4440)
			class178_sub2_578_.aBoolean4440 = false;
		else {
			class178_sub2_578_.aShort4400 = aShort4400;
			class178_sub2_578_.aShort4434 = aShort4434;
			class178_sub2_578_.aShort4435 = aShort4435;
			class178_sub2_578_.aShort4419 = aShort4419;
			class178_sub2_578_.aShort4409 = aShort4409;
			class178_sub2_578_.aShort4399 = aShort4399;
			class178_sub2_578_.aShort4401 = aShort4401;
			class178_sub2_578_.aShort4402 = aShort4402;
			class178_sub2_578_.aBoolean4440 = true;
		}
		class178_sub2_578_.aClass354Array4405 = aClass354Array4405;
		class178_sub2_578_.aClass232Array4397 = aClass232Array4397;
		class178_sub2_578_.aShortArray4404 = aShortArray4404;
		class178_sub2_578_.anIntArrayArray4403 = anIntArrayArray4403;
		class178_sub2_578_.anIntArray4421 = anIntArray4421;
		class178_sub2_578_.aClass89Array4427 = aClass89Array4427;
		if (i <= 80)
			return null;
		class178_sub2_578_.anIntArrayArray4429 = anIntArrayArray4429;
		return class178_sub2_578_;
	}

	public void method1736(Model class178, int i, int i_599_, int i_600_, boolean bool) {
		Class178_Sub2 class178_sub2_601_ = (Class178_Sub2) class178;
		if (anInt4433 != 0 && class178_sub2_601_.anInt4433 != 0) {
			int i_602_ = class178_sub2_601_.anInt4437;
			int[] is = class178_sub2_601_.anIntArray4406;
			int[] is_603_ = class178_sub2_601_.anIntArray4450;
			int[] is_604_ = class178_sub2_601_.anIntArray4422;
			short[] is_605_ = class178_sub2_601_.aShortArray4420;
			short[] is_606_ = class178_sub2_601_.aShortArray4448;
			short[] is_607_ = class178_sub2_601_.aShortArray4413;
			byte[] is_608_ = class178_sub2_601_.aByteArray4425;
			short[] is_609_;
			short[] is_610_;
			short[] is_611_;
			byte[] is_612_;
			if (aClass47_4423 == null) {
				is_609_ = null;
				is_611_ = null;
				is_610_ = null;
				is_612_ = null;
			} else {
				is_609_ = aClass47_4423.aShortArray445;
				is_610_ = aClass47_4423.aShortArray453;
				is_611_ = aClass47_4423.aShortArray446;
				is_612_ = aClass47_4423.aByteArray450;
			}
			short[] is_613_;
			short[] is_614_;
			byte[] is_615_;
			short[] is_616_;
			if (class178_sub2_601_.aClass47_4423 != null) {
				is_614_ = class178_sub2_601_.aClass47_4423.aShortArray445;
				is_615_ = class178_sub2_601_.aClass47_4423.aByteArray450;
				is_616_ = class178_sub2_601_.aClass47_4423.aShortArray446;
				is_613_ = class178_sub2_601_.aClass47_4423.aShortArray453;
			} else {
				is_613_ = null;
				is_614_ = null;
				is_615_ = null;
				is_616_ = null;
			}
			int[] is_617_ = class178_sub2_601_.anIntArray4395;
			short[] is_618_ = class178_sub2_601_.aShortArray4414;
			if (!class178_sub2_601_.aBoolean4440)
				class178_sub2_601_.method1793((byte) 88);
			int i_619_ = class178_sub2_601_.aShort4402;
			int i_620_ = class178_sub2_601_.aShort4399;
			int i_621_ = class178_sub2_601_.aShort4400;
			int i_622_ = class178_sub2_601_.aShort4409;
			int i_623_ = class178_sub2_601_.aShort4401;
			int i_624_ = class178_sub2_601_.aShort4435;
			for (int i_625_ = 0; anInt4437 > i_625_; i_625_++) {
				int i_626_ = -i_599_ + anIntArray4450[i_625_];
				if (i_626_ >= i_619_ && i_626_ <= i_620_) {
					int i_627_ = anIntArray4406[i_625_] - i;
					if (i_627_ >= i_621_ && i_622_ >= i_627_) {
						int i_628_ = -i_600_ + anIntArray4422[i_625_];
						if (i_623_ <= i_628_ && i_624_ >= i_628_) {
							int i_629_ = -1;
							int i_630_ = anIntArray4395[i_625_];
							int i_631_ = anIntArray4395[i_625_ + 1];
							for (int i_632_ = i_630_; i_631_ > i_632_; i_632_++) {
								i_629_ = aShortArray4414[i_632_] - 1;
								if (i_629_ == -1 || aByteArray4425[i_629_] != 0)
									break;
							}
							if (i_629_ != -1) {
								for (int i_633_ = 0; i_602_ > i_633_; i_633_++) {
									if (is[i_633_] == i_627_ && is_604_[i_633_] == i_628_
											&& is_603_[i_633_] == i_626_) {
										int i_634_ = -1;
										i_630_ = is_617_[i_633_];
										i_631_ = is_617_[i_633_ + 1];
										for (int i_635_ = i_630_; i_631_ > i_635_; i_635_++) {
											i_634_ = is_618_[i_635_] - 1;
											if (i_634_ == -1 || is_608_[i_634_] != 0)
												break;
										}
										if (i_634_ != -1) {
											if (is_609_ == null) {
												aClass47_4423 = new Class47();
												is_609_ = aClass47_4423.aShortArray445 = (Class331
														.method3412(aShortArray4420, true));
												is_610_ = aClass47_4423.aShortArray453 = (Class331
														.method3412(aShortArray4448, true));
												is_611_ = aClass47_4423.aShortArray446 = (Class331
														.method3412(aShortArray4413, true));
												is_612_ = aClass47_4423.aByteArray450 = (Class296_Sub39_Sub19
														.method2901((byte) -116, aByteArray4425));
											}
											if (is_614_ == null) {
												Class47 class47 = (class178_sub2_601_.aClass47_4423 = new Class47());
												is_614_ = class47.aShortArray445 = (Class331.method3412(is_605_, true));
												is_613_ = class47.aShortArray453 = (Class331.method3412(is_606_, true));
												is_616_ = class47.aShortArray446 = (Class331.method3412(is_607_, true));
												is_615_ = class47.aByteArray450 = (Class296_Sub39_Sub19
														.method2901((byte) -127, is_608_));
											}
											short i_636_ = aShortArray4420[i_629_];
											short i_637_ = aShortArray4448[i_629_];
											short i_638_ = aShortArray4413[i_629_];
											i_630_ = is_617_[i_633_];
											i_631_ = is_617_[i_633_ + 1];
											byte i_639_ = aByteArray4425[i_629_];
											for (int i_640_ = i_630_; i_640_ < i_631_; i_640_++) {
												int i_641_ = is_618_[i_640_] - 1;
												if (i_641_ == -1)
													break;
												if (is_615_[i_641_] != 0) {
													is_614_[i_641_] += i_636_;
													is_613_[i_641_] += i_637_;
													is_616_[i_641_] += i_638_;
													is_615_[i_641_] += i_639_;
												}
											}
											i_638_ = is_607_[i_634_];
											i_639_ = is_608_[i_634_];
											i_636_ = is_605_[i_634_];
											i_630_ = anIntArray4395[i_625_];
											i_631_ = anIntArray4395[i_625_ + 1];
											i_637_ = is_606_[i_634_];
											for (int i_642_ = i_630_; i_631_ > i_642_; i_642_++) {
												int i_643_ = (aShortArray4414[i_642_] - 1);
												if (i_643_ == -1)
													break;
												if (is_612_[i_643_] != 0) {
													is_609_[i_643_] += i_636_;
													is_610_[i_643_] += i_637_;
													is_611_[i_643_] += i_638_;
													is_612_[i_643_] += i_639_;
												}
											}
											if (aClass272_4428 == null && aClass272_4446 != null)
												aClass272_4446.anInterface5_2521 = null;
											if (aClass272_4428 != null)
												aClass272_4428.anInterface5_2521 = null;
											if ((class178_sub2_601_.aClass272_4428) == null
													&& (class178_sub2_601_.aClass272_4446) != null)
												class178_sub2_601_.aClass272_4446.anInterface5_2521 = null;
											if ((class178_sub2_601_.aClass272_4428) != null)
												class178_sub2_601_.aClass272_4428.anInterface5_2521 = null;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void LA(int i) {
		aShort4411 = (short) i;
		if (aClass272_4446 != null)
			aClass272_4446.anInterface5_2521 = null;
		if (aClass272_4428 != null)
			aClass272_4428.anInterface5_2521 = null;
	}

	public boolean method1732(int i, int i_644_, Class373 class373, boolean bool, int i_645_) {
		return method1788(i_644_, i, bool, -1, i_645_, class373, 16717);
	}

	private void method1791(byte i) {
		if (i >= 0 && aBoolean4430) {
			aBoolean4430 = false;
			if (aClass89Array4427 == null && aClass232Array4397 == null && aClass354Array4405 == null
					&& !Class162.method1626((byte) 108, anInt4441, anInt4412)) {
				boolean bool = false;
				boolean bool_646_ = false;
				boolean bool_647_ = false;
				if (anIntArray4406 != null && !Class116.method1014(anInt4412, anInt4441, (byte) 104)) {
					if (aClass272_4438 == null || aClass272_4438.anInterface5_2521 != null) {
						bool = true;
						if (!aBoolean4440)
							method1793((byte) -60);
					} else
						aBoolean4430 = true;
				}
				if (anIntArray4450 != null && !Class221.method2075(anInt4441, anInt4412, (byte) -97)) {
					if (aClass272_4438 != null && aClass272_4438.anInterface5_2521 == null)
						aBoolean4430 = true;
					else {
						if (!aBoolean4440)
							method1793((byte) -83);
						bool_646_ = true;
					}
				}
				if (anIntArray4422 != null && !Class41_Sub12.method438(anInt4412, -100, anInt4441)) {
					if (aClass272_4438 != null && aClass272_4438.anInterface5_2521 == null)
						aBoolean4430 = true;
					else {
						if (!aBoolean4440)
							method1793((byte) 73);
						bool_647_ = true;
					}
				}
				if (bool_647_)
					anIntArray4422 = null;
				if (bool_646_)
					anIntArray4450 = null;
				if (bool)
					anIntArray4406 = null;
			}
			if (aShortArray4414 != null && anIntArray4406 == null && anIntArray4450 == null && anIntArray4422 == null) {
				aShortArray4414 = null;
				anIntArray4395 = null;
			}
			if (aByteArray4425 != null && !Class31.method339(anInt4441, (byte) 95, anInt4412)) {
				if (aClass272_4428 != null) {
					if (aClass272_4428.anInterface5_2521 != null) {
						aByteArray4425 = null;
						aShortArray4420 = aShortArray4448 = aShortArray4413 = null;
					} else
						aBoolean4430 = true;
				} else if (aClass272_4446 != null && aClass272_4446.anInterface5_2521 == null)
					aBoolean4430 = true;
				else {
					aByteArray4425 = null;
					aShortArray4420 = aShortArray4448 = aShortArray4413 = null;
				}
			}
			if (aShortArray4394 != null && !Class93.method857(anInt4441, anInt4412, (byte) -93)) {
				if (aClass272_4446 != null && aClass272_4446.anInterface5_2521 == null)
					aBoolean4430 = true;
				else
					aShortArray4394 = null;
			}
			if (aByteArray4426 != null && !Class368.method3806(anInt4412, true, anInt4441)) {
				if (aClass272_4446 != null && aClass272_4446.anInterface5_2521 == null)
					aBoolean4430 = true;
				else
					aByteArray4426 = null;
			}
			if (aFloatArray4410 != null && !Class296_Sub51_Sub33.method3181(anInt4412, anInt4441, 118)) {
				if (aClass272_4432 != null && aClass272_4432.anInterface5_2521 == null)
					aBoolean4430 = true;
				else
					aFloatArray4410 = aFloatArray4398 = null;
			}
			if (aShortArray4416 != null && !IncomingPacket.method2112(anInt4412, (byte) -89, anInt4441)) {
				if (aClass272_4446 == null || aClass272_4446.anInterface5_2521 != null)
					aShortArray4416 = null;
				else
					aBoolean4430 = true;
			}
			if (aShortArray4442 != null && !Class16_Sub1_Sub1.method241(anInt4412, anInt4441, (byte) 24)) {
				if ((aClass119_4431 != null && aClass119_4431.anInterface20_1194 == null)
						|| (aClass272_4446 != null && aClass272_4446.anInterface5_2521 == null))
					aBoolean4430 = true;
				else
					aShortArray4442 = aShortArray4393 = aShortArray4390 = null;
			}
			if (anIntArrayArray4403 != null && !Class254.method2211(anInt4412, anInt4441, 384)) {
				aShortArray4396 = null;
				anIntArrayArray4403 = null;
			}
			if (anIntArrayArray4429 != null && !s_Sub3.method3372(anInt4441, -1, anInt4412)) {
				anIntArrayArray4429 = null;
				aShortArray4404 = null;
			}
			if (anIntArrayArray4443 != null && !Class268.method2297(anInt4441, anInt4412, true))
				anIntArrayArray4443 = null;
			if (anIntArray4421 != null && (anInt4412 & 0x800) == 0 && (anInt4412 & 0x40000) == 0)
				anIntArray4421 = null;
		}
	}

	public int RA() {
		if (!aBoolean4440)
			method1793((byte) -81);
		return aShort4409;
	}

	private short method1792(int i, int i_648_, Mesh class132, int i_649_, int i_650_, int i_651_, float f, int i_652_,
			float f_653_, long l) {
		int i_654_ = anIntArray4395[i_650_];
		int i_655_ = anIntArray4395[i_650_ + 1];
		int i_656_ = 0;
		if (i > -20)
			method1715(null, null, -68);
		for (int i_657_ = i_654_; i_657_ < i_655_; i_657_++) {
			short i_658_ = aShortArray4414[i_657_];
			if (i_658_ == 0) {
				i_656_ = i_657_;
				break;
			}
			if (l == Class203.aLongArray3566[i_657_])
				return (short) (i_658_ - 1);
		}
		aShortArray4414[i_656_] = (short) (anInt4439 + 1);
		Class203.aLongArray3566[i_656_] = l;
		aShortArray4420[anInt4439] = (short) i_652_;
		aShortArray4448[anInt4439] = (short) i_649_;
		aShortArray4413[anInt4439] = (short) i_651_;
		aByteArray4425[anInt4439] = (byte) i_648_;
		aFloatArray4410[anInt4439] = f;
		aFloatArray4398[anInt4439] = f_653_;
		return (short) anInt4439++;
	}

	public void FA(int i) {
		int i_659_ = Class296_Sub4.anIntArray4598[i];
		int i_660_ = Class296_Sub4.anIntArray4618[i];
		for (int i_661_ = 0; i_661_ < anInt4437; i_661_++) {
			int i_662_ = ((-(i_659_ * anIntArray4422[i_661_]) + i_660_ * anIntArray4450[i_661_]) >> 14);
			anIntArray4422[i_661_] = (i_659_ * anIntArray4450[i_661_] + i_660_ * anIntArray4422[i_661_]) >> 14;
			anIntArray4450[i_661_] = i_662_;
		}
		if (aClass272_4438 != null)
			aClass272_4438.anInterface5_2521 = null;
		aBoolean4440 = false;
	}

	public int na() {
		if (!aBoolean4440)
			method1793((byte) 107);
		return aShort4434;
	}

	public int da() {
		return aShort4411;
	}

	public void method1717(int i, int i_663_, int i_664_, int i_665_) {
		for (int i_666_ = 0; i_666_ < anInt4433; i_666_++) {
			int i_667_ = aShortArray4394[i_666_] & 0xffff;
			int i_668_ = i_667_ >> 10 & 0x3f;
			int i_669_ = (i_667_ & 0x3c0) >> 7;
			int i_670_ = i_667_ & 0x7f;
			if (i != -1)
				i_668_ += (-i_668_ + i) * i_665_ >> 7;
			if (i_663_ != -1)
				i_669_ += i_665_ * (i_663_ - i_669_) >> 7;
			if (i_664_ != -1)
				i_670_ = ((-i_670_ + i_664_) * i_665_ >> 7) + i_670_;
			aShortArray4394[i_666_] = (short) Class48.bitOR(Class48.bitOR(i_669_ << 7, i_668_ << 10), i_670_);
		}
		anInt4391++;
		if (aClass354Array4405 != null) {
			for (int i_671_ = 0; i_671_ < anInt4418; i_671_++) {
				Class354 class354 = aClass354Array4405[i_671_];
				Class331 class331 = aClass331Array4392[i_671_];
				class331.anInt2938 = (class331.anInt2938 & ~0xffffff
						| ((Class166_Sub1.anIntArray4300[aShortArray4394[class354.anInt3054] & 0xffff]) & 0xffffff));
			}
		}
		if (aClass272_4446 != null)
			aClass272_4446.anInterface5_2521 = null;
	}

	public boolean NA() {
		if (anIntArrayArray4429 == null)
			return false;
		for (int i = 0; anInt4449 > i; i++) {
			anIntArray4406[i] <<= 4;
			anIntArray4450[i] <<= 4;
			anIntArray4422[i] <<= 4;
		}
		Class368.anInt3132 = 0;
		Class41_Sub20.anInt3796 = 0;
		Class288.anInt2647 = 0;
		return true;
	}

	public boolean method1731(int i, int i_672_, Class373 class373, boolean bool, int i_673_, int i_674_) {
		return method1788(i_672_, i, bool, i_674_, i_673_, class373, 16717);
	}

	public void method1720() {
		/* empty */
	}

	public void v() {
		for (int i = 0; i < anInt4437; i++)
			anIntArray4422[i] = -anIntArray4422[i];
		for (int i = 0; anInt4439 > i; i++)
			aShortArray4413[i] = (short) -aShortArray4413[i];
		for (int i = 0; anInt4433 > i; i++) {
			short i_675_ = aShortArray4442[i];
			aShortArray4442[i] = aShortArray4390[i];
			aShortArray4390[i] = i_675_;
		}
		if (aClass272_4428 == null && aClass272_4446 != null)
			aClass272_4446.anInterface5_2521 = null;
		if (aClass272_4428 != null)
			aClass272_4428.anInterface5_2521 = null;
		aBoolean4440 = false;
		if (aClass119_4431 != null)
			aClass119_4431.anInterface20_1194 = null;
		if (aClass272_4438 != null)
			aClass272_4438.anInterface5_2521 = null;
	}

	public boolean method1737() {
		if (aShortArray4416 == null)
			return true;
		for (int i = 0; i < aShortArray4416.length; i++) {
			if (aShortArray4416[i] != -1 && !aHa_Sub3_4415.aD1299.is_ready(aShortArray4416[i]))
				return false;
		}
		return true;
	}

	public Model method1728(byte i, int i_676_, boolean bool) {
		boolean bool_677_ = false;
		Class178_Sub2 class178_sub2_678_;
		Class178_Sub2 class178_sub2_679_;
		if (i <= 0 || i > 7)
			class178_sub2_679_ = class178_sub2_678_ = new Class178_Sub2(aHa_Sub3_4415);
		else {
			bool_677_ = true;
			class178_sub2_678_ = aHa_Sub3_4415.aClass178_Sub2Array4222[i - 1];
			class178_sub2_679_ = aHa_Sub3_4415.aClass178_Sub2Array4214[i - 1];
		}
		return method1790((byte) 81, bool_677_, i_676_, class178_sub2_678_, bool, class178_sub2_679_);
	}

	public int G() {
		if (!aBoolean4440)
			method1793((byte) 116);
		return aShort4435;
	}

	private void method1793(byte i) {
		int i_680_ = 32767;
		int i_681_ = 32767;
		int i_682_ = 32767;
		int i_683_ = -32768;
		int i_684_ = -32768;
		int i_685_ = -32768;
		int i_686_ = 0;
		int i_687_ = 0;
		int i_688_ = -107 / ((i - 28) / 38);
		for (int i_689_ = 0; i_689_ < anInt4437; i_689_++) {
			int i_690_ = anIntArray4406[i_689_];
			int i_691_ = anIntArray4450[i_689_];
			if (i_680_ > i_690_)
				i_680_ = i_690_;
			if (i_691_ > i_684_)
				i_684_ = i_691_;
			if (i_683_ < i_690_)
				i_683_ = i_690_;
			if (i_691_ < i_681_)
				i_681_ = i_691_;
			int i_692_ = anIntArray4422[i_689_];
			if (i_682_ > i_692_)
				i_682_ = i_692_;
			if (i_685_ < i_692_)
				i_685_ = i_692_;
			int i_693_ = i_692_ * i_692_ + i_690_ * i_690_;
			if (i_693_ > i_686_)
				i_686_ = i_693_;
			i_693_ = i_691_ * i_691_ + i_692_ * i_692_ + i_690_ * i_690_;
			if (i_693_ > i_687_)
				i_687_ = i_693_;
		}
		aShort4409 = (short) i_683_;
		aShort4399 = (short) i_684_;
		aShort4402 = (short) i_681_;
		aShort4401 = (short) i_682_;
		aShort4400 = (short) i_680_;
		aShort4435 = (short) i_685_;
		aShort4434 = (short) (int) (Math.sqrt((double) i_686_) + 0.99);
		aShort4419 = (short) (int) (Math.sqrt((double) i_687_) + 0.99);
		aBoolean4440 = true;
	}

	public void method1730() {
		/* empty */
	}

	public byte[] method1733() {
		return aByteArray4426;
	}

	public void VA(int i) {
		int i_694_ = Class296_Sub4.anIntArray4598[i];
		int i_695_ = Class296_Sub4.anIntArray4618[i];
		for (int i_696_ = 0; i_696_ < anInt4437; i_696_++) {
			int i_697_ = ((anIntArray4450[i_696_] * i_694_ + anIntArray4406[i_696_] * i_695_) >> 14);
			anIntArray4450[i_696_] = (-(i_694_ * anIntArray4406[i_696_]) + i_695_ * anIntArray4450[i_696_]) >> 14;
			anIntArray4406[i_696_] = i_697_;
		}
		aBoolean4440 = false;
		if (aClass272_4438 != null)
			aClass272_4438.anInterface5_2521 = null;
	}

	Class178_Sub2(ha_Sub3 var_ha_Sub3) {
		aBoolean4430 = true;
		anInt4439 = 0;
		anInt4437 = 0;
		anInt4433 = 0;
		anInt4447 = 0;
		anInt4449 = 0;
		aBoolean4407 = false;
		aHa_Sub3_4415 = var_ha_Sub3;
		aClass272_4438 = new Class272(null, 5126, 3, 0);
		aClass272_4432 = new Class272(null, 5126, 2, 0);
		aClass272_4428 = new Class272(null, 5126, 3, 0);
		aClass272_4446 = new Class272(null, 5121, 4, 0);
		aClass119_4431 = new Class119();
	}

	Class178_Sub2(ha_Sub3 var_ha_Sub3, Mesh class132, int i, int i_698_, int i_699_, int i_700_) {
		aBoolean4430 = true;
		anInt4439 = 0;
		anInt4437 = 0;
		anInt4433 = 0;
		anInt4447 = 0;
		anInt4449 = 0;
		aBoolean4407 = false;
		aHa_Sub3_4415 = var_ha_Sub3;
		anInt4412 = i;
		anInt4441 = i_700_;
		if (Class296_Sub39_Sub9.method2831(67, i, i_700_))
			aClass272_4438 = new Class272(null, 5126, 3, 0);
		if (Class379_Sub1.method3967(i_700_, true, i))
			aClass272_4432 = new Class272(null, 5126, 2, 0);
		if (ReferenceWrapper.method2789(-1, i_700_, i))
			aClass272_4428 = new Class272(null, 5126, 3, 0);
		if (Class30.method328(i, i_700_, -1))
			aClass272_4446 = new Class272(null, 5121, 4, 0);
		if (Class206.method1990(i, i_700_, (byte) 89))
			aClass119_4431 = new Class119();
		d var_d = var_ha_Sub3.aD1299;
		anIntArray4395 = new int[class132.highest_face_index + 1];
		int[] is = new int[class132.num_faces];
		for (int i_701_ = 0; class132.num_faces > i_701_; i_701_++) {
			if (class132.aByteArray1354 == null || class132.aByteArray1354[i_701_] != 2) {
				if (class132.faces_material != null && class132.faces_material[i_701_] != -1) {
					MaterialRaw class170 = var_d.method14((class132.faces_material[i_701_] & 0xffff), -9412);
					if (((anInt4441 & 0x40) == 0 || !class170.aBoolean1792) && class170.aBoolean1786)
						continue;
				}
				is[anInt4433++] = i_701_;
				anIntArray4395[class132.faces_a[i_701_]]++;
				anIntArray4395[class132.faces_b[i_701_]]++;
				anIntArray4395[class132.faceS_c[i_701_]]++;
			}
		}
		anInt4447 = anInt4433;
		long[] ls = new long[anInt4433];
		boolean bool = (anInt4412 & 0x100) != 0;
		for (int i_702_ = 0; i_702_ < anInt4433; i_702_++) {
			int i_703_ = is[i_702_];
			MaterialRaw class170 = null;
			int i_704_ = 0;
			int i_705_ = 0;
			int i_706_ = 0;
			int i_707_ = 0;
			if (class132.billboards != null) {
				boolean bool_708_ = false;
				for (int i_709_ = 0; i_709_ < class132.billboards.length; i_709_++) {
					Billboard class385 = class132.billboards[i_709_];
					if (i_703_ == class385.face) {
						BillboardRaw class101 = Class296_Sub51_Sub4.method3086((byte) -105, class385.id);
						if (class101.aBoolean1077)
							bool_708_ = true;
						if (class101.texture_id != -1) {
							MaterialRaw class170_710_ = var_d.method14(class101.texture_id, -9412);
							if (class170_710_.anInt1788 == 2)
								aBoolean4407 = true;
						}
					}
				}
				if (bool_708_) {
					ls[i_702_] = 9223372036854775807L;
					anInt4447--;
					continue;
				}
			}
			int i_711_ = -1;
			if (class132.faces_material != null) {
				i_711_ = class132.faces_material[i_703_];
				if (i_711_ != -1) {
					class170 = var_d.method14(i_711_ & 0xffff, -9412);
					if ((anInt4441 & 0x40) != 0 && class170.aBoolean1792) {
						i_711_ = -1;
						class170 = null;
					} else {
						i_706_ = class170.aByte1774;
						if (class170.speed_u != 0 || class170.speed_v != 0)
							aBoolean4408 = true;
						i_707_ = class170.aByte1784;
					}
				}
			}
			boolean bool_712_ = ((class132.faces_alpha != null && class132.faces_alpha[i_703_] != 0)
					|| class170 != null && class170.anInt1788 != 0);
			if ((bool || bool_712_) && class132.faces_priority != null)
				i_704_ += class132.faces_priority[i_703_] << 17;
			if (bool_712_)
				i_704_ += 65536;
			i_704_ += (i_706_ & 0xff) << 8;
			i_704_ += i_707_ & 0xff;
			i_705_ += (i_711_ & 0xffff) << 16;
			i_705_ += i_702_ & 0xffff;
			ls[i_702_] = ((long) i_704_ << 32) + (long) i_705_;
			Class178_Sub2 class178_sub2_713_ = this;
			class178_sub2_713_.aBoolean4408 = (class178_sub2_713_.aBoolean4408
					| (class170 != null && (class170.speed_u != 0 || class170.speed_v != 0)));
			aBoolean4407 |= bool_712_;
		}
		Connection.method1965(ls, -19851, is);
		anIntArray4450 = class132.vertices_y;
		aShortArray4404 = class132.aShortArray1366;
		anInt4437 = class132.highest_face_index;
		anIntArray4406 = class132.vertices_x;
		anInt4449 = class132.num_vertices;
		anIntArray4422 = class132.vertices_z;
		aClass89Array4427 = class132.emitters;
		Class160[] class160s = new Class160[anInt4437];
		aClass232Array4397 = class132.effectors;
		if (class132.billboards != null) {
			anInt4418 = class132.billboards.length;
			aClass331Array4392 = new Class331[anInt4418];
			aClass354Array4405 = new Class354[anInt4418];
			for (int i_714_ = 0; i_714_ < anInt4418; i_714_++) {
				Billboard class385 = class132.billboards[i_714_];
				BillboardRaw class101 = Class296_Sub51_Sub4.method3086((byte) -104, class385.id);
				int i_715_ = -1;
				for (int i_716_ = 0; anInt4433 > i_716_; i_716_++) {
					if (class385.face == is[i_716_]) {
						i_715_ = i_716_;
						break;
					}
				}
				if (i_715_ == -1)
					throw new RuntimeException();
				int i_717_ = ((Class166_Sub1.anIntArray4300[(class132.faces_colour[class385.face] & 0xffff)])
						& 0xffffff);
				i_717_ = i_717_ | -(class132.faces_alpha != null ? class132.faces_alpha[class385.face] : 0) + 255 << 24;
				aClass354Array4405[i_714_] = (new Class354(i_715_, class132.faces_a[class385.face],
						class132.faces_b[class385.face], class132.faceS_c[class385.face], class101.anInt1072,
						class101.anInt1073, class101.texture_id, class101.anInt1079, class101.anInt1076,
						class101.aBoolean1077, class101.aBoolean1082, class385.anInt3257));
				aClass331Array4392[i_714_] = new Class331(i_717_);
			}
		}
		int i_718_ = anInt4433 * 3;
		Class203.aLongArray3566 = new long[i_718_];
		aByteArray4425 = new byte[i_718_];
		if (class132.aShortArray1352 != null)
			aShortArray4396 = new short[anInt4433];
		aShortArray4448 = new short[i_718_];
		aByteArray4426 = new byte[anInt4433];
		aShortArray4413 = new short[i_718_];
		aShort4411 = (short) i_699_;
		aShort4444 = (short) i_698_;
		aShortArray4414 = new short[i_718_];
		aShortArray4442 = new short[anInt4433];
		aShortArray4420 = new short[i_718_];
		aShortArray4394 = new short[anInt4433];
		aShortArray4393 = new short[anInt4433];
		aFloatArray4410 = new float[i_718_];
		aShortArray4416 = new short[anInt4433];
		aShortArray4390 = new short[anInt4433];
		aFloatArray4398 = new float[i_718_];
		int i_719_ = 0;
		for (int i_720_ = 0; i_720_ < class132.highest_face_index; i_720_++) {
			int i_721_ = anIntArray4395[i_720_];
			anIntArray4395[i_720_] = i_719_;
			i_719_ += i_721_;
			class160s[i_720_] = new Class160();
		}
		anIntArray4395[class132.highest_face_index] = i_719_;
		Class221 class221 = Class41_Sub23.method490(-741, is, anInt4433, class132);
		Class314[] class314s = new Class314[class132.num_faces];
		for (int i_722_ = 0; i_722_ < class132.num_faces; i_722_++) {
			short i_723_ = class132.faces_a[i_722_];
			short i_724_ = class132.faces_b[i_722_];
			short i_725_ = class132.faceS_c[i_722_];
			int i_726_ = anIntArray4406[i_724_] - anIntArray4406[i_723_];
			int i_727_ = anIntArray4450[i_724_] - anIntArray4450[i_723_];
			int i_728_ = anIntArray4422[i_724_] - anIntArray4422[i_723_];
			int i_729_ = anIntArray4406[i_725_] - anIntArray4406[i_723_];
			int i_730_ = -anIntArray4450[i_723_] + anIntArray4450[i_725_];
			int i_731_ = -anIntArray4422[i_723_] + anIntArray4422[i_725_];
			int i_732_ = i_731_ * i_727_ - i_730_ * i_728_;
			int i_733_ = -(i_731_ * i_726_) + i_729_ * i_728_;
			int i_734_;
			for (i_734_ = i_730_ * i_726_ - i_727_ * i_729_; (i_732_ > 8192 || i_733_ > 8192 || i_734_ > 8192
					|| i_732_ < -8192 || i_733_ < -8192 || i_734_ < -8192); i_734_ >>= 1) {
				i_732_ >>= 1;
				i_733_ >>= 1;
			}
			int i_735_ = (int) Math.sqrt((double) (i_732_ * i_732_ + i_733_ * i_733_ + i_734_ * i_734_));
			if (i_735_ <= 0)
				i_735_ = 1;
			i_733_ = i_733_ * 256 / i_735_;
			i_734_ = i_734_ * 256 / i_735_;
			i_732_ = i_732_ * 256 / i_735_;
			byte i_736_ = (class132.aByteArray1354 != null ? class132.aByteArray1354[i_722_] : (byte) 0);
			if (i_736_ != 0) {
				if (i_736_ == 1) {
					Class314 class314 = class314s[i_722_] = new Class314();
					class314.anInt2784 = i_732_;
					class314.anInt2783 = i_734_;
					class314.anInt2785 = i_733_;
				}
			} else {
				Class160 class160 = class160s[i_723_];
				class160.anInt1669 += i_732_;
				class160.anInt1670++;
				class160.anInt1667 += i_733_;
				class160.anInt1666 += i_734_;
				class160 = class160s[i_724_];
				class160.anInt1670++;
				class160.anInt1667 += i_733_;
				class160.anInt1666 += i_734_;
				class160.anInt1669 += i_732_;
				class160 = class160s[i_725_];
				class160.anInt1670++;
				class160.anInt1667 += i_733_;
				class160.anInt1666 += i_734_;
				class160.anInt1669 += i_732_;
			}
		}
		for (int i_737_ = 0; i_737_ < anInt4433; i_737_++) {
			int i_738_ = is[i_737_];
			int i_739_ = class132.faces_colour[i_738_] & 0xffff;
			int i_740_;
			if (class132.faces_texture != null)
				i_740_ = class132.faces_texture[i_738_];
			else
				i_740_ = -1;
			int i_741_;
			if (class132.faces_alpha != null)
				i_741_ = class132.faces_alpha[i_738_] & 0xff;
			else
				i_741_ = 0;
			short i_742_ = (class132.faces_material == null ? (short) -1 : class132.faces_material[i_738_]);
			if (i_742_ != -1 && (anInt4441 & 0x40) != 0) {
				MaterialRaw class170 = var_d.method14(i_742_ & 0xffff, -9412);
				if (class170.aBoolean1792)
					i_742_ = (short) -1;
			}
			float f = 0.0F;
			float f_743_ = 0.0F;
			float f_744_ = 0.0F;
			float f_745_ = 0.0F;
			float f_746_ = 0.0F;
			float f_747_ = 0.0F;
			int i_748_ = 0;
			int i_749_ = 0;
			int i_750_ = 0;
			if (i_742_ != -1) {
				if (i_740_ == -1) {
					f_744_ = 1.0F;
					i_749_ = 2;
					f_745_ = 1.0F;
					f_746_ = 0.0F;
					f_743_ = 1.0F;
					f_747_ = 0.0F;
					f = 0.0F;
					i_748_ = 1;
				} else {
					i_740_ &= 0xff;
					byte i_751_ = class132.textures_mapping_type[i_740_];
					if (i_751_ == 0) {
						short i_752_ = class132.faces_a[i_738_];
						short i_753_ = class132.faces_b[i_738_];
						short i_754_ = class132.faceS_c[i_738_];
						short i_755_ = class132.textures_mapping_p[i_740_];
						short i_756_ = class132.textures_mapping_m[i_740_];
						short i_757_ = class132.textures_mapping_n[i_740_];
						float f_758_ = (float) class132.vertices_x[i_755_];
						float f_759_ = (float) class132.vertices_y[i_755_];
						float f_760_ = (float) class132.vertices_z[i_755_];
						float f_761_ = (-f_758_ + (float) class132.vertices_x[i_756_]);
						float f_762_ = (float) class132.vertices_y[i_756_] - f_759_;
						float f_763_ = (-f_760_ + (float) class132.vertices_z[i_756_]);
						float f_764_ = (-f_758_ + (float) class132.vertices_x[i_757_]);
						float f_765_ = (float) class132.vertices_y[i_757_] - f_759_;
						float f_766_ = (float) class132.vertices_z[i_757_] - f_760_;
						float f_767_ = (-f_758_ + (float) class132.vertices_x[i_752_]);
						float f_768_ = (-f_759_ + (float) class132.vertices_y[i_752_]);
						float f_769_ = (-f_760_ + (float) class132.vertices_z[i_752_]);
						float f_770_ = (-f_758_ + (float) class132.vertices_x[i_753_]);
						float f_771_ = (-f_759_ + (float) class132.vertices_y[i_753_]);
						float f_772_ = (float) class132.vertices_z[i_753_] - f_760_;
						float f_773_ = (-f_758_ + (float) class132.vertices_x[i_754_]);
						float f_774_ = (float) class132.vertices_y[i_754_] - f_759_;
						float f_775_ = (-f_760_ + (float) class132.vertices_z[i_754_]);
						float f_776_ = -(f_765_ * f_763_) + f_766_ * f_762_;
						float f_777_ = -(f_761_ * f_766_) + f_764_ * f_763_;
						float f_778_ = -(f_762_ * f_764_) + f_765_ * f_761_;
						float f_779_ = f_778_ * f_765_ - f_777_ * f_766_;
						float f_780_ = f_766_ * f_776_ - f_764_ * f_778_;
						float f_781_ = f_777_ * f_764_ - f_776_ * f_765_;
						float f_782_ = 1.0F / (f_780_ * f_762_ + f_761_ * f_779_ + f_763_ * f_781_);
						f_746_ = (f_775_ * f_781_ + (f_779_ * f_773_ + f_780_ * f_774_)) * f_782_;
						f_744_ = (f_772_ * f_781_ + (f_771_ * f_780_ + f_770_ * f_779_)) * f_782_;
						f = (f_769_ * f_781_ + (f_780_ * f_768_ + f_767_ * f_779_)) * f_782_;
						f_781_ = -(f_762_ * f_776_) + f_777_ * f_761_;
						f_779_ = -(f_763_ * f_777_) + f_762_ * f_778_;
						f_780_ = f_776_ * f_763_ - f_761_ * f_778_;
						f_782_ = 1.0F / (f_780_ * f_765_ + f_764_ * f_779_ + f_781_ * f_766_);
						f_747_ = (f_780_ * f_774_ + f_773_ * f_779_ + f_781_ * f_775_) * f_782_;
						f_745_ = (f_780_ * f_771_ + f_770_ * f_779_ + f_772_ * f_781_) * f_782_;
						f_743_ = f_782_ * (f_769_ * f_781_ + (f_767_ * f_779_ + f_768_ * f_780_));
					} else {
						short i_783_ = class132.faces_a[i_738_];
						short i_784_ = class132.faces_b[i_738_];
						short i_785_ = class132.faceS_c[i_738_];
						int i_786_ = class221.anIntArray2153[i_740_];
						int i_787_ = class221.anIntArray2157[i_740_];
						int i_788_ = class221.anIntArray2154[i_740_];
						float[] fs = class221.aFloatArrayArray2156[i_740_];
						byte i_789_ = class132.aByteArray1367[i_740_];
						float f_790_ = (float) class132.anIntArray1371[i_740_] / 256.0F;
						if (i_751_ == 1) {
							float f_791_ = ((float) class132.anIntArray1379[i_740_] / 1024.0F);
							Class215.method2027(class132.vertices_x[i_783_], fs, i_786_, f_791_,
									class132.vertices_y[i_783_], i_788_, -3, f_790_, Class181_Sub1.aFloatArray4515,
									class132.vertices_z[i_783_], i_789_, i_787_);
							f = Class181_Sub1.aFloatArray4515[0];
							f_743_ = Class181_Sub1.aFloatArray4515[1];
							Class215.method2027(class132.vertices_x[i_784_], fs, i_786_, f_791_,
									class132.vertices_y[i_784_], i_788_, -3, f_790_, Class181_Sub1.aFloatArray4515,
									class132.vertices_z[i_784_], i_789_, i_787_);
							f_745_ = Class181_Sub1.aFloatArray4515[1];
							f_744_ = Class181_Sub1.aFloatArray4515[0];
							Class215.method2027(class132.vertices_x[i_785_], fs, i_786_, f_791_,
									class132.vertices_y[i_785_], i_788_, -3, f_790_, Class181_Sub1.aFloatArray4515,
									class132.vertices_z[i_785_], i_789_, i_787_);
							f_747_ = Class181_Sub1.aFloatArray4515[1];
							f_746_ = Class181_Sub1.aFloatArray4515[0];
							float f_792_ = f_791_ / 2.0F;
							if ((i_789_ & 0x1) != 0) {
								if (f_747_ - f_743_ > f_792_) {
									i_749_ = 1;
									f_747_ -= f_791_;
								} else if (f_743_ - f_747_ > f_792_) {
									f_747_ += f_791_;
									i_749_ = 2;
								}
								if (f_745_ - f_743_ > f_792_) {
									i_748_ = 1;
									f_745_ -= f_791_;
								} else if (f_792_ < f_743_ - f_745_) {
									i_748_ = 2;
									f_745_ += f_791_;
								}
							} else {
								if (f_746_ - f > f_792_) {
									i_749_ = 1;
									f_746_ -= f_791_;
								} else if (f - f_746_ > f_792_) {
									f_746_ += f_791_;
									i_749_ = 2;
								}
								if (f_792_ < f_744_ - f) {
									i_748_ = 1;
									f_744_ -= f_791_;
								} else if (-f_744_ + f > f_792_) {
									f_744_ += f_791_;
									i_748_ = 2;
								}
							}
						} else if (i_751_ == 2) {
							float f_793_ = ((float) class132.anIntArray1347[i_740_] / 256.0F);
							float f_794_ = ((float) class132.anIntArray1348[i_740_] / 256.0F);
							int i_795_ = (class132.vertices_x[i_784_] - class132.vertices_x[i_783_]);
							int i_796_ = (-class132.vertices_y[i_783_] + class132.vertices_y[i_784_]);
							int i_797_ = (-class132.vertices_z[i_783_] + class132.vertices_z[i_784_]);
							int i_798_ = (class132.vertices_x[i_785_] - class132.vertices_x[i_783_]);
							int i_799_ = (-class132.vertices_y[i_783_] + class132.vertices_y[i_785_]);
							int i_800_ = (-class132.vertices_z[i_783_] + class132.vertices_z[i_785_]);
							int i_801_ = i_800_ * i_796_ - i_799_ * i_797_;
							int i_802_ = i_797_ * i_798_ - i_800_ * i_795_;
							int i_803_ = -(i_798_ * i_796_) + i_799_ * i_795_;
							float f_804_ = (64.0F / (float) class132.anIntArray1344[i_740_]);
							float f_805_ = (64.0F / (float) class132.anIntArray1365[i_740_]);
							float f_806_ = (64.0F / (float) class132.anIntArray1379[i_740_]);
							float f_807_ = ((fs[1] * (float) i_802_ + (float) i_801_ * fs[0] + fs[2] * (float) i_803_)
									/ f_804_);
							float f_808_ = (((float) i_801_ * fs[3] + fs[4] * (float) i_802_ + (float) i_803_ * fs[5])
									/ f_805_);
							float f_809_ = (((float) i_803_ * fs[8] + ((float) i_802_ * fs[7] + (float) i_801_ * fs[6]))
									/ f_806_);
							i_750_ = ConfigurationsLoader.method184(f_808_, (byte) 83, f_807_, f_809_);
							Class245.method2185(i_789_, Class181_Sub1.aFloatArray4515, i_750_,
									class132.vertices_y[i_783_], i_787_, f_794_, f_793_, 109, i_786_, f_790_, i_788_,
									class132.vertices_z[i_783_], fs, class132.vertices_x[i_783_]);
							f_743_ = Class181_Sub1.aFloatArray4515[1];
							f = Class181_Sub1.aFloatArray4515[0];
							Class245.method2185(i_789_, Class181_Sub1.aFloatArray4515, i_750_,
									class132.vertices_y[i_784_], i_787_, f_794_, f_793_, 85, i_786_, f_790_, i_788_,
									class132.vertices_z[i_784_], fs, class132.vertices_x[i_784_]);
							f_744_ = Class181_Sub1.aFloatArray4515[0];
							f_745_ = Class181_Sub1.aFloatArray4515[1];
							Class245.method2185(i_789_, Class181_Sub1.aFloatArray4515, i_750_,
									class132.vertices_y[i_785_], i_787_, f_794_, f_793_, 63, i_786_, f_790_, i_788_,
									class132.vertices_z[i_785_], fs, class132.vertices_x[i_785_]);
							f_746_ = Class181_Sub1.aFloatArray4515[0];
							f_747_ = Class181_Sub1.aFloatArray4515[1];
						} else if (i_751_ == 3) {
							Class296_Sub51_Sub39.method3201(i_789_, i_786_, fs, class132.vertices_x[i_783_], 103,
									Class181_Sub1.aFloatArray4515, class132.vertices_y[i_783_], i_787_, f_790_, i_788_,
									class132.vertices_z[i_783_]);
							f_743_ = Class181_Sub1.aFloatArray4515[1];
							f = Class181_Sub1.aFloatArray4515[0];
							Class296_Sub51_Sub39.method3201(i_789_, i_786_, fs, class132.vertices_x[i_784_], 120,
									Class181_Sub1.aFloatArray4515, class132.vertices_y[i_784_], i_787_, f_790_, i_788_,
									class132.vertices_z[i_784_]);
							f_744_ = Class181_Sub1.aFloatArray4515[0];
							f_745_ = Class181_Sub1.aFloatArray4515[1];
							Class296_Sub51_Sub39.method3201(i_789_, i_786_, fs, class132.vertices_x[i_785_], 85,
									Class181_Sub1.aFloatArray4515, class132.vertices_y[i_785_], i_787_, f_790_, i_788_,
									class132.vertices_z[i_785_]);
							f_746_ = Class181_Sub1.aFloatArray4515[0];
							f_747_ = Class181_Sub1.aFloatArray4515[1];
							if ((i_789_ & 0x1) != 0) {
								if (!(-f_743_ + f_747_ > 0.5F)) {
									if (-f_747_ + f_743_ > 0.5F) {
										f_747_++;
										i_749_ = 2;
									}
								} else {
									f_747_--;
									i_749_ = 1;
								}
								if (-f_743_ + f_745_ > 0.5F) {
									i_748_ = 1;
									f_745_--;
								} else if (f_743_ - f_745_ > 0.5F) {
									i_748_ = 2;
									f_745_++;
								}
							} else {
								if (!(-f + f_744_ > 0.5F)) {
									if (f - f_744_ > 0.5F) {
										f_744_++;
										i_748_ = 2;
									}
								} else {
									i_748_ = 1;
									f_744_--;
								}
								if (!(-f + f_746_ > 0.5F)) {
									if (f - f_746_ > 0.5F) {
										i_749_ = 2;
										f_746_++;
									}
								} else {
									f_746_--;
									i_749_ = 1;
								}
							}
						}
					}
				}
			}
			byte i_810_;
			if (class132.aByteArray1354 == null)
				i_810_ = (byte) 0;
			else
				i_810_ = class132.aByteArray1354[i_738_];
			if (i_810_ != 0) {
				if (i_810_ == 1) {
					Class314 class314 = class314s[i_738_];
					long l = ((long) ((class314.anInt2783 + 256 << 22) + (class314.anInt2785 + 256 << 12)
							+ (class314.anInt2784 <= 0 ? 2048 : 1024) + (i_740_ << 2))
							- -((long) i_741_ + ((long) (i_750_ << 24) + (long) (i_739_ << 8)) << 32));
					aShortArray4442[i_737_] = method1792(-68, 0, class132, class314.anInt2785, class132.faces_a[i_738_],
							class314.anInt2783, f, class314.anInt2784, f_743_, l);
					aShortArray4393[i_737_] = method1792(-121, 0, class132, class314.anInt2785,
							class132.faces_b[i_738_], class314.anInt2783, f_744_, class314.anInt2784, f_745_,
							l + (long) i_748_);
					aShortArray4390[i_737_] = method1792(-36, 0, class132, class314.anInt2785, class132.faceS_c[i_738_],
							class314.anInt2783, f_746_, class314.anInt2784, f_747_, l + (long) i_749_);
				}
			} else {
				long l = (((long) (i_739_ << 8) + ((long) (i_750_ << 24) - -(long) i_741_) << 32)
						+ (long) (i_740_ << 2));
				short i_811_ = class132.faces_a[i_738_];
				short i_812_ = class132.faces_b[i_738_];
				short i_813_ = class132.faceS_c[i_738_];
				Class160 class160 = class160s[i_811_];
				aShortArray4442[i_737_] = method1792(-91, class160.anInt1670, class132, class160.anInt1667, i_811_,
						class160.anInt1666, f, class160.anInt1669, f_743_, l);
				class160 = class160s[i_812_];
				aShortArray4393[i_737_] = method1792(-108, class160.anInt1670, class132, class160.anInt1667, i_812_,
						class160.anInt1666, f_744_, class160.anInt1669, f_745_, l + (long) i_748_);
				class160 = class160s[i_813_];
				aShortArray4390[i_737_] = method1792(-63, class160.anInt1670, class132, class160.anInt1667, i_813_,
						class160.anInt1666, f_746_, class160.anInt1669, f_747_, (long) i_749_ + l);
			}
			if (class132.faces_alpha != null)
				aByteArray4426[i_737_] = class132.faces_alpha[i_738_];
			if (class132.aShortArray1352 != null)
				aShortArray4396[i_737_] = class132.aShortArray1352[i_738_];
			aShortArray4394[i_737_] = class132.faces_colour[i_738_];
			aShortArray4416[i_737_] = i_742_;
		}
		int i_814_ = 0;
		short i_815_ = -10000;
		for (int i_816_ = 0; i_816_ < anInt4447; i_816_++) {
			short i_817_ = aShortArray4416[i_816_];
			if (i_817_ != i_815_) {
				i_814_++;
				i_815_ = i_817_;
			}
		}
		anIntArray4421 = new int[i_814_ + 1];
		i_815_ = (short) -10000;
		i_814_ = 0;
		for (int i_818_ = 0; anInt4447 > i_818_; i_818_++) {
			short i_819_ = aShortArray4416[i_818_];
			if (i_819_ != i_815_) {
				anIntArray4421[i_814_++] = i_818_;
				i_815_ = i_819_;
			}
		}
		anIntArray4421[i_814_] = anInt4447;
		Class203.aLongArray3566 = null;
		aShortArray4420 = NPCNode.method2449(0, anInt4439, aShortArray4420);
		aShortArray4448 = NPCNode.method2449(0, anInt4439, aShortArray4448);
		aShortArray4413 = NPCNode.method2449(0, anInt4439, aShortArray4413);
		aByteArray4425 = Class343_Sub1.method3651(-3, anInt4439, aByteArray4425);
		aFloatArray4410 = Class360_Sub10.method3753(0, anInt4439, aFloatArray4410);
		aFloatArray4398 = Class360_Sub10.method3753(0, anInt4439, aFloatArray4398);
		if (class132.vertices_label != null && s_Sub3.method3372(anInt4441, -1, i))
			anIntArrayArray4429 = class132.method1385((byte) 111, false);
		if (class132.billboards != null && Class268.method2297(anInt4441, i, true))
			anIntArrayArray4443 = class132.method1390((byte) 115);
		if (class132.faces_label != null && Class254.method2211(i, anInt4441, 384)) {
			int i_820_ = 0;
			int[] is_821_ = new int[256];
			for (int i_822_ = 0; i_822_ < anInt4433; i_822_++) {
				int i_823_ = class132.faces_label[is[i_822_]];
				if (i_823_ >= 0) {
					is_821_[i_823_]++;
					if (i_823_ > i_820_)
						i_820_ = i_823_;
				}
			}
			anIntArrayArray4403 = new int[i_820_ + 1][];
			for (int i_824_ = 0; i_820_ >= i_824_; i_824_++) {
				anIntArrayArray4403[i_824_] = new int[is_821_[i_824_]];
				is_821_[i_824_] = 0;
			}
			for (int i_825_ = 0; i_825_ < anInt4433; i_825_++) {
				int i_826_ = class132.faces_label[is[i_825_]];
				if (i_826_ >= 0)
					anIntArrayArray4403[i_826_][is_821_[i_826_]++] = i_825_;
			}
		}
	}

	static {
		new Class81("", 76);
	}
}
