package net.zaros.client;
/* Class219_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.DataInputStream;
import java.io.IOException;
import java.net.URL;

final class Class219_Sub1 extends Class219 {
	private byte[][] aByteArrayArray4562 = new byte[10][];
	private int anInt4563;
	static int anInt4564;
	private int[] anIntArray4565;
	private int anInt4566;
	private Packet aClass296_Sub17_4567 = new Packet(null);
	private Js5 aClass138_4568;
	static int anInt4569;
	static boolean[] loadedInterfaces;
	private Packet aClass296_Sub17_4571 = new Packet(null);

	static final void method2061(byte i, int i_0_) {
		synchronized (Class258.aClass113_2415) {
			Class258.aClass113_2415.clean(i_0_);
		}
		synchronized (ParticleEmitterRaw.aClass113_1725) {
			ParticleEmitterRaw.aClass113_1725.clean(i_0_);
		}
		if (i > 0)
			anInt4564 = 97;
	}

	static final void method2062(String string, byte i, Throwable throwable) {
		do {
			try {
				String string_1_ = "";
				if (throwable != null)
					string_1_ = Class399.method4134(92, throwable);
				if (string != null) {
					if (throwable != null)
						string_1_ += " | ";
					string_1_ += (String) string;
				}
				Class296_Sub20.method2653(string_1_, (byte) -42);
				string_1_ = Class76.method784(string_1_, "%3a", ":", (byte) -43);
				string_1_ = Class76.method784(string_1_, "%40", "@", (byte) -55);
				string_1_ = Class76.method784(string_1_, "%26", "&", (byte) -70);
				string_1_ = Class76.method784(string_1_, "%23", "#", (byte) -76);
				if (Class122_Sub1.anApplet5656 != null) {
					Class278 class278 = (Class281.aClass398_2600.method4130((byte) -121, new URL(Class122_Sub1.anApplet5656.getCodeBase(), ("clienterror.ws?c=" + Class338_Sub3_Sub2.anInt6565 + "&u=" + (Class366_Sub6.name3 == null ? String.valueOf(Class187.aLong1922) : Class366_Sub6.name3) + "&v1=" + Class398.aString3343 + "&v2=" + Class398.aString3333 + "&e=" + string_1_))));
					int i_2_ = 92 / ((i + 60) / 35);
					while (class278.anInt2540 == 0)
						Class106_Sub1.method942(1L, 0);
					if (class278.anInt2540 != 1)
						break;
					DataInputStream datainputstream = (DataInputStream) class278.anObject2539;
					datainputstream.read();
					datainputstream.close();
				}
			} catch (Exception exception) {
				/* empty */
			}
			break;
		} while (false);
	}

	final int method2049(byte i, byte[] is) throws IOException {
		if (anIntArray4565 == null) {
			if (!aClass138_4568.fileExists(anInt4563, 0))
				return 0;
			byte[] is_3_ = aClass138_4568.getFile(anInt4563, 0);
			if (is_3_ == null)
				throw new IllegalStateException("");
			aClass296_Sub17_4571.pos = 0;
			aClass296_Sub17_4571.data = is_3_;
			int i_4_ = is_3_.length >> 1;
			anIntArray4565 = new int[i_4_];
			for (int i_5_ = 0; i_5_ < i_4_; i_5_++)
				anIntArray4565[i_5_] = aClass296_Sub17_4571.g2();
		}
		if (anIntArray4565.length <= anInt4566)
			return -1;
		method2065(10);
		int i_6_ = -38 / ((i - 50) / 39);
		aClass296_Sub17_4571.pos = 0;
		aClass296_Sub17_4571.data = is;
		while (aClass296_Sub17_4571.data.length > aClass296_Sub17_4571.pos) {
			if (aClass296_Sub17_4567.data == null) {
				if (aByteArrayArray4562[0] == null) {
					aClass296_Sub17_4571.data = null;
					return aClass296_Sub17_4571.pos;
				}
				aClass296_Sub17_4567.data = aByteArrayArray4562[0];
			}
			int i_7_ = (aClass296_Sub17_4571.data.length - aClass296_Sub17_4571.pos);
			int i_8_ = (-aClass296_Sub17_4567.pos + aClass296_Sub17_4567.data.length);
			if (i_8_ <= i_7_) {
				aClass296_Sub17_4571.writeBytes((aClass296_Sub17_4567.data), aClass296_Sub17_4567.pos, i_8_);
				anInt4566++;
				aClass296_Sub17_4567.data = null;
				aClass296_Sub17_4567.pos = 0;
				for (int i_9_ = 0; i_9_ < 9; i_9_++)
					aByteArrayArray4562[i_9_] = aByteArrayArray4562[i_9_ + 1];
				aByteArrayArray4562[9] = null;
				if (anInt4566 >= anIntArray4565.length) {
					aClass296_Sub17_4571.data = null;
					return aClass296_Sub17_4571.pos;
				}
			} else {
				aClass296_Sub17_4567.readBytes((aClass296_Sub17_4571.data), aClass296_Sub17_4571.pos, i_7_);
				aClass296_Sub17_4571.data = null;
				return is.length;
			}
		}
		aClass296_Sub17_4571.data = null;
		return is.length;
	}

	static final void writeCrcValues(ByteStream class296_sub17_sub1, byte i) {
		class296_sub17_sub1.p4(Class381.fs0.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class270.fs1.getReferenceCRC(i + 22268));
		class296_sub17_sub1.p4(Class296_Sub39_Sub1.fs2.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class360_Sub6.fs3.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class93.fs4.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class324.fs5.getReferenceCRC(i ^ 0x5704));
		class296_sub17_sub1.p4(Class42_Sub4.fs6.getReferenceCRC(22354));
		if (i != 86)
			getChildID((byte) 18, 43);
		class296_sub17_sub1.p4(Class184.fs7.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class205_Sub2.fs8.getReferenceCRC(i ^ 0x5704));
		class296_sub17_sub1.p4(Class205_Sub1.aClass138_5641.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class296_Sub51_Sub29.aClass138_6494.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class296_Sub22.aClass138_4722.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class72.cs2FS.getReferenceCRC(22354));
		class296_sub17_sub1.p4(LookupTable.aClass138_53.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class296_Sub15.aClass138_4671.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class33.aClass138_329.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class326.fs16.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class296_Sub30.aClass138_4822.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class199.aClass138_2005.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class63.aClass138_726.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class196.fs20.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class365.gfxFS.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class296_Sub51_Sub17.aClass138_6429.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class205_Sub2.aClass138_5631.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class296_Sub51_Sub34.aClass138_6523.getReferenceCRC(i ^ 0x5704));
		class296_sub17_sub1.p4(Class296_Sub13.aClass138_4653.getReferenceCRC(22354));
		class296_sub17_sub1.p4(StaticMethods.aClass138_5927.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class324.aClass138_2862.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class296_Sub51_Sub21.fs28.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class338_Sub2.aClass138_5201.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class211.aClass138_2099.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class122_Sub1_Sub1.aClass138_6604.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class134.fs35.getReferenceCRC(22354));
		class296_sub17_sub1.p4(Class296_Sub14.method2512(false));
		class296_sub17_sub1.p4(LongNode.method2658(-115));
		class296_sub17_sub1.p4(Class77.aClass138_879.getReferenceCRC(22354));
	}

	static final int getChildID(byte i, int i_10_) {
		if (i != -44)
			loadedInterfaces = null;
		return i_10_ & 0x3ff;
	}

	final void method2065(int i) {
		if (anIntArray4565 != null && i == 10) {
			int i_11_ = 0;
			while_128_ : do {
				for (;;) {
					if (i_11_ >= 10)
						break while_128_;
					if (i_11_ + anInt4566 >= anIntArray4565.length)
						break;
					if (aByteArrayArray4562[i_11_] == null && aClass138_4568.fileExists((anIntArray4565[i_11_ + anInt4566]), 0))
						aByteArrayArray4562[i_11_] = aClass138_4568.getFile((anIntArray4565[anInt4566 + i_11_]), 0);
					i_11_++;
				}
				break;
			} while (false);
		}
	}

	public static void method2066(int i) {
		if (i != -7523)
			getChildID((byte) 58, -23);
		loadedInterfaces = null;
	}

	Class219_Sub1(int i, Js5 class138, int i_12_) {
		super(i);
		aClass138_4568 = class138;
		anInt4563 = i_12_;
	}

	static final void method2067(boolean bool) {
		if (bool != true)
			anInt4569 = 64;
		Class296_Sub34 class296_sub34 = (Class296_Sub34) Class403.aClass155_3379.removeFirst((byte) 115);
		boolean bool_13_ = InvisiblePlayer.aClass51_1982 != null || Class2.anInt59 > 0;
		int i = class296_sub34.method2732(-6);
		int i_14_ = class296_sub34.method2736(-6);
		if (bool_13_)
			Class399.anInt3357 = 1;
		if (!bool_13_)
			Class97.method873(i_14_, i, 106, Class216.aClass296_Sub39_Sub9_2115);
		else
			Class296_Sub17_Sub2.aClass296_Sub39_Sub9_6046 = Class216.aClass296_Sub39_Sub9_2115;
	}
}
