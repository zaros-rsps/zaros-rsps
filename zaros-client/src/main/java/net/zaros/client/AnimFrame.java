package net.zaros.client;

/* Class60 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class AnimFrame {
	short[] aShortArray679;
	private static byte[] aByteArray680;
	boolean aBoolean681;
	short[] aShortArray682;
	private static short[] aShortArray683 = new short[500];
	boolean aBoolean684 = false;
	short[] aShortArray685;
	byte[] aByteArray686;
	short[] aShortArray687;
	boolean aBoolean688 = false;
	private static short[] aShortArray689 = new short[500];
	private static short[] aShortArray690;
	int anInt691 = 0;
	private static short[] aShortArray692;
	short[] aShortArray693;
	AnimBase aClass296_Sub26_694;
	private static short[] aShortArray695;

	public static void method688() {
		aShortArray689 = null;
		aShortArray695 = null;
		aShortArray692 = null;
		aShortArray690 = null;
		aShortArray683 = null;
		aByteArray680 = null;
	}

	AnimFrame(byte[] is, AnimBase class296_sub26) {
		aBoolean681 = false;
		aClass296_Sub26_694 = null;
		aClass296_Sub26_694 = class296_sub26;
		try {
			Packet class296_sub17 = new Packet(is);
			Packet class296_sub17_0_ = new Packet(is);
			class296_sub17.g1();
			class296_sub17.pos += 2;
			int i = class296_sub17.g1();
			int i_1_ = 0;
			int i_2_ = -1;
			int i_3_ = -1;
			class296_sub17_0_.pos = class296_sub17.pos + i;
			for (int i_4_ = 0; i_4_ < i; i_4_++) {
				int i_5_ = aClass296_Sub26_694.anIntArray4768[i_4_];
				if (i_5_ == 0) {
					i_2_ = i_4_;
				}
				int i_6_ = class296_sub17.g1();
				if (i_6_ > 0) {
					if (i_5_ == 0) {
						i_3_ = i_4_;
					}
					aShortArray689[i_1_] = (short) i_4_;
					short i_7_ = 0;
					if (i_5_ == 3 || i_5_ == 10) {
						i_7_ = (short) 128;
					}
					if ((i_6_ & 0x1) != 0) {
						aShortArray695[i_1_] = (short) class296_sub17_0_.gSmart1or2s();
					} else {
						aShortArray695[i_1_] = i_7_;
					}
					if ((i_6_ & 0x2) != 0) {
						aShortArray692[i_1_] = (short) class296_sub17_0_.gSmart1or2s();
					} else {
						aShortArray692[i_1_] = i_7_;
					}
					if ((i_6_ & 0x4) != 0) {
						aShortArray690[i_1_] = (short) class296_sub17_0_.gSmart1or2s();
					} else {
						aShortArray690[i_1_] = i_7_;
					}
					aByteArray680[i_1_] = (byte) (i_6_ >>> 3 & 0x3);
					if (i_5_ == 2 || i_5_ == 9) {
						aShortArray695[i_1_] = (short) (aShortArray695[i_1_] << 2 & 0x3fff);
						aShortArray692[i_1_] = (short) (aShortArray692[i_1_] << 2 & 0x3fff);
						aShortArray690[i_1_] = (short) (aShortArray690[i_1_] << 2 & 0x3fff);
					}
					aShortArray683[i_1_] = (short) -1;
					if (i_5_ == 1 || i_5_ == 2 || i_5_ == 3) {
						if (i_2_ > i_3_) {
							aShortArray683[i_1_] = (short) i_2_;
							i_3_ = i_2_;
						}
					} else if (i_5_ == 5) {
						aBoolean688 = true;
					} else if (i_5_ == 7) {
						aBoolean684 = true;
					} else if (i_5_ == 9 || i_5_ == 10 || i_5_ == 8) {
						aBoolean681 = true;
					}
					i_1_++;
				}
			}
			if (class296_sub17_0_.pos != is.length) {
				throw new RuntimeException();
			}
			anInt691 = i_1_;
			aShortArray685 = new short[i_1_];
			aShortArray682 = new short[i_1_];
			aShortArray693 = new short[i_1_];
			aShortArray679 = new short[i_1_];
			aShortArray687 = new short[i_1_];
			aByteArray686 = new byte[i_1_];
			for (int i_8_ = 0; i_8_ < i_1_; i_8_++) {
				aShortArray685[i_8_] = aShortArray689[i_8_];
				aShortArray682[i_8_] = aShortArray695[i_8_];
				aShortArray693[i_8_] = aShortArray692[i_8_];
				aShortArray679[i_8_] = aShortArray690[i_8_];
				aShortArray687[i_8_] = aShortArray683[i_8_];
				aByteArray686[i_8_] = aByteArray680[i_8_];
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			anInt691 = 0;
			aBoolean688 = false;
			aBoolean684 = false;
		}
	}

	static {
		aByteArray680 = new byte[500];
		aShortArray692 = new short[500];
		aShortArray695 = new short[500];
		aShortArray690 = new short[500];
	}
}
