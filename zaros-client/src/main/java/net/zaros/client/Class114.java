package net.zaros.client;

/* Class114 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class114 {
	private static float aFloat1173;
	static int anInt1174;
	private static float[][] aFloatArrayArray1175 = new float[2][8];
	private int[] anIntArray1176;
	private int[][][] anIntArrayArrayArray1177;
	private int[][][] anIntArrayArrayArray1178 = new int[2][2][4];
	int[] anIntArray1179 = new int[2];
	static int[][] anIntArrayArray1180 = new int[2][8];

	private final float method1000(int i, int i_0_, float f) {
		float f_1_ = ((float) anIntArrayArrayArray1178[i][0][i_0_] + f * (float) (anIntArrayArrayArray1178[i][1][i_0_] - anIntArrayArrayArray1178[i][0][i_0_]));
		f_1_ *= 1.2207031E-4F;
		return method1002(f_1_);
	}

	final void method1001(Packet class296_sub17, Class20 class20) {
		int i = class296_sub17.g1();
		anIntArray1179[0] = i >> 4;
		anIntArray1179[1] = i & 0xf;
		if (i != 0) {
			anIntArray1176[0] = class296_sub17.g2();
			anIntArray1176[1] = class296_sub17.g2();
			int i_2_ = class296_sub17.g1();
			for (int i_3_ = 0; i_3_ < 2; i_3_++) {
				for (int i_4_ = 0; i_4_ < anIntArray1179[i_3_]; i_4_++) {
					anIntArrayArrayArray1178[i_3_][0][i_4_] = class296_sub17.g2();
					anIntArrayArrayArray1177[i_3_][0][i_4_] = class296_sub17.g2();
				}
			}
			for (int i_5_ = 0; i_5_ < 2; i_5_++) {
				for (int i_6_ = 0; i_6_ < anIntArray1179[i_5_]; i_6_++) {
					if ((i_2_ & 1 << i_5_ * 4 << i_6_) != 0) {
						anIntArrayArrayArray1178[i_5_][1][i_6_] = class296_sub17.g2();
						anIntArrayArrayArray1177[i_5_][1][i_6_] = class296_sub17.g2();
					} else {
						anIntArrayArrayArray1178[i_5_][1][i_6_] = anIntArrayArrayArray1178[i_5_][0][i_6_];
						anIntArrayArrayArray1177[i_5_][1][i_6_] = anIntArrayArrayArray1177[i_5_][0][i_6_];
					}
				}
			}
			if (i_2_ != 0 || anIntArray1176[1] != anIntArray1176[0])
				class20.method285(class296_sub17);
		} else
			anIntArray1176[0] = anIntArray1176[1] = 0;
	}

	private static final float method1002(float f) {
		float f_7_ = (float) Math.pow(2.0, (double) f) * 32.703197F;
		return f_7_ * 3.1415927F / 11025.0F;
	}

	final int method1003(int i, float f) {
		if (i == 0) {
			float f_8_ = ((float) anIntArray1176[0] + (float) (anIntArray1176[1] - anIntArray1176[0]) * f);
			f_8_ *= 0.0030517578F;
			aFloat1173 = (float) Math.pow(0.1, (double) (f_8_ / 20.0F));
			anInt1174 = (int) (aFloat1173 * 65536.0F);
		}
		if (anIntArray1179[i] == 0)
			return 0;
		float f_9_ = method1005(i, 0, f);
		aFloatArrayArray1175[i][0] = f_9_ * -2.0F * (float) Math.cos((double) method1000(i, 0, f));
		aFloatArrayArray1175[i][1] = f_9_ * f_9_;
		for (int i_10_ = 1; i_10_ < anIntArray1179[i]; i_10_++) {
			f_9_ = method1005(i, i_10_, f);
			float f_11_ = (f_9_ * -2.0F * (float) Math.cos((double) method1000(i, i_10_, f)));
			float f_12_ = f_9_ * f_9_;
			aFloatArrayArray1175[i][i_10_ * 2 + 1] = aFloatArrayArray1175[i][i_10_ * 2 - 1] * f_12_;
			aFloatArrayArray1175[i][i_10_ * 2] = (aFloatArrayArray1175[i][i_10_ * 2 - 1] * f_11_ + aFloatArrayArray1175[i][i_10_ * 2 - 2] * f_12_);
			for (int i_13_ = i_10_ * 2 - 1; i_13_ >= 2; i_13_--)
				aFloatArrayArray1175[i][i_13_] += (aFloatArrayArray1175[i][i_13_ - 1] * f_11_ + aFloatArrayArray1175[i][i_13_ - 2] * f_12_);
			aFloatArrayArray1175[i][1] += aFloatArrayArray1175[i][0] * f_11_ + f_12_;
			aFloatArrayArray1175[i][0] += f_11_;
		}
		if (i == 0) {
			for (int i_14_ = 0; i_14_ < anIntArray1179[0] * 2; i_14_++)
				aFloatArrayArray1175[0][i_14_] *= aFloat1173;
		}
		for (int i_15_ = 0; i_15_ < anIntArray1179[i] * 2; i_15_++)
			anIntArrayArray1180[i][i_15_] = (int) (aFloatArrayArray1175[i][i_15_] * 65536.0F);
		return anIntArray1179[i] * 2;
	}

	public static void method1004() {
		aFloatArrayArray1175 = null;
		anIntArrayArray1180 = null;
	}

	private final float method1005(int i, int i_16_, float f) {
		float f_17_ = ((float) anIntArrayArrayArray1177[i][0][i_16_] + f * (float) (anIntArrayArrayArray1177[i][1][i_16_] - anIntArrayArrayArray1177[i][0][i_16_]));
		f_17_ *= 0.0015258789F;
		return 1.0F - (float) Math.pow(10.0, (double) (-f_17_ / 20.0F));
	}

	public Class114() {
		anIntArrayArrayArray1177 = new int[2][2][4];
		anIntArray1176 = new int[2];
	}
}
