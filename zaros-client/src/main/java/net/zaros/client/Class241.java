package net.zaros.client;

public abstract class Class241 {
	static short aShort2299;
	static int anInt2300 = 0;
	static int anInt2301;
	static Class296_Sub39_Sub1 aClass296_Sub39_Sub1_2302;
	static NPCNode[] npcNodes = new NPCNode[1024];
	static IncomingPacket aClass231_2304;

	static final void findPathToItem(int toX, int toY) {
		if (Class296_Sub50.game == Class296_Sub32.stellardawn) {
			if (!Class405.findPathStandart(-2, toX, toY, 1, 1, 0, 0, false))
				Class405.findPathStandart(-3, toX, toY, 1, 1, 0, 0, false);
		} else if (!Class405.findPathStandart(-3, toX, toY, 1, 1, 0, 0, false))
			Class405.findPathStandart(-2, toX, toY, 1, 1, 0, 0, false);
	}

	public static void method2150(boolean bool) {
		aClass231_2304 = null;
		npcNodes = null;
		aClass296_Sub39_Sub1_2302 = null;
		if (bool != true)
			method2150(false);
	}

	static final void method2151(boolean bool, int i, int i_3_, int i_4_, boolean bool_5_, int i_6_) {
		if (Class338_Sub2.aClass247ArrayArrayArray5195 == null)
			Class41_Sub13.aHa3774.method1088(i, i_6_, -16777216, 1, i_3_, i_4_);
		else {
			boolean bool_7_ = false;
			if (za_Sub1.anInt6554 == 0) {
				if ((Class296_Sub51_Sub11.localPlayer.tileX) < 0
						|| (Class198.currentMapSizeX * 512 <= (Class296_Sub51_Sub11.localPlayer.tileX))
						|| (Class296_Sub51_Sub11.localPlayer.tileY) < 0
						|| (Class296_Sub38.currentMapSizeY * 512 <= (Class296_Sub51_Sub11.localPlayer.tileY)))
					bool_7_ = true;
			} else if (!StaticMethods.aBoolean1183)
				bool_7_ = true;
			if (bool_7_)
				Class41_Sub13.aHa3774.method1088(i, i_6_, -16777216, 1, i_3_, i_4_);
			else {
				Class296_Sub2.anInt4590++;
				if ((Class296_Sub51_Sub11.localPlayer != null) && ((Class296_Sub51_Sub11.localPlayer.tileX)
						+ (-(Class296_Sub51_Sub11.localPlayer.getSize() * 256) + 256)) >> 9 == Class210_Sub1.foundX
						&& ((Class296_Sub51_Sub11.localPlayer.tileY)
								- (Class296_Sub51_Sub11.localPlayer.getSize() - 1) * 256) >> 9 == Class205.foundY) {
					Class210_Sub1.foundX = -1;
					Class205.foundY = -1;
					EquipmentData.method3304((byte) 76);
				}
				Class16_Sub3.method248(-1);
				if (!bool)
					Animation.method542(true);
				Class95.method866(i_6_, i_4_, 28885, true, i, i_3_);
				i = Class157.anInt1598;
				ModeWhat.anInt1192 = ModeWhat.anInt1193;
				i_4_ = Class296_Sub29.anInt4814;
				i_6_ = Class344.anInt3007;
				i_3_ = NodeDeque.anInt1592;
				if (Class361.anInt3103 != 1) {
					if (Class361.anInt3103 != 4) {
						if (Class361.anInt3103 == 5)
							Class327.method3388(i_4_, 107);
					} else {
						int i_8_ = (int) Class153.aFloat1572;
						if (i_8_ < Class352.anInt3044 >> 8)
							i_8_ = Class352.anInt3044 >> 8;
						if (Class296_Sub51_Sub13.aBooleanArray6414[4] && Class61.anIntArray710[4] + 128 > i_8_)
							i_8_ = Class61.anIntArray710[4] + 128;
						int i_9_ = (int) Class41_Sub26.aFloat3806 & 0x3fff;
						Class9.method194(i_8_, 100, Class296_Sub14.anInt4668, i_4_,
								aa_Sub1.method155(-1537652855, FileWorker.anInt3005, Class338_Sub3_Sub1.anInt6563,
										Class127_Sub1.anInt4287) - 200,
								(i_8_ >> 3) * 3 + GameClient.zoom << 2, Class296_Sub24.anInt4762, i_9_);
					}
				} else {
					int i_10_ = (int) Class153.aFloat1572;
					if (i_10_ < Class352.anInt3044 >> 8)
						i_10_ = Class352.anInt3044 >> 8;
					if (Class296_Sub51_Sub13.aBooleanArray6414[4] && Class61.anIntArray710[4] + 128 > i_10_)
						i_10_ = Class61.anIntArray710[4] + 128;
					int i_11_ = ((int) Class41_Sub26.aFloat3806 + Class134.anInt1387 & 0x3fff);
					Class9.method194(i_10_, 100, Class296_Sub14.anInt4668, i_4_,
							aa_Sub1.method155(-1537652855, FileWorker.anInt3005,
									(Class296_Sub51_Sub11.localPlayer.tileX), (Class296_Sub51_Sub11.localPlayer.tileY))
									- 200,
							(i_10_ >> 3) * 3 + GameClient.zoom << 2, Class296_Sub24.anInt4762, i_11_);
				}
				int i_12_ = Class219.camPosX;
				int i_13_ = TranslatableString.camPosY;
				int i_14_ = Class124.camPosZ;
				int i_15_ = Class296_Sub17_Sub2.camRotX;
				int i_16_ = Class44_Sub1.camRotY;
				for (int i_17_ = 0; i_17_ < 5; i_17_++) {
					if (Class296_Sub51_Sub13.aBooleanArray6414[i_17_]) {
						int i_18_ = (int) (Math.random() * (double) ((Class304.anIntArray2732[i_17_]) * 2 + 1)
								- (double) Class304.anIntArray2732[i_17_]
								+ (Math.sin((double) (Packet.anIntArray4677[i_17_]) / 100.0
										* (double) (Class37.anIntArray365[i_17_]))
										* (double) (Class61.anIntArray710[i_17_])));
						if (i_17_ == 3)
							Class44_Sub1.camRotY = i_18_ + Class44_Sub1.camRotY & 0x3fff;
						if (i_17_ == 1)
							TranslatableString.camPosY += i_18_ << 2;
						if (i_17_ == 4) {
							Class296_Sub17_Sub2.camRotX += i_18_;
							if (Class296_Sub17_Sub2.camRotX >= 1024) {
								if (Class296_Sub17_Sub2.camRotX > 3072)
									Class296_Sub17_Sub2.camRotX = 3072;
							} else
								Class296_Sub17_Sub2.camRotX = 1024;
						}
						if (i_17_ == 0)
							Class219.camPosX += i_18_ << 2;
						if (i_17_ == 2)
							Class124.camPosZ += i_18_ << 2;
					}
				}
				if (Class219.camPosX < 0)
					Class219.camPosX = 0;
				if ((Class228.anInt2201 << 9) - 1 < Class219.camPosX)
					Class219.camPosX = (Class228.anInt2201 << 9) - 1;
				if (Class124.camPosZ < 0)
					Class124.camPosZ = 0;
				if (Class124.camPosZ > (Class368_Sub12.anInt5488 << 9) - 1)
					Class124.camPosZ = (Class368_Sub12.anInt5488 << 9) - 1;
				Class296_Sub44.method2927(-87);
				Class41_Sub13.aHa3774.KA(i, i_3_, i + i_6_, i_3_ + i_4_);
				Class41.method384(bool_5_, !bool_5_);
				int i_19_ = ha_Sub3.anInt4115;
				Class223.aClass373_2162.method3907(Class219.camPosX, TranslatableString.camPosY, Class124.camPosZ,
						-Class296_Sub17_Sub2.camRotX & 0x3fff, -Class44_Sub1.camRotY & 0x3fff,
						-Class268.anInt2488 & 0x3fff);
				Class41_Sub13.aHa3774.a(Class223.aClass373_2162);
				Class41_Sub13.aHa3774.DA(i + i_6_ / 2, i_4_ / 2 + i_3_, Class41_Sub17.anInt3783 << 1,
						Class41_Sub17.anInt3783 << 1);
				if (!Class368_Sub5_Sub2.aBoolean6597) {
					if (s_Sub3.aClass262_5164 == null) {
						Class41_Sub13.aHa3774.GA(i_19_);
						Class41_Sub13.aHa3774.ya();
					} else {
						Class41_Sub13.aHa3774.xa(1.0F);
						Class41_Sub13.aHa3774.ZA(16777215, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F);
						s_Sub3.aClass262_5164.method2254(i_6_, Class217.anInt2120 << 3, false, Class44_Sub1.camRotY,
								i_3_, true, Class41_Sub13.aHa3774, i_19_, i, i_4_, Class268.anInt2488, -111,
								Class296_Sub17_Sub2.camRotX);
					}
				} else {
					Class368_Sub5_Sub2.method3829(-127, ha_Sub3.anInt4115);
					if (ModeWhat.anInt1192 != Class368_Sub5.anInt5453)
						Class41.aBoolean388 = true;
					Class368_Sub5.anInt5453 = ModeWhat.anInt1192;
					Class41_Sub13.aHa3774.GA(i_19_);
					Class41_Sub13.aHa3774.ya();
				}
				Class108.method948(-2060);
				Js5.method1454(126, i_4_ / 2 + i_3_, Class41_Sub17.anInt3783 << 1, Class41_Sub17.anInt3783 << 1,
						i_6_ / 2 + i);
				Class347.method3670(Class219.camPosX, Class124.camPosZ, TranslatableString.camPosY,
						-Class44_Sub1.camRotY & 0x3fff, bool_5_, -Class296_Sub17_Sub2.camRotX & 0x3fff,
						-Class268.anInt2488 & 0x3fff);
				Class296_Sub20.method2655(32);
				byte i_20_ = (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5029.method449(126) != 2 ? (byte) 1
						: (byte) Class296_Sub2.anInt4590);
				if (Class368_Sub5_Sub2.aBoolean6597) {
					Class373_Sub2.method3933(-Class268.anInt2488 & 0x3fff, (-Class296_Sub17_Sub2.camRotX & 0x3fff),
							-Class44_Sub1.camRotY & 0x3fff, 0);
					Class296_Sub17_Sub2.method2638(true,
							Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub9_4999.method430(116) == 0,
							StaticMethods.anIntArray3143, (Class296_Sub51_Sub11.localPlayer.z) + 1,
							TranslatableString.camPosY, Class309.anIntArray2754, 8429,
							(Class296_Sub51_Sub11.localPlayer.tileX) >> 9,
							(Class296_Sub51_Sub11.localPlayer.tileY) >> 9, Class41_Sub23.anIntArray3801,
							ModeWhat.anInt1192, Class404.anIntArray3382, Class29.anInt307, i_20_,
							Class198.aByteArrayArrayArray2003, Class219.camPosX, Class124.camPosZ,
							Class368_Sub9.anIntArray5473);
				} else
					Class140.method1468(Class29.anInt307, Class219.camPosX, TranslatableString.camPosY,
							Class124.camPosZ, Class198.aByteArrayArrayArray2003, Class41_Sub23.anIntArray3801,
							Class368_Sub9.anIntArray5473, Class309.anIntArray2754, StaticMethods.anIntArray3143,
							Class404.anIntArray3382, (Class296_Sub51_Sub11.localPlayer.z) + 1, i_20_,
							(Class296_Sub51_Sub11.localPlayer.tileX) >> 9,
							(Class296_Sub51_Sub11.localPlayer.tileY) >> 9,
							Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub9_4999.method430(117) == 0, true,
							(Class296_Sub39_Sub10.aBoolean6177 ? ModeWhat.anInt1192 : -1), 0, false);
				Class108.method948(-2060);
				if (Class366_Sub6.anInt5392 == 11) {
					Class69_Sub4.method747(256, i, true, i_4_, i_6_, i_3_, 256);
					NodeDeque.method1575(256, i_3_, 256, i_4_, i, (byte) -40, i_6_);
					Class396.method4073(i, i_6_, 256, 256, i_4_, 4966, i_3_);
					Class344.method3655(i_3_, i, 32734, i_6_, i_4_);
				}
				Class368_Sub9.method3841();
				Class44_Sub1.camRotY = i_16_;
				Class296_Sub2.aBoolean4588 = false;
				TranslatableString.camPosY = i_13_;
				Class296_Sub17_Sub2.camRotX = i_15_;
				Class219.camPosX = i_12_;
				Class124.camPosZ = i_14_;
				if (Class164.aBoolean1687 && Class42.aClass285_395.method2375((byte) -109) == 0)
					Class164.aBoolean1687 = false;
				if (Class164.aBoolean1687) {
					Class41_Sub13.aHa3774.method1088(i, i_6_, -16777216, 1, i_3_, i_4_);
					ModeWhat.method1021(Class123_Sub1_Sub1.aClass92_5814, 8493,
							(TranslatableString.aClass120_1208.getTranslation(Class394.langID)), Class41_Sub13.aHa3774,
							false, Class205_Sub1.aClass55_5642);
				}
				Class41.method384(false, false);
			}
		}
	}

	public Class241() {
		/* empty */
	}

	static {
		aClass296_Sub39_Sub1_2302 = null;
		aClass231_2304 = new IncomingPacket(140, 1);
	}
}
