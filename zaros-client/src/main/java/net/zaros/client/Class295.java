package net.zaros.client;
import jaclib.memory.Buffer;

class Class295 {
	private ha_Sub3 aHa_Sub3_2691;
	Buffer aBuffer2692;

	final void method2422(byte[] is, int i) {
		if (aBuffer2692 == null || aBuffer2692.getSize() < i)
			aBuffer2692 = aHa_Sub3_2691.aNativeHeap4138.a(i, false);
		aBuffer2692.a(is, 0, 0, i);
	}

	Class295(ha_Sub3 var_ha_Sub3, byte[] is, int i) {
		aHa_Sub3_2691 = var_ha_Sub3;
		aBuffer2692 = aHa_Sub3_2691.aNativeHeap4138.a(i, false);
		if (is != null)
			aBuffer2692.a(is, 0, 0, i);
	}

	Class295(ha_Sub3 var_ha_Sub3, Buffer buffer) {
		aHa_Sub3_2691 = var_ha_Sub3;
		aBuffer2692 = buffer;
	}
}
