package net.zaros.client;


/* Class42_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class42_Sub2 extends Class42 {
	private int anInt3836;
	private int anInt3837;
	private int anInt3838;
	private int anInt3839;

	final void method529(int i, int i_0_, int i_1_) {
		int i_2_ = anInt3839 * i_0_ >> 12;
		int i_3_ = i_0_ * anInt3838 >> 12;
		int i_4_ = i * anInt3836 >> 12;
		if (i_1_ >= -11)
			anInt3837 = 74;
		int i_5_ = i * anInt3837 >> 12;
		Class199.method1947(anInt397, false, i_3_, i_4_, i_2_, i_5_);
	}

	static final void method535(int i, int i_6_, int i_7_, boolean bool, InterfaceComponent class51) {
		int i_8_ = class51.anInt578;
		int i_9_ = class51.anInt623;
		if (class51.aByte552 != 0) {
			if (class51.aByte552 == 1)
				class51.anInt578 = -class51.width + i;
			else if (class51.aByte552 == 2)
				class51.anInt578 = i * class51.width >> 14;
		} else
			class51.anInt578 = class51.width;
		if (class51.aByte604 == 0)
			class51.anInt623 = class51.height;
		else if (class51.aByte604 == 1)
			class51.anInt623 = i_6_ - class51.height;
		else if (class51.aByte604 == 2)
			class51.anInt623 = i_6_ * class51.height >> 14;
		if (class51.aByte552 == 4)
			class51.anInt578 = class51.anInt623 * class51.anInt566 / class51.anInt530;
		if (class51.aByte604 == 4)
			class51.anInt623 = class51.anInt530 * class51.anInt578 / class51.anInt566;
		if (BillboardRaw.aBoolean1071 && (GameClient.method115(class51).settingsHash != 0 || class51.type == 0)) {
			if (class51.anInt623 >= 5 || class51.anInt578 >= 5) {
				if (class51.anInt623 <= 0)
					class51.anInt623 = 5;
				if (class51.anInt578 <= 0)
					class51.anInt578 = 5;
			} else {
				class51.anInt623 = 5;
				class51.anInt578 = 5;
			}
		}
		if (class51.contentType == Class83.anInt918)
			Class187.aClass51_1923 = class51;
		if (bool && class51.anObjectArray485 != null && (class51.anInt578 != i_8_ || i_9_ != class51.anInt623)) {
			CS2Call class296_sub46 = new CS2Call();
			class296_sub46.callArgs = class51.anObjectArray485;
			class296_sub46.callerInterface = class51;
			Class179.aClass155_1854.addLast((byte) 122, class296_sub46);
		}
		if (i_7_ != -2)
			method535(99, 112, -8, true, null);
	}

	Class42_Sub2(int i, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_) {
		super(-1, i_13_, i_14_);
		anInt3838 = i_11_;
		anInt3839 = i;
		anInt3836 = i_10_;
		anInt3837 = i_12_;
	}

	final void method531(int i, int i_15_, int i_16_) {
		if (i != 68)
			anInt3837 = -5;
	}

	final void method528(boolean bool, int i, int i_17_) {
		if (bool)
			anInt3837 = -61;
	}
}
