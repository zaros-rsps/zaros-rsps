package net.zaros.client;

/* Class100 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class100 {
	static int anInt1067;
	static int anInt1068;
	static Sprite[] aClass397Array1069;
	static boolean aBoolean1070 = false;

	static final void method877(int i, int i_0_) {
		int i_1_ = 13 / ((i + 38) / 56);
		if (i_0_ != -1 && Class219_Sub1.loadedInterfaces[i_0_]) {
			BillboardRaw.interfacesFS.clearChildBuffers(i_0_, false);
			Class192.openedInterfaceComponents[i_0_] = null;
			Class338_Sub10.aClass51ArrayArray5272[i_0_] = null;
			Class219_Sub1.loadedInterfaces[i_0_] = false;
		}
	}

	static final GameLoopTask nextTask() {
		GameLoopTask task = (GameLoopTask) aa_Sub1.aClass145_3719.getFront();
		if (task != null) {
			task.unlink();
			task.queue_unlink();
			return task;
		}
		do {
			task = (GameLoopTask) ObjectDefinitionLoader.gameTaskQueue.getFront();
			if (task == null) {
				return null;
			}
			if (task.method2804() > Class72.method771(-112)) {
				return null;
			}
			task.unlink();
			task.queue_unlink();
		} while ((task.queue_uid & ~0x7fffffffffffffffL) == 0L);
		return task;
	}

	public static void method879(byte i) {
		aClass397Array1069 = null;
		if (i != 88) {
			aClass397Array1069 = null;
		}
	}

	static final void method880(ha var_ha, int i) {
		int i_2_ = -77 / ((i + 77) / 44);
		int i_3_ = 0;
		int i_4_ = 0;
		if (Class368_Sub5_Sub2.aBoolean6597) {
			i_3_ = Class387.method4034(true);
			i_4_ = GraphicsLoader.method2286(true);
		}
		var_ha.KA(i_3_, i_4_, Class241.anInt2301 + i_3_, 350 + i_4_);
		var_ha.aa(i_3_, i_4_, Class241.anInt2301, 350, Class39.anInt385 << 24 | 0x332277, 1);
		Class368_Sub8.method3838(false, i_3_, i_4_, i_4_ + 350, Class241.anInt2301 + i_3_);
		int i_5_ = 350 / Class41_Sub15.anInt3779;
		if (Class357.anInt3082 > 0) {
			int i_6_ = -Class41_Sub15.anInt3779 + 342;
			int i_7_ = i_5_ * i_6_ / (i_5_ + Class357.anInt3082 - 1);
			int i_8_ = 4;
			if (Class357.anInt3082 > 1) {
				i_8_ += (i_6_ - i_7_) * (Class357.anInt3082 - 1 - za_Sub1.anInt6552) / (Class357.anInt3082 - 1);
			}
			var_ha.aa(Class241.anInt2301 - 16 + i_3_, i_4_ + i_8_, 12, i_7_, Class39.anInt385 << 24 | 0x332277, 2);
			for (int i_9_ = za_Sub1.anInt6552; za_Sub1.anInt6552 + i_5_ > i_9_; i_9_++) {
				if (Class357.anInt3082 <= i_9_) {
					break;
				}
				String[] strings = Class41_Sub30.method522((byte) 63, Class296_Sub51_Sub15.aStringArray6425[i_9_], '\010');
				int i_10_ = (Class241.anInt2301 - 24) / strings.length;
				for (int i_11_ = 0; strings.length > i_11_; i_11_++) {
					int i_12_ = 8 + i_11_ * i_10_;
					var_ha.KA(i_3_ + i_12_, i_4_, i_12_ + i_3_ + i_10_ - 8, i_4_ + 350);
					Class205_Sub1.aClass55_5642.a(-16777216, 1659, -1, i_3_ + i_12_, Class254.method2214(strings[i_11_], false), -Class296_Sub35_Sub2.anInt6113 - 2 + i_4_ - (Class123_Sub1_Sub1.aClass92_5814.anInt992 - 350) - Class41_Sub15.anInt3779 * (-za_Sub1.anInt6552 + i_9_));
				}
			}
		}
		Class41_Sub2.aClass55_3742.a(127, i_4_ + 350 - 20, i_3_ + Class241.anInt2301 - 25, -1, -16777216, "Build: 666");
		var_ha.KA(i_3_, i_4_, Class241.anInt2301 + i_3_, i_4_ + 350);
		var_ha.method1089(Class241.anInt2301, -1, i_4_ - Class296_Sub35_Sub2.anInt6113 + 350, i_3_, -108);
		Class49.aClass55_461.a(-16777216, 1659, -1, i_3_ + 10, "--> " + Class254.method2214(Class355_Sub2.aString3503, false), i_4_ + 350 - 1 - Class304.aClass92_2729.anInt992);
		if (Class41.aBoolean390) {
			int i_13_ = -1;
			if (Class29.anInt307 % 30 > 15) {
				i_13_ = 16777215;
			}
			var_ha.method1095(i_4_ - Class304.aClass92_2729.anInt992 + 339, Class304.aClass92_2729.method851(-119, "--> " + Class254.method2214(Class355_Sub2.aString3503, false).substring(0, OutputStream_Sub2.anInt44)) + 10 + i_3_, i_13_, 12, 106);
		}
	}

	static {
		anInt1068 = 0;
	}
}
