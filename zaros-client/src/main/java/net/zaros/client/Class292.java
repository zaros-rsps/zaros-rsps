package net.zaros.client;

/* Class292 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class292 {
	private Js5 aClass138_2657;
	static IncomingPacket aClass231_2658 = new IncomingPacket(121, 4);
	static int anInt2659;
	int anInt2660 = 0;
	static Class269 aClass269_2661 = new Class269(14, 0);
	int anInt2662 = 0;
	private Js5 aClass138_2663;
	private AdvancedMemoryCache aClass113_2664 = new AdvancedMemoryCache(64);
	static int anInt2665;
	private Interface7 anInterface7_2666 = null;
	static Class269 aClass269_2667 = new Class269(15, 4);
	static Class269 aClass269_2668 = new Class269(16, -2);
	static Class269 aClass269_2669 = new Class269(17, 0);
	static Class269 aClass269_2670 = new Class269(19, -2);
	static Class269 aClass269_2671 = new Class269(22, -2);
	static Class269 aClass269_2672 = new Class269(23, 4);
	static Class269 aClass269_2673 = new Class269(24, -1);
	static Class269 aClass269_2674 = new Class269(26, 0);
	static Class269 aClass269_2675 = new Class269(27, 0);
	static Class269 aClass269_2676 = new Class269(28, -2);
	static Class269 aClass269_2677 = new Class269(29, -2);
	static Class269 aClass269_2678 = new Class269(30, -2);
	private static Class269[] aClass269Array2679 = new Class269[32];

	final String method2410(int i, Class161 class161, int[] is, long l) {
		if (anInterface7_2666 != null) {
			String string = anInterface7_2666.method35(is, l, class161, 118);
			if (string != null)
				return string;
		}
		int i_0_ = 75 / ((i + 62) / 42);
		return Long.toString(l);
	}

	static final void method2411(int i) {
		if (i != -32048)
			method2412(false);
		if (Class274.aClass381_2524 != null)
			Class274.aClass381_2524.method4002((byte) 22);
		if (Class325.aClass381_2869 != null)
			Class325.aClass381_2869.method4002((byte) 22);
	}

	public static void method2412(boolean bool) {
		aClass269_2669 = null;
		aClass231_2658 = null;
		if (bool)
			method2411(-48);
		aClass269_2676 = null;
		aClass269_2674 = null;
		aClass269_2670 = null;
		aClass269_2675 = null;
		aClass269_2673 = null;
		aClass269Array2679 = null;
		aClass269_2678 = null;
		aClass269_2668 = null;
		aClass269_2667 = null;
		aClass269_2672 = null;
		aClass269_2677 = null;
		aClass269_2671 = null;
		aClass269_2661 = null;
	}

	final Class296_Sub39_Sub10 method2413(int i, int i_1_) {
		Class296_Sub39_Sub10 class296_sub39_sub10 = (Class296_Sub39_Sub10) aClass113_2664.get((long) i);
		if (class296_sub39_sub10 != null)
			return class296_sub39_sub10;
		byte[] is;
		if (i >= 32768)
			is = aClass138_2657.getFile(1, i & 0x7fff);
		else
			is = aClass138_2663.getFile(1, i);
		if (i_1_ >= -124)
			aClass269_2669 = null;
		class296_sub39_sub10 = new Class296_Sub39_Sub10();
		class296_sub39_sub10.aClass292_6181 = this;
		if (is != null)
			class296_sub39_sub10.method2838(new Packet(is), 80);
		if (i >= 32768)
			class296_sub39_sub10.method2836((byte) -117);
		aClass113_2664.put(class296_sub39_sub10, (long) i);
		return class296_sub39_sub10;
	}

	Class292(int i, Js5 class138, Js5 class138_2_, Interface7 interface7) {
		aClass138_2663 = class138;
		aClass138_2657 = class138_2_;
		anInterface7_2666 = interface7;
		if (aClass138_2663 != null)
			anInt2662 = aClass138_2663.getLastFileId(1);
		if (aClass138_2657 != null)
			anInt2660 = aClass138_2657.getLastFileId(1);
	}

	static {
		Class269[] class269s = Class41_Sub23.method491(103);
		for (int i = 0; class269s.length > i; i++)
			aClass269Array2679[class269s[i].protocolId] = class269s[i];
	}
}
