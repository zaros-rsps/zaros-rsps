package net.zaros.client;
import jaclib.memory.Stream;

final class Class296_Sub17_Sub2 extends Packet {
	static int camRotX;
	static Class296_Sub39_Sub9 aClass296_Sub39_Sub9_6046;
	static boolean[] aBooleanArray6047;
	static ModeWhat modeWhat;

	static final void method2634(int i) {
		if (Class116.aBoolean3679 && i <= -111) {
			while (Class328.anInt2908 < AnimationsLoader.aClass210_Sub1Array2760.length) {
				Class210_Sub1 class210_sub1 = AnimationsLoader.aClass210_Sub1Array2760[Class328.anInt2908];
				if (class210_sub1 == null || class210_sub1.anInt4538 != -1)
					Class328.anInt2908++;
				else {
					if (Class12.aClass296_Sub48_136 == null)
						Class12.aClass296_Sub48_136 = (SeekableFile.aClass175_2399.method1705((byte) -91, class210_sub1.aString4543));
					int i_0_ = Class12.aClass296_Sub48_136.anInt4976;
					if (i_0_ == -1)
						break;
					class210_sub1.anInt4538 = i_0_;
					Class12.aClass296_Sub48_136 = null;
					Class328.anInt2908++;
				}
			}
		}
	}

	static final void method2635(int i, boolean bool, int i_1_, boolean bool_2_, int i_3_) {
		Class13.method224(i_3_, bool, bool_2_, -15, AnimationsLoader.aClass210_Sub1Array2760.length - 1, 0, i_1_);
		if (i != 13647)
			aClass296_Sub39_Sub9_6046 = null;
		Class328.anInt2908 = 0;
		Class12.aClass296_Sub48_136 = null;
	}

	final void method2636(byte i, float f) {
		if (i < 50)
			aBooleanArray6047 = null;
		int i_4_ = Stream.floatToRawIntBits(f);
		data[pos++] = (byte) (i_4_ >> 24);
		data[pos++] = (byte) (i_4_ >> 16);
		data[pos++] = (byte) (i_4_ >> 8);
		data[pos++] = (byte) i_4_;
	}

	final void method2637(byte i, float f) {
		if (i >= -77)
			method2636((byte) -102, -0.6838187F);
		int i_5_ = Stream.floatToRawIntBits(f);
		data[pos++] = (byte) i_5_;
		data[pos++] = (byte) (i_5_ >> 8);
		data[pos++] = (byte) (i_5_ >> 16);
		data[pos++] = (byte) (i_5_ >> 24);
	}

	static final void method2638(boolean bool, boolean bool_6_, int[] is, int i, int i_7_, int[] is_8_, int i_9_, int i_10_, int i_11_, int[] is_12_, int i_13_, int[] is_14_, int i_15_, byte i_16_, byte[][][] is_17_, int i_18_, int i_19_, int[] is_20_) {
		if (ConfigsRegister.anInt3674 != -1) {
			int[] is_21_ = Class296_Sub51_Sub36.aHa6529.Y();
			int i_22_ = is_21_[0];
			int i_23_ = is_21_[1];
			int i_24_ = is_21_[2];
			int i_25_ = is_21_[3];
			int i_26_ = i_24_;
			int i_27_ = i_25_;
			if (ConfigsRegister.anInt3674 == 1) {
				i_26_ = (int) ((double) i_24_ * (double) Class261.anInt2424 / (double) Class359.anInt3089);
				i_27_ = (int) ((double) Class261.anInt2424 * (double) i_25_ / (double) Class359.anInt3089);
			}
			if (!Class41.aBoolean388) {
				if (ConfigsRegister.anInt3674 == 1)
					Class127_Sub1.method1359(i_9_ ^ 0x20ed);
				int i_28_ = -Class105.anInt3663 + i_18_;
				int i_29_ = -Class296_Sub51_Sub37.anInt6533 + i_7_;
				int i_30_ = -StaticMethods.anInt6084 + i_19_;
				int i_31_ = (int) ((Class240.aDouble2258 * (double) i_30_ + (Class251.aDouble2362 * (double) i_28_ + Class238.aDouble3630 * (double) i_29_)) * (double) i_26_ / (double) i_13_);
				int i_32_ = (int) ((double) i_27_ * (Class131.aDouble1343 * (double) i_29_ + Class119.aDouble1195 * (double) i_28_ + Class246.aDouble2331 * (double) i_30_) / (double) i_13_);
				double d = (Class296_Sub51_Sub18.aDouble6434 * (double) i_30_ + (Class69_Sub3.aDouble5726 * (double) i_29_ + (double) i_28_ * CS2Stack.aDouble2250));
				int i_33_ = -Class16.anInt182 + (Class347.anInt3026 + i_31_);
				int i_34_ = -Class366.anInt3122 + i_32_ + Class41_Sub2.anInt3741;
				int i_35_ = Class235.anInt2227 + i_33_;
				int i_36_ = i_34_ + Class261.anInt2424;
				if ((i_33_ >= 0 && i_34_ >= 0 && Class296_Sub39_Sub12.anInt6197 >= i_35_ && Class359.anInt3089 >= i_36_) || ConfigsRegister.anInt3674 == 2) {
					Class34.anInt337 = i_33_;
					if (ConfigsRegister.anInt3674 == 2)
						Class379_Sub1.aDouble5674 = -d;
					Class391.anInt3298 = i_34_;
				} else if (i_35_ > 0 && i_36_ > 0 && i_33_ < Class296_Sub39_Sub12.anInt6197 && i_34_ < Class359.anInt3089) {
					int i_37_ = -Class347.anInt3026 + i_33_;
					int i_38_ = -Class41_Sub2.anInt3741 + i_34_;
					int i_39_ = 0;
					int i_40_ = 0;
					int i_41_ = 0;
					int i_42_ = 0;
					double d_43_ = 0.0;
					if (ConfigsRegister.anInt3674 != 0) {
						if (ConfigsRegister.anInt3674 == 1) {
							i_41_ = i_37_ / Class290.anInt2656;
							i_42_ = i_38_ / Class395.anInt3317;
							i_40_ = i_42_ * Class395.anInt3317;
							i_39_ = Class290.anInt2656 * i_41_;
							d_43_ = ((d + Class379_Sub1.aDouble5674) * (double) (i_40_ * i_38_ + i_37_ * i_39_) / (double) (i_38_ * i_38_ + i_37_ * i_37_));
						}
					} else {
						d_43_ = d + Class379_Sub1.aDouble5674;
						i_39_ = i_37_;
						i_40_ = i_38_;
					}
					d_43_ = -d_43_;
					int i_44_ = 0;
					int i_45_ = 0;
					int i_46_ = 0;
					int i_47_ = 0;
					int i_48_ = 0;
					int i_49_;
					int i_50_;
					int i_51_;
					int i_52_;
					if (i_39_ >= 0) {
						i_51_ = -i_39_ + Class296_Sub39_Sub12.anInt6197;
						i_50_ = 0;
						if (ConfigsRegister.anInt3674 == 1) {
							i_48_ = i_41_;
							i_46_ = Class317.anInt3705 - i_41_;
						}
						i_52_ = i_39_;
						i_49_ = i_51_;
					} else {
						i_49_ = 0;
						i_50_ = -i_39_;
						i_51_ = i_39_ + Class296_Sub39_Sub12.anInt6197;
						if (ConfigsRegister.anInt3674 == 1) {
							i_48_ = -i_41_;
							i_46_ = 0;
						}
						i_52_ = i_50_;
					}
					int i_53_ = 0;
					int i_54_;
					int i_55_;
					int i_56_;
					int i_57_;
					int i_58_;
					int i_59_;
					if (i_40_ >= 0) {
						i_55_ = -i_40_ + Class359.anInt3089;
						i_56_ = 0;
						i_58_ = 0;
						i_59_ = i_55_;
						i_57_ = i_40_;
						i_54_ = i_55_;
						if (ConfigsRegister.anInt3674 == 1) {
							i_45_ = i_42_;
							i_47_ = 0;
							i_44_ = -i_42_ + Applet_Sub1.anInt7;
							i_53_ = i_44_;
						}
					} else {
						i_54_ = 0;
						i_55_ = Class359.anInt3089 + i_40_;
						i_56_ = -i_40_;
						i_57_ = i_56_;
						if (ConfigsRegister.anInt3674 == 1) {
							i_45_ = -i_42_;
							i_44_ = 0;
							i_53_ = Applet_Sub1.anInt7 + i_42_;
							i_47_ = i_45_;
						}
						i_58_ = i_57_;
						i_59_ = i_55_;
					}
					Class404 class404 = Class379_Sub3.aClass264_5687.aClass404_2468;
					for (Class338_Sub2 class338_sub2 = (Class338_Sub2) class404.method4160((byte) -75); class338_sub2 != null; class338_sub2 = (Class338_Sub2) class404.method4163(-24917)) {
						Class338_Sub5[] class338_sub5s = class338_sub2.aClass338_Sub5Array5194;
						boolean bool_60_ = true;
						for (int i_61_ = 0; i_61_ < class338_sub5s.length; i_61_++) {
							Class338_Sub5 class338_sub5 = class338_sub5s[i_61_];
							int i_62_ = class338_sub5.anInt5225;
							int i_63_ = class338_sub5.anInt5223;
							int i_64_ = class338_sub5.anInt5222;
							int i_65_ = class338_sub5.anInt5221;
							class338_sub5.anInt5222 = i_64_ -= i_39_;
							class338_sub5.anInt5223 = i_63_ -= i_40_;
							int i_66_ = class338_sub5.anInt5226;
							class338_sub5.anInt5225 = i_62_ = -i_39_ + i_62_;
							class338_sub5.anInt5221 = i_65_ = -i_40_ + i_65_;
							if (bool_60_) {
								int i_67_ = -i_66_ + (i_62_ >= i_64_ ? i_64_ : i_62_);
								if (Class296_Sub39_Sub12.anInt6197 >= i_67_) {
									int i_68_ = -i_66_ + (i_65_ > i_63_ ? i_63_ : i_65_);
									if (i_68_ <= Class359.anInt3089) {
										int i_69_ = i_66_ + (i_62_ < i_64_ ? i_64_ : i_62_);
										if (i_69_ >= 0) {
											int i_70_ = i_66_ + (i_65_ <= i_63_ ? i_63_ : i_65_);
											if (i_70_ >= 0)
												bool_60_ = false;
										}
									}
								}
							}
						}
						if (bool_60_) {
							class338_sub2.method3438(false);
							SubCache.method3258(class338_sub2, 5362);
						}
					}
					if (ConfigsRegister.anInt3674 == 0)
						Class296_Sub51_Sub36.aHa6529.a(Class241_Sub2_Sub1.anInterface13_5901);
					Class296_Sub51_Sub36.aHa6529.F(-i_39_, -i_40_);
					Class296_Sub51_Sub36.aHa6529.b(i_50_, i_56_, i_51_, i_55_, d_43_);
					EffectiveVertex.method2114(Class379_Sub1.aDouble5674 + d_43_, (byte) 2);
					Class296_Sub51_Sub10.aDouble6395 = d_43_ + Class379_Sub1.aDouble5674;
					if (ConfigsRegister.anInt3674 != 1) {
						Class368_Sub14.anInt5509 = (-i_39_ + (i_22_ + Class347.anInt3026) - Class16.anInt182);
						Class219_Sub1.anInt4564 = i_27_;
						Class27.anInt297 = i_26_;
						StaticMethods.anInt287 = -i_40_ + (i_23_ + Class41_Sub2.anInt3741 - Class366.anInt3122);
						Class296_Sub51_Sub36.aHa6529.DA(Class368_Sub14.anInt5509, StaticMethods.anInt287, Class27.anInt297, Class219_Sub1.anInt4564);
					} else {
						Class27.anInt297 = i_26_;
						Class368_Sub14.anInt5509 = -i_39_ + i_22_ - Class16.anInt182;
						StaticMethods.anInt287 = i_23_ - (Class366.anInt3122 + i_40_);
						Class219_Sub1.anInt4564 = i_27_;
						Class296_Sub51_Sub36.aHa6529.DA(Class368_Sub14.anInt5509, StaticMethods.anInt287, Class27.anInt297, Class219_Sub1.anInt4564);
					}
					Class41_Sub27.method504(Class379_Sub3.aClass264_5687);
					if (i_57_ > 0) {
						Class296_Sub51_Sub36.aHa6529.KA(0, i_54_, (Class296_Sub39_Sub12.anInt6197), i_57_ + i_54_);
						Class296_Sub51_Sub36.aHa6529.ya();
						Class296_Sub51_Sub36.aHa6529.GA(Class368_Sub3.anInt5437);
						Class140.method1468(i_15_, i_18_, i_7_, i_19_, is_17_, is_12_, is_20_, is_8_, is, is_14_, i, i_16_, i_10_, i_11_, bool_6_, bool, i_13_, 1, false);
					}
					if (i_52_ > 0) {
						Class296_Sub51_Sub36.aHa6529.KA(i_49_, i_58_, i_49_ + i_52_, i_59_ + i_58_);
						Class296_Sub51_Sub36.aHa6529.ya();
						Class296_Sub51_Sub36.aHa6529.GA(Class368_Sub3.anInt5437);
						Class140.method1468(i_15_, i_18_, i_7_, i_19_, is_17_, is_12_, is_20_, is_8_, is, is_14_, i, i_16_, i_10_, i_11_, bool_6_, bool, i_13_, 1, false);
					}
					Class296_Sub51_Sub36.aHa6529.la();
					SubCache.method3256();
					if (ConfigsRegister.anInt3674 == 0)
						Class296_Sub51_Sub36.aHa6529.y();
					Class16.anInt182 += i_39_;
					Class366.anInt3122 += i_40_;
					Class379_Sub1.aDouble5674 += d_43_;
					Class34.anInt337 = Class347.anInt3026 - (-i_31_ + Class16.anInt182);
					Class391.anInt3298 = -Class366.anInt3122 + (Class41_Sub2.anInt3741 + i_32_);
					if (ConfigsRegister.anInt3674 == 1) {
						Class292.anInt2665 += i_41_;
						Class404.anInt3385 += i_42_;
						for (int i_71_ = 0; i_71_ < Applet_Sub1.anInt7; i_71_++) {
							int i_72_ = ((Class296_Sub28.method2683((byte) 108, Class404.anInt3385 + i_71_, Applet_Sub1.anInt7)) * Class317.anInt3705);
							for (int i_73_ = 0; Class317.anInt3705 > i_73_; i_73_++) {
								int i_74_ = ((Class296_Sub28.method2683((byte) 90, Class292.anInt2665 + i_73_, Class317.anInt3705)) + i_72_);
								boolean bool_75_ = (i_44_ <= i_71_ && i_71_ < i_44_ + i_45_ || (i_71_ >= i_47_ && i_71_ < i_47_ + i_53_ && i_46_ <= i_73_ && i_48_ + i_46_ > i_73_));
								IOException_Sub1.anInterface13Array38[i_74_].method53(Class290.anInt2656 * i_73_, i_71_ * Class395.anInt3317, Class290.anInt2656, Class395.anInt3317, 0, 0, bool_75_, true);
							}
						}
					}
				} else
					Class41.aBoolean388 = true;
			}
			if (Class41.aBoolean388) {
				Class34.anInt337 = Class347.anInt3026;
				Class391.anInt3298 = Class41_Sub2.anInt3741;
				StaticMethods.anInt6084 = i_19_;
				Class379_Sub1.aDouble5674 = 0.0;
				Class105.anInt3663 = i_18_;
				Class16.anInt182 = 0;
				Class296_Sub51_Sub37.anInt6533 = i_7_;
				Class366.anInt3122 = 0;
				if (ConfigsRegister.anInt3674 == 0)
					Class296_Sub51_Sub36.aHa6529.a(Class241_Sub2_Sub1.anInterface13_5901);
				Class296_Sub51_Sub36.aHa6529.la();
				Class296_Sub51_Sub36.aHa6529.ya();
				Class296_Sub51_Sub36.aHa6529.GA(Class368_Sub3.anInt5437);
				Class373_Sub3.aClass373_5612.method3907(Class105.anInt3663, Class296_Sub51_Sub37.anInt6533, StaticMethods.anInt6084, EmissiveTriangle.anInt958, Class252.anInt2386, Class368_Sub23.anInt5569);
				Class296_Sub51_Sub36.aHa6529.a(Class373_Sub3.aClass373_5612);
				if (ConfigsRegister.anInt3674 == 1) {
					Class27.anInt297 = i_26_;
					StaticMethods.anInt287 = i_23_;
					Class368_Sub14.anInt5509 = i_22_;
					Class219_Sub1.anInt4564 = i_27_;
					Class296_Sub51_Sub36.aHa6529.DA(Class368_Sub14.anInt5509, StaticMethods.anInt287, Class27.anInt297, Class219_Sub1.anInt4564);
				} else {
					Class219_Sub1.anInt4564 = i_27_;
					StaticMethods.anInt287 = i_23_ + Class41_Sub2.anInt3741;
					Class368_Sub14.anInt5509 = i_22_ + Class347.anInt3026;
					Class27.anInt297 = i_26_;
					Class296_Sub51_Sub36.aHa6529.DA(Class368_Sub14.anInt5509, StaticMethods.anInt287, Class27.anInt297, Class219_Sub1.anInt4564);
				}
				Class296_Sub51_Sub10.aDouble6395 = 0.0;
				Class379_Sub3.aClass264_5687.method2277((byte) 122);
				Class41_Sub27.method504(Class379_Sub3.aClass264_5687);
				Class140.method1468(i_15_, i_18_, i_7_, i_19_, is_17_, is_12_, is_20_, is_8_, is, is_14_, i, i_16_, i_10_, i_11_, bool_6_, bool, i_13_, 1, false);
				SubCache.method3256();
				Class41.aBoolean388 = false;
				if (ConfigsRegister.anInt3674 == 0)
					Class296_Sub51_Sub36.aHa6529.y();
				if (ConfigsRegister.anInt3674 == 1)
					Class16_Sub1.method236(85);
			}
			if (ConfigsRegister.anInt3674 == 0)
				Class241_Sub2_Sub1.anInterface13_5901.method52(Class34.anInt337, Class391.anInt3298, Class235.anInt2227, Class261.anInt2424, 0, 0, true, true);
			Class41_Sub20.anInt3795++;
			EffectiveVertex.method2114(Class379_Sub1.aDouble5674, (byte) 2);
			Class217.aDouble2121 = Class379_Sub1.aDouble5674;
			if (ConfigsRegister.anInt3674 != 0 && ConfigsRegister.anInt3674 != 2) {
				if (ConfigsRegister.anInt3674 == 1) {
					Class361.anInt3099 = i_23_ - Class366.anInt3122;
					Class63.anInt727 = i_26_;
					StaticMethods.anInt1843 = i_22_ - Class16.anInt182;
					Class198.anInt2001 = i_27_;
					Class296_Sub51_Sub36.aHa6529.DA(StaticMethods.anInt1843, Class361.anInt3099, Class63.anInt727, Class198.anInt2001);
					Class296_Sub51_Sub36.aHa6529.KA(Class34.anInt337, Class391.anInt3298, (Class235.anInt2227 + Class34.anInt337), (Class391.anInt3298 + Class261.anInt2424));
				}
			} else {
				if (ConfigsRegister.anInt3674 == 2) {
					Class296_Sub51_Sub36.aHa6529.GA(Class368_Sub3.anInt5437);
					Class296_Sub51_Sub36.aHa6529.ya();
				}
				Class63.anInt727 = i_26_;
				Class361.anInt3099 = (-Class391.anInt3298 + (Class41_Sub2.anInt3741 + i_23_) - Class366.anInt3122);
				Class198.anInt2001 = i_27_;
				StaticMethods.anInt1843 = -Class34.anInt337 + i_22_ - (-Class347.anInt3026 + Class16.anInt182);
				Class296_Sub51_Sub36.aHa6529.DA(StaticMethods.anInt1843, Class361.anInt3099, Class63.anInt727, Class198.anInt2001);
			}
			Class140.method1468(i_15_, i_18_, i_7_, i_19_, is_17_, is_12_, is_20_, is_8_, is, is_14_, i, i_16_, i_10_, i_11_, bool_6_, bool, i_13_, ConfigsRegister.anInt3674 != 2 ? 2 : 0, ConfigsRegister.anInt3674 == 1);
			if (i_9_ != 8429)
				method2635(-18, true, 0, false, -3);
			Class296_Sub51_Sub36.aHa6529.la();
			Class296_Sub51_Sub36.aHa6529.DA(i_22_, i_23_, i_24_, i_25_);
		}
	}

	public static void method2639(int i) {
		if (i != -9688)
			aClass296_Sub39_Sub9_6046 = null;
		modeWhat = null;
		aBooleanArray6047 = null;
		aClass296_Sub39_Sub9_6046 = null;
	}

	Class296_Sub17_Sub2(int i) {
		super(i);
	}
}
