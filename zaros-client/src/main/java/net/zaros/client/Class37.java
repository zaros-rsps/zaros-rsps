package net.zaros.client;

/* Class37 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class37 {
	short aShort357;
	short aShort358;
	int anInt359;
	boolean aBoolean360;
	byte aByte361;
	short aShort362;
	int anInt363;
	int anInt364;
	static int[] anIntArray365 = new int[5];
	int anInt366;
	int anInt367;

	public static void method362(byte i) {
		anIntArray365 = null;
		if (i != 75)
			method364(null, 102, false, 3, (byte) 117, 103);
	}

	static final void method363(int i, int i_0_, int i_1_, byte i_2_, int i_3_, int i_4_) {
		int i_5_ = ParticleEmitterRaw.method1668(i_3_, RuntimeException_Sub1.anInt3391, EmissiveTriangle.anInt952, (byte) 12);
		int i_6_ = ParticleEmitterRaw.method1668(i, RuntimeException_Sub1.anInt3391, EmissiveTriangle.anInt952, (byte) 122);
		int i_7_ = ParticleEmitterRaw.method1668(i_0_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -35);
		int i_8_ = ParticleEmitterRaw.method1668(i_4_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -83);
		int i_9_ = i_5_;
		int i_10_ = -102 % ((-58 - i_2_) / 53);
		for (/**/; i_9_ <= i_6_; i_9_++)
			Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_9_]), i_8_, (byte) -46, i_1_, i_7_);
	}

	static final void method364(InterfaceComponent[] class51s, int i, boolean bool, int i_11_, byte i_12_, int i_13_) {
		if (i_12_ >= 108) {
			for (int i_14_ = 0; i_14_ < class51s.length; i_14_++) {
				InterfaceComponent class51 = class51s[i_14_];
				if (class51 != null && class51.parentId == i_11_) {
					Class42_Sub2.method535(i, i_13_, -2, bool, class51);
					Class8.method193(false, class51, i_13_, i);
					if (class51.anInt624 > -class51.anInt578 + class51.scrollMaxH)
						class51.anInt624 = class51.scrollMaxH - class51.anInt578;
					if (-class51.anInt623 + class51.scrollMaxW < class51.anInt632)
						class51.anInt632 = -class51.anInt623 + class51.scrollMaxW;
					if (class51.anInt624 < 0)
						class51.anInt624 = 0;
					if (class51.anInt632 < 0)
						class51.anInt632 = 0;
					if (class51.type == 0)
						Class61.method695(bool, -55, class51);
				}
			}
		}
	}

	Class37(int i, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_, int i_22_, boolean bool, boolean bool_23_, int i_24_) {
		aShort362 = (short) i_18_;
		aByte361 = (byte) i_22_;
		anInt359 = i_17_;
		anInt364 = i_24_;
		aShort358 = (short) i_19_;
		aShort357 = (short) i_20_;
		anInt363 = i_16_;
		anInt366 = i_15_;
		anInt367 = i;
		aBoolean360 = bool_23_;
	}
}
