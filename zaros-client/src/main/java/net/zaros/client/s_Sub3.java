package net.zaros.client;

/* s_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jaclib.memory.Buffer;
import jaclib.memory.Stream;

import java.util.Random;

public class s_Sub3 extends s {
	static IncomingPacket aClass231_5142 = new IncomingPacket(79, 1);
	private int anInt5143;
	private NodeDeque aClass155_5144;
	int[][][] anIntArrayArrayArray5145;
	private float aFloat5146 = -3.4028235E38F;
	private int anInt5147;
	private int anInt5148;
	private int[][][] anIntArrayArrayArray5149;
	int anInt5150;
	ha_Sub1 aHa_Sub1_5151;
	short[][] aShortArrayArray5152;
	private Class296_Sub11[][][] aClass296_Sub11ArrayArrayArray5153;
	int[][][] anIntArrayArrayArray5154;
	int[][][] anIntArrayArrayArray5155;
	private int anInt5156;
	private float aFloat5157 = 3.4028235E38F;
	private int[][][] anIntArrayArrayArray5158;
	private int[][][] anIntArrayArrayArray5159;
	private byte[][] aByteArrayArray5160;
	private Class386 aClass386_5161;

	static String[][] aStringArrayArray5163 = { { "M1", "M2", "S1", "F" }, { "M1", "M2", "M3", "S1", "S2", "F" },
			{ "M1", "M2", "M3", "M4", "S1", "S2", "S3", "F" } };
	static Class262 aClass262_5164;
	private int anInt5165;
	private HashTable aClass263_5166;
	private Class296_Sub11[] aClass296_Sub11Array5167;
	private byte[][] aByteArrayArray5168;
	private Interface15_Impl2 anInterface15_Impl2_5169;
	private Interface15_Impl2 anInterface15_Impl2_5170;
	private float[][] aFloatArrayArray5171;
	private int anInt5172;
	Class127 aClass127_5173;
	private float[][] aFloatArrayArray5174;
	private float[][] aFloatArrayArray5175;

	public void wa(r var_r, int i, int i_0_, int i_1_, int i_2_, boolean bool) {
		if (aClass386_5161 != null && var_r != null) {
			int i_3_ = (i - (i_0_ * aHa_Sub1_5151.anInt3969 >> 8) >> aHa_Sub1_5151.anInt4036);
			int i_4_ = (i_1_ - (i_0_ * aHa_Sub1_5151.anInt4027 >> 8) >> aHa_Sub1_5151.anInt4036);
			aClass386_5161.method4033(i_4_, (byte) -75, i_3_, var_r);
		}
	}

	static public boolean method3372(int i, int i_5_, int i_6_) {
		if (i_5_ != -1)
			method3372(120, 83, -52);
		if ((i_6_ & 0x20) == 0)
			return false;
		return true;
	}

	static public int method3373(int i, Random random, int i_7_) {
		if (i_7_ != 6445)
			return 37;
		if (i <= 0)
			throw new IllegalArgumentException();
		if (Class30.method329(2844, i))
			return (int) ((long) i * ((long) random.nextInt() & 0xffffffffL) >> 32);
		int i_8_ = -2147483648 - (int) (4294967296L % (long) i);
		int i_9_;
		do
			i_9_ = random.nextInt();
		while (i_9_ >= i_8_);
		return Class296_Sub28.method2683((byte) 109, i_9_, i);
	}

	public void CA(r var_r, int i, int i_10_, int i_11_, int i_12_, boolean bool) {
		if (aClass386_5161 != null && var_r != null) {
			int i_13_ = (i - (aHa_Sub1_5151.anInt3969 * i_10_ >> 8) >> aHa_Sub1_5151.anInt4036);
			int i_14_ = (-(i_10_ * aHa_Sub1_5151.anInt4027 >> 8) + i_11_ >> aHa_Sub1_5151.anInt4036);
			aClass386_5161.method4030(i_13_, (byte) -1, i_14_, var_r);
		}
	}

	private void method3374(int i, int i_15_, int i_16_, int i_17_, int i_18_, boolean[][] bools, int i_19_,
			boolean bool) {
		if (i_18_ < -34) {
			if (aClass296_Sub11Array5167 != null) {
				int i_20_ = i_15_ + i_15_ + 1;
				i_20_ *= i_20_;
				if (i_20_ > Class351.anIntArray3042.length)
					Class351.anIntArray3042 = new int[i_20_];
				int i_21_ = -i_15_ + i_17_;
				int i_22_ = i_21_;
				if (i_21_ < 0)
					i_21_ = 0;
				int i_23_ = -i_15_ + i_16_;
				int i_24_ = i_23_;
				if (i_23_ < 0)
					i_23_ = 0;
				int i_25_ = i_15_ + i_17_;
				if (i_25_ > anInt2832 - 1)
					i_25_ = anInt2832 - 1;
				int i_26_ = i_15_ + i_16_;
				aa_Sub2.anInt3725 = 0;
				if (i_26_ > anInt2834 - 1)
					i_26_ = anInt2834 - 1;
				for (int i_27_ = i_21_; i_25_ >= i_27_; i_27_++) {
					boolean[] bools_28_ = bools[i_27_ - i_22_];
					for (int i_29_ = i_23_; i_29_ <= i_26_; i_29_++) {
						if (bools_28_[i_29_ - i_24_])
							Class351.anIntArray3042[aa_Sub2.anInt3725++] = anInt2832 * i_29_ + i_27_;
					}
				}
				if (i == -1)
					aHa_Sub1_5151.method1130(8);
				else {
					aHa_Sub1_5151.method1117(-117, (float) i);
					aHa_Sub1_5151.method1124(16);
				}
				aHa_Sub1_5151.method1102(-128);
				aHa_Sub1_5151.method1137(4, (anInt5150 & 0x7) != 0);
				aHa_Sub1_5151.method1133(false, -1, 0, false);
				aHa_Sub1_5151.method1178(anInterface15_Impl2_5169, (byte) -117, 0);
				for (int i_30_ = 0; i_30_ < aClass296_Sub11Array5167.length; i_30_++)
					aClass296_Sub11Array5167[i_30_].method2501(Class351.anIntArray3042, 1, aa_Sub2.anInt3725);
				Class373_Sub2 class373_sub2 = aHa_Sub1_5151.method1215(65);
				class373_sub2.method3902(0, -1, 0);
				aHa_Sub1_5151.method1202(0);
				if (!aClass155_5144.method1577((byte) -77)) {
					int i_31_ = aHa_Sub1_5151.anInt3947;
					int i_32_ = aHa_Sub1_5151.anInt3990;
					aHa_Sub1_5151.L(0, i_32_, aHa_Sub1_5151.anInt3960);
					aHa_Sub1_5151.method1137(4, false);
					aHa_Sub1_5151.method1106(-94, false);
					aHa_Sub1_5151.method1161(31156, 128);
					aHa_Sub1_5151.method1133(false, -2, 0, false);
					aHa_Sub1_5151.method1140(aHa_Sub1_5151.anInterface6_4037, false);
					aHa_Sub1_5151.method1177(Js5.aClass125_1411, 9815, Class41_Sub4.aClass125_3745);
					aHa_Sub1_5151.method1158((byte) -100, 0, Class151.aClass287_1553);
					aHa_Sub1_5151.method1219((byte) 54, Class347.aClass287_3025, 0);
					for (Node class296 = aClass155_5144
							.removeFirst((byte) 113); class296 != null; class296 = aClass155_5144.removeNext(1001)) {
						Class296_Sub22 class296_sub22 = (Class296_Sub22) class296;
						class296_sub22.method2664(i_16_, i_17_, bools, i_15_, 122);
					}
					aHa_Sub1_5151.method1158((byte) -114, 0, Class199.aClass287_2007);
					aHa_Sub1_5151.method1219((byte) 54, Class199.aClass287_2007, 0);
					aHa_Sub1_5151.method1140(null, false);
					aHa_Sub1_5151.L(i_31_, i_32_, aHa_Sub1_5151.anInt3960);
				}
				if (aClass386_5161 != null) {
					aHa_Sub1_5151.method1178(anInterface15_Impl2_5169, (byte) -119, 0);
					aHa_Sub1_5151.method1178(anInterface15_Impl2_5170, (byte) -128, 1);
					aHa_Sub1_5151.method1209(aClass127_5173, (byte) -125);
					aClass386_5161.method4031(i_16_, i_17_, i_15_, 92, bool, bools);
				}
			}
		}
	}

	public void method3352(int i, int i_33_) {
		/* empty */
	}

	public void method3348(int i, int i_34_, int i_35_, boolean[][] bools, boolean bool, int i_36_, int i_37_) {
		method3374(i_36_, i_35_, i_34_, i, -125, bools, i_37_, bool);
	}

	public void U(int i, int i_38_, int[] is, int[] is_39_, int[] is_40_, int[] is_41_, int[] is_42_, int[] is_43_,
			int[] is_44_, int[] is_45_, int i_46_, int i_47_, int i_48_, boolean bool) {
		d var_d = aHa_Sub1_5151.aD1299;
		if (is_41_ != null && anIntArrayArrayArray5149 == null)
			anIntArrayArrayArray5149 = new int[anInt2832][anInt2834][];
		if (is_39_ != null && anIntArrayArrayArray5158 == null)
			anIntArrayArrayArray5158 = new int[anInt2832][anInt2834][];
		anIntArrayArrayArray5145[i][i_38_] = is;
		anIntArrayArrayArray5154[i][i_38_] = is_40_;
		anIntArrayArrayArray5155[i][i_38_] = is_42_;
		anIntArrayArrayArray5159[i][i_38_] = is_43_;
		if (anIntArrayArrayArray5149 != null)
			anIntArrayArrayArray5149[i][i_38_] = is_41_;
		if (anIntArrayArrayArray5158 != null)
			anIntArrayArrayArray5158[i][i_38_] = is_39_;
		Class296_Sub11[] class296_sub11s = (aClass296_Sub11ArrayArrayArray5153[i][i_38_] = new Class296_Sub11[is_42_.length]);
		for (int i_49_ = 0; i_49_ < is_42_.length; i_49_++) {
			int i_50_ = is_44_[i_49_];
			int i_51_ = is_45_[i_49_];
			if ((anInt5150 & 0x20) != 0 && i_50_ != -1 && var_d.method14(i_50_, -9412).aBoolean1792) {
				i_51_ = 128;
				i_50_ = -1;
			}
			long l = (long) i_50_
					| ((long) i_46_ << 28 | ((long) i_48_ << 48 | (long) i_47_ << 42) | (long) (i_51_ << 14));
			Node class296;
			for (class296 = aClass263_5166.get(l); class296 != null; class296 = aClass263_5166.getUnknown(false)) {
				Class296_Sub11 class296_sub11 = (Class296_Sub11) class296;
				if (class296_sub11.anInt4639 == i_50_ && class296_sub11.aFloat4645 == (float) i_51_
						&& class296_sub11.anInt4638 == i_46_ && i_47_ == class296_sub11.anInt4643
						&& class296_sub11.anInt4637 == i_48_)
					break;
			}
			if (class296 == null) {
				class296_sub11s[i_49_] = new Class296_Sub11(this, i_50_, i_51_, i_46_, i_47_, i_48_);
				aClass263_5166.put(l, class296_sub11s[i_49_]);
			} else
				class296_sub11s[i_49_] = (Class296_Sub11) class296;
		}
		if (bool)
			aByteArrayArray5160[i][i_38_] = (byte) Class48.bitOR(aByteArrayArray5160[i][i_38_], 1);
		if (is_42_.length > anInt5172)
			anInt5172 = is_42_.length;
		anInt5165 += is_42_.length;
	}

	public void method3354(int i, int i_52_, int i_53_, boolean[][] bools, boolean bool, int i_54_) {
		method3374(-1, i_53_, i_52_, i, -62, bools, i_54_, bool);
	}

	public void ka(int i, int i_55_, int i_56_) {
		if ((aByteArrayArray5168[i][i_55_] & 0xff) < i_56_)
			aByteArrayArray5168[i][i_55_] = (byte) i_56_;
	}

	public void method3356(int i, int i_57_, int[] is, int[] is_58_, int[] is_59_, int[] is_60_, int[] is_61_,
			int[] is_62_, int[] is_63_, int[] is_64_, int[] is_65_, int[] is_66_, int[] is_67_, int i_68_, int i_69_,
			int i_70_, boolean bool) {
		int i_71_ = is_64_.length;
		int[] is_72_ = new int[i_71_ * 3];
		int[] is_73_ = new int[i_71_ * 3];
		int[] is_74_ = new int[i_71_ * 3];
		int[] is_75_ = new int[i_71_ * 3];
		int[] is_76_ = new int[i_71_ * 3];
		int[] is_77_ = new int[i_71_ * 3];
		int[] is_78_ = is_58_ != null ? new int[i_71_ * 3] : null;
		int[] is_79_ = is_60_ != null ? new int[i_71_ * 3] : null;
		int i_80_ = 0;
		for (int i_81_ = 0; i_81_ < i_71_; i_81_++) {
			int i_82_ = is_61_[i_81_];
			int i_83_ = is_62_[i_81_];
			int i_84_ = is_63_[i_81_];
			is_72_[i_80_] = is[i_82_];
			is_73_[i_80_] = is_59_[i_82_];
			is_74_[i_80_] = is_64_[i_81_];
			is_76_[i_80_] = is_66_[i_81_];
			is_77_[i_80_] = is_67_[i_81_];
			is_75_[i_80_] = is_65_ == null ? is_64_[i_81_] : is_65_[i_81_];
			if (is_58_ != null)
				is_78_[i_80_] = is_58_[i_82_];
			if (is_60_ != null)
				is_79_[i_80_] = is_60_[i_82_];
			i_80_++;
			is_72_[i_80_] = is[i_83_];
			is_73_[i_80_] = is_59_[i_83_];
			is_74_[i_80_] = is_64_[i_81_];
			is_76_[i_80_] = is_66_[i_81_];
			is_77_[i_80_] = is_67_[i_81_];
			is_75_[i_80_] = is_65_ != null ? is_65_[i_81_] : is_64_[i_81_];
			if (is_58_ != null)
				is_78_[i_80_] = is_58_[i_83_];
			if (is_60_ != null)
				is_79_[i_80_] = is_60_[i_83_];
			i_80_++;
			is_72_[i_80_] = is[i_84_];
			is_73_[i_80_] = is_59_[i_84_];
			is_74_[i_80_] = is_64_[i_81_];
			is_76_[i_80_] = is_66_[i_81_];
			is_77_[i_80_] = is_67_[i_81_];
			is_75_[i_80_] = is_65_ == null ? is_64_[i_81_] : is_65_[i_81_];
			if (is_58_ != null)
				is_78_[i_80_] = is_58_[i_84_];
			if (is_60_ != null)
				is_79_[i_80_] = is_60_[i_84_];
			i_80_++;
		}
		U(i, i_57_, is_72_, is_78_, is_73_, is_79_, is_74_, is_75_, is_76_, is_77_, i_68_, i_69_, i_70_, bool);
	}

	s_Sub3(ha_Sub1 var_ha_Sub1, int i, int i_85_, int i_86_, int i_87_, int[][] is, int[][] is_88_, int i_89_) {
		super(i_86_, i_87_, i_89_, is);
		aClass155_5144 = new NodeDeque();
		anInt5143 = anInt2835 - 2;
		aHa_Sub1_5151 = var_ha_Sub1;
		anIntArrayArrayArray5159 = new int[i_86_][i_87_][];
		anIntArrayArrayArray5145 = new int[i_86_][i_87_][];
		anInt5148 = 1 << anInt5143;
		aFloatArrayArray5175 = new float[anInt2832 + 1][anInt2834 + 1];
		aFloatArrayArray5174 = new float[anInt2832 + 1][anInt2834 + 1];
		anInt5150 = i_85_;
		aByteArrayArray5168 = new byte[i_86_ + 1][i_87_ + 1];
		aByteArrayArray5160 = new byte[i_86_][i_87_];
		aFloatArrayArray5171 = new float[anInt2832 + 1][anInt2834 + 1];
		aShortArrayArray5152 = new short[i_86_ * i_87_][];
		anIntArrayArrayArray5155 = new int[i_86_][i_87_][];
		aClass296_Sub11ArrayArrayArray5153 = new Class296_Sub11[i_86_][i_87_][];
		anIntArrayArrayArray5158 = new int[i_86_][i_87_][];
		anIntArrayArrayArray5154 = new int[i_86_][i_87_][];
		for (int i_90_ = 0; anInt2834 >= i_90_; i_90_++) {
			for (int i_91_ = 0; anInt2832 >= i_91_; i_91_++) {
				int i_92_ = anIntArrayArray2831[i_91_][i_90_];
				if ((float) i_92_ < aFloat5157)
					aFloat5157 = (float) i_92_;
				if ((float) i_92_ > aFloat5146)
					aFloat5146 = (float) i_92_;
				if (i_91_ > 0 && i_90_ > 0 && anInt2832 > i_91_ && i_90_ < anInt2834) {
					int i_93_ = -is_88_[i_91_ - 1][i_90_] + is_88_[i_91_ + 1][i_90_];
					int i_94_ = is_88_[i_91_][i_90_ + 1] - is_88_[i_91_][i_90_ - 1];
					float f = (float) (1.0 / Math.sqrt((double) (i_89_ * (i_89_ * 4) + i_93_ * i_93_ + i_94_ * i_94_)));
					aFloatArrayArray5174[i_91_][i_90_] = f * (float) i_93_;
					aFloatArrayArray5171[i_91_][i_90_] = f * (float) (-i_89_ * 2);
					aFloatArrayArray5175[i_91_][i_90_] = (float) i_94_ * f;
				}
			}
		}
		aFloat5146++;
		aFloat5157--;
		aClass263_5166 = new HashTable(128);
		if ((anInt5150 & 0x10) != 0)
			aClass386_5161 = new Class386(aHa_Sub1_5151, this);
	}

	public static void method3375(int i) {
		int i_95_ = 13 % ((-18 - i) / 34);
		RSA.MODULUS = null;
		aClass231_5142 = null;
		aClass262_5164 = null;
		aStringArrayArray5163 = null;
	}

	public boolean method3351(r var_r, int i, int i_96_, int i_97_, int i_98_, boolean bool) {
		if (aClass386_5161 == null || var_r == null)
			return false;
		int i_99_ = (i - (i_96_ * aHa_Sub1_5151.anInt3969 >> 8) >> aHa_Sub1_5151.anInt4036);
		int i_100_ = (i_97_ - (aHa_Sub1_5151.anInt4027 * i_96_ >> 8) >> aHa_Sub1_5151.anInt4036);
		return aClass386_5161.method4027(i_99_, i_100_, var_r, (byte) -110);
	}

	public void method3357(Class296_Sub35 class296_sub35, int[] is) {
		aClass155_5144.addLast((byte) -100, new Class296_Sub22(aHa_Sub1_5151, this, class296_sub35, is));
	}

	public void YA() {
		if (anInt5165 > 0) {
			byte[][] is = new byte[anInt2832 + 1][anInt2834 + 1];
			for (int i = 1; i < anInt2832; i++) {
				for (int i_101_ = 1; i_101_ < anInt2834; i_101_++)
					is[i][i_101_] = (byte) ((aByteArrayArray5168[i - 1][i_101_] >> 2)
							+ (aByteArrayArray5168[i + 1][i_101_] >> 3)
							+ ((aByteArrayArray5168[i][i_101_ - 1] >> 2) + (aByteArrayArray5168[i][i_101_ + 1] >> 3))
							+ (aByteArrayArray5168[i][i_101_] >> 1));
			}
			Class296_Sub11[] class296_sub11s = new Class296_Sub11[aClass263_5166.size((byte) -4)];
			aClass263_5166.toArray(5125, class296_sub11s);
			for (int i = 0; class296_sub11s.length > i; i++)
				class296_sub11s[i].method2499(anInt5165, (byte) 61);
			int i = 20;
			if (anIntArrayArrayArray5149 != null)
				i += 4;
			if ((anInt5150 & 0x7) != 0)
				i += 12;
			jaclib.memory.heap.NativeHeapBuffer nativeheapbuffer = aHa_Sub1_5151.aNativeHeap3932.a(anInt5165 * 4,
					false);
			jaclib.memory.heap.NativeHeapBuffer nativeheapbuffer_102_ = aHa_Sub1_5151.aNativeHeap3932.a(i * anInt5165,
					false);
			Stream stream = new Stream(nativeheapbuffer_102_);
			Stream stream_103_ = new Stream(nativeheapbuffer);
			Class296_Sub11[] class296_sub11s_104_ = new Class296_Sub11[anInt5165];
			int i_105_ = Statics.method629(false, anInt5165 / 4);
			if (i_105_ < 1)
				i_105_ = 1;
			HashTable class263 = new HashTable(i_105_);
			Class296_Sub11[] class296_sub11s_106_ = new Class296_Sub11[anInt5172];
			for (int i_107_ = 0; i_107_ < anInt2832; i_107_++) {
				for (int i_108_ = 0; anInt2834 > i_108_; i_108_++) {
					if (anIntArrayArrayArray5155[i_107_][i_108_] != null) {
						Class296_Sub11[] class296_sub11s_109_ = (aClass296_Sub11ArrayArrayArray5153[i_107_][i_108_]);
						int[] is_110_ = anIntArrayArrayArray5145[i_107_][i_108_];
						int[] is_111_ = anIntArrayArrayArray5154[i_107_][i_108_];
						int[] is_112_ = anIntArrayArrayArray5159[i_107_][i_108_];
						int[] is_113_ = anIntArrayArrayArray5155[i_107_][i_108_];
						int[] is_114_ = (anIntArrayArrayArray5158 != null ? anIntArrayArrayArray5158[i_107_][i_108_]
								: null);
						if (is_112_ == null)
							is_112_ = is_113_;
						int[] is_115_ = (anIntArrayArrayArray5149 != null ? anIntArrayArrayArray5149[i_107_][i_108_]
								: null);
						float f = aFloatArrayArray5174[i_107_][i_108_];
						float f_116_ = aFloatArrayArray5171[i_107_][i_108_];
						float f_117_ = aFloatArrayArray5175[i_107_][i_108_];
						float f_118_ = aFloatArrayArray5174[i_107_][i_108_ + 1];
						float f_119_ = aFloatArrayArray5171[i_107_][i_108_ + 1];
						float f_120_ = aFloatArrayArray5175[i_107_][i_108_ + 1];
						float f_121_ = aFloatArrayArray5174[i_107_ + 1][i_108_ + 1];
						float f_122_ = aFloatArrayArray5171[i_107_ + 1][i_108_ + 1];
						float f_123_ = aFloatArrayArray5175[i_107_ + 1][i_108_ + 1];
						float f_124_ = aFloatArrayArray5174[i_107_ + 1][i_108_];
						float f_125_ = aFloatArrayArray5171[i_107_ + 1][i_108_];
						float f_126_ = aFloatArrayArray5175[i_107_ + 1][i_108_];
						int i_127_ = is[i_107_][i_108_] & 0xff;
						int i_128_ = is[i_107_][i_108_ + 1] & 0xff;
						int i_129_ = is[i_107_ + 1][i_108_ + 1] & 0xff;
						int i_130_ = is[i_107_ + 1][i_108_] & 0xff;
						int i_131_ = 0;
						while_257_: for (int i_132_ = 0; is_113_.length > i_132_; i_132_++) {
							Class296_Sub11 class296_sub11 = class296_sub11s_109_[i_132_];
							for (int i_133_ = 0; i_131_ > i_133_; i_133_++) {
								if (class296_sub11 == class296_sub11s_106_[i_133_])
									continue while_257_;
							}
							class296_sub11s_106_[i_131_++] = class296_sub11;
						}
						short[] is_134_ = (aShortArrayArray5152[(i_107_
								+ i_108_ * anInt2832)] = new short[is_113_.length]);
						for (int i_135_ = 0; is_113_.length > i_135_; i_135_++) {
							int i_136_ = (i_107_ << anInt2835) + is_110_[i_135_];
							int i_137_ = (i_108_ << anInt2835) + is_111_[i_135_];
							int i_138_ = i_136_ >> anInt5143;
							int i_139_ = i_137_ >> anInt5143;
							int i_140_ = is_113_[i_135_];
							int i_141_ = is_112_[i_135_];
							int i_142_ = is_114_ == null ? 0 : is_114_[i_135_];
							long l = (long) i_139_
									| ((long) (i_138_ << 16) | ((long) i_140_ << 32 | (long) i_141_ << 48));
							int i_143_ = is_110_[i_135_];
							int i_144_ = is_111_[i_135_];
							int i_145_ = 74;
							int i_146_ = 0;
							float f_147_ = 1.0F;
							float f_148_;
							float f_149_;
							float f_150_;
							if (i_143_ != 0 || i_144_ != 0) {
								if (i_143_ == 0 && anInt2836 == i_144_) {
									f_148_ = f_118_;
									f_149_ = f_119_;
									f_150_ = f_120_;
									i_145_ -= i_128_;
								} else if (i_143_ != anInt2836 || i_144_ != anInt2836) {
									if (anInt2836 != i_143_ || i_144_ != 0) {
										float f_151_ = ((float) i_143_ / (float) anInt2836);
										float f_152_ = ((float) i_144_ / (float) anInt2836);
										float f_153_ = f + (-f + f_124_) * f_151_;
										float f_154_ = ((-f_116_ + f_125_) * f_151_ + f_116_);
										float f_155_ = (f_117_ + (f_126_ - f_117_) * f_151_);
										float f_156_ = ((f_121_ - f_118_) * f_151_ + f_118_);
										float f_157_ = (f_119_ + (-f_119_ + f_122_) * f_151_);
										f_149_ = f_154_ + ((-f_154_ + f_157_) * f_152_);
										float f_158_ = (f_120_ + (-f_120_ + f_123_) * f_151_);
										f_148_ = f_153_ + f_152_ * (f_156_ - f_153_);
										f_150_ = f_155_ + f_152_ * (f_158_ - f_155_);
										int i_159_ = ((i_143_ * (-i_127_ + i_130_) >> anInt2835) + i_127_);
										int i_160_ = (i_128_ + ((-i_128_ + i_129_) * i_143_ >> anInt2835));
										i_145_ -= (i_144_ * (-i_159_ + i_160_) >> anInt2835) + i_159_;
									} else {
										f_150_ = f_126_;
										f_149_ = f_125_;
										f_148_ = f_124_;
										i_145_ -= i_130_;
									}
								} else {
									f_148_ = f_121_;
									i_145_ -= i_129_;
									f_150_ = f_123_;
									f_149_ = f_122_;
								}
							} else {
								f_148_ = f;
								i_145_ -= i_127_;
								f_149_ = f_116_;
								f_150_ = f_117_;
							}
							if (i_140_ != -1) {
								int i_161_ = i_145_ * (i_140_ & 0x7f) >> 7;
								if (i_161_ < 2)
									i_161_ = 2;
								else if (i_161_ > 126)
									i_161_ = 126;
								i_146_ = (Class166_Sub1.anIntArray4300[i_161_ | i_140_ & 0xff80]);
								if ((anInt5150 & 0x7) == 0) {
									f_147_ = ((f_150_ * aHa_Sub1_5151.aFloatArray4030[2])
											+ ((aHa_Sub1_5151.aFloatArray4030[0] * f_148_)
													+ f_149_ * (aHa_Sub1_5151.aFloatArray4030[1])));
									f_147_ = ((f_147_
											* (f_147_ > 0.0F ? aHa_Sub1_5151.aFloat4022 : aHa_Sub1_5151.aFloat4007))
											+ aHa_Sub1_5151.aFloat4010);
								}
							}
							Node class296 = null;
							if ((i_136_ & anInt5148 - 1) == 0 && (anInt5148 - 1 & i_137_) == 0)
								class296 = class263.get(l);
							int i_162_;
							if (class296 == null) {
								int i_163_;
								if (i_141_ == i_140_)
									i_163_ = i_146_;
								else {
									int i_164_ = i_145_ * (i_141_ & 0x7f) >> 7;
									if (i_164_ >= 2) {
										if (i_164_ > 126)
											i_164_ = 126;
									} else
										i_164_ = 2;
									i_163_ = (Class166_Sub1.anIntArray4300[i_164_ | i_141_ & 0xff80]);
									if ((anInt5150 & 0x7) == 0) {
										float f_165_ = ((aHa_Sub1_5151.aFloatArray4030[0]) * f_148_
												+ f_149_ * (aHa_Sub1_5151.aFloatArray4030[1])
												+ (aHa_Sub1_5151.aFloatArray4030[2]) * f_150_);
										f_165_ = (aHa_Sub1_5151.aFloat4010
												+ (f_147_ * (!(f_147_ > 0.0F) ? aHa_Sub1_5151.aFloat4007
														: (aHa_Sub1_5151.aFloat4022))));
										int i_166_ = (i_163_ & 0xffd3f6) >> 16;
										int i_167_ = i_163_ >> 8 & 0xff;
										int i_168_ = i_163_ & 0xff;
										i_166_ *= f_165_;
										i_167_ *= f_165_;
										if (i_166_ < 0)
											i_166_ = 0;
										else if (i_166_ > 255)
											i_166_ = 255;
										if (i_167_ >= 0) {
											if (i_167_ > 255)
												i_167_ = 255;
										} else
											i_167_ = 0;
										i_168_ *= f_165_;
										if (i_168_ >= 0) {
											if (i_168_ > 255)
												i_168_ = 255;
										} else
											i_168_ = 0;
										i_163_ = i_168_ | (i_167_ << 8 | i_166_ << 16);
									}
								}
								if (Stream.a()) {
									stream.a((float) i_136_);
									stream.a((float) (i_142_ + this.method3349(0, i_137_, i_136_)));
									stream.a((float) i_137_);
									stream.a((float) i_136_);
									stream.a((float) i_137_);
									if (anIntArrayArrayArray5149 != null)
										stream.a((float) (is_115_ == null ? 0 : (is_115_[i_135_] - 1)));
									if ((anInt5150 & 0x7) != 0) {
										stream.a(f_148_);
										stream.a(f_149_);
										stream.a(f_150_);
									}
								} else {
									stream.b((float) i_136_);
									stream.b((float) (this.method3349(0, i_137_, i_136_) + i_142_));
									stream.b((float) i_137_);
									stream.b((float) i_136_);
									stream.b((float) i_137_);
									if (anIntArrayArrayArray5149 != null)
										stream.b((float) (is_115_ == null ? 0 : (is_115_[i_135_] - 1)));
									if ((anInt5150 & 0x7) != 0) {
										stream.b(f_148_);
										stream.b(f_149_);
										stream.b(f_150_);
									}
								}
								if (aHa_Sub1_5151.anInt3956 == 0)
									stream_103_.b(i_163_ | ~0xffffff);
								else
									stream_103_.c(i_163_ | ~0xffffff);
								i_162_ = anInt5156++;
								is_134_[i_135_] = (short) i_162_;
								if (i_140_ != -1)
									class296_sub11s_104_[i_162_] = class296_sub11s_109_[i_135_];
								class263.put(l, new Class296_Sub20(is_134_[i_135_]));
							} else {
								is_134_[i_135_] = ((Class296_Sub20) class296).aShort4715;
								i_162_ = is_134_[i_135_] & 0xffff;
								if (i_140_ != -1
										&& (class296_sub11s_109_[i_135_].uid < (class296_sub11s_104_[i_162_].uid)))
									class296_sub11s_104_[i_162_] = class296_sub11s_109_[i_135_];
							}
							for (int i_169_ = 0; i_169_ < i_131_; i_169_++)
								class296_sub11s_106_[i_169_].method2496(i_162_, i_146_, -13621, i_145_, f_147_);
							anInt5147++;
						}
					}
				}
			}
			for (int i_170_ = 0; i_170_ < anInt5156; i_170_++) {
				Class296_Sub11 class296_sub11 = class296_sub11s_104_[i_170_];
				if (class296_sub11 != null)
					class296_sub11.method2504(106, i_170_);
			}
			for (int i_171_ = 0; i_171_ < anInt2832; i_171_++) {
				for (int i_172_ = 0; anInt2834 > i_172_; i_172_++) {
					short[] is_173_ = aShortArrayArray5152[anInt2832 * i_172_ + i_171_];
					if (is_173_ != null) {
						int i_174_ = 0;
						int i_175_ = 0;
						while (i_175_ < is_173_.length) {
							int i_176_ = is_173_[i_175_++] & 0xffff;
							int i_177_ = is_173_[i_175_++] & 0xffff;
							int i_178_ = is_173_[i_175_++] & 0xffff;
							Class296_Sub11 class296_sub11 = class296_sub11s_104_[i_176_];
							Class296_Sub11 class296_sub11_179_ = class296_sub11s_104_[i_177_];
							Class296_Sub11 class296_sub11_180_ = class296_sub11s_104_[i_178_];
							Class296_Sub11 class296_sub11_181_ = null;
							if (class296_sub11 != null) {
								class296_sub11_181_ = class296_sub11;
								class296_sub11.method2502(-4, i_174_, i_171_, i_172_);
							}
							if (class296_sub11_179_ != null) {
								class296_sub11_179_.method2502(67, i_174_, i_171_, i_172_);
								if (class296_sub11_181_ == null || (class296_sub11_179_.uid < class296_sub11_181_.uid))
									class296_sub11_181_ = class296_sub11_179_;
							}
							if (class296_sub11_180_ != null) {
								class296_sub11_180_.method2502(-121, i_174_, i_171_, i_172_);
								if (class296_sub11_181_ == null || (class296_sub11_180_.uid < class296_sub11_181_.uid))
									class296_sub11_181_ = class296_sub11_180_;
							}
							if (class296_sub11_181_ != null) {
								if (class296_sub11 != null)
									class296_sub11_181_.method2504(103, i_176_);
								if (class296_sub11_179_ != null)
									class296_sub11_181_.method2504(-21, i_177_);
								if (class296_sub11_180_ != null)
									class296_sub11_181_.method2504(99, i_178_);
								class296_sub11_181_.method2502(-125, i_174_, i_171_, i_172_);
							}
							i_174_++;
						}
					}
				}
			}
			stream.b();
			stream_103_.b();
			anInterface15_Impl2_5170 = aHa_Sub1_5151.method1205(false, -102);
			anInterface15_Impl2_5170.method48(false, nativeheapbuffer, 4, anInt5156 * 4);
			anInterface15_Impl2_5169 = aHa_Sub1_5151.method1205(false, 105);
			anInterface15_Impl2_5169.method48(false, nativeheapbuffer_102_, i, anInt5156 * i);
			if ((anInt5150 & 0x7) == 0) {
				if (anIntArrayArrayArray5149 == null)
					aClass127_5173 = (aHa_Sub1_5151.method1233((byte) -46,
							(new Class211[] {
									new Class211(new Class356[] { Class356.aClass356_3072, Class356.aClass356_3077 }),
									new Class211(Class356.aClass356_3075) })));
				else
					aClass127_5173 = (aHa_Sub1_5151.method1233((byte) -46,
							(new Class211[] { new Class211(new Class356[] { Class356.aClass356_3072,
									Class356.aClass356_3077, Class356.aClass356_3076 }),
									new Class211(Class356.aClass356_3075) })));
			} else if (anIntArrayArrayArray5149 != null)
				aClass127_5173 = (aHa_Sub1_5151.method1233((byte) -46,
						(new Class211[] {
								new Class211(new Class356[] { Class356.aClass356_3072, Class356.aClass356_3077,
										Class356.aClass356_3076, Class356.aClass356_3073 }),
								new Class211(Class356.aClass356_3075) })));
			else
				aClass127_5173 = (aHa_Sub1_5151.method1233((byte) -46,
						(new Class211[] { new Class211(new Class356[] { Class356.aClass356_3072,
								Class356.aClass356_3077, Class356.aClass356_3073 }),
								new Class211(Class356.aClass356_3075) })));
			int i_182_ = 0;
			for (int i_183_ = 0; class296_sub11s.length > i_183_; i_183_++) {
				if (class296_sub11s[i_183_].anInt4641 > 0)
					class296_sub11s[i_182_++] = class296_sub11s[i_183_];
			}
			aClass296_Sub11Array5167 = new Class296_Sub11[i_182_];
			long[] ls = new long[i_182_];
			for (int i_184_ = 0; i_182_ > i_184_; i_184_++) {
				Class296_Sub11 class296_sub11 = class296_sub11s[i_184_];
				ls[i_184_] = class296_sub11.uid;
				aClass296_Sub11Array5167[i_184_] = class296_sub11;
				class296_sub11.method2503(true, anInt5156);
			}
			Class308.method3289(-1791647608, aClass296_Sub11Array5167, ls);
			if (aClass386_5161 != null)
				aClass386_5161.method4026(93);
		} else
			aClass386_5161 = null;
		anIntArrayArrayArray5158 = null;
		aByteArrayArray5168 = null;
		aFloatArrayArray5174 = aFloatArrayArray5171 = aFloatArrayArray5175 = null;
		aClass296_Sub11ArrayArrayArray5153 = null;
		anIntArrayArrayArray5145 = anIntArrayArrayArray5154 = null;
		anIntArrayArrayArray5155 = null;
		aClass263_5166 = null;
		anIntArrayArrayArray5159 = null;
		anIntArrayArrayArray5149 = null;
	}

	public void method3350(int i, int i_185_, int i_186_, int i_187_, int i_188_, int i_189_, int i_190_,
			boolean[][] bools) {
		if (anInt5165 > 0) {
			Interface15_Impl1 interface15_impl1 = aHa_Sub1_5151.method1223(anInt5147, (byte) -111);
			int i_191_ = 0;
			int i_192_ = 32767;
			int i_193_ = -32768;
			for (int i_194_ = 0; i_194_ < 4; i_194_++) {
				i_191_ = 0;
				Buffer buffer = interface15_impl1.method30((byte) -122, true);
				if (buffer != null) {
					Stream stream = aHa_Sub1_5151.method1156(buffer, -112);
					if (Stream.a()) {
						for (int i_195_ = i_188_; i_190_ > i_195_; i_195_++) {
							int i_196_ = anInt2832 * i_195_ + i_187_;
							for (int i_197_ = i_187_; i_197_ < i_189_; i_197_++) {
								if (bools[i_197_ - i_187_][i_195_ - i_188_]) {
									short[] is = aShortArrayArray5152[i_196_];
									if (is != null) {
										for (int i_198_ = 0; is.length > i_198_; i_198_++) {
											int i_199_ = is[i_198_] & 0xffff;
											stream.d(i_199_);
											if (i_199_ < i_192_)
												i_192_ = i_199_;
											if (i_199_ > i_193_)
												i_193_ = i_199_;
											i_191_++;
										}
									}
								}
								i_196_++;
							}
						}
					} else {
						for (int i_200_ = i_188_; i_200_ < i_190_; i_200_++) {
							int i_201_ = i_187_ + anInt2832 * i_200_;
							for (int i_202_ = i_187_; i_202_ < i_189_; i_202_++) {
								if (bools[-i_187_ + i_202_][-i_188_ + i_200_]) {
									short[] is = aShortArrayArray5152[i_201_];
									if (is != null) {
										for (int i_203_ = 0; is.length > i_203_; i_203_++) {
											int i_204_ = is[i_203_] & 0xffff;
											i_191_++;
											stream.a(i_204_);
											if (i_204_ > i_193_)
												i_193_ = i_204_;
											if (i_192_ > i_204_)
												i_192_ = i_204_;
										}
									}
								}
								i_201_++;
							}
						}
					}
					stream.b();
					if (interface15_impl1.method33(32185))
						break;
				}
			}
			if (i_191_ > 0) {
				aHa_Sub1_5151.method1105((byte) -81);
				aHa_Sub1_5151.method1201(-25357, false);
				aHa_Sub1_5151.method1137(4, false);
				aHa_Sub1_5151.method1110(false, -110);
				aHa_Sub1_5151.method1106(-105, false);
				aHa_Sub1_5151.method1161(31156, 0);
				aHa_Sub1_5151.method1133(false, -2, 0, false);
				aHa_Sub1_5151.method1140(null, false);
				Class373_Sub2 class373_sub2 = aHa_Sub1_5151.method1215(98);
				float[] fs = aHa_Sub1_5151.method1135(6);
				fs[0] = (float) i_186_ / ((float) anInt2836 * 128.0F * (float) aHa_Sub1_5151.anInt3921);
				fs[12] = -1.0F
						- (((float) (i_187_ * i_186_) / 128.0F - (float) (i * 2)) / (float) aHa_Sub1_5151.anInt3921);
				fs[13] = -(((float) (i_185_ * 2) + (float) (i_190_ * i_186_) / 128.0F)
						/ (float) aHa_Sub1_5151.anInt3920) + 1.0F;
				fs[1] = 0.0F;
				fs[7] = 0.0F;
				fs[3] = 0.0F;
				fs[9] = 0.0F;
				fs[8] = 0.0F;
				fs[11] = 0.0F;
				fs[10] = 1.0F / (aFloat5146 - aFloat5157);
				fs[15] = 1.0F;
				fs[2] = 0.0F;
				fs[4] = 0.0F;
				fs[5] = (float) i_186_ / ((float) aHa_Sub1_5151.anInt3920 * ((float) anInt2836 * 128.0F));
				fs[6] = 0.0F;
				fs[14] = -aFloat5157 / (aFloat5146 - aFloat5157);
				class373_sub2.method3940(0.0F, 0.0F, 1.0F, 0.0F, 1.0F, 0.0F, 1.0F, 0.0F, 0.0F, -118);
				aHa_Sub1_5151.method1168(false);
				aHa_Sub1_5151.method1202(0);
				if ((anInt5150 & 0x7) != 0) {
					aHa_Sub1_5151.method1137(4, true);
					aHa_Sub1_5151.method1138(0);
				} else
					aHa_Sub1_5151.method1137(4, false);
				aHa_Sub1_5151.method1101(false, (byte) -53);
				aHa_Sub1_5151.method1178(anInterface15_Impl2_5169, (byte) -127, 0);
				aHa_Sub1_5151.method1178(anInterface15_Impl2_5170, (byte) -124, 1);
				aHa_Sub1_5151.method1209(aClass127_5173, (byte) -122);
				aHa_Sub1_5151.method1232(Class166_Sub1.aClass289_4298, interface15_impl1, i_193_ + 1 - i_192_,
						i_191_ / 3, 0, 106, i_192_);
				aHa_Sub1_5151.method1101(true, (byte) -116);
			}
		}
	}

	public r fa(int i, int i_205_, r var_r) {
		if ((aByteArrayArray5160[i][i_205_] & 0x1) == 0)
			return null;
		int i_206_ = anInt2836 >> aHa_Sub1_5151.anInt4036;
		r_Sub2 var_r_Sub2 = (r_Sub2) var_r;
		r_Sub2 var_r_Sub2_207_;
		if (var_r_Sub2 != null && var_r_Sub2.method2870(i_206_, -124914704, i_206_)) {
			var_r_Sub2_207_ = var_r_Sub2;
			var_r_Sub2_207_.method2866(-1);
		} else
			var_r_Sub2_207_ = new r_Sub2(aHa_Sub1_5151, i_206_, i_206_);
		var_r_Sub2_207_.method2869(0, 0, i_206_, i_206_, -112);
		method3376(0, var_r_Sub2_207_, i, i_205_);
		return var_r_Sub2_207_;
	}

	private void method3376(int i, r_Sub2 var_r_Sub2, int i_208_, int i_209_) {
		int[] is = anIntArrayArrayArray5145[i_208_][i_209_];
		int[] is_210_ = anIntArrayArrayArray5154[i_208_][i_209_];
		int i_211_ = is.length;
		if (Class189.anIntArray1929.length < i_211_) {
			Class189.anIntArray1929 = new int[i_211_];
			Class296_Sub35_Sub1.anIntArray6108 = new int[i_211_];
		}
		for (int i_212_ = i; i_211_ > i_212_; i_212_++) {
			Class189.anIntArray1929[i_212_] = is[i_212_] >> aHa_Sub1_5151.anInt4036;
			Class296_Sub35_Sub1.anIntArray6108[i_212_] = is_210_[i_212_] >> aHa_Sub1_5151.anInt4036;
		}
		int i_213_ = 0;
		while (i_211_ > i_213_) {
			int i_214_ = Class189.anIntArray1929[i_213_];
			int i_215_ = Class296_Sub35_Sub1.anIntArray6108[i_213_++];
			int i_216_ = Class189.anIntArray1929[i_213_];
			int i_217_ = Class296_Sub35_Sub1.anIntArray6108[i_213_++];
			int i_218_ = Class189.anIntArray1929[i_213_];
			int i_219_ = Class296_Sub35_Sub1.anIntArray6108[i_213_++];
			if (((-i_216_ + i_214_) * (-i_219_ + i_217_) - (-i_216_ + i_218_) * (-i_215_ + i_217_)) > 0)
				var_r_Sub2.method2868(-709523440, i_215_, i_214_, i_216_, i_219_, i_217_, i_218_);
		}
	}
}
