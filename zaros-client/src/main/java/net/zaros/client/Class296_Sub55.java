package net.zaros.client;

/* Class296_Sub55 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub55 extends Node {
	private Interface20 anInterface20_5060;
	private Class272 aClass272_5061;
	private Class296_Sub35 aClass296_Sub35_5062;
	private Class272 aClass272_5063;
	private float[][] aFloatArrayArray5064;
	private Packet aClass296_Sub17_5065;
	private s_Sub2 aS_Sub2_5066;
	private int anInt5067;
	private HashTable aClass263_5068;
	private int anInt5069;
	private int anInt5070;
	private Interface5 anInterface5_5071;
	private float[][] aFloatArrayArray5072;
	static float aFloat5073;
	private int anInt5074;
	private int anInt5075;
	private float[][] aFloatArrayArray5076;
	private ha_Sub3 aHa_Sub3_5077;
	private int anInt5078;
	private Class296_Sub17_Sub2 aClass296_Sub17_Sub2_5079;

	private final void method3213(short i, boolean bool) {
		if (bool)
			method3214(-75, true, -128, -55, 55, 124, -109);
		if (aHa_Sub3_5077.aBoolean4184)
			aClass296_Sub17_5065.p2(i);
		else
			aClass296_Sub17_5065.method2590(i);
	}

	private final void method3214(int i, boolean bool, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		long l = -1L;
		int i_5_ = (i_0_ << aS_Sub2_5066.anInt2835) + i_1_;
		int i_6_ = i_3_ + (i_4_ << aS_Sub2_5066.anInt2835);
		int i_7_ = aS_Sub2_5066.method3349(0, i_6_, i_5_);
		if ((i_1_ & 0x7f) == 0 || (i_3_ & 0x7f) == 0) {
			l = ((long) i_6_ & 0xffffL) << 16 | (long) i_5_ & 0xffffL;
			Node class296 = aClass263_5068.get(l);
			if (class296 != null) {
				method3213(((Class296_Sub20) class296).aShort4715, false);
				return;
			}
		}
		short i_8_ = (short) anInt5067++;
		if (l != -1L)
			aClass263_5068.put(l, new Class296_Sub20(i_8_));
		float f;
		float f_9_;
		float f_10_;
		if (i_1_ == 0 && i_3_ == 0) {
			f_10_ = aFloatArrayArray5064[i_2_][i];
			f = aFloatArrayArray5076[i_2_][i];
			f_9_ = aFloatArrayArray5072[i_2_][i];
		} else if (i_1_ == aS_Sub2_5066.anInt2836 && i_3_ == 0) {
			f = aFloatArrayArray5076[i_2_ + 1][i];
			f_9_ = aFloatArrayArray5072[i_2_ + 1][i];
			f_10_ = aFloatArrayArray5064[i_2_ + 1][i];
		} else if (aS_Sub2_5066.anInt2836 != i_1_ || aS_Sub2_5066.anInt2836 != i_3_) {
			if (i_1_ != 0 || i_3_ != aS_Sub2_5066.anInt2836) {
				float f_11_ = (float) i_1_ / (float) aS_Sub2_5066.anInt2836;
				float f_12_ = (float) i_3_ / (float) aS_Sub2_5066.anInt2836;
				float f_13_ = aFloatArrayArray5064[i_2_][i];
				float f_14_ = aFloatArrayArray5072[i_2_][i];
				float f_15_ = aFloatArrayArray5076[i_2_][i];
				float f_16_ = aFloatArrayArray5064[i_2_ + 1][i];
				float f_17_ = aFloatArrayArray5072[i_2_ + 1][i];
				f_13_ += (aFloatArrayArray5064[i_2_][i + 1] - f_13_) * f_11_;
				float f_18_ = aFloatArrayArray5076[i_2_ + 1][i];
				f_14_ += f_11_ * (aFloatArrayArray5072[i_2_][i + 1] - f_14_);
				f_15_ += (-f_15_ + aFloatArrayArray5076[i_2_][i + 1]) * f_11_;
				f_16_ += ((-f_16_ + aFloatArrayArray5064[i_2_ + 1][i + 1]) * f_11_);
				f_17_ += f_11_ * (-f_17_ + aFloatArrayArray5072[i_2_ + 1][i + 1]);
				f_9_ = (f_17_ - f_14_) * f_12_ + f_14_;
				f_10_ = (f_16_ - f_13_) * f_12_ + f_13_;
				f_18_ += ((-f_18_ + aFloatArrayArray5076[i_2_ + 1][i + 1]) * f_11_);
				f = f_15_ + (f_18_ - f_15_) * f_12_;
			} else {
				f = aFloatArrayArray5076[i_2_][i + 1];
				f_10_ = aFloatArrayArray5064[i_2_][i + 1];
				f_9_ = aFloatArrayArray5072[i_2_][i + 1];
			}
		} else {
			f_10_ = aFloatArrayArray5064[i_2_ + 1][i + 1];
			f_9_ = aFloatArrayArray5072[i_2_ + 1][i + 1];
			f = aFloatArrayArray5076[i_2_ + 1][i + 1];
		}
		float f_19_ = (float) (aClass296_Sub35_5062.method2749(bool) - i_5_);
		float f_20_ = (float) (-i_7_ + aClass296_Sub35_5062.method2751(-28925));
		float f_21_ = (float) (-i_6_ + aClass296_Sub35_5062.method2750(-4444));
		float f_22_ = (float) Math.sqrt((double) (f_19_ * f_19_ + f_20_ * f_20_ + f_21_ * f_21_));
		float f_23_ = 1.0F / f_22_;
		f_20_ *= f_23_;
		f_21_ *= f_23_;
		f_19_ *= f_23_;
		float f_24_ = f_22_ / (float) aClass296_Sub35_5062.method2748(116);
		float f_25_ = 1.0F - f_24_ * f_24_;
		if (f_25_ < 0.0F)
			f_25_ = 0.0F;
		float f_26_ = f * f_21_ + (f_10_ * f_19_ + f_9_ * f_20_);
		if (f_26_ < 0.0F)
			f_26_ = 0.0F;
		float f_27_ = f_26_ * f_25_ * 2.0F;
		if (f_27_ > 1.0F)
			f_27_ = 1.0F;
		int i_28_ = aClass296_Sub35_5062.method2746(-24996);
		int i_29_ = (int) (f_27_ * (float) ((i_28_ & 0xffed85) >> 16));
		if (i_29_ > 255)
			i_29_ = 255;
		int i_30_ = (int) ((float) (i_28_ >> 8 & 0xff) * f_27_);
		if (i_30_ > 255)
			i_30_ = 255;
		int i_31_ = (int) (f_27_ * (float) (i_28_ & 0xff));
		if (aHa_Sub3_5077.aBoolean4184) {
			aClass296_Sub17_Sub2_5079.method2636((byte) 101, (float) i_5_);
			aClass296_Sub17_Sub2_5079.method2636((byte) 71, (float) i_7_);
			aClass296_Sub17_Sub2_5079.method2636((byte) 71, (float) i_6_);
		} else {
			aClass296_Sub17_Sub2_5079.method2637((byte) -103, (float) i_5_);
			aClass296_Sub17_Sub2_5079.method2637((byte) -97, (float) i_7_);
			aClass296_Sub17_Sub2_5079.method2637((byte) -127, (float) i_6_);
		}
		if (i_31_ > 255)
			i_31_ = 255;
		aClass296_Sub17_Sub2_5079.p1(i_29_);
		aClass296_Sub17_Sub2_5079.p1(i_30_);
		aClass296_Sub17_Sub2_5079.p1(i_31_);
		aClass296_Sub17_Sub2_5079.p1(255);
		method3213(i_8_, false);
	}

	final void method3215(int i, boolean[][] bools, int i_32_, int i_33_, int i_34_) {
		if (i_34_ != -7732)
			method3214(53, true, -40, 34, -107, 75, -109);
		if (anInterface20_5060 != null && anInt5078 <= i_33_ + i_32_ && anInt5070 >= -i_33_ + i_32_ && i + i_33_ >= anInt5069 && i - i_33_ <= anInt5074) {
			for (int i_35_ = anInt5069; anInt5074 >= i_35_; i_35_++) {
				for (int i_36_ = anInt5078; anInt5070 >= i_36_; i_36_++) {
					int i_37_ = i_36_ - i_32_;
					int i_38_ = -i + i_35_;
					if (i_37_ > -i_33_ && i_37_ < i_33_ && -i_33_ < i_38_ && i_33_ > i_38_ && bools[i_37_ + i_33_][i_33_ + i_38_]) {
						aHa_Sub3_5077.method1354(false, (int) (aClass296_Sub35_5062.method2745((byte) 100) * 255.0F) << 24);
						aHa_Sub3_5077.method1323(null, aClass272_5063, null, false, aClass272_5061);
						aHa_Sub3_5077.method1348(anInt5075, anInterface20_5060, 4, (byte) -17, 0);
						return;
					}
				}
			}
		}
	}

	Class296_Sub55(ha_Sub3 var_ha_Sub3, s_Sub2 var_s_Sub2, Class296_Sub35 class296_sub35, int[] is) {
		aClass296_Sub35_5062 = class296_sub35;
		aS_Sub2_5066 = var_s_Sub2;
		aHa_Sub3_5077 = var_ha_Sub3;
		int i = (aClass296_Sub35_5062.method2748(76) - (var_s_Sub2.anInt2836 >> 1));
		anInt5078 = (-i + aClass296_Sub35_5062.method2749(true) >> var_s_Sub2.anInt2835);
		anInt5070 = (aClass296_Sub35_5062.method2749(true) + i >> var_s_Sub2.anInt2835);
		anInt5069 = (-i + aClass296_Sub35_5062.method2750(-4444) >> var_s_Sub2.anInt2835);
		anInt5074 = (aClass296_Sub35_5062.method2750(-4444) + i >> var_s_Sub2.anInt2835);
		int i_39_ = -anInt5078 + anInt5070 + 1;
		int i_40_ = -anInt5069 + (anInt5074 + 1);
		aFloatArrayArray5064 = new float[i_39_ + 1][i_40_ + 1];
		aFloatArrayArray5076 = new float[i_39_ + 1][i_40_ + 1];
		aFloatArrayArray5072 = new float[i_39_ + 1][i_40_ + 1];
		for (int i_41_ = 0; i_40_ >= i_41_; i_41_++) {
			int i_42_ = anInt5069 + i_41_;
			if (i_42_ > 0 && i_42_ < aS_Sub2_5066.anInt2834 - 1) {
				for (int i_43_ = 0; i_43_ <= i_39_; i_43_++) {
					int i_44_ = i_43_ + anInt5078;
					if (i_44_ > 0 && i_44_ < aS_Sub2_5066.anInt2832 - 1) {
						int i_45_ = (var_s_Sub2.method3355(i_42_, (byte) -106, i_44_ + 1) - var_s_Sub2.method3355(i_42_, (byte) -116, i_44_ - 1));
						int i_46_ = (var_s_Sub2.method3355(i_42_ + 1, (byte) -124, i_44_) - var_s_Sub2.method3355(i_42_ - 1, (byte) -119, i_44_));
						float f = (float) (1.0 / Math.sqrt((double) (i_46_ * i_46_ + 65536 + (i_45_ * i_45_))));
						aFloatArrayArray5064[i_43_][i_41_] = f * (float) i_45_;
						aFloatArrayArray5072[i_43_][i_41_] = f * -256.0F;
						aFloatArrayArray5076[i_43_][i_41_] = f * (float) i_46_;
					}
				}
			}
		}
		int i_47_ = 0;
		for (int i_48_ = anInt5069; i_48_ <= anInt5074; i_48_++) {
			if (i_48_ >= 0 && i_48_ < var_s_Sub2.anInt2834) {
				for (int i_49_ = anInt5078; i_49_ <= anInt5070; i_49_++) {
					if (i_49_ >= 0 && i_49_ < var_s_Sub2.anInt2832) {
						int i_50_ = is[i_47_];
						int[] is_51_ = (var_s_Sub2.anIntArrayArrayArray5117[i_49_][i_48_]);
						if (is_51_ != null && i_50_ != 0) {
							if (i_50_ == 1) {
								int i_52_ = 0;
								while (is_51_.length > i_52_) {
									if (is_51_[i_52_++] != -1 && is_51_[i_52_++] != -1 && is_51_[i_52_++] != -1)
										anInt5075 += 3;
								}
							} else
								anInt5075 += 3;
						}
					}
					i_47_++;
				}
			} else
				i_47_ = i_47_ + (anInt5070 - anInt5078);
		}
		if (anInt5075 <= 0) {
			anInterface20_5060 = null;
			anInterface5_5071 = null;
			aClass272_5061 = null;
			aClass272_5063 = null;
		} else {
			aClass296_Sub17_5065 = new Packet(anInt5075 * 2);
			aClass296_Sub17_Sub2_5079 = new Class296_Sub17_Sub2(anInt5075 * 16);
			aClass263_5068 = new HashTable(Class8.get_next_high_pow2(anInt5075));
			int i_53_ = 0;
			i_47_ = 0;
			for (int i_54_ = anInt5069; i_54_ <= anInt5074; i_54_++) {
				if (i_54_ >= 0 && var_s_Sub2.anInt2834 > i_54_) {
					int i_55_ = 0;
					for (int i_56_ = anInt5078; i_56_ <= anInt5070; i_56_++) {
						if (i_56_ >= 0 && i_56_ < var_s_Sub2.anInt2832) {
							int i_57_ = is[i_47_];
							int[] is_58_ = (var_s_Sub2.anIntArrayArrayArray5117[i_56_][i_54_]);
							if (is_58_ != null && i_57_ != 0) {
								if (i_57_ != 1) {
									if (i_57_ == 3) {
										method3214(i_53_, true, i_56_, 0, i_55_, 0, i_54_);
										method3214(i_53_, true, i_56_, var_s_Sub2.anInt2836, i_55_, 0, i_54_);
										method3214(i_53_, true, i_56_, 0, i_55_, var_s_Sub2.anInt2836, i_54_);
									} else if (i_57_ != 2) {
										if (i_57_ != 5) {
											if (i_57_ == 4) {
												method3214(i_53_, true, i_56_, 0, i_55_, (var_s_Sub2.anInt2836), i_54_);
												method3214(i_53_, true, i_56_, 0, i_55_, 0, i_54_);
												method3214(i_53_, true, i_56_, (var_s_Sub2.anInt2836), i_55_, (var_s_Sub2.anInt2836), i_54_);
											}
										} else {
											method3214(i_53_, true, i_56_, var_s_Sub2.anInt2836, i_55_, var_s_Sub2.anInt2836, i_54_);
											method3214(i_53_, true, i_56_, 0, i_55_, var_s_Sub2.anInt2836, i_54_);
											method3214(i_53_, true, i_56_, var_s_Sub2.anInt2836, i_55_, 0, i_54_);
										}
									} else {
										method3214(i_53_, true, i_56_, var_s_Sub2.anInt2836, i_55_, 0, i_54_);
										method3214(i_53_, true, i_56_, var_s_Sub2.anInt2836, i_55_, var_s_Sub2.anInt2836, i_54_);
										method3214(i_53_, true, i_56_, 0, i_55_, 0, i_54_);
									}
								} else {
									int[] is_59_ = (var_s_Sub2.anIntArrayArrayArray5116[i_56_][i_54_]);
									int[] is_60_ = (var_s_Sub2.anIntArrayArrayArray5127[i_56_][i_54_]);
									int i_61_ = 0;
									while (i_61_ < is_58_.length) {
										if (is_58_[i_61_] != -1 && is_58_[i_61_ + 1] != -1 && is_58_[i_61_ + 2] != -1) {
											method3214(i_53_, true, i_56_, is_59_[i_61_], i_55_, is_60_[i_61_], i_54_);
											i_61_++;
											method3214(i_53_, true, i_56_, is_59_[i_61_], i_55_, is_60_[i_61_], i_54_);
											i_61_++;
											method3214(i_53_, true, i_56_, is_59_[i_61_], i_55_, is_60_[i_61_], i_54_);
											i_61_++;
										} else
											i_61_ += 3;
									}
								}
							}
						}
						i_55_++;
						i_47_++;
					}
				} else
					i_47_ += -anInt5078 + anInt5070;
				i_53_++;
			}
			anInterface20_5060 = aHa_Sub3_5077.method1310(65280, aClass296_Sub17_5065.pos, 5123, aClass296_Sub17_5065.data, false);
			anInterface5_5071 = aHa_Sub3_5077.method1321(aClass296_Sub17_Sub2_5079.pos, false, 16, (aClass296_Sub17_Sub2_5079.data), 5888);
			aClass272_5061 = new Class272(anInterface5_5071, 5126, 3, 0);
			aClass272_5063 = new Class272(anInterface5_5071, 5121, 4, 12);
		}
		aClass296_Sub17_5065 = null;
		aFloatArrayArray5064 = aFloatArrayArray5072 = aFloatArrayArray5076 = null;
		aClass296_Sub17_Sub2_5079 = null;
		aClass263_5068 = null;
	}
}
