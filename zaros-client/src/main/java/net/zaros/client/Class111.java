package net.zaros.client;
/* Class111 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.math.BigInteger;

final class Class111 {
	private Class343_Sub1[] aClass343_Sub1Array1144;
	private BigInteger aBigInteger1145;
	private Class315 aClass315_1146;
	private BigInteger aBigInteger1147;
	private Class296_Sub39_Sub20_Sub1 aClass296_Sub39_Sub20_Sub1_1148;
	private Class285 aClass285_1150;
	private Packet aClass296_Sub17_1151;

	final boolean method972(byte i) {
		if (aClass296_Sub17_1151 != null) {
			return true;
		}
		if (aClass296_Sub39_Sub20_Sub1_1148 == null) {
			if (aClass285_1150.method2371(23192)) {
				return false;
			}
			aClass296_Sub39_Sub20_Sub1_1148 = aClass285_1150.method2373(255, (byte) 0, 255, true, true);
		}
		if (aClass296_Sub39_Sub20_Sub1_1148.aBoolean6256) {
			return false;
		}
		Packet class296_sub17 = new Packet(aClass296_Sub39_Sub20_Sub1_1148.method2904(4));
		class296_sub17.pos = 5;
		int i_0_ = class296_sub17.g1();
		class296_sub17.pos += i_0_ * 72;
		byte[] is = new byte[-class296_sub17.pos + class296_sub17.data.length];
		class296_sub17.readBytes(is, 0, is.length);
		byte[] is_1_ = is;
		/*
		// TODO UPDATE SERVER RSA [THIS DISABLES IT]
		if (is_1_.length != 65)
			throw new RuntimeException();
		byte[] is_3_ = Class163.method1629((byte) -17, class296_sub17.buffer, -is.length + class296_sub17.position - 5, 5);
		for (int i_4_ = 0; i_4_ < 64; i_4_++) {
			if (is_3_[i_4_] != is_1_[i_4_ + 1])
				throw new RuntimeException();
		}*/
		if (i != 97) {
			method972((byte) 117);
		}
		aClass343_Sub1Array1144 = new Class343_Sub1[i_0_];
		aClass296_Sub17_1151 = class296_sub17;
		return true;
	}

	public static void method973(int i) {
		int i_5_ = 75 % ((-27 - i) / 32);
		PlayerUpdate.updatablePlayersIndexes = null;
	}

	final Class343_Sub1 method974(int i, byte i_6_, Js5DiskStore class168, Js5DiskStore class168_7_) {
		int i_8_ = 13 % ((i_6_ + 34) / 36);
		return method976(i, class168, true, (byte) -96, class168_7_);
	}

	final void method975(int i) {
		if (aClass343_Sub1Array1144 != null && i >= 65) {
			for (int i_9_ = 0; i_9_ < aClass343_Sub1Array1144.length; i_9_++) {
				if (aClass343_Sub1Array1144[i_9_] != null) {
					aClass343_Sub1Array1144[i_9_].method3646(-1);
				}
			}
			for (int i_10_ = 0; aClass343_Sub1Array1144.length > i_10_; i_10_++) {
				if (aClass343_Sub1Array1144[i_10_] != null) {
					aClass343_Sub1Array1144[i_10_].method3644(-1);
				}
			}
		}
	}

	Class111(Class285 class285, Class315 class315, BigInteger biginteger, BigInteger biginteger_11_) {
		aBigInteger1145 = biginteger_11_;
		aClass315_1146 = class315;
		aClass285_1150 = class285;
		aBigInteger1147 = biginteger;
		if (!aClass285_1150.method2371(23192)) {
			aClass296_Sub39_Sub20_Sub1_1148 = aClass285_1150.method2373(255, (byte) 0, 255, true, true);
		}
	}

	private final Class343_Sub1 method976(int i, Js5DiskStore class168, boolean bool, byte i_12_, Js5DiskStore class168_13_) {
		if (aClass296_Sub17_1151 == null) {
			throw new RuntimeException();
		}
		if (i < 0 || i >= aClass343_Sub1Array1144.length) {
			throw new RuntimeException();
		}

		if (aClass343_Sub1Array1144[i] != null) {
			return aClass343_Sub1Array1144[i];
		}
		aClass296_Sub17_1151.pos = i * 72 + 6;
		int i_14_ = aClass296_Sub17_1151.g4();
		int i_15_ = aClass296_Sub17_1151.g4();
		byte[] is = new byte[64];
		int i_16_ = 59 % ((35 - i_12_) / 54);
		aClass296_Sub17_1151.readBytes(is, 0, 64);
		Class343_Sub1 class343_sub1 = new Class343_Sub1(i, class168_13_, class168, aClass285_1150, aClass315_1146, i_14_, is, i_15_, bool);
		aClass343_Sub1Array1144[i] = class343_sub1;
		return class343_sub1;
	}
}
