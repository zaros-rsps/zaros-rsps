package net.zaros.client;

/* Class328 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class328 {
	static OutgoingPacket aClass311_2906;
	private Js5 aClass138_2907;
	static int anInt2908 = 0;
	private AdvancedMemoryCache aClass113_2909 = new AdvancedMemoryCache(64);
	int anInt2910;
	static Class81 aClass81_2911;
	static NodeDeque aClass155_2912;

	final void method3392(byte i, int i_0_) {
		synchronized (aClass113_2909) {
			aClass113_2909.clean(i_0_);
			if (i != 28)
				method3395((byte) -55);
		}
	}

	final void method3393(byte i) {
		synchronized (aClass113_2909) {
			aClass113_2909.clear();
			if (i >= -116)
				anInt2910 = 96;
		}
	}

	final Class135 method3394(byte i, int i_1_) {
		if (i >= -22)
			return null;
		Class135 class135;
		synchronized (aClass113_2909) {
			class135 = (Class135) aClass113_2909.get((long) i_1_);
		}
		if (class135 != null)
			return class135;
		byte[] is;
		synchronized (aClass138_2907) {
			is = aClass138_2907.getFile(47, i_1_);
		}
		class135 = new Class135();
		if (is != null)
			class135.method1410(new Packet(is), -1);
		synchronized (aClass113_2909) {
			aClass113_2909.put(class135, (long) i_1_);
		}
		return class135;
	}

	public static void method3395(byte i) {
		aClass311_2906 = null;
		aClass155_2912 = null;
		int i_2_ = 58 / ((i + 58) / 58);
		aClass81_2911 = null;
	}

	Class328(GameType class35, int i, Js5 class138) {
		aClass138_2907 = class138;
		if (aClass138_2907 != null)
			anInt2910 = aClass138_2907.getLastFileId(47);
		else
			anInt2910 = 0;
	}

	final void method3396(int i) {
		if (i != 18)
			anInt2908 = -114;
		synchronized (aClass113_2909) {
			aClass113_2909.clearSoftReferences();
		}
	}

	static {
		aClass311_2906 = new OutgoingPacket(25, 8);
		aClass81_2911 = new Class81("", 18);
		aClass155_2912 = new NodeDeque();
	}
}
