package net.zaros.client;

/* Class160 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class160 {
	int anInt1666;
	int anInt1667;
	static int anInt1668 = 1339;
	int anInt1669;
	int anInt1670;

	static final void method1618(int i) {
		if (i != -8783)
			method1619(75, -14, false, null, null);
		int i_0_ = 0;
		if (Class343_Sub1.aClass296_Sub50_5282 != null)
			i_0_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub10_5009.method435(118);
		if (i_0_ == 2) {
			int i_1_ = StaticMethods.anInt1838 > 800 ? 800 : StaticMethods.anInt1838;
			Class241.anInt2301 = i_1_;
			int i_2_ = Class152.anInt1568 <= 600 ? Class152.anInt1568 : 600;
			Class241_Sub2_Sub2.anInt5908 = (StaticMethods.anInt1838 - i_1_) / 2;
			Class384.anInt3254 = i_2_;
			CS2Stack.anInt2251 = 0;
		} else if (i_0_ == 1) {
			int i_3_ = StaticMethods.anInt1838 <= 1024 ? StaticMethods.anInt1838 : 1024;
			Class241_Sub2_Sub2.anInt5908 = (StaticMethods.anInt1838 - i_3_) / 2;
			Class241.anInt2301 = i_3_;
			int i_4_ = Class152.anInt1568 > 768 ? 768 : Class152.anInt1568;
			CS2Stack.anInt2251 = 0;
			Class384.anInt3254 = i_4_;
		} else {
			Class241_Sub2_Sub2.anInt5908 = 0;
			CS2Stack.anInt2251 = 0;
			Class241.anInt2301 = StaticMethods.anInt1838;
			Class384.anInt3254 = Class152.anInt1568;
		}
	}

	static final void method1619(int i, int i_5_, boolean bool, int[] is, Mobile class338_sub3_sub1_sub3) {
		if (class338_sub3_sub1_sub3.anIntArray6789 != null) {
			boolean bool_6_ = true;
			for (int i_7_ = 0; class338_sub3_sub1_sub3.anIntArray6789.length > i_7_; i_7_++) {
				if (class338_sub3_sub1_sub3.anIntArray6789[i_7_] != is[i_7_]) {
					bool_6_ = false;
					break;
				}
			}
			Animator class44 = class338_sub3_sub1_sub3.aClass44_6802;
			if (bool_6_ && class44.method570((byte) 40)) {
				Animation class43 = class338_sub3_sub1_sub3.aClass44_6802.method563(-1);
				int i_8_ = class43.anInt410;
				if (i_8_ == 1)
					class44.method564(i, (byte) -37);
				if (i_8_ == 2)
					class44.method554((byte) 18);
			}
		}
		boolean bool_9_ = true;
		for (int i_10_ = i_5_; is.length > i_10_; i_10_++) {
			if (is[i_10_] != -1)
				bool_9_ = false;
			if (class338_sub3_sub1_sub3.anIntArray6789 == null || class338_sub3_sub1_sub3.anIntArray6789[i_10_] == -1 || ((Class296_Sub51_Sub13.animationsLoader.getAnimation(is[i_10_], (byte) -124).anInt408) >= (Class296_Sub51_Sub13.animationsLoader.getAnimation(class338_sub3_sub1_sub3.anIntArray6789[i_10_], (byte) -124).anInt408))) {
				class338_sub3_sub1_sub3.anIntArray6789 = is;
				class338_sub3_sub1_sub3.aClass44_6802.method558(false, i);
				if (bool)
					class338_sub3_sub1_sub3.anInt6829 = class338_sub3_sub1_sub3.currentWayPoint;
			}
		}
		if (bool_9_) {
			class338_sub3_sub1_sub3.anIntArray6789 = is;
			class338_sub3_sub1_sub3.aClass44_6802.method558(false, i);
			if (bool)
				class338_sub3_sub1_sub3.anInt6829 = class338_sub3_sub1_sub3.currentWayPoint;
		}
	}

	public Class160() {
		/* empty */
	}
}
