package net.zaros.client;
public class NPCUpdate {

	static final void processGNP() {
		Class104.loopCycle++;
		Class318.removedNpcsCount = 0;
		Class205_Sub2.updatableNpcsCount = 0;
		NPCUpdate.processNpcs();
		NPCUpdate.addNewNpcs();
		NPCUpdate.parseFlagUpdates(false);
		boolean bool_5_ = false;
		for (int i = 0; i < Class318.removedNpcsCount; i++) {
			int i_6_ = Class368_Sub8.npcsToRemoveIndexes[i];
			NPCNode class296_sub7 = ((NPCNode) Class41_Sub18.localNpcs.get((long) i_6_));
			NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
			if (class338_sub3_sub1_sub3_sub2.lastUpdate != Class104.loopCycle) {
				if (Class318.aBoolean2814 && Class296_Sub38.method2774((byte) -64, i_6_))
					Class41_Sub9.method427((byte) -110);
				if (class338_sub3_sub1_sub3_sub2.definition.method1493(true))
					Class296_Sub29_Sub2.method2699((byte) -81, class338_sub3_sub1_sub3_sub2);
				class338_sub3_sub1_sub3_sub2.setDefinition(null);
				class296_sub7.unlink();
				bool_5_ = true;
			}
		}
		if (bool_5_) {
			Class368_Sub23.npcNodeCount = Class41_Sub18.localNpcs.size((byte) -4);
			Class41_Sub18.localNpcs.toArray(5125, Class241.npcNodes);
		}
		if (Class296_Sub45_Sub2.aClass204_6277.protSize != (Class296_Sub45_Sub2.aClass204_6277.in_stream.pos))
			throw new RuntimeException("gnp1 pos:" + (Class296_Sub45_Sub2.aClass204_6277.in_stream.pos) + " psize:" + (Class296_Sub45_Sub2.aClass204_6277.protSize));
		for (int i = 0; i < Class367.npcsCount; i++) {
			if (Class41_Sub18.localNpcs.get((long) ReferenceTable.npcIndexes[i]) == null)
				throw new RuntimeException("gnp2 pos:" + i + " size:" + Class367.npcsCount);
		}
		if (Class368_Sub23.npcNodeCount - Class367.npcsCount != 0)
			throw new RuntimeException("gnp3 mis:" + (-Class367.npcsCount + Class368_Sub23.npcNodeCount));
		for (int i = 0; i < Class368_Sub23.npcNodeCount; i++) {
			if (Class104.loopCycle != (Class241.npcNodes[i].value.lastUpdate))
				throw new RuntimeException("gnp4 uk:" + (Class241.npcNodes[i].value.index));
		}
	}

	static final void processNpcs() {
		ByteStream in = Class296_Sub45_Sub2.aClass204_6277.in_stream;
		in.prepareBITStream();
		int npcsSize = in.readBITS(8);
		if (npcsSize < Class367.npcsCount) {
			for (int i_13_ = npcsSize; i_13_ < Class367.npcsCount; i_13_++)
				Class368_Sub8.npcsToRemoveIndexes[Class318.removedNpcsCount++] = ReferenceTable.npcIndexes[i_13_];
		}
		if (Class367.npcsCount < npcsSize)
			throw new RuntimeException("gnpov1");
		Class367.npcsCount = 0;
		for (int i = 0; i < npcsSize; i++) {
			int npcIndex = ReferenceTable.npcIndexes[i];
			NPC localNpc = (((NPCNode) Class41_Sub18.localNpcs.get((long) npcIndex)).value);
			int needsUpdating = in.readBITS(1);
			if (needsUpdating == 0) {
				ReferenceTable.npcIndexes[Class367.npcsCount++] = npcIndex;
				localNpc.lastUpdate = Class104.loopCycle;
			} else {
				int updateType = in.readBITS(2);
				if (updateType == 0) { // only flag update required
					ReferenceTable.npcIndexes[Class367.npcsCount++] = npcIndex;
					localNpc.lastUpdate = Class104.loopCycle;
					Class205_Sub3.npcsToUpdate[Class205_Sub2.updatableNpcsCount++] = npcIndex;
				} else if (updateType == 1) { // walking update required
					ReferenceTable.npcIndexes[Class367.npcsCount++] = npcIndex;
					localNpc.lastUpdate = Class104.loopCycle;
					int type = in.readBITS(3);
					localNpc.walk(1, type);
					int updateRequired = in.readBITS(1);
					if (updateRequired == 1)
						Class205_Sub3.npcsToUpdate[Class205_Sub2.updatableNpcsCount++] = npcIndex;
				} else if (updateType == 2) { // running or backwards walking update required
					ReferenceTable.npcIndexes[Class367.npcsCount++] = npcIndex;
					localNpc.lastUpdate = Class104.loopCycle;
					if (in.readBITS(1) != 1) {
						int type = in.readBITS(3);
						localNpc.walk(0, type);
					} else {
						int type0 = in.readBITS(3);
						localNpc.walk(2, type0);
						int type1 = in.readBITS(3);
						localNpc.walk(2, type1);
					}
					int updateRequired = in.readBITS(1);
					if (updateRequired == 1)
						Class205_Sub3.npcsToUpdate[Class205_Sub2.updatableNpcsCount++] = npcIndex;
				} else if (updateType == 3) // remove or teleport
					Class368_Sub8.npcsToRemoveIndexes[Class318.removedNpcsCount++] = npcIndex;
			}
		}
	}

	static final void addNewNpcs() {
		ByteStream in = Class296_Sub45_Sub2.aClass204_6277.in_stream;
		while (in.bitOffset((Class296_Sub45_Sub2.aClass204_6277.protSize)) >= 15) {
			int index = in.readBITS(15);
			if (index == 32767)
				break;
			boolean newNpc = false;
			NPCNode node = ((NPCNode) Class41_Sub18.localNpcs.get((long) index));
			if (node == null) {
				NPC n = new NPC();
				n.index = index;
				node = new NPCNode(n);
				Class41_Sub18.localNpcs.put((long) index, node);
				newNpc = true;
				Class241.npcNodes[Class368_Sub23.npcNodeCount++] = node;
			}
			NPC n = node.value;
			ReferenceTable.npcIndexes[Class367.npcsCount++] = index;
			n.lastUpdate = Class104.loopCycle;
			if (n.definition != null && n.definition.method1493(true))
				Class296_Sub29_Sub2.method2699((byte) -98, n);
			int updateRequired = in.readBITS(1);
			if (updateRequired == 1)
				Class205_Sub3.npcsToUpdate[Class205_Sub2.updatableNpcsCount++] = index;
			int posZ = in.readBITS(2);
			int posY = in.readBITS(5);
			if (posY > 15)
				posY -= 32;
			int clearQueue = in.readBITS(1);
			int direction = ((in.readBITS(3) + 4 & ~0x46fffff8) << 11);
			int posX = in.readBITS(5);
			n.setDefinition((Class352.npcDefinitionLoader.getDefinition(in.readBITS(15))));
			if (posX > 15)
				posX -= 32;
			n.setSize(n.definition.size);
			n.anInt6815 = n.definition.anInt1459 << 3;
			if (newNpc)
				n.method3517(true, true, direction);
			n.updatePosition((Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0]) + posX, clearQueue == 1, 90, n.getSize(), posZ, posY + (Class296_Sub51_Sub11.localPlayer.waypointQueueX[0]));
			if (n.definition.method1493(true))
				Class368_Sub20.method3862(null, n.z, n, false, 0, null, n.waypointQueueX[0], n.wayPointQueueY[0]);
		}
		in.endBITStream();
	}

	static final void parseFlagUpdates(boolean bool) {
		ByteStream in = Class296_Sub45_Sub2.aClass204_6277.in_stream;
		for (int i = 0; Class205_Sub2.updatableNpcsCount > i; i++) {
			int npcIndex = Class205_Sub3.npcsToUpdate[i];
			NPC n = (((NPCNode) Class41_Sub18.localNpcs.get((long) npcIndex)).value);
			int flag = in.g1();
			if ((flag & 0x2) != 0) // expandbyte
				flag += in.g1() << 8;
			if ((flag & 0x2000) != 0) // expandshort
				flag += in.g1() << 16;
			if ((flag & 0x4000) != 0) { // forcewalk
				n.anInt6813 = in.g1b();
				n.anInt6811 = in.readSignedByteA();
				n.anInt6821 = in.readSignedByteA();
				n.anInt6805 = in.readSignedByteS();
				n.anInt6809 = in.readUnsignedShortA() + Class29.anInt307;
				n.anInt6807 = (in.readUnsignedLEShortA() + Class29.anInt307);
				n.anInt6819 = in.readUnsignedByteA();
				n.anInt6829 = 0;
				n.anInt6813 += n.waypointQueueX[0];
				n.anInt6811 += n.wayPointQueueY[0];
				n.anInt6821 += n.waypointQueueX[0];
				n.currentWayPoint = 1;
				n.anInt6805 += n.wayPointQueueY[0];
			}
			if ((flag & 0x8) != 0) { // faceto
				n.interactingWithIndex = in.readUnsignedLEShortA();
				if (n.interactingWithIndex == 65535)
					n.interactingWithIndex = -1;
			}
			if ((flag & 0x1000) != 0) { // gfx1
				int i_15_ = in.readUnsignedLEShortA();
				if (i_15_ == 65535)
					i_15_ = -1;
				int i_16_ = in.readLEInt();
				int i_17_ = in.readUnsignedByteA();
				int i_18_ = i_17_ & 0x7;
				int i_19_ = (i_17_ & 0x79) >> 3;
				if (i_19_ == 15)
					i_19_ = -1;
				boolean bool_20_ = (i_17_ & 0x91) >> 7 == 1;
				n.playGraphic(i_16_, 1, i_18_, i_19_, bool_20_, 65535, i_15_);
			}
			if ((flag & 0x800) != 0) {
				int i_21_ = in.readUnsignedByteC();
				int[] is = new int[i_21_];
				int[] is_22_ = new int[i_21_];
				for (int i_23_ = 0; i_23_ < i_21_; i_23_++) {
					int i_24_ = in.readUnsignedLEShortA();
					if ((i_24_ & 0xc000) != 49152)
						is[i_23_] = i_24_;
					else {
						int i_25_ = in.readUnsignedLEShort();
						is[i_23_] = i_25_ | i_24_ << 16;
					}
					is_22_[i_23_] = in.readUnsignedShortA();
				}
				n.method3512((byte) -119, is, is_22_);
			}
			if ((flag & 0x20000) != 0) { // set combat
				n.combatLevel = in.readUnsignedLEShort();
				if (n.combatLevel == 65535)
					n.combatLevel = (n.definition.combatLevel);
			}
			if ((flag & 0x80000) != 0) { // gfx2
				int i_26_ = in.g2();
				if (i_26_ == 65535)
					i_26_ = -1;
				int i_27_ = in.readInt_v1();
				int i_28_ = in.readUnsignedByteS();
				int i_29_ = i_28_ & 0x7;
				int i_30_ = (i_28_ & 0x7f) >> 3;
				if (i_30_ == 15)
					i_30_ = -1;
				boolean bool_31_ = (i_28_ & 0x80) >> 7 == 1;
				n.playGraphic(i_27_, 2, i_29_, i_30_, bool_31_, 65535, i_26_);
			}
			if ((flag & 0x1) != 0) { // animation
				int[] data = new int[4];
				for (int a = 0; a < 4; a++) {
					data[a] = in.readUnsignedLEShortA();
					if (data[a] == 65535)
						data[a] = -1;
				}
				int delay = in.readUnsignedByteA();
				Class160.method1619(delay, 0, true, data, n);
			}
			if ((flag & 0x10) != 0) { // hits
				int hitsCount = in.readUnsignedByteA();
				if (hitsCount > 0) {
					for (int a = 0; a < hitsCount; a++) {
						int soakType = -1;
						int hitDamage = -1;
						int hitType = in.readSmart();
						int soakDamage = -1;
						if (hitType != 32767) {
							if (hitType == 32766)
								hitType = -1;
							else
								hitDamage = in.readSmart();
						} else {
							hitType = in.readSmart();
							hitDamage = in.readSmart();
							soakType = in.readSmart();
							soakDamage = in.readSmart();
						}
						int hitDelay = in.readSmart();
						int hpbarRatio = in.readUnsignedByteA();
						n.applyHit(hitType, soakType, hitDamage, soakDamage, hitDelay, hpbarRatio, Class29.anInt307);
					}
				}
			}
			if ((flag & 0x200) != 0) { // secondary hp bar
				int i_42_ = in.readUnsignedShortA();
				n.anInt6785 = in.readUnsignedByteA();
				n.anInt6765 = in.readUnsignedByteS();
				n.anInt6780 = i_42_ & 0x7fff;
				n.aBoolean6764 = (i_42_ & 0x8000) != 0;
				n.anInt6799 = (n.anInt6780 + (Class29.anInt307 + n.anInt6785));
			}
			if ((flag & 0x10000) != 0) { // gfx3
				int i_43_ = in.readUnsignedLEShortA();
				int i_44_ = in.readLEInt();
				if (i_43_ == 65535)
					i_43_ = -1;
				int i_45_ = in.readUnsignedByteA();
				int i_46_ = i_45_ & 0x7;
				int i_47_ = i_45_ >> 3 & 0xf;
				if (i_47_ == 15)
					i_47_ = -1;
				boolean bool_48_ = (i_45_ >> 7 & 0x1) == 1;
				n.playGraphic(i_44_, 3, i_46_, i_47_, bool_48_, 65535, i_43_);
			}
			if ((flag & 0x100) != 0) {
				n.aByte6808 = in.g1b();
				n.aByte6818 = in.readSignedByteS();
				n.aByte6822 = in.g1b();
				n.aByte6806 = (byte) in.g1();
				n.anInt6820 = Class29.anInt307 + in.readUnsignedLEShort();
				n.anInt6803 = (Class29.anInt307 + in.g2());
			}
			if ((flag & 0x8000) != 0) {
				int i_49_ = in.g1();
				int[] is = new int[i_49_];
				int[] is_50_ = new int[i_49_];
				int[] is_51_ = new int[i_49_];
				for (int i_52_ = 0; i_52_ < i_49_; i_52_++) {
					int i_53_ = in.readUnsignedLEShort();
					if (i_53_ == 65535)
						i_53_ = -1;
					is[i_52_] = i_53_;
					is_50_[i_52_] = in.readUnsignedByteC();
					is_51_[i_52_] = in.readUnsignedShortA();
				}
				Class16_Sub2.method243(is, is_50_, is_51_, n, true);
			}
			if ((flag & 0x4) != 0) { // gfx4
				int i_54_ = in.readUnsignedLEShortA();
				if (i_54_ == 65535)
					i_54_ = -1;
				int i_55_ = in.readLEInt();
				int i_56_ = in.readUnsignedByteC();
				int i_57_ = i_56_ & 0x7;
				int i_58_ = i_56_ >> 3 & 0xf;
				if (i_58_ == 15)
					i_58_ = -1;
				boolean bool_59_ = (i_56_ >> 7 & 0x1) == 1;
				n.playGraphic(i_55_, 0, i_57_, i_58_, bool_59_, 65535, i_54_);
			}
			if ((flag & 0x20) != 0) { // face location
				n.faceX = in.readUnsignedLEShortA();
				n.faceY = in.readUnsignedLEShortA();
			}
			if ((flag & 0x40000) != 0) {
				int i_60_ = (n.definition.if_models).length;
				int i_61_ = 0;
				if (n.definition.modifiedColours != null)
					i_61_ = (n.definition.modifiedColours).length;
				if (n.definition.aShortArray1491 != null)
					i_61_ = (n.definition.aShortArray1491).length;
				int i_62_ = 0;
				int i_63_ = in.readUnsignedByteA();
				if ((i_63_ & 0x1) != 1) {
					int[] is = null;
					if ((i_63_ & 0x2) == 2) {
						is = new int[i_60_];
						for (int i_64_ = 0; i_64_ < i_60_; i_64_++)
							is[i_64_] = in.readUnsignedLEShort();
					}
					short[] is_65_ = null;
					if ((i_63_ & 0x4) == 4) {
						is_65_ = new short[i_61_];
						for (int i_66_ = 0; i_66_ < i_61_; i_66_++)
							is_65_[i_66_] = (short) in.readUnsignedLEShortA();
					}
					short[] is_67_ = null;
					if ((i_63_ & 0x8) == 8) {
						is_67_ = new short[i_62_];
						for (int i_68_ = 0; i_68_ < i_62_; i_68_++)
							is_67_[i_68_] = (short) in.readUnsignedShortA();
					}
					long l = ((long) npcIndex | ((long) n.anInt6899++ << 32));
					new Class286(l, is, is_65_, is_67_);
				}
			}
			if ((flag & 0x400) != 0) {
				int i_69_ = (n.definition.models).length;
				int i_70_ = 0;
				if (n.definition.modifiedColours != null)
					i_70_ = (n.definition.modifiedColours).length;
				int i_71_ = 0;
				if (n.definition.aShortArray1491 != null)
					i_71_ = (n.definition.aShortArray1491).length;
				int i_72_ = in.readUnsignedByteC();
				if ((i_72_ & 0x1) != 1) {
					int[] is = null;
					if ((i_72_ & 0x2) == 2) {
						is = new int[i_69_];
						for (int i_73_ = 0; i_69_ > i_73_; i_73_++)
							is[i_73_] = in.readUnsignedLEShort();
					}
					short[] is_74_ = null;
					if ((i_72_ & 0x4) == 4) {
						is_74_ = new short[i_70_];
						for (int i_75_ = 0; i_70_ > i_75_; i_75_++)
							is_74_[i_75_] = (short) in.g2();
					}
					short[] is_76_ = null;
					if ((i_72_ & 0x8) == 8) {
						is_76_ = new short[i_71_];
						for (int i_77_ = 0; i_71_ > i_77_; i_77_++)
							is_76_[i_77_] = (short) in.readUnsignedShortA();
					}
					long l = (((long) n.anInt6901++ << 32) | (long) npcIndex);
					n.aClass286_6900 = new Class286(l, is, is_74_, is_76_);
				} else
					n.aClass286_6900 = null;
			}
			if ((flag & 0x40) != 0) { // transform
				if (n.definition.method1493(true))
					Class296_Sub29_Sub2.method2699((byte) 34, n);
				n.setDefinition(Class352.npcDefinitionLoader.getDefinition(in.readUnsignedShortA()));
				n.setSize(n.definition.size);
				n.anInt6815 = (n.definition.anInt1459 << 3);
				if (n.definition.method1493(true))
					Class368_Sub20.method3862(null, n.z, n, bool, 0, null, n.waypointQueueX[0], n.wayPointQueueY[0]);
			}
			if ((flag & 0x100000) != 0) { // setname
				n.name = in.gstr();
				if ("".equals(n.name) || (n.name.equals(n.definition.name)))
					n.name = (n.definition.name);
			}
			if ((flag & 0x80) != 0) // speak
				n.method3536(in.gstr(), 0, 0, 117);
		}
	}

}
