package net.zaros.client;
/* Class326 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Font;
import java.math.BigInteger;

final class Class326 {
	boolean aBoolean2872;
	static Class326 aClass326_2873 = new Class326(0, TranslatableString.aClass120_1212, TranslatableString.aClass120_1212, 0, 1);
	private int anInt2874;
	int anInt2875;
	int anInt2876;
	static Class326 aClass326_2877 = new Class326(1, TranslatableString.aClass120_1212, 2);
	TranslatableString aClass120_2878;
	TranslatableString aClass120_2879;
	static Class326 aClass326_2880 = new Class326(2, TranslatableString.aClass120_1212, TranslatableString.aClass120_1212, 2, 3);
	boolean aBoolean2881;
	static Class326 aClass326_2882 = new Class326(3, TranslatableString.aClass120_1212, 3);
	static Class326 aClass326_2883 = new Class326(4, TranslatableString.aClass120_1212, TranslatableString.aClass120_1212, 3, 4);
	static Class326 aClass326_2884 = new Class326(5, TranslatableString.aClass120_1212, 4);
	static Class326 aClass326_2885 = new Class326(6, TranslatableString.aClass120_1212, 4);
	static Class326 aClass326_2886 = new Class326(7, TranslatableString.aClass120_1212, TranslatableString.aClass120_1212, 4, 5);
	static Class326 aClass326_2887 = new Class326(8, TranslatableString.aClass120_1212, TranslatableString.aClass120_1212, 5, 98, true, true);
	static Class326 aClass326_2888 = new Class326(9, TranslatableString.aClass120_1212, 99);
	static Class326 aClass326_2889 = new Class326(10, TranslatableString.aClass120_1212, 100);
	static Class326 aClass326_2890 = new Class326(11, TranslatableString.aClass120_1213, TranslatableString.aClass120_1213, 0, 92, true, true);
	static Class326 aClass326_2891 = new Class326(12, TranslatableString.aClass120_1213, TranslatableString.aClass120_1213, 92, 92);
	private static Class326 aClass326_2892 = new Class326(13, TranslatableString.aClass120_1213, TranslatableString.aClass120_1213, 92, 93);
	static Class326 aClass326_2893 = new Class326(14, TranslatableString.aClass120_1213, TranslatableString.aClass120_1213, 94, 95);
	static Class326 aClass326_2894 = new Class326(15, TranslatableString.aClass120_1213, TranslatableString.aClass120_1213, 96, 97);
	static Class326 aClass326_2895 = new Class326(16, TranslatableString.aClass120_1213, 97);
	static Class326 aClass326_2896 = new Class326(17, TranslatableString.aClass120_1213, 97);
	static Class326 aClass326_2897 = new Class326(18, TranslatableString.aClass120_1213, 100);
	static Class326 aClass326_2898 = new Class326(19, TranslatableString.aClass120_1213, 100);
	static Class326 aClass326_2899 = new Class326(20, TranslatableString.aClass120_1213, 100);
	static Js5 fs16;
	static IncomingPacket aClass231_2901 = new IncomingPacket(96, -1);
	static BigInteger aBigInteger2902 = new BigInteger("65537");
	static Font aFont2903;
	static IncomingPacket aClass231_2904 = new IncomingPacket(90, -1);

	private Class326(int i, TranslatableString class120, int i_0_) {
		this(i, class120, class120, i_0_, i_0_, true, false);
	}

	static final Class326[] method3385(int i) {
		if (i != 100)
			aClass326_2880 = null;
		return (new Class326[]{aClass326_2873, aClass326_2877, aClass326_2880, aClass326_2882, aClass326_2883, aClass326_2884, aClass326_2885, aClass326_2886, aClass326_2887, aClass326_2888, aClass326_2889, aClass326_2890, aClass326_2891, aClass326_2892, aClass326_2893, aClass326_2894, aClass326_2895, aClass326_2896, aClass326_2897, aClass326_2898, aClass326_2899});
	}

	final int method3386(byte i) {
		if (i != -44)
			aClass326_2892 = null;
		return anInt2874;
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	public static void method3387(int i) {
		aClass326_2895 = null;
		aClass326_2887 = null;
		aClass326_2873 = null;
		aClass326_2880 = null;
		fs16 = null;
		aClass326_2891 = null;
		if (i == 90) {
			aClass231_2904 = null;
			aClass326_2897 = null;
			aClass326_2896 = null;
			aClass326_2884 = null;
			aClass326_2877 = null;
			aClass326_2892 = null;
			aClass326_2893 = null;
			aClass326_2889 = null;
			aClass326_2890 = null;
			aClass326_2888 = null;
			aClass326_2886 = null;
			aClass326_2885 = null;
			aClass326_2882 = null;
			aClass326_2894 = null;
			aClass231_2901 = null;
			aFont2903 = null;
			aClass326_2898 = null;
			aClass326_2883 = null;
			aBigInteger2902 = null;
			aClass326_2899 = null;
		}
	}

	private Class326(int i, TranslatableString class120, TranslatableString class120_1_, int i_2_, int i_3_) {
		this(i, class120, class120_1_, i_2_, i_3_, true, false);
	}

	private Class326(int i, TranslatableString class120, TranslatableString class120_4_, int i_5_, int i_6_, boolean bool, boolean bool_7_) {
		aBoolean2881 = bool_7_;
		anInt2876 = i_6_;
		aClass120_2879 = class120;
		anInt2874 = i;
		aBoolean2872 = bool;
		aClass120_2878 = class120_4_;
		anInt2875 = i_5_;
	}
}
