package net.zaros.client;

/* Class55_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class55_Sub1 extends Class55 {
	private byte[][] aByteArrayArray3863;
	private int[] anIntArray3864;
	private ha_Sub2 aHa_Sub2_3865;
	private int[] anIntArray3866;
	private int[] anIntArray3867;
	private int[] anIntArray3868;
	private int[] anIntArray3869;

	private final void method655(byte[] is, int[] is_0_, int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_,
			int i_6_) {
		int i_7_ = -(i_3_ >> 2);
		i_3_ = -(i_3_ & 0x3);
		for (int i_8_ = -i_4_; i_8_ < 0; i_8_++) {
			for (int i_9_ = i_7_; i_9_ < 0; i_9_++) {
				if (is[i_1_++] != 0)
					is_0_[i_2_++] = i;
				else
					i_2_++;
				if (is[i_1_++] != 0)
					is_0_[i_2_++] = i;
				else
					i_2_++;
				if (is[i_1_++] != 0)
					is_0_[i_2_++] = i;
				else
					i_2_++;
				if (is[i_1_++] != 0)
					is_0_[i_2_++] = i;
				else
					i_2_++;
			}
			for (int i_10_ = i_3_; i_10_ < 0; i_10_++) {
				if (is[i_1_++] != 0)
					is_0_[i_2_++] = i;
				else
					i_2_++;
			}
			i_2_ += i_5_;
			i_1_ += i_6_;
		}
	}

	private final void method656(byte[] is, int[] is_11_, int[] is_12_, int i, int i_13_, int i_14_, int i_15_,
			int i_16_, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_, aa var_aa, int i_22_, int i_23_) {
		aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
		int[] is_24_ = var_aa_Sub2.anIntArray3726;
		int[] is_25_ = var_aa_Sub2.anIntArray3729;
		int i_26_ = i_19_ - aHa_Sub2_3865.anInt4079;
		int i_27_ = i_20_;
		if (i_23_ > i_27_) {
			i_27_ = i_23_;
			i_14_ += (i_23_ - i_20_) * aHa_Sub2_3865.anInt4084;
			i_13_ += (i_23_ - i_20_) * i_21_;
		}
		int i_28_ = (i_23_ + is_24_.length < i_20_ + i_16_ ? i_23_ + is_24_.length : i_20_ + i_16_);
		boolean bool = false;
		for (int i_29_ = i_27_; i_29_ < i_28_; i_29_++) {
			int i_30_ = is_24_[i_29_ - i_23_] + i_22_;
			int i_31_ = is_25_[i_29_ - i_23_];
			int i_32_ = i_15_;
			if (i_26_ > i_30_) {
				int i_33_ = i_26_ - i_30_;
				if (i_33_ >= i_31_) {
					i_13_ += i_15_ + i_18_;
					i_14_ += i_15_ + i_17_;
					continue;
				}
				i_31_ -= i_33_;
			} else {
				int i_34_ = i_30_ - i_26_;
				if (i_34_ >= i_15_) {
					i_13_ += i_15_ + i_18_;
					i_14_ += i_15_ + i_17_;
					continue;
				}
				i_13_ += i_34_;
				i_32_ -= i_34_;
				i_14_ += i_34_;
			}
			int i_35_ = 0;
			if (i_32_ < i_31_)
				i_31_ = i_32_;
			else
				i_35_ = i_32_ - i_31_;
			for (int i_36_ = -i_31_; i_36_ < 0; i_36_++) {
				int i_37_;
				if ((i_37_ = is[i_13_++]) != 0)
					is_11_[i_14_++] = is_12_[i_37_ & 0xff];
				else
					i_14_++;
			}
			i_13_ += i_35_ + i_18_;
			i_14_ += i_35_ + i_17_;
		}
	}

	private final void method657(byte[] is, int[] is_38_, int[] is_39_, int i, int i_40_, int i_41_, int i_42_,
			int i_43_, int i_44_) {
		int i_45_ = -(i_41_ >> 2);
		i_41_ = -(i_41_ & 0x3);
		boolean bool = false;
		for (int i_46_ = -i_42_; i_46_ < 0; i_46_++) {
			for (int i_47_ = i_45_; i_47_ < 0; i_47_++) {
				int i_48_;
				if ((i_48_ = is[i++]) != 0)
					is_38_[i_40_++] = is_39_[i_48_ & 0xff];
				else
					i_40_++;
				if ((i_48_ = is[i++]) != 0)
					is_38_[i_40_++] = is_39_[i_48_ & 0xff];
				else
					i_40_++;
				if ((i_48_ = is[i++]) != 0)
					is_38_[i_40_++] = is_39_[i_48_ & 0xff];
				else
					i_40_++;
				if ((i_48_ = is[i++]) != 0)
					is_38_[i_40_++] = is_39_[i_48_ & 0xff];
				else
					i_40_++;
			}
			for (int i_49_ = i_41_; i_49_ < 0; i_49_++) {
				int i_50_;
				if ((i_50_ = is[i++]) != 0)
					is_38_[i_40_++] = is_39_[i_50_ & 0xff];
				else
					i_40_++;
			}
			i_40_ += i_43_;
			i += i_44_;
		}
	}

	private final void method658(byte[] is, int[] is_51_, int i, int i_52_, int i_53_, int i_54_, int i_55_, int i_56_,
			int i_57_, int i_58_, int i_59_, int i_60_, aa var_aa, int i_61_, int i_62_) {
		aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
		int[] is_63_ = var_aa_Sub2.anIntArray3726;
		int[] is_64_ = var_aa_Sub2.anIntArray3729;
		int i_65_ = i_58_ - aHa_Sub2_3865.anInt4079;
		int i_66_ = i_59_;
		if (i_62_ > i_66_) {
			i_66_ = i_62_;
			i_53_ += (i_62_ - i_59_) * aHa_Sub2_3865.anInt4084;
			i_52_ += (i_62_ - i_59_) * i_60_;
		}
		int i_67_ = (i_62_ + is_63_.length < i_59_ + i_55_ ? i_62_ + is_63_.length : i_59_ + i_55_);
		for (int i_68_ = i_66_; i_68_ < i_67_; i_68_++) {
			int i_69_ = is_63_[i_68_ - i_62_] + i_61_;
			int i_70_ = is_64_[i_68_ - i_62_];
			int i_71_ = i_54_;
			if (i_65_ > i_69_) {
				int i_72_ = i_65_ - i_69_;
				if (i_72_ >= i_70_) {
					i_52_ += i_54_ + i_57_;
					i_53_ += i_54_ + i_56_;
					continue;
				}
				i_70_ -= i_72_;
			} else {
				int i_73_ = i_69_ - i_65_;
				if (i_73_ >= i_54_) {
					i_52_ += i_54_ + i_57_;
					i_53_ += i_54_ + i_56_;
					continue;
				}
				i_52_ += i_73_;
				i_71_ -= i_73_;
				i_53_ += i_73_;
			}
			int i_74_ = 0;
			if (i_71_ < i_70_)
				i_70_ = i_71_;
			else
				i_74_ = i_71_ - i_70_;
			for (int i_75_ = -i_70_; i_75_ < 0; i_75_++) {
				if (is[i_52_++] != 0)
					is_51_[i_53_++] = i;
				else
					i_53_++;
			}
			i_52_ += i_74_ + i_52_;
			i_53_ += i_74_ + i_56_;
		}
	}

	Class55_Sub1(ha_Sub2 var_ha_Sub2, Class92 class92, Class186[] class186s, int[] is, int[] is_76_) {
		super(var_ha_Sub2, class92);
		aHa_Sub2_3865 = var_ha_Sub2;
		aHa_Sub2_3865 = var_ha_Sub2;
		anIntArray3866 = is;
		anIntArray3867 = is_76_;
		aByteArrayArray3863 = new byte[class186s.length][];
		anIntArray3869 = new int[class186s.length];
		anIntArray3868 = new int[class186s.length];
		for (int i = 0; i < class186s.length; i++) {
			aByteArrayArray3863[i] = class186s[i].aByteArray1901;
			anIntArray3869[i] = class186s[i].anInt1906;
			anIntArray3868[i] = class186s[i].anInt1903;
		}
		anIntArray3864 = class186s[0].anIntArray1900;
	}

	public void a(char c, int i, int i_77_, int i_78_, boolean bool, aa var_aa, int i_79_, int i_80_) {
		if (var_aa == null)
			fa(c, i, i_77_, i_78_, bool);
		else {
			i += anIntArray3868[c];
			i_77_ += anIntArray3869[c];
			int i_81_ = anIntArray3866[c];
			int i_82_ = anIntArray3867[c];
			int i_83_ = aHa_Sub2_3865.anInt4084;
			int i_84_ = i + i_77_ * i_83_;
			int i_85_ = i_83_ - i_81_;
			int i_86_ = 0;
			int i_87_ = 0;
			if (i_77_ < aHa_Sub2_3865.anInt4085) {
				int i_88_ = aHa_Sub2_3865.anInt4085 - i_77_;
				i_82_ -= i_88_;
				i_77_ = aHa_Sub2_3865.anInt4085;
				i_87_ += i_88_ * i_81_;
				i_84_ += i_88_ * i_83_;
			}
			if (i_77_ + i_82_ > aHa_Sub2_3865.anInt4078)
				i_82_ -= i_77_ + i_82_ - aHa_Sub2_3865.anInt4078;
			if (i < aHa_Sub2_3865.anInt4079) {
				int i_89_ = aHa_Sub2_3865.anInt4079 - i;
				i_81_ -= i_89_;
				i = aHa_Sub2_3865.anInt4079;
				i_87_ += i_89_;
				i_84_ += i_89_;
				i_86_ += i_89_;
				i_85_ += i_89_;
			}
			if (i + i_81_ > aHa_Sub2_3865.anInt4104) {
				int i_90_ = i + i_81_ - aHa_Sub2_3865.anInt4104;
				i_81_ -= i_90_;
				i_86_ += i_90_;
				i_85_ += i_90_;
			}
			if (i_81_ > 0 && i_82_ > 0) {
				if (bool)
					method658(aByteArrayArray3863[c], aHa_Sub2_3865.anIntArray4108, i_78_, i_87_, i_84_, i_81_, i_82_,
							i_85_, i_86_, i, i_77_, anIntArray3866[c], var_aa, i_79_, i_80_);
				else
					method656(aByteArrayArray3863[c], aHa_Sub2_3865.anIntArray4108, anIntArray3864, i_78_, i_87_, i_84_,
							i_81_, i_82_, i_85_, i_86_, i, i_77_, anIntArray3866[c], var_aa, i_79_, i_80_);
			}
		}
	}

	public void fa(char c, int i, int i_91_, int i_92_, boolean bool) {
		i += anIntArray3868[c];
		i_91_ += anIntArray3869[c];
		int i_93_ = anIntArray3866[c];
		int i_94_ = anIntArray3867[c];
		int i_95_ = aHa_Sub2_3865.anInt4084;
		int i_96_ = i + i_91_ * i_95_;
		int i_97_ = i_95_ - i_93_;
		int i_98_ = 0;
		int i_99_ = 0;
		if (i_91_ < aHa_Sub2_3865.anInt4085) {
			int i_100_ = aHa_Sub2_3865.anInt4085 - i_91_;
			i_94_ -= i_100_;
			i_91_ = aHa_Sub2_3865.anInt4085;
			i_99_ += i_100_ * i_93_;
			i_96_ += i_100_ * i_95_;
		}
		if (i_91_ + i_94_ > aHa_Sub2_3865.anInt4078)
			i_94_ -= i_91_ + i_94_ - aHa_Sub2_3865.anInt4078;
		if (i < aHa_Sub2_3865.anInt4079) {
			int i_101_ = aHa_Sub2_3865.anInt4079 - i;
			i_93_ -= i_101_;
			i = aHa_Sub2_3865.anInt4079;
			i_99_ += i_101_;
			i_96_ += i_101_;
			i_98_ += i_101_;
			i_97_ += i_101_;
		}
		if (i + i_93_ > aHa_Sub2_3865.anInt4104) {
			int i_102_ = i + i_93_ - aHa_Sub2_3865.anInt4104;
			i_93_ -= i_102_;
			i_98_ += i_102_;
			i_97_ += i_102_;
		}
		if (i_93_ > 0 && i_94_ > 0) {
			if (bool)
				method655(aByteArrayArray3863[c], aHa_Sub2_3865.anIntArray4108, i_92_, i_99_, i_96_, i_93_, i_94_,
						i_97_, i_98_);
			else
				method657(aByteArrayArray3863[c], aHa_Sub2_3865.anIntArray4108, anIntArray3864, i_99_, i_96_, i_93_,
						i_94_, i_97_, i_98_);
		}
	}
}
