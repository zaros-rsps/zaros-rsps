package net.zaros.client;
import jaggl.OpenGL;

final class Class296_Sub39_Sub4 extends Queuable implements Interface18 {
	private int anInt5733;
	private int anInt5734 = -1;
	static int[][] anIntArrayArray5735 = {{6, 6}, {6, 6}, {6, 5, 5}, {5, 6, 5}, {5, 5, 6}, {6, 5, 5}, {5, 0, 4, 1}, {7, 7, 1, 2}, {7, 1, 2, 7}, {8, 9, 4, 0, 8, 9}, {0, 8, 9, 8, 9, 4}, {11, 0, 10, 11, 4, 2}, {6, 6}, {7, 7, 1, 2}, {7, 7, 1, 2}};
	private int anInt5736;
	private ha_Sub3 aHa_Sub3_5737;
	int anInt5738;
	int anInt5739;
	private int anInt5740;
	private int anInt5741;
	static IncomingPacket aClass231_5742 = new IncomingPacket(19, -2);
	static long aLong5743;
	static int anInt5744;

	final void method2799(int i, int i_0_, int i_1_) {
		OpenGL.glFramebufferRenderbufferEXT(i_0_, i, 36161, anInt5736);
		anInt5733 = i_0_;
		if (i_1_ != -238823728)
			anInt5738 = 68;
		anInt5734 = i;
	}

	protected final void finalize() throws Throwable {
		method2801(0);
		super.finalize();
	}

	public final void method73(boolean bool) {
		OpenGL.glFramebufferRenderbufferEXT(anInt5733, anInt5734, 36161, 0);
		anInt5733 = -1;
		if (bool != true)
			anInt5736 = 113;
		anInt5734 = -1;
	}

	static final void method2800(int i, int i_2_, int i_3_, InterfaceComponent class51) {
		if (InvisiblePlayer.aClass51_1982 == null && !Class318.aBoolean2814 && (class51 != null && Class368_Sub3.method3814((byte) -53, class51) != null)) {
			InvisiblePlayer.aClass51_1982 = class51;
			StaticMethods.aClass51_4842 = Class368_Sub3.method3814((byte) -53, class51);
			Class245.aBoolean2327 = false;
			Class296_Sub38.anInt4899 = i_3_;
			ha.anInt1293 = i_2_;
			Class208.anInt2090 = i;
		}
	}

	final void method2801(int i) {
		if (i < anInt5736) {
			aHa_Sub3_5737.method1326(anInt5736, anInt5740, 1485573640);
			anInt5736 = 0;
		}
	}

	static final int method2802(int i, int i_4_, int i_5_, int i_6_, int i_7_) {
		if (i_7_ != -11202)
			method2800(-105, 12, -49, null);
		int i_8_ = -Class296_Sub4.anIntArray4618[i_5_ * 8192 / i_6_] + 65536 >> 1;
		return (i_4_ * (65536 - i_8_) >> 16) + (i * i_8_ >> 16);
	}

	public static void method2803(int i) {
		aClass231_5742 = null;
		if (i >= 64)
			anIntArrayArray5735 = null;
	}

	Class296_Sub39_Sub4(ha_Sub3 var_ha_Sub3, int i, int i_9_, int i_10_) {
		anInt5733 = -1;
		anInt5741 = i;
		anInt5739 = i_10_;
		anInt5738 = i_9_;
		aHa_Sub3_5737 = var_ha_Sub3;
		OpenGL.glGenRenderbuffersEXT(1, Class296_Sub48.anIntArray4975, 0);
		anInt5736 = Class296_Sub48.anIntArray4975[0];
		OpenGL.glBindRenderbufferEXT(36161, anInt5736);
		OpenGL.glRenderbufferStorageEXT(36161, anInt5741, anInt5738, anInt5739);
		anInt5740 = anInt5739 * (anInt5738 * aHa_Sub3_5737.method1290(anInt5741, (byte) 31));
	}

	Class296_Sub39_Sub4(ha_Sub3 var_ha_Sub3, int i, int i_11_, int i_12_, int i_13_) {
		anInt5733 = -1;
		anInt5739 = i_12_;
		anInt5741 = i;
		aHa_Sub3_5737 = var_ha_Sub3;
		anInt5738 = i_11_;
		OpenGL.glGenRenderbuffersEXT(1, Class296_Sub48.anIntArray4975, 0);
		anInt5736 = Class296_Sub48.anIntArray4975[0];
		OpenGL.glBindRenderbufferEXT(36161, anInt5736);
		OpenGL.glRenderbufferStorageMultisampleEXT(36161, i_13_, anInt5741, anInt5738, anInt5739);
		anInt5740 = anInt5739 * (anInt5738 * aHa_Sub3_5737.method1290(anInt5741, (byte) 36));
	}
}
