package net.zaros.client;

/* Class325 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class325 {
	int anInt2865;
	static IncomingPacket aClass231_2866 = new IncomingPacket(40, 9);
	Class394 aClass394_2867;
	static int anInt2868 = -1;
	static Class381 aClass381_2869;
	private int anInt2870;
	int anInt2871;

	final void method3379(byte i, Packet class296_sub17) {
		for (;;) {
			int i_0_ = class296_sub17.g1();
			if (i_0_ == 0)
				break;
			method3382(i_0_, class296_sub17, (byte) -91);
		}
		if (i != -103)
			method3384(null, -116, -40, 75, null);
	}

	public static void method3380(int i) {
		aClass231_2866 = null;
		int i_1_ = 65 % ((-61 - i) / 51);
		aClass381_2869 = null;
	}

	final synchronized Class186 method3381(byte i) {
		if (i <= 30)
			aClass231_2866 = null;
		Class186 class186 = ((Class186) aClass394_2867.aClass113_3309.get((long) anInt2870));
		if (class186 != null)
			return class186;
		class186 = Class186.method1876(aClass394_2867.aClass138_3307, anInt2870, 0);
		if (class186 != null)
			aClass394_2867.aClass113_3309.put(class186, (long) anInt2870);
		return class186;
	}

	private final void method3382(int i, Packet class296_sub17, byte i_2_) {
		if (i != 1) {
			if (i == 2) {
				anInt2865 = class296_sub17.g1();
				anInt2871 = class296_sub17.g1();
			}
		} else
			anInt2870 = class296_sub17.g2();
		if (i_2_ != -91) {
			/* empty */
		}
	}

	public Class325() {
		/* empty */
	}

	static final byte[] unwrapBuffer(boolean bool, int i, Object object) {
		if (object == null)
			return null;
		if (object instanceof byte[]) {
			byte[] is = (byte[]) object;
			if (bool)
				return Class296_Sub27.method2675(is, true);
			return is;
		}
		if (object instanceof Class104) {
			Class104 class104 = (Class104) object;
			return class104.method899(9629);
		}
		if (i != -1)
			aClass231_2866 = null;
		throw new IllegalArgumentException();
	}

	static final void method3384(InterfaceComponent class51, int i, int i_3_, int i_4_, ha var_ha) {
		int i_5_ = 63;
		if (i_4_ > 2) {
			int i_6_ = 7;
			for (int i_7_ = 63; i_7_ >= 0; i_7_--) {
				Class338.method3437(true, false, false);
				int i_8_ = (i_7_ & 0x3f) << 10 | (i_6_ & 0x7) << 7 | i_5_ & 0x7f;
				int i_9_ = Class166_Sub1.anIntArray4300[i_8_];
				Class355_Sub1.method3702((byte) 78, true, false);
				var_ha.aa(i, i_3_ + ((-i_7_ + 63) * class51.anInt623 >> 6), class51.anInt578, (class51.anInt623 >> 6) + 1, i_9_, 0);
			}
		}
	}
}
