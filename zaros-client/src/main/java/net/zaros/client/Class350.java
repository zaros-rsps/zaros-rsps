package net.zaros.client;

/* Class350 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class350 {
	private AdvancedMemoryCache aClass113_3037 = new AdvancedMemoryCache(128);
	static IncomingPacket aClass231_3038 = new IncomingPacket(15, -1);
	private Js5 aClass138_3039;
	static volatile Object anObject3040 = null;

	final Class277 method3677(int i, int i_0_) {
		Class277 class277;
		synchronized (aClass113_3037) {
			class277 = (Class277) aClass113_3037.get((long) i);
		}
		if (class277 != null)
			return class277;
		byte[] is = aClass138_3039.getFile(Class41_Sub6.method417((byte) -119, i), EffectiveVertex.method2118((byte) 117, i));
		class277 = new Class277();
		if (i_0_ != -6115)
			aClass113_3037 = null;
		if (is != null)
			class277.method2327(new Packet(is), 255);
		synchronized (aClass113_3037) {
			aClass113_3037.put(class277, (long) i);
		}
		return class277;
	}

	static final int[] method3678(boolean bool) {
		if (bool != true)
			method3679((byte) -97, '=');
		return new int[]{Class368_Sub15.anInt5518, Mesh.anInt1364, Class190.anInt1936};
	}

	static final boolean method3679(byte i, char c) {
		try {
			if (i <= 87)
				return true;
			if ((c < '0' || c > '9') && (c < 'A' || c > 'Z') && (c < 'a' || c > 'z'))
				return false;
			return true;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "tu.D(" + i + ',' + c + ')');
		}
	}

	Class350(GameType class35, int i, Js5 class138) {
		aClass138_3039 = class138;
		if (aClass138_3039 != null) {
			int i_1_ = aClass138_3039.getLastGroupId() - 1;
			aClass138_3039.getLastFileId(i_1_);
		}
	}

	public static void method3680(int i) {
		anObject3040 = null;
		if (i >= 118)
			aClass231_3038 = null;
	}
}
