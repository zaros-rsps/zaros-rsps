package net.zaros.client;

/* Class368_Sub21 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub21 extends Class368 {
	private int anInt5551;
	private int anInt5552;
	private int anInt5553;
	private int anInt5554;
	static MidiDecoder aClass296_Sub47_5555 = null;
	private int anInt5556;
	private int anInt5557;

	final void method3807(byte i) {
		Class379.aClass302Array3624[anInt5554].method3262(0).applyHit(anInt5556, anInt5553, anInt5557, anInt5552, 0, anInt5551, Class29.anInt307);
		int i_0_ = -107 / ((i - 52) / 52);
	}

	Class368_Sub21(Packet class296_sub17) {
		super(class296_sub17);
		anInt5554 = class296_sub17.g2();
		int i = class296_sub17.g1();
		if ((i & 0x1) != 0) {
			anInt5556 = class296_sub17.g2();
			anInt5557 = class296_sub17.g2();
		} else {
			anInt5556 = -1;
			anInt5557 = -1;
		}
		if ((i & 0x2) == 0) {
			anInt5553 = -1;
			anInt5552 = -1;
		} else {
			anInt5553 = class296_sub17.g2();
			anInt5552 = class296_sub17.g2();
		}
		if ((i & 0x4) != 0) {
			int i_1_ = class296_sub17.g2();
			int i_2_ = class296_sub17.g2();
			int i_3_ = i_1_ * 255 / i_2_;
			if (i_1_ > 0 && i_3_ < 1)
				i_3_ = 1;
			anInt5551 = i_3_;
		} else
			anInt5551 = -1;
	}

	static final void method3863(String string, int i, int i_4_) {
		Connection class204 = Class296_Sub51_Sub13.method3111(true);
		Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 107, Class3.aClass311_69);
		class296_sub1.out.p1(1 + Class117.method1015((byte) -117, string));
		class296_sub1.out.writeString(string);
		if (i_4_ != -1)
			method3863(null, 86, -63);
		class296_sub1.out.method2620(i);
		class204.sendPacket(class296_sub1, (byte) 119);
	}

	static final void method3864(int i) {
		Class389.anInt3277 = -2;
		if (i > -83)
			aClass296_Sub47_5555 = null;
		Class296_Sub39_Sub12.anInt6200 = -2;
		Class296_Sub39_Sub12.loginState = 0;
	}

	public static void method3865(byte i) {
		aClass296_Sub47_5555 = null;
		if (i != -110)
			aClass296_Sub47_5555 = null;
	}
}
