package net.zaros.client;

/* Class240 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class240 {
	Class373_Sub3 aClass373_Sub3_2257;
	static double aDouble2258;
	boolean aBoolean2259;
	int anInt2260 = 0;
	Runnable aRunnable2261;
	boolean aBoolean2262;
	static float aFloat2263;
	static int anInt2264 = 1;
	int anInt2265;
	int anInt2266 = 0;
	int anInt2267;
	int anInt2268;
	boolean aBoolean2269;
	private ha_Sub2 aHa_Sub2_2270;
	int anInt2271;
	int anInt2272;
	int anInt2273;
	int[] anIntArray2274;
	int[] anIntArray2275;
	int[] anIntArray2276;
	int[] anIntArray2277;
	int[] anIntArray2278;
	float[] aFloatArray2279;
	int[] anIntArray2280;
	int[] anIntArray2281;
	int[] anIntArray2282;
	Class178_Sub1[] aClass178_Sub1Array2283;
	int[] anIntArray2284;
	Class11 aClass11_2285;
	int[] anIntArray2286;
	int[] anIntArray2287;
	int[] anIntArray2288;
	Class178_Sub1[] aClass178_Sub1Array2289;
	int[] anIntArray2290;
	int[] anIntArray2291;
	int[] anIntArray2292;
	int[] anIntArray2293;
	int[] anIntArray2294;
	int[] anIntArray2295;
	int[] anIntArray2296;
	int[] anIntArray2297;
	int[] anIntArray2298;

	final void method2145(byte i, Runnable runnable) {
		aRunnable2261 = runnable;
		if (i != 41)
			anIntArray2278 = null;
	}

	final void method2146(byte i) {
		aClass11_2285 = new Class11(aHa_Sub2_2270, this);
		if (i != 11)
			anInt2266 = 37;
	}

	static final boolean method2147(int i) {
		if (i != -10564)
			aDouble2258 = 1.6817702063691295;
		if (NodeDeque.js) {
			try {
				Class297.method3231("showVideoAd", CS2Script.anApplet6140, false);
				return true;
			} catch (Throwable throwable) {
				/* empty */
			}
		}
		return false;
	}

	static final void method2148(boolean bool) {
		if (HashTable.method2271((byte) -72, Class366_Sub6.anInt5392)) {
			if (Class296_Sub45_Sub2.aClass204_6276.aClass154_2045 != null)
				Class41_Sub8.method422(1, 7);
			else
				Class41_Sub8.method422(1, 5);
		} else if (Class366_Sub6.anInt5392 == 5 || Class366_Sub6.anInt5392 == 6)
			Class41_Sub8.method422(1, 3);
		else if (Class366_Sub6.anInt5392 == 13)
			Class41_Sub8.method422(1, 3);
		if (bool != true)
			method2148(false);
	}

	Class240(ha_Sub2 var_ha_Sub2) {
		aBoolean2262 = true;
		anInt2265 = 0;
		anInt2268 = 0;
		aBoolean2269 = false;
		aClass373_Sub3_2257 = new Class373_Sub3();
		anIntArray2275 = new int[8];
		anIntArray2276 = new int[Class178_Sub1.anInt4378];
		anIntArray2277 = new int[64];
		aFloatArray2279 = new float[2];
		anIntArray2278 = new int[Class178_Sub1.anInt4378];
		aClass178_Sub1Array2283 = new Class178_Sub1[7];
		anIntArray2274 = new int[10];
		anIntArray2287 = new int[10];
		anIntArray2288 = new int[Class178_Sub1.anInt4378];
		anIntArray2282 = new int[Class178_Sub1.anInt4378];
		anIntArray2281 = new int[64];
		anIntArray2280 = new int[10000];
		anIntArray2295 = new int[8];
		anIntArray2284 = new int[64];
		anIntArray2290 = new int[Class178_Sub1.anInt4378];
		aClass178_Sub1Array2289 = new Class178_Sub1[7];
		anIntArray2297 = new int[10];
		anIntArray2298 = new int[10000];
		anIntArray2294 = new int[Class178_Sub1.anInt4378];
		anIntArray2296 = new int[10];
		anIntArray2286 = new int[Class178_Sub1.anInt4378];
		anIntArray2293 = new int[64];
		anIntArray2292 = new int[8];
		aHa_Sub2_2270 = var_ha_Sub2;
		anInt2267 = aHa_Sub2_2270.anInt4087 - 255;
		aClass11_2285 = new Class11(var_ha_Sub2, this);
		for (int i = 0; i < 7; i++) {
			aClass178_Sub1Array2283[i] = new Class178_Sub1(aHa_Sub2_2270);
			aClass178_Sub1Array2289[i] = new Class178_Sub1(aHa_Sub2_2270);
		}
		anIntArray2291 = new int[Class178_Sub1.anInt4380];
		for (int i = 0; i < Class178_Sub1.anInt4380; i++)
			anIntArray2291[i] = -1;
	}
}
