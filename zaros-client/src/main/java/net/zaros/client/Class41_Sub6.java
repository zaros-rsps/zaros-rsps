package net.zaros.client;

/* Class41_Sub6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub6 extends Class41 {
	static SubInPacket aClass260_3758 = new SubInPacket(10, 7);
	static int texture_group = -1;
	static int anInt3760;

	Class41_Sub6(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final int method380(int i, byte i_0_) {
		if (i_0_ != 41)
			return -60;
		return 1;
	}

	final int method383(byte i) {
		if (i != 110)
			anInt3760 = -69;
		return 3;
	}

	public static void method415(int i) {
		aClass260_3758 = null;
		if (i >= -63)
			texture_group = 26;
	}

	final int method416(int i) {
		if (i < 114)
			aClass260_3758 = null;
		return anInt389;
	}

	Class41_Sub6(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final void method381(int i, byte i_1_) {
		anInt389 = i;
		if (i_1_ != -110)
			method415(-35);
	}

	static final int method417(byte i, int i_2_) {
		if (i > -113)
			anInt3760 = 5;
		return i_2_ >>> 8;
	}

	final void method386(int i) {
		if (i != 2)
			texture_group = -53;
		if (anInt389 < 0 || anInt389 > 4)
			anInt389 = method383((byte) 110);
	}
}
