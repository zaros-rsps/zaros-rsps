package net.zaros.client;

/* Class338_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub1 extends Class338 {
	private boolean aBoolean5176;
	boolean aBoolean5177 = false;
	private Class404 aClass404_5178;
	Class338_Sub8_Sub2_Sub1[] aClass338_Sub8_Sub2_Sub1Array5179;
	private long aLong5180;
	private long aLong5181;
	private static boolean[] aBooleanArray5182 = new boolean[32];
	int anInt5183;
	private static boolean[] aBooleanArray5184 = new boolean[8];
	NodeDeque aClass155_5185;
	private int anInt5186;
	int anInt5187;
	Class390 aClass390_5188;
	private boolean aBoolean5189;
	private int anInt5190;
	boolean aBoolean5191;

	final void method3439() {
		aBoolean5176 = true;
	}

	final void method3440() {
		aBoolean5189 = true;
	}

	final void method3441(long l) {
		aLong5181 = l;
	}

	static final Class338_Sub1 method3442(int i, boolean bool) {
		if (Model.anInt1848 != Class379_Sub2_Sub1.anInt6606) {
			Class338_Sub1 class338_sub1 = (Class380.aClass338_Sub1Array3201[Class379_Sub2_Sub1.anInt6606]);
			Class379_Sub2_Sub1.anInt6606 = (Class379_Sub2_Sub1.anInt6606 + 1 & Class316.anIntArray2801[AdvancedMemoryCache.anInt1167]);
			class338_sub1.method3447(i, bool);
			return class338_sub1;
		}
		return new Class338_Sub1(i, bool);
	}

	private final void method3443(ha var_ha, EmissiveTriangle[] class89s, boolean bool) {
		for (int i = 0; i < 32; i++)
			aBooleanArray5182[i] = false;
		while_46_ : for (Class338_Sub6 class338_sub6 = (Class338_Sub6) aClass404_5178.method4160((byte) -80); class338_sub6 != null; class338_sub6 = (Class338_Sub6) aClass404_5178.method4163(-24917)) {
			if (class89s != null) {
				for (int i = 0; i < class89s.length; i++) {
					if (class338_sub6.aClass89_5233 == class89s[i] || (class338_sub6.aClass89_5233 == class89s[i].aClass89_956)) {
						aBooleanArray5182[i] = true;
						class338_sub6.method3596((byte) -95);
						class338_sub6.aBoolean5231 = false;
						continue while_46_;
					}
				}
			}
			if (!bool) {
				if (class338_sub6.anInt5234 == 0) {
					class338_sub6.method3438(false);
					anInt5186--;
				} else
					class338_sub6.aBoolean5231 = true;
			}
		}
		if (class89s != null) {
			for (int i = 0; i < class89s.length; i++) {
				if (i == 32 || anInt5186 == 32)
					break;
				if (!aBooleanArray5182[i]) {
					Class338_Sub6 class338_sub6 = new Class338_Sub6(var_ha, class89s[i], this, aLong5181);
					aClass404_5178.method4158(class338_sub6, 1);
					anInt5186++;
					aBooleanArray5182[i] = true;
				}
			}
		}
	}

	final Class390 method3444() {
		return aClass390_5188;
	}

	public static void method3445() {
		aBooleanArray5182 = null;
		aBooleanArray5184 = null;
	}

	private final void method3446(EffectiveVertex[] class232s, boolean bool) {
		for (int i = 0; i < 8; i++)
			aBooleanArray5184[i] = false;
		while_48_ : for (Class296_Sub39_Sub17 class296_sub39_sub17 = ((Class296_Sub39_Sub17) aClass155_5185.removeFirst((byte) 119)); class296_sub39_sub17 != null; class296_sub39_sub17 = (Class296_Sub39_Sub17) aClass155_5185.removeNext(1001)) {
			if (class232s != null) {
				for (int i = 0; i < class232s.length; i++) {
					if (class296_sub39_sub17.aClass232_6240 == class232s[i] || (class296_sub39_sub17.aClass232_6240 == class232s[i].aClass232_2223)) {
						aBooleanArray5184[i] = true;
						class296_sub39_sub17.method2893(2);
						continue while_48_;
					}
				}
			}
			if (!bool) {
				class296_sub39_sub17.unlink();
				anInt5190--;
				if (class296_sub39_sub17.is_queue_linked()) {
					class296_sub39_sub17.queue_unlink();
					Class16_Sub1_Sub1.anInt5803--;
				}
			}
		}
		if (class232s != null) {
			for (int i = 0; i < class232s.length; i++) {
				if (i == 8 || anInt5190 == 8)
					break;
				if (!aBooleanArray5184[i]) {
					Class296_Sub39_Sub17 class296_sub39_sub17 = null;
					if (class232s[i].method2119(255).anInt1030 == 1 && Class16_Sub1_Sub1.anInt5803 < 32) {
						class296_sub39_sub17 = new Class296_Sub39_Sub17(class232s[i], this);
						Class241_Sub1.aClass400_4577.method4136(class296_sub39_sub17, (byte) 126, (long) class232s[i].anInt2216);
						Class16_Sub1_Sub1.anInt5803++;
					}
					if (class296_sub39_sub17 == null)
						class296_sub39_sub17 = new Class296_Sub39_Sub17(class232s[i], this);
					aClass155_5185.addLast((byte) -19, class296_sub39_sub17);
					anInt5190++;
					aBooleanArray5184[i] = true;
				}
			}
		}
	}

	private final void method3447(int i, boolean bool) {
		Class368_Sub19.aClass404_5542.method4158(this, 1);
		aLong5181 = (long) i;
		aLong5180 = (long) i;
		aBoolean5189 = true;
		aBoolean5191 = bool;
	}

	final boolean method3448(ha var_ha, long l) {
		if (aLong5181 != aLong5180)
			method3439();
		else
			method3451();
		if (l - aLong5181 > 750L) {
			method3449();
			return false;
		}
		int i = (int) (l - aLong5180);
		if (aBoolean5189) {
			for (Class338_Sub6 class338_sub6 = (Class338_Sub6) aClass404_5178.method4160((byte) 106); class338_sub6 != null; class338_sub6 = (Class338_Sub6) aClass404_5178.method4163(-24917)) {
				for (int i_0_ = 0; i_0_ < class338_sub6.aClass169_5228.anInt1738; i_0_++)
					class338_sub6.method3594(!aBoolean5176, l, -121, 1, var_ha);
			}
			aBoolean5189 = false;
		}
		for (Class338_Sub6 class338_sub6 = (Class338_Sub6) aClass404_5178.method4160((byte) -60); class338_sub6 != null; class338_sub6 = (Class338_Sub6) aClass404_5178.method4163(-24917))
			class338_sub6.method3594(!aBoolean5176, l, -124, i, var_ha);
		aLong5180 = l;
		return true;
	}

	final void method3449() {
		aBoolean5177 = true;
		for (Class296_Sub39_Sub17 class296_sub39_sub17 = ((Class296_Sub39_Sub17) aClass155_5185.removeFirst((byte) 126)); class296_sub39_sub17 != null; class296_sub39_sub17 = (Class296_Sub39_Sub17) aClass155_5185.removeNext(1001)) {
			if (class296_sub39_sub17.aClass95_6245.anInt1030 == 1)
				class296_sub39_sub17.queue_unlink();
		}
		for (int i = 0; i < aClass338_Sub8_Sub2_Sub1Array5179.length; i++) {
			if (aClass338_Sub8_Sub2_Sub1Array5179[i] != null) {
				aClass338_Sub8_Sub2_Sub1Array5179[i].method3610();
				aClass338_Sub8_Sub2_Sub1Array5179[i] = null;
			}
		}
		anInt5183 = 0;
		aClass404_5178 = new Class404();
		anInt5186 = 0;
		aClass155_5185 = new NodeDeque();
		anInt5190 = 0;
		this.method3438(false);
		Class380.aClass338_Sub1Array3201[Model.anInt1848] = this;
		Model.anInt1848 = (Model.anInt1848 + 1 & Class316.anIntArray2801[AdvancedMemoryCache.anInt1167]);
	}

	final void method3450(int i, int i_1_, int i_2_, int i_3_, int i_4_) {
		anInt5187 = i;
	}

	private final void method3451() {
		aBoolean5176 = false;
	}

	final void method3452(ha var_ha, long l, EmissiveTriangle[] class89s, EffectiveVertex[] class232s, boolean bool) {
		if (!aBoolean5177) {
			method3443(var_ha, class89s, bool);
			method3446(class232s, bool);
			aLong5181 = l;
		}
	}

	final Class390 method3453() {
		aClass390_5188.aClass84_3282.method817(102);
		for (int i = 0; i < aClass338_Sub8_Sub2_Sub1Array5179.length; i++) {
			if (aClass338_Sub8_Sub2_Sub1Array5179[i] != null && (aClass338_Sub8_Sub2_Sub1Array5179[i].aClass338_Sub6_6863 != null))
				aClass390_5188.aClass84_3282.method815(24150, aClass338_Sub8_Sub2_Sub1Array5179[i]);
		}
		return aClass390_5188;
	}

	final void method3454(ha var_ha) {
		aClass390_5188.aClass84_3282.method817(89);
		for (Class338_Sub6 class338_sub6 = (Class338_Sub6) aClass404_5178.method4160((byte) -25); class338_sub6 != null; class338_sub6 = (Class338_Sub6) aClass404_5178.method4163(-24917))
			class338_sub6.method3595(-1, var_ha, aLong5180);
	}

	private Class338_Sub1(int i, boolean bool) {
		aBoolean5176 = false;
		anInt5183 = 0;
		aClass404_5178 = new Class404();
		anInt5186 = 0;
		aClass155_5185 = new NodeDeque();
		anInt5190 = 0;
		aBoolean5189 = false;
		aBoolean5191 = false;
		aClass390_5188 = new Class390();
		aClass338_Sub8_Sub2_Sub1Array5179 = new Class338_Sub8_Sub2_Sub1[8192];
		method3447(i, bool);
	}
}
