package net.zaros.client;

/* Class296_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class StringNode extends Node {
	static IncomingPacket aClass231_4621 = new IncomingPacket(127, -1);
	static boolean aBoolean4622 = false;
	public String value;

	public static void method2445(byte i) {
		aClass231_4621 = null;
		if (i != 52) {
			method2445((byte) -2);
		}
	}

	static final void logout(int i, boolean bool) {
		Connection[] class204s = Class296_Sub45_Sub2.aClass204Array6278;
		for (Connection class204 : class204s) {
			class204.method1966(i + 317);
		}
		Class368_Sub21.method3864(-126);
		TextureOperation.method3069(-117);
		Class338_Sub3_Sub1_Sub1.method3485();
		for (int i_1_ = 0; i_1_ < 4; i_1_++) {
			BITConfigDefinition.mapClips[i_1_].setup();
		}
		Class254.method2212(28461, false);
		System.gc();
		StaticMethods.method2461(2, 0);
		Class30.anInt318 = -1;
		Class124.aBoolean1282 = false;
		Class296_Sub34.method2734(255);
		Class296_Sub34.method2733(true, i);
		AdvancedMemoryCache.method987(1);
		Class121.method1033(i + 16381);
		Class368_Sub2.method3812(0);
		if (bool) {
			Class41_Sub8.method422(1, 13);
		} else {
			Class41_Sub8.method422(1, 3);
			try {
				Class297.method3231("loggedout", CS2Script.anApplet6140, false);
			} catch (Throwable throwable) {
				/* empty */
			}
		}
	}

	public StringNode() {
		/* empty */
	}

	public StringNode(String string) {
		value = string;
	}
}
