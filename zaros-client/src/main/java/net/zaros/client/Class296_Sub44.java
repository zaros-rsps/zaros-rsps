package net.zaros.client;

/* Class296_Sub44 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub44 extends Node {
	static int[] anIntArray4941 = {0, -1, 0, 1};
	int anInt4942;
	boolean aBoolean4943;
	int anInt4944;
	int anInt4945;
	int anInt4946;
	static OutgoingPacket aClass311_4947 = new OutgoingPacket(2, -1);
	int anInt4948;
	static Class278 aClass278_4949;

	static final void method2924(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_) {
		if (i_0_ < ConfigurationDefinition.anInt676 || Class288.anInt2652 < i || i_3_ < EmissiveTriangle.anInt952 || RuntimeException_Sub1.anInt3391 < i_2_) {
			if (i_5_ != 1)
				Class338_Sub3_Sub5_Sub2.method3588(i, 0, i_5_, i_0_, i_2_, i_1_, i_3_);
			else
				Class338_Sub3_Sub2_Sub1.method3555(i_2_, i_3_, i_1_, i_0_, i, (byte) 113);
		} else if (i_5_ != 1)
			Class75.method781(i_5_, i_3_, i_1_, (byte) 104, i_2_, i, i_0_);
		else
			Class110.method969(i_3_, i_1_, 9404, i_0_, i_2_, i);
		if (i_4_ != 28127)
			anIntArray4941 = null;
	}

	static final void method2925(int i, byte i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_) {
		Class300.method3247(i_12_, 0, i_8_, i_7_, 512, i_9_, false);
		if (i_6_ != -44)
			anIntArray4941 = null;
	}

	public static void method2926(int i) {
		int i_14_ = -77 % ((i + 35) / 49);
		aClass278_4949 = null;
		aClass311_4947 = null;
		anIntArray4941 = null;
	}

	static final void method2927(int i) {
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5029.method449(123) == 2) {
			byte i_15_ = (byte) (Class296_Sub2.anInt4590 - 4 & 0xff);
			int i_16_ = Class296_Sub2.anInt4590 % Class198.currentMapSizeX;
			for (int i_17_ = 0; i_17_ < 4; i_17_++) {
				for (int i_18_ = 0; Class296_Sub38.currentMapSizeY > i_18_; i_18_++)
					Class198.aByteArrayArrayArray2003[i_17_][i_16_][i_18_] = i_15_;
			}
			if (FileWorker.anInt3005 != 3) {
				for (int i_19_ = 0; i_19_ < 2; i_19_++) {
					Class41_Sub23.anIntArray3801[i_19_] = -1000000;
					Class368_Sub9.anIntArray5473[i_19_] = 1000000;
					Class309.anIntArray2754[i_19_] = 0;
					Class404.anIntArray3382[i_19_] = 1000000;
					StaticMethods.anIntArray3143[i_19_] = 0;
				}
				int i_20_ = (Class296_Sub51_Sub11.localPlayer.tileX);
				int i_21_ = (Class296_Sub51_Sub11.localPlayer.tileY);
				int i_22_ = -53 % ((-9 - i) / 35);
				do {
					if (Class361.anInt3103 != 1 && Class368_Sub11.anInt5486 == -1) {
						int i_23_ = aa_Sub1.method155(-1537652855, FileWorker.anInt3005, Class219.camPosX, Class124.camPosZ);
						if (i_23_ - TranslatableString.camPosY < 3200 && ((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][Class219.camPosX >> 9][Class124.camPosZ >> 9]) & 0x4) != 0)
							Class219.method2058(Class219.camPosX >> 9, Class338_Sub2.aClass247ArrayArrayArray5195, (byte) 1, Class124.camPosZ >> 9, false, 1);
					} else {
						if (Class361.anInt3103 != 1) {
							i_21_ = Class242.anInt2309;
							i_20_ = Class368_Sub11.anInt5486;
						}
						if (((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_20_ >> 9][i_21_ >> 9]) & 0x4) != 0)
							Class219.method2058(i_20_ >> 9, Class338_Sub2.aClass247ArrayArrayArray5195, (byte) 1, i_21_ >> 9, false, 0);
						if (Class296_Sub17_Sub2.camRotX >= 2560)
							break;
						int i_24_ = Class219.camPosX >> 9;
						int i_25_ = Class124.camPosZ >> 9;
						int i_26_ = i_20_ >> 9;
						int i_27_ = i_21_ >> 9;
						int i_28_;
						if (i_26_ > i_24_)
							i_28_ = i_26_ - i_24_;
						else
							i_28_ = i_24_ - i_26_;
						int i_29_;
						if (i_25_ < i_27_)
							i_29_ = i_27_ - i_25_;
						else
							i_29_ = -i_27_ + i_25_;
						if (i_28_ == 0 && i_29_ == 0 || -Class198.currentMapSizeX >= i_28_ || Class198.currentMapSizeX <= i_28_ || i_29_ <= -Class296_Sub38.currentMapSizeY || Class296_Sub38.currentMapSizeY <= i_29_)
							Class219_Sub1.method2062(("RC: " + i_24_ + "," + i_25_ + " " + i_26_ + "," + i_27_ + " " + Class206.worldBaseX + "," + (Class41_Sub26.worldBaseY)), (byte) -114, null);
						else if (i_29_ >= i_28_) {
							int i_30_ = i_28_ * 65536 / i_29_;
							int i_31_ = 32768;
							while (i_25_ != i_27_) {
								if (i_27_ > i_25_)
									i_25_++;
								else if (i_25_ > i_27_)
									i_25_--;
								if (((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_][i_25_]) & 0x4) != 0) {
									Class219.method2058(i_24_, (Class338_Sub2.aClass247ArrayArrayArray5195), (byte) 1, i_25_, false, 1);
									break;
								}
								i_31_ += i_30_;
								if (i_31_ >= 65536) {
									i_31_ -= 65536;
									if (i_24_ >= i_26_) {
										if (i_24_ > i_26_)
											i_24_--;
									} else
										i_24_++;
									if (((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_][i_25_]) & 0x4) != 0) {
										Class219.method2058(i_24_, (Class338_Sub2.aClass247ArrayArrayArray5195), (byte) 1, i_25_, false, 1);
										break;
									}
								}
							}
						} else {
							int i_32_ = i_29_ * 65536 / i_28_;
							int i_33_ = 32768;
							while (i_24_ != i_26_) {
								if (i_26_ <= i_24_) {
									if (i_26_ < i_24_)
										i_24_--;
								} else
									i_24_++;
								if (((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_][i_25_]) & 0x4) != 0) {
									Class219.method2058(i_24_, (Class338_Sub2.aClass247ArrayArrayArray5195), (byte) 1, i_25_, false, 1);
									break;
								}
								i_33_ += i_32_;
								if (i_33_ >= 65536) {
									i_33_ -= 65536;
									if (i_27_ <= i_25_) {
										if (i_25_ > i_27_)
											i_25_--;
									} else
										i_25_++;
									if (((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_][i_25_]) & 0x4) != 0) {
										Class219.method2058(i_24_, (Class338_Sub2.aClass247ArrayArrayArray5195), (byte) 1, i_25_, false, 1);
										break;
									}
								}
							}
						}
					}
					break;
				} while (false);
			}
		}
	}

	static final boolean method2928(int i, int i_34_, int i_35_) {
		if (i != 255)
			aClass311_4947 = null;
		if ((i_34_ & 0x21) == 0)
			return false;
		return true;
	}

	Class296_Sub44(int i, int i_36_, int i_37_, int i_38_, int i_39_, boolean bool) {
		anInt4944 = i_38_;
		anInt4948 = i_39_;
		anInt4942 = i;
		anInt4945 = i_36_;
		anInt4946 = i_37_;
		aBoolean4943 = bool;
	}
}
