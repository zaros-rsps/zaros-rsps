package net.zaros.client;

/* Class296_Sub51_Sub20 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub20 extends TextureOperation {
	static Class294 aClass294_6441;
	private int anInt6442;
	static String[] ignoreNames2 = new String[100];
	private int anInt6444 = 0;

	final int[] get_monochrome_output(int i, int i_0_) {
		int[] is = aClass318_5035.method3335(i_0_, (byte) 28);
		if (i != 0)
			get_monochrome_output(72, 87);
		if (aClass318_5035.aBoolean2819) {
			int[] is_1_ = this.method3064(0, 0, i_0_);
			for (int i_2_ = 0; Class41_Sub10.anInt3769 > i_2_; i_2_++) {
				int i_3_ = is_1_[i_2_];
				is[i_2_] = i_3_ >= anInt6444 && anInt6442 >= i_3_ ? 4096 : 0;
			}
		}
		return is;
	}

	public static void method3129(byte i) {
		aClass294_6441 = null;
		if (i != 114)
			method3129((byte) -9);
		ignoreNames2 = null;
	}

	final void method3071(int i, Packet class296_sub17, int i_4_) {
		int i_5_ = i_4_;
		do {
			if (i_5_ != 0) {
				if (i_5_ != 1)
					break;
			} else {
				anInt6444 = class296_sub17.g2();
				break;
			}
			anInt6442 = class296_sub17.g2();
		} while (false);
		if (i >= -84)
			method3129((byte) -85);
	}

	public Class296_Sub51_Sub20() {
		super(1, true);
		anInt6442 = 4096;
	}

	static {
		aClass294_6441 = new Class294(3, 2);
	}
}
