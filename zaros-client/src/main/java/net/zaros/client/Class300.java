package net.zaros.client;

/* Class300 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class300 implements Interface21 {
	static boolean[][] aBooleanArrayArray3697 = {new boolean[4], {false, true, true, false}, {true, false, true, false}, {true, false, true, false}, {false, false, true, false}, {false, false, true, false}, {true, false, true, false}, {true, false, false, true}, {true, false, false, true}, {true, true, false, false}, new boolean[4], {false, true, false, true}, new boolean[4]};
	private Js5 aClass138_3698;

	public final int method79(int i) {
		if (aClass138_3698.filesCompleted(-2))
			return 100;
		if (i != 20667)
			method3248((byte) 54);
		return aClass138_3698.getTotalCompletion(i - 20667);
	}

	public static void method3246(int i) {
		if (i != -4410)
			aBooleanArrayArray3697 = null;
		aBooleanArrayArray3697 = null;
	}

	static final void method3247(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, boolean bool) {
		if (i_3_ != 512)
			method3248((byte) 113);
		if (i_4_ < 512 || i < 512 || Class198.currentMapSizeX * 512 - 1024 < i_4_ || Class296_Sub38.currentMapSizeY * 512 - 1024 < i)
			StaticMethods.anIntArray4629[0] = StaticMethods.anIntArray4629[1] = -1;
		else {
			int i_5_ = -i_2_ + aa_Sub1.method155(i_3_ ^ ~0x5ba6ba76, i_1_, i_4_, i);
			if (Class368_Sub5_Sub2.aBoolean6597)
				Class41_Sub4.method407(true, -25490);
			else {
				Class223.aClass373_2162.method3904(i_0_, 0, 0);
				Class41_Sub13.aHa3774.a(Class223.aClass373_2162);
			}
			if (Class296_Sub39_Sub10.aBoolean6177)
				Class41_Sub13.aHa3774.HA(i_4_, i_5_, i, ModeWhat.anInt1192, StaticMethods.anIntArray4629);
			else if (bool)
				Class41_Sub13.aHa3774.H(i_4_, i_5_, i, StaticMethods.anIntArray4629);
			else
				Class41_Sub13.aHa3774.da(i_4_, i_5_, i, StaticMethods.anIntArray4629);
			if (Class368_Sub5_Sub2.aBoolean6597)
				Class296_Sub28.method2681(-19895);
			else {
				Class223.aClass373_2162.method3904(-i_0_, 0, 0);
				Class41_Sub13.aHa3774.a(Class223.aClass373_2162);
			}
		}
	}

	static final void method3248(byte i) {
		if (i == 19) {
			Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 111, Class41_Sub20.aClass311_3797);
			class296_sub1.out.p1(Class8.method192((byte) 101));
			class296_sub1.out.p2(Class241.anInt2301);
			class296_sub1.out.p2(Class384.anInt3254);
			class296_sub1.out.p1(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5015.method475(127));
			Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
		}
	}

	Class300(Js5 class138) {
		aClass138_3698 = class138;
	}

	public final Class388 method78(int i) {
		if (i != 20598)
			method78(85);
		return Class388.aClass388_3271;
	}

	static final void method3249(int i) {
		Class250.aClass15_2360.method228(5, false);
		Class262.aClass117_2449.method1019(5, (byte) -124);
		Class296_Sub51_Sub13.aClass24_6420.method298(5, (byte) -119);
		Class379.objectDefinitionLoader.method367((byte) 118, 5);
		Class352.npcDefinitionLoader.method1415(64, 5);
		Class296_Sub39_Sub1.itemDefinitionLoader.clean(5);
		Class296_Sub51_Sub13.animationsLoader.updateReferences(5);
		Class157.graphicsLoader.method2285(5, false);
		Class296_Sub43.bitConfigsLoader.updateReferences(5, 255);
		if (i >= -62)
			method3248((byte) -19);
		ConfigurationsLoader.configsLoader.updateReferences(true, 5);
		StaticMethods.aClass328_6070.method3392((byte) 28, 5);
		HashTable.aClass149_2456.method1518((byte) 120, 5);
		Class41_Sub10.aClass62_3768.method697(5, (byte) -5);
		Class31.aClass245_324.method2181(5, 36);
		ConfigurationsLoader.aClass401_86.method4142(false, 5);
		Class296_Sub22.itemExtraDataDefinitionLoader.method1658((byte) 122, 5);
		Class296_Sub51_Sub38.aClass405_6542.method4171(5, 0);
		InvisiblePlayer.aClass279_1977.method2338(5, false);
		Class49.aClass182_457.method1845(58, 5);
		Class386.aClass335_3268.method3431((byte) 84, 5);
		Class355.aClass394_3067.method4060(5, 48);
		Class338_Sub9.aClass76_5266.method788(5, false);
		ParamType.aClass164_3248.method1635((byte) 118, 5);
		Class219_Sub1.method2061((byte) -66, 5);
		Class175.method1704(50, (byte) -115);
		Class194.method1928(50, true);
		Class296_Sub4.method2442(5, 29464);
		Class187.method1884((byte) -22, 5);
		Class121.aClass113_1269.clean(5);
		Class296_Sub15_Sub1.aClass113_5994.clean(5);
		Class264.aClass113_2471.clean(5);
		Class366_Sub6.aClass113_5390.clean(5);
		CS2Executor.aClass113_1531.clean(5);
	}

	static final void method3250(int i, int i_6_) {
		Class296_Sub39_Sub18.anInt6247 = i;
		synchronized (ParticleEmitterRaw.aClass113_1725) {
			ParticleEmitterRaw.aClass113_1725.clear();
			if (i_6_ > -22)
				method3249(64);
		}
		synchronized (Class258.aClass113_2415) {
			Class258.aClass113_2415.clear();
		}
	}
}
