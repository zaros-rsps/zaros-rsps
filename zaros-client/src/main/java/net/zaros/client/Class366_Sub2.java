package net.zaros.client;
import jaggl.OpenGL;

final class Class366_Sub2 extends Class366 {
	private Class69_Sub3[] aClass69_Sub3Array5365;
	private boolean aBoolean5366;
	static OutgoingPacket aClass311_5367 = new OutgoingPacket(16, 7);
	private boolean aBoolean5368 = false;
	static int anInt5369;
	static int anInt5370;
	private Class341 aClass341_5371;
	static int lookatVelocity;

	final void method3764(boolean bool, int i, Class69 class69) {
		aHa_Sub3_3121.method1316(class69, (byte) -109);
		if (!bool)
			aHa_Sub3_3121.method1272((byte) -107, i);
	}

	static final int method3774(int i, String string) {
		if (i > -8)
			return -82;
		return Class18.method277(114, string, 10, true);
	}

	Class366_Sub2(ha_Sub3 var_ha_Sub3) {
		super(var_ha_Sub3);
		if (var_ha_Sub3.aBoolean4251) {
			aBoolean5366 = var_ha_Sub3.anInt4224 < 3;
			int i = aBoolean5366 ? 48 : 127;
			byte[][] is = new byte[6][4096];
			byte[][] is_0_ = new byte[6][4096];
			byte[][] is_1_ = new byte[6][4096];
			int i_2_ = 0;
			for (int i_3_ = 0; i_3_ < 64; i_3_++) {
				for (int i_4_ = 0; i_4_ < 64; i_4_++) {
					float f = (float) i_4_ * 2.0F / 64.0F + -1.0F;
					float f_5_ = (float) i_3_ * 2.0F / 64.0F + -1.0F;
					float f_6_ = (float) (1.0 / Math.sqrt((double) (f_5_ * f_5_ + (f * f + 1.0F))));
					f_5_ *= f_6_;
					f *= f_6_;
					for (int i_7_ = 0; i_7_ < 6; i_7_++) {
						float f_8_;
						if (i_7_ != 0) {
							if (i_7_ != 1) {
								if (i_7_ == 2)
									f_8_ = f_5_;
								else if (i_7_ == 3)
									f_8_ = -f_5_;
								else if (i_7_ != 4)
									f_8_ = -f_6_;
								else
									f_8_ = f_6_;
							} else
								f_8_ = f;
						} else
							f_8_ = -f;
						int i_9_;
						int i_10_;
						int i_11_;
						if (!(f_8_ > 0.0F))
							i_9_ = i_10_ = i_11_ = 0;
						else {
							i_9_ = (int) ((double) i * Math.pow((double) f_8_, 96.0));
							i_10_ = (int) (Math.pow((double) f_8_, 36.0) * (double) i);
							i_11_ = (int) (Math.pow((double) f_8_, 12.0) * (double) i);
						}
						is_0_[i_7_][i_2_] = (byte) i_9_;
						is_1_[i_7_][i_2_] = (byte) i_10_;
						is[i_7_][i_2_] = (byte) i_11_;
					}
					i_2_++;
				}
			}
			aClass69_Sub3Array5365 = new Class69_Sub3[3];
			aClass69_Sub3Array5365[0] = new Class69_Sub3(aHa_Sub3_3121, 6406, 64, false, is_0_, 6406);
			aClass69_Sub3Array5365[1] = new Class69_Sub3(aHa_Sub3_3121, 6406, 64, false, is_1_, 6406);
			aClass69_Sub3Array5365[2] = new Class69_Sub3(aHa_Sub3_3121, 6406, 64, false, is, 6406);
			method3776(0);
		}
	}

	final void method3768(byte i, boolean bool) {
		if (aClass341_5371 != null && bool) {
			if (!aBoolean5366) {
				aHa_Sub3_3121.method1330(112, 2);
				aHa_Sub3_3121.method1316(aHa_Sub3_3121.aClass69_Sub1_4195, (byte) -125);
				aHa_Sub3_3121.method1330(i ^ ~0x2c, 0);
			}
			aClass341_5371.method3625((byte) -119, '\0');
			aBoolean5368 = true;
		} else
			aHa_Sub3_3121.method1346(true, 770, 0, 34168);
		if (i != -88)
			method3768((byte) 50, false);
	}

	final void method3766(int i) {
		if (!aBoolean5368)
			aHa_Sub3_3121.method1346(true, 770, 0, 5890);
		else {
			if (!aBoolean5366) {
				aHa_Sub3_3121.method1330(117, 2);
				aHa_Sub3_3121.method1316(null, (byte) -103);
			}
			aHa_Sub3_3121.method1330(115, 1);
			aHa_Sub3_3121.method1316(null, (byte) -104);
			aHa_Sub3_3121.method1330(118, 0);
			aClass341_5371.method3625((byte) 74, '\001');
			aBoolean5368 = false;
		}
		aHa_Sub3_3121.method1306(8448, 8448, -22394);
		if (i <= 30)
			method3769(124, (byte) 43, -46);
	}

	final void method3770(byte i, boolean bool) {
		aHa_Sub3_3121.method1306(8448, 7681, -22394);
		if (i != 33) {
			/* empty */
		}
	}

	final boolean method3763(int i) {
		int i_12_ = 46 % ((73 - i) / 40);
		return true;
	}

	public static void method3775(byte i) {
		aClass311_5367 = null;
		if (i >= -79)
			anInt5369 = -1;
	}

	final void method3769(int i, byte i_13_, int i_14_) {
		if (i_13_ == -81) {
			if (aBoolean5368) {
				aHa_Sub3_3121.method1330(112, 1);
				aHa_Sub3_3121.method1316(aClass69_Sub3Array5365[i - 1], (byte) -101);
				aHa_Sub3_3121.method1330(116, 0);
			}
		}
	}

	private final void method3776(int i) {
		aClass341_5371 = new Class341(aHa_Sub3_3121, 2);
		aClass341_5371.method3627((byte) -126, (char) i);
		aHa_Sub3_3121.method1330(109, 1);
		OpenGL.glTexGeni(8192, 9472, 34065);
		OpenGL.glTexGeni(8193, 9472, 34065);
		OpenGL.glTexGeni(8194, 9472, 34065);
		OpenGL.glEnable(3168);
		OpenGL.glEnable(3169);
		OpenGL.glEnable(3170);
		OpenGL.glMatrixMode(5890);
		OpenGL.glLoadIdentity();
		OpenGL.glRotatef(22.5F, 1.0F, 0.0F, 0.0F);
		OpenGL.glMatrixMode(5888);
		if (aBoolean5366) {
			aHa_Sub3_3121.method1306(260, 7681, -22394);
			aHa_Sub3_3121.method1283(0, 5890, 770, (byte) -113);
			aHa_Sub3_3121.method1346(true, 770, 0, 34167);
		} else {
			aHa_Sub3_3121.method1306(7681, 8448, -22394);
			aHa_Sub3_3121.method1283(0, 34168, 768, (byte) -116);
			aHa_Sub3_3121.method1330(122, 2);
			aHa_Sub3_3121.method1306(260, 7681, -22394);
			aHa_Sub3_3121.method1283(0, 34168, 768, (byte) -122);
			aHa_Sub3_3121.method1283(1, 34168, 770, (byte) -113);
			aHa_Sub3_3121.method1346(true, 770, 0, 34167);
		}
		aHa_Sub3_3121.method1330(117, 0);
		aClass341_5371.method3622(-126);
		aClass341_5371.method3627((byte) -122, 1);
		aHa_Sub3_3121.method1330(110, 1);
		OpenGL.glDisable(3168);
		OpenGL.glDisable(3169);
		OpenGL.glDisable(3170);
		OpenGL.glMatrixMode(5890);
		OpenGL.glLoadIdentity();
		OpenGL.glMatrixMode(5888);
		if (!aBoolean5366) {
			aHa_Sub3_3121.method1306(8448, 8448, -22394);
			aHa_Sub3_3121.method1283(0, 5890, 768, (byte) -127);
			aHa_Sub3_3121.method1330(119, 2);
			aHa_Sub3_3121.method1306(8448, 8448, -22394);
			aHa_Sub3_3121.method1283(0, 5890, 768, (byte) -123);
			aHa_Sub3_3121.method1283(1, 34168, 768, (byte) -126);
			aHa_Sub3_3121.method1346(true, 770, 0, 5890);
		} else {
			aHa_Sub3_3121.method1306(8448, 8448, -22394);
			aHa_Sub3_3121.method1283(0, 5890, 768, (byte) -113);
			aHa_Sub3_3121.method1346(true, 770, 0, 5890);
		}
		aHa_Sub3_3121.method1330(122, 0);
		aClass341_5371.method3622(-123);
	}
}
