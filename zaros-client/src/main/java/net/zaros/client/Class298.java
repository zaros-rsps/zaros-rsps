package net.zaros.client;

/* Class298 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class298 {
	private NodeDeque aClass155_2698;
	static int anInt2699;
	private Node aClass296_2700;

	final void method3233(NodeDeque class155, int i) {
		if (i == 24459)
			aClass155_2698 = class155;
	}

	static final void method3234(int i, boolean bool, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		if (i_4_ == 8 || i_4_ == 16) {
			if (i_4_ != 8) {
				int i_5_ = (i_2_ << Class313.anInt2779) + Js5TextureLoader.anInt3440;
				int i_6_ = -Js5TextureLoader.anInt3440 + i_5_;
				int i_7_ = i_1_ << Class313.anInt2779;
				int i_8_ = i_7_ + Js5TextureLoader.anInt3440;
				int i_9_ = Class244.aSArray2320[i_0_].method3355(i_1_, (byte) -118, i_2_ + 1);
				int i_10_ = Class244.aSArray2320[i_0_].method3355(i_1_ + 1, (byte) -128, i_2_);
				Class343_Sub1.aClass324Array5273[Class22.anInt249++] = new Class324(i_4_, i_0_, i_5_, i_6_, i_6_, i_5_, i_9_, i_10_, i_10_ - i, -i + i_9_, i_7_, i_8_, i_8_, i_7_);
			} else {
				int i_11_ = i_2_ << Class313.anInt2779;
				int i_12_ = Js5TextureLoader.anInt3440 + i_11_;
				int i_13_ = i_1_ << Class313.anInt2779;
				int i_14_ = i_13_ + Js5TextureLoader.anInt3440;
				int i_15_ = Class244.aSArray2320[i_0_].method3355(i_1_, (byte) -128, i_2_);
				int i_16_ = Class244.aSArray2320[i_0_].method3355(i_1_ + 1, (byte) -105, i_2_ + 1);
				Class343_Sub1.aClass324Array5273[Class22.anInt249++] = new Class324(i_4_, i_0_, i_11_, i_12_, i_12_, i_11_, i_15_, i_16_, i_16_ - i, -i + i_15_, i_13_, i_14_, i_14_, i_13_);
			}
		} else {
			Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i_0_][i_2_][i_1_];
			if (class247 == null)
				class247 = new Class247(i_0_);
			if (i_4_ != 1) {
				if (i_4_ == 2) {
					class247.aShort2344 = (short) i_3_;
					class247.aShort2340 = (short) i;
				}
			} else {
				class247.aShort2347 = (short) i;
				class247.aShort2345 = (short) i_3_;
			}
			if (Class190.aBoolean1943)
				Class320.method3341(false);
		}
		if (bool != true)
			anInt2699 = 61;
	}

	final Node method3235(boolean bool) {
		if (bool != true)
			method3233(null, -47);
		Node class296 = aClass296_2700;
		if (aClass155_2698.aClass296_1586 == class296) {
			aClass296_2700 = null;
			return null;
		}
		aClass296_2700 = class296.next;
		return class296;
	}

	final Node method3236(byte i) {
		Node class296 = aClass155_2698.aClass296_1586.next;
		if (i <= 16)
			aClass296_2700 = null;
		if (aClass155_2698.aClass296_1586 == class296) {
			aClass296_2700 = null;
			return null;
		}
		aClass296_2700 = class296.next;
		return class296;
	}

	public Class298() {
		/* empty */
	}

	Class298(NodeDeque class155) {
		aClass155_2698 = class155;
	}

	static final void method3237(int i, byte i_17_, int i_18_, int i_19_) {
		if (i_17_ <= -18)
			Class41_Sub18.aByteArrayArrayArray3786 = new byte[i_18_][i][i_19_];
	}
}
