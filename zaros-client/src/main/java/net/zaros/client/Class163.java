package net.zaros.client;
/* Class163 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class163 {
	static Class161 aClass161_1677;
	static int[] anIntArray1678 = {0, 1, 2, 3, 4, 5, 6, 14};
	static NodeDeque aClass155_1679;

	static final int method1627(byte[][] is, byte[] is_0_, int[] is_1_, int i, int i_2_, byte i_3_, byte[][] is_4_, int[] is_5_) {
		int i_6_ = is_5_[i_2_];
		int i_7_ = i_6_ + is_1_[i_2_];
		int i_8_ = is_5_[i];
		int i_9_ = is_1_[i] + i_8_;
		int i_10_ = i_6_;
		if (i_8_ > i_6_)
			i_10_ = i_8_;
		int i_11_ = i_7_;
		if (i_7_ > i_9_)
			i_11_ = i_9_;
		int i_12_ = is_0_[i_2_] & 0xff;
		if ((is_0_[i] & 0xff) < i_12_)
			i_12_ = is_0_[i] & 0xff;
		byte[] is_13_ = is[i_2_];
		byte[] is_14_ = is_4_[i];
		int i_15_ = -i_6_ + i_10_;
		int i_16_ = i_10_ - i_8_;
		if (i_3_ != -13)
			method1627(null, null, null, -10, 59, (byte) -93, null, null);
		for (int i_17_ = i_10_; i_17_ < i_11_; i_17_++) {
			int i_18_ = is_13_[i_15_++] + is_14_[i_16_++];
			if (i_18_ < i_12_)
				i_12_ = i_18_;
		}
		return -i_12_;
	}

	public static void method1628(int i) {
		int i_19_ = -13 / ((87 - i) / 37);
		anIntArray1678 = null;
		aClass161_1677 = null;
		aClass155_1679 = null;
	}

	static final byte[] method1629(byte i, byte[] is, int i_20_, int i_21_) {
		byte[] is_22_;
		if (i_21_ <= 0)
			is_22_ = is;
		else {
			is_22_ = new byte[i_20_];
			for (int i_23_ = 0; i_20_ > i_23_; i_23_++)
				is_22_[i_23_] = is[i_23_ + i_21_];
		}
		Class283 class283 = new Class283();
		class283.method2359(false);
		class283.method2360((byte) 117, (long) (i_20_ * 8), is_22_);
		byte[] is_24_ = new byte[64];
		class283.method2357(18993, is_24_, 0);
		if (i > -11)
			return null;
		return is_24_;
	}

	static {
		aClass161_1677 = new Class161(7, 0, 1, 1);
		aClass155_1679 = new NodeDeque();
	}
}
