package net.zaros.client;

import net.zaros.client.logic.clans.settings.ClanSettings;

/* Class368_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub2 extends Class368 {
	private int anInt5425;
	static ClanSettings unaffinedClanSettings;
	static boolean aBoolean5427;
	static String additionalInfo = null;
	private String aString5429;
	static int anInt5430 = 0;
	static boolean aBoolean5431;

	static final Packet method3811(boolean bool) {
		Packet class296_sub17 = new Packet(518);
		Class41_Sub28.anIntArray3816 = new int[4];
		Class41_Sub28.anIntArray3816[3] = (int) (Math.random() * 9.9999999E7);
		Class41_Sub28.anIntArray3816[2] = (int) (Math.random() * 9.9999999E7);
		Class41_Sub28.anIntArray3816[1] = (int) (Math.random() * 9.9999999E7);
		if (bool != true)
			return null;
		Class41_Sub28.anIntArray3816[0] = (int) (Math.random() * 9.9999999E7);
		class296_sub17.p1(10);
		class296_sub17.p4(Class41_Sub28.anIntArray3816[0]);
		class296_sub17.p4(Class41_Sub28.anIntArray3816[1]);
		class296_sub17.p4(Class41_Sub28.anIntArray3816[2]);
		class296_sub17.p4(Class41_Sub28.anIntArray3816[3]);
		return class296_sub17;
	}

	final void method3807(byte i) {
		if (Class399.someXTEARelatedFileID != -1)
			CS2Executor.method1535(Class399.someXTEARelatedFileID, aString5429, anInt5425);
		int i_0_ = 51 % ((52 - i) / 52);
	}

	static final void method3812(int i) {
		za_Sub1.anInt6554 = 0;
		Class187.someAppereanceBuffer = null;
		Class296_Sub40.currentMapLoadType = Class338_Sub2.mapLoadType = 0;
		Class399.someXTEARelatedFileID = -1;
		Class296_Sub36.someSecondaryXteaBuffer = null;
		HardReferenceWrapper.method2795((byte) -126);
		Class41_Sub6.anInt3760 = 0;
		Class41_Sub26.worldBaseY = 0;
		Class97.anInt1050 = 0;
		Class206.worldBaseX = 0;
		for (int i_1_ = 0; i_1_ < Class338_Sub2.aClass225Array5200.length; i_1_++)
			Class338_Sub2.aClass225Array5200[i_1_] = null;
		PlayerUpdate.clearPlayers();
		for (int i_2_ = 0; i_2_ < 2048; i_2_++)
			PlayerUpdate.visiblePlayers[i_2_] = null;
		Class367.npcsCount = i;
		Class41_Sub18.localNpcs.clear();
		Class368_Sub23.npcNodeCount = 0;
		Class296_Sub11.aClass263_4647.clear();
		BillboardRaw.method883((byte) -124);
		Class296_Sub45_Sub2.interfaceCounter = 0;
		Class16_Sub3_Sub1.configsRegister.resetConfigurations(i);
		Class166.aClass296_Sub54_1692 = null;
		Class296_Sub51_Sub2.affinedClanSettings = null;
		GameClient.aClass296_Sub48_3717 = null;
		unaffinedClanSettings = null;
		Class338_Sub3_Sub3_Sub2.aLong6628 = 0L;
		EmissiveTriangle.affinedClanChannel = null;
	}

	Class368_Sub2(Packet class296_sub17) {
		super(class296_sub17);
		aString5429 = class296_sub17.gstr();
		anInt5425 = class296_sub17.g2();
	}

	public static void method3813(int i) {
		unaffinedClanSettings = null;
		additionalInfo = null;
		if (i != 24842)
			additionalInfo = null;
	}
}
