package net.zaros.client;

/* Class296_Sub45_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub45_Sub2 extends Class296_Sub45 {
	private int anInt6273;
	private NodeDeque aClass155_6274 = new NodeDeque();
	static SubInPacket aClass260_6275 = new SubInPacket(1, 2);
	static Connection aClass204_6276 = new Connection();
	static Connection aClass204_6277 = new Connection();
	static Connection[] aClass204Array6278 = {aClass204_6277, aClass204_6276};
	static IncomingPacket aClass231_6279 = new IncomingPacket(62, -1);
	private boolean aBoolean6280;
	private boolean aBoolean6281;
	private int anInt6282 = 256;
	private int anInt6283;
	private int anInt6284 = 256;
	static int anInt6285 = -1;
	static int interfaceCounter = 0;
	private int anInt6287;
	static int anInt6288;

	final synchronized int method2976(int i) {
		if (i < 104)
			aClass155_6274 = null;
		return anInt6283;
	}

	final synchronized void method2977(boolean bool) {
		aBoolean6280 = true;
		if (bool)
			method2979((byte) 82, false);
	}

	final Class296_Sub45 method2930() {
		return null;
	}

	public static void method2978(int i) {
		aClass260_6275 = null;
		aClass231_6279 = null;
		if (i != -6298)
			method2985(81, 14, -78, 113, -97);
		aClass204_6277 = null;
		aClass204_6276 = null;
		aClass204Array6278 = null;
	}

	final synchronized void method2979(byte i, boolean bool) {
		if (i < -30)
			aBoolean6281 = bool;
	}

	final synchronized void method2934(int[] is, int i, int i_0_) {
		if (!aBoolean6281) {
			if (method2986((byte) 93) == null) {
				if (aBoolean6280) {
					this.unlink();
					Class369.aClass301_3133.clear();
				}
			} else {
				int i_1_ = i_0_ + i;
				if (HardReferenceWrapper.aBoolean6695)
					i_1_ <<= 1;
				int i_2_ = 0;
				int i_3_ = 0;
				if (anInt6273 == 2)
					i_3_ = 1;
				while (i_1_ > i) {
					Class296_Sub39_Sub7 class296_sub39_sub7 = method2986((byte) 122);
					if (class296_sub39_sub7 == null)
						break;
					short[][] is_4_ = class296_sub39_sub7.aShortArrayArray6155;
					while (i < i_1_) {
						if (anInt6287 >= is_4_[0].length)
							break;
						if (!HardReferenceWrapper.aBoolean6695)
							is[i++] += (is_4_[i_2_][anInt6287] * anInt6282 + is_4_[i_3_][anInt6287] * anInt6284);
						else {
							is[i++] = is_4_[i_2_][anInt6287] * anInt6282;
							is[i++] = anInt6284 * is_4_[i_3_][anInt6287];
						}
						anInt6287++;
					}
					if (anInt6287 >= is_4_[0].length)
						method2982(19);
				}
			}
		}
	}

	static final void method2980(boolean bool) {
		if (Class296_Sub30.anInt4820 <= 1)
			Class343_Sub1.aClass296_Sub50_5282.method3060(2, 66, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub12_4992);
		else
			Class343_Sub1.aClass296_Sub50_5282.method3060(4, 76, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub12_4992);
		if (bool != true)
			interfaceCounter = 35;
	}

	final void method2981(byte i, int i_5_) {
		anInt6282 = i_5_;
		anInt6284 = i_5_;
		if (i <= 52)
			method2934(null, 97, 76);
	}

	private final synchronized void method2982(int i) {
		if (i == 19) {
			Class296_Sub39_Sub7 class296_sub39_sub7 = method2986((byte) 118);
			if (class296_sub39_sub7 != null) {
				class296_sub39_sub7.unlink();
				anInt6287 = 0;
				anInt6283--;
				Class369.aClass301_3133.put(class296_sub39_sub7.method2817(929566272), class296_sub39_sub7);
			}
		}
	}

	static final void method2983(int i, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_) {
		int i_11_ = 0;
		int i_12_ = i_9_;
		int i_13_ = i_10_ * i_10_;
		int i_14_ = i_9_ * i_9_;
		int i_15_ = -7 / ((i_6_ - 45) / 44);
		int i_16_ = i_14_ << 1;
		int i_17_ = i_13_ << 1;
		int i_18_ = i_9_ << 1;
		int i_19_ = i_13_ * (-i_18_ + 1) + i_16_;
		int i_20_ = i_14_ - (i_18_ - 1) * i_17_;
		int i_21_ = i_13_ << 2;
		int i_22_ = i_14_ << 2;
		int i_23_ = ((i_11_ << 1) + 3) * i_16_;
		int i_24_ = i_17_ * ((i_12_ << 1) - 3);
		int i_25_ = i_22_ * (i_11_ + 1);
		Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_7_]), i_10_ + i, (byte) 126, i_8_, i - i_10_);
		int i_26_ = i_21_ * (i_12_ - 1);
		while (i_12_ > 0) {
			if (i_19_ < 0) {
				while (i_19_ < 0) {
					i_20_ += i_25_;
					i_19_ += i_23_;
					i_25_ += i_22_;
					i_11_++;
					i_23_ += i_22_;
				}
			}
			if (i_20_ < 0) {
				i_20_ += i_25_;
				i_19_ += i_23_;
				i_25_ += i_22_;
				i_11_++;
				i_23_ += i_22_;
			}
			i_20_ -= i_24_;
			i_19_ -= i_26_;
			i_26_ -= i_21_;
			i_24_ -= i_21_;
			i_12_--;
			int i_27_ = i_7_ - i_12_;
			int i_28_ = i_12_ + i_7_;
			int i_29_ = i_11_ + i;
			int i_30_ = -i_11_ + i;
			Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_27_]), i_29_, (byte) -59, i_8_, i_30_);
			Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_28_]), i_29_, (byte) 125, i_8_, i_30_);
		}
	}

	final int method2929() {
		return 1;
	}

	final Class296_Sub39_Sub7 method2984(int i, double d, int i_31_) {
		if (i != 1903774304)
			anInt6288 = -3;
		long l = (long) (i_31_ | anInt6273 << 0);
		Class296_Sub39_Sub7 class296_sub39_sub7 = ((Class296_Sub39_Sub7) Class369.aClass301_3133.get(l));
		if (class296_sub39_sub7 == null)
			class296_sub39_sub7 = new Class296_Sub39_Sub7(new short[anInt6273][i_31_], d);
		else {
			class296_sub39_sub7.aDouble6156 = d;
			Class369.aClass301_3133.remove(l);
		}
		return class296_sub39_sub7;
	}

	static final void method2985(int i, int i_32_, int i_33_, int i_34_, int i_35_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i << 32 | (long) i_35_, 19);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.anInt6145 = i_34_;
		class296_sub39_sub5.intParam = i_32_;
		if (i_33_ != 62)
			method2985(-81, -56, -9, 17, -125);
	}

	private final synchronized Class296_Sub39_Sub7 method2986(byte i) {
		if (i <= 70)
			return null;
		return (Class296_Sub39_Sub7) aClass155_6274.removeFirst((byte) 117);
	}

	final synchronized void method2987(byte i, Class296_Sub39_Sub7 class296_sub39_sub7) {
		for (/**/; anInt6283 >= 100; anInt6283--)
			aClass155_6274.method1573(1);
		if (i >= -109)
			anInt6284 = 53;
		aClass155_6274.addLast((byte) 116, class296_sub39_sub7);
		anInt6283++;
	}

	final synchronized void method2933(int i) {
		if (!aBoolean6281) {
			for (;;) {
				Class296_Sub39_Sub7 class296_sub39_sub7 = method2986((byte) 105);
				if (class296_sub39_sub7 == null) {
					if (aBoolean6280) {
						this.unlink();
						Class369.aClass301_3133.clear();
					}
					break;
				}
				if ((-anInt6287 + class296_sub39_sub7.aShortArrayArray6155[0].length) > i) {
					anInt6287 += i;
					break;
				}
				i -= (class296_sub39_sub7.aShortArrayArray6155[0].length - anInt6287);
				method2982(19);
			}
		}
	}

	final Class296_Sub45 method2932() {
		return null;
	}

	final synchronized double method2988(byte i) {
		if (anInt6283 < 1)
			return -1.0;
		if (i != 59)
			return -0.6889792015952905;
		Class296_Sub39_Sub7 class296_sub39_sub7 = (Class296_Sub39_Sub7) aClass155_6274.removeFirst((byte) 123);
		if (class296_sub39_sub7 == null)
			return -1.0;
		return (class296_sub39_sub7.aDouble6156 - (double) ((float) (class296_sub39_sub7.aShortArrayArray6155[0]).length / (float) Class298.anInt2699));
	}

	Class296_Sub45_Sub2(int i) {
		anInt6283 = 0;
		anInt6273 = i;
	}
}
