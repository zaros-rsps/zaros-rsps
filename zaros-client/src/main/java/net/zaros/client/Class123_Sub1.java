package net.zaros.client;
/* Class123_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

class Class123_Sub1 extends Class123 {
	private int anInt3904;
	private int anInt3905;
	private int anInt3906;
	private int anInt3907;
	private int anInt3908;
	static int[][][] anIntArrayArrayArray3909 = new int[2][][];
	private byte[] aByteArray3910;
	private int anInt3911;
	private int anInt3912;
	private int anInt3913;

	Class123_Sub1(int i, int i_0_, int i_1_, int i_2_, int i_3_, float f, float f_4_, float f_5_) {
		super(i, i_0_, i_1_, i_2_, i_3_);
		anInt3907 = (int) (f_5_ * 4096.0F);
		anInt3904 = (int) (f_4_ * 4096.0F);
		anInt3906 = anInt3905 = (int) (Math.pow(0.5, (double) -f) * 4096.0);
	}

	final void method1053(boolean bool, int i, int i_6_) {
		if (i != 0) {
			anInt3908 = anInt3907 * anInt3913 >> 12;
			anInt3913 = anInt3904 + (i_6_ < 0 ? i_6_ : i_6_);
			if (anInt3908 < 0)
				anInt3908 = 0;
			else if (anInt3908 > 4096)
				anInt3908 = 4096;
			anInt3913 = anInt3913 * anInt3913 >> 12;
			anInt3913 = anInt3908 * anInt3913 >> 12;
			anInt3912 += anInt3906 * anInt3913 >> 12;
			anInt3906 = anInt3905 * anInt3906 >> 12;
		} else {
			anInt3913 = -(i_6_ >= 0 ? i_6_ : -i_6_) + anInt3904;
			anInt3908 = 4096;
			anInt3913 = anInt3913 * anInt3913 >> 12;
			anInt3912 = anInt3913;
		}
		if (bool != true)
			method1055(false);
	}

	void method1058(byte i, byte i_7_, int i_8_) {
		int i_9_ = -7 / ((i + 24) / 42);
		aByteArray3910[i_8_] = i_7_;
	}

	public static void method1059(boolean bool) {
		if (bool == true)
			anIntArrayArrayArray3909 = null;
	}

	final void method1057(byte i) {
		anInt3912 >>= 4;
		anInt3906 = anInt3905;
		if (anInt3912 < 0)
			anInt3912 = 0;
		else if (anInt3912 > 255)
			anInt3912 = 255;
		method1058((byte) -123, (byte) anInt3912, anInt3911++);
		anInt3912 = 0;
		int i_10_ = 27 % ((37 - i) / 41);
	}

	final void method1055(boolean bool) {
		if (bool)
			anInt3908 = -56;
		anInt3911 = 0;
		anInt3912 = 0;
	}
}
