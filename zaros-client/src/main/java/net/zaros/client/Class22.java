package net.zaros.client;

/* Class22 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class22 implements Runnable {
	static int anInt249;
	volatile Class381[] aClass381Array250 = new Class381[2];
	Class398 aClass398_251;
	static SubInPacket aClass260_252 = new SubInPacket(8, 8);
	volatile boolean aBoolean253 = false;
	volatile boolean aBoolean254 = false;

	public final void run() {
		aBoolean254 = true;
		try {
			while (!aBoolean253) {
				for (int i = 0; i < 2; i++) {
					Class381 class381 = aClass381Array250[i];
					if (class381 != null)
						class381.method3989((byte) -49);
				}
				Class106_Sub1.method942(10L, 0);
				Class362.method3758(aClass398_251, true, null);
			}
		} catch (Exception exception) {
			Class219_Sub1.method2062(null, (byte) 89, exception);
		} finally {
			aBoolean254 = false;
		}
	}

	public static void method290(int i) {
		if (i == 2)
			aClass260_252 = null;
	}
}
