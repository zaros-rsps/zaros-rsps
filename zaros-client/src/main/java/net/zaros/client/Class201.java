package net.zaros.client;

/* Class201 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Frame;

import com.ms.com.IUnknown;
import com.ms.directX.DDSurfaceDesc;
import com.ms.directX.DirectDraw;
import com.ms.directX.IEnumModesCallback;
import com.ms.win32.User32;

final class Class201 implements IEnumModesCallback {
	private DirectDraw aDirectDraw2038 = new DirectDraw();
	private static int anInt2039;
	private static int[] anIntArray2040;

	public final void method1955(DDSurfaceDesc ddsurfacedesc, IUnknown iunknown) {
		if (anIntArray2040 == null)
			anInt2039 += 4;
		else {
			anIntArray2040[anInt2039++] = ddsurfacedesc.width;
			anIntArray2040[anInt2039++] = ddsurfacedesc.height;
			anIntArray2040[anInt2039++] = ddsurfacedesc.rgbBitCount;
			anIntArray2040[anInt2039++] = ddsurfacedesc.refreshRate;
		}
	}

	final void method1956(int i, Frame frame, int i_0_, byte i_1_, int i_2_, int i_3_) {
		frame.setVisible(true);
		Object wcomponentpeer = ReflectionUtil.getPeer(frame);
		int i_4_ = (int) ReflectionUtil.getHwnd(wcomponentpeer);
		User32.SetWindowLong(i_4_, -16, -2147483648);
		User32.SetWindowLong(i_4_, -20, 8);
		aDirectDraw2038.setCooperativeLevel(frame, 17);
		aDirectDraw2038.setDisplayMode(i_0_, i_2_, i, i_3_, 0);
		frame.setBounds(0, 0, i_0_, i_2_);
		frame.toFront();
		frame.requestFocus();
	}

	final void method1957(Frame frame, int i) {
		aDirectDraw2038.restoreDisplayMode();
		aDirectDraw2038.setCooperativeLevel(frame, i);
	}

	final int[] method1958(byte i) {
		aDirectDraw2038.enumDisplayModes(0, null, null, this);
		anIntArray2040 = new int[anInt2039];
		anInt2039 = 0;
		aDirectDraw2038.enumDisplayModes(0, null, null, this);
		if (i <= 68)
			method1957(null, -28);
		int[] is = anIntArray2040;
		anInt2039 = 0;
		anIntArray2040 = null;
		return is;
	}

	public Class201() {
		aDirectDraw2038.initialize(null);
	}
}
