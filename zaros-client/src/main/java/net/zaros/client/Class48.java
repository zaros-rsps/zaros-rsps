package net.zaros.client;

/* Class48 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class48 {
	static int[] anIntArray454;
	static long aLong455 = 0L;

	static final void method600(int i) {
		if (!Class318.aBoolean2814) {
			Class262.aBoolean1324 = Class296_Sub45_Sub2.anInt6285 != -1 && Class230.anInt2210 >= Class296_Sub45_Sub2.anInt6285 || Class230.anInt2210 * 16 + (!SeekableFile.aBoolean2397 ? 22 : 26) > Class384.anInt3254;
		}
		Class152.aClass155_1569.method1581(327680);
		Class41_Sub21.aClass155_3799.method1581(327680);
		for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeFirst((byte) 120); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeNext(1001)) {
			int i_0_ = class296_sub39_sub9.anInt6165;
			if (i_0_ < 1000) {
				class296_sub39_sub9.unlink();
				if (i_0_ != 49 && i_0_ != 48 && i_0_ != 59 && i_0_ != 10 && i_0_ != 5 && i_0_ != 52 && i_0_ != 20) {
					Class152.aClass155_1569.addLast((byte) -56, class296_sub39_sub9);
				} else {
					Class41_Sub21.aClass155_3799.addLast((byte) 100, class296_sub39_sub9);
				}
			}
		}
		Class152.aClass155_1569.method1571(HardReferenceWrapper.aClass155_6698, 2);
		Class41_Sub21.aClass155_3799.method1571(HardReferenceWrapper.aClass155_6698, 2);
		if (Class230.anInt2210 > 1) {
			if (!StaticMethods.aBoolean5054 || !Class2.aClass166_66.method1637(81, 79) || Class230.anInt2210 <= 2) {
				Class216.aClass296_Sub39_Sub9_2115 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.aClass296_1586.prev;
			} else {
				Class216.aClass296_Sub39_Sub9_2115 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.aClass296_1586.prev.prev;
			}
			StaticMethods.aClass296_Sub39_Sub9_6085 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.aClass296_1586.prev;
		} else {
			StaticMethods.aClass296_Sub39_Sub9_6085 = null;
			Class216.aClass296_Sub39_Sub9_2115 = null;
		}
		if (i < 88) {
			method600(-25);
		}
		int i_1_ = -1;
		Class296_Sub34 class296_sub34 = (Class296_Sub34) Class403.aClass155_3379.removeFirst((byte) 127);
		if (class296_sub34 != null) {
			i_1_ = class296_sub34.method2735(-82);
		}
		do {
			if (!Class318.aBoolean2814) {
				if (i_1_ == 0 && (Class296_Sub9_Sub1.anInt5982 == 1 && Class230.anInt2210 > 2 || Class189_Sub1.method1904(-7703))) {
					i_1_ = 2;
				}
				if (i_1_ == 2 && Class230.anInt2210 > 0 && class296_sub34 != null) {
					if (InvisiblePlayer.aClass51_1982 == null && Class2.anInt59 == 0) {
						ConfigsRegister.method588(22, class296_sub34.method2736(-6), class296_sub34.method2732(-6));
					} else {
						Class399.anInt3357 = 2;
					}
				}
				if (i_1_ == 0) {
					if (Class216.aClass296_Sub39_Sub9_2115 != null) {
						Class219_Sub1.method2067(true);
					} else if (Class127.aBoolean1304) {
						Class285.method2368(0);
					}
				}
				if (InvisiblePlayer.aClass51_1982 == null && Class2.anInt59 == 0) {
					Class399.anInt3357 = 0;
					Class296_Sub17_Sub2.aClass296_Sub39_Sub9_6046 = null;
				}
			} else {
				if (i_1_ == -1) {
					int i_2_ = Class84.aClass189_924.method1895((byte) -55);
					int i_3_ = Class84.aClass189_924.method1897(0);
					boolean bool = false;
					if (Class241.aClass296_Sub39_Sub1_2302 != null) {
						if (Class41_Sub5.anInt3753 - 10 <= i_2_ && i_2_ <= StaticMethods.anInt3152 + 10 + Class41_Sub5.anInt3753 && Class270.anInt2510 - 10 <= i_3_ && i_3_ <= Class270.anInt2510 + Class95.anInt1036 + 10) {
							bool = true;
						} else {
							StaticMethods.method2465(108);
						}
					}
					if (!bool) {
						if (i_2_ >= Class252.anInt2382 - 10 && Class81.anInt3666 + Class252.anInt2382 + 10 >= i_2_ && Class100.anInt1067 - 10 <= i_3_ && Class296_Sub39_Sub20.anInt6254 + Class100.anInt1067 + 10 >= i_3_) {
							if (Class262.aBoolean1324) {
								int i_4_ = -1;
								int i_5_ = -1;
								for (int i_6_ = 0; i_6_ < Class239.anInt2254; i_6_++) {
									if (SeekableFile.aBoolean2397) {
										int i_7_ = Class100.anInt1067 + 33 + i_6_ * 16;
										if (i_3_ > i_7_ - 13 && i_3_ < i_7_ + 4) {
											i_5_ = i_7_ - 13;
											i_4_ = i_6_;
											break;
										}
									} else {
										int i_8_ = Class100.anInt1067 - (-31 - i_6_ * 16);
										if (i_3_ > i_8_ - 13 && i_8_ + 3 > i_3_) {
											i_5_ = i_8_ - 13;
											i_4_ = i_6_;
											break;
										}
									}
								}
								if (i_4_ != -1) {
									int i_9_ = 0;
									Class83 class83 = new Class83(Class152.aClass145_1558);
									for (Class296_Sub39_Sub1 class296_sub39_sub1 = (Class296_Sub39_Sub1) class83.method812(0); class296_sub39_sub1 != null; class296_sub39_sub1 = (Class296_Sub39_Sub1) class83.method810((byte) 100)) {
										if (i_9_ == i_4_) {
											if (class296_sub39_sub1.anInt6117 > 1) {
												Class63.method702(16, i_5_, class296_sub39_sub1, i_3_);
											}
											break;
										}
										i_9_++;
									}
								}
							}
						} else {
							Class41_Sub9.method427((byte) -110);
						}
					}
				}
				if (i_1_ != 0) {
					break;
				}
				int i_10_ = class296_sub34.method2732(-6);
				int i_11_ = class296_sub34.method2736(-6);
				if (Class241.aClass296_Sub39_Sub1_2302 == null || Class41_Sub5.anInt3753 > i_10_ || StaticMethods.anInt3152 + Class41_Sub5.anInt3753 < i_10_ || Class270.anInt2510 > i_11_ || Class95.anInt1036 + Class270.anInt2510 < i_11_) {
					if (Class252.anInt2382 <= i_10_ && Class252.anInt2382 + Class81.anInt3666 >= i_10_ && Class100.anInt1067 <= i_11_ && Class296_Sub39_Sub20.anInt6254 + Class100.anInt1067 >= i_11_) {
						if (Class262.aBoolean1324) {
							int i_12_ = -1;
							for (int i_13_ = 0; Class239.anInt2254 > i_13_; i_13_++) {
								if (SeekableFile.aBoolean2397) {
									int i_14_ = Class100.anInt1067 + 33 + i_13_ * 16;
									if (i_11_ > i_14_ - 13 && i_11_ < i_14_ + 4) {
										i_12_ = i_13_;
										break;
									}
								} else {
									int i_15_ = Class100.anInt1067 + 31 + i_13_ * 16;
									if (i_11_ > i_15_ - 13 && i_15_ + 3 > i_11_) {
										i_12_ = i_13_;
										break;
									}
								}
							}
							if (i_12_ != -1) {
								int i_16_ = 0;
								Class83 class83 = new Class83(Class152.aClass145_1558);
								for (Class296_Sub39_Sub1 class296_sub39_sub1 = (Class296_Sub39_Sub1) class83.method812(0); class296_sub39_sub1 != null; class296_sub39_sub1 = (Class296_Sub39_Sub1) class83.method810((byte) 121)) {
									if (i_16_ == i_12_) {
										Class97.method873(i_11_, i_10_, -85, (Class296_Sub39_Sub9) class296_sub39_sub1.aClass145_6119.head.queue_next);
										Class41_Sub9.method427((byte) -110);
										break;
									}
									i_16_++;
								}
							}
						} else {
							int i_17_ = -1;
							for (int i_18_ = 0; i_18_ < Class230.anInt2210; i_18_++) {
								if (SeekableFile.aBoolean2397) {
									int i_19_ = (Class230.anInt2210 + -i_18_ - 1) * 16 + Class100.anInt1067 + 33;
									if (i_19_ - 13 < i_11_ && i_11_ < i_19_ + 4) {
										i_17_ = i_18_;
									}
								} else {
									int i_20_ = Class100.anInt1067 + (-i_18_ + Class230.anInt2210 - 1) * 16 + 31;
									if (i_11_ > i_20_ - 13 && i_20_ + 3 > i_11_) {
										i_17_ = i_18_;
									}
								}
							}
							if (i_17_ != -1) {
								int i_21_ = 0;
								Class298 class298 = new Class298(HardReferenceWrapper.aClass155_6698);
								for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) class298.method3236((byte) 70); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) class298.method3235(true)) {
									if (i_21_ == i_17_) {
										Class97.method873(i_11_, i_10_, 99, class296_sub39_sub9);
										break;
									}
									i_21_++;
								}
							}
							Class41_Sub9.method427((byte) -110);
						}
					}
					break;
				}
				int i_22_ = -1;
				for (int i_23_ = 0; Class241.aClass296_Sub39_Sub1_2302.anInt6117 > i_23_; i_23_++) {
					if (SeekableFile.aBoolean2397) {
						int i_24_ = Class270.anInt2510 + i_23_ * 16 + 33;
						if (i_11_ > i_24_ - 13 && i_24_ + 4 > i_11_) {
							i_22_ = i_23_;
						}
					} else {
						int i_25_ = Class270.anInt2510 - (-(i_23_ * 16) - 31);
						if (i_25_ - 13 < i_11_ && i_25_ + 3 > i_11_) {
							i_22_ = i_23_;
						}
					}
				}
				if (i_22_ != -1) {
					int i_26_ = 0;
					Class83 class83 = new Class83(Class241.aClass296_Sub39_Sub1_2302.aClass145_6119);
					for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) class83.method812(0); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) class83.method810((byte) 122)) {
						if (i_26_ == i_22_) {
							Class97.method873(i_11_, i_10_, -42, class296_sub39_sub9);
							break;
						}
						i_26_++;
					}
				}
				Class41_Sub9.method427((byte) -110);
			}
			break;
		} while (false);
	}

	static final void method601(int i, int i_27_) {
		if (i_27_ < 86) {
			method600(121);
		}
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask(i, 16);
		class296_sub39_sub5.insertIntoQueue();
	}

	static final void method602(int i, int i_28_, int i_29_, int i_30_, int i_31_, int i_32_, int i_33_, int i_34_, int i_35_, int i_36_) {
		if (i_32_ != 3) {
			method601(23, 91);
		}
		if (i_34_ != i_31_ || i_35_ != i_30_ || i_36_ != i || i_33_ != i_28_) {
			int i_37_ = i_31_;
			int i_38_ = i_35_;
			int i_39_ = i_31_ * 3;
			int i_40_ = i_35_ * 3;
			int i_41_ = i_34_ * 3;
			int i_42_ = i_30_ * 3;
			int i_43_ = i_36_ * 3;
			int i_44_ = i_28_ * 3;
			int i_45_ = -i_43_ + i + i_41_ - i_31_;
			int i_46_ = i_42_ + -i_44_ + i_33_ - i_35_;
			int i_47_ = i_39_ + -i_41_ + i_43_ - i_41_;
			int i_48_ = -i_42_ + i_44_ - i_42_ + i_40_;
			int i_49_ = -i_39_ + i_41_;
			int i_50_ = i_42_ - i_40_;
			for (int i_51_ = 128; i_51_ <= 4096; i_51_ += 128) {
				int i_52_ = i_51_ * i_51_ >> 12;
				int i_53_ = i_51_ * i_52_ >> 12;
				int i_54_ = i_45_ * i_53_;
				int i_55_ = i_53_ * i_46_;
				int i_56_ = i_47_ * i_52_;
				int i_57_ = i_48_ * i_52_;
				int i_58_ = i_49_ * i_51_;
				int i_59_ = i_50_ * i_51_;
				int i_60_ = i_31_ + (i_58_ + i_56_ + i_54_ >> 12);
				int i_61_ = (i_55_ + i_57_ + i_59_ >> 12) + i_35_;
				Class32.method342(i_60_, i_61_, i_29_, (byte) -119, i_37_, i_38_);
				i_37_ = i_60_;
				i_38_ = i_61_;
			}
		} else {
			Class32.method342(i, i_33_, i_29_, (byte) -119, i_31_, i_35_);
		}
	}

	public static int bitOR(int i, int i_62_) {
		return i | i_62_;
	}

	public static void method604(byte i) {
		if (i == 21) {
			anIntArray454 = null;
		}
	}

	static {
		anIntArray454 = new int[] { 16776960, 16711680, 65280, 65535, 16711935, 16777215 };
	}
}
