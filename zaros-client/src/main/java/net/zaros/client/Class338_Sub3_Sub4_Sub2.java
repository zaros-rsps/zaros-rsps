package net.zaros.client;

/* Class338_Sub3_Sub4_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub3_Sub4_Sub2 extends Class338_Sub3_Sub4 implements Interface14 {
	static int[] anIntArray6672 = new int[4096];
	private boolean aBoolean6673;
	Model aClass178_6674;
	private boolean aBoolean6675;
	private byte aByte6676;
	private r aR6677;
	private int locid;
	private Class96 aClass96_6679;
	private byte aByte6680;
	private boolean aBoolean6681;
	private boolean aBoolean6682;

	@Override
	public final void method60(byte i, ha var_ha) {
		int i_0_ = 15 % ((-23 - i) / 35);
		Object object = null;
		r var_r;
		if (aR6677 != null || !aBoolean6673) {
			var_r = aR6677;
			aR6677 = null;
		} else {
			Class188 class188 = method3579(var_ha, 262144, true, 0);
			var_r = class188 == null ? null : class188.aR1925;
		}
		if (var_r != null) {
			Class296_Sub45_Sub4.method3017(var_r, aByte5203, tileX, tileY, null);
		}
	}

	@Override
	final Class96 method3461(ha var_ha, int i) {
		if (aClass96_6679 == null) {
			aClass96_6679 = Class41_Sub21.method478(anInt5213, method3580(var_ha, 0, -1), tileX, tileY, (byte) -66);
		}
		int i_1_ = -56 / ((i - 79) / 44);
		return aClass96_6679;
	}

	@Override
	public final int method54(int i) {
		if (i != -11077) {
			aBoolean6682 = false;
		}
		return aByte6676;
	}

	@Override
	public final int method57(byte i) {
		if (i < 83) {
			return 91;
		}
		return locid;
	}

	@Override
	final void method3472(byte i) {
		int i_2_ = 49 % ((i + 56) / 38);
		aBoolean6675 = false;
		if (aClass178_6674 != null) {
			aClass178_6674.s(aClass178_6674.ua() & ~0x10000);
		}
	}

	@Override
	final boolean method3475(int i, int i_3_, ha var_ha, int i_4_) {
		if (i_3_ >= -48) {
			aBoolean6681 = false;
		}
		Model class178 = method3580(var_ha, 131072, -1);
		if (class178 != null) {
			Class373 class373 = var_ha.f();
			class373.method3902(tileX, anInt5213, tileY);
			if (!Class296_Sub39_Sub10.aBoolean6177) {
				return class178.method1732(i_4_, i, class373, false, 0);
			}
			return class178.method1731(i_4_, i, class373, false, 0, ModeWhat.anInt1192);
		}
		return false;
	}

	@Override
	final int method3462(byte i) {
		if (i != 28) {
			return 31;
		}
		if (aClass178_6674 == null) {
			return 0;
		}
		return aClass178_6674.ma();
	}

	@Override
	final boolean method3468(int i) {
		return aBoolean6675;
	}

	@Override
	public final void method56(ha var_ha, byte i) {
		Object object = null;
		if (i == -117) {
			r var_r;
			if (aR6677 != null || !aBoolean6673) {
				var_r = aR6677;
				aR6677 = null;
			} else {
				Class188 class188 = method3579(var_ha, 262144, true, 0);
				var_r = class188 == null ? null : class188.aR1925;
			}
			if (var_r != null) {
				Class296_Sub39_Sub20_Sub2.method2910(var_r, aByte5203, tileX, tileY, null);
			}
		}
	}

	@Override
	final boolean method3469(int i) {
		if (i < 82) {
			aBoolean6682 = true;
		}
		if (aClass178_6674 != null) {
			return aClass178_6674.F();
		}
		return false;
	}

	@Override
	final Class338_Sub2 method3473(ha var_ha, byte i) {
		if (aClass178_6674 == null) {
			return null;
		}
		Class373 class373 = var_ha.f();
		class373.method3902(tileX, anInt5213, tileY);
		Class338_Sub2 class338_sub2 = Class144.method1481(1, true, aBoolean6681);
		if (i > -84) {
			method3581(null, (byte) -80);
		}
		if (!Class296_Sub39_Sub10.aBoolean6177) {
			aClass178_6674.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
		} else {
			aClass178_6674.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
		}
		return class338_sub2;
	}

	private final Class188 method3579(ha var_ha, int i, boolean bool, int i_5_) {
		if (i_5_ != 0) {
			return null;
		}
		ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(locid);
		s var_s;
		s var_s_6_;
		if (aBoolean6682) {
			var_s = Class52.aSArray636[aByte5203];
			var_s_6_ = Class244.aSArray2320[0];
		} else {
			var_s = Class244.aSArray2320[aByte5203];
			if (aByte5203 >= 3) {
				var_s_6_ = null;
			} else {
				var_s_6_ = Class244.aSArray2320[aByte5203 + 1];
			}
		}
		return class70.method750(var_s, var_s_6_, var_ha, bool, -954198435, tileY, tileX, i, null, aByte6680, anInt5213, aByte6676);
	}

	@Override
	public final boolean method55(byte i) {
		if (i != -57) {
			anIntArray6672 = null;
		}
		return aBoolean6673;
	}

	@Override
	final boolean method3459(int i) {
		if (i != 0) {
			aByte6680 = (byte) 101;
		}
		if (aClass178_6674 != null) {
			if (aClass178_6674.r()) {
				return false;
			}
			return true;
		}
		return true;
	}

	Class338_Sub3_Sub4_Sub2(ha var_ha, ObjectDefinition class70, int i, int i_7_, int i_8_, int i_9_, int i_10_, boolean bool, int i_11_, int i_12_, boolean bool_13_) {
		super(i_8_, i_9_, i_10_, i, i_7_, Class226.method2089(i_11_, (byte) -118, i_12_));
		locid = class70.ID;
		tileY = i_10_;
		aByte6676 = (byte) i_11_;
		aBoolean6675 = bool_13_;
		aBoolean6682 = bool;
		tileX = i_8_;
		aBoolean6681 = class70.interactonType != 0 && !bool;
		aByte6680 = (byte) i_12_;
		aBoolean6673 = var_ha.B() && class70.aBoolean779 && !aBoolean6682 && Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method465(124) != 0;
		int i_14_ = 2048;
		if (aBoolean6675) {
			i_14_ |= 0x10000;
		}
		Class188 class188 = method3579(var_ha, i_14_, aBoolean6673, 0);
		if (class188 != null) {
			aR6677 = class188.aR1925;
			aClass178_6674 = class188.aClass178_1926;
			if (aBoolean6675) {
				aClass178_6674 = aClass178_6674.method1728((byte) 0, i_14_, false);
			}
		}
	}

	@Override
	public final int method59(int i) {
		if (i < 16) {
			method3581(null, (byte) 39);
		}
		return aByte6680;
	}

	@Override
	public final void method58(int i) {
		if (aClass178_6674 != null) {
			aClass178_6674.method1722();
		}
		if (i != -19727) {
			method3581(null, (byte) 43);
		}
	}

	@Override
	final void method3460(int i, ha var_ha) {
		int i_15_ = 110 / ((i + 41) / 62);
	}

	private final Model method3580(ha var_ha, int i, int i_16_) {
		if (aClass178_6674 != null && var_ha.e(aClass178_6674.ua(), i) == 0) {
			return aClass178_6674;
		}
		if (i_16_ != -1) {
			method3466((byte) -11);
		}
		Class188 class188 = method3579(var_ha, i, false, 0);
		if (class188 != null) {
			return class188.aClass178_1926;
		}
		return null;
	}

	@Override
	final void method3467(int i, int i_17_, Class338_Sub3 class338_sub3, boolean bool, int i_18_, int i_19_, ha var_ha) {
		int i_20_ = -125 % ((i_19_ - 20) / 48);
		if (!(class338_sub3 instanceof Class338_Sub3_Sub4_Sub2)) {
			if (class338_sub3 instanceof Class338_Sub3_Sub1_Sub1) {
				Class338_Sub3_Sub1_Sub1 class338_sub3_sub1_sub1 = (Class338_Sub3_Sub1_Sub1) class338_sub3;
				if (aClass178_6674 != null && class338_sub3_sub1_sub1.aClass178_6634 != null) {
					aClass178_6674.method1736(class338_sub3_sub1_sub1.aClass178_6634, i_17_, i_18_, i, bool);
				}
			}
		} else {
			Class338_Sub3_Sub4_Sub2 class338_sub3_sub4_sub2_21_ = (Class338_Sub3_Sub4_Sub2) class338_sub3;
			if (aClass178_6674 != null && class338_sub3_sub4_sub2_21_.aClass178_6674 != null) {
				aClass178_6674.method1736(class338_sub3_sub4_sub2_21_.aClass178_6674, i_17_, i_18_, i, bool);
			}
		}
	}

	static final void method3581(ha var_ha, byte i) {
		if (i == -119 && (Class230.anInt2210 >= 2 || Class127.aBoolean1304) && InvisiblePlayer.aClass51_1982 == null) {
			String string;
			if (!Class127.aBoolean1304 || Class230.anInt2210 >= 2) {
				if (StaticMethods.aBoolean5054 && Class2.aClass166_66.method1637(81, i + 177) && Class230.anInt2210 > 2) {
					string = Class241_Sub1_Sub1.method2155(false, Class216.aClass296_Sub39_Sub9_2115);
				} else {
					Class296_Sub39_Sub9 class296_sub39_sub9 = Class216.aClass296_Sub39_Sub9_2115;
					if (class296_sub39_sub9 == null) {
						return;
					}
					string = Class241_Sub1_Sub1.method2155(false, class296_sub39_sub9);
					int[] is = null;
					if (Class295_Sub1.method2424(class296_sub39_sub9.anInt6165, (byte) 101)) {
						is = Class296_Sub39_Sub1.itemDefinitionLoader.list((int) class296_sub39_sub9.aLong6162).quests;
					} else if (class296_sub39_sub9.anInt6167 != -1) {
						is = Class296_Sub39_Sub1.itemDefinitionLoader.list(class296_sub39_sub9.anInt6167).quests;
					} else if (Class50.method609(true, class296_sub39_sub9.anInt6165)) {
						NPCNode class296_sub7 = (NPCNode) Class41_Sub18.localNpcs.get((int) class296_sub39_sub9.aLong6162);
						if (class296_sub7 != null) {
							NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
							NPCDefinition class147 = class338_sub3_sub1_sub3_sub2.definition;
							if (class147.configData != null) {
								class147 = class147.getNPCDefinitionByConfigID(Class16_Sub3_Sub1.configsRegister);
							}
							if (class147 != null) {
								is = class147.anIntArray1499;
							}
						}
					} else if (Class234.method2124((byte) 51, class296_sub39_sub9.anInt6165)) {
						ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition((int) (class296_sub39_sub9.aLong6162 >>> 32 & 0x7fffffffL));
						if (class70.transforms != null) {
							class70 = class70.method757(Class16_Sub3_Sub1.configsRegister, false);
						}
						if (class70 != null) {
							is = class70.anIntArray783;
						}
					}
					if (is != null) {
						string += Class338_Sub3_Sub1_Sub1.method3484((byte) 101, is);
					}
				}
			} else {
				string = Class228.aString2200 + TranslatableString.aClass120_1228.getTranslation(Class394.langID) + ConfigsRegister.aString3672 + " ->";
			}
			if (Class230.anInt2210 > 2) {
				string += "<col=ffffff> / " + (Class230.anInt2210 - 2) + TranslatableString.aClass120_1221.getTranslation(Class394.langID);
			}
			if (Applet_Sub1.aClass51_12 == null) {
				if (Class194.aClass51_1975 != null && Class363.runescape == Class296_Sub50.game) {
					int i_22_ = Class49.aClass55_461.a(16777215, string, (byte) -111, Class123_Sub1_Sub2.anIntArray5817, Class348.anInt3034, 0, Class379_Sub2.aRandom5683, Class44_Sub1_Sub1.aClass397Array5811, Class41_Sub5.anInt3754 + 4, ModeWhat.anInt1189 + 16);
					Class375.method3953((byte) -119, 16, Class304.aClass92_2729.method851(-94, string) + i_22_, Class41_Sub5.anInt3754 + 4, ModeWhat.anInt1189);
				}
			} else {
				Class55 class55 = Applet_Sub1.aClass51_12.method636(122, var_ha);
				if (class55 == null) {
					class55 = Class49.aClass55_461;
				}
				class55.a(-28329, Class355.anIntArray3068, Class123_Sub1_Sub2.anIntArray5817, Class28.anInt299, Applet_Sub1.aClass51_12.shadowColor, Applet_Sub1.aClass51_12.anInt623, string, Class44_Sub1_Sub1.aClass397Array5811, Applet_Sub1.aClass51_12.anInt578, Class380.anInt3206, Applet_Sub1.aClass51_12.textColour, Applet_Sub1.aClass51_12.anInt480, Class379_Sub2.aRandom5683, Class348.anInt3034, Applet_Sub1.aClass51_12.anInt600);
				Class375.method3953((byte) -123, Class355.anIntArray3068[3], Class355.anIntArray3068[2], Class355.anIntArray3068[0], Class355.anIntArray3068[1]);
			}
		}
	}

	@Override
	final int method3466(byte i) {
		if (i <= 77) {
			method3467(-31, -105, null, true, -67, -76, null);
		}
		if (aClass178_6674 != null) {
			return aClass178_6674.fa();
		}
		return 0;
	}

	public static void method3582(byte i) {
		if (i >= 47) {
			anIntArray6672 = null;
		}
	}
}
