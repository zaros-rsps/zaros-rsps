package net.zaros.client;

import net.zaros.client.configs.objtype.ObjCustomisation;
import net.zaros.client.configs.objtype.ObjType;

/* Class268 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class268 {
	int[] anIntArray2485 = null;
	static int anInt2486;
	int anInt2487;
	static int anInt2488;
	static int anInt2489;
	int anInt2490;
	int anInt2491 = 4;
	boolean aBoolean2492 = true;
	int anInt2493;
	static Js5 aClass138_2494;
	static boolean aBoolean2495;
	short[][] aShortArrayArray2496;
	int anInt2497;
	boolean aBoolean2498;
	int anInt2499;
	short[][][] aShortArrayArrayArray2500;
	int[] anIntArray2501;

	static final void method2295(int i, boolean bool) {
		if (bool != true)
			anInt2486 = 92;
		Class99.anInt1062 = i;
		synchronized (Class280.aClass113_2555) {
			Class280.aClass113_2555.clear();
		}
	}

	static final void method2296(int i) {
		if (!Class296_Sub51_Sub39.aBoolean6549) {
			Class296_Sub36.aFloat4865 += (-24.0F - Class296_Sub36.aFloat4865) / 2.0F;
			if (i != 3628)
				method2295(-72, true);
			Mesh.aBoolean1359 = true;
			Class296_Sub51_Sub39.aBoolean6549 = true;
		}
	}

	static final boolean method2297(int i, int i_0_, boolean bool) {
		if (bool != true)
			return true;
		if ((i_0_ & 0x400) == 0)
			return false;
		return true;
	}

	private final void method2298(Packet class296_sub17, byte i) {
		if (i == -115) {
			boolean bool = false;
			for (;;) {
				int i_1_ = class296_sub17.g1();
				if (i_1_ == 0)
					break;
				if (i_1_ != 1) {
					if (i_1_ != 2) {
						if (i_1_ == 3) {
							anInt2491 = class296_sub17.g1();
							anIntArray2485 = new int[anInt2491];
							anIntArray2501 = new int[anInt2491];
						} else if (i_1_ != 4) {
							if (i_1_ == 5)
								anInt2490 = class296_sub17.readUnsignedMedInt();
							else if (i_1_ == 6)
								anInt2493 = class296_sub17.readUnsignedMedInt();
							else if (i_1_ != 7) {
								if (i_1_ != 8) {
									if (i_1_ == 9)
										anInt2499 = class296_sub17.g1();
									else if (i_1_ != 10) {
										if (i_1_ == 11)
											anInt2487 = class296_sub17.g1();
									} else
										aBoolean2498 = false;
								} else
									aBoolean2492 = false;
							} else {
								aShortArrayArrayArray2500 = new short[10][4][];
								aShortArrayArray2496 = new short[10][4];
								for (int i_2_ = 0; i_2_ < 10; i_2_++) {
									for (int i_3_ = 0; i_3_ < 4; i_3_++) {
										int i_4_ = class296_sub17.g2();
										if (i_4_ == 65535)
											i_4_ = -1;
										aShortArrayArray2496[i_2_][i_3_] = (short) i_4_;
										int i_5_ = class296_sub17.g2();
										aShortArrayArrayArray2500[i_2_][i_3_] = new short[i_5_];
										for (int i_6_ = 0; i_6_ < i_5_; i_6_++) {
											int i_7_ = class296_sub17.g2();
											if (i_7_ == 65535)
												i_7_ = -1;
											aShortArrayArrayArray2500[i_2_][i_3_][i_6_] = (short) i_7_;
										}
									}
								}
							}
						}
					} else
						anInt2497 = class296_sub17.g2();
				} else {
					if (anIntArray2501 == null) {
						anInt2491 = 4;
						anIntArray2485 = new int[4];
						anIntArray2501 = new int[4];
					}
					for (int i_8_ = 0; anIntArray2501.length > i_8_; i_8_++) {
						anIntArray2501[i_8_] = class296_sub17.g2b();
						anIntArray2485[i_8_] = class296_sub17.g2b();
					}
					bool = true;
				}
			}
			if (!bool) {
				if (anIntArray2501 == null) {
					anIntArray2501 = new int[4];
					anIntArray2485 = new int[4];
					anInt2491 = 4;
				}
				for (int i_9_ = 0; i_9_ < anIntArray2501.length; i_9_++) {
					anIntArray2501[i_9_] = 0;
					anIntArray2485[i_9_] = i_9_ * 20;
				}
			}
		}
	}

	static final ObjCustomisation readColourData(Packet buff, ObjType definition) {
		ObjCustomisation data = new ObjCustomisation(definition);
		int flag = buff.g1();
		boolean has0 = (flag & 0x1) != 0;
		boolean has1 = (flag & 0x2) != 0;
		boolean has2 = (flag & 0x4) != 0;
		boolean has3 = (flag & 0x8) != 0;
		if (has0) {
			data.maleEquipModel[0] = buff.gSmart2or4s();
			data.femaleEquipModel[0] = buff.gSmart2or4s();
			if (definition.maleEquip2 != -1 || definition.femaleEquip2 != -1) {
				data.maleEquipModel[1] = buff.gSmart2or4s();
				data.femaleEquipModel[1] = buff.gSmart2or4s();
			}
			if (definition.maleEquip3 != -1 || definition.femaleEquip3 != -1) {
				data.maleEquipModel[2] = buff.gSmart2or4s();
				data.femaleEquipModel[2] = buff.gSmart2or4s();
			}
		}
		if (has1) {
			data.maleHeadModels[0] = buff.gSmart2or4s();
			data.femaleHeadModels[0] = buff.gSmart2or4s();
			if (definition.manHead2 != -1 || definition.womanHead2 != -1) {
				data.maleHeadModels[1] = buff.gSmart2or4s();
				data.femaleHeadModels[1] = buff.gSmart2or4s();
			}
		}
		if (has2) {
			int i_14_ = buff.g2();
			int[] is = new int[4];
			is[3] = 15 & i_14_ >> 12;
			is[2] = 15 & i_14_ >> 8;
			is[0] = 15 & i_14_;
			is[1] = (i_14_ & 255) >> 4;
			for (int i_15_ = 0; i_15_ < 4; i_15_++) {
				if (is[i_15_] != 15)
					data.recolourDst[is[i_15_]] = (short) buff.g2();
			}
		}
		if (has3) {
			int i_16_ = buff.g1();
			int[] is = new int[2];
			is[0] = i_16_ & 15;
			is[1] = (255 & i_16_) >> 4;
			for (int i_17_ = 0; i_17_ < 2; i_17_++) {
				if (is[i_17_] != 15)
					data.retextureDst[is[i_17_]] = (short) buff.g2();
			}
		}
		return data;
	}

	public static void method2300(boolean bool) {
		if (bool != true)
			aClass138_2494 = null;
		aClass138_2494 = null;
	}

	static final void method2301(byte i, InterfaceComponent[] class51s, int i_18_) {
		for (int i_19_ = 0; class51s.length > i_19_; i_19_++) {
			InterfaceComponent class51 = class51s[i_19_];
			if (class51 != null) {
				if (class51.type == 0) {
					if (class51.aClass51Array538 != null)
						method2301((byte) -70, class51.aClass51Array538, i_18_);
					Class296_Sub13 class296_sub13 = ((Class296_Sub13) Class386.aClass263_3264.get((long) class51.uid));
					if (class296_sub13 != null)
						Class296_Sub28.method2686(i_18_, class296_sub13.anInt4657, (byte) 84);
				}
				if (i_18_ == 0 && class51.anObjectArray502 != null) {
					CS2Call class296_sub46 = new CS2Call();
					class296_sub46.callerInterface = class51;
					class296_sub46.callArgs = class51.anObjectArray502;
					CS2Executor.runCS2(class296_sub46);
				}
				if (i_18_ == 1 && class51.anObjectArray539 != null) {
					if (class51.anInt592 >= 0) {
						InterfaceComponent class51_20_ = InterfaceComponent.getInterfaceComponent(class51.uid);
						if (class51_20_ == null || class51_20_.aClass51Array555 == null || (class51_20_.aClass51Array555.length <= class51.anInt592) || (class51_20_.aClass51Array555[class51.anInt592] != class51))
							continue;
					}
					CS2Call class296_sub46 = new CS2Call();
					class296_sub46.callArgs = class51.anObjectArray539;
					class296_sub46.callerInterface = class51;
					CS2Executor.runCS2(class296_sub46);
				}
			}
		}
		if (i != -70) {
			/* empty */
		}
	}

	static final void method2302(int i, int i_21_, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_, int i_27_) {
		if (i_22_ != 1024)
			anInt2488 = 4;
		Class296_Sub42 class296_sub42 = null;
		for (Class296_Sub42 class296_sub42_28_ = (Class296_Sub42) aa.aClass155_50.removeFirst((byte) 120); class296_sub42_28_ != null; class296_sub42_28_ = (Class296_Sub42) aa.aClass155_50.removeNext(1001)) {
			if (class296_sub42_28_.anInt4932 == i_23_ && class296_sub42_28_.anInt4923 == i_25_ && i_24_ == class296_sub42_28_.anInt4928 && class296_sub42_28_.anInt4927 == i) {
				class296_sub42 = class296_sub42_28_;
				break;
			}
		}
		if (class296_sub42 == null) {
			class296_sub42 = new Class296_Sub42();
			class296_sub42.anInt4928 = i_24_;
			class296_sub42.anInt4923 = i_25_;
			class296_sub42.anInt4927 = i;
			class296_sub42.anInt4932 = i_23_;
			if (i_25_ >= 0 && i_24_ >= 0 && Class198.currentMapSizeX > i_25_ && Class296_Sub38.currentMapSizeY > i_24_)
				Class322.method3346(95, class296_sub42);
			aa.aClass155_50.addLast((byte) 115, class296_sub42);
		}
		class296_sub42.anInt4936 = i_26_;
		class296_sub42.anInt4933 = i_27_;
		class296_sub42.aBoolean4935 = true;
		class296_sub42.aBoolean4929 = false;
		class296_sub42.anInt4934 = i_21_;
	}

	Class268(Js5 class138) {
		anInt2487 = 3;
		anInt2499 = 2;
		anInt2497 = -1;
		aBoolean2498 = true;
		anIntArray2501 = null;
		byte[] is = class138.get(3);
		method2298(new Packet(is), (byte) -115);
	}
}
