package net.zaros.client;

/* Class352 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class352 {
	static HashTable aClass263_3043 = new HashTable(32);
	static int anInt3044 = 0;
	static NPCDefinitionLoader npcDefinitionLoader;
	static byte[] aByteArray3046;

	static final void findPathToObject(long hash, int posX, int posY) {
		int objType = ((int) hash & 0x7daf0) >> 14;
		int face = (int) hash >> 20 & 0x3;
		int objectID = (int) (hash >>> 32) & 0x7fffffff;
		if (objType == 10 || objType == 11 || objType == 22) {
			ObjectDefinition obj = Class379.objectDefinitionLoader.getObjectDefinition(objectID);
			int sizeY;
			int sizeX;
			if (face == 0 || face == 2) {
				sizeX = obj.sizeX;
				sizeY = obj.sizeY;
			} else {
				sizeY = obj.sizeX;
				sizeX = obj.sizeY;
			}
			int fc = obj.anInt772;
			if (face != 0)
				fc = (fc << face & 0xf) + (fc >> 4 - face);
			Class405.findPathStandart(0, posX, posY, sizeX, sizeY, 0, fc, true);
		} else
			Class405.findPathStandart(objType, posX, posY, 0, 0, face, 0, true);
	}

	static final int method3686(int i, boolean bool) {
		if (Class338_Sub3_Sub1_Sub5.anIntArray6843 == null)
			return 0;
		if (!bool && Class353.aClass322Array3049 != null)
			return Class338_Sub3_Sub1_Sub5.anIntArray6843.length * 2;
		int i_8_ = 0;
		for (int i_9_ = i; i_9_ < Class338_Sub3_Sub1_Sub5.anIntArray6843.length; i_9_++) {
			int i_10_ = Class338_Sub3_Sub1_Sub5.anIntArray6843[i_9_];
			if (Class296_Sub22.aClass138_4733.hasEntryBuffer(i_10_))
				i_8_++;
			if (Class368_Sub23.aClass138_5571.hasEntryBuffer(i_10_))
				i_8_++;
		}
		return i_8_;
	}

	public static void method3687(int i) {
		npcDefinitionLoader = null;
		if (i <= 1)
			method3687(-6);
		aClass263_3043 = null;
		aByteArray3046 = null;
	}

	static final int method3688(Class125 class125, boolean bool) {
		if (Class41_Sub4.aClass125_3745 != class125) {
			if (class125 == Js5.aClass125_1411)
				return 8448;
			if (class125 != Class280.aClass125_2589) {
				if (class125 != Class41_Sub1.aClass125_3735) {
					if (Class390.aClass125_3283 == class125)
						return 34023;
				} else
					return 260;
			} else
				return 34165;
		} else
			return 7681;
		if (bool != true)
			method3687(-98);
		throw new IllegalArgumentException();
	}
}
