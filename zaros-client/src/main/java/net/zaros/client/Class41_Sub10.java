package net.zaros.client;

/* Class41_Sub10 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub10 extends Class41 {
	static int anInt3767;
	static Class62 aClass62_3768;
	static int anInt3769;

	Class41_Sub10(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final int method383(byte i) {
		if (i != 110)
			method435(80);
		if (!aClass296_Sub50_392.method3054(61)) {
			if (!aClass296_Sub50_392.aClass41_Sub30_5006.method527(-119) || !Class287.method2389(aClass296_Sub50_392.aClass41_Sub30_5006.method521(116), (byte) 26))
				return 0;
			return 1;
		}
		return 2;
	}

	final void method386(int i) {
		if (aClass296_Sub50_392.method3054(i ^ 0x4a))
			anInt389 = 2;
		if (i != 2)
			anInt3769 = 100;
		if (anInt389 < 0 || anInt389 > 2)
			anInt389 = method383((byte) 110);
	}

	final void method381(int i, byte i_0_) {
		anInt389 = i;
		if (i_0_ != -110)
			method431(9, null);
	}

	static final void method431(int i, Mobile class338_sub3_sub1_sub3) {
		if (i != -1073741824)
			anInt3769 = 59;
		if (class338_sub3_sub1_sub3.anIntArray6794 != null || class338_sub3_sub1_sub3.anIntArray6823 != null) {
			boolean bool = true;
			for (int i_1_ = 0; class338_sub3_sub1_sub3.anIntArray6794.length > i_1_; i_1_++) {
				int i_2_ = -1;
				if (class338_sub3_sub1_sub3.anIntArray6794 != null)
					i_2_ = class338_sub3_sub1_sub3.anIntArray6794[i_1_];
				if (i_2_ == -1) {
					if (!class338_sub3_sub1_sub3.method3515(-76, -1, i_1_))
						bool = false;
				} else {
					bool = false;
					boolean bool_3_ = false;
					boolean bool_4_ = false;
					int i_5_;
					int i_6_;
					if ((i_2_ & ~0x3fffffff) != -1073741824) {
						if ((i_2_ & 0x8000) != 0) {
							int i_7_ = i_2_ & 0x7fff;
							Player class338_sub3_sub1_sub3_sub1 = (PlayerUpdate.visiblePlayers[i_7_]);
							if (class338_sub3_sub1_sub3_sub1 == null) {
								class338_sub3_sub1_sub3.method3515(-47, -1, i_1_);
								continue;
							}
							i_5_ = (class338_sub3_sub1_sub3.tileX - class338_sub3_sub1_sub3_sub1.tileX);
							i_6_ = (-class338_sub3_sub1_sub3_sub1.tileY + class338_sub3_sub1_sub3.tileY);
						} else {
							NPCNode class296_sub7 = ((NPCNode) (Class41_Sub18.localNpcs.get((long) i_2_)));
							if (class296_sub7 != null) {
								NPC class338_sub3_sub1_sub3_sub2 = (class296_sub7.value);
								i_5_ = (-class338_sub3_sub1_sub3_sub2.tileX + class338_sub3_sub1_sub3.tileX);
								i_6_ = (class338_sub3_sub1_sub3.tileY - (class338_sub3_sub1_sub3_sub2.tileY));
							} else {
								class338_sub3_sub1_sub3.method3515(-110, -1, i_1_);
								continue;
							}
						}
					} else {
						int i_8_ = i_2_ & 0xfffffff;
						int i_9_ = i_8_ >> 14;
						i_5_ = (-256 - (-Class206.worldBaseX + i_9_) * 512 + class338_sub3_sub1_sub3.tileX);
						int i_10_ = i_8_ & 0x3fff;
						i_6_ = (-((i_10_ - Class41_Sub26.worldBaseY) * 512) - 256 + class338_sub3_sub1_sub3.tileY);
					}
					if (i_5_ != 0 || i_6_ != 0)
						class338_sub3_sub1_sub3.method3515(-54, (int) (Math.atan2((double) i_5_, (double) i_6_) * 2607.5945876176133) & 0x3fff, i_1_);
				}
			}
			if (bool) {
				class338_sub3_sub1_sub3.anIntArray6794 = null;
				class338_sub3_sub1_sub3.anIntArray6823 = null;
			}
		}
	}

	final boolean method432(int i) {
		if (i != -25952)
			return false;
		if (aClass296_Sub50_392.method3054(118))
			return false;
		return true;
	}

	public static void method433(byte i) {
		aClass62_3768 = null;
		int i_11_ = -1 % ((-69 - i) / 53);
	}

	Class41_Sub10(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final int method380(int i, byte i_12_) {
		if (i_12_ != 41)
			return -60;
		if (aClass296_Sub50_392.method3054(59))
			return 3;
		return 1;
	}

	final int method435(int i) {
		if (i < 114)
			return -83;
		return anInt389;
	}
}
