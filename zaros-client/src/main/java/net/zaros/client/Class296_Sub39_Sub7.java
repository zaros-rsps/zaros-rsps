package net.zaros.client;


/* Class296_Sub39_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub7 extends Queuable {
	short[][] aShortArrayArray6155;
	double aDouble6156;
	static OutgoingPacket aClass311_6157 = new OutgoingPacket(65, 11);

	static final void method2816(byte i, boolean bool, String string) {
		int i_0_ = 94 % ((i + 45) / 52);
		Class234.method2123((byte) -27, -1, bool, -1, string);
	}

	final long method2817(int i) {
		if (i != 929566272)
			aShortArrayArray6155 = null;
		return (long) (aShortArrayArray6155[0].length | aShortArrayArray6155.length << 0);
	}

	static final void method2818(Class93 class93) {
		if (Class368_Sub4.anInt5440 < 65535) {
			Class296_Sub35 class296_sub35 = class93.aClass296_Sub35_999;
			Class302.aClass93Array2721[Class368_Sub4.anInt5440] = class93;
			Class296_Sub17_Sub2.aBooleanArray6047[Class368_Sub4.anInt5440] = false;
			Class368_Sub4.anInt5440++;
			int i = class93.anInt1006;
			if (class93.aBoolean1000)
				i = 0;
			int i_1_ = class93.anInt1006;
			if (class93.aBoolean997)
				i_1_ = Class368_Sub9.anInt5477 - 1;
			for (int i_2_ = i; i_2_ <= i_1_; i_2_++) {
				int i_3_ = 0;
				int i_4_ = ((class296_sub35.method2750(-4444) - class296_sub35.method2748(49) + Class304.anInt2737) >> Class313.anInt2779);
				if (i_4_ < 0) {
					i_3_ -= i_4_;
					i_4_ = 0;
				}
				int i_5_ = ((class296_sub35.method2750(-4444) + class296_sub35.method2748(50) - Class304.anInt2737) >> Class313.anInt2779);
				if (i_5_ >= Class368_Sub12.anInt5488)
					i_5_ = Class368_Sub12.anInt5488 - 1;
				for (int i_6_ = i_4_; i_6_ <= i_5_; i_6_++) {
					int i_7_ = class93.aShortArray1010[i_3_++];
					int i_8_ = (((class296_sub35.method2749(true) - class296_sub35.method2748(75) + Class304.anInt2737) >> Class313.anInt2779) + (i_7_ >>> 8));
					int i_9_ = i_8_ + (i_7_ & 0xff) - 1;
					if (i_8_ < 0)
						i_8_ = 0;
					if (i_9_ >= Class228.anInt2201)
						i_9_ = Class228.anInt2201 - 1;
					for (int i_10_ = i_8_; i_10_ <= i_9_; i_10_++) {
						long l = (Class49.aLongArrayArrayArray462[i_2_][i_10_][i_6_]);
						if ((l & 0xffffL) == 0L)
							Class49.aLongArrayArrayArray462[i_2_][i_10_][i_6_] = l | (long) Class368_Sub4.anInt5440;
						else if ((l & 0xffff0000L) == 0L)
							Class49.aLongArrayArrayArray462[i_2_][i_10_][i_6_] = l | (long) Class368_Sub4.anInt5440 << 16;
						else if ((l & 0xffff00000000L) == 0L)
							Class49.aLongArrayArrayArray462[i_2_][i_10_][i_6_] = l | (long) Class368_Sub4.anInt5440 << 32;
						else if ((l & ~0xffffffffffffL) == 0L)
							Class49.aLongArrayArrayArray462[i_2_][i_10_][i_6_] = l | (long) Class368_Sub4.anInt5440 << 48;
					}
				}
			}
		}
	}

	Class296_Sub39_Sub7(short[][] is, double d) {
		aShortArrayArray6155 = is;
		aDouble6156 = d;
	}

	static final void method2819(int i, int i_11_, int i_12_, InterfaceComponent class51) {
		if (Class127.aBoolean1304) {
			ParamType class383 = (Class353.anInt3047 == -1 ? null : Class296_Sub22.itemExtraDataDefinitionLoader.list(Class353.anInt3047));
			if (GameClient.method115(class51).method2706((byte) -128) && (Class321.anInt2824 & 0x20) != 0 && (class383 == null || (class51.method631(Class353.anInt3047, 109, class383.defaultValue) != class383.defaultValue)))
				StaticMethods.method1008(-99, false, class51.anInt592, IOException_Sub1.anInt36, class51.clickedItem, Class228.aString2200, class51.uid, 0L, 20, true, (long) (class51.uid | class51.anInt592 << 0), (ConfigsRegister.aString3672 + " -> " + class51.componentName), false);
		}
		for (int i_13_ = 9; i_13_ >= 5; i_13_--) {
			String string = Class7.method187(-7, class51, i_13_);
			if (string != null) {
				LookupTable.anInt54++;
				StaticMethods.method1008(-103, false, class51.anInt592, EffectiveVertex.method2116(i_13_, class51, (byte) 110), class51.clickedItem, string, class51.uid, (long) (i_13_ + 1), 1006, true, (long) (class51.uid | class51.anInt592 << 0), class51.componentName, false);
			}
		}
		if (i_11_ >= -109)
			method2816((byte) 82, false, null);
		String string = Class368_Sub19.method3856(class51, 106);
		if (string != null)
			StaticMethods.method1008(7, false, class51.anInt592, class51.anInt576, class51.clickedItem, string, class51.uid, 0L, 53, true, (long) (class51.anInt592 << 0 | class51.uid), class51.componentName, false);
		for (int i_14_ = 4; i_14_ >= 0; i_14_--) {
			String string_15_ = Class7.method187(-115, class51, i_14_);
			if (string_15_ != null) {
				StaticMethods.method1008(-118, false, class51.anInt592, EffectiveVertex.method2116(i_14_, class51, (byte) 16), class51.clickedItem, string_15_, class51.uid, (long) (i_14_ + 1), 23, true, (long) (class51.uid | class51.anInt592 << 0), class51.componentName, false);
				LookupTable.anInt54++;
			}
		}
		if (GameClient.method115(class51).method2708(5)) {
			if (class51.aString577 != null)
				StaticMethods.method1008(-96, false, class51.anInt592, -1, class51.clickedItem, class51.aString577, class51.uid, 0L, 30, true, (long) (class51.anInt592 << 0 | class51.uid), "", false);
			else
				StaticMethods.method1008(-111, false, class51.anInt592, -1, class51.clickedItem, TranslatableString.aClass120_1206.getTranslation(Class394.langID), class51.uid, 0L, 30, true, (long) (class51.uid | class51.anInt592 << 0), "", false);
		}
	}

	public static void method2820(boolean bool) {
		if (bool != true)
			method2819(-97, 28, 54, null);
		aClass311_6157 = null;
	}
}
