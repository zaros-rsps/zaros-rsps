package net.zaros.client;
import jaggl.OpenGL;

final class Class205_Sub3 extends Class205 implements Interface6_Impl1 {
	private int anInt5644;
	private int anInt5645;
	static int[] npcsToUpdate = new int[250];

	public final void method67(boolean bool, int i, int i_0_, int i_1_, byte[] is, int i_2_, int i_3_, int i_4_, Class202 class202) {
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glPixelStorei(3317, 1);
		OpenGL.glPixelStorei(3314, i);
		OpenGL.glTexSubImage2Dub(anInt3507, 0, i_2_, i_1_, i_0_, i_4_, BITConfigDefinition.method2350(6409, class202), 5121, is, i_3_);
		OpenGL.glPixelStorei(3314, 0);
		if (bool != true)
			method72(99);
		OpenGL.glPixelStorei(3317, 4);
	}

	final void method1983(float[] fs, int i, int i_5_, int i_6_, Class202 class202, int i_7_, boolean bool, int i_8_, int i_9_) {
		aHa_Sub1_Sub1_3508.method1140(this, bool);
		OpenGL.glPixelStorei(3314, i_6_);
		OpenGL.glTexSubImage2Df(anInt3507, 0, i, i_7_, i_5_, i_9_, BITConfigDefinition.method2350(6409, class202), 5121, fs, i_8_);
		OpenGL.glPixelStorei(3314, 0);
	}

	Class205_Sub3(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Class202 class202, int i, int i_10_, boolean bool, byte[] is, int i_11_, int i_12_) {
		super(var_ha_Sub1_Sub1, 3553, class202, Class67.aClass67_745, i * i_10_, bool);
		anInt5644 = i;
		anInt5645 = i_10_;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glPixelStorei(3317, 1);
		if (!bool || i_12_ != 0 || i_11_ != 0) {
			OpenGL.glPixelStorei(3314, i_12_);
			OpenGL.glTexImage2Dub(anInt3507, 0, this.method1971(105), i, i_10_, 0, BITConfigDefinition.method2350(6409, aClass202_3514), 5121, is, i_11_);
			OpenGL.glPixelStorei(3314, 0);
		} else
			this.method1978(is, 5121, anInt3507, i_10_, i);
		OpenGL.glPixelStorei(3317, 4);
	}

	public final void method68(int i, int i_13_, int i_14_, int[] is, int i_15_, int i_16_, int i_17_, int i_18_) {
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glPixelStorei(3314, i_16_);
		OpenGL.glTexSubImage2Di(anInt3507, 0, i, i_18_, i_13_, i_17_, 32993, aHa_Sub1_Sub1_3508.anInt5852, is, i_14_);
		if (i_15_ != -25989)
			method68(26, -111, 64, null, -98, -120, 24, 34);
		OpenGL.glPixelStorei(3314, 0);
	}

	Class205_Sub3(ha_Sub1_Sub1 var_ha_Sub1_Sub1, int i, int i_19_, boolean bool, int[] is, int i_20_, int i_21_) {
		super(var_ha_Sub1_Sub1, 3553, za_Sub2.aClass202_6555, Class67.aClass67_745, i * i_19_, bool);
		anInt5644 = i;
		anInt5645 = i_19_;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		if (!bool || i_21_ != 0 || i_20_ != 0) {
			OpenGL.glPixelStorei(3314, i_21_);
			OpenGL.glTexImage2Di(anInt3507, 0, 6408, anInt5644, anInt5645, 0, 32993, aHa_Sub1_Sub1_3508.anInt5852, is, i_20_ * 4);
			OpenGL.glPixelStorei(3314, 0);
		} else
			this.method1972(anInt3507, is, i_19_, (byte) 119, i);
	}

	Class205_Sub3(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Class202 class202, int i, int i_22_, boolean bool, float[] fs, int i_23_, int i_24_) {
		super(var_ha_Sub1_Sub1, 3553, class202, Class67.aClass67_749, i_22_ * i, bool);
		anInt5645 = i_22_;
		anInt5644 = i;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		if (!bool && i_24_ == 0 && i_23_ == 0)
			this.method1975(anInt3507, (byte) 119, fs, i, i_22_);
		else {
			OpenGL.glPixelStorei(3314, i_24_);
			OpenGL.glTexImage2Df(anInt3507, 0, this.method1971(125), i, i_22_, 0, BITConfigDefinition.method2350(6409, aClass202_3514), 5126, fs, i_23_ * 4);
			OpenGL.glPixelStorei(3314, 0);
		}
	}

	public final int method70(int i) {
		int i_25_ = 102 / ((i + 33) / 37);
		return anInt5644;
	}

	public final boolean method72(int i) {
		int i_26_ = -108 / ((60 - i) / 37);
		return true;
	}

	Class205_Sub3(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Class202 class202, Class67 class67, int i, int i_27_) {
		super(var_ha_Sub1_Sub1, 3553, class202, class67, i_27_ * i, false);
		anInt5644 = i;
		anInt5645 = i_27_;
		aHa_Sub1_Sub1_3508.method1140(this, false);
		OpenGL.glTexImage2Dub(anInt3507, 0, this.method1971(-63), i, i_27_, 0, BITConfigDefinition.method2350(6409, aClass202_3514), HashTable.method2264(-103, aClass67_3513), null, 0);
	}

	public final float method69(int i, float f) {
		if (i >= -86)
			method69(100, -0.6870325F);
		return f / (float) anInt5644;
	}

	public final void method64(byte i, boolean bool, boolean bool_28_) {
		aHa_Sub1_Sub1_3508.method1140(this, false);
		if (i == 49) {
			OpenGL.glTexParameteri(anInt3507, 10242, bool_28_ ? 10497 : 33071);
			OpenGL.glTexParameteri(anInt3507, 10243, bool ? 10497 : 33071);
		}
	}

	public final void method65(int i, int[] is, int i_29_, int i_30_, int i_31_, int i_32_, int i_33_) {
		int[] is_34_ = new int[anInt5644 * anInt5645];
		aHa_Sub1_Sub1_3508.method1140(this, false);
		if (i_32_ != 16438)
			method67(false, -118, 3, -53, null, -111, 93, -84, null);
		OpenGL.glGetTexImagei(anInt3507, 0, 32993, 5121, is_34_, 0);
		for (int i_35_ = 0; i_31_ > i_35_; i_35_++)
			ArrayTools.removeElements(is_34_, anInt5644 * (-i_35_ + (i_31_ - (1 - i_33_))), is, i_35_ * i_30_ + i_29_, i_30_);
	}

	public static void method1984(byte i) {
		npcsToUpdate = null;
		RouteFinder.queueY = null;
		if (i >= -31)
			method1984((byte) -18);
	}

	public final int method66(int i) {
		if (i != 3776)
			npcsToUpdate = null;
		return anInt5645;
	}

	public final float method71(float f, byte i) {
		int i_36_ = -78 / ((i - 52) / 54);
		return f / (float) anInt5645;
	}
}
