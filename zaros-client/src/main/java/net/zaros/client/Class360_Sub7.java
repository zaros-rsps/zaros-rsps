package net.zaros.client;
import jagdx.IDirect3DDevice;
import jagdx.IDirect3DVertexShader;

final class Class360_Sub7 extends Class360 {
	private ha_Sub1_Sub2 aHa_Sub1_Sub2_5334;
	private IDirect3DVertexShader anIDirect3DVertexShader5335;
	private Class184 aClass184_5336;
	private static float[] aFloatArray5337 = new float[16];

	final void method3736(byte i, Interface6 interface6, int i_0_) {
		int i_1_ = -48 / ((72 - i) / 49);
	}

	final void method3725(int i) {
		aHa_Sub1_Sub2_5334.method1248(1326468944, null);
		aHa_Sub1_3093.method1158((byte) -100, 0, Class199.aClass287_2007);
		aHa_Sub1_3093.method1158((byte) -99, 1, Class347.aClass287_3025);
		aHa_Sub1_3093.method1158((byte) -101, 2, Class151.aClass287_1553);
		int i_2_ = 127 % ((58 - i) / 56);
		aHa_Sub1_3093.method1214(true, -11);
	}

	final void method3724(byte i) {
		if (i != -50)
			aHa_Sub1_Sub2_5334 = null;
		if (anIDirect3DVertexShader5335 != null) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5334.anIDirect3DDevice5865;
			Class373_Sub2 class373_sub2 = aHa_Sub1_Sub2_5334.method1122(101);
			idirect3ddevice.a(0, class373_sub2.method3944(aFloatArray5337, (byte) 31));
		}
	}

	final void method3719(byte i) {
		int i_3_ = 47 % ((-37 - i) / 43);
		if (anIDirect3DVertexShader5335 != null) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5334.anIDirect3DDevice5865;
			if (aHa_Sub1_3093.anInt3990 > 0) {
				float f = aHa_Sub1_3093.aFloat3964;
				float f_4_ = aHa_Sub1_3093.aFloat3989;
				float f_5_ = f_4_ - 512.0F;
				idirect3ddevice.b(10, f_5_, 1.0F / (-f_5_ + f_4_), f_4_, 1.0F / (-f_4_ + f));
			} else
				idirect3ddevice.b(10, 0.0F, 0.0F, 0.0F, 0.0F);
			aHa_Sub1_3093.method1139(0, aHa_Sub1_3093.anInt3947);
		}
	}

	final void method3720(int i) {
		if (i == 8250) {
			if (anIDirect3DVertexShader5335 != null) {
				IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5334.anIDirect3DDevice5865;
				Class373_Sub2 class373_sub2 = aHa_Sub1_Sub2_5334.method1122(86);
				idirect3ddevice.a(0, class373_sub2.method3944(aFloatArray5337, (byte) 31));
			}
		}
	}

	final void method3733(byte i, boolean bool) {
		aHa_Sub1_3093.method1158((byte) -98, 0, Class199.aClass287_2007);
		aHa_Sub1_3093.method1158((byte) -120, 1, Class151.aClass287_1553);
		aHa_Sub1_3093.method1170(2, Class347.aClass287_3025, true, false, (byte) -93);
		aHa_Sub1_3093.method1214(false, 123);
		aHa_Sub1_Sub2_5334.method1248(1326468944, anIDirect3DVertexShader5335);
		method3724((byte) -50);
		method3730(false);
		if (i >= -125)
			aHa_Sub1_Sub2_5334 = null;
		method3734(-13412);
		method3719((byte) 60);
	}

	Class360_Sub7(ha_Sub1_Sub2 var_ha_Sub1_Sub2, Js5 class138, Class184 class184) {
		super(var_ha_Sub1_Sub2);
		aHa_Sub1_Sub2_5334 = var_ha_Sub1_Sub2;
		aClass184_5336 = class184;
		if (class138 != null && aClass184_5336.method1859(16) && ((aHa_Sub1_Sub2_5334.aD3DCAPS5858.VertexShaderVersion & 0xffff) >= 257))
			anIDirect3DVertexShader5335 = (aHa_Sub1_Sub2_5334.anIDirect3DDevice5865.a(class138.getFile_("transparent_water", "dx")));
		else
			anIDirect3DVertexShader5335 = null;
	}

	final boolean method3723(byte i) {
		int i_6_ = 124 / ((i + 49) / 36);
		return anIDirect3DVertexShader5335 != null;
	}

	final void method3731(boolean bool, byte i) {
		aHa_Sub1_3093.method1177(Class280.aClass125_2589, 9815, Class41_Sub1.aClass125_3735);
		if (i != -71)
			method3734(-2);
	}

	final void method3730(boolean bool) {
		if (!bool) {
			if (anIDirect3DVertexShader5335 != null) {
				IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5334.anIDirect3DDevice5865;
				Class373_Sub2 class373_sub2 = aHa_Sub1_3093.method1226(-23501);
				class373_sub2.method3945(aFloatArray5337, (byte) 80);
				aFloatArray5337[4] *= 0.25F;
				aFloatArray5337[6] *= 0.25F;
				aFloatArray5337[1] *= 0.25F;
				aFloatArray5337[3] *= 0.25F;
				aFloatArray5337[7] *= 0.25F;
				aFloatArray5337[2] *= 0.25F;
				aFloatArray5337[5] *= 0.25F;
				aFloatArray5337[0] *= 0.25F;
				idirect3ddevice.SetVertexShaderConstantF(8, aFloatArray5337, 2);
			}
		}
	}

	final void method3732(int i, int i_7_, int i_8_) {
		if (i_7_ >= -6)
			method3732(-114, -20, -34);
		IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5334.anIDirect3DDevice5865;
		if (aClass184_5336.aBoolean1883) {
			float f = (float) (aHa_Sub1_3093.anInt4006 % 4000) / 4000.0F;
			aHa_Sub1_3093.method1140(aClass184_5336.anInterface6_Impl2_1886, false);
			idirect3ddevice.b(11, f, 0.0F, 0.0F, 0.0F);
		} else {
			int i_9_ = aHa_Sub1_3093.anInt4006 % 4000 * 16 / 4000;
			aHa_Sub1_3093.method1140((aClass184_5336.anInterface6_Impl1Array1887[i_9_]), false);
			idirect3ddevice.b(11, 0.0F, 0.0F, 0.0F, 0.0F);
		}
	}

	final void method3734(int i) {
		if (i == -13412) {
			if (anIDirect3DVertexShader5335 != null) {
				IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5334.anIDirect3DDevice5865;
				idirect3ddevice.a(4, aHa_Sub1_3093.method1182(1, aFloatArray5337));
			}
		}
	}
}
