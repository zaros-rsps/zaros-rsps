package net.zaros.client;
/* Class366_Sub6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Rectangle;
import java.io.IOException;

import jaggl.OpenGL;

final class Class366_Sub6 extends Class366 {
	private Class389 aClass389_5389;
	static AdvancedMemoryCache aClass113_5390;
	static String name3;
	static int anInt5392 = 0;
	private boolean aBoolean5393 = false;
	private boolean aBoolean5394 = false;
	private Class361 aClass361_5395;

	final void method3769(int i, byte i_0_, int i_1_) {
		if (aBoolean5393) {
			int i_2_ = 1 << (i & 0x3);
			float f = (float) (1 << (i >> 3 & 0x7)) / 32.0F;
			int i_3_ = i_1_ & 0xffff;
			float f_4_ = (float) (i_1_ >> 16 & 0x3) / 8.0F;
			float f_5_ = (float) ((i_1_ & 0x7f19ad) >> 19) / 16.0F;
			float f_6_ = (float) (i_1_ >> 23 & 0xf) / 16.0F;
			int i_7_ = i_1_ >> 27 & 0xf;
			long l = aClass389_5389.aLong3280;
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "time"), (float) (i_2_ * aHa_Sub3_3121.anInt4134 % 40000) / 40000.0F);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "scale"), f);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "breakWaterDepth"), (float) i_3_);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "breakWaterOffset"), f_4_);
			OpenGL.glUniform2fARB(OpenGL.glGetUniformLocationARB(l, "waveIntensity"), f_6_, f_5_);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "waveExponent"), (float) i_7_);
		}
		if (i_0_ != -81)
			anInt5392 = 68;
	}

	final boolean method3763(int i) {
		int i_8_ = -98 % ((i - 73) / 40);
		return aBoolean5394;
	}

	public static void method3786(byte i) {
		name3 = null;
		if (i < 61)
			method3789(null, (byte) -92);
		aClass113_5390 = null;
	}

	static final void method3787(Packet class296_sub17, byte i) {
		byte[] is = new byte[24];
		if (Applet_Sub1.aClass255_10 != null) {
			try {
				Applet_Sub1.aClass255_10.seek(0L);
				Applet_Sub1.aClass255_10.method2217(is, (byte) 58);
				int i_9_;
				for (i_9_ = 0; i_9_ < 24; i_9_++) {
					if (is[i_9_] != 0)
						break;
				}
				if (i_9_ >= 24)
					throw new IOException();
			} catch (Exception exception) {
				for (int i_10_ = 0; i_10_ < 24; i_10_++)
					is[i_10_] = (byte) -1;
			}
		}
		class296_sub17.writeBytes(is, 0, 24);
		if (i != 95)
			aClass113_5390 = null;
	}

	Class366_Sub6(ha_Sub3 var_ha_Sub3, Class361 class361) {
		super(var_ha_Sub3);
		aClass361_5395 = class361;
		if (aClass361_5395.aClass69_Sub2_3100 != null && aHa_Sub3_3121.aBoolean4229 && aHa_Sub3_3121.aBoolean4174) {
			Class77 class77 = (Class296_Sub51_Sub3.method3084(19399, aHa_Sub3_3121, "uniform float time;\nuniform float scale;\nvarying vec3 wvVertex;\nvarying float waterDepth;\nvoid main() {\nwaterDepth = gl_MultiTexCoord0.z;\nvec4 ecVertex = gl_ModelViewMatrix*gl_Vertex;\nwvVertex.x = dot(gl_NormalMatrix[0], ecVertex.xyz);\nwvVertex.y = dot(gl_NormalMatrix[1], ecVertex.xyz);\nwvVertex.z = dot(gl_NormalMatrix[2], ecVertex.xyz);\ngl_TexCoord[0].x = dot(gl_TextureMatrix[0][0], gl_MultiTexCoord0)*scale;\ngl_TexCoord[0].y = dot(gl_TextureMatrix[0][1], gl_MultiTexCoord0)*scale;\ngl_TexCoord[0].z = time;\ngl_TexCoord[0].w = 1.0;\ngl_FogFragCoord = 1.0-clamp((gl_Fog.end+ecVertex.z)*gl_Fog.scale, 0.0, 1.0);\ngl_Position = ftransform();\n}\n", 35633));
			Class77 class77_11_ = (Class296_Sub51_Sub3.method3084(19399, aHa_Sub3_3121, "varying vec3 wvVertex;\nvarying float waterDepth;\nuniform vec3 sunDir;\nuniform vec4 sunColour;\nuniform float sunExponent;\nuniform vec2 waveIntensity;\nuniform float waveExponent;\nuniform float breakWaterDepth;\nuniform float breakWaterOffset;\nuniform sampler3D normalSampler;\nuniform samplerCube envMapSampler;\nvoid main() {\nvec4 wnNormal = texture3D(normalSampler, gl_TexCoord[0].xyz).rbga;\nwnNormal.xyz = 2.0*wnNormal.xyz-1.0;\nvec3 wnVector = normalize(wvVertex);\nvec3 wnReflection = reflect(wnVector, wnNormal.xyz);\nvec3 envColour = textureCube(envMapSampler, wnReflection).rgb;\nvec4 specularColour = sunColour*pow(clamp(-dot(sunDir, wnReflection), 0.0, 1.0), sunExponent);\nfloat shoreFactor = clamp(waterDepth/breakWaterDepth-breakWaterOffset*wnNormal.w, 0.0, 1.0);\nfloat waveFactor = pow(1.0-shoreFactor, waveExponent)-0.5;\nwaveFactor = -4.0*waveFactor*waveFactor+1.0;\nfloat ndote = dot(wnVector, wnNormal.xyz);\nfloat fresnel = pow(1.0-abs(ndote), 1.0);\nvec4 surfaceColour = mix(vec4(envColour, fresnel*shoreFactor), (waveIntensity.x*wnNormal.wwww)+waveIntensity.y, waveFactor)+specularColour*shoreFactor;\ngl_FragColor = vec4(mix(surfaceColour.rgb, gl_Fog.color.rgb, gl_FogFragCoord), surfaceColour.a);\n}\n", 35632));
			aClass389_5389 = Class296_Sub51_Sub19.method3127(1, (new Class77[]{class77, class77_11_}), aHa_Sub3_3121);
			aBoolean5394 = aClass389_5389 != null;
		}
	}

	final void method3768(byte i, boolean bool) {
		if (i != -88)
			method3789(null, (byte) 84);
		Class69_Sub3 class69_sub3 = aHa_Sub3_3121.method1329((byte) 8);
		if (aBoolean5394 && class69_sub3 != null) {
			float f = ((1.0F - Math.abs(aHa_Sub3_3121.aFloatArray4202[1])) * 2.0F + 1.0F);
			aHa_Sub3_3121.method1330(118, 1);
			aHa_Sub3_3121.method1316(class69_sub3, (byte) -115);
			aHa_Sub3_3121.method1330(123, 0);
			aHa_Sub3_3121.method1316(aClass361_5395.aClass69_Sub2_3100, (byte) -122);
			long l = aClass389_5389.aLong3280;
			OpenGL.glUseProgramObjectARB(l);
			OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l, "normalSampler"), 0);
			OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l, "envMapSampler"), 1);
			OpenGL.glUniform3fARB(OpenGL.glGetUniformLocationARB(l, "sunDir"), -aHa_Sub3_3121.aFloatArray4202[0], -aHa_Sub3_3121.aFloatArray4202[1], -aHa_Sub3_3121.aFloatArray4202[2]);
			OpenGL.glUniform4fARB(OpenGL.glGetUniformLocationARB(l, "sunColour"), f * aHa_Sub3_3121.aFloat4261, aHa_Sub3_3121.aFloat4186 * f, aHa_Sub3_3121.aFloat4193 * f, 1.0F);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "sunExponent"), Math.abs(aHa_Sub3_3121.aFloatArray4202[1]) * 928.0F + 64.0F);
			aBoolean5393 = true;
		}
	}

	final void method3770(byte i, boolean bool) {
		if (i != 33)
			method3768((byte) -72, false);
	}

	static final void method3788(int i, Rectangle[] rectangles, int i_12_) throws Exception_Sub1 {
		if (ConfigsRegister.anInt3674 != 1)
			Class296_Sub51_Sub36.aHa6529.a(rectangles, i_12_, 0, 0);
		else
			Class296_Sub51_Sub36.aHa6529.a(rectangles, i_12_, Class34.anInt337, Class391.anInt3298);
		if (i != 24)
			anInt5392 = 106;
	}

	final void method3766(int i) {
		if (aBoolean5393) {
			aHa_Sub3_3121.method1330(110, 1);
			aHa_Sub3_3121.method1316(null, (byte) -115);
			aHa_Sub3_3121.method1330(113, 0);
			aHa_Sub3_3121.method1316(null, (byte) -122);
			OpenGL.glUseProgramObjectARB(0L);
			aBoolean5393 = false;
		}
		if (i <= 30)
			method3789(null, (byte) 106);
	}

	static final void method3789(Player p, byte i) {
		int i_13_ = -52 / (-i / 56);
		Class296_Sub36 class296_sub36 = ((Class296_Sub36) (ha_Sub1_Sub1.aClass263_5823.get((long) p.index)));
		if (class296_sub36 != null) {
			if (class296_sub36.aClass296_Sub45_Sub1_4871 != null) {
				Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36.aClass296_Sub45_Sub1_4871);
				class296_sub36.aClass296_Sub45_Sub1_4871 = null;
			}
			class296_sub36.unlink();
		}
	}

	final void method3764(boolean bool, int i, Class69 class69) {
		if (!aBoolean5393) {
			aHa_Sub3_3121.method1316(class69, (byte) -128);
			aHa_Sub3_3121.method1272((byte) -107, i);
		}
		if (bool)
			aClass113_5390 = null;
	}

	static {
		aClass113_5390 = new AdvancedMemoryCache(8);
	}
}
