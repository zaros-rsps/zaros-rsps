package net.zaros.client;

/* Class16 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

class Class16 {
	static IncomingPacket aClass231_181 = new IncomingPacket(54, -1);
	static int anInt182;
	static int anInt183;

	public static void method233(int i) {
		if (i != -1)
			anInt182 = 11;
		aClass231_181 = null;
	}

	static final int method234(int i, int i_0_, int i_1_, int i_2_) {
		i &= 0x3;
		if (i == 0)
			return i_2_;
		if (i_0_ == i)
			return i_1_;
		if (i == 2)
			return -i_2_ + 4095;
		return -i_1_ + 4095;
	}
}
