package net.zaros.client;
import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;
import jagtheora.theora.DecoderContext;
import jagtheora.theora.Frame;
import jagtheora.theora.GranulePos;
import jagtheora.theora.SetupInfo;
import jagtheora.theora.TheoraComment;
import jagtheora.theora.TheoraInfo;

final class Class296_Sub15_Sub3 extends Class296_Sub15 {
	private long aLong6010;
	static int anInt6011;
	static int anInt6012 = 0;
	private double aDouble6013;
	private GranulePos aGranulePos6014;
	private DecoderContext aDecoderContext6015;
	private TheoraInfo aTheoraInfo6016;
	private Sprite aClass397_6017;
	private int anInt6018;
	private boolean aBoolean6019;
	private int anInt6020;
	private boolean aBoolean6021;
	private Frame aFrame6022;
	private TheoraComment aTheoraComment6023;
	private boolean aBoolean6024;
	private SetupInfo aSetupInfo6025 = new SetupInfo();
	private boolean aBoolean6026;

	final Sprite method2535(byte i, ha var_ha) {
		if (aFrame6022 == null)
			return null;
		int i_0_ = 58 / ((i - 66) / 39);
		if (!aBoolean6019 && aClass397_6017 != null)
			return aClass397_6017;
		aClass397_6017 = var_ha.a(aFrame6022.pixels, 0, aFrame6022.a, aFrame6022.a, aFrame6022.b, false);
		aBoolean6019 = false;
		return aClass397_6017;
	}

	final float method2536(byte i) {
		if (!aBoolean6026 || aTheoraInfo6016.a())
			return 0.0F;
		if (i < 32)
			method2535((byte) -25, null);
		return ((float) aTheoraInfo6016.fpsNumerator / (float) aTheoraInfo6016.fpsDenominator);
	}

	final void method2519(int i) {
		if (aFrame6022 != null)
			aFrame6022.b();
		if (aDecoderContext6015 != null) {
			aDecoderContext6015.b();
			aDecoderContext6015 = null;
		}
		if (aGranulePos6014 != null) {
			aGranulePos6014.b();
			aGranulePos6014 = null;
		}
		aTheoraInfo6016.b();
		aTheoraComment6023.b();
		aSetupInfo6025.b();
		if (i != 16)
			aGranulePos6014 = null;
	}

	final void method2517(OggPacket oggpacket, int i) {
		if (i != 16)
			method2519(-98);
		if (!aBoolean6026) {
			int i_1_ = aSetupInfo6025.decodeHeader(aTheoraInfo6016, aTheoraComment6023, oggpacket);
			if (i_1_ != 0) {
				if (i_1_ < 0)
					throw new IllegalStateException(String.valueOf(i_1_));
			} else {
				aBoolean6026 = true;
				if (aTheoraInfo6016.frameWidth > 2048 || aTheoraInfo6016.frameHeight > 1024)
					throw new IllegalStateException();
				aDecoderContext6015 = new DecoderContext(aTheoraInfo6016, aSetupInfo6025);
				aGranulePos6014 = new GranulePos();
				aFrame6022 = new Frame(aTheoraInfo6016.frameWidth, aTheoraInfo6016.frameHeight);
				anInt6018 = aDecoderContext6015.getMaxPostProcessingLevel();
				method2537(anInt6020, (byte) -79);
			}
		} else {
			aLong6010 = Class72.method771(-120);
			int i_2_ = aDecoderContext6015.decodePacketIn(oggpacket, aGranulePos6014);
			if (i_2_ < 0)
				throw new IllegalStateException(String.valueOf(i_2_));
			aDecoderContext6015.granuleFrame(aGranulePos6014);
			aDouble6013 = aDecoderContext6015.granuleTime(aGranulePos6014);
			if (aBoolean6024) {
				boolean bool = oggpacket.isKeyFrame() == 1;
				if (bool)
					aBoolean6024 = false;
				else
					return;
			}
			if (!aBoolean6021 || oggpacket.isKeyFrame() == 1) {
				if (aDecoderContext6015.decodeFrame(aFrame6022) != 0)
					throw new IllegalStateException(String.valueOf(i_2_));
				aBoolean6019 = true;
			}
		}
	}

	Class296_Sub15_Sub3(OggStreamState oggstreamstate) {
		super(oggstreamstate);
		aTheoraInfo6016 = new TheoraInfo();
		aTheoraComment6023 = new TheoraComment();
	}

	private final void method2537(int i, byte i_3_) {
		int i_4_ = 14 % ((33 - i_3_) / 42);
		anInt6020 = i;
		if (aBoolean6026) {
			if (anInt6018 < anInt6020)
				anInt6020 = anInt6018;
			if (anInt6020 < 0)
				anInt6020 = 0;
			aDecoderContext6015.setPostProcessingLevel(anInt6020);
		}
	}

	final double method2538(int i) {
		if (i != 0)
			aBoolean6019 = false;
		return aDouble6013;
	}

	static final void method2539(int i, int i_5_, byte i_6_) {
		if (Class296_Sub51_Sub36.aHa6529 != null) {
			int i_7_ = Class317.anInt3705;
			if (i_6_ != -116)
				anInt6011 = 81;
			int i_8_ = Applet_Sub1.anInt7;
			Class296_Sub51_Sub34.method3185(i_5_, i_6_ + 114, i);
			if (ConfigsRegister.anInt3674 != 0) {
				if (ConfigsRegister.anInt3674 == 1 && (IOException_Sub1.anInterface13Array38 == null || i_7_ != Class317.anInt3705 || i_8_ != Applet_Sub1.anInt7)) {
					IOException_Sub1.anInterface13Array38 = (new Interface13[Applet_Sub1.anInt7 * Class317.anInt3705]);
					for (int i_9_ = 0; i_9_ < IOException_Sub1.anInterface13Array38.length; i_9_++)
						IOException_Sub1.anInterface13Array38[i_9_] = (Class296_Sub51_Sub36.aHa6529.a(Class296_Sub51_Sub36.aHa6529.b(Class290.anInt2656, Class395.anInt3317), Class296_Sub51_Sub36.aHa6529.a((Class290.anInt2656), (Class395.anInt3317))));
					BillboardRaw.anIntArray1080 = new int[Class317.anInt3705 * Applet_Sub1.anInt7];
					Class41_Sub20.anInt3795 = 1;
				}
			} else {
				Class241_Sub2_Sub1.anInterface13_5901 = null;
				Class241_Sub2_Sub1.anInterface13_5901 = (Class296_Sub51_Sub36.aHa6529.a(Class296_Sub51_Sub36.aHa6529.b((Class296_Sub39_Sub12.anInt6197), Class359.anInt3089), Class296_Sub51_Sub36.aHa6529.a((Class296_Sub39_Sub12.anInt6197), Class359.anInt3089)));
			}
			Class41.aBoolean388 = true;
		}
	}

	static final Class338_Sub3_Sub5 method2540(int i, int i_10_, int i_11_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_10_][i_11_];
		if (class247 == null || class247.aClass338_Sub3_Sub5_2346 == null)
			return null;
		return class247.aClass338_Sub3_Sub5_2346;
	}

	static final void insertNode(Queuable class296_sub39, int i, Queuable class296_sub39_12_) {
		if (class296_sub39.queue_previous != null)
			class296_sub39.queue_unlink();
		class296_sub39.queue_previous = class296_sub39_12_;
		class296_sub39.queue_next = class296_sub39_12_.queue_next;
		class296_sub39.queue_previous.queue_next = class296_sub39;
		if (i == 825)
			class296_sub39.queue_next.queue_previous = class296_sub39;
	}

	final boolean method2542(byte i) {
		if (i <= 70)
			anInt6018 = -75;
		return aBoolean6026;
	}

	final long method2543(int i) {
		if (i > -103)
			return 43L;
		return aLong6010;
	}

	static {
		anInt6011 = 100;
	}
}
