package net.zaros.client;
import jaggl.OpenGL;

final class Class360_Sub3 extends Class360 {
	private boolean aBoolean5306;
	static float aFloat5307;
	private boolean aBoolean5308;
	static Class296_Sub28 aClass296_Sub28_5309;
	private Class184 aClass184_5310;
	static Class278 aClass278_5311;
	private Class39 aClass39_5312;

	Class360_Sub3(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Js5 class138, Class184 class184) {
		super(var_ha_Sub1_Sub1);
		aClass184_5310 = class184;
		if (class138 != null && var_ha_Sub1_Sub1.aBoolean5846 && var_ha_Sub1_Sub1.aBoolean5843) {
			Class179 class179 = (Class182.method1849(var_ha_Sub1_Sub1, class138.getFile_("environment_mapped_water_v", "gl"), 35633, (byte) -115));
			Class179 class179_0_ = (Class182.method1849(var_ha_Sub1_Sub1, class138.getFile_("environment_mapped_water_f", "gl"), 35632, (byte) -115));
			aClass39_5312 = Class284.method2361(var_ha_Sub1_Sub1, new Class179[]{class179, class179_0_}, 0);
			aBoolean5306 = (aClass39_5312 != null && aClass184_5310.method1858((byte) -108));
		} else
			aBoolean5306 = false;
	}

	final void method3725(int i) {
		if (aBoolean5308) {
			aHa_Sub1_3093.method1151(1, 16760);
			aHa_Sub1_3093.method1140(null, false);
			aHa_Sub1_3093.method1151(0, 16760);
			aHa_Sub1_3093.method1140(null, false);
			OpenGL.glUseProgramObjectARB(0L);
			aBoolean5308 = false;
		}
		int i_1_ = -8 % ((i - 58) / 56);
	}

	final void method3731(boolean bool, byte i) {
		if (i != -71)
			method3736((byte) -25, null, -15);
	}

	final void method3732(int i, int i_2_, int i_3_) {
		if (i_2_ >= -6)
			aClass39_5312 = null;
		if (aBoolean5308) {
			int i_4_ = 1 << (i_3_ & 0x3);
			float f = (float) (1 << ((i_3_ & 0x3a) >> 3)) / 32.0F;
			int i_5_ = i & 0xffff;
			float f_6_ = (float) (i >> 16 & 0x3) / 8.0F;
			long l = aClass39_5312.aLong387;
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "time"), (float) (i_4_ * aHa_Sub1_3093.anInt4006 % 40000) / 40000.0F);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "scale"), f);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "breakWaterDepth"), (float) i_5_);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "breakWaterOffset"), f_6_);
		}
	}

	public static void method3739(int i) {
		aClass296_Sub28_5309 = null;
		aClass278_5311 = null;
		if (i <= 90)
			method3739(63);
	}

	static final void method3740(int i) {
		for (Class296_Sub14 class296_sub14 = ((Class296_Sub14) Class296_Sub51_Sub11.aClass263_6399.getFirst(true)); class296_sub14 != null; class296_sub14 = (Class296_Sub14) Class296_Sub51_Sub11.aClass263_6399.getNext(i ^ 0x316)) {
			if (!class296_sub14.aBoolean4669)
				Class108.method949(class296_sub14.anInt4658, 11771);
			else
				class296_sub14.aBoolean4669 = false;
		}
		if (i != 790)
			aClass296_Sub28_5309 = null;
	}

	static final void method3741(int i, int i_7_, byte i_8_, int i_9_) {
		if (i_8_ < 80)
			method3741(123, -48, (byte) 49, 33);
		String string = ("tele " + i_7_ + "," + (i_9_ >> 6) + "," + (i >> 6) + "," + (i_9_ & 0x3f) + "," + (i & 0x3f));
		System.out.println(string);
		Class41_Sub28.method513(string, true, false, -105);
	}

	final void method3736(byte i, Interface6 interface6, int i_10_) {
		int i_11_ = 68 % ((i - 72) / 49);
		if (!aBoolean5308) {
			aHa_Sub1_3093.method1140(interface6, false);
			aHa_Sub1_3093.method1197((byte) 22, i_10_);
		}
	}

	final boolean method3723(byte i) {
		int i_12_ = -85 % ((i + 49) / 36);
		return aBoolean5306;
	}

	final void method3733(byte i, boolean bool) {
		if (i > -125)
			method3733((byte) 59, true);
		Interface6_Impl3 interface6_impl3 = aHa_Sub1_3093.method1116((byte) -114);
		if (aBoolean5306 && interface6_impl3 != null) {
			aHa_Sub1_3093.method1151(1, 16760);
			aHa_Sub1_3093.method1140(interface6_impl3, false);
			aHa_Sub1_3093.method1151(0, 16760);
			aHa_Sub1_3093.method1140(aClass184_5310.anInterface6_Impl2_1885, false);
			long l = aClass39_5312.aLong387;
			OpenGL.glUseProgramObjectARB(l);
			OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l, "normalSampler"), 0);
			OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l, "envMapSampler"), 1);
			OpenGL.glUniform3fARB(OpenGL.glGetUniformLocationARB(l, "sunDir"), -aHa_Sub1_3093.aFloatArray4030[0], -aHa_Sub1_3093.aFloatArray4030[1], -aHa_Sub1_3093.aFloatArray4030[2]);
			OpenGL.glUniform4fARB(OpenGL.glGetUniformLocationARB(l, "sunColour"), aHa_Sub1_3093.aFloat4000, aHa_Sub1_3093.aFloat4019, aHa_Sub1_3093.aFloat4043, 1.0F);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "sunExponent"), Math.abs(aHa_Sub1_3093.aFloatArray4030[1]) * 928.0F + 96.0F);
			aBoolean5308 = true;
		}
	}
}
