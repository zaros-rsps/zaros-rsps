package net.zaros.client;
/* Class296_Sub51_Sub33 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Random;

final class Class296_Sub51_Sub33 extends TextureOperation {
	private int anInt6517;
	private int anInt6518;
	private int anInt6519 = 0;
	private int anInt6520;
	private int anInt6521;
	static IncomingPacket aClass231_6522 = new IncomingPacket(3, -1);

	static final boolean method3179(byte i, String string) {
		if (i >= -97)
			method3179((byte) -54, null);
		return Class154.aHashtable1581.containsKey(string);
	}

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		if (i < -84) {
			int i_1_ = i_0_;
			while_247_ : do {
				while_246_ : do {
					while_245_ : do {
						do {
							if (i_1_ != 0) {
								if (i_1_ != 1) {
									if (i_1_ != 2) {
										if (i_1_ != 3) {
											if (i_1_ == 4)
												break while_246_;
											break while_247_;
										}
									} else
										break;
									break while_245_;
								}
							} else {
								anInt6517 = class296_sub17.g1();
								return;
							}
							anInt6520 = class296_sub17.g2();
							return;
						} while (false);
						anInt6518 = class296_sub17.g1();
						return;
					} while (false);
					anInt6519 = class296_sub17.g2();
					return;
				} while (false);
				anInt6521 = class296_sub17.g2();
				break;
			} while (false);
		}
	}

	static final void method3180(boolean bool) throws Exception_Sub1 {
		if (ConfigsRegister.anInt3674 == 1)
			Class296_Sub51_Sub36.aHa6529.c(Class34.anInt337, Class391.anInt3298);
		else
			Class296_Sub51_Sub36.aHa6529.c(0, 0);
		if (bool)
			aClass231_6522 = null;
	}

	public Class296_Sub51_Sub33() {
		super(0, true);
		anInt6518 = 16;
		anInt6517 = 0;
		anInt6520 = 2000;
		anInt6521 = 4096;
	}

	final void method3076(byte i) {
		int i_2_ = -21 / ((i + 58) / 40);
		Class367.method3800(4096);
	}

	static final boolean method3181(int i, int i_3_, int i_4_) {
		if (i_4_ < 1)
			method3182(107);
		if (!Class215.method2025((byte) -20, i_3_, i) && !Class273.method2313(67, i_3_, i))
			return false;
		return true;
	}

	final int[] get_monochrome_output(int i, int i_5_) {
		int[] is = aClass318_5035.method3335(i_5_, (byte) 28);
		if (i != 0)
			method3183(-39);
		if (aClass318_5035.aBoolean2819) {
			int i_6_ = anInt6521 >> 1;
			int[][] is_7_ = aClass318_5035.method3337(105);
			Random random = new Random((long) anInt6517);
			for (int i_8_ = 0; i_8_ < anInt6520; i_8_++) {
				int i_9_ = (anInt6521 <= 0 ? anInt6519 : anInt6519 + (s_Sub3.method3373(anInt6521, random, i + 6445) - i_6_));
				i_9_ = (i_9_ & 0xffd) >> 4;
				int i_10_ = s_Sub3.method3373(Class41_Sub10.anInt3769, random, i + 6445);
				int i_11_ = s_Sub3.method3373(Class296_Sub35_Sub2.anInt6114, random, i ^ 0x192d);
				int i_12_ = ((Class259.anIntArray2419[i_9_] * anInt6518 >> 12) + i_10_);
				int i_13_ = (Class2.anIntArray56[i_9_] * anInt6518 >> 12) + i_11_;
				int i_14_ = -i_11_ + i_13_;
				int i_15_ = -i_10_ + i_12_;
				if (i_15_ != 0 || i_14_ != 0) {
					if (i_14_ < 0)
						i_14_ = -i_14_;
					if (i_15_ < 0)
						i_15_ = -i_15_;
					boolean bool = i_15_ < i_14_;
					if (bool) {
						int i_16_ = i_10_;
						i_10_ = i_11_;
						int i_17_ = i_12_;
						i_12_ = i_13_;
						i_11_ = i_16_;
						i_13_ = i_17_;
					}
					if (i_10_ > i_12_) {
						int i_18_ = i_10_;
						i_10_ = i_12_;
						int i_19_ = i_11_;
						i_11_ = i_13_;
						i_12_ = i_18_;
						i_13_ = i_19_;
					}
					int i_20_ = i_11_;
					int i_21_ = -i_10_ + i_12_;
					int i_22_ = -i_11_ + i_13_;
					int i_23_ = -i_21_ / 2;
					int i_24_ = 2048 / i_21_;
					int i_25_ = -(s_Sub3.method3373(4096, random, 6445) >> 2) + 1024;
					if (i_22_ < 0)
						i_22_ = -i_22_;
					int i_26_ = i_13_ <= i_11_ ? -1 : 1;
					for (int i_27_ = i_10_; i_12_ > i_27_; i_27_++) {
						int i_28_ = (i_27_ - i_10_) * i_24_ + i_25_ + 1024;
						int i_29_ = Class41_Sub25.anInt3803 & i_27_;
						int i_30_ = Class67.anInt753 & i_20_;
						if (bool)
							is_7_[i_30_][i_29_] = i_28_;
						else
							is_7_[i_29_][i_30_] = i_28_;
						i_23_ += i_22_;
						if (i_23_ > 0) {
							i_23_ = -i_21_ + i_23_;
							i_20_ += i_26_;
						}
					}
				}
			}
		}
		return is;
	}

	static final void method3182(int i) {
		int i_31_ = 115 / ((i + 74) / 39);
		if (!Class296_Sub51_Sub24.aBoolean6465) {
			Class304.aFloat2733 += (-Class304.aFloat2733 + 12.0F) / 2.0F;
			Class296_Sub51_Sub24.aBoolean6465 = true;
			Mesh.aBoolean1359 = true;
		}
	}

	public static void method3183(int i) {
		aClass231_6522 = null;
		if (i >= -54)
			aClass231_6522 = null;
	}
}
