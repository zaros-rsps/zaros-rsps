package net.zaros.client;

/* Class132 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

public class Mesh {
	public int[] anIntArray1344;
	public byte[] textures_mapping_type;
	public short[] faceS_c;
	public int[] anIntArray1347;
	public int[] anIntArray1348;
	public int[] vertices_z;
	public int num_vertices = 0;
	public short[] textures_mapping_n;
	public short[] aShortArray1352;
	public short[] faces_b;
	public byte[] aByteArray1354;
	public byte[] faces_priority;
	public int[] vertices_x;
	public EmissiveTriangle[] emitters;
	public short[] textures_mapping_m;
	public static boolean aBoolean1359 = true;
	public EffectiveVertex[] effectors;
	public int[] vertices_y;
	public int num_faces = 0;
	public short[] textures_mapping_p;
	public static int anInt1364;
	public int[] anIntArray1365;
	public short[] aShortArray1366;
	public byte[] aByteArray1367;
	public int num_textures = 0;
	public int[] vertices_label;
	public int[] faces_label;
	public int[] anIntArray1371;
	public byte aByte1372 = 0;
	public short[] faces_colour;
	public byte[] faces_alpha;
	public int highest_face_index = 0;
	public Billboard[] billboards;
	public short[] faces_material;
	public short[] faces_a;
	public int[] anIntArray1379;
	public int version_number = 12;
	public byte[] aByteArray1381;
	public byte[] faces_texture;

	public int[][] method1384(boolean bool) {
		int[] is = new int[256];
		int i = 0;
		for (int i_0_ = 0; i_0_ < num_faces; i_0_++) {
			int i_1_ = faces_label[i_0_];
			if (i_1_ >= 0) {
				is[i_1_]++;
				if (i < i_1_) {
					i = i_1_;
				}
			}
		}
		int[][] is_2_ = new int[i + 1][];
		for (int i_3_ = 0; i >= i_3_; i_3_++) {
			is_2_[i_3_] = new int[is[i_3_]];
			is[i_3_] = 0;
		}
		if (bool) {
			method1384(true);
		}
		for (int i_4_ = 0; num_faces > i_4_; i_4_++) {
			int i_5_ = faces_label[i_4_];
			if (i_5_ >= 0) {
				is_2_[i_5_][is[i_5_]++] = i_4_;
			}
		}
		return is_2_;
	}

	public int[][] method1385(byte i, boolean bool) {
		int[] is = new int[256];
		int i_6_ = 0;
		int i_7_ = !bool ? highest_face_index : num_vertices;
		for (int i_8_ = 0; i_8_ < i_7_; i_8_++) {
			int i_9_ = vertices_label[i_8_];
			if (i_9_ >= 0) {
				if (i_6_ < i_9_) {
					i_6_ = i_9_;
				}
				is[i_9_]++;
			}
		}
		int[][] is_10_ = new int[i_6_ + 1][];
		for (int i_11_ = 0; i_6_ >= i_11_; i_11_++) {
			is_10_[i_11_] = new int[is[i_11_]];
			is[i_11_] = 0;
		}
		int i_12_ = 0;
		if (i != 111) {
			return null;
		}
		for (/**/; i_7_ > i_12_; i_12_++) {
			int i_13_ = vertices_label[i_12_];
			if (i_13_ >= 0) {
				is_10_[i_13_][is[i_13_]++] = i_12_;
			}
		}
		return is_10_;
	}

	public byte method1386(short i, boolean bool, short i_14_, short i_15_, byte i_16_, byte i_17_, byte i_18_, short i_19_, short i_20_, short i_21_) {
		if (num_textures >= 255) {
			throw new IllegalStateException();
		}
		textures_mapping_type[num_textures] = (byte) 3;
		textures_mapping_p[num_textures] = i_20_;
		textures_mapping_m[num_textures] = i_21_;
		textures_mapping_n[num_textures] = i_19_;
		anIntArray1344[num_textures] = i_14_;
		anIntArray1365[num_textures] = i_15_;
		anIntArray1379[num_textures] = i;
		aByteArray1381[num_textures] = i_18_;
		if (bool != true) {
			num_vertices = 103;
		}
		aByteArray1367[num_textures] = i_17_;
		anIntArray1371[num_textures] = i_16_;
		return (byte) num_textures++;
	}

	static public char method1387(byte i, byte i_22_) {
		if (i > -26) {
			method1387((byte) -56, (byte) -3);
		}
		int i_23_ = i_22_ & 0xff;
		if (i_23_ == 0) {
			throw new IllegalArgumentException("Non cp1252 character 0x" + Integer.toString(i_23_, 16) + " provided");
		}
		if (i_23_ >= 128 && i_23_ < 160) {
			int i_24_ = r.aCharArray6207[i_23_ - 128];
			if (i_24_ == 0) {
				i_24_ = 63;
			}
			i_23_ = i_24_;
		}
		return (char) i_23_;
	}

	public void scale(int i) {
		for (int i_26_ = 0; num_vertices > i_26_; i_26_++) {
			vertices_x[i_26_] <<= i;
			vertices_y[i_26_] <<= i;
			vertices_z[i_26_] <<= i;
		}
		if (num_textures > 0 && anIntArray1344 != null) {
			for (int i_27_ = 0; anIntArray1344.length > i_27_; i_27_++) {
				anIntArray1344[i_27_] <<= i;
				anIntArray1365[i_27_] <<= i;
				if (textures_mapping_type[i_27_] != 1) {
					anIntArray1379[i_27_] <<= i;
				}
			}
		}
	}

	public int method1389(int i, int i_28_, int i_29_, short i_30_, int i_31_, byte i_32_, byte i_33_, short i_34_, byte i_35_) {
		faces_a[num_faces] = (short) i;
		if (i_29_ > -61) {
			return 25;
		}
		faces_b[num_faces] = (short) i_31_;
		faceS_c[num_faces] = (short) i_28_;
		aByteArray1354[num_faces] = i_35_;
		faces_texture[num_faces] = i_33_;
		faces_colour[num_faces] = i_34_;
		faces_alpha[num_faces] = i_32_;
		faces_material[num_faces] = i_30_;
		return num_faces++;
	}

	public int[][] method1390(byte i) {
		int[] is = new int[256];
		int i_36_ = 0;
		for (Billboard billboard : billboards) {
			int i_38_ = billboard.anInt3258;
			if (i_38_ >= 0) {
				is[i_38_]++;
				if (i_36_ < i_38_) {
					i_36_ = i_38_;
				}
			}
		}
		int[][] is_39_ = new int[i_36_ + 1][];
		for (int i_40_ = 0; i_40_ <= i_36_; i_40_++) {
			is_39_[i_40_] = new int[is[i_40_]];
			is[i_40_] = 0;
		}
		for (int i_42_ = 0; i_42_ < billboards.length; i_42_++) {
			int i_43_ = billboards[i_42_].anInt3258;
			if (i_43_ >= 0) {
				is_39_[i_43_][is[i_43_]++] = i_42_;
			}
		}
		return is_39_;
	}

	private void deserialise_new(byte[] data) {
		Packet first = new Packet(data);
		Packet second = new Packet(data);
		Packet third = new Packet(data);
		Packet fouth = new Packet(data);
		Packet fifth = new Packet(data);
		Packet sixth = new Packet(data);
		Packet seventh = new Packet(data);
		first.pos = data.length - 23;
		num_vertices = first.g2();
		num_faces = first.g2();
		num_textures = first.g1();
		int settings = first.g1();
		boolean setting_facetypes = (settings & 0x1) == 1;
		boolean setting_particles = (settings & 0x2) == 2;
		boolean setting_billboards = (settings & 0x4) == 4;
		boolean setting_version = (settings & 0x8) == 8;
		boolean hasExtendedVertexSkins = (settings & 0x10) == 16;
		boolean hasExtendedFaceSkins = (settings & 0x20) == 32;
		boolean hasExtendedBillboards = (settings & 0x40) == 64;
		if (setting_version) {
			first.pos -= 7;
			version_number = first.g1();
			first.pos += 6;
		}
		int setting_priority = first.g1();
		int setting_facealphas = first.g1();
		int setting_facelabels = first.g1();
		int setting_facetextures = first.g1();
		int setting_vertexlabels = first.g1();
		int modelVerticesX = first.g2();
		int modelVerticesY = first.g2();
		int modelVerticesZ = first.g2();
		int face_indices = first.g2();
		int i_63_ = first.g2();
		int numVertexSkins;
		if (hasExtendedVertexSkins) {
			numVertexSkins = first.g2();
		} else if (setting_vertexlabels == 1) {
			numVertexSkins = num_vertices;
		} else {
			numVertexSkins = 0;
		}
		int numFaceSkins;
		if (hasExtendedFaceSkins) {
			numFaceSkins = first.g2();
		} else if (setting_facelabels == 1) {
			numFaceSkins = num_faces;
		} else {
			numFaceSkins = 0;
		}
		int simpleTextureFaceCount = 0;
		int complexTextureFaceCount = 0;
		int cubeTextureFaceCount = 0;
		if (num_textures > 0) {
			textures_mapping_type = new byte[num_textures];
			first.pos = 0;
			for (int i_67_ = 0; num_textures > i_67_; i_67_++) {
				byte i_68_ = textures_mapping_type[i_67_] = first.g1b();
				if (i_68_ >= 1 && i_68_ <= 3) {
					complexTextureFaceCount++;
				}
				if (i_68_ == 0) {
					simpleTextureFaceCount++;
				}
				if (i_68_ == 2) {
					cubeTextureFaceCount++;
				}
			}
		}
		int offset = num_textures;
		int vertexFlagsOffset = offset;
		offset += num_vertices;
		int faceTypesOffset = offset;
		if (setting_facetypes) {
			offset += num_faces;
		}
		int faceIndexTypesOffset = offset;
		offset += num_faces;
		int facePrioritiesOffset = offset;
		if (setting_priority == 255) {
			offset += num_faces;
		}
		int faceSkinsOffset = offset;
		offset += numFaceSkins;
		int vertexSkinsOffset = offset;
		offset += numVertexSkins;
		int faceAlphasOffset = offset;
		if (setting_facealphas == 1) {
			offset += num_faces;
		}
		int faceIndicesOffset = offset;
		offset += face_indices;
		int faceMaterialsOffset = offset;
		if (setting_facetextures == 1) {
			offset += num_faces * 2;
		}
		int faceTextureIndicesOffset = offset;
		offset += i_63_;
		int faceColorsOffset = offset;
		offset += num_faces * 2;
		int vertexXOffsetOffset = offset;
		offset += modelVerticesX;
		int vertexYOffsetOffset = offset;
		offset += modelVerticesY;
		int vertexZOffsetOffset = offset;
		offset += modelVerticesZ;
		int simpleTexturesOffset = offset;
		offset += simpleTextureFaceCount * 6;
		int complexTexturesOffset = offset;
		offset += complexTextureFaceCount * 6;
		int textureBytes = 6;
		if (version_number != 14) {
			if (version_number >= 15) {
				textureBytes = 9;
			}
		} else {
			textureBytes = 7;
		}
		int texturesScaleOffset = offset;
		offset += textureBytes * complexTextureFaceCount;
		int texturesRotationOffset = offset;
		offset += complexTextureFaceCount;
		int texturesDirectionOffset = offset;
		offset += complexTextureFaceCount;
		int texturesTranslationOffset = offset;
		offset += cubeTextureFaceCount * 2 + complexTextureFaceCount;
		int particleEffectsOffset = offset;
		vertices_x = new int[num_vertices];
		vertices_y = new int[num_vertices];
		vertices_z = new int[num_vertices];
		faces_a = new short[num_faces];
		faces_b = new short[num_faces];
		faceS_c = new short[num_faces];
		if (setting_vertexlabels == 1) {
			vertices_label = new int[num_vertices];
		}
		if (setting_facetypes) {
			aByteArray1354 = new byte[num_faces];
		}
		if (setting_priority == 255) {
			faces_priority = new byte[num_faces];
		} else {
			aByte1372 = (byte) setting_priority;
		}
		if (setting_facealphas == 1) {
			faces_alpha = new byte[num_faces];
		}
		if (setting_facetextures == 1) {
			faces_material = new short[num_faces];
		}
		if (setting_facelabels == 1) {
			faces_label = new int[num_faces];
		}
		faces_colour = new short[num_faces];
		if (num_textures > 0) {
			textures_mapping_n = new short[num_textures];
			if (cubeTextureFaceCount > 0) {
				anIntArray1347 = new int[cubeTextureFaceCount];
				anIntArray1348 = new int[cubeTextureFaceCount];
			}
			textures_mapping_p = new short[num_textures];
			textures_mapping_m = new short[num_textures];
			if (complexTextureFaceCount > 0) {
				anIntArray1371 = new int[complexTextureFaceCount];
				anIntArray1379 = new int[complexTextureFaceCount];
				anIntArray1344 = new int[complexTextureFaceCount];
				aByteArray1381 = new byte[complexTextureFaceCount];
				aByteArray1367 = new byte[complexTextureFaceCount];
				anIntArray1365 = new int[complexTextureFaceCount];
			}
		}
		if (setting_facetextures == 1 && num_textures > 0) {
			faces_texture = new byte[num_faces];
		}
		first.pos = vertexFlagsOffset;
		second.pos = vertexXOffsetOffset;
		third.pos = vertexYOffsetOffset;
		fouth.pos = vertexZOffsetOffset;
		fifth.pos = vertexSkinsOffset;
		int i_92_ = 0;
		int i_93_ = 0;
		int i_94_ = 0;
		for (int vertex = 0; num_vertices > vertex; vertex++) {
			int i_96_ = first.g1();
			int i_97_ = 0;
			if ((i_96_ & 0x1) != 0) {
				i_97_ = second.gSmart1or2s();
			}
			int i_98_ = 0;
			if ((i_96_ & 0x2) != 0) {
				i_98_ = third.gSmart1or2s();
			}
			int i_99_ = 0;
			if ((i_96_ & 0x4) != 0) {
				i_99_ = fouth.gSmart1or2s();
			}
			vertices_x[vertex] = i_97_ + i_92_;
			vertices_y[vertex] = i_98_ + i_93_;
			vertices_z[vertex] = i_99_ + i_94_;
			i_93_ = vertices_y[vertex];
			i_94_ = vertices_z[vertex];
			i_92_ = vertices_x[vertex];
			if (setting_vertexlabels == 1) {
				if (hasExtendedVertexSkins) {
					vertices_label[vertex] = fifth.gSmart1or2n();
				} else {
					vertices_label[vertex] = fifth.g1();
					if (vertices_label[vertex] == 255) {
						vertices_label[vertex] = -1;
					}
				}
			}
		}
		first.pos = faceColorsOffset;
		second.pos = faceTypesOffset;
		third.pos = facePrioritiesOffset;
		fouth.pos = faceAlphasOffset;
		fifth.pos = faceSkinsOffset;
		sixth.pos = faceMaterialsOffset;
		seventh.pos = faceTextureIndicesOffset;
		for (int face = 0; num_faces > face; face++) {
			faces_colour[face] = (short) first.g2();
			if (setting_facetypes) {
				aByteArray1354[face] = second.g1b();
			}
			if (setting_priority == 255) {
				faces_priority[face] = third.g1b();
			}
			if (setting_facealphas == 1) {
				faces_alpha[face] = fouth.g1b();
			}
			if (setting_facelabels == 1) {
				if (hasExtendedFaceSkins) {
					faces_label[face] = fifth.gSmart1or2n();
				} else {
					faces_label[face] = fifth.g1();
					if (faces_label[face] == 255) {
						faces_label[face] = -1;
					}
				}
			}
			if (setting_facetextures == 1) {
				faces_material[face] = (short) (sixth.g2() - 1);
			}
			if (faces_texture != null) {
				if (faces_material[face] == -1) {
					faces_texture[face] = (byte) -1;
				} else {
					faces_texture[face] = (byte) (seventh.g1() - 1);
				}
			}
		}
		highest_face_index = -1;
		first.pos = faceIndicesOffset;
		second.pos = faceIndexTypesOffset;
		short a = 0;
		short b = 0;
		short c = 0;
		int pacc = 0;
		for (int face = 0; face < num_faces; face++) {
			int i_106_ = second.g1();
			if (i_106_ == 1) {
				a = (short) (first.gSmart1or2s() + pacc);
				pacc = a;
				b = (short) (first.gSmart1or2s() + pacc);
				pacc = b;
				c = (short) (pacc + first.gSmart1or2s());
				pacc = c;
				faces_a[face] = a;
				faces_b[face] = b;
				faceS_c[face] = c;
				if (highest_face_index < a) {
					highest_face_index = a;
				}
				if (b > highest_face_index) {
					highest_face_index = b;
				}
				if (c > highest_face_index) {
					highest_face_index = c;
				}
			}
			if (i_106_ == 2) {
				b = c;
				c = (short) (first.gSmart1or2s() + pacc);
				pacc = c;
				faces_a[face] = a;
				faces_b[face] = b;
				faceS_c[face] = c;
				if (highest_face_index < c) {
					highest_face_index = c;
				}
			}
			if (i_106_ == 3) {
				a = c;
				c = (short) (first.gSmart1or2s() + pacc);
				faces_a[face] = a;
				pacc = c;
				faces_b[face] = b;
				faceS_c[face] = c;
				if (highest_face_index < c) {
					highest_face_index = c;
				}
			}
			if (i_106_ == 4) {
				short i_107_ = a;
				a = b;
				b = i_107_;
				c = (short) (first.gSmart1or2s() + pacc);
				pacc = c;
				faces_a[face] = a;
				faces_b[face] = b;
				faceS_c[face] = c;
				if (highest_face_index < c) {
					highest_face_index = c;
				}
			}
		}
		highest_face_index++;
		first.pos = simpleTexturesOffset;
		second.pos = complexTexturesOffset;
		third.pos = texturesScaleOffset;
		fouth.pos = texturesRotationOffset;
		fifth.pos = texturesDirectionOffset;
		sixth.pos = texturesTranslationOffset;
		for (int i_108_ = 0; i_108_ < num_textures; i_108_++) {
			int i_109_ = textures_mapping_type[i_108_] & 0xff;
			if (i_109_ == 0) {
				textures_mapping_p[i_108_] = (short) first.g2();
				textures_mapping_m[i_108_] = (short) first.g2();
				textures_mapping_n[i_108_] = (short) first.g2();
			}
			if (i_109_ == 1) {
				textures_mapping_p[i_108_] = (short) second.g2();
				textures_mapping_m[i_108_] = (short) second.g2();
				textures_mapping_n[i_108_] = (short) second.g2();
				if (version_number < 15) {
					anIntArray1344[i_108_] = third.g2();
					if (version_number >= 14) {
						anIntArray1365[i_108_] = third.readUnsignedMedInt();
					} else {
						anIntArray1365[i_108_] = third.g2();
					}
					anIntArray1379[i_108_] = third.g2();
				} else {
					anIntArray1344[i_108_] = third.readUnsignedMedInt();
					anIntArray1365[i_108_] = third.readUnsignedMedInt();
					anIntArray1379[i_108_] = third.readUnsignedMedInt();
				}
				aByteArray1381[i_108_] = fouth.g1b();
				aByteArray1367[i_108_] = fifth.g1b();
				anIntArray1371[i_108_] = sixth.g1b();
			}
			if (i_109_ == 2) {
				textures_mapping_p[i_108_] = (short) second.g2();
				textures_mapping_m[i_108_] = (short) second.g2();
				textures_mapping_n[i_108_] = (short) second.g2();
				if (version_number < 15) {
					anIntArray1344[i_108_] = third.g2();
					if (version_number >= 14) {
						anIntArray1365[i_108_] = third.readUnsignedMedInt();
					} else {
						anIntArray1365[i_108_] = third.g2();
					}
					anIntArray1379[i_108_] = third.g2();
				} else {
					anIntArray1344[i_108_] = third.readUnsignedMedInt();
					anIntArray1365[i_108_] = third.readUnsignedMedInt();
					anIntArray1379[i_108_] = third.readUnsignedMedInt();
				}
				aByteArray1381[i_108_] = fouth.g1b();
				aByteArray1367[i_108_] = fifth.g1b();
				anIntArray1371[i_108_] = sixth.g1b();
				anIntArray1347[i_108_] = sixth.g1b();
				anIntArray1348[i_108_] = sixth.g1b();
			}
			if (i_109_ == 3) {
				textures_mapping_p[i_108_] = (short) second.g2();
				textures_mapping_m[i_108_] = (short) second.g2();
				textures_mapping_n[i_108_] = (short) second.g2();
				if (version_number < 15) {
					anIntArray1344[i_108_] = third.g2();
					if (version_number >= 14) {
						anIntArray1365[i_108_] = third.readUnsignedMedInt();
					} else {
						anIntArray1365[i_108_] = third.g2();
					}
					anIntArray1379[i_108_] = third.g2();
				} else {
					anIntArray1344[i_108_] = third.readUnsignedMedInt();
					anIntArray1365[i_108_] = third.readUnsignedMedInt();
					anIntArray1379[i_108_] = third.readUnsignedMedInt();
				}
				aByteArray1381[i_108_] = fouth.g1b();
				aByteArray1367[i_108_] = fifth.g1b();
				anIntArray1371[i_108_] = sixth.g1b();
			}
		}
		first.pos = particleEffectsOffset;
		if (setting_particles) {
			int num_emitters = first.g1();
			if (num_emitters > 0) {
				emitters = new EmissiveTriangle[num_emitters];
				for (int i_111_ = 0; i_111_ < num_emitters; i_111_++) {
					int i_112_ = first.g2();
					int i_113_ = first.g2();
					byte i_114_;
					if (setting_priority == 255) {
						i_114_ = faces_priority[i_113_];
					} else {
						i_114_ = (byte) setting_priority;
					}
					emitters[i_111_] = new EmissiveTriangle(i_112_, faces_a[i_113_], faces_b[i_113_], faceS_c[i_113_], i_114_);
				}
			}
			int num_effectors = first.g1();
			if (num_effectors > 0) {
				effectors = new EffectiveVertex[num_effectors];
				for (int i_116_ = 0; num_effectors > i_116_; i_116_++) {
					int i_117_ = first.g2();
					int i_118_ = first.g2();
					effectors[i_116_] = new EffectiveVertex(i_117_, i_118_);
				}
			}
		}
		if (setting_billboards) {
			int i_119_ = first.g1();
			if (i_119_ > 0) {
				billboards = new Billboard[i_119_];
				for (int i_120_ = 0; i_119_ > i_120_; i_120_++) {
					int i_121_ = first.g2();
					int i_122_ = first.g2();
					int depth;
					if (hasExtendedBillboards) {
						depth = first.gSmart1or2n();
					} else {
						depth = first.g1();
						if (depth == 255) {
							depth = -1;
						}
					}
					byte i_124_ = first.g1b();
					billboards[i_120_] = new Billboard(i_121_, i_122_, depth, i_124_);
				}
			}
		}
	}

	public void method1392(int i, int i_125_, int i_126_, byte i_127_) {
		if (i_127_ != 84) {
			vertices_z = null;
		}
		if (i_125_ != 0) {
			int i_128_ = Class296_Sub4.anIntArray4598[i_125_];
			int i_129_ = Class296_Sub4.anIntArray4618[i_125_];
			for (int i_130_ = 0; num_vertices > i_130_; i_130_++) {
				int i_131_ = i_129_ * vertices_x[i_130_] + vertices_y[i_130_] * i_128_ >> 14;
				vertices_y[i_130_] = i_129_ * vertices_y[i_130_] - i_128_ * vertices_x[i_130_] >> 14;
				vertices_x[i_130_] = i_131_;
			}
		}
		if (i_126_ != 0) {
			int i_132_ = Class296_Sub4.anIntArray4598[i_126_];
			int i_133_ = Class296_Sub4.anIntArray4618[i_126_];
			for (int i_134_ = 0; i_134_ < num_vertices; i_134_++) {
				int i_135_ = -(i_132_ * vertices_z[i_134_]) + i_133_ * vertices_y[i_134_] >> 14;
				vertices_z[i_134_] = i_132_ * vertices_y[i_134_] + i_133_ * vertices_z[i_134_] >> 14;
				vertices_y[i_134_] = i_135_;
			}
		}
		if (i != 0) {
			int i_136_ = Class296_Sub4.anIntArray4598[i];
			int i_137_ = Class296_Sub4.anIntArray4618[i];
			for (int i_138_ = 0; i_138_ < num_vertices; i_138_++) {
				int i_139_ = vertices_z[i_138_] * i_136_ + i_137_ * vertices_x[i_138_] >> 14;
				vertices_z[i_138_] = i_137_ * vertices_z[i_138_] - vertices_x[i_138_] * i_136_ >> 14;
				vertices_x[i_138_] = i_139_;
			}
		}
	}

	static public void method1393(byte i) {
		if (i == 100) {
			if (Class39.anInt385 < 102) {
				Class39.anInt385 += 6;
			}
			if (Class296_Sub51_Sub3.anInt6347 != -1 && Class230.aLong2208 < Class72.method771(i ^ ~0x11)) {
				for (int i_140_ = Class296_Sub51_Sub3.anInt6347; Class199.aStringArray2004.length > i_140_; i_140_++) {
					if (Class199.aStringArray2004[i_140_].startsWith("pause")) {
						int i_141_ = 5;
						try {
							i_141_ = Integer.parseInt(Class199.aStringArray2004[i_140_].substring(6));
						} catch (Exception exception) {
							/* empty */
						}
						Class41_Sub18.writeConsoleMessage("Pausing for " + i_141_ + " seconds...");
						Class296_Sub51_Sub3.anInt6347 = i_140_ + 1;
						Class230.aLong2208 = i_141_ * 1000 + Class72.method771(-124);
						return;
					}
					Class355_Sub2.aString3503 = Class199.aStringArray2004[i_140_];
					Class144.method1483(false, -88);
				}
				Class296_Sub51_Sub3.anInt6347 = -1;
			}
			if (Class359.anInt3090 != 0) {
				za_Sub1.anInt6552 -= Class359.anInt3090 * 5;
				if (za_Sub1.anInt6552 >= Class357.anInt3082) {
					za_Sub1.anInt6552 = Class357.anInt3082 - 1;
				}
				Class359.anInt3090 = 0;
				if (za_Sub1.anInt6552 < 0) {
					za_Sub1.anInt6552 = 0;
				}
			}
			for (int i_142_ = 0; Class338_Sub8_Sub1.anInt6576 > i_142_; i_142_++) {
				Interface1 interface1 = Class341.anInterface1Array2978[i_142_];
				int i_143_ = interface1.method5(13856);
				char c = interface1.method2(-23600);
				int i_144_ = interface1.method1((byte) -105);
				if (i_143_ == 84) {
					Class144.method1483(false, i ^ ~0x6);
				}
				if (i_143_ != 80) {
					if (i_143_ != 66 || (i_144_ & 0x4) == 0) {
						if (i_143_ == 67 && (i_144_ & 0x4) != 0) {
							if (Class296_Sub51_Sub10.aClipboard6394 != null) {
								try {
									Transferable transferable = Class296_Sub51_Sub10.aClipboard6394.getContents(null);
									if (transferable != null) {
										String string = (String) transferable.getTransferData(DataFlavor.stringFlavor);
										if (string != null) {
											String[] strings = Class41_Sub30.method522((byte) 63, string, '\n');
											Class355.method3694(i ^ 0x8880, strings);
										}
									}
								} catch (Exception exception) {
									/* empty */
								}
							}
						} else if (i_143_ != 85 || OutputStream_Sub2.anInt44 <= 0) {
							if (i_143_ == 101 && OutputStream_Sub2.anInt44 < Class355_Sub2.aString3503.length()) {
								Class355_Sub2.aString3503 = Class355_Sub2.aString3503.substring(0, OutputStream_Sub2.anInt44) + Class355_Sub2.aString3503.substring(OutputStream_Sub2.anInt44 + 1);
							} else if (i_143_ != 96 || OutputStream_Sub2.anInt44 <= 0) {
								if (i_143_ != 97 || OutputStream_Sub2.anInt44 >= Class355_Sub2.aString3503.length()) {
									if (i_143_ == 102) {
										OutputStream_Sub2.anInt44 = 0;
									} else if (i_143_ == 103) {
										OutputStream_Sub2.anInt44 = Class355_Sub2.aString3503.length();
									} else if (i_143_ != 104 || Class296_Sub51_Sub15.aStringArray6425.length <= Class296_Sub51_Sub9.anInt6389) {
										if (i_143_ != 105 || Class296_Sub51_Sub9.anInt6389 <= 0) {
											if (Class350.method3679((byte) 116, c) || "\\/.:, _-+[]~@".indexOf(c) != -1) {
												Class355_Sub2.aString3503 = Class355_Sub2.aString3503.substring(0, OutputStream_Sub2.anInt44) + Class341.anInterface1Array2978[i_142_].method2(-23600) + Class355_Sub2.aString3503.substring(OutputStream_Sub2.anInt44);
												OutputStream_Sub2.anInt44++;
											}
										} else {
											Class296_Sub51_Sub9.anInt6389--;
											Class96.method868((byte) -35);
											OutputStream_Sub2.anInt44 = Class355_Sub2.aString3503.length();
										}
									} else {
										Class296_Sub51_Sub9.anInt6389++;
										Class96.method868((byte) -35);
										OutputStream_Sub2.anInt44 = Class355_Sub2.aString3503.length();
									}
								} else {
									OutputStream_Sub2.anInt44++;
								}
							} else {
								OutputStream_Sub2.anInt44--;
							}
						} else {
							Class355_Sub2.aString3503 = Class355_Sub2.aString3503.substring(0, OutputStream_Sub2.anInt44 - 1) + Class355_Sub2.aString3503.substring(OutputStream_Sub2.anInt44);
							OutputStream_Sub2.anInt44--;
						}
					} else if (Class296_Sub51_Sub10.aClipboard6394 != null) {
						String string = "";
						for (int i_145_ = Class296_Sub51_Sub15.aStringArray6425.length - 1; i_145_ >= 0; i_145_--) {
							if (Class296_Sub51_Sub15.aStringArray6425[i_145_] != null && Class296_Sub51_Sub15.aStringArray6425[i_145_].length() > 0) {
								string += Class296_Sub51_Sub15.aStringArray6425[i_145_] + '\n';
							}
						}
						Class296_Sub51_Sub10.aClipboard6394.setContents(new StringSelection(string), null);
					}
				} else {
					Class144.method1483(true, -89);
				}
			}
			Class338_Sub8_Sub1.anInt6576 = 0;
			Class368_Sub2.anInt5430 = 0;
			Class366_Sub8.method3794(true);
		}
	}

	public void retexture(short i, short i_146_) {
		if (faces_material != null) {
			for (int i_148_ = 0; num_faces > i_148_; i_148_++) {
				if (faces_material[i_148_] == i) {
					faces_material[i_148_] = i_146_;
				}
			}
		}
	}

	public void recolour(short i, short i_149_) {
		for (int i_151_ = 0; num_faces > i_151_; i_151_++) {
			if (faces_colour[i_151_] == i) {
				faces_colour[i_151_] = i_149_;
			}
		}
	}

	public void deserialise_old(byte[] is) {
		boolean bool = false;
		boolean bool_152_ = false;
		Packet class296_sub17 = new Packet(is);
		Packet class296_sub17_153_ = new Packet(is);
		Packet class296_sub17_154_ = new Packet(is);
		Packet class296_sub17_155_ = new Packet(is);
		Packet class296_sub17_156_ = new Packet(is);
		class296_sub17.pos = is.length - 18;
		num_vertices = class296_sub17.g2();
		num_faces = class296_sub17.g2();
		num_textures = class296_sub17.g1();
		int i_157_ = class296_sub17.g1();
		int i_158_ = class296_sub17.g1();
		int i_159_ = class296_sub17.g1();
		int i_160_ = class296_sub17.g1();
		int i_161_ = class296_sub17.g1();
		int i_162_ = class296_sub17.g2();
		int i_163_ = class296_sub17.g2();
		int i_164_ = class296_sub17.g2();
		int i_165_ = class296_sub17.g2();
		int i_166_ = 0;
		int i_167_ = i_166_;
		i_166_ += num_vertices;
		int i_168_ = i_166_;
		i_166_ += num_faces;
		int i_169_ = i_166_;
		if (i_158_ == 255) {
			i_166_ += num_faces;
		}
		int i_170_ = i_166_;
		if (i_160_ == 1) {
			i_166_ += num_faces;
		}
		int i_171_ = i_166_;
		if (i_157_ == 1) {
			i_166_ += num_faces;
		}
		int i_172_ = i_166_;
		if (i_161_ == 1) {
			i_166_ += num_vertices;
		}
		int i_173_ = i_166_;
		if (i_159_ == 1) {
			i_166_ += num_faces;
		}
		int i_174_ = i_166_;
		i_166_ += i_165_;
		int i_175_ = i_166_;
		i_166_ += num_faces * 2;
		int i_176_ = i_166_;
		i_166_ += num_textures * 6;
		int i_177_ = i_166_;
		i_166_ += i_162_;
		int i_178_ = i_166_;
		i_166_ += i_163_;
		int i_179_ = i_166_;
		vertices_y = new int[num_vertices];
		faces_colour = new short[num_faces];
		faces_b = new short[num_faces];
		class296_sub17.pos = i_167_;
		if (i_161_ == 1) {
			vertices_label = new int[num_vertices];
		}
		faces_a = new short[num_faces];
		faceS_c = new short[num_faces];
		if (i_160_ == 1) {
			faces_label = new int[num_faces];
		}
		if (num_textures > 0) {
			textures_mapping_p = new short[num_textures];
			textures_mapping_n = new short[num_textures];
			textures_mapping_m = new short[num_textures];
			textures_mapping_type = new byte[num_textures];
		}
		if (i_159_ == 1) {
			faces_alpha = new byte[num_faces];
		}
		vertices_x = new int[num_vertices];
		i_166_ += i_164_;
		if (i_157_ == 1) {
			faces_material = new short[num_faces];
			aByteArray1354 = new byte[num_faces];
			faces_texture = new byte[num_faces];
		}
		vertices_z = new int[num_vertices];
		if (i_158_ != 255) {
			aByte1372 = (byte) i_158_;
		} else {
			faces_priority = new byte[num_faces];
		}
		class296_sub17_153_.pos = i_177_;
		class296_sub17_154_.pos = i_178_;
		class296_sub17_155_.pos = i_179_;
		class296_sub17_156_.pos = i_172_;
		int i_180_ = 0;
		int i_181_ = 0;
		int i_182_ = 0;
		for (int i_183_ = 0; num_vertices > i_183_; i_183_++) {
			int i_184_ = class296_sub17.g1();
			int i_185_ = 0;
			if ((i_184_ & 0x1) != 0) {
				i_185_ = class296_sub17_153_.gSmart1or2s();
			}
			int i_186_ = 0;
			if ((i_184_ & 0x2) != 0) {
				i_186_ = class296_sub17_154_.gSmart1or2s();
			}
			int i_187_ = 0;
			if ((i_184_ & 0x4) != 0) {
				i_187_ = class296_sub17_155_.gSmart1or2s();
			}
			vertices_x[i_183_] = i_185_ + i_180_;
			vertices_y[i_183_] = i_181_ + i_186_;
			vertices_z[i_183_] = i_182_ + i_187_;
			i_180_ = vertices_x[i_183_];
			i_181_ = vertices_y[i_183_];
			i_182_ = vertices_z[i_183_];
			if (i_161_ == 1) {
				vertices_label[i_183_] = class296_sub17_156_.g1();
			}
		}
		class296_sub17.pos = i_175_;
		class296_sub17_153_.pos = i_171_;
		class296_sub17_154_.pos = i_169_;
		class296_sub17_155_.pos = i_173_;
		class296_sub17_156_.pos = i_170_;
		for (int i_188_ = 0; i_188_ < num_faces; i_188_++) {
			faces_colour[i_188_] = (short) class296_sub17.g2();
			if (i_157_ == 1) {
				int i_189_ = class296_sub17_153_.g1();
				if ((i_189_ & 0x1) == 1) {
					bool = true;
					aByteArray1354[i_188_] = (byte) 1;
				} else {
					aByteArray1354[i_188_] = (byte) 0;
				}
				if ((i_189_ & 0x2) == 2) {
					faces_texture[i_188_] = (byte) (i_189_ >> 2);
					faces_material[i_188_] = faces_colour[i_188_];
					faces_colour[i_188_] = (short) 127;
					if (faces_material[i_188_] != -1) {
						bool_152_ = true;
					}
				} else {
					faces_texture[i_188_] = (byte) -1;
					faces_material[i_188_] = (short) -1;
				}
			}
			if (i_158_ == 255) {
				faces_priority[i_188_] = class296_sub17_154_.g1b();
			}
			if (i_159_ == 1) {
				faces_alpha[i_188_] = class296_sub17_155_.g1b();
			}
			if (i_160_ == 1) {
				faces_label[i_188_] = class296_sub17_156_.g1();
			}
		}
		highest_face_index = -1;
		class296_sub17.pos = i_174_;
		class296_sub17_153_.pos = i_168_;
		short i_190_ = 0;
		short i_191_ = 0;
		short i_192_ = 0;
		int i_193_ = 0;
		for (int i_194_ = 0; num_faces > i_194_; i_194_++) {
			int i_195_ = class296_sub17_153_.g1();
			if (i_195_ == 1) {
				i_190_ = (short) (i_193_ + class296_sub17.gSmart1or2s());
				i_193_ = i_190_;
				i_191_ = (short) (i_193_ + class296_sub17.gSmart1or2s());
				i_193_ = i_191_;
				i_192_ = (short) (class296_sub17.gSmart1or2s() + i_193_);
				i_193_ = i_192_;
				faces_a[i_194_] = i_190_;
				faces_b[i_194_] = i_191_;
				faceS_c[i_194_] = i_192_;
				if (highest_face_index < i_190_) {
					highest_face_index = i_190_;
				}
				if (i_191_ > highest_face_index) {
					highest_face_index = i_191_;
				}
				if (i_192_ > highest_face_index) {
					highest_face_index = i_192_;
				}
			}
			if (i_195_ == 2) {
				i_191_ = i_192_;
				i_192_ = (short) (i_193_ + class296_sub17.gSmart1or2s());
				i_193_ = i_192_;
				faces_a[i_194_] = i_190_;
				faces_b[i_194_] = i_191_;
				faceS_c[i_194_] = i_192_;
				if (highest_face_index < i_192_) {
					highest_face_index = i_192_;
				}
			}
			if (i_195_ == 3) {
				i_190_ = i_192_;
				i_192_ = (short) (class296_sub17.gSmart1or2s() + i_193_);
				i_193_ = i_192_;
				faces_a[i_194_] = i_190_;
				faces_b[i_194_] = i_191_;
				faceS_c[i_194_] = i_192_;
				if (i_192_ > highest_face_index) {
					highest_face_index = i_192_;
				}
			}
			if (i_195_ == 4) {
				short i_196_ = i_190_;
				i_190_ = i_191_;
				i_191_ = i_196_;
				i_192_ = (short) (class296_sub17.gSmart1or2s() + i_193_);
				i_193_ = i_192_;
				faces_a[i_194_] = i_190_;
				faces_b[i_194_] = i_191_;
				faceS_c[i_194_] = i_192_;
				if (i_192_ > highest_face_index) {
					highest_face_index = i_192_;
				}
			}
		}
		class296_sub17.pos = i_176_;
		highest_face_index++;
		for (int i_197_ = 0; num_textures > i_197_; i_197_++) {
			textures_mapping_type[i_197_] = (byte) 0;
			textures_mapping_p[i_197_] = (short) class296_sub17.g2();
			textures_mapping_m[i_197_] = (short) class296_sub17.g2();
			textures_mapping_n[i_197_] = (short) class296_sub17.g2();
		}
		if (faces_texture != null) {
			boolean bool_198_ = false;
			for (int i_199_ = 0; i_199_ < num_faces; i_199_++) {
				int i_200_ = faces_texture[i_199_] & 0xff;
				if (i_200_ != 255) {
					if ((textures_mapping_p[i_200_] & 0xffff) == faces_a[i_199_] && (textures_mapping_m[i_200_] & 0xffff) == faces_b[i_199_] && faceS_c[i_199_] == (textures_mapping_n[i_200_] & 0xffff)) {
						faces_texture[i_199_] = (byte) -1;
					} else {
						bool_198_ = true;
					}
				}
			}
			if (!bool_198_) {
				faces_texture = null;
			}
		}
		if (!bool_152_) {
			faces_material = null;
		}
		if (!bool) {
			aByteArray1354 = null;
		}
	}

	public void translate(int i_201_, int i, int i_202_) {
		for (int i_204_ = 0; num_vertices > i_204_; i_204_++) {
			vertices_x[i_204_] += i_201_;
			vertices_y[i_204_] += i_202_;
			vertices_z[i_204_] += i;
		}
	}

	private int method1398(int i, Mesh class132_205_, short i_206_, byte i_207_) {
		int i_208_ = class132_205_.vertices_x[i];
		int i_209_ = class132_205_.vertices_y[i];
		int i_210_ = class132_205_.vertices_z[i];
		for (int i_211_ = 0; i_211_ < num_vertices; i_211_++) {
			if (i_208_ == vertices_x[i_211_] && i_209_ == vertices_y[i_211_] && i_210_ == vertices_z[i_211_]) {
				aShortArray1366[i_211_] = (short) Class48.bitOR(aShortArray1366[i_211_], i_206_);
				return i_211_;
			}
		}
		if (i_207_ <= 121) {
			anIntArray1348 = null;
		}
		vertices_x[num_vertices] = i_208_;
		vertices_y[num_vertices] = i_209_;
		vertices_z[num_vertices] = i_210_;
		aShortArray1366[num_vertices] = i_206_;
		vertices_label[num_vertices] = class132_205_.vertices_label != null ? class132_205_.vertices_label[i] : -1;
		return num_vertices++;
	}

	public int method1399(int i, int i_212_, int i_213_, int i_214_) {
		for (int i_215_ = 0; num_vertices > i_215_; i_215_++) {
			if (i_213_ == vertices_x[i_215_] && vertices_y[i_215_] == i && vertices_z[i_215_] == i_212_) {
				return i_215_;
			}
		}
		vertices_x[num_vertices] = i_213_;
		vertices_y[num_vertices] = i;
		vertices_z[num_vertices] = i_212_;
		highest_face_index = i_214_ + num_vertices;
		return num_vertices++;
	}

	public Mesh() {
		/* empty */
	}

	public Mesh(byte[] data) {
		if (data[data.length - 1] != -1 || data[data.length - 2] != -1) {
			deserialise_old(data);
		} else {
			deserialise_new(data);
		}
	}

	Mesh(int i, int i_216_, int i_217_) {
		faces_a = new short[i_216_];
		vertices_z = new int[i];
		faces_alpha = new byte[i_216_];
		vertices_y = new int[i];
		aByteArray1354 = new byte[i_216_];
		faceS_c = new short[i_216_];
		faces_colour = new short[i_216_];
		faces_priority = new byte[i_216_];
		faces_material = new short[i_216_];
		faces_label = new int[i_216_];
		vertices_label = new int[i];
		vertices_x = new int[i];
		if (i_217_ > 0) {
			textures_mapping_p = new short[i_217_];
			textures_mapping_m = new short[i_217_];
			aByteArray1367 = new byte[i_217_];
			anIntArray1347 = new int[i_217_];
			anIntArray1348 = new int[i_217_];
			anIntArray1371 = new int[i_217_];
			anIntArray1365 = new int[i_217_];
			anIntArray1379 = new int[i_217_];
			textures_mapping_n = new short[i_217_];
			anIntArray1344 = new int[i_217_];
			textures_mapping_type = new byte[i_217_];
			aByteArray1381 = new byte[i_217_];
		}
		faces_texture = new byte[i_216_];
		faces_b = new short[i_216_];
	}

	public Mesh(Mesh[] class132s, int i) {
		num_textures = 0;
		num_vertices = 0;
		num_faces = 0;
		int i_218_ = 0;
		int i_219_ = 0;
		int i_220_ = 0;
		boolean bool = false;
		boolean bool_221_ = false;
		boolean bool_222_ = false;
		boolean bool_223_ = false;
		boolean bool_224_ = false;
		aByte1372 = (byte) -1;
		boolean bool_225_ = false;
		for (int i_226_ = 0; i_226_ < i; i_226_++) {
			Mesh class132_227_ = class132s[i_226_];
			if (class132_227_ != null) {
				num_textures += class132_227_.num_textures;
				num_vertices += class132_227_.num_vertices;
				num_faces += class132_227_.num_faces;
				if (class132_227_.emitters != null) {
					i_218_ += class132_227_.emitters.length;
				}
				if (class132_227_.billboards != null) {
					i_220_ += class132_227_.billboards.length;
				}
				bool = bool | class132_227_.aByteArray1354 != null;
				if (class132_227_.effectors != null) {
					i_219_ += class132_227_.effectors.length;
				}
				bool_223_ = bool_223_ | class132_227_.faces_texture != null;
				if (class132_227_.faces_priority != null) {
					bool_221_ = true;
				} else {
					if (aByte1372 == -1) {
						aByte1372 = class132_227_.aByte1372;
					}
					if (class132_227_.aByte1372 != aByte1372) {
						bool_221_ = true;
					}
				}
				bool_222_ = bool_222_ | class132_227_.faces_alpha != null;
				bool_224_ = bool_224_ | class132_227_.faces_material != null;
				bool_225_ = bool_225_ | class132_227_.faces_label != null;
			}
		}
		vertices_label = new int[num_vertices];
		aShortArray1352 = new short[num_faces];
		vertices_x = new int[num_vertices];
		if (i_218_ > 0) {
			emitters = new EmissiveTriangle[i_218_];
		}
		if (bool_225_) {
			faces_label = new int[num_faces];
		}
		if (bool_223_) {
			faces_texture = new byte[num_faces];
		}
		if (i_219_ > 0) {
			effectors = new EffectiveVertex[i_219_];
		}
		faceS_c = new short[num_faces];
		faces_b = new short[num_faces];
		if (num_textures > 0) {
			textures_mapping_type = new byte[num_textures];
			textures_mapping_m = new short[num_textures];
			anIntArray1371 = new int[num_textures];
			anIntArray1347 = new int[num_textures];
			aByteArray1381 = new byte[num_textures];
			textures_mapping_p = new short[num_textures];
			textures_mapping_n = new short[num_textures];
			anIntArray1344 = new int[num_textures];
			anIntArray1348 = new int[num_textures];
			anIntArray1379 = new int[num_textures];
			aByteArray1367 = new byte[num_textures];
			anIntArray1365 = new int[num_textures];
		}
		if (i_220_ > 0) {
			billboards = new Billboard[i_220_];
		}
		if (bool_222_) {
			faces_alpha = new byte[num_faces];
		}
		aShortArray1366 = new short[num_vertices];
		faces_colour = new short[num_faces];
		if (bool_221_) {
			faces_priority = new byte[num_faces];
		}
		if (bool_224_) {
			faces_material = new short[num_faces];
		}
		vertices_z = new int[num_vertices];
		if (bool) {
			aByteArray1354 = new byte[num_faces];
		}
		faces_a = new short[num_faces];
		vertices_y = new int[num_vertices];
		i_219_ = 0;
		num_textures = 0;
		num_vertices = 0;
		i_218_ = 0;
		i_220_ = 0;
		num_faces = 0;
		for (int i_228_ = 0; i_228_ < i; i_228_++) {
			short i_229_ = (short) (1 << i_228_);
			Mesh class132_230_ = class132s[i_228_];
			if (class132_230_ != null) {
				if (class132_230_.billboards != null) {
					for (int i_231_ = 0; class132_230_.billboards.length > i_231_; i_231_++) {
						Billboard class385 = class132_230_.billboards[i_231_];
						billboards[i_220_++] = class385.method4021(-103, class385.face + num_faces);
					}
				}
				for (int i_232_ = 0; class132_230_.num_faces > i_232_; i_232_++) {
					if (bool && class132_230_.aByteArray1354 != null) {
						aByteArray1354[num_faces] = class132_230_.aByteArray1354[i_232_];
					}
					if (bool_221_) {
						if (class132_230_.faces_priority == null) {
							faces_priority[num_faces] = class132_230_.aByte1372;
						} else {
							faces_priority[num_faces] = class132_230_.faces_priority[i_232_];
						}
					}
					if (bool_222_ && class132_230_.faces_alpha != null) {
						faces_alpha[num_faces] = class132_230_.faces_alpha[i_232_];
					}
					if (bool_224_) {
						if (class132_230_.faces_material == null) {
							faces_material[num_faces] = (short) -1;
						} else {
							faces_material[num_faces] = class132_230_.faces_material[i_232_];
						}
					}
					if (bool_225_) {
						if (class132_230_.faces_label != null) {
							faces_label[num_faces] = class132_230_.faces_label[i_232_];
						} else {
							faces_label[num_faces] = -1;
						}
					}
					faces_a[num_faces] = (short) method1398(class132_230_.faces_a[i_232_], class132_230_, i_229_, (byte) 124);
					faces_b[num_faces] = (short) method1398(class132_230_.faces_b[i_232_], class132_230_, i_229_, (byte) 124);
					faceS_c[num_faces] = (short) method1398(class132_230_.faceS_c[i_232_], class132_230_, i_229_, (byte) 122);
					aShortArray1352[num_faces] = i_229_;
					faces_colour[num_faces] = class132_230_.faces_colour[i_232_];
					num_faces++;
				}
				if (class132_230_.emitters != null) {
					for (int i_233_ = 0; class132_230_.emitters.length > i_233_; i_233_++) {
						int i_234_ = method1398(class132_230_.emitters[i_233_].anInt954, class132_230_, i_229_, (byte) 125);
						int i_235_ = method1398(class132_230_.emitters[i_233_].anInt953, class132_230_, i_229_, (byte) 125);
						int i_236_ = method1398(class132_230_.emitters[i_233_].anInt964, class132_230_, i_229_, (byte) 126);
						emitters[i_218_] = class132_230_.emitters[i_233_].method832(i_236_, i_234_, 13007, i_235_);
						i_218_++;
					}
				}
				if (class132_230_.effectors != null) {
					for (int i_237_ = 0; i_237_ < class132_230_.effectors.length; i_237_++) {
						int i_238_ = method1398(class132_230_.effectors[i_237_].anInt2220, class132_230_, i_229_, (byte) 122);
						effectors[i_219_] = class132_230_.effectors[i_237_].method2113(i_238_, (byte) 125);
						i_219_++;
					}
				}
			}
		}
		int i_239_ = 0;
		highest_face_index = num_vertices;
		for (int i_240_ = 0; i > i_240_; i_240_++) {
			short i_241_ = (short) (1 << i_240_);
			Mesh class132_242_ = class132s[i_240_];
			if (class132_242_ != null) {
				for (int i_243_ = 0; class132_242_.num_faces > i_243_; i_243_++) {
					if (bool_223_) {
						faces_texture[i_239_++] = (byte) (class132_242_.faces_texture != null && class132_242_.faces_texture[i_243_] != -1 ? num_textures + class132_242_.faces_texture[i_243_] : -1);
					}
				}
				for (int i_244_ = 0; i_244_ < class132_242_.num_textures; i_244_++) {
					byte i_245_ = textures_mapping_type[num_textures] = class132_242_.textures_mapping_type[i_244_];
					if (i_245_ == 0) {
						textures_mapping_p[num_textures] = (short) method1398(class132_242_.textures_mapping_p[i_244_], class132_242_, i_241_, (byte) 127);
						textures_mapping_m[num_textures] = (short) method1398(class132_242_.textures_mapping_m[i_244_], class132_242_, i_241_, (byte) 124);
						textures_mapping_n[num_textures] = (short) method1398(class132_242_.textures_mapping_n[i_244_], class132_242_, i_241_, (byte) 122);
					}
					if (i_245_ >= 1 && i_245_ <= 3) {
						textures_mapping_p[num_textures] = class132_242_.textures_mapping_p[i_244_];
						textures_mapping_m[num_textures] = class132_242_.textures_mapping_m[i_244_];
						textures_mapping_n[num_textures] = class132_242_.textures_mapping_n[i_244_];
						anIntArray1344[num_textures] = class132_242_.anIntArray1344[i_244_];
						anIntArray1365[num_textures] = class132_242_.anIntArray1365[i_244_];
						anIntArray1379[num_textures] = class132_242_.anIntArray1379[i_244_];
						aByteArray1381[num_textures] = class132_242_.aByteArray1381[i_244_];
						aByteArray1367[num_textures] = class132_242_.aByteArray1367[i_244_];
						anIntArray1371[num_textures] = class132_242_.anIntArray1371[i_244_];
					}
					if (i_245_ == 2) {
						anIntArray1347[num_textures] = class132_242_.anIntArray1347[i_244_];
						anIntArray1348[num_textures] = class132_242_.anIntArray1348[i_244_];
					}
					num_textures++;
				}
			}
		}
	}
}
