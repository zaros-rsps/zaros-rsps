package net.zaros.client;

/* Class397_Sub1_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class397_Sub1_Sub3 extends Class397_Sub1 {
	int[] anIntArray6693;

	public void method4104(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		if (i_2_ > 0 && i_3_ > 0) {
			int i_8_ = 0;
			int i_9_ = 0;
			int i_10_ = anInt5772 + anInt5758 + anInt5757;
			int i_11_ = anInt5761 + anInt5756 + anInt5750;
			int i_12_ = (i_10_ << 16) / i_2_;
			int i_13_ = (i_11_ << 16) / i_3_;
			if (anInt5772 > 0) {
				int i_14_ = ((anInt5772 << 16) + i_12_ - 1) / i_12_;
				i += i_14_;
				i_8_ += i_14_ * i_12_ - (anInt5772 << 16);
			}
			if (anInt5761 > 0) {
				int i_15_ = ((anInt5761 << 16) + i_13_ - 1) / i_13_;
				i_0_ += i_15_;
				i_9_ += i_15_ * i_13_ - (anInt5761 << 16);
			}
			if (anInt5758 < i_10_)
				i_2_ = ((anInt5758 << 16) - i_8_ + i_12_ - 1) / i_12_;
			if (anInt5756 < i_11_)
				i_3_ = ((anInt5756 << 16) - i_9_ + i_13_ - 1) / i_13_;
			int i_16_ = i + i_0_ * aHa_Sub2_5768.anInt4084;
			int i_17_ = aHa_Sub2_5768.anInt4084 - i_2_;
			if (i_0_ + i_3_ > aHa_Sub2_5768.anInt4078)
				i_3_ -= i_0_ + i_3_ - aHa_Sub2_5768.anInt4078;
			if (i_0_ < aHa_Sub2_5768.anInt4085) {
				int i_18_ = aHa_Sub2_5768.anInt4085 - i_0_;
				i_3_ -= i_18_;
				i_16_ += i_18_ * aHa_Sub2_5768.anInt4084;
				i_9_ += i_13_ * i_18_;
			}
			if (i + i_2_ > aHa_Sub2_5768.anInt4104) {
				int i_19_ = i + i_2_ - aHa_Sub2_5768.anInt4104;
				i_2_ -= i_19_;
				i_17_ += i_19_;
			}
			if (i < aHa_Sub2_5768.anInt4079) {
				int i_20_ = aHa_Sub2_5768.anInt4079 - i;
				i_2_ -= i_20_;
				i_16_ += i_20_;
				i_8_ += i_12_ * i_20_;
				i_17_ += i_20_;
			}
			float[] fs = aHa_Sub2_5768.aFloatArray4074;
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_6_ == 0) {
				if (i_4_ == 1) {
					int i_21_ = i_8_;
					for (int i_22_ = -i_3_; i_22_ < 0; i_22_++) {
						int i_23_ = (i_9_ >> 16) * anInt5758;
						for (int i_24_ = -i_2_; i_24_ < 0; i_24_++) {
							if ((float) i_1_ < fs[i_16_]) {
								is[i_16_] = anIntArray6693[(i_8_ >> 16) + i_23_];
								fs[i_16_] = (float) i_1_;
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_21_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_4_ == 0) {
					int i_25_ = (i_5_ & 0xff0000) >> 16;
					int i_26_ = (i_5_ & 0xff00) >> 8;
					int i_27_ = i_5_ & 0xff;
					int i_28_ = i_8_;
					for (int i_29_ = -i_3_; i_29_ < 0; i_29_++) {
						int i_30_ = (i_9_ >> 16) * anInt5758;
						for (int i_31_ = -i_2_; i_31_ < 0; i_31_++) {
							if ((float) i_1_ < fs[i_16_]) {
								int i_32_ = anIntArray6693[(i_8_ >> 16) + i_30_];
								int i_33_ = (i_32_ & 0xff0000) * i_25_ & ~0xffffff;
								int i_34_ = (i_32_ & 0xff00) * i_26_ & 0xff0000;
								int i_35_ = (i_32_ & 0xff) * i_27_ & 0xff00;
								is[i_16_] = (i_33_ | i_34_ | i_35_) >>> 8;
								fs[i_16_] = (float) i_1_;
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_28_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_4_ == 3) {
					int i_36_ = i_8_;
					for (int i_37_ = -i_3_; i_37_ < 0; i_37_++) {
						int i_38_ = (i_9_ >> 16) * anInt5758;
						for (int i_39_ = -i_2_; i_39_ < 0; i_39_++) {
							if ((float) i_1_ < fs[i_16_]) {
								int i_40_ = anIntArray6693[(i_8_ >> 16) + i_38_];
								int i_41_ = i_40_ + i_5_;
								int i_42_ = (i_40_ & 0xff00ff) + (i_5_ & 0xff00ff);
								int i_43_ = ((i_42_ & 0x1000100) + (i_41_ - i_42_ & 0x10000));
								is[i_16_] = i_41_ - i_43_ | i_43_ - (i_43_ >>> 8);
								fs[i_16_] = (float) i_1_;
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_36_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_4_ == 2) {
					int i_44_ = i_5_ >>> 24;
					int i_45_ = 256 - i_44_;
					int i_46_ = (i_5_ & 0xff00ff) * i_45_ & ~0xff00ff;
					int i_47_ = (i_5_ & 0xff00) * i_45_ & 0xff0000;
					i_5_ = (i_46_ | i_47_) >>> 8;
					int i_48_ = i_8_;
					for (int i_49_ = -i_3_; i_49_ < 0; i_49_++) {
						int i_50_ = (i_9_ >> 16) * anInt5758;
						for (int i_51_ = -i_2_; i_51_ < 0; i_51_++) {
							if ((float) i_1_ < fs[i_16_]) {
								int i_52_ = anIntArray6693[(i_8_ >> 16) + i_50_];
								i_46_ = (i_52_ & 0xff00ff) * i_44_ & ~0xff00ff;
								i_47_ = (i_52_ & 0xff00) * i_44_ & 0xff0000;
								is[i_16_] = ((i_46_ | i_47_) >>> 8) + i_5_;
								fs[i_16_] = (float) i_1_;
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_48_;
						i_16_ += i_17_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_6_ == 1) {
				if (i_4_ == 1) {
					int i_53_ = i_8_;
					for (int i_54_ = -i_3_; i_54_ < 0; i_54_++) {
						int i_55_ = (i_9_ >> 16) * anInt5758;
						for (int i_56_ = -i_2_; i_56_ < 0; i_56_++) {
							if ((float) i_1_ < fs[i_16_]) {
								int i_57_ = anIntArray6693[(i_8_ >> 16) + i_55_];
								if (i_57_ != 0) {
									is[i_16_] = i_57_;
									fs[i_16_] = (float) i_1_;
								}
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_53_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_4_ == 0) {
					int i_58_ = i_8_;
					if ((i_5_ & 0xffffff) == 16777215) {
						int i_59_ = i_5_ >>> 24;
						int i_60_ = 256 - i_59_;
						for (int i_61_ = -i_3_; i_61_ < 0; i_61_++) {
							int i_62_ = (i_9_ >> 16) * anInt5758;
							for (int i_63_ = -i_2_; i_63_ < 0; i_63_++) {
								if ((float) i_1_ < fs[i_16_]) {
									int i_64_ = anIntArray6693[(i_8_ >> 16) + i_62_];
									if (i_64_ != 0) {
										int i_65_ = is[i_16_];
										is[i_16_] = ((((i_64_ & 0xff00ff) * i_59_ + (i_65_ & 0xff00ff) * i_60_)
												& ~0xff00ff)
												+ (((i_64_ & 0xff00) * i_59_ + (i_65_ & 0xff00) * i_60_)
														& 0xff0000)) >> 8;
										fs[i_16_] = (float) i_1_;
									}
								}
								i_8_ += i_12_;
								i_16_++;
							}
							i_9_ += i_13_;
							i_8_ = i_58_;
							i_16_ += i_17_;
						}
					} else {
						int i_66_ = (i_5_ & 0xff0000) >> 16;
						int i_67_ = (i_5_ & 0xff00) >> 8;
						int i_68_ = i_5_ & 0xff;
						int i_69_ = i_5_ >>> 24;
						int i_70_ = 256 - i_69_;
						for (int i_71_ = -i_3_; i_71_ < 0; i_71_++) {
							int i_72_ = (i_9_ >> 16) * anInt5758;
							for (int i_73_ = -i_2_; i_73_ < 0; i_73_++) {
								if ((float) i_1_ < fs[i_16_]) {
									int i_74_ = anIntArray6693[(i_8_ >> 16) + i_72_];
									if (i_74_ != 0) {
										if (i_69_ != 255) {
											int i_75_ = ((i_74_ & 0xff0000) * i_66_ & ~0xffffff);
											int i_76_ = ((i_74_ & 0xff00) * i_67_ & 0xff0000);
											int i_77_ = ((i_74_ & 0xff) * i_68_ & 0xff00);
											i_74_ = ((i_75_ | i_76_ | i_77_) >>> 8);
											int i_78_ = is[i_16_];
											is[i_16_] = ((((i_74_ & 0xff00ff) * i_69_ + ((i_78_ & 0xff00ff) * i_70_))
													& ~0xff00ff)
													+ (((i_74_ & 0xff00) * i_69_ + ((i_78_ & 0xff00) * i_70_))
															& 0xff0000)) >> 8;
											fs[i_16_] = (float) i_1_;
										} else {
											int i_79_ = ((i_74_ & 0xff0000) * i_66_ & ~0xffffff);
											int i_80_ = ((i_74_ & 0xff00) * i_67_ & 0xff0000);
											int i_81_ = ((i_74_ & 0xff) * i_68_ & 0xff00);
											is[i_16_] = (i_79_ | i_80_ | i_81_) >>> 8;
											fs[i_16_] = (float) i_1_;
										}
									}
								}
								i_8_ += i_12_;
								i_16_++;
							}
							i_9_ += i_13_;
							i_8_ = i_58_;
							i_16_ += i_17_;
						}
						return;
					}
					return;
				}
				if (i_4_ == 3) {
					int i_82_ = i_8_;
					int i_83_ = i_5_ >>> 24;
					int i_84_ = 256 - i_83_;
					for (int i_85_ = -i_3_; i_85_ < 0; i_85_++) {
						int i_86_ = (i_9_ >> 16) * anInt5758;
						for (int i_87_ = -i_2_; i_87_ < 0; i_87_++) {
							if ((float) i_1_ < fs[i_16_]) {
								int i_88_ = anIntArray6693[(i_8_ >> 16) + i_86_];
								int i_89_ = i_88_ + i_5_;
								int i_90_ = (i_88_ & 0xff00ff) + (i_5_ & 0xff00ff);
								int i_91_ = ((i_90_ & 0x1000100) + (i_89_ - i_90_ & 0x10000));
								i_91_ = i_89_ - i_91_ | i_91_ - (i_91_ >>> 8);
								if (i_88_ == 0 && i_83_ != 255) {
									i_88_ = i_91_;
									i_91_ = is[i_16_];
									i_91_ = ((((i_88_ & 0xff00ff) * i_83_ + (i_91_ & 0xff00ff) * i_84_) & ~0xff00ff)
											+ (((i_88_ & 0xff00) * i_83_ + (i_91_ & 0xff00) * i_84_) & 0xff0000)) >> 8;
								}
								is[i_16_] = i_91_;
								fs[i_16_] = (float) i_1_;
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_82_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_4_ == 2) {
					int i_92_ = i_5_ >>> 24;
					int i_93_ = 256 - i_92_;
					int i_94_ = (i_5_ & 0xff00ff) * i_93_ & ~0xff00ff;
					int i_95_ = (i_5_ & 0xff00) * i_93_ & 0xff0000;
					i_5_ = (i_94_ | i_95_) >>> 8;
					int i_96_ = i_8_;
					for (int i_97_ = -i_3_; i_97_ < 0; i_97_++) {
						int i_98_ = (i_9_ >> 16) * anInt5758;
						for (int i_99_ = -i_2_; i_99_ < 0; i_99_++) {
							if ((float) i_1_ < fs[i_16_]) {
								int i_100_ = anIntArray6693[(i_8_ >> 16) + i_98_];
								if (i_100_ != 0) {
									i_94_ = ((i_100_ & 0xff00ff) * i_92_ & ~0xff00ff);
									i_95_ = (i_100_ & 0xff00) * i_92_ & 0xff0000;
									is[i_16_] = ((i_94_ | i_95_) >>> 8) + i_5_;
									fs[i_16_] = (float) i_1_;
								}
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_96_;
						i_16_ += i_17_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_6_ == 2) {
				if (i_4_ == 1) {
					int i_101_ = i_8_;
					for (int i_102_ = -i_3_; i_102_ < 0; i_102_++) {
						int i_103_ = (i_9_ >> 16) * anInt5758;
						for (int i_104_ = -i_2_; i_104_ < 0; i_104_++) {
							if ((float) i_1_ < fs[i_16_]) {
								int i_105_ = anIntArray6693[(i_8_ >> 16) + i_103_];
								if (i_105_ != 0) {
									int i_106_ = is[i_16_];
									int i_107_ = i_105_ + i_106_;
									int i_108_ = ((i_105_ & 0xff00ff) + (i_106_ & 0xff00ff));
									i_106_ = ((i_108_ & 0x1000100) + (i_107_ - i_108_ & 0x10000));
									is[i_16_] = i_107_ - i_106_ | i_106_ - (i_106_ >>> 8);
									fs[i_16_] = (float) i_1_;
								}
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_101_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_4_ == 0) {
					int i_109_ = i_8_;
					int i_110_ = (i_5_ & 0xff0000) >> 16;
					int i_111_ = (i_5_ & 0xff00) >> 8;
					int i_112_ = i_5_ & 0xff;
					for (int i_113_ = -i_3_; i_113_ < 0; i_113_++) {
						int i_114_ = (i_9_ >> 16) * anInt5758;
						for (int i_115_ = -i_2_; i_115_ < 0; i_115_++) {
							if ((float) i_1_ < fs[i_16_]) {
								int i_116_ = anIntArray6693[(i_8_ >> 16) + i_114_];
								if (i_116_ != 0) {
									int i_117_ = ((i_116_ & 0xff0000) * i_110_ & ~0xffffff);
									int i_118_ = ((i_116_ & 0xff00) * i_111_ & 0xff0000);
									int i_119_ = (i_116_ & 0xff) * i_112_ & 0xff00;
									i_116_ = (i_117_ | i_118_ | i_119_) >>> 8;
									int i_120_ = is[i_16_];
									int i_121_ = i_116_ + i_120_;
									int i_122_ = ((i_116_ & 0xff00ff) + (i_120_ & 0xff00ff));
									i_120_ = ((i_122_ & 0x1000100) + (i_121_ - i_122_ & 0x10000));
									is[i_16_] = i_121_ - i_120_ | i_120_ - (i_120_ >>> 8);
									fs[i_16_] = (float) i_1_;
								}
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_109_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_4_ == 3) {
					int i_123_ = i_8_;
					for (int i_124_ = -i_3_; i_124_ < 0; i_124_++) {
						int i_125_ = (i_9_ >> 16) * anInt5758;
						for (int i_126_ = -i_2_; i_126_ < 0; i_126_++) {
							if ((float) i_1_ < fs[i_16_]) {
								int i_127_ = anIntArray6693[(i_8_ >> 16) + i_125_];
								int i_128_ = i_127_ + i_5_;
								int i_129_ = (i_127_ & 0xff00ff) + (i_5_ & 0xff00ff);
								int i_130_ = ((i_129_ & 0x1000100) + (i_128_ - i_129_ & 0x10000));
								i_127_ = i_128_ - i_130_ | i_130_ - (i_130_ >>> 8);
								i_130_ = is[i_16_];
								i_128_ = i_127_ + i_130_;
								i_129_ = (i_127_ & 0xff00ff) + (i_130_ & 0xff00ff);
								i_130_ = (i_129_ & 0x1000100) + (i_128_ - i_129_ & 0x10000);
								is[i_16_] = i_128_ - i_130_ | i_130_ - (i_130_ >>> 8);
								fs[i_16_] = (float) i_1_;
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_123_;
						i_16_ += i_17_;
					}
					return;
				}
				if (i_4_ == 2) {
					int i_131_ = i_5_ >>> 24;
					int i_132_ = 256 - i_131_;
					int i_133_ = (i_5_ & 0xff00ff) * i_132_ & ~0xff00ff;
					int i_134_ = (i_5_ & 0xff00) * i_132_ & 0xff0000;
					i_5_ = (i_133_ | i_134_) >>> 8;
					int i_135_ = i_8_;
					for (int i_136_ = -i_3_; i_136_ < 0; i_136_++) {
						int i_137_ = (i_9_ >> 16) * anInt5758;
						for (int i_138_ = -i_2_; i_138_ < 0; i_138_++) {
							if ((float) i_1_ < fs[i_16_]) {
								int i_139_ = anIntArray6693[(i_8_ >> 16) + i_137_];
								if (i_139_ != 0) {
									i_133_ = ((i_139_ & 0xff00ff) * i_131_ & ~0xff00ff);
									i_134_ = ((i_139_ & 0xff00) * i_131_ & 0xff0000);
									i_139_ = ((i_133_ | i_134_) >>> 8) + i_5_;
									int i_140_ = is[i_16_];
									int i_141_ = i_139_ + i_140_;
									int i_142_ = ((i_139_ & 0xff00ff) + (i_140_ & 0xff00ff));
									i_140_ = ((i_142_ & 0x1000100) + (i_141_ - i_142_ & 0x10000));
									is[i_16_] = i_141_ - i_140_ | i_140_ - (i_140_ >>> 8);
									fs[i_16_] = (float) i_1_;
								}
							}
							i_8_ += i_12_;
							i_16_++;
						}
						i_9_ += i_13_;
						i_8_ = i_135_;
						i_16_ += i_17_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	Class397_Sub1_Sub3(ha_Sub2 var_ha_Sub2, int[] is, int i, int i_143_, int i_144_, int i_145_, boolean bool) {
		super(var_ha_Sub2, i_144_, i_145_);
		if (bool)
			anIntArray6693 = new int[i_144_ * i_145_];
		else
			anIntArray6693 = is;
		i_143_ -= anInt5758;
		int i_146_ = 0;
		for (int i_147_ = 0; i_147_ < i_145_; i_147_++) {
			for (int i_148_ = 0; i_148_ < i_144_; i_148_++) {
				int i_149_ = is[i++];
				if (i_149_ >>> 24 == 255)
					anIntArray6693[i_146_++] = (i_149_ & 0xffffff) == 0 ? -16777215 : i_149_;
				else
					anIntArray6693[i_146_++] = 0;
			}
			i += i_143_;
		}
	}

	public void method4101(int i, int i_150_) {
		int[] is = aHa_Sub2_5768.anIntArray4108;
		if (Class397_Sub1.anInt5766 == 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_151_ = Class397_Sub1.anInt5771;
				while (i_151_ < 0) {
					int i_152_ = Class397_Sub1.anInt5759;
					int i_153_ = Class397_Sub1.anInt5765;
					int i_154_ = Class397_Sub1.anInt5775;
					int i_155_ = Class397_Sub1.anInt5770;
					if (i_153_ >= 0 && i_154_ >= 0 && i_153_ - (anInt5758 << 12) < 0
							&& i_154_ - (anInt5756 << 12) < 0) {
						for (/**/; i_155_ < 0; i_155_++) {
							int i_156_ = (i_154_ >> 12) * anInt5758 + (i_153_ >> 12);
							int i_157_ = i_152_++;
							int[] is_158_ = is;
							int i_159_ = i;
							int i_160_ = i_150_;
							if (i_160_ == 0) {
								if (i_159_ == 1)
									is_158_[i_157_] = anIntArray6693[i_156_];
								else if (i_159_ == 0) {
									int i_161_ = anIntArray6693[i_156_++];
									int i_162_ = (((i_161_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_163_ = (((i_161_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_164_ = (((i_161_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_158_[i_157_] = (i_162_ | i_163_ | i_164_) >>> 8;
								} else if (i_159_ == 3) {
									int i_165_ = anIntArray6693[i_156_++];
									int i_166_ = Class397_Sub1.anInt5767;
									int i_167_ = i_165_ + i_166_;
									int i_168_ = ((i_165_ & 0xff00ff) + (i_166_ & 0xff00ff));
									int i_169_ = ((i_168_ & 0x1000100) + (i_167_ - i_168_ & 0x10000));
									is_158_[i_157_] = i_167_ - i_169_ | i_169_ - (i_169_ >>> 8);
								} else if (i_159_ == 2) {
									int i_170_ = anIntArray6693[i_156_];
									int i_171_ = (((i_170_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_172_ = (((i_170_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_158_[i_157_] = (((i_171_ | i_172_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_160_ == 1) {
								if (i_159_ == 1) {
									int i_173_ = anIntArray6693[i_156_];
									if (i_173_ != 0)
										is_158_[i_157_] = i_173_;
								} else if (i_159_ == 0) {
									int i_174_ = anIntArray6693[i_156_];
									if (i_174_ != 0) {
										if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
											int i_175_ = (Class397_Sub1.anInt5767 >>> 24);
											int i_176_ = 256 - i_175_;
											int i_177_ = is_158_[i_157_];
											is_158_[i_157_] = (((((i_174_ & 0xff00ff) * i_175_)
													+ ((i_177_ & 0xff00ff) * i_176_)) & ~0xff00ff)
													+ ((((i_174_ & 0xff00) * i_175_) + ((i_177_ & 0xff00) * i_176_))
															& 0xff0000)) >> 8;
										} else if (Class397_Sub1.anInt5755 != 255) {
											int i_178_ = (((i_174_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_179_ = (((i_174_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_180_ = (((i_174_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											i_174_ = (i_178_ | i_179_ | i_180_) >>> 8;
											int i_181_ = is_158_[i_157_];
											is_158_[i_157_] = (((((i_174_ & 0xff00ff) * (Class397_Sub1.anInt5755))
													+ ((i_181_ & 0xff00ff) * (Class397_Sub1.anInt5749))) & ~0xff00ff)
													+ ((((i_174_ & 0xff00) * (Class397_Sub1.anInt5755))
															+ ((i_181_ & 0xff00) * (Class397_Sub1.anInt5749)))
															& 0xff0000)) >> 8;
										} else {
											int i_182_ = (((i_174_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_183_ = (((i_174_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_184_ = (((i_174_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											is_158_[i_157_] = (i_182_ | i_183_ | i_184_) >>> 8;
										}
									}
								} else if (i_159_ == 3) {
									int i_185_ = anIntArray6693[i_156_];
									int i_186_ = Class397_Sub1.anInt5767;
									int i_187_ = i_185_ + i_186_;
									int i_188_ = ((i_185_ & 0xff00ff) + (i_186_ & 0xff00ff));
									int i_189_ = ((i_188_ & 0x1000100) + (i_187_ - i_188_ & 0x10000));
									i_189_ = i_187_ - i_189_ | i_189_ - (i_189_ >>> 8);
									if (i_185_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_185_ = i_189_;
										i_189_ = is_158_[i_157_];
										i_189_ = (((((i_185_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_189_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_185_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_189_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_158_[i_157_] = i_189_;
								} else if (i_159_ == 2) {
									int i_190_ = anIntArray6693[i_156_];
									if (i_190_ != 0) {
										int i_191_ = (((i_190_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_192_ = (((i_190_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										is_158_[i_157_++] = (((i_191_ | i_192_) >>> 8) + Class397_Sub1.anInt5773);
									}
								} else
									throw new IllegalArgumentException();
							} else if (i_160_ == 2) {
								if (i_159_ == 1) {
									int i_193_ = anIntArray6693[i_156_];
									if (i_193_ != 0) {
										int i_194_ = is_158_[i_157_];
										int i_195_ = i_193_ + i_194_;
										int i_196_ = ((i_193_ & 0xff00ff) + (i_194_ & 0xff00ff));
										i_194_ = ((i_196_ & 0x1000100) + (i_195_ - i_196_ & 0x10000));
										is_158_[i_157_] = (i_195_ - i_194_ | i_194_ - (i_194_ >>> 8));
									}
								} else if (i_159_ == 0) {
									int i_197_ = anIntArray6693[i_156_];
									if (i_197_ != 0) {
										int i_198_ = (((i_197_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_199_ = (((i_197_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_200_ = (((i_197_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_197_ = (i_198_ | i_199_ | i_200_) >>> 8;
										int i_201_ = is_158_[i_157_];
										int i_202_ = i_197_ + i_201_;
										int i_203_ = ((i_197_ & 0xff00ff) + (i_201_ & 0xff00ff));
										i_201_ = ((i_203_ & 0x1000100) + (i_202_ - i_203_ & 0x10000));
										is_158_[i_157_] = (i_202_ - i_201_ | i_201_ - (i_201_ >>> 8));
									}
								} else if (i_159_ == 3) {
									int i_204_ = anIntArray6693[i_156_];
									int i_205_ = Class397_Sub1.anInt5767;
									int i_206_ = i_204_ + i_205_;
									int i_207_ = ((i_204_ & 0xff00ff) + (i_205_ & 0xff00ff));
									int i_208_ = ((i_207_ & 0x1000100) + (i_206_ - i_207_ & 0x10000));
									i_204_ = i_206_ - i_208_ | i_208_ - (i_208_ >>> 8);
									i_208_ = is_158_[i_157_];
									i_206_ = i_204_ + i_208_;
									i_207_ = (i_204_ & 0xff00ff) + (i_208_ & 0xff00ff);
									i_208_ = ((i_207_ & 0x1000100) + (i_206_ - i_207_ & 0x10000));
									is_158_[i_157_] = i_206_ - i_208_ | i_208_ - (i_208_ >>> 8);
								} else if (i_159_ == 2) {
									int i_209_ = anIntArray6693[i_156_];
									if (i_209_ != 0) {
										int i_210_ = (((i_209_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_211_ = (((i_209_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										i_209_ = (((i_210_ | i_211_) >>> 8) + Class397_Sub1.anInt5773);
										int i_212_ = is_158_[i_157_];
										int i_213_ = i_209_ + i_212_;
										int i_214_ = ((i_209_ & 0xff00ff) + (i_212_ & 0xff00ff));
										i_212_ = ((i_214_ & 0x1000100) + (i_213_ - i_214_ & 0x10000));
										is_158_[i_157_] = (i_213_ - i_212_ | i_212_ - (i_212_ >>> 8));
									}
								}
							} else
								throw new IllegalArgumentException();
						}
					}
					i_151_++;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_215_ = Class397_Sub1.anInt5771;
				while (i_215_ < 0) {
					int i_216_ = Class397_Sub1.anInt5759;
					int i_217_ = Class397_Sub1.anInt5765;
					int i_218_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_219_ = Class397_Sub1.anInt5770;
					if (i_217_ >= 0 && i_217_ - (anInt5758 << 12) < 0) {
						int i_220_;
						if ((i_220_ = i_218_ - (anInt5756 << 12)) >= 0) {
							i_220_ = ((Class397_Sub1.anInt5777 - i_220_) / Class397_Sub1.anInt5777);
							i_219_ += i_220_;
							i_218_ += Class397_Sub1.anInt5777 * i_220_;
							i_216_ += i_220_;
						}
						if ((i_220_ = ((i_218_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_219_)
							i_219_ = i_220_;
						for (/**/; i_219_ < 0; i_219_++) {
							int i_221_ = (i_218_ >> 12) * anInt5758 + (i_217_ >> 12);
							int i_222_ = i_216_++;
							int[] is_223_ = is;
							int i_224_ = i;
							int i_225_ = i_150_;
							if (i_225_ == 0) {
								if (i_224_ == 1)
									is_223_[i_222_] = anIntArray6693[i_221_];
								else if (i_224_ == 0) {
									int i_226_ = anIntArray6693[i_221_++];
									int i_227_ = (((i_226_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_228_ = (((i_226_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_229_ = (((i_226_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_223_[i_222_] = (i_227_ | i_228_ | i_229_) >>> 8;
								} else if (i_224_ == 3) {
									int i_230_ = anIntArray6693[i_221_++];
									int i_231_ = Class397_Sub1.anInt5767;
									int i_232_ = i_230_ + i_231_;
									int i_233_ = ((i_230_ & 0xff00ff) + (i_231_ & 0xff00ff));
									int i_234_ = ((i_233_ & 0x1000100) + (i_232_ - i_233_ & 0x10000));
									is_223_[i_222_] = i_232_ - i_234_ | i_234_ - (i_234_ >>> 8);
								} else if (i_224_ == 2) {
									int i_235_ = anIntArray6693[i_221_];
									int i_236_ = (((i_235_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_237_ = (((i_235_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_223_[i_222_] = (((i_236_ | i_237_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_225_ == 1) {
								if (i_224_ == 1) {
									int i_238_ = anIntArray6693[i_221_];
									if (i_238_ != 0)
										is_223_[i_222_] = i_238_;
								} else if (i_224_ == 0) {
									int i_239_ = anIntArray6693[i_221_];
									if (i_239_ != 0) {
										if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
											int i_240_ = (Class397_Sub1.anInt5767 >>> 24);
											int i_241_ = 256 - i_240_;
											int i_242_ = is_223_[i_222_];
											is_223_[i_222_] = (((((i_239_ & 0xff00ff) * i_240_)
													+ ((i_242_ & 0xff00ff) * i_241_)) & ~0xff00ff)
													+ ((((i_239_ & 0xff00) * i_240_) + ((i_242_ & 0xff00) * i_241_))
															& 0xff0000)) >> 8;
										} else if (Class397_Sub1.anInt5755 != 255) {
											int i_243_ = (((i_239_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_244_ = (((i_239_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_245_ = (((i_239_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											i_239_ = (i_243_ | i_244_ | i_245_) >>> 8;
											int i_246_ = is_223_[i_222_];
											is_223_[i_222_] = (((((i_239_ & 0xff00ff) * (Class397_Sub1.anInt5755))
													+ ((i_246_ & 0xff00ff) * (Class397_Sub1.anInt5749))) & ~0xff00ff)
													+ ((((i_239_ & 0xff00) * (Class397_Sub1.anInt5755))
															+ ((i_246_ & 0xff00) * (Class397_Sub1.anInt5749)))
															& 0xff0000)) >> 8;
										} else {
											int i_247_ = (((i_239_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_248_ = (((i_239_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_249_ = (((i_239_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											is_223_[i_222_] = (i_247_ | i_248_ | i_249_) >>> 8;
										}
									}
								} else if (i_224_ == 3) {
									int i_250_ = anIntArray6693[i_221_];
									int i_251_ = Class397_Sub1.anInt5767;
									int i_252_ = i_250_ + i_251_;
									int i_253_ = ((i_250_ & 0xff00ff) + (i_251_ & 0xff00ff));
									int i_254_ = ((i_253_ & 0x1000100) + (i_252_ - i_253_ & 0x10000));
									i_254_ = i_252_ - i_254_ | i_254_ - (i_254_ >>> 8);
									if (i_250_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_250_ = i_254_;
										i_254_ = is_223_[i_222_];
										i_254_ = (((((i_250_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_254_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_250_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_254_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_223_[i_222_] = i_254_;
								} else if (i_224_ == 2) {
									int i_255_ = anIntArray6693[i_221_];
									if (i_255_ != 0) {
										int i_256_ = (((i_255_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_257_ = (((i_255_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										is_223_[i_222_++] = (((i_256_ | i_257_) >>> 8) + Class397_Sub1.anInt5773);
									}
								} else
									throw new IllegalArgumentException();
							} else if (i_225_ == 2) {
								if (i_224_ == 1) {
									int i_258_ = anIntArray6693[i_221_];
									if (i_258_ != 0) {
										int i_259_ = is_223_[i_222_];
										int i_260_ = i_258_ + i_259_;
										int i_261_ = ((i_258_ & 0xff00ff) + (i_259_ & 0xff00ff));
										i_259_ = ((i_261_ & 0x1000100) + (i_260_ - i_261_ & 0x10000));
										is_223_[i_222_] = (i_260_ - i_259_ | i_259_ - (i_259_ >>> 8));
									}
								} else if (i_224_ == 0) {
									int i_262_ = anIntArray6693[i_221_];
									if (i_262_ != 0) {
										int i_263_ = (((i_262_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_264_ = (((i_262_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_265_ = (((i_262_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_262_ = (i_263_ | i_264_ | i_265_) >>> 8;
										int i_266_ = is_223_[i_222_];
										int i_267_ = i_262_ + i_266_;
										int i_268_ = ((i_262_ & 0xff00ff) + (i_266_ & 0xff00ff));
										i_266_ = ((i_268_ & 0x1000100) + (i_267_ - i_268_ & 0x10000));
										is_223_[i_222_] = (i_267_ - i_266_ | i_266_ - (i_266_ >>> 8));
									}
								} else if (i_224_ == 3) {
									int i_269_ = anIntArray6693[i_221_];
									int i_270_ = Class397_Sub1.anInt5767;
									int i_271_ = i_269_ + i_270_;
									int i_272_ = ((i_269_ & 0xff00ff) + (i_270_ & 0xff00ff));
									int i_273_ = ((i_272_ & 0x1000100) + (i_271_ - i_272_ & 0x10000));
									i_269_ = i_271_ - i_273_ | i_273_ - (i_273_ >>> 8);
									i_273_ = is_223_[i_222_];
									i_271_ = i_269_ + i_273_;
									i_272_ = (i_269_ & 0xff00ff) + (i_273_ & 0xff00ff);
									i_273_ = ((i_272_ & 0x1000100) + (i_271_ - i_272_ & 0x10000));
									is_223_[i_222_] = i_271_ - i_273_ | i_273_ - (i_273_ >>> 8);
								} else if (i_224_ == 2) {
									int i_274_ = anIntArray6693[i_221_];
									if (i_274_ != 0) {
										int i_275_ = (((i_274_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_276_ = (((i_274_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										i_274_ = (((i_275_ | i_276_) >>> 8) + Class397_Sub1.anInt5773);
										int i_277_ = is_223_[i_222_];
										int i_278_ = i_274_ + i_277_;
										int i_279_ = ((i_274_ & 0xff00ff) + (i_277_ & 0xff00ff));
										i_277_ = ((i_279_ & 0x1000100) + (i_278_ - i_279_ & 0x10000));
										is_223_[i_222_] = (i_278_ - i_277_ | i_277_ - (i_277_ >>> 8));
									}
								}
							} else
								throw new IllegalArgumentException();
							i_218_ += Class397_Sub1.anInt5777;
						}
					}
					i_215_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_280_ = Class397_Sub1.anInt5771;
				while (i_280_ < 0) {
					int i_281_ = Class397_Sub1.anInt5759;
					int i_282_ = Class397_Sub1.anInt5765;
					int i_283_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_284_ = Class397_Sub1.anInt5770;
					if (i_282_ >= 0 && i_282_ - (anInt5758 << 12) < 0) {
						if (i_283_ < 0) {
							int i_285_ = ((Class397_Sub1.anInt5777 - 1 - i_283_) / Class397_Sub1.anInt5777);
							i_284_ += i_285_;
							i_283_ += Class397_Sub1.anInt5777 * i_285_;
							i_281_ += i_285_;
						}
						int i_286_;
						if ((i_286_ = ((i_283_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
								/ Class397_Sub1.anInt5777)) > i_284_)
							i_284_ = i_286_;
						for (/**/; i_284_ < 0; i_284_++) {
							int i_287_ = (i_283_ >> 12) * anInt5758 + (i_282_ >> 12);
							int i_288_ = i_281_++;
							int[] is_289_ = is;
							int i_290_ = i;
							int i_291_ = i_150_;
							if (i_291_ == 0) {
								if (i_290_ == 1)
									is_289_[i_288_] = anIntArray6693[i_287_];
								else if (i_290_ == 0) {
									int i_292_ = anIntArray6693[i_287_++];
									int i_293_ = (((i_292_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_294_ = (((i_292_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_295_ = (((i_292_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_289_[i_288_] = (i_293_ | i_294_ | i_295_) >>> 8;
								} else if (i_290_ == 3) {
									int i_296_ = anIntArray6693[i_287_++];
									int i_297_ = Class397_Sub1.anInt5767;
									int i_298_ = i_296_ + i_297_;
									int i_299_ = ((i_296_ & 0xff00ff) + (i_297_ & 0xff00ff));
									int i_300_ = ((i_299_ & 0x1000100) + (i_298_ - i_299_ & 0x10000));
									is_289_[i_288_] = i_298_ - i_300_ | i_300_ - (i_300_ >>> 8);
								} else if (i_290_ == 2) {
									int i_301_ = anIntArray6693[i_287_];
									int i_302_ = (((i_301_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_303_ = (((i_301_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_289_[i_288_] = (((i_302_ | i_303_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_291_ == 1) {
								if (i_290_ == 1) {
									int i_304_ = anIntArray6693[i_287_];
									if (i_304_ != 0)
										is_289_[i_288_] = i_304_;
								} else if (i_290_ == 0) {
									int i_305_ = anIntArray6693[i_287_];
									if (i_305_ != 0) {
										if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
											int i_306_ = (Class397_Sub1.anInt5767 >>> 24);
											int i_307_ = 256 - i_306_;
											int i_308_ = is_289_[i_288_];
											is_289_[i_288_] = (((((i_305_ & 0xff00ff) * i_306_)
													+ ((i_308_ & 0xff00ff) * i_307_)) & ~0xff00ff)
													+ ((((i_305_ & 0xff00) * i_306_) + ((i_308_ & 0xff00) * i_307_))
															& 0xff0000)) >> 8;
										} else if (Class397_Sub1.anInt5755 != 255) {
											int i_309_ = (((i_305_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_310_ = (((i_305_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_311_ = (((i_305_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											i_305_ = (i_309_ | i_310_ | i_311_) >>> 8;
											int i_312_ = is_289_[i_288_];
											is_289_[i_288_] = (((((i_305_ & 0xff00ff) * (Class397_Sub1.anInt5755))
													+ ((i_312_ & 0xff00ff) * (Class397_Sub1.anInt5749))) & ~0xff00ff)
													+ ((((i_305_ & 0xff00) * (Class397_Sub1.anInt5755))
															+ ((i_312_ & 0xff00) * (Class397_Sub1.anInt5749)))
															& 0xff0000)) >> 8;
										} else {
											int i_313_ = (((i_305_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_314_ = (((i_305_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_315_ = (((i_305_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											is_289_[i_288_] = (i_313_ | i_314_ | i_315_) >>> 8;
										}
									}
								} else if (i_290_ == 3) {
									int i_316_ = anIntArray6693[i_287_];
									int i_317_ = Class397_Sub1.anInt5767;
									int i_318_ = i_316_ + i_317_;
									int i_319_ = ((i_316_ & 0xff00ff) + (i_317_ & 0xff00ff));
									int i_320_ = ((i_319_ & 0x1000100) + (i_318_ - i_319_ & 0x10000));
									i_320_ = i_318_ - i_320_ | i_320_ - (i_320_ >>> 8);
									if (i_316_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_316_ = i_320_;
										i_320_ = is_289_[i_288_];
										i_320_ = (((((i_316_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_320_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_316_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_320_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_289_[i_288_] = i_320_;
								} else if (i_290_ == 2) {
									int i_321_ = anIntArray6693[i_287_];
									if (i_321_ != 0) {
										int i_322_ = (((i_321_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_323_ = (((i_321_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										is_289_[i_288_++] = (((i_322_ | i_323_) >>> 8) + Class397_Sub1.anInt5773);
									}
								} else
									throw new IllegalArgumentException();
							} else if (i_291_ == 2) {
								if (i_290_ == 1) {
									int i_324_ = anIntArray6693[i_287_];
									if (i_324_ != 0) {
										int i_325_ = is_289_[i_288_];
										int i_326_ = i_324_ + i_325_;
										int i_327_ = ((i_324_ & 0xff00ff) + (i_325_ & 0xff00ff));
										i_325_ = ((i_327_ & 0x1000100) + (i_326_ - i_327_ & 0x10000));
										is_289_[i_288_] = (i_326_ - i_325_ | i_325_ - (i_325_ >>> 8));
									}
								} else if (i_290_ == 0) {
									int i_328_ = anIntArray6693[i_287_];
									if (i_328_ != 0) {
										int i_329_ = (((i_328_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_330_ = (((i_328_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_331_ = (((i_328_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_328_ = (i_329_ | i_330_ | i_331_) >>> 8;
										int i_332_ = is_289_[i_288_];
										int i_333_ = i_328_ + i_332_;
										int i_334_ = ((i_328_ & 0xff00ff) + (i_332_ & 0xff00ff));
										i_332_ = ((i_334_ & 0x1000100) + (i_333_ - i_334_ & 0x10000));
										is_289_[i_288_] = (i_333_ - i_332_ | i_332_ - (i_332_ >>> 8));
									}
								} else if (i_290_ == 3) {
									int i_335_ = anIntArray6693[i_287_];
									int i_336_ = Class397_Sub1.anInt5767;
									int i_337_ = i_335_ + i_336_;
									int i_338_ = ((i_335_ & 0xff00ff) + (i_336_ & 0xff00ff));
									int i_339_ = ((i_338_ & 0x1000100) + (i_337_ - i_338_ & 0x10000));
									i_335_ = i_337_ - i_339_ | i_339_ - (i_339_ >>> 8);
									i_339_ = is_289_[i_288_];
									i_337_ = i_335_ + i_339_;
									i_338_ = (i_335_ & 0xff00ff) + (i_339_ & 0xff00ff);
									i_339_ = ((i_338_ & 0x1000100) + (i_337_ - i_338_ & 0x10000));
									is_289_[i_288_] = i_337_ - i_339_ | i_339_ - (i_339_ >>> 8);
								} else if (i_290_ == 2) {
									int i_340_ = anIntArray6693[i_287_];
									if (i_340_ != 0) {
										int i_341_ = (((i_340_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_342_ = (((i_340_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										i_340_ = (((i_341_ | i_342_) >>> 8) + Class397_Sub1.anInt5773);
										int i_343_ = is_289_[i_288_];
										int i_344_ = i_340_ + i_343_;
										int i_345_ = ((i_340_ & 0xff00ff) + (i_343_ & 0xff00ff));
										i_343_ = ((i_345_ & 0x1000100) + (i_344_ - i_345_ & 0x10000));
										is_289_[i_288_] = (i_344_ - i_343_ | i_343_ - (i_343_ >>> 8));
									}
								}
							} else
								throw new IllegalArgumentException();
							i_283_ += Class397_Sub1.anInt5777;
						}
					}
					i_280_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5766 < 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_346_ = Class397_Sub1.anInt5771;
				while (i_346_ < 0) {
					int i_347_ = Class397_Sub1.anInt5759;
					int i_348_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_349_ = Class397_Sub1.anInt5775;
					int i_350_ = Class397_Sub1.anInt5770;
					if (i_349_ >= 0 && i_349_ - (anInt5756 << 12) < 0) {
						int i_351_;
						if ((i_351_ = i_348_ - (anInt5758 << 12)) >= 0) {
							i_351_ = ((Class397_Sub1.anInt5766 - i_351_) / Class397_Sub1.anInt5766);
							i_350_ += i_351_;
							i_348_ += Class397_Sub1.anInt5766 * i_351_;
							i_347_ += i_351_;
						}
						if ((i_351_ = ((i_348_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_350_)
							i_350_ = i_351_;
						for (/**/; i_350_ < 0; i_350_++) {
							int i_352_ = (i_349_ >> 12) * anInt5758 + (i_348_ >> 12);
							int i_353_ = i_347_++;
							int[] is_354_ = is;
							int i_355_ = i;
							int i_356_ = i_150_;
							if (i_356_ == 0) {
								if (i_355_ == 1)
									is_354_[i_353_] = anIntArray6693[i_352_];
								else if (i_355_ == 0) {
									int i_357_ = anIntArray6693[i_352_++];
									int i_358_ = (((i_357_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_359_ = (((i_357_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_360_ = (((i_357_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_354_[i_353_] = (i_358_ | i_359_ | i_360_) >>> 8;
								} else if (i_355_ == 3) {
									int i_361_ = anIntArray6693[i_352_++];
									int i_362_ = Class397_Sub1.anInt5767;
									int i_363_ = i_361_ + i_362_;
									int i_364_ = ((i_361_ & 0xff00ff) + (i_362_ & 0xff00ff));
									int i_365_ = ((i_364_ & 0x1000100) + (i_363_ - i_364_ & 0x10000));
									is_354_[i_353_] = i_363_ - i_365_ | i_365_ - (i_365_ >>> 8);
								} else if (i_355_ == 2) {
									int i_366_ = anIntArray6693[i_352_];
									int i_367_ = (((i_366_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_368_ = (((i_366_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_354_[i_353_] = (((i_367_ | i_368_) >>> 8) + Class397_Sub1.anInt5773);
								} else
									throw new IllegalArgumentException();
							} else if (i_356_ == 1) {
								if (i_355_ == 1) {
									int i_369_ = anIntArray6693[i_352_];
									if (i_369_ != 0)
										is_354_[i_353_] = i_369_;
								} else if (i_355_ == 0) {
									int i_370_ = anIntArray6693[i_352_];
									if (i_370_ != 0) {
										if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
											int i_371_ = (Class397_Sub1.anInt5767 >>> 24);
											int i_372_ = 256 - i_371_;
											int i_373_ = is_354_[i_353_];
											is_354_[i_353_] = (((((i_370_ & 0xff00ff) * i_371_)
													+ ((i_373_ & 0xff00ff) * i_372_)) & ~0xff00ff)
													+ ((((i_370_ & 0xff00) * i_371_) + ((i_373_ & 0xff00) * i_372_))
															& 0xff0000)) >> 8;
										} else if (Class397_Sub1.anInt5755 != 255) {
											int i_374_ = (((i_370_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_375_ = (((i_370_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_376_ = (((i_370_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											i_370_ = (i_374_ | i_375_ | i_376_) >>> 8;
											int i_377_ = is_354_[i_353_];
											is_354_[i_353_] = (((((i_370_ & 0xff00ff) * (Class397_Sub1.anInt5755))
													+ ((i_377_ & 0xff00ff) * (Class397_Sub1.anInt5749))) & ~0xff00ff)
													+ ((((i_370_ & 0xff00) * (Class397_Sub1.anInt5755))
															+ ((i_377_ & 0xff00) * (Class397_Sub1.anInt5749)))
															& 0xff0000)) >> 8;
										} else {
											int i_378_ = (((i_370_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
											int i_379_ = (((i_370_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
											int i_380_ = (((i_370_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
											is_354_[i_353_] = (i_378_ | i_379_ | i_380_) >>> 8;
										}
									}
								} else if (i_355_ == 3) {
									int i_381_ = anIntArray6693[i_352_];
									int i_382_ = Class397_Sub1.anInt5767;
									int i_383_ = i_381_ + i_382_;
									int i_384_ = ((i_381_ & 0xff00ff) + (i_382_ & 0xff00ff));
									int i_385_ = ((i_384_ & 0x1000100) + (i_383_ - i_384_ & 0x10000));
									i_385_ = i_383_ - i_385_ | i_385_ - (i_385_ >>> 8);
									if (i_381_ == 0 && Class397_Sub1.anInt5755 != 255) {
										i_381_ = i_385_;
										i_385_ = is_354_[i_353_];
										i_385_ = (((((i_381_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_385_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_381_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_385_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									}
									is_354_[i_353_] = i_385_;
								} else if (i_355_ == 2) {
									int i_386_ = anIntArray6693[i_352_];
									if (i_386_ != 0) {
										int i_387_ = (((i_386_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_388_ = (((i_386_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										is_354_[i_353_++] = (((i_387_ | i_388_) >>> 8) + Class397_Sub1.anInt5773);
									}
								} else
									throw new IllegalArgumentException();
							} else if (i_356_ == 2) {
								if (i_355_ == 1) {
									int i_389_ = anIntArray6693[i_352_];
									if (i_389_ != 0) {
										int i_390_ = is_354_[i_353_];
										int i_391_ = i_389_ + i_390_;
										int i_392_ = ((i_389_ & 0xff00ff) + (i_390_ & 0xff00ff));
										i_390_ = ((i_392_ & 0x1000100) + (i_391_ - i_392_ & 0x10000));
										is_354_[i_353_] = (i_391_ - i_390_ | i_390_ - (i_390_ >>> 8));
									}
								} else if (i_355_ == 0) {
									int i_393_ = anIntArray6693[i_352_];
									if (i_393_ != 0) {
										int i_394_ = (((i_393_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_395_ = (((i_393_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_396_ = (((i_393_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_393_ = (i_394_ | i_395_ | i_396_) >>> 8;
										int i_397_ = is_354_[i_353_];
										int i_398_ = i_393_ + i_397_;
										int i_399_ = ((i_393_ & 0xff00ff) + (i_397_ & 0xff00ff));
										i_397_ = ((i_399_ & 0x1000100) + (i_398_ - i_399_ & 0x10000));
										is_354_[i_353_] = (i_398_ - i_397_ | i_397_ - (i_397_ >>> 8));
									}
								} else if (i_355_ == 3) {
									int i_400_ = anIntArray6693[i_352_];
									int i_401_ = Class397_Sub1.anInt5767;
									int i_402_ = i_400_ + i_401_;
									int i_403_ = ((i_400_ & 0xff00ff) + (i_401_ & 0xff00ff));
									int i_404_ = ((i_403_ & 0x1000100) + (i_402_ - i_403_ & 0x10000));
									i_400_ = i_402_ - i_404_ | i_404_ - (i_404_ >>> 8);
									i_404_ = is_354_[i_353_];
									i_402_ = i_400_ + i_404_;
									i_403_ = (i_400_ & 0xff00ff) + (i_404_ & 0xff00ff);
									i_404_ = ((i_403_ & 0x1000100) + (i_402_ - i_403_ & 0x10000));
									is_354_[i_353_] = i_402_ - i_404_ | i_404_ - (i_404_ >>> 8);
								} else if (i_355_ == 2) {
									int i_405_ = anIntArray6693[i_352_];
									if (i_405_ != 0) {
										int i_406_ = (((i_405_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
										int i_407_ = (((i_405_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
										i_405_ = (((i_406_ | i_407_) >>> 8) + Class397_Sub1.anInt5773);
										int i_408_ = is_354_[i_353_];
										int i_409_ = i_405_ + i_408_;
										int i_410_ = ((i_405_ & 0xff00ff) + (i_408_ & 0xff00ff));
										i_408_ = ((i_410_ & 0x1000100) + (i_409_ - i_410_ & 0x10000));
										is_354_[i_353_] = (i_409_ - i_408_ | i_408_ - (i_408_ >>> 8));
									}
								}
							} else
								throw new IllegalArgumentException();
							i_348_ += Class397_Sub1.anInt5766;
						}
					}
					i_346_++;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_411_ = Class397_Sub1.anInt5771;
				while (i_411_ < 0) {
					int i_412_ = Class397_Sub1.anInt5759;
					int i_413_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_414_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_415_ = Class397_Sub1.anInt5770;
					int i_416_;
					if ((i_416_ = i_413_ - (anInt5758 << 12)) >= 0) {
						i_416_ = ((Class397_Sub1.anInt5766 - i_416_) / Class397_Sub1.anInt5766);
						i_415_ += i_416_;
						i_413_ += Class397_Sub1.anInt5766 * i_416_;
						i_414_ += Class397_Sub1.anInt5777 * i_416_;
						i_412_ += i_416_;
					}
					if ((i_416_ = ((i_413_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_415_)
						i_415_ = i_416_;
					if ((i_416_ = i_414_ - (anInt5756 << 12)) >= 0) {
						i_416_ = ((Class397_Sub1.anInt5777 - i_416_) / Class397_Sub1.anInt5777);
						i_415_ += i_416_;
						i_413_ += Class397_Sub1.anInt5766 * i_416_;
						i_414_ += Class397_Sub1.anInt5777 * i_416_;
						i_412_ += i_416_;
					}
					if ((i_416_ = ((i_414_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_415_)
						i_415_ = i_416_;
					for (/**/; i_415_ < 0; i_415_++) {
						int i_417_ = (i_414_ >> 12) * anInt5758 + (i_413_ >> 12);
						int i_418_ = i_412_++;
						int[] is_419_ = is;
						int i_420_ = i;
						int i_421_ = i_150_;
						if (i_421_ == 0) {
							if (i_420_ == 1)
								is_419_[i_418_] = anIntArray6693[i_417_];
							else if (i_420_ == 0) {
								int i_422_ = anIntArray6693[i_417_++];
								int i_423_ = (((i_422_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_424_ = (((i_422_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_425_ = (((i_422_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_419_[i_418_] = (i_423_ | i_424_ | i_425_) >>> 8;
							} else if (i_420_ == 3) {
								int i_426_ = anIntArray6693[i_417_++];
								int i_427_ = Class397_Sub1.anInt5767;
								int i_428_ = i_426_ + i_427_;
								int i_429_ = ((i_426_ & 0xff00ff) + (i_427_ & 0xff00ff));
								int i_430_ = ((i_429_ & 0x1000100) + (i_428_ - i_429_ & 0x10000));
								is_419_[i_418_] = i_428_ - i_430_ | i_430_ - (i_430_ >>> 8);
							} else if (i_420_ == 2) {
								int i_431_ = anIntArray6693[i_417_];
								int i_432_ = (((i_431_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_433_ = (((i_431_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_419_[i_418_] = (((i_432_ | i_433_) >>> 8) + Class397_Sub1.anInt5773);
							} else
								throw new IllegalArgumentException();
						} else if (i_421_ == 1) {
							if (i_420_ == 1) {
								int i_434_ = anIntArray6693[i_417_];
								if (i_434_ != 0)
									is_419_[i_418_] = i_434_;
							} else if (i_420_ == 0) {
								int i_435_ = anIntArray6693[i_417_];
								if (i_435_ != 0) {
									if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
										int i_436_ = Class397_Sub1.anInt5767 >>> 24;
										int i_437_ = 256 - i_436_;
										int i_438_ = is_419_[i_418_];
										is_419_[i_418_] = ((((i_435_ & 0xff00ff) * i_436_
												+ ((i_438_ & 0xff00ff) * i_437_)) & ~0xff00ff)
												+ (((i_435_ & 0xff00) * i_436_ + ((i_438_ & 0xff00) * i_437_))
														& 0xff0000)) >> 8;
									} else if (Class397_Sub1.anInt5755 != 255) {
										int i_439_ = (((i_435_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_440_ = (((i_435_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_441_ = (((i_435_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_435_ = (i_439_ | i_440_ | i_441_) >>> 8;
										int i_442_ = is_419_[i_418_];
										is_419_[i_418_] = (((((i_435_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_442_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_435_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_442_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									} else {
										int i_443_ = (((i_435_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_444_ = (((i_435_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_445_ = (((i_435_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										is_419_[i_418_] = (i_443_ | i_444_ | i_445_) >>> 8;
									}
								}
							} else if (i_420_ == 3) {
								int i_446_ = anIntArray6693[i_417_];
								int i_447_ = Class397_Sub1.anInt5767;
								int i_448_ = i_446_ + i_447_;
								int i_449_ = ((i_446_ & 0xff00ff) + (i_447_ & 0xff00ff));
								int i_450_ = ((i_449_ & 0x1000100) + (i_448_ - i_449_ & 0x10000));
								i_450_ = i_448_ - i_450_ | i_450_ - (i_450_ >>> 8);
								if (i_446_ == 0 && Class397_Sub1.anInt5755 != 255) {
									i_446_ = i_450_;
									i_450_ = is_419_[i_418_];
									i_450_ = (((((i_446_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_450_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_446_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_450_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								}
								is_419_[i_418_] = i_450_;
							} else if (i_420_ == 2) {
								int i_451_ = anIntArray6693[i_417_];
								if (i_451_ != 0) {
									int i_452_ = (((i_451_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_453_ = (((i_451_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_419_[i_418_++] = (((i_452_ | i_453_) >>> 8) + Class397_Sub1.anInt5773);
								}
							} else
								throw new IllegalArgumentException();
						} else if (i_421_ == 2) {
							if (i_420_ == 1) {
								int i_454_ = anIntArray6693[i_417_];
								if (i_454_ != 0) {
									int i_455_ = is_419_[i_418_];
									int i_456_ = i_454_ + i_455_;
									int i_457_ = ((i_454_ & 0xff00ff) + (i_455_ & 0xff00ff));
									i_455_ = ((i_457_ & 0x1000100) + (i_456_ - i_457_ & 0x10000));
									is_419_[i_418_] = i_456_ - i_455_ | i_455_ - (i_455_ >>> 8);
								}
							} else if (i_420_ == 0) {
								int i_458_ = anIntArray6693[i_417_];
								if (i_458_ != 0) {
									int i_459_ = (((i_458_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_460_ = (((i_458_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_461_ = (((i_458_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_458_ = (i_459_ | i_460_ | i_461_) >>> 8;
									int i_462_ = is_419_[i_418_];
									int i_463_ = i_458_ + i_462_;
									int i_464_ = ((i_458_ & 0xff00ff) + (i_462_ & 0xff00ff));
									i_462_ = ((i_464_ & 0x1000100) + (i_463_ - i_464_ & 0x10000));
									is_419_[i_418_] = i_463_ - i_462_ | i_462_ - (i_462_ >>> 8);
								}
							} else if (i_420_ == 3) {
								int i_465_ = anIntArray6693[i_417_];
								int i_466_ = Class397_Sub1.anInt5767;
								int i_467_ = i_465_ + i_466_;
								int i_468_ = ((i_465_ & 0xff00ff) + (i_466_ & 0xff00ff));
								int i_469_ = ((i_468_ & 0x1000100) + (i_467_ - i_468_ & 0x10000));
								i_465_ = i_467_ - i_469_ | i_469_ - (i_469_ >>> 8);
								i_469_ = is_419_[i_418_];
								i_467_ = i_465_ + i_469_;
								i_468_ = (i_465_ & 0xff00ff) + (i_469_ & 0xff00ff);
								i_469_ = (i_468_ & 0x1000100) + (i_467_ - i_468_ & 0x10000);
								is_419_[i_418_] = i_467_ - i_469_ | i_469_ - (i_469_ >>> 8);
							} else if (i_420_ == 2) {
								int i_470_ = anIntArray6693[i_417_];
								if (i_470_ != 0) {
									int i_471_ = (((i_470_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_472_ = (((i_470_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_470_ = (((i_471_ | i_472_) >>> 8) + Class397_Sub1.anInt5773);
									int i_473_ = is_419_[i_418_];
									int i_474_ = i_470_ + i_473_;
									int i_475_ = ((i_470_ & 0xff00ff) + (i_473_ & 0xff00ff));
									i_473_ = ((i_475_ & 0x1000100) + (i_474_ - i_475_ & 0x10000));
									is_419_[i_418_] = i_474_ - i_473_ | i_473_ - (i_473_ >>> 8);
								}
							}
						} else
							throw new IllegalArgumentException();
						i_413_ += Class397_Sub1.anInt5766;
						i_414_ += Class397_Sub1.anInt5777;
					}
					i_411_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_476_ = Class397_Sub1.anInt5771;
				while (i_476_ < 0) {
					int i_477_ = Class397_Sub1.anInt5759;
					int i_478_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_479_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_480_ = Class397_Sub1.anInt5770;
					int i_481_;
					if ((i_481_ = i_478_ - (anInt5758 << 12)) >= 0) {
						i_481_ = ((Class397_Sub1.anInt5766 - i_481_) / Class397_Sub1.anInt5766);
						i_480_ += i_481_;
						i_478_ += Class397_Sub1.anInt5766 * i_481_;
						i_479_ += Class397_Sub1.anInt5777 * i_481_;
						i_477_ += i_481_;
					}
					if ((i_481_ = ((i_478_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_480_)
						i_480_ = i_481_;
					if (i_479_ < 0) {
						i_481_ = ((Class397_Sub1.anInt5777 - 1 - i_479_) / Class397_Sub1.anInt5777);
						i_480_ += i_481_;
						i_478_ += Class397_Sub1.anInt5766 * i_481_;
						i_479_ += Class397_Sub1.anInt5777 * i_481_;
						i_477_ += i_481_;
					}
					if ((i_481_ = ((i_479_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
							/ Class397_Sub1.anInt5777)) > i_480_)
						i_480_ = i_481_;
					for (/**/; i_480_ < 0; i_480_++) {
						int i_482_ = (i_479_ >> 12) * anInt5758 + (i_478_ >> 12);
						int i_483_ = i_477_++;
						int[] is_484_ = is;
						int i_485_ = i;
						int i_486_ = i_150_;
						if (i_486_ == 0) {
							if (i_485_ == 1)
								is_484_[i_483_] = anIntArray6693[i_482_];
							else if (i_485_ == 0) {
								int i_487_ = anIntArray6693[i_482_++];
								int i_488_ = (((i_487_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_489_ = (((i_487_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_490_ = (((i_487_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_484_[i_483_] = (i_488_ | i_489_ | i_490_) >>> 8;
							} else if (i_485_ == 3) {
								int i_491_ = anIntArray6693[i_482_++];
								int i_492_ = Class397_Sub1.anInt5767;
								int i_493_ = i_491_ + i_492_;
								int i_494_ = ((i_491_ & 0xff00ff) + (i_492_ & 0xff00ff));
								int i_495_ = ((i_494_ & 0x1000100) + (i_493_ - i_494_ & 0x10000));
								is_484_[i_483_] = i_493_ - i_495_ | i_495_ - (i_495_ >>> 8);
							} else if (i_485_ == 2) {
								int i_496_ = anIntArray6693[i_482_];
								int i_497_ = (((i_496_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_498_ = (((i_496_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_484_[i_483_] = (((i_497_ | i_498_) >>> 8) + Class397_Sub1.anInt5773);
							} else
								throw new IllegalArgumentException();
						} else if (i_486_ == 1) {
							if (i_485_ == 1) {
								int i_499_ = anIntArray6693[i_482_];
								if (i_499_ != 0)
									is_484_[i_483_] = i_499_;
							} else if (i_485_ == 0) {
								int i_500_ = anIntArray6693[i_482_];
								if (i_500_ != 0) {
									if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
										int i_501_ = Class397_Sub1.anInt5767 >>> 24;
										int i_502_ = 256 - i_501_;
										int i_503_ = is_484_[i_483_];
										is_484_[i_483_] = ((((i_500_ & 0xff00ff) * i_501_
												+ ((i_503_ & 0xff00ff) * i_502_)) & ~0xff00ff)
												+ (((i_500_ & 0xff00) * i_501_ + ((i_503_ & 0xff00) * i_502_))
														& 0xff0000)) >> 8;
									} else if (Class397_Sub1.anInt5755 != 255) {
										int i_504_ = (((i_500_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_505_ = (((i_500_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_506_ = (((i_500_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_500_ = (i_504_ | i_505_ | i_506_) >>> 8;
										int i_507_ = is_484_[i_483_];
										is_484_[i_483_] = (((((i_500_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_507_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_500_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_507_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									} else {
										int i_508_ = (((i_500_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_509_ = (((i_500_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_510_ = (((i_500_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										is_484_[i_483_] = (i_508_ | i_509_ | i_510_) >>> 8;
									}
								}
							} else if (i_485_ == 3) {
								int i_511_ = anIntArray6693[i_482_];
								int i_512_ = Class397_Sub1.anInt5767;
								int i_513_ = i_511_ + i_512_;
								int i_514_ = ((i_511_ & 0xff00ff) + (i_512_ & 0xff00ff));
								int i_515_ = ((i_514_ & 0x1000100) + (i_513_ - i_514_ & 0x10000));
								i_515_ = i_513_ - i_515_ | i_515_ - (i_515_ >>> 8);
								if (i_511_ == 0 && Class397_Sub1.anInt5755 != 255) {
									i_511_ = i_515_;
									i_515_ = is_484_[i_483_];
									i_515_ = (((((i_511_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_515_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_511_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_515_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								}
								is_484_[i_483_] = i_515_;
							} else if (i_485_ == 2) {
								int i_516_ = anIntArray6693[i_482_];
								if (i_516_ != 0) {
									int i_517_ = (((i_516_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_518_ = (((i_516_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_484_[i_483_++] = (((i_517_ | i_518_) >>> 8) + Class397_Sub1.anInt5773);
								}
							} else
								throw new IllegalArgumentException();
						} else if (i_486_ == 2) {
							if (i_485_ == 1) {
								int i_519_ = anIntArray6693[i_482_];
								if (i_519_ != 0) {
									int i_520_ = is_484_[i_483_];
									int i_521_ = i_519_ + i_520_;
									int i_522_ = ((i_519_ & 0xff00ff) + (i_520_ & 0xff00ff));
									i_520_ = ((i_522_ & 0x1000100) + (i_521_ - i_522_ & 0x10000));
									is_484_[i_483_] = i_521_ - i_520_ | i_520_ - (i_520_ >>> 8);
								}
							} else if (i_485_ == 0) {
								int i_523_ = anIntArray6693[i_482_];
								if (i_523_ != 0) {
									int i_524_ = (((i_523_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_525_ = (((i_523_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_526_ = (((i_523_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_523_ = (i_524_ | i_525_ | i_526_) >>> 8;
									int i_527_ = is_484_[i_483_];
									int i_528_ = i_523_ + i_527_;
									int i_529_ = ((i_523_ & 0xff00ff) + (i_527_ & 0xff00ff));
									i_527_ = ((i_529_ & 0x1000100) + (i_528_ - i_529_ & 0x10000));
									is_484_[i_483_] = i_528_ - i_527_ | i_527_ - (i_527_ >>> 8);
								}
							} else if (i_485_ == 3) {
								int i_530_ = anIntArray6693[i_482_];
								int i_531_ = Class397_Sub1.anInt5767;
								int i_532_ = i_530_ + i_531_;
								int i_533_ = ((i_530_ & 0xff00ff) + (i_531_ & 0xff00ff));
								int i_534_ = ((i_533_ & 0x1000100) + (i_532_ - i_533_ & 0x10000));
								i_530_ = i_532_ - i_534_ | i_534_ - (i_534_ >>> 8);
								i_534_ = is_484_[i_483_];
								i_532_ = i_530_ + i_534_;
								i_533_ = (i_530_ & 0xff00ff) + (i_534_ & 0xff00ff);
								i_534_ = (i_533_ & 0x1000100) + (i_532_ - i_533_ & 0x10000);
								is_484_[i_483_] = i_532_ - i_534_ | i_534_ - (i_534_ >>> 8);
							} else if (i_485_ == 2) {
								int i_535_ = anIntArray6693[i_482_];
								if (i_535_ != 0) {
									int i_536_ = (((i_535_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_537_ = (((i_535_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_535_ = (((i_536_ | i_537_) >>> 8) + Class397_Sub1.anInt5773);
									int i_538_ = is_484_[i_483_];
									int i_539_ = i_535_ + i_538_;
									int i_540_ = ((i_535_ & 0xff00ff) + (i_538_ & 0xff00ff));
									i_538_ = ((i_540_ & 0x1000100) + (i_539_ - i_540_ & 0x10000));
									is_484_[i_483_] = i_539_ - i_538_ | i_538_ - (i_538_ >>> 8);
								}
							}
						} else
							throw new IllegalArgumentException();
						i_478_ += Class397_Sub1.anInt5766;
						i_479_ += Class397_Sub1.anInt5777;
					}
					i_476_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5777 == 0) {
			int i_541_ = Class397_Sub1.anInt5771;
			while (i_541_ < 0) {
				int i_542_ = Class397_Sub1.anInt5759;
				int i_543_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
				int i_544_ = Class397_Sub1.anInt5775;
				int i_545_ = Class397_Sub1.anInt5770;
				if (i_544_ >= 0 && i_544_ - (anInt5756 << 12) < 0) {
					if (i_543_ < 0) {
						int i_546_ = ((Class397_Sub1.anInt5766 - 1 - i_543_) / Class397_Sub1.anInt5766);
						i_545_ += i_546_;
						i_543_ += Class397_Sub1.anInt5766 * i_546_;
						i_542_ += i_546_;
					}
					int i_547_;
					if ((i_547_ = ((i_543_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
							/ Class397_Sub1.anInt5766)) > i_545_)
						i_545_ = i_547_;
					for (/**/; i_545_ < 0; i_545_++) {
						int i_548_ = (i_544_ >> 12) * anInt5758 + (i_543_ >> 12);
						int i_549_ = i_542_++;
						int[] is_550_ = is;
						int i_551_ = i;
						int i_552_ = i_150_;
						if (i_552_ == 0) {
							if (i_551_ == 1)
								is_550_[i_549_] = anIntArray6693[i_548_];
							else if (i_551_ == 0) {
								int i_553_ = anIntArray6693[i_548_++];
								int i_554_ = (((i_553_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_555_ = (((i_553_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_556_ = (((i_553_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								is_550_[i_549_] = (i_554_ | i_555_ | i_556_) >>> 8;
							} else if (i_551_ == 3) {
								int i_557_ = anIntArray6693[i_548_++];
								int i_558_ = Class397_Sub1.anInt5767;
								int i_559_ = i_557_ + i_558_;
								int i_560_ = ((i_557_ & 0xff00ff) + (i_558_ & 0xff00ff));
								int i_561_ = ((i_560_ & 0x1000100) + (i_559_ - i_560_ & 0x10000));
								is_550_[i_549_] = i_559_ - i_561_ | i_561_ - (i_561_ >>> 8);
							} else if (i_551_ == 2) {
								int i_562_ = anIntArray6693[i_548_];
								int i_563_ = (((i_562_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_564_ = (((i_562_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_550_[i_549_] = (((i_563_ | i_564_) >>> 8) + Class397_Sub1.anInt5773);
							} else
								throw new IllegalArgumentException();
						} else if (i_552_ == 1) {
							if (i_551_ == 1) {
								int i_565_ = anIntArray6693[i_548_];
								if (i_565_ != 0)
									is_550_[i_549_] = i_565_;
							} else if (i_551_ == 0) {
								int i_566_ = anIntArray6693[i_548_];
								if (i_566_ != 0) {
									if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
										int i_567_ = Class397_Sub1.anInt5767 >>> 24;
										int i_568_ = 256 - i_567_;
										int i_569_ = is_550_[i_549_];
										is_550_[i_549_] = ((((i_566_ & 0xff00ff) * i_567_
												+ ((i_569_ & 0xff00ff) * i_568_)) & ~0xff00ff)
												+ (((i_566_ & 0xff00) * i_567_ + ((i_569_ & 0xff00) * i_568_))
														& 0xff0000)) >> 8;
									} else if (Class397_Sub1.anInt5755 != 255) {
										int i_570_ = (((i_566_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_571_ = (((i_566_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_572_ = (((i_566_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										i_566_ = (i_570_ | i_571_ | i_572_) >>> 8;
										int i_573_ = is_550_[i_549_];
										is_550_[i_549_] = (((((i_566_ & 0xff00ff) * Class397_Sub1.anInt5755)
												+ ((i_573_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
												+ ((((i_566_ & 0xff00) * Class397_Sub1.anInt5755)
														+ ((i_573_ & 0xff00) * (Class397_Sub1.anInt5749)))
														& 0xff0000)) >> 8;
									} else {
										int i_574_ = (((i_566_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
										int i_575_ = (((i_566_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
										int i_576_ = (((i_566_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
										is_550_[i_549_] = (i_574_ | i_575_ | i_576_) >>> 8;
									}
								}
							} else if (i_551_ == 3) {
								int i_577_ = anIntArray6693[i_548_];
								int i_578_ = Class397_Sub1.anInt5767;
								int i_579_ = i_577_ + i_578_;
								int i_580_ = ((i_577_ & 0xff00ff) + (i_578_ & 0xff00ff));
								int i_581_ = ((i_580_ & 0x1000100) + (i_579_ - i_580_ & 0x10000));
								i_581_ = i_579_ - i_581_ | i_581_ - (i_581_ >>> 8);
								if (i_577_ == 0 && Class397_Sub1.anInt5755 != 255) {
									i_577_ = i_581_;
									i_581_ = is_550_[i_549_];
									i_581_ = (((((i_577_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_581_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_577_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_581_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								}
								is_550_[i_549_] = i_581_;
							} else if (i_551_ == 2) {
								int i_582_ = anIntArray6693[i_548_];
								if (i_582_ != 0) {
									int i_583_ = (((i_582_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_584_ = (((i_582_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									is_550_[i_549_++] = (((i_583_ | i_584_) >>> 8) + Class397_Sub1.anInt5773);
								}
							} else
								throw new IllegalArgumentException();
						} else if (i_552_ == 2) {
							if (i_551_ == 1) {
								int i_585_ = anIntArray6693[i_548_];
								if (i_585_ != 0) {
									int i_586_ = is_550_[i_549_];
									int i_587_ = i_585_ + i_586_;
									int i_588_ = ((i_585_ & 0xff00ff) + (i_586_ & 0xff00ff));
									i_586_ = ((i_588_ & 0x1000100) + (i_587_ - i_588_ & 0x10000));
									is_550_[i_549_] = i_587_ - i_586_ | i_586_ - (i_586_ >>> 8);
								}
							} else if (i_551_ == 0) {
								int i_589_ = anIntArray6693[i_548_];
								if (i_589_ != 0) {
									int i_590_ = (((i_589_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_591_ = (((i_589_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_592_ = (((i_589_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_589_ = (i_590_ | i_591_ | i_592_) >>> 8;
									int i_593_ = is_550_[i_549_];
									int i_594_ = i_589_ + i_593_;
									int i_595_ = ((i_589_ & 0xff00ff) + (i_593_ & 0xff00ff));
									i_593_ = ((i_595_ & 0x1000100) + (i_594_ - i_595_ & 0x10000));
									is_550_[i_549_] = i_594_ - i_593_ | i_593_ - (i_593_ >>> 8);
								}
							} else if (i_551_ == 3) {
								int i_596_ = anIntArray6693[i_548_];
								int i_597_ = Class397_Sub1.anInt5767;
								int i_598_ = i_596_ + i_597_;
								int i_599_ = ((i_596_ & 0xff00ff) + (i_597_ & 0xff00ff));
								int i_600_ = ((i_599_ & 0x1000100) + (i_598_ - i_599_ & 0x10000));
								i_596_ = i_598_ - i_600_ | i_600_ - (i_600_ >>> 8);
								i_600_ = is_550_[i_549_];
								i_598_ = i_596_ + i_600_;
								i_599_ = (i_596_ & 0xff00ff) + (i_600_ & 0xff00ff);
								i_600_ = (i_599_ & 0x1000100) + (i_598_ - i_599_ & 0x10000);
								is_550_[i_549_] = i_598_ - i_600_ | i_600_ - (i_600_ >>> 8);
							} else if (i_551_ == 2) {
								int i_601_ = anIntArray6693[i_548_];
								if (i_601_ != 0) {
									int i_602_ = (((i_601_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
									int i_603_ = (((i_601_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
									i_601_ = (((i_602_ | i_603_) >>> 8) + Class397_Sub1.anInt5773);
									int i_604_ = is_550_[i_549_];
									int i_605_ = i_601_ + i_604_;
									int i_606_ = ((i_601_ & 0xff00ff) + (i_604_ & 0xff00ff));
									i_604_ = ((i_606_ & 0x1000100) + (i_605_ - i_606_ & 0x10000));
									is_550_[i_549_] = i_605_ - i_604_ | i_604_ - (i_604_ >>> 8);
								}
							}
						} else
							throw new IllegalArgumentException();
						i_543_ += Class397_Sub1.anInt5766;
					}
				}
				i_541_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else if (Class397_Sub1.anInt5777 < 0) {
			for (int i_607_ = Class397_Sub1.anInt5771; i_607_ < 0; i_607_++) {
				int i_608_ = Class397_Sub1.anInt5759;
				int i_609_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
				int i_610_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
				int i_611_ = Class397_Sub1.anInt5770;
				if (i_609_ < 0) {
					int i_612_ = ((Class397_Sub1.anInt5766 - 1 - i_609_) / Class397_Sub1.anInt5766);
					i_611_ += i_612_;
					i_609_ += Class397_Sub1.anInt5766 * i_612_;
					i_610_ += Class397_Sub1.anInt5777 * i_612_;
					i_608_ += i_612_;
				}
				int i_613_;
				if ((i_613_ = (i_609_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
						/ Class397_Sub1.anInt5766) > i_611_)
					i_611_ = i_613_;
				if ((i_613_ = i_610_ - (anInt5756 << 12)) >= 0) {
					i_613_ = ((Class397_Sub1.anInt5777 - i_613_) / Class397_Sub1.anInt5777);
					i_611_ += i_613_;
					i_609_ += Class397_Sub1.anInt5766 * i_613_;
					i_610_ += Class397_Sub1.anInt5777 * i_613_;
					i_608_ += i_613_;
				}
				if ((i_613_ = ((i_610_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_611_)
					i_611_ = i_613_;
				for (/**/; i_611_ < 0; i_611_++) {
					int i_614_ = (i_610_ >> 12) * anInt5758 + (i_609_ >> 12);
					int i_615_ = i_608_++;
					int[] is_616_ = is;
					int i_617_ = i;
					int i_618_ = i_150_;
					if (i_618_ == 0) {
						if (i_617_ == 1)
							is_616_[i_615_] = anIntArray6693[i_614_];
						else if (i_617_ == 0) {
							int i_619_ = anIntArray6693[i_614_++];
							int i_620_ = (((i_619_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
							int i_621_ = ((i_619_ & 0xff00) * Class397_Sub1.anInt5753 & 0xff0000);
							int i_622_ = ((i_619_ & 0xff) * Class397_Sub1.anInt5752 & 0xff00);
							is_616_[i_615_] = (i_620_ | i_621_ | i_622_) >>> 8;
						} else if (i_617_ == 3) {
							int i_623_ = anIntArray6693[i_614_++];
							int i_624_ = Class397_Sub1.anInt5767;
							int i_625_ = i_623_ + i_624_;
							int i_626_ = (i_623_ & 0xff00ff) + (i_624_ & 0xff00ff);
							int i_627_ = ((i_626_ & 0x1000100) + (i_625_ - i_626_ & 0x10000));
							is_616_[i_615_] = i_625_ - i_627_ | i_627_ - (i_627_ >>> 8);
						} else if (i_617_ == 2) {
							int i_628_ = anIntArray6693[i_614_];
							int i_629_ = (((i_628_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
							int i_630_ = ((i_628_ & 0xff00) * Class397_Sub1.anInt5755 & 0xff0000);
							is_616_[i_615_] = (((i_629_ | i_630_) >>> 8) + Class397_Sub1.anInt5773);
						} else
							throw new IllegalArgumentException();
					} else if (i_618_ == 1) {
						if (i_617_ == 1) {
							int i_631_ = anIntArray6693[i_614_];
							if (i_631_ != 0)
								is_616_[i_615_] = i_631_;
						} else if (i_617_ == 0) {
							int i_632_ = anIntArray6693[i_614_];
							if (i_632_ != 0) {
								if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
									int i_633_ = Class397_Sub1.anInt5767 >>> 24;
									int i_634_ = 256 - i_633_;
									int i_635_ = is_616_[i_615_];
									is_616_[i_615_] = ((((i_632_ & 0xff00ff) * i_633_ + (i_635_ & 0xff00ff) * i_634_)
											& ~0xff00ff)
											+ (((i_632_ & 0xff00) * i_633_ + (i_635_ & 0xff00) * i_634_)
													& 0xff0000)) >> 8;
								} else if (Class397_Sub1.anInt5755 != 255) {
									int i_636_ = (((i_632_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_637_ = (((i_632_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_638_ = (((i_632_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_632_ = (i_636_ | i_637_ | i_638_) >>> 8;
									int i_639_ = is_616_[i_615_];
									is_616_[i_615_] = (((((i_632_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_639_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_632_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_639_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								} else {
									int i_640_ = (((i_632_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_641_ = (((i_632_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_642_ = (((i_632_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_616_[i_615_] = (i_640_ | i_641_ | i_642_) >>> 8;
								}
							}
						} else if (i_617_ == 3) {
							int i_643_ = anIntArray6693[i_614_];
							int i_644_ = Class397_Sub1.anInt5767;
							int i_645_ = i_643_ + i_644_;
							int i_646_ = (i_643_ & 0xff00ff) + (i_644_ & 0xff00ff);
							int i_647_ = ((i_646_ & 0x1000100) + (i_645_ - i_646_ & 0x10000));
							i_647_ = i_645_ - i_647_ | i_647_ - (i_647_ >>> 8);
							if (i_643_ == 0 && Class397_Sub1.anInt5755 != 255) {
								i_643_ = i_647_;
								i_647_ = is_616_[i_615_];
								i_647_ = (((((i_643_ & 0xff00ff) * Class397_Sub1.anInt5755)
										+ ((i_647_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
										+ ((((i_643_ & 0xff00) * Class397_Sub1.anInt5755)
												+ ((i_647_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
							}
							is_616_[i_615_] = i_647_;
						} else if (i_617_ == 2) {
							int i_648_ = anIntArray6693[i_614_];
							if (i_648_ != 0) {
								int i_649_ = (((i_648_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_650_ = (((i_648_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_616_[i_615_++] = (((i_649_ | i_650_) >>> 8) + Class397_Sub1.anInt5773);
							}
						} else
							throw new IllegalArgumentException();
					} else if (i_618_ == 2) {
						if (i_617_ == 1) {
							int i_651_ = anIntArray6693[i_614_];
							if (i_651_ != 0) {
								int i_652_ = is_616_[i_615_];
								int i_653_ = i_651_ + i_652_;
								int i_654_ = ((i_651_ & 0xff00ff) + (i_652_ & 0xff00ff));
								i_652_ = (i_654_ & 0x1000100) + (i_653_ - i_654_ & 0x10000);
								is_616_[i_615_] = i_653_ - i_652_ | i_652_ - (i_652_ >>> 8);
							}
						} else if (i_617_ == 0) {
							int i_655_ = anIntArray6693[i_614_];
							if (i_655_ != 0) {
								int i_656_ = (((i_655_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_657_ = (((i_655_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_658_ = (((i_655_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								i_655_ = (i_656_ | i_657_ | i_658_) >>> 8;
								int i_659_ = is_616_[i_615_];
								int i_660_ = i_655_ + i_659_;
								int i_661_ = ((i_655_ & 0xff00ff) + (i_659_ & 0xff00ff));
								i_659_ = (i_661_ & 0x1000100) + (i_660_ - i_661_ & 0x10000);
								is_616_[i_615_] = i_660_ - i_659_ | i_659_ - (i_659_ >>> 8);
							}
						} else if (i_617_ == 3) {
							int i_662_ = anIntArray6693[i_614_];
							int i_663_ = Class397_Sub1.anInt5767;
							int i_664_ = i_662_ + i_663_;
							int i_665_ = (i_662_ & 0xff00ff) + (i_663_ & 0xff00ff);
							int i_666_ = ((i_665_ & 0x1000100) + (i_664_ - i_665_ & 0x10000));
							i_662_ = i_664_ - i_666_ | i_666_ - (i_666_ >>> 8);
							i_666_ = is_616_[i_615_];
							i_664_ = i_662_ + i_666_;
							i_665_ = (i_662_ & 0xff00ff) + (i_666_ & 0xff00ff);
							i_666_ = (i_665_ & 0x1000100) + (i_664_ - i_665_ & 0x10000);
							is_616_[i_615_] = i_664_ - i_666_ | i_666_ - (i_666_ >>> 8);
						} else if (i_617_ == 2) {
							int i_667_ = anIntArray6693[i_614_];
							if (i_667_ != 0) {
								int i_668_ = (((i_667_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_669_ = (((i_667_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								i_667_ = (((i_668_ | i_669_) >>> 8) + Class397_Sub1.anInt5773);
								int i_670_ = is_616_[i_615_];
								int i_671_ = i_667_ + i_670_;
								int i_672_ = ((i_667_ & 0xff00ff) + (i_670_ & 0xff00ff));
								i_670_ = (i_672_ & 0x1000100) + (i_671_ - i_672_ & 0x10000);
								is_616_[i_615_] = i_671_ - i_670_ | i_670_ - (i_670_ >>> 8);
							}
						}
					} else
						throw new IllegalArgumentException();
					i_609_ += Class397_Sub1.anInt5766;
					i_610_ += Class397_Sub1.anInt5777;
				}
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else {
			for (int i_673_ = Class397_Sub1.anInt5771; i_673_ < 0; i_673_++) {
				int i_674_ = Class397_Sub1.anInt5759;
				int i_675_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
				int i_676_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
				int i_677_ = Class397_Sub1.anInt5770;
				if (i_675_ < 0) {
					int i_678_ = ((Class397_Sub1.anInt5766 - 1 - i_675_) / Class397_Sub1.anInt5766);
					i_677_ += i_678_;
					i_675_ += Class397_Sub1.anInt5766 * i_678_;
					i_676_ += Class397_Sub1.anInt5777 * i_678_;
					i_674_ += i_678_;
				}
				int i_679_;
				if ((i_679_ = (i_675_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
						/ Class397_Sub1.anInt5766) > i_677_)
					i_677_ = i_679_;
				if (i_676_ < 0) {
					i_679_ = ((Class397_Sub1.anInt5777 - 1 - i_676_) / Class397_Sub1.anInt5777);
					i_677_ += i_679_;
					i_675_ += Class397_Sub1.anInt5766 * i_679_;
					i_676_ += Class397_Sub1.anInt5777 * i_679_;
					i_674_ += i_679_;
				}
				if ((i_679_ = (i_676_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
						/ Class397_Sub1.anInt5777) > i_677_)
					i_677_ = i_679_;
				for (/**/; i_677_ < 0; i_677_++) {
					int i_680_ = (i_676_ >> 12) * anInt5758 + (i_675_ >> 12);
					int i_681_ = i_674_++;
					int[] is_682_ = is;
					int i_683_ = i;
					int i_684_ = i_150_;
					if (i_684_ == 0) {
						if (i_683_ == 1)
							is_682_[i_681_] = anIntArray6693[i_680_];
						else if (i_683_ == 0) {
							int i_685_ = anIntArray6693[i_680_++];
							int i_686_ = (((i_685_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
							int i_687_ = ((i_685_ & 0xff00) * Class397_Sub1.anInt5753 & 0xff0000);
							int i_688_ = ((i_685_ & 0xff) * Class397_Sub1.anInt5752 & 0xff00);
							is_682_[i_681_] = (i_686_ | i_687_ | i_688_) >>> 8;
						} else if (i_683_ == 3) {
							int i_689_ = anIntArray6693[i_680_++];
							int i_690_ = Class397_Sub1.anInt5767;
							int i_691_ = i_689_ + i_690_;
							int i_692_ = (i_689_ & 0xff00ff) + (i_690_ & 0xff00ff);
							int i_693_ = ((i_692_ & 0x1000100) + (i_691_ - i_692_ & 0x10000));
							is_682_[i_681_] = i_691_ - i_693_ | i_693_ - (i_693_ >>> 8);
						} else if (i_683_ == 2) {
							int i_694_ = anIntArray6693[i_680_];
							int i_695_ = (((i_694_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
							int i_696_ = ((i_694_ & 0xff00) * Class397_Sub1.anInt5755 & 0xff0000);
							is_682_[i_681_] = (((i_695_ | i_696_) >>> 8) + Class397_Sub1.anInt5773);
						} else
							throw new IllegalArgumentException();
					} else if (i_684_ == 1) {
						if (i_683_ == 1) {
							int i_697_ = anIntArray6693[i_680_];
							if (i_697_ != 0)
								is_682_[i_681_] = i_697_;
						} else if (i_683_ == 0) {
							int i_698_ = anIntArray6693[i_680_];
							if (i_698_ != 0) {
								if ((Class397_Sub1.anInt5767 & 0xffffff) == 16777215) {
									int i_699_ = Class397_Sub1.anInt5767 >>> 24;
									int i_700_ = 256 - i_699_;
									int i_701_ = is_682_[i_681_];
									is_682_[i_681_] = ((((i_698_ & 0xff00ff) * i_699_ + (i_701_ & 0xff00ff) * i_700_)
											& ~0xff00ff)
											+ (((i_698_ & 0xff00) * i_699_ + (i_701_ & 0xff00) * i_700_)
													& 0xff0000)) >> 8;
								} else if (Class397_Sub1.anInt5755 != 255) {
									int i_702_ = (((i_698_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_703_ = (((i_698_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_704_ = (((i_698_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									i_698_ = (i_702_ | i_703_ | i_704_) >>> 8;
									int i_705_ = is_682_[i_681_];
									is_682_[i_681_] = (((((i_698_ & 0xff00ff) * Class397_Sub1.anInt5755)
											+ ((i_705_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
											+ ((((i_698_ & 0xff00) * Class397_Sub1.anInt5755)
													+ ((i_705_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
								} else {
									int i_706_ = (((i_698_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
									int i_707_ = (((i_698_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
									int i_708_ = (((i_698_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
									is_682_[i_681_] = (i_706_ | i_707_ | i_708_) >>> 8;
								}
							}
						} else if (i_683_ == 3) {
							int i_709_ = anIntArray6693[i_680_];
							int i_710_ = Class397_Sub1.anInt5767;
							int i_711_ = i_709_ + i_710_;
							int i_712_ = (i_709_ & 0xff00ff) + (i_710_ & 0xff00ff);
							int i_713_ = ((i_712_ & 0x1000100) + (i_711_ - i_712_ & 0x10000));
							i_713_ = i_711_ - i_713_ | i_713_ - (i_713_ >>> 8);
							if (i_709_ == 0 && Class397_Sub1.anInt5755 != 255) {
								i_709_ = i_713_;
								i_713_ = is_682_[i_681_];
								i_713_ = (((((i_709_ & 0xff00ff) * Class397_Sub1.anInt5755)
										+ ((i_713_ & 0xff00ff) * Class397_Sub1.anInt5749)) & ~0xff00ff)
										+ ((((i_709_ & 0xff00) * Class397_Sub1.anInt5755)
												+ ((i_713_ & 0xff00) * Class397_Sub1.anInt5749)) & 0xff0000)) >> 8;
							}
							is_682_[i_681_] = i_713_;
						} else if (i_683_ == 2) {
							int i_714_ = anIntArray6693[i_680_];
							if (i_714_ != 0) {
								int i_715_ = (((i_714_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_716_ = (((i_714_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								is_682_[i_681_++] = (((i_715_ | i_716_) >>> 8) + Class397_Sub1.anInt5773);
							}
						} else
							throw new IllegalArgumentException();
					} else if (i_684_ == 2) {
						if (i_683_ == 1) {
							int i_717_ = anIntArray6693[i_680_];
							if (i_717_ != 0) {
								int i_718_ = is_682_[i_681_];
								int i_719_ = i_717_ + i_718_;
								int i_720_ = ((i_717_ & 0xff00ff) + (i_718_ & 0xff00ff));
								i_718_ = (i_720_ & 0x1000100) + (i_719_ - i_720_ & 0x10000);
								is_682_[i_681_] = i_719_ - i_718_ | i_718_ - (i_718_ >>> 8);
							}
						} else if (i_683_ == 0) {
							int i_721_ = anIntArray6693[i_680_];
							if (i_721_ != 0) {
								int i_722_ = (((i_721_ & 0xff0000) * Class397_Sub1.anInt5746) & ~0xffffff);
								int i_723_ = (((i_721_ & 0xff00) * Class397_Sub1.anInt5753) & 0xff0000);
								int i_724_ = (((i_721_ & 0xff) * Class397_Sub1.anInt5752) & 0xff00);
								i_721_ = (i_722_ | i_723_ | i_724_) >>> 8;
								int i_725_ = is_682_[i_681_];
								int i_726_ = i_721_ + i_725_;
								int i_727_ = ((i_721_ & 0xff00ff) + (i_725_ & 0xff00ff));
								i_725_ = (i_727_ & 0x1000100) + (i_726_ - i_727_ & 0x10000);
								is_682_[i_681_] = i_726_ - i_725_ | i_725_ - (i_725_ >>> 8);
							}
						} else if (i_683_ == 3) {
							int i_728_ = anIntArray6693[i_680_];
							int i_729_ = Class397_Sub1.anInt5767;
							int i_730_ = i_728_ + i_729_;
							int i_731_ = (i_728_ & 0xff00ff) + (i_729_ & 0xff00ff);
							int i_732_ = ((i_731_ & 0x1000100) + (i_730_ - i_731_ & 0x10000));
							i_728_ = i_730_ - i_732_ | i_732_ - (i_732_ >>> 8);
							i_732_ = is_682_[i_681_];
							i_730_ = i_728_ + i_732_;
							i_731_ = (i_728_ & 0xff00ff) + (i_732_ & 0xff00ff);
							i_732_ = (i_731_ & 0x1000100) + (i_730_ - i_731_ & 0x10000);
							is_682_[i_681_] = i_730_ - i_732_ | i_732_ - (i_732_ >>> 8);
						} else if (i_683_ == 2) {
							int i_733_ = anIntArray6693[i_680_];
							if (i_733_ != 0) {
								int i_734_ = (((i_733_ & 0xff00ff) * Class397_Sub1.anInt5755) & ~0xff00ff);
								int i_735_ = (((i_733_ & 0xff00) * Class397_Sub1.anInt5755) & 0xff0000);
								i_733_ = (((i_734_ | i_735_) >>> 8) + Class397_Sub1.anInt5773);
								int i_736_ = is_682_[i_681_];
								int i_737_ = i_733_ + i_736_;
								int i_738_ = ((i_733_ & 0xff00ff) + (i_736_ & 0xff00ff));
								i_736_ = (i_738_ & 0x1000100) + (i_737_ - i_738_ & 0x10000);
								is_682_[i_681_] = i_737_ - i_736_ | i_736_ - (i_736_ >>> 8);
							}
						}
					} else
						throw new IllegalArgumentException();
					i_675_ += Class397_Sub1.anInt5766;
					i_676_ += Class397_Sub1.anInt5777;
				}
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		}
	}

	public Class397_Sub1_Sub3(ha_Sub2 var_ha_Sub2, int i, int i_739_) {
		super(var_ha_Sub2, i, i_739_);
		anIntArray6693 = new int[i * i_739_];
	}

	public void method4084(int i, int i_740_, int i_741_) {
		throw new IllegalStateException("Can't capture alpha into a java_sprite_24");
	}

	public void method4090(int i, int i_742_, int i_743_, int i_744_, int i_745_, int i_746_) {
		int[] is = aHa_Sub2_5768.anIntArray4108;
		for (int i_747_ = 0; i_747_ < i_744_; i_747_++) {
			int i_748_ = (i_742_ + i_747_) * anInt5758 + i;
			int i_749_ = (i_746_ + i_747_) * aHa_Sub2_5768.anInt4084 + i_745_;
			for (int i_750_ = 0; i_750_ < i_743_; i_750_++)
				anIntArray6693[i_748_ + i_750_] = is[i_749_ + i_750_];
		}
	}

	Class397_Sub1_Sub3(ha_Sub2 var_ha_Sub2, int[] is, int i, int i_751_) {
		super(var_ha_Sub2, i, i_751_);
		anIntArray6693 = is;
	}

	public void method4094(int i, int i_752_, int i_753_, int i_754_, int i_755_, int i_756_, int i_757_, int i_758_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		if (i_753_ > 0 && i_754_ > 0) {
			int i_759_ = 0;
			int i_760_ = 0;
			int i_761_ = aHa_Sub2_5768.anInt4084;
			int i_762_ = anInt5772 + anInt5758 + anInt5757;
			int i_763_ = anInt5761 + anInt5756 + anInt5750;
			int i_764_ = (i_762_ << 16) / i_753_;
			int i_765_ = (i_763_ << 16) / i_754_;
			if (anInt5772 > 0) {
				int i_766_ = ((anInt5772 << 16) + i_764_ - 1) / i_764_;
				i += i_766_;
				i_759_ += i_766_ * i_764_ - (anInt5772 << 16);
			}
			if (anInt5761 > 0) {
				int i_767_ = ((anInt5761 << 16) + i_765_ - 1) / i_765_;
				i_752_ += i_767_;
				i_760_ += i_767_ * i_765_ - (anInt5761 << 16);
			}
			if (anInt5758 < i_762_)
				i_753_ = ((anInt5758 << 16) - i_759_ + i_764_ - 1) / i_764_;
			if (anInt5756 < i_763_)
				i_754_ = ((anInt5756 << 16) - i_760_ + i_765_ - 1) / i_765_;
			int i_768_ = i + i_752_ * i_761_;
			int i_769_ = i_761_ - i_753_;
			if (i_752_ + i_754_ > aHa_Sub2_5768.anInt4078)
				i_754_ -= i_752_ + i_754_ - aHa_Sub2_5768.anInt4078;
			if (i_752_ < aHa_Sub2_5768.anInt4085) {
				int i_770_ = aHa_Sub2_5768.anInt4085 - i_752_;
				i_754_ -= i_770_;
				i_768_ += i_770_ * i_761_;
				i_760_ += i_765_ * i_770_;
			}
			if (i + i_753_ > aHa_Sub2_5768.anInt4104) {
				int i_771_ = i + i_753_ - aHa_Sub2_5768.anInt4104;
				i_753_ -= i_771_;
				i_769_ += i_771_;
			}
			if (i < aHa_Sub2_5768.anInt4079) {
				int i_772_ = aHa_Sub2_5768.anInt4079 - i;
				i_753_ -= i_772_;
				i_768_ += i_772_;
				i_759_ += i_764_ * i_772_;
				i_769_ += i_772_;
			}
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_757_ == 0) {
				if (i_755_ == 1) {
					int i_773_ = i_759_;
					for (int i_774_ = -i_754_; i_774_ < 0; i_774_++) {
						int i_775_ = (i_760_ >> 16) * anInt5758;
						for (int i_776_ = -i_753_; i_776_ < 0; i_776_++) {
							is[i_768_++] = anIntArray6693[(i_759_ >> 16) + i_775_];
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_773_;
						i_768_ += i_769_;
					}
					return;
				}
				if (i_755_ == 0) {
					int i_777_ = (i_756_ & 0xff0000) >> 16;
					int i_778_ = (i_756_ & 0xff00) >> 8;
					int i_779_ = i_756_ & 0xff;
					int i_780_ = i_759_;
					for (int i_781_ = -i_754_; i_781_ < 0; i_781_++) {
						int i_782_ = (i_760_ >> 16) * anInt5758;
						for (int i_783_ = -i_753_; i_783_ < 0; i_783_++) {
							int i_784_ = anIntArray6693[(i_759_ >> 16) + i_782_];
							int i_785_ = (i_784_ & 0xff0000) * i_777_ & ~0xffffff;
							int i_786_ = (i_784_ & 0xff00) * i_778_ & 0xff0000;
							int i_787_ = (i_784_ & 0xff) * i_779_ & 0xff00;
							is[i_768_++] = (i_785_ | i_786_ | i_787_) >>> 8;
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_780_;
						i_768_ += i_769_;
					}
					return;
				}
				if (i_755_ == 3) {
					int i_788_ = i_759_;
					for (int i_789_ = -i_754_; i_789_ < 0; i_789_++) {
						int i_790_ = (i_760_ >> 16) * anInt5758;
						for (int i_791_ = -i_753_; i_791_ < 0; i_791_++) {
							int i_792_ = anIntArray6693[(i_759_ >> 16) + i_790_];
							int i_793_ = i_792_ + i_756_;
							int i_794_ = (i_792_ & 0xff00ff) + (i_756_ & 0xff00ff);
							int i_795_ = ((i_794_ & 0x1000100) + (i_793_ - i_794_ & 0x10000));
							is[i_768_++] = i_793_ - i_795_ | i_795_ - (i_795_ >>> 8);
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_788_;
						i_768_ += i_769_;
					}
					return;
				}
				if (i_755_ == 2) {
					int i_796_ = i_756_ >>> 24;
					int i_797_ = 256 - i_796_;
					int i_798_ = (i_756_ & 0xff00ff) * i_797_ & ~0xff00ff;
					int i_799_ = (i_756_ & 0xff00) * i_797_ & 0xff0000;
					i_756_ = (i_798_ | i_799_) >>> 8;
					int i_800_ = i_759_;
					for (int i_801_ = -i_754_; i_801_ < 0; i_801_++) {
						int i_802_ = (i_760_ >> 16) * anInt5758;
						for (int i_803_ = -i_753_; i_803_ < 0; i_803_++) {
							int i_804_ = anIntArray6693[(i_759_ >> 16) + i_802_];
							i_798_ = (i_804_ & 0xff00ff) * i_796_ & ~0xff00ff;
							i_799_ = (i_804_ & 0xff00) * i_796_ & 0xff0000;
							is[i_768_++] = ((i_798_ | i_799_) >>> 8) + i_756_;
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_800_;
						i_768_ += i_769_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_757_ == 1) {
				if (i_755_ == 1) {
					int i_805_ = i_759_;
					for (int i_806_ = -i_754_; i_806_ < 0; i_806_++) {
						int i_807_ = (i_760_ >> 16) * anInt5758;
						for (int i_808_ = -i_753_; i_808_ < 0; i_808_++) {
							int i_809_ = anIntArray6693[(i_759_ >> 16) + i_807_];
							if (i_809_ != 0)
								is[i_768_++] = i_809_;
							else
								i_768_++;
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_805_;
						i_768_ += i_769_;
					}
					return;
				}
				if (i_755_ == 0) {
					int i_810_ = i_759_;
					if ((i_756_ & 0xffffff) == 16777215) {
						int i_811_ = i_756_ >>> 24;
						int i_812_ = 256 - i_811_;
						for (int i_813_ = -i_754_; i_813_ < 0; i_813_++) {
							int i_814_ = (i_760_ >> 16) * anInt5758;
							for (int i_815_ = -i_753_; i_815_ < 0; i_815_++) {
								int i_816_ = anIntArray6693[(i_759_ >> 16) + i_814_];
								if (i_816_ != 0) {
									int i_817_ = is[i_768_];
									is[i_768_++] = ((((i_816_ & 0xff00ff) * i_811_ + (i_817_ & 0xff00ff) * i_812_)
											& ~0xff00ff)
											+ (((i_816_ & 0xff00) * i_811_ + (i_817_ & 0xff00) * i_812_)
													& 0xff0000)) >> 8;
								} else
									i_768_++;
								i_759_ += i_764_;
							}
							i_760_ += i_765_;
							i_759_ = i_810_;
							i_768_ += i_769_;
						}
					} else {
						int i_818_ = (i_756_ & 0xff0000) >> 16;
						int i_819_ = (i_756_ & 0xff00) >> 8;
						int i_820_ = i_756_ & 0xff;
						int i_821_ = i_756_ >>> 24;
						int i_822_ = 256 - i_821_;
						for (int i_823_ = -i_754_; i_823_ < 0; i_823_++) {
							int i_824_ = (i_760_ >> 16) * anInt5758;
							for (int i_825_ = -i_753_; i_825_ < 0; i_825_++) {
								int i_826_ = anIntArray6693[(i_759_ >> 16) + i_824_];
								if (i_826_ != 0) {
									if (i_821_ != 255) {
										int i_827_ = ((i_826_ & 0xff0000) * i_818_ & ~0xffffff);
										int i_828_ = ((i_826_ & 0xff00) * i_819_ & 0xff0000);
										int i_829_ = ((i_826_ & 0xff) * i_820_ & 0xff00);
										i_826_ = (i_827_ | i_828_ | i_829_) >>> 8;
										int i_830_ = is[i_768_];
										is[i_768_++] = ((((i_826_ & 0xff00ff) * i_821_ + ((i_830_ & 0xff00ff) * i_822_))
												& ~0xff00ff)
												+ (((i_826_ & 0xff00) * i_821_ + ((i_830_ & 0xff00) * i_822_))
														& 0xff0000)) >> 8;
									} else {
										int i_831_ = ((i_826_ & 0xff0000) * i_818_ & ~0xffffff);
										int i_832_ = ((i_826_ & 0xff00) * i_819_ & 0xff0000);
										int i_833_ = ((i_826_ & 0xff) * i_820_ & 0xff00);
										is[i_768_++] = (i_831_ | i_832_ | i_833_) >>> 8;
									}
								} else
									i_768_++;
								i_759_ += i_764_;
							}
							i_760_ += i_765_;
							i_759_ = i_810_;
							i_768_ += i_769_;
						}
						return;
					}
					return;
				}
				if (i_755_ == 3) {
					int i_834_ = i_759_;
					int i_835_ = i_756_ >>> 24;
					int i_836_ = 256 - i_835_;
					for (int i_837_ = -i_754_; i_837_ < 0; i_837_++) {
						int i_838_ = (i_760_ >> 16) * anInt5758;
						for (int i_839_ = -i_753_; i_839_ < 0; i_839_++) {
							int i_840_ = anIntArray6693[(i_759_ >> 16) + i_838_];
							int i_841_ = i_840_ + i_756_;
							int i_842_ = (i_840_ & 0xff00ff) + (i_756_ & 0xff00ff);
							int i_843_ = ((i_842_ & 0x1000100) + (i_841_ - i_842_ & 0x10000));
							i_843_ = i_841_ - i_843_ | i_843_ - (i_843_ >>> 8);
							if (i_840_ == 0 && i_835_ != 255) {
								i_840_ = i_843_;
								i_843_ = is[i_768_];
								i_843_ = ((((i_840_ & 0xff00ff) * i_835_ + (i_843_ & 0xff00ff) * i_836_) & ~0xff00ff)
										+ (((i_840_ & 0xff00) * i_835_ + (i_843_ & 0xff00) * i_836_) & 0xff0000)) >> 8;
							}
							is[i_768_++] = i_843_;
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_834_;
						i_768_ += i_769_;
					}
					return;
				}
				if (i_755_ == 2) {
					int i_844_ = i_756_ >>> 24;
					int i_845_ = 256 - i_844_;
					int i_846_ = (i_756_ & 0xff00ff) * i_845_ & ~0xff00ff;
					int i_847_ = (i_756_ & 0xff00) * i_845_ & 0xff0000;
					i_756_ = (i_846_ | i_847_) >>> 8;
					int i_848_ = i_759_;
					for (int i_849_ = -i_754_; i_849_ < 0; i_849_++) {
						int i_850_ = (i_760_ >> 16) * anInt5758;
						for (int i_851_ = -i_753_; i_851_ < 0; i_851_++) {
							int i_852_ = anIntArray6693[(i_759_ >> 16) + i_850_];
							if (i_852_ != 0) {
								i_846_ = (i_852_ & 0xff00ff) * i_844_ & ~0xff00ff;
								i_847_ = (i_852_ & 0xff00) * i_844_ & 0xff0000;
								is[i_768_++] = ((i_846_ | i_847_) >>> 8) + i_756_;
							} else
								i_768_++;
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_848_;
						i_768_ += i_769_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_757_ == 2) {
				if (i_755_ == 1) {
					int i_853_ = i_759_;
					for (int i_854_ = -i_754_; i_854_ < 0; i_854_++) {
						int i_855_ = (i_760_ >> 16) * anInt5758;
						for (int i_856_ = -i_753_; i_856_ < 0; i_856_++) {
							int i_857_ = anIntArray6693[(i_759_ >> 16) + i_855_];
							if (i_857_ != 0) {
								int i_858_ = is[i_768_];
								int i_859_ = i_857_ + i_858_;
								int i_860_ = ((i_857_ & 0xff00ff) + (i_858_ & 0xff00ff));
								i_858_ = (i_860_ & 0x1000100) + (i_859_ - i_860_ & 0x10000);
								is[i_768_++] = i_859_ - i_858_ | i_858_ - (i_858_ >>> 8);
							} else
								i_768_++;
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_853_;
						i_768_ += i_769_;
					}
					return;
				}
				if (i_755_ == 0) {
					int i_861_ = i_759_;
					int i_862_ = (i_756_ & 0xff0000) >> 16;
					int i_863_ = (i_756_ & 0xff00) >> 8;
					int i_864_ = i_756_ & 0xff;
					for (int i_865_ = -i_754_; i_865_ < 0; i_865_++) {
						int i_866_ = (i_760_ >> 16) * anInt5758;
						for (int i_867_ = -i_753_; i_867_ < 0; i_867_++) {
							int i_868_ = anIntArray6693[(i_759_ >> 16) + i_866_];
							if (i_868_ != 0) {
								int i_869_ = (i_868_ & 0xff0000) * i_862_ & ~0xffffff;
								int i_870_ = (i_868_ & 0xff00) * i_863_ & 0xff0000;
								int i_871_ = (i_868_ & 0xff) * i_864_ & 0xff00;
								i_868_ = (i_869_ | i_870_ | i_871_) >>> 8;
								int i_872_ = is[i_768_];
								int i_873_ = i_868_ + i_872_;
								int i_874_ = ((i_868_ & 0xff00ff) + (i_872_ & 0xff00ff));
								i_872_ = (i_874_ & 0x1000100) + (i_873_ - i_874_ & 0x10000);
								is[i_768_++] = i_873_ - i_872_ | i_872_ - (i_872_ >>> 8);
							} else
								i_768_++;
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_861_;
						i_768_ += i_769_;
					}
					return;
				}
				if (i_755_ == 3) {
					int i_875_ = i_759_;
					for (int i_876_ = -i_754_; i_876_ < 0; i_876_++) {
						int i_877_ = (i_760_ >> 16) * anInt5758;
						for (int i_878_ = -i_753_; i_878_ < 0; i_878_++) {
							int i_879_ = anIntArray6693[(i_759_ >> 16) + i_877_];
							int i_880_ = i_879_ + i_756_;
							int i_881_ = (i_879_ & 0xff00ff) + (i_756_ & 0xff00ff);
							int i_882_ = ((i_881_ & 0x1000100) + (i_880_ - i_881_ & 0x10000));
							i_879_ = i_880_ - i_882_ | i_882_ - (i_882_ >>> 8);
							i_882_ = is[i_768_];
							i_880_ = i_879_ + i_882_;
							i_881_ = (i_879_ & 0xff00ff) + (i_882_ & 0xff00ff);
							i_882_ = (i_881_ & 0x1000100) + (i_880_ - i_881_ & 0x10000);
							is[i_768_++] = i_880_ - i_882_ | i_882_ - (i_882_ >>> 8);
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_875_;
						i_768_ += i_769_;
					}
					return;
				}
				if (i_755_ == 2) {
					int i_883_ = i_756_ >>> 24;
					int i_884_ = 256 - i_883_;
					int i_885_ = (i_756_ & 0xff00ff) * i_884_ & ~0xff00ff;
					int i_886_ = (i_756_ & 0xff00) * i_884_ & 0xff0000;
					i_756_ = (i_885_ | i_886_) >>> 8;
					int i_887_ = i_759_;
					for (int i_888_ = -i_754_; i_888_ < 0; i_888_++) {
						int i_889_ = (i_760_ >> 16) * anInt5758;
						for (int i_890_ = -i_753_; i_890_ < 0; i_890_++) {
							int i_891_ = anIntArray6693[(i_759_ >> 16) + i_889_];
							if (i_891_ != 0) {
								i_885_ = (i_891_ & 0xff00ff) * i_883_ & ~0xff00ff;
								i_886_ = (i_891_ & 0xff00) * i_883_ & 0xff0000;
								i_891_ = ((i_885_ | i_886_) >>> 8) + i_756_;
								int i_892_ = is[i_768_];
								int i_893_ = i_891_ + i_892_;
								int i_894_ = ((i_891_ & 0xff00ff) + (i_892_ & 0xff00ff));
								i_892_ = (i_894_ & 0x1000100) + (i_893_ - i_894_ & 0x10000);
								is[i_768_++] = i_893_ - i_892_ | i_892_ - (i_892_ >>> 8);
							} else
								i_768_++;
							i_759_ += i_764_;
						}
						i_760_ += i_765_;
						i_759_ = i_887_;
						i_768_ += i_769_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	public void method4079(int i, int i_895_, int i_896_, int i_897_, int i_898_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		int i_899_ = aHa_Sub2_5768.anInt4084;
		i += anInt5772;
		i_895_ += anInt5761;
		int i_900_ = i_895_ * i_899_ + i;
		int i_901_ = 0;
		int i_902_ = anInt5756;
		int i_903_ = anInt5758;
		int i_904_ = i_899_ - i_903_;
		int i_905_ = 0;
		if (i_895_ < aHa_Sub2_5768.anInt4085) {
			int i_906_ = aHa_Sub2_5768.anInt4085 - i_895_;
			i_902_ -= i_906_;
			i_895_ = aHa_Sub2_5768.anInt4085;
			i_901_ += i_906_ * i_903_;
			i_900_ += i_906_ * i_899_;
		}
		if (i_895_ + i_902_ > aHa_Sub2_5768.anInt4078)
			i_902_ -= i_895_ + i_902_ - aHa_Sub2_5768.anInt4078;
		if (i < aHa_Sub2_5768.anInt4079) {
			int i_907_ = aHa_Sub2_5768.anInt4079 - i;
			i_903_ -= i_907_;
			i = aHa_Sub2_5768.anInt4079;
			i_901_ += i_907_;
			i_900_ += i_907_;
			i_905_ += i_907_;
			i_904_ += i_907_;
		}
		if (i + i_903_ > aHa_Sub2_5768.anInt4104) {
			int i_908_ = i + i_903_ - aHa_Sub2_5768.anInt4104;
			i_903_ -= i_908_;
			i_905_ += i_908_;
			i_904_ += i_908_;
		}
		if (i_903_ > 0 && i_902_ > 0) {
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_898_ == 0) {
				if (i_896_ == 1) {
					for (int i_909_ = -i_902_; i_909_ < 0; i_909_++) {
						int i_910_ = i_900_ + i_903_ - 3;
						while (i_900_ < i_910_) {
							is[i_900_++] = anIntArray6693[i_901_++];
							is[i_900_++] = anIntArray6693[i_901_++];
							is[i_900_++] = anIntArray6693[i_901_++];
							is[i_900_++] = anIntArray6693[i_901_++];
						}
						i_910_ += 3;
						while (i_900_ < i_910_)
							is[i_900_++] = anIntArray6693[i_901_++];
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				if (i_896_ == 0) {
					int i_911_ = (i_897_ & 0xff0000) >> 16;
					int i_912_ = (i_897_ & 0xff00) >> 8;
					int i_913_ = i_897_ & 0xff;
					for (int i_914_ = -i_902_; i_914_ < 0; i_914_++) {
						for (int i_915_ = -i_903_; i_915_ < 0; i_915_++) {
							int i_916_ = anIntArray6693[i_901_++];
							int i_917_ = (i_916_ & 0xff0000) * i_911_ & ~0xffffff;
							int i_918_ = (i_916_ & 0xff00) * i_912_ & 0xff0000;
							int i_919_ = (i_916_ & 0xff) * i_913_ & 0xff00;
							is[i_900_++] = (i_917_ | i_918_ | i_919_) >>> 8;
						}
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				if (i_896_ == 3) {
					for (int i_920_ = -i_902_; i_920_ < 0; i_920_++) {
						for (int i_921_ = -i_903_; i_921_ < 0; i_921_++) {
							int i_922_ = anIntArray6693[i_901_++];
							int i_923_ = i_922_ + i_897_;
							int i_924_ = (i_922_ & 0xff00ff) + (i_897_ & 0xff00ff);
							int i_925_ = ((i_924_ & 0x1000100) + (i_923_ - i_924_ & 0x10000));
							is[i_900_++] = i_923_ - i_925_ | i_925_ - (i_925_ >>> 8);
						}
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				if (i_896_ == 2) {
					int i_926_ = i_897_ >>> 24;
					int i_927_ = 256 - i_926_;
					int i_928_ = (i_897_ & 0xff00ff) * i_927_ & ~0xff00ff;
					int i_929_ = (i_897_ & 0xff00) * i_927_ & 0xff0000;
					i_897_ = (i_928_ | i_929_) >>> 8;
					for (int i_930_ = -i_902_; i_930_ < 0; i_930_++) {
						for (int i_931_ = -i_903_; i_931_ < 0; i_931_++) {
							int i_932_ = anIntArray6693[i_901_++];
							i_928_ = (i_932_ & 0xff00ff) * i_926_ & ~0xff00ff;
							i_929_ = (i_932_ & 0xff00) * i_926_ & 0xff0000;
							is[i_900_++] = ((i_928_ | i_929_) >>> 8) + i_897_;
						}
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_898_ == 1) {
				if (i_896_ == 1) {
					for (int i_933_ = -i_902_; i_933_ < 0; i_933_++) {
						int i_934_ = i_900_ + i_903_ - 3;
						while (i_900_ < i_934_) {
							int i_935_ = anIntArray6693[i_901_++];
							if (i_935_ != 0)
								is[i_900_++] = i_935_;
							else
								i_900_++;
							i_935_ = anIntArray6693[i_901_++];
							if (i_935_ != 0)
								is[i_900_++] = i_935_;
							else
								i_900_++;
							i_935_ = anIntArray6693[i_901_++];
							if (i_935_ != 0)
								is[i_900_++] = i_935_;
							else
								i_900_++;
							i_935_ = anIntArray6693[i_901_++];
							if (i_935_ != 0)
								is[i_900_++] = i_935_;
							else
								i_900_++;
						}
						i_934_ += 3;
						while (i_900_ < i_934_) {
							int i_936_ = anIntArray6693[i_901_++];
							if (i_936_ != 0)
								is[i_900_++] = i_936_;
							else
								i_900_++;
						}
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				if (i_896_ == 0) {
					if ((i_897_ & 0xffffff) == 16777215) {
						int i_937_ = i_897_ >>> 24;
						int i_938_ = 256 - i_937_;
						for (int i_939_ = -i_902_; i_939_ < 0; i_939_++) {
							for (int i_940_ = -i_903_; i_940_ < 0; i_940_++) {
								int i_941_ = anIntArray6693[i_901_++];
								if (i_941_ != 0) {
									int i_942_ = is[i_900_];
									is[i_900_++] = ((((i_941_ & 0xff00ff) * i_937_ + (i_942_ & 0xff00ff) * i_938_)
											& ~0xff00ff)
											+ (((i_941_ & 0xff00) * i_937_ + (i_942_ & 0xff00) * i_938_)
													& 0xff0000)) >> 8;
								} else
									i_900_++;
							}
							i_900_ += i_904_;
							i_901_ += i_905_;
						}
					} else {
						int i_943_ = (i_897_ & 0xff0000) >> 16;
						int i_944_ = (i_897_ & 0xff00) >> 8;
						int i_945_ = i_897_ & 0xff;
						int i_946_ = i_897_ >>> 24;
						int i_947_ = 256 - i_946_;
						for (int i_948_ = -i_902_; i_948_ < 0; i_948_++) {
							for (int i_949_ = -i_903_; i_949_ < 0; i_949_++) {
								int i_950_ = anIntArray6693[i_901_++];
								if (i_950_ != 0) {
									if (i_946_ != 255) {
										int i_951_ = ((i_950_ & 0xff0000) * i_943_ & ~0xffffff);
										int i_952_ = ((i_950_ & 0xff00) * i_944_ & 0xff0000);
										int i_953_ = ((i_950_ & 0xff) * i_945_ & 0xff00);
										i_950_ = (i_951_ | i_952_ | i_953_) >>> 8;
										int i_954_ = is[i_900_];
										is[i_900_++] = ((((i_950_ & 0xff00ff) * i_946_ + ((i_954_ & 0xff00ff) * i_947_))
												& ~0xff00ff)
												+ (((i_950_ & 0xff00) * i_946_ + ((i_954_ & 0xff00) * i_947_))
														& 0xff0000)) >> 8;
									} else {
										int i_955_ = ((i_950_ & 0xff0000) * i_943_ & ~0xffffff);
										int i_956_ = ((i_950_ & 0xff00) * i_944_ & 0xff0000);
										int i_957_ = ((i_950_ & 0xff) * i_945_ & 0xff00);
										is[i_900_++] = (i_955_ | i_956_ | i_957_) >>> 8;
									}
								} else
									i_900_++;
							}
							i_900_ += i_904_;
							i_901_ += i_905_;
						}
						return;
					}
					return;
				}
				if (i_896_ == 3) {
					int i_958_ = i_897_ >>> 24;
					int i_959_ = 256 - i_958_;
					for (int i_960_ = -i_902_; i_960_ < 0; i_960_++) {
						for (int i_961_ = -i_903_; i_961_ < 0; i_961_++) {
							int i_962_ = anIntArray6693[i_901_++];
							int i_963_ = i_962_ + i_897_;
							int i_964_ = (i_962_ & 0xff00ff) + (i_897_ & 0xff00ff);
							int i_965_ = ((i_964_ & 0x1000100) + (i_963_ - i_964_ & 0x10000));
							i_965_ = i_963_ - i_965_ | i_965_ - (i_965_ >>> 8);
							if (i_962_ == 0 && i_958_ != 255) {
								i_962_ = i_965_;
								i_965_ = is[i_900_];
								i_965_ = ((((i_962_ & 0xff00ff) * i_958_ + (i_965_ & 0xff00ff) * i_959_) & ~0xff00ff)
										+ (((i_962_ & 0xff00) * i_958_ + (i_965_ & 0xff00) * i_959_) & 0xff0000)) >> 8;
							}
							is[i_900_++] = i_965_;
						}
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				if (i_896_ == 2) {
					int i_966_ = i_897_ >>> 24;
					int i_967_ = 256 - i_966_;
					int i_968_ = (i_897_ & 0xff00ff) * i_967_ & ~0xff00ff;
					int i_969_ = (i_897_ & 0xff00) * i_967_ & 0xff0000;
					i_897_ = (i_968_ | i_969_) >>> 8;
					for (int i_970_ = -i_902_; i_970_ < 0; i_970_++) {
						for (int i_971_ = -i_903_; i_971_ < 0; i_971_++) {
							int i_972_ = anIntArray6693[i_901_++];
							if (i_972_ != 0) {
								i_968_ = (i_972_ & 0xff00ff) * i_966_ & ~0xff00ff;
								i_969_ = (i_972_ & 0xff00) * i_966_ & 0xff0000;
								is[i_900_++] = ((i_968_ | i_969_) >>> 8) + i_897_;
							} else
								i_900_++;
						}
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_898_ == 2) {
				if (i_896_ == 1) {
					for (int i_973_ = -i_902_; i_973_ < 0; i_973_++) {
						for (int i_974_ = -i_903_; i_974_ < 0; i_974_++) {
							int i_975_ = anIntArray6693[i_901_++];
							if (i_975_ != 0) {
								int i_976_ = is[i_900_];
								int i_977_ = i_975_ + i_976_;
								int i_978_ = ((i_975_ & 0xff00ff) + (i_976_ & 0xff00ff));
								i_976_ = (i_978_ & 0x1000100) + (i_977_ - i_978_ & 0x10000);
								is[i_900_++] = i_977_ - i_976_ | i_976_ - (i_976_ >>> 8);
							} else
								i_900_++;
						}
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				if (i_896_ == 0) {
					int i_979_ = (i_897_ & 0xff0000) >> 16;
					int i_980_ = (i_897_ & 0xff00) >> 8;
					int i_981_ = i_897_ & 0xff;
					for (int i_982_ = -i_902_; i_982_ < 0; i_982_++) {
						for (int i_983_ = -i_903_; i_983_ < 0; i_983_++) {
							int i_984_ = anIntArray6693[i_901_++];
							if (i_984_ != 0) {
								int i_985_ = (i_984_ & 0xff0000) * i_979_ & ~0xffffff;
								int i_986_ = (i_984_ & 0xff00) * i_980_ & 0xff0000;
								int i_987_ = (i_984_ & 0xff) * i_981_ & 0xff00;
								i_984_ = (i_985_ | i_986_ | i_987_) >>> 8;
								int i_988_ = is[i_900_];
								int i_989_ = i_984_ + i_988_;
								int i_990_ = ((i_984_ & 0xff00ff) + (i_988_ & 0xff00ff));
								i_988_ = (i_990_ & 0x1000100) + (i_989_ - i_990_ & 0x10000);
								is[i_900_++] = i_989_ - i_988_ | i_988_ - (i_988_ >>> 8);
							} else
								i_900_++;
						}
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				if (i_896_ == 3) {
					for (int i_991_ = -i_902_; i_991_ < 0; i_991_++) {
						for (int i_992_ = -i_903_; i_992_ < 0; i_992_++) {
							int i_993_ = anIntArray6693[i_901_++];
							int i_994_ = i_993_ + i_897_;
							int i_995_ = (i_993_ & 0xff00ff) + (i_897_ & 0xff00ff);
							int i_996_ = ((i_995_ & 0x1000100) + (i_994_ - i_995_ & 0x10000));
							i_993_ = i_994_ - i_996_ | i_996_ - (i_996_ >>> 8);
							i_996_ = is[i_900_];
							i_994_ = i_993_ + i_996_;
							i_995_ = (i_993_ & 0xff00ff) + (i_996_ & 0xff00ff);
							i_996_ = (i_995_ & 0x1000100) + (i_994_ - i_995_ & 0x10000);
							is[i_900_++] = i_994_ - i_996_ | i_996_ - (i_996_ >>> 8);
						}
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				if (i_896_ == 2) {
					int i_997_ = i_897_ >>> 24;
					int i_998_ = 256 - i_997_;
					int i_999_ = (i_897_ & 0xff00ff) * i_998_ & ~0xff00ff;
					int i_1000_ = (i_897_ & 0xff00) * i_998_ & 0xff0000;
					i_897_ = (i_999_ | i_1000_) >>> 8;
					for (int i_1001_ = -i_902_; i_1001_ < 0; i_1001_++) {
						for (int i_1002_ = -i_903_; i_1002_ < 0; i_1002_++) {
							int i_1003_ = anIntArray6693[i_901_++];
							if (i_1003_ != 0) {
								i_999_ = ((i_1003_ & 0xff00ff) * i_997_ & ~0xff00ff);
								i_1000_ = (i_1003_ & 0xff00) * i_997_ & 0xff0000;
								i_1003_ = ((i_999_ | i_1000_) >>> 8) + i_897_;
								int i_1004_ = is[i_900_];
								int i_1005_ = i_1003_ + i_1004_;
								int i_1006_ = ((i_1003_ & 0xff00ff) + (i_1004_ & 0xff00ff));
								i_1004_ = ((i_1006_ & 0x1000100) + (i_1005_ - i_1006_ & 0x10000));
								is[i_900_++] = i_1005_ - i_1004_ | i_1004_ - (i_1004_ >>> 8);
							} else
								i_900_++;
						}
						i_900_ += i_904_;
						i_901_ += i_905_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	public void method4093(int i, int i_1007_, aa var_aa, int i_1008_, int i_1009_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		i += anInt5772;
		i_1007_ += anInt5761;
		int i_1010_ = 0;
		int i_1011_ = aHa_Sub2_5768.anInt4084;
		int i_1012_ = anInt5758;
		int i_1013_ = anInt5756;
		int i_1014_ = i_1011_ - i_1012_;
		int i_1015_ = 0;
		int i_1016_ = i + i_1007_ * i_1011_;
		if (i_1007_ < aHa_Sub2_5768.anInt4085) {
			int i_1017_ = aHa_Sub2_5768.anInt4085 - i_1007_;
			i_1013_ -= i_1017_;
			i_1007_ = aHa_Sub2_5768.anInt4085;
			i_1010_ += i_1017_ * i_1012_;
			i_1016_ += i_1017_ * i_1011_;
		}
		if (i_1007_ + i_1013_ > aHa_Sub2_5768.anInt4078)
			i_1013_ -= i_1007_ + i_1013_ - aHa_Sub2_5768.anInt4078;
		if (i < aHa_Sub2_5768.anInt4079) {
			int i_1018_ = aHa_Sub2_5768.anInt4079 - i;
			i_1012_ -= i_1018_;
			i = aHa_Sub2_5768.anInt4079;
			i_1010_ += i_1018_;
			i_1016_ += i_1018_;
			i_1015_ += i_1018_;
			i_1014_ += i_1018_;
		}
		if (i + i_1012_ > aHa_Sub2_5768.anInt4104) {
			int i_1019_ = i + i_1012_ - aHa_Sub2_5768.anInt4104;
			i_1012_ -= i_1019_;
			i_1015_ += i_1019_;
			i_1014_ += i_1019_;
		}
		if (i_1012_ > 0 && i_1013_ > 0) {
			aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
			int[] is = var_aa_Sub2.anIntArray3726;
			int[] is_1020_ = var_aa_Sub2.anIntArray3729;
			int[] is_1021_ = aHa_Sub2_5768.anIntArray4108;
			int i_1022_ = i_1007_;
			if (i_1009_ > i_1022_) {
				i_1022_ = i_1009_;
				i_1016_ += (i_1009_ - i_1007_) * i_1011_;
				i_1010_ += (i_1009_ - i_1007_) * anInt5758;
			}
			int i_1023_ = (i_1009_ + is.length < i_1007_ + i_1013_ ? i_1009_ + is.length : i_1007_ + i_1013_);
			for (int i_1024_ = i_1022_; i_1024_ < i_1023_; i_1024_++) {
				int i_1025_ = is[i_1024_ - i_1009_] + i_1008_;
				int i_1026_ = is_1020_[i_1024_ - i_1009_];
				int i_1027_ = i_1012_;
				if (i > i_1025_) {
					int i_1028_ = i - i_1025_;
					if (i_1028_ >= i_1026_) {
						i_1010_ += i_1012_ + i_1015_;
						i_1016_ += i_1012_ + i_1014_;
						continue;
					}
					i_1026_ -= i_1028_;
				} else {
					int i_1029_ = i_1025_ - i;
					if (i_1029_ >= i_1012_) {
						i_1010_ += i_1012_ + i_1015_;
						i_1016_ += i_1012_ + i_1014_;
						continue;
					}
					i_1010_ += i_1029_;
					i_1027_ -= i_1029_;
					i_1016_ += i_1029_;
				}
				int i_1030_ = 0;
				if (i_1027_ < i_1026_)
					i_1026_ = i_1027_;
				else
					i_1030_ = i_1027_ - i_1026_;
				for (int i_1031_ = -i_1026_; i_1031_ < 0; i_1031_++) {
					int i_1032_ = anIntArray6693[i_1010_++];
					if (i_1032_ != 0)
						is_1021_[i_1016_++] = i_1032_;
					else
						i_1016_++;
				}
				i_1010_ += i_1030_ + i_1015_;
				i_1016_ += i_1030_ + i_1014_;
			}
		}
	}

	public void method4103(int i, int i_1033_, int i_1034_, int i_1035_, int i_1036_, int i_1037_, int i_1038_,
			int i_1039_, int i_1040_) {
		if (i_1035_ > 0 && i_1036_ > 0) {
			int i_1041_ = 0;
			int i_1042_ = 0;
			int i_1043_ = anInt5772 + anInt5758 + anInt5757;
			int i_1044_ = anInt5761 + anInt5756 + anInt5750;
			int i_1045_ = (i_1043_ << 16) / i_1035_;
			int i_1046_ = (i_1044_ << 16) / i_1036_;
			if (anInt5772 > 0) {
				int i_1047_ = ((anInt5772 << 16) + i_1045_ - 1) / i_1045_;
				i += i_1047_;
				i_1041_ += i_1047_ * i_1045_ - (anInt5772 << 16);
			}
			if (anInt5761 > 0) {
				int i_1048_ = ((anInt5761 << 16) + i_1046_ - 1) / i_1046_;
				i_1033_ += i_1048_;
				i_1042_ += i_1048_ * i_1046_ - (anInt5761 << 16);
			}
			if (anInt5758 < i_1043_)
				i_1035_ = ((anInt5758 << 16) - i_1041_ + i_1045_ - 1) / i_1045_;
			if (anInt5756 < i_1044_)
				i_1036_ = ((anInt5756 << 16) - i_1042_ + i_1046_ - 1) / i_1046_;
			int i_1049_ = i + i_1033_ * aHa_Sub2_5768.anInt4084;
			int i_1050_ = aHa_Sub2_5768.anInt4084 - i_1035_;
			if (i_1033_ + i_1036_ > aHa_Sub2_5768.anInt4078)
				i_1036_ -= i_1033_ + i_1036_ - aHa_Sub2_5768.anInt4078;
			if (i_1033_ < aHa_Sub2_5768.anInt4085) {
				int i_1051_ = aHa_Sub2_5768.anInt4085 - i_1033_;
				i_1036_ -= i_1051_;
				i_1049_ += i_1051_ * aHa_Sub2_5768.anInt4084;
				i_1042_ += i_1046_ * i_1051_;
			}
			if (i + i_1035_ > aHa_Sub2_5768.anInt4104) {
				int i_1052_ = i + i_1035_ - aHa_Sub2_5768.anInt4104;
				i_1035_ -= i_1052_;
				i_1050_ += i_1052_;
			}
			if (i < aHa_Sub2_5768.anInt4079) {
				int i_1053_ = aHa_Sub2_5768.anInt4079 - i;
				i_1035_ -= i_1053_;
				i_1049_ += i_1053_;
				i_1041_ += i_1045_ * i_1053_;
				i_1050_ += i_1053_;
			}
			float[] fs = aHa_Sub2_5768.aFloatArray4074;
			int[] is = aHa_Sub2_5768.anIntArray4108;
			if (i_1039_ == 0) {
				if (i_1037_ == 1) {
					int i_1054_ = i_1041_;
					for (int i_1055_ = -i_1036_; i_1055_ < 0; i_1055_++) {
						int i_1056_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1057_ = -i_1035_; i_1057_ < 0; i_1057_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								is[i_1049_] = (anIntArray6693[(i_1041_ >> 16) + i_1056_]);
								fs[i_1049_] = (float) i_1034_;
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1054_;
						i_1049_ += i_1050_;
					}
					return;
				}
				if (i_1037_ == 0) {
					int i_1058_ = (i_1038_ & 0xff0000) >> 16;
					int i_1059_ = (i_1038_ & 0xff00) >> 8;
					int i_1060_ = i_1038_ & 0xff;
					int i_1061_ = i_1041_;
					for (int i_1062_ = -i_1036_; i_1062_ < 0; i_1062_++) {
						int i_1063_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1064_ = -i_1035_; i_1064_ < 0; i_1064_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								int i_1065_ = (anIntArray6693[(i_1041_ >> 16) + i_1063_]);
								int i_1066_ = ((i_1065_ & 0xff0000) * i_1058_ & ~0xffffff);
								int i_1067_ = (i_1065_ & 0xff00) * i_1059_ & 0xff0000;
								int i_1068_ = (i_1065_ & 0xff) * i_1060_ & 0xff00;
								is[i_1049_] = (i_1066_ | i_1067_ | i_1068_) >>> 8;
								fs[i_1049_] = (float) i_1034_;
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1061_;
						i_1049_ += i_1050_;
					}
					return;
				}
				if (i_1037_ == 3) {
					int i_1069_ = i_1041_;
					for (int i_1070_ = -i_1036_; i_1070_ < 0; i_1070_++) {
						int i_1071_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1072_ = -i_1035_; i_1072_ < 0; i_1072_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								int i_1073_ = (anIntArray6693[(i_1041_ >> 16) + i_1071_]);
								int i_1074_ = i_1073_ + i_1038_;
								int i_1075_ = ((i_1073_ & 0xff00ff) + (i_1038_ & 0xff00ff));
								int i_1076_ = ((i_1075_ & 0x1000100) + (i_1074_ - i_1075_ & 0x10000));
								is[i_1049_] = i_1074_ - i_1076_ | i_1076_ - (i_1076_ >>> 8);
								fs[i_1049_] = (float) i_1034_;
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1069_;
						i_1049_ += i_1050_;
					}
					return;
				}
				if (i_1037_ == 2) {
					int i_1077_ = i_1038_ >>> 24;
					int i_1078_ = 256 - i_1077_;
					int i_1079_ = (i_1038_ & 0xff00ff) * i_1078_ & ~0xff00ff;
					int i_1080_ = (i_1038_ & 0xff00) * i_1078_ & 0xff0000;
					i_1038_ = (i_1079_ | i_1080_) >>> 8;
					int i_1081_ = i_1041_;
					for (int i_1082_ = -i_1036_; i_1082_ < 0; i_1082_++) {
						int i_1083_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1084_ = -i_1035_; i_1084_ < 0; i_1084_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								int i_1085_ = (anIntArray6693[(i_1041_ >> 16) + i_1083_]);
								i_1079_ = ((i_1085_ & 0xff00ff) * i_1077_ & ~0xff00ff);
								i_1080_ = (i_1085_ & 0xff00) * i_1077_ & 0xff0000;
								is[i_1049_] = ((i_1079_ | i_1080_) >>> 8) + i_1038_;
								fs[i_1049_] = (float) i_1034_;
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1081_;
						i_1049_ += i_1050_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_1039_ == 1) {
				if (i_1037_ == 1) {
					int i_1086_ = i_1041_;
					for (int i_1087_ = -i_1036_; i_1087_ < 0; i_1087_++) {
						int i_1088_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1089_ = -i_1035_; i_1089_ < 0; i_1089_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								int i_1090_ = (anIntArray6693[(i_1041_ >> 16) + i_1088_]);
								if (i_1090_ != 0) {
									is[i_1049_] = i_1090_;
									fs[i_1049_] = (float) i_1034_;
								}
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1086_;
						i_1049_ += i_1050_;
					}
					return;
				}
				if (i_1037_ == 0) {
					int i_1091_ = i_1041_;
					if ((i_1038_ & 0xffffff) == 16777215) {
						int i_1092_ = i_1038_ >>> 24;
						int i_1093_ = 256 - i_1092_;
						for (int i_1094_ = -i_1036_; i_1094_ < 0; i_1094_++) {
							int i_1095_ = (i_1042_ >> 16) * anInt5758;
							for (int i_1096_ = -i_1035_; i_1096_ < 0; i_1096_++) {
								if ((float) i_1034_ < fs[i_1049_]) {
									int i_1097_ = (anIntArray6693[(i_1041_ >> 16) + i_1095_]);
									if (i_1097_ != 0) {
										int i_1098_ = is[i_1049_];
										is[i_1049_] = ((((i_1097_ & 0xff00ff) * i_1092_
												+ ((i_1098_ & 0xff00ff) * i_1093_)) & ~0xff00ff)
												+ (((i_1097_ & 0xff00) * i_1092_ + ((i_1098_ & 0xff00) * i_1093_))
														& 0xff0000)) >> 8;
										fs[i_1049_] = (float) i_1034_;
									}
								}
								i_1041_ += i_1045_;
								i_1049_++;
							}
							i_1042_ += i_1046_;
							i_1041_ = i_1091_;
							i_1049_ += i_1050_;
						}
					} else {
						int i_1099_ = (i_1038_ & 0xff0000) >> 16;
						int i_1100_ = (i_1038_ & 0xff00) >> 8;
						int i_1101_ = i_1038_ & 0xff;
						int i_1102_ = i_1038_ >>> 24;
						int i_1103_ = 256 - i_1102_;
						for (int i_1104_ = -i_1036_; i_1104_ < 0; i_1104_++) {
							int i_1105_ = (i_1042_ >> 16) * anInt5758;
							for (int i_1106_ = -i_1035_; i_1106_ < 0; i_1106_++) {
								if ((float) i_1034_ < fs[i_1049_]) {
									int i_1107_ = (anIntArray6693[(i_1041_ >> 16) + i_1105_]);
									if (i_1107_ != 0) {
										if (i_1102_ != 255) {
											int i_1108_ = (((i_1107_ & 0xff0000) * i_1099_) & ~0xffffff);
											int i_1109_ = ((i_1107_ & 0xff00) * i_1100_ & 0xff0000);
											int i_1110_ = ((i_1107_ & 0xff) * i_1101_ & 0xff00);
											i_1107_ = (i_1108_ | i_1109_ | i_1110_) >>> 8;
											int i_1111_ = is[i_1049_];
											is[i_1049_] = (((((i_1107_ & 0xff00ff) * i_1102_)
													+ ((i_1111_ & 0xff00ff) * i_1103_)) & ~0xff00ff)
													+ ((((i_1107_ & 0xff00) * i_1102_) + ((i_1111_ & 0xff00) * i_1103_))
															& 0xff0000)) >> 8;
											fs[i_1049_] = (float) i_1034_;
										} else {
											int i_1112_ = (((i_1107_ & 0xff0000) * i_1099_) & ~0xffffff);
											int i_1113_ = ((i_1107_ & 0xff00) * i_1100_ & 0xff0000);
											int i_1114_ = ((i_1107_ & 0xff) * i_1101_ & 0xff00);
											is[i_1049_] = (i_1112_ | i_1113_ | i_1114_) >>> 8;
											fs[i_1049_] = (float) i_1034_;
										}
									}
								}
								i_1041_ += i_1045_;
								i_1049_++;
							}
							i_1042_ += i_1046_;
							i_1041_ = i_1091_;
							i_1049_ += i_1050_;
						}
						return;
					}
					return;
				}
				if (i_1037_ == 3) {
					int i_1115_ = i_1041_;
					int i_1116_ = i_1038_ >>> 24;
					int i_1117_ = 256 - i_1116_;
					for (int i_1118_ = -i_1036_; i_1118_ < 0; i_1118_++) {
						int i_1119_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1120_ = -i_1035_; i_1120_ < 0; i_1120_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								int i_1121_ = (anIntArray6693[(i_1041_ >> 16) + i_1119_]);
								int i_1122_ = i_1121_ + i_1038_;
								int i_1123_ = ((i_1121_ & 0xff00ff) + (i_1038_ & 0xff00ff));
								int i_1124_ = ((i_1123_ & 0x1000100) + (i_1122_ - i_1123_ & 0x10000));
								i_1124_ = i_1122_ - i_1124_ | i_1124_ - (i_1124_ >>> 8);
								if (i_1121_ == 0 && i_1116_ != 255) {
									i_1121_ = i_1124_;
									i_1124_ = is[i_1049_];
									i_1124_ = ((((i_1121_ & 0xff00ff) * i_1116_ + (i_1124_ & 0xff00ff) * i_1117_)
											& ~0xff00ff)
											+ (((i_1121_ & 0xff00) * i_1116_ + (i_1124_ & 0xff00) * i_1117_)
													& 0xff0000)) >> 8;
								}
								is[i_1049_] = i_1124_;
								fs[i_1049_] = (float) i_1034_;
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1115_;
						i_1049_ += i_1050_;
					}
					return;
				}
				if (i_1037_ == 2) {
					int i_1125_ = i_1038_ >>> 24;
					int i_1126_ = 256 - i_1125_;
					int i_1127_ = (i_1038_ & 0xff00ff) * i_1126_ & ~0xff00ff;
					int i_1128_ = (i_1038_ & 0xff00) * i_1126_ & 0xff0000;
					i_1038_ = (i_1127_ | i_1128_) >>> 8;
					int i_1129_ = i_1041_;
					for (int i_1130_ = -i_1036_; i_1130_ < 0; i_1130_++) {
						int i_1131_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1132_ = -i_1035_; i_1132_ < 0; i_1132_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								int i_1133_ = (anIntArray6693[(i_1041_ >> 16) + i_1131_]);
								if (i_1133_ != 0) {
									i_1127_ = ((i_1133_ & 0xff00ff) * i_1125_ & ~0xff00ff);
									i_1128_ = ((i_1133_ & 0xff00) * i_1125_ & 0xff0000);
									is[i_1049_] = (((i_1127_ | i_1128_) >>> 8) + i_1038_);
									fs[i_1049_] = (float) i_1034_;
								}
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1129_;
						i_1049_ += i_1050_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			if (i_1039_ == 2) {
				if (i_1037_ == 1) {
					int i_1134_ = i_1041_;
					for (int i_1135_ = -i_1036_; i_1135_ < 0; i_1135_++) {
						int i_1136_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1137_ = -i_1035_; i_1137_ < 0; i_1137_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								int i_1138_ = (anIntArray6693[(i_1041_ >> 16) + i_1136_]);
								if (i_1138_ != 0) {
									int i_1139_ = is[i_1049_];
									int i_1140_ = i_1138_ + i_1139_;
									int i_1141_ = ((i_1138_ & 0xff00ff) + (i_1139_ & 0xff00ff));
									i_1139_ = ((i_1141_ & 0x1000100) + (i_1140_ - i_1141_ & 0x10000));
									is[i_1049_] = (i_1140_ - i_1139_ | i_1139_ - (i_1139_ >>> 8));
									fs[i_1049_] = (float) i_1034_;
								}
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1134_;
						i_1049_ += i_1050_;
					}
					return;
				}
				if (i_1037_ == 0) {
					int i_1142_ = i_1041_;
					int i_1143_ = (i_1038_ & 0xff0000) >> 16;
					int i_1144_ = (i_1038_ & 0xff00) >> 8;
					int i_1145_ = i_1038_ & 0xff;
					for (int i_1146_ = -i_1036_; i_1146_ < 0; i_1146_++) {
						int i_1147_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1148_ = -i_1035_; i_1148_ < 0; i_1148_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								int i_1149_ = (anIntArray6693[(i_1041_ >> 16) + i_1147_]);
								if (i_1149_ != 0) {
									int i_1150_ = ((i_1149_ & 0xff0000) * i_1143_ & ~0xffffff);
									int i_1151_ = ((i_1149_ & 0xff00) * i_1144_ & 0xff0000);
									int i_1152_ = (i_1149_ & 0xff) * i_1145_ & 0xff00;
									i_1149_ = (i_1150_ | i_1151_ | i_1152_) >>> 8;
									int i_1153_ = is[i_1049_];
									int i_1154_ = i_1149_ + i_1153_;
									int i_1155_ = ((i_1149_ & 0xff00ff) + (i_1153_ & 0xff00ff));
									i_1153_ = ((i_1155_ & 0x1000100) + (i_1154_ - i_1155_ & 0x10000));
									is[i_1049_] = (i_1154_ - i_1153_ | i_1153_ - (i_1153_ >>> 8));
									fs[i_1049_] = (float) i_1034_;
								}
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1142_;
						i_1049_ += i_1050_;
					}
					return;
				}
				if (i_1037_ == 3) {
					int i_1156_ = i_1041_;
					for (int i_1157_ = -i_1036_; i_1157_ < 0; i_1157_++) {
						int i_1158_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1159_ = -i_1035_; i_1159_ < 0; i_1159_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								int i_1160_ = (anIntArray6693[(i_1041_ >> 16) + i_1158_]);
								int i_1161_ = i_1160_ + i_1038_;
								int i_1162_ = ((i_1160_ & 0xff00ff) + (i_1038_ & 0xff00ff));
								int i_1163_ = ((i_1162_ & 0x1000100) + (i_1161_ - i_1162_ & 0x10000));
								i_1160_ = i_1161_ - i_1163_ | i_1163_ - (i_1163_ >>> 8);
								i_1163_ = is[i_1049_];
								i_1161_ = i_1160_ + i_1163_;
								i_1162_ = (i_1160_ & 0xff00ff) + (i_1163_ & 0xff00ff);
								i_1163_ = ((i_1162_ & 0x1000100) + (i_1161_ - i_1162_ & 0x10000));
								is[i_1049_] = i_1161_ - i_1163_ | i_1163_ - (i_1163_ >>> 8);
								fs[i_1049_] = (float) i_1034_;
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1156_;
						i_1049_ += i_1050_;
					}
					return;
				}
				if (i_1037_ == 2) {
					int i_1164_ = i_1038_ >>> 24;
					int i_1165_ = 256 - i_1164_;
					int i_1166_ = (i_1038_ & 0xff00ff) * i_1165_ & ~0xff00ff;
					int i_1167_ = (i_1038_ & 0xff00) * i_1165_ & 0xff0000;
					i_1038_ = (i_1166_ | i_1167_) >>> 8;
					int i_1168_ = i_1041_;
					for (int i_1169_ = -i_1036_; i_1169_ < 0; i_1169_++) {
						int i_1170_ = (i_1042_ >> 16) * anInt5758;
						for (int i_1171_ = -i_1035_; i_1171_ < 0; i_1171_++) {
							if ((float) i_1034_ < fs[i_1049_]) {
								int i_1172_ = (anIntArray6693[(i_1041_ >> 16) + i_1170_]);
								if (i_1172_ != 0) {
									i_1166_ = ((i_1172_ & 0xff00ff) * i_1164_ & ~0xff00ff);
									i_1167_ = ((i_1172_ & 0xff00) * i_1164_ & 0xff0000);
									i_1172_ = (((i_1166_ | i_1167_) >>> 8) + i_1038_);
									int i_1173_ = is[i_1049_];
									int i_1174_ = i_1172_ + i_1173_;
									int i_1175_ = ((i_1172_ & 0xff00ff) + (i_1173_ & 0xff00ff));
									i_1173_ = ((i_1175_ & 0x1000100) + (i_1174_ - i_1175_ & 0x10000));
									is[i_1049_] = (i_1174_ - i_1173_ | i_1173_ - (i_1173_ >>> 8));
									fs[i_1049_] = (float) i_1034_;
								}
							}
							i_1041_ += i_1045_;
							i_1049_++;
						}
						i_1042_ += i_1046_;
						i_1041_ = i_1168_;
						i_1049_ += i_1050_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			throw new IllegalArgumentException();
		}
	}

	public void method4102(int[] is, int[] is_1176_, int i, int i_1177_) {
		int[] is_1178_ = aHa_Sub2_5768.anIntArray4108;
		if (Class397_Sub1.anInt5766 == 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_1179_ = Class397_Sub1.anInt5771;
				while (i_1179_ < 0) {
					int i_1180_ = i_1179_ + i_1177_;
					if (i_1180_ >= 0) {
						if (i_1180_ >= is.length)
							break;
						int i_1181_ = Class397_Sub1.anInt5759;
						int i_1182_ = Class397_Sub1.anInt5765;
						int i_1183_ = Class397_Sub1.anInt5775;
						int i_1184_ = Class397_Sub1.anInt5770;
						if (i_1182_ >= 0 && i_1183_ >= 0 && i_1182_ - (anInt5758 << 12) < 0
								&& i_1183_ - (anInt5756 << 12) < 0) {
							int i_1185_ = is[i_1180_] - i;
							int i_1186_ = -is_1176_[i_1180_];
							int i_1187_ = (i_1185_ - (i_1181_ - Class397_Sub1.anInt5759));
							if (i_1187_ > 0) {
								i_1181_ += i_1187_;
								i_1184_ += i_1187_;
								i_1182_ += Class397_Sub1.anInt5766 * i_1187_;
								i_1183_ += Class397_Sub1.anInt5777 * i_1187_;
							} else
								i_1186_ -= i_1187_;
							if (i_1184_ < i_1186_)
								i_1184_ = i_1186_;
							for (/**/; i_1184_ < 0; i_1184_++) {
								int i_1188_ = (anIntArray6693[((i_1183_ >> 12) * anInt5758 + (i_1182_ >> 12))]);
								if (i_1188_ != 0)
									is_1178_[i_1181_++] = i_1188_;
								else
									i_1181_++;
							}
						}
					}
					i_1179_++;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_1189_ = Class397_Sub1.anInt5771;
				while (i_1189_ < 0) {
					int i_1190_ = i_1189_ + i_1177_;
					if (i_1190_ >= 0) {
						if (i_1190_ >= is.length)
							break;
						int i_1191_ = Class397_Sub1.anInt5759;
						int i_1192_ = Class397_Sub1.anInt5765;
						int i_1193_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1194_ = Class397_Sub1.anInt5770;
						if (i_1192_ >= 0 && i_1192_ - (anInt5758 << 12) < 0) {
							int i_1195_;
							if ((i_1195_ = i_1193_ - (anInt5756 << 12)) >= 0) {
								i_1195_ = ((Class397_Sub1.anInt5777 - i_1195_) / Class397_Sub1.anInt5777);
								i_1194_ += i_1195_;
								i_1193_ += Class397_Sub1.anInt5777 * i_1195_;
								i_1191_ += i_1195_;
							}
							if ((i_1195_ = ((i_1193_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_1194_)
								i_1194_ = i_1195_;
							int i_1196_ = is[i_1190_] - i;
							int i_1197_ = -is_1176_[i_1190_];
							int i_1198_ = (i_1196_ - (i_1191_ - Class397_Sub1.anInt5759));
							if (i_1198_ > 0) {
								i_1191_ += i_1198_;
								i_1194_ += i_1198_;
								i_1192_ += Class397_Sub1.anInt5766 * i_1198_;
								i_1193_ += Class397_Sub1.anInt5777 * i_1198_;
							} else
								i_1197_ -= i_1198_;
							if (i_1194_ < i_1197_)
								i_1194_ = i_1197_;
							for (/**/; i_1194_ < 0; i_1194_++) {
								int i_1199_ = (anIntArray6693[((i_1193_ >> 12) * anInt5758 + (i_1192_ >> 12))]);
								if (i_1199_ != 0)
									is_1178_[i_1191_++] = i_1199_;
								else
									i_1191_++;
								i_1193_ += Class397_Sub1.anInt5777;
							}
						}
					}
					i_1189_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_1200_ = Class397_Sub1.anInt5771;
				while (i_1200_ < 0) {
					int i_1201_ = i_1200_ + i_1177_;
					if (i_1201_ >= 0) {
						if (i_1201_ >= is.length)
							break;
						int i_1202_ = Class397_Sub1.anInt5759;
						int i_1203_ = Class397_Sub1.anInt5765;
						int i_1204_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1205_ = Class397_Sub1.anInt5770;
						if (i_1203_ >= 0 && i_1203_ - (anInt5758 << 12) < 0) {
							if (i_1204_ < 0) {
								int i_1206_ = ((Class397_Sub1.anInt5777 - 1 - i_1204_) / Class397_Sub1.anInt5777);
								i_1205_ += i_1206_;
								i_1204_ += Class397_Sub1.anInt5777 * i_1206_;
								i_1202_ += i_1206_;
							}
							int i_1207_;
							if ((i_1207_ = ((i_1204_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
									/ Class397_Sub1.anInt5777)) > i_1205_)
								i_1205_ = i_1207_;
							int i_1208_ = is[i_1201_] - i;
							int i_1209_ = -is_1176_[i_1201_];
							int i_1210_ = (i_1208_ - (i_1202_ - Class397_Sub1.anInt5759));
							if (i_1210_ > 0) {
								i_1202_ += i_1210_;
								i_1205_ += i_1210_;
								i_1203_ += Class397_Sub1.anInt5766 * i_1210_;
								i_1204_ += Class397_Sub1.anInt5777 * i_1210_;
							} else
								i_1209_ -= i_1210_;
							if (i_1205_ < i_1209_)
								i_1205_ = i_1209_;
							for (/**/; i_1205_ < 0; i_1205_++) {
								int i_1211_ = (anIntArray6693[((i_1204_ >> 12) * anInt5758 + (i_1203_ >> 12))]);
								if (i_1211_ != 0)
									is_1178_[i_1202_++] = i_1211_;
								else
									i_1202_++;
								i_1204_ += Class397_Sub1.anInt5777;
							}
						}
					}
					i_1200_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5766 < 0) {
			if (Class397_Sub1.anInt5777 == 0) {
				int i_1212_ = Class397_Sub1.anInt5771;
				while (i_1212_ < 0) {
					int i_1213_ = i_1212_ + i_1177_;
					if (i_1213_ >= 0) {
						if (i_1213_ >= is.length)
							break;
						int i_1214_ = Class397_Sub1.anInt5759;
						int i_1215_ = (Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764);
						int i_1216_ = Class397_Sub1.anInt5775;
						int i_1217_ = Class397_Sub1.anInt5770;
						if (i_1216_ >= 0 && i_1216_ - (anInt5756 << 12) < 0) {
							int i_1218_;
							if ((i_1218_ = i_1215_ - (anInt5758 << 12)) >= 0) {
								i_1218_ = ((Class397_Sub1.anInt5766 - i_1218_) / Class397_Sub1.anInt5766);
								i_1217_ += i_1218_;
								i_1215_ += Class397_Sub1.anInt5766 * i_1218_;
								i_1214_ += i_1218_;
							}
							if ((i_1218_ = ((i_1215_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_1217_)
								i_1217_ = i_1218_;
							int i_1219_ = is[i_1213_] - i;
							int i_1220_ = -is_1176_[i_1213_];
							int i_1221_ = (i_1219_ - (i_1214_ - Class397_Sub1.anInt5759));
							if (i_1221_ > 0) {
								i_1214_ += i_1221_;
								i_1217_ += i_1221_;
								i_1215_ += Class397_Sub1.anInt5766 * i_1221_;
								i_1216_ += Class397_Sub1.anInt5777 * i_1221_;
							} else
								i_1220_ -= i_1221_;
							if (i_1217_ < i_1220_)
								i_1217_ = i_1220_;
							for (/**/; i_1217_ < 0; i_1217_++) {
								int i_1222_ = (anIntArray6693[((i_1216_ >> 12) * anInt5758 + (i_1215_ >> 12))]);
								if (i_1222_ != 0)
									is_1178_[i_1214_++] = i_1222_;
								else
									i_1214_++;
								i_1215_ += Class397_Sub1.anInt5766;
							}
						}
					}
					i_1212_++;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else if (Class397_Sub1.anInt5777 < 0) {
				int i_1223_ = Class397_Sub1.anInt5771;
				while (i_1223_ < 0) {
					int i_1224_ = i_1223_ + i_1177_;
					if (i_1224_ >= 0) {
						if (i_1224_ >= is.length)
							break;
						int i_1225_ = Class397_Sub1.anInt5759;
						int i_1226_ = (Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764);
						int i_1227_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1228_ = Class397_Sub1.anInt5770;
						int i_1229_;
						if ((i_1229_ = i_1226_ - (anInt5758 << 12)) >= 0) {
							i_1229_ = ((Class397_Sub1.anInt5766 - i_1229_) / Class397_Sub1.anInt5766);
							i_1228_ += i_1229_;
							i_1226_ += Class397_Sub1.anInt5766 * i_1229_;
							i_1227_ += Class397_Sub1.anInt5777 * i_1229_;
							i_1225_ += i_1229_;
						}
						if ((i_1229_ = ((i_1226_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_1228_)
							i_1228_ = i_1229_;
						if ((i_1229_ = i_1227_ - (anInt5756 << 12)) >= 0) {
							i_1229_ = ((Class397_Sub1.anInt5777 - i_1229_) / Class397_Sub1.anInt5777);
							i_1228_ += i_1229_;
							i_1226_ += Class397_Sub1.anInt5766 * i_1229_;
							i_1227_ += Class397_Sub1.anInt5777 * i_1229_;
							i_1225_ += i_1229_;
						}
						if ((i_1229_ = ((i_1227_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_1228_)
							i_1228_ = i_1229_;
						int i_1230_ = is[i_1224_] - i;
						int i_1231_ = -is_1176_[i_1224_];
						int i_1232_ = i_1230_ - (i_1225_ - Class397_Sub1.anInt5759);
						if (i_1232_ > 0) {
							i_1225_ += i_1232_;
							i_1228_ += i_1232_;
							i_1226_ += Class397_Sub1.anInt5766 * i_1232_;
							i_1227_ += Class397_Sub1.anInt5777 * i_1232_;
						} else
							i_1231_ -= i_1232_;
						if (i_1228_ < i_1231_)
							i_1228_ = i_1231_;
						for (/**/; i_1228_ < 0; i_1228_++) {
							int i_1233_ = (anIntArray6693[(i_1227_ >> 12) * anInt5758 + (i_1226_ >> 12)]);
							if (i_1233_ != 0)
								is_1178_[i_1225_++] = i_1233_;
							else
								i_1225_++;
							i_1226_ += Class397_Sub1.anInt5766;
							i_1227_ += Class397_Sub1.anInt5777;
						}
					}
					i_1223_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			} else {
				int i_1234_ = Class397_Sub1.anInt5771;
				while (i_1234_ < 0) {
					int i_1235_ = i_1234_ + i_1177_;
					if (i_1235_ >= 0) {
						if (i_1235_ >= is.length)
							break;
						int i_1236_ = Class397_Sub1.anInt5759;
						int i_1237_ = (Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764);
						int i_1238_ = (Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769);
						int i_1239_ = Class397_Sub1.anInt5770;
						int i_1240_;
						if ((i_1240_ = i_1237_ - (anInt5758 << 12)) >= 0) {
							i_1240_ = ((Class397_Sub1.anInt5766 - i_1240_) / Class397_Sub1.anInt5766);
							i_1239_ += i_1240_;
							i_1237_ += Class397_Sub1.anInt5766 * i_1240_;
							i_1238_ += Class397_Sub1.anInt5777 * i_1240_;
							i_1236_ += i_1240_;
						}
						if ((i_1240_ = ((i_1237_ - Class397_Sub1.anInt5766) / Class397_Sub1.anInt5766)) > i_1239_)
							i_1239_ = i_1240_;
						if (i_1238_ < 0) {
							i_1240_ = ((Class397_Sub1.anInt5777 - 1 - i_1238_) / Class397_Sub1.anInt5777);
							i_1239_ += i_1240_;
							i_1237_ += Class397_Sub1.anInt5766 * i_1240_;
							i_1238_ += Class397_Sub1.anInt5777 * i_1240_;
							i_1236_ += i_1240_;
						}
						if ((i_1240_ = ((i_1238_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
								/ Class397_Sub1.anInt5777)) > i_1239_)
							i_1239_ = i_1240_;
						int i_1241_ = is[i_1235_] - i;
						int i_1242_ = -is_1176_[i_1235_];
						int i_1243_ = i_1241_ - (i_1236_ - Class397_Sub1.anInt5759);
						if (i_1243_ > 0) {
							i_1236_ += i_1243_;
							i_1239_ += i_1243_;
							i_1237_ += Class397_Sub1.anInt5766 * i_1243_;
							i_1238_ += Class397_Sub1.anInt5777 * i_1243_;
						} else
							i_1242_ -= i_1243_;
						if (i_1239_ < i_1242_)
							i_1239_ = i_1242_;
						for (/**/; i_1239_ < 0; i_1239_++) {
							int i_1244_ = (anIntArray6693[(i_1238_ >> 12) * anInt5758 + (i_1237_ >> 12)]);
							if (i_1244_ != 0)
								is_1178_[i_1236_++] = i_1244_;
							else
								i_1236_++;
							i_1237_ += Class397_Sub1.anInt5766;
							i_1238_ += Class397_Sub1.anInt5777;
						}
					}
					i_1234_++;
					Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
					Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
					Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
				}
			}
		} else if (Class397_Sub1.anInt5777 == 0) {
			int i_1245_ = Class397_Sub1.anInt5771;
			while (i_1245_ < 0) {
				int i_1246_ = i_1245_ + i_1177_;
				if (i_1246_ >= 0) {
					if (i_1246_ >= is.length)
						break;
					int i_1247_ = Class397_Sub1.anInt5759;
					int i_1248_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_1249_ = Class397_Sub1.anInt5775;
					int i_1250_ = Class397_Sub1.anInt5770;
					if (i_1249_ >= 0 && i_1249_ - (anInt5756 << 12) < 0) {
						if (i_1248_ < 0) {
							int i_1251_ = ((Class397_Sub1.anInt5766 - 1 - i_1248_) / Class397_Sub1.anInt5766);
							i_1250_ += i_1251_;
							i_1248_ += Class397_Sub1.anInt5766 * i_1251_;
							i_1247_ += i_1251_;
						}
						int i_1252_;
						if ((i_1252_ = ((i_1248_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
								/ Class397_Sub1.anInt5766)) > i_1250_)
							i_1250_ = i_1252_;
						int i_1253_ = is[i_1246_] - i;
						int i_1254_ = -is_1176_[i_1246_];
						int i_1255_ = i_1253_ - (i_1247_ - Class397_Sub1.anInt5759);
						if (i_1255_ > 0) {
							i_1247_ += i_1255_;
							i_1250_ += i_1255_;
							i_1248_ += Class397_Sub1.anInt5766 * i_1255_;
							i_1249_ += Class397_Sub1.anInt5777 * i_1255_;
						} else
							i_1254_ -= i_1255_;
						if (i_1250_ < i_1254_)
							i_1250_ = i_1254_;
						for (/**/; i_1250_ < 0; i_1250_++) {
							int i_1256_ = (anIntArray6693[(i_1249_ >> 12) * anInt5758 + (i_1248_ >> 12)]);
							if (i_1256_ != 0)
								is_1178_[i_1247_++] = i_1256_;
							else
								i_1247_++;
							i_1248_ += Class397_Sub1.anInt5766;
						}
					}
				}
				i_1245_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else if (Class397_Sub1.anInt5777 < 0) {
			int i_1257_ = Class397_Sub1.anInt5771;
			while (i_1257_ < 0) {
				int i_1258_ = i_1257_ + i_1177_;
				if (i_1258_ >= 0) {
					if (i_1258_ >= is.length)
						break;
					int i_1259_ = Class397_Sub1.anInt5759;
					int i_1260_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_1261_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_1262_ = Class397_Sub1.anInt5770;
					if (i_1260_ < 0) {
						int i_1263_ = ((Class397_Sub1.anInt5766 - 1 - i_1260_) / Class397_Sub1.anInt5766);
						i_1262_ += i_1263_;
						i_1260_ += Class397_Sub1.anInt5766 * i_1263_;
						i_1261_ += Class397_Sub1.anInt5777 * i_1263_;
						i_1259_ += i_1263_;
					}
					int i_1264_;
					if ((i_1264_ = ((i_1260_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
							/ Class397_Sub1.anInt5766)) > i_1262_)
						i_1262_ = i_1264_;
					if ((i_1264_ = i_1261_ - (anInt5756 << 12)) >= 0) {
						i_1264_ = ((Class397_Sub1.anInt5777 - i_1264_) / Class397_Sub1.anInt5777);
						i_1262_ += i_1264_;
						i_1260_ += Class397_Sub1.anInt5766 * i_1264_;
						i_1261_ += Class397_Sub1.anInt5777 * i_1264_;
						i_1259_ += i_1264_;
					}
					if ((i_1264_ = ((i_1261_ - Class397_Sub1.anInt5777) / Class397_Sub1.anInt5777)) > i_1262_)
						i_1262_ = i_1264_;
					int i_1265_ = is[i_1258_] - i;
					int i_1266_ = -is_1176_[i_1258_];
					int i_1267_ = i_1265_ - (i_1259_ - Class397_Sub1.anInt5759);
					if (i_1267_ > 0) {
						i_1259_ += i_1267_;
						i_1262_ += i_1267_;
						i_1260_ += Class397_Sub1.anInt5766 * i_1267_;
						i_1261_ += Class397_Sub1.anInt5777 * i_1267_;
					} else
						i_1266_ -= i_1267_;
					if (i_1262_ < i_1266_)
						i_1262_ = i_1266_;
					for (/**/; i_1262_ < 0; i_1262_++) {
						int i_1268_ = (anIntArray6693[(i_1261_ >> 12) * anInt5758 + (i_1260_ >> 12)]);
						if (i_1268_ != 0)
							is_1178_[i_1259_++] = i_1268_;
						else
							i_1259_++;
						i_1260_ += Class397_Sub1.anInt5766;
						i_1261_ += Class397_Sub1.anInt5777;
					}
				}
				i_1257_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		} else {
			int i_1269_ = Class397_Sub1.anInt5771;
			while (i_1269_ < 0) {
				int i_1270_ = i_1269_ + i_1177_;
				if (i_1270_ >= 0) {
					if (i_1270_ >= is.length)
						break;
					int i_1271_ = Class397_Sub1.anInt5759;
					int i_1272_ = Class397_Sub1.anInt5765 + Class397_Sub1.anInt5764;
					int i_1273_ = Class397_Sub1.anInt5775 + Class397_Sub1.anInt5769;
					int i_1274_ = Class397_Sub1.anInt5770;
					if (i_1272_ < 0) {
						int i_1275_ = ((Class397_Sub1.anInt5766 - 1 - i_1272_) / Class397_Sub1.anInt5766);
						i_1274_ += i_1275_;
						i_1272_ += Class397_Sub1.anInt5766 * i_1275_;
						i_1273_ += Class397_Sub1.anInt5777 * i_1275_;
						i_1271_ += i_1275_;
					}
					int i_1276_;
					if ((i_1276_ = ((i_1272_ + 1 - (anInt5758 << 12) - Class397_Sub1.anInt5766)
							/ Class397_Sub1.anInt5766)) > i_1274_)
						i_1274_ = i_1276_;
					if (i_1273_ < 0) {
						i_1276_ = ((Class397_Sub1.anInt5777 - 1 - i_1273_) / Class397_Sub1.anInt5777);
						i_1274_ += i_1276_;
						i_1272_ += Class397_Sub1.anInt5766 * i_1276_;
						i_1273_ += Class397_Sub1.anInt5777 * i_1276_;
						i_1271_ += i_1276_;
					}
					if ((i_1276_ = ((i_1273_ + 1 - (anInt5756 << 12) - Class397_Sub1.anInt5777)
							/ Class397_Sub1.anInt5777)) > i_1274_)
						i_1274_ = i_1276_;
					int i_1277_ = is[i_1270_] - i;
					int i_1278_ = -is_1176_[i_1270_];
					int i_1279_ = i_1277_ - (i_1271_ - Class397_Sub1.anInt5759);
					if (i_1279_ > 0) {
						i_1271_ += i_1279_;
						i_1274_ += i_1279_;
						i_1272_ += Class397_Sub1.anInt5766 * i_1279_;
						i_1273_ += Class397_Sub1.anInt5777 * i_1279_;
					} else
						i_1278_ -= i_1279_;
					if (i_1274_ < i_1278_)
						i_1274_ = i_1278_;
					for (/**/; i_1274_ < 0; i_1274_++) {
						int i_1280_ = (anIntArray6693[(i_1273_ >> 12) * anInt5758 + (i_1272_ >> 12)]);
						if (i_1280_ != 0)
							is_1178_[i_1271_++] = i_1280_;
						else
							i_1271_++;
						i_1272_ += Class397_Sub1.anInt5766;
						i_1273_ += Class397_Sub1.anInt5777;
					}
				}
				i_1269_++;
				Class397_Sub1.anInt5765 += Class397_Sub1.anInt5751;
				Class397_Sub1.anInt5775 += Class397_Sub1.anInt5745;
				Class397_Sub1.anInt5759 += Class397_Sub1.anInt5776;
			}
		}
	}
}
