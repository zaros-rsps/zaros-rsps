package net.zaros.client;

/* Class366 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class366 {
	ha_Sub3 aHa_Sub3_3121;
	static int anInt3122;

	abstract boolean method3763(int i);

	abstract void method3764(boolean bool, int i, Class69 class69);

	static final float method3765(int i, float f, float f_0_, float f_1_) {
		if (i != 11404)
			method3765(-3, 0.15601666F, -0.39610994F, -0.9664367F);
		return f_0_ * (-f + f_1_) + f;
	}

	abstract void method3766(int i);

	static final void method3767(int i, byte i_2_, int i_3_, int i_4_, int i_5_, int i_6_) {
		if (i_2_ > 78) {
			if (i_6_ == i_4_)
				Class69_Sub1_Sub1.method734(i_3_, i_5_, 1, i, i_6_);
			else if (ConfigurationDefinition.anInt676 <= -i_6_ + i && Class288.anInt2652 >= i_6_ + i && -i_4_ + i_3_ >= EmissiveTriangle.anInt952 && i_3_ + i_4_ <= RuntimeException_Sub1.anInt3391)
				Class296_Sub45_Sub2.method2983(i, 102, i_3_, i_5_, i_4_, i_6_);
			else
				StaticMethods.method2472(i_5_, 11012, i, i_3_, i_4_, i_6_);
		}
	}

	Class366(ha_Sub3 var_ha_Sub3) {
		aHa_Sub3_3121 = var_ha_Sub3;
	}

	abstract void method3768(byte i, boolean bool);

	abstract void method3769(int i, byte i_7_, int i_8_);

	abstract void method3770(byte i, boolean bool);
}
