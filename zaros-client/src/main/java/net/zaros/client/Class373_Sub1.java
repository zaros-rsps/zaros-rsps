package net.zaros.client;

/* Class373_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class373_Sub1 extends Class373 {
	float aFloat5576;
	float aFloat5577;
	static OutgoingPacket aClass311_5578;
	float aFloat5579;
	static int[] anIntArray5580 = {-1, -1, 1, 1};
	float aFloat5581;
	float aFloat5582;
	float aFloat5583;
	float aFloat5584;
	float aFloat5585;
	float aFloat5586;
	float aFloat5587;
	float aFloat5588;
	float aFloat5589;

	public final void method3902(int i, int i_0_, int i_1_) {
		aFloat5576 = (float) i;
		aFloat5577 = aFloat5583 = aFloat5589 = 1.0F;
		aFloat5584 = aFloat5587 = aFloat5585 = aFloat5579 = aFloat5581 = aFloat5586 = 0.0F;
		aFloat5588 = (float) i_1_;
		aFloat5582 = (float) i_0_;
	}

	final void method3918(int i) {
		aFloat5583 = -aFloat5583;
		aFloat5582 = -aFloat5582;
		aFloat5587 = -aFloat5587;
		aFloat5584 = -aFloat5584;
		aFloat5586 = -aFloat5586;
		if (i == 3) {
			aFloat5579 = -aFloat5579;
			aFloat5589 = -aFloat5589;
			aFloat5588 = -aFloat5588;
		}
	}

	public final void method3914(int i) {
		float f = ha.aFloatArray1298[i & 0x3fff];
		float f_2_ = ha.aFloatArray1297[i & 0x3fff];
		float f_3_ = aFloat5584;
		float f_4_ = aFloat5583;
		float f_5_ = aFloat5586;
		float f_6_ = aFloat5582;
		aFloat5584 = f * f_3_ - f_2_ * aFloat5587;
		aFloat5587 = f_3_ * f_2_ + aFloat5587 * f;
		aFloat5583 = f * f_4_ - f_2_ * aFloat5579;
		aFloat5586 = f_5_ * f - f_2_ * aFloat5589;
		aFloat5579 = f_2_ * f_4_ + aFloat5579 * f;
		aFloat5582 = f * f_6_ - f_2_ * aFloat5588;
		aFloat5589 = f_2_ * f_5_ + aFloat5589 * f;
		aFloat5588 = f_2_ * f_6_ + aFloat5588 * f;
	}

	public final void method3906(int i) {
		aFloat5583 = 1.0F;
		aFloat5577 = aFloat5589 = ha.aFloatArray1298[i & 0x3fff];
		aFloat5581 = ha.aFloatArray1297[i & 0x3fff];
		aFloat5587 = -aFloat5581;
		aFloat5585 = aFloat5576 = aFloat5584 = aFloat5586 = aFloat5582 = aFloat5579 = aFloat5588 = 0.0F;
	}

	public final void method3917(int i) {
		float f = ha.aFloatArray1298[i & 0x3fff];
		float f_7_ = ha.aFloatArray1297[i & 0x3fff];
		float f_8_ = aFloat5577;
		float f_9_ = aFloat5585;
		float f_10_ = aFloat5581;
		aFloat5577 = f_7_ * aFloat5587 + f_8_ * f;
		float f_11_ = aFloat5576;
		aFloat5585 = aFloat5579 * f_7_ + f_9_ * f;
		aFloat5587 = f * aFloat5587 - f_8_ * f_7_;
		aFloat5581 = f_10_ * f + f_7_ * aFloat5589;
		aFloat5579 = f * aFloat5579 - f_9_ * f_7_;
		aFloat5576 = f_11_ * f + f_7_ * aFloat5588;
		aFloat5589 = -(f_10_ * f_7_) + f * aFloat5589;
		aFloat5588 = f * aFloat5588 - f_11_ * f_7_;
	}

	public final void method3905(int i, int i_12_, int i_13_, int[] is) {
		i -= aFloat5576;
		i_13_ -= aFloat5588;
		i_12_ -= aFloat5582;
		is[1] = (int) ((float) i_12_ * aFloat5583 + (float) i * aFloat5585 + aFloat5579 * (float) i_13_);
		is[0] = (int) (aFloat5587 * (float) i_13_ + (aFloat5584 * (float) i_12_ + (float) i * aFloat5577));
		is[2] = (int) (aFloat5581 * (float) i + (float) i_12_ * aFloat5586 + aFloat5589 * (float) i_13_);
	}

	public final void method3910() {
		aFloat5577 = aFloat5583 = aFloat5589 = 1.0F;
		aFloat5584 = aFloat5587 = aFloat5585 = aFloat5579 = aFloat5581 = aFloat5586 = aFloat5576 = aFloat5582 = aFloat5588 = 0.0F;
	}

	final float[] method3919(int i) {
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[5] = aFloat5583;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[14] = 0.0F;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[10] = aFloat5589;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[13] = 0.0F;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[9] = aFloat5586;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[12] = 0.0F;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[2] = aFloat5587;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[4] = aFloat5585;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[0] = aFloat5577;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[8] = aFloat5581;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[6] = aFloat5579;
		int i_14_ = 37 % ((-18 - i) / 33);
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[1] = aFloat5584;
		return Class296_Sub39_Sub20_Sub1.aFloatArray6725;
	}

	public final void method3915(Class373 class373) {
		Class373_Sub1 class373_sub1_15_ = (Class373_Sub1) class373;
		aFloat5579 = class373_sub1_15_.aFloat5579;
		aFloat5581 = class373_sub1_15_.aFloat5581;
		aFloat5586 = class373_sub1_15_.aFloat5586;
		aFloat5589 = class373_sub1_15_.aFloat5589;
		aFloat5585 = class373_sub1_15_.aFloat5585;
		aFloat5584 = class373_sub1_15_.aFloat5584;
		aFloat5582 = class373_sub1_15_.aFloat5582;
		aFloat5587 = class373_sub1_15_.aFloat5587;
		aFloat5583 = class373_sub1_15_.aFloat5583;
		aFloat5577 = class373_sub1_15_.aFloat5577;
		aFloat5588 = class373_sub1_15_.aFloat5588;
		aFloat5576 = class373_sub1_15_.aFloat5576;
	}

	public final void method3904(int i, int i_16_, int i_17_) {
		aFloat5588 += (float) i_17_;
		aFloat5582 += (float) i_16_;
		aFloat5576 += (float) i;
	}

	public final void method3901(int i, int i_18_, int i_19_, int[] is) {
		is[1] = (int) ((float) i_18_ * aFloat5583 + (float) i * aFloat5584 + (float) i_19_ * aFloat5586);
		is[0] = (int) (aFloat5577 * (float) i + (float) i_18_ * aFloat5585 + aFloat5581 * (float) i_19_);
		is[2] = (int) ((float) i_19_ * aFloat5589 + (aFloat5579 * (float) i_18_ + aFloat5587 * (float) i));
	}

	public static void method3920(int i) {
		aClass311_5578 = null;
		anIntArray5580 = null;
		if (i != 9070)
			method3920(111);
	}

	public final void method3900(int i) {
		float f = ha.aFloatArray1298[i & 0x3fff];
		float f_20_ = ha.aFloatArray1297[i & 0x3fff];
		float f_21_ = aFloat5577;
		float f_22_ = aFloat5585;
		float f_23_ = aFloat5581;
		float f_24_ = aFloat5576;
		aFloat5577 = f_21_ * f - f_20_ * aFloat5584;
		aFloat5584 = aFloat5584 * f + f_21_ * f_20_;
		aFloat5585 = f_22_ * f - aFloat5583 * f_20_;
		aFloat5583 = f_20_ * f_22_ + aFloat5583 * f;
		aFloat5581 = f * f_23_ - aFloat5586 * f_20_;
		aFloat5586 = f * aFloat5586 + f_23_ * f_20_;
		aFloat5576 = -(f_20_ * aFloat5582) + f * f_24_;
		aFloat5582 = f_20_ * f_24_ + aFloat5582 * f;
	}

	public final Class373 method3916() {
		Class373_Sub1 class373_sub1_25_ = new Class373_Sub1();
		class373_sub1_25_.aFloat5586 = aFloat5586;
		class373_sub1_25_.aFloat5582 = aFloat5582;
		class373_sub1_25_.aFloat5584 = aFloat5584;
		class373_sub1_25_.aFloat5576 = aFloat5576;
		class373_sub1_25_.aFloat5581 = aFloat5581;
		class373_sub1_25_.aFloat5588 = aFloat5588;
		class373_sub1_25_.aFloat5583 = aFloat5583;
		class373_sub1_25_.aFloat5577 = aFloat5577;
		class373_sub1_25_.aFloat5589 = aFloat5589;
		class373_sub1_25_.aFloat5585 = aFloat5585;
		class373_sub1_25_.aFloat5579 = aFloat5579;
		class373_sub1_25_.aFloat5587 = aFloat5587;
		return class373_sub1_25_;
	}

	public final void method3909(int i) {
		aFloat5577 = 1.0F;
		aFloat5583 = aFloat5589 = ha.aFloatArray1298[i & 0x3fff];
		aFloat5579 = ha.aFloatArray1297[i & 0x3fff];
		aFloat5585 = aFloat5581 = aFloat5576 = aFloat5584 = aFloat5582 = aFloat5587 = aFloat5588 = 0.0F;
		aFloat5586 = -aFloat5579;
	}

	final void method3921(float f, float f_26_, boolean bool, float[] fs, float f_27_, float f_28_) {
		if (bool)
			aFloat5583 = 1.0372007F;
		fs[1] = aFloat5586 * f_28_ + (aFloat5584 * f + f_27_ * aFloat5583);
		fs[0] = aFloat5581 * f_28_ + (f * aFloat5577 + aFloat5585 * f_27_);
		fs[2] = f_28_ * aFloat5589 + (aFloat5579 * f_27_ + aFloat5587 * f);
		float f_29_;
		float f_30_;
		float f_31_;
		if (!(f > 0.00390625F) && !(f < -0.00390625F)) {
			if (f_27_ > 0.00390625F || f_27_ < -0.00390625F) {
				float f_32_ = -f_26_ / f_27_;
				f_29_ = aFloat5576 + aFloat5585 * f_32_;
				f_30_ = f_32_ * aFloat5579 + aFloat5588;
				f_31_ = f_32_ * aFloat5583 + aFloat5582;
			} else {
				float f_33_ = -f_26_ / f_28_;
				f_29_ = f_33_ * aFloat5581 + aFloat5576;
				f_30_ = f_33_ * aFloat5589 + aFloat5588;
				f_31_ = aFloat5586 * f_33_ + aFloat5582;
			}
		} else {
			float f_34_ = -f_26_ / f;
			f_29_ = f_34_ * aFloat5577 + aFloat5576;
			f_30_ = aFloat5588 + f_34_ * aFloat5587;
			f_31_ = f_34_ * aFloat5584 + aFloat5582;
		}
		fs[3] = -(f_30_ * fs[2] + (f_29_ * fs[0] + fs[1] * f_31_));
	}

	final void method3922(float f, float f_35_, int i, int i_36_, float f_37_, int i_38_, int i_39_) {
		if (i_39_ == 0) {
			aFloat5583 = (float) i_36_;
			aFloat5577 = (float) i_38_;
			aFloat5584 = aFloat5587 = aFloat5585 = aFloat5579 = aFloat5581 = aFloat5586 = 0.0F;
			aFloat5589 = 1.0F;
		} else {
			float f_40_ = ha.aFloatArray1298[i_39_ & 0x3fff];
			float f_41_ = ha.aFloatArray1297[i_39_ & 0x3fff];
			aFloat5585 = (float) i_36_ * -f_41_;
			aFloat5584 = f_41_ * (float) i_38_;
			aFloat5583 = f_40_ * (float) i_36_;
			aFloat5577 = f_40_ * (float) i_38_;
			aFloat5589 = 1.0F;
			aFloat5587 = aFloat5579 = aFloat5581 = aFloat5586 = 0.0F;
		}
		if (i != 6173)
			aFloat5582 = -1.5684538F;
		aFloat5576 = f_37_;
		aFloat5588 = f;
		aFloat5582 = f_35_;
	}

	public final void method3911(int i) {
		aFloat5589 = 1.0F;
		aFloat5577 = aFloat5583 = ha.aFloatArray1298[i & 0x3fff];
		aFloat5584 = ha.aFloatArray1297[i & 0x3fff];
		aFloat5585 = -aFloat5584;
		aFloat5581 = aFloat5576 = aFloat5586 = aFloat5582 = aFloat5587 = aFloat5579 = aFloat5588 = 0.0F;
	}

	final float[] method3923(boolean bool) {
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[5] = aFloat5583;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[14] = aFloat5588;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[1] = aFloat5584;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[6] = aFloat5579;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[8] = aFloat5581;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[4] = aFloat5585;
		if (bool != true)
			method3918(-61);
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[12] = aFloat5576;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[9] = aFloat5586;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[0] = aFloat5577;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[2] = aFloat5587;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[10] = aFloat5589;
		Class296_Sub39_Sub20_Sub1.aFloatArray6725[13] = aFloat5582;
		return Class296_Sub39_Sub20_Sub1.aFloatArray6725;
	}

	public final void method3903(int i, int i_42_, int i_43_, int[] is) {
		is[1] = (int) (aFloat5582 + (aFloat5583 * (float) i_42_ + (float) i * aFloat5584 + aFloat5586 * (float) i_43_));
		is[2] = (int) (aFloat5588 + (aFloat5587 * (float) i + aFloat5579 * (float) i_42_ + aFloat5589 * (float) i_43_));
		is[0] = (int) (aFloat5576 + ((float) i_43_ * aFloat5581 + ((float) i * aFloat5577 + aFloat5585 * (float) i_42_)));
	}

	final void method3924(byte i, Class373 class373) {
		Class373_Sub1 class373_sub1_44_ = (Class373_Sub1) class373;
		aFloat5577 = class373_sub1_44_.aFloat5577;
		aFloat5585 = class373_sub1_44_.aFloat5584;
		aFloat5581 = class373_sub1_44_.aFloat5587;
		aFloat5587 = class373_sub1_44_.aFloat5581;
		aFloat5583 = class373_sub1_44_.aFloat5583;
		aFloat5586 = class373_sub1_44_.aFloat5579;
		aFloat5584 = class373_sub1_44_.aFloat5585;
		aFloat5589 = class373_sub1_44_.aFloat5589;
		int i_45_ = -84 / ((44 - i) / 34);
		aFloat5579 = class373_sub1_44_.aFloat5586;
		aFloat5576 = -(aFloat5581 * class373_sub1_44_.aFloat5588 + (class373_sub1_44_.aFloat5576 * aFloat5577 + class373_sub1_44_.aFloat5582 * aFloat5585));
		aFloat5582 = -(aFloat5586 * class373_sub1_44_.aFloat5588 + (class373_sub1_44_.aFloat5576 * aFloat5584 + aFloat5583 * class373_sub1_44_.aFloat5582));
		aFloat5588 = -(aFloat5579 * class373_sub1_44_.aFloat5582 + class373_sub1_44_.aFloat5576 * aFloat5587 + class373_sub1_44_.aFloat5588 * aFloat5589);
	}

	public final void method3908(int[] is) {
		float f = -aFloat5576 + (float) is[0];
		float f_46_ = (float) is[1] - aFloat5582;
		float f_47_ = -aFloat5588 + (float) is[2];
		is[2] = (int) (f * aFloat5581 + f_46_ * aFloat5586 + f_47_ * aFloat5589);
		is[0] = (int) (f_46_ * aFloat5584 + aFloat5577 * f + aFloat5587 * f_47_);
		is[1] = (int) (f_46_ * aFloat5583 + f * aFloat5585 + aFloat5579 * f_47_);
	}

	public final void method3907(int i, int i_48_, int i_49_, int i_50_, int i_51_, int i_52_) {
		float f = ha.aFloatArray1298[i_50_ & 0x3fff];
		float f_53_ = ha.aFloatArray1297[i_50_ & 0x3fff];
		float f_54_ = ha.aFloatArray1298[i_51_ & 0x3fff];
		float f_55_ = ha.aFloatArray1297[i_51_ & 0x3fff];
		float f_56_ = ha.aFloatArray1298[i_52_ & 0x3fff];
		float f_57_ = ha.aFloatArray1297[i_52_ & 0x3fff];
		float f_58_ = f_56_ * f_53_;
		float f_59_ = f_53_ * f_57_;
		aFloat5583 = f * f_56_;
		aFloat5586 = f_54_ * f_58_ + f_55_ * f_57_;
		aFloat5579 = -f_53_;
		aFloat5584 = -f_54_ * f_57_ + f_58_ * f_55_;
		aFloat5585 = f_57_ * f;
		aFloat5589 = f_54_ * f;
		aFloat5581 = -f_55_ * f_56_ + f_59_ * f_54_;
		aFloat5577 = f_59_ * f_55_ + f_54_ * f_56_;
		aFloat5587 = f_55_ * f;
		aFloat5582 = -(aFloat5586 * (float) i_49_) + (aFloat5584 * (float) -i - (float) i_48_ * aFloat5583);
		aFloat5576 = (-((float) i_48_ * aFloat5585) + (float) -i * aFloat5577 - aFloat5581 * (float) i_49_);
		aFloat5588 = -((float) i_49_ * aFloat5589) + ((float) -i * aFloat5587 - aFloat5579 * (float) i_48_);
	}

	static final void method3925(int i, int i_60_, int i_61_, int i_62_) {
		i <<= 3;
		i_61_ <<= 3;
		i_60_ <<= 3;
		Class41_Sub26.aFloat3806 = (float) i;
		if (i_62_ != -30302)
			anIntArray5580 = null;
		if (Class361.anInt3103 == 2) {
			Class44_Sub1.camRotY = i;
			Class296_Sub17_Sub2.camRotX = i_61_;
			Class268.anInt2488 = i_60_;
		}
		Class153.aFloat1572 = (float) i_61_;
		Class123_Sub2_Sub1.method1074(i_62_ + 30298);
		Mesh.aBoolean1359 = true;
	}

	public Class373_Sub1() {
		method3910();
	}

	static {
		aClass311_5578 = new OutgoingPacket(41, 12);
	}
}
