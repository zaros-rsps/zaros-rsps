package net.zaros.client;
import jaggl.OpenGL;

final class Class366_Sub8 extends Class366 {
	static NodeDeque aClass155_5409;
	static IncomingPacket aClass231_5410 = new IncomingPacket(102, 5);
	static IncomingPacket aClass231_5411 = new IncomingPacket(13, -2);
	static d aD5412;
	private boolean aBoolean5413 = false;
	private Class341 aClass341_5414;

	final void method3769(int i, byte i_0_, int i_1_) {
		if (i_0_ != -81)
			method3763(-126);
	}

	final void method3770(byte i, boolean bool) {
		if (i == 33)
			aHa_Sub3_3121.method1306(8448, 7681, -22394);
	}

	static final void method3794(boolean bool) {
		if (bool != true)
			aClass155_5409 = null;
		for (int i = 0; i < 100; i++)
			Class360_Sub9.aBooleanArray5345[i] = true;
	}

	final boolean method3763(int i) {
		int i_2_ = 94 / ((i - 73) / 40);
		return true;
	}

	Class366_Sub8(ha_Sub3 var_ha_Sub3) {
		super(var_ha_Sub3);
		if (var_ha_Sub3.aBoolean4251) {
			aClass341_5414 = new Class341(var_ha_Sub3, 2);
			aClass341_5414.method3627((byte) -121, 0);
			aHa_Sub3_3121.method1330(113, 1);
			aHa_Sub3_3121.method1306(34165, 7681, -22394);
			aHa_Sub3_3121.method1283(2, 34168, 770, (byte) -121);
			aHa_Sub3_3121.method1346(true, 770, 0, 34167);
			OpenGL.glTexGeni(8192, 9472, 34066);
			OpenGL.glTexGeni(8193, 9472, 34066);
			OpenGL.glTexGeni(8194, 9472, 34066);
			OpenGL.glEnable(3168);
			OpenGL.glEnable(3169);
			OpenGL.glEnable(3170);
			aHa_Sub3_3121.method1330(117, 0);
			aClass341_5414.method3622(-126);
			aClass341_5414.method3627((byte) -107, 1);
			aHa_Sub3_3121.method1330(117, 1);
			aHa_Sub3_3121.method1306(8448, 8448, -22394);
			aHa_Sub3_3121.method1283(2, 34166, 770, (byte) -127);
			aHa_Sub3_3121.method1346(true, 770, 0, 5890);
			OpenGL.glDisable(3168);
			OpenGL.glDisable(3169);
			OpenGL.glDisable(3170);
			OpenGL.glMatrixMode(5890);
			OpenGL.glLoadIdentity();
			OpenGL.glMatrixMode(5888);
			aHa_Sub3_3121.method1330(119, 0);
			aClass341_5414.method3622(-120);
		}
	}

	final void method3768(byte i, boolean bool) {
		Class69_Sub3 class69_sub3 = aHa_Sub3_3121.method1329((byte) 8);
		if (aClass341_5414 != null && class69_sub3 != null && bool) {
			aClass341_5414.method3625((byte) -101, '\0');
			aHa_Sub3_3121.method1330(i + 206, 1);
			aHa_Sub3_3121.method1316(class69_sub3, (byte) -114);
			OpenGL.glMatrixMode(5890);
			OpenGL.glLoadMatrixf(aHa_Sub3_3121.aClass373_Sub1_4171.method3919(116), 0);
			OpenGL.glMatrixMode(5888);
			aHa_Sub3_3121.method1330(i ^ ~0x2b, 0);
			aBoolean5413 = true;
		} else
			aHa_Sub3_3121.method1346(true, 770, 0, 34168);
		if (i != -88)
			method3766(-34);
	}

	final void method3766(int i) {
		if (aBoolean5413) {
			aClass341_5414.method3625((byte) -123, '\001');
			aHa_Sub3_3121.method1330(121, 1);
			aHa_Sub3_3121.method1316(null, (byte) -123);
			aHa_Sub3_3121.method1330(111, 0);
		} else
			aHa_Sub3_3121.method1346(true, 770, 0, 5890);
		if (i <= 30)
			aBoolean5413 = true;
		aHa_Sub3_3121.method1306(8448, 8448, -22394);
		aBoolean5413 = false;
	}

	public static void method3795(byte i) {
		aClass155_5409 = null;
		if (i < -51) {
			aClass231_5411 = null;
			aD5412 = null;
			aClass231_5410 = null;
		}
	}

	final void method3764(boolean bool, int i, Class69 class69) {
		if (bool)
			method3763(25);
		aHa_Sub3_3121.method1316(class69, (byte) -119);
		aHa_Sub3_3121.method1272((byte) -107, i);
	}
}
