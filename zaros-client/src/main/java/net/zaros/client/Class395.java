package net.zaros.client;
/* Class395 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Rectangle;

final class Class395 {
	private d aD3310;
	private AdvancedMemoryCache aClass113_3311 = new AdvancedMemoryCache(256);
	private ha_Sub1 aHa_Sub1_3312;
	static IncomingPacket aClass231_3313 = new IncomingPacket(106, -1);
	static int anInt3314 = 0;
	static int anInt3315;
	static Sprite aClass397_3316;
	static int anInt3317;

	static final boolean method4064(int i, int i_0_, int i_1_) {
		if (i_1_ >= 1000 && i_0_ < 1000) {
			return true;
		}
		if ((i_1_ ^ 0xffffffff) > i && i_0_ < 1000) {
			if (ClotchesLoader.method299(92, i_0_)) {
				return true;
			}
			if (ClotchesLoader.method299(-82, i_1_)) {
				return false;
			}
			return true;
		}
		if (i_1_ >= 1000 && i_0_ >= 1000) {
			return true;
		}
		return false;
	}

	final void method4065(byte i) {
		if (i >= 18) {
			aClass113_3311.clear();
		}
	}

	public static void method4066(int i) {
		aClass231_3313 = null;
		if (i == -13283) {
			aClass397_3316 = null;
		}
	}

	static final void method4067(int i, byte[] is, int i_2_, int i_3_, byte[] is_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		int i_9_ = -50 / ((37 - i_8_) / 49);
		int i_10_ = -(i_5_ >> 2);
		i_5_ = -(i_5_ & 0x3);
		for (int i_11_ = -i_3_; i_11_ < 0; i_11_++) {
			for (int i_12_ = i_10_; i_12_ < 0; i_12_++) {
				is[i_7_++] += is_4_[i_2_++];
				is[i_7_++] += is_4_[i_2_++];
				is[i_7_++] += is_4_[i_2_++];
				is[i_7_++] += is_4_[i_2_++];
			}
			for (int i_13_ = i_5_; i_13_ < 0; i_13_++) {
				is[i_7_++] += is_4_[i_2_++];
			}
			i_7_ += i_6_;
			i_2_ += i;
		}
	}

	static final void method4068(int i, int i_14_, int i_15_, int i_16_, int i_17_) {
		int i_18_ = 0;
		if (i_16_ != -30544) {
			method4069(121, 86);
		}
		for (/**/; Class154_Sub1.anInt4291 > i_18_; i_18_++) {
			Rectangle rectangle = s.aRectangleArray2837[i_18_];
			if (rectangle.width + rectangle.x > i_14_ && i_14_ + i_17_ > rectangle.x && i < rectangle.height + rectangle.y && rectangle.y < i_15_ + i) {
				Class93.aBooleanArray1002[i_18_] = true;
			}
		}
		Class368_Sub8.method3838(false, i_14_, i, i_15_ + i, i_14_ + i_17_);
	}

	static final void method4069(int i, int i_19_) {
		Class318.anInt2817 = i_19_;
		if (i != 1000) {
			aClass231_3313 = null;
		}
		Class249.aClass113_2355.clear();
	}

	final void method4070(byte i) {
		aClass113_3311.clean(5);
		if (i > -98) {
			aD3310 = null;
		}
	}

	final Interface6_Impl1 method4071(int i, byte i_20_) {
		Object object = aClass113_3311.get(i);
		if (object != null) {
			return (Interface6_Impl1) object;
		}
		if (!aD3310.is_ready(i)) {
			return null;
		}
		int i_21_ = 100 % ((-65 - i_20_) / 44);
		MaterialRaw class170 = aD3310.method14(i, -9412);
		int i_22_ = !class170.small_sized ? aHa_Sub1_3312.anInt4026 : 64;
		Interface6_Impl1 interface6_impl1;
		if (class170.aBoolean1790 && aHa_Sub1_3312.b()) {
			float[] fs = aD3310.method16(i_22_, 0.7F, false, i, i_22_, 92);
			interface6_impl1 = aHa_Sub1_3312.method1230(fs, za_Sub2.aClass202_6555, class170.aByte1795 != 0, 0, i_22_, i_22_);
		} else {
			int[] is;
			if (class170.anInt1788 != 2 && Class41_Sub23.method485(-88, class170.aByte1774)) {
				is = aD3310.get_transparent_pixels(i_22_, i_22_, true, (byte) 115, 0.7F, i);
			} else {
				is = aD3310.method17(0.7F, i_22_, false, (byte) -104, i_22_, i);
			}
			interface6_impl1 = aHa_Sub1_3312.method1179(i_22_, i_22_, class170.aByte1795 != 0, is, -461);
		}
		interface6_impl1.method64((byte) 49, class170.aBoolean1780, class170.aBoolean1779);
		aClass113_3311.put(interface6_impl1, i);
		return interface6_impl1;
	}

	Class395(ha_Sub1 var_ha_Sub1, d var_d) {
		aHa_Sub1_3312 = var_ha_Sub1;
		aD3310 = var_d;
	}
}
