package net.zaros.client;

/* Class196 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class196 {
	static Js5 fs20;
	private int anInt1986 = -1;
	private String[] aStringArray1987;
	private int anInt1988;
	static int[] anIntArray1989 = new int[1];
	private boolean aBoolean1990 = false;
	static int anInt1991 = 0;

	private final void method1933(int i, int i_0_) {
		String[] strings = new String[method1935(-102, i)];
		ArrayTools.removeElement(aStringArray1987, 0, strings, 0, aStringArray1987.length);
		if (i_0_ == 11782)
			aStringArray1987 = strings;
	}

	public final String toString() {
		StringBuffer stringbuffer = new StringBuffer();
		stringbuffer.append("[");
		for (int i = 0; i < anInt1986; i++) {
			if (i != 0)
				stringbuffer.append(", ");
			stringbuffer.append(aStringArray1987[i]);
		}
		stringbuffer.append("]");
		return stringbuffer.toString();
	}

	public static void method1934(boolean bool) {
		anIntArray1989 = null;
		if (bool)
			anInt1991 = -86;
		fs20 = null;
	}

	private final int method1935(int i, int i_1_) {
		int i_2_ = -121 % ((-36 - i) / 60);
		int i_3_ = aStringArray1987.length;
		while (i_1_ >= i_3_) {
			if (aBoolean1990) {
				if (i_3_ != 0)
					i_3_ *= anInt1988;
				else
					i_3_ = 1;
			} else
				i_3_ += anInt1988;
		}
		return i_3_;
	}

	final String[] method1936(boolean bool) {
		String[] strings = new String[anInt1986 + 1];
		if (bool != true)
			aBoolean1990 = true;
		ArrayTools.removeElement(aStringArray1987, 0, strings, 0, anInt1986 + 1);
		return strings;
	}

	final void method1937(int i, String string) {
		method1939(string, 1, anInt1986 + 1);
		if (i != 0)
			anInt1991 = -114;
	}

	static final boolean method1938(int i, int i_4_) {
		if (i > -106)
			anIntArray1989 = null;
		if (i_4_ != 3 && i_4_ != 7 && i_4_ != 9 && i_4_ != 11)
			return false;
		return true;
	}

	private final void method1939(String string, int i, int i_5_) {
		if (i_5_ > anInt1986)
			anInt1986 = i_5_;
		if (i_5_ >= aStringArray1987.length)
			method1933(i_5_, 11782);
		if (i == 1)
			aStringArray1987[i_5_] = string;
	}

	Class196(int i, boolean bool) {
		aStringArray1987 = new String[0];
		aBoolean1990 = bool;
		anInt1988 = i;
	}

	static final float method1940(int i, float f) {
		if (i != 1618)
			return 0.8991209F;
		return f * f * f * (f * (f * 6.0F + -15.0F) + 10.0F);
	}
}
