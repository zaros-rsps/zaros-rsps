package net.zaros.client;

/* Class338_Sub8_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub8_Sub1 extends Class338_Sub8 {
	short aShort6574;
	String aString6575;
	static int anInt6576 = 0;
	int anInt6577 = (int) (Class72.method771(-123) / 1000L);

	static final boolean method3604(int i, byte i_0_, int i_1_) {
		if (i_1_ == 11)
			i_1_ = 10;
		ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(i);
		if (i_0_ < 35)
			method3607((byte) -117);
		if (i_1_ >= 5 && i_1_ <= 8)
			i_1_ = 4;
		return class70.method748(i_1_, 0);
	}

	static final void method3605(int i) {
		if (i == 1)
			Class286.aString2643 = Class379.aString3625 = "";
	}

	static final int method3606(int i, int i_2_, int i_3_, byte i_4_) {
		if ((Class41_Sub18.aByteArrayArrayArray3786[i_2_][i_3_][i] & 0x8) != 0)
			return 0;
		int i_5_ = 70 % ((82 - i_4_) / 43);
		if (i_2_ > 0 && (Class41_Sub18.aByteArrayArrayArray3786[1][i_3_][i] & 0x2) != 0)
			return i_2_ - 1;
		return i_2_;
	}

	static final Class210_Sub1 method3607(byte i) {
		if (AnimationsLoader.aClass210_Sub1Array2760.length > Class69_Sub1_Sub1.anInt6689)
			return (AnimationsLoader.aClass210_Sub1Array2760[Class69_Sub1_Sub1.anInt6689++]);
		if (i < 97)
			method3605(112);
		return null;
	}

	Class338_Sub8_Sub1(String string, int i) {
		aString6575 = string;
		aShort6574 = (short) i;
	}
}
