package net.zaros.client;

/* Class296_Sub51_Sub13 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub13 extends TextureOperation {
	static boolean[] aBooleanArray6414 = new boolean[5];
	private int anInt6415 = -1;
	static AnimationsLoader animationsLoader;
	private int anInt6417;
	private int anInt6418;
	private int[] anIntArray6419;
	static ClotchesLoader aClass24_6420;

	static final Connection method3111(boolean bool) {
		if (bool != true)
			aClass24_6420 = null;
		if (BITConfigDefinition.method2353(-8, Class366_Sub6.anInt5392))
			return Class296_Sub45_Sub2.aClass204_6276;
		return Class296_Sub45_Sub2.aClass204_6277;
	}

	final void method3070(int i, int i_0_, int i_1_) {
		super.method3070(i, i_0_, i_1_);
		if (anInt6415 >= 0 && Class134.aD1388 != null) {
			int i_2_ = (!Class134.aD1388.method14(anInt6415, -9412).small_sized ? 128 : 64);
			anIntArray6419 = Class134.aD1388.get_transparent_pixels(i_2_, i_2_, false, (byte) 125, 1.0F, anInt6415);
			anInt6417 = i_2_;
			anInt6418 = i_2_;
		}
	}

	static final void method3112(Js5 class138, byte i, ha var_ha) {
		Class186[] class186s = Class186.method1871(class138, Class41_Sub25.anInt3804, 0);
		Class69_Sub4.aClass397Array5732 = new Sprite[class186s.length];
		for (int i_3_ = 0; class186s.length > i_3_; i_3_++)
			Class69_Sub4.aClass397Array5732[i_3_] = var_ha.a(class186s[i_3_], true);
		class186s = Class186.method1871(class138, Class368_Sub5.anInt5446, 0);
		Class368_Sub10.aClass397Array5482 = new Sprite[class186s.length];
		for (int i_4_ = 0; i_4_ < class186s.length; i_4_++)
			Class368_Sub10.aClass397Array5482[i_4_] = var_ha.a(class186s[i_4_], true);
		class186s = Class186.method1871(class138, Class379_Sub1.anInt5672, 0);
		Class296_Sub51_Sub24.aClass397Array6463 = new Sprite[class186s.length];
		for (int i_5_ = 0; class186s.length > i_5_; i_5_++)
			Class296_Sub51_Sub24.aClass397Array6463[i_5_] = var_ha.a(class186s[i_5_], true);
		class186s = Class186.method1871(class138, Class16.anInt183, 0);
		Class296_Sub9_Sub1.aClass397Array5986 = new Sprite[class186s.length];
		for (int i_6_ = 0; i_6_ < class186s.length; i_6_++)
			Class296_Sub9_Sub1.aClass397Array5986[i_6_] = var_ha.a(class186s[i_6_], true);
		class186s = Class186.method1871(class138, Class116.anInt3676, 0);
		Class151.aClass397Array1555 = new Sprite[class186s.length];
		for (int i_7_ = 0; class186s.length > i_7_; i_7_++)
			Class151.aClass397Array1555[i_7_] = var_ha.a(class186s[i_7_], true);
		if (i >= -99)
			method3112(null, (byte) -102, null);
		class186s = Class186.method1871(class138, Class296_Sub11.anInt4650, 0);
		Class100.aClass397Array1069 = new Sprite[class186s.length];
		for (int i_8_ = 0; class186s.length > i_8_; i_8_++)
			Class100.aClass397Array1069[i_8_] = var_ha.a(class186s[i_8_], true);
		class186s = Class186.method1871(class138, Class307.anInt2749, 0);
		StaticMethods.aClass397Array6073 = new Sprite[class186s.length];
		for (int i_9_ = 0; i_9_ < class186s.length; i_9_++)
			StaticMethods.aClass397Array6073[i_9_] = var_ha.a(class186s[i_9_], true);
		class186s = Class186.method1871(class138, Class206.anInt2067, 0);
		Class379_Sub1.aClass397Array5671 = new Sprite[class186s.length];
		for (int i_10_ = 0; i_10_ < class186s.length; i_10_++)
			Class379_Sub1.aClass397Array5671[i_10_] = var_ha.a(class186s[i_10_], true);
		class186s = Class186.method1871(class138, Class389.anInt3278, 0);
		Class205.aClass397Array3505 = new Sprite[class186s.length];
		for (int i_11_ = 0; i_11_ < class186s.length; i_11_++)
			Class205.aClass397Array3505[i_11_] = var_ha.a(class186s[i_11_], true);
		class186s = Class186.method1871(class138, Class366_Sub2.anInt5370, 0);
		Class356.aClass397Array3081 = new Sprite[class186s.length];
		for (int i_12_ = 0; class186s.length > i_12_; i_12_++)
			Class356.aClass397Array3081[i_12_] = var_ha.a(class186s[i_12_], true);
		class186s = Class186.method1871(class138, Class284.anInt2619, 0);
		Class85.aClass397Array929 = new Sprite[class186s.length];
		for (int i_13_ = 0; i_13_ < class186s.length; i_13_++)
			Class85.aClass397Array929[i_13_] = var_ha.a(class186s[i_13_], true);
		Class296_Sub39_Sub17.aClass397_6237 = var_ha.a(Class186.method1876(class138, Class296_Sub39_Sub15_Sub1.anInt6718, 0), true);
		Class296_Sub2.aClass397_4589 = var_ha.a(Class186.method1876(class138, Class205_Sub2.anInt5630, 0), true);
		class186s = Class186.method1871(class138, ISAACCipher.anInt1897, 0);
		GraphicsLoader.aClass397Array2478 = new Sprite[class186s.length];
		for (int i_14_ = 0; i_14_ < class186s.length; i_14_++)
			GraphicsLoader.aClass397Array2478[i_14_] = var_ha.a(class186s[i_14_], true);
	}

	public static void method3113(byte i) {
		if (i != -97)
			animationsLoader = null;
		animationsLoader = null;
		aBooleanArray6414 = null;
		aClass24_6420 = null;
	}

	final void method3071(int i, Packet class296_sub17, int i_15_) {
		if (i >= -84)
			animationsLoader = null;
		if (i_15_ == 0)
			anInt6415 = class296_sub17.g2();
	}

	final void method3073(int i) {
		int i_16_ = 66 / ((-58 - i) / 39);
		super.method3073(80);
		anIntArray6419 = null;
	}

	final int[][] get_colour_output(int i, int i_17_) {
		if (i_17_ != 17621)
			method3113((byte) 48);
		int[][] is = aClass86_5034.method823((byte) 10, i);
		if (aClass86_5034.aBoolean939) {
			int i_18_ = ((Class296_Sub35_Sub2.anInt6114 == anInt6418 ? i : anInt6418 * i / Class296_Sub35_Sub2.anInt6114) * anInt6417);
			int[] is_19_ = is[0];
			int[] is_20_ = is[1];
			int[] is_21_ = is[2];
			if (Class41_Sub10.anInt3769 != anInt6417) {
				for (int i_22_ = 0; Class41_Sub10.anInt3769 > i_22_; i_22_++) {
					int i_23_ = anInt6417 * i_22_ / Class41_Sub10.anInt3769;
					int i_24_ = anIntArray6419[i_23_ + i_18_];
					is_21_[i_22_] = i_24_ << 4 & 4080;
					is_20_[i_22_] = 4080 & i_24_ >> 4;
					is_19_[i_22_] = i_24_ >> 12 & 4080;
				}
			} else {
				for (int i_25_ = 0; i_25_ < Class41_Sub10.anInt3769; i_25_++) {
					int i_26_ = anIntArray6419[i_18_++];
					is_21_[i_25_] = 4080 & i_26_ << 4;
					is_20_[i_25_] = (65280 & i_26_) >> 4;
					is_19_[i_25_] = (i_26_ & 16711680) >> 12;
				}
			}
		}
		return is;
	}

	public Class296_Sub51_Sub13() {
		super(0, false);
	}

	final int method3062(byte i) {
		if (i != 2)
			return -44;
		return anInt6415;
	}
}
