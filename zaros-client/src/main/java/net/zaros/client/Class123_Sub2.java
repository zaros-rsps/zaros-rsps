package net.zaros.client;

/* Class123_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

class Class123_Sub2 extends Class123 {
	private int anInt3914;
	private int[] anIntArray3915 = new int[anInt1271];
	static OutgoingPacket aClass311_3916 = new OutgoingPacket(18, 1);
	private int anInt3917;
	private byte[] aByteArray3918;
	static int[][] anIntArrayArray3919 = {{2, 4, 6, 0}, {0, 2, 4, 6}, {0, 2, 4}, {4, 0, 2}, {2, 4, 0}, {0, 2, 4}, {6, 0, 1, 2, 4, 5}, {0, 4, 7, 6}, {4, 7, 6, 0}, {0, 8, 6, 2, 9, 4}, {2, 9, 4, 0, 8, 6}, {2, 11, 4, 6, 10, 0}, {2, 4, 6, 0}};

	final void method1053(boolean bool, int i, int i_0_) {
		if (bool != true)
			method1055(false);
		anInt3914 += i_0_ * anIntArray3915[i] >> 12;
	}

	public static void method1067(byte i) {
		aClass311_3916 = null;
		anIntArrayArray3919 = null;
		if (i != 51)
			method1069(38);
	}

	final void method1055(boolean bool) {
		anInt3914 = 0;
		if (bool)
			anInt3917 = 21;
		anInt3917 = 0;
	}

	final void method1057(byte i) {
		anInt3914 = Math.abs(anInt3914);
		int i_1_ = -66 % ((i - 37) / 41);
		if (anInt3914 >= 4096)
			anInt3914 = 4095;
		method1072((byte) (anInt3914 >> 4), anInt3917++, (byte) -45);
		anInt3914 = 0;
	}

	static final boolean method1068(int i, byte i_2_, int i_3_) {
		if (i_2_ < 60)
			aClass311_3916 = null;
		if ((i_3_ & 0x800) == 0)
			return false;
		return true;
	}

	static final void method1069(int i) {
		Class360_Sub5.anInt5328 = i;
		for (int i_4_ = 0; i_4_ < Class228.anInt2201; i_4_++) {
			for (int i_5_ = 0; i_5_ < Class368_Sub12.anInt5488; i_5_++) {
				if (Class338_Sub2.aClass247ArrayArrayArray5195[i][i_4_][i_5_] == null)
					Class338_Sub2.aClass247ArrayArrayArray5195[i][i_4_][i_5_] = new Class247(i);
			}
		}
	}

	static final Class338_Sub3_Sub1 method1070(int i, int i_6_, int i_7_, Class var_class) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_6_][i_7_];
		if (class247 == null)
			return null;
		for (Class234 class234 = class247.aClass234_2341; class234 != null; class234 = class234.aClass234_2224) {
			Class338_Sub3_Sub1 class338_sub3_sub1 = class234.aClass338_Sub3_Sub1_2226;
			if (var_class.isAssignableFrom(class338_sub3_sub1.getClass()) && class338_sub3_sub1.aShort6560 == i_6_ && class338_sub3_sub1.aShort6564 == i_7_)
				return class338_sub3_sub1;
		}
		return null;
	}

	static final void method1071(Class296_Sub45_Sub4 class296_sub45_sub4, boolean bool, boolean bool_8_) {
		if (bool_8_ != true)
			method1071(null, true, true);
		Class274.aClass381_2524.method3996(-107, class296_sub45_sub4);
		if (bool)
			Class10.method196(Class296_Sub15.aClass138_4671, class296_sub45_sub4, -110, Class93.fs4, Class274.aClass381_2524, Class33.aClass138_329);
	}

	void method1072(byte i, int i_9_, byte i_10_) {
		if (i_10_ != -45)
			method1072((byte) 16, -116, (byte) -82);
		aByteArray3918[anInt3917++] = (byte) (((255 & i) >> 1) + 127);
	}

	Class123_Sub2(int i, int i_11_, int i_12_, int i_13_, int i_14_, float f) {
		super(i, i_11_, i_12_, i_13_, i_14_);
		for (int i_15_ = 0; i_15_ < anInt1271; i_15_++)
			anIntArray3915[i_15_] = (short) (int) (Math.pow((double) f, (double) i_15_) * 4096.0);
	}
}
