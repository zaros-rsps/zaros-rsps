package net.zaros.client;

/* Class130 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class130 {
	static Sprite aClass397_1334;
	private byte[] aByteArray1335;
	private int[] anIntArray1336;
	static Class108 aClass108_1337 = new Class108();
	private int[] anIntArray1338;
	static Class108 aClass108_1339 = new Class108();

	final int method1375(int i, int i_0_, int i_1_, byte[] is, int i_2_, byte[] is_3_) {
		int i_4_ = 0;
		int i_5_ = i_0_ << 3;
		for (i_2_ += i_1_; i_1_ < i_2_; i_1_++) {
			int i_6_ = is_3_[i_1_] & 0xff;
			int i_7_ = anIntArray1336[i_6_];
			int i_8_ = aByteArray1335[i_6_];
			if (i_8_ == 0)
				throw new RuntimeException("No codeword for data value " + i_6_);
			int i_9_ = i_5_ >> 3;
			int i_10_ = i_5_ & 0x7;
			i_4_ &= -i_10_ >> 31;
			int i_11_ = i_9_ + (i_10_ + i_8_ - 1 >> 3);
			i_10_ += 24;
			is[i_9_] = (byte) (i_4_ = Class48.bitOR(i_4_, i_7_ >>> i_10_));
			if (i_9_ < i_11_) {
				i_9_++;
				i_10_ -= 8;
				is[i_9_] = (byte) (i_4_ = i_7_ >>> i_10_);
				if (i_11_ > i_9_) {
					i_9_++;
					i_10_ -= 8;
					is[i_9_] = (byte) (i_4_ = i_7_ >>> i_10_);
					if (i_9_ < i_11_) {
						i_9_++;
						i_10_ -= 8;
						is[i_9_] = (byte) (i_4_ = i_7_ >>> i_10_);
						if (i_11_ > i_9_) {
							i_9_++;
							i_10_ -= 8;
							is[i_9_] = (byte) (i_4_ = i_7_ << -i_10_);
						}
					}
				}
			}
			i_5_ += i_8_;
		}
		if (i != 7)
			method1380(-74, null);
		return -i_0_ + (i_5_ + 7 >> 3);
	}

	static final boolean method1376(int i, int i_12_) {
		if (i_12_ != -14723)
			aClass397_1334 = null;
		if (i != 11 && i != 12 && i != 13)
			return false;
		return true;
	}

	static final void method1377(int i) {
		Class368_Sub16.aHa5527.xa(((float) Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub6_5014.method416(121) * 0.1F + 0.7F) * 1.1523438F);
		Class368_Sub16.aHa5527.ZA(ItemsNode.anInt4593, 0.69921875F, 1.2F, -200.0F, -240.0F, -200.0F);
		Class368_Sub16.aHa5527.L(ByteStream.anInt6042, -1, 0);
		Class368_Sub16.aHa5527.a(Class368.aClass241_3131);
		if (i != 23783)
			method1378(false);
	}

	public static void method1378(boolean bool) {
		aClass397_1334 = null;
		aClass108_1339 = null;
		aClass108_1337 = null;
		if (bool != true)
			method1376(-46, -105);
	}

	final int method1379(byte[] is, int i, int i_13_, int i_14_, int i_15_, byte[] is_16_) {
		if (i_13_ == 0)
			return 0;
		i_13_ += i;
		if (i_14_ != -27318)
			method1378(true);
		int i_17_ = 0;
		int i_18_ = i_15_;
		for (;;) {
			byte i_19_ = is[i_18_];
			if (i_19_ >= 0)
				i_17_++;
			else
				i_17_ = anIntArray1338[i_17_];
			int i_20_;
			if ((i_20_ = anIntArray1338[i_17_]) < 0) {
				is_16_[i++] = (byte) (i_20_ ^ 0xffffffff);
				if (i_13_ <= i)
					break;
				i_17_ = 0;
			}
			if ((i_19_ & 0x40) != 0)
				i_17_ = anIntArray1338[i_17_];
			else
				i_17_++;
			if ((i_20_ = anIntArray1338[i_17_]) < 0) {
				is_16_[i++] = (byte) (i_20_ ^ 0xffffffff);
				if (i_13_ <= i)
					break;
				i_17_ = 0;
			}
			if ((i_19_ & 0x20) != 0)
				i_17_ = anIntArray1338[i_17_];
			else
				i_17_++;
			if ((i_20_ = anIntArray1338[i_17_]) < 0) {
				is_16_[i++] = (byte) (i_20_ ^ 0xffffffff);
				if (i >= i_13_)
					break;
				i_17_ = 0;
			}
			if ((i_19_ & 0x10) != 0)
				i_17_ = anIntArray1338[i_17_];
			else
				i_17_++;
			if ((i_20_ = anIntArray1338[i_17_]) < 0) {
				is_16_[i++] = (byte) (i_20_ ^ 0xffffffff);
				if (i_13_ <= i)
					break;
				i_17_ = 0;
			}
			if ((i_19_ & 0x8) != 0)
				i_17_ = anIntArray1338[i_17_];
			else
				i_17_++;
			if ((i_20_ = anIntArray1338[i_17_]) < 0) {
				is_16_[i++] = (byte) (i_20_ ^ 0xffffffff);
				if (i >= i_13_)
					break;
				i_17_ = 0;
			}
			if ((i_19_ & 0x4) == 0)
				i_17_++;
			else
				i_17_ = anIntArray1338[i_17_];
			if ((i_20_ = anIntArray1338[i_17_]) < 0) {
				is_16_[i++] = (byte) (i_20_ ^ 0xffffffff);
				if (i >= i_13_)
					break;
				i_17_ = 0;
			}
			if ((i_19_ & 0x2) != 0)
				i_17_ = anIntArray1338[i_17_];
			else
				i_17_++;
			if ((i_20_ = anIntArray1338[i_17_]) < 0) {
				is_16_[i++] = (byte) (i_20_ ^ 0xffffffff);
				if (i_13_ <= i)
					break;
				i_17_ = 0;
			}
			if ((i_19_ & 0x1) == 0)
				i_17_++;
			else
				i_17_ = anIntArray1338[i_17_];
			if ((i_20_ = anIntArray1338[i_17_]) < 0) {
				is_16_[i++] = (byte) (i_20_ ^ 0xffffffff);
				if (i >= i_13_)
					break;
				i_17_ = 0;
			}
			i_18_++;
		}
		return -i_15_ + (i_18_ + 1);
	}

	static final void method1380(int i, String string) {
		Class347.settings = string;
		if (CS2Script.anApplet6140 != null) {
			try {
				String string_21_ = CS2Script.anApplet6140.getParameter("cookieprefix");
				String string_22_ = CS2Script.anApplet6140.getParameter("cookiehost");
				String string_23_ = (string_21_ + "settings=" + string + "; version=1; path=/; domain=" + string_22_);
				if (string.length() == 0)
					string_23_ += "; Expires=Thu, 01-Jan-1970 00:00:00 GMT; Max-Age=0";
				else
					string_23_ += ("; Expires=" + Class92.method843((byte) 120, (Class72.method771(-126) - -94608000000L)) + "; Max-Age=" + 94608000L);
				Class297.method3232("document.cookie=\"" + string_23_ + "\"", CS2Script.anApplet6140, i + 1126780049);
			} catch (Throwable throwable) {
				/* empty */
			}
			if (i != -1126756703)
				aClass397_1334 = null;
		}
	}

	Class130(byte[] is) {
		int i = is.length;
		anIntArray1336 = new int[i];
		aByteArray1335 = is;
		anIntArray1338 = new int[8];
		int[] is_24_ = new int[33];
		int i_25_ = 0;
		for (int i_26_ = 0; i > i_26_; i_26_++) {
			int i_27_ = is[i_26_];
			if (i_27_ != 0) {
				int i_28_ = 1 << -i_27_ + 32;
				int i_29_ = is_24_[i_27_];
				anIntArray1336[i_26_] = i_29_;
				int i_30_;
				if ((i_29_ & i_28_) != 0)
					i_30_ = is_24_[i_27_ - 1];
				else {
					for (int i_31_ = i_27_ - 1; i_31_ >= 1; i_31_--) {
						int i_32_ = is_24_[i_31_];
						if (i_29_ != i_32_)
							break;
						int i_33_ = 1 << -i_31_ + 32;
						if ((i_33_ & i_32_) != 0) {
							is_24_[i_31_] = is_24_[i_31_ - 1];
							break;
						}
						is_24_[i_31_] = Class48.bitOR(i_32_, i_33_);
					}
					i_30_ = i_28_ | i_29_;
				}
				is_24_[i_27_] = i_30_;
				for (int i_34_ = i_27_ + 1; i_34_ <= 32; i_34_++) {
					if (i_29_ == is_24_[i_34_])
						is_24_[i_34_] = i_30_;
				}
				int i_35_ = 0;
				for (int i_36_ = 0; i_36_ < i_27_; i_36_++) {
					int i_37_ = -2147483648 >>> i_36_;
					if ((i_37_ & i_29_) == 0)
						i_35_++;
					else {
						if (anIntArray1338[i_35_] == 0)
							anIntArray1338[i_35_] = i_25_;
						i_35_ = anIntArray1338[i_35_];
					}
					i_37_ >>>= 1;
					if (anIntArray1338.length <= i_35_) {
						int[] is_38_ = new int[anIntArray1338.length * 2];
						for (int i_39_ = 0; anIntArray1338.length > i_39_; i_39_++)
							is_38_[i_39_] = anIntArray1338[i_39_];
						anIntArray1338 = is_38_;
					}
				}
				if (i_35_ >= i_25_)
					i_25_ = i_35_ + 1;
				anIntArray1338[i_35_] = i_26_ ^ 0xffffffff;
			}
		}
	}
}
