package net.zaros.client;

/* Class69_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class69_Sub1_Sub1 extends Class69_Sub1 {
	int anInt6683;
	float aFloat6684;
	boolean aBoolean6685;
	static int[] anIntArray6686 = new int[5];
	float aFloat6687;
	int anInt6688;
	static int anInt6689 = 999999;

	Class69_Sub1_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_0_, int i_1_, int i_2_) {
		super(var_ha_Sub3, i, i_0_, i_1_, i_2_);
		if (anInt3682 == 34037) {
			aFloat6687 = (float) i_1_;
			aFloat6684 = (float) i_2_;
			aBoolean6685 = false;
		} else {
			aFloat6687 = aFloat6684 = 1.0F;
			aBoolean6685 = true;
		}
		anInt6688 = i_2_;
		anInt6683 = i_1_;
	}

	Class69_Sub1_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_3_, int i_4_, int i_5_, boolean bool, byte[] is, int i_6_) {
		super(var_ha_Sub3, i, i_3_, i_4_, i_5_, bool, is, i_6_, true);
		if (anInt3682 == 34037) {
			aFloat6687 = (float) i_4_;
			aFloat6684 = (float) i_5_;
			aBoolean6685 = false;
		} else {
			aBoolean6685 = true;
			aFloat6687 = aFloat6684 = 1.0F;
		}
		anInt6688 = i_5_;
		anInt6683 = i_4_;
	}

	static final void method734(int i, int i_7_, int i_8_, int i_9_, int i_10_) {
		if (ConfigurationDefinition.anInt676 <= i_9_ - i_10_ && i_10_ + i_9_ <= Class288.anInt2652 && -i_10_ + i >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i + i_10_)
			Class249.method2196(i_10_, i_9_, -1, i_7_, i);
		else
			Class296_Sub29_Sub2.method2697(i_7_, i_9_, i_10_, i, -105);
		if (i_8_ != 1)
			anInt6689 = 10;
	}

	Class69_Sub1_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_11_, int i_12_, int i_13_, int[] is) {
		super(var_ha_Sub3, 3553, 6408, i_12_, i_13_);
		anInt6688 = i_11_;
		anInt6683 = i;
		this.method730(0, 0, 0, i, true, is, 0, i_11_, 0);
		aBoolean6685 = false;
		aFloat6684 = (float) i_11_ / (float) i_13_;
		aFloat6687 = (float) i / (float) i_12_;
		this.method733(false, false, true);
	}

	Class69_Sub1_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_14_, int i_15_, boolean bool, int[] is, int i_16_, int i_17_) {
		super(var_ha_Sub3, i, 6408, i_14_, i_15_, bool, is, i_16_, i_17_, true);
		if (anInt3682 == 34037) {
			aBoolean6685 = false;
			aFloat6687 = (float) i_14_;
			aFloat6684 = (float) i_15_;
		} else {
			aBoolean6685 = true;
			aFloat6687 = aFloat6684 = 1.0F;
		}
		anInt6683 = i_14_;
		anInt6688 = i_15_;
	}

	static final float[] method735(int i, float f, float f_18_, int i_19_, int i_20_, int i_21_, int i_22_, float f_23_) {
		float[] fs = new float[9];
		float[] fs_24_ = new float[9];
		float f_25_ = (float) Math.cos((double) ((float) i_21_ * 0.024543693F));
		float f_26_ = (float) Math.sin((double) ((float) i_21_ * 0.024543693F));
		fs[7] = 0.0F;
		fs[1] = 0.0F;
		fs[0] = f_25_;
		fs[5] = 0.0F;
		fs[3] = 0.0F;
		float f_27_ = -f_25_ + 1.0F;
		fs[8] = f_25_;
		fs[6] = -f_26_;
		fs[2] = f_26_;
		fs[4] = 1.0F;
		float[] fs_28_ = new float[9];
		float f_29_ = 1.0F;
		float f_30_ = 0.0F;
		f_25_ = (float) i / 32767.0F;
		f_26_ = -(float) Math.sqrt((double) (1.0F - f_25_ * f_25_));
		f_27_ = -f_25_ + 1.0F;
		float f_31_ = (float) Math.sqrt((double) (i_19_ * i_19_ + i_20_ * i_20_));
		if (f_31_ == 0.0F && f_25_ == 0.0F)
			fs_24_ = fs;
		else {
			if (f_31_ != 0.0F) {
				f_29_ = (float) -i_19_ / f_31_;
				f_30_ = (float) i_20_ / f_31_;
			}
			fs_28_[2] = f_27_ * (f_29_ * f_30_);
			fs_28_[3] = f_26_ * -f_30_;
			fs_28_[8] = f_30_ * f_30_ * f_27_ + f_25_;
			fs_28_[1] = f_30_ * f_26_;
			fs_28_[7] = -f_29_ * f_26_;
			fs_28_[4] = f_25_;
			fs_28_[5] = f_26_ * f_29_;
			fs_28_[6] = f_30_ * f_29_ * f_27_;
			fs_28_[0] = f_25_ + f_29_ * f_29_ * f_27_;
			fs_24_[0] = fs[1] * fs_28_[3] + fs[0] * fs_28_[0] + fs[2] * fs_28_[6];
			fs_24_[1] = fs[2] * fs_28_[7] + (fs[0] * fs_28_[1] + fs[1] * fs_28_[4]);
			fs_24_[2] = fs[0] * fs_28_[2] + fs_28_[5] * fs[1] + fs[2] * fs_28_[8];
			fs_24_[3] = fs[4] * fs_28_[3] + fs[3] * fs_28_[0] + fs_28_[6] * fs[5];
			fs_24_[4] = fs[4] * fs_28_[4] + fs[3] * fs_28_[1] + fs_28_[7] * fs[5];
			fs_24_[6] = fs_28_[3] * fs[7] + fs[6] * fs_28_[0] + fs[8] * fs_28_[6];
			fs_24_[5] = fs_28_[8] * fs[5] + (fs[4] * fs_28_[5] + fs[3] * fs_28_[2]);
			fs_24_[7] = fs_28_[4] * fs[7] + fs[6] * fs_28_[1] + fs[8] * fs_28_[7];
			fs_24_[8] = fs[8] * fs_28_[8] + (fs_28_[2] * fs[6] + fs[7] * fs_28_[5]);
		}
		fs_24_[4] *= f;
		fs_24_[7] *= f_23_;
		fs_24_[1] *= f_18_;
		fs_24_[6] *= f_23_;
		fs_24_[i_22_] *= f;
		fs_24_[8] *= f_23_;
		fs_24_[0] *= f_18_;
		fs_24_[2] *= f_18_;
		fs_24_[3] *= f;
		return fs_24_;
	}

	Class69_Sub1_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_32_, int i_33_, int i_34_, int i_35_, byte[] is, int i_36_) {
		super(var_ha_Sub3, 3553, i, i_34_, i_35_);
		anInt6683 = i_32_;
		anInt6688 = i_33_;
		this.method731((byte) -30, 0, 0, i_32_, i_33_, is, true, 0, i_36_, 0);
		aFloat6684 = (float) i_33_ / (float) i_35_;
		aFloat6687 = (float) i_32_ / (float) i_34_;
		aBoolean6685 = false;
		this.method733(false, false, true);
	}

	public static void method736(byte i) {
		anIntArray6686 = null;
		if (i != -91)
			method734(-52, -100, 114, 101, -17);
	}

	Class69_Sub1_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_37_, int i_38_, int i_39_, int i_40_) {
		super(var_ha_Sub3, 3553, i, i_39_, i_40_);
		aFloat6687 = (float) i_37_ / (float) i_39_;
		anInt6688 = i_38_;
		aBoolean6685 = false;
		aFloat6684 = (float) i_38_ / (float) i_40_;
		anInt6683 = i_37_;
		this.method733(false, false, true);
	}

	Class69_Sub1_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_41_, int i_42_, int i_43_, int i_44_, boolean bool) {
		super(var_ha_Sub3, i, i_41_, i_42_, i_43_, i_44_);
		if (anInt3682 == 34037) {
			aFloat6684 = (float) i_44_;
			aFloat6687 = (float) i_43_;
			aBoolean6685 = false;
		} else {
			aFloat6687 = aFloat6684 = 1.0F;
			aBoolean6685 = true;
		}
		anInt6683 = i_43_;
		anInt6688 = i_44_;
	}

	Class69_Sub1_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_45_, int i_46_, int i_47_, int i_48_, int i_49_, boolean bool) {
		super(var_ha_Sub3, 3553, i, i_45_, i_48_, i_49_);
		anInt6688 = i_47_;
		aFloat6684 = (float) i_47_ / (float) i_49_;
		aBoolean6685 = false;
		aFloat6687 = (float) i_46_ / (float) i_48_;
		anInt6683 = i_46_;
		this.method733(false, false, true);
	}
}
