package net.zaros.client;

import jaggl.OpenGL;

final class Class366_Sub5 extends Class366 {
	private Class341 aClass341_5380;
	static short[] aShortArray5381 = new short[256];
	private int anInt5382;
	private Class361 aClass361_5383;
	private float aFloat5384;
	private Class99 aClass99_5385;
	static Class404 aClass404_5386;
	static int anInt5387 = 0;
	private float[] aFloatArray5388;

	static final void method3782(InterfaceComponent[] class51s, byte i, int i_0_) {
		if (i > -50)
			anInt5387 = 51;
		for (int i_1_ = 0; i_1_ < class51s.length; i_1_++) {
			InterfaceComponent class51 = class51s[i_1_];
			if (class51 != null && i_0_ == class51.parentId && !GameClient.method110(class51)) {
				if (class51.type == 0) {
					method3782(class51s, (byte) -84, class51.uid);
					if (class51.aClass51Array538 != null)
						method3782(class51.aClass51Array538, (byte) -101, class51.uid);
					Class296_Sub13 class296_sub13 = ((Class296_Sub13) Class386.aClass263_3264.get((long) class51.uid));
					if (class296_sub13 != null)
						Class332.method3414(class296_sub13.anInt4657, true);
				}
				if (class51.type == 6 && class51.anInt497 != -1) {
					if (class51.aClass44_554 == null) {
						class51.aClass44_554 = new Class44_Sub2();
						class51.aClass44_554.method549((byte) 115, class51.anInt497);
					}
					if (class51.aClass44_554.method559(Class217.anInt2119, (byte) 124) && class51.aClass44_554.method546(106))
						class51.aClass44_554.method547(14899);
				}
			}
		}
	}

	final boolean method3763(int i) {
		int i_2_ = 66 / ((73 - i) / 40);
		return true;
	}

	final void method3764(boolean bool, int i, Class69 class69) {
		aHa_Sub3_3121.method1316(class69, (byte) -103);
		aHa_Sub3_3121.method1272((byte) -107, i);
		if (bool)
			method3763(4);
	}

	final void method3768(byte i, boolean bool) {
		if (aClass341_5380 != null && i == -88) {
			aClass341_5380.method3625((byte) 12, '\0');
			aHa_Sub3_3121.method1330(112, 1);
			OpenGL.glMatrixMode(5890);
			OpenGL.glLoadMatrixf(aHa_Sub3_3121.aClass373_Sub1_4171.method3923(true), 0);
			OpenGL.glMatrixMode(5888);
			aHa_Sub3_3121.method1330(120, 0);
			if (anInt5382 != aHa_Sub3_3121.anInt4134) {
				int i_3_ = aHa_Sub3_3121.anInt4134 % 5000 * 128 / 5000;
				for (int i_4_ = 0; i_4_ < 64; i_4_++) {
					OpenGL.glProgramLocalParameter4fvARB(34336, i_4_, aFloatArray5388, i_3_);
					i_3_ += 2;
				}
				if (!aClass361_5383.aBoolean3102)
					OpenGL.glProgramLocalParameter4fARB(34336, 65, 0.0F, 0.0F, 0.0F, 1.0F);
				else
					aFloat5384 = (float) (aHa_Sub3_3121.anInt4134 % 4000) / 4000.0F;
				anInt5382 = aHa_Sub3_3121.anInt4134;
			}
		}
	}

	final void method3766(int i) {
		if (aClass341_5380 != null) {
			aClass341_5380.method3625((byte) -107, '\001');
			aHa_Sub3_3121.method1330(122, 1);
			if (i <= 30)
				aFloat5384 = -0.5906753F;
			aHa_Sub3_3121.method1316(null, (byte) -124);
			aHa_Sub3_3121.method1330(115, 0);
		}
	}

	public static void method3783(int i) {
		aClass404_5386 = null;
		if (i < 87)
			aClass404_5386 = null;
		aShortArray5381 = null;
	}

	final void method3770(byte i, boolean bool) {
		if (i != 33)
			method3763(-54);
	}

	final void method3769(int i, byte i_5_, int i_6_) {
		if (aClass341_5380 != null) {
			aHa_Sub3_3121.method1330(117, 1);
			if ((i & 0x80) == 0) {
				if ((i_6_ & 0x1) == 1) {
					if (!aClass361_5383.aBoolean3102) {
						int i_7_ = aHa_Sub3_3121.anInt4134 % 4000 * 16 / 4000;
						aHa_Sub3_3121.method1316((aClass361_5383.aClass69_Sub1Array3095[i_7_]), (byte) -120);
						OpenGL.glProgramLocalParameter4fARB(34336, 65, 0.0F, 0.0F, 0.0F, 1.0F);
					} else {
						aHa_Sub3_3121.method1316((aClass361_5383.aClass69_Sub2_3097), (byte) -115);
						OpenGL.glProgramLocalParameter4fARB(34336, 65, aFloat5384, 0.0F, 0.0F, 1.0F);
					}
				} else {
					if (!aClass361_5383.aBoolean3102)
						aHa_Sub3_3121.method1316((aClass361_5383.aClass69_Sub1Array3095[0]), (byte) -123);
					else
						aHa_Sub3_3121.method1316((aClass361_5383.aClass69_Sub2_3097), (byte) -118);
					OpenGL.glProgramLocalParameter4fARB(34336, 65, 0.0F, 0.0F, 0.0F, 1.0F);
				}
			} else
				aHa_Sub3_3121.method1316(null, (byte) -107);
			aHa_Sub3_3121.method1330(127, 0);
			if ((i & 0x40) == 0) {
				Class126.aFloatArray1291[2] = aHa_Sub3_3121.aFloat4217 * aHa_Sub3_3121.aFloat4193;
				Class126.aFloatArray1291[1] = aHa_Sub3_3121.aFloat4217 * aHa_Sub3_3121.aFloat4186;
				Class126.aFloatArray1291[0] = aHa_Sub3_3121.aFloat4217 * aHa_Sub3_3121.aFloat4261;
				OpenGL.glProgramLocalParameter4fvARB(34336, 66, Class126.aFloatArray1291, 0);
			} else
				OpenGL.glProgramLocalParameter4fARB(34336, 66, 1.0F, 1.0F, 1.0F, 1.0F);
			int i_8_ = i & 0x3;
			if (i_8_ != 2) {
				if (i_8_ != 3)
					OpenGL.glProgramLocalParameter4fARB(34336, 64, 0.025F, 1.0F, 1.0F, 1.0F);
				else
					OpenGL.glProgramLocalParameter4fARB(34336, 64, 0.1F, 1.0F, 1.0F, 1.0F);
			} else
				OpenGL.glProgramLocalParameter4fARB(34336, 64, 0.05F, 1.0F, 1.0F, 1.0F);
			if (i_5_ != -81)
				anInt5382 = -80;
		}
	}

	Class366_Sub5(ha_Sub3 var_ha_Sub3, Class361 class361) {
		super(var_ha_Sub3);
		aClass361_5383 = class361;
		if (aHa_Sub3_3121.aBoolean4228 && aHa_Sub3_3121.anInt4224 >= 2) {
			aClass99_5385 = (Class296_Sub42.method2919(aHa_Sub3_3121, -127, 34336, "!!ARBvp1.0\nOPTION  ARB_position_invariant;\nATTRIB  iPos         = vertex.position;\nATTRIB  iColour      = vertex.color;\nATTRIB  iTexCoord    = vertex.texcoord[0];\nOUTPUT  oColour      = result.color;\nOUTPUT  oTexCoord0   = result.texcoord[0];\nOUTPUT  oTexCoord1   = result.texcoord[1];\nOUTPUT  oFogCoord    = result.fogcoord;\nPARAM   time         = program.local[65];\nPARAM   turbulence   = program.local[64];\nPARAM   lightAmbient = program.local[66]; \nPARAM   pMatrix[4]   = { state.matrix.projection };\nPARAM   mvMatrix[4]  = { state.matrix.modelview };\nPARAM   ivMatrix[4]  = { state.matrix.texture[1] };\nPARAM   texMatrix[4]  = { state.matrix.texture[0] };\nPARAM   fNoise[64]   = { program.local[0..63] };\nTEMP    noise, viewPos, worldPos, texCoord;\nADDRESS noiseAddr;\nDP4   viewPos.x, mvMatrix[0], iPos;\nDP4   viewPos.y, mvMatrix[1], iPos;\nDP4   viewPos.z, mvMatrix[2], iPos;\nDP4   viewPos.w, mvMatrix[3], iPos;\nMOV   oFogCoord.x, -viewPos.z;\nDP4   worldPos.x, ivMatrix[0], viewPos;\nDP4   worldPos.y, ivMatrix[1], viewPos;\nDP4   worldPos.z, ivMatrix[2], viewPos;\nDP4   worldPos.w, ivMatrix[3], viewPos;\nADD   noise.x, worldPos.x, worldPos.z;SUB   noise.y, worldPos.z, worldPos.x;MUL   noise, noise, 0.0001220703125;\nFRC   noise, noise;\nMUL   noise, noise, 64;\nARL   noiseAddr.x, noise.x;\nMOV   noise.x, fNoise[noiseAddr.x].x;\nARL   noiseAddr.x, noise.y;\nMOV   noise.y, fNoise[noiseAddr.x].y;\nMUL   noise, noise, turbulence.x;\nDP4   texCoord.x, texMatrix[0], iTexCoord;\nDP4   texCoord.y, texMatrix[1], iTexCoord;\nADD   oTexCoord0.xy, texCoord, noise;\nMOV   oTexCoord0.z, 0;\nMOV   oTexCoord0.w, 1;\nMUL   oTexCoord1.xy, texCoord, 0.125;\nMOV   oTexCoord1.zw, time.xxxw;\nMUL   oColour.xyz, iColour, lightAmbient;\nMOV   oColour.w, iColour.w;\nEND"));
			if (aClass99_5385 != null) {
				int[][] is = TranslatableString.method1032(0.4F, 4, 3, 4, 256, 4, 0, false, 64);
				int[][] is_9_ = TranslatableString.method1032(0.4F, 4, 3, 4, 256, 4, 8, false, 64);
				int i = 0;
				aFloatArray5388 = new float[32768];
				for (int i_10_ = 0; i_10_ < 256; i_10_++) {
					int[] is_11_ = is[i_10_];
					int[] is_12_ = is_9_[i_10_];
					for (int i_13_ = 0; i_13_ < 64; i_13_++) {
						aFloatArray5388[i++] = (float) is_11_[i_13_] / 4096.0F;
						aFloatArray5388[i++] = (float) is_12_[i_13_] / 4096.0F;
					}
				}
				method3785((byte) 14);
			}
		}
	}

	static final void method3784(int i) {
		if (i >= -112)
			method3783(46);
		if (!Class296_Sub51_Sub39.aBoolean6549) {
			Class296_Sub51_Sub39.aBoolean6549 = true;
			Mesh.aBoolean1359 = true;
			Class296_Sub36.aFloat4865 += (-Class296_Sub36.aFloat4865 + 24.0F) / 2.0F;
		}
	}

	private final void method3785(byte i) {
		int i_14_ = -119 % ((75 - i) / 36);
		aClass341_5380 = new Class341(aHa_Sub3_3121, 2);
		aClass341_5380.method3627((byte) -99, 0);
		aHa_Sub3_3121.method1330(121, 1);
		aHa_Sub3_3121.method1354(false, -16777216);
		aHa_Sub3_3121.method1306(260, 7681, -22394);
		aHa_Sub3_3121.method1346(true, 770, 0, 34166);
		aHa_Sub3_3121.method1330(106, 0);
		OpenGL.glBindProgramARB(34336, aClass99_5385.anInt1061);
		OpenGL.glEnable(34336);
		aClass341_5380.method3622(-128);
		aClass341_5380.method3627((byte) -104, 1);
		aHa_Sub3_3121.method1330(123, 1);
		OpenGL.glMatrixMode(5890);
		OpenGL.glLoadIdentity();
		OpenGL.glMatrixMode(5888);
		aHa_Sub3_3121.method1272((byte) -107, 0);
		aHa_Sub3_3121.method1346(true, 770, 0, 5890);
		aHa_Sub3_3121.method1330(115, 0);
		OpenGL.glBindProgramARB(34336, 0);
		OpenGL.glDisable(34336);
		OpenGL.glDisable(34820);
		aClass341_5380.method3622(-120);
	}

	static {
		aClass404_5386 = new Class404();
	}
}
