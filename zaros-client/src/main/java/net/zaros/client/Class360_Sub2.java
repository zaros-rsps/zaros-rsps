package net.zaros.client;

/* Class360_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class360_Sub2 extends Class360 {
	static IncomingPacket aClass231_5302 = new IncomingPacket(119, 11);
	private float aFloat5303 = 0.0F;
	static s[] aSArray5304;
	private Class184 aClass184_5305;

	final void method3725(int i) {
		aHa_Sub1_3093.method1151(1, 16760);
		aHa_Sub1_3093.method1177(Js5.aClass125_1411, 9815, Js5.aClass125_1411);
		aHa_Sub1_3093.method1158((byte) -112, 0, Class199.aClass287_2007);
		aHa_Sub1_3093.method1219((byte) 54, Class199.aClass287_2007, 0);
		aHa_Sub1_3093.method1218(11688, 1);
		aHa_Sub1_3093.method1140(null, false);
		aHa_Sub1_3093.method1151(0, 16760);
		aHa_Sub1_3093.method1219((byte) 54, Class199.aClass287_2007, 0);
		int i_0_ = -57 / ((58 - i) / 56);
	}

	static final void method3737(boolean bool) {
		Class338_Sub2.mapLoadType = 0;
		ByteStream class296_sub17_sub1 = Class296_Sub45_Sub2.aClass204_6277.in_stream;
		boolean bool_1_ = class296_sub17_sub1.readUnsignedByteC() == 1;
		int i = class296_sub17_sub1.readUnsignedLEShort();
		int i_2_ = class296_sub17_sub1.readUnsignedLEShortA();
		if (bool != true)
			aSArray5304 = null;
		int i_3_ = class296_sub17_sub1.readUnsignedByteS();
		Class384.clearNpcsOnCondition();
		Class41_Sub28.setMapSize(i_3_);
		int i_4_ = ((-class296_sub17_sub1.pos + Class296_Sub45_Sub2.aClass204_6277.protSize) / 16);
		Class355_Sub2.xteaKeys = new int[i_4_][4];
		for (int i_5_ = 0; i_4_ > i_5_; i_5_++) {
			for (int i_6_ = 0; i_6_ < 4; i_6_++)
				Class355_Sub2.xteaKeys[i_5_][i_6_] = class296_sub17_sub1.g4();
		}
		ParticleEmitterRaw.aByteArrayArray1772 = null;
		Class56.anIntArray659 = new int[i_4_];
		Class188.anIntArray1924 = new int[i_4_];
		StaticMethods.anIntArray1844 = new int[i_4_];
		Class107_Sub1.aByteArrayArray5667 = new byte[i_4_][];
		Class296_Sub43.anIntArray4940 = new int[i_4_];
		StaticMethods.aByteArrayArray3167 = new byte[i_4_][];
		Class243.anIntArray2317 = null;
		Class4.anIntArray70 = new int[i_4_];
		Class166.aByteArrayArray1691 = new byte[i_4_][];
		Class296_Sub51_Sub31.aByteArrayArray6509 = new byte[i_4_][];
		i_4_ = 0;
		for (int i_7_ = (i_2_ - (Class198.currentMapSizeX >> 4)) / 8; i_7_ <= (i_2_ + (Class198.currentMapSizeX >> 4)) / 8; i_7_++) {
			for (int i_8_ = (-(Class296_Sub38.currentMapSizeY >> 4) + i) / 8; (i + (Class296_Sub38.currentMapSizeY >> 4)) / 8 >= i_8_; i_8_++) {
				Class296_Sub43.anIntArray4940[i_4_] = i_8_ + (i_7_ << 8);
				Class56.anIntArray659[i_4_] = Class324.fs5.getFileIndex("m" + i_7_ + "_" + i_8_);
				Class4.anIntArray70[i_4_] = Class324.fs5.getFileIndex("l" + i_7_ + "_" + i_8_);
				Class188.anIntArray1924[i_4_] = Class324.fs5.getFileIndex("um" + i_7_ + "_" + i_8_);
				StaticMethods.anIntArray1844[i_4_] = Class324.fs5.getFileIndex("ul" + i_7_ + "_" + i_8_);
				i_4_++;
			}
		}
		StaticMethods.method1010(0, 12, i, bool_1_, i_2_);
	}

	final void method3730(boolean bool) {
		if (aHa_Sub1_3093.method1114(bool) == 0) {
			Class373_Sub2 class373_sub2 = aHa_Sub1_3093.method1226(-23501);
			aHa_Sub1_3093.method1151(1, 16760);
			Class373_Sub2 class373_sub2_9_ = aHa_Sub1_3093.method1153(98);
			class373_sub2_9_.method3915(class373_sub2);
			class373_sub2_9_.method3927(1.0F, 0.125F, 0.125F, (byte) 62);
			class373_sub2_9_.method3946(0.0F, aFloat5303, 16383, 0.0F);
			aHa_Sub1_3093.method1181(Class2.aClass230_62, (byte) 125);
			aHa_Sub1_3093.method1151(0, 16760);
		}
	}

	public static void method3738(int i) {
		if (i != 0)
			aSArray5304 = null;
		aClass231_5302 = null;
		aSArray5304 = null;
	}

	final void method3731(boolean bool, byte i) {
		aHa_Sub1_3093.method1177(Js5.aClass125_1411, 9815, Class41_Sub4.aClass125_3745);
		if (i != -71)
			aClass184_5305 = null;
	}

	Class360_Sub2(ha_Sub1 var_ha_Sub1, Class184 class184) {
		super(var_ha_Sub1);
		aClass184_5305 = class184;
	}

	final boolean method3723(byte i) {
		int i_10_ = 89 % ((-49 - i) / 36);
		return aClass184_5305.method1859(16);
	}

	final void method3736(byte i, Interface6 interface6, int i_11_) {
		int i_12_ = -117 % ((72 - i) / 49);
		aHa_Sub1_3093.method1140(interface6, false);
	}

	final void method3732(int i, int i_13_, int i_14_) {
		aHa_Sub1_3093.method1151(1, 16760);
		if ((i_14_ & 0x80) != 0)
			aHa_Sub1_3093.method1140(null, false);
		else if ((i & 0x1) != 1) {
			if (aClass184_5305.aBoolean1883)
				aHa_Sub1_3093.method1140(aClass184_5305.anInterface6_Impl2_1886, false);
			else
				aHa_Sub1_3093.method1140((aClass184_5305.anInterface6_Impl1Array1887[0]), false);
		} else if (aClass184_5305.aBoolean1883) {
			aFloat5303 = (float) (aHa_Sub1_3093.anInt4006 % 4000) / 4000.0F;
			aHa_Sub1_3093.method1140(aClass184_5305.anInterface6_Impl2_1886, false);
		} else {
			int i_15_ = aHa_Sub1_3093.anInt4006 % 4000 * 16 / 4000;
			aHa_Sub1_3093.method1140((aClass184_5305.anInterface6_Impl1Array1887[i_15_]), false);
		}
		if (i_13_ >= -6)
			method3732(27, -64, -72);
		aHa_Sub1_3093.method1151(0, 16760);
	}

	final void method3733(byte i, boolean bool) {
		aHa_Sub1_3093.method1151(1, 16760);
		if (i > -125)
			method3736((byte) -41, null, -63);
		aHa_Sub1_3093.method1177(Class41_Sub1.aClass125_3735, 9815, Class41_Sub4.aClass125_3745);
		aHa_Sub1_3093.method1170(0, Class199.aClass287_2007, false, true, (byte) -62);
		aHa_Sub1_3093.method1219((byte) 54, Class153.aClass287_1578, 0);
		aHa_Sub1_3093.method1218(11688, 0);
		aHa_Sub1_3093.method1151(0, 16760);
		aHa_Sub1_3093.method1139(0, -16777216);
		aHa_Sub1_3093.method1219((byte) 54, Class151.aClass287_1553, 0);
		method3730(false);
	}
}
