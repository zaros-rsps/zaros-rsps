package net.zaros.client;

/* Class296_Sub39_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class HardReferenceWrapper extends ReferenceWrapper {
	static Class161 aClass161_6694 = new Class161(8, 0, 4, 1);
	static boolean aBoolean6695;
	static float aFloat6696 = 1.0F;
	private Object anObject6697;
	static NodeDeque aClass155_6698 = new NodeDeque();
	static Class258 aClass258_6699 = new Class258();
	static Class262 aClass262_6700;

	HardReferenceWrapper(Object object, int i) {
		super(i);
		anObject6697 = object;
	}

	static final void method2792(byte i, int i_0_) {
		World.anInt1159 = i_0_;
		if (i < 59)
			method2795((byte) 83);
		Class338_Sub3_Sub1_Sub4.aClass113_6641.clear();
	}

	final boolean isSoft(boolean bool) {
		if (bool != true)
			anObject6697 = null;
		return false;
	}

	static final void method2793(int i, Class296_Sub39_Sub1 class296_sub39_sub1) {
		class296_sub39_sub1.queue_unlink();
		boolean bool = false;
		if (i <= 61)
			aClass161_6694 = null;
		for (Class296_Sub39_Sub1 class296_sub39_sub1_1_ = ((Class296_Sub39_Sub1) Class152.aClass145_1558.getFront()); class296_sub39_sub1_1_ != null; class296_sub39_sub1_1_ = ((Class296_Sub39_Sub1) Class152.aClass145_1558.getNext())) {
			if (Class395.method4064(-1001, class296_sub39_sub1.method2785((byte) 106), class296_sub39_sub1_1_.method2785((byte) 113))) {
				bool = true;
				Class296_Sub15_Sub3.insertNode(class296_sub39_sub1, 825, class296_sub39_sub1_1_);
				break;
			}
		}
		if (!bool)
			Class152.aClass145_1558.insert(class296_sub39_sub1, -2);
	}

	public static void method2794(byte i) {
		aClass161_6694 = null;
		aClass258_6699 = null;
		aClass262_6700 = null;
		if (i > -7)
			method2795((byte) 48);
		aClass155_6698 = null;
	}

	final Object getReference(int i) {
		if (i <= 66)
			aBoolean6695 = true;
		return anObject6697;
	}

	static final void method2795(byte i) {
		if (i < -123) {
			Class352.aClass263_3043.clear();
			Class98.aClass155_1055.method1581(327680);
			Class41_Sub14.aClass45Array3777 = null;
			InputStream_Sub1.aClass218Array34 = null;
			Class294.anInt2687 = 1;
			if (Class368_Sub15.aBoolean5523) {
				Class368_Sub15.aBoolean5523 = false;
				Class125.aShort1286 = Class241.aShort2299;
				Class296_Sub27.aShort4784 = Class296_Sub51_Sub6.aShort6369;
				BillboardRaw.aShort1074 = Class296_Sub13.aShort4655;
				Class296_Sub14.aShort4659 = Class93.aShort1008;
			}
			PlayerEquipment.anInt3375 = -1;
			Class296_Sub39_Sub10.aClass368Array6179 = null;
			Class69_Sub3.anInt5728 = 0;
			Class88.aClass220Array946 = null;
			Class379.aClass302Array3624 = null;
			Class41_Sub19.aClass306_3792 = null;
			Class379_Sub2_Sub1.anInt6609 = 0;
		}
	}
}
