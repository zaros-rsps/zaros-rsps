package net.zaros.client;
/* Class42_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Dimension;

final class Class42_Sub1 extends Class42 {
	private int anInt3824;
	private int anInt3825;
	private int anInt3826;
	private int anInt3827;
	private int anInt3828;
	private int anInt3829;
	private int anInt3830;
	private int anInt3831;
	static SubInPacket aClass260_3832 = new SubInPacket(3, 19);
	static AdvancedMemoryCache aClass113_3833 = new AdvancedMemoryCache(64);
	static int anInt3834;
	static int anInt3835;

	static final void method532(int i) {
		if (CS2Script.anApplet6140 != null && i == 6231) {
			try {
				String string = CS2Script.anApplet6140.getParameter("cookiehost");
				int i_0_ = (int) (Class72.method771(i - 6344) / 86400000L) - 11745;
				String string_1_ = ("usrdob=" + i_0_ + "; version=1; path=/; domain=" + string);
				Class297.method3232("document.cookie=\"" + string_1_ + "\"", CS2Script.anApplet6140, 23346);
			} catch (Throwable throwable) {
				/* empty */
			}
		}
	}

	final void method529(int i, int i_2_, int i_3_) {
		int i_4_ = i_2_ * anInt3824 >> 12;
		int i_5_ = anInt3827 * i >> 12;
		int i_6_ = anInt3830 * i_2_ >> 12;
		if (i_3_ > -11)
			anInt3831 = -113;
		int i_7_ = anInt3826 * i >> 12;
		int i_8_ = i_2_ * anInt3825 >> 12;
		int i_9_ = anInt3829 * i >> 12;
		int i_10_ = i_2_ * anInt3828 >> 12;
		int i_11_ = i * anInt3831 >> 12;
		ReferenceTable.method839(i_11_, 126, i_6_, i_5_, i_10_, i_7_, anInt397, i_8_, i_9_, i_4_);
	}

	final void method528(boolean bool, int i, int i_12_) {
		if (bool)
			anInt3834 = -11;
	}

	Class42_Sub1(int i, int i_13_, int i_14_, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_) {
		super(-1, i_20_, i_21_);
		anInt3828 = i_18_;
		anInt3829 = i_17_;
		anInt3826 = i_15_;
		anInt3824 = i;
		anInt3827 = i_13_;
		anInt3831 = i_19_;
		anInt3830 = i_14_;
		anInt3825 = i_16_;
	}

	final void method531(int i, int i_22_, int i_23_) {
		if (i != 68)
			aClass113_3833 = null;
	}

	static final void method533(boolean bool) {
		if (!Class41_Sub13.aHa3774.p())
			Class33.method348(!bool, false, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(117));
		else {
			Class41_Sub13.aHa3774.a(Class230.aCanvas2209);
			Canvas_Sub1.method124((byte) 25);
			if (!Class368_Sub5_Sub2.aBoolean6597) {
				Dimension dimension = Class230.aCanvas2209.getSize();
				Class41_Sub13.aHa3774.b(Class230.aCanvas2209, dimension.width, dimension.height);
			} else
				Class299.method3240(Class230.aCanvas2209, 9527);
			Class41_Sub13.aHa3774.b(Class230.aCanvas2209);
		}
		Class366_Sub8.method3794(bool);
		Class41.aBoolean388 = true;
	}

	public static void method534(byte i) {
		if (i != 65)
			aClass113_3833 = null;
		aClass260_3832 = null;
		aClass113_3833 = null;
	}
}
