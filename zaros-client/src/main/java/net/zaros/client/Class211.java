package net.zaros.client;

/* Class211 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class211 {
	static Js5 aClass138_2099;
	private long aLong2100;
	private int anInt2101;
	static float aFloat2102;

	private final int method2011(int i, int i_0_) {
		if (i > -80)
			return 53;
		return (int) (aLong2100 >> Class356.anInt3080 * i_0_) & 0xf;
	}

	static final String method2012(int i, byte[] is, int i_1_, int i_2_) {
		if (i != 192)
			method2014(57);
		char[] cs = new char[i_1_];
		int i_3_ = 0;
		int i_4_ = i_2_;
		int i_5_ = i_2_ + i_1_;
		while (i_5_ > i_4_) {
			int i_6_ = is[i_4_++] & 0xff;
			int i_7_;
			if (i_6_ < 128) {
				if (i_6_ != 0)
					i_7_ = i_6_;
				else
					i_7_ = 65533;
			} else if (i_6_ < 192)
				i_7_ = 65533;
			else if (i_6_ >= 224) {
				if (i_6_ < 240) {
					if (i_4_ + 1 < i_5_ && (is[i_4_] & 0xc0) == 128 && (is[i_4_ + 1] & 0xc0) == 128) {
						i_7_ = (i_6_ << 12 & 0xf000 | (is[i_4_++] & 0x3f) << 6 | is[i_4_++] & 0x3f);
						if (i_7_ < 2048)
							i_7_ = 65533;
					} else
						i_7_ = 65533;
				} else if (i_6_ < 248) {
					if (i_4_ + 2 < i_5_ && (is[i_4_] & 0xc0) == 128 && (is[i_4_ + 1] & 0xc0) == 128 && (is[i_4_ + 2] & 0xc0) == 128) {
						i_7_ = (is[i_4_++] << 12 & 0x3f000 | i_6_ << 18 & 0x1c0000 | is[i_4_++] << 6 & 0xfc0 | is[i_4_++] & 0x3f);
						if (i_7_ >= 65536 && i_7_ <= 1114111)
							i_7_ = 65533;
						else
							i_7_ = 65533;
					} else
						i_7_ = 65533;
				} else
					i_7_ = 65533;
			} else if (i_5_ <= i_4_ || (is[i_4_] & 0xc0) != 128)
				i_7_ = 65533;
			else {
				i_7_ = i_6_ << 6 & 0x7c0 | is[i_4_++] & 0x3f;
				if (i_7_ < 128)
					i_7_ = 65533;
			}
			cs[i_3_++] = (char) i_7_;
		}
		return new String(cs, 0, i_3_);
	}

	private final void method2013(int i, Class356 class356) {
		if (i == 31633)
			aLong2100 |= (long) (class356.anInt3070 << anInt2101++ * Class356.anInt3080);
	}

	public static void method2014(int i) {
		aClass138_2099 = null;
		if (i < 57)
			aClass138_2099 = null;
	}

	final int method2015(int i) {
		if (i != 192)
			method2015(76);
		return anInt2101;
	}

	static final Class296_Sub45_Sub4 method2016(int i, Class296_Sub45_Sub4 class296_sub45_sub4) {
		Class296_Sub45_Sub4 class296_sub45_sub4_8_ = (class296_sub45_sub4 != null ? new Class296_Sub45_Sub4(class296_sub45_sub4) : new Class296_Sub45_Sub4());
		if (i != -24345)
			return null;
		class296_sub45_sub4_8_.method2998(-1444068849, 9, 128);
		return class296_sub45_sub4_8_;
	}

	static final void method2017(byte i) {
		if (ClipData.aClass190ArrayArray184 != null) {
			for (int i_9_ = 0; ClipData.aClass190ArrayArray184.length > i_9_; i_9_++) {
				for (int i_10_ = 0; ClipData.aClass190ArrayArray184[i_9_].length > i_10_; i_10_++)
					ClipData.aClass190ArrayArray184[i_9_][i_10_] = Class218.aClass190_2125;
			}
		}
		if (i != 59)
			aClass138_2099 = null;
	}

	final Class356 method2018(boolean bool, int i) {
		if (bool != true)
			method2017((byte) -38);
		return Class356.method3707(method2011(-116, i), -17);
	}

	Class211(Class356 class356) {
		aLong2100 = (long) class356.anInt3070;
		anInt2101 = 1;
	}

	Class211(Class356[] class356s) {
		for (int i = 0; i < class356s.length; i++)
			method2013(31633, class356s[i]);
	}
}
