package net.zaros.client;

/* Class296_Sub34_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub34_Sub2 extends Class296_Sub34 {
	static int anInt6096 = -1;
	int anInt6097;
	static Class294 aClass294_6098;
	int anInt6099;
	int anInt6100;
	static boolean[] aBooleanArray6101 = new boolean[200];
	long aLong6102;
	int anInt6103;
	static OutgoingPacket aClass311_6104;
	static ModeWhere localModeWhere;
	static float aFloat6106;

	@Override
	final int method2732(int i) {
		if (i != -6) {
			method2736(-15);
		}
		return anInt6100;
	}

	@Override
	final int method2738(int i) {
		if (i != 4) {
			aClass294_6098 = null;
		}
		return anInt6103;
	}

	static final void method2739(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, byte i_6_,
			ha var_ha) {
		if (i_6_ >= -3) {
			method2740(null, -51, null);
		}
		Interface14 interface14 = (Interface14) StaticMethods.method2730(i_0_, i, i_1_);
		if (interface14 != null) {
			ObjectDefinition class70 = Class379.objectDefinitionLoader
					.getObjectDefinition(interface14.method57((byte) 114));
			int i_7_ = interface14.method59(100) & 0x3;
			int i_8_ = interface14.method54(-11077);
			if (class70.anInt765 == -1) {
				int i_9_ = i_5_;
				if (class70.interactonType > 0) {
					i_9_ = i_2_;
				}
				if (i_8_ == 0 || i_8_ == 2) {
					if (i_7_ == 0) {
						var_ha.method1095(i_3_, i_4_, i_9_, 4, 107);
					} else if (i_7_ == 1) {
						var_ha.method1089(4, i_9_, i_3_, i_4_, -100);
					} else if (i_7_ == 2) {
						var_ha.method1095(i_3_, i_4_ + 3, i_9_, 4, 123);
					} else if (i_7_ == 3) {
						var_ha.method1089(4, i_9_, i_3_ + 3, i_4_, -101);
					}
				}
				if (i_8_ == 3) {
					if (i_7_ == 0) {
						var_ha.method1088(i_4_, 1, i_9_, 1, i_3_, 1);
					} else if (i_7_ == 1) {
						var_ha.method1088(i_4_ + 3, 1, i_9_, 1, i_3_, 1);
					} else if (i_7_ == 2) {
						var_ha.method1088(i_4_ + 3, 1, i_9_, 1, i_3_ + 3, 1);
					} else if (i_7_ == 3) {
						var_ha.method1088(i_4_, 1, i_9_, 1, i_3_ + 3, 1);
					}
				}
				if (i_8_ == 2) {
					if (i_7_ != 0) {
						if (i_7_ == 1) {
							var_ha.method1095(i_3_, i_4_ + 3, i_9_, 4, 112);
						} else if (i_7_ == 2) {
							var_ha.method1089(4, i_9_, i_3_ + 3, i_4_, -119);
						} else if (i_7_ == 3) {
							var_ha.method1095(i_3_, i_4_, i_9_, 4, 120);
						}
					} else {
						var_ha.method1089(4, i_9_, i_3_, i_4_, -121);
					}
				}
			} else {
				Class8.method189(i_4_, 103, i_3_, i_7_, var_ha, class70);
			}
		}
		interface14 = (Interface14) Class123_Sub2.method1070(i_0_, i, i_1_, Interface14.class);
		if (interface14 != null) {
			ObjectDefinition class70 = Class379.objectDefinitionLoader
					.getObjectDefinition(interface14.method57((byte) 110));
			int i_10_ = interface14.method59(31) & 0x3;
			int i_11_ = interface14.method54(-11077);
			if (class70.anInt765 == -1) {
				if (i_11_ == 9) {
					int i_12_ = -1118482;
					if (class70.interactonType > 0) {
						i_12_ = -1179648;
					}
					if (i_10_ == 0 || i_10_ == 2) {
						var_ha.method1094(i_3_, i_3_ + 3, i_4_ + 3, i_4_, 126, i_12_);
					} else {
						var_ha.method1094(i_3_ + 3, i_3_, i_4_ + 3, i_4_, 125, i_12_);
					}
				}
			} else {
				Class8.method189(i_4_, -128, i_3_, i_10_, var_ha, class70);
			}
		}
		interface14 = (Interface14) Class296_Sub15_Sub3.method2540(i_0_, i, i_1_);
		if (interface14 != null) {
			ObjectDefinition class70 = Class379.objectDefinitionLoader
					.getObjectDefinition(interface14.method57((byte) 98));
			int i_13_ = interface14.method59(95) & 0x3;
			if (class70.anInt765 != -1) {
				Class8.method189(i_4_, 114, i_3_, i_13_, var_ha, class70);
			}
		}
	}

	static final void method2740(Object[] objects, int i, int[] is) {
		if (i == 4) {
			Class123_Sub1_Sub1.method1063(2, objects, is.length - 1, is, 0);
		}
	}

	static final boolean method2741(int i, int i_14_, byte i_15_) {
		if (i_15_ > -39) {
			method2739(7, 83, 60, 22, -64, 97, -101, (byte) 10, null);
		}
		return (Class296_Sub44.method2928(255, i, i_14_) | MaterialRaw.method1673(i, i_14_, (byte) -5)
				| Class41_Sub26.method500(i_14_, 92, i)) & Class296_Sub39_Sub9.method2831(62, i, i_14_);
	}

	@Override
	final int method2735(int i) {
		if (i >= -77) {
			anInt6103 = 66;
		}
		return anInt6099;
	}

	@Override
	final int method2736(int i) {
		if (i != -6) {
			return -71;
		}
		return anInt6097;
	}

	public static void method2742(int i) {
		if (i != 1) {
			method2742(47);
		}
		aClass311_6104 = null;
		localModeWhere = null;
		aClass294_6098 = null;
		aBooleanArray6101 = null;
	}

	@Override
	final long method2737(int i) {
		if (i != -3) {
			anInt6099 = -23;
		}
		return aLong6102;
	}

	static {
		aClass294_6098 = new Class294(0, 1);
		aClass311_6104 = new OutgoingPacket(42, 17);
		localModeWhere = new ModeWhere("LOCAL", "", "local", 4);
	}
}
