package net.zaros.client;

import net.zaros.client.configs.objtype.ObjCustomisation;
import net.zaros.client.configs.objtype.ObjType;

/* Class338_Sub3_Sub1_Sub3_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Player extends Mobile {
	int combatLevelWithSummoning;
	private byte titleID;
	int someFacingDirection;
	boolean aBoolean6868;
	int anInt6869;
	boolean aBoolean6870;
	private byte gender = 0;
	static InterfaceComponent aClass51_6872 = null;
	int prayerICON;
	boolean hasDisplayName;
	int anInt6875;
	int anInt6876;
	String displayName;
	int team;
	boolean aBoolean6879;
	private int renderEmote;
	int anInt6881;
	PlayerEquipment composite;
	int moveX;
	private byte aByte6884;
	boolean somethingWithIgnores;
	int combatLevel;
	int anInt6887;
	int anInt6888;
	int moveY;
	int anInt6890;
	boolean needUpdateMoving;
	String name;
	int skullICON;
	int anInt6894;

	@Override
	final void method3460(int i, ha var_ha) {
		if (composite != null && (aBoolean6835 || method3530(0, var_ha, 76))) {
			Class373 class373 = var_ha.f();
			class373.method3906(aClass5_6804.method175((byte) -81));
			class373.method3904(tileX, anInt5213 - 5, tileY);
			method3500(aBoolean6835, aClass178Array6828, true, var_ha, class373);
			for (int i_0_ = 0; i_0_ < aClass178Array6828.length; i_0_++) {
				aClass178Array6828[i_0_] = null;
			}
			int i_1_ = -83 % ((i + 41) / 62);
		}
	}

	final void addWayPoint(byte walkType, int mapX, int mapY) {
		if (currentWayPoint < waypointQueueX.length - 1) {
			currentWayPoint++;
		}
		for (int i = currentWayPoint; i > 0; i--) {
			waypointQueueX[i] = waypointQueueX[i - 1];
			wayPointQueueY[i] = wayPointQueueY[i - 1];
			walkingTypes[i] = walkingTypes[i - 1];
		}
		waypointQueueX[0] = mapX;
		wayPointQueueY[0] = mapY;
		walkingTypes[0] = walkType;
	}

	@Override
	final boolean method3468(int i) {
		if (i > -29) {
			return false;
		}
		return false;
	}

	@Override
	final void method3472(byte i) {
		int i_7_ = -14 % ((-56 - i) / 38);
		throw new IllegalStateException();
	}

	final String getName(boolean displayname) {
		if (displayname) {
			return name;
		}
		return displayName;
	}

	final void updatePosition(int mapX, int mapY, byte walkingType) {
		if (aClass44_6802.method570((byte) 40) && aClass44_6802.method563(-1).walkingProperties == 1) {
			anIntArray6789 = null;
			aClass44_6802.method549((byte) 115, -1);
		}
		for (int i_11_ = 0; i_11_ < aClass212Array6817.length; i_11_++) {
			if (aClass212Array6817[i_11_].anInt2105 != -1) {
				Graphic gfx = Class157.graphicsLoader.getGraphic(aClass212Array6817[i_11_].anInt2105);
				if (gfx.aBoolean267 && gfx.anInt264 != -1 && Class296_Sub51_Sub13.animationsLoader.getAnimation(gfx.anInt264, (byte) -124).walkingProperties == 1) {
					aClass212Array6817[i_11_].aClass44_2108.method549((byte) 115, -1);
					aClass212Array6817[i_11_].anInt2105 = -1;
				}
			}
		}
		someFacingDirection = -1;
		if (mapX < 0 || Class198.currentMapSizeX <= mapX || mapY < 0 || mapY >= Class296_Sub38.currentMapSizeY) {
			setPosition(mapX, mapY);
		} else if (waypointQueueX[0] < 0 || Class198.currentMapSizeX <= waypointQueueX[0] || wayPointQueueY[0] < 0 || Class296_Sub38.currentMapSizeY <= wayPointQueueY[0]) {
			setPosition(mapX, mapY);
		} else {
			if (walkingType == 2) {
				Class296_Sub51_Sub26.clippedWalk(this, mapX, mapY, (byte) 2);
			}
			addWayPoint(walkingType, mapX, mapY);
		}
	}

	private final void method3523(int i, ha var_ha, Class373 class373, int i_12_, int i_13_, int i_14_, Model class178, byte i_15_) {
		int i_16_ = i_13_ * i_13_ + i * i;
		if (i_16_ >= 262144 && i_12_ >= i_16_) {
			int i_17_ = (int) (Math.atan2(i_13_, i) * 2607.5945876176133 - aClass5_6804.method175((byte) -81)) & 0x3fff;
			if (i_15_ <= 74) {
				method3460(-2, null);
			}
			Model class178_18_ = NPCDefinitionLoader.method1420(anInt6795, var_ha, (byte) 122, anInt6769, anInt6801, i_14_, i_17_);
			if (class178_18_ != null) {
				var_ha.C(false);
				class178_18_.method1715(class373, null, 0);
				var_ha.C(true);
			}
		}
	}

	@Override
	final Class96 method3461(ha var_ha, int i) {
		int i_19_ = 8 % ((i - 79) / 44);
		return null;
	}

	Player(int i) {
		super(i);
		someFacingDirection = -1;
		prayerICON = -1;
		aBoolean6879 = false;
		anInt6876 = -1;
		anInt6875 = -1;
		anInt6869 = -1;
		anInt6881 = 0;
		anInt6888 = 0;
		aBoolean6870 = false;
		combatLevel = 0;
		titleID = (byte) 0;
		combatLevelWithSummoning = 0;
		anInt6887 = 255;
		aBoolean6868 = false;
		aByte6884 = (byte) 0;
		team = 0;
		somethingWithIgnores = false;
		anInt6890 = -1;
		skullICON = -1;
		hasDisplayName = false;
		needUpdateMoving = false;
		anInt6894 = -1;
	}

	@Override
	final boolean method3475(int i, int i_20_, ha var_ha, int i_21_) {
		if (composite == null || !method3530(131072, var_ha, 119)) {
			return false;
		}
		Class373 class373 = var_ha.f();
		int i_22_ = aClass5_6804.method175((byte) -81);
		class373.method3906(i_22_);
		class373.method3904(tileX, anInt5213, tileY);
		boolean bool = false;
		if (i_20_ > -48) {
			aBoolean6870 = false;
		}
		for (Model element : aClass178Array6828) {
			if (element != null && (!Class296_Sub39_Sub10.aBoolean6177 ? element.method1732(i_21_, i, class373, true, 0) : element.method1731(i_21_, i, class373, true, 0, ModeWhat.anInt1192))) {
				bool = true;
				break;
			}
		}
		for (int i_24_ = 0; i_24_ < aClass178Array6828.length; i_24_++) {
			aClass178Array6828[i_24_] = null;
		}
		return bool;
	}

	final void method3524(String string, int i, int i_25_, int i_26_) {
		if (i_26_ > 116) {
			method3507(i, string, -128, i_25_, Class338_Sub3_Sub4.method3566((byte) 43) * NPCDefinition.aClass268_1478.anInt2487);
		}
	}

	final String getFullName(boolean isDisplay) {
		String n = "";
		if (BITConfigDefinition.aStringArray2605 != null) {
			n += BITConfigDefinition.aStringArray2605[aByte6884];
		}
		int[] is;
		if (gender == 1 && ByteStream.anIntArray6043 != null) {
			is = ByteStream.anIntArray6043;
		} else {
			is = ObjectDefinition.anIntArray771;
		}
		if (is != null && is[aByte6884] != -1) {
			Class277 class277 = Class85.aClass350_927.method3677(is[aByte6884], -6115);
			if (class277.aChar2534 == 's') {
				n += class277.method2325((byte) 121, titleID & 0xff);
			} else {
				Class219_Sub1.method2062("gdn1", (byte) 1, new Throwable());
				is[aByte6884] = -1;
			}
		}
		if (!isDisplay) {
			n += displayName;
		} else {
			n += name;
		}
		if (Mobile.aStringArray6793 != null) {
			n += Mobile.aStringArray6793[aByte6884];
		}
		return n;
	}

	@Override
	final Class338_Sub2 method3473(ha var_ha, byte i) {
		if (composite == null || !method3530(2048, var_ha, 111)) {
			return null;
		}
		Class373 class373 = var_ha.f();
		int i_27_ = aClass5_6804.method175((byte) -81);
		class373.method3906(i_27_);
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[z][tileX >> Class313.anInt2779][tileY >> Class313.anInt2779];
		if (class247 != null && class247.aClass338_Sub3_Sub5_2346 != null) {
			int i_28_ = -class247.aClass338_Sub3_Sub5_2346.aShort6573 + anInt6770;
			anInt6770 -= i_28_ / 10.0F;
		} else {
			anInt6770 -= anInt6770 / 10.0F;
		}
		class373.method3904(tileX, anInt5213 - 20 - anInt6770, tileY);
		if (i > -84) {
			aByte6884 = (byte) -60;
		}
		aBoolean6834 = false;
		Class338_Sub2 class338_sub2 = null;
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub16_5019.method453(123) == 1) {
			Class280 class280 = method3516(false);
			if (class280.aBoolean2591 && (composite.morphismId == -1 || Class352.npcDefinitionLoader.getDefinition(composite.morphismId).aBoolean1457)) {
				Animator class44 = !aClass44_6802.method570((byte) 40) || !aClass44_6802.method567(1) ? null : aClass44_6802;
				Animator class44_29_ = aClass44_6777.method570((byte) 40) && (!aBoolean6783 || class44 == null) ? aClass44_6777 : null;
				Model class178 = Class382.method4007(var_ha, anInt6795, anInt6769, 0, 1, anInt6801, (byte) 123, class44_29_ != null ? class44_29_ : class44, aClass178Array6828[0], 240, i_27_, 0, 160);
				if (class178 != null) {
					class338_sub2 = Class144.method1481(aClass178Array6828.length + 1, true, true);
					aBoolean6834 = true;
					var_ha.C(false);
					if (Class296_Sub39_Sub10.aBoolean6177) {
						class178.method1719(class373, class338_sub2.aClass338_Sub5Array5194[aClass178Array6828.length], ModeWhat.anInt1192, 0);
					} else {
						class178.method1715(class373, class338_sub2.aClass338_Sub5Array5194[aClass178Array6828.length], 0);
					}
					var_ha.C(true);
				}
			}
		}
		if (this == Class296_Sub51_Sub11.localPlayer) {
			for (int i_30_ = Class338_Sub2.aClass225Array5200.length - 1; i_30_ >= 0; i_30_--) {
				Class225 class225 = Class338_Sub2.aClass225Array5200[i_30_];
				if (class225 != null && class225.anInt2182 != -1) {
					if (class225.iconType == 1) {
						NPCNode class296_sub7 = (NPCNode) Class41_Sub18.localNpcs.get(class225.targetIndex);
						if (class296_sub7 != null) {
							NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
							int i_31_ = class338_sub3_sub1_sub3_sub2.tileX - Class296_Sub51_Sub11.localPlayer.tileX;
							int i_32_ = -Class296_Sub51_Sub11.localPlayer.tileY + class338_sub3_sub1_sub3_sub2.tileY;
							if (!Class296_Sub39_Sub10.aBoolean6177) {
								method3523(i_32_, var_ha, class373, 92160000, i_31_, class225.anInt2182, aClass178Array6828[0], (byte) 112);
							} else {
								method3529(92160000, var_ha, class373, i_31_, class225.anInt2182, i_32_, ModeWhat.anInt1192, aClass178Array6828[0], 5863);
							}
						}
					}
					if (class225.iconType == 2) {
						int i_33_ = class225.anInt2183 - Class296_Sub51_Sub11.localPlayer.tileX;
						int i_34_ = -Class296_Sub51_Sub11.localPlayer.tileY + class225.anInt2176;
						int i_35_ = class225.anInt2179 << 9;
						i_35_ *= i_35_;
						if (!Class296_Sub39_Sub10.aBoolean6177) {
							method3523(i_34_, var_ha, class373, i_35_, i_33_, class225.anInt2182, aClass178Array6828[0], (byte) 106);
						} else {
							method3529(i_35_, var_ha, class373, i_33_, class225.anInt2182, i_34_, ModeWhat.anInt1192, aClass178Array6828[0], 5863);
						}
					}
					if (class225.iconType == 10 && class225.targetIndex >= 0 && PlayerUpdate.visiblePlayers.length > class225.targetIndex) {
						Player class338_sub3_sub1_sub3_sub1_36_ = PlayerUpdate.visiblePlayers[class225.targetIndex];
						if (class338_sub3_sub1_sub3_sub1_36_ != null) {
							int i_37_ = class338_sub3_sub1_sub3_sub1_36_.tileX - Class296_Sub51_Sub11.localPlayer.tileX;
							int i_38_ = class338_sub3_sub1_sub3_sub1_36_.tileY - Class296_Sub51_Sub11.localPlayer.tileY;
							if (!Class296_Sub39_Sub10.aBoolean6177) {
								method3523(i_38_, var_ha, class373, 92160000, i_37_, class225.anInt2182, aClass178Array6828[0], (byte) 78);
							} else {
								method3529(92160000, var_ha, class373, i_37_, class225.anInt2182, i_38_, ModeWhat.anInt1192, aClass178Array6828[0], 5863);
							}
						}
					}
				}
			}
			class373.method3906(i_27_);
			class373.method3904(tileX, anInt5213, tileY);
		}
		class373.method3906(i_27_);
		class373.method3904(tileX, anInt5213 + -anInt6770 - 5, tileY);
		if (class338_sub2 == null) {
			class338_sub2 = Class144.method1481(aClass178Array6828.length, true, true);
		}
		method3500(false, aClass178Array6828, true, var_ha, class373);
		if (Class296_Sub39_Sub10.aBoolean6177) {
			for (int i_39_ = 0; i_39_ < aClass178Array6828.length; i_39_++) {
				if (aClass178Array6828[i_39_] != null) {
					aClass178Array6828[i_39_].method1719(class373, class338_sub2.aClass338_Sub5Array5194[i_39_], ModeWhat.anInt1192, Class296_Sub51_Sub11.localPlayer == this ? 1 : 0);
				}
			}
		} else {
			for (int i_40_ = 0; i_40_ < aClass178Array6828.length; i_40_++) {
				if (aClass178Array6828[i_40_] != null) {
					aClass178Array6828[i_40_].method1715(class373, class338_sub2.aClass338_Sub5Array5194[i_40_], this == Class296_Sub51_Sub11.localPlayer ? 1 : 0);
				}
			}
		}
		if (aClass338_Sub1_6831 != null) {
			Class390 class390 = aClass338_Sub1_6831.method3444();
			if (!Class296_Sub39_Sub10.aBoolean6177) {
				var_ha.a(class390);
			} else {
				var_ha.a(class390, ModeWhat.anInt1192);
			}
		}
		for (int i_41_ = 0; aClass178Array6828.length > i_41_; i_41_++) {
			if (aClass178Array6828[i_41_] != null) {
				aBoolean6834 |= aClass178Array6828[i_41_].F();
			}
			aClass178Array6828[i_41_] = null;
		}
		anInt6781 = Class296_Sub2.anInt4590;
		return class338_sub2;
	}

	@Override
	final void method3467(int i, int i_42_, Class338_Sub3 class338_sub3, boolean bool, int i_43_, int i_44_, ha var_ha) {
		int i_45_ = 125 / ((20 - i_44_) / 48);
		throw new IllegalStateException();
	}

	final void updateAppereance(Packet buff) {
		buff.pos = 0;
		int bitFlag = buff.g1();
		gender = (byte) (bitFlag & 0x1);
		boolean hadDisplayName = hasDisplayName;
		hasDisplayName = (bitFlag & 0x2) != 0;
		boolean pvpZone = (bitFlag & 0x4) != 0;
		int previousSize = super.getSize();
		setSize(((bitFlag & 0x3c) >> 3) + 1);
		aByte6884 = (byte) (bitFlag >> 6 & 0x3);
		tileX += getSize() - previousSize << 8;
		tileY += -previousSize + getSize() << 8;
		titleID = buff.g1b();
		prayerICON = buff.g1b();
		skullICON = buff.g1b();
		somethingWithIgnores = buff.g1b() == 1;
		if (Class41_Sub29.modeWhere == BITConfigsLoader.liveModeWhere && Class338_Sub3_Sub5.rights >= 2) {
			somethingWithIgnores = false;
		}
		team = 0;
		int transformId = -1;
		int recolorMask = 0; // custom
		int[] itemIds = new int[Class338_Sub3_Sub3_Sub1.equipmentData.colourData.length];
		ObjCustomisation[] customisation = new ObjCustomisation[Class338_Sub3_Sub3_Sub1.equipmentData.colourData.length];
		ObjType[] types = new ObjType[Class338_Sub3_Sub3_Sub1.equipmentData.colourData.length];
		for (int index = 0; index < Class338_Sub3_Sub3_Sub1.equipmentData.colourData.length; index++) {
			if (Class338_Sub3_Sub3_Sub1.equipmentData.colourData[index] != 1) {
				int msb = buff.g1();
				if (msb == 0) {
					itemIds[index] = 0;
				} else {
					int lsb = buff.g1();
					int id = (msb << 8) + lsb;
					if (index == 0 && id == 65535) {
						transformId = buff.g2();
						team = buff.g1();
						break;
					}
					if (id >= 16384) {
						id -= 16384;
						itemIds[index] = id | 0x40000000;
						types[index] = Class296_Sub39_Sub1.itemDefinitionLoader.list(id);
						int team = types[index].team;
						if (team != 0) {
							this.team = team;
						}
					} else {
						itemIds[index] = id - 256 | ~0x7fffffff;
					}
				}
			}
		}
		if (transformId == -1) {
			int mask = recolorMask = buff.g2();
			int offset = 0;
			for (int i = 0; i < Class338_Sub3_Sub3_Sub1.equipmentData.colourData.length; i++) {
				if (Class338_Sub3_Sub3_Sub1.equipmentData.colourData[i] == 0) {
					if ((1 << offset & mask) != 0) {
						customisation[i] = Class268.readColourData(buff, types[i]);
					}
					offset++;
				}
			}
		}
		int[] bodyColours = new int[10];
		for (int i = 0; i < 10; i++) {
			int data = buff.g1();
			if (i >= Class42_Sub4.aShortArrayArrayArray3846.length || data < 0 || Class42_Sub4.aShortArrayArrayArray3846[i][0].length <= data) {
				data = 0;
			}
			bodyColours[i] = data;
		}
		renderEmote = buff.g2();
		name = buff.gstr();
		displayName = name;
		if (Class296_Sub51_Sub11.localPlayer == this) {
			Class366_Sub6.name3 = name;
		}
		combatLevel = buff.g1();
		if (!pvpZone) {
			anInt6888 = 0;
			combatLevelWithSummoning = buff.g1();
			anInt6876 = buff.g1();
			if (anInt6876 == 255) {
				anInt6876 = -1;
			}
		} else {
			anInt6888 = buff.g2();
			if (anInt6888 == 65535) {
				anInt6888 = -1;
			}
			combatLevelWithSummoning = combatLevel;
			anInt6876 = -1;
		}
		int i_61_ = anInt6881;
		anInt6881 = buff.g1();
		if (anInt6881 == 0) {
			Class366_Sub6.method3789(this, (byte) -98);
		} else {
			int i_62_ = anInt6869;
			int i_63_ = anInt6890;
			int i_64_ = anInt6894;
			int i_65_ = anInt6875;
			int i_66_ = anInt6887;
			anInt6869 = buff.g2();
			anInt6890 = buff.g2();
			anInt6894 = buff.g2();
			anInt6875 = buff.g2();
			anInt6887 = buff.g1();
			if (!hadDisplayName != !hasDisplayName || anInt6881 != i_61_ || anInt6869 != i_62_ || anInt6890 != i_63_ || anInt6894 != i_64_ || anInt6875 != i_65_ || anInt6887 != i_66_) {
				Class123_Sub2_Sub2.method1076(this, (byte) 125);
			}
		}
		if (composite == null) {
			composite = new PlayerEquipment();
		}
		int oldNPCID = composite.morphismId;
		int[] oldColours = composite.body_colours;
		composite.update(itemIds, bodyColours, gender == 1, customisation, getRenderEmote(), transformId);
		if (oldNPCID != transformId) {
			tileX = (waypointQueueX[0] << 9) + (getSize() << 8);
			tileY = (wayPointQueueY[0] << 9) + (getSize() << 8);
		}
		if (index == Class362.myPlayerIndex && oldColours != null) {
			for (int i = 0; i < bodyColours.length; i++) {
				if (bodyColours[i] != oldColours[i]) {
					Class296_Sub39_Sub1.itemDefinitionLoader.uncacheRequests();
					break;
				}
			}
		}
		if (aClass338_Sub1_6831 != null) {
			aClass338_Sub1_6831.method3440();
		}
		if (aClass44_6777.method570((byte) 40) && aBoolean6783) {
			Class280 class280 = method3516(false);
			if (!class280.method2342(6660, aClass44_6777.method557((byte) -69))) {
				aClass44_6777.method549((byte) 115, -1);
				aBoolean6783 = false;
			}
		}
	}

	@Override
	final Class79 method3497(int i) {
		if (i < 4) {
			combatLevel = -32;
		}
		if (aClass79_6796 != null) {
			if (aClass79_6796.aString886 == null) {
				return null;
			}
			if (StaticMethods.anInt5970 == 0 || StaticMethods.anInt5970 == 3 || StaticMethods.anInt5970 == 1 && Class97.method872(0, displayName)) {
				return aClass79_6796;
			}
		}
		return null;
	}

	@Override
	final boolean method3494(byte i) {
		int i_70_ = 63 % ((35 - i) / 33);
		return NPCDefinition.aClass268_1478.aBoolean2498;
	}

	final void setPosition(int mapX, int mapY) {
		currentWayPoint = 0;
		anInt6836 = 0;
		anInt6829 = 0;
		waypointQueueX[0] = mapX;
		wayPointQueueY[0] = mapY;
		int size = getSize();
		tileX = waypointQueueX[0] * 512 + size * 256;
		tileY = wayPointQueueY[0] * 512 + size * 256;
		if (this == Class296_Sub51_Sub11.localPlayer) {
			Class368_Sub22.method3866(11);
		}
		if (aClass338_Sub1_6831 != null) {
			aClass338_Sub1_6831.method3440();
		}
	}

	public Player() {
		someFacingDirection = -1;
		prayerICON = -1;
		aBoolean6879 = false;
		anInt6876 = -1;
		anInt6875 = -1;
		anInt6869 = -1;
		anInt6881 = 0;
		anInt6888 = 0;
		aBoolean6870 = false;
		combatLevel = 0;
		titleID = (byte) 0;
		combatLevelWithSummoning = 0;
		anInt6887 = 255;
		aBoolean6868 = false;
		aByte6884 = (byte) 0;
		team = 0;
		somethingWithIgnores = false;
		anInt6890 = -1;
		skullICON = -1;
		hasDisplayName = false;
		needUpdateMoving = false;
		anInt6894 = -1;
	}

	@Override
	final int method3505(int i) {
		if (i > -85) {
			aBoolean6879 = false;
		}
		return -1;
	}

	final boolean hasComposite() {
		if (composite == null) {
			return false;
		}
		return true;
	}

	private final void method3529(int i, ha var_ha, Class373 class373, int i_74_, int i_75_, int i_76_, int i_77_, Model class178, int i_78_) {
		if (i_78_ == 5863) {
			int i_79_ = i_74_ * i_74_ + i_76_ * i_76_;
			if (i_79_ >= 262144 && i_79_ <= i) {
				int i_80_ = (int) (Math.atan2(i_74_, i_76_) * 2607.5945876176133 - aClass5_6804.method175((byte) -81)) & 0x3fff;
				Model class178_81_ = NPCDefinitionLoader.method1420(anInt6795, var_ha, (byte) 122, anInt6769, anInt6801, i_75_, i_80_);
				if (class178_81_ != null) {
					var_ha.C(false);
					class178_81_.method1719(class373, null, i_77_, 0);
					var_ha.C(true);
				}
			}
		}
	}

	@Override
	final int getRenderEmote() {
		return renderEmote;
	}

	private final boolean method3530(int i, ha var_ha, int i_82_) {
		int i_83_ = i;
		Class280 class280 = method3516(false);
		Animator class44 = !aClass44_6802.method570((byte) 40) || aClass44_6802.method567(1) ? null : aClass44_6802;
		Animator class44_84_ = aClass44_6777.method570((byte) 40) && !aBoolean6870 && (!aBoolean6783 || class44 == null) ? aClass44_6777 : null;
		int i_85_ = class280.anInt2585;
		int i_86_ = class280.anInt2584;
		if (i_85_ != 0 || i_86_ != 0 || class280.anInt2567 != 0 || class280.anInt2587 != 0) {
			i |= 0x7;
		}
		if (i_82_ <= 74) {
			return false;
		}
		int i_87_ = aClass5_6804.method175((byte) -81);
		boolean bool = aByte6806 != 0 && Class29.anInt307 >= anInt6820 && anInt6803 > Class29.anInt307;
		if (bool) {
			i |= 0x80000;
		}
		Model class178 = aClass178Array6828[0] = composite.method4153(Class16_Sub3_Sub1.configsRegister, Class296_Sub39_Sub1.itemDefinitionLoader, true, class44_84_, Class296_Sub51_Sub13.aClass24_6420, Class352.npcDefinitionLoader, aClass44_Sub1_Sub1Array6814, i_87_, var_ha, 30812, anIntArray6823, Class338_Sub3_Sub3_Sub1.equipmentData, i, class44, Class296_Sub51_Sub13.animationsLoader, Class41_Sub10.aClass62_3768);
		int i_88_ = Class122_Sub1.method1040(21077);
		if (FileWorker.anInt3004 < 96 && i_88_ > 50) {
			Class384.method4016((byte) -116);
		}
		if (BITConfigsLoader.liveModeWhere != Class41_Sub29.modeWhere && i_88_ < 50) {
			int i_89_;
			for (i_89_ = -i_88_ + 50; ISAACCipher.anInt1898 < i_89_; ISAACCipher.anInt1898++) {
				Class367.aByteArrayArray3124[ISAACCipher.anInt1898] = new byte[102400];
			}
			while (ISAACCipher.anInt1898 > i_89_) {
				ISAACCipher.anInt1898--;
				Class367.aByteArrayArray3124[ISAACCipher.anInt1898] = null;
			}
		} else if (BITConfigsLoader.liveModeWhere != Class41_Sub29.modeWhere) {
			ISAACCipher.anInt1898 = 0;
			Class367.aByteArrayArray3124 = new byte[50][];
		}
		if (class178 == null) {
			return false;
		}
		anInt6790 = class178.fa();
		anInt6800 = class178.ma();
		method3503(false, class178);
		if (i_85_ == 0 && i_86_ == 0) {
			method3518(0, true, getSize() << 9, 0, getSize() << 9, i_87_);
		} else {
			method3518(class280.anInt2580, true, i_86_, class280.anInt2556, i_85_, i_87_);
			if (anInt6801 != 0) {
				class178.FA(anInt6801);
			}
			if (anInt6769 != 0) {
				class178.VA(anInt6769);
			}
			if (anInt6795 != 0) {
				class178.H(0, anInt6795, 0);
			}
		}
		if (bool) {
			class178.method1717(aByte6808, aByte6818, aByte6822, aByte6806 & 0xff);
		}
		if (!aBoolean6870) {
			method3504(var_ha, i_85_, i_83_, i_86_, 28149, class280, i_87_);
		}
		return true;
	}

	public static void method3531(int i) {
		if (i == -25357) {
			aClass51_6872 = null;
		}
	}

	@Override
	final int getSize() {
		if (composite != null && composite.morphismId != -1) {
			return Class352.npcDefinitionLoader.getDefinition(composite.morphismId).size;
		}
		return super.getSize();
	}
}
