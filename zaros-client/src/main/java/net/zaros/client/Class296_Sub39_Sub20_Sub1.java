package net.zaros.client;

/* Class296_Sub39_Sub20_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub20_Sub1 extends Class296_Sub39_Sub20 {
	int anInt6722;
	static OutgoingPacket aClass311_6723;
	byte aByte6724;
	static float[] aFloatArray6725 = {1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F};
	Packet aClass296_Sub17_6726;

	static final String method2905(int i, byte i_0_) {
		String string = Integer.toString(i);
		for (int i_1_ = string.length() - 3; i_1_ > 0; i_1_ -= 3)
			string = string.substring(0, i_1_) + "," + string.substring(i_1_);
		if (i_0_ >= -35)
			aClass311_6723 = null;
		if (string.length() > 9)
			return (" <col=00ff80>" + string.substring(0, string.length() - 8) + TranslatableString.aClass120_1229.getTranslation(Class394.langID) + " (" + string + ")</col>");
		if (string.length() > 6)
			return (" <col=ffffff>" + string.substring(0, string.length() - 4) + TranslatableString.aClass120_1231.getTranslation(Class394.langID) + " (" + string + ")</col>");
		return " <col=ffff00>" + string + "</col>";
	}

	static final boolean method2906(int i, int i_2_, boolean bool) {
		if (bool != true)
			aFloatArray6725 = null;
		if ((i_2_ & 0xc580) == 0)
			return false;
		return true;
	}

	final int method2902(int i) {
		if (i != 100)
			return 28;
		if (aClass296_Sub17_6726 == null)
			return 0;
		return (aClass296_Sub17_6726.pos * 100 / (-aByte6724 + aClass296_Sub17_6726.data.length));
	}

	final byte[] method2904(int i) {
		if (i != 4)
			method2902(-34);
		if (aBoolean6256 || (aClass296_Sub17_6726.data.length - aByte6724 > aClass296_Sub17_6726.pos))
			throw new RuntimeException();
		return aClass296_Sub17_6726.data;
	}

	public static void method2907(byte i) {
		aClass311_6723 = null;
		aFloatArray6725 = null;
		if (i != -118)
			method2908((byte) -78);
	}

	static final void method2908(byte i) {
		if (MaterialRaw.aFileOutputStream1776 != null) {
			try {
				MaterialRaw.aFileOutputStream1776.close();
			} catch (java.io.IOException ioexception) {
				/* empty */
			}
		}
		if (i != -103)
			method2907((byte) 101);
		MaterialRaw.aFileOutputStream1776 = null;
	}

	static {
		aClass311_6723 = new OutgoingPacket(37, 4);
	}
}
