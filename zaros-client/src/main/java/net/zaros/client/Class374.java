package net.zaros.client;
import jaclib.memory.Buffer;

import jagdx.IDirect3DIndexBuffer;
import jagdx.aj;

final class Class374 implements Interface15_Impl1 {
	private int anInt5696;
	private int anInt5697;
	private boolean aBoolean5698;
	IDirect3DIndexBuffer anIDirect3DIndexBuffer5699;
	private Class67 aClass67_5700;
	private ha_Sub1_Sub2 aHa_Sub1_Sub2_5701;
	private boolean aBoolean5702 = false;

	public final boolean method33(int i) {
		if (aBoolean5702 && aj.a(anIDirect3DIndexBuffer5699.Unlock(), (int) 98)) {
			aBoolean5702 = false;
			return true;
		}
		if (i != 32185)
			method31((byte) -66);
		return false;
	}

	public final void method31(byte i) {
		if (anIDirect3DIndexBuffer5699 != null) {
			anIDirect3DIndexBuffer5699.b((byte) 83);
			anIDirect3DIndexBuffer5699 = null;
		}
		anInt5697 = 0;
		if (i != 82)
			method33(-75);
		anInt5696 = 0;
	}

	public final Class67 method34(int i) {
		if (i != 16839)
			method32(41, 58);
		return aClass67_5700;
	}

	public final Buffer method30(byte i, boolean bool) {
		if (i > -17)
			aBoolean5702 = false;
		if (anIDirect3DIndexBuffer5699 == null)
			return null;
		bool &= aBoolean5698;
		if (!aBoolean5702 && aj.a(anIDirect3DIndexBuffer5699.Lock(0, anInt5697, bool ? 8192 : 0, (aHa_Sub1_Sub2_5701.aGeometryBuffer5863)), (int) 114)) {
			aBoolean5702 = true;
			return aHa_Sub1_Sub2_5701.aGeometryBuffer5863;
		}
		return null;
	}

	public final int method61(byte i) {
		if (i <= 115)
			return 19;
		return anInt5696;
	}

	public final void method32(int i, int i_0_) {
		anInt5696 = aClass67_5700.anInt741 * i;
		if (i_0_ != -21709)
			method31((byte) -57);
		if (anInt5696 > anInt5697) {
			int i_1_ = 8;
			int i_2_;
			if (!aBoolean5698)
				i_2_ = 1;
			else {
				i_1_ |= 0x200;
				i_2_ = 0;
			}
			if (anIDirect3DIndexBuffer5699 != null)
				anIDirect3DIndexBuffer5699.b((byte) 83);
			anIDirect3DIndexBuffer5699 = (aHa_Sub1_Sub2_5701.anIDirect3DDevice5865.a(anInt5696, i_1_, aClass67_5700 == Class67.aClass67_746 ? 101 : 102, i_2_, anIDirect3DIndexBuffer5699));
			anInt5697 = anInt5696;
		}
	}

	Class374(ha_Sub1_Sub2 var_ha_Sub1_Sub2, Class67 class67, boolean bool) {
		aClass67_5700 = class67;
		aHa_Sub1_Sub2_5701 = var_ha_Sub1_Sub2;
		aBoolean5698 = bool;
	}
}
