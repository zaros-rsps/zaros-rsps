package net.zaros.client;

/* Class296_Sub51_Sub29 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub29 extends TextureOperation {
	private int[] anIntArray6492 = new int[257];
	private int[][] anIntArrayArray6493;
	static Js5 aClass138_6494;

	public static void method3164(int i) {
		aClass138_6494 = null;
		if (i != 4)
			aClass138_6494 = null;
	}

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		if (i > -84)
			aClass138_6494 = null;
		if (i_0_ == 0) {
			int i_1_ = class296_sub17.g1();
			if (i_1_ == 0) {
				anIntArrayArray6493 = new int[class296_sub17.g1()][4];
				for (int i_2_ = 0; i_2_ < anIntArrayArray6493.length; i_2_++) {
					anIntArrayArray6493[i_2_][0] = class296_sub17.g2();
					anIntArrayArray6493[i_2_][1] = class296_sub17.g1() << 4;
					anIntArrayArray6493[i_2_][2] = class296_sub17.g1() << 4;
					anIntArrayArray6493[i_2_][3] = class296_sub17.g1() << 4;
				}
			} else
				method3166(3, i_1_);
		}
	}

	private final void method3165(byte i) {
		if (i < -35) {
			int i_3_ = anIntArrayArray6493.length;
			if (i_3_ > 0) {
				for (int i_4_ = 0; i_4_ < 257; i_4_++) {
					int i_5_ = 0;
					int i_6_ = i_4_ << 4;
					for (int i_7_ = 0; i_3_ > i_7_; i_7_++) {
						if (anIntArrayArray6493[i_7_][0] > i_6_)
							break;
						i_5_++;
					}
					int i_8_;
					int i_9_;
					int i_10_;
					if (i_3_ <= i_5_) {
						int[] is = anIntArrayArray6493[i_3_ - 1];
						i_10_ = is[1];
						i_9_ = is[3];
						i_8_ = is[2];
					} else {
						int[] is = anIntArrayArray6493[i_5_];
						if (i_5_ > 0) {
							int[] is_11_ = anIntArrayArray6493[i_5_ - 1];
							int i_12_ = ((i_6_ - is_11_[0] << 12) / (is[0] - is_11_[0]));
							int i_13_ = -i_12_ + 4096;
							i_8_ = is_11_[2] * i_13_ + is[2] * i_12_ >> 12;
							i_9_ = is[3] * i_12_ + i_13_ * is_11_[3] >> 12;
							i_10_ = i_13_ * is_11_[1] + i_12_ * is[1] >> 12;
						} else {
							i_9_ = is[3];
							i_10_ = is[1];
							i_8_ = is[2];
						}
					}
					i_8_ >>= 4;
					i_9_ >>= 4;
					i_10_ >>= 4;
					if (i_9_ >= 0) {
						if (i_9_ > 255)
							i_9_ = 255;
					} else
						i_9_ = 0;
					if (i_10_ < 0)
						i_10_ = 0;
					else if (i_10_ > 255)
						i_10_ = 255;
					if (i_8_ < 0)
						i_8_ = 0;
					else if (i_8_ > 255)
						i_8_ = 255;
					anIntArray6492[i_4_] = Class48.bitOR(Class48.bitOR(i_10_ << 16, i_8_ << 8), i_9_);
				}
			}
		}
	}

	final void method3076(byte i) {
		int i_14_ = 92 / ((-58 - i) / 40);
		if (anIntArrayArray6493 == null)
			method3166(3, 1);
		method3165((byte) -76);
	}

	private final void method3166(int i, int i_15_) {
		if (i != 3)
			anIntArrayArray6493 = null;
		if (i_15_ != 0) {
			int i_16_ = i_15_;
			while_166_ : do {
				while_165_ : do {
					while_164_ : do {
						while_163_ : do {
							do {
								if (i_16_ != 1) {
									if (i_16_ != 2) {
										if (i_16_ != 3) {
											if (i_16_ != 4) {
												if (i_16_ != 5) {
													if (i_16_ != 6)
														break while_166_;
												} else
													break while_164_;
												break while_165_;
											}
										} else
											break;
										break while_163_;
									}
								} else {
									anIntArrayArray6493 = new int[2][4];
									anIntArrayArray6493[0][2] = 0;
									anIntArrayArray6493[0][1] = 0;
									anIntArrayArray6493[0][0] = 0;
									anIntArrayArray6493[0][3] = 0;
									anIntArrayArray6493[1][1] = 4096;
									anIntArrayArray6493[1][3] = 4096;
									anIntArrayArray6493[1][0] = 4096;
									anIntArrayArray6493[1][2] = 4096;
									return;
								}
								anIntArrayArray6493 = new int[8][4];
								anIntArrayArray6493[0][0] = 0;
								anIntArrayArray6493[0][1] = 2650;
								anIntArrayArray6493[0][2] = 2602;
								anIntArrayArray6493[0][3] = 2361;
								anIntArrayArray6493[1][3] = 1558;
								anIntArrayArray6493[1][0] = 2867;
								anIntArrayArray6493[1][2] = 1799;
								anIntArrayArray6493[1][1] = 2313;
								anIntArrayArray6493[2][1] = 2618;
								anIntArrayArray6493[2][3] = 1413;
								anIntArrayArray6493[2][0] = 3072;
								anIntArrayArray6493[2][2] = 1734;
								anIntArrayArray6493[3][0] = 3276;
								anIntArrayArray6493[3][3] = 947;
								anIntArrayArray6493[3][1] = 2296;
								anIntArrayArray6493[3][2] = 1220;
								anIntArrayArray6493[4][0] = 3481;
								anIntArrayArray6493[4][2] = 963;
								anIntArrayArray6493[4][3] = 722;
								anIntArrayArray6493[4][1] = 2072;
								anIntArrayArray6493[5][3] = 1766;
								anIntArrayArray6493[5][1] = 2730;
								anIntArrayArray6493[5][2] = 2152;
								anIntArrayArray6493[5][0] = 3686;
								anIntArrayArray6493[6][1] = 2232;
								anIntArrayArray6493[6][3] = 915;
								anIntArrayArray6493[6][0] = 3891;
								anIntArrayArray6493[6][2] = 1060;
								anIntArrayArray6493[7][2] = 1413;
								anIntArrayArray6493[7][3] = 1140;
								anIntArrayArray6493[7][1] = 1686;
								anIntArrayArray6493[7][0] = 4096;
								return;
							} while (false);
							anIntArrayArray6493 = new int[7][4];
							anIntArrayArray6493[0][1] = 0;
							anIntArrayArray6493[0][2] = 0;
							anIntArrayArray6493[0][0] = 0;
							anIntArrayArray6493[0][3] = 4096;
							anIntArrayArray6493[1][1] = 0;
							anIntArrayArray6493[1][0] = 663;
							anIntArrayArray6493[1][3] = 4096;
							anIntArrayArray6493[1][2] = 4096;
							anIntArrayArray6493[2][2] = 4096;
							anIntArrayArray6493[2][1] = 0;
							anIntArrayArray6493[2][0] = 1363;
							anIntArrayArray6493[2][3] = 0;
							anIntArrayArray6493[3][3] = 0;
							anIntArrayArray6493[3][2] = 4096;
							anIntArrayArray6493[3][1] = 4096;
							anIntArrayArray6493[3][0] = 2048;
							anIntArrayArray6493[4][3] = 0;
							anIntArrayArray6493[4][0] = 2727;
							anIntArrayArray6493[4][2] = 0;
							anIntArrayArray6493[4][1] = 4096;
							anIntArrayArray6493[5][1] = 4096;
							anIntArrayArray6493[5][0] = 3411;
							anIntArrayArray6493[5][2] = 0;
							anIntArrayArray6493[5][3] = 4096;
							anIntArrayArray6493[6][3] = 4096;
							anIntArrayArray6493[6][2] = 0;
							anIntArrayArray6493[6][1] = 0;
							anIntArrayArray6493[6][0] = 4096;
							return;
						} while (false);
						anIntArrayArray6493 = new int[6][4];
						anIntArrayArray6493[0][3] = 0;
						anIntArrayArray6493[0][1] = 0;
						anIntArrayArray6493[0][0] = 0;
						anIntArrayArray6493[0][2] = 0;
						anIntArrayArray6493[1][0] = 1843;
						anIntArrayArray6493[1][3] = 1493;
						anIntArrayArray6493[1][2] = 0;
						anIntArrayArray6493[1][1] = 0;
						anIntArrayArray6493[2][3] = 2939;
						anIntArrayArray6493[2][0] = 2457;
						anIntArrayArray6493[2][1] = 0;
						anIntArrayArray6493[2][2] = 0;
						anIntArrayArray6493[3][3] = 3565;
						anIntArrayArray6493[3][0] = 2781;
						anIntArrayArray6493[3][1] = 0;
						anIntArrayArray6493[3][2] = 1124;
						anIntArrayArray6493[4][3] = 4031;
						anIntArrayArray6493[4][1] = 546;
						anIntArrayArray6493[4][0] = 3481;
						anIntArrayArray6493[4][2] = 3084;
						anIntArrayArray6493[5][0] = 4096;
						anIntArrayArray6493[5][2] = 4096;
						anIntArrayArray6493[5][1] = 4096;
						anIntArrayArray6493[5][3] = 4096;
						return;
					} while (false);
					anIntArrayArray6493 = new int[16][4];
					anIntArrayArray6493[0][2] = 192;
					anIntArrayArray6493[0][0] = 0;
					anIntArrayArray6493[0][1] = 80;
					anIntArrayArray6493[0][3] = 321;
					anIntArrayArray6493[1][1] = 321;
					anIntArrayArray6493[1][0] = 155;
					anIntArrayArray6493[1][3] = 562;
					anIntArrayArray6493[1][2] = 449;
					anIntArrayArray6493[2][1] = 578;
					anIntArrayArray6493[2][0] = 389;
					anIntArrayArray6493[2][3] = 803;
					anIntArrayArray6493[2][2] = 690;
					anIntArrayArray6493[3][2] = 995;
					anIntArrayArray6493[3][0] = 671;
					anIntArrayArray6493[3][3] = 1140;
					anIntArrayArray6493[3][1] = 947;
					anIntArrayArray6493[4][2] = 1397;
					anIntArrayArray6493[4][1] = 1285;
					anIntArrayArray6493[4][0] = 897;
					anIntArrayArray6493[4][3] = 1509;
					anIntArrayArray6493[5][2] = 1429;
					anIntArrayArray6493[5][0] = 1175;
					anIntArrayArray6493[5][3] = 1413;
					anIntArrayArray6493[5][1] = 1525;
					anIntArrayArray6493[6][3] = 1333;
					anIntArrayArray6493[6][0] = 1368;
					anIntArrayArray6493[6][1] = 1734;
					anIntArrayArray6493[6][2] = 1461;
					anIntArrayArray6493[7][0] = 1507;
					anIntArrayArray6493[7][1] = 1413;
					anIntArrayArray6493[7][3] = 1702;
					anIntArrayArray6493[7][2] = 1525;
					anIntArrayArray6493[8][1] = 1108;
					anIntArrayArray6493[8][2] = 1590;
					anIntArrayArray6493[8][3] = 2056;
					anIntArrayArray6493[8][0] = 1736;
					anIntArrayArray6493[9][1] = 1766;
					anIntArrayArray6493[9][0] = 2088;
					anIntArrayArray6493[9][3] = 2666;
					anIntArrayArray6493[9][2] = 2056;
					anIntArrayArray6493[10][2] = 2586;
					anIntArrayArray6493[10][0] = 2355;
					anIntArrayArray6493[10][1] = 2409;
					anIntArrayArray6493[10][3] = 3276;
					anIntArrayArray6493[11][1] = 3116;
					anIntArrayArray6493[11][3] = 3228;
					anIntArrayArray6493[11][2] = 3148;
					anIntArrayArray6493[11][0] = 2691;
					anIntArrayArray6493[12][2] = 3710;
					anIntArrayArray6493[12][1] = 3806;
					anIntArrayArray6493[12][3] = 3196;
					anIntArrayArray6493[12][0] = 3031;
					anIntArrayArray6493[13][1] = 3437;
					anIntArrayArray6493[13][2] = 3421;
					anIntArrayArray6493[13][3] = 3019;
					anIntArrayArray6493[13][0] = 3522;
					anIntArrayArray6493[14][1] = 3116;
					anIntArrayArray6493[14][0] = 3727;
					anIntArrayArray6493[14][2] = 3148;
					anIntArrayArray6493[14][3] = 3228;
					anIntArrayArray6493[15][1] = 2377;
					anIntArrayArray6493[15][2] = 2505;
					anIntArrayArray6493[15][3] = 2746;
					anIntArrayArray6493[15][0] = 4096;
					return;
				} while (false);
				anIntArrayArray6493 = new int[4][4];
				anIntArrayArray6493[0][3] = 0;
				anIntArrayArray6493[0][2] = 4096;
				anIntArrayArray6493[0][0] = 2048;
				anIntArrayArray6493[0][1] = 0;
				anIntArrayArray6493[1][2] = 4096;
				anIntArrayArray6493[1][1] = 4096;
				anIntArrayArray6493[1][3] = 0;
				anIntArrayArray6493[1][0] = 2867;
				anIntArrayArray6493[2][1] = 4096;
				anIntArrayArray6493[2][2] = 4096;
				anIntArrayArray6493[2][3] = 0;
				anIntArrayArray6493[2][0] = 3276;
				anIntArrayArray6493[3][2] = 0;
				anIntArrayArray6493[3][0] = 4096;
				anIntArrayArray6493[3][3] = 0;
				anIntArrayArray6493[3][1] = 4096;
				return;
			} while (false);
			throw new RuntimeException("Invalid gradient preset");
		}
	}

	public Class296_Sub51_Sub29() {
		super(1, false);
	}

	final int[][] get_colour_output(int i, int i_17_) {
		if (i_17_ != 17621)
			method3165((byte) 56);
		int[][] is = aClass86_5034.method823((byte) 8, i);
		if (aClass86_5034.aBoolean939) {
			int[] is_18_ = this.method3064(0, i_17_ - 17621, i);
			int[] is_19_ = is[0];
			int[] is_20_ = is[1];
			int[] is_21_ = is[2];
			for (int i_22_ = 0; Class41_Sub10.anInt3769 > i_22_; i_22_++) {
				int i_23_ = is_18_[i_22_] >> 4;
				if (i_23_ < 0)
					i_23_ = 0;
				if (i_23_ > 256)
					i_23_ = 256;
				i_23_ = anIntArray6492[i_23_];
				is_19_[i_22_] = (16711680 & i_23_) >> 12;
				is_20_[i_22_] = (65280 & i_23_) >> 4;
				is_21_[i_22_] = (i_23_ & 255) << 4;
			}
		}
		return is;
	}
}
