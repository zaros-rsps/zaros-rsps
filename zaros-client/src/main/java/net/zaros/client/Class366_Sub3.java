package net.zaros.client;

import net.zaros.client.logic.clans.channel.ClanChannelMember;

/* Class366_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class366_Sub3 extends Class366 {
	static IncomingPacket aClass231_5373 = new IncomingPacket(118, -1);
	static int anInt5374;

	final void method3766(int i) {
		if (i >= 30)
			aHa_Sub3_3121.method1327(true, false);
	}

	Class366_Sub3(ha_Sub3 var_ha_Sub3) {
		super(var_ha_Sub3);
	}

	public static void method3777(boolean bool) {
		aClass231_5373 = null;
		if (bool)
			aClass231_5373 = null;
	}

	final void method3768(byte i, boolean bool) {
		aHa_Sub3_3121.method1327(true, true);
		if (i != -88)
			method3769(79, (byte) -96, -113);
	}

	final void method3770(byte i, boolean bool) {
		if (i != 33)
			aClass231_5373 = null;
	}

	final boolean method3763(int i) {
		int i_0_ = -43 / ((73 - i) / 40);
		return true;
	}

	final void method3769(int i, byte i_1_, int i_2_) {
		if (i_1_ != -81)
			anInt5374 = 34;
	}

	static final void method3778(byte i, int i_3_) {
		if (EmissiveTriangle.affinedClanChannel != null && (i_3_ >= 0 && i_3_ < EmissiveTriangle.affinedClanChannel.members_count)) {
			ClanChannelMember class329 = EmissiveTriangle.affinedClanChannel.members[i_3_];
			if (i != 66)
				anInt5374 = -107;
			if (class329.rank == -1) {
				Connection class204 = Class296_Sub51_Sub13.method3111(true);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 91, Class173.aClass311_1802);
				class296_sub1.out.p1(Class117.method1015((byte) -98, class329.name) + 2);
				class296_sub1.out.p2(i_3_);
				class296_sub1.out.writeString(class329.name);
				class204.sendPacket(class296_sub1, (byte) 119);
			}
		}
	}

	final void method3764(boolean bool, int i, Class69 class69) {
		if (bool)
			method3770((byte) -122, false);
		aHa_Sub3_3121.method1316(class69, (byte) -123);
		aHa_Sub3_3121.method1272((byte) -107, i);
	}
}
