package net.zaros.client;

/* Class401 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class401 {
	private AdvancedMemoryCache aClass113_3362 = new AdvancedMemoryCache(64);
	private Js5 aClass138_3363;
	Js5 aClass138_3364;
	static int[] anIntArray3365;
	static int anInt3366 = 0;
	AdvancedMemoryCache aClass113_3367 = new AdvancedMemoryCache(64);

	final void method4138(int i) {
		if (i != 9)
			aClass138_3363 = null;
		synchronized (aClass113_3362) {
			aClass113_3362.clear();
		}
		synchronized (aClass113_3367) {
			aClass113_3367.clear();
		}
	}

	final void method4139(int i) {
		synchronized (aClass113_3362) {
			aClass113_3362.clearSoftReferences();
		}
		if (i >= -25)
			anIntArray3365 = null;
		synchronized (aClass113_3367) {
			aClass113_3367.clearSoftReferences();
		}
	}

	public static void method4140(byte i) {
		if (i > 47)
			anIntArray3365 = null;
	}

	final void method4141(int i, byte i_0_, int i_1_) {
		int i_2_ = -63 / ((i_0_ + 9) / 46);
		aClass113_3362 = new AdvancedMemoryCache(i);
		aClass113_3367 = new AdvancedMemoryCache(i_1_);
	}

	final void method4142(boolean bool, int i) {
		synchronized (aClass113_3362) {
			aClass113_3362.clean(i);
		}
		if (!bool) {
			synchronized (aClass113_3367) {
				aClass113_3367.clean(i);
			}
		}
	}

	final Class28 method4143(int i, int i_3_) {
		if (i_3_ != 24180)
			method4140((byte) -54);
		Class28 class28;
		synchronized (aClass113_3362) {
			class28 = (Class28) aClass113_3362.get((long) i);
		}
		if (class28 != null)
			return class28;
		byte[] is;
		synchronized (aClass138_3363) {
			is = aClass138_3363.getFile(34, i);
		}
		class28 = new Class28();
		class28.aClass401_301 = this;
		if (is != null)
			class28.method317(new Packet(is), -28563);
		synchronized (aClass113_3362) {
			aClass113_3362.put(class28, (long) i);
		}
		return class28;
	}

	static final void method4144(byte i, int i_4_, int i_5_, int i_6_) {
		if (i < 98)
			anInt3366 = 106;
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_4_, 9);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.anInt6145 = i_5_;
		class296_sub39_sub5.intParam = i_6_;
	}

	Class401(GameType class35, int i, Js5 class138, Js5 class138_7_) {
		aClass138_3364 = class138_7_;
		aClass138_3363 = class138;
		aClass138_3363.getLastFileId(34);
	}
}
