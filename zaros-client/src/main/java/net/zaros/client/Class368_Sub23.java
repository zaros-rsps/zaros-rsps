package net.zaros.client;

/* Class368_Sub23 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub23 extends Class368 {
	
	private int anInt5562;
	
	private int anInt5563;
	
	private int anInt5564;
	
	static OutgoingPacket aClass311_5565;
	
	private int anInt5566;
	
	private int anInt5567;
	
	private int anInt5568;
	
	static int anInt5569 = -1;
	
	static int anInt5570;
	
	static Js5 aClass138_5571;
	
	static int npcNodeCount;
	
	static boolean aBoolean5573;
	
	static Js5DiskStore aClass168_5574;
	
	static int[] anIntArray5575;
	
	final void method3807(byte i) {
		Class88.aClass220Array946[anInt5566].method2071(0, (byte) -25);
		Class88.aClass220Array946[anInt5563].method2071(1, (byte) -25);
		StaticMethods.anInt3196 = anInt5564;
		ClotchesLoader.anInt279 = 1;
		Class156.anInt3593 = anInt5568;
		Class42_Sub3.anInt3844 = 0;
		Class296_Sub45_Sub4.anInt6316 = 0;
		Class361.anInt3103 = 3;
		Class296_Sub51_Sub28.anInt6489 = anInt5567;
		Class296_Sub22.anInt4739 = anInt5562;
		int i_0_ = 18 % ((52 - i) / 52);
		Class296_Sub51_Sub27_Sub1.method3157(false);
		StaticMethods.aBoolean1183 = true;
	}
	
	Class368_Sub23(Packet class296_sub17) {
		super(class296_sub17);
		anInt5566 = class296_sub17.g2();
		anInt5563 = class296_sub17.g2();
		anInt5562 = class296_sub17.g2();
		anInt5567 = class296_sub17.g2();
		anInt5568 = class296_sub17.g2();
		anInt5564 = class296_sub17.g2();
	}
	
	public static void method3868(byte i) {
		if (i <= 111) {
			method3868((byte) -84);
		}
		aClass168_5574 = null;
		aClass138_5571 = null;
		anIntArray5575 = null;
		aClass311_5565 = null;
	}
	
	static void method3869(boolean bool, int i) {
		try {
			Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 113, Class386.closeInterfacePacket);
			Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
			Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.getFirst(true);
			int i_1_ = -100 % ((i - 69) / 46);
			for (/**/; class296_sub13 != null; class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.getNext(0)) {
				if (!class296_sub13.isLinked((byte) -84)) {
					class296_sub13 = ((Class296_Sub13) Class386.aClass263_3264.getFirst(true));
					if (class296_sub13 == null) {
						break;
					}
				}
				if (class296_sub13.anInt4656 == 0) {
					Class47.method596(class296_sub13, true, bool, (byte) 59);
				}
			}
			if (Player.aClass51_6872 != null) {
				Class332.method3416(Player.aClass51_6872, (byte) 99);
				Player.aClass51_6872 = null;
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	static {
		aClass311_5565 = new OutgoingPacket(7, 8);
		npcNodeCount = 0;
		aBoolean5573 = false;
	}
}
