package net.zaros.client;

/* Class262 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class262 {
	private int anInt2426;
	private Class174[] aClass174Array2427;
	private int anInt2428;
	private int anInt2429;
	private int anInt2430;
	private Class262 aClass262_2431;
	private int anInt2432;
	private int anInt2433;
	private int anInt2434;
	private int anInt2435;
	private int anInt2436;
	private Sprite aClass397_2437;
	private boolean aBoolean2438 = true;
	static OutgoingPacket aClass311_2439 = new OutgoingPacket(82, -1);
	private int anInt2440;
	private Class174[] aClass174Array2441;
	private byte[] aByteArray2442;
	private Class174 aClass174_2443;
	private int anInt2444;
	private boolean aBoolean2445;
	private int anInt2446;
	static int[] anIntArray2447;
	private int anInt2448 = -1;
	static Class117 aClass117_2449;
	private int anInt2450;
	private Model aClass178_2451;
	public static boolean aBoolean1324 = false;
	static int[][] anIntArrayArray2452;
	static long aLong2453 = -1L;

	final void method2250(int i, int i_0_, byte i_1_) {
		anInt2446 = (i - anInt2433) * i_0_ / 255 + anInt2433;
		int i_2_ = 54 % ((42 - i_1_) / 33);
	}

	private final void method2251(boolean bool, int i, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, boolean bool_9_, int i_10_, ha var_ha, int i_11_, int i_12_, int i_13_) {
		int i_14_ = -i_3_ + 255;
		if (aClass178_2451 != null) {
			if (bool) {
				var_ha.GA(i_12_);
				var_ha.ya();
			}
			method2263(17789, i_3_, i_11_, i_8_, var_ha, i_5_);
		} else {
			i_5_ = i_5_ + i_7_ & 0x3fff;
			var_ha.ya();
			if (anInt2436 != -1 && anInt2426 != 0) {
				MaterialRaw class170 = Class366_Sub8.aD5412.method14(anInt2436, -9412);
				if (aClass397_2437 == null && Class366_Sub8.aD5412.is_ready(anInt2436)) {
					int[] is = (class170.anInt1788 != 2 ? Class366_Sub8.aD5412.get_transparent_pixels(anInt2426, anInt2426, false, (byte) 87, 0.7F, anInt2436) : Class366_Sub8.aD5412.method17(0.7F, anInt2426, false, (byte) -104, anInt2426, anInt2436));
					anInt2430 = is[0];
					anInt2429 = is[is.length - 1];
					aClass397_2437 = var_ha.method1096(106, 0, anInt2426, anInt2426, anInt2426, is);
				}
				int i_15_ = i_14_ == 255 ? class170.anInt1788 != 2 ? 0 : 1 : 1;
				if (i_15_ == 1 && bool)
					var_ha.aa(i_4_, i_6_, i_13_, i_10_, i_12_, 0);
				if (aClass397_2437 != null) {
					int i_16_ = i_11_ * i_10_ / -4096;
					int i_17_;
					for (i_17_ = i_5_ * i_10_ / 4096 + (i_13_ - i_10_) / 2; i_10_ < i_17_; i_17_ -= i_10_) {
						/* empty */
					}
					for (/**/; i_17_ < 0; i_17_ = i_17_ + i_10_) {
						/* empty */
					}
					if (anInt2435 != 1) {
						for (/**/; i_10_ < i_16_; i_16_ -= i_10_) {
							/* empty */
						}
						for (/**/; i_16_ < 0; i_16_ += i_10_) {
							/* empty */
						}
						for (int i_18_ = i_17_ - i_10_; i_18_ < i_13_; i_18_ += i_10_) {
							for (int i_19_ = i_16_ - i_10_; i_10_ > i_19_; i_19_ += i_10_)
								aClass397_2437.method4083(i_18_ + i_4_, i_6_ + i_19_, i_10_, i_10_, 0, (i_14_ << 24 | 0xffffff), i_15_);
						}
					} else {
						for (int i_20_ = -i_10_ + i_17_; i_20_ < i_13_; i_20_ += i_10_)
							aClass397_2437.method4083(i_4_ + i_20_, i_6_ + i_16_, i_10_, i_10_, 0, i_14_ << 24 | 0xffffff, i_15_);
						if ((anInt2430 & ~0xffffff) != 0)
							var_ha.method1088(0, i_13_, anInt2430, 1, 0, i_6_ + i_16_ + 1);
						if ((anInt2429 & ~0xffffff) != 0)
							var_ha.method1088(0, i_13_, anInt2429, i ^ ~0x1, i_6_ + i_16_ + i_10_, -i_6_ - (i_16_ - (-i_10_ + i_10_)));
					}
				}
			} else
				var_ha.aa(i_4_, i_6_, i_13_, i_10_, i_12_ | i_14_ << 24, 1);
		}
		for (int i_21_ = i + anInt2434; i_21_ >= 0; i_21_--)
			aClass174Array2427[i_21_].method1691(var_ha, i_4_, i_6_, i_13_, i_10_, i_11_, i_5_, anInt2450, anInt2428, anInt2444, i_14_);
		var_ha.ya();
	}

	final void method2252(byte i) {
		aBoolean2445 = false;
		int i_22_ = -64 / ((i + 44) / 33);
		anInt2446 = 0;
		aClass262_2431 = null;
	}

	public static void method2253(byte i) {
		aClass117_2449 = null;
		aClass311_2439 = null;
		if (i > -37)
			method2262(null, (byte) 90);
		anIntArray2447 = null;
		anIntArrayArray2452 = null;
	}

	final void method2254(int i, int i_23_, boolean bool, int i_24_, int i_25_, boolean bool_26_, ha var_ha, int i_27_, int i_28_, int i_29_, int i_30_, int i_31_, int i_32_) {
		if (i_31_ > -53)
			aClass174Array2427 = null;
		int i_33_ = 0;
		if (aBoolean2445)
			i_33_ = anInt2446;
		if (aClass262_2431 != null) {
			Class262 class262_34_ = this;
			Class262 class262_35_ = aClass262_2431;
			if (class262_34_.hashCode() > class262_35_.hashCode()) {
				i_33_ = 255 - i_33_;
				class262_34_ = aClass262_2431;
				class262_35_ = this;
			}
			class262_34_.method2251(bool_26_, -1, i_33_, i_28_, i_24_, i_25_, i_23_, i_30_, bool, i_29_, var_ha, i_32_, i_27_, i);
			class262_35_.method2251(false, -1, -i_33_ + 255, i_28_, i_24_, i_25_, i_23_, i_30_, bool, i_29_, var_ha, i_32_, i_27_, i);
		} else
			method2251(bool_26_, -1, i_33_, i_28_, i_24_, i_25_, i_23_, i_30_, bool, i_29_, var_ha, i_32_, i_27_, i);
	}

	static final void method2255(int i, byte[] is, int i_36_, int i_37_, int i_38_, int i_39_) {
		if (i > i_36_) {
			i_37_ = i - i_36_ >> 2;
			i_39_ += i_36_;
			while (--i_37_ >= 0) {
				is[i_39_++] = (byte) 1;
				is[i_39_++] = (byte) 1;
				is[i_39_++] = (byte) 1;
				is[i_39_++] = (byte) 1;
			}
			i_37_ = -i_36_ + i & 0x3;
			while (--i_37_ >= 0)
				is[i_39_++] = (byte) 1;
			if (i_38_ != -1)
				method2255(42, null, -16, 56, 5, 100);
		}
	}

	final boolean method2256(boolean bool) {
		if (bool != true)
			anInt2432 = 115;
		return aBoolean2445;
	}

	final void method2257(int i, Class262 class262_40_) {
		if (aBoolean2445)
			anInt2433 = anInt2446;
		else if (class262_40_ != null && class262_40_.aBoolean2445)
			anInt2433 = -class262_40_.anInt2446 + 255;
		else
			anInt2433 = 0;
		if (i > 29) {
			aBoolean2445 = true;
			anInt2446 = 0;
			aClass262_2431 = class262_40_;
		}
	}

	final boolean method2258(boolean bool, ha var_ha, int i, int i_41_) {
		if (anInt2448 != i) {
			anInt2448 = i;
			int i_42_ = Statics.method629(!bool, i);
			if (i_42_ > 512)
				i_42_ = 512;
			if (i_42_ <= 0)
				i_42_ = 1;
			if (anInt2426 != i_42_) {
				anInt2426 = i_42_;
				aClass397_2437 = null;
			}
			if (aClass174Array2441 != null) {
				anInt2434 = 0;
				int[] is = new int[aClass174Array2441.length];
				for (int i_43_ = 0; aClass174Array2441.length > i_43_; i_43_++) {
					Class174 class174 = aClass174Array2441[i_43_];
					if (class174.method1697(anInt2450, anInt2428, anInt2444, anInt2448)) {
						is[anInt2434] = class174.anInt1821;
						aClass174Array2427[anInt2434++] = class174;
					}
				}
				Class123_Sub1_Sub1.method1063(2, aClass174Array2427, anInt2434 - 1, is, 0);
			}
			aBoolean2438 = true;
		}
		if (bool != true)
			anInt2429 = -2;
		boolean bool_44_ = false;
		if (aBoolean2438) {
			aBoolean2438 = false;
			for (int i_45_ = anInt2434 - 1; i_45_ >= 0; i_45_--) {
				boolean bool_46_ = aClass174Array2427[i_45_].method1698(var_ha, aClass174_2443);
				Class262 class262_47_ = this;
				class262_47_.aBoolean2438 = class262_47_.aBoolean2438 | !bool_46_;
				bool_44_ |= bool_46_;
			}
		}
		if (i_41_ == 0 || !var_ha.z())
			aClass178_2451 = null;
		else if (aClass178_2451 == null && anInt2432 >= 0)
			method2260(var_ha, (byte) 84);
		if (aClass262_2431 != null && this != aClass262_2431) {
			aClass262_2431.method2252((byte) 118);
			bool_44_ |= aClass262_2431.method2258(true, var_ha, i, i_41_);
		}
		return bool_44_;
	}

	final Class262 method2259(byte i) {
		if (i != -24)
			aClass174Array2441 = null;
		return aClass262_2431;
	}

	private final void method2260(ha var_ha, byte i) {
		try {
			Js5 class138 = Class239.aClass138_2255;
			boolean bool = class138.hasFileBuffer((byte) -91, anInt2432);
			if (i == 84) {
				if (bool) {
					var_ha.ZA(16777215, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F);
					Mesh class132 = Class296_Sub51_Sub1.fromJs5((Class239.aClass138_2255), anInt2432, 0);
					aClass178_2451 = var_ha.a(class132, 1099776, 0, 255, 1);
					byte[] is = aClass178_2451.method1733();
					if (is == null)
						aByteArray2442 = null;
					else {
						aByteArray2442 = new byte[is.length];
						ArrayTools.removeElement(is, 0, aByteArray2442, 0, is.length);
					}
				}
			}
		} catch (Exception exception) {
			/* empty */
		}
	}

	final void method2261(int i, ha var_ha, int i_48_, int i_49_, int i_50_, int i_51_, int i_52_, int i_53_, int i_54_, int i_55_, int i_56_) {
		method2254(i, i_52_, false, i_54_, i_55_, true, var_ha, i_56_, i_53_, i_50_, 0, -118, i_48_);
		if (i_51_ != -10704)
			anInt2430 = 7;
	}

	static final void method2262(ha var_ha, byte i) {
		if (i == -87) {
			int i_57_ = 0;
			int i_58_ = 0;
			if (Class368_Sub5_Sub2.aBoolean6597) {
				i_57_ = Class387.method4034(true);
				i_58_ = GraphicsLoader.method2286(true);
			}
			int i_59_ = -10660793;
			Class230.method2107(i_59_, -16777216, i_58_ + Class100.anInt1067, true, Class296_Sub39_Sub20.anInt6254, Class81.anInt3666, i_57_ + Class252.anInt2382, var_ha);
			Class49.aClass55_461.a(-1, 1659, i_59_, Class252.anInt2382 + 3 + i_57_, TranslatableString.aClass120_1220.getTranslation(Class394.langID), Class100.anInt1067 + 14 + i_58_);
			int i_60_ = Class84.aClass189_924.method1895((byte) -55) + i_57_;
			int i_61_ = Class84.aClass189_924.method1897(0) + i_58_;
			if (Class262.aBoolean1324) {
				int i_62_ = 0;
				for (Class296_Sub39_Sub1 class296_sub39_sub1 = ((Class296_Sub39_Sub1) Class152.aClass145_1558.getFront()); class296_sub39_sub1 != null; class296_sub39_sub1 = ((Class296_Sub39_Sub1) Class152.aClass145_1558.getNext())) {
					int i_63_ = i_62_ * 16 + (i_58_ + 31 + Class100.anInt1067);
					i_62_++;
					if (class296_sub39_sub1.anInt6117 == 1)
						Class296_Sub51_Sub38.method3198(Class81.anInt3666, Class296_Sub39_Sub20.anInt6254, -84, -1, i_61_, i_63_, i_58_ + Class100.anInt1067, var_ha, i_57_ + Class252.anInt2382, i_60_, ((Class296_Sub39_Sub9) (class296_sub39_sub1.aClass145_6119.head.queue_next)), -256);
					else
						Class56.method667(Class252.anInt2382 + i_57_, i_61_, class296_sub39_sub1, Class296_Sub39_Sub20.anInt6254, -1, (byte) -77, -256, i_60_, Class81.anInt3666, i_63_, var_ha, Class100.anInt1067 + i_58_);
				}
				if (Class241.aClass296_Sub39_Sub1_2302 != null) {
					Class230.method2107(i_59_, -16777216, Class270.anInt2510, true, Class95.anInt1036, StaticMethods.anInt3152, Class41_Sub5.anInt3753, var_ha);
					i_62_ = 0;
					Class49.aClass55_461.a(-1, i + 1746, i_59_, Class41_Sub5.anInt3753 + 3, (Class241.aClass296_Sub39_Sub1_2302.aString6116), Class270.anInt2510 + 14);
					for (Class296_Sub39_Sub9 class296_sub39_sub9 = ((Class296_Sub39_Sub9) Class241.aClass296_Sub39_Sub1_2302.aClass145_6119.getFront()); class296_sub39_sub9 != null; class296_sub39_sub9 = ((Class296_Sub39_Sub9) Class241.aClass296_Sub39_Sub1_2302.aClass145_6119.getNext())) {
						int i_64_ = Class270.anInt2510 + (31 + i_62_ * 16);
						i_62_++;
						Class296_Sub51_Sub38.method3198(StaticMethods.anInt3152, Class95.anInt1036, -82, -1, i_61_, i_64_, Class270.anInt2510, var_ha, Class41_Sub5.anInt3753, i_60_, class296_sub39_sub9, -256);
					}
					Class395.method4068(Class270.anInt2510, Class41_Sub5.anInt3753, Class95.anInt1036, -30544, StaticMethods.anInt3152);
				}
			} else {
				int i_65_ = 0;
				for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeFirst((byte) 127); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeNext(i ^ ~0x3bf)) {
					int i_66_ = ((Class230.anInt2210 + (-1 - i_65_)) * 16 + 31 + i_58_ + Class100.anInt1067);
					i_65_++;
					Class296_Sub51_Sub38.method3198(Class81.anInt3666, Class296_Sub39_Sub20.anInt6254, -106, -1, i_61_, i_66_, i_58_ + Class100.anInt1067, var_ha, i_57_ + Class252.anInt2382, i_60_, class296_sub39_sub9, -256);
				}
			}
			Class395.method4068(i_58_ + Class100.anInt1067, Class252.anInt2382 + i_57_, Class296_Sub39_Sub20.anInt6254, i ^ 0x7719, Class81.anInt3666);
		}
	}

	private final void method2263(int i, int i_67_, int i_68_, int i_69_, ha var_ha, int i_70_) {
		Class373 class373 = var_ha.o().method3916();
		if (i != 17789)
			anInt2450 = -119;
		Class373 class373_71_ = var_ha.m();
		class373_71_.method3902(0, 0, 0);
		class373_71_.method3917(i_70_ & 0x3fff);
		class373_71_.method3914(i_68_ & 0x3fff);
		class373_71_.method3900(i_69_ & 0x3fff);
		var_ha.a(class373_71_);
		Class373 class373_72_ = var_ha.m();
		class373_72_.method3910();
		if (anInt2440 != i_67_) {
			aClass178_2451.method1724((byte) i_67_, aByteArray2442);
			anInt2440 = i_67_;
		}
		aClass178_2451.method1715(class373_72_, null, 0);
		var_ha.a(class373);
	}

	Class262(int i, Class174[] class174s, int i_73_, int i_74_, int i_75_, int i_76_, int i_77_, int i_78_) {
		anInt2436 = i;
		anInt2428 = i_75_;
		anInt2444 = i_76_;
		aClass174Array2441 = class174s;
		anInt2435 = i_77_;
		anInt2450 = i_74_;
		if (class174s == null) {
			aClass174Array2427 = null;
			aClass174_2443 = null;
		} else {
			aClass174Array2427 = new Class174[class174s.length];
			aClass174_2443 = i_73_ >= 0 ? class174s[i_73_] : null;
		}
		anInt2432 = i_78_;
	}

	static {
		anIntArrayArray2452 = new int[][]{{2, 4, 6, 0}, {0, 2, 3, 5, 6, 4}, {0, 1, 4, 5}, {4, 6, 0, 2}, {2, 4, 0}, {0, 2, 4}, {6, 0, 1, 2, 4, 5}, {0, 1, 2, 4, 6, 7}, {4, 7, 6, 0}, {0, 8, 6, 1, 9, 2, 9, 4}, {2, 9, 4, 0, 8, 6}, {2, 11, 3, 7, 10, 10, 6, 6}, {2, 4, 6, 0}};
	}
}
