package net.zaros.client;


/* Class296_Sub51_Sub25 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub25 extends TextureOperation {
	private byte[] aByteArray6466;
	int anInt6467;
	private short[] aShortArray6468;
	boolean aBoolean6469;
	int anInt6470;
	int anInt6471;
	int anInt6472 = 4;
	private short[] aShortArray6473;
	int anInt6474;

	private final void method3143(int i) {
		if (i < 34)
			anInt6471 = 9;
		if (anInt6471 <= 0) {
			if (aShortArray6468 != null && anInt6472 == aShortArray6468.length) {
				aShortArray6473 = new short[anInt6472];
				for (int i_0_ = 0; anInt6472 > i_0_; i_0_++)
					aShortArray6473[i_0_] = (short) (int) Math.pow(2.0, (double) i_0_);
			}
		} else {
			aShortArray6468 = new short[anInt6472];
			aShortArray6473 = new short[anInt6472];
			for (int i_1_ = 0; anInt6472 > i_1_; i_1_++) {
				aShortArray6468[i_1_] = (short) (int) (Math.pow((double) ((float) anInt6471 / 4096.0F), (double) i_1_) * 4096.0);
				aShortArray6473[i_1_] = (short) (int) Math.pow(2.0, (double) i_1_);
			}
		}
	}

	private final int method3144(int i, int i_2_, int i_3_, int i_4_, int i_5_, byte i_6_, int i_7_) {
		int i_8_ = i_7_ >> 12;
		int i_9_ = i_8_ + 1;
		i_7_ &= 0xfff;
		if (i_9_ >= i_5_)
			i_9_ = 0;
		i_8_ &= 0xff;
		int i_10_ = i_2_ - 4096;
		int i_11_ = i_7_ - 4096;
		i_9_ &= 0xff;
		int i_12_ = aByteArray6466[i + i_8_] & 0x3;
		int i_13_ = Class296_Sub39_Sub10.anIntArray6180[i_7_];
		int i_14_;
		if (i_12_ > 1)
			i_14_ = i_12_ == 2 ? -i_2_ + i_7_ : -i_2_ - i_7_;
		else
			i_14_ = i_12_ == 0 ? i_2_ + i_7_ : -i_7_ + i_2_;
		i_12_ = aByteArray6466[i + i_9_] & 0x3;
		int i_15_;
		if (i_12_ <= 1)
			i_15_ = i_12_ == 0 ? i_2_ + i_11_ : i_2_ - i_11_;
		else
			i_15_ = i_12_ == 2 ? i_11_ - i_2_ : -i_11_ - i_2_;
		if (i_6_ > -56)
			return 31;
		i_12_ = aByteArray6466[i_4_ + i_8_] & 0x3;
		int i_16_ = (i_13_ * (i_15_ - i_14_) >> 12) + i_14_;
		if (i_12_ <= 1)
			i_14_ = i_12_ != 0 ? i_10_ - i_7_ : i_7_ + i_10_;
		else
			i_14_ = i_12_ != 2 ? -i_10_ - i_7_ : i_7_ - i_10_;
		i_12_ = aByteArray6466[i_9_ + i_4_] & 0x3;
		if (i_12_ <= 1)
			i_15_ = i_12_ == 0 ? i_11_ + i_10_ : i_10_ - i_11_;
		else
			i_15_ = i_12_ == 2 ? -i_10_ + i_11_ : -i_10_ - i_11_;
		int i_17_ = i_14_ + (i_13_ * (i_15_ - i_14_) >> 12);
		return i_16_ + (i_3_ * (i_17_ - i_16_) >> 12);
	}

	static final void method3145(Class338_Sub3[] class338_sub3s, int i, int i_18_) {
		if (i < i_18_) {
			int i_19_ = (i + i_18_) / 2;
			int i_20_ = i;
			Class338_Sub3 class338_sub3 = class338_sub3s[i_19_];
			class338_sub3s[i_19_] = class338_sub3s[i_18_];
			class338_sub3s[i_18_] = class338_sub3;
			int i_21_ = class338_sub3.anInt5208;
			for (int i_22_ = i; i_22_ < i_18_; i_22_++) {
				if (class338_sub3s[i_22_].anInt5208 > i_21_ + (i_22_ & 0x1)) {
					Class338_Sub3 class338_sub3_23_ = class338_sub3s[i_22_];
					class338_sub3s[i_22_] = class338_sub3s[i_20_];
					class338_sub3s[i_20_++] = class338_sub3_23_;
				}
			}
			class338_sub3s[i_18_] = class338_sub3s[i_20_];
			class338_sub3s[i_20_] = class338_sub3;
			method3145(class338_sub3s, i, i_20_ - 1);
			method3145(class338_sub3s, i_20_ + 1, i_18_);
		}
	}

	final void method3071(int i, Packet class296_sub17, int i_24_) {
		if (i > -84)
			method3143(83);
		int i_25_ = i_24_;
		while_144_ : do {
			while_143_ : do {
				while_142_ : do {
					while_141_ : do {
						while_140_ : do {
							do {
								if (i_25_ != 0) {
									if (i_25_ != 1) {
										if (i_25_ != 2) {
											if (i_25_ != 3) {
												if (i_25_ != 4) {
													if (i_25_ != 5) {
														if (i_25_ == 6)
															break while_143_;
														break while_144_;
													}
												} else
													break while_141_;
												break while_142_;
											}
										} else
											break;
										break while_140_;
									}
								} else {
									aBoolean6469 = class296_sub17.g1() == 1;
									return;
								}
								anInt6472 = class296_sub17.g1();
								return;
							} while (false);
							anInt6471 = class296_sub17.g2b();
							if (anInt6471 < 0) {
								aShortArray6468 = new short[anInt6472];
								for (i_25_ = 0; anInt6472 > i_25_; i_25_++)
									aShortArray6468[i_25_] = (short) class296_sub17.g2b();
							}
							break while_144_;
						} while (false);
						anInt6467 = anInt6470 = class296_sub17.g1();
						return;
					} while (false);
					anInt6474 = class296_sub17.g1();
					return;
				} while (false);
				anInt6467 = class296_sub17.g1();
				return;
			} while (false);
			anInt6470 = class296_sub17.g1();
			break;
		} while (false);
	}

	public Class296_Sub51_Sub25() {
		super(0, true);
		aBoolean6469 = true;
		anInt6467 = 4;
		anInt6474 = 0;
		anInt6470 = 4;
		anInt6471 = 1638;
		aByteArray6466 = new byte[512];
	}

	static final void method3146(byte i) {
		if (Class69.anInt3688 < 0) {
			BITConfigDefinition.anInt2604 = -1;
			Class69.anInt3688 = 0;
			Class296_Sub51_Sub5.anInt6365 = -1;
		}
		if (i == -10) {
			if (Class106.anInt1116 < Class69.anInt3688) {
				BITConfigDefinition.anInt2604 = -1;
				Class69.anInt3688 = Class106.anInt1116;
				Class296_Sub51_Sub5.anInt6365 = -1;
			}
			if (Class219_Sub1.anInt4569 < 0) {
				Class296_Sub51_Sub5.anInt6365 = -1;
				BITConfigDefinition.anInt2604 = -1;
				Class219_Sub1.anInt4569 = 0;
			}
			if (Class106.anInt1114 < Class219_Sub1.anInt4569) {
				BITConfigDefinition.anInt2604 = -1;
				Class296_Sub51_Sub5.anInt6365 = -1;
				Class219_Sub1.anInt4569 = Class106.anInt1114;
			}
		}
	}

	final int[] get_monochrome_output(int i, int i_26_) {
		int[] is = aClass318_5035.method3335(i_26_, (byte) 28);
		if (aClass318_5035.aBoolean2819)
			method3147(i_26_, (byte) 110, is);
		if (i != 0)
			anInt6474 = 8;
		return is;
	}

	final void method3147(int i, byte i_27_, int[] is) {
		int i_28_ = anInt6470 * Class294.anIntArray2686[i];
		if (i_27_ >= 68) {
			if (anInt6472 == 1) {
				int i_29_ = aShortArray6473[0] << 12;
				int i_30_ = aShortArray6468[0];
				int i_31_ = i_29_ * anInt6470 >> 12;
				int i_32_ = i_29_ * i_28_ >> 12;
				int i_33_ = i_29_ * anInt6467 >> 12;
				int i_34_ = i_32_ >> 12;
				int i_35_ = i_34_ + 1;
				if (i_31_ <= i_35_)
					i_35_ = 0;
				i_32_ &= 0xfff;
				int i_36_ = Class296_Sub39_Sub10.anIntArray6180[i_32_];
				int i_37_ = aByteArray6466[i_35_ & 0xff] & 0xff;
				int i_38_ = aByteArray6466[i_34_ & 0xff] & 0xff;
				if (aBoolean6469) {
					for (int i_39_ = 0; i_39_ < Class41_Sub10.anInt3769; i_39_++) {
						int i_40_ = Class33.anIntArray334[i_39_] * anInt6467;
						int i_41_ = method3144(i_38_, i_32_, i_36_, i_37_, i_33_, (byte) -77, i_29_ * i_40_ >> 12);
						i_41_ = i_30_ * i_41_ >> 12;
						is[i_39_] = (i_41_ >> 1) + 2048;
					}
				} else {
					for (int i_42_ = 0; Class41_Sub10.anInt3769 > i_42_; i_42_++) {
						int i_43_ = anInt6467 * Class33.anIntArray334[i_42_];
						int i_44_ = method3144(i_38_, i_32_, i_36_, i_37_, i_33_, (byte) -81, i_29_ * i_43_ >> 12);
						is[i_42_] = i_44_ * i_30_ >> 12;
					}
				}
			} else {
				int i_45_ = aShortArray6468[0];
				if (i_45_ > 8 || i_45_ < -8) {
					int i_46_ = aShortArray6473[0] << 12;
					int i_47_ = i_46_ * i_28_ >> 12;
					int i_48_ = i_46_ * anInt6467 >> 12;
					int i_49_ = i_46_ * anInt6470 >> 12;
					int i_50_ = i_47_ >> 12;
					int i_51_ = i_50_ + 1;
					i_47_ &= 0xfff;
					if (i_49_ <= i_51_)
						i_51_ = 0;
					int i_52_ = aByteArray6466[i_50_ & 0xff] & 0xff;
					int i_53_ = Class296_Sub39_Sub10.anIntArray6180[i_47_];
					int i_54_ = aByteArray6466[i_51_ & 0xff] & 0xff;
					for (int i_55_ = 0; Class41_Sub10.anInt3769 > i_55_; i_55_++) {
						int i_56_ = anInt6467 * Class33.anIntArray334[i_55_];
						int i_57_ = method3144(i_52_, i_47_, i_53_, i_54_, i_48_, (byte) -107, i_56_ * i_46_ >> 12);
						is[i_55_] = i_45_ * i_57_ >> 12;
					}
				}
				for (int i_58_ = 1; i_58_ < anInt6472; i_58_++) {
					i_45_ = aShortArray6468[i_58_];
					if (i_45_ > 8 || i_45_ < -8) {
						int i_59_ = aShortArray6473[i_58_] << 12;
						int i_60_ = anInt6470 * i_59_ >> 12;
						int i_61_ = anInt6467 * i_59_ >> 12;
						int i_62_ = i_28_ * i_59_ >> 12;
						int i_63_ = i_62_ >> 12;
						int i_64_ = i_63_ + 1;
						i_62_ &= 0xfff;
						if (i_60_ <= i_64_)
							i_64_ = 0;
						int i_65_ = aByteArray6466[i_64_ & 0xff] & 0xff;
						int i_66_ = aByteArray6466[i_63_ & 0xff] & 0xff;
						int i_67_ = Class296_Sub39_Sub10.anIntArray6180[i_62_];
						if (aBoolean6469 && anInt6472 - 1 == i_58_) {
							for (int i_68_ = 0; i_68_ < Class41_Sub10.anInt3769; i_68_++) {
								int i_69_ = Class33.anIntArray334[i_68_] * anInt6467;
								int i_70_ = method3144(i_66_, i_62_, i_67_, i_65_, i_61_, (byte) -111, i_59_ * i_69_ >> 12);
								i_70_ = is[i_68_] + (i_70_ * i_45_ >> 12);
								is[i_68_] = (i_70_ >> 1) + 2048;
							}
						} else {
							for (int i_71_ = 0; Class41_Sub10.anInt3769 > i_71_; i_71_++) {
								int i_72_ = Class33.anIntArray334[i_71_] * anInt6467;
								int i_73_ = method3144(i_66_, i_62_, i_67_, i_65_, i_61_, (byte) -118, i_59_ * i_72_ >> 12);
								is[i_71_] += i_45_ * i_73_ >> 12;
							}
						}
					}
				}
			}
		}
	}

	final void method3076(byte i) {
		aByteArray6466 = GameClient.method117(true, anInt6474);
		method3143(114);
		for (int i_74_ = anInt6472 - 1; i_74_ >= 1; i_74_--) {
			short i_75_ = aShortArray6468[i_74_];
			if (i_75_ > 8 || i_75_ < -8)
				break;
			anInt6472--;
		}
		int i_76_ = 12 % ((-58 - i) / 40);
	}
}
