package net.zaros.client;

/* Class123_Sub2_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class123_Sub2_Sub2 extends Class123_Sub2 {
	static int[][] anIntArrayArray5820 = new int[6][];
	private byte[] aByteArray5821;
	static int anInt5822 = 0;

	public static void method1075(int i) {
		if (i != -24536)
			anInt5822 = -13;
		anIntArrayArray5820 = null;
	}

	public Class123_Sub2_Sub2() {
		super(12, 5, 16, 2, 2, 0.45F);
	}

	final void method1072(byte i, int i_0_, byte i_1_) {
		i = (byte) (((i & 0xff) >> 1) + 127);
		int i_2_ = i_0_ * 2;
		if (i_1_ != -45)
			aByteArray5821 = null;
		aByteArray5821[i_2_++] = i;
		aByteArray5821[i_2_] = i;
	}

	static final void method1076(Player class338_sub3_sub1_sub3_sub1, byte i) {
		Class296_Sub36 class296_sub36 = ((Class296_Sub36) (ha_Sub1_Sub1.aClass263_5823.get((long) class338_sub3_sub1_sub3_sub1.index)));
		if (class296_sub36 == null)
			Class368_Sub20.method3862(null, class338_sub3_sub1_sub3_sub1.z, null, false, 0, class338_sub3_sub1_sub3_sub1, class338_sub3_sub1_sub3_sub1.waypointQueueX[0], class338_sub3_sub1_sub3_sub1.wayPointQueueY[0]);
		else
			class296_sub36.method2763(i ^ 0x7d);
		if (i != 125)
			method1075(50);
	}

	final byte[] method1077(int i, int i_3_, int i_4_, int i_5_) {
		aByteArray5821 = new byte[i_5_ * 2 * i_3_ * i_4_];
		if (i != -24924)
			method1072((byte) 14, 122, (byte) -96);
		this.method1051(i_5_, i_4_, 97, i_3_);
		return aByteArray5821;
	}
}
