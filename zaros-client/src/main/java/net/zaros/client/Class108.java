package net.zaros.client;

/* Class108 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class108 {
	final boolean method947(int i) {
		if (i != 27229)
			method947(97);
		return (Class130.aClass108_1339 == this | this == Class130.aClass108_1337);
	}

	static final void method948(int i) {
		if (Class274.aClass381_2524 != null)
			Class274.aClass381_2524.method3989((byte) -49);
		if (i == -2060) {
			if (Class325.aClass381_2869 != null)
				Class325.aClass381_2869.method3989((byte) -49);
		}
	}

	static final void method949(int i, int i_0_) {
		if (i_0_ == 11771) {
			Class296_Sub14 class296_sub14 = ((Class296_Sub14) Class296_Sub51_Sub11.aClass263_6399.get((long) i));
			if (class296_sub14 != null) {
				class296_sub14.aClass219_Sub1_4667.method2045((byte) -66);
				Class338_Sub3_Sub3_Sub2.method3564(-1, class296_sub14.anInt4658, (class296_sub14.aBoolean4664));
				class296_sub14.unlink();
			}
		}
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	public Class108() {
		/* empty */
	}

	static final void method950(int i) {
		if (i == -32060) {
			if (Class137.aHa1408 != null) {
				Class137.aHa1408.method1091((byte) -97);
				Class355.aClass55_5909 = null;
				Class137.aHa1408 = null;
			}
		}
	}
}
