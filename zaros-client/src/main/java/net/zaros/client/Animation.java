package net.zaros.client;

/* Class43 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Animation {
	int anInt399 = -1;
	int[] anIntArray400;
	int weaponDisplayed;
	int[] anIntArray402;
	int anInt403;
	int anInt404 = -1;
	int[] anIntArray405;
	static float aFloat406;
	boolean isTweened;
	int anInt408;
	boolean aBoolean409;
	int anInt410;
	static long aLong411 = 0L;
	int shieldDisplayed;
	boolean aBoolean413;
	boolean[] aBooleanArray414;
	int[] anIntArray415;
	int[] anIntArray416;
	int walkingProperties;
	int[][] anIntArrayArray418;
	int[] anIntArray419;
	static IncomingPacket aClass231_420 = new IncomingPacket(30, 3);
	int ID;
	static Class294 aClass294_422 = new Class294(4, 1);

	private final void readOpcode(int opcode, Packet buff) {
		if (opcode == 1) {
			int i_2_ = buff.g2();
			anIntArray405 = new int[i_2_];
			for (int i_3_ = 0; i_3_ < i_2_; i_3_++) {
				anIntArray405[i_3_] = buff.g2();
			}
			anIntArray402 = new int[i_2_];
			for (int i_4_ = 0; i_4_ < i_2_; i_4_++) {
				anIntArray402[i_4_] = buff.g2();
			}
			for (int i_5_ = 0; i_2_ > i_5_; i_5_++) {
				anIntArray402[i_5_] = (buff.g2() << 16) + anIntArray402[i_5_];
			}
		} else if (opcode == 2) {
			anInt399 = buff.g2();//5651, 9867
		} else if (opcode == 3) {
			aBooleanArray414 = new boolean[256];
			int i_6_ = buff.g1();
			for (int i_7_ = 0; i_6_ > i_7_; i_7_++) {
				aBooleanArray414[buff.g1()] = true;
			}
		} else if (opcode == 5) {
			anInt408 = buff.g1();
		} else if (opcode != 6) {
			if (opcode == 7) {
				weaponDisplayed = buff.g2();
			} else if (opcode != 8) {
				if (opcode != 9) {
					if (opcode != 10) {
						if (opcode != 11) {
							if (opcode == 12) {
								int i_8_ = buff.g1();
								anIntArray419 = new int[i_8_];
								for (int i_9_ = 0; i_9_ < i_8_; i_9_++) {
									anIntArray419[i_9_] = buff.g2();
								}
								for (int i_10_ = 0; i_8_ > i_10_; i_10_++) {
									anIntArray419[i_10_] = (buff.g2() << 16) + anIntArray419[i_10_];
								}
							} else if (opcode == 13) {
								int i_11_ = buff.g2();
								anIntArrayArray418 = new int[i_11_][];
								for (int i_12_ = 0; i_12_ < i_11_; i_12_++) {
									int i_13_ = buff.g1();
									if (i_13_ > 0) {
										anIntArrayArray418[i_12_] = new int[i_13_];
										anIntArrayArray418[i_12_][0] = buff.readUnsignedMedInt();
										for (int i_14_ = 1; i_13_ > i_14_; i_14_++) {
											anIntArrayArray418[i_12_][i_14_] = buff.g2();
										}
									}
								}
							} else if (opcode != 14) {
								if (opcode != 15) {
									if (opcode != 16) {
										if (opcode == 18) {
											aBoolean409 = true;
										} else if (opcode == 19) {
											if (anIntArray416 == null) {
												anIntArray416 = new int[anIntArrayArray418.length];
												for (int i_15_ = 0; i_15_ < anIntArrayArray418.length; i_15_++) {
													anIntArray416[i_15_] = 255;
												}
											}
											anIntArray416[buff.g1()] = buff.g1();
										} else if (opcode == 20) {
											if (anIntArray415 == null || anIntArray400 == null) {
												anIntArray415 = new int[anIntArrayArray418.length];
												anIntArray400 = new int[anIntArrayArray418.length];
												for (int i_16_ = 0; anIntArrayArray418.length > i_16_; i_16_++) {
													anIntArray415[i_16_] = 256;
													anIntArray400[i_16_] = 256;
												}
											}
											int i_17_ = buff.g1();
											anIntArray415[i_17_] = buff.g2();
											anIntArray400[i_17_] = buff.g2();
										}
									}
								} else {
									isTweened = true;
								}
							} else {
								aBoolean413 = true;
							}
						} else {
							anInt410 = buff.g1();
						}
					} else {
						walkingProperties = buff.g1();
					}
				} else {
					anInt404 = buff.g1();
				}
			} else {
				anInt403 = buff.g1();
			}
		} else {
			shieldDisplayed = buff.g2();
		}
	}

	final void init(Packet buff) {
		for (;;) {
			int opc = buff.g1();
			if (opc == 0) {
				break;
			}
			readOpcode(opc, buff);
		}
	}

	static final void method542(boolean bool) {
		for (Class296_Sub39_Sub18 class296_sub39_sub18 = (Class296_Sub39_Sub18) Class72.aClass155_844.removeFirst((byte) 115); class296_sub39_sub18 != null; class296_sub39_sub18 = (Class296_Sub39_Sub18) Class72.aClass155_844.removeNext(1001)) {
			Class338_Sub3_Sub1_Sub2 class338_sub3_sub1_sub2 = class296_sub39_sub18.aClass338_Sub3_Sub1_Sub2_6246;
			if (Class29.anInt307 <= class338_sub3_sub1_sub2.anInt6738) {
				if (Class29.anInt307 >= class338_sub3_sub1_sub2.anInt6751) {
					class338_sub3_sub1_sub2.method3487(5937);
					if (class338_sub3_sub1_sub2.anInt6736 > 0) {
						if (za_Sub1.anInt6554 == 3) {
							Mobile class338_sub3_sub1_sub3 = Class379.aClass302Array3624[class338_sub3_sub1_sub2.anInt6736 - 1].method3262(0);
							if (class338_sub3_sub1_sub3 != null && class338_sub3_sub1_sub3.tileX >= 0 && class338_sub3_sub1_sub3.tileX < Class198.currentMapSizeX * 512 && class338_sub3_sub1_sub3.tileY >= 0 && class338_sub3_sub1_sub3.tileY < Class296_Sub38.currentMapSizeY * 512) {
								class338_sub3_sub1_sub2.method3491(class338_sub3_sub1_sub3.tileX, aa_Sub1.method155(-1537652855, class338_sub3_sub1_sub2.z, class338_sub3_sub1_sub3.tileX, class338_sub3_sub1_sub3.tileY) - class338_sub3_sub1_sub2.anInt6762, Class29.anInt307, class338_sub3_sub1_sub3.tileY, -1926441266);
							}
						} else {
							NPCNode class296_sub7 = (NPCNode) Class41_Sub18.localNpcs.get(class338_sub3_sub1_sub2.anInt6736 - 1);
							if (class296_sub7 != null) {
								NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
								if (class338_sub3_sub1_sub3_sub2.tileX >= 0 && class338_sub3_sub1_sub3_sub2.tileX < Class198.currentMapSizeX * 512 && class338_sub3_sub1_sub3_sub2.tileY >= 0 && class338_sub3_sub1_sub3_sub2.tileY < Class296_Sub38.currentMapSizeY * 512) {
									class338_sub3_sub1_sub2.method3491(class338_sub3_sub1_sub3_sub2.tileX, aa_Sub1.method155(-1537652855, class338_sub3_sub1_sub2.z, class338_sub3_sub1_sub3_sub2.tileX, class338_sub3_sub1_sub3_sub2.tileY) - class338_sub3_sub1_sub2.anInt6762, Class29.anInt307, class338_sub3_sub1_sub3_sub2.tileY, -1926441266);
								}
							}
						}
					}
					if (class338_sub3_sub1_sub2.anInt6736 < 0) {
						int i = -class338_sub3_sub1_sub2.anInt6736 - 1;
						Player class338_sub3_sub1_sub3_sub1;
						if (Class362.myPlayerIndex == i) {
							class338_sub3_sub1_sub3_sub1 = Class296_Sub51_Sub11.localPlayer;
						} else {
							class338_sub3_sub1_sub3_sub1 = PlayerUpdate.visiblePlayers[i];
						}
						if (class338_sub3_sub1_sub3_sub1 != null && class338_sub3_sub1_sub3_sub1.tileX >= 0 && class338_sub3_sub1_sub3_sub1.tileX < Class198.currentMapSizeX * 512 && class338_sub3_sub1_sub3_sub1.tileY >= 0 && Class296_Sub38.currentMapSizeY * 512 > class338_sub3_sub1_sub3_sub1.tileY) {
							class338_sub3_sub1_sub2.method3491(class338_sub3_sub1_sub3_sub1.tileX, aa_Sub1.method155(-1537652855, class338_sub3_sub1_sub2.z, class338_sub3_sub1_sub3_sub1.tileX, class338_sub3_sub1_sub3_sub1.tileY) - class338_sub3_sub1_sub2.anInt6762, Class29.anInt307, class338_sub3_sub1_sub3_sub1.tileY, -1926441266);
						}
					}
					class338_sub3_sub1_sub2.method3489(!bool, Class217.anInt2119);
					Class125.method1079(class338_sub3_sub1_sub2, true);
				}
			} else {
				class296_sub39_sub18.unlink();
				class338_sub3_sub1_sub2.method3486(1643253353);
			}
		}
		if (bool != true) {
			method542(false);
		}
	}

	public static void method543(byte i) {
		if (i != 92) {
			aLong411 = -48L;
		}
		aClass231_420 = null;
		aClass294_422 = null;
	}

	final void method544(boolean bool) {
		if (bool != true) {
			readOpcode(-114, null);
		}
		if (anInt404 == -1) {
			if (aBooleanArray414 != null) {
				anInt404 = 2;
			} else {
				anInt404 = 0;
			}
		}
		if (walkingProperties == -1) {
			if (aBooleanArray414 != null) {
				walkingProperties = 2;
			} else {
				walkingProperties = 0;
			}
		}
	}

	public Animation() {
		anInt403 = 99;
		anInt410 = 2;
		aBoolean413 = false;
		aBoolean409 = false;
		walkingProperties = -1;
		anInt408 = 5;
		isTweened = false;
		weaponDisplayed = -1;
		shieldDisplayed = -1;
	}
}
