package net.zaros.client;
/* Class41_Sub18 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Date;

final class Class41_Sub18 extends Class41 {
	static byte[][][] aByteArrayArrayArray3786;
	static HashTable localNpcs = new HashTable(64);
	static Class186 aClass186_3788;
	static IncomingPacket aClass231_3789 = new IncomingPacket(35, 7);

	final void method381(int i, byte i_0_) {
		anInt389 = i;
		if (i_0_ != -110)
			aClass231_3789 = null;
	}

	static final void writeConsoleMessage(String string) {
		if (Class296_Sub51_Sub15.aStringArray6425 == null)
			Class318.method3336((byte) -73);
		Class366_Sub1.aCalendar5363.setTime(new Date(Class72.method771(-127)));
		int i_1_ = Class366_Sub1.aCalendar5363.get(11);
		int i_2_ = Class366_Sub1.aCalendar5363.get(12);
		int i_3_ = Class366_Sub1.aCalendar5363.get(13);
		String string_4_ = (Integer.toString(i_1_ / 10) + i_1_ % 10 + ":" + i_2_ / 10 + i_2_ % 10 + ":" + i_3_ / 10 + i_3_ % 10);
		String[] strings = Class41_Sub30.method522((byte) 63, string, '\n');
		for (int i_5_ = 0; strings.length > i_5_; i_5_++) {
			for (int i_6_ = Class357.anInt3082; i_6_ > 0; i_6_--)
				Class296_Sub51_Sub15.aStringArray6425[i_6_] = Class296_Sub51_Sub15.aStringArray6425[i_6_ - 1];
			Class296_Sub51_Sub15.aStringArray6425[0] = string_4_ + ": " + strings[i_5_];
			if (MaterialRaw.aFileOutputStream1776 != null) {
				try {
					MaterialRaw.aFileOutputStream1776.write(Class280.method2345((byte) -84, ((Class296_Sub51_Sub15.aStringArray6425[0]) + "\n")));
				} catch (java.io.IOException ioexception) {
					/* empty */
				}
			}
			if (Class296_Sub51_Sub15.aStringArray6425.length - 1 > Class357.anInt3082) {
				Class357.anInt3082++;
				if (za_Sub1.anInt6552 > 0)
					za_Sub1.anInt6552++;
			}
		}
	}

	final int method383(byte i) {
		if (i != 110)
			method380(-27, (byte) 33);
		return 2;
	}

	final void method386(int i) {
		if (aClass296_Sub50_392.method3054(i ^ 0x43))
			anInt389 = 0;
		if (aClass296_Sub50_392.aClass41_Sub26_4991.method497(126) == 0)
			anInt389 = 0;
		if (anInt389 < 0 || anInt389 > 2)
			anInt389 = method383((byte) 110);
		if (i != 2)
			method380(41, (byte) -78);
	}

	Class41_Sub18(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final boolean method463(int i) {
		if (i != -25952)
			localNpcs = null;
		if (aClass296_Sub50_392.method3054(99))
			return false;
		if (aClass296_Sub50_392.aClass41_Sub26_4991.method497(125) == 0)
			return false;
		return true;
	}

	public static void method464(int i) {
		aClass231_3789 = null;
		localNpcs = null;
		if (i == 2) {
			aClass186_3788 = null;
			aByteArrayArrayArray3786 = null;
		}
	}

	final int method465(int i) {
		if (i < 114)
			aByteArrayArrayArray3786 = null;
		return anInt389;
	}

	Class41_Sub18(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final int method380(int i, byte i_7_) {
		if (i_7_ != 41)
			method464(-56);
		if (aClass296_Sub50_392.method3054(88))
			return 3;
		if (aClass296_Sub50_392.aClass41_Sub26_4991.method497(125) == 0)
			return 3;
		return 1;
	}
}
