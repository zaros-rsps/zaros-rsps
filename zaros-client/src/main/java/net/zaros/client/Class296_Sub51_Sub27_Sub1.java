package net.zaros.client;
/* Class296_Sub51_Sub27_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Date;

final class Class296_Sub51_Sub27_Sub1 extends Class296_Sub51_Sub27 {
	static World aClass112_6733;
	static int anInt6734;

	public static void method3156(byte i) {
		aClass112_6733 = null;
		if (i != 6)
			method3160(null, -95);
	}

	public Class296_Sub51_Sub27_Sub1() {
		/* empty */
	}

	static final void method3157(boolean bool) {
		if (Class42_Sub3.anInt3844 != -1 && ClotchesLoader.anInt279 != -1) {
			int i = ((((-Class156.anInt3593 + StaticMethods.anInt3196) * Class296_Sub45_Sub4.anInt6316) >> 16) + Class156.anInt3593);
			Class296_Sub45_Sub4.anInt6316 += i;
			if (Class296_Sub45_Sub4.anInt6316 < 65535) {
				Class296_Sub45_Sub4.aBoolean6319 = false;
				Class241_Sub1_Sub1.aBoolean5890 = false;
			} else {
				Class296_Sub45_Sub4.anInt6316 = 65535;
				if (!Class296_Sub45_Sub4.aBoolean6319)
					Class241_Sub1_Sub1.aBoolean5890 = true;
				else
					Class241_Sub1_Sub1.aBoolean5890 = false;
				Class296_Sub45_Sub4.aBoolean6319 = true;
			}
			float f = (float) Class296_Sub45_Sub4.anInt6316 / 65535.0F;
			float[] fs = new float[3];
			int i_0_ = Class296_Sub22.anInt4739 * 2;
			for (int i_1_ = 0; i_1_ < 3; i_1_++) {
				int i_2_ = ((Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844][i_0_][i_1_]) * 3);
				int i_3_ = ((Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844][i_0_ + 1][i_1_]) * 3);
				int i_4_ = ((-(Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844][i_0_ + 3][i_1_]) + (Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844][i_0_ + 2][i_1_]) + (Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844][i_0_ + 2][i_1_])) * 3);
				int i_5_ = (Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844][i_0_][i_1_]);
				int i_6_ = i_3_ - i_2_;
				int i_7_ = i_4_ + (-(i_3_ * 2) + i_2_);
				int i_8_ = (i_3_ + (Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844][i_0_ + 2][i_1_]) + (-i_5_ - i_4_));
				fs[i_1_] = (((float) i_7_ + (float) i_8_ * f) * f + (float) i_6_) * f + (float) i_5_;
			}
			Class219.camPosX = (int) fs[0] - Class206.worldBaseX * 512;
			TranslatableString.camPosY = (int) fs[1] * -1;
			Class124.camPosZ = -(Class41_Sub26.worldBaseY * 512) + (int) fs[2];
			float[] fs_9_ = new float[3];
			int i_10_ = Class296_Sub51_Sub28.anInt6489 * 2;
			if (bool)
				method3157(false);
			for (int i_11_ = 0; i_11_ < 3; i_11_++) {
				int i_12_ = ((Class123_Sub1.anIntArrayArrayArray3909[ClotchesLoader.anInt279][i_10_][i_11_]) * 3);
				int i_13_ = ((Class123_Sub1.anIntArrayArrayArray3909[ClotchesLoader.anInt279][i_10_ + 1][i_11_]) * 3);
				int i_14_ = (((Class123_Sub1.anIntArrayArrayArray3909[ClotchesLoader.anInt279][i_10_ + 2][i_11_]) + ((Class123_Sub1.anIntArrayArrayArray3909[ClotchesLoader.anInt279][i_10_ + 2][i_11_]) - (Class123_Sub1.anIntArrayArrayArray3909[ClotchesLoader.anInt279][i_10_ + 3][i_11_]))) * 3);
				int i_15_ = (Class123_Sub1.anIntArrayArrayArray3909[ClotchesLoader.anInt279][i_10_][i_11_]);
				int i_16_ = i_13_ - i_12_;
				int i_17_ = i_14_ + (-(i_13_ * 2) + i_12_);
				int i_18_ = -i_14_ + i_13_ + ((Class123_Sub1.anIntArrayArrayArray3909[ClotchesLoader.anInt279][i_10_ + 2][i_11_]) - i_15_);
				fs_9_[i_11_] = (f * ((float) i_17_ + (float) i_18_ * f) + (float) i_16_) * f + (float) i_15_;
			}
			float f_19_ = -fs[0] + fs_9_[0];
			float f_20_ = (fs_9_[1] - fs[1]) * -1.0F;
			float f_21_ = fs_9_[2] - fs[2];
			double d = Math.sqrt((double) (f_21_ * f_21_ + f_19_ * f_19_));
			Class296_Sub17_Sub2.camRotX = ((int) (Math.atan2((double) f_20_, d) * 2607.5945876176133) & 0x3fff);
			Class44_Sub1.camRotY = (int) (-Math.atan2((double) f_19_, (double) f_21_) * 2607.5945876176133) & 0x3fff;
			Class268.anInt2488 = ((Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844][i_0_][3]) + ((Class296_Sub45_Sub4.anInt6316 * (-(Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844][i_0_][3]) + (Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844][i_0_ + 2][3]))) >> 16));
		}
	}

	static final void method3158(int i, Js5 class138, Js5 class138_22_, Js5 class138_23_, Js5 class138_24_) {
		if (i != 10816)
			anInt6734 = 80;
		Class226_Sub1.aClass138_4573 = class138_23_;
		BillboardRaw.interfacesFS = class138_22_;
		Class296_Sub51_Sub31.aClass138_6510 = class138_24_;
		Class192.openedInterfaceComponents = new InterfaceComponent[BillboardRaw.interfacesFS.getLastGroupId()][];
		Class219_Sub1.loadedInterfaces = new boolean[BillboardRaw.interfacesFS.getLastGroupId()];
	}

	static final void method3159(int i, long l) {
		if (i < 101)
			aClass112_6733 = null;
		Class296_Sub38.aCalendar4898.setTime(new Date(l));
	}

	static final boolean method3160(ModeWhere class248, int i) {
		if (i != 30618)
			aClass112_6733 = null;
		if (ModeWhere.officeModeWhere != class248 && class248 != Class121.office2ModeWhere && class248 != Class3.office3ModeWhere && class248 != Class294.office4ModeWhere && Class41_Sub12.intBetaModeWhere != class248)
			return false;
		return true;
	}

	final int[][] get_colour_output(int i, int i_25_) {
		int[][] is = aClass86_5034.method823((byte) 122, i);
		if (aClass86_5034.aBoolean939 && this.method3155(1696640376)) {
			int[] is_26_ = is[0];
			int[] is_27_ = is[1];
			int[] is_28_ = is[2];
			int i_29_ = anInt6482 * (i % anInt6482);
			for (int i_30_ = 0; i_30_ < Class41_Sub10.anInt3769; i_30_++) {
				int i_31_ = anIntArray6478[i_30_ % anInt6481 + i_29_];
				is_28_[i_30_] = i_31_ << 4 & 4080;
				is_27_[i_30_] = (65280 & i_31_) >> 4;
				is_26_[i_30_] = i_31_ >> 12 & 4080;
			}
		}
		if (i_25_ != 17621)
			return null;
		return is;
	}

	static final boolean method3161(int i, int i_32_, int i_33_) {
		if (i != -19214)
			anInt6734 = 107;
		if ((i_33_ & 0x84080) == 0)
			return false;
		return true;
	}

	static final void method3162(int i) {
		if (Class295_Sub1.anIntArray3691 == null) {
			Class295_Sub1.anIntArray3691 = new int[65536];
			double d = Math.random() * 0.03 + -0.015 + 0.7;
			int i_34_ = -120 / ((i + 50) / 48);
			int i_35_ = 0;
			for (int i_36_ = 0; i_36_ < 512; i_36_++) {
				float f = ((float) (i_36_ >> 3) / 64.0F + 0.0078125F) * 360.0F;
				float f_37_ = (float) (i_36_ & 0x7) / 8.0F + 0.0625F;
				for (int i_38_ = 0; i_38_ < 128; i_38_++) {
					float f_39_ = (float) i_38_ / 128.0F;
					float f_40_ = 0.0F;
					float f_41_ = 0.0F;
					float f_42_ = 0.0F;
					float f_43_ = f / 60.0F;
					int i_44_ = (int) f_43_;
					int i_45_ = i_44_ % 6;
					float f_46_ = f_43_ - (float) i_44_;
					float f_47_ = (1.0F - f_37_) * f_39_;
					float f_48_ = (-(f_46_ * f_37_) + 1.0F) * f_39_;
					float f_49_ = (1.0F - f_37_ * (-f_46_ + 1.0F)) * f_39_;
					if (i_45_ == 0) {
						f_40_ = f_39_;
						f_42_ = f_47_;
						f_41_ = f_49_;
					} else if (i_45_ != 1) {
						if (i_45_ == 2) {
							f_42_ = f_49_;
							f_41_ = f_39_;
							f_40_ = f_47_;
						} else if (i_45_ == 3) {
							f_42_ = f_39_;
							f_41_ = f_48_;
							f_40_ = f_47_;
						} else if (i_45_ != 4) {
							if (i_45_ == 5) {
								f_42_ = f_48_;
								f_40_ = f_39_;
								f_41_ = f_47_;
							}
						} else {
							f_42_ = f_39_;
							f_40_ = f_49_;
							f_41_ = f_47_;
						}
					} else {
						f_41_ = f_39_;
						f_42_ = f_47_;
						f_40_ = f_48_;
					}
					f_40_ = (float) Math.pow((double) f_40_, d);
					f_41_ = (float) Math.pow((double) f_41_, d);
					f_42_ = (float) Math.pow((double) f_42_, d);
					int i_50_ = (int) (f_40_ * 256.0F);
					int i_51_ = (int) (f_41_ * 256.0F);
					int i_52_ = (int) (f_42_ * 256.0F);
					int i_53_ = (i_50_ << 16) + ((i_51_ << 8) - 16777216) + i_52_;
					Class295_Sub1.anIntArray3691[i_35_++] = i_53_;
				}
			}
		}
	}
}
