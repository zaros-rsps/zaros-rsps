package net.zaros.client;

/* Class338_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class338_Sub3 extends Class338 {
	Class338_Sub3 aClass338_Sub3_5202;
	byte aByte5203;
	int anInt5204;
	byte z;
	boolean aBoolean5206 = false;
	boolean aBoolean5207;
	int anInt5208;
	static int anInt5209 = 255;
	int tileY;
	int anInt5211;
	int tileX;
	int anInt5213;

	int method3458(int i) {
		if (i != 0)
			return 84;
		return 0;
	}

	abstract boolean method3459(int i);

	abstract void method3460(int i, ha var_ha);

	abstract Class96 method3461(ha var_ha, int i);

	abstract int method3462(byte i);

	abstract int method3463(int i, Class296_Sub35[] class296_sub35s);

	static final void method3464(int i, int i_0_, int i_1_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_0_][i_1_];
		if (class247 != null) {
			Class230.method2106(class247.aClass338_Sub3_Sub5_2346);
			if (class247.aClass338_Sub3_Sub5_2346 != null)
				class247.aClass338_Sub3_Sub5_2346 = null;
		}
	}

	abstract boolean method3465(int i);

	abstract int method3466(byte i);

	abstract void method3467(int i, int i_2_, Class338_Sub3 class338_sub3_3_, boolean bool, int i_4_, int i_5_, ha var_ha);

	abstract boolean method3468(int i);

	abstract boolean method3469(int i);

	abstract boolean method3470(int i, ha var_ha);

	static final void method3471(int i, int i_6_, InterfaceComponent class51, int i_7_) {
		if (i_6_ >= -114)
			anInt5209 = -110;
		Class98 class98 = class51.method633((byte) -79, Class41_Sub13.aHa3774);
		if (class98 != null) {
			Class41_Sub13.aHa3774.KA(i, i_7_, i + class51.anInt578, class51.anInt623 + i_7_);
			if (Class338_Sub3_Sub5_Sub1.anInt6645 >= 3)
				Class41_Sub13.aHa3774.A(-16777216, class98.anAa1056, i, i_7_);
			else
				Class296_Sub39_Sub17.aClass397_6237.method4086((float) i + (float) class51.anInt578 / 2.0F, (float) class51.anInt623 / 2.0F + (float) i_7_, 4096, (int) -Class41_Sub26.aFloat3806 << 2 & 0xfffc, class98.anAa1056, i, i_7_);
		}
	}

	abstract void method3472(byte i);

	abstract Class338_Sub2 method3473(ha var_ha, byte i);

	final int method3474(int i, Class296_Sub35[] class296_sub35s, int i_8_, int i_9_) {
		long l = Class49.aLongArrayArrayArray462[z][i][i_8_];
		long l_10_ = 0L;
		if (i_9_ != 4)
			return 91;
		int i_11_ = 0;
		for (/**/; l_10_ <= 48L; l_10_ += 16L) {
			int i_12_ = (int) (l >> (int) l_10_ & 0xffffL);
			if (i_12_ <= 0)
				break;
			class296_sub35s[i_11_++] = Class302.aClass93Array2721[i_12_ - 1].aClass296_Sub35_999;
		}
		for (int i_13_ = i_11_; i_13_ < 4; i_13_++)
			class296_sub35s[i_13_] = null;
		return i_11_;
	}

	abstract boolean method3475(int i, int i_14_, ha var_ha, int i_15_);

	public Class338_Sub3() {
		/* empty */
	}
}
