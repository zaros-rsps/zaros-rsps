package net.zaros.client;

/* Class186 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class186 {
	public int anInt1899;
	public int[] anIntArray1900;
	public byte[] aByteArray1901;
	public int anInt1902;
	public int anInt1903;
	public int anInt1904;
	public byte[] aByteArray1905;
	public int anInt1906;
	public int anInt1907;

	public static final Class186[] method1868(Js5 class138, int i) {
		byte[] is = class138.get(i);
		if (is == null)
			return null;
		return method1881(is);
	}

	public int[] method1869() {
		int i = method1880();
		int[] is = new int[i * method1882()];
		if (aByteArray1905 != null) {
			for (int i_0_ = 0; i_0_ < anInt1904; i_0_++) {
				int i_1_ = i_0_ * anInt1899;
				int i_2_ = anInt1903 + (i_0_ + anInt1906) * i;
				for (int i_3_ = 0; i_3_ < anInt1899; i_3_++) {
					is[i_2_++] = (aByteArray1905[i_1_] << 24 | anIntArray1900[aByteArray1901[i_1_] & 0xff]);
					i_1_++;
				}
			}
		} else {
			for (int i_4_ = 0; i_4_ < anInt1904; i_4_++) {
				int i_5_ = i_4_ * anInt1899;
				int i_6_ = anInt1903 + (i_4_ + anInt1906) * i;
				for (int i_7_ = 0; i_7_ < anInt1899; i_7_++) {
					int i_8_ = anIntArray1900[aByteArray1901[i_5_++] & 0xff];
					if (i_8_ != 0)
						is[i_6_++] = i_8_ | ~0xffffff;
					else
						is[i_6_++] = 0;
				}
			}
		}
		return is;
	}

	public void method1870(int i) {
		int i_9_ = method1880();
		int i_10_ = method1882();
		if (anInt1899 != i_9_ || anInt1904 != i_10_) {
			int i_11_ = i;
			if (i_11_ > anInt1903)
				i_11_ = anInt1903;
			int i_12_ = i;
			if (i_12_ + anInt1903 + anInt1899 > i_9_)
				i_12_ = i_9_ - anInt1903 - anInt1899;
			int i_13_ = i;
			if (i_13_ > anInt1906)
				i_13_ = anInt1906;
			int i_14_ = i;
			if (i_14_ + anInt1906 + anInt1904 > i_10_)
				i_14_ = i_10_ - anInt1906 - anInt1904;
			int i_15_ = anInt1899 + i_11_ + i_12_;
			int i_16_ = anInt1904 + i_13_ + i_14_;
			byte[] is = new byte[i_15_ * i_16_];
			if (aByteArray1905 == null) {
				for (int i_17_ = 0; i_17_ < anInt1904; i_17_++) {
					int i_18_ = i_17_ * anInt1899;
					int i_19_ = (i_17_ + i_13_) * i_15_ + i_11_;
					for (int i_20_ = 0; i_20_ < anInt1899; i_20_++)
						is[i_19_++] = aByteArray1901[i_18_++];
				}
			} else {
				byte[] is_21_ = new byte[i_15_ * i_16_];
				for (int i_22_ = 0; i_22_ < anInt1904; i_22_++) {
					int i_23_ = i_22_ * anInt1899;
					int i_24_ = (i_22_ + i_13_) * i_15_ + i_11_;
					for (int i_25_ = 0; i_25_ < anInt1899; i_25_++) {
						is_21_[i_24_] = aByteArray1905[i_23_];
						is[i_24_++] = aByteArray1901[i_23_++];
					}
				}
				aByteArray1905 = is_21_;
			}
			anInt1903 -= i_11_;
			anInt1906 -= i_13_;
			anInt1907 -= i_12_;
			anInt1902 -= i_14_;
			anInt1899 = i_15_;
			anInt1904 = i_16_;
			aByteArray1901 = is;
		}
	}

	public static final Class186[] method1871(Js5 class138, int i, int i_26_) {
		byte[] is = class138.getFile(i, i_26_);
		if (is == null)
			return null;
		return method1881(is);
	}

	public static final Class186 method1872(Js5 class138, int i) {
		byte[] is = class138.get(i);
		if (is == null)
			return null;
		return method1881(is)[0];
	}

	public void method1873() {
		byte[] is = new byte[anInt1899 * anInt1904];
		int i = 0;
		if (aByteArray1905 == null) {
			for (int i_27_ = 0; i_27_ < anInt1899; i_27_++) {
				for (int i_28_ = anInt1904 - 1; i_28_ >= 0; i_28_--)
					is[i++] = aByteArray1901[i_27_ + i_28_ * anInt1899];
			}
			aByteArray1901 = is;
		} else {
			byte[] is_29_ = new byte[anInt1899 * anInt1904];
			for (int i_30_ = 0; i_30_ < anInt1899; i_30_++) {
				for (int i_31_ = anInt1904 - 1; i_31_ >= 0; i_31_--) {
					is[i] = aByteArray1901[i_30_ + i_31_ * anInt1899];
					is_29_[i++] = aByteArray1905[i_30_ + i_31_ * anInt1899];
				}
			}
			aByteArray1901 = is;
			aByteArray1905 = is_29_;
		}
		int i_32_ = anInt1906;
		anInt1906 = anInt1903;
		anInt1903 = anInt1902;
		anInt1902 = anInt1907;
		anInt1907 = anInt1906;
		i_32_ = anInt1904;
		anInt1904 = anInt1899;
		anInt1899 = i_32_;
	}

	public void method1874() {
		int i = method1880();
		int i_33_ = method1882();
		if (anInt1899 != i || anInt1904 != i_33_) {
			byte[] is = new byte[i * i_33_];
			if (aByteArray1905 != null) {
				byte[] is_34_ = new byte[i * i_33_];
				for (int i_35_ = 0; i_35_ < anInt1904; i_35_++) {
					int i_36_ = i_35_ * anInt1899;
					int i_37_ = (i_35_ + anInt1906) * i + anInt1903;
					for (int i_38_ = 0; i_38_ < anInt1899; i_38_++) {
						is[i_37_] = aByteArray1901[i_36_];
						is_34_[i_37_++] = aByteArray1905[i_36_++];
					}
				}
				aByteArray1905 = is_34_;
			} else {
				for (int i_39_ = 0; i_39_ < anInt1904; i_39_++) {
					int i_40_ = i_39_ * anInt1899;
					int i_41_ = (i_39_ + anInt1906) * i + anInt1903;
					for (int i_42_ = 0; i_42_ < anInt1899; i_42_++)
						is[i_41_++] = aByteArray1901[i_40_++];
				}
			}
			anInt1903 = anInt1907 = anInt1906 = anInt1902 = 0;
			anInt1899 = i;
			anInt1904 = i_33_;
			aByteArray1901 = is;
		}
	}

	public void method1875() {
		byte[] is = aByteArray1901;
		if (aByteArray1905 == null) {
			for (int i = (anInt1904 >> 1) - 1; i >= 0; i--) {
				int i_43_ = i * anInt1899;
				int i_44_ = (anInt1904 - i - 1) * anInt1899;
				for (int i_45_ = -anInt1899; i_45_ < 0; i_45_++) {
					byte i_46_ = is[i_43_];
					is[i_43_] = is[i_44_];
					is[i_44_] = i_46_;
					i_43_++;
					i_44_++;
				}
			}
		} else {
			byte[] is_47_ = aByteArray1905;
			for (int i = (anInt1904 >> 1) - 1; i >= 0; i--) {
				int i_48_ = i * anInt1899;
				int i_49_ = (anInt1904 - i - 1) * anInt1899;
				for (int i_50_ = -anInt1899; i_50_ < 0; i_50_++) {
					byte i_51_ = is[i_48_];
					is[i_48_] = is[i_49_];
					is[i_49_] = i_51_;
					i_51_ = is_47_[i_48_];
					is_47_[i_48_] = is_47_[i_49_];
					is_47_[i_49_] = i_51_;
					i_48_++;
					i_49_++;
				}
			}
		}
		int i = anInt1906;
		anInt1906 = anInt1902;
		anInt1902 = i;
	}

	public static Class186 method1876(Js5 class138, int i, int i_52_) {
		byte[] is = class138.getFile(i, i_52_);
		if (is == null)
			return null;
		return method1881(is)[0];
	}

	public void method1877(int i) {
		int i_53_ = -1;
		if (anIntArray1900.length < 255) {
			for (int i_54_ = 0; i_54_ < anIntArray1900.length; i_54_++) {
				if (anIntArray1900[i_54_] == i) {
					i_53_ = i_54_;
					break;
				}
			}
			if (i_53_ == -1) {
				i_53_ = anIntArray1900.length;
				int[] is = new int[anIntArray1900.length + 1];
				ArrayTools.removeElements(anIntArray1900, 0, is, 0, anIntArray1900.length);
				anIntArray1900 = is;
				is[i_53_] = i;
			}
		} else {
			int i_55_ = 2147483647;
			int i_56_ = i >> 16 & 0xff;
			int i_57_ = i >> 8 & 0xff;
			int i_58_ = i & 0xff;
			for (int i_59_ = 0; i_59_ < anIntArray1900.length; i_59_++) {
				int i_60_ = anIntArray1900[i_59_];
				int i_61_ = i_60_ >> 16 & 0xff;
				int i_62_ = i_60_ >> 8 & 0xff;
				int i_63_ = i_60_ & 0xff;
				int i_64_ = i_56_ - i_61_;
				if (i_64_ < 0)
					i_64_ = -i_64_;
				int i_65_ = i_57_ - i_62_;
				if (i_65_ < 0)
					i_65_ = -i_65_;
				int i_66_ = i_58_ - i_63_;
				if (i_66_ < 0)
					i_66_ = -i_66_;
				int i_67_ = i_64_ + i_65_ + i_66_;
				if (i_67_ < i_55_) {
					i_55_ = i_67_;
					i_53_ = i_59_;
				}
			}
		}
		for (int i_68_ = anInt1904 - 1; i_68_ > 0; i_68_--) {
			int i_69_ = i_68_ * anInt1899;
			for (int i_70_ = anInt1899 - 1; i_70_ > 0; i_70_--) {
				if (anIntArray1900[aByteArray1901[i_70_ + i_69_] & 0xff] == 0
						&& (anIntArray1900[aByteArray1901[(i_70_ + i_69_ - 1 - anInt1899)] & 0xff] != 0))
					aByteArray1901[i_70_ + i_69_] = (byte) i_53_;
			}
		}
	}

	public void method1878(int i) {
		int i_71_ = -1;
		if (anIntArray1900.length < 255) {
			for (int i_72_ = 0; i_72_ < anIntArray1900.length; i_72_++) {
				if (anIntArray1900[i_72_] == i) {
					i_71_ = i_72_;
					break;
				}
			}
			if (i_71_ == -1) {
				i_71_ = anIntArray1900.length;
				int[] is = new int[anIntArray1900.length + 1];
				ArrayTools.removeElements(anIntArray1900, 0, is, 0, anIntArray1900.length);
				anIntArray1900 = is;
				is[i_71_] = i;
			}
		} else {
			int i_73_ = 2147483647;
			int i_74_ = i >> 16 & 0xff;
			int i_75_ = i >> 8 & 0xff;
			int i_76_ = i & 0xff;
			for (int i_77_ = 0; i_77_ < anIntArray1900.length; i_77_++) {
				int i_78_ = anIntArray1900[i_77_];
				int i_79_ = i_78_ >> 16 & 0xff;
				int i_80_ = i_78_ >> 8 & 0xff;
				int i_81_ = i_78_ & 0xff;
				int i_82_ = i_74_ - i_79_;
				if (i_82_ < 0)
					i_82_ = -i_82_;
				int i_83_ = i_75_ - i_80_;
				if (i_83_ < 0)
					i_83_ = -i_83_;
				int i_84_ = i_76_ - i_81_;
				if (i_84_ < 0)
					i_84_ = -i_84_;
				int i_85_ = i_82_ + i_83_ + i_84_;
				if (i_85_ < i_73_) {
					i_73_ = i_85_;
					i_71_ = i_77_;
				}
			}
		}
		int i_86_ = 0;
		byte[] is = new byte[anInt1899 * anInt1904];
		for (int i_87_ = 0; i_87_ < anInt1904; i_87_++) {
			for (int i_88_ = 0; i_88_ < anInt1899; i_88_++) {
				int i_89_ = aByteArray1901[i_86_] & 0xff;
				if (anIntArray1900[i_89_] == 0) {
					if (i_88_ > 0 && (anIntArray1900[aByteArray1901[i_86_ - 1] & 0xff] != 0))
						i_89_ = i_71_;
					else if (i_87_ > 0 && ((anIntArray1900[aByteArray1901[i_86_ - anInt1899] & 0xff]) != 0))
						i_89_ = i_71_;
					else if (i_88_ < anInt1899 - 1 && (anIntArray1900[aByteArray1901[i_86_ + 1] & 0xff]) != 0)
						i_89_ = i_71_;
					else if (i_87_ < anInt1904 - 1 && ((anIntArray1900[aByteArray1901[i_86_ + anInt1899] & 0xff]) != 0))
						i_89_ = i_71_;
				}
				is[i_86_++] = (byte) i_89_;
			}
		}
		aByteArray1901 = is;
	}

	public void method1879() {
		byte[] is = aByteArray1901;
		if (aByteArray1905 == null) {
			for (int i = anInt1904 - 1; i >= 0; i--) {
				int i_90_ = i * anInt1899;
				for (int i_91_ = (i + 1) * anInt1899; i_90_ < i_91_; i_90_++) {
					i_91_--;
					byte i_92_ = is[i_90_];
					is[i_90_] = is[i_91_];
					is[i_91_] = i_92_;
				}
			}
		} else {
			byte[] is_93_ = aByteArray1905;
			for (int i = anInt1904 - 1; i >= 0; i--) {
				int i_94_ = i * anInt1899;
				for (int i_95_ = (i + 1) * anInt1899; i_94_ < i_95_; i_94_++) {
					i_95_--;
					byte i_96_ = is[i_94_];
					is[i_94_] = is[i_95_];
					is[i_95_] = i_96_;
					i_96_ = is_93_[i_94_];
					is_93_[i_94_] = is_93_[i_95_];
					is_93_[i_95_] = i_96_;
				}
			}
		}
		int i = anInt1903;
		anInt1903 = anInt1907;
		anInt1907 = i;
	}

	public Class186() {
		/* empty */
	}

	final int method1880() {
		return anInt1899 + anInt1903 + anInt1907;
	}

	private static final Class186[] method1881(byte[] is) {
		Packet class296_sub17 = new Packet(is);
		class296_sub17.pos = is.length - 2;
		int i = class296_sub17.g2();
		Class186[] class186s = new Class186[i];
		for (int i_97_ = 0; i_97_ < i; i_97_++)
			class186s[i_97_] = new Class186();
		class296_sub17.pos = is.length - 7 - i * 8;
		int i_98_ = class296_sub17.g2();
		int i_99_ = class296_sub17.g2();
		int i_100_ = (class296_sub17.g1() & 0xff) + 1;
		for (int i_101_ = 0; i_101_ < i; i_101_++)
			class186s[i_101_].anInt1903 = class296_sub17.g2();
		for (int i_102_ = 0; i_102_ < i; i_102_++)
			class186s[i_102_].anInt1906 = class296_sub17.g2();
		for (int i_103_ = 0; i_103_ < i; i_103_++)
			class186s[i_103_].anInt1899 = class296_sub17.g2();
		for (int i_104_ = 0; i_104_ < i; i_104_++)
			class186s[i_104_].anInt1904 = class296_sub17.g2();
		for (int i_105_ = 0; i_105_ < i; i_105_++) {
			Class186 class186 = class186s[i_105_];
			class186.anInt1907 = i_98_ - class186.anInt1899 - class186.anInt1903;
			class186.anInt1902 = i_99_ - class186.anInt1904 - class186.anInt1906;
		}
		class296_sub17.pos = is.length - 7 - i * 8 - (i_100_ - 1) * 3;
		int[] is_106_ = new int[i_100_];
		for (int i_107_ = 1; i_107_ < i_100_; i_107_++) {
			is_106_[i_107_] = class296_sub17.readUnsignedMedInt();
			if (is_106_[i_107_] == 0)
				is_106_[i_107_] = 1;
		}
		for (int i_108_ = 0; i_108_ < i; i_108_++)
			class186s[i_108_].anIntArray1900 = is_106_;
		class296_sub17.pos = 0;
		for (int i_109_ = 0; i_109_ < i; i_109_++) {
			Class186 class186 = class186s[i_109_];
			int i_110_ = class186.anInt1899 * class186.anInt1904;
			class186.aByteArray1901 = new byte[i_110_];
			int i_111_ = class296_sub17.g1();
			if ((i_111_ & 0x2) == 0) {
				if ((i_111_ & 0x1) == 0) {
					for (int i_112_ = 0; i_112_ < i_110_; i_112_++)
						class186.aByteArray1901[i_112_] = class296_sub17.g1b();
				} else {
					for (int i_113_ = 0; i_113_ < class186.anInt1899; i_113_++) {
						for (int i_114_ = 0; i_114_ < class186.anInt1904; i_114_++)
							class186.aByteArray1901[i_113_ + i_114_ * class186.anInt1899] = class296_sub17.g1b();
					}
				}
			} else {
				boolean bool = false;
				class186.aByteArray1905 = new byte[i_110_];
				if ((i_111_ & 0x1) == 0) {
					for (int i_115_ = 0; i_115_ < i_110_; i_115_++)
						class186.aByteArray1901[i_115_] = class296_sub17.g1b();
					for (int i_116_ = 0; i_116_ < i_110_; i_116_++) {
						byte i_117_ = (class186.aByteArray1905[i_116_] = class296_sub17.g1b());
						bool = bool | i_117_ != -1;
					}
				} else {
					for (int i_118_ = 0; i_118_ < class186.anInt1899; i_118_++) {
						for (int i_119_ = 0; i_119_ < class186.anInt1904; i_119_++)
							class186.aByteArray1901[i_118_ + i_119_ * class186.anInt1899] = class296_sub17.g1b();
					}
					for (int i_120_ = 0; i_120_ < class186.anInt1899; i_120_++) {
						for (int i_121_ = 0; i_121_ < class186.anInt1904; i_121_++) {
							byte i_122_ = (class186.aByteArray1905[i_120_
									+ i_121_ * class186.anInt1899] = class296_sub17.g1b());
							bool = bool | i_122_ != -1;
						}
					}
				}
				if (!bool)
					class186.aByteArray1905 = null;
			}
		}
		return class186s;
	}

	final int method1882() {
		return anInt1904 + anInt1906 + anInt1902;
	}
}
