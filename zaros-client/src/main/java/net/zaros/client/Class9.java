package net.zaros.client;
/* Class9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class9 {
	static final void method194(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_) {
		int i_7_ = i_2_ - 334;
		if (i_7_ >= 0) {
			if (i_7_ > 100)
				i_7_ = 100;
		} else
			i_7_ = 0;
		int i_8_ = Class105.aShort3661 + (Class366_Sub7.aShort5396 - Class105.aShort3661) * i_7_ / i_0_;
		ModeWhat.anInt1192 = ModeWhat.anInt1193 * i_8_ >> 8;
		i_4_ = i_4_ * i_8_ >> 8;
		int i_9_ = -i + 16384 & 0x3fff;
		int i_10_ = -i_6_ + 16384 & 0x3fff;
		int i_11_ = 0;
		int i_12_ = 0;
		int i_13_ = i_4_;
		if (i_9_ != 0) {
			i_12_ = -i_13_ * Class296_Sub4.anIntArray4598[i_9_] >> 14;
			i_13_ = Class296_Sub4.anIntArray4618[i_9_] * i_13_ >> 14;
		}
		if (i_10_ != 0) {
			i_11_ = i_13_ * Class296_Sub4.anIntArray4598[i_10_] >> 14;
			i_13_ = i_13_ * Class296_Sub4.anIntArray4618[i_10_] >> 14;
		}
		Class296_Sub17_Sub2.camRotX = i;
		Class44_Sub1.camRotY = i_6_;
		TranslatableString.camPosY = -i_12_ + i_3_;
		Class124.camPosZ = i_5_ - i_13_;
		Class268.anInt2488 = 0;
		Class219.camPosX = i_1_ - i_11_;
	}
}
