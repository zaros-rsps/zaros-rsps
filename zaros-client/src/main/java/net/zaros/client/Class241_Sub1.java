package net.zaros.client;

/* Class241_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class241_Sub1 extends Class241 {
	static OutgoingPacket aClass311_4574 = new OutgoingPacket(58, 9);
	static OutgoingPacket aClass311_4575 = new OutgoingPacket(29, 3);
	static OutgoingPacket aClass311_4576 = new OutgoingPacket(77, 2);
	static Class400 aClass400_4577 = new Class400(8);
	static int anInt4578;

	abstract Interface6_Impl3 method2152(int i);

	public static void method2153(int i) {
		aClass311_4576 = null;
		if (i != 4475) {
			aClass311_4575 = null;
		}
		aClass400_4577 = null;
		aClass311_4575 = null;
		aClass311_4574 = null;
	}

	static final void method2154(int i, int i_0_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[0][i][i_0_];
		for (int i_1_ = 0; i_1_ < 3; i_1_++) {
			Class247 class247_2_ = Class338_Sub2.aClass247ArrayArrayArray5195[i_1_][i][i_0_] = Class338_Sub2.aClass247ArrayArrayArray5195[i_1_ + 1][i][i_0_];
			if (class247_2_ != null) {
				for (Class234 class234 = class247_2_.aClass234_2341; class234 != null; class234 = class234.aClass234_2224) {
					Class338_Sub3_Sub1 class338_sub3_sub1 = class234.aClass338_Sub3_Sub1_2226;
					if (class338_sub3_sub1.aShort6560 == i && class338_sub3_sub1.aShort6564 == i_0_) {
						class338_sub3_sub1.z--;
					}
				}
				if (class247_2_.aClass338_Sub3_Sub5_2346 != null) {
					class247_2_.aClass338_Sub3_Sub5_2346.z--;
				}
				if (class247_2_.aClass338_Sub3_Sub4_2348 != null) {
					class247_2_.aClass338_Sub3_Sub4_2348.z--;
				}
				if (class247_2_.aClass338_Sub3_Sub4_2343 != null) {
					class247_2_.aClass338_Sub3_Sub4_2343.z--;
				}
				if (class247_2_.aClass338_Sub3_Sub3_2342 != null) {
					class247_2_.aClass338_Sub3_Sub3_2342.z--;
				}
				if (class247_2_.aClass338_Sub3_Sub3_2337 != null) {
					class247_2_.aClass338_Sub3_Sub3_2337.z--;
				}
			}
		}
		if (Class338_Sub2.aClass247ArrayArrayArray5195[0][i][i_0_] == null) {
			Class338_Sub2.aClass247ArrayArrayArray5195[0][i][i_0_] = new Class247(0);
			Class338_Sub2.aClass247ArrayArrayArray5195[0][i][i_0_].aByte2349 = (byte) 1;
		}
		Class338_Sub2.aClass247ArrayArrayArray5195[0][i][i_0_].aClass247_2338 = class247;
		Class338_Sub2.aClass247ArrayArrayArray5195[3][i][i_0_] = null;
	}
}
