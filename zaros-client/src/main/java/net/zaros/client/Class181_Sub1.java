package net.zaros.client;

/* Class181_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class181_Sub1 extends Class181 {
	static Class209 aClass209_4513 = new Class209(60);
	int anInt4514 = 99;
	static float[] aFloatArray4515 = new float[2];

	final void method1831(boolean bool, ha var_ha, int i) {
		Class368_Sub11.method3843();
		if (!bool) {
			if (anInt1870 > 1) {
				for (int i_0_ = 0; anInt1859 > i_0_; i_0_++) {
					for (int i_1_ = 0; i_1_ < anInt1868; i_1_++) {
						if ((Class41_Sub18.aByteArrayArrayArray3786[1][i_0_][i_1_] & 0x2) == 2) {
							Class241_Sub1.method2154(i_0_, i_1_);
						}
					}
				}
			}
			for (int i_2_ = 0; i_2_ < anInt1870; i_2_++) {
				for (int i_3_ = 0; anInt1868 >= i_3_; i_3_++) {
					for (int i_4_ = 0; i_4_ <= anInt1859; i_4_++) {
						if ((aByteArrayArrayArray1871[i_2_][i_4_][i_3_] & 0x4) != 0) {
							int i_5_ = i_4_;
							int i_6_ = i_4_;
							int i_7_ = i_3_;
							int i_8_ = i_3_;
							for (/**/; i_7_ > 0 && (aByteArrayArrayArray1871[i_2_][i_4_][i_7_ - 1] & 0x4) != 0; i_7_--) {
								if (-i_7_ + i_8_ >= 10) {
									break;
								}
							}
							for (/**/; i_8_ < anInt1868 && (aByteArrayArrayArray1871[i_2_][i_4_][i_8_ + 1] & 0x4) != 0; i_8_++) {
								if (i_8_ - i_7_ >= 10) {
									break;
								}
							}
							while_152_: for (/**/; i_5_ > 0; i_5_--) {
								if (i_6_ - i_5_ >= 10) {
									break;
								}
								for (int i_9_ = i_7_; i_9_ <= i_8_; i_9_++) {
									if ((aByteArrayArrayArray1871[i_2_][i_5_ - 1][i_9_] & 0x4) == 0) {
										break while_152_;
									}
								}
							}
							while_153_: for (/**/; anInt1859 > i_6_; i_6_++) {
								if (i_6_ - i_5_ >= 10) {
									break;
								}
								for (int i_10_ = i_7_; i_8_ >= i_10_; i_10_++) {
									if ((aByteArrayArrayArray1871[i_2_][i_6_ + 1][i_10_] & 0x4) == 0) {
										break while_153_;
									}
								}
							}
							if ((-i_7_ + i_8_ + 1) * (i_6_ - i_5_ + 1) >= 4) {
								int i_11_ = anIntArrayArrayArray1866[i_2_][i_5_][i_7_];
								Class360_Sub9.method3750(i_11_, 4, i_2_, i_11_, (i_8_ << 9) + 512, i_7_ << 9, (i_6_ << 9) + 512, i ^ ~0x2738, i_5_ << 9);
								for (int i_12_ = i_5_; i_12_ <= i_6_; i_12_++) {
									for (int i_13_ = i_7_; i_13_ <= i_8_; i_13_++) {
										aByteArrayArrayArray1871[i_2_][i_12_][i_13_] = (byte) (aByteArrayArrayArray1871[i_2_][i_12_][i_13_] & -5);
									}
								}
							}
						}
					}
				}
			}
			Class296_Sub49.method3048((byte) -118);
		}
		if (i != -1) {
			aFloatArray4515 = null;
		}
		aByteArrayArrayArray1871 = null;
	}

	final void method1832(boolean bool, Packet class296_sub17, ha var_ha, int i, int i_14_, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_) {
		if (!aBoolean1860) {
			boolean bool_20_ = false;
			Class190 class190 = null;
			int i_21_ = (i_18_ & 0x7) * 8;
			int i_22_ = (i_19_ & 0x7) * 8;
			while (class296_sub17.pos < class296_sub17.data.length) {
				int i_23_ = class296_sub17.g1();
				if (i_23_ == 0) {
					if (class190 != null) {
						class190.method1920(class296_sub17, (byte) -63);
					} else {
						class190 = new Class190(class296_sub17);
					}
				} else if (i_23_ == 1) {
					int i_24_ = class296_sub17.g1();
					if (i_24_ > 0) {
						for (int i_25_ = 0; i_25_ < i_24_; i_25_++) {
							Class93 class93 = new Class93(var_ha, class296_sub17, 2);
							if (class93.anInt1001 == 31) {
								Class153 class153 = Class386.aClass335_3268.method3427((byte) 89, class296_sub17.g2());
								class93.method858(class153.anInt1573, (byte) 82, class153.anInt1575, class153.anInt1574, class153.anInt1571);
							}
							if (var_ha.q() > 0) {
								Class296_Sub35 class296_sub35 = class93.aClass296_Sub35_999;
								int i_26_ = class296_sub35.method2749(bool) >> 9;
								int i_27_ = class296_sub35.method2750(-4444) >> 9;
								if (class93.anInt1006 == i && i_26_ >= i_21_ && i_26_ < i_21_ + 8 && i_22_ <= i_27_ && i_27_ < i_22_ + 8) {
									int i_28_ = (i_17_ << 9) + Class16.method234(i_14_, 1, class296_sub35.method2750(-4444) & 0xfff, class296_sub35.method2749(true) & 0xfff);
									int i_29_ = Class189_Sub1.method1905(i_14_, class296_sub35.method2750(-4444) & 0xfff, 16, class296_sub35.method2749(true) & 0xfff) + (i_15_ << 9);
									i_26_ = i_28_ >> 9;
									i_27_ = i_29_ >> 9;
									if (i_26_ >= 0 && i_27_ >= 0 && i_26_ < anInt1859 && i_27_ < anInt1868) {
										class296_sub35.method2747(i_28_, anIntArrayArrayArray1866[i][i_26_][i_27_] - class296_sub35.method2751(-28925), i_29_, 16184);
										Class296_Sub39_Sub7.method2818(class93);
									}
								}
							}
						}
					}
				} else if (i_23_ != 2) {
					if (i_23_ != 128) {
						if (i_23_ != 129) {
							throw new IllegalStateException("");
						}
						if (aByteArrayArrayArray1864 == null) {
							aByteArrayArrayArray1864 = new byte[4][][];
						}
						for (int i_30_ = 0; i_30_ < 4; i_30_++) {
							byte i_31_ = class296_sub17.g1b();
							if (i_31_ == 0 && aByteArrayArrayArray1864[i_16_] != null) {
								if (i >= i_30_) {
									int i_32_ = i_17_;
									int i_33_ = i_17_ + 7;
									int i_34_ = i_15_;
									if (i_33_ < 0) {
										i_33_ = 0;
									} else if (i_33_ >= anInt1859) {
										i_33_ = anInt1859;
									}
									if (i_34_ < 0) {
										i_34_ = 0;
									} else if (anInt1868 <= i_34_) {
										i_34_ = anInt1868;
									}
									int i_35_ = i_15_ + 7;
									if (i_32_ >= 0) {
										if (anInt1859 <= i_32_) {
											i_32_ = anInt1859;
										}
									} else {
										i_32_ = 0;
									}
									if (i_35_ >= 0) {
										if (anInt1868 <= i_35_) {
											i_35_ = anInt1868;
										}
									} else {
										i_35_ = 0;
									}
									for (/**/; i_32_ < i_33_; i_32_++) {
										for (/**/; i_35_ > i_34_; i_34_++) {
											aByteArrayArrayArray1864[i_16_][i_32_][i_34_] = (byte) 0;
										}
									}
								}
							} else if (i_31_ == 1) {
								if (aByteArrayArrayArray1864[i_16_] == null) {
									aByteArrayArrayArray1864[i_16_] = new byte[anInt1859 + 1][anInt1868 + 1];
								}
								for (int i_36_ = 0; i_36_ < 64; i_36_ += 4) {
									for (int i_37_ = 0; i_37_ < 64; i_37_ += 4) {
										byte i_38_ = class296_sub17.g1b();
										if (i_30_ <= i) {
											for (int i_39_ = i_36_; i_39_ < i_36_ + 4; i_39_++) {
												for (int i_40_ = i_37_; i_37_ + 4 > i_40_; i_40_++) {
													if (i_39_ >= i_21_ && i_21_ + 8 > i_39_ && i_40_ >= i_22_ && i_22_ + 8 > i_40_) {
														int i_41_ = i_17_ + ClipData.method264(i_39_ & 0x7, i_40_ & 0x7, i_14_, (byte) -110);
														int i_42_ = Class67.method713(7, i_14_, i_39_ & 0x7, i_40_ & 0x7) + i_15_;
														if (i_41_ >= 0 && anInt1859 > i_41_ && i_42_ >= 0 && i_42_ < anInt1868) {
															bool_20_ = true;
															aByteArrayArrayArray1864[i_16_][i_41_][i_42_] = i_38_;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					} else {
						if (class190 == null) {
							class190 = new Class190();
						}
						class190.method1917(8, class296_sub17);
					}
				} else {
					if (class190 == null) {
						class190 = new Class190();
					}
					class190.method1919(-1, class296_sub17);
				}
			}
			if (bool == true) {
				if (class190 != null) {
					Class355_Sub2.method3705(i_17_ >> 3, class190, (byte) -70, i_15_ >> 3);
				}
				if (!bool_20_ && aByteArrayArrayArray1864 != null && aByteArrayArrayArray1864[i_16_] != null) {
					int i_43_ = i_17_ + 7;
					int i_44_ = i_15_ + 7;
					for (int i_45_ = i_17_; i_45_ < i_43_; i_45_++) {
						for (int i_46_ = i_15_; i_44_ > i_46_; i_46_++) {
							aByteArrayArrayArray1864[i_16_][i_45_][i_46_] = (byte) 0;
						}
					}
				}
			}
		}
	}

	final void method1833(int i, int i_47_, byte[] is, ha var_ha, int i_48_, int i_49_, byte i_50_, int i_51_, int i_52_, int i_53_, ClipData[] class17s) {
		if (i_50_ != -9) {
			method1832(false, null, null, -122, 78, -59, -47, 86, -115, 1);
		}
		Packet class296_sub17 = new Packet(is);
		int i_54_ = -1;
		for (;;) {
			int i_55_ = class296_sub17.method2595();
			if (i_55_ == 0) {
				break;
			}
			i_54_ += i_55_;
			int i_56_ = 0;
			for (;;) {
				int i_57_ = class296_sub17.readSmart();
				if (i_57_ == 0) {
					break;
				}
				i_56_ += i_57_ - 1;
				int i_58_ = i_56_ & 0x3f;
				int i_59_ = (i_56_ & 0xfd4) >> 6;
				int i_60_ = i_56_ >> 12;
				int i_61_ = class296_sub17.g1();
				int i_62_ = i_61_ >> 2;
				int i_63_ = i_61_ & 0x3;
				if (i_48_ == i_60_ && i_53_ <= i_59_ && i_59_ < i_53_ + 8 && i_58_ >= i_49_ && i_49_ + 8 > i_58_) {
					ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(i_54_);
					int i_64_ = i_47_ + Class296_Sub9_Sub1.method2491(i_58_ & 0x7, i_63_, 1, class70.sizeX, i_51_, class70.sizeY, i_59_ & 0x7);
					int i_65_ = Class55.a(class70.sizeX, i_58_ & 0x7, i_51_, i_59_ & 0x7, i_63_, -80, class70.sizeY) + i;
					if (i_64_ > 0 && i_65_ > 0 && i_64_ < anInt1859 - 1 && i_65_ < anInt1868 - 1) {
						ClipData class17 = null;
						if (!aBoolean1860) {
							int i_66_ = i_52_;
							if ((Class41_Sub18.aByteArrayArrayArray3786[1][i_64_][i_65_] & 0x2) == 2) {
								i_66_--;
							}
							if (i_66_ >= 0) {
								class17 = class17s[i_66_];
							}
						}
						method1838(i_54_, var_ha, i_62_, class17, i_64_, i_52_, i_63_ + i_51_ & 0x3, i_65_, i_52_, -1, -127);
					}
				}
			}
		}
	}

	static final InterfaceComponent method1834(InterfaceComponent class51, int i) {
		if (class51.parentId != -1) {
			return InterfaceComponent.getInterfaceComponent(class51.parentId);
		}
		if (i != -2) {
			return null;
		}
		int i_67_ = class51.uid >>> 16;
		Class244 class244 = new Class244(Class386.aClass263_3264);
		for (Class296_Sub13 class296_sub13 = (Class296_Sub13) class244.method2177(i - 60); class296_sub13 != null; class296_sub13 = (Class296_Sub13) class244.method2175(i + 121)) {
			if (class296_sub13.anInt4657 == i_67_) {
				return InterfaceComponent.getInterfaceComponent((int) class296_sub13.uid);
			}
		}
		return null;
	}

	final void method1835(ha var_ha, int i, int i_68_, int i_69_, int i_70_, ClipData class17, boolean bool) {
		Interface14 interface14 = method1837(i_69_, i_70_, -1, i, i_68_);
		if (interface14 != null) {
			ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(interface14.method57((byte) 93));
			int i_71_ = interface14.method54(-11077);
			int i_72_ = interface14.method59(107);
			if (class70.method766((byte) -94)) {
				Class296_Sub51_Sub1.method3079(class70, i, (byte) 13, i_68_, i_69_);
			}
			if (interface14.method55((byte) -57)) {
				interface14.method60((byte) -75, var_ha);
			}
			if (i_70_ != 0) {
				if (i_70_ == 1) {
					Class42_Sub3.method537(i_69_, i_68_, i);
				} else if (i_70_ == 2) {
					Class183.method1856(i_69_, i_68_, i, Interface14.class);
					if (class70.blockingType != 0 && class70.sizeX + i_68_ < anInt1859 && i + class70.sizeX < anInt1868 && i_68_ + class70.sizeY < anInt1859 && i + class70.sizeY < anInt1868) {
						class17.unflagObject(i_68_, class70.sizeY, i_72_, !class70.notClipped, class70.sizeX, i, class70.projectileClipped);
					}
					if (i_71_ == 9) {
						if ((i_72_ & 0x1) != 0) {
							za_Sub2.method3228(16, i_69_, false, i, i_68_);
						} else {
							za_Sub2.method3228(8, i_69_, false, i, i_68_);
						}
					}
				} else if (i_70_ == 3) {
					Class338_Sub3.method3464(i_69_, i_68_, i);
					if (class70.blockingType == 1) {
						class17.unflagDecoration(i_68_, i);
					}
				}
			} else {
				Class16_Sub3_Sub1.method253(i_69_, i_68_, i);
				if (class70.blockingType != 0) {
					class17.unflagWall(i_68_, i, i_71_, i_72_, class70.projectileClipped, !class70.notClipped);
				}
				if (class70.anInt832 == 1) {
					if (i_72_ == 0) {
						za_Sub2.method3228(1, i_69_, false, i, i_68_);
					} else if (i_72_ == 1) {
						za_Sub2.method3228(2, i_69_, false, i + 1, i_68_);
					} else if (i_72_ != 2) {
						if (i_72_ == 3) {
							za_Sub2.method3228(2, i_69_, false, i, i_68_);
						}
					} else {
						za_Sub2.method3228(1, i_69_, false, i, i_68_ + 1);
					}
				}
			}
		}
		if (bool) {
			aFloatArray4515 = null;
		}
	}

	public static void method1836(int i) {
		int i_73_ = 59 / ((22 - i) / 45);
		aClass209_4513 = null;
		aFloatArray4515 = null;
	}

	final Interface14 method1837(int i, int i_74_, int i_75_, int i_76_, int i_77_) {
		Interface14 interface14 = null;
		if (i_74_ == 0) {
			interface14 = (Interface14) StaticMethods.method2730(i, i_77_, i_76_);
		}
		if (i_74_ == 1) {
			interface14 = (Interface14) Class172.method1679(i, i_77_, i_76_);
		}
		if (i_74_ == 2) {
			interface14 = (Interface14) Class123_Sub2.method1070(i, i_77_, i_76_, Interface14.class);
		}
		if (i_74_ == 3) {
			interface14 = (Interface14) Class296_Sub15_Sub3.method2540(i, i_77_, i_76_);
		}
		if (i_75_ != -1) {
			return null;
		}
		return interface14;
	}

	final void method1838(int i, ha var_ha, int type, ClipData clip, int i_79_, int i_80_, int i_81_, int i_82_, int i_83_, int i_84_, int i_85_) {
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026.method496(125) != 0 || Class296_Sub40.method2912(23266, i_82_, NPCDefinitionLoader.anInt1401, i_79_, i_80_)) {
			if (anInt4514 > i_83_) {
				anInt4514 = i_83_;
			}
			ObjectDefinition definition = Class379.objectDefinitionLoader.getObjectDefinition(i);
			if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991.method497(119) != 0 || !definition.aBoolean777) {
				int i_86_;
				int i_87_;
				if (i_81_ == 1 || i_81_ == 3) {
					i_87_ = definition.sizeX;
					i_86_ = definition.sizeY;
				} else {
					i_86_ = definition.sizeX;
					i_87_ = definition.sizeY;
				}
				int i_88_;
				int i_89_;
				if (anInt1859 >= i_79_ + i_86_) {
					i_89_ = i_79_ + (i_86_ + 1 >> 1);
					i_88_ = i_79_ + (i_86_ >> 1);
				} else {
					i_88_ = i_79_;
					i_89_ = i_79_ + 1;
				}
				int i_90_;
				int i_91_;
				if (anInt1868 >= i_87_ + i_82_) {
					i_91_ = i_82_ + (i_87_ + 1 >> 1);
					i_90_ = (i_87_ >> 1) + i_82_;
				} else {
					i_90_ = i_82_;
					i_91_ = i_82_ + 1;
				}
				s var_s = Class360_Sub2.aSArray5304[i_80_];
				int i_92_ = var_s.method3355(i_90_, (byte) -114, i_88_) + var_s.method3355(i_90_, (byte) -126, i_89_) + var_s.method3355(i_91_, (byte) -107, i_88_) + var_s.method3355(i_91_, (byte) -111, i_89_) >> 2;
				int i_93_ = (i_86_ << 8) + (i_79_ << 9);
				if (i_85_ < -58) {
					int i_94_ = (i_87_ << 8) + (i_82_ << 9);
					boolean bool = Class318.aBoolean2809 && !aBoolean1860 && definition.aBoolean775;
					if (definition.method766((byte) -94)) {
						Class368_Sub20.method3862(definition, i_83_, null, false, i_81_, null, i_79_, i_82_);
					}
					boolean bool_95_ = i_84_ == -1 && !definition.method752(-120) && definition.transforms == null && !definition.aBoolean803 && !definition.aBoolean835;
					if (!Class296_Sub1.aBoolean4584 || (!Class362.method3757(true, type) || definition.anInt832 == 1) && (!ItemsNode.method2441(-26121, type) || definition.anInt832 != 0)) {
						if (type == 22) {
							if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997.method484(123) != 0 || definition.interactonType != 0 || definition.blockingType == 1 || definition.aBoolean789) {
								Class338_Sub3_Sub5 class338_sub3_sub5;
								if (bool_95_) {
									Class338_Sub3_Sub5_Sub2 class338_sub3_sub5_sub2 = new Class338_Sub3_Sub5_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_81_, bool);
									class338_sub3_sub5 = class338_sub3_sub5_sub2;
									if (class338_sub3_sub5_sub2.method55((byte) -57)) {
										class338_sub3_sub5_sub2.method56(var_ha, (byte) -117);
									}
								} else {
									class338_sub3_sub5 = new Class338_Sub3_Sub5_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_81_, i_84_);
								}
								Class166.method1643(i_83_, i_79_, i_82_, class338_sub3_sub5);
								if (definition.blockingType == 1 && clip != null) {
									clip.flagDecoration(i_79_, i_82_);
								}
							}
						} else if (type == 10 || type == 11) {
							Class338_Sub3_Sub1_Sub1 class338_sub3_sub1_sub1 = null;
							int i_96_;
							Class338_Sub3_Sub1 class338_sub3_sub1;
							if (bool_95_) {
								Class338_Sub3_Sub1_Sub1 class338_sub3_sub1_sub1_97_ = new Class338_Sub3_Sub1_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_79_, i_86_ + i_79_ - 1, i_82_, i_82_ + i_87_ - 1, type, i_81_, bool);
								class338_sub3_sub1 = class338_sub3_sub1_sub1_97_;
								i_96_ = class338_sub3_sub1_sub1_97_.method3482(15);
								class338_sub3_sub1_sub1 = class338_sub3_sub1_sub1_97_;
							} else {
								i_96_ = 15;
								class338_sub3_sub1 = new Class338_Sub3_Sub1_Sub4(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_79_, i_79_ + i_86_ - 1, i_82_, i_87_ - 1 + i_82_, type, i_81_, i_84_);
							}
							if (Class125.method1079(class338_sub3_sub1, false)) {
								if (class338_sub3_sub1_sub1 != null && class338_sub3_sub1_sub1.method55((byte) -57)) {
									class338_sub3_sub1_sub1.method56(var_ha, (byte) -117);
								}
								if (definition.aBoolean769 && Class318.aBoolean2809) {
									if (i_96_ > 30) {
										i_96_ = 30;
									}
									for (int i_98_ = 0; i_86_ >= i_98_; i_98_++) {
										for (int i_99_ = 0; i_99_ <= i_87_; i_99_++) {
											var_s.ka(i_79_ + i_98_, i_82_ + i_99_, i_96_);
										}
									}
								}
							}
							if (definition.blockingType != 0 && clip != null) {
								clip.flagObject(i_79_, i_82_, i_86_, i_87_, definition.projectileClipped, !definition.notClipped);
							}
						} else if (type >= 12 && type <= 17 || type >= 18 && type <= 21) {
							Class338_Sub3_Sub1 class338_sub3_sub1;
							if (bool_95_) {
								Class338_Sub3_Sub1_Sub1 class338_sub3_sub1_sub1 = new Class338_Sub3_Sub1_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_79_, i_79_ + i_86_ - 1, i_82_, i_82_ - (-i_87_ + 1), type, i_81_, bool);
								class338_sub3_sub1 = class338_sub3_sub1_sub1;
								if (class338_sub3_sub1_sub1.method55((byte) -57)) {
									class338_sub3_sub1_sub1.method56(var_ha, (byte) -117);
								}
							} else {
								class338_sub3_sub1 = new Class338_Sub3_Sub1_Sub4(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_79_, i_79_ + i_86_ - 1, i_82_, i_82_ + i_87_ - 1, type, i_81_, i_84_);
							}
							Class125.method1079(class338_sub3_sub1, false);
							if (Class318.aBoolean2809 && !aBoolean1860 && type >= 12 && type <= 17 && type != 13 && i_83_ > 0 && definition.anInt832 != 0) {
								aByteArrayArrayArray1871[i_83_][i_79_][i_82_] = (byte) Class48.bitOR(aByteArrayArrayArray1871[i_83_][i_79_][i_82_], 4);
							}
							if (definition.blockingType != 0 && clip != null) {
								clip.flagObject(i_79_, i_82_, i_86_, i_87_, definition.projectileClipped, !definition.notClipped);
							}
						} else if (type == 0) {
							int i_100_ = definition.anInt832;
							if (Class341.aBoolean2977 && definition.anInt832 == -1) {
								i_100_ = 1;
							}
							Class338_Sub3_Sub4 class338_sub3_sub4;
							if (bool_95_) {
								Class338_Sub3_Sub4_Sub2 class338_sub3_sub4_sub2 = new Class338_Sub3_Sub4_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, type, i_81_, bool);
								if (class338_sub3_sub4_sub2.method55((byte) -57)) {
									class338_sub3_sub4_sub2.method56(var_ha, (byte) -117);
								}
								class338_sub3_sub4 = class338_sub3_sub4_sub2;
							} else {
								class338_sub3_sub4 = new Class338_Sub3_Sub4_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, type, i_81_, i_84_);
							}
							Class239.method2144(i_83_, i_79_, i_82_, class338_sub3_sub4, null);
							if (i_81_ == 0) {
								if (Class318.aBoolean2809 && definition.aBoolean769) {
									var_s.ka(i_79_, i_82_, 50);
									var_s.ka(i_79_, i_82_ + 1, 50);
								}
								if (i_100_ == 1 && !aBoolean1860) {
									Class298.method3234(definition.anInt823, true, i_83_, i_82_, i_79_, definition.anInt773, 1);
								}
							} else if (i_81_ == 1) {
								if (Class318.aBoolean2809 && definition.aBoolean769) {
									var_s.ka(i_79_, i_82_ + 1, 50);
									var_s.ka(i_79_ + 1, i_82_ + 1, 50);
								}
								if (i_100_ == 1 && !aBoolean1860) {
									Class298.method3234(definition.anInt823, true, i_83_, i_82_ + 1, i_79_, -definition.anInt773, 2);
								}
							} else if (i_81_ != 2) {
								if (i_81_ == 3) {
									if (Class318.aBoolean2809 && definition.aBoolean769) {
										var_s.ka(i_79_, i_82_, 50);
										var_s.ka(i_79_ + 1, i_82_, 50);
									}
									if (i_100_ == 1 && !aBoolean1860) {
										Class298.method3234(definition.anInt823, true, i_83_, i_82_, i_79_, definition.anInt773, 2);
									}
								}
							} else {
								if (Class318.aBoolean2809 && definition.aBoolean769) {
									var_s.ka(i_79_ + 1, i_82_, 50);
									var_s.ka(i_79_ + 1, i_82_ + 1, 50);
								}
								if (i_100_ == 1 && !aBoolean1860) {
									Class298.method3234(definition.anInt823, true, i_83_, i_82_, i_79_ + 1, -definition.anInt773, 1);
								}
							}
							if (definition.blockingType != 0 && clip != null) {
								clip.flagWall(i_79_, i_82_, type, i_81_, !definition.notClipped, definition.projectileClipped);
							}
							if (definition.anInt822 != 64) {
								Class134.method1405(i_83_, i_79_, i_82_, definition.anInt822);
							}
						} else if (type == 1) {
							Class338_Sub3_Sub4 class338_sub3_sub4;
							if (!bool_95_) {
								class338_sub3_sub4 = new Class338_Sub3_Sub4_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, type, i_81_, i_84_);
							} else {
								Class338_Sub3_Sub4_Sub2 class338_sub3_sub4_sub2 = new Class338_Sub3_Sub4_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, type, i_81_, bool);
								class338_sub3_sub4 = class338_sub3_sub4_sub2;
								if (class338_sub3_sub4_sub2.method55((byte) -57)) {
									class338_sub3_sub4_sub2.method56(var_ha, (byte) -117);
								}
							}
							Class239.method2144(i_83_, i_79_, i_82_, class338_sub3_sub4, null);
							if (definition.aBoolean769 && Class318.aBoolean2809) {
								if (i_81_ != 0) {
									if (i_81_ == 1) {
										var_s.ka(i_79_ + 1, i_82_ + 1, 50);
									} else if (i_81_ == 2) {
										var_s.ka(i_79_ + 1, i_82_, 50);
									} else if (i_81_ == 3) {
										var_s.ka(i_79_, i_82_, 50);
									}
								} else {
									var_s.ka(i_79_, i_82_ + 1, 50);
								}
							}
							if (definition.blockingType != 0 && clip != null) {
								clip.flagWall(i_79_, i_82_, type, i_81_, !definition.notClipped, definition.projectileClipped);
							}
						} else if (type == 2) {
							int i_101_ = i_81_ + 1 & 0x3;
							Class338_Sub3_Sub4 class338_sub3_sub4;
							Class338_Sub3_Sub4 class338_sub3_sub4_102_;
							if (bool_95_) {
								Class338_Sub3_Sub4_Sub2 class338_sub3_sub4_sub2 = new Class338_Sub3_Sub4_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, type, i_81_ + 4, bool);
								Class338_Sub3_Sub4_Sub2 class338_sub3_sub4_sub2_103_ = new Class338_Sub3_Sub4_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, type, i_101_, bool);
								if (class338_sub3_sub4_sub2.method55((byte) -57)) {
									class338_sub3_sub4_sub2.method56(var_ha, (byte) -117);
								}
								class338_sub3_sub4_102_ = class338_sub3_sub4_sub2_103_;
								class338_sub3_sub4 = class338_sub3_sub4_sub2;
								if (class338_sub3_sub4_sub2_103_.method55((byte) -57)) {
									class338_sub3_sub4_sub2_103_.method56(var_ha, (byte) -117);
								}
							} else {
								class338_sub3_sub4 = new Class338_Sub3_Sub4_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, type, i_81_ + 4, i_84_);
								class338_sub3_sub4_102_ = new Class338_Sub3_Sub4_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, type, i_101_, i_84_);
							}
							Class239.method2144(i_83_, i_79_, i_82_, class338_sub3_sub4, class338_sub3_sub4_102_);
							if ((definition.anInt832 == 1 || Class341.aBoolean2977 && definition.anInt832 == -1) && !aBoolean1860) {
								if (i_81_ == 0) {
									Class298.method3234(definition.anInt823, true, i_83_, i_82_, i_79_, definition.anInt773, 1);
									Class298.method3234(definition.anInt823, true, i_83_, i_82_ + 1, i_79_, definition.anInt773, 2);
								} else if (i_81_ == 1) {
									Class298.method3234(definition.anInt823, true, i_83_, i_82_, i_79_ + 1, definition.anInt773, 1);
									Class298.method3234(definition.anInt823, true, i_83_, i_82_ + 1, i_79_, definition.anInt773, 2);
								} else if (i_81_ == 2) {
									Class298.method3234(definition.anInt823, true, i_83_, i_82_, i_79_ + 1, definition.anInt773, 1);
									Class298.method3234(definition.anInt823, true, i_83_, i_82_, i_79_, definition.anInt773, 2);
								} else if (i_81_ == 3) {
									Class298.method3234(definition.anInt823, true, i_83_, i_82_, i_79_, definition.anInt773, 1);
									Class298.method3234(definition.anInt823, true, i_83_, i_82_, i_79_, definition.anInt773, 2);
								}
							}
							if (definition.blockingType != 0 && clip != null) {
								clip.flagWall(i_79_, i_82_, type, i_81_, !definition.notClipped, definition.projectileClipped);
							}
							if (definition.anInt822 != 64) {
								Class134.method1405(i_83_, i_79_, i_82_, definition.anInt822);
							}
						} else if (type == 3) {
							Class338_Sub3_Sub4 class338_sub3_sub4;
							if (bool_95_) {
								Class338_Sub3_Sub4_Sub2 class338_sub3_sub4_sub2 = new Class338_Sub3_Sub4_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, type, i_81_, bool);
								class338_sub3_sub4 = class338_sub3_sub4_sub2;
								if (class338_sub3_sub4_sub2.method55((byte) -57)) {
									class338_sub3_sub4_sub2.method56(var_ha, (byte) -117);
								}
							} else {
								class338_sub3_sub4 = new Class338_Sub3_Sub4_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, type, i_81_, i_84_);
							}
							Class239.method2144(i_83_, i_79_, i_82_, class338_sub3_sub4, null);
							if (definition.aBoolean769 && Class318.aBoolean2809) {
								if (i_81_ == 0) {
									var_s.ka(i_79_, i_82_ + 1, 50);
								} else if (i_81_ != 1) {
									if (i_81_ != 2) {
										if (i_81_ == 3) {
											var_s.ka(i_79_, i_82_, 50);
										}
									} else {
										var_s.ka(i_79_ + 1, i_82_, 50);
									}
								} else {
									var_s.ka(i_79_ + 1, i_82_ + 1, 50);
								}
							}
							if (definition.blockingType != 0 && clip != null) {
								clip.flagWall(i_79_, i_82_, type, i_81_, !definition.notClipped, definition.projectileClipped);
							}
						} else if (type == 9) {
							Class338_Sub3_Sub1 class338_sub3_sub1;
							if (bool_95_) {
								Class338_Sub3_Sub1_Sub1 class338_sub3_sub1_sub1 = new Class338_Sub3_Sub1_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_79_, i_79_, i_82_, i_82_, type, i_81_, bool);
								class338_sub3_sub1 = class338_sub3_sub1_sub1;
								if (class338_sub3_sub1_sub1.method55((byte) -57)) {
									class338_sub3_sub1_sub1.method56(var_ha, (byte) -117);
								}
							} else {
								class338_sub3_sub1 = new Class338_Sub3_Sub1_Sub4(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_79_, i_86_ + i_79_ - 1, i_82_, i_87_ - 1 + i_82_, type, i_81_, i_84_);
							}
							Class125.method1079(class338_sub3_sub1, false);
							if (definition.anInt832 == 1 && !aBoolean1860) {
								int i_104_;
								if ((i_81_ & 0x1) != 0) {
									i_104_ = 16;
								} else {
									i_104_ = 8;
								}
								Class298.method3234(definition.anInt823, true, i_83_, i_82_, i_79_, 0, i_104_);
							}
							if (definition.blockingType != 0 && clip != null) {
								clip.flagObject(i_79_, i_82_, i_86_, i_87_, definition.projectileClipped, !definition.notClipped);
							}
							if (definition.anInt822 != 64) {
								Class134.method1405(i_83_, i_79_, i_82_, definition.anInt822);
							}
						} else if (type == 4) {
							Class338_Sub3_Sub3 class338_sub3_sub3;
							if (bool_95_) {
								Class338_Sub3_Sub3_Sub1 class338_sub3_sub3_sub1 = new Class338_Sub3_Sub3_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, 0, 0, type, i_81_);
								if (class338_sub3_sub3_sub1.method55((byte) -57)) {
									class338_sub3_sub3_sub1.method56(var_ha, (byte) -117);
								}
								class338_sub3_sub3 = class338_sub3_sub3_sub1;
							} else {
								class338_sub3_sub3 = new Class338_Sub3_Sub3_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, 0, 0, type, i_81_, i_84_);
							}
							ha.method1085(i_83_, i_79_, i_82_, class338_sub3_sub3, null);
						} else if (type == 5) {
							int i_105_ = 65;
							Interface14 interface14 = (Interface14) StaticMethods.method2730(i_83_, i_79_, i_82_);
							if (interface14 != null) {
								i_105_ = Class379.objectDefinitionLoader.getObjectDefinition(interface14.method57((byte) 117)).anInt822 + 1;
							}
							Class338_Sub3_Sub3 class338_sub3_sub3;
							if (!bool_95_) {
								class338_sub3_sub3 = new Class338_Sub3_Sub3_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, Class77.anIntArray877[i_81_] * i_105_, Class296_Sub44.anIntArray4941[i_81_] * i_105_, type, i_81_, i_84_);
							} else {
								Class338_Sub3_Sub3_Sub1 class338_sub3_sub3_sub1 = new Class338_Sub3_Sub3_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_105_ * Class77.anIntArray877[i_81_], i_105_ * Class296_Sub44.anIntArray4941[i_81_], type, i_81_);
								if (class338_sub3_sub3_sub1.method55((byte) -57)) {
									class338_sub3_sub3_sub1.method56(var_ha, (byte) -117);
								}
								class338_sub3_sub3 = class338_sub3_sub3_sub1;
							}
							ha.method1085(i_83_, i_79_, i_82_, class338_sub3_sub3, null);
						} else if (type == 6) {
							int i_106_ = 33;
							Interface14 interface14 = (Interface14) StaticMethods.method2730(i_83_, i_79_, i_82_);
							if (interface14 != null) {
								i_106_ = Class379.objectDefinitionLoader.getObjectDefinition(interface14.method57((byte) 108)).anInt822 / 2 + 1;
							}
							Class338_Sub3_Sub3 class338_sub3_sub3;
							if (!bool_95_) {
								class338_sub3_sub3 = new Class338_Sub3_Sub3_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_106_ * Class306.anIntArray2742[i_81_], Class373_Sub1.anIntArray5580[i_81_] * i_106_, type, i_81_ + 4, i_84_);
							} else {
								Class338_Sub3_Sub3_Sub1 class338_sub3_sub3_sub1 = new Class338_Sub3_Sub3_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, Class77.anIntArray877[i_81_] * i_106_, i_106_ * Class296_Sub44.anIntArray4941[i_81_], type, i_81_ + 4);
								if (class338_sub3_sub3_sub1.method55((byte) -57)) {
									class338_sub3_sub3_sub1.method56(var_ha, (byte) -117);
								}
								class338_sub3_sub3 = class338_sub3_sub3_sub1;
							}
							ha.method1085(i_83_, i_79_, i_82_, class338_sub3_sub3, null);
						} else if (type == 7) {
							int i_107_ = i_81_ + 2 & 0x3;
							Class338_Sub3_Sub3 class338_sub3_sub3;
							if (bool_95_) {
								Class338_Sub3_Sub3_Sub1 class338_sub3_sub3_sub1 = new Class338_Sub3_Sub3_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, 0, 0, type, i_107_ + 4);
								if (class338_sub3_sub3_sub1.method55((byte) -57)) {
									class338_sub3_sub3_sub1.method56(var_ha, (byte) -117);
								}
								class338_sub3_sub3 = class338_sub3_sub3_sub1;
							} else {
								class338_sub3_sub3 = new Class338_Sub3_Sub3_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, 0, 0, type, i_107_ + 4, i_84_);
							}
							ha.method1085(i_83_, i_79_, i_82_, class338_sub3_sub3, null);
						} else if (type == 8) {
							int i_108_ = i_81_ + 2 & 0x3;
							int i_109_ = 33;
							Interface14 interface14 = (Interface14) StaticMethods.method2730(i_83_, i_79_, i_82_);
							if (interface14 != null) {
								i_109_ = Class379.objectDefinitionLoader.getObjectDefinition(interface14.method57((byte) 108)).anInt822 / 2 + 1;
							}
							Class338_Sub3_Sub3 class338_sub3_sub3;
							Class338_Sub3_Sub3 class338_sub3_sub3_110_;
							if (bool_95_) {
								Class338_Sub3_Sub3_Sub1 class338_sub3_sub3_sub1 = new Class338_Sub3_Sub3_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, i_109_ * Class306.anIntArray2742[i_81_], i_109_ * Class373_Sub1.anIntArray5580[i_81_], type, i_81_ + 4);
								Class338_Sub3_Sub3_Sub1 class338_sub3_sub3_sub1_111_ = new Class338_Sub3_Sub3_Sub1(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, 0, 0, type, i_108_ + 4);
								if (class338_sub3_sub3_sub1.method55((byte) -57)) {
									class338_sub3_sub3_sub1.method56(var_ha, (byte) -117);
								}
								class338_sub3_sub3_110_ = class338_sub3_sub3_sub1_111_;
								class338_sub3_sub3 = class338_sub3_sub3_sub1;
								if (class338_sub3_sub3_sub1_111_.method55((byte) -57)) {
									class338_sub3_sub3_sub1_111_.method56(var_ha, (byte) -117);
								}
							} else {
								Class338_Sub3_Sub3_Sub2 class338_sub3_sub3_sub2 = new Class338_Sub3_Sub3_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, Class306.anIntArray2742[i_81_] * i_109_, i_109_ * Class373_Sub1.anIntArray5580[i_81_], type, i_81_ + 4, i_84_);
								Class338_Sub3_Sub3_Sub2 class338_sub3_sub3_sub2_112_ = new Class338_Sub3_Sub3_Sub2(var_ha, definition, i_83_, i_80_, i_93_, i_92_, i_94_, aBoolean1860, 0, 0, type, i_108_ + 4, i_84_);
								class338_sub3_sub3 = class338_sub3_sub3_sub2;
								class338_sub3_sub3_110_ = class338_sub3_sub3_sub2_112_;
							}
							ha.method1085(i_83_, i_79_, i_82_, class338_sub3_sub3, class338_sub3_sub3_110_);
						}
					}
				}
			}
		}
	}

	final void method1839(Packet class296_sub17, ha var_ha, int i, int i_113_, int i_114_) {
		if (!aBoolean1860) {
			boolean bool = false;
			Class190 class190 = null;
			while (class296_sub17.pos < class296_sub17.data.length) {
				int i_115_ = class296_sub17.g1();
				if (i_115_ == 0) {
					if (class190 == null) {
						class190 = new Class190(class296_sub17);
					} else {
						class190.method1920(class296_sub17, (byte) -58);
					}
				} else if (i_115_ != 1) {
					if (i_115_ == 2) {
						if (class190 == null) {
							class190 = new Class190();
						}
						class190.method1919(-1, class296_sub17);
					} else if (i_115_ != 128) {
						if (i_115_ != 129) {
							throw new IllegalStateException("");
						}
						if (aByteArrayArrayArray1864 == null) {
							aByteArrayArrayArray1864 = new byte[4][][];
						}
						bool = true;
						for (int i_116_ = 0; i_116_ < 4; i_116_++) {
							byte i_117_ = class296_sub17.g1b();
							if (i_117_ == 0 && aByteArrayArrayArray1864[i_116_] != null) {
								int i_118_ = i;
								int i_119_ = i + 64;
								int i_120_ = i_113_;
								if (i_120_ >= 0) {
									if (i_120_ >= anInt1868) {
										i_120_ = anInt1868;
									}
								} else {
									i_120_ = 0;
								}
								if (i_118_ < 0) {
									i_118_ = 0;
								} else if (i_118_ >= anInt1859) {
									i_118_ = anInt1859;
								}
								int i_121_ = i_113_ + 64;
								if (i_119_ >= 0) {
									if (i_119_ >= anInt1859) {
										i_119_ = anInt1859;
									}
								} else {
									i_119_ = 0;
								}
								if (i_121_ >= 0) {
									if (i_121_ >= anInt1868) {
										i_121_ = anInt1868;
									}
								} else {
									i_121_ = 0;
								}
								for (/**/; i_119_ > i_118_; i_118_++) {
									for (/**/; i_120_ < i_121_; i_120_++) {
										aByteArrayArrayArray1864[i_116_][i_118_][i_120_] = (byte) 0;
									}
								}
							} else if (i_117_ == 1) {
								if (aByteArrayArrayArray1864[i_116_] == null) {
									aByteArrayArrayArray1864[i_116_] = new byte[anInt1859 + 1][anInt1868 + 1];
								}
								for (int i_122_ = 0; i_122_ < 64; i_122_ += 4) {
									for (int i_123_ = 0; i_123_ < 64; i_123_ += 4) {
										byte i_124_ = class296_sub17.g1b();
										for (int i_125_ = i_122_ + i; i_125_ < i + i_122_ + 4; i_125_++) {
											for (int i_126_ = i_113_ + i_123_; i_113_ + i_123_ + 4 > i_126_; i_126_++) {
												if (i_125_ >= 0 && anInt1859 > i_125_ && i_126_ >= 0 && anInt1868 > i_126_) {
													aByteArrayArrayArray1864[i_116_][i_125_][i_126_] = i_124_;
												}
											}
										}
									}
								}
							} else if (i_117_ == 2) {
								if (aByteArrayArrayArray1864[i_116_] == null) {
									aByteArrayArrayArray1864[i_116_] = new byte[anInt1859 + 1][anInt1868 + 1];
								}
								if (i_116_ > 0) {
									int i_127_ = i;
									int i_128_ = i + 64;
									int i_129_ = i_113_;
									if (i_127_ >= 0) {
										if (i_127_ >= anInt1859) {
											i_127_ = anInt1859;
										}
									} else {
										i_127_ = 0;
									}
									if (i_129_ >= 0) {
										if (anInt1868 <= i_129_) {
											i_129_ = anInt1868;
										}
									} else {
										i_129_ = 0;
									}
									if (i_128_ >= 0) {
										if (anInt1859 <= i_128_) {
											i_128_ = anInt1859;
										}
									} else {
										i_128_ = 0;
									}
									int i_130_ = i_113_ + 64;
									if (i_130_ >= 0) {
										if (anInt1868 <= i_130_) {
											i_130_ = anInt1868;
										}
									} else {
										i_130_ = 0;
									}
									for (/**/; i_127_ < i_128_; i_127_++) {
										for (/**/; i_130_ > i_129_; i_129_++) {
											aByteArrayArrayArray1864[i_116_][i_127_][i_129_] = aByteArrayArrayArray1864[i_116_ - 1][i_127_][i_129_];
										}
									}
								}
							}
						}
					} else {
						if (class190 == null) {
							class190 = new Class190();
						}
						class190.method1917(i_114_ ^ ~0x8, class296_sub17);
					}
				} else {
					int i_131_ = class296_sub17.g1();
					if (i_131_ > 0) {
						for (int i_132_ = 0; i_131_ > i_132_; i_132_++) {
							Class93 class93 = new Class93(var_ha, class296_sub17, 2);
							if (class93.anInt1001 == 31) {
								Class153 class153 = Class386.aClass335_3268.method3427((byte) 89, class296_sub17.g2());
								class93.method858(class153.anInt1573, (byte) 82, class153.anInt1575, class153.anInt1574, class153.anInt1571);
							}
							if (var_ha.q() > 0) {
								Class296_Sub35 class296_sub35 = class93.aClass296_Sub35_999;
								int i_133_ = class296_sub35.method2749(true) + (i << 9);
								int i_134_ = (i_113_ << 9) + class296_sub35.method2750(-4444);
								int i_135_ = i_133_ >> 9;
								int i_136_ = i_134_ >> 9;
								if (i_135_ >= 0 && i_136_ >= 0 && i_135_ < anInt1859 && i_136_ < anInt1868) {
									class296_sub35.method2747(i_133_, anIntArrayArrayArray1866[class93.anInt1006][i_135_][i_136_] - class296_sub35.method2751(i_114_ - 28924), i_134_, 16184);
									Class296_Sub39_Sub7.method2818(class93);
								}
							}
						}
					}
				}
			}
			if (class190 != null) {
				for (int i_137_ = 0; i_137_ < 8; i_137_++) {
					for (int i_138_ = 0; i_138_ < 8; i_138_++) {
						int i_139_ = (i >> 3) + i_137_;
						int i_140_ = (i_113_ >> 3) + i_138_;
						if (i_139_ >= 0 && anInt1859 >> 3 > i_139_ && i_140_ >= 0 && i_140_ < anInt1868 >> 3) {
							Class355_Sub2.method3705(i_139_, class190, (byte) -108, i_140_);
						}
					}
				}
			}
			if (i_114_ != -1) {
				anInt4514 = -5;
			}
			if (!bool && aByteArrayArrayArray1864 != null) {
				for (int i_141_ = 0; i_141_ < 4; i_141_++) {
					if (aByteArrayArrayArray1864[i_141_] != null) {
						for (int i_142_ = 0; i_142_ < 16; i_142_++) {
							for (int i_143_ = 0; i_143_ < 16; i_143_++) {
								int i_144_ = i_142_ + (i >> 2);
								int i_145_ = i_143_ + (i_113_ >> 2);
								if (i_144_ >= 0 && i_144_ < 26 && i_145_ >= 0 && i_145_ < 26) {
									aByteArrayArrayArray1864[i_141_][i_144_][i_145_] = (byte) 0;
								}
							}
						}
					}
				}
			}
		}
	}

	final void method1840(byte[] is, ClipData[] class17s, int i, ha var_ha, int i_146_, int i_147_) {
		Packet class296_sub17 = new Packet(is);
		int i_149_ = -1;
		for (;;) {
			int i_150_ = class296_sub17.method2595();
			if (i_150_ == 0) {
				break;
			}
			i_149_ += i_150_;
			int i_151_ = 0;
			for (;;) {
				int i_152_ = class296_sub17.readSmart();
				if (i_152_ == 0) {
					break;
				}
				i_151_ += i_152_ - 1;
				int i_153_ = i_151_ & 0x3f;
				int i_154_ = (i_151_ & 0xff6) >> 6;
				int i_155_ = i_151_ >> 12;
				int i_156_ = class296_sub17.g1();
				int i_157_ = i_156_ >> 2;
				int i_158_ = i_156_ & 0x3;
				int i_159_ = i_147_ + i_154_;
				int i_160_ = i_153_ + i;
				if (i_159_ > 0 && i_160_ > 0 && anInt1859 - 1 > i_159_ && i_160_ < anInt1868 - 1) {
					ClipData class17 = null;
					if (!aBoolean1860) {
						int i_161_ = i_155_;
						if ((Class41_Sub18.aByteArrayArrayArray3786[1][i_159_][i_160_] & 0x2) == 2) {
							i_161_--;
						}
						if (i_161_ >= 0) {
							class17 = class17s[i_161_];
						}
					}
					method1838(i_149_, var_ha, i_157_, class17, i_159_, i_155_, i_158_, i_160_, i_155_, -1, -100);
				}
			}
		}
	}

	Class181_Sub1(int i, int i_162_, int i_163_, boolean bool) {
		super(i, i_162_, i_163_, bool, Class250.aClass15_2360, Class262.aClass117_2449);
	}
}
