package net.zaros.client;

/* Class20 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class20 {
	int anInt237;
	int anInt238;
	private int[] anIntArray239;
	int anInt240;
	private int anInt241 = 2;
	private int[] anIntArray242 = new int[2];
	private int anInt243;
	private int anInt244;
	private int anInt245;
	private int anInt246;
	private int anInt247;

	final int method284(int i) {
		if (anInt244 >= anInt245) {
			anInt246 = anIntArray239[anInt243++] << 15;
			if (anInt243 >= anInt241)
				anInt243 = anInt241 - 1;
			anInt245 = (int) ((double) anIntArray242[anInt243] / 65536.0 * (double) i);
			if (anInt245 > anInt244)
				anInt247 = (((anIntArray239[anInt243] << 15) - anInt246) / (anInt245 - anInt244));
		}
		anInt246 += anInt247;
		anInt244++;
		return anInt246 - anInt247 >> 15;
	}

	final void method285(Packet class296_sub17) {
		anInt241 = class296_sub17.g1();
		anIntArray242 = new int[anInt241];
		anIntArray239 = new int[anInt241];
		for (int i = 0; i < anInt241; i++) {
			anIntArray242[i] = class296_sub17.g2();
			anIntArray239[i] = class296_sub17.g2();
		}
	}

	final void method286(Packet class296_sub17) {
		anInt238 = class296_sub17.g1();
		anInt237 = class296_sub17.g4();
		anInt240 = class296_sub17.g4();
		method285(class296_sub17);
	}

	final void method287() {
		anInt245 = 0;
		anInt243 = 0;
		anInt247 = 0;
		anInt246 = 0;
		anInt244 = 0;
	}

	public Class20() {
		anIntArray239 = new int[2];
		anIntArray242[0] = 0;
		anIntArray242[1] = 65535;
		anIntArray239[0] = 0;
		anIntArray239[1] = 65535;
	}
}
