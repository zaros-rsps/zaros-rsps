package net.zaros.client;

/* Class397 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Sprite implements Interface19 {
	public abstract void method4075(float f, float f_0_, float f_1_, float f_2_, float f_3_, float f_4_, int i,
			int i_5_, int i_6_, int i_7_);

	public abstract void method4076(int[] is);

	public Sprite() {
		/* empty */
	}

	private final void method4077(float f, float f_8_, float f_9_, float f_10_, float f_11_, float f_12_, int i,
			int i_13_, int i_14_) {
		method4075(f, f_8_, f_9_, f_10_, f_11_, f_12_, i, i_13_, i_14_, 1);
	}

	private final void method4078(float f, float f_15_, float f_16_, float f_17_, int i, int i_18_, int i_19_,
			int i_20_, int i_21_) {
		if (i != 0) {
			double d = (i_18_ & 0xffff) * 9.587379924285257E-5;
			float f_22_ = (float) Math.sin(d) * i;
			float f_23_ = (float) Math.cos(d) * i;
			float f_24_ = (-f_16_ * f_23_ + -f_17_ * f_22_) / 4096.0F + f;
			float f_25_ = (f_16_ * f_22_ + -f_17_ * f_23_) / 4096.0F + f_15_;
			float f_26_ = ((method4099() - f_16_) * f_23_ + -f_17_ * f_22_) / 4096.0F + f;
			float f_27_ = (-(method4099() - f_16_) * f_22_ + -f_17_ * f_23_) / 4096.0F + f_15_;
			float f_28_ = (-f_16_ * f_23_ + (method4088() - f_17_) * f_22_) / 4096.0F + f;
			float f_29_ = (f_16_ * f_22_ + (method4088() - f_17_) * f_23_) / 4096.0F + f_15_;
			method4077(f_24_, f_25_, f_26_, f_27_, f_28_, f_29_, i_19_, i_20_, i_21_);
		}
	}

	public abstract void method4079(int i, int i_30_, int i_31_, int i_32_, int i_33_);

	final void method4080(int i, int i_34_, int i_35_, int i_36_) {
		method4094(i, i_34_, i_35_, i_36_, 1, 0, 1, 1);
	}

	final void method4081(float f, float f_37_, int i, int i_38_) {
		method4078(f, f_37_, method4099() / 2.0F, method4088() / 2.0F, i, i_38_, 1, 0, 1);
	}

	final void method4082(float f, float f_39_, float f_40_, float f_41_, int i, int i_42_, aa var_aa, int i_43_,
			int i_44_) {
		if (i != 0) {
			double d = (i_42_ & 0xffff) * 9.587379924285257E-5;
			float f_45_ = (float) Math.sin(d) * i;
			float f_46_ = (float) Math.cos(d) * i;
			float f_47_ = (-f_40_ * f_46_ + -f_41_ * f_45_) / 4096.0F + f;
			float f_48_ = (f_40_ * f_45_ + -f_41_ * f_46_) / 4096.0F + f_39_;
			float f_49_ = ((method4099() - f_40_) * f_46_ + -f_41_ * f_45_) / 4096.0F + f;
			float f_50_ = (-(method4099() - f_40_) * f_45_ + -f_41_ * f_46_) / 4096.0F + f_39_;
			float f_51_ = (-f_40_ * f_46_ + (method4088() - f_41_) * f_45_) / 4096.0F + f;
			float f_52_ = (f_40_ * f_45_ + (method4088() - f_41_) * f_46_) / 4096.0F + f_39_;
			method4089(f_47_, f_48_, f_49_, f_50_, f_51_, f_52_, var_aa, i_43_, i_44_);
		}
	}

	final void method4083(int i, int i_53_, int i_54_, int i_55_, int i_56_, int i_57_, int i_58_) {
		method4094(i, i_53_, i_54_, i_55_, i_56_, i_57_, i_58_, 1);
	}

	public abstract void method4084(int i, int i_59_, int i_60_);

	final void method4085(int i, int i_61_, int i_62_, int i_63_) {
		method4098(i, i_61_, i_62_, i_63_, 1, 0, 1);
	}

	final void method4086(float f, float f_64_, int i, int i_65_, aa var_aa, int i_66_, int i_67_) {
		method4082(f, f_64_, method4099() / 2.0F, method4088() / 2.0F, i, i_65_, var_aa, i_66_, i_67_);
	}

	public abstract int method4087();

	public abstract int method4088();

	private final void method4089(float f, float f_68_, float f_69_, float f_70_, float f_71_, float f_72_, aa var_aa,
			int i, int i_73_) {
		method4091(f, f_68_, f_69_, f_70_, f_71_, f_72_, 1, var_aa, i, i_73_);
	}

	public abstract void method4090(int i, int i_74_, int i_75_, int i_76_, int i_77_, int i_78_);

	public abstract void method4091(float f, float f_79_, float f_80_, float f_81_, float f_82_, float f_83_, int i,
			aa var_aa, int i_84_, int i_85_);

	public abstract int method4092();

	public abstract void method4093(int i, int i_86_, aa var_aa, int i_87_, int i_88_);

	public abstract void method4094(int i, int i_89_, int i_90_, int i_91_, int i_92_, int i_93_, int i_94_, int i_95_);

	public void method4095(float f, float f_96_, int i, int i_97_, int i_98_, int i_99_, int i_100_) {
		method4078(f, f_96_, method4099() / 2.0F, method4088() / 2.0F, i, i_97_, i_98_, i_99_, i_100_);
	}

	public void method4096(int i, int i_101_) {
		method4079(i, i_101_, 1, 0, 1);
	}

	public abstract void method4097(int i, int i_102_, int i_103_, int i_104_);

	public abstract void method4098(int i, int i_105_, int i_106_, int i_107_, int i_108_, int i_109_, int i_110_);

	public abstract int method4099();
}
