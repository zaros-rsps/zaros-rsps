package net.zaros.client;
import jagdx.IDirect3DBaseTexture;
import jagdx.IDirect3DVolumeTexture;
import jagdx.PixelBuffer;
import jagdx.aj;

final class Class64_Sub1 extends Class64 implements Interface6_Impl2 {
	private int anInt3890;
	private int anInt3891;
	private IDirect3DVolumeTexture anIDirect3DVolumeTexture3892;
	private int anInt3893;

	public final void method28(int i) {
		aHa_Sub1_Sub2_731.method1242(this, (byte) -90);
		if (i != -9994)
			anInt3891 = 54;
	}

	Class64_Sub1(ha_Sub1_Sub2 var_ha_Sub1_Sub2, Class202 class202, int i, int i_0_, int i_1_, byte[] is) {
		super(var_ha_Sub1_Sub2, class202, Class67.aClass67_745, false, i_1_ * (i_0_ * i));
		anInt3893 = i;
		anInt3890 = i_1_;
		anInt3891 = i_0_;
		anIDirect3DVolumeTexture3892 = (aHa_Sub1_Sub2_731.anIDirect3DDevice5865.a(i, i_0_, i_1_, 1, 0, ha_Sub1_Sub2.method1253(0, aClass67_730, class202), 1));
		PixelBuffer pixelbuffer = aHa_Sub1_Sub2_731.aPixelBuffer5867;
		int i_2_ = anIDirect3DVolumeTexture3892.LockBox(0, 0, 0, 0, i, i_0_, i_1_, 0, pixelbuffer);
		if (aj.a(i_2_, (int) 102)) {
			int i_3_ = aClass202_729.anInt2042 * anInt3893;
			int i_4_ = i_3_ * anInt3891;
			int i_5_ = pixelbuffer.getSlicePitch();
			if (i_5_ == i_4_)
				pixelbuffer.a(is, 0, 0, i_3_ * (anInt3891 * anInt3890));
			else {
				int i_6_ = pixelbuffer.getRowPitch();
				if (i_3_ == i_6_) {
					for (int i_7_ = 0; i_7_ < anInt3890; i_7_++)
						pixelbuffer.a(is, i_7_ * i_4_, i_5_ * i_7_, i_4_);
				} else {
					for (int i_8_ = 0; i_8_ < anInt3890; i_8_++) {
						for (int i_9_ = 0; anInt3891 > i_9_; i_9_++)
							pixelbuffer.a(is, i_4_ * i_8_ + i_3_ * i_9_, i_8_ * i_5_ + i_6_ * i_9_, i_3_);
					}
				}
			}
			anIDirect3DVolumeTexture3892.UnlockBox(0);
		}
	}

	final IDirect3DBaseTexture method706(int i) {
		if (i != 12931)
			method28(-115);
		return anIDirect3DVolumeTexture3892;
	}

	public final void method29(Class131 class131, int i) {
		super.method29(class131, i);
	}
}
