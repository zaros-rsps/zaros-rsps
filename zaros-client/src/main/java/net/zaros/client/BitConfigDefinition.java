package net.zaros.client;

/* Class282 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class BITConfigDefinition {
	static ClipData[] mapClips;
	int configID;
	static int anInt2604 = -1;
	static String[] aStringArray2605;
	int bitLength;
	int bitOffs;

	static final int method2350(int i, Class202 class202) {
		if (i != 6409)
			method2353(-99, -76);
		if (Class122_Sub1_Sub1.aClass202_6602 == class202)
			return 6407;
		if (za_Sub2.aClass202_6555 == class202)
			return 6408;
		if (Class13.aClass202_3516 == class202)
			return 6406;
		if (class202 != StaticMethods.aClass202_6068) {
			if (class202 == Class125.aClass202_1285)
				return 6410;
			if (Class55.aClass202_654 == class202)
				return 6145;
		} else
			return 6409;
		throw new IllegalStateException();
	}

	private final void method2351(Packet class296_sub17, boolean bool, int i) {
		if (bool == true) {
			if (i == 1) {
				configID = class296_sub17.g2();
				bitOffs = class296_sub17.g1();
				bitLength = class296_sub17.g1();
			}
		}
	}

	static final void method2352(int i, int i_0_) {
		if ((Class187.anInt1910 ^ 0xffffffff) != i) {
			if (Class187.anInt1910 == 2)
				Class296_Sub39_Sub12.anInt6200 = i_0_;
		} else
			Class389.anInt3277 = i_0_;
	}

	static final boolean method2353(int i, int i_1_) {
		if (i != -8)
			method2353(-35, 125);
		if (i_1_ != 7 && i_1_ != 8 && i_1_ != 9 && i_1_ != 10)
			return false;
		return true;
	}

	final void init(Packet class296_sub17, byte i) {
		if (i >= 32) {
			for (;;) {
				int i_2_ = class296_sub17.g1();
				if (i_2_ == 0)
					break;
				method2351(class296_sub17, true, i_2_);
			}
		}
	}

	public BITConfigDefinition() {
		/* empty */
	}

	public static void method2355(byte i) {
		aStringArray2605 = null;
		mapClips = null;
		int i_3_ = 9 % (i / 57);
	}

	static {
		mapClips = new ClipData[4];
	}
}
