package net.zaros.client;
import jaclib.memory.Buffer;
import jaclib.memory.Stream;
import jaclib.memory.heap.NativeHeapBuffer;

final class Class296_Sub11 extends Node {
	private ha_Sub1 aHa_Sub1_4636;
	int anInt4637;
	int anInt4638;
	int anInt4639;
	private int[] anIntArray4640;
	int anInt4641 = 0;
	private NativeHeapBuffer aNativeHeapBuffer4642;
	int anInt4643;
	private Stream aStream4644;
	float aFloat4645;
	private s_Sub3 aS_Sub3_4646;
	static HashTable aClass263_4647 = new HashTable(64);
	private Interface15_Impl2 anInterface15_Impl2_4648;
	static int anInt4649;
	static int anInt4650;

	final void method2496(int i, int i_0_, int i_1_, int i_2_, float f) {
		if (i_1_ == -13621) {
			if (anInt4639 != -1) {
				MaterialRaw class170 = aHa_Sub1_4636.aD1299.method14(anInt4639, i_1_ + 4209);
				int i_3_ = class170.aByte1773 & 0xff;
				if (i_3_ != 0 && class170.aByte1774 != 4) {
					int i_4_;
					if (i_2_ >= 0) {
						if (i_2_ > 127)
							i_4_ = 16777215;
						else
							i_4_ = i_2_ * 131586;
					} else
						i_4_ = 0;
					if (i_3_ == 256)
						i_0_ = i_4_;
					else {
						int i_5_ = i_3_;
						int i_6_ = -i_3_ + 256;
						i_0_ = (((i_5_ * (i_4_ & 0xff00ff) + (i_0_ & 0xff00ff) * i_6_) & ~0xff00ff) + ((i_5_ * (i_4_ & 0xff00) + (i_0_ & 0xff00) * i_6_) & 0xff0000)) >> 8;
					}
				}
				int i_7_ = class170.aByte1793 & 0xff;
				if (i_7_ != 0) {
					i_7_ += 256;
					int i_8_ = i_7_ * (i_0_ >> 16 & 0xff);
					if (i_8_ > 65535)
						i_8_ = 65535;
					int i_9_ = i_7_ * ((i_0_ & 0xff00) >> 8);
					if (i_9_ > 65535)
						i_9_ = 65535;
					int i_10_ = i_7_ * (i_0_ & 0xff);
					if (i_10_ > 65535)
						i_10_ = 65535;
					i_0_ = (i_9_ & 0xff00) + ((i_8_ << 8 & 0xff0029) + (i_10_ >> 8));
				}
			}
			if (f != 1.0F) {
				int i_11_ = (i_0_ & 0xff51c5) >> 16;
				int i_12_ = (i_0_ & 0xffbc) >> 8;
				int i_13_ = i_0_ & 0xff;
				i_11_ *= f;
				i_12_ *= f;
				if (i_11_ >= 0) {
					if (i_11_ > 255)
						i_11_ = 255;
				} else
					i_11_ = 0;
				if (i_12_ >= 0) {
					if (i_12_ > 255)
						i_12_ = 255;
				} else
					i_12_ = 0;
				i_13_ *= f;
				if (i_13_ >= 0) {
					if (i_13_ > 255)
						i_13_ = 255;
				} else
					i_13_ = 0;
				i_0_ = i_13_ | (i_11_ << 16 | i_12_ << 8);
			}
			aStream4644.f(i * 4);
			if (aHa_Sub1_4636.anInt3956 == 0) {
				aStream4644.e((byte) i_0_);
				aStream4644.e((byte) (i_0_ >> 8));
				aStream4644.e((byte) (i_0_ >> 16));
			} else {
				aStream4644.e((byte) (i_0_ >> 16));
				aStream4644.e((byte) (i_0_ >> 8));
				aStream4644.e((byte) i_0_);
			}
		}
	}

	public static void method2497(byte i) {
		aClass263_4647 = null;
		int i_14_ = -61 % ((i - 89) / 34);
	}

	static final void method2498(byte i, Class296_Sub39_Sub9 class296_sub39_sub9) {
		if (class296_sub39_sub9 != null) {
			HardReferenceWrapper.aClass155_6698.addLast((byte) 120, class296_sub39_sub9);
			if (i <= 105)
				aClass263_4647 = null;
			Class230.anInt2210++;
			Object object = null;
			Class296_Sub39_Sub1 class296_sub39_sub1;
			if (class296_sub39_sub9.aBoolean6173 || "".equals(class296_sub39_sub9.aString6166)) {
				class296_sub39_sub1 = new Class296_Sub39_Sub1(class296_sub39_sub9.aString6166);
				Class239.anInt2254++;
			} else {
				long l = class296_sub39_sub9.aLong6161;
				for (class296_sub39_sub1 = ((Class296_Sub39_Sub1) FileOnDisk.aClass263_670.get(l)); class296_sub39_sub1 != null; class296_sub39_sub1 = ((Class296_Sub39_Sub1) FileOnDisk.aClass263_670.getUnknown(false))) {
					if (class296_sub39_sub1.aString6116.equals(class296_sub39_sub9.aString6166))
						break;
				}
				if (class296_sub39_sub1 == null) {
					class296_sub39_sub1 = ((Class296_Sub39_Sub1) ReferenceTable.aClass113_986.get(l));
					if (class296_sub39_sub1 != null && !class296_sub39_sub1.aString6116.equals(class296_sub39_sub9.aString6166))
						class296_sub39_sub1 = null;
					if (class296_sub39_sub1 == null)
						class296_sub39_sub1 = new Class296_Sub39_Sub1(class296_sub39_sub9.aString6166);
					FileOnDisk.aClass263_670.put(l, class296_sub39_sub1);
					Class239.anInt2254++;
				}
			}
			if (class296_sub39_sub1.method2780(class296_sub39_sub9, -1))
				HardReferenceWrapper.method2793(64, class296_sub39_sub1);
		}
	}

	final void method2499(int i, byte i_15_) {
		aNativeHeapBuffer4642 = aHa_Sub1_4636.method1109(true, (byte) -87, i * 4);
		aStream4644 = new Stream(aNativeHeapBuffer4642, 0, i * 4);
		if (i_15_ != 61)
			method2504(-105, -14);
	}

	static final void method2500(int i, int i_16_, String string, int i_17_, int i_18_, ha var_ha, int i_19_, int i_20_) {
		if (Class363.aClass397_3107 == null || Class49.aClass397_463 == null) {
			if (Class205_Sub2.fs8.hasEntryBuffer(Class366_Sub3.anInt5374) && Class205_Sub2.fs8.hasEntryBuffer(Class93.anInt1003)) {
				Class363.aClass397_3107 = var_ha.a(Class186.method1876((Class205_Sub2.fs8), Class366_Sub3.anInt5374, 0), true);
				Class186 class186 = Class186.method1876(Class205_Sub2.fs8, Class93.anInt1003, 0);
				Class49.aClass397_463 = var_ha.a(class186, true);
				class186.method1879();
				Class296_Sub39_Sub17.aClass397_6244 = var_ha.a(class186, true);
			} else
				var_ha.aa(i, i_19_, i_16_, i_18_, Class277.anInt2535 | -Class314.anInt2786 + 255 << 24, 1);
		}
		if (Class363.aClass397_3107 != null && Class49.aClass397_463 != null) {
			int i_21_ = ((-(Class49.aClass397_463.method4087() * 2) + i_16_) / Class363.aClass397_3107.method4087());
			for (int i_22_ = 0; i_21_ > i_22_; i_22_++)
				Class363.aClass397_3107.method4096((i + Class49.aClass397_463.method4087() + i_22_ * Class363.aClass397_3107.method4087()), i_19_);
			Class49.aClass397_463.method4096(i, i_19_);
			Class296_Sub39_Sub17.aClass397_6244.method4096(-Class296_Sub39_Sub17.aClass397_6244.method4087() + (i_16_ + i), i_19_);
		}
		Class49.aClass55_461.a(-1, 1659, Class25.anInt285 | ~0xffffff, i + 3, string, i_19_ + 14);
		if (i_17_ == 0)
			var_ha.aa(i, i_18_ + i_19_, i_16_, -i_18_ + i_20_, Class277.anInt2535 | -Class314.anInt2786 + 255 << 24, 1);
	}

	final void method2501(int[] is, int i, int i_23_) {
		if (i == 1) {
			Interface15_Impl1 interface15_impl1 = aHa_Sub1_4636.method1223(anInt4641 * 3, (byte) -116);
			Buffer buffer = interface15_impl1.method30((byte) -100, true);
			if (buffer != null) {
				Stream stream = aHa_Sub1_4636.method1156(buffer, -127);
				int i_24_ = 0;
				int i_25_ = 32767;
				int i_26_ = -32768;
				if (Stream.a()) {
					for (int i_27_ = 0; i_23_ > i_27_; i_27_++) {
						int i_28_ = is[i_27_];
						int i_29_ = anIntArray4640[i_28_];
						short[] is_30_ = aS_Sub3_4646.aShortArrayArray5152[i_28_];
						if (i_29_ != 0 && is_30_ != null) {
							int i_31_ = 0;
							int i_32_ = 0;
							while (is_30_.length > i_32_) {
								if ((i_29_ & 1 << i_31_++) == 0)
									i_32_ += 3;
								else {
									for (int i_33_ = 0; i_33_ < 3; i_33_++) {
										int i_34_ = is_30_[i_32_++] & 0xffff;
										stream.d(i_34_);
										if (i_26_ < i_34_)
											i_26_ = i_34_;
										if (i_25_ > i_34_)
											i_25_ = i_34_;
									}
									i_24_++;
								}
							}
						}
					}
				} else {
					for (int i_35_ = 0; i_35_ < i_23_; i_35_++) {
						int i_36_ = is[i_35_];
						short[] is_37_ = aS_Sub3_4646.aShortArrayArray5152[i_36_];
						int i_38_ = anIntArray4640[i_36_];
						if (i_38_ != 0 && is_37_ != null) {
							int i_39_ = 0;
							int i_40_ = 0;
							while (i_40_ < is_37_.length) {
								if ((1 << i_39_++ & i_38_) != 0) {
									for (int i_41_ = 0; i_41_ < 3; i_41_++) {
										int i_42_ = is_37_[i_40_++] & 0xffff;
										if (i_25_ > i_42_)
											i_25_ = i_42_;
										stream.a(i_42_);
										if (i_42_ > i_26_)
											i_26_ = i_42_;
									}
									i_24_++;
								} else
									i_40_ += 3;
							}
						}
					}
				}
				stream.b();
				if (interface15_impl1.method33(i ^ 0x7db8) && i_24_ > 0) {
					aHa_Sub1_4636.method1133(((aS_Sub3_4646.anInt5150 & 0x8) != 0), anInt4639, 0, ((aS_Sub3_4646.anInt5150 & 0x7) != 0));
					if (aHa_Sub1_4636.aBoolean4038)
						aHa_Sub1_4636.EA(2147483647, anInt4638, anInt4643, anInt4637);
					Class373_Sub2 class373_sub2 = aHa_Sub1_4636.method1153(104);
					class373_sub2.method3931(1.0F, 1.0F / aFloat4645, 1.0F / aFloat4645, i - 34);
					aHa_Sub1_4636.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
					aHa_Sub1_4636.method1178(anInterface15_Impl2_4648, (byte) -127, 1);
					aHa_Sub1_4636.method1209(aS_Sub3_4646.aClass127_5173, (byte) -126);
					aHa_Sub1_4636.method1232(Class166_Sub1.aClass289_4298, interface15_impl1, i_26_ + 1 - i_25_, i_24_, 0, 44, i_25_);
					aHa_Sub1_4636.method1125(0);
				}
			}
		}
	}

	final void method2502(int i, int i_43_, int i_44_, int i_45_) {
		int i_46_ = -117 % ((i + 66) / 55);
		anIntArray4640[i_44_ + aS_Sub3_4646.anInt2832 * i_45_] = (anIntArray4640[i_44_ + aS_Sub3_4646.anInt2832 * i_45_]) | 1 << i_43_;
		anInt4641++;
	}

	final void method2503(boolean bool, int i) {
		aStream4644.b();
		if (bool == true) {
			anInterface15_Impl2_4648 = aHa_Sub1_4636.method1205(false, -85);
			anInterface15_Impl2_4648.method48(false, aNativeHeapBuffer4642, 4, i * 4);
			aStream4644 = null;
			aNativeHeapBuffer4642 = null;
		}
	}

	final void method2504(int i, int i_47_) {
		aStream4644.f(i_47_ * 4 + 3);
		int i_48_ = 45 / ((38 - i) / 55);
		aStream4644.e(-1);
	}

	Class296_Sub11(s_Sub3 var_s_Sub3, int i, int i_49_, int i_50_, int i_51_, int i_52_) {
		aS_Sub3_4646 = var_s_Sub3;
		anInt4637 = i_52_;
		anInt4643 = i_51_;
		anInt4638 = i_50_;
		aFloat4645 = (float) i_49_;
		anIntArray4640 = new int[aS_Sub3_4646.anInt2834 * aS_Sub3_4646.anInt2832];
		anInt4639 = i;
		aHa_Sub1_4636 = aS_Sub3_4646.aHa_Sub1_5151;
	}
}
