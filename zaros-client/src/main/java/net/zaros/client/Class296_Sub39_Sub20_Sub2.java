package net.zaros.client;

/* Class296_Sub39_Sub20_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub20_Sub2 extends Class296_Sub39_Sub20 {
	static int anInt6727;
	static int anInt6728;
	static int[][] anIntArrayArray6729 = {{0, 2, 4, 6}, {6, 0, 2, 4}, {6, 0, 2}, {2, 6, 0}, {0, 2, 6}, {6, 0, 2}, {5, 6, 0, 1, 2, 4}, {7, 2, 4, 4}, {2, 4, 4, 7}, {6, 6, 4, 0, 2, 2}, {0, 2, 2, 6, 6, 4}, {0, 2, 2, 4, 6, 6}, {0, 2, 4, 6}};
	byte[] aByteArray6730;
	int anInt6731;
	Js5DiskStore aClass168_6732;

	public static void method2909(int i) {
		if (i != 0)
			anIntArrayArray6729 = null;
		anIntArrayArray6729 = null;
	}

	static final boolean method2910(r var_r, int i, int i_0_, int i_1_, boolean[] bools) {
		boolean bool = false;
		if (Class360_Sub2.aSArray5304 != Class52.aSArray636) {
			int i_2_ = Class244.aSArray2320[i].method3349(0, i_1_, i_0_);
			int i_3_ = 0;
			for (/**/; i_3_ <= i; i_3_++) {
				s var_s = Class244.aSArray2320[i_3_];
				if (var_s != null) {
					int i_4_ = i_2_ - var_s.method3349(0, i_1_, i_0_);
					if (bools != null) {
						bools[i_3_] = var_s.method3351(var_r, i_0_, i_4_, i_1_, 0, false);
						if (!bools[i_3_])
							continue;
					}
					var_s.CA(var_r, i_0_, i_4_, i_1_, 0, false);
					bool = true;
				}
			}
		}
		return bool;
	}

	final int method2902(int i) {
		if (aBoolean6256)
			return 0;
		if (i != 100)
			anInt6727 = -46;
		return 100;
	}

	final byte[] method2904(int i) {
		if (i != 4)
			method2902(102);
		if (aBoolean6256)
			throw new RuntimeException();
		return aByteArray6730;
	}

	public Class296_Sub39_Sub20_Sub2() {
		/* empty */
	}

	static {
		anInt6727 = 0;
	}
}
