package net.zaros.client;

/* Class42_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class42_Sub4 extends Class42 {
	public static short[][][] aShortArrayArrayArray3846;
	static Js5 fs6;
	private int anInt3848;
	static int anInt3849 = -50;
	private int anInt3850;
	static int anInt3851 = 0;
	private int anInt3852;
	private int anInt3853;
	static int[] anIntArray3854 = new int[1];

	@Override
	final void method531(int i, int i_0_, int i_1_) {
		if (i != 68) {
			method539(-89);
		}
		int i_2_ = anInt3850 * i_1_ >> 12;
		int i_3_ = i_1_ * anInt3852 >> 12;
		int i_4_ = anInt3853 * i_0_ >> 12;
		int i_5_ = anInt3848 * i_0_ >> 12;
		Class296_Sub35_Sub2.method2757(93, i_3_, i_4_, i_2_, anInt394, i_5_);
	}

	@Override
	final void method529(int i, int i_6_, int i_7_) {
		int i_8_ = anInt3850 * i_6_ >> 12;
		if (i_7_ > -11) {
			method539(116);
		}
		int i_9_ = i_6_ * anInt3852 >> 12;
		int i_10_ = i * anInt3853 >> 12;
		int i_11_ = anInt3848 * i >> 12;
		Class296_Sub44.method2924(i_9_, i_8_, anInt397, i_11_, i_10_, 28127, anInt393);
	}

	@Override
	final void method528(boolean bool, int i, int i_12_) {
		if (bool) {
			fs6 = null;
		}
		int i_13_ = i_12_ * anInt3850 >> 12;
		int i_14_ = anInt3852 * i_12_ >> 12;
		int i_15_ = anInt3853 * i >> 12;
		int i_16_ = i * anInt3848 >> 12;
		Class161.method1620(i_16_, i_13_, i_14_, -128, anInt394, anInt397, anInt393, i_15_);
	}

	public static void method539(int i) {
		fs6 = null;
		aShortArrayArrayArray3846 = null;
		if (i != -2015) {
			aShortArrayArrayArray3846 = null;
		}
		anIntArray3854 = null;
	}

	Class42_Sub4(int i, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_, int i_22_) {
		super(i_20_, i_21_, i_22_);
		anInt3848 = i_19_;
		anInt3852 = i_18_;
		anInt3853 = i_17_;
		anInt3850 = i;
	}
}
