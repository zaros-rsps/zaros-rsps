package net.zaros.client;
/* Class41_Sub15 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub15 extends Class41 {
	static int anInt3779;

	Class41_Sub15(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final int method380(int i, byte i_0_) {
		if (i_0_ != 41)
			anInt3779 = -115;
		return 1;
	}

	final void method386(int i) {
		if (i == 2) {
			if (anInt389 < 0 || anInt389 > 4)
				anInt389 = method383((byte) 110);
		}
	}

	final void method381(int i, byte i_1_) {
		if (i_1_ != -110)
			anInt3779 = 3;
		anInt389 = i;
	}

	final int method383(byte i) {
		if (i != 110)
			method452(-58);
		return 0;
	}

	final int method452(int i) {
		if (i <= 114)
			anInt3779 = 60;
		return anInt389;
	}

	Class41_Sub15(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}
}
