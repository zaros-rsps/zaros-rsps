package net.zaros.client;

/* Class41_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub4 extends Class41 {
	static Class125 aClass125_3745 = new Class125();
	static OutgoingPacket aClass311_3746 = new OutgoingPacket(22, -1);
	static OutgoingPacket aClass311_3747;
	static int[] anIntArray3748 = new int[3];
	static IncomingPacket aClass231_3749;
	static OutgoingPacket aClass311_3750;

	final int method383(byte i) {
		if (i != 110)
			aClass311_3746 = null;
		return 0;
	}

	Class41_Sub4(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final void method381(int i, byte i_0_) {
		if (i_0_ != -110)
			method380(-128, (byte) -52);
		anInt389 = i;
	}

	final int method402(int i) {
		if (i <= 114)
			anIntArray3748 = null;
		return anInt389;
	}

	public static void method403(byte i) {
		anIntArray3748 = null;
		aClass311_3747 = null;
		aClass231_3749 = null;
		if (i != -117)
			method406(20, -91, -7, 46, 3, (byte) 22, 9, -43, -104, 9);
		aClass311_3746 = null;
		aClass311_3750 = null;
		aClass125_3745 = null;
	}

	static final int animFileID(int animID) {
		return animID >>> 7;
	}

	static final void method406(int i, int i_30_, int i_31_, int i_32_, int i_33_, byte i_34_, int i_35_, int i_36_, int i_37_, int i_38_) {
		if (i_34_ < 119)
			method406(-125, -16, -98, 124, -126, (byte) -46, -6, -55, 58, -109);
		if (i_36_ != i_32_ || i_30_ != i_38_ || i_33_ != i_31_ || i_35_ != i_37_) {
			int i_39_ = i_36_;
			int i_40_ = i_30_;
			int i_41_ = i_36_ * 3;
			int i_42_ = i_30_ * 3;
			int i_43_ = i_32_ * 3;
			int i_44_ = i_38_ * 3;
			int i_45_ = i_31_ * 3;
			int i_46_ = i_35_ * 3;
			int i_47_ = -i_36_ + (-i_45_ + (i_33_ + i_43_));
			int i_48_ = i_37_ - (i_46_ - i_44_) - i_30_;
			int i_49_ = i_41_ - i_43_ + (-i_43_ + i_45_);
			int i_50_ = -i_44_ + (i_46_ - i_44_) + i_42_;
			int i_51_ = -i_41_ + i_43_;
			int i_52_ = -i_42_ + i_44_;
			for (int i_53_ = 128; i_53_ <= 4096; i_53_ += 128) {
				int i_54_ = i_53_ * i_53_ >> 12;
				int i_55_ = i_54_ * i_53_ >> 12;
				int i_56_ = i_47_ * i_55_;
				int i_57_ = i_55_ * i_48_;
				int i_58_ = i_49_ * i_54_;
				int i_59_ = i_54_ * i_50_;
				int i_60_ = i_51_ * i_53_;
				int i_61_ = i_52_ * i_53_;
				int i_62_ = i_36_ + (i_60_ + i_56_ + i_58_ >> 12);
				int i_63_ = i_30_ + (i_59_ + (i_57_ + i_61_) >> 12);
				Class199.method1947(i, false, i_62_, i_40_, i_39_, i_63_);
				i_40_ = i_63_;
				i_39_ = i_62_;
			}
		} else
			Class199.method1947(i, false, i_33_, i_30_, i_36_, i_37_);
	}

	final void method386(int i) {
		if (i == 2) {
			int i_64_ = aClass296_Sub50_392.method3055(true).method327(false);
			if (i_64_ < 96)
				anInt389 = 0;
			if (anInt389 > 1 && i_64_ < 128)
				anInt389 = 1;
			if (anInt389 > 2 && i_64_ < 192)
				anInt389 = 2;
			if (anInt389 < 0 || anInt389 > 3)
				anInt389 = method383((byte) 110);
		}
	}

	final int method380(int i, byte i_65_) {
		if (i_65_ != 41)
			return -50;
		int i_66_ = aClass296_Sub50_392.method3055(true).method327(false);
		if (i_66_ < 96)
			return 3;
		if (i > 1 && i_66_ < 128)
			return 3;
		if (i > 3 && i_66_ < 192)
			return 3;
		return 1;
	}

	Class41_Sub4(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	static final void method407(boolean bool, int i) {
		if (i != -25490)
			method403((byte) 92);
		Class94.aClass373_1016.method3915(Class296_Sub51_Sub36.aHa6529.o());
		int[] is = Class296_Sub51_Sub36.aHa6529.Y();
		Class379_Sub2.anInt5685 = is[3];
		ha_Sub1_Sub1.anInt5825 = is[1];
		IntegerNode.anInt4674 = is[2];
		Class296_Sub39_Sub19.anInt6251 = is[0];
		if (bool) {
			Class296_Sub51_Sub36.aHa6529.DA(StaticMethods.anInt1843, Class361.anInt3099, Class63.anInt727, Class198.anInt2001);
			EffectiveVertex.method2114(Class217.aDouble2121, (byte) 2);
		} else {
			Class296_Sub51_Sub36.aHa6529.DA(Class368_Sub14.anInt5509, StaticMethods.anInt287, Class27.anInt297, Class219_Sub1.anInt4564);
			EffectiveVertex.method2114(Class296_Sub51_Sub10.aDouble6395, (byte) 2);
		}
	}

	final boolean method408(int i) {
		int i_67_ = aClass296_Sub50_392.method3055(true).method327(false);
		if (i != -25952)
			return true;
		if (i_67_ < 96)
			return false;
		return true;
	}

	static {
		aClass311_3747 = new OutgoingPacket(30, -1);
		aClass231_3749 = new IncomingPacket(9, 10);
		aClass311_3750 = new OutgoingPacket(15, 4);
	}
}
