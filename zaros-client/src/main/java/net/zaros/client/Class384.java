package net.zaros.client;

/* Class384 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class384 {
	static String aString3252 = "";
	static Class81 aClass81_3253 = new Class81("", 16);
	static int anInt3254;

	static final void clearNpcsOnCondition() {
		if (Class338_Sub2.mapLoadType != -1 && Class296_Sub40.currentMapLoadType != -1) {
			if (Class338_Sub2.mapLoadType == 1 || Class338_Sub2.mapLoadType == 3 || (Class296_Sub40.currentMapLoadType != Class338_Sub2.mapLoadType && (Class338_Sub2.mapLoadType == 0 || Class296_Sub40.currentMapLoadType == 0))) {
				Class367.npcsCount = 0;
				Class368_Sub23.npcNodeCount = 0;
				Class41_Sub18.localNpcs.clear();
			}
			Class296_Sub40.currentMapLoadType = Class338_Sub2.mapLoadType;
		}
	}

	static final void method4016(byte i) {
		if (!Class41_Sub3.aBoolean3744) {
			Billboard.method4024(Class399.aClass247ArrayArrayArray3355, -30633);
			if (Class351.aClass247ArrayArrayArray3041 != null)
				Billboard.method4024(Class351.aClass247ArrayArrayArray3041, -30633);
			int i_0_ = 125 / ((i - 25) / 55);
			Class41_Sub3.aBoolean3744 = true;
		}
	}

	public static void method4017(boolean bool) {
		if (bool)
			aClass81_3253 = null;
		aClass81_3253 = null;
		aString3252 = null;
	}

	static final void method4018(byte i) {
		Class368_Sub16.aHa5527.xa(((float) Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub6_5014.method416(118) * 0.1F + 0.7F) * Animation.aFloat406);
		Class368_Sub16.aHa5527.ZA(Mobile.anInt6787, OutputStream_Sub2.aFloat41, Class30.aFloat316, (float) (Class42_Sub4.anInt3849 << 2), (float) (Class296_Sub38.anInt4900 << 2), (float) (HashTable.anInt2466 << 2));
		int i_1_ = -76 % ((i - 17) / 56);
		Class368_Sub16.aHa5527.a(Class296_Sub51_Sub23.aClass241_6458);
	}

	static final void method4019(InterfaceComponent class51, InterfaceComponent class51_2_, int i) {
		Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 123, Class15.aClass311_180);
		class296_sub1.out.p2(class51.clickedItem);
		class296_sub1.out.writeInt_v2(class51_2_.uid);
		class296_sub1.out.p2(class51.anInt592);
		if (i >= -19)
			method4018((byte) -14);
		class296_sub1.out.p2(class51_2_.anInt592);
		class296_sub1.out.p2(class51_2_.clickedItem);
		class296_sub1.out.p4(class51.uid);
		Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
	}
}
