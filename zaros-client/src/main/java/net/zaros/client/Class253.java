package net.zaros.client;

/* Class253 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class253 {
	static Class230 aClass230_2387 = new Class230();
	static Class373 aClass373_2388;
	static Class326 aClass326_2389;

	public static void method2207(byte i) {
		aClass230_2387 = null;
		aClass326_2389 = null;
		aClass373_2388 = null;
		if (i >= -45)
			aClass230_2387 = null;
	}

	static Class296_Sub1 allocateOut(ISAACCipher class185, byte i, OutgoingPacket packet) {
		Class296_Sub1 class296_sub1 = Class368_Sub4.method3821((byte) -68);
		class296_sub1.aClass311_4581 = packet;
		class296_sub1.anInt4585 = packet.anInt2768;
		if (class296_sub1.anInt4585 != -1) {
			if (class296_sub1.anInt4585 == -2)
				class296_sub1.out = new ByteStream(10000);
			else if (class296_sub1.anInt4585 > 18) {
				if (class296_sub1.anInt4585 <= 98)
					class296_sub1.out = new ByteStream(100);
				else
					class296_sub1.out = new ByteStream(260);
			} else
				class296_sub1.out = new ByteStream(20);
		} else
			class296_sub1.out = new ByteStream(260);
		class296_sub1.out.setCipher(class185);
		class296_sub1.out.writeOpcode(class296_sub1.aClass311_4581.method3302((byte) -120));
		class296_sub1.anInt4587 = 0;
		if (i <= 83)
			method2207((byte) -88);
		return class296_sub1;
	}

	static final Class42_Sub3 method2209(int i, Packet class296_sub17) {
		if (i != 0)
			method2209(87, null);
		return new Class42_Sub3(class296_sub17.g2b(), class296_sub17.g2b(), class296_sub17.g2b(), class296_sub17.g2b(), class296_sub17.readUnsignedMedInt(), class296_sub17.readUnsignedMedInt(), class296_sub17.g1());
	}
}
