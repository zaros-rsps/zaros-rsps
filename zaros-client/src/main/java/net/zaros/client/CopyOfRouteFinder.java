package net.zaros.client;

public class CopyOfRouteFinder {

	static int[][] directions = new int[128][128];
	static int[][] distances = new int[128][128];
	static int[] queueX = new int[4096];
	static int[] queueY = new int[4096];
	static int FoundX;
	static int FoundY;
	
	/**
	 * Finds route,
	 * @return
	 * returns number of steps found.
	 */
	static final int findRoute(ClipData clipData, int clipType, int creatureSize, int fromMapX, int fromMapY, int toMapX, int toMapY, int targetSizeX, int targetSizeY, int targetFace, int[] pathBufferX, int[] pathBufferY, boolean findAlternative, int i_1_) {
		for (int x = 0; x < 128; x++) {
			for (int y = 0; y < 128; y++) {
				CopyOfRouteFinder.directions[x][y] = 0;
				CopyOfRouteFinder.distances[x][y] = 99999999;
			}
		}
		boolean foundPath;
		
		switch (creatureSize) {
			case 1:
				foundPath = CopyOfRouteFinder.findRouteSize1(clipData, clipType, fromMapX, fromMapY, toMapX, toMapY, targetSizeX, targetSizeY, targetFace, i_1_);
				break;
			case 2:
				foundPath = CopyOfRouteFinder.findRouteSize2(clipData, clipType, fromMapX, fromMapY, toMapX, toMapY, targetSizeX, targetSizeY, targetFace, i_1_);
				break;
			default:
				foundPath = CopyOfRouteFinder.findRouteSizeX(clipData, clipType, fromMapX, fromMapY, toMapX, toMapY, targetSizeX, targetSizeY, targetFace, creatureSize, i_1_);
				break;
		}
		
		int arrayOffsetX = fromMapX - 64;
		int arrayOffsetY = fromMapY - 64;
		int foundX = CopyOfRouteFinder.FoundX;
		int foundY = CopyOfRouteFinder.FoundY;
		if (!foundPath) {
			if (!findAlternative)
				return -1;
			int lowestCost = 2147483647;
			int lowestDistance = 2147483647;
			int checkDistance = 10;
			for (int checkX = toMapX - checkDistance; toMapX + checkDistance >= checkX; checkX++) {
				for (int checkY = toMapY - checkDistance; toMapY + checkDistance >= checkY; checkY++) {
					int x = checkX - arrayOffsetX;
					int y = checkY - arrayOffsetY;
					if (x >= 0 && y >= 0 && x < 128 && y < 128 && (CopyOfRouteFinder.distances[x][y] < 100)) {
						int deltaX = 0;
						if (toMapX <= checkX) {
							if (targetSizeX + toMapX - 1 < checkX)
								deltaX = 1 - toMapX - (targetSizeX - checkX);
						} else
							deltaX = toMapX - checkX;
						int deltaY = 0;
						if (toMapY <= checkY) {
							if (targetSizeY + toMapY - 1 < checkY)
								deltaY = 1 - toMapY - (targetSizeY - checkY);
						} else
							deltaY = toMapY - checkY;
						int cost = deltaX * deltaX + deltaY * deltaY;
						if (cost < lowestCost || (cost == lowestCost && (CopyOfRouteFinder.distances[x][y]) < lowestDistance)) {
							lowestDistance = (CopyOfRouteFinder.distances[x][y]);
							foundY = checkY;
							foundX = checkX;
							lowestCost = cost;
						}
					}
				}
			}
			if (lowestCost == 2147483647)
				return -1;
		}
		if (foundX == fromMapX && fromMapY == foundY)
			return 0;
		int queueWrite = 0;
		CopyOfRouteFinder.queueX[queueWrite] = foundX;
		CopyOfRouteFinder.queueY[queueWrite++] = foundY;
		int lastWritenDirection;
		int direction = (lastWritenDirection = CopyOfRouteFinder.directions[foundX - arrayOffsetX][foundY - arrayOffsetY]);
		while (fromMapX != foundX || foundY != fromMapY) {
			if (lastWritenDirection != direction) {
				CopyOfRouteFinder.queueX[queueWrite] = foundX;
				CopyOfRouteFinder.queueY[queueWrite++] = foundY;
				lastWritenDirection = direction;
			}
			if ((direction & 0x1) == 0) {
				if ((direction & 0x4) != 0)
					foundY--;
			} else
				foundY++;
			if ((direction & 0x2) != 0)
				foundX++;
			else if ((direction & 0x8) != 0)
				foundX--;
			direction = CopyOfRouteFinder.directions[foundX - arrayOffsetX][foundY - arrayOffsetY];
		}
		int numSteps = 0;
		while (queueWrite-- > 0) {
			pathBufferX[numSteps] = CopyOfRouteFinder.queueX[queueWrite];
			pathBufferY[numSteps++] = CopyOfRouteFinder.queueY[queueWrite];
			if (pathBufferX.length <= numSteps)
				break;
		}
		return numSteps;
	}


	private static final boolean findRouteSizeX(ClipData clipData, int clipType, int fromMapX, int fromMapY, int toMapX, int toMapY, int targetSizeX, int targetSizeY, int targetFace, int creatureSize, int i_3_) {
		int i_11_ = fromMapX;
		int i_12_ = fromMapY;
		int i_13_ = 64;
		int i_14_ = 64;
		int i_15_ = fromMapX - i_13_;
		directions[i_13_][i_14_] = 99;
		int i_16_ = -i_14_ + fromMapY;
		distances[i_13_][i_14_] = 0;
		int i_17_ = 0;
		int i_18_ = 0;
		CopyOfRouteFinder.queueX[i_17_] = i_11_;
		CopyOfRouteFinder.queueY[i_17_++] = i_12_;
		int[][] is = clipData.clip;
		while_12_ : while (i_18_ != i_17_) {
			i_12_ = CopyOfRouteFinder.queueY[i_18_];
			i_11_ = CopyOfRouteFinder.queueX[i_18_];
			i_13_ = i_11_ - i_15_;
			i_18_ = i_18_ + 1 & 0xfff;
			i_14_ = i_12_ - i_16_;
			int i_19_ = -clipData.offsetX + i_11_;
			int i_20_ = i_12_ - clipData.offsetY;
			int i_21_ = clipType;
			while_4_ : do {
				while_3_ : do {
					while_2_ : do {
						while_1_ : do {
							do {
								if (i_21_ != -4) {
									if (i_21_ != -3) {
										if (i_21_ != -2) {
											if (i_21_ != -1) {
												if (i_21_ == 0 || i_21_ == 1 || i_21_ == 2 || i_21_ == 3 || i_21_ == 9)
													break while_2_;
												break while_3_;
											}
										} else
											break;
										break while_1_;
									}
								} else {
									if (i_11_ == toMapX && toMapY == i_12_) {
										CopyOfRouteFinder.FoundY = i_12_;
										CopyOfRouteFinder.FoundX = i_11_;
										return true;
									}
									break while_4_;
								}
								if (ClipData.checkMove(i_11_, i_12_, toMapX, toMapY, creatureSize, creatureSize, targetSizeX, targetSizeY)) {
									CopyOfRouteFinder.FoundX = i_11_;
									CopyOfRouteFinder.FoundY = i_12_;
									return true;
								}
								break while_4_;
							} while (false);
							if (clipData.checkMove(i_11_, i_12_, toMapX, toMapY, creatureSize, creatureSize, targetSizeX, targetSizeY, i_3_)) {
								CopyOfRouteFinder.FoundY = i_12_;
								CopyOfRouteFinder.FoundX = i_11_;
								return true;
							}
							break while_4_;
						} while (false);
						if (clipData.checkMove_(i_11_, i_12_, toMapX, toMapY, creatureSize, targetSizeX, targetSizeY, i_3_)) {
							CopyOfRouteFinder.FoundX = i_11_;
							CopyOfRouteFinder.FoundY = i_12_;
							return true;
						}
						break while_4_;
					} while (false);
					if (clipData.checkMove_(clipType, toMapX, toMapY, i_11_, i_12_, targetFace, creatureSize)) {
						CopyOfRouteFinder.FoundY = i_12_;
						CopyOfRouteFinder.FoundX = i_11_;
						return true;
					}
					break while_4_;
				} while (false);
				if (clipData.checkMove(clipType, i_11_, i_12_, toMapX, toMapY, creatureSize, targetFace)) {
					CopyOfRouteFinder.FoundY = i_12_;
					CopyOfRouteFinder.FoundX = i_11_;
					return true;
				}
			} while (false);
			i_21_ = distances[i_13_][i_14_] + 1;
			while_5_ : do {
				if (i_13_ > 0 && directions[i_13_ - 1][i_14_] == 0 && (is[i_19_ - 1][i_20_] & 0x43a40000) == 0 && ((is[i_19_ - 1][creatureSize + (i_20_ - 1)] & 0x4e240000) == 0)) {
					for (int i_22_ = 1; i_22_ < creatureSize - 1; i_22_++) {
						if ((is[i_19_ - 1][i_22_ + i_20_] & 0x4fa40000) != 0)
							break while_5_;
					}
					CopyOfRouteFinder.queueX[i_17_] = i_11_ - 1;
					CopyOfRouteFinder.queueY[i_17_] = i_12_;
					i_17_ = i_17_ + 1 & 0xfff;
					directions[i_13_ - 1][i_14_] = 2;
					distances[i_13_ - 1][i_14_] = i_21_;
				}
			} while (false);
			while_6_ : do {
				if (-creatureSize + 128 > i_13_ && directions[i_13_ + 1][i_14_] == 0 && (is[i_19_ + creatureSize][i_20_] & 0x60e40000) == 0 && ((is[i_19_ + creatureSize][creatureSize + (i_20_ - 1)] & 0x78240000) == 0)) {
					for (int i_23_ = 1; creatureSize - 1 > i_23_; i_23_++) {
						if ((is[i_19_ + creatureSize][i_23_ + i_20_] & 0x78e40000) != 0)
							break while_6_;
					}
					CopyOfRouteFinder.queueX[i_17_] = i_11_ + 1;
					CopyOfRouteFinder.queueY[i_17_] = i_12_;
					directions[i_13_ + 1][i_14_] = 8;
					i_17_ = i_17_ + 1 & 0xfff;
					distances[i_13_ + 1][i_14_] = i_21_;
				}
			} while (false);
			while_7_ : do {
				if (i_14_ > 0 && directions[i_13_][i_14_ - 1] == 0 && (is[i_19_][i_20_ - 1] & 0x43a40000) == 0 && (is[creatureSize + i_19_ - 1][i_20_ - 1] & 0x60e40000) == 0) {
					for (int i_24_ = 1; creatureSize - 1 > i_24_; i_24_++) {
						if ((is[i_19_ + i_24_][i_20_ - 1] & 0x63e40000) != 0)
							break while_7_;
					}
					CopyOfRouteFinder.queueX[i_17_] = i_11_;
					CopyOfRouteFinder.queueY[i_17_] = i_12_ - 1;
					directions[i_13_][i_14_ - 1] = 1;
					i_17_ = i_17_ + 1 & 0xfff;
					distances[i_13_][i_14_ - 1] = i_21_;
				}
			} while (false);
			while_8_ : do {
				if (i_14_ < -creatureSize + 128 && directions[i_13_][i_14_ + 1] == 0 && (is[i_19_][i_20_ + creatureSize] & 0x4e240000) == 0 && ((is[creatureSize + i_19_ - 1][creatureSize + i_20_] & 0x78240000) == 0)) {
					for (int i_25_ = 1; creatureSize - 1 > i_25_; i_25_++) {
						if ((is[i_19_ + i_25_][i_20_ + creatureSize] & 0x7e240000) != 0)
							break while_8_;
					}
					CopyOfRouteFinder.queueX[i_17_] = i_11_;
					CopyOfRouteFinder.queueY[i_17_] = i_12_ + 1;
					directions[i_13_][i_14_ + 1] = 4;
					i_17_ = i_17_ + 1 & 0xfff;
					distances[i_13_][i_14_ + 1] = i_21_;
				}
			} while (false);
			while_9_ : do {
				if (i_13_ > 0 && i_14_ > 0 && directions[i_13_ - 1][i_14_ - 1] == 0 && (is[i_19_ - 1][i_20_ - 1] & 0x43a40000) == 0) {
					for (int i_26_ = 1; i_26_ < creatureSize; i_26_++) {
						if (((is[i_19_ - 1][i_20_ + (-1 + i_26_)] & 0x4fa40000) != 0) || ((is[i_19_ + i_26_ - 1][i_20_ - 1] & 0x63e40000) != 0))
							break while_9_;
					}
					CopyOfRouteFinder.queueX[i_17_] = i_11_ - 1;
					CopyOfRouteFinder.queueY[i_17_] = i_12_ - 1;
					directions[i_13_ - 1][i_14_ - 1] = 3;
					i_17_ = i_17_ + 1 & 0xfff;
					distances[i_13_ - 1][i_14_ - 1] = i_21_;
				}
			} while (false);
			while_10_ : do {
				if (i_13_ < -creatureSize + 128 && i_14_ > 0 && directions[i_13_ + 1][i_14_ - 1] == 0 && (is[i_19_ + creatureSize][i_20_ - 1] & 0x60e40000) == 0) {
					for (int i_27_ = 1; i_27_ < creatureSize; i_27_++) {
						if ((is[creatureSize + i_19_][i_27_ + (i_20_ - 1)] & 0x78e40000) != 0 || ((is[i_19_ + i_27_][i_20_ - 1] & 0x63e40000) != 0))
							break while_10_;
					}
					CopyOfRouteFinder.queueX[i_17_] = i_11_ + 1;
					CopyOfRouteFinder.queueY[i_17_] = i_12_ - 1;
					directions[i_13_ + 1][i_14_ - 1] = 9;
					i_17_ = i_17_ + 1 & 0xfff;
					distances[i_13_ + 1][i_14_ - 1] = i_21_;
				}
			} while (false);
			while_11_ : do {
				if (i_13_ > 0 && i_14_ < -creatureSize + 128 && directions[i_13_ - 1][i_14_ + 1] == 0 && (is[i_19_ - 1][i_20_ + creatureSize] & 0x4e240000) == 0) {
					for (int i_28_ = 1; i_28_ < creatureSize; i_28_++) {
						if ((is[i_19_ - 1][i_20_ + i_28_] & 0x4fa40000) != 0 || (is[i_28_ + (i_19_ - 1)][i_20_ + creatureSize] & 0x7e240000) != 0)
							break while_11_;
					}
					CopyOfRouteFinder.queueX[i_17_] = i_11_ - 1;
					CopyOfRouteFinder.queueY[i_17_] = i_12_ + 1;
					directions[i_13_ - 1][i_14_ + 1] = 6;
					i_17_ = i_17_ + 1 & 0xfff;
					distances[i_13_ - 1][i_14_ + 1] = i_21_;
				}
			} while (false);
			if (i_13_ < 128 - creatureSize && i_14_ < -creatureSize + 128 && directions[i_13_ + 1][i_14_ + 1] == 0 && (is[i_19_ + creatureSize][i_20_ + creatureSize] & 0x78240000) == 0) {
				for (int i_29_ = 1; i_29_ < creatureSize; i_29_++) {
					if ((is[i_19_ + i_29_][i_20_ + creatureSize] & 0x7e240000) != 0 || ((is[i_19_ + creatureSize][i_29_ + i_20_] & 0x78e40000) != 0))
						continue while_12_;
				}
				CopyOfRouteFinder.queueX[i_17_] = i_11_ + 1;
				CopyOfRouteFinder.queueY[i_17_] = i_12_ + 1;
				i_17_ = i_17_ + 1 & 0xfff;
				directions[i_13_ + 1][i_14_ + 1] = 12;
				distances[i_13_ + 1][i_14_ + 1] = i_21_;
			}
		}
		CopyOfRouteFinder.FoundY = i_12_;
		CopyOfRouteFinder.FoundX = i_11_;
		return false;
	}
	
	static final boolean findRouteSize2(ClipData clipData, int clipType, int fromMapX, int fromMapY, int toMapX, int toMapY, int targetSizeX, int targetSizeY, int targetFace, int i_13_) {
		int currentMapX = fromMapX;
		int currentMapY = fromMapY;
		int currentArrayOffsetX = 64;
		int currentArrayOffsetY = 64;
		int baseArrayOffsetX = fromMapX - currentArrayOffsetX;
		int baseArrayOffsetY = fromMapY - currentArrayOffsetY;
		directions[currentArrayOffsetX][currentArrayOffsetY] = 99;
		distances[currentArrayOffsetX][currentArrayOffsetY] = 0;
		int queueWrite = 0;
		int queueRead = 0;
		CopyOfRouteFinder.queueX[queueWrite] = currentMapX;
		CopyOfRouteFinder.queueY[queueWrite++] = currentMapY;
		int[][] clip = clipData.clip;
		while (queueWrite != queueRead) {
			currentMapX = CopyOfRouteFinder.queueX[queueRead];
			currentMapY = CopyOfRouteFinder.queueY[queueRead];
			int currentClipMapX = currentMapX - clipData.offsetX;
			int cirrentClipMapY = currentMapY - clipData.offsetY;
			currentArrayOffsetX = currentMapX - baseArrayOffsetX;
			currentArrayOffsetY = currentMapY - baseArrayOffsetY;
			queueRead = queueRead + 1 & 0xfff;
			
			
			if (clipType == -4) { // standart clip type , ignores target and creature size check
				if (toMapX == currentMapX && toMapY == currentMapY) {
					CopyOfRouteFinder.FoundY = currentMapY;
					CopyOfRouteFinder.FoundX = currentMapX;
					return true;
				}
			}
			else if (clipType == -3) {
				if (ClipData.checkMove(currentMapX, currentMapY, toMapX, toMapY, 2, 2, targetSizeX, targetSizeY)) {
					CopyOfRouteFinder.FoundX = currentMapX;
					CopyOfRouteFinder.FoundY = currentMapY;
					return true;
				}
			}
			else if (clipType == -2) {
				if (clipData.checkMove(currentMapX, currentMapY, toMapX, toMapY, 2, 2, targetSizeX, targetSizeY, i_13_)) {
					CopyOfRouteFinder.FoundX = currentMapX;
					CopyOfRouteFinder.FoundY = currentMapY;
					return true;
				}
			}
			else if (clipType == -1) {
				if (clipData.checkMove_(currentMapX, currentMapY, toMapX, toMapY, 2, targetSizeX, targetSizeY, i_13_)) {
					CopyOfRouteFinder.FoundX = currentMapX;
					CopyOfRouteFinder.FoundY = currentMapY;
					return true;
				}
			}
			else if (clipType == 0 || clipType == 1 || clipType == 2 || clipType == 3 || clipType == 9) { // used for walking to objects mostly
				if (clipData.checkMove_(clipType, toMapX, toMapY, currentMapX, currentMapY, targetFace, 2)) {
					CopyOfRouteFinder.FoundY = currentMapY;
					CopyOfRouteFinder.FoundX = currentMapX;
					return true;
				}
			}
			else if (clipData.checkMove(clipType, currentMapX, currentMapY, toMapX, toMapY, 2, targetFace)) {
				CopyOfRouteFinder.FoundX = currentMapX;
				CopyOfRouteFinder.FoundY = currentMapY;
				return true;
			}
			
			int distance = distances[currentArrayOffsetX][currentArrayOffsetY] + 1;
			if (currentArrayOffsetX > 0 && directions[currentArrayOffsetX - 1][currentArrayOffsetY] == 0 && (clip[currentClipMapX - 1][cirrentClipMapY] & 0x43a40000) == 0 && (clip[currentClipMapX - 1][cirrentClipMapY + 1] & 0x4e240000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX - 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY;
				queueWrite = queueWrite + 1 & 0xfff;
				directions[currentArrayOffsetX - 1][currentArrayOffsetY] = 2;
				distances[currentArrayOffsetX - 1][currentArrayOffsetY] = distance;
			}
			if (currentArrayOffsetX < 126 && directions[currentArrayOffsetX + 1][currentArrayOffsetY] == 0 && (clip[currentClipMapX + 2][cirrentClipMapY] & 0x60e40000) == 0 && (clip[currentClipMapX + 2][cirrentClipMapY + 1] & 0x78240000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX + 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY;
				directions[currentArrayOffsetX + 1][currentArrayOffsetY] = 8;
				queueWrite = queueWrite + 1 & 0xfff;
				distances[currentArrayOffsetX + 1][currentArrayOffsetY] = distance;
			}
			if (currentArrayOffsetY > 0 && directions[currentArrayOffsetX][currentArrayOffsetY - 1] == 0 && (clip[currentClipMapX][cirrentClipMapY - 1] & 0x43a40000) == 0 && (clip[currentClipMapX + 1][cirrentClipMapY - 1] & 0x60e40000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY - 1;
				directions[currentArrayOffsetX][currentArrayOffsetY - 1] = 1;
				queueWrite = queueWrite + 1 & 0xfff;
				distances[currentArrayOffsetX][currentArrayOffsetY - 1] = distance;
			}
			if (currentArrayOffsetY < 126 && directions[currentArrayOffsetX][currentArrayOffsetY + 1] == 0 && (clip[currentClipMapX][cirrentClipMapY + 2] & 0x4e240000) == 0 && (clip[currentClipMapX + 1][cirrentClipMapY + 2] & 0x78240000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY + 1;
				directions[currentArrayOffsetX][currentArrayOffsetY + 1] = 4;
				queueWrite = queueWrite + 1 & 0xfff;
				distances[currentArrayOffsetX][currentArrayOffsetY + 1] = distance;
			}
			if (currentArrayOffsetX > 0 && currentArrayOffsetY > 0 && directions[currentArrayOffsetX - 1][currentArrayOffsetY - 1] == 0 && (clip[currentClipMapX - 1][cirrentClipMapY] & 0x4fa40000) == 0 && (clip[currentClipMapX - 1][cirrentClipMapY - 1] & 0x43a40000) == 0 && (clip[currentClipMapX][cirrentClipMapY - 1] & 0x63e40000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX - 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY - 1;
				queueWrite = queueWrite + 1 & 0xfff;
				directions[currentArrayOffsetX - 1][currentArrayOffsetY - 1] = 3;
				distances[currentArrayOffsetX - 1][currentArrayOffsetY - 1] = distance;
			}
			if (currentArrayOffsetX < 126 && currentArrayOffsetY > 0 && directions[currentArrayOffsetX + 1][currentArrayOffsetY - 1] == 0 && (clip[currentClipMapX + 1][cirrentClipMapY - 1] & 0x63e40000) == 0 && (clip[currentClipMapX + 2][cirrentClipMapY - 1] & 0x60e40000) == 0 && (clip[currentClipMapX + 2][cirrentClipMapY] & 0x78e40000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX + 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY - 1;
				directions[currentArrayOffsetX + 1][currentArrayOffsetY - 1] = 9;
				queueWrite = queueWrite + 1 & 0xfff;
				distances[currentArrayOffsetX + 1][currentArrayOffsetY - 1] = distance;
			}
			if (currentArrayOffsetX > 0 && currentArrayOffsetY < 126 && directions[currentArrayOffsetX - 1][currentArrayOffsetY + 1] == 0 && (clip[currentClipMapX - 1][cirrentClipMapY + 1] & 0x4fa40000) == 0 && (clip[currentClipMapX - 1][cirrentClipMapY + 2] & 0x4e240000) == 0 && (clip[currentClipMapX][cirrentClipMapY + 2] & 0x7e240000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX - 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY + 1;
				directions[currentArrayOffsetX - 1][currentArrayOffsetY + 1] = 6;
				queueWrite = queueWrite + 1 & 0xfff;
				distances[currentArrayOffsetX - 1][currentArrayOffsetY + 1] = distance;
			}
			if (currentArrayOffsetX < 126 && currentArrayOffsetY < 126 && directions[currentArrayOffsetX + 1][currentArrayOffsetY + 1] == 0 && (clip[currentClipMapX + 1][cirrentClipMapY + 2] & 0x7e240000) == 0 && (clip[currentClipMapX + 2][cirrentClipMapY + 2] & 0x78240000) == 0 && (clip[currentClipMapX + 2][cirrentClipMapY + 1] & 0x78e40000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX + 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY + 1;
				queueWrite = queueWrite + 1 & 0xfff;
				directions[currentArrayOffsetX + 1][currentArrayOffsetY + 1] = 12;
				distances[currentArrayOffsetX + 1][currentArrayOffsetY + 1] = distance;
			}
		}
		CopyOfRouteFinder.FoundY = currentMapY;
		CopyOfRouteFinder.FoundX = currentMapX;
		return false;
	}

	private static final boolean findRouteSize1(ClipData clipData, int clipType, int fromMapX, int fromMapY, int toMapX, int toMapY, int targetSizeX, int targetSizeY, int targetFace, int i_2_) {
		int currentMapX = fromMapX;
		int currentMapY = fromMapY;
		int currentArrayOffsetX = 64;
		int currentArrayOffsetY = 64;
		int baseArrayOffsetX = fromMapX - currentArrayOffsetX;
		int baseArrayOffsetY = fromMapY - currentArrayOffsetY;
		directions[currentArrayOffsetX][currentArrayOffsetY] = 99;
		distances[currentArrayOffsetX][currentArrayOffsetY] = 0;
		int queueWrite = 0;
		int queueRead = 0;
		CopyOfRouteFinder.queueX[queueWrite] = currentMapX;
		CopyOfRouteFinder.queueY[queueWrite++] = currentMapY;
		int[][] clip = clipData.clip;
		while (queueRead != queueWrite) {
			currentMapX = CopyOfRouteFinder.queueX[queueRead];
			currentMapY = CopyOfRouteFinder.queueY[queueRead];
			int currentClipMapX = currentMapX - clipData.offsetX;
			int currentClipMapY = currentMapY - clipData.offsetY;
			currentArrayOffsetX = currentMapX - baseArrayOffsetX;
			currentArrayOffsetY = currentMapY - baseArrayOffsetY;
			queueRead = queueRead + 1 & 0xfff;
			
			if (clipType == -4) { // standart clip type , ignores target and creature size check
				if (toMapX == currentMapX && toMapY == currentMapY) {
					CopyOfRouteFinder.FoundY = currentMapY;
					CopyOfRouteFinder.FoundX = currentMapX;
					return true;
				}
			}
			else if (clipType == -3) {
				if (ClipData.checkMove(currentMapX, currentMapY, toMapX, toMapY, 1, 1, targetSizeX, targetSizeY)) {
					CopyOfRouteFinder.FoundX = currentMapX;
					CopyOfRouteFinder.FoundY = currentMapY;
					return true;
				}
			}
			else if (clipType == -2) {
				if (clipData.checkMove(currentMapX, currentMapY, toMapX, toMapY, 1, 1, targetSizeX, targetSizeY, i_2_)) {
					CopyOfRouteFinder.FoundX = currentMapX;
					CopyOfRouteFinder.FoundY = currentMapY;
					return true;
				}
			}
			else if (clipType == -1) {
				if (clipData.checkMove_(currentMapX, currentMapY, toMapX, toMapY, 1, targetSizeX, targetSizeY, i_2_)) {
					CopyOfRouteFinder.FoundX = currentMapX;
					CopyOfRouteFinder.FoundY = currentMapY;
					return true;
				}
			}
			else if (clipType == 0 || clipType == 1 || clipType == 2 || clipType == 3 || clipType == 9) { // used for walking to objects mostly
				if (clipData.checkMove_(clipType, toMapX, toMapY, currentMapX, currentMapY, targetFace, 1)) {
					CopyOfRouteFinder.FoundY = currentMapY;
					CopyOfRouteFinder.FoundX = currentMapX;
					return true;
				}
			}
			else if (clipData.checkMove(clipType, currentMapX, currentMapY, toMapX, toMapY, 1, targetFace)) {
				CopyOfRouteFinder.FoundX = currentMapX;
				CopyOfRouteFinder.FoundY = currentMapY;
				return true;
			}
			
			int distance = distances[currentArrayOffsetX][currentArrayOffsetY] + 1;
			if (currentArrayOffsetX > 0 && directions[currentArrayOffsetX - 1][currentArrayOffsetY] == 0 && (clip[currentClipMapX - 1][currentClipMapY] & 0x42240000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX - 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY;
				directions[currentArrayOffsetX - 1][currentArrayOffsetY] = 2;
				queueWrite = queueWrite + 1 & 0xfff;
				distances[currentArrayOffsetX - 1][currentArrayOffsetY] = distance;
			}
			if (currentArrayOffsetX < 127 && directions[currentArrayOffsetX + 1][currentArrayOffsetY] == 0 && (clip[currentClipMapX + 1][currentClipMapY] & 0x60240000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX + 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY;
				directions[currentArrayOffsetX + 1][currentArrayOffsetY] = 8;
				queueWrite = queueWrite + 1 & 0xfff;
				distances[currentArrayOffsetX + 1][currentArrayOffsetY] = distance;
			}
			if (currentArrayOffsetY > 0 && directions[currentArrayOffsetX][currentArrayOffsetY - 1] == 0 && (clip[currentClipMapX][currentClipMapY - 1] & 0x40a40000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY - 1;
				directions[currentArrayOffsetX][currentArrayOffsetY - 1] = 1;
				queueWrite = queueWrite + 1 & 0xfff;
				distances[currentArrayOffsetX][currentArrayOffsetY - 1] = distance;
			}
			if (currentArrayOffsetY < 127 && directions[currentArrayOffsetX][currentArrayOffsetY + 1] == 0 && (clip[currentClipMapX][currentClipMapY + 1] & 0x48240000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY + 1;
				directions[currentArrayOffsetX][currentArrayOffsetY + 1] = 4;
				queueWrite = queueWrite + 1 & 0xfff;
				distances[currentArrayOffsetX][currentArrayOffsetY + 1] = distance;
			}
			if (currentArrayOffsetX > 0 && currentArrayOffsetY > 0 && directions[currentArrayOffsetX - 1][currentArrayOffsetY - 1] == 0 && (clip[currentClipMapX - 1][currentClipMapY - 1] & 0x43a40000) == 0 && (clip[currentClipMapX - 1][currentClipMapY] & 0x42240000) == 0 && (clip[currentClipMapX][currentClipMapY - 1] & 0x40a40000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX - 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY - 1;
				queueWrite = queueWrite + 1 & 0xfff;
				directions[currentArrayOffsetX - 1][currentArrayOffsetY - 1] = 3;
				distances[currentArrayOffsetX - 1][currentArrayOffsetY - 1] = distance;
			}
			if (currentArrayOffsetX < 127 && currentArrayOffsetY > 0 && directions[currentArrayOffsetX + 1][currentArrayOffsetY - 1] == 0 && (clip[currentClipMapX + 1][currentClipMapY - 1] & 0x60e40000) == 0 && (clip[currentClipMapX + 1][currentClipMapY] & 0x60240000) == 0 && (clip[currentClipMapX][currentClipMapY - 1] & 0x40a40000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX + 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY - 1;
				queueWrite = queueWrite + 1 & 0xfff;
				directions[currentArrayOffsetX + 1][currentArrayOffsetY - 1] = 9;
				distances[currentArrayOffsetX + 1][currentArrayOffsetY - 1] = distance;
			}
			if (currentArrayOffsetX > 0 && currentArrayOffsetY < 127 && directions[currentArrayOffsetX - 1][currentArrayOffsetY + 1] == 0 && (clip[currentClipMapX - 1][currentClipMapY + 1] & 0x4e240000) == 0 && (clip[currentClipMapX - 1][currentClipMapY] & 0x42240000) == 0 && (clip[currentClipMapX][currentClipMapY + 1] & 0x48240000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX - 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY + 1;
				queueWrite = queueWrite + 1 & 0xfff;
				directions[currentArrayOffsetX - 1][currentArrayOffsetY + 1] = 6;
				distances[currentArrayOffsetX - 1][currentArrayOffsetY + 1] = distance;
			}
			if (currentArrayOffsetX < 127 && currentArrayOffsetY < 127 && directions[currentArrayOffsetX + 1][currentArrayOffsetY + 1] == 0 && (clip[currentClipMapX + 1][currentClipMapY + 1] & 0x78240000) == 0 && (clip[currentClipMapX + 1][currentClipMapY] & 0x60240000) == 0 && (clip[currentClipMapX][currentClipMapY + 1] & 0x48240000) == 0) {
				CopyOfRouteFinder.queueX[queueWrite] = currentMapX + 1;
				CopyOfRouteFinder.queueY[queueWrite] = currentMapY + 1;
				queueWrite = queueWrite + 1 & 0xfff;
				directions[currentArrayOffsetX + 1][currentArrayOffsetY + 1] = 12;
				distances[currentArrayOffsetX + 1][currentArrayOffsetY + 1] = distance;
			}
		}
		CopyOfRouteFinder.FoundX = currentMapX;
		CopyOfRouteFinder.FoundY = currentMapY;
		return false;
	}

	

	





}
