package net.zaros.client;

import net.zaros.client.configs.objtype.ObjType;

/* Class27 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class27 {
	static int anInt295 = 0;
	static int anInt296;
	static int anInt297;

	static final void method314(boolean bool, String string, int i, int i_8_, byte i_9_, String string_10_, boolean bool_11_) {
		Class63.aClass138_726.discardUnpacked_ = 1;
		string_10_ = string_10_.toLowerCase();
		short[] is = new short[16];
		int i_12_ = -1;
		String string_13_ = null;
		if (i_8_ != -1) {
			ParamType class383 = Class296_Sub22.itemExtraDataDefinitionLoader.list(i_8_);
			if (class383 == null || class383.method4013(-120) == !bool)
				return;
			if (class383.method4013(-123))
				string_13_ = class383.name;
			else
				i_12_ = class383.defaultValue;
		}
		int i_14_ = 0;
		if (i_9_ <= 94)
			anInt297 = 74;
		for (int i_15_ = 0; Class296_Sub39_Sub1.itemDefinitionLoader.size > i_15_; i_15_++) {
			ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_15_);
			if ((!bool_11_ || class158.stockmarket) && class158.certTemplate == -1 && class158.lendTemplate == -1 && class158.boughtTemplate == -1 && class158.opcode96 == 0 && (class158.name.toLowerCase().indexOf(string_10_) != -1)) {
				if (i_8_ != -1) {
					if (bool) {
						if (!string.equals(class158.getStringParam(i_8_, string_13_)))
							continue;
					} else if (i != class158.getIntParam(i_8_, i_12_))
						continue;
				}
				if (i_14_ >= 250) {
					StaticMethods.aShortArray5932 = null;
					Class342.anInt2989 = -1;
					return;
				}
				if (is.length <= i_14_) {
					short[] is_16_ = new short[is.length * 2];
					for (int i_17_ = 0; i_14_ > i_17_; i_17_++)
						is_16_[i_17_] = is[i_17_];
					is = is_16_;
				}
				is[i_14_++] = (short) i_15_;
			}
		}
		Class203.anInt3569 = 0;
		StaticMethods.aShortArray5932 = is;
		Class342.anInt2989 = i_14_;
		String[] strings = new String[Class342.anInt2989];
		for (int i_18_ = 0; i_18_ < Class342.anInt2989; i_18_++)
			strings[i_18_] = (Class296_Sub39_Sub1.itemDefinitionLoader.list(is[i_18_]).name);
		Class304.method3272(true, strings, StaticMethods.aShortArray5932);
		Class63.aClass138_726.clearEntryBuffers((byte) -126);
		Class63.aClass138_726.discardUnpacked_ = 2;
	}
}
