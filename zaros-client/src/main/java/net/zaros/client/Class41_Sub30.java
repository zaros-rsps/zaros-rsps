package net.zaros.client;

/* Class41_Sub30 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub30 extends Class41 {
	private boolean aBoolean3822 = true;
	boolean aBoolean3823 = false;

	final int method521(int i) {
		if (i < 114)
			method527(29);
		return anInt389;
	}

	static final String[] method522(byte i, String string, char c) {
		try {
			int i_0_ = ClotchesLoader.method302(c, (byte) 90, string);
			if (i != 63)
				return null;
			String[] strings = new String[i_0_ + 1];
			int i_1_ = 0;
			int i_2_ = 0;
			for (int i_3_ = 0; i_3_ < i_0_; i_3_++) {
				int i_4_;
				for (i_4_ = i_2_; string.charAt(i_4_) != c; i_4_++) {
					/* empty */
				}
				strings[i_1_++] = string.substring(i_2_, i_4_);
				i_2_ = i_4_ + 1;
			}
			strings[i_0_] = string.substring(i_2_);
			return strings;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, ("wb.J(" + i + ',' + (string != null ? "{...}" : "null") + ',' + c + ')'));
		}
	}

	static final int method523(byte i) {
		if (i <= 121)
			method523((byte) -126);
		return 15;
	}

	final int method380(int i, byte i_5_) {
		if (!aClass296_Sub50_392.method3055(true).method332((byte) 119))
			return 3;
		if (i == 3 && !Class296_Sub51_Sub33.method3179((byte) -106, "jagdx"))
			return 3;
		if (i_5_ != 41)
			aBoolean3823 = true;
		return 2;
	}

	static final void method524(int i, ha_Sub3 var_ha_Sub3) {
		if (i == 128) {
			if (ConfigsRegister.anObject3669 == null) {
				Class123_Sub1_Sub2 class123_sub1_sub2 = new Class123_Sub1_Sub2();
				byte[] is = class123_sub1_sub2.method1064(16, 128, 128, (byte) 17);
				ConfigsRegister.anObject3669 = Class166.wrapBuffer(false, 81, is);
			}
			if (InputStream_Sub2.anObject35 == null) {
				Class123_Sub2_Sub2 class123_sub2_sub2 = new Class123_Sub2_Sub2();
				byte[] is = class123_sub2_sub2.method1077(-24924, 128, 16, 128);
				InputStream_Sub2.anObject35 = Class166.wrapBuffer(false, -120, is);
			}
			Class91 class91 = var_ha_Sub3.aClass91_4137;
			if (class91.method841(i ^ 0x81) && GameType.anObject339 == null) {
				byte[] is = Class241_Sub2_Sub1.method2161(0.6F, 128, 16.0F, 16, new Class226_Sub1(419684), 128, 4.0F, 0.5F, 4.0F, (byte) 60, 8);
				GameType.anObject339 = Class166.wrapBuffer(false, 92, is);
			}
		}
	}

	final void method381(int i, byte i_6_) {
		aBoolean3823 = false;
		anInt389 = i;
		if (i_6_ != -110)
			aBoolean3823 = true;
	}

	final boolean method525(int i) {
		if (i != -25952)
			aBoolean3823 = true;
		if (!aClass296_Sub50_392.method3055(true).method332((byte) -7))
			return false;
		return true;
	}

	final void method386(int i) {
		if (!aClass296_Sub50_392.method3055(true).method332((byte) 114))
			anInt389 = 0;
		if (i != 2)
			method527(-11);
		if (anInt389 < 0 || anInt389 > 5)
			anInt389 = method383((byte) 110);
	}

	Class41_Sub30(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	Class41_Sub30(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final void method526(boolean bool, byte i) {
		aBoolean3822 = bool;
		if (i < 116)
			method383((byte) -47);
	}

	final boolean method527(int i) {
		if (i > -79)
			method521(9);
		return aBoolean3822;
	}

	final int method383(byte i) {
		if (i != 110)
			return 103;
		aBoolean3823 = true;
		if (!aClass296_Sub50_392.method3055(true).method332((byte) -45))
			return 0;
		return 2;
	}
}
