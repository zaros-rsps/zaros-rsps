package net.zaros.client;

/* Class398 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;

final class Class398 implements Runnable {
	boolean aBoolean3321;
	static String osName;
	static String aString3323;
	private Object anObject3324;
	private boolean aBoolean3325;
	FileOnDisk aClass58_3326;
	static String aString3327;
	private Callback_Sub1 aCallback_Sub1_3328;
	private Class257 anObject3329;
	private Class267 anObject3330;
	FileOnDisk aClass58_3331 = null;
	boolean aBoolean3332;
	static String aString3333;
	private static volatile long aLong3334 = 0L;
	private static String aString3335;
	FileOnDisk[] aClass58Array3336;
	static Method aMethod3337;
	private static int anInt3338;
	private static String aString3339;
	FileOnDisk aClass58_3340;
	private Class278 aClass278_3341;
	private Class278 aClass278_3342;
	static String aString3343;
	EventQueue anEventQueue3344;
	private Thread aThread3345;
	private Class201 aClass201_3346;
	static Method aMethod3347;
	private static String aString3348;

	final void method4112(byte i) {
		synchronized (this) {
			if (i >= -44)
				aString3327 = null;
			aBoolean3325 = true;
			this.notifyAll();
		}
		try {
			aThread3345.join();
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
		if (aClass58_3331 != null) {
			try {
				aClass58_3331.close(0);
			} catch (IOException ioexception) {
				/* empty */
			}
		}
		if (aClass58_3326 != null) {
			try {
				aClass58_3326.close(0);
			} catch (IOException ioexception) {
				/* empty */
			}
		}
		if (aClass58Array3336 != null) {
			for (int i_0_ = 0; i_0_ < aClass58Array3336.length; i_0_++) {
				if (aClass58Array3336[i_0_] != null) {
					try {
						aClass58Array3336[i_0_].close(0);
					} catch (IOException ioexception) {
						/* empty */
					}
				}
			}
		}
		if (aClass58_3340 != null) {
			try {
				aClass58_3340.close(0);
			} catch (IOException ioexception) {
				/* empty */
			}
		}
	}

	static final FileOnDisk method4113(int i, String string) {
		int i_1_ = -43 / ((i + 49) / 58);
		return method4127(aString3335, string, anInt3338, (byte) -50);
	}

	private final Class278 method4114(int i, byte i_2_, Object object, int i_3_, int i_4_) {
		Class278 class278 = new Class278();
		class278.anInt2543 = i_4_;
		class278.anInt2545 = i;
		class278.anInt2544 = i_3_;
		class278.anObject2542 = object;
		if (i_2_ != -112)
			return null;
		synchronized (this) {
			if (aClass278_3342 != null) {
				aClass278_3342.aClass278_2541 = class278;
				aClass278_3342 = class278;
			} else
				aClass278_3342 = aClass278_3341 = class278;
			this.notify();
		}
		return class278;
	}

	final void method4115(int i) {
		if (i == 19631)
			aLong3334 = Class72.method771(-128) + 5000L;
	}

	final Object method4116(int i) {
		int i_5_ = -126 / ((25 - i) / 38);
		return anObject3324;
	}

	final Class278 method4117(String string, byte i, boolean bool) {
		if (i < 52)
			method4113(111, null);
		if (bool)
			return method4114(0, (byte) -112, string, 12, 0);
		return method4114(0, (byte) -112, string, 13, 0);
	}

	final Class278 method4118(Class var_class, byte i, String string) {
		int i_6_ = -88 / ((-50 - i) / 61);
		return method4114(0, (byte) -112, new Object[] { var_class, string }, 9, 0);
	}

	final Class278 method4119(String string, Class var_class, Class[] var_classes, boolean bool) {
		if (bool != true)
			aClass58Array3336 = null;
		return method4114(0, (byte) -112, new Object[] { var_class, string, var_classes }, 8, 0);
	}

	final Class278 method4120(byte i) {
		if (i != 3)
			return null;
		return method4114(0, (byte) -112, null, 5, 0);
	}

	final Class278 method4121(int i, int i_7_) {
		if (i < 10)
			anObject3324 = null;
		return method4114(i_7_, (byte) -112, null, 3, 0);
	}

	final Class278 method4122(boolean bool, int i, int i_8_, String string) {
		if (i != 0)
			method4113(-36, null);
		return method4114(i_8_, (byte) -112, string, bool ? 22 : 1, 0);
	}

	final Class278 method4123(Runnable runnable, int i, int i_9_) {
		if (i_9_ != -14396)
			aMethod3347 = null;
		return method4114(i, (byte) -112, runnable, 2, 0);
	}

	final Class278 method4124(int i, Frame frame) {
		if (i <= 100)
			aClass278_3342 = null;
		return method4114(0, (byte) -112, frame, 7, 0);
	}

	final Class278 method4125(String string, int i) {
		if (i != 7)
			aCallback_Sub1_3328 = null;
		return method4114(0, (byte) -112, string, 16, 0);
	}

	final Class278 method4126(int i, int i_10_, Point point, int[] is, Component component, int i_11_) {
		if (i_11_ <= 39)
			return null;
		return method4114(i, (byte) -112, new Object[] { component, is, point }, 17, i_10_);
	}

	private static final FileOnDisk method4127(String string, String string_12_, int i, byte i_13_) {
		String string_14_;
		if (i != 33) {
			if (i == 34)
				string_14_ = ("jagex_" + string + "_preferences" + string_12_ + "_wip.dat");
			else
				string_14_ = "jagex_" + string + "_preferences" + string_12_ + ".dat";
		} else
			string_14_ = "jagex_" + string + "_preferences" + string_12_ + "_rc.dat";
		if (i_13_ != -50)
			return null;
		String[] strings = { "c:/rscache/", "/rscache/", aString3348, "c:/windows/", "c:/winnt/", "c:/", "/tmp/", "" };
		for (int i_15_ = 0; i_15_ < strings.length; i_15_++) {
			String string_16_ = strings[i_15_];
			if (string_16_.length() <= 0 || new File(string_16_).exists()) {
				try {
					FileOnDisk class58 = new FileOnDisk(new File(string_16_, string_14_), "rw", 10000L);
					return class58;
				} catch (Exception exception) {
					/* empty */
				}
			}
		}
		return null;
	}

	final Class278 method4128(int i, int i_17_, int i_18_, int i_19_, byte i_20_) {
		int i_21_ = -27 % ((-42 - i_20_) / 35);
		return method4114((i_18_ << 16) + i, (byte) -112, null, 6, i_19_ + (i_17_ << 16));
	}

	final boolean method4129(File file, byte[] is, int i) {
		try {
			FileOutputStream fileoutputstream = new FileOutputStream(file);
			fileoutputstream.write(is, 0, is.length);
			if (i != -18301)
				aThread3345 = null;
			fileoutputstream.close();
			return true;
		} catch (IOException ioexception) {
			throw new RuntimeException();
		}
	}

	final Class278 method4130(byte i, URL url) {
		if (i != -121)
			aMethod3337 = null;
		return method4114(0, (byte) -112, url, 4, 0);
	}

	public final void run() {
		for (;;) {
			Class278 class278;
			synchronized (this) {
				for (;;) {
					if (aBoolean3325)
						return;
					if (aClass278_3341 != null) {
						class278 = aClass278_3341;
						aClass278_3341 = aClass278_3341.aClass278_2541;
						if (aClass278_3341 == null)
							aClass278_3342 = null;
						break;
					}
					try {
						this.wait();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
			}
			try {
				int i = class278.anInt2544;
				if (i != 1) {
					if (i != 22) {
						if (i != 2) {
							if (i != 4) {
								if (i != 8) {
									if (i != 9) {
										if (i == 18) {
											Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
											class278.anObject2539 = clipboard.getContents(null);
										} else if (i != 19) {
											if (aBoolean3332) {
												if (i != 3) {
													if (i != 21) {
														if (i != 5) {
															if (i != 6) {
																if (i == 7) {
																	if (!aBoolean3321)
																		anObject3330.method2292();
																	else
																		aClass201_3346.method1957((Frame) class278.anObject2542, 8);
																} else if (i != 12) {
																	if (i == 13) {
																		FileOnDisk class58 = method4127("", (String) class278.anObject2542, anInt3338, (byte) -50);
																		class278.anObject2539 = class58;
																	} else if (!aBoolean3332 || i != 14) {
																		if (aBoolean3332 && i == 15) {
																			boolean bool = class278.anInt2545 != 0;
																			Component component = (Component) class278.anObject2542;
																			if (aBoolean3321)
																				aCallback_Sub1_3328.method80(bool, 65535, component);
																			else
																				Class257.class.getDeclaredMethod("method2236", new Class[] { Component.class, Boolean.TYPE }).invoke(anObject3329, new Object[] { component, new Boolean(bool) });
																		} else if (!aBoolean3321 && i == 17) {
																			Object[] objects = (Object[]) class278.anObject2542;
																			anObject3329.method2235((Component) objects[0], (int[]) objects[1], new Integer(class278.anInt2545), new Integer(class278.anInt2543), (Point) objects[2]);
																		} else {
																			if (i != 16)
																				throw new Exception("");
																			try {
																				if (!osName.startsWith("win"))
																					throw new Exception();
																				String string = (String) class278.anObject2542;
																				if (!string.startsWith("http://") && !string.startsWith("https://"))
																					throw new Exception();
																				String string_22_ = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?&=,.%+-_#:/*";
																				for (int i_23_ = 0; i_23_ < string.length(); i_23_++) {
																					if (string_22_.indexOf(string.charAt(i_23_)) == -1)
																						throw new Exception();
																				}
																				Runtime.getRuntime().exec("cmd /c start \"j\" \"" + string + "\"");
																				class278.anObject2539 = null;
																			} catch (Exception exception) {
																				class278.anObject2539 = exception;
																				throw exception;
																			}
																		}
																	} else {
																		int i_24_ = class278.anInt2545;
																		int i_25_ = class278.anInt2543;
																		if (aBoolean3321)
																			aCallback_Sub1_3328.method81(i_24_, i_25_, -123);
																		else
																			anObject3329.method2237(new Integer(i_24_), new Integer(i_25_));
																	}
																} else {
																	FileOnDisk class58 = (method4127(aString3335, ((String) class278.anObject2542), anInt3338, (byte) -50));
																	class278.anObject2539 = class58;
																}
															} else {
																Frame frame = (new Frame("Jagex Full Screen"));
																class278.anObject2539 = frame;
																frame.setResizable(false);
																if (!aBoolean3321)
																	anObject3330.method2291(frame, (new Integer(class278.anInt2545 >>> 16)), (new Integer(class278.anInt2545 & 0xffff)), (new Integer(class278.anInt2543 >> 16)), (new Integer(class278.anInt2543 & 0xffff)));
																else
																	aClass201_3346.method1956(((class278.anInt2543) >> 16), frame, ((class278.anInt2545) >>> 16), (byte) 80, ((class278.anInt2545) & 0xffff), ((class278.anInt2543) & 0xffff));
															}
														} else if (!aBoolean3321)
															class278.anObject2539 = (Class267.class.getMethod("method2294", (new Class[0])).invoke(anObject3330, (new Object[0])));
														else
															class278.anObject2539 = (aClass201_3346.method1958((byte) 107));
													} else {
														if ((Class72.method771(-121)) < aLong3334)
															throw new IOException();
														class278.anObject2539 = InetAddress.getByName((String) (class278.anObject2542)).getAddress();
													}
												} else {
													if (Class72.method771(-126) < aLong3334)
														throw new IOException();
													String string = ((String.valueOf((class278.anInt2545) >> 24 & 0xff)) + "." + (((class278.anInt2545) >> 16) & 0xff) + "." + (((class278.anInt2545) & 0xff37) >> 8) + "." + ((class278.anInt2545) & 0xff));
													class278.anObject2539 = InetAddress.getByName(string).getHostName();
												}
											} else
												throw new Exception("");
										} else {
											Transferable transferable = ((Transferable) class278.anObject2542);
											Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
											clipboard.setContents(transferable, null);
										}
									} else {
										Object[] objects = (Object[]) class278.anObject2542;
										if (aBoolean3332 && ((Class) objects[0]).getClassLoader() == null)
											throw new SecurityException();
										class278.anObject2539 = (((Class) objects[0]).getDeclaredField((String) objects[1]));
									}
								} else {
									Object[] objects = (Object[]) class278.anObject2542;
									if (aBoolean3332 && ((Class) objects[0]).getClassLoader() == null)
										throw new SecurityException();
									class278.anObject2539 = (((Class) objects[0]).getDeclaredMethod((String) objects[1], (Class[]) objects[2]));
								}
							} else {
								if (Class72.method771(-110) < aLong3334)
									throw new IOException();
								class278.anObject2539 = new DataInputStream(((URL) (class278.anObject2542)).openStream());
							}
						} else {
							Thread thread = new Thread((Runnable) class278.anObject2542);
							thread.setDaemon(true);
							thread.start();
							thread.setPriority(class278.anInt2545);
							class278.anObject2539 = thread;
						}
					} else {
						if (Class72.method771(-121) < aLong3334)
							throw new IOException();
						try {
							class278.anObject2539 = Class47.method598(125, (String) class278.anObject2542, class278.anInt2545).method1586(-1);
						} catch (IOException_Sub1 ioexception_sub1) {
							class278.anObject2539 = ioexception_sub1.getMessage();
							throw ioexception_sub1;
						}
					}
				} else {
					if (aLong3334 > Class72.method771(-120))
						throw new IOException();
					class278.anObject2539 = new Socket(InetAddress.getByName((String) (class278.anObject2542)), class278.anInt2545);
				}
				class278.anInt2540 = 1;
			} catch (Throwable throwable) {
				class278.anInt2540 = 2;
			}
			synchronized (class278) {
				class278.notify();
			}
		}
	}

	final boolean method4131(int i) {
		if (i != -1343340912)
			return true;
		if (!aBoolean3332)
			return false;
		if (aBoolean3321) {
			if (aClass201_3346 == null)
				return false;
			return true;
		}
		if (anObject3330 == null)
			return false;
		return true;
	}

	Class398(int i, String string, int i_26_, boolean bool) throws Exception {
		aBoolean3321 = false;
		aClass58_3340 = null;
		aClass278_3341 = null;
		aBoolean3325 = false;
		aClass58_3326 = null;
		aClass278_3342 = null;
		aBoolean3332 = false;
		aString3333 = "1.1";
		aString3335 = string;
		aBoolean3332 = bool;
		anInt3338 = i;
		aString3343 = "Unknown";
		try {
			aString3343 = System.getProperty("java.vendor");
			aString3333 = System.getProperty("java.version");
		} catch (Exception exception) {
			/* empty */
		}
		if (aString3343.toLowerCase().indexOf("microsoft") != -1)
			aBoolean3321 = true;
		try {
			aString3339 = System.getProperty("os.name");
		} catch (Exception exception) {
			aString3339 = "Unknown";
		}
		osName = aString3339.toLowerCase();
		try {
			aString3327 = System.getProperty("os.arch").toLowerCase();
		} catch (Exception exception) {
			aString3327 = "";
		}
		try {
			aString3323 = System.getProperty("os.version").toLowerCase();
		} catch (Exception exception) {
			aString3323 = "";
		}
		try {
			aString3348 = System.getProperty("user.home");
			if (aString3348 != null)
				aString3348 += "/";
		} catch (Exception exception) {
			/* empty */
		}
		if (aString3348 == null)
			aString3348 = "~/";
		try {
			anEventQueue3344 = Toolkit.getDefaultToolkit().getSystemEventQueue();
		} catch (Throwable throwable) {
			/* empty */
		}
		if (!aBoolean3321) {
			try {
				aMethod3347 = (java.awt.Component.class.getDeclaredMethod("setFocusTraversalKeysEnabled", new Class[] { Boolean.TYPE }));
			} catch (Exception exception) {
				/* empty */
			}
			try {
				aMethod3337 = (java.awt.Container.class.getDeclaredMethod("setFocusCycleRoot", new Class[] { Boolean.TYPE }));
			} catch (Exception exception) {
				/* empty */
			}
		}
		Class19.method283(aString3335, (byte) -122, anInt3338);
		if (aBoolean3332) {
			aClass58_3340 = new FileOnDisk(Class19.method281("random.dat", anInt3338, null, -107), "rw", 25L);
			aClass58_3331 = new FileOnDisk(Class19.method282("main_file_cache.dat2", -2), "rw", 314572800L);
			aClass58_3326 = new FileOnDisk(Class19.method282("main_file_cache.idx255", -2), "rw", 1048576L);
			aClass58Array3336 = new FileOnDisk[i_26_];
			for (int i_27_ = 0; i_27_ < i_26_; i_27_++)
				aClass58Array3336[i_27_] = new FileOnDisk(Class19.method282(("main_file_cache.idx" + i_27_), -2), "rw", 1048576L);
			if (aBoolean3321) {
				try {
					anObject3324 = Class214.class.newInstance();
				} catch (Throwable throwable) {
					/* empty */
				}
			}
			try {
				if (!aBoolean3321)
					anObject3330 = new Class267();
				else
					aClass201_3346 = new Class201();
			} catch (Throwable throwable) {
				/* empty */
			}
			try {
				if (aBoolean3321)
					aCallback_Sub1_3328 = new Callback_Sub1();
				else
					anObject3329 = Class257.class.newInstance();
			} catch (Throwable throwable) {
				/* empty */
			}
		}
		if (aBoolean3332 && !aBoolean3321) {
			ThreadGroup threadgroup = Thread.currentThread().getThreadGroup();
			for (ThreadGroup threadgroup_28_ = threadgroup.getParent(); threadgroup_28_ != null; threadgroup_28_ = threadgroup.getParent())
				threadgroup = threadgroup_28_;
			Thread[] threads = new Thread[1000];
			threadgroup.enumerate(threads);
			for (int i_29_ = 0; i_29_ < threads.length; i_29_++) {
				if (threads[i_29_] != null && threads[i_29_].getName().startsWith("AWT"))
					threads[i_29_].setPriority(1);
			}
		}
		aBoolean3325 = false;
		aThread3345 = new Thread(this);
		aThread3345.setPriority(10);
		aThread3345.setDaemon(true);
		aThread3345.start();
	}
}
