package net.zaros.client;
/* Class296_Sub45_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub45_Sub3 extends Class296_Sub45 {
	NodeDeque aClass155_6289 = new NodeDeque();
	private Class296_Sub45_Sub4 aClass296_Sub45_Sub4_6290;
	static Class209 aClass209_6291 = new Class209(42);
	Class296_Sub45_Sub5 aClass296_Sub45_Sub5_6292 = new Class296_Sub45_Sub5();
	static Class108 aClass108_6293 = new Class108();
	static int anInt6294;
	static Class25 aClass25_6295 = null;

	private final void method2989(Class296_Sub4 class296_sub4, int i, int[] is, int i_0_, int i_1_, byte i_2_) {
		if (i_2_ != -25)
			method2989(null, -28, null, -22, 102, (byte) 126);
		if ((aClass296_Sub45_Sub4_6290.anIntArray6304[class296_sub4.anInt4597] & 0x4) != 0 && class296_sub4.anInt4617 < 0) {
			int i_3_ = ((aClass296_Sub45_Sub4_6290.anIntArray6308[class296_sub4.anInt4597]) / Class298.anInt2699);
			for (;;) {
				int i_4_ = (i_3_ + 1048575 - class296_sub4.anInt4619) / i_3_;
				if (i_4_ > i_1_)
					break;
				class296_sub4.aClass296_Sub45_Sub1_4607.method2934(is, i, i_4_);
				i_1_ -= i_4_;
				i += i_4_;
				class296_sub4.anInt4619 += i_4_ * i_3_ - 1048576;
				int i_5_ = Class298.anInt2699 / 100;
				int i_6_ = 262144 / i_3_;
				if (i_6_ < i_5_)
					i_5_ = i_6_;
				Class296_Sub45_Sub1 class296_sub45_sub1 = class296_sub4.aClass296_Sub45_Sub1_4607;
				if ((aClass296_Sub45_Sub4_6290.anIntArray6315[class296_sub4.anInt4597]) != 0) {
					class296_sub4.aClass296_Sub45_Sub1_4607 = (Class296_Sub45_Sub1.method2970(class296_sub4.aClass296_Sub19_Sub1_4605, class296_sub45_sub1.method2974(), 0, class296_sub45_sub1.method2954()));
					aClass296_Sub45_Sub4_6290.method3012((class296_sub4.aClass296_Sub41_4610.aShortArray4918[(class296_sub4.anInt4608)]) < 0, class296_sub4, i_2_ ^ ~0x3532);
					class296_sub4.aClass296_Sub45_Sub1_4607.method2946(i_5_, class296_sub45_sub1.method2957());
				} else
					class296_sub4.aClass296_Sub45_Sub1_4607 = (Class296_Sub45_Sub1.method2970(class296_sub4.aClass296_Sub19_Sub1_4605, class296_sub45_sub1.method2974(), class296_sub45_sub1.method2957(), class296_sub45_sub1.method2954()));
				if ((class296_sub4.aClass296_Sub41_4610.aShortArray4918[class296_sub4.anInt4608]) < 0)
					class296_sub4.aClass296_Sub45_Sub1_4607.method2960(-1);
				class296_sub45_sub1.method2967(i_5_);
				class296_sub45_sub1.method2934(is, i, -i + i_0_);
				if (class296_sub45_sub1.method2971())
					aClass296_Sub45_Sub5_6292.method3035(class296_sub45_sub1);
			}
			class296_sub4.anInt4619 += i_1_ * i_3_;
		}
		class296_sub4.aClass296_Sub45_Sub1_4607.method2934(is, i, i_1_);
	}

	final Class296_Sub45 method2932() {
		Class296_Sub4 class296_sub4;
		do {
			class296_sub4 = (Class296_Sub4) aClass155_6289.removeNext(1001);
			if (class296_sub4 == null)
				return null;
		} while (class296_sub4.aClass296_Sub45_Sub1_4607 == null);
		return class296_sub4.aClass296_Sub45_Sub1_4607;
	}

	final void method2933(int i) {
		aClass296_Sub45_Sub5_6292.method2933(i);
		while_238_ : for (Class296_Sub4 class296_sub4 = (Class296_Sub4) aClass155_6289.removeFirst((byte) 127); class296_sub4 != null; class296_sub4 = (Class296_Sub4) aClass155_6289.removeNext(1001)) {
			if (!aClass296_Sub45_Sub4_6290.method3015(class296_sub4, true)) {
				int i_7_ = i;
				while (i_7_ > class296_sub4.anInt4614) {
					method2990(-1, class296_sub4.anInt4614, class296_sub4);
					i_7_ -= class296_sub4.anInt4614;
					if (aClass296_Sub45_Sub4_6290.method3026(0, (byte) -119, i_7_, class296_sub4, null))
						continue while_238_;
				}
				method2990(-1, i_7_, class296_sub4);
				class296_sub4.anInt4614 -= i_7_;
			}
		}
	}

	final int method2929() {
		return 0;
	}

	final void method2934(int[] is, int i, int i_8_) {
		aClass296_Sub45_Sub5_6292.method2934(is, i, i_8_);
		while_240_ : for (Class296_Sub4 class296_sub4 = (Class296_Sub4) aClass155_6289.removeFirst((byte) 122); class296_sub4 != null; class296_sub4 = (Class296_Sub4) aClass155_6289.removeNext(1001)) {
			if (!aClass296_Sub45_Sub4_6290.method3015(class296_sub4, true)) {
				int i_9_ = i_8_;
				int i_10_ = i;
				while (i_9_ > class296_sub4.anInt4614) {
					method2989(class296_sub4, i_10_, is, i_9_ + i_10_, class296_sub4.anInt4614, (byte) -25);
					i_9_ -= class296_sub4.anInt4614;
					i_10_ += class296_sub4.anInt4614;
					if (aClass296_Sub45_Sub4_6290.method3026(i_10_, (byte) -119, i_9_, class296_sub4, is))
						continue while_240_;
				}
				method2989(class296_sub4, i_10_, is, i_10_ + i_9_, i_9_, (byte) -25);
				class296_sub4.anInt4614 -= i_9_;
			}
		}
	}

	private final void method2990(int i, int i_11_, Class296_Sub4 class296_sub4) {
		if (i != -1)
			aClass209_6291 = null;
		if ((aClass296_Sub45_Sub4_6290.anIntArray6304[class296_sub4.anInt4597] & 0x4) != 0 && class296_sub4.anInt4617 < 0) {
			int i_12_ = ((aClass296_Sub45_Sub4_6290.anIntArray6308[class296_sub4.anInt4597]) / Class298.anInt2699);
			int i_13_ = (-class296_sub4.anInt4619 + i_12_ + 1048575) / i_12_;
			class296_sub4.anInt4619 = i_11_ * i_12_ + class296_sub4.anInt4619 & 0xfffff;
			if (i_11_ >= i_13_) {
				if ((aClass296_Sub45_Sub4_6290.anIntArray6315[class296_sub4.anInt4597]) == 0)
					class296_sub4.aClass296_Sub45_Sub1_4607 = (Class296_Sub45_Sub1.method2970(class296_sub4.aClass296_Sub19_Sub1_4605, class296_sub4.aClass296_Sub45_Sub1_4607.method2974(), class296_sub4.aClass296_Sub45_Sub1_4607.method2957(), class296_sub4.aClass296_Sub45_Sub1_4607.method2954()));
				else {
					class296_sub4.aClass296_Sub45_Sub1_4607 = (Class296_Sub45_Sub1.method2970(class296_sub4.aClass296_Sub19_Sub1_4605, class296_sub4.aClass296_Sub45_Sub1_4607.method2974(), 0, class296_sub4.aClass296_Sub45_Sub1_4607.method2954()));
					aClass296_Sub45_Sub4_6290.method3012((class296_sub4.aClass296_Sub41_4610.aShortArray4918[(class296_sub4.anInt4608)]) < 0, class296_sub4, i ^ ~0x352a);
				}
				if ((class296_sub4.aClass296_Sub41_4610.aShortArray4918[class296_sub4.anInt4608]) < 0)
					class296_sub4.aClass296_Sub45_Sub1_4607.method2960(-1);
				i_11_ = class296_sub4.anInt4619 / i_12_;
			}
		}
		class296_sub4.aClass296_Sub45_Sub1_4607.method2933(i_11_);
	}

	final Class296_Sub45 method2930() {
		Class296_Sub4 class296_sub4 = (Class296_Sub4) aClass155_6289.removeFirst((byte) 116);
		if (class296_sub4 == null)
			return null;
		if (class296_sub4.aClass296_Sub45_Sub1_4607 != null)
			return class296_sub4.aClass296_Sub45_Sub1_4607;
		return method2932();
	}

	public static void method2991(int i) {
		aClass108_6293 = null;
		aClass209_6291 = null;
		aClass25_6295 = null;
		if (i != -1)
			method2991(-90);
	}

	Class296_Sub45_Sub3(Class296_Sub45_Sub4 class296_sub45_sub4) {
		aClass296_Sub45_Sub4_6290 = class296_sub45_sub4;
	}
}
