package net.zaros.client;

/* Class296_Sub39_Sub20 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class296_Sub39_Sub20 extends Queuable {
	boolean aBoolean6253;
	static int anInt6254;
	static int anInt6255 = 0;
	volatile boolean aBoolean6256 = true;
	boolean aBoolean6257;

	abstract int method2902(int i);

	static final void method2903(int i, Mobile class338_sub3_sub1_sub3, boolean bool) {
		int i_0_ = -1;
		if (i <= 78)
			method2903(-95, null, true);
		int i_1_ = 0;
		if (class338_sub3_sub1_sub3.anInt6809 > Class29.anInt307)
			ReferenceTable.method838((byte) 54, class338_sub3_sub1_sub3);
		else if (class338_sub3_sub1_sub3.anInt6807 >= Class29.anInt307)
			Class296_Sub51_Sub21.method3131(0, class338_sub3_sub1_sub3);
		else {
			Class71.method768(class338_sub3_sub1_sub3, (byte) 104, bool);
			i_0_ = Class41_Sub5.anInt3757;
			i_1_ = Class45.anInt438;
		}
		if (class338_sub3_sub1_sub3.tileX < 512 || class338_sub3_sub1_sub3.tileY < 512 || (class338_sub3_sub1_sub3.tileX >= Class198.currentMapSizeX * 512 - 512) || ((Class296_Sub38.currentMapSizeY - 1) * 512 <= class338_sub3_sub1_sub3.tileY)) {
			class338_sub3_sub1_sub3.aClass44_6802.method549((byte) 115, -1);
			for (int i_2_ = 0; i_2_ < class338_sub3_sub1_sub3.aClass212Array6817.length; i_2_++) {
				class338_sub3_sub1_sub3.aClass212Array6817[i_2_].anInt2105 = -1;
				class338_sub3_sub1_sub3.aClass212Array6817[i_2_].aClass44_2108.method549((byte) 115, -1);
			}
			class338_sub3_sub1_sub3.anInt6807 = 0;
			i_0_ = -1;
			class338_sub3_sub1_sub3.anIntArray6789 = null;
			i_1_ = 0;
			class338_sub3_sub1_sub3.anInt6809 = 0;
			class338_sub3_sub1_sub3.tileX = (class338_sub3_sub1_sub3.waypointQueueX[0] * 512 + class338_sub3_sub1_sub3.getSize() * 256);
			class338_sub3_sub1_sub3.tileY = (class338_sub3_sub1_sub3.wayPointQueueY[0] * 512 + class338_sub3_sub1_sub3.getSize() * 256);
			class338_sub3_sub1_sub3.method3498((byte) 117);
		}
		if ((Class296_Sub51_Sub11.localPlayer == class338_sub3_sub1_sub3) && (class338_sub3_sub1_sub3.tileX < 6144 || class338_sub3_sub1_sub3.tileY < 6144 || (class338_sub3_sub1_sub3.tileX >= Class198.currentMapSizeX * 512 - 6144) || (class338_sub3_sub1_sub3.tileY >= Class296_Sub38.currentMapSizeY * 512 - 6144))) {
			class338_sub3_sub1_sub3.aClass44_6802.method549((byte) 115, -1);
			for (int i_3_ = 0; class338_sub3_sub1_sub3.aClass212Array6817.length > i_3_; i_3_++) {
				class338_sub3_sub1_sub3.aClass212Array6817[i_3_].anInt2105 = -1;
				class338_sub3_sub1_sub3.aClass212Array6817[i_3_].aClass44_2108.method549((byte) 115, -1);
			}
			class338_sub3_sub1_sub3.anIntArray6789 = null;
			class338_sub3_sub1_sub3.anInt6809 = 0;
			class338_sub3_sub1_sub3.anInt6807 = 0;
			i_1_ = 0;
			i_0_ = -1;
			class338_sub3_sub1_sub3.tileX = (class338_sub3_sub1_sub3.waypointQueueX[0] * 512 + class338_sub3_sub1_sub3.getSize() * 256);
			class338_sub3_sub1_sub3.tileY = (class338_sub3_sub1_sub3.wayPointQueueY[0] * 512 + class338_sub3_sub1_sub3.getSize() * 256);
			class338_sub3_sub1_sub3.method3498((byte) 112);
		}
		int i_4_ = Class368_Sub4.method3822(class338_sub3_sub1_sub3, (byte) -67);
		Class41_Sub10.method431(-1073741824, class338_sub3_sub1_sub3);
		Class123_Sub1_Sub2.method1065(i_1_, 0, i_4_, i_0_, class338_sub3_sub1_sub3);
		Class397_Sub3.method4110((byte) 1, i_0_, class338_sub3_sub1_sub3);
		MaterialRaw.method1674(class338_sub3_sub1_sub3, (byte) -128);
	}

	abstract byte[] method2904(int i);

	public Class296_Sub39_Sub20() {
		/* empty */
	}
}
