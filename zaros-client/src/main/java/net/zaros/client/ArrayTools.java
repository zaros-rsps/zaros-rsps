package net.zaros.client;

/**
 * @author Walied K. Yassen
 */
public class ArrayTools {

	public static void quicksort(String[] strings, int[] is) {
		Class368_Sub20.quicksort(0, strings.length - 1, false, strings, is);
	}

	public static void removeElement(long[] ls, int i, long[] ls_15_, int i_16_, int i_17_) {
		if (ls == ls_15_) {
			if (i == i_16_) {
				return;
			}
			if (i_16_ > i && i_16_ < i + i_17_) {
				i_17_--;
				i += i_17_;
				i_16_ += i_17_;
				i_17_ = i - i_17_;
				i_17_ += 3;
				while (i >= i_17_) {
					ls_15_[i_16_--] = ls[i--];
					ls_15_[i_16_--] = ls[i--];
					ls_15_[i_16_--] = ls[i--];
					ls_15_[i_16_--] = ls[i--];
				}
				i_17_ -= 3;
				while (i >= i_17_) {
					ls_15_[i_16_--] = ls[i--];
				}
				return;
			}
		}
		i_17_ += i;
		i_17_ -= 3;
		while (i < i_17_) {
			ls_15_[i_16_++] = ls[i++];
			ls_15_[i_16_++] = ls[i++];
			ls_15_[i_16_++] = ls[i++];
			ls_15_[i_16_++] = ls[i++];
		}
		i_17_ += 3;
		while (i < i_17_) {
			ls_15_[i_16_++] = ls[i++];
		}
	}

	public static void removeElement(Object[] objects, int i, Object[] objects_0_, int i_1_, int i_2_) {
		if (objects == objects_0_) {
			if (i == i_1_) {
				return;
			}
			if (i_1_ > i && i_1_ < i + i_2_) {
				i_2_--;
				i += i_2_;
				i_1_ += i_2_;
				i_2_ = i - i_2_;
				i_2_ += 7;
				while (i >= i_2_) {
					objects_0_[i_1_--] = objects[i--];
					objects_0_[i_1_--] = objects[i--];
					objects_0_[i_1_--] = objects[i--];
					objects_0_[i_1_--] = objects[i--];
					objects_0_[i_1_--] = objects[i--];
					objects_0_[i_1_--] = objects[i--];
					objects_0_[i_1_--] = objects[i--];
					objects_0_[i_1_--] = objects[i--];
				}
				i_2_ -= 7;
				while (i >= i_2_) {
					objects_0_[i_1_--] = objects[i--];
				}
				return;
			}
		}
		i_2_ += i;
		i_2_ -= 7;
		while (i < i_2_) {
			objects_0_[i_1_++] = objects[i++];
			objects_0_[i_1_++] = objects[i++];
			objects_0_[i_1_++] = objects[i++];
			objects_0_[i_1_++] = objects[i++];
			objects_0_[i_1_++] = objects[i++];
			objects_0_[i_1_++] = objects[i++];
			objects_0_[i_1_++] = objects[i++];
			objects_0_[i_1_++] = objects[i++];
		}
		i_2_ += 7;
		while (i < i_2_) {
			objects_0_[i_1_++] = objects[i++];
		}
	}

	public static void removeElement(byte[] is, int i, byte[] is_7_, int i_8_, int i_9_) {
		if (is == is_7_) {
			if (i == i_8_) {
				return;
			}
			if (i_8_ > i && i_8_ < i + i_9_) {
				i_9_--;
				i += i_9_;
				i_8_ += i_9_;
				i_9_ = i - i_9_;
				i_9_ += 7;
				while (i >= i_9_) {
					is_7_[i_8_--] = is[i--];
					is_7_[i_8_--] = is[i--];
					is_7_[i_8_--] = is[i--];
					is_7_[i_8_--] = is[i--];
					is_7_[i_8_--] = is[i--];
					is_7_[i_8_--] = is[i--];
					is_7_[i_8_--] = is[i--];
					is_7_[i_8_--] = is[i--];
				}
				i_9_ -= 7;
				while (i >= i_9_) {
					is_7_[i_8_--] = is[i--];
				}
				return;
			}
		}
		i_9_ += i;
		i_9_ -= 7;
		while (i < i_9_) {
			is_7_[i_8_++] = is[i++];
			is_7_[i_8_++] = is[i++];
			is_7_[i_8_++] = is[i++];
			is_7_[i_8_++] = is[i++];
			is_7_[i_8_++] = is[i++];
			is_7_[i_8_++] = is[i++];
			is_7_[i_8_++] = is[i++];
			is_7_[i_8_++] = is[i++];
		}
		i_9_ += 7;
		while (i < i_9_) {
			is_7_[i_8_++] = is[i++];
		}
	}

	public static void removeElements(int[] is, int i, int[] is_18_, int i_19_, int i_20_) {
		if (is == is_18_) {
			if (i == i_19_) {
				return;
			}
			if (i_19_ > i && i_19_ < i + i_20_) {
				i_20_--;
				i += i_20_;
				i_19_ += i_20_;
				i_20_ = i - i_20_;
				i_20_ += 7;
				while (i >= i_20_) {
					is_18_[i_19_--] = is[i--];
					is_18_[i_19_--] = is[i--];
					is_18_[i_19_--] = is[i--];
					is_18_[i_19_--] = is[i--];
					is_18_[i_19_--] = is[i--];
					is_18_[i_19_--] = is[i--];
					is_18_[i_19_--] = is[i--];
					is_18_[i_19_--] = is[i--];
				}
				i_20_ -= 7;
				while (i >= i_20_) {
					is_18_[i_19_--] = is[i--];
				}
				return;
			}
		}
		i_20_ += i;
		i_20_ -= 7;
		while (i < i_20_) {
			is_18_[i_19_++] = is[i++];
			is_18_[i_19_++] = is[i++];
			is_18_[i_19_++] = is[i++];
			is_18_[i_19_++] = is[i++];
			is_18_[i_19_++] = is[i++];
			is_18_[i_19_++] = is[i++];
			is_18_[i_19_++] = is[i++];
			is_18_[i_19_++] = is[i++];
		}
		i_20_ += 7;
		while (i < i_20_) {
			is_18_[i_19_++] = is[i++];
		}
	}

}
