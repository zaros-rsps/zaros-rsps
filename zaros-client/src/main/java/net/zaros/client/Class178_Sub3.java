package net.zaros.client;

/* Class178_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.File;

import jaclib.memory.Buffer;
import jaclib.memory.Stream;

public class Class178_Sub3 extends Model {
	private short[] aShortArray4451;
	private int anInt4452;
	private short[] aShortArray4453;
	private int[][] anIntArrayArray4454;
	private short[] aShortArray4455;
	private boolean aBoolean4456 = true;
	private short aShort4457;
	private boolean aBoolean4458;
	private Class271[] aClass271Array4459;
	private short[] aShortArray4460;
	private short[] aShortArray4461;
	private float[] aFloatArray4462;
	private byte[] aByteArray4463;
	private short[] aShortArray4464;
	private int[] anIntArray4465;
	private float[] aFloatArray4466;
	private Class307 aClass307_4467;
	private int anInt4468;
	private int anInt4469;
	static IncomingPacket aClass231_4470 = new IncomingPacket(59, 0);
	private Class307 aClass307_4471;
	private int[] anIntArray4472;
	private Class223 aClass223_4473;
	private int anInt4474 = 0;
	private int[][] anIntArrayArray4475;
	private short aShort4476;
	private short[] aShortArray4477;
	private int anInt4478;
	private int[][] anIntArrayArray4479;
	private Class307 aClass307_4480;
	private int anInt4481;
	private int[] anIntArray4482;
	private short[] aShortArray4483;
	private short[] aShortArray4484;
	private int anInt4485;
	private Class307 aClass307_4486;
	private EmissiveTriangle[] aClass89Array4487;
	private short[] aShortArray4488;
	private int[] anIntArray4489;
	private int anInt4490;
	private int[] anIntArray4491;
	private ha_Sub1 aHa_Sub1_4492;
	private int anInt4493;
	private int anInt4494;
	private int anInt4495;
	private boolean aBoolean4496;
	private int anInt4497;
	private int anInt4498;
	private boolean aBoolean4499;
	private int anInt4500;
	private Class293 aClass293_4501;
	private boolean aBoolean4502;
	private int anInt4503;
	private EffectiveVertex[] aClass232Array4504;
	private int[] anIntArray4505;
	private byte[] aByteArray4506;
	private short[] aShortArray4507;
	private int anInt4508;
	private short[] aShortArray4509;
	private Class37[] aClass37Array4510;
	private int[] anIntArray4511;
	private short[] aShortArray4512;

	public void C(int i) {
		aShort4476 = (short) i;
		method1796(1018);
	}

	static public byte[] method1794(byte i, File file) {
		if (i != 85)
			return null;
		return Node.method2431(i ^ 0x54, file, (int) file.length());
	}

	public void method1795(int i) {
		if (i != -8)
			p(12, 4, null, null, 67, 9, 87);
		if (aClass307_4467 != null)
			aClass307_4467.method3286(1);
		if (aClass307_4471 != null)
			aClass307_4471.method3286(1);
		if (aClass307_4486 != null)
			aClass307_4486.method3286(i ^ ~0x6);
		if (aClass307_4480 != null)
			aClass307_4480.method3286(1);
		if (aClass293_4501 != null)
			aClass293_4501.method2417((byte) -94);
	}

	private void method1796(int i) {
		if (aClass307_4486 != null)
			aClass307_4486.aBoolean2750 = false;
		if (i != 1018)
			ma();
	}

	public boolean F() {
		return aBoolean4499;
	}

	public boolean method1732(int i, int i_0_, Class373 class373, boolean bool, int i_1_) {
		return method1802(i, bool, i_1_, class373, (byte) -105, -1, i_0_);
	}

	public int G() {
		if (!aBoolean4496)
			method1805((byte) -113);
		return anInt4485;
	}

	public void ia(short i, short i_2_) {
		for (int i_3_ = 0; i_3_ < anInt4508; i_3_++) {
			if (i == aShortArray4477[i_3_])
				aShortArray4477[i_3_] = i_2_;
		}
		if (aClass37Array4510 != null) {
			for (int i_4_ = 0; i_4_ < anInt4490; i_4_++) {
				Class37 class37 = aClass37Array4510[i_4_];
				Class271 class271 = aClass271Array4459[i_4_];
				class271.anInt2517 = (class271.anInt2517 & ~0xffffff
						| ((Class166_Sub1.anIntArray4300[aShortArray4477[class37.anInt367] & 0xffff]) & 0xffffff));
			}
		}
		method1796(1018);
	}

	public void s(int i) {
		if (aClass307_4467 != null)
			aClass307_4467.aBoolean2747 = Class296_Sub51_Sub26.method3150(i, (byte) 119, anInt4481);
		if (aClass307_4471 != null)
			aClass307_4471.aBoolean2747 = Class16_Sub1_Sub1.method239(0, i, anInt4481);
		if (aClass307_4486 != null)
			aClass307_4486.aBoolean2747 = Class184.method1857(anInt4481, i, (byte) -109);
		if (aClass307_4480 != null)
			aClass307_4480.aBoolean2747 = Class47.method599((byte) -46, i, anInt4481);
		anInt4500 = i;
		if (aClass223_4473 != null && (anInt4500 & 0x10000) == 0) {
			aShortArray4483 = aClass223_4473.aShortArray2163;
			aByteArray4506 = aClass223_4473.aByteArray2166;
			aShortArray4507 = aClass223_4473.aShortArray2165;
			aShortArray4484 = aClass223_4473.aShortArray2164;
			aClass223_4473 = null;
		}
		aBoolean4456 = true;
		method1807(29055);
	}

	Class178_Sub3(ha_Sub1 var_ha_Sub1, Mesh class132, int i, int i_5_, int i_6_, int i_7_) {
		this(var_ha_Sub1, i, i_7_, true, false);
		d var_d = var_ha_Sub1.aD1299;
		anIntArray4472 = new int[class132.highest_face_index + 1];
		int[] is = new int[class132.num_faces];
		for (int i_8_ = 0; class132.num_faces > i_8_; i_8_++) {
			if (class132.aByteArray1354 == null || class132.aByteArray1354[i_8_] != 2) {
				if (class132.faces_material != null && class132.faces_material[i_8_] != -1) {
					MaterialRaw class170 = var_d.method14((class132.faces_material[i_8_] & 0xffff), -9412);
					if (((anInt4481 & 0x40) == 0 || !class170.aBoolean1792) && class170.aBoolean1786)
						continue;
				}
				is[anInt4508++] = i_8_;
				anIntArray4472[class132.faces_a[i_8_]]++;
				anIntArray4472[class132.faces_b[i_8_]]++;
				anIntArray4472[class132.faceS_c[i_8_]]++;
			}
		}
		anInt4493 = anInt4508;
		long[] ls = new long[anInt4508];
		boolean bool = (anInt4500 & 0x100) != 0;
		for (int i_9_ = 0; i_9_ < anInt4508; i_9_++) {
			int i_10_ = is[i_9_];
			MaterialRaw class170 = null;
			int i_11_ = 0;
			int i_12_ = 0;
			int i_13_ = 0;
			int i_14_ = 0;
			if (class132.billboards != null) {
				boolean bool_15_ = false;
				for (int i_16_ = 0; class132.billboards.length > i_16_; i_16_++) {
					Billboard class385 = class132.billboards[i_16_];
					if (i_10_ == class385.face) {
						BillboardRaw class101 = Class296_Sub51_Sub4.method3086((byte) 103, class385.id);
						if (class101.aBoolean1077)
							bool_15_ = true;
						if (class101.texture_id != -1) {
							MaterialRaw class170_17_ = var_d.method14(class101.texture_id, -9412);
							if (class170_17_.anInt1788 == 2)
								aBoolean4499 = true;
						}
					}
				}
				if (bool_15_) {
					ls[i_9_] = 9223372036854775807L;
					anInt4493--;
					continue;
				}
			}
			int i_18_ = -1;
			if (class132.faces_material != null) {
				i_18_ = class132.faces_material[i_10_];
				if (i_18_ != -1) {
					class170 = var_d.method14(i_18_ & 0xffff, -9412);
					if ((anInt4481 & 0x40) == 0 || !class170.aBoolean1792) {
						i_13_ = class170.aByte1774;
						i_14_ = class170.aByte1784;
					} else {
						i_18_ = -1;
						class170 = null;
					}
				}
			}
			boolean bool_19_ = ((class132.faces_alpha != null && class132.faces_alpha[i_10_] != 0)
					|| class170 != null && class170.anInt1788 != 0);
			if ((bool || bool_19_) && class132.faces_priority != null)
				i_11_ += class132.faces_priority[i_10_] << 17;
			if (bool_19_)
				i_11_ += 65536;
			i_11_ += i_13_ << 8 & 0xff00;
			i_11_ += i_14_ & 0xff;
			i_12_ += i_18_ << 16 & ~0xffff;
			i_12_ += i_9_ & 0xffff;
			ls[i_9_] = (long) i_12_ + ((long) i_11_ << 32);
			Class178_Sub3 class178_sub3_20_ = this;
			class178_sub3_20_.aBoolean4502 = (class178_sub3_20_.aBoolean4502
					| (class170 != null && (class170.speed_u != 0 || class170.speed_v != 0)));
			aBoolean4499 |= bool_19_;
		}
		Connection.method1965(ls, -19851, is);
		anIntArray4482 = class132.vertices_x;
		anIntArray4505 = class132.vertices_y;
		anInt4468 = class132.highest_face_index;
		anInt4478 = class132.num_vertices;
		aShortArray4453 = class132.aShortArray1366;
		anIntArray4489 = class132.vertices_z;
		aClass232Array4504 = class132.effectors;
		aClass89Array4487 = class132.emitters;
		Class348[] class348s = new Class348[anInt4468];
		if (class132.billboards != null) {
			anInt4490 = class132.billboards.length;
			aClass37Array4510 = new Class37[anInt4490];
			aClass271Array4459 = new Class271[anInt4490];
			for (int i_21_ = 0; i_21_ < anInt4490; i_21_++) {
				Billboard class385 = class132.billboards[i_21_];
				BillboardRaw class101 = Class296_Sub51_Sub4.method3086((byte) -103, class385.id);
				int i_22_ = -1;
				for (int i_23_ = 0; anInt4508 > i_23_; i_23_++) {
					if (class385.face == is[i_23_]) {
						i_22_ = i_23_;
						break;
					}
				}
				if (i_22_ == -1)
					throw new RuntimeException();
				int i_24_ = ((Class166_Sub1.anIntArray4300[(class132.faces_colour[class385.face] & 0xffff)])
						& 0xffffff);
				i_24_ = i_24_ | -(class132.faces_alpha != null ? class132.faces_alpha[class385.face] : 0) + 255 << 24;
				aClass37Array4510[i_21_] = new Class37(i_22_, class132.faces_a[class385.face],
						class132.faces_b[class385.face], class132.faceS_c[class385.face], class101.anInt1072,
						class101.anInt1073, class101.texture_id, class101.anInt1079, class101.anInt1076,
						class101.aBoolean1077, class101.aBoolean1082, class385.anInt3257);
				aClass271Array4459[i_21_] = new Class271(i_24_);
			}
		}
		int i_25_ = anInt4508 * 3;
		aShort4476 = (short) i_5_;
		Class107.aLongArray3573 = new long[i_25_];
		aFloatArray4462 = new float[i_25_];
		aShortArray4477 = new short[anInt4508];
		aByteArray4463 = new byte[anInt4508];
		aShortArray4451 = new short[i_25_];
		aShortArray4455 = new short[i_25_];
		if (class132.aShortArray1352 != null)
			aShortArray4461 = new short[anInt4508];
		aByteArray4506 = new byte[i_25_];
		aShortArray4512 = new short[anInt4508];
		aFloatArray4466 = new float[i_25_];
		aShortArray4507 = new short[i_25_];
		aShortArray4484 = new short[i_25_];
		aShort4457 = (short) i_6_;
		aShortArray4509 = new short[anInt4508];
		aShortArray4483 = new short[i_25_];
		aShortArray4464 = new short[anInt4508];
		aShortArray4460 = new short[i_25_];
		aShortArray4488 = new short[anInt4508];
		int i_26_ = 0;
		for (int i_27_ = 0; i_27_ < class132.highest_face_index; i_27_++) {
			int i_28_ = anIntArray4472[i_27_];
			anIntArray4472[i_27_] = i_26_;
			class348s[i_27_] = new Class348();
			i_26_ += i_28_;
		}
		anIntArray4472[class132.highest_face_index] = i_26_;
		Class221 class221 = Class41_Sub23.method490(-741, is, anInt4508, class132);
		Class332[] class332s = new Class332[class132.num_faces];
		for (int i_29_ = 0; class132.num_faces > i_29_; i_29_++) {
			short i_30_ = class132.faces_a[i_29_];
			short i_31_ = class132.faces_b[i_29_];
			short i_32_ = class132.faceS_c[i_29_];
			int i_33_ = -anIntArray4482[i_30_] + anIntArray4482[i_31_];
			int i_34_ = anIntArray4505[i_31_] - anIntArray4505[i_30_];
			int i_35_ = -anIntArray4489[i_30_] + anIntArray4489[i_31_];
			int i_36_ = anIntArray4482[i_32_] - anIntArray4482[i_30_];
			int i_37_ = -anIntArray4505[i_30_] + anIntArray4505[i_32_];
			int i_38_ = anIntArray4489[i_32_] - anIntArray4489[i_30_];
			int i_39_ = i_38_ * i_34_ - i_37_ * i_35_;
			int i_40_ = -(i_33_ * i_38_) + i_36_ * i_35_;
			int i_41_;
			for (i_41_ = -(i_34_ * i_36_) + i_37_ * i_33_; (i_39_ > 8192 || i_40_ > 8192 || i_41_ > 8192
					|| i_39_ < -8192 || i_40_ < -8192 || i_41_ < -8192); i_41_ >>= 1) {
				i_39_ >>= 1;
				i_40_ >>= 1;
			}
			int i_42_ = (int) Math.sqrt((double) (i_41_ * i_41_ + (i_40_ * i_40_ + i_39_ * i_39_)));
			if (i_42_ <= 0)
				i_42_ = 1;
			i_41_ = i_41_ * 256 / i_42_;
			i_39_ = i_39_ * 256 / i_42_;
			i_40_ = i_40_ * 256 / i_42_;
			byte i_43_ = (class132.aByteArray1354 == null ? (byte) 0 : class132.aByteArray1354[i_29_]);
			if (i_43_ == 0) {
				Class348 class348 = class348s[i_30_];
				class348.anInt3030 += i_40_;
				class348.anInt3032 += i_39_;
				class348.anInt3031++;
				class348.anInt3029 += i_41_;
				class348 = class348s[i_31_];
				class348.anInt3029 += i_41_;
				class348.anInt3030 += i_40_;
				class348.anInt3031++;
				class348.anInt3032 += i_39_;
				class348 = class348s[i_32_];
				class348.anInt3032 += i_39_;
				class348.anInt3030 += i_40_;
				class348.anInt3029 += i_41_;
				class348.anInt3031++;
			} else if (i_43_ == 1) {
				Class332 class332 = class332s[i_29_] = new Class332();
				class332.anInt2942 = i_41_;
				class332.anInt2940 = i_39_;
				class332.anInt2939 = i_40_;
			}
		}
		for (int i_44_ = 0; i_44_ < anInt4508; i_44_++) {
			int i_45_ = is[i_44_];
			int i_46_ = class132.faces_colour[i_45_] & 0xffff;
			int i_47_;
			if (class132.faces_texture == null)
				i_47_ = -1;
			else
				i_47_ = class132.faces_texture[i_45_];
			int i_48_;
			if (class132.faces_alpha == null)
				i_48_ = 0;
			else
				i_48_ = class132.faces_alpha[i_45_] & 0xff;
			short i_49_ = (class132.faces_material == null ? (short) -1 : class132.faces_material[i_45_]);
			if (i_49_ != -1 && (anInt4481 & 0x40) != 0) {
				MaterialRaw class170 = var_d.method14(i_49_ & 0xffff, -9412);
				if (class170.aBoolean1792)
					i_49_ = (short) -1;
			}
			float f = 0.0F;
			float f_50_ = 0.0F;
			float f_51_ = 0.0F;
			float f_52_ = 0.0F;
			float f_53_ = 0.0F;
			float f_54_ = 0.0F;
			int i_55_ = 0;
			int i_56_ = 0;
			int i_57_ = 0;
			if (i_49_ != -1) {
				if (i_47_ == -1) {
					i_55_ = 1;
					f_52_ = 1.0F;
					f_51_ = 1.0F;
					f = 0.0F;
					i_56_ = 2;
					f_53_ = 0.0F;
					f_54_ = 0.0F;
					f_50_ = 1.0F;
				} else {
					i_47_ &= 0xff;
					byte i_58_ = class132.textures_mapping_type[i_47_];
					if (i_58_ == 0) {
						short i_59_ = class132.faces_a[i_45_];
						short i_60_ = class132.faces_b[i_45_];
						short i_61_ = class132.faceS_c[i_45_];
						short i_62_ = class132.textures_mapping_p[i_47_];
						short i_63_ = class132.textures_mapping_m[i_47_];
						short i_64_ = class132.textures_mapping_n[i_47_];
						float f_65_ = (float) class132.vertices_x[i_62_];
						float f_66_ = (float) class132.vertices_y[i_62_];
						float f_67_ = (float) class132.vertices_z[i_62_];
						float f_68_ = -f_65_ + (float) class132.vertices_x[i_63_];
						float f_69_ = (float) class132.vertices_y[i_63_] - f_66_;
						float f_70_ = -f_67_ + (float) class132.vertices_z[i_63_];
						float f_71_ = (float) class132.vertices_x[i_64_] - f_65_;
						float f_72_ = (float) class132.vertices_y[i_64_] - f_66_;
						float f_73_ = -f_67_ + (float) class132.vertices_z[i_64_];
						float f_74_ = (float) class132.vertices_x[i_59_] - f_65_;
						float f_75_ = -f_66_ + (float) class132.vertices_y[i_59_];
						float f_76_ = -f_67_ + (float) class132.vertices_z[i_59_];
						float f_77_ = -f_65_ + (float) class132.vertices_x[i_60_];
						float f_78_ = (float) class132.vertices_y[i_60_] - f_66_;
						float f_79_ = (float) class132.vertices_z[i_60_] - f_67_;
						float f_80_ = -f_65_ + (float) class132.vertices_x[i_61_];
						float f_81_ = (float) class132.vertices_y[i_61_] - f_66_;
						float f_82_ = (float) class132.vertices_z[i_61_] - f_67_;
						float f_83_ = -(f_70_ * f_72_) + f_73_ * f_69_;
						float f_84_ = f_71_ * f_70_ - f_68_ * f_73_;
						float f_85_ = f_68_ * f_72_ - f_69_ * f_71_;
						float f_86_ = -(f_73_ * f_84_) + f_85_ * f_72_;
						float f_87_ = f_83_ * f_73_ - f_71_ * f_85_;
						float f_88_ = -(f_83_ * f_72_) + f_84_ * f_71_;
						float f_89_ = 1.0F / (f_70_ * f_88_ + (f_86_ * f_68_ + f_69_ * f_87_));
						f_51_ = (f_77_ * f_86_ + f_87_ * f_78_ + f_88_ * f_79_) * f_89_;
						f = ((f_88_ * f_76_ + (f_86_ * f_74_ + f_87_ * f_75_)) * f_89_);
						f_53_ = (f_80_ * f_86_ + f_87_ * f_81_ + f_88_ * f_82_) * f_89_;
						f_88_ = f_84_ * f_68_ - f_83_ * f_69_;
						f_86_ = f_69_ * f_85_ - f_70_ * f_84_;
						f_87_ = -(f_85_ * f_68_) + f_83_ * f_70_;
						f_89_ = 1.0F / (f_88_ * f_73_ + (f_86_ * f_71_ + f_87_ * f_72_));
						f_50_ = f_89_ * (f_76_ * f_88_ + (f_87_ * f_75_ + f_74_ * f_86_));
						f_54_ = (f_86_ * f_80_ + f_87_ * f_81_ + f_82_ * f_88_) * f_89_;
						f_52_ = f_89_ * (f_77_ * f_86_ + f_87_ * f_78_ + f_88_ * f_79_);
					} else {
						short i_90_ = class132.faces_a[i_45_];
						short i_91_ = class132.faces_b[i_45_];
						short i_92_ = class132.faceS_c[i_45_];
						int i_93_ = class221.anIntArray2153[i_47_];
						int i_94_ = class221.anIntArray2157[i_47_];
						int i_95_ = class221.anIntArray2154[i_47_];
						float[] fs = class221.aFloatArrayArray2156[i_47_];
						byte i_96_ = class132.aByteArray1367[i_47_];
						float f_97_ = (float) class132.anIntArray1371[i_47_] / 256.0F;
						if (i_58_ != 1) {
							if (i_58_ != 2) {
								if (i_58_ == 3) {
									Class296_Sub51_Sub39.method3201(i_96_, i_93_, fs, class132.vertices_x[i_90_], 54,
											IOException_Sub1.aFloatArray37, class132.vertices_y[i_90_], i_94_, f_97_,
											i_95_, class132.vertices_z[i_90_]);
									f_50_ = IOException_Sub1.aFloatArray37[1];
									f = IOException_Sub1.aFloatArray37[0];
									Class296_Sub51_Sub39.method3201(i_96_, i_93_, fs, class132.vertices_x[i_91_], 21,
											IOException_Sub1.aFloatArray37, class132.vertices_y[i_91_], i_94_, f_97_,
											i_95_, class132.vertices_z[i_91_]);
									f_52_ = IOException_Sub1.aFloatArray37[1];
									f_51_ = IOException_Sub1.aFloatArray37[0];
									Class296_Sub51_Sub39.method3201(i_96_, i_93_, fs, class132.vertices_x[i_92_], 36,
											IOException_Sub1.aFloatArray37, class132.vertices_y[i_92_], i_94_, f_97_,
											i_95_, class132.vertices_z[i_92_]);
									f_53_ = IOException_Sub1.aFloatArray37[0];
									f_54_ = IOException_Sub1.aFloatArray37[1];
									if ((i_96_ & 0x1) == 0) {
										if (!(-f + f_51_ > 0.5F)) {
											if (-f_51_ + f > 0.5F) {
												i_55_ = 2;
												f_51_++;
											}
										} else {
											f_51_--;
											i_55_ = 1;
										}
										if (!(f_53_ - f > 0.5F)) {
											if (-f_53_ + f > 0.5F) {
												i_56_ = 2;
												f_53_++;
											}
										} else {
											f_53_--;
											i_56_ = 1;
										}
									} else {
										if (!(-f_50_ + f_54_ > 0.5F)) {
											if (-f_54_ + f_50_ > 0.5F) {
												f_54_++;
												i_56_ = 2;
											}
										} else {
											i_56_ = 1;
											f_54_--;
										}
										if (f_52_ - f_50_ > 0.5F) {
											i_55_ = 1;
											f_52_--;
										} else if (-f_52_ + f_50_ > 0.5F) {
											f_52_++;
											i_55_ = 2;
										}
									}
								}
							} else {
								float f_98_ = ((float) class132.anIntArray1347[i_47_] / 256.0F);
								float f_99_ = ((float) class132.anIntArray1348[i_47_] / 256.0F);
								int i_100_ = (class132.vertices_x[i_91_] - class132.vertices_x[i_90_]);
								int i_101_ = (-class132.vertices_y[i_90_] + class132.vertices_y[i_91_]);
								int i_102_ = (-class132.vertices_z[i_90_] + class132.vertices_z[i_91_]);
								int i_103_ = (class132.vertices_x[i_92_] - class132.vertices_x[i_90_]);
								int i_104_ = (class132.vertices_y[i_92_] - class132.vertices_y[i_90_]);
								int i_105_ = (-class132.vertices_z[i_90_] + class132.vertices_z[i_92_]);
								int i_106_ = -(i_102_ * i_104_) + i_105_ * i_101_;
								int i_107_ = i_102_ * i_103_ - i_100_ * i_105_;
								int i_108_ = -(i_101_ * i_103_) + i_104_ * i_100_;
								float f_109_ = 64.0F / (float) (class132.anIntArray1344[i_47_]);
								float f_110_ = 64.0F / (float) (class132.anIntArray1365[i_47_]);
								float f_111_ = 64.0F / (float) (class132.anIntArray1379[i_47_]);
								float f_112_ = ((fs[2] * (float) i_108_
										+ ((float) i_107_ * fs[1] + fs[0] * (float) i_106_)) / f_109_);
								float f_113_ = ((fs[5] * (float) i_108_
										+ (fs[4] * (float) i_107_ + (float) i_106_ * fs[3])) / f_110_);
								float f_114_ = (((float) i_107_ * fs[7] + fs[6] * (float) i_106_
										+ (float) i_108_ * fs[8]) / f_111_);
								i_57_ = ConfigurationsLoader.method184(f_113_, (byte) 83, f_112_, f_114_);
								Class245.method2185(i_96_, IOException_Sub1.aFloatArray37, i_57_,
										class132.vertices_y[i_90_], i_94_, f_99_, f_98_, 64, i_93_, f_97_, i_95_,
										class132.vertices_z[i_90_], fs, class132.vertices_x[i_90_]);
								f_50_ = IOException_Sub1.aFloatArray37[1];
								f = IOException_Sub1.aFloatArray37[0];
								Class245.method2185(i_96_, IOException_Sub1.aFloatArray37, i_57_,
										class132.vertices_y[i_91_], i_94_, f_99_, f_98_, 105, i_93_, f_97_, i_95_,
										class132.vertices_z[i_91_], fs, class132.vertices_x[i_91_]);
								f_52_ = IOException_Sub1.aFloatArray37[1];
								f_51_ = IOException_Sub1.aFloatArray37[0];
								Class245.method2185(i_96_, IOException_Sub1.aFloatArray37, i_57_,
										class132.vertices_y[i_92_], i_94_, f_99_, f_98_, 80, i_93_, f_97_, i_95_,
										class132.vertices_z[i_92_], fs, class132.vertices_x[i_92_]);
								f_54_ = IOException_Sub1.aFloatArray37[1];
								f_53_ = IOException_Sub1.aFloatArray37[0];
							}
						} else {
							float f_115_ = ((float) class132.anIntArray1379[i_47_] / 1024.0F);
							Class215.method2027(class132.vertices_x[i_90_], fs, i_93_, f_115_,
									class132.vertices_y[i_90_], i_95_, -3, f_97_, IOException_Sub1.aFloatArray37,
									class132.vertices_z[i_90_], i_96_, i_94_);
							f = IOException_Sub1.aFloatArray37[0];
							f_50_ = IOException_Sub1.aFloatArray37[1];
							Class215.method2027(class132.vertices_x[i_91_], fs, i_93_, f_115_,
									class132.vertices_y[i_91_], i_95_, -3, f_97_, IOException_Sub1.aFloatArray37,
									class132.vertices_z[i_91_], i_96_, i_94_);
							f_52_ = IOException_Sub1.aFloatArray37[1];
							f_51_ = IOException_Sub1.aFloatArray37[0];
							Class215.method2027(class132.vertices_x[i_92_], fs, i_93_, f_115_,
									class132.vertices_y[i_92_], i_95_, -3, f_97_, IOException_Sub1.aFloatArray37,
									class132.vertices_z[i_92_], i_96_, i_94_);
							f_54_ = IOException_Sub1.aFloatArray37[1];
							f_53_ = IOException_Sub1.aFloatArray37[0];
							float f_116_ = f_115_ / 2.0F;
							if ((i_96_ & 0x1) != 0) {
								if (f_116_ < -f_50_ + f_52_) {
									f_52_ -= f_115_;
									i_55_ = 1;
								} else if (f_50_ - f_52_ > f_116_) {
									i_55_ = 2;
									f_52_ += f_115_;
								}
								if (!(f_54_ - f_50_ > f_116_)) {
									if (f_116_ < f_50_ - f_54_) {
										f_54_ += f_115_;
										i_56_ = 2;
									}
								} else {
									i_56_ = 1;
									f_54_ -= f_115_;
								}
							} else {
								if (!(f_51_ - f > f_116_)) {
									if (f_116_ < -f_51_ + f) {
										i_55_ = 2;
										f_51_ += f_115_;
									}
								} else {
									f_51_ -= f_115_;
									i_55_ = 1;
								}
								if (f_116_ < -f + f_53_) {
									i_56_ = 1;
									f_53_ -= f_115_;
								} else if (f_116_ < f - f_53_) {
									f_53_ += f_115_;
									i_56_ = 2;
								}
							}
						}
					}
				}
			}
			byte i_117_;
			if (class132.aByteArray1354 != null)
				i_117_ = class132.aByteArray1354[i_45_];
			else
				i_117_ = (byte) 0;
			if (i_117_ == 0) {
				long l = ((long) (i_47_ << 2) - -((long) (i_46_ << 8) + ((long) (i_57_ << 24) + (long) i_48_) << 32));
				short i_118_ = class132.faces_a[i_45_];
				short i_119_ = class132.faces_b[i_45_];
				short i_120_ = class132.faceS_c[i_45_];
				Class348 class348 = class348s[i_118_];
				aShortArray4464[i_44_] = method1800(class348.anInt3030, class348.anInt3031, f, class348.anInt3032,
						class348.anInt3029, l, class132, (byte) -64, i_44_, i_118_, f_50_);
				class348 = class348s[i_119_];
				aShortArray4509[i_44_] = method1800(class348.anInt3030, class348.anInt3031, f_51_, class348.anInt3032,
						class348.anInt3029, (long) i_55_ + l, class132, (byte) -64, i_44_, i_119_, f_52_);
				class348 = class348s[i_120_];
				aShortArray4488[i_44_] = method1800(class348.anInt3030, class348.anInt3031, f_53_, class348.anInt3032,
						class348.anInt3029, l + (long) i_56_, class132, (byte) -64, i_44_, i_120_, f_54_);
			} else if (i_117_ == 1) {
				Class332 class332 = class332s[i_45_];
				long l = ((long) ((class332.anInt2942 + 256 << 22)
						+ ((i_47_ << 2) + ((class332.anInt2940 > 0 ? 1024 : 2048) + (class332.anInt2939 + 256 << 12))))
						+ ((long) i_48_ + ((long) (i_46_ << 8) + (long) (i_57_ << 24)) << 32));
				aShortArray4464[i_44_] = method1800(class332.anInt2939, 0, f, class332.anInt2940, class332.anInt2942, l,
						class132, (byte) -64, i_44_, class132.faces_a[i_45_], f_50_);
				aShortArray4509[i_44_] = method1800(class332.anInt2939, 0, f_51_, class332.anInt2940,
						class332.anInt2942, (long) i_55_ + l, class132, (byte) -64, i_44_, class132.faces_b[i_45_],
						f_52_);
				aShortArray4488[i_44_] = method1800(class332.anInt2939, 0, f_53_, class332.anInt2940,
						class332.anInt2942, l + (long) i_56_, class132, (byte) -64, i_44_, class132.faceS_c[i_45_],
						f_54_);
			}
			if (class132.faces_alpha != null)
				aByteArray4463[i_44_] = class132.faces_alpha[i_45_];
			if (class132.aShortArray1352 != null)
				aShortArray4461[i_44_] = class132.aShortArray1352[i_45_];
			aShortArray4477[i_44_] = class132.faces_colour[i_45_];
			aShortArray4512[i_44_] = i_49_;
		}
		if (anInt4493 > 0) {
			int i_121_ = 1;
			short i_122_ = aShortArray4512[0];
			for (int i_123_ = 0; i_123_ < anInt4493; i_123_++) {
				short i_124_ = aShortArray4512[i_123_];
				if (i_124_ != i_122_) {
					i_121_++;
					i_122_ = i_124_;
				}
			}
			anIntArray4511 = new int[i_121_];
			anIntArray4465 = new int[i_121_ + 1];
			anIntArray4491 = new int[i_121_];
			anIntArray4465[0] = 0;
			int i_125_ = anInt4474;
			i_121_ = 0;
			int i_126_ = 0;
			i_122_ = aShortArray4512[0];
			for (int i_127_ = 0; i_127_ < anInt4493; i_127_++) {
				short i_128_ = aShortArray4512[i_127_];
				if (i_128_ != i_122_) {
					anIntArray4511[i_121_] = i_125_;
					anIntArray4491[i_121_] = i_126_ - (i_125_ - 1);
					anIntArray4465[++i_121_] = i_127_;
					i_122_ = i_128_;
					i_125_ = anInt4474;
					i_126_ = 0;
				}
				int i_129_ = aShortArray4464[i_127_];
				if (i_125_ > i_129_)
					i_125_ = i_129_;
				if (i_126_ < i_129_)
					i_126_ = i_129_;
				i_129_ = aShortArray4509[i_127_];
				if (i_129_ < i_125_)
					i_125_ = i_129_;
				if (i_126_ < i_129_)
					i_126_ = i_129_;
				i_129_ = aShortArray4488[i_127_];
				if (i_125_ > i_129_)
					i_125_ = i_129_;
				if (i_126_ < i_129_)
					i_126_ = i_129_;
			}
			anIntArray4511[i_121_] = i_125_;
			anIntArray4491[i_121_] = -i_125_ + i_126_ + 1;
			anIntArray4465[++i_121_] = anInt4493;
		}
		Class107.aLongArray3573 = null;
		aShortArray4455 = Class182.method1848(aShortArray4455, 74, anInt4474);
		aShortArray4451 = Class182.method1848(aShortArray4451, 74, anInt4474);
		aShortArray4507 = Class182.method1848(aShortArray4507, 74, anInt4474);
		aShortArray4484 = Class182.method1848(aShortArray4484, 74, anInt4474);
		aShortArray4483 = Class182.method1848(aShortArray4483, 74, anInt4474);
		aByteArray4506 = Class272.method2311(aByteArray4506, anInt4474, -21);
		aFloatArray4466 = Class296_Sub39_Sub1.method2781(aFloatArray4466, 26587, anInt4474);
		aFloatArray4462 = Class296_Sub39_Sub1.method2781(aFloatArray4462, 26587, anInt4474);
		if (class132.vertices_label != null && Class41_Sub5.method413(anInt4481, i, 3709))
			anIntArrayArray4475 = class132.method1385((byte) 111, false);
		if (class132.billboards != null && Class4.method174(anInt4481, i, 1019))
			anIntArrayArray4479 = class132.method1390((byte) -97);
		if (class132.faces_label != null && Class175.method1702(anInt4481, true, i)) {
			int i_130_ = 0;
			int[] is_131_ = new int[256];
			for (int i_132_ = 0; i_132_ < anInt4508; i_132_++) {
				int i_133_ = class132.faces_label[is[i_132_]];
				if (i_133_ >= 0) {
					if (i_130_ < i_133_)
						i_130_ = i_133_;
					is_131_[i_133_]++;
				}
			}
			anIntArrayArray4454 = new int[i_130_ + 1][];
			for (int i_134_ = 0; i_134_ <= i_130_; i_134_++) {
				anIntArrayArray4454[i_134_] = new int[is_131_[i_134_]];
				is_131_[i_134_] = 0;
			}
			for (int i_135_ = 0; anInt4508 > i_135_; i_135_++) {
				int i_136_ = class132.faces_label[is[i_135_]];
				if (i_136_ >= 0)
					anIntArrayArray4454[i_136_][is_131_[i_136_]++] = i_135_;
			}
		}
	}

	public int EA() {
		if (!aBoolean4496)
			method1805((byte) -49);
		return anInt4495;
	}

	public boolean method1737() {
		if (aShortArray4512 == null)
			return true;
		for (int i = 0; i < aShortArray4512.length; i++) {
			if (aShortArray4512[i] != -1 && !aHa_Sub1_4492.aD1299.is_ready(aShortArray4512[i]))
				return false;
		}
		return true;
	}

	public boolean method1731(int i, int i_137_, Class373 class373, boolean bool, int i_138_, int i_139_) {
		return method1802(i, bool, i_138_, class373, (byte) -92, i_139_, i_137_);
	}

	public int HA() {
		if (!aBoolean4496)
			method1805((byte) -66);
		return anInt4503;
	}

	private void method1797(int i) {
		int i_140_ = 36 % ((-81 - i) / 37);
		if (aClass37Array4510 != null) {
			aHa_Sub1_4492.C(!aBoolean4499);
			aHa_Sub1_4492.method1137(4, false);
			aHa_Sub1_4492.method1158((byte) -124, 1, Class151.aClass287_1553);
			aHa_Sub1_4492.method1219((byte) 54, Class151.aClass287_1553, 1);
			for (int i_141_ = 0; i_141_ < anInt4490; i_141_++) {
				Class37 class37 = aClass37Array4510[i_141_];
				Class271 class271 = aClass271Array4459[i_141_];
				if (!class37.aBoolean360 || !aHa_Sub1_4492.b()) {
					float f = ((float) (anIntArray4482[class37.anInt363]
							+ (anIntArray4482[class37.anInt366] + anIntArray4482[class37.anInt359])) * 0.3333333F);
					float f_142_ = ((float) (anIntArray4505[class37.anInt363]
							+ (anIntArray4505[class37.anInt366] + anIntArray4505[class37.anInt359])) * 0.3333333F);
					float f_143_ = ((float) (anIntArray4489[class37.anInt359]
							+ (anIntArray4489[class37.anInt366] + anIntArray4489[class37.anInt363])) * 0.3333333F);
					float f_144_ = (Class161.aFloat1672 + (Class207.aFloat2079 * f_142_ + Class240.aFloat2263 * f
							+ Class338_Sub3_Sub4_Sub1.aFloat6658 * f_143_));
					float f_145_ = (OutputStream_Sub2.aFloat42 + (Class16_Sub3_Sub1.aFloat5806 * f_143_
							+ (Class288.aFloat2650 * f_142_ + Class296_Sub34_Sub2.aFloat6106 * f)));
					float f_146_ = (StaticMethods.aFloat6081 * f_143_
							+ (f_142_ * Class281.aFloat2601 + f * World.aFloat1161) + SubInPacket.aFloat2422);
					float f_147_ = ((float) (1.0
							/ Math.sqrt((double) (f_144_ * f_144_ + f_145_ * f_145_ + f_146_ * f_146_)))
							* (float) class37.anInt364);
					Class373_Sub2 class373_sub2 = aHa_Sub1_4492.method1215(62);
					class373_sub2.method3942(-(f_146_ * f_147_) + f_146_,
							-(f_147_ * f_145_) + ((float) class271.anInt2511 + f_145_),
							class37.aShort358 * class271.anInt2513 >> 7, 12,
							class37.aShort362 * class271.anInt2515 >> 7,
							-(f_147_ * f_144_) + ((float) class271.anInt2516 + f_144_), class271.anInt2514);
					class373_sub2.method3947(-16988, aHa_Sub1_4492.aClass373_Sub2_3942);
					aHa_Sub1_4492.method1202(0);
					int i_148_ = class271.anInt2517;
					aHa_Sub1_4492.method1133(false, class37.aShort357, 0, false);
					aHa_Sub1_4492.method1161(31156, class37.aByte361);
					aHa_Sub1_4492.method1139(0, i_148_);
					aHa_Sub1_4492.method1142(-96);
				}
			}
			aHa_Sub1_4492.method1219((byte) 54, Class153.aClass287_1578, 1);
			aHa_Sub1_4492.method1158((byte) -125, 1, Class153.aClass287_1578);
			aHa_Sub1_4492.C(true);
		}
	}

	public int ua() {
		return anInt4500;
	}

	public void method1718(Class373 class373) {
		Class373_Sub2 class373_sub2 = (Class373_Sub2) class373;
		if (aClass89Array4487 != null) {
			for (int i = 0; aClass89Array4487.length > i; i++) {
				EmissiveTriangle class89 = aClass89Array4487[i];
				EmissiveTriangle class89_149_ = class89;
				if (class89.aClass89_956 != null)
					class89_149_ = class89.aClass89_956;
				class89_149_.anInt951 = (int) (class373_sub2.aFloat5593
						+ ((class373_sub2.aFloat5595 * (float) anIntArray4505[class89.anInt954])
								+ (class373_sub2.aFloat5598 * (float) anIntArray4482[class89.anInt954])
								+ (class373_sub2.aFloat5602 * (float) (anIntArray4489[class89.anInt954]))));
				class89_149_.anInt950 = (int) (class373_sub2.aFloat5590
						+ ((class373_sub2.aFloat5596 * (float) anIntArray4505[class89.anInt954])
								+ (class373_sub2.aFloat5603 * (float) anIntArray4482[class89.anInt954])
								+ ((float) anIntArray4489[class89.anInt954] * class373_sub2.aFloat5597)));
				class89_149_.anInt957 = (int) (class373_sub2.aFloat5594
						+ (((float) anIntArray4489[class89.anInt954] * class373_sub2.aFloat5591)
								+ ((class373_sub2.aFloat5599 * (float) anIntArray4505[class89.anInt954])
										+ ((float) anIntArray4482[class89.anInt954] * class373_sub2.aFloat5601))));
				class89_149_.anInt961 = (int) (class373_sub2.aFloat5593
						+ ((class373_sub2.aFloat5595 * (float) anIntArray4505[class89.anInt953])
								+ ((float) anIntArray4482[class89.anInt953] * class373_sub2.aFloat5598)
								+ ((float) anIntArray4489[class89.anInt953] * class373_sub2.aFloat5602)));
				class89_149_.anInt963 = (int) (class373_sub2.aFloat5590
						+ (((float) anIntArray4489[class89.anInt953] * class373_sub2.aFloat5597)
								+ ((class373_sub2.aFloat5596 * (float) anIntArray4505[class89.anInt953])
										+ ((float) anIntArray4482[class89.anInt953] * class373_sub2.aFloat5603))));
				class89_149_.anInt960 = (int) (((float) anIntArray4489[class89.anInt953] * class373_sub2.aFloat5591)
						+ ((class373_sub2.aFloat5601 * (float) anIntArray4482[class89.anInt953])
								+ ((float) anIntArray4505[class89.anInt953] * class373_sub2.aFloat5599))
						+ class373_sub2.aFloat5594);
				class89_149_.anInt965 = (int) (class373_sub2.aFloat5593
						+ (((float) anIntArray4489[class89.anInt964] * class373_sub2.aFloat5602)
								+ ((class373_sub2.aFloat5598 * (float) anIntArray4482[class89.anInt964])
										+ ((float) anIntArray4505[class89.anInt964] * class373_sub2.aFloat5595))));
				class89_149_.anInt948 = (int) (class373_sub2.aFloat5590
						+ ((class373_sub2.aFloat5597 * (float) anIntArray4489[class89.anInt964])
								+ (((float) anIntArray4482[class89.anInt964] * class373_sub2.aFloat5603)
										+ ((float) anIntArray4505[class89.anInt964] * class373_sub2.aFloat5596))));
				class89_149_.anInt949 = (int) (class373_sub2.aFloat5594
						+ (((float) anIntArray4505[class89.anInt964] * class373_sub2.aFloat5599)
								+ (class373_sub2.aFloat5601 * (float) anIntArray4482[class89.anInt964])
								+ (class373_sub2.aFloat5591 * (float) (anIntArray4489[class89.anInt964]))));
			}
		}
		if (aClass232Array4504 != null) {
			for (int i = 0; aClass232Array4504.length > i; i++) {
				EffectiveVertex class232 = aClass232Array4504[i];
				EffectiveVertex class232_150_ = class232;
				if (class232.aClass232_2223 != null)
					class232_150_ = class232.aClass232_2223;
				if (class232.aClass373_2222 == null)
					class232.aClass373_2222 = class373_sub2.method3916();
				else
					class232.aClass373_2222.method3915(class373_sub2);
				class232_150_.anInt2219 = (int) (class373_sub2.aFloat5593
						+ ((class373_sub2.aFloat5595 * (float) anIntArray4505[class232.anInt2220])
								+ (class373_sub2.aFloat5598 * (float) (anIntArray4482[class232.anInt2220]))
								+ (class373_sub2.aFloat5602 * (float) (anIntArray4489[class232.anInt2220]))));
				class232_150_.anInt2217 = (int) (((float) anIntArray4482[class232.anInt2220] * class373_sub2.aFloat5603)
						+ ((float) anIntArray4505[class232.anInt2220] * class373_sub2.aFloat5596)
						+ (class373_sub2.aFloat5597 * (float) anIntArray4489[class232.anInt2220])
						+ class373_sub2.aFloat5590);
				class232_150_.anInt2214 = (int) ((class373_sub2.aFloat5599 * (float) anIntArray4505[class232.anInt2220])
						+ (class373_sub2.aFloat5601 * (float) anIntArray4482[class232.anInt2220])
						+ (class373_sub2.aFloat5591 * (float) anIntArray4489[class232.anInt2220])
						+ class373_sub2.aFloat5594);
			}
		}
	}

	public boolean r() {
		return aBoolean4502;
	}

	public void method1724(byte i, byte[] is) {
		if (is != null) {
			for (int i_151_ = 0; i_151_ < anInt4508; i_151_++) {
				int i_152_ = (-((255 - (is[i_151_] & 0xff)) * (255 - (i & 0xff)) / 255) + 255);
				aByteArray4463[i_151_] = (byte) i_152_;
			}
		} else {
			for (int i_153_ = 0; anInt4508 > i_153_; i_153_++)
				aByteArray4463[i_153_] = i;
		}
		method1796(1018);
	}

	public int RA() {
		if (!aBoolean4496)
			method1805((byte) -72);
		return anInt4452;
	}

	private boolean method1798(int i, int i_154_, int i_155_, int i_156_, int i_157_, int i_158_, int i_159_,
			int i_160_, int i_161_) {
		if (i_160_ < i_156_ && i > i_160_ && i_154_ > i_160_)
			return false;
		if (i_160_ > i_156_ && i_160_ > i && i_160_ > i_154_)
			return false;
		if (i_155_ >= -13)
			return true;
		if (i_161_ < i_158_ && i_159_ > i_161_ && i_161_ < i_157_)
			return false;
		if (i_158_ < i_161_ && i_161_ > i_159_ && i_157_ < i_161_)
			return false;
		return true;
	}

	public void VA(int i) {
		int i_162_ = Class296_Sub4.anIntArray4598[i];
		int i_163_ = Class296_Sub4.anIntArray4618[i];
		for (int i_164_ = 0; anInt4468 > i_164_; i_164_++) {
			int i_165_ = ((i_163_ * anIntArray4482[i_164_] + anIntArray4505[i_164_] * i_162_) >> 14);
			anIntArray4505[i_164_] = (-(i_162_ * anIntArray4482[i_164_]) + i_163_ * anIntArray4505[i_164_]) >> 14;
			anIntArray4482[i_164_] = i_165_;
		}
		method1806(-5);
		aBoolean4496 = false;
	}

	public void method1720() {
		/* empty */
	}

	public void method1722() {
		if (anInt4474 > 0 && anInt4493 > 0) {
			method1811(12);
			method1808(-9359);
			method1807(29055);
		}
	}

	private void method1799(byte i, r_Sub2 var_r_Sub2) {
		if (Class296_Sub30.anIntArray4821.length < anInt4474) {
			Class296_Sub30.anIntArray4821 = new int[anInt4474];
			Class196.anIntArray1989 = new int[anInt4474];
		}
		for (int i_166_ = 0; anInt4468 > i_166_; i_166_++) {
			int i_167_ = (-var_r_Sub2.anInt6713 + ((anIntArray4482[i_166_]
					- (anIntArray4505[i_166_] * aHa_Sub1_4492.anInt3969 >> 8)) >> aHa_Sub1_4492.anInt4036));
			int i_168_ = ((-(anIntArray4505[i_166_] * aHa_Sub1_4492.anInt4027 >> 8)
					+ anIntArray4489[i_166_] >> aHa_Sub1_4492.anInt4036) - var_r_Sub2.anInt6708);
			int i_169_ = anIntArray4472[i_166_];
			int i_170_ = anIntArray4472[i_166_ + 1];
			for (int i_171_ = i_169_; i_170_ > i_171_; i_171_++) {
				int i_172_ = aShortArray4460[i_171_] - 1;
				if (i_172_ == -1)
					break;
				Class296_Sub30.anIntArray4821[i_172_] = i_167_;
				Class196.anIntArray1989[i_172_] = i_168_;
			}
		}
		if (i != 60)
			aa((short) -91, (short) -54);
		for (int i_173_ = 0; anInt4493 > i_173_; i_173_++) {
			if (aByteArray4463 == null || aByteArray4463[i_173_] <= 128) {
				short i_174_ = aShortArray4464[i_173_];
				short i_175_ = aShortArray4509[i_173_];
				short i_176_ = aShortArray4488[i_173_];
				int i_177_ = Class296_Sub30.anIntArray4821[i_174_];
				int i_178_ = Class296_Sub30.anIntArray4821[i_175_];
				int i_179_ = Class296_Sub30.anIntArray4821[i_176_];
				int i_180_ = Class196.anIntArray1989[i_174_];
				int i_181_ = Class196.anIntArray1989[i_175_];
				int i_182_ = Class196.anIntArray1989[i_176_];
				if ((-((i_181_ - i_180_) * (-i_178_ + i_179_)) + (i_181_ - i_182_) * (i_177_ - i_178_)) > 0)
					var_r_Sub2.method2868(-709523440, i_180_, i_177_, i_178_, i_182_, i_181_, i_179_);
			}
		}
	}

	public void k(int i) {
		int i_183_ = Class296_Sub4.anIntArray4598[i];
		int i_184_ = Class296_Sub4.anIntArray4618[i];
		for (int i_185_ = 0; i_185_ < anInt4468; i_185_++) {
			int i_186_ = ((i_183_ * anIntArray4489[i_185_] + i_184_ * anIntArray4482[i_185_]) >> 14);
			anIntArray4489[i_185_] = (-(i_183_ * anIntArray4482[i_185_]) + i_184_ * anIntArray4489[i_185_]) >> 14;
			anIntArray4482[i_185_] = i_186_;
		}
		for (int i_187_ = 0; anInt4474 > i_187_; i_187_++) {
			int i_188_ = ((aShortArray4507[i_187_] * i_184_ + i_183_ * aShortArray4483[i_187_]) >> 14);
			aShortArray4483[i_187_] = (short) ((aShortArray4483[i_187_] * i_184_
					- aShortArray4507[i_187_] * i_183_) >> 14);
			aShortArray4507[i_187_] = (short) i_188_;
		}
		method1806(-5);
		method1804(-1);
		aBoolean4496 = false;
	}

	public void I(int i, int[] is, int i_189_, int i_190_, int i_191_, boolean bool, int i_192_, int[] is_193_) {
		int i_194_ = is.length;
		if (i == 0) {
			i_189_ <<= 4;
			i_190_ <<= 4;
			i_191_ <<= 4;
			int i_195_ = 0;
			HashTable.anInt2458 = 0;
			Class57.anInt667 = 0;
			Class182.anInt1876 = 0;
			for (int i_196_ = 0; i_196_ < i_194_; i_196_++) {
				int i_197_ = is[i_196_];
				if (i_197_ < anIntArrayArray4475.length) {
					int[] is_198_ = anIntArrayArray4475[i_197_];
					for (int i_199_ = 0; is_198_.length > i_199_; i_199_++) {
						int i_200_ = is_198_[i_199_];
						if (aShortArray4453 == null || (aShortArray4453[i_200_] & i_192_) != 0) {
							Class182.anInt1876 += anIntArray4482[i_200_];
							Class57.anInt667 += anIntArray4505[i_200_];
							HashTable.anInt2458 += anIntArray4489[i_200_];
							i_195_++;
						}
					}
				}
			}
			if (i_195_ > 0) {
				Class57.anInt667 = i_190_ + Class57.anInt667 / i_195_;
				Class182.anInt1876 = i_189_ + Class182.anInt1876 / i_195_;
				HashTable.anInt2458 = i_191_ + HashTable.anInt2458 / i_195_;
				StaticMethods.aBoolean5925 = true;
			} else {
				Class57.anInt667 = i_190_;
				Class182.anInt1876 = i_189_;
				HashTable.anInt2458 = i_191_;
			}
		} else if (i == 1) {
			if (is_193_ != null) {
				int i_201_ = (i_191_ * is_193_[2] + (i_189_ * is_193_[0] + (is_193_[1] * i_190_ + 8192)) >> 14);
				int i_202_ = ((is_193_[5] * i_191_ + 8192 + i_190_ * is_193_[4] + i_189_ * is_193_[3]) >> 14);
				int i_203_ = ((is_193_[8] * i_191_ + (i_190_ * is_193_[7] + is_193_[6] * i_189_) + 8192) >> 14);
				i_190_ = i_202_;
				i_191_ = i_203_;
				i_189_ = i_201_;
			}
			i_189_ <<= 4;
			i_191_ <<= 4;
			i_190_ <<= 4;
			for (int i_204_ = 0; i_194_ > i_204_; i_204_++) {
				int i_205_ = is[i_204_];
				if (anIntArrayArray4475.length > i_205_) {
					int[] is_206_ = anIntArrayArray4475[i_205_];
					for (int i_207_ = 0; i_207_ < is_206_.length; i_207_++) {
						int i_208_ = is_206_[i_207_];
						if (aShortArray4453 == null || (i_192_ & aShortArray4453[i_208_]) != 0) {
							anIntArray4482[i_208_] += i_189_;
							anIntArray4505[i_208_] += i_190_;
							anIntArray4489[i_208_] += i_191_;
						}
					}
				}
			}
		} else if (i == 2) {
			if (is_193_ == null) {
				for (int i_209_ = 0; i_209_ < i_194_; i_209_++) {
					int i_210_ = is[i_209_];
					if (i_210_ < anIntArrayArray4475.length) {
						int[] is_211_ = anIntArrayArray4475[i_210_];
						for (int i_212_ = 0; is_211_.length > i_212_; i_212_++) {
							int i_213_ = is_211_[i_212_];
							if (aShortArray4453 == null || (aShortArray4453[i_213_] & i_192_) != 0) {
								anIntArray4482[i_213_] -= Class182.anInt1876;
								anIntArray4505[i_213_] -= Class57.anInt667;
								anIntArray4489[i_213_] -= HashTable.anInt2458;
								if (i_191_ != 0) {
									int i_214_ = Class296_Sub4.anIntArray4598[i_191_];
									int i_215_ = Class296_Sub4.anIntArray4618[i_191_];
									int i_216_ = ((i_214_ * anIntArray4505[i_213_] + 16383
											+ i_215_ * anIntArray4482[i_213_]) >> 14);
									anIntArray4505[i_213_] = (anIntArray4505[i_213_] * i_215_
											- anIntArray4482[i_213_] * i_214_ + 16383) >> 14;
									anIntArray4482[i_213_] = i_216_;
								}
								if (i_189_ != 0) {
									int i_217_ = Class296_Sub4.anIntArray4598[i_189_];
									int i_218_ = Class296_Sub4.anIntArray4618[i_189_];
									int i_219_ = ((anIntArray4505[i_213_] * i_218_ - anIntArray4489[i_213_] * i_217_
											+ 16383) >> 14);
									anIntArray4489[i_213_] = (anIntArray4489[i_213_] * i_218_
											+ (anIntArray4505[i_213_] * i_217_ + 16383)) >> 14;
									anIntArray4505[i_213_] = i_219_;
								}
								if (i_190_ != 0) {
									int i_220_ = Class296_Sub4.anIntArray4598[i_190_];
									int i_221_ = Class296_Sub4.anIntArray4618[i_190_];
									int i_222_ = ((i_220_ * anIntArray4489[i_213_] + i_221_ * anIntArray4482[i_213_]
											+ 16383) >> 14);
									anIntArray4489[i_213_] = ((-(i_220_ * anIntArray4482[i_213_]) + 16383
											+ anIntArray4489[i_213_] * i_221_) >> 14);
									anIntArray4482[i_213_] = i_222_;
								}
								anIntArray4482[i_213_] += Class182.anInt1876;
								anIntArray4505[i_213_] += Class57.anInt667;
								anIntArray4489[i_213_] += HashTable.anInt2458;
							}
						}
					}
				}
				if (bool) {
					for (int i_223_ = 0; i_223_ < i_194_; i_223_++) {
						int i_224_ = is[i_223_];
						if (anIntArrayArray4475.length > i_224_) {
							int[] is_225_ = anIntArrayArray4475[i_224_];
							for (int i_226_ = 0; is_225_.length > i_226_; i_226_++) {
								int i_227_ = is_225_[i_226_];
								if (aShortArray4453 == null || ((i_192_ & aShortArray4453[i_227_]) != 0)) {
									int i_228_ = anIntArray4472[i_227_];
									int i_229_ = anIntArray4472[i_227_ + 1];
									for (int i_230_ = i_228_; i_229_ > i_230_; i_230_++) {
										int i_231_ = aShortArray4460[i_230_] - 1;
										if (i_231_ == -1)
											break;
										if (i_191_ != 0) {
											int i_232_ = (Class296_Sub4.anIntArray4598[i_191_]);
											int i_233_ = (Class296_Sub4.anIntArray4618[i_191_]);
											int i_234_ = (((aShortArray4484[i_231_] * i_232_) + 16383
													+ i_233_ * (aShortArray4507[i_231_])) >> 14);
											aShortArray4484[i_231_] = (short) ((-((aShortArray4507[i_231_]) * i_232_)
													+ (i_233_ * (aShortArray4484[i_231_])) + 16383) >> 14);
											aShortArray4507[i_231_] = (short) i_234_;
										}
										if (i_189_ != 0) {
											int i_235_ = (Class296_Sub4.anIntArray4598[i_189_]);
											int i_236_ = (Class296_Sub4.anIntArray4618[i_189_]);
											int i_237_ = (-(i_235_ * aShortArray4483[i_231_])
													+ (i_236_ * (aShortArray4484[i_231_]) + 16383)) >> 14;
											aShortArray4483[i_231_] = (short) (((aShortArray4484[i_231_]) * i_235_
													+ (((aShortArray4483[i_231_]) * i_236_) + 16383)) >> 14);
											aShortArray4484[i_231_] = (short) i_237_;
										}
										if (i_190_ != 0) {
											int i_238_ = (Class296_Sub4.anIntArray4598[i_190_]);
											int i_239_ = (Class296_Sub4.anIntArray4618[i_190_]);
											int i_240_ = (((aShortArray4507[i_231_] * i_239_)
													+ ((aShortArray4483[i_231_] * i_238_) + 16383)) >> 14);
											aShortArray4483[i_231_] = (short) ((-(i_238_ * (aShortArray4507[i_231_]))
													+ ((i_239_ * (aShortArray4483[i_231_])) + 16383)) >> 14);
											aShortArray4507[i_231_] = (short) i_240_;
										}
									}
								}
							}
						}
					}
					method1804(-1);
				}
			} else {
				int i_241_ = is_193_[9] << 4;
				int i_242_ = is_193_[10] << 4;
				int i_243_ = is_193_[11] << 4;
				int i_244_ = is_193_[12] << 4;
				int i_245_ = is_193_[13] << 4;
				int i_246_ = is_193_[14] << 4;
				if (StaticMethods.aBoolean5925) {
					int i_247_ = ((is_193_[0] * Class182.anInt1876 + 8192 + is_193_[3] * Class57.anInt667
							+ is_193_[6] * HashTable.anInt2458) >> 14);
					int i_248_ = ((HashTable.anInt2458 * is_193_[7] + is_193_[1] * Class182.anInt1876
							+ Class57.anInt667 * is_193_[4] + 8192) >> 14);
					int i_249_ = ((is_193_[8] * HashTable.anInt2458 + Class57.anInt667 * is_193_[5]
							+ (is_193_[2] * Class182.anInt1876 + 8192)) >> 14);
					i_247_ += i_244_;
					i_248_ += i_245_;
					i_249_ += i_246_;
					Class182.anInt1876 = i_247_;
					Class57.anInt667 = i_248_;
					StaticMethods.aBoolean5925 = false;
					HashTable.anInt2458 = i_249_;
				}
				int[] is_250_ = new int[9];
				int i_251_ = Class296_Sub4.anIntArray4618[i_189_];
				int i_252_ = Class296_Sub4.anIntArray4598[i_189_];
				int i_253_ = Class296_Sub4.anIntArray4618[i_190_];
				int i_254_ = Class296_Sub4.anIntArray4598[i_190_];
				int i_255_ = Class296_Sub4.anIntArray4618[i_191_];
				int i_256_ = Class296_Sub4.anIntArray4598[i_191_];
				int i_257_ = i_252_ * i_255_ + 8192 >> 14;
				int i_258_ = i_252_ * i_256_ + 8192 >> 14;
				is_250_[0] = i_253_ * i_255_ + 8192 + i_258_ * i_254_ >> 14;
				is_250_[6] = i_255_ * -i_254_ + i_253_ * i_258_ + 8192 >> 14;
				is_250_[5] = -i_252_;
				is_250_[3] = i_256_ * i_251_ + 8192 >> 14;
				is_250_[7] = i_256_ * i_254_ - (-(i_253_ * i_257_) - 8192) >> 14;
				is_250_[8] = i_253_ * i_251_ + 8192 >> 14;
				is_250_[2] = i_251_ * i_254_ + 8192 >> 14;
				is_250_[1] = i_257_ * i_254_ + i_256_ * -i_253_ + 8192 >> 14;
				is_250_[4] = i_255_ * i_251_ + 8192 >> 14;
				int i_259_ = ((-HashTable.anInt2458 * is_250_[2] + is_250_[0] * -Class182.anInt1876 + 8192
						+ -Class57.anInt667 * is_250_[1]) >> 14);
				int i_260_ = ((-Class182.anInt1876 * is_250_[3]
						- (-(is_250_[4] * -Class57.anInt667) + (-(is_250_[5] * -HashTable.anInt2458) - 8192))) >> 14);
				int i_261_ = ((-Class182.anInt1876 * is_250_[6] + -Class57.anInt667 * is_250_[7]
						+ -HashTable.anInt2458 * is_250_[8] + 8192) >> 14);
				int i_262_ = Class182.anInt1876 + i_259_;
				int i_263_ = i_260_ + Class57.anInt667;
				int i_264_ = HashTable.anInt2458 + i_261_;
				int[] is_265_ = new int[9];
				for (int i_266_ = 0; i_266_ < 3; i_266_++) {
					for (int i_267_ = 0; i_267_ < 3; i_267_++) {
						int i_268_ = 0;
						for (int i_269_ = 0; i_269_ < 3; i_269_++)
							i_268_ += (is_250_[i_266_ * 3 + i_269_] * is_193_[i_267_ * 3 + i_269_]);
						is_265_[i_267_ + i_266_ * 3] = i_268_ + 8192 >> 14;
					}
				}
				int i_270_ = (i_246_ * is_250_[2] + (i_245_ * is_250_[1] + is_250_[0] * i_244_ + 8192) >> 14);
				int i_271_ = ((i_246_ * is_250_[5] + 8192 + i_245_ * is_250_[4] + i_244_ * is_250_[3]) >> 14);
				int i_272_ = ((is_250_[8] * i_246_ + (i_244_ * is_250_[6] + is_250_[7] * i_245_) + 8192) >> 14);
				i_270_ += i_262_;
				i_271_ += i_263_;
				i_272_ += i_264_;
				int[] is_273_ = new int[9];
				for (int i_274_ = 0; i_274_ < 3; i_274_++) {
					for (int i_275_ = 0; i_275_ < 3; i_275_++) {
						int i_276_ = 0;
						for (int i_277_ = 0; i_277_ < 3; i_277_++)
							i_276_ += (is_265_[i_277_ * 3 + i_275_] * is_193_[i_274_ * 3 + i_277_]);
						is_273_[i_274_ * 3 + i_275_] = i_276_ + 8192 >> 14;
					}
				}
				int i_278_ = ((is_193_[0] * i_270_ + (is_193_[1] * i_271_ + i_272_ * is_193_[2]) + 8192) >> 14);
				int i_279_ = ((i_272_ * is_193_[5] + (i_271_ * is_193_[4] + is_193_[3] * i_270_) + 8192) >> 14);
				int i_280_ = ((is_193_[8] * i_272_ + (is_193_[6] * i_270_ + i_271_ * is_193_[7]) + 8192) >> 14);
				i_278_ += i_241_;
				i_279_ += i_242_;
				i_280_ += i_243_;
				for (int i_281_ = 0; i_194_ > i_281_; i_281_++) {
					int i_282_ = is[i_281_];
					if (anIntArrayArray4475.length > i_282_) {
						int[] is_283_ = anIntArrayArray4475[i_282_];
						for (int i_284_ = 0; i_284_ < is_283_.length; i_284_++) {
							int i_285_ = is_283_[i_284_];
							if (aShortArray4453 == null || (aShortArray4453[i_285_] & i_192_) != 0) {
								int i_286_ = ((is_273_[0] * anIntArray4482[i_285_] + anIntArray4505[i_285_] * is_273_[1]
										+ is_273_[2] * anIntArray4489[i_285_] + 8192) >> 14);
								int i_287_ = ((is_273_[4] * anIntArray4505[i_285_]
										+ (is_273_[3] * anIntArray4482[i_285_] + (is_273_[5] * anIntArray4489[i_285_]))
										+ 8192) >> 14);
								int i_288_ = ((is_273_[8] * anIntArray4489[i_285_] + is_273_[6] * anIntArray4482[i_285_]
										- (-(is_273_[7] * anIntArray4505[i_285_]) - 8192)) >> 14);
								i_286_ += i_278_;
								i_287_ += i_279_;
								anIntArray4482[i_285_] = i_286_;
								i_288_ += i_280_;
								anIntArray4505[i_285_] = i_287_;
								anIntArray4489[i_285_] = i_288_;
							}
						}
					}
				}
			}
		} else if (i == 3) {
			if (is_193_ != null) {
				int i_289_ = is_193_[9] << 4;
				int i_290_ = is_193_[10] << 4;
				int i_291_ = is_193_[11] << 4;
				int i_292_ = is_193_[12] << 4;
				int i_293_ = is_193_[13] << 4;
				int i_294_ = is_193_[14] << 4;
				if (StaticMethods.aBoolean5925) {
					int i_295_ = ((is_193_[0] * Class182.anInt1876 + Class57.anInt667 * is_193_[3] + 8192
							+ HashTable.anInt2458 * is_193_[6]) >> 14);
					int i_296_ = ((HashTable.anInt2458 * is_193_[7]
							+ (is_193_[1] * Class182.anInt1876 + Class57.anInt667 * is_193_[4]) + 8192) >> 14);
					int i_297_ = ((Class182.anInt1876 * is_193_[2]
							+ (is_193_[5] * Class57.anInt667 - (-(HashTable.anInt2458 * is_193_[8]) - 8192))) >> 14);
					i_296_ += i_293_;
					i_295_ += i_292_;
					i_297_ += i_294_;
					Class182.anInt1876 = i_295_;
					Class57.anInt667 = i_296_;
					StaticMethods.aBoolean5925 = false;
					HashTable.anInt2458 = i_297_;
				}
				int i_298_ = i_189_ << 15 >> 7;
				int i_299_ = i_190_ << 15 >> 7;
				int i_300_ = i_191_ << 15 >> 7;
				int i_301_ = i_298_ * -Class182.anInt1876 + 8192 >> 14;
				int i_302_ = i_299_ * -Class57.anInt667 + 8192 >> 14;
				int i_303_ = i_300_ * -HashTable.anInt2458 + 8192 >> 14;
				int i_304_ = i_301_ + Class182.anInt1876;
				int i_305_ = i_302_ + Class57.anInt667;
				int i_306_ = HashTable.anInt2458 + i_303_;
				int[] is_307_ = new int[9];
				is_307_[1] = i_298_ * is_193_[3] + 8192 >> 14;
				is_307_[0] = is_193_[0] * i_298_ + 8192 >> 14;
				is_307_[2] = is_193_[6] * i_298_ + 8192 >> 14;
				is_307_[4] = i_299_ * is_193_[4] + 8192 >> 14;
				is_307_[3] = is_193_[1] * i_299_ + 8192 >> 14;
				is_307_[6] = i_300_ * is_193_[2] + 8192 >> 14;
				is_307_[5] = is_193_[7] * i_299_ + 8192 >> 14;
				is_307_[8] = i_300_ * is_193_[8] + 8192 >> 14;
				is_307_[7] = i_300_ * is_193_[5] + 8192 >> 14;
				int i_308_ = i_298_ * i_292_ + 8192 >> 14;
				int i_309_ = i_299_ * i_293_ + 8192 >> 14;
				int i_310_ = i_300_ * i_294_ + 8192 >> 14;
				i_308_ += i_304_;
				i_309_ += i_305_;
				i_310_ += i_306_;
				int[] is_311_ = new int[9];
				for (int i_312_ = 0; i_312_ < 3; i_312_++) {
					for (int i_313_ = 0; i_313_ < 3; i_313_++) {
						int i_314_ = 0;
						for (int i_315_ = 0; i_315_ < 3; i_315_++)
							i_314_ += (is_307_[i_315_ * 3 + i_313_] * is_193_[i_312_ * 3 + i_315_]);
						is_311_[i_312_ * 3 + i_313_] = i_314_ + 8192 >> 14;
					}
				}
				int i_316_ = ((is_193_[2] * i_310_ + is_193_[0] * i_308_ + is_193_[1] * i_309_ + 8192) >> 14);
				int i_317_ = ((i_310_ * is_193_[5] + i_309_ * is_193_[4] + is_193_[3] * i_308_ + 8192) >> 14);
				i_317_ += i_290_;
				int i_318_ = ((i_310_ * is_193_[8] + (i_309_ * is_193_[7] + is_193_[6] * i_308_) + 8192) >> 14);
				i_316_ += i_289_;
				i_318_ += i_291_;
				for (int i_319_ = 0; i_194_ > i_319_; i_319_++) {
					int i_320_ = is[i_319_];
					if (anIntArrayArray4475.length > i_320_) {
						int[] is_321_ = anIntArrayArray4475[i_320_];
						for (int i_322_ = 0; is_321_.length > i_322_; i_322_++) {
							int i_323_ = is_321_[i_322_];
							if (aShortArray4453 == null || (i_192_ & aShortArray4453[i_323_]) != 0) {
								int i_324_ = ((anIntArray4489[i_323_] * is_311_[2] + 8192
										+ (is_311_[0] * anIntArray4482[i_323_]
												+ (anIntArray4505[i_323_] * is_311_[1]))) >> 14);
								int i_325_ = ((anIntArray4489[i_323_] * is_311_[5] + anIntArray4482[i_323_] * is_311_[3]
										+ 8192 + anIntArray4505[i_323_] * is_311_[4]) >> 14);
								int i_326_ = ((anIntArray4489[i_323_] * is_311_[8] + anIntArray4505[i_323_] * is_311_[7]
										+ is_311_[6] * anIntArray4482[i_323_] + 8192) >> 14);
								i_325_ += i_317_;
								i_324_ += i_316_;
								i_326_ += i_318_;
								anIntArray4482[i_323_] = i_324_;
								anIntArray4505[i_323_] = i_325_;
								anIntArray4489[i_323_] = i_326_;
							}
						}
					}
				}
			} else {
				for (int i_327_ = 0; i_327_ < i_194_; i_327_++) {
					int i_328_ = is[i_327_];
					if (anIntArrayArray4475.length > i_328_) {
						int[] is_329_ = anIntArrayArray4475[i_328_];
						for (int i_330_ = 0; i_330_ < is_329_.length; i_330_++) {
							int i_331_ = is_329_[i_330_];
							if (aShortArray4453 == null || (aShortArray4453[i_331_] & i_192_) != 0) {
								anIntArray4482[i_331_] -= Class182.anInt1876;
								anIntArray4505[i_331_] -= Class57.anInt667;
								anIntArray4489[i_331_] -= HashTable.anInt2458;
								anIntArray4482[i_331_] = anIntArray4482[i_331_] * i_189_ >> 7;
								anIntArray4505[i_331_] = anIntArray4505[i_331_] * i_190_ >> 7;
								anIntArray4489[i_331_] = i_191_ * anIntArray4489[i_331_] >> 7;
								anIntArray4482[i_331_] += Class182.anInt1876;
								anIntArray4505[i_331_] += Class57.anInt667;
								anIntArray4489[i_331_] += HashTable.anInt2458;
							}
						}
					}
				}
			}
		} else if (i == 5) {
			if (anIntArrayArray4454 != null) {
				boolean bool_332_ = false;
				for (int i_333_ = 0; i_194_ > i_333_; i_333_++) {
					int i_334_ = is[i_333_];
					if (anIntArrayArray4454.length > i_334_) {
						int[] is_335_ = anIntArrayArray4454[i_334_];
						for (int i_336_ = 0; is_335_.length > i_336_; i_336_++) {
							int i_337_ = is_335_[i_336_];
							if (aShortArray4461 == null || (aShortArray4461[i_337_] & i_192_) != 0) {
								int i_338_ = ((aByteArray4463[i_337_] & 0xff) + i_189_ * 8);
								if (i_338_ < 0)
									i_338_ = 0;
								else if (i_338_ > 255)
									i_338_ = 255;
								aByteArray4463[i_337_] = (byte) i_338_;
							}
						}
						bool_332_ = bool_332_ | is_335_.length > 0;
					}
				}
				if (bool_332_) {
					if (aClass37Array4510 != null) {
						for (int i_339_ = 0; anInt4490 > i_339_; i_339_++) {
							Class37 class37 = aClass37Array4510[i_339_];
							Class271 class271 = aClass271Array4459[i_339_];
							class271.anInt2517 = (class271.anInt2517 & 0xffffff
									| -(aByteArray4463[class37.anInt367] & 0xff) + 255 << 24);
						}
					}
					method1796(1018);
				}
			}
		} else if (i == 7) {
			if (anIntArrayArray4454 != null) {
				boolean bool_340_ = false;
				for (int i_341_ = 0; i_194_ > i_341_; i_341_++) {
					int i_342_ = is[i_341_];
					if (i_342_ < anIntArrayArray4454.length) {
						int[] is_343_ = anIntArrayArray4454[i_342_];
						for (int i_344_ = 0; is_343_.length > i_344_; i_344_++) {
							int i_345_ = is_343_[i_344_];
							if (aShortArray4461 == null || (i_192_ & aShortArray4461[i_345_]) != 0) {
								int i_346_ = aShortArray4477[i_345_] & 0xffff;
								int i_347_ = (i_346_ & 0xfd8d) >> 10;
								int i_348_ = i_346_ >> 7 & 0x7;
								i_347_ = i_347_ + i_189_ & 0x3f;
								i_348_ += i_190_ / 4;
								int i_349_ = i_346_ & 0x7f;
								i_349_ += i_191_;
								if (i_348_ >= 0) {
									if (i_348_ > 7)
										i_348_ = 7;
								} else
									i_348_ = 0;
								if (i_349_ >= 0) {
									if (i_349_ > 127)
										i_349_ = 127;
								} else
									i_349_ = 0;
								aShortArray4477[i_345_] = (short) (Class48.bitOR(i_349_,
										Class48.bitOR(i_347_ << 10, (i_348_ << 7))));
							}
						}
						bool_340_ = bool_340_ | is_343_.length > 0;
					}
				}
				if (bool_340_) {
					if (aClass37Array4510 != null) {
						for (int i_350_ = 0; anInt4490 > i_350_; i_350_++) {
							Class37 class37 = aClass37Array4510[i_350_];
							Class271 class271 = aClass271Array4459[i_350_];
							class271.anInt2517 = ((Class166_Sub1.anIntArray4300[(aShortArray4477[class37.anInt367]
									& 0xffff)]) & 0xffffff | class271.anInt2517 & ~0xffffff);
						}
					}
					method1796(1018);
				}
			}
		} else if (i == 8) {
			if (anIntArrayArray4479 != null) {
				for (int i_351_ = 0; i_194_ > i_351_; i_351_++) {
					int i_352_ = is[i_351_];
					if (i_352_ < anIntArrayArray4479.length) {
						int[] is_353_ = anIntArrayArray4479[i_352_];
						for (int i_354_ = 0; i_354_ < is_353_.length; i_354_++) {
							Class271 class271 = aClass271Array4459[is_353_[i_354_]];
							class271.anInt2511 += i_190_;
							class271.anInt2516 += i_189_;
						}
					}
				}
			}
		} else if (i == 10) {
			if (anIntArrayArray4479 != null) {
				for (int i_355_ = 0; i_194_ > i_355_; i_355_++) {
					int i_356_ = is[i_355_];
					if (i_356_ < anIntArrayArray4479.length) {
						int[] is_357_ = anIntArrayArray4479[i_356_];
						for (int i_358_ = 0; i_358_ < is_357_.length; i_358_++) {
							Class271 class271 = aClass271Array4459[is_357_[i_358_]];
							class271.anInt2513 = i_190_ * class271.anInt2513 >> 7;
							class271.anInt2515 = class271.anInt2515 * i_189_ >> 7;
						}
					}
				}
			}
		} else if (i == 9) {
			if (anIntArrayArray4479 != null) {
				for (int i_359_ = 0; i_359_ < i_194_; i_359_++) {
					int i_360_ = is[i_359_];
					if (i_360_ < anIntArrayArray4479.length) {
						int[] is_361_ = anIntArrayArray4479[i_360_];
						for (int i_362_ = 0; i_362_ < is_361_.length; i_362_++) {
							Class271 class271 = aClass271Array4459[is_361_[i_362_]];
							class271.anInt2514 = class271.anInt2514 + i_189_ & 0x3fff;
						}
					}
				}
			}
		}
	}

	public void method1719(Class373 class373, Class338_Sub5 class338_sub5, int i, int i_363_) {
		if (anInt4474 != 0) {
			Class373_Sub2 class373_sub2 = aHa_Sub1_4492.aClass373_Sub2_3941;
			if (!aBoolean4496)
				method1805((byte) -75);
			Class373_Sub2 class373_sub2_364_ = (Class373_Sub2) class373;
			Class281.aFloat2601 = (class373_sub2_364_.aFloat5595 * class373_sub2.aFloat5601
					+ class373_sub2.aFloat5599 * class373_sub2_364_.aFloat5596
					+ class373_sub2.aFloat5591 * class373_sub2_364_.aFloat5599);
			SubInPacket.aFloat2422 = (class373_sub2_364_.aFloat5594 * class373_sub2.aFloat5591
					+ (class373_sub2.aFloat5599 * class373_sub2_364_.aFloat5590
							+ (class373_sub2_364_.aFloat5593 * class373_sub2.aFloat5601))
					+ class373_sub2.aFloat5594);
			float f = (Class281.aFloat2601 * (float) anInt4497 + SubInPacket.aFloat2422);
			float f_365_ = (SubInPacket.aFloat2422 + Class281.aFloat2601 * (float) anInt4495);
			float f_366_;
			float f_367_;
			if (!(f_365_ < f)) {
				f_366_ = (float) -anInt4498 + f;
				f_367_ = f_365_ + (float) anInt4498;
			} else {
				f_366_ = f_365_ - (float) anInt4498;
				f_367_ = f + (float) anInt4498;
			}
			if (!(aHa_Sub1_4492.aFloat4014 <= f_366_) && !((float) aHa_Sub1_4492.anInt4035 >= f_367_)) {
				Class161.aFloat1672 = (class373_sub2.aFloat5593
						+ ((class373_sub2.aFloat5602 * class373_sub2_364_.aFloat5594)
								+ ((class373_sub2.aFloat5595 * class373_sub2_364_.aFloat5590)
										+ (class373_sub2_364_.aFloat5593 * class373_sub2.aFloat5598))));
				Class207.aFloat2079 = (class373_sub2.aFloat5602 * class373_sub2_364_.aFloat5599
						+ ((class373_sub2_364_.aFloat5596 * class373_sub2.aFloat5595)
								+ (class373_sub2.aFloat5598 * class373_sub2_364_.aFloat5595)));
				float f_368_ = (Class207.aFloat2079 * (float) anInt4497 + Class161.aFloat1672);
				float f_369_ = (Class161.aFloat1672 + (float) anInt4495 * Class207.aFloat2079);
				float f_370_;
				float f_371_;
				if (f_368_ > f_369_) {
					f_371_ = (float) aHa_Sub1_4492.anInt3992 * ((float) -anInt4498 + f_369_);
					f_370_ = ((f_368_ + (float) anInt4498) * (float) aHa_Sub1_4492.anInt3992);
				} else {
					f_370_ = (float) aHa_Sub1_4492.anInt3992 * ((float) anInt4498 + f_369_);
					f_371_ = (float) aHa_Sub1_4492.anInt3992 * ((float) -anInt4498 + f_368_);
				}
				if (!(aHa_Sub1_4492.aFloat4034 <= f_371_ / (float) i)
						&& !(aHa_Sub1_4492.aFloat4018 >= f_370_ / (float) i)) {
					OutputStream_Sub2.aFloat42 = (class373_sub2.aFloat5590
							+ ((class373_sub2_364_.aFloat5590 * class373_sub2.aFloat5596)
									+ (class373_sub2_364_.aFloat5593 * class373_sub2.aFloat5603)
									+ (class373_sub2_364_.aFloat5594 * class373_sub2.aFloat5597)));
					Class288.aFloat2650 = ((class373_sub2.aFloat5597 * class373_sub2_364_.aFloat5599)
							+ ((class373_sub2.aFloat5596 * class373_sub2_364_.aFloat5596)
									+ (class373_sub2.aFloat5603 * class373_sub2_364_.aFloat5595)));
					float f_372_ = (Class288.aFloat2650 * (float) anInt4497 + OutputStream_Sub2.aFloat42);
					float f_373_ = ((float) anInt4495 * Class288.aFloat2650 + OutputStream_Sub2.aFloat42);
					float f_374_;
					float f_375_;
					if (!(f_373_ < f_372_)) {
						f_374_ = ((float) aHa_Sub1_4492.anInt3993 * (f_373_ + (float) anInt4498));
						f_375_ = ((float) aHa_Sub1_4492.anInt3993 * ((float) -anInt4498 + f_372_));
					} else {
						f_374_ = ((f_372_ + (float) anInt4498) * (float) aHa_Sub1_4492.anInt3993);
						f_375_ = ((float) aHa_Sub1_4492.anInt3993 * (f_373_ - (float) anInt4498));
					}
					if (!(f_375_ / (float) i >= aHa_Sub1_4492.aFloat4044)
							&& !(f_374_ / (float) i <= aHa_Sub1_4492.aFloat3976)) {
						if (class338_sub5 != null || aClass37Array4510 != null) {
							Class296_Sub34_Sub2.aFloat6106 = ((class373_sub2_364_.aFloat5601 * class373_sub2.aFloat5597)
									+ ((class373_sub2_364_.aFloat5603 * class373_sub2.aFloat5596)
											+ (class373_sub2_364_.aFloat5598 * class373_sub2.aFloat5603)));
							Class16_Sub3_Sub1.aFloat5806 = ((class373_sub2_364_.aFloat5591 * class373_sub2.aFloat5597)
									+ ((class373_sub2.aFloat5603 * class373_sub2_364_.aFloat5602)
											+ (class373_sub2_364_.aFloat5597 * class373_sub2.aFloat5596)));
							Class338_Sub3_Sub4_Sub1.aFloat6658 = ((class373_sub2_364_.aFloat5597
									* class373_sub2.aFloat5595)
									+ (class373_sub2_364_.aFloat5602 * class373_sub2.aFloat5598)
									+ (class373_sub2.aFloat5602 * class373_sub2_364_.aFloat5591));
							World.aFloat1161 = ((class373_sub2.aFloat5599 * class373_sub2_364_.aFloat5603)
									+ (class373_sub2.aFloat5601 * class373_sub2_364_.aFloat5598)
									+ (class373_sub2_364_.aFloat5601 * class373_sub2.aFloat5591));
							Class240.aFloat2263 = ((class373_sub2.aFloat5595 * class373_sub2_364_.aFloat5603)
									+ (class373_sub2.aFloat5598 * class373_sub2_364_.aFloat5598)
									+ (class373_sub2.aFloat5602 * class373_sub2_364_.aFloat5601));
							StaticMethods.aFloat6081 = ((class373_sub2.aFloat5591 * class373_sub2_364_.aFloat5591)
									+ ((class373_sub2.aFloat5599 * class373_sub2_364_.aFloat5597)
											+ (class373_sub2.aFloat5601 * class373_sub2_364_.aFloat5602)));
						}
						if (class338_sub5 != null) {
							int i_376_ = anInt4494 + anInt4452 >> 1;
							int i_377_ = anInt4485 + anInt4503 >> 1;
							int i_378_ = (int) (((float) anInt4497 * Class207.aFloat2079)
									+ (Class161.aFloat1672 + (Class240.aFloat2263 * (float) i_376_))
									+ ((float) i_377_ * (Class338_Sub3_Sub4_Sub1.aFloat6658)));
							int i_379_ = (int) ((Class16_Sub3_Sub1.aFloat5806 * (float) i_377_)
									+ ((Class288.aFloat2650 * (float) anInt4497) + (OutputStream_Sub2.aFloat42
											+ ((float) i_376_ * (Class296_Sub34_Sub2.aFloat6106)))));
							int i_380_ = (int) (((float) i_377_ * StaticMethods.aFloat6081)
									+ (((float) anInt4497 * Class281.aFloat2601)
											+ ((World.aFloat1161 * (float) i_376_) + SubInPacket.aFloat2422)));
							int i_381_ = (int) (Class161.aFloat1672 + (float) i_376_ * Class240.aFloat2263
									+ (Class207.aFloat2079 * (float) anInt4495)
									+ ((float) i_377_ * (Class338_Sub3_Sub4_Sub1.aFloat6658)));
							int i_382_ = (int) (OutputStream_Sub2.aFloat42
									+ (Class296_Sub34_Sub2.aFloat6106 * (float) i_376_)
									+ ((float) anInt4495 * Class288.aFloat2650)
									+ ((float) i_377_ * Class16_Sub3_Sub1.aFloat5806));
							class338_sub5.anInt5221 = (i_382_ * aHa_Sub1_4492.anInt3993 / i + aHa_Sub1_4492.anInt3988);
							class338_sub5.anInt5225 = (aHa_Sub1_4492.anInt3992 * i_378_ / i + aHa_Sub1_4492.anInt3970);
							class338_sub5.anInt5222 = (aHa_Sub1_4492.anInt3970 + i_381_ * aHa_Sub1_4492.anInt3992 / i);
							int i_383_ = (int) ((StaticMethods.aFloat6081 * (float) i_377_)
									+ (((float) anInt4495 * Class281.aFloat2601)
											+ (SubInPacket.aFloat2422 + ((float) i_376_ * World.aFloat1161))));
							class338_sub5.anInt5223 = (aHa_Sub1_4492.anInt3993 * i_379_ / i + aHa_Sub1_4492.anInt3988);
							if (aHa_Sub1_4492.anInt4035 <= i_380_ || aHa_Sub1_4492.anInt4035 <= i_383_) {
								class338_sub5.anInt5226 = ((aHa_Sub1_4492.anInt3992 * (anInt4498 + i_378_) / i)
										+ aHa_Sub1_4492.anInt3970 - class338_sub5.anInt5225);
								class338_sub5.aBoolean5224 = true;
							}
						}
						aHa_Sub1_4492.method1117(123, (float) i);
						aHa_Sub1_4492.method1124(16);
						aHa_Sub1_4492.method1136(class373_sub2_364_, 11);
						method1801((byte) 67);
						method1797(-123);
					}
				}
			}
		}
	}

	public boolean NA() {
		if (anIntArrayArray4475 == null)
			return false;
		for (int i = 0; anInt4478 > i; i++) {
			anIntArray4482[i] <<= 4;
			anIntArray4505[i] <<= 4;
			anIntArray4489[i] <<= 4;
		}
		Class57.anInt667 = 0;
		Class182.anInt1876 = 0;
		HashTable.anInt2458 = 0;
		return true;
	}

	public void method1730() {
		/* empty */
	}

	private short method1800(int i, int i_384_, float f, int i_385_, int i_386_, long l, Mesh class132, byte i_387_,
			int i_388_, int i_389_, float f_390_) {
		int i_391_ = anIntArray4472[i_389_];
		int i_392_ = anIntArray4472[i_389_ + 1];
		int i_393_ = 0;
		for (int i_394_ = i_391_; i_394_ < i_392_; i_394_++) {
			short i_395_ = aShortArray4460[i_394_];
			if (i_395_ == 0) {
				i_393_ = i_394_;
				break;
			}
			if (l == Class107.aLongArray3573[i_394_])
				return (short) (i_395_ - 1);
		}
		aShortArray4460[i_393_] = (short) (anInt4474 + 1);
		Class107.aLongArray3573[i_393_] = l;
		aShortArray4451[anInt4474] = (short) i_388_;
		aShortArray4455[anInt4474] = (short) i_389_;
		aShortArray4507[anInt4474] = (short) i_385_;
		aShortArray4484[anInt4474] = (short) i;
		aShortArray4483[anInt4474] = (short) i_386_;
		aByteArray4506[anInt4474] = (byte) i_384_;
		aFloatArray4466[anInt4474] = f;
		if (i_387_ != -64)
			return (short) -26;
		aFloatArray4462[anInt4474] = f_390_;
		return (short) anInt4474++;
	}

	public void method1726(Class373 class373, int i, boolean bool) {
		if (aShortArray4453 != null) {
			int[] is = new int[3];
			for (int i_396_ = 0; i_396_ < anInt4468; i_396_++) {
				if ((aShortArray4453[i_396_] & i) != 0) {
					if (bool)
						class373.method3905(anIntArray4482[i_396_], anIntArray4505[i_396_], anIntArray4489[i_396_], is);
					else
						class373.method3903(anIntArray4482[i_396_], anIntArray4505[i_396_], anIntArray4489[i_396_], is);
					anIntArray4482[i_396_] = is[0];
					anIntArray4505[i_396_] = is[1];
					anIntArray4489[i_396_] = is[2];
				}
			}
		}
	}

	public void p(int i, int i_397_, s var_s, s var_s_398_, int i_399_, int i_400_, int i_401_) {
		if (!aBoolean4496)
			method1805((byte) -46);
		int i_402_ = anInt4494 + i_399_;
		int i_403_ = anInt4452 + i_399_;
		int i_404_ = anInt4503 + i_401_;
		int i_405_ = i_401_ + anInt4485;
		if (i != 1 && i != 2 && i != 3 && i != 5
				|| (i_402_ >= 0 && (i_403_ + var_s.anInt2836 >> var_s.anInt2835 < var_s.anInt2832) && i_404_ >= 0
						&& (var_s.anInt2834 > i_405_ + var_s.anInt2836 >> var_s.anInt2835))) {
			if (i == 4 || i == 5) {
				if (var_s_398_ == null || (i_402_ < 0
						|| (var_s_398_.anInt2836 + i_403_ >> var_s_398_.anInt2835) >= var_s_398_.anInt2832 || i_404_ < 0
						|| (var_s_398_.anInt2834 <= (i_405_ + var_s_398_.anInt2836 >> var_s_398_.anInt2835))))
					return;
			} else {
				i_402_ >>= var_s.anInt2835;
				i_403_ = i_403_ + (var_s.anInt2836 - 1) >> var_s.anInt2835;
				i_404_ >>= var_s.anInt2835;
				i_405_ = i_405_ + (var_s.anInt2836 - 1) >> var_s.anInt2835;
				if (i_400_ == var_s.method3355(i_404_, (byte) -106, i_402_)
						&& var_s.method3355(i_404_, (byte) -105, i_403_) == i_400_
						&& var_s.method3355(i_405_, (byte) -109, i_402_) == i_400_
						&& var_s.method3355(i_405_, (byte) -128, i_403_) == i_400_)
					return;
			}
			if (i != 1) {
				if (i != 2) {
					if (i == 3) {
						int i_406_ = (i_397_ & 0xff) * 4;
						int i_407_ = ((i_397_ & 0xff8e) >> 8) * 4;
						int i_408_ = i_397_ >> 16 << 6 & 0x3fc0;
						int i_409_ = i_397_ >> 24 << 6 & 0x3fc0;
						if (-(i_406_ >> 1) + i_399_ < 0
								|| (var_s.anInt2832 << var_s.anInt2835 <= (i_406_ >> 1) + (i_399_ + var_s.anInt2836))
								|| -(i_407_ >> 1) + i_401_ < 0
								|| (i_401_ + ((i_407_ >> 1) + var_s.anInt2836) >= var_s.anInt2834 << var_s.anInt2835))
							return;
						this.method1734(i_406_, var_s, i_400_, i_408_, i_399_, i_409_, i_401_, i_407_, 65535);
					} else if (i == 4) {
						int i_410_ = -anInt4497 + anInt4495;
						for (int i_411_ = 0; anInt4468 > i_411_; i_411_++)
							anIntArray4505[i_411_] = (i_410_ + anIntArray4505[i_411_] - i_400_ + (var_s_398_
									.method3349(0, anIntArray4489[i_411_] + i_401_, anIntArray4482[i_411_] + i_399_)));
					} else if (i == 5) {
						int i_412_ = -anInt4497 + anInt4495;
						for (int i_413_ = 0; anInt4468 > i_413_; i_413_++) {
							int i_414_ = i_399_ + anIntArray4482[i_413_];
							int i_415_ = anIntArray4489[i_413_] + i_401_;
							int i_416_ = var_s.method3349(0, i_415_, i_414_);
							int i_417_ = var_s_398_.method3349(0, i_415_, i_414_);
							int i_418_ = -i_397_ + i_416_ - i_417_;
							anIntArray4505[i_413_] = ((anIntArray4505[i_413_] << 8) / i_412_ * i_418_ >> 8) - i_400_
									+ i_416_;
						}
					}
				} else {
					int i_419_ = anInt4497;
					if (i_419_ == 0)
						return;
					for (int i_420_ = 0; i_420_ < anInt4468; i_420_++) {
						int i_421_ = (anIntArray4505[i_420_] << 16) / i_419_;
						if (i_397_ > i_421_)
							anIntArray4505[i_420_] = (anIntArray4505[i_420_]
									+ ((-i_421_ + i_397_) * ((var_s.method3349(0, anIntArray4489[i_420_] + i_401_,
											i_399_ + anIntArray4482[i_420_])) - i_400_) / i_397_));
					}
				}
			} else {
				for (int i_422_ = 0; anInt4468 > i_422_; i_422_++)
					anIntArray4505[i_422_] = (anIntArray4505[i_422_]
							+ (var_s.method3349(0, anIntArray4489[i_422_] + i_401_, anIntArray4482[i_422_] + i_399_)
									- i_400_));
			}
			method1806(-5);
			aBoolean4496 = false;
		}
	}

	private void method1801(byte i) {
		if (anInt4493 != 0) {
			if (i != 67)
				aClass307_4486 = null;
			if (method1811(12) && method1808(-9359)) {
				aHa_Sub1_4492.method1178((aClass307_4467.anInterface15_Impl2_2752), (byte) -128, 0);
				aHa_Sub1_4492.method1178((aClass307_4486.anInterface15_Impl2_2752), (byte) -117, 1);
				aHa_Sub1_4492.method1178((aClass307_4471.anInterface15_Impl2_2752), (byte) -119, 2);
				boolean bool;
				if ((anInt4481 & 0x37) == 0) {
					aHa_Sub1_4492.method1137(4, false);
					bool = false;
					aHa_Sub1_4492.method1209(aHa_Sub1_4492.aClass127_4051, (byte) -127);
				} else {
					aHa_Sub1_4492.method1137(4, true);
					bool = true;
					aHa_Sub1_4492.method1178((aClass307_4480.anInterface15_Impl2_2752), (byte) -120, 3);
					aHa_Sub1_4492.method1209(aHa_Sub1_4492.aClass127_4047, (byte) -121);
				}
				for (int i_423_ = 0; anIntArray4511.length > i_423_; i_423_++) {
					int i_424_ = anIntArray4465[i_423_];
					int i_425_ = anIntArray4465[i_423_ + 1];
					int i_426_ = aShortArray4512[i_424_] & 0xffff;
					if (i_426_ == 65535)
						i_426_ = -1;
					aHa_Sub1_4492.method1133(true, i_426_, i ^ 0x43, bool);
					aHa_Sub1_4492.method1232(Class166_Sub1.aClass289_4298, (aClass293_4501.anInterface15_Impl1_2683),
							anIntArray4491[i_423_], -i_424_ + i_425_, i_424_ * 3, 106, anIntArray4511[i_423_]);
				}
			}
			method1807(29055);
		}
	}

	public EmissiveTriangle[] method1735() {
		return aClass89Array4487;
	}

	public Model method1728(byte i, int i_427_, boolean bool) {
		boolean bool_428_ = false;
		Class178_Sub3 class178_sub3_429_;
		Class178_Sub3 class178_sub3_430_;
		if (i > 0 && i <= 7) {
			class178_sub3_429_ = aHa_Sub1_4492.aClass178_Sub3Array4056[i - 1];
			class178_sub3_430_ = aHa_Sub1_4492.aClass178_Sub3Array4058[i - 1];
			bool_428_ = true;
		} else
			class178_sub3_429_ = class178_sub3_430_ = new Class178_Sub3(aHa_Sub1_4492, 0, 0, true, false);
		return method1809(class178_sub3_430_, i_427_, class178_sub3_429_, bool, true, bool_428_);
	}

	public void H(int i, int i_431_, int i_432_) {
		for (int i_433_ = 0; i_433_ < anInt4468; i_433_++) {
			if (i != 0)
				anIntArray4482[i_433_] += i;
			if (i_431_ != 0)
				anIntArray4505[i_433_] += i_431_;
			if (i_432_ != 0)
				anIntArray4489[i_433_] += i_432_;
		}
		method1806(-5);
		aBoolean4496 = false;
	}

	private boolean method1802(int i, boolean bool, int i_434_, Class373 class373, byte i_435_, int i_436_,
			int i_437_) {
		Class373_Sub2 class373_sub2 = (Class373_Sub2) class373;
		Class373_Sub2 class373_sub2_438_ = aHa_Sub1_4492.aClass373_Sub2_3941;
		float f = (class373_sub2_438_.aFloat5602 * class373_sub2.aFloat5594
				+ (class373_sub2_438_.aFloat5598 * class373_sub2.aFloat5593
						+ class373_sub2_438_.aFloat5595 * class373_sub2.aFloat5590)
				+ class373_sub2_438_.aFloat5593);
		float f_439_ = (class373_sub2.aFloat5593 * class373_sub2_438_.aFloat5603
				+ class373_sub2_438_.aFloat5596 * class373_sub2.aFloat5590
				+ class373_sub2.aFloat5594 * class373_sub2_438_.aFloat5597 + class373_sub2_438_.aFloat5590);
		float f_440_ = (class373_sub2.aFloat5590 * class373_sub2_438_.aFloat5599
				+ class373_sub2.aFloat5593 * class373_sub2_438_.aFloat5601
				+ class373_sub2.aFloat5594 * class373_sub2_438_.aFloat5591 + class373_sub2_438_.aFloat5594);
		Class296_Sub34_Sub2.aFloat6106 = (class373_sub2_438_.aFloat5603 * class373_sub2.aFloat5598
				+ class373_sub2.aFloat5603 * class373_sub2_438_.aFloat5596
				+ class373_sub2.aFloat5601 * class373_sub2_438_.aFloat5597);
		World.aFloat1161 = (class373_sub2.aFloat5598 * class373_sub2_438_.aFloat5601
				+ class373_sub2_438_.aFloat5599 * class373_sub2.aFloat5603
				+ class373_sub2.aFloat5601 * class373_sub2_438_.aFloat5591);
		Class281.aFloat2601 = (class373_sub2_438_.aFloat5599 * class373_sub2.aFloat5596
				+ class373_sub2_438_.aFloat5601 * class373_sub2.aFloat5595
				+ class373_sub2.aFloat5599 * class373_sub2_438_.aFloat5591);
		StaticMethods.aFloat6081 = (class373_sub2.aFloat5597 * class373_sub2_438_.aFloat5599
				+ class373_sub2_438_.aFloat5601 * class373_sub2.aFloat5602
				+ class373_sub2_438_.aFloat5591 * class373_sub2.aFloat5591);
		Class207.aFloat2079 = (class373_sub2_438_.aFloat5602 * class373_sub2.aFloat5599
				+ (class373_sub2_438_.aFloat5598 * class373_sub2.aFloat5595
						+ class373_sub2.aFloat5596 * class373_sub2_438_.aFloat5595));
		Class240.aFloat2263 = (class373_sub2.aFloat5601 * class373_sub2_438_.aFloat5602
				+ (class373_sub2.aFloat5603 * class373_sub2_438_.aFloat5595
						+ class373_sub2_438_.aFloat5598 * class373_sub2.aFloat5598));
		Class338_Sub3_Sub4_Sub1.aFloat6658 = (class373_sub2.aFloat5591 * class373_sub2_438_.aFloat5602
				+ (class373_sub2.aFloat5597 * class373_sub2_438_.aFloat5595
						+ class373_sub2.aFloat5602 * class373_sub2_438_.aFloat5598));
		Class288.aFloat2650 = (class373_sub2.aFloat5596 * class373_sub2_438_.aFloat5596
				+ class373_sub2.aFloat5595 * class373_sub2_438_.aFloat5603
				+ class373_sub2.aFloat5599 * class373_sub2_438_.aFloat5597);
		Class16_Sub3_Sub1.aFloat5806 = (class373_sub2.aFloat5591 * class373_sub2_438_.aFloat5597
				+ (class373_sub2_438_.aFloat5603 * class373_sub2.aFloat5602
						+ class373_sub2_438_.aFloat5596 * class373_sub2.aFloat5597));
		boolean bool_441_ = false;
		float f_442_ = 3.4028235E38F;
		float f_443_ = -3.4028235E38F;
		float f_444_ = 3.4028235E38F;
		float f_445_ = -3.4028235E38F;
		int i_446_ = aHa_Sub1_4492.anInt3992;
		int i_447_ = aHa_Sub1_4492.anInt3993;
		if (!aBoolean4496)
			method1805((byte) -24);
		int i_448_ = anInt4452 - anInt4494 >> 1;
		int i_449_ = -anInt4497 + anInt4495 >> 1;
		int i_450_ = anInt4485 - anInt4503 >> 1;
		int i_451_ = i_448_ + anInt4494;
		int i_452_ = anInt4497 + i_449_;
		int i_453_ = anInt4503 + i_450_;
		int i_454_ = -(i_448_ << i_434_) + i_451_;
		int i_455_ = -(i_449_ << i_434_) + i_452_;
		int i_456_ = i_453_ - (i_450_ << i_434_);
		int i_457_ = i_451_ + (i_448_ << i_434_);
		int i_458_ = (i_449_ << i_434_) + i_452_;
		Node.anIntArray2693[0] = i_454_;
		int i_459_ = (i_450_ << i_434_) + i_453_;
		Class296_Sub51_Sub7.anIntArray6370[0] = i_455_;
		Node.anIntArray2693[1] = i_457_;
		Class296_Sub56.anIntArray5081[0] = i_456_;
		Class296_Sub51_Sub7.anIntArray6370[1] = i_455_;
		Node.anIntArray2693[2] = i_454_;
		Class296_Sub56.anIntArray5081[1] = i_456_;
		Class296_Sub51_Sub7.anIntArray6370[2] = i_458_;
		Node.anIntArray2693[3] = i_457_;
		if (i_435_ > -55)
			aClass293_4501 = null;
		Class296_Sub56.anIntArray5081[2] = i_456_;
		Class296_Sub51_Sub7.anIntArray6370[3] = i_458_;
		Class296_Sub56.anIntArray5081[3] = i_456_;
		Node.anIntArray2693[4] = i_454_;
		Class296_Sub51_Sub7.anIntArray6370[4] = i_455_;
		Node.anIntArray2693[5] = i_457_;
		Class296_Sub56.anIntArray5081[4] = i_459_;
		Class296_Sub51_Sub7.anIntArray6370[5] = i_455_;
		Class296_Sub56.anIntArray5081[5] = i_459_;
		Node.anIntArray2693[6] = i_454_;
		Class296_Sub51_Sub7.anIntArray6370[6] = i_458_;
		Node.anIntArray2693[7] = i_457_;
		Class296_Sub56.anIntArray5081[6] = i_459_;
		Class296_Sub51_Sub7.anIntArray6370[7] = i_458_;
		Class296_Sub56.anIntArray5081[7] = i_459_;
		for (int i_460_ = 0; i_460_ < 8; i_460_++) {
			float f_461_ = (float) Class296_Sub51_Sub7.anIntArray6370[i_460_];
			float f_462_ = (float) Node.anIntArray2693[i_460_];
			float f_463_ = (float) Class296_Sub56.anIntArray5081[i_460_];
			float f_464_ = (f_463_ * Class338_Sub3_Sub4_Sub1.aFloat6658
					+ (f_461_ * Class207.aFloat2079 + Class240.aFloat2263 * f_462_) + f);
			float f_465_ = f_440_ + (f_461_ * Class281.aFloat2601 + f_462_ * World.aFloat1161
					+ StaticMethods.aFloat6081 * f_463_);
			float f_466_ = (Class16_Sub3_Sub1.aFloat5806 * f_463_
					+ (f_462_ * Class296_Sub34_Sub2.aFloat6106 + f_461_ * Class288.aFloat2650) + f_439_);
			if (f_465_ >= (float) aHa_Sub1_4492.anInt4035) {
				if (i_436_ > 0)
					f_465_ = (float) i_436_;
				float f_467_ = ((float) aHa_Sub1_4492.anInt3970 + f_464_ * (float) i_446_ / f_465_);
				if (f_442_ > f_467_)
					f_442_ = f_467_;
				float f_468_ = ((float) aHa_Sub1_4492.anInt3988 + (float) i_447_ * f_466_ / f_465_);
				if (f_467_ > f_443_)
					f_443_ = f_467_;
				if (f_468_ > f_445_)
					f_445_ = f_468_;
				bool_441_ = true;
				if (f_444_ > f_468_)
					f_444_ = f_468_;
			}
		}
		if (bool_441_ && f_442_ < (float) i && (float) i < f_443_ && f_444_ < (float) i_437_
				&& f_445_ > (float) i_437_) {
			if (bool)
				return true;
			if (Class296_Sub30.anIntArray4821.length < anInt4474) {
				Class196.anIntArray1989 = new int[anInt4474];
				Class296_Sub30.anIntArray4821 = new int[anInt4474];
			}
			for (int i_469_ = 0; i_469_ < anInt4468; i_469_++) {
				float f_470_ = (float) anIntArray4482[i_469_];
				float f_471_ = (float) anIntArray4489[i_469_];
				float f_472_ = (float) anIntArray4505[i_469_];
				float f_473_ = (Class338_Sub3_Sub4_Sub1.aFloat6658 * f_471_
						+ (f_470_ * Class240.aFloat2263 + Class207.aFloat2079 * f_472_) + f);
				float f_474_ = (f_471_ * Class16_Sub3_Sub1.aFloat5806
						+ (f_472_ * Class288.aFloat2650 + f_470_ * Class296_Sub34_Sub2.aFloat6106) + f_439_);
				float f_475_ = f_440_ + (f_471_ * StaticMethods.aFloat6081
						+ (f_472_ * Class281.aFloat2601 + World.aFloat1161 * f_470_));
				if (!((float) aHa_Sub1_4492.anInt4035 <= f_475_)) {
					int i_476_ = anIntArray4472[i_469_];
					int i_477_ = anIntArray4472[i_469_ + 1];
					for (int i_478_ = i_476_; i_478_ < i_477_; i_478_++) {
						int i_479_ = aShortArray4460[i_478_] - 1;
						if (i_479_ == -1)
							break;
						Class296_Sub30.anIntArray4821[(aShortArray4460[i_478_] - 1)] = -999999;
					}
				} else {
					if (i_436_ > 0)
						f_475_ = (float) i_436_;
					int i_480_ = (int) ((float) aHa_Sub1_4492.anInt3970 + (float) i_446_ * f_473_ / f_475_);
					int i_481_ = (int) ((float) i_447_ * f_474_ / f_475_ + (float) aHa_Sub1_4492.anInt3988);
					int i_482_ = anIntArray4472[i_469_];
					int i_483_ = anIntArray4472[i_469_ + 1];
					for (int i_484_ = i_482_; i_484_ < i_483_; i_484_++) {
						int i_485_ = aShortArray4460[i_484_] - 1;
						if (i_485_ == -1)
							break;
						Class296_Sub30.anIntArray4821[i_485_] = i_480_;
						Class196.anIntArray1989[i_485_] = i_481_;
					}
				}
			}
			for (int i_486_ = 0; i_486_ < anInt4508; i_486_++) {
				if ((Class296_Sub30.anIntArray4821[aShortArray4464[i_486_]] != -999999)
						&& (Class296_Sub30.anIntArray4821[aShortArray4509[i_486_]] != -999999)
						&& (Class296_Sub30.anIntArray4821[aShortArray4488[i_486_]] != -999999)
						&& (method1798(Class196.anIntArray1989[aShortArray4509[i_486_]],
								Class196.anIntArray1989[aShortArray4488[i_486_]], -34,
								Class196.anIntArray1989[aShortArray4464[i_486_]],
								(Class296_Sub30.anIntArray4821[aShortArray4488[i_486_]]),
								(Class296_Sub30.anIntArray4821[aShortArray4464[i_486_]]),
								(Class296_Sub30.anIntArray4821[aShortArray4509[i_486_]]), i_437_, i)))
					return true;
			}
		}
		return false;
	}

	public void method1736(Model class178, int i, int i_487_, int i_488_, boolean bool) {
		Class178_Sub3 class178_sub3_489_ = (Class178_Sub3) class178;
		if (anInt4508 != 0 && class178_sub3_489_.anInt4508 != 0) {
			int i_490_ = class178_sub3_489_.anInt4468;
			int[] is = class178_sub3_489_.anIntArray4482;
			int[] is_491_ = class178_sub3_489_.anIntArray4505;
			int[] is_492_ = class178_sub3_489_.anIntArray4489;
			short[] is_493_ = class178_sub3_489_.aShortArray4507;
			short[] is_494_ = class178_sub3_489_.aShortArray4484;
			short[] is_495_ = class178_sub3_489_.aShortArray4483;
			byte[] is_496_ = class178_sub3_489_.aByteArray4506;
			short[] is_497_;
			byte[] is_498_;
			short[] is_499_;
			short[] is_500_;
			if (aClass223_4473 != null) {
				is_499_ = aClass223_4473.aShortArray2165;
				is_498_ = aClass223_4473.aByteArray2166;
				is_497_ = aClass223_4473.aShortArray2164;
				is_500_ = aClass223_4473.aShortArray2163;
			} else {
				is_497_ = null;
				is_498_ = null;
				is_499_ = null;
				is_500_ = null;
			}
			short[] is_501_;
			short[] is_502_;
			short[] is_503_;
			byte[] is_504_;
			if (class178_sub3_489_.aClass223_4473 != null) {
				is_504_ = class178_sub3_489_.aClass223_4473.aByteArray2166;
				is_501_ = class178_sub3_489_.aClass223_4473.aShortArray2165;
				is_503_ = class178_sub3_489_.aClass223_4473.aShortArray2164;
				is_502_ = class178_sub3_489_.aClass223_4473.aShortArray2163;
			} else {
				is_501_ = null;
				is_502_ = null;
				is_503_ = null;
				is_504_ = null;
			}
			int[] is_505_ = class178_sub3_489_.anIntArray4472;
			short[] is_506_ = class178_sub3_489_.aShortArray4460;
			if (!class178_sub3_489_.aBoolean4496)
				class178_sub3_489_.method1805((byte) -111);
			int i_507_ = class178_sub3_489_.anInt4497;
			int i_508_ = class178_sub3_489_.anInt4495;
			int i_509_ = class178_sub3_489_.anInt4494;
			int i_510_ = class178_sub3_489_.anInt4452;
			int i_511_ = class178_sub3_489_.anInt4503;
			int i_512_ = class178_sub3_489_.anInt4485;
			for (int i_513_ = 0; i_513_ < anInt4468; i_513_++) {
				int i_514_ = -i_487_ + anIntArray4505[i_513_];
				if (i_514_ >= i_507_ && i_514_ <= i_508_) {
					int i_515_ = -i + anIntArray4482[i_513_];
					if (i_515_ >= i_509_ && i_510_ >= i_515_) {
						int i_516_ = anIntArray4489[i_513_] - i_488_;
						if (i_516_ >= i_511_ && i_512_ >= i_516_) {
							int i_517_ = -1;
							int i_518_ = anIntArray4472[i_513_];
							int i_519_ = anIntArray4472[i_513_ + 1];
							for (int i_520_ = i_518_; i_520_ < i_519_; i_520_++) {
								i_517_ = aShortArray4460[i_520_] - 1;
								if (i_517_ == -1 || aByteArray4506[i_517_] != 0)
									break;
							}
							if (i_517_ != -1) {
								for (int i_521_ = 0; i_490_ > i_521_; i_521_++) {
									if (is[i_521_] == i_515_ && is_492_[i_521_] == i_516_
											&& is_491_[i_521_] == i_514_) {
										int i_522_ = -1;
										i_518_ = is_505_[i_521_];
										i_519_ = is_505_[i_521_ + 1];
										for (int i_523_ = i_518_; i_519_ > i_523_; i_523_++) {
											i_522_ = is_506_[i_523_] - 1;
											if (i_522_ == -1 || is_496_[i_522_] != 0)
												break;
										}
										if (i_522_ != -1) {
											if (is_499_ == null) {
												aClass223_4473 = new Class223();
												is_499_ = aClass223_4473.aShortArray2165 = (Class331
														.method3412(aShortArray4507, true));
												is_497_ = aClass223_4473.aShortArray2164 = (Class331
														.method3412(aShortArray4484, true));
												is_500_ = aClass223_4473.aShortArray2163 = (Class331
														.method3412(aShortArray4483, true));
												is_498_ = aClass223_4473.aByteArray2166 = (Class296_Sub39_Sub19
														.method2901((byte) -119, aByteArray4506));
											}
											if (is_501_ == null) {
												Class223 class223 = (class178_sub3_489_.aClass223_4473 = new Class223());
												is_501_ = class223.aShortArray2165 = (Class331.method3412(is_493_,
														true));
												is_503_ = class223.aShortArray2164 = (Class331.method3412(is_494_,
														true));
												is_502_ = class223.aShortArray2163 = (Class331.method3412(is_495_,
														true));
												is_504_ = class223.aByteArray2166 = (Class296_Sub39_Sub19
														.method2901((byte) -124, is_496_));
											}
											short i_524_ = aShortArray4507[i_517_];
											short i_525_ = aShortArray4484[i_517_];
											short i_526_ = aShortArray4483[i_517_];
											byte i_527_ = aByteArray4506[i_517_];
											i_519_ = is_505_[i_521_ + 1];
											i_518_ = is_505_[i_521_];
											for (int i_528_ = i_518_; i_519_ > i_528_; i_528_++) {
												int i_529_ = is_506_[i_528_] - 1;
												if (i_529_ == -1)
													break;
												if (is_504_[i_529_] != 0) {
													is_501_[i_529_] += i_524_;
													is_503_[i_529_] += i_525_;
													is_502_[i_529_] += i_526_;
													is_504_[i_529_] += i_527_;
												}
											}
											i_525_ = is_494_[i_522_];
											i_518_ = anIntArray4472[i_513_];
											i_526_ = is_495_[i_522_];
											i_527_ = is_496_[i_522_];
											i_524_ = is_493_[i_522_];
											i_519_ = anIntArray4472[i_513_ + 1];
											for (int i_530_ = i_518_; i_519_ > i_530_; i_530_++) {
												int i_531_ = (aShortArray4460[i_530_] - 1);
												if (i_531_ == -1)
													break;
												if (is_498_[i_531_] != 0) {
													is_499_[i_531_] += i_524_;
													is_497_[i_531_] += i_525_;
													is_500_[i_531_] += i_526_;
													is_498_[i_531_] += i_527_;
												}
											}
											class178_sub3_489_.method1804(-1);
											method1804(-1);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public int V() {
		if (!aBoolean4496)
			method1805((byte) -39);
		return anInt4494;
	}

	public EffectiveVertex[] method1729() {
		return aClass232Array4504;
	}

	private void method1803(int i) {
		if (i == 21924) {
			if (aClass293_4501 != null)
				aClass293_4501.aBoolean2681 = false;
		}
	}

	public void method1715(Class373 class373, Class338_Sub5 class338_sub5, int i) {
		if (anInt4474 != 0) {
			Class373_Sub2 class373_sub2 = aHa_Sub1_4492.aClass373_Sub2_3941;
			Class373_Sub2 class373_sub2_532_ = (Class373_Sub2) class373;
			if (!aBoolean4496)
				method1805((byte) -128);
			SubInPacket.aFloat2422 = (class373_sub2.aFloat5594
					+ (class373_sub2.aFloat5591 * class373_sub2_532_.aFloat5594
							+ ((class373_sub2.aFloat5601 * class373_sub2_532_.aFloat5593)
									+ (class373_sub2.aFloat5599 * class373_sub2_532_.aFloat5590))));
			Class281.aFloat2601 = (class373_sub2.aFloat5591 * class373_sub2_532_.aFloat5599
					+ (class373_sub2_532_.aFloat5595 * class373_sub2.aFloat5601
							+ (class373_sub2.aFloat5599 * class373_sub2_532_.aFloat5596)));
			float f = (SubInPacket.aFloat2422 + (float) anInt4497 * Class281.aFloat2601);
			float f_533_ = (SubInPacket.aFloat2422 + (float) anInt4495 * Class281.aFloat2601);
			float f_534_;
			float f_535_;
			if (f_533_ < f) {
				f_534_ = f + (float) anInt4498;
				f_535_ = f_533_ - (float) anInt4498;
			} else {
				f_534_ = (float) anInt4498 + f_533_;
				f_535_ = (float) -anInt4498 + f;
			}
			if (!(aHa_Sub1_4492.aFloat4024 <= f_535_) && !(f_534_ <= (float) aHa_Sub1_4492.anInt4035)) {
				Class161.aFloat1672 = (class373_sub2.aFloat5602 * class373_sub2_532_.aFloat5594
						+ ((class373_sub2_532_.aFloat5593 * class373_sub2.aFloat5598)
								+ (class373_sub2_532_.aFloat5590 * class373_sub2.aFloat5595))
						+ class373_sub2.aFloat5593);
				Class207.aFloat2079 = (class373_sub2_532_.aFloat5599 * class373_sub2.aFloat5602
						+ ((class373_sub2.aFloat5595 * class373_sub2_532_.aFloat5596)
								+ (class373_sub2.aFloat5598 * class373_sub2_532_.aFloat5595)));
				float f_536_ = (Class207.aFloat2079 * (float) anInt4497 + Class161.aFloat1672);
				float f_537_ = (Class207.aFloat2079 * (float) anInt4495 + Class161.aFloat1672);
				float f_538_;
				float f_539_;
				if (!(f_537_ < f_536_)) {
					f_539_ = ((float) aHa_Sub1_4492.anInt3992 * (f_536_ - (float) anInt4498));
					f_538_ = ((f_537_ + (float) anInt4498) * (float) aHa_Sub1_4492.anInt3992);
				} else {
					f_538_ = ((float) aHa_Sub1_4492.anInt3992 * (f_536_ + (float) anInt4498));
					f_539_ = ((float) aHa_Sub1_4492.anInt3992 * (f_537_ - (float) anInt4498));
				}
				if (!(aHa_Sub1_4492.aFloat4034 <= f_539_ / f_534_) && !(aHa_Sub1_4492.aFloat4018 >= f_538_ / f_534_)) {
					Class288.aFloat2650 = ((class373_sub2_532_.aFloat5596 * class373_sub2.aFloat5596)
							+ (class373_sub2_532_.aFloat5595 * class373_sub2.aFloat5603)
							+ (class373_sub2_532_.aFloat5599 * class373_sub2.aFloat5597));
					OutputStream_Sub2.aFloat42 = ((class373_sub2.aFloat5596 * class373_sub2_532_.aFloat5590)
							+ (class373_sub2_532_.aFloat5593 * class373_sub2.aFloat5603)
							+ (class373_sub2.aFloat5597 * class373_sub2_532_.aFloat5594) + class373_sub2.aFloat5590);
					float f_540_ = (OutputStream_Sub2.aFloat42 + Class288.aFloat2650 * (float) anInt4497);
					float f_541_ = (OutputStream_Sub2.aFloat42 + (float) anInt4495 * Class288.aFloat2650);
					float f_542_;
					float f_543_;
					if (!(f_541_ < f_540_)) {
						f_543_ = (((float) -anInt4498 + f_540_) * (float) aHa_Sub1_4492.anInt3993);
						f_542_ = ((float) aHa_Sub1_4492.anInt3993 * (f_541_ + (float) anInt4498));
					} else {
						f_542_ = ((float) aHa_Sub1_4492.anInt3993 * (f_540_ + (float) anInt4498));
						f_543_ = ((float) aHa_Sub1_4492.anInt3993 * (f_541_ - (float) anInt4498));
					}
					if (!(aHa_Sub1_4492.aFloat4044 <= f_543_ / f_534_)
							&& !(aHa_Sub1_4492.aFloat3976 >= f_542_ / f_534_)) {
						if (class338_sub5 != null || aClass37Array4510 != null) {
							World.aFloat1161 = ((class373_sub2_532_.aFloat5601 * class373_sub2.aFloat5591)
									+ ((class373_sub2_532_.aFloat5598 * class373_sub2.aFloat5601)
											+ (class373_sub2.aFloat5599 * class373_sub2_532_.aFloat5603)));
							Class16_Sub3_Sub1.aFloat5806 = ((class373_sub2.aFloat5597 * class373_sub2_532_.aFloat5591)
									+ ((class373_sub2_532_.aFloat5602 * class373_sub2.aFloat5603)
											+ (class373_sub2.aFloat5596 * class373_sub2_532_.aFloat5597)));
							Class296_Sub34_Sub2.aFloat6106 = ((class373_sub2_532_.aFloat5601 * class373_sub2.aFloat5597)
									+ ((class373_sub2_532_.aFloat5598 * class373_sub2.aFloat5603)
											+ (class373_sub2_532_.aFloat5603 * class373_sub2.aFloat5596)));
							Class338_Sub3_Sub4_Sub1.aFloat6658 = ((class373_sub2.aFloat5598
									* class373_sub2_532_.aFloat5602)
									+ (class373_sub2_532_.aFloat5597 * class373_sub2.aFloat5595)
									+ (class373_sub2.aFloat5602 * class373_sub2_532_.aFloat5591));
							StaticMethods.aFloat6081 = ((class373_sub2_532_.aFloat5602 * class373_sub2.aFloat5601)
									+ (class373_sub2_532_.aFloat5597 * class373_sub2.aFloat5599)
									+ (class373_sub2_532_.aFloat5591 * class373_sub2.aFloat5591));
							Class240.aFloat2263 = ((class373_sub2_532_.aFloat5598 * class373_sub2.aFloat5598)
									+ (class373_sub2_532_.aFloat5603 * class373_sub2.aFloat5595)
									+ (class373_sub2.aFloat5602 * class373_sub2_532_.aFloat5601));
						}
						if (class338_sub5 != null) {
							boolean bool = false;
							boolean bool_544_ = true;
							int i_545_ = anInt4452 + anInt4494 >> 1;
							int i_546_ = anInt4485 + anInt4503 >> 1;
							int i_547_ = (int) ((Class207.aFloat2079 * (float) anInt4497)
									+ ((Class240.aFloat2263 * (float) i_545_) + Class161.aFloat1672)
									+ ((float) i_546_ * (Class338_Sub3_Sub4_Sub1.aFloat6658)));
							int i_548_ = (int) ((Class288.aFloat2650 * (float) anInt4497)
									+ (OutputStream_Sub2.aFloat42 + (Class296_Sub34_Sub2.aFloat6106 * (float) i_545_))
									+ ((float) i_546_ * Class16_Sub3_Sub1.aFloat5806));
							int i_549_ = (int) ((StaticMethods.aFloat6081 * (float) i_546_)
									+ (((float) i_545_ * World.aFloat1161) + SubInPacket.aFloat2422
											+ (Class281.aFloat2601 * (float) anInt4497)));
							if (i_549_ >= aHa_Sub1_4492.anInt4035) {
								class338_sub5.anInt5225 = (aHa_Sub1_4492.anInt3970
										+ (i_547_ * aHa_Sub1_4492.anInt3992 / i_549_));
								class338_sub5.anInt5223 = (aHa_Sub1_4492.anInt3988
										+ (i_548_ * aHa_Sub1_4492.anInt3993 / i_549_));
							} else
								bool = true;
							int i_550_ = (int) ((Class207.aFloat2079 * (float) anInt4495)
									+ (((float) i_545_ * Class240.aFloat2263) + Class161.aFloat1672)
									+ ((float) i_546_ * (Class338_Sub3_Sub4_Sub1.aFloat6658)));
							int i_551_ = (int) (((float) i_546_ * Class16_Sub3_Sub1.aFloat5806)
									+ (((float) i_545_ * Class296_Sub34_Sub2.aFloat6106) + OutputStream_Sub2.aFloat42
											+ (Class288.aFloat2650 * (float) anInt4495)));
							int i_552_ = (int) (((float) i_546_ * StaticMethods.aFloat6081)
									+ (((float) i_545_ * World.aFloat1161) + SubInPacket.aFloat2422
											+ (Class281.aFloat2601 * (float) anInt4495)));
							if (aHa_Sub1_4492.anInt4035 > i_552_)
								bool = true;
							else {
								class338_sub5.anInt5222 = (aHa_Sub1_4492.anInt3992 * i_550_ / i_552_)
										+ aHa_Sub1_4492.anInt3970;
								class338_sub5.anInt5221 = (aHa_Sub1_4492.anInt3988
										+ (aHa_Sub1_4492.anInt3993 * i_551_ / i_552_));
							}
							if (bool) {
								if (i_549_ < aHa_Sub1_4492.anInt4035 && i_552_ < aHa_Sub1_4492.anInt4035)
									bool_544_ = false;
								else if (i_549_ < aHa_Sub1_4492.anInt4035) {
									int i_553_ = ((i_552_ - aHa_Sub1_4492.anInt4035 << 16) / (i_552_ - i_549_));
									int i_554_ = (((i_550_ - i_547_) * i_553_ >> 16) + i_550_);
									int i_555_ = i_551_ + (i_553_ * (-i_548_ + i_551_) >> 16);
									class338_sub5.anInt5225 = (aHa_Sub1_4492.anInt3970
											+ (aHa_Sub1_4492.anInt3992 * i_554_ / aHa_Sub1_4492.anInt4035));
									class338_sub5.anInt5223 = (aHa_Sub1_4492.anInt3988
											+ (i_555_ * aHa_Sub1_4492.anInt3993 / aHa_Sub1_4492.anInt4035));
								} else if (i_552_ < aHa_Sub1_4492.anInt4035) {
									int i_556_ = ((-aHa_Sub1_4492.anInt4035 + i_549_ << 16) / (-i_552_ + i_549_));
									int i_557_ = i_547_ + ((-i_550_ + i_547_) * i_556_ >> 16);
									class338_sub5.anInt5225 = (aHa_Sub1_4492.anInt3970
											+ (i_557_ * aHa_Sub1_4492.anInt3992 / aHa_Sub1_4492.anInt4035));
									int i_558_ = i_548_ + ((i_548_ - i_551_) * i_556_ >> 16);
									class338_sub5.anInt5223 = ((i_558_ * aHa_Sub1_4492.anInt3993
											/ aHa_Sub1_4492.anInt4035) + aHa_Sub1_4492.anInt3988);
								}
							}
							if (bool_544_) {
								class338_sub5.aBoolean5224 = true;
								if (i_549_ <= i_552_)
									class338_sub5.anInt5226 = (-class338_sub5.anInt5222
											+ (aHa_Sub1_4492.anInt3992 * (i_550_ + anInt4498) / i_552_)
											+ aHa_Sub1_4492.anInt3970);
								else
									class338_sub5.anInt5226 = (-class338_sub5.anInt5225
											+ ((aHa_Sub1_4492.anInt3992 * (anInt4498 + i_547_) / i_549_)
													+ aHa_Sub1_4492.anInt3970));
							}
						}
						aHa_Sub1_4492.method1130(8);
						aHa_Sub1_4492.method1136(class373_sub2_532_, 11);
						method1801((byte) 67);
						method1797(-118);
					}
				}
			}
		}
	}

	public void method1717(int i, int i_559_, int i_560_, int i_561_) {
		for (int i_562_ = 0; i_562_ < anInt4508; i_562_++) {
			int i_563_ = aShortArray4477[i_562_] & 0xffff;
			int i_564_ = i_563_ >> 10 & 0x3f;
			int i_565_ = (i_563_ & 0x3f0) >> 7;
			if (i_559_ != -1)
				i_565_ = ((-i_565_ + i_559_) * i_561_ >> 7) + i_565_;
			if (i != -1)
				i_564_ = (i_561_ * (-i_564_ + i) >> 7) + i_564_;
			int i_566_ = i_563_ & 0x7f;
			if (i_560_ != -1)
				i_566_ = (i_561_ * (-i_566_ + i_560_) >> 7) + i_566_;
			aShortArray4477[i_562_] = (short) Class48.bitOR(i_566_, Class48.bitOR(i_565_ << 7, i_564_ << 10));
		}
		if (aClass37Array4510 != null) {
			for (int i_567_ = 0; i_567_ < anInt4490; i_567_++) {
				Class37 class37 = aClass37Array4510[i_567_];
				Class271 class271 = aClass271Array4459[i_567_];
				class271.anInt2517 = ((Class166_Sub1.anIntArray4300[aShortArray4477[class37.anInt367] & 0xffff])
						& 0xffffff) | class271.anInt2517 & ~0xffffff;
			}
		}
		method1796(1018);
	}

	private void method1804(int i) {
		if (i != (anInt4481 & 0x37 ^ 0xffffffff)) {
			if (aClass307_4480 != null)
				aClass307_4480.aBoolean2750 = false;
		} else if (aClass307_4486 != null)
			aClass307_4486.aBoolean2750 = false;
	}

	public void O(int i, int i_568_, int i_569_) {
		for (int i_570_ = 0; anInt4468 > i_570_; i_570_++) {
			if (i != 128)
				anIntArray4482[i_570_] = anIntArray4482[i_570_] * i >> 7;
			if (i_568_ != 128)
				anIntArray4505[i_570_] = anIntArray4505[i_570_] * i_568_ >> 7;
			if (i_569_ != 128)
				anIntArray4489[i_570_] = i_569_ * anIntArray4489[i_570_] >> 7;
		}
		method1806(-5);
		aBoolean4496 = false;
	}

	public int fa() {
		if (!aBoolean4496)
			method1805((byte) -80);
		return anInt4497;
	}

	public int ma() {
		if (!aBoolean4496)
			method1805((byte) -111);
		return anInt4469;
	}

	public void P(int i, int i_571_, int i_572_, int i_573_) {
		if (i == 0) {
			HashTable.anInt2458 = 0;
			Class182.anInt1876 = 0;
			int i_574_ = 0;
			Class57.anInt667 = 0;
			for (int i_575_ = 0; anInt4468 > i_575_; i_575_++) {
				Class182.anInt1876 += anIntArray4482[i_575_];
				Class57.anInt667 += anIntArray4505[i_575_];
				HashTable.anInt2458 = HashTable.anInt2458 + anIntArray4489[i_575_];
				i_574_++;
			}
			if (i_574_ > 0) {
				HashTable.anInt2458 = i_573_ + HashTable.anInt2458 / i_574_;
				Class57.anInt667 = i_572_ + Class57.anInt667 / i_574_;
				Class182.anInt1876 = Class182.anInt1876 / i_574_ + i_571_;
			} else {
				Class182.anInt1876 = i_571_;
				Class57.anInt667 = i_572_;
				HashTable.anInt2458 = i_573_;
			}
		} else if (i == 1) {
			for (int i_576_ = 0; i_576_ < anInt4468; i_576_++) {
				anIntArray4482[i_576_] += i_571_;
				anIntArray4505[i_576_] += i_572_;
				anIntArray4489[i_576_] += i_573_;
			}
		} else if (i == 2) {
			for (int i_577_ = 0; i_577_ < anInt4468; i_577_++) {
				anIntArray4482[i_577_] -= Class182.anInt1876;
				anIntArray4505[i_577_] -= Class57.anInt667;
				anIntArray4489[i_577_] -= HashTable.anInt2458;
				if (i_573_ != 0) {
					int i_578_ = Class296_Sub4.anIntArray4598[i_573_];
					int i_579_ = Class296_Sub4.anIntArray4618[i_573_];
					int i_580_ = ((i_579_ * anIntArray4482[i_577_] + 16383 + i_578_ * anIntArray4505[i_577_]) >> 14);
					anIntArray4505[i_577_] = (i_579_ * anIntArray4505[i_577_] + 16383
							- i_578_ * anIntArray4482[i_577_]) >> 14;
					anIntArray4482[i_577_] = i_580_;
				}
				if (i_571_ != 0) {
					int i_581_ = Class296_Sub4.anIntArray4598[i_571_];
					int i_582_ = Class296_Sub4.anIntArray4618[i_571_];
					int i_583_ = ((i_582_ * anIntArray4505[i_577_] - (i_581_ * anIntArray4489[i_577_] - 16383)) >> 14);
					anIntArray4489[i_577_] = (anIntArray4505[i_577_] * i_581_
							+ (i_582_ * anIntArray4489[i_577_] + 16383)) >> 14;
					anIntArray4505[i_577_] = i_583_;
				}
				if (i_572_ != 0) {
					int i_584_ = Class296_Sub4.anIntArray4598[i_572_];
					int i_585_ = Class296_Sub4.anIntArray4618[i_572_];
					int i_586_ = ((i_584_ * anIntArray4489[i_577_] + (anIntArray4482[i_577_] * i_585_ + 16383)) >> 14);
					anIntArray4489[i_577_] = (-(anIntArray4482[i_577_] * i_584_) + anIntArray4489[i_577_] * i_585_
							+ 16383) >> 14;
					anIntArray4482[i_577_] = i_586_;
				}
				anIntArray4482[i_577_] += Class182.anInt1876;
				anIntArray4505[i_577_] += Class57.anInt667;
				anIntArray4489[i_577_] += HashTable.anInt2458;
			}
		} else if (i == 3) {
			for (int i_587_ = 0; anInt4468 > i_587_; i_587_++) {
				anIntArray4482[i_587_] -= Class182.anInt1876;
				anIntArray4505[i_587_] -= Class57.anInt667;
				anIntArray4489[i_587_] -= HashTable.anInt2458;
				anIntArray4482[i_587_] = anIntArray4482[i_587_] * i_571_ / 128;
				anIntArray4505[i_587_] = anIntArray4505[i_587_] * i_572_ / 128;
				anIntArray4489[i_587_] = anIntArray4489[i_587_] * i_573_ / 128;
				anIntArray4482[i_587_] += Class182.anInt1876;
				anIntArray4505[i_587_] += Class57.anInt667;
				anIntArray4489[i_587_] += HashTable.anInt2458;
			}
		} else if (i == 5) {
			for (int i_588_ = 0; anInt4508 > i_588_; i_588_++) {
				int i_589_ = i_571_ * 8 + (aByteArray4463[i_588_] & 0xff);
				if (i_589_ < 0)
					i_589_ = 0;
				else if (i_589_ > 255)
					i_589_ = 255;
				aByteArray4463[i_588_] = (byte) i_589_;
			}
			if (aClass37Array4510 != null) {
				for (int i_590_ = 0; i_590_ < anInt4490; i_590_++) {
					Class37 class37 = aClass37Array4510[i_590_];
					Class271 class271 = aClass271Array4459[i_590_];
					class271.anInt2517 = (255 - (aByteArray4463[class37.anInt367] & 0xff) << 24)
							| class271.anInt2517 & 0xffffff;
				}
			}
			method1796(1018);
		} else if (i == 7) {
			for (int i_591_ = 0; i_591_ < anInt4508; i_591_++) {
				int i_592_ = aShortArray4477[i_591_] & 0xffff;
				int i_593_ = i_592_ >> 10 & 0x3f;
				int i_594_ = i_592_ >> 7 & 0x7;
				int i_595_ = i_592_ & 0x7f;
				i_594_ += i_572_ / 4;
				i_593_ = i_593_ + i_571_ & 0x3f;
				i_595_ += i_573_;
				if (i_594_ >= 0) {
					if (i_594_ > 7)
						i_594_ = 7;
				} else
					i_594_ = 0;
				if (i_595_ < 0)
					i_595_ = 0;
				else if (i_595_ > 127)
					i_595_ = 127;
				aShortArray4477[i_591_] = (short) Class48.bitOR(Class48.bitOR(i_594_ << 7, (i_593_ << 10)), i_595_);
			}
			if (aClass37Array4510 != null) {
				for (int i_596_ = 0; anInt4490 > i_596_; i_596_++) {
					Class37 class37 = aClass37Array4510[i_596_];
					Class271 class271 = aClass271Array4459[i_596_];
					class271.anInt2517 = ((Class166_Sub1.anIntArray4300[aShortArray4477[class37.anInt367] & 0xffff])
							& 0xffffff) | class271.anInt2517 & ~0xffffff;
				}
			}
			method1796(1018);
		} else if (i == 8) {
			for (int i_597_ = 0; anInt4490 > i_597_; i_597_++) {
				Class271 class271 = aClass271Array4459[i_597_];
				class271.anInt2511 += i_572_;
				class271.anInt2516 += i_571_;
			}
		} else if (i == 10) {
			for (int i_598_ = 0; i_598_ < anInt4490; i_598_++) {
				Class271 class271 = aClass271Array4459[i_598_];
				class271.anInt2515 = class271.anInt2515 * i_571_ >> 7;
				class271.anInt2513 = class271.anInt2513 * i_572_ >> 7;
			}
		} else if (i == 9) {
			for (int i_599_ = 0; i_599_ < anInt4490; i_599_++) {
				Class271 class271 = aClass271Array4459[i_599_];
				class271.anInt2514 = class271.anInt2514 + i_571_ & 0x3fff;
			}
		}
	}

	public int da() {
		return aShort4457;
	}

	private void method1805(byte i) {
		int i_600_ = 32767;
		int i_601_ = 32767;
		if (i <= -20) {
			int i_602_ = 32767;
			int i_603_ = -32768;
			int i_604_ = -32768;
			int i_605_ = -32768;
			int i_606_ = 0;
			int i_607_ = 0;
			for (int i_608_ = 0; i_608_ < anInt4468; i_608_++) {
				int i_609_ = anIntArray4482[i_608_];
				int i_610_ = anIntArray4505[i_608_];
				if (i_600_ > i_609_)
					i_600_ = i_609_;
				int i_611_ = anIntArray4489[i_608_];
				if (i_610_ < i_601_)
					i_601_ = i_610_;
				if (i_603_ < i_609_)
					i_603_ = i_609_;
				if (i_604_ < i_610_)
					i_604_ = i_610_;
				if (i_611_ < i_602_)
					i_602_ = i_611_;
				if (i_611_ > i_605_)
					i_605_ = i_611_;
				int i_612_ = i_609_ * i_609_ + i_611_ * i_611_;
				if (i_606_ < i_612_)
					i_606_ = i_612_;
				i_612_ = i_611_ * i_611_ + i_609_ * i_609_ + i_610_ * i_610_;
				if (i_607_ < i_612_)
					i_607_ = i_612_;
			}
			anInt4497 = i_601_;
			anInt4452 = i_603_;
			anInt4494 = i_600_;
			anInt4495 = i_604_;
			anInt4485 = i_605_;
			anInt4503 = i_602_;
			anInt4498 = (int) (Math.sqrt((double) i_606_) + 0.99);
			anInt4469 = (int) (Math.sqrt((double) i_607_) + 0.99);
			aBoolean4496 = true;
		}
	}

	public void FA(int i) {
		int i_613_ = Class296_Sub4.anIntArray4598[i];
		int i_614_ = Class296_Sub4.anIntArray4618[i];
		for (int i_615_ = 0; i_615_ < anInt4468; i_615_++) {
			int i_616_ = ((-(anIntArray4489[i_615_] * i_613_) + anIntArray4505[i_615_] * i_614_) >> 14);
			anIntArray4489[i_615_] = (i_614_ * anIntArray4489[i_615_] + i_613_ * anIntArray4505[i_615_]) >> 14;
			anIntArray4505[i_615_] = i_616_;
		}
		method1806(-5);
		aBoolean4496 = false;
	}

	public int WA() {
		return aShort4476;
	}

	public void v() {
		for (int i = 0; i < anInt4468; i++)
			anIntArray4489[i] = -anIntArray4489[i];
		for (int i = 0; i < anInt4474; i++)
			aShortArray4483[i] = (short) -aShortArray4483[i];
		for (int i = 0; i < anInt4508; i++) {
			short i_617_ = aShortArray4464[i];
			aShortArray4464[i] = aShortArray4488[i];
			aShortArray4488[i] = i_617_;
		}
		method1806(-5);
		method1804(-1);
		method1803(21924);
		aBoolean4496 = false;
	}

	public r ba(r var_r) {
		if (anInt4474 == 0)
			return null;
		if (!aBoolean4496)
			method1805((byte) -52);
		int i;
		int i_618_;
		if (aHa_Sub1_4492.anInt3969 <= 0) {
			i = (-(anInt4497 * aHa_Sub1_4492.anInt3969 >> 8) + anInt4494 >> aHa_Sub1_4492.anInt4036);
			i_618_ = (anInt4452 - (anInt4495 * aHa_Sub1_4492.anInt3969 >> 8) >> aHa_Sub1_4492.anInt4036);
		} else {
			i = (anInt4494 - (anInt4495 * aHa_Sub1_4492.anInt3969 >> 8) >> aHa_Sub1_4492.anInt4036);
			i_618_ = (-(anInt4497 * aHa_Sub1_4492.anInt3969 >> 8) + anInt4452 >> aHa_Sub1_4492.anInt4036);
		}
		int i_619_;
		int i_620_;
		if (aHa_Sub1_4492.anInt4027 > 0) {
			i_619_ = (anInt4503 - (aHa_Sub1_4492.anInt4027 * anInt4495 >> 8) >> aHa_Sub1_4492.anInt4036);
			i_620_ = (-(anInt4497 * aHa_Sub1_4492.anInt4027 >> 8) + anInt4485 >> aHa_Sub1_4492.anInt4036);
		} else {
			i_619_ = (anInt4503 - (anInt4497 * aHa_Sub1_4492.anInt4027 >> 8) >> aHa_Sub1_4492.anInt4036);
			i_620_ = (-(aHa_Sub1_4492.anInt4027 * anInt4495 >> 8) + anInt4485 >> aHa_Sub1_4492.anInt4036);
		}
		int i_621_ = -i + i_618_ + 1;
		int i_622_ = i_620_ - i_619_ + 1;
		r_Sub2 var_r_Sub2 = (r_Sub2) var_r;
		r_Sub2 var_r_Sub2_623_;
		if (var_r_Sub2 == null || !var_r_Sub2.method2870(i_621_, -124914704, i_622_))
			var_r_Sub2_623_ = new r_Sub2(aHa_Sub1_4492, i_621_, i_622_);
		else {
			var_r_Sub2_623_ = var_r_Sub2;
			var_r_Sub2_623_.method2866(-1);
		}
		var_r_Sub2_623_.method2869(i_619_, i, i_618_, i_620_, -124);
		method1799((byte) 60, var_r_Sub2_623_);
		return var_r_Sub2_623_;
	}

	public void LA(int i) {
		aShort4457 = (short) i;
		method1806(-5);
		method1804(-1);
	}

	private void method1806(int i) {
		if (aClass307_4467 != null)
			aClass307_4467.aBoolean2750 = false;
		if (i != -5)
			aClass271Array4459 = null;
	}

	private void method1807(int i) {
		if (aBoolean4456) {
			aBoolean4456 = false;
			if (aClass89Array4487 == null && aClass232Array4504 == null && aClass37Array4510 == null
					&& !Class382.method4006(anInt4500, -20268, anInt4481)) {
				boolean bool = false;
				boolean bool_624_ = false;
				boolean bool_625_ = false;
				if (anIntArray4482 != null && !Class289.method2399(anInt4481, anInt4500, -27448)) {
					if (aClass307_4467 == null || aClass307_4467.method3288(i ^ 0x717f)) {
						bool = true;
						if (!aBoolean4496)
							method1805((byte) -114);
					} else
						aBoolean4456 = true;
				}
				if (anIntArray4505 != null && !Class287.method2390((byte) -105, anInt4500, anInt4481)) {
					if (aClass307_4467 != null && !aClass307_4467.method3288(0))
						aBoolean4456 = true;
					else {
						bool_624_ = true;
						if (!aBoolean4496)
							method1805((byte) -90);
					}
				}
				if (anIntArray4489 != null && !Class41_Sub14.method448(anInt4481, anInt4500, true)) {
					if (aClass307_4467 != null && !aClass307_4467.method3288(0))
						aBoolean4456 = true;
					else {
						if (!aBoolean4496)
							method1805((byte) -61);
						bool_625_ = true;
					}
				}
				if (bool_625_)
					anIntArray4489 = null;
				if (bool_624_)
					anIntArray4505 = null;
				if (bool)
					anIntArray4482 = null;
			}
			if (aShortArray4460 != null && anIntArray4482 == null && anIntArray4505 == null && anIntArray4489 == null) {
				anIntArray4472 = null;
				aShortArray4460 = null;
			}
			if (aByteArray4506 != null && !Class126.method1082(i ^ 0x7120, anInt4481, anInt4500)) {
				if ((anInt4481 & 0x37) == 0 ? aClass307_4486 == null || aClass307_4486.method3288(0)
						: aClass307_4480 == null || aClass307_4480.method3288(0)) {
					aShortArray4507 = aShortArray4484 = aShortArray4483 = null;
					aByteArray4506 = null;
				} else
					aBoolean4456 = true;
			}
			if (aShortArray4477 != null && !StaticMethods.method2452(i ^ ~0x7847, anInt4481, anInt4500)) {
				if (aClass307_4486 == null || aClass307_4486.method3288(0))
					aShortArray4477 = null;
				else
					aBoolean4456 = true;
			}
			if (aByteArray4463 != null && !Class284.method2365(73, anInt4481, anInt4500)) {
				if (aClass307_4486 == null || aClass307_4486.method3288(0))
					aByteArray4463 = null;
				else
					aBoolean4456 = true;
			}
			if (aFloatArray4466 != null && !Class161.method1621(anInt4500, 1884, anInt4481)) {
				if (aClass307_4471 != null && !aClass307_4471.method3288(0))
					aBoolean4456 = true;
				else
					aFloatArray4466 = aFloatArray4462 = null;
			}
			if (aShortArray4512 != null && !ClothDefinition.method3662(anInt4481, anInt4500, 0)) {
				if (aClass307_4486 != null && !aClass307_4486.method3288(0))
					aBoolean4456 = true;
				else
					aShortArray4512 = null;
			}
			if (i == 29055) {
				if (aShortArray4464 != null && !Class338_Sub3_Sub4_Sub1.method3574((byte) 105, anInt4481, anInt4500)) {
					if ((aClass293_4501 != null && !aClass293_4501.method2414(-7703))
							|| (aClass307_4486 != null && !aClass307_4486.method3288(0)))
						aBoolean4456 = true;
					else
						aShortArray4464 = aShortArray4509 = aShortArray4488 = null;
				}
				if (aShortArray4455 != null) {
					if (aClass307_4467 == null || aClass307_4467.method3288(0))
						aShortArray4455 = null;
					else
						aBoolean4456 = true;
				}
				if (aShortArray4451 != null) {
					if (aClass307_4486 == null || aClass307_4486.method3288(i - 29055))
						aShortArray4451 = null;
					else
						aBoolean4456 = true;
				}
				if (anIntArrayArray4454 != null && !Class175.method1702(anInt4481, true, anInt4500)) {
					anIntArrayArray4454 = null;
					aShortArray4461 = null;
				}
				if (anIntArrayArray4475 != null && !Class41_Sub5.method413(anInt4481, anInt4500, 3709)) {
					anIntArrayArray4475 = null;
					aShortArray4453 = null;
				}
				if (anIntArrayArray4479 != null && !Class4.method174(anInt4481, anInt4500, 1019))
					anIntArrayArray4479 = null;
				if (anIntArray4465 != null && (anInt4500 & 0x800) == 0 && (anInt4500 & 0x40000) == 0) {
					anIntArray4511 = null;
					anIntArray4465 = null;
					anIntArray4491 = null;
				}
			}
		}
	}

	private boolean method1808(int i) {
		if (aClass293_4501.aBoolean2681)
			return true;
		if (aClass293_4501.anInterface15_Impl1_2682 == null)
			aClass293_4501.anInterface15_Impl1_2682 = aHa_Sub1_4492.method1154(aBoolean4458, 60);
		if (i != -9359)
			r();
		Interface15_Impl1 interface15_impl1 = aClass293_4501.anInterface15_Impl1_2682;
		interface15_impl1.method32(anInt4493 * 6, -21709);
		Buffer buffer = interface15_impl1.method30((byte) -123, true);
		if (buffer != null) {
			Stream stream = aHa_Sub1_4492.method1156(buffer, -114);
			if (!Stream.a()) {
				for (int i_626_ = 0; anInt4493 > i_626_; i_626_++) {
					stream.a(aShortArray4464[i_626_]);
					stream.a(aShortArray4509[i_626_]);
					stream.a(aShortArray4488[i_626_]);
				}
			} else {
				for (int i_627_ = 0; i_627_ < anInt4493; i_627_++) {
					stream.d(aShortArray4464[i_627_]);
					stream.d(aShortArray4509[i_627_]);
					stream.d(aShortArray4488[i_627_]);
				}
			}
			stream.b();
			if (interface15_impl1.method33(i ^ ~0x5937)) {
				aClass293_4501.aBoolean2681 = true;
				aBoolean4456 = true;
				aClass293_4501.anInterface15_Impl1_2683 = interface15_impl1;
				return true;
			}
		}
		return false;
	}

	private Model method1809(Class178_Sub3 class178_sub3_628_, int i, Class178_Sub3 class178_sub3_629_, boolean bool,
			boolean bool_630_, boolean bool_631_) {
		class178_sub3_629_.anInt4490 = anInt4490;
		class178_sub3_629_.anInt4474 = anInt4474;
		class178_sub3_629_.anInt4500 = i;
		class178_sub3_629_.anInt4468 = anInt4468;
		class178_sub3_629_.aShort4457 = aShort4457;
		class178_sub3_629_.aShort4476 = aShort4476;
		class178_sub3_629_.aBoolean4502 = aBoolean4502;
		class178_sub3_629_.anInt4493 = anInt4493;
		class178_sub3_629_.anInt4481 = anInt4481;
		class178_sub3_629_.anInt4478 = anInt4478;
		class178_sub3_629_.anInt4508 = anInt4508;
		if ((i & 0x100) == 0)
			class178_sub3_629_.aBoolean4499 = aBoolean4499;
		else
			class178_sub3_629_.aBoolean4499 = true;
		boolean bool_632_ = Class175.method1701((byte) -9, i, anInt4481);
		if (bool_630_ != true)
			aClass271Array4459 = null;
		boolean bool_633_ = r.method2859(i, (byte) -15, anInt4481);
		boolean bool_634_ = Class41_Sub20.method471(0, i, anInt4481);
		boolean bool_635_ = bool_633_ | bool_632_ | bool_634_;
		if (bool_635_) {
			if (bool_632_) {
				if (class178_sub3_628_.anIntArray4482 != null && anInt4478 <= class178_sub3_628_.anIntArray4482.length)
					class178_sub3_629_.anIntArray4482 = class178_sub3_628_.anIntArray4482;
				else
					class178_sub3_629_.anIntArray4482 = class178_sub3_628_.anIntArray4482 = new int[anInt4478];
			} else
				class178_sub3_629_.anIntArray4482 = anIntArray4482;
			if (!bool_633_)
				class178_sub3_629_.anIntArray4505 = anIntArray4505;
			else if (class178_sub3_628_.anIntArray4505 == null || class178_sub3_628_.anIntArray4505.length < anInt4478)
				class178_sub3_629_.anIntArray4505 = class178_sub3_628_.anIntArray4505 = new int[anInt4478];
			else
				class178_sub3_629_.anIntArray4505 = class178_sub3_628_.anIntArray4505;
			if (bool_634_) {
				if (class178_sub3_628_.anIntArray4489 == null || class178_sub3_628_.anIntArray4489.length < anInt4478)
					class178_sub3_629_.anIntArray4489 = class178_sub3_628_.anIntArray4489 = new int[anInt4478];
				else
					class178_sub3_629_.anIntArray4489 = class178_sub3_628_.anIntArray4489;
			} else
				class178_sub3_629_.anIntArray4489 = anIntArray4489;
			for (int i_636_ = 0; anInt4478 > i_636_; i_636_++) {
				if (bool_632_)
					class178_sub3_629_.anIntArray4482[i_636_] = anIntArray4482[i_636_];
				if (bool_633_)
					class178_sub3_629_.anIntArray4505[i_636_] = anIntArray4505[i_636_];
				if (bool_634_)
					class178_sub3_629_.anIntArray4489[i_636_] = anIntArray4489[i_636_];
			}
		} else {
			class178_sub3_629_.anIntArray4505 = anIntArray4505;
			class178_sub3_629_.anIntArray4489 = anIntArray4489;
			class178_sub3_629_.anIntArray4482 = anIntArray4482;
		}
		if (!Class296_Sub51_Sub26.method3150(i, (byte) 69, anInt4481)) {
			if (!Class230.method2105(123, anInt4481, i))
				class178_sub3_629_.aClass307_4467 = null;
			else
				class178_sub3_629_.aClass307_4467 = aClass307_4467;
		} else {
			class178_sub3_629_.aClass307_4467 = class178_sub3_628_.aClass307_4467;
			class178_sub3_629_.aClass307_4467.anInterface15_Impl2_2752 = aClass307_4467.anInterface15_Impl2_2752;
			class178_sub3_629_.aClass307_4467.aBoolean2750 = aClass307_4467.aBoolean2750;
			class178_sub3_629_.aClass307_4467.aBoolean2747 = true;
		}
		if (!Class296_Sub51_Sub27_Sub1.method3161(-19214, anInt4481, i))
			class178_sub3_629_.aShortArray4477 = aShortArray4477;
		else {
			if (class178_sub3_628_.aShortArray4477 == null || anInt4508 > class178_sub3_628_.aShortArray4477.length)
				class178_sub3_629_.aShortArray4477 = class178_sub3_628_.aShortArray4477 = new short[anInt4508];
			else
				class178_sub3_629_.aShortArray4477 = class178_sub3_628_.aShortArray4477;
			for (int i_637_ = 0; anInt4508 > i_637_; i_637_++)
				class178_sub3_629_.aShortArray4477[i_637_] = aShortArray4477[i_637_];
		}
		if (Class367.method3803(i, -68, anInt4481)) {
			if (class178_sub3_628_.aByteArray4463 == null || class178_sub3_628_.aByteArray4463.length < anInt4508)
				class178_sub3_629_.aByteArray4463 = class178_sub3_628_.aByteArray4463 = new byte[anInt4508];
			else
				class178_sub3_629_.aByteArray4463 = class178_sub3_628_.aByteArray4463;
			for (int i_638_ = 0; anInt4508 > i_638_; i_638_++)
				class178_sub3_629_.aByteArray4463[i_638_] = aByteArray4463[i_638_];
		} else
			class178_sub3_629_.aByteArray4463 = aByteArray4463;
		if (Class184.method1857(anInt4481, i, (byte) 51)) {
			class178_sub3_629_.aClass307_4486 = class178_sub3_628_.aClass307_4486;
			class178_sub3_629_.aClass307_4486.aBoolean2747 = true;
			class178_sub3_629_.aClass307_4486.aBoolean2750 = aClass307_4486.aBoolean2750;
			class178_sub3_629_.aClass307_4486.anInterface15_Impl2_2752 = aClass307_4486.anInterface15_Impl2_2752;
		} else if (!SubInPacket.method2242(i, -120, anInt4481))
			class178_sub3_629_.aClass307_4486 = null;
		else
			class178_sub3_629_.aClass307_4486 = aClass307_4486;
		if (Class157.method1589(i, -12205, anInt4481)) {
			if (class178_sub3_628_.aShortArray4507 != null && anInt4474 <= class178_sub3_628_.aShortArray4507.length) {
				class178_sub3_629_.aShortArray4507 = class178_sub3_628_.aShortArray4507;
				class178_sub3_629_.aShortArray4483 = class178_sub3_628_.aShortArray4483;
				class178_sub3_629_.aShortArray4484 = class178_sub3_628_.aShortArray4484;
			} else {
				int i_639_ = anInt4474;
				class178_sub3_629_.aShortArray4483 = class178_sub3_628_.aShortArray4483 = new short[i_639_];
				class178_sub3_629_.aShortArray4507 = class178_sub3_628_.aShortArray4507 = new short[i_639_];
				class178_sub3_629_.aShortArray4484 = class178_sub3_628_.aShortArray4484 = new short[i_639_];
			}
			if (aClass223_4473 != null) {
				if (class178_sub3_628_.aClass223_4473 == null)
					class178_sub3_628_.aClass223_4473 = new Class223();
				Class223 class223 = (class178_sub3_629_.aClass223_4473 = class178_sub3_628_.aClass223_4473);
				if (class223.aShortArray2165 == null || class223.aShortArray2165.length < anInt4474) {
					int i_640_ = anInt4474;
					class223.aShortArray2163 = new short[i_640_];
					class223.aShortArray2164 = new short[i_640_];
					class223.aShortArray2165 = new short[i_640_];
					class223.aByteArray2166 = new byte[i_640_];
				}
				for (int i_641_ = 0; i_641_ < anInt4474; i_641_++) {
					class178_sub3_629_.aShortArray4507[i_641_] = aShortArray4507[i_641_];
					class178_sub3_629_.aShortArray4484[i_641_] = aShortArray4484[i_641_];
					class178_sub3_629_.aShortArray4483[i_641_] = aShortArray4483[i_641_];
					class223.aShortArray2165[i_641_] = aClass223_4473.aShortArray2165[i_641_];
					class223.aShortArray2164[i_641_] = aClass223_4473.aShortArray2164[i_641_];
					class223.aShortArray2163[i_641_] = aClass223_4473.aShortArray2163[i_641_];
					class223.aByteArray2166[i_641_] = aClass223_4473.aByteArray2166[i_641_];
				}
			} else {
				class178_sub3_629_.aClass223_4473 = null;
				for (int i_642_ = 0; anInt4474 > i_642_; i_642_++) {
					class178_sub3_629_.aShortArray4507[i_642_] = aShortArray4507[i_642_];
					class178_sub3_629_.aShortArray4484[i_642_] = aShortArray4484[i_642_];
					class178_sub3_629_.aShortArray4483[i_642_] = aShortArray4483[i_642_];
				}
			}
			class178_sub3_629_.aByteArray4506 = aByteArray4506;
		} else {
			class178_sub3_629_.aByteArray4506 = aByteArray4506;
			class178_sub3_629_.aShortArray4484 = aShortArray4484;
			class178_sub3_629_.aClass223_4473 = aClass223_4473;
			class178_sub3_629_.aShortArray4483 = aShortArray4483;
			class178_sub3_629_.aShortArray4507 = aShortArray4507;
		}
		if (!Class47.method599((byte) -46, i, anInt4481)) {
			if (ConfigurationDefinition.method687(i, anInt4481, (byte) 40))
				class178_sub3_629_.aClass307_4480 = aClass307_4480;
			else
				class178_sub3_629_.aClass307_4480 = null;
		} else {
			class178_sub3_629_.aClass307_4480 = class178_sub3_628_.aClass307_4480;
			class178_sub3_629_.aClass307_4480.anInterface15_Impl2_2752 = aClass307_4480.anInterface15_Impl2_2752;
			class178_sub3_629_.aClass307_4480.aBoolean2750 = aClass307_4480.aBoolean2750;
			class178_sub3_629_.aClass307_4480.aBoolean2747 = true;
		}
		if (!StaticMethods.method2477(10, anInt4481, i)) {
			class178_sub3_629_.aFloatArray4462 = aFloatArray4462;
			class178_sub3_629_.aFloatArray4466 = aFloatArray4466;
		} else {
			if (class178_sub3_628_.aFloatArray4466 == null || class178_sub3_628_.aFloatArray4466.length < anInt4508) {
				int i_643_ = anInt4474;
				class178_sub3_629_.aFloatArray4466 = class178_sub3_628_.aFloatArray4466 = new float[i_643_];
				class178_sub3_629_.aFloatArray4462 = class178_sub3_628_.aFloatArray4462 = new float[i_643_];
			} else {
				class178_sub3_629_.aFloatArray4462 = class178_sub3_628_.aFloatArray4462;
				class178_sub3_629_.aFloatArray4466 = class178_sub3_628_.aFloatArray4466;
			}
			for (int i_644_ = 0; i_644_ < anInt4474; i_644_++) {
				class178_sub3_629_.aFloatArray4466[i_644_] = aFloatArray4466[i_644_];
				class178_sub3_629_.aFloatArray4462[i_644_] = aFloatArray4462[i_644_];
			}
		}
		if (!Class16_Sub1_Sub1.method239(0, i, anInt4481)) {
			if (!Class123_Sub2.method1068(anInt4481, (byte) 110, i))
				class178_sub3_629_.aClass307_4471 = null;
			else
				class178_sub3_629_.aClass307_4471 = aClass307_4471;
		} else {
			class178_sub3_629_.aClass307_4471 = class178_sub3_628_.aClass307_4471;
			class178_sub3_629_.aClass307_4471.aBoolean2747 = true;
			class178_sub3_629_.aClass307_4471.anInterface15_Impl2_2752 = aClass307_4471.anInterface15_Impl2_2752;
			class178_sub3_629_.aClass307_4471.aBoolean2750 = aClass307_4471.aBoolean2750;
		}
		if (!Class366_Sub1.method3771(i, -1, anInt4481)) {
			class178_sub3_629_.aShortArray4488 = aShortArray4488;
			class178_sub3_629_.aShortArray4464 = aShortArray4464;
			class178_sub3_629_.aShortArray4509 = aShortArray4509;
		} else {
			if (class178_sub3_628_.aShortArray4464 == null || anInt4508 > class178_sub3_628_.aShortArray4464.length) {
				int i_645_ = anInt4508;
				class178_sub3_629_.aShortArray4488 = class178_sub3_628_.aShortArray4488 = new short[i_645_];
				class178_sub3_629_.aShortArray4464 = class178_sub3_628_.aShortArray4464 = new short[i_645_];
				class178_sub3_629_.aShortArray4509 = class178_sub3_628_.aShortArray4509 = new short[i_645_];
			} else {
				class178_sub3_629_.aShortArray4464 = class178_sub3_628_.aShortArray4464;
				class178_sub3_629_.aShortArray4509 = class178_sub3_628_.aShortArray4509;
				class178_sub3_629_.aShortArray4488 = class178_sub3_628_.aShortArray4488;
			}
			for (int i_646_ = 0; i_646_ < anInt4508; i_646_++) {
				class178_sub3_629_.aShortArray4464[i_646_] = aShortArray4464[i_646_];
				class178_sub3_629_.aShortArray4509[i_646_] = aShortArray4509[i_646_];
				class178_sub3_629_.aShortArray4488[i_646_] = aShortArray4488[i_646_];
			}
		}
		if (!Class338_Sub3_Sub2_Sub1.method3556((byte) 100, i, anInt4481)) {
			if (!Class41_Sub26.method498(i, anInt4481, (byte) -41))
				class178_sub3_629_.aClass293_4501 = null;
			else
				class178_sub3_629_.aClass293_4501 = aClass293_4501;
		} else {
			class178_sub3_629_.aClass293_4501 = class178_sub3_628_.aClass293_4501;
			class178_sub3_629_.aClass293_4501.aBoolean2680 = true;
			class178_sub3_629_.aClass293_4501.aBoolean2681 = aClass293_4501.aBoolean2681;
			class178_sub3_629_.aClass293_4501.anInterface15_Impl1_2683 = aClass293_4501.anInterface15_Impl1_2683;
		}
		if (Class139.method1465(anInt4481, i, 113)) {
			if (class178_sub3_628_.aShortArray4512 != null && anInt4508 <= class178_sub3_628_.aShortArray4512.length)
				class178_sub3_629_.aShortArray4512 = class178_sub3_628_.aShortArray4512;
			else {
				int i_647_ = anInt4508;
				class178_sub3_629_.aShortArray4512 = class178_sub3_628_.aShortArray4512 = new short[i_647_];
			}
			for (int i_648_ = 0; anInt4508 > i_648_; i_648_++)
				class178_sub3_629_.aShortArray4512[i_648_] = aShortArray4512[i_648_];
		} else
			class178_sub3_629_.aShortArray4512 = aShortArray4512;
		if (Class164.method1633(46, i, anInt4481)) {
			if (class178_sub3_628_.aClass271Array4459 != null
					&& anInt4490 <= class178_sub3_628_.aClass271Array4459.length) {
				class178_sub3_629_.aClass271Array4459 = class178_sub3_628_.aClass271Array4459;
				for (int i_649_ = 0; i_649_ < anInt4490; i_649_++)
					class178_sub3_629_.aClass271Array4459[i_649_].method2309(128, aClass271Array4459[i_649_]);
			} else {
				int i_650_ = anInt4490;
				class178_sub3_629_.aClass271Array4459 = class178_sub3_628_.aClass271Array4459 = new Class271[i_650_];
				for (int i_651_ = 0; anInt4490 > i_651_; i_651_++)
					class178_sub3_629_.aClass271Array4459[i_651_] = aClass271Array4459[i_651_].method2308(128);
			}
		} else
			class178_sub3_629_.aClass271Array4459 = aClass271Array4459;
		class178_sub3_629_.anIntArrayArray4479 = anIntArrayArray4479;
		class178_sub3_629_.aShortArray4460 = aShortArray4460;
		class178_sub3_629_.anIntArrayArray4454 = anIntArrayArray4454;
		class178_sub3_629_.anIntArrayArray4475 = anIntArrayArray4475;
		class178_sub3_629_.aShortArray4453 = aShortArray4453;
		class178_sub3_629_.anIntArray4465 = anIntArray4465;
		class178_sub3_629_.anIntArray4511 = anIntArray4511;
		class178_sub3_629_.aShortArray4455 = aShortArray4455;
		class178_sub3_629_.aClass37Array4510 = aClass37Array4510;
		if (aBoolean4496) {
			class178_sub3_629_.anInt4485 = anInt4485;
			class178_sub3_629_.anInt4494 = anInt4494;
			class178_sub3_629_.anInt4495 = anInt4495;
			class178_sub3_629_.anInt4497 = anInt4497;
			class178_sub3_629_.aBoolean4496 = true;
			class178_sub3_629_.anInt4452 = anInt4452;
			class178_sub3_629_.anInt4503 = anInt4503;
			class178_sub3_629_.anInt4469 = anInt4469;
			class178_sub3_629_.anInt4498 = anInt4498;
		} else
			class178_sub3_629_.aBoolean4496 = false;
		class178_sub3_629_.aClass232Array4504 = aClass232Array4504;
		class178_sub3_629_.aShortArray4451 = aShortArray4451;
		class178_sub3_629_.anIntArray4472 = anIntArray4472;
		class178_sub3_629_.aClass89Array4487 = aClass89Array4487;
		class178_sub3_629_.anIntArray4491 = anIntArray4491;
		class178_sub3_629_.aShortArray4461 = aShortArray4461;
		return class178_sub3_629_;
	}

	public void wa() {
		for (int i = 0; i < anInt4478; i++) {
			anIntArray4482[i] = anIntArray4482[i] + 7 >> 4;
			anIntArray4505[i] = anIntArray4505[i] + 7 >> 4;
			anIntArray4489[i] = anIntArray4489[i] + 7 >> 4;
		}
		method1806(-5);
		aBoolean4496 = false;
	}

	public static void method1810(byte i) {
		aClass231_4470 = null;
		if (i > 0)
			aClass231_4470 = null;
	}

	public byte[] method1733() {
		return aByteArray4463;
	}

	public void aa(short i, short i_652_) {
		d var_d = aHa_Sub1_4492.aD1299;
		for (int i_653_ = 0; i_653_ < anInt4508; i_653_++) {
			if (aShortArray4512[i_653_] == i)
				aShortArray4512[i_653_] = i_652_;
		}
		byte i_654_ = 0;
		byte i_655_ = 0;
		if (i != -1) {
			MaterialRaw class170 = var_d.method14(i & 0xffff, -9412);
			i_654_ = class170.aByte1773;
			i_655_ = class170.aByte1793;
		}
		byte i_656_ = 0;
		byte i_657_ = 0;
		if (i_652_ != -1) {
			MaterialRaw class170 = var_d.method14(i_652_ & 0xffff, -9412);
			i_656_ = class170.aByte1773;
			if (class170.speed_u != 0 || class170.speed_v != 0)
				aBoolean4502 = true;
			i_657_ = class170.aByte1793;
		}
		if (i_656_ != i_654_ | i_655_ != i_657_) {
			if (aClass37Array4510 != null) {
				for (int i_658_ = 0; i_658_ < anInt4490; i_658_++) {
					Class37 class37 = aClass37Array4510[i_658_];
					Class271 class271 = aClass271Array4459[i_658_];
					class271.anInt2517 = (class271.anInt2517 & ~0xffffff
							| ((Class166_Sub1.anIntArray4300[aShortArray4477[class37.anInt367] & 0xffff]) & 0xffffff));
				}
			}
			method1796(1018);
		}
	}

	private boolean method1811(int i) {
		if (i != 12)
			return false;
		boolean bool = !aClass307_4486.aBoolean2750;
		boolean bool_659_ = (anInt4481 & 0x37) != 0 && !aClass307_4480.aBoolean2750;
		boolean bool_660_ = !aClass307_4467.aBoolean2750;
		boolean bool_661_ = !aClass307_4471.aBoolean2750;
		if (!bool_660_ && !bool && !bool_659_ && !bool_661_)
			return true;
		boolean bool_662_ = true;
		if (bool_660_) {
			if (aShortArray4455 == null)
				return true;
			if (aClass307_4467.anInterface15_Impl2_2746 == null)
				aClass307_4467.anInterface15_Impl2_2746 = aHa_Sub1_4492.method1205(aBoolean4458, -87);
			Interface15_Impl2 interface15_impl2 = aClass307_4467.anInterface15_Impl2_2746;
			interface15_impl2.method46(anInt4474 * 12, -74, 12);
			Buffer buffer = interface15_impl2.method47(true, -8102);
			if (buffer != null) {
				aHa_Sub1_4492.aNativeInterface3923.copyPositions(anIntArray4482, anIntArray4505, anIntArray4489,
						aShortArray4455, 0, 12, anInt4474, buffer.getAddress());
				if (interface15_impl2.method49(2968)) {
					aClass307_4467.aBoolean2750 = true;
					aClass307_4467.anInterface15_Impl2_2752 = interface15_impl2;
				} else
					bool_662_ = false;
			} else
				bool_662_ = false;
		}
		if (bool) {
			if (aClass307_4486.anInterface15_Impl2_2746 == null)
				aClass307_4486.anInterface15_Impl2_2746 = aHa_Sub1_4492.method1205(aBoolean4458, -92);
			Interface15_Impl2 interface15_impl2 = aClass307_4486.anInterface15_Impl2_2746;
			interface15_impl2.method46(anInt4474 * 4, -75, 4);
			Buffer buffer = interface15_impl2.method47(true, -8102);
			if (buffer == null)
				bool_662_ = false;
			else {
				if ((anInt4481 & 0x37) != 0)
					aHa_Sub1_4492.aNativeInterface3923.copyColours(aShortArray4477, aByteArray4463, aShortArray4512,
							aShort4476, aShortArray4451, 0, 4, anInt4474, buffer.getAddress());
				else {
					byte[] is;
					short[] is_663_;
					short[] is_664_;
					short[] is_665_;
					if (aClass223_4473 == null) {
						is_663_ = aShortArray4484;
						is_665_ = aShortArray4483;
						is = aByteArray4506;
						is_664_ = aShortArray4507;
					} else {
						is = aClass223_4473.aByteArray2166;
						is_663_ = aClass223_4473.aShortArray2164;
						is_664_ = aClass223_4473.aShortArray2165;
						is_665_ = aClass223_4473.aShortArray2163;
					}
					aHa_Sub1_4492.aNativeInterface3923.copyLighting(aShortArray4477, aByteArray4463, aShortArray4512,
							is_664_, is_663_, is_665_, is, aShort4476, aShort4457, aShortArray4451, 0, 4, anInt4474,
							buffer.getAddress());
				}
				if (interface15_impl2.method49(2968)) {
					aClass307_4486.aBoolean2750 = true;
					aClass307_4486.anInterface15_Impl2_2752 = interface15_impl2;
				} else
					bool_662_ = false;
			}
		}
		if (bool_659_) {
			if (aClass307_4480.anInterface15_Impl2_2746 == null)
				aClass307_4480.anInterface15_Impl2_2746 = aHa_Sub1_4492.method1205(aBoolean4458, 106);
			Interface15_Impl2 interface15_impl2 = aClass307_4480.anInterface15_Impl2_2746;
			interface15_impl2.method46(anInt4474 * 12, 26, 12);
			Buffer buffer = interface15_impl2.method47(true, -8102);
			if (buffer != null) {
				short[] is;
				byte[] is_666_;
				short[] is_667_;
				short[] is_668_;
				if (aClass223_4473 == null) {
					is = aShortArray4483;
					is_668_ = aShortArray4484;
					is_667_ = aShortArray4507;
					is_666_ = aByteArray4506;
				} else {
					is = aClass223_4473.aShortArray2163;
					is_666_ = aClass223_4473.aByteArray2166;
					is_667_ = aClass223_4473.aShortArray2165;
					is_668_ = aClass223_4473.aShortArray2164;
				}
				aHa_Sub1_4492.aNativeInterface3923.copyNormals(is_667_, is_668_, is, is_666_, 3.0F / (float) aShort4457,
						3.0F / (float) (aShort4457 / 2 + aShort4457), 0, 12, anInt4474, buffer.getAddress());
				if (interface15_impl2.method49(2968)) {
					aClass307_4480.anInterface15_Impl2_2752 = interface15_impl2;
					aClass307_4480.aBoolean2750 = true;
				} else
					bool_662_ = false;
			} else
				bool_662_ = false;
		}
		if (bool_661_) {
			if (aClass307_4471.anInterface15_Impl2_2746 == null)
				aClass307_4471.anInterface15_Impl2_2746 = aHa_Sub1_4492.method1205(aBoolean4458, -117);
			Interface15_Impl2 interface15_impl2 = aClass307_4471.anInterface15_Impl2_2746;
			interface15_impl2.method46(anInt4474 * 8, -99, 8);
			Buffer buffer = interface15_impl2.method47(true, -8102);
			if (buffer == null)
				bool_662_ = false;
			else {
				aHa_Sub1_4492.aNativeInterface3923.copyTexCoords(aFloatArray4466, aFloatArray4462, 0, 8, anInt4474,
						buffer.getAddress());
				if (interface15_impl2.method49(2968)) {
					aClass307_4471.aBoolean2750 = true;
					aClass307_4471.anInterface15_Impl2_2752 = interface15_impl2;
				} else
					bool_662_ = false;
			}
		}
		return bool_662_;
	}

	static public void method1812(int i) {
		if (i <= 57)
			method1810((byte) -80);
		Class174.method1699();
	}

	public void a(int i) {
		int i_669_ = Class296_Sub4.anIntArray4598[i];
		int i_670_ = Class296_Sub4.anIntArray4618[i];
		for (int i_671_ = 0; anInt4468 > i_671_; i_671_++) {
			int i_672_ = ((anIntArray4489[i_671_] * i_669_ + anIntArray4482[i_671_] * i_670_) >> 14);
			anIntArray4489[i_671_] = (-(anIntArray4482[i_671_] * i_669_) + i_670_ * anIntArray4489[i_671_]) >> 14;
			anIntArray4482[i_671_] = i_672_;
		}
		method1806(-5);
		aBoolean4496 = false;
	}

	public void method1725(int i, int[] is, int i_673_, int i_674_, int i_675_, int i_676_, boolean bool) {
		int i_677_ = is.length;
		if (i == 0) {
			i_673_ <<= 4;
			i_674_ <<= 4;
			i_675_ <<= 4;
			Class182.anInt1876 = 0;
			HashTable.anInt2458 = 0;
			Class57.anInt667 = 0;
			int i_678_ = 0;
			for (int i_679_ = 0; i_677_ > i_679_; i_679_++) {
				int i_680_ = is[i_679_];
				if (i_680_ < anIntArrayArray4475.length) {
					int[] is_681_ = anIntArrayArray4475[i_680_];
					for (int i_682_ = 0; i_682_ < is_681_.length; i_682_++) {
						int i_683_ = is_681_[i_682_];
						Class182.anInt1876 += anIntArray4482[i_683_];
						Class57.anInt667 += anIntArray4505[i_683_];
						i_678_++;
						HashTable.anInt2458 += anIntArray4489[i_683_];
					}
				}
			}
			if (i_678_ <= 0) {
				HashTable.anInt2458 = i_675_;
				Class182.anInt1876 = i_673_;
				Class57.anInt667 = i_674_;
			} else {
				Class182.anInt1876 = i_673_ + Class182.anInt1876 / i_678_;
				HashTable.anInt2458 = i_675_ + HashTable.anInt2458 / i_678_;
				Class57.anInt667 = i_674_ + Class57.anInt667 / i_678_;
			}
		} else if (i == 1) {
			i_675_ <<= 4;
			i_673_ <<= 4;
			i_674_ <<= 4;
			for (int i_684_ = 0; i_677_ > i_684_; i_684_++) {
				int i_685_ = is[i_684_];
				if (anIntArrayArray4475.length > i_685_) {
					int[] is_686_ = anIntArrayArray4475[i_685_];
					for (int i_687_ = 0; i_687_ < is_686_.length; i_687_++) {
						int i_688_ = is_686_[i_687_];
						anIntArray4482[i_688_] += i_673_;
						anIntArray4505[i_688_] += i_674_;
						anIntArray4489[i_688_] += i_675_;
					}
				}
			}
		} else if (i == 2) {
			for (int i_689_ = 0; i_689_ < i_677_; i_689_++) {
				int i_690_ = is[i_689_];
				if (i_690_ < anIntArrayArray4475.length) {
					int[] is_691_ = anIntArrayArray4475[i_690_];
					if ((i_676_ & 0x1) != 0) {
						for (int i_692_ = 0; is_691_.length > i_692_; i_692_++) {
							int i_693_ = is_691_[i_692_];
							anIntArray4482[i_693_] -= Class182.anInt1876;
							anIntArray4505[i_693_] -= Class57.anInt667;
							anIntArray4489[i_693_] -= HashTable.anInt2458;
							if (i_673_ != 0) {
								int i_694_ = Class296_Sub4.anIntArray4598[i_673_];
								int i_695_ = Class296_Sub4.anIntArray4618[i_673_];
								int i_696_ = ((i_695_ * anIntArray4505[i_693_] + 16383
										- i_694_ * anIntArray4489[i_693_]) >> 14);
								anIntArray4489[i_693_] = (i_695_ * anIntArray4489[i_693_]
										+ anIntArray4505[i_693_] * i_694_ + 16383) >> 14;
								anIntArray4505[i_693_] = i_696_;
							}
							if (i_675_ != 0) {
								int i_697_ = Class296_Sub4.anIntArray4598[i_675_];
								int i_698_ = Class296_Sub4.anIntArray4618[i_675_];
								int i_699_ = (i_697_ * anIntArray4505[i_693_] + anIntArray4482[i_693_] * i_698_
										+ 16383) >> 14;
								anIntArray4505[i_693_] = (anIntArray4505[i_693_] * i_698_
										- anIntArray4482[i_693_] * i_697_ + 16383) >> 14;
								anIntArray4482[i_693_] = i_699_;
							}
							if (i_674_ != 0) {
								int i_700_ = Class296_Sub4.anIntArray4598[i_674_];
								int i_701_ = Class296_Sub4.anIntArray4618[i_674_];
								int i_702_ = (anIntArray4482[i_693_] * i_701_ + i_700_ * anIntArray4489[i_693_]
										+ 16383) >> 14;
								anIntArray4489[i_693_] = ((anIntArray4489[i_693_] * i_701_ + 16383
										- anIntArray4482[i_693_] * i_700_) >> 14);
								anIntArray4482[i_693_] = i_702_;
							}
							anIntArray4482[i_693_] += Class182.anInt1876;
							anIntArray4505[i_693_] += Class57.anInt667;
							anIntArray4489[i_693_] += HashTable.anInt2458;
						}
					} else {
						for (int i_703_ = 0; is_691_.length > i_703_; i_703_++) {
							int i_704_ = is_691_[i_703_];
							anIntArray4482[i_704_] -= Class182.anInt1876;
							anIntArray4505[i_704_] -= Class57.anInt667;
							anIntArray4489[i_704_] -= HashTable.anInt2458;
							if (i_675_ != 0) {
								int i_705_ = Class296_Sub4.anIntArray4598[i_675_];
								int i_706_ = Class296_Sub4.anIntArray4618[i_675_];
								int i_707_ = ((anIntArray4505[i_704_] * i_705_ + 16383
										+ i_706_ * anIntArray4482[i_704_]) >> 14);
								anIntArray4505[i_704_] = (anIntArray4505[i_704_] * i_706_
										- anIntArray4482[i_704_] * i_705_ + 16383) >> 14;
								anIntArray4482[i_704_] = i_707_;
							}
							if (i_673_ != 0) {
								int i_708_ = Class296_Sub4.anIntArray4598[i_673_];
								int i_709_ = Class296_Sub4.anIntArray4618[i_673_];
								int i_710_ = ((i_709_ * anIntArray4505[i_704_] + 16383
										- anIntArray4489[i_704_] * i_708_) >> 14);
								anIntArray4489[i_704_] = (anIntArray4489[i_704_] * i_709_
										+ anIntArray4505[i_704_] * i_708_ + 16383) >> 14;
								anIntArray4505[i_704_] = i_710_;
							}
							if (i_674_ != 0) {
								int i_711_ = Class296_Sub4.anIntArray4598[i_674_];
								int i_712_ = Class296_Sub4.anIntArray4618[i_674_];
								int i_713_ = ((anIntArray4489[i_704_] * i_711_
										+ (anIntArray4482[i_704_] * i_712_ + 16383)) >> 14);
								anIntArray4489[i_704_] = (i_712_ * anIntArray4489[i_704_]
										- anIntArray4482[i_704_] * i_711_ + 16383) >> 14;
								anIntArray4482[i_704_] = i_713_;
							}
							anIntArray4482[i_704_] += Class182.anInt1876;
							anIntArray4505[i_704_] += Class57.anInt667;
							anIntArray4489[i_704_] += HashTable.anInt2458;
						}
					}
				}
			}
			if (bool) {
				for (int i_714_ = 0; i_677_ > i_714_; i_714_++) {
					int i_715_ = is[i_714_];
					if (anIntArrayArray4475.length > i_715_) {
						int[] is_716_ = anIntArrayArray4475[i_715_];
						for (int i_717_ = 0; i_717_ < is_716_.length; i_717_++) {
							int i_718_ = is_716_[i_717_];
							int i_719_ = anIntArray4472[i_718_];
							int i_720_ = anIntArray4472[i_718_ + 1];
							for (int i_721_ = i_719_; i_721_ < i_720_; i_721_++) {
								int i_722_ = aShortArray4460[i_721_] - 1;
								if (i_722_ == -1)
									break;
								if (i_675_ != 0) {
									int i_723_ = Class296_Sub4.anIntArray4598[i_675_];
									int i_724_ = Class296_Sub4.anIntArray4618[i_675_];
									int i_725_ = ((i_723_ * aShortArray4484[i_722_]
											- (-(i_724_ * aShortArray4507[i_722_]) - 16383)) >> 14);
									aShortArray4484[i_722_] = (short) ((-(i_723_ * (aShortArray4507[i_722_])) + 16383
											+ (aShortArray4484[i_722_] * i_724_)) >> 14);
									aShortArray4507[i_722_] = (short) i_725_;
								}
								if (i_673_ != 0) {
									int i_726_ = Class296_Sub4.anIntArray4598[i_673_];
									int i_727_ = Class296_Sub4.anIntArray4618[i_673_];
									int i_728_ = ((-(aShortArray4483[i_722_] * i_726_) + 16383
											+ aShortArray4484[i_722_] * i_727_) >> 14);
									aShortArray4483[i_722_] = (short) (((i_727_ * aShortArray4483[i_722_])
											+ (aShortArray4484[i_722_] * i_726_) + 16383) >> 14);
									aShortArray4484[i_722_] = (short) i_728_;
								}
								if (i_674_ != 0) {
									int i_729_ = Class296_Sub4.anIntArray4598[i_674_];
									int i_730_ = Class296_Sub4.anIntArray4618[i_674_];
									int i_731_ = ((aShortArray4483[i_722_] * i_729_ + 16383
											+ i_730_ * aShortArray4507[i_722_]) >> 14);
									aShortArray4483[i_722_] = (short) (((i_730_ * aShortArray4483[i_722_])
											- (aShortArray4507[i_722_] * i_729_) + 16383) >> 14);
									aShortArray4507[i_722_] = (short) i_731_;
								}
							}
						}
					}
				}
				method1804(-1);
			}
		} else if (i == 3) {
			for (int i_732_ = 0; i_732_ < i_677_; i_732_++) {
				int i_733_ = is[i_732_];
				if (i_733_ < anIntArrayArray4475.length) {
					int[] is_734_ = anIntArrayArray4475[i_733_];
					for (int i_735_ = 0; is_734_.length > i_735_; i_735_++) {
						int i_736_ = is_734_[i_735_];
						anIntArray4482[i_736_] -= Class182.anInt1876;
						anIntArray4505[i_736_] -= Class57.anInt667;
						anIntArray4489[i_736_] -= HashTable.anInt2458;
						anIntArray4482[i_736_] = anIntArray4482[i_736_] * i_673_ >> 7;
						anIntArray4505[i_736_] = i_674_ * anIntArray4505[i_736_] >> 7;
						anIntArray4489[i_736_] = i_675_ * anIntArray4489[i_736_] >> 7;
						anIntArray4482[i_736_] += Class182.anInt1876;
						anIntArray4505[i_736_] += Class57.anInt667;
						anIntArray4489[i_736_] += HashTable.anInt2458;
					}
				}
			}
		} else if (i == 5) {
			if (anIntArrayArray4454 != null) {
				boolean bool_737_ = false;
				for (int i_738_ = 0; i_677_ > i_738_; i_738_++) {
					int i_739_ = is[i_738_];
					if (anIntArrayArray4454.length > i_739_) {
						int[] is_740_ = anIntArrayArray4454[i_739_];
						for (int i_741_ = 0; is_740_.length > i_741_; i_741_++) {
							int i_742_ = is_740_[i_741_];
							int i_743_ = i_673_ * 8 + (aByteArray4463[i_742_] & 0xff);
							if (i_743_ < 0)
								i_743_ = 0;
							else if (i_743_ > 255)
								i_743_ = 255;
							aByteArray4463[i_742_] = (byte) i_743_;
						}
						bool_737_ = bool_737_ | is_740_.length > 0;
					}
				}
				if (bool_737_) {
					if (aClass37Array4510 != null) {
						for (int i_744_ = 0; i_744_ < anInt4490; i_744_++) {
							Class37 class37 = aClass37Array4510[i_744_];
							Class271 class271 = aClass271Array4459[i_744_];
							class271.anInt2517 = ((-(aByteArray4463[class37.anInt367] & 0xff) + 255) << 24
									| class271.anInt2517 & 0xffffff);
						}
					}
					method1796(1018);
				}
			}
		} else if (i == 7) {
			if (anIntArrayArray4454 != null) {
				boolean bool_745_ = false;
				for (int i_746_ = 0; i_677_ > i_746_; i_746_++) {
					int i_747_ = is[i_746_];
					if (anIntArrayArray4454.length > i_747_) {
						int[] is_748_ = anIntArrayArray4454[i_747_];
						for (int i_749_ = 0; i_749_ < is_748_.length; i_749_++) {
							int i_750_ = is_748_[i_749_];
							int i_751_ = aShortArray4477[i_750_] & 0xffff;
							int i_752_ = i_751_ >> 10 & 0x3f;
							int i_753_ = (i_751_ & 0x3e3) >> 7;
							i_752_ = i_752_ + i_673_ & 0x3f;
							int i_754_ = i_751_ & 0x7f;
							i_753_ += i_674_ / 4;
							i_754_ += i_675_;
							if (i_753_ < 0)
								i_753_ = 0;
							else if (i_753_ > 7)
								i_753_ = 7;
							if (i_754_ >= 0) {
								if (i_754_ > 127)
									i_754_ = 127;
							} else
								i_754_ = 0;
							aShortArray4477[i_750_] = (short) (Class48.bitOR(i_754_,
									Class48.bitOR(i_753_ << 7, i_752_ << 10)));
						}
						bool_745_ = bool_745_ | is_748_.length > 0;
					}
				}
				if (bool_745_) {
					if (aClass37Array4510 != null) {
						for (int i_755_ = 0; anInt4490 > i_755_; i_755_++) {
							Class37 class37 = aClass37Array4510[i_755_];
							Class271 class271 = aClass271Array4459[i_755_];
							class271.anInt2517 = (class271.anInt2517 & ~0xffffff
									| (Class166_Sub1.anIntArray4300[(aShortArray4477[class37.anInt367] & 0xffff)])
											& 0xffffff);
						}
					}
					method1796(1018);
				}
			}
		} else if (i == 8) {
			if (anIntArrayArray4479 != null) {
				for (int i_756_ = 0; i_677_ > i_756_; i_756_++) {
					int i_757_ = is[i_756_];
					if (i_757_ < anIntArrayArray4479.length) {
						int[] is_758_ = anIntArrayArray4479[i_757_];
						for (int i_759_ = 0; i_759_ < is_758_.length; i_759_++) {
							Class271 class271 = aClass271Array4459[is_758_[i_759_]];
							class271.anInt2516 += i_673_;
							class271.anInt2511 += i_674_;
						}
					}
				}
			}
		} else if (i == 10) {
			if (anIntArrayArray4479 != null) {
				for (int i_760_ = 0; i_677_ > i_760_; i_760_++) {
					int i_761_ = is[i_760_];
					if (i_761_ < anIntArrayArray4479.length) {
						int[] is_762_ = anIntArrayArray4479[i_761_];
						for (int i_763_ = 0; is_762_.length > i_763_; i_763_++) {
							Class271 class271 = aClass271Array4459[is_762_[i_763_]];
							class271.anInt2515 = class271.anInt2515 * i_673_ >> 7;
							class271.anInt2513 = class271.anInt2513 * i_674_ >> 7;
						}
					}
				}
			}
		} else if (i == 9) {
			if (anIntArrayArray4479 != null) {
				for (int i_764_ = 0; i_677_ > i_764_; i_764_++) {
					int i_765_ = is[i_764_];
					if (i_765_ < anIntArrayArray4479.length) {
						int[] is_766_ = anIntArrayArray4479[i_765_];
						for (int i_767_ = 0; i_767_ < is_766_.length; i_767_++) {
							Class271 class271 = aClass271Array4459[is_766_[i_767_]];
							class271.anInt2514 = i_673_ + class271.anInt2514 & 0x3fff;
						}
					}
				}
			}
		}
	}

	public int na() {
		if (!aBoolean4496)
			method1805((byte) -89);
		return anInt4498;
	}

	Class178_Sub3(ha_Sub1 var_ha_Sub1, int i, int i_768_, boolean bool, boolean bool_769_) {
		anInt4468 = 0;
		anInt4493 = 0;
		aBoolean4502 = false;
		aBoolean4458 = false;
		anInt4478 = 0;
		aBoolean4496 = false;
		anInt4508 = 0;
		aBoolean4499 = false;
		aBoolean4458 = bool_769_;
		aHa_Sub1_4492 = var_ha_Sub1;
		anInt4481 = i_768_;
		anInt4500 = i;
		if (bool || Class230.method2105(85, anInt4481, anInt4500))
			aClass307_4467 = new Class307(Class296_Sub51_Sub26.method3150(anInt4500, (byte) 108, anInt4481));
		if (bool || Class123_Sub2.method1068(anInt4481, (byte) 114, anInt4500))
			aClass307_4471 = new Class307(Class16_Sub1_Sub1.method239(0, anInt4500, anInt4481));
		if (bool || SubInPacket.method2242(anInt4500, -83, anInt4481))
			aClass307_4486 = new Class307(Class184.method1857(anInt4481, anInt4500, (byte) 12));
		if (bool || ConfigurationDefinition.method687(anInt4500, anInt4481, (byte) 40))
			aClass307_4480 = new Class307(Class47.method599((byte) -46, anInt4500, anInt4481));
		if (bool || Class41_Sub26.method498(anInt4500, anInt4481, (byte) -126))
			aClass293_4501 = new Class293(Class338_Sub3_Sub2_Sub1.method3556((byte) 77, anInt4500, anInt4481));
	}
}
