package net.zaros.client;
/* Class296_Sub51_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Random;

final class Class296_Sub51_Sub5 extends TextureOperation {
	private int anInt6357 = 2048;
	private int anInt6358;
	private int anInt6359;
	private int anInt6360;
	private int anInt6361;
	private int anInt6362 = 819;
	private int anInt6363;
	private int anInt6364;
	static int anInt6365 = -1;
	private int anInt6366;
	private int anInt6367;

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		int i_1_ = i_0_;
		while_90_ : do {
			while_89_ : do {
				while_88_ : do {
					while_87_ : do {
						while_86_ : do {
							while_85_ : do {
								while_84_ : do {
									do {
										if (i_1_ != 0) {
											if (i_1_ != 1) {
												if (i_1_ != 2) {
													if (i_1_ != 3) {
														if (i_1_ != 4) {
															if (i_1_ != 5) {
																if (i_1_ != 6) {
																	if (i_1_ != 7) {
																		if (i_1_ == 8)
																			break while_89_;
																		break while_90_;
																	}
																} else
																	break while_87_;
																break while_88_;
															}
														} else
															break while_85_;
														break while_86_;
													}
												} else
													break;
												break while_84_;
											}
										} else {
											anInt6360 = class296_sub17.g1();
											break while_90_;
										}
										anInt6359 = class296_sub17.g2();
										break while_90_;
									} while (false);
									anInt6357 = class296_sub17.g2();
									break while_90_;
								} while (false);
								anInt6367 = class296_sub17.g2();
								break while_90_;
							} while (false);
							anInt6362 = class296_sub17.g2();
							break while_90_;
						} while (false);
						anInt6364 = class296_sub17.g2();
						break while_90_;
					} while (false);
					anInt6363 = class296_sub17.g1();
					break while_90_;
				} while (false);
				anInt6361 = class296_sub17.g2();
				break while_90_;
			} while (false);
			anInt6366 = class296_sub17.g2();
		} while (false);
		if (i >= -84)
			method3089(-27);
	}

	public Class296_Sub51_Sub5() {
		super(0, true);
		anInt6361 = 1024;
		anInt6359 = 1024;
		anInt6363 = 0;
		anInt6366 = 1024;
		anInt6364 = 1024;
		anInt6367 = 409;
		anInt6360 = 0;
	}

	static final void method3089(int i) {
		Class307.method3287(0);
		int i_2_ = -120 % ((i - 58) / 32);
		Class108.method948(-2060);
	}

	final int[] get_monochrome_output(int i, int i_3_) {
		int[] is = aClass318_5035.method3335(i_3_, (byte) 28);
		if (i != 0)
			return null;
		if (aClass318_5035.aBoolean2819) {
			int[][] is_4_ = aClass318_5035.method3337(i + 106);
			int i_5_ = 0;
			int i_6_ = 0;
			int i_7_ = 0;
			int i_8_ = 0;
			int i_9_ = 0;
			boolean bool = true;
			boolean bool_10_ = true;
			int i_11_ = 0;
			int i_12_ = 0;
			int i_13_ = anInt6359 * Class41_Sub10.anInt3769 >> 12;
			int i_14_ = anInt6357 * Class41_Sub10.anInt3769 >> 12;
			int i_15_ = anInt6367 * Class296_Sub35_Sub2.anInt6114 >> 12;
			int i_16_ = anInt6362 * Class296_Sub35_Sub2.anInt6114 >> 12;
			if (i_16_ <= 1)
				return is_4_[i_3_];
			anInt6358 = anInt6364 * (Class41_Sub10.anInt3769 / 8) >> 12;
			int i_17_ = 1 + Class41_Sub10.anInt3769 / i_13_;
			int[][] is_18_ = new int[i_17_][3];
			int[][] is_19_ = new int[i_17_][3];
			Random random = new Random((long) anInt6360);
			for (;;) {
				int i_20_ = i_13_ + s_Sub3.method3373(-i_13_ + i_14_, random, 6445);
				int i_21_ = s_Sub3.method3373(-i_15_ + i_16_, random, 6445) + i_15_;
				int i_22_ = i_8_ + i_20_;
				if (i_22_ > Class41_Sub10.anInt3769) {
					i_20_ = Class41_Sub10.anInt3769 - i_8_;
					i_22_ = Class41_Sub10.anInt3769;
				}
				int i_23_;
				if (!bool_10_) {
					int i_24_ = i_9_;
					int[] is_25_ = is_19_[i_9_];
					int i_26_ = 0;
					int i_27_ = i_22_ + i_5_;
					if (i_27_ < 0)
						i_27_ += Class41_Sub10.anInt3769;
					if (i_27_ > Class41_Sub10.anInt3769)
						i_27_ -= Class41_Sub10.anInt3769;
					for (;;) {
						int[] is_28_ = is_19_[i_24_];
						if (i_27_ >= is_28_[0] && i_27_ <= is_28_[1])
							break;
						i_26_++;
						if (i_11_ <= ++i_24_)
							i_24_ = 0;
					}
					i_23_ = is_25_[2];
					if (i_9_ != i_24_) {
						int i_29_ = i_8_ + i_5_;
						if (i_29_ < 0)
							i_29_ += Class41_Sub10.anInt3769;
						if (Class41_Sub10.anInt3769 < i_29_)
							i_29_ -= Class41_Sub10.anInt3769;
						for (int i_30_ = 1; i_30_ <= i_26_; i_30_++) {
							int[] is_31_ = is_19_[(i_30_ + i_9_) % i_11_];
							i_23_ = Math.max(i_23_, is_31_[2]);
						}
						for (int i_32_ = 0; i_26_ >= i_32_; i_32_++) {
							int[] is_33_ = is_19_[(i_32_ + i_9_) % i_11_];
							int i_34_ = is_33_[2];
							if (i_23_ != i_34_) {
								int i_35_ = is_33_[0];
								int i_36_ = is_33_[1];
								int i_37_;
								int i_38_;
								if (i_29_ < i_27_) {
									i_37_ = Math.max(i_29_, i_35_);
									i_38_ = Math.min(i_27_, i_36_);
								} else if (i_35_ == 0) {
									i_37_ = 0;
									i_38_ = Math.min(i_27_, i_36_);
								} else {
									i_37_ = Math.max(i_29_, i_35_);
									i_38_ = Class41_Sub10.anInt3769;
								}
								method3091(i_23_ - i_34_, i_38_ - i_37_, i_37_ + i_7_, -6, is_4_, random, i_34_);
							}
						}
					}
					i_9_ = i_24_;
				} else
					i_23_ = 0;
				if (i_23_ + i_21_ <= Class296_Sub35_Sub2.anInt6114)
					bool = false;
				else
					i_21_ = Class296_Sub35_Sub2.anInt6114 - i_23_;
				if (i_22_ != Class41_Sub10.anInt3769) {
					int[] is_39_ = is_18_[i_12_++];
					is_39_[2] = i_23_ + i_21_;
					is_39_[0] = i_8_;
					is_39_[1] = i_22_;
					method3091(i_21_, i_20_, i_8_ + i_6_, -6, is_4_, random, i_23_);
					i_8_ = i_22_;
				} else {
					method3091(i_21_, i_20_, i_6_ + i_8_, i - 6, is_4_, random, i_23_);
					if (bool)
						break;
					bool = true;
					int[] is_40_ = is_18_[i_12_++];
					is_40_[1] = i_22_;
					is_40_[0] = i_8_;
					is_40_[2] = i_21_ + i_23_;
					int[][] is_41_ = is_19_;
					is_19_ = is_18_;
					is_18_ = is_41_;
					i_11_ = i_12_;
					i_7_ = i_6_;
					i_12_ = 0;
					i_6_ = s_Sub3.method3373(Class41_Sub10.anInt3769, random, 6445);
					i_5_ = -i_7_ + i_6_;
					i_8_ = 0;
					int i_42_ = i_5_;
					if (i_42_ < 0)
						i_42_ += Class41_Sub10.anInt3769;
					if (i_42_ > Class41_Sub10.anInt3769)
						i_42_ -= Class41_Sub10.anInt3769;
					i_9_ = 0;
					for (;;) {
						int[] is_43_ = is_19_[i_9_];
						if (i_42_ >= is_43_[0] && i_42_ <= is_43_[1])
							break;
						if (++i_9_ >= i_11_)
							i_9_ = 0;
					}
					bool_10_ = false;
				}
			}
		}
		return is;
	}

	static final void method3090(boolean bool) {
		Class338_Sub3_Sub5_Sub1.aBoolean6647 = false;
		if (bool == true)
			Class366_Sub8.method3794(bool);
	}

	private final void method3091(int i, int i_44_, int i_45_, int i_46_, int[][] is, Random random, int i_47_) {
		if (i_46_ != -6)
			get_monochrome_output(65, -112);
		int i_48_ = (anInt6366 <= 0 ? 4096 : -s_Sub3.method3373(anInt6366, random, 6445) + 4096);
		int i_49_ = anInt6358 * anInt6361 >> 12;
		int i_50_ = (anInt6358 - (i_49_ <= 0 ? 0 : s_Sub3.method3373(i_49_, random, i_46_ ^ ~0x1928)));
		if (i_45_ >= Class41_Sub10.anInt3769)
			i_45_ -= Class41_Sub10.anInt3769;
		if (i_50_ > 0) {
			if (i > 0 && i_44_ > 0) {
				int i_51_ = i_44_ / 2;
				int i_52_ = i / 2;
				int i_53_ = i_51_ < i_50_ ? i_51_ : i_50_;
				int i_54_ = i_52_ < i_50_ ? i_52_ : i_50_;
				int i_55_ = i_45_ + i_53_;
				int i_56_ = -(i_53_ * 2) + i_44_;
				for (int i_57_ = 0; i > i_57_; i_57_++) {
					int[] is_58_ = is[i_47_ + i_57_];
					if (i_57_ < i_54_) {
						int i_59_ = i_57_ * i_48_ / i_54_;
						if (anInt6363 != 0) {
							for (int i_60_ = 0; i_53_ > i_60_; i_60_++) {
								int i_61_ = i_48_ * i_60_ / i_53_;
								is_58_[((Class41_Sub25.anInt3803) & i_45_ + i_60_)] = is_58_[((-i_60_ + i_45_ + i_44_ - 1) & (Class41_Sub25.anInt3803))] = i_61_ >= i_59_ ? i_59_ : i_61_;
							}
						} else {
							for (int i_62_ = 0; i_53_ > i_62_; i_62_++) {
								int i_63_ = i_62_ * i_48_ / i_53_;
								is_58_[(i_62_ + i_45_ & (Class41_Sub25.anInt3803))] = is_58_[((i_45_ - 1 - (-i_44_ + i_62_)) & (Class41_Sub25.anInt3803))] = i_63_ * i_59_ >> 12;
							}
						}
						if (Class41_Sub10.anInt3769 >= i_55_ + i_56_)
							Class291.method2407(is_58_, i_55_, i_56_, i_59_);
						else {
							int i_64_ = Class41_Sub10.anInt3769 - i_55_;
							Class291.method2407(is_58_, i_55_, i_64_, i_59_);
							Class291.method2407(is_58_, 0, -i_64_ + i_56_, i_59_);
						}
					} else {
						int i_65_ = -i_57_ + (i - 1);
						if (i_65_ < i_54_) {
							int i_66_ = i_65_ * i_48_ / i_54_;
							if (anInt6363 == 0) {
								for (int i_67_ = 0; i_67_ < i_53_; i_67_++) {
									int i_68_ = i_67_ * i_48_ / i_53_;
									is_58_[(i_67_ + i_45_ & (Class41_Sub25.anInt3803))] = is_58_[((i_44_ - 1 + i_45_ - i_67_ & Class41_Sub25.anInt3803))] = i_68_ * i_66_ >> 12;
								}
							} else {
								for (int i_69_ = 0; i_69_ < i_53_; i_69_++) {
									int i_70_ = i_69_ * i_48_ / i_53_;
									is_58_[(i_69_ + i_45_ & (Class41_Sub25.anInt3803))] = is_58_[((Class41_Sub25.anInt3803 & i_45_ - 1 + i_44_ - i_69_))] = i_66_ <= i_70_ ? i_66_ : i_70_;
								}
							}
							if (Class41_Sub10.anInt3769 >= i_55_ + i_56_)
								Class291.method2407(is_58_, i_55_, i_56_, i_66_);
							else {
								int i_71_ = Class41_Sub10.anInt3769 - i_55_;
								Class291.method2407(is_58_, i_55_, i_71_, i_66_);
								Class291.method2407(is_58_, 0, i_56_ - i_71_, i_66_);
							}
						} else {
							for (int i_72_ = 0; i_72_ < i_53_; i_72_++)
								is_58_[(i_72_ + i_45_ & (Class41_Sub25.anInt3803))] = is_58_[((Class41_Sub25.anInt3803 & -i_72_ - 1 + (i_45_ + i_44_)))] = i_48_ * i_72_ / i_53_;
							if (Class41_Sub10.anInt3769 >= i_55_ + i_56_)
								Class291.method2407(is_58_, i_55_, i_56_, i_48_);
							else {
								int i_73_ = -i_55_ + Class41_Sub10.anInt3769;
								Class291.method2407(is_58_, i_55_, i_73_, i_48_);
								Class291.method2407(is_58_, 0, i_56_ - i_73_, i_48_);
							}
						}
					}
				}
			}
		} else if (Class41_Sub10.anInt3769 < i_44_ + i_45_) {
			int i_74_ = -i_45_ + Class41_Sub10.anInt3769;
			for (int i_75_ = 0; i_75_ < i; i_75_++) {
				int[] is_76_ = is[i_47_ + i_75_];
				Class291.method2407(is_76_, i_45_, i_74_, i_48_);
				Class291.method2407(is_76_, 0, i_44_ - i_74_, i_48_);
			}
		} else {
			for (int i_77_ = 0; i_77_ < i; i_77_++)
				Class291.method2407(is[i_47_ + i_77_], i_45_, i_44_, i_48_);
		}
	}

	static final boolean method3092(int i, int i_78_, int i_79_) {
		if (i_78_ != -24276)
			method3092(-12, 69, -88);
		if ((i & 0x100100) == 0)
			return false;
		return true;
	}

	final void method3076(byte i) {
		int i_80_ = 48 % ((i + 58) / 40);
	}
}
