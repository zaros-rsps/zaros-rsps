package net.zaros.client;

/* Class41_Sub24 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub24 extends Class41 {
	static HashTable aClass263_3802 = new HashTable(16);

	Class41_Sub24(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	public static void method492(int i) {
		if (i != 16)
			method493(null, 22, false, (byte) -98);
		aClass263_3802 = null;
	}

	final int method380(int i, byte i_0_) {
		if (i_0_ != 41)
			return 57;
		return 1;
	}

	static final boolean method493(String string, int i, boolean bool, byte i_1_) {
		if (i < 2 || i > 36)
			throw new IllegalArgumentException("Invalid radix:" + i);
		boolean bool_2_ = false;
		boolean bool_3_ = false;
		int i_4_ = 0;
		int i_5_ = string.length();
		for (int i_6_ = 0; i_5_ > i_6_; i_6_++) {
			int i_7_ = string.charAt(i_6_);
			if (i_6_ == 0) {
				if (i_7_ == 45) {
					bool_2_ = true;
					continue;
				}
				if (i_7_ == 43 && bool)
					continue;
			}
			if (i_7_ >= 48 && i_7_ <= 57)
				i_7_ -= 48;
			else if (i_7_ >= 65 && i_7_ <= 90)
				i_7_ -= 55;
			else if (i_7_ >= 97 && i_7_ <= 122)
				i_7_ -= 87;
			else
				return false;
			if (i_7_ >= i)
				return false;
			if (bool_2_)
				i_7_ = -i_7_;
			int i_8_ = i_7_ + i * i_4_;
			if (i_8_ / i != i_4_)
				return false;
			bool_3_ = true;
			i_4_ = i_8_;
		}
		int i_9_ = 20 / ((-10 - i_1_) / 51);
		return bool_3_;
	}

	final int method494(int i) {
		if (i <= 114)
			method383((byte) 17);
		return anInt389;
	}

	Class41_Sub24(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final int method383(byte i) {
		if (i != 110)
			aClass263_3802 = null;
		return 1;
	}

	final void method386(int i) {
		if (i != 2)
			aClass263_3802 = null;
		if (anInt389 != 1 && anInt389 != 0)
			anInt389 = method383((byte) 110);
	}

	final void method381(int i, byte i_10_) {
		if (i_10_ != -110)
			method380(124, (byte) -123);
		anInt389 = i;
	}
}
