package net.zaros.client;

/* aa_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class aa_Sub1 extends aa {
	static Queue aClass145_3719 = new Queue();
	Class69_Sub1_Sub1 aClass69_Sub1_Sub1_3720;
	static byte[][][] aByteArrayArrayArray3721;
	static Class209 aClass209_3722 = new Class209(13);
	static boolean memb1 = false;

	static final int method155(int i, int i_0_, int i_1_, int i_2_) {
		if (Class360_Sub2.aSArray5304 == null)
			return 0;
		int i_3_ = i_1_ >> 9;
		if (i != -1537652855)
			method156((byte) -46);
		int i_4_ = i_2_ >> 9;
		if (i_3_ < 0 || i_4_ < 0 || i_3_ > Class198.currentMapSizeX - 1 || i_4_ > Class296_Sub38.currentMapSizeY - 1)
			return 0;
		int i_5_ = i_0_;
		if (i_5_ < 3 && (Class41_Sub18.aByteArrayArrayArray3786[1][i_3_][i_4_] & 0x2) != 0)
			i_5_++;
		return Class360_Sub2.aSArray5304[i_5_].method3349(0, i_2_, i_1_);
	}

	aa_Sub1(ha_Sub3 var_ha_Sub3, int i, int i_6_, byte[] is) {
		aClass69_Sub1_Sub1_3720 = Class338_Sub3_Sub5.method3583(is, var_ha_Sub3, i_6_, 6406, i, -121, false, 6406);
		aClass69_Sub1_Sub1_3720.method733(false, false, true);
	}

	public static void method156(byte i) {
		aClass145_3719 = null;
		if (i <= 9)
			aClass209_3722 = null;
		aClass209_3722 = null;
		aByteArrayArrayArray3721 = null;
	}
}
