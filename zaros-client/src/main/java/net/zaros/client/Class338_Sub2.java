package net.zaros.client;

/* Class338_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub2 extends Class338 {
	static Class209 aClass209_5192 = new Class209(3);
	Class338_Sub3 aClass338_Sub3_5193;
	Class338_Sub5[] aClass338_Sub5Array5194;
	static Class247[][][] aClass247ArrayArrayArray5195;
	boolean aBoolean5196;
	static int[] anIntArray5197 = {4, 2, 1, 1, 2, 2, 3, 1, 3, 3, 3, 2, 0};
	static int anInt5198;
	static int mapLoadType;
	static Class225[] aClass225Array5200 = new Class225[8];
	static Js5 aClass138_5201;

	final boolean method3455(int i, int i_0_, byte i_1_, ha var_ha) {
		if (i_1_ != 51)
			return true;
		int i_2_ = aClass338_Sub3_5193.method3458(0);
		if (aClass338_Sub5Array5194 != null) {
			for (int i_3_ = 0; aClass338_Sub5Array5194.length > i_3_; i_3_++) {
				aClass338_Sub5Array5194[i_3_].anInt5226 <<= i_2_;
				if (aClass338_Sub5Array5194[i_3_].method3591(i, i_0_) && aClass338_Sub3_5193.method3475(i_0_, -84, var_ha, i)) {
					aClass338_Sub5Array5194[i_3_].anInt5226 >>= i_2_;
					return true;
				}
				aClass338_Sub5Array5194[i_3_].anInt5226 >>= i_2_;
			}
		}
		return false;
	}

	static final void method3456(boolean bool, int i) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i, 8);
		if (bool != true)
			mapLoadType = 35;
		class296_sub39_sub5.insertIntoQueue();
	}

	public static void method3457(int i) {
		aClass225Array5200 = null;
		aClass247ArrayArrayArray5195 = null;
		aClass209_5192 = null;
		aClass138_5201 = null;
		if (i != -27017)
			mapLoadType = -104;
		anIntArray5197 = null;
	}

	static {
		mapLoadType = 0;
	}
}
