package net.zaros.client;

/* Class368_Sub11 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub11 extends Class368 {
	static IncomingPacket aClass231_5483 = new IncomingPacket(67, 4);
	private int anInt5484;
	private int anInt5485;
	static int anInt5486;

	Class368_Sub11(Packet class296_sub17) {
		super(class296_sub17);
		anInt5484 = class296_sub17.g2();
		anInt5485 = class296_sub17.g4();
	}

	final void method3807(byte i) {
		Class387.anInt3711 = Class29.anInt307;
		AdvancedMemoryCache.anInt1169 = Class122.anInt3550;
		int i_0_ = 124 / ((52 - i) / 52);
		Class69_Sub1.anInt5712 = Class338_Sub3.anInt5209;
		Class332.anInt2943 = Class369.anInt3139;
		Class166_Sub1.anInt4304 = Class29.anInt307 + anInt5484;
		CS2Stack.anInt2246 = Class338_Sub8_Sub2.anInt6588;
		Class338_Sub8_Sub2.anInt6588 = (anInt5485 & 0xffa196) >>> 16;
		Class122.anInt3550 = anInt5485 >>> 8 & 0x440000ff;
		Class369.anInt3139 = anInt5485 & 0xff;
		Class338_Sub3.anInt5209 = anInt5485 >>> 24;
	}

	static final void method3843() {
		for (int i = Class360_Sub5.anInt5328; i < Class368_Sub9.anInt5477; i++) {
			for (int i_1_ = 0; i_1_ < Class228.anInt2201; i_1_++) {
				for (int i_2_ = 0; i_2_ < Class368_Sub12.anInt5488; i_2_++) {
					Class247 class247 = (Class338_Sub2.aClass247ArrayArrayArray5195[i][i_1_][i_2_]);
					if (class247 != null) {
						Class338_Sub3_Sub4 class338_sub3_sub4 = class247.aClass338_Sub3_Sub4_2348;
						Class338_Sub3_Sub4 class338_sub3_sub4_3_ = class247.aClass338_Sub3_Sub4_2343;
						if (class338_sub3_sub4 != null && class338_sub3_sub4.method3468(-34)) {
							ha_Sub3.method1317(class338_sub3_sub4, i, i_1_, i_2_, 1, 1);
							if (class338_sub3_sub4_3_ != null && class338_sub3_sub4_3_.method3468(-51)) {
								ha_Sub3.method1317(class338_sub3_sub4_3_, i, i_1_, i_2_, 1, 1);
								class338_sub3_sub4_3_.method3467(0, 0, class338_sub3_sub4, false, 0, -102, Class16_Sub2.aHa3733);
								class338_sub3_sub4_3_.method3472((byte) -3);
							}
							class338_sub3_sub4.method3472((byte) -97);
						}
						for (Class234 class234 = class247.aClass234_2341; class234 != null; class234 = class234.aClass234_2224) {
							Class338_Sub3_Sub1 class338_sub3_sub1 = class234.aClass338_Sub3_Sub1_2226;
							if (class338_sub3_sub1 != null && class338_sub3_sub1.method3468(-109)) {
								ha_Sub3.method1317(class338_sub3_sub1, i, i_1_, i_2_, (class338_sub3_sub1.aShort6559 - class338_sub3_sub1.aShort6560 + 1), (class338_sub3_sub1.aShort6558 - class338_sub3_sub1.aShort6564 + 1));
								class338_sub3_sub1.method3472((byte) -104);
							}
						}
						Class338_Sub3_Sub5 class338_sub3_sub5 = class247.aClass338_Sub3_Sub5_2346;
						if (class338_sub3_sub5 != null && class338_sub3_sub5.method3468(-128)) {
							Class41_Sub29.method519(class338_sub3_sub5, i, i_1_, i_2_);
							class338_sub3_sub5.method3472((byte) 93);
						}
					}
				}
			}
		}
	}

	static final boolean method3844(int i, int i_4_) {
		if (i <= 83)
			return false;
		if (i_4_ != 0 && i_4_ != 2)
			return false;
		return true;
	}

	public static void method3845(int i) {
		aClass231_5483 = null;
		if (i != 4)
			aClass231_5483 = null;
	}
}
