package net.zaros.client;

/* Class296_Sub51_Sub12 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub12 extends TextureOperation {
	private int anInt6403;
	private int anInt6404;
	private int anInt6405;
	private int anInt6406;
	private int anInt6407 = 0;
	private int anInt6408;
	static Class209 aClass209_6409;
	private int anInt6410;
	static int anInt6411 = 0;
	private int anInt6412 = 0;
	private int anInt6413;

	private final void method3108(int i, int i_27_, int i_28_, int i_29_) {
		int i_30_ = i_28_ >= i_27_ ? i_28_ : i_27_;
		i_30_ = i_30_ >= i ? i_30_ : i;
		int i_31_ = i_27_ >= i_28_ ? i_28_ : i_27_;
		i_31_ = i_31_ > i ? i : i_31_;
		int i_32_ = i_30_ - i_31_;
		anInt6403 = (i_30_ + i_31_) / 2;
		if (i_32_ > 0) {
			int i_33_ = (i_30_ - i_27_ << 12) / i_32_;
			int i_34_ = (-i_28_ + i_30_ << 12) / i_32_;
			int i_35_ = (i_30_ - i << 12) / i_32_;
			if (i_27_ == i_30_)
				anInt6408 = i_28_ != i_31_ ? 4096 - i_34_ : i_35_ + 20480;
			else if (i_30_ == i_28_)
				anInt6408 = i == i_31_ ? i_33_ + 4096 : -i_35_ + 12288;
			else
				anInt6408 = i_27_ != i_31_ ? -i_33_ + 20480 : i_34_ + 12288;
			anInt6408 /= 6;
		} else
			anInt6408 = 0;
		if (anInt6403 > 0 && anInt6403 < 4096)
			anInt6413 = (i_32_ << 12) / (anInt6403 > 2048 ? -(anInt6403 * 2) + 8192 : anInt6403 * 2);
		else
			anInt6413 = 0;
		if (i_29_ != -30251)
			PlayerUpdate.updateInvisiblePlayer(null, -9);
	}

	final int[][] get_colour_output(int i, int i_36_) {
		int[][] is = aClass86_5034.method823((byte) 33, i);
		if (i_36_ != 17621)
			method3071(-72, null, -115);
		if (aClass86_5034.aBoolean939) {
			int[][] is_37_ = this.method3075((byte) 111, 0, i);
			int[] is_38_ = is_37_[0];
			int[] is_39_ = is_37_[1];
			int[] is_40_ = is_37_[2];
			int[] is_41_ = is[0];
			int[] is_42_ = is[1];
			int[] is_43_ = is[2];
			for (int i_44_ = 0; Class41_Sub10.anInt3769 > i_44_; i_44_++) {
				method3108(is_40_[i_44_], is_38_[i_44_], is_39_[i_44_], -30251);
				anInt6408 += anInt6406;
				anInt6403 += anInt6412;
				anInt6413 += anInt6407;
				for (/**/; anInt6408 < 0; anInt6408 += 4096) {
					/* empty */
				}
				if (anInt6413 < 0)
					anInt6413 = 0;
				for (/**/; anInt6408 > 4096; anInt6408 -= 4096) {
					/* empty */
				}
				if (anInt6413 > 4096)
					anInt6413 = 4096;
				if (anInt6403 < 0)
					anInt6403 = 0;
				if (anInt6403 > 4096)
					anInt6403 = 4096;
				method3109(anInt6413, anInt6408, anInt6403, -6054);
				is_41_[i_44_] = anInt6405;
				is_42_[i_44_] = anInt6404;
				is_43_[i_44_] = anInt6410;
			}
		}
		return is;
	}

	public Class296_Sub51_Sub12() {
		super(1, false);
		anInt6406 = 0;
	}

	final void method3071(int i, Packet class296_sub17, int i_45_) {
		int i_46_ = i_45_;
		while_113_ : do {
			do {
				if (i_46_ != 0) {
					if (i_46_ != 1) {
						if (i_46_ == 2)
							break;
						break while_113_;
					}
				} else {
					anInt6406 = class296_sub17.g2b();
					break while_113_;
				}
				anInt6407 = (class296_sub17.g1b() << 12) / 100;
				break while_113_;
			} while (false);
			anInt6412 = (class296_sub17.g1b() << 12) / 100;
		} while (false);
		if (i > -84)
			method3108(-37, 112, -18, 104);
	}

	private final void method3109(int i, int i_47_, int i_48_, int i_49_) {
		if (i_49_ != -6054)
			get_colour_output(-85, -17);
		int i_50_ = (i_48_ > 2048 ? -(i * i_48_ >> 12) + (i_48_ + i) : i_48_ * (i + 4096) >> 12);
		while_117_ : do {
			if (i_50_ > 0) {
				i_47_ *= 6;
				int i_51_ = i_48_ + i_48_ - i_50_;
				int i_52_ = (i_50_ - i_51_ << 12) / i_50_;
				int i_53_ = i_47_ >> 12;
				int i_54_ = i_47_ - (i_53_ << 12);
				int i_55_ = i_50_;
				i_55_ = i_52_ * i_55_ >> 12;
				i_55_ = i_54_ * i_55_ >> 12;
				int i_56_ = i_51_ + i_55_;
				int i_57_ = i_50_ - i_55_;
				int i_58_ = i_53_;
				while_116_ : do {
					while_115_ : do {
						while_114_ : do {
							do {
								if (i_58_ != 0) {
									if (i_58_ != 1) {
										if (i_58_ != 2) {
											if (i_58_ != 3) {
												if (i_58_ != 4) {
													if (i_58_ != 5)
														break while_117_;
												} else
													break while_115_;
												break while_116_;
											}
										} else
											break;
										break while_114_;
									}
								} else {
									anInt6410 = i_51_;
									anInt6404 = i_56_;
									anInt6405 = i_50_;
									return;
								}
								anInt6410 = i_51_;
								anInt6404 = i_50_;
								anInt6405 = i_57_;
								return;
							} while (false);
							anInt6410 = i_56_;
							anInt6404 = i_50_;
							anInt6405 = i_51_;
							return;
						} while (false);
						anInt6410 = i_50_;
						anInt6404 = i_57_;
						anInt6405 = i_51_;
						return;
					} while (false);
					anInt6405 = i_56_;
					anInt6410 = i_50_;
					anInt6404 = i_51_;
					return;
				} while (false);
				anInt6410 = i_57_;
				anInt6405 = i_50_;
				anInt6404 = i_51_;
				break;
			}
			anInt6405 = anInt6404 = anInt6410 = i_48_;
		} while (false);
	}

	public static void method3110(byte i) {
		aClass209_6409 = null;
		if (i != 32)
			aClass209_6409 = null;
	}

	static {
		aClass209_6409 = new Class209(32);
	}
}
