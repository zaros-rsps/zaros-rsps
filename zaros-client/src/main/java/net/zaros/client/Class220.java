package net.zaros.client;

/* Class220 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class220 {
	private int[] anIntArray2142;
	private int[] anIntArray2143;
	private int[] anIntArray2144;
	private int[] anIntArray2145;
	private int[] anIntArray2146;
	private int[] anIntArray2147;
	static GameType game4 = new GameType("game4", "Game 4", 3);
	private int[] anIntArray2149;
	static int anInt2150 = -1;
	static Class151 aClass151_2151;

	static final int method2068(int i, int i_0_) {
		int i_1_ = i_0_ & 0x3f;
		int i_2_ = (i_0_ & 0xd2) >> 6;
		if (i != -30724)
			aClass151_2151 = null;
		if (i_1_ != 18) {
			if (i_1_ == 19 || i_1_ == 21) {
				if (i_2_ == 0)
					return 16;
				if (i_2_ == 1)
					return 32;
				if (i_2_ == 2)
					return 64;
				if (i_2_ == 3)
					return 128;
			}
		} else {
			if (i_2_ == 0)
				return 1;
			if (i_2_ == 1)
				return 2;
			if (i_2_ == 2)
				return 4;
			if (i_2_ == 3)
				return 8;
		}
		return 0;
	}

	static final String method2069(int i, boolean bool, int i_3_, int i_4_) {
		if (i < 2 || i > 36)
			throw new IllegalArgumentException("Invalid radix:" + i);
		if (!bool || i_3_ < 0)
			return Integer.toString(i_3_, i);
		int i_5_ = 2;
		for (int i_6_ = i_3_ / i; i_6_ != 0; i_6_ /= i)
			i_5_++;
		char[] cs = new char[i_5_];
		cs[0] = '+';
		for (int i_7_ = i_5_ - 1; i_7_ > 0; i_7_--) {
			int i_8_ = i_3_;
			i_3_ /= i;
			int i_9_ = i_8_ - i_3_ * i;
			if (i_9_ >= 10)
				cs[i_7_] = (char) (i_9_ + 87);
			else
				cs[i_7_] = (char) (i_9_ + 48);
		}
		if (i_4_ <= 36)
			game4 = null;
		return new String(cs);
	}

	Class220(Packet class296_sub17) {
		int i = class296_sub17.readSmart();
		anIntArray2147 = new int[i];
		anIntArray2149 = new int[i];
		anIntArray2145 = new int[i];
		anIntArray2144 = new int[i];
		anIntArray2143 = new int[i];
		anIntArray2142 = new int[i];
		anIntArray2146 = new int[i];
		for (int i_10_ = 0; i > i_10_; i_10_++) {
			anIntArray2147[i_10_] = class296_sub17.g2() - 5120;
			anIntArray2144[i_10_] = class296_sub17.g2() - 5120;
			anIntArray2146[i_10_] = class296_sub17.g2b();
			anIntArray2143[i_10_] = class296_sub17.g2() - 5120;
			anIntArray2149[i_10_] = class296_sub17.g2() - 5120;
			anIntArray2145[i_10_] = class296_sub17.g2b();
			anIntArray2142[i_10_] = class296_sub17.g2b();
		}
	}

	static final void method2070(long l, int i) {
		int i_11_ = Class338_Sub3_Sub1.anInt6563;
		if (i >= 51) {
			if (Class296_Sub14.anInt4668 != i_11_) {
				int i_12_ = -Class296_Sub14.anInt4668 + i_11_;
				int i_13_ = (int) ((long) i_12_ * l / 320L);
				if (i_12_ > 0) {
					if (i_13_ == 0)
						i_13_ = 1;
					else if (i_12_ < i_13_)
						i_13_ = i_12_;
				} else if (i_13_ != 0) {
					if (i_12_ > i_13_)
						i_13_ = i_12_;
				} else
					i_13_ = -1;
				Class296_Sub14.anInt4668 += i_13_;
			}
			int i_14_ = Class127_Sub1.anInt4287;
			Class41_Sub26.aFloat3806 += Class296_Sub36.aFloat4865 * (float) l / 40.0F * 8.0F;
			Class153.aFloat1572 += (float) l * Class304.aFloat2733 / 40.0F * 8.0F;
			if (i_14_ != Class296_Sub24.anInt4762) {
				int i_15_ = i_14_ - Class296_Sub24.anInt4762;
				int i_16_ = (int) ((long) i_15_ * l / 320L);
				if (i_15_ > 0) {
					if (i_16_ != 0) {
						if (i_15_ < i_16_)
							i_16_ = i_15_;
					} else
						i_16_ = 1;
				} else if (i_16_ != 0) {
					if (i_15_ > i_16_)
						i_16_ = i_15_;
				} else
					i_16_ = -1;
				Class296_Sub24.anInt4762 += i_16_;
			}
			Class123_Sub2_Sub1.method1074(-4);
		}
	}

	final void method2071(int i, byte i_17_) {
		if (i_17_ != -25)
			anIntArray2143 = null;
		int[][] is = new int[anIntArray2147.length << 1][4];
		for (int i_18_ = 0; anIntArray2147.length > i_18_; i_18_++) {
			is[i_18_ * 2][0] = anIntArray2147[i_18_];
			is[i_18_ * 2][1] = anIntArray2146[i_18_];
			is[i_18_ * 2][2] = anIntArray2144[i_18_];
			is[i_18_ * 2][3] = anIntArray2142[i_18_];
			is[i_18_ * 2 + 1][0] = anIntArray2143[i_18_];
			is[i_18_ * 2 + 1][1] = anIntArray2145[i_18_];
			is[i_18_ * 2 + 1][2] = anIntArray2149[i_18_];
			is[i_18_ * 2 + 1][3] = anIntArray2142[i_18_];
		}
		Class123_Sub1.anIntArrayArrayArray3909[i] = is;
	}

	static final int method2072(int i, boolean bool, int i_19_) {
		if (bool)
			return 27;
		double d = Math.log((double) i) / Math.log(2.0);
		double d_20_ = Math.log((double) i_19_) / Math.log(2.0);
		double d_21_ = (-d_20_ + d) * Math.random() + d_20_;
		return (int) (Math.pow(2.0, d_21_) + 0.5);
	}

	public static void method2073(int i) {
		aClass151_2151 = null;
		int i_22_ = -54 % ((i - 38) / 51);
		game4 = null;
	}

	static final void method2074(int i, boolean bool) {
		if (bool != true)
			anInt2150 = 98;
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i, 11);
		class296_sub39_sub5.insertIntoQueue();
	}
}
