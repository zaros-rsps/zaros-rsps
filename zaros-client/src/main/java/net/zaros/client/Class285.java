package net.zaros.client;

/* Class285 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;

final class Class285 {
	static IncomingPacket aClass231_2622 = new IncomingPacket(49, 3);
	private Queue aClass145_2623 = new Queue();
	static int[] anIntArray2624;
	static int anInt2625 = 0;
	private Queue aClass145_2626 = new Queue();
	private Queue aClass145_2627 = new Queue();
	private Queue aClass145_2628 = new Queue();
	private int anInt2629;
	private long aLong2630;
	private Class36 aClass36_2631;
	private Packet message_packet = new Packet(6);
	volatile int anInt2633;
	private byte aByte2634 = 0;
	volatile int anInt2635;
	private Packet aClass296_Sub17_2636;
	private Class296_Sub39_Sub20_Sub1 aClass296_Sub39_Sub20_Sub1_2637;

	final void method2366(byte i) {
		if (i <= 65) {
			method2372(120);
		}
		if (aClass36_2631 != null) {
			aClass36_2631.method361(false);
		}
	}

	final void method2367(int i) {
		if (aClass36_2631 != null) {
			try {
				message_packet.pos = 0;
				message_packet.p1(7);
				message_packet.write24BitInt(0);
				message_packet.p2(0);
				aClass36_2631.method358(0, message_packet.data, message_packet.data.length, (byte) 123);
			} catch (IOException ioexception) {
				try {
					aClass36_2631.method361(false);
				} catch (Exception exception) {
					/* empty */
				}
				anInt2635 = -2;
				anInt2633++;
				aClass36_2631 = null;
			}
		}
	}

	static final void method2368(int i) {
		if (Class127.aBoolean1304) {
			InterfaceComponent class51 = Class103.method894(i, Class366_Sub4.anInt5375, Class180.anInt1857);
			if (class51 != null && class51.anObjectArray557 != null) {
				CS2Call class296_sub46 = new CS2Call();
				class296_sub46.callerInterface = class51;
				class296_sub46.callArgs = class51.anObjectArray557;
				CS2Executor.runCS2(class296_sub46);
			}
			Class127.aBoolean1304 = false;
			Class41_Sub19.anInt3793 = -1;
			Class69.anInt3689 = -1;
			if (class51 != null) {
				Class332.method3416(class51, (byte) 116);
			}
		}
	}

	static final Class107_Sub1 method2369(int i, Packet class296_sub17) {
		if (i > -123) {
			method2369(125, null);
		}
		Class107 class107 = Class338_Sub3_Sub3_Sub1.method3559(class296_sub17, (byte) -80);
		int i_0_ = class296_sub17.method2566();
		return new Class107_Sub1(class107.anInt3576, class107.aClass252_3575, class107.aClass357_3571, class107.anInt3570, class107.anInt3577, i_0_);
	}

	final boolean method2370(int i) {
		if (aClass36_2631 != null) {
			long l = Class72.method771(-118);
			int i_1_ = (int) (l - aLong2630);
			aLong2630 = l;
			if (i_1_ > 200) {
				i_1_ = 200;
			}
			anInt2629 += i_1_;
			if (anInt2629 > 30000) {
				try {
					aClass36_2631.method361(false);
				} catch (Exception exception) {
					/* empty */
				}
				aClass36_2631 = null;
			}
		}
		if (aClass36_2631 == null) {
			if (method2375((byte) -109) == 0 && method2374((byte) 116) == 0) {
				return true;
			}
			return false;
		}
		try {
			aClass36_2631.method356((byte) 87);
			for (Class296_Sub39_Sub20_Sub1 class296_sub39_sub20_sub1 = (Class296_Sub39_Sub20_Sub1) aClass145_2623.getFront(); class296_sub39_sub20_sub1 != null; class296_sub39_sub20_sub1 = (Class296_Sub39_Sub20_Sub1) aClass145_2623.getNext()) {
				message_packet.pos = 0;
				message_packet.p1(1);
				message_packet.p5(class296_sub39_sub20_sub1.queue_uid);
				aClass36_2631.method358(0, message_packet.data, message_packet.data.length, (byte) 125);
				aClass145_2626.insert(class296_sub39_sub20_sub1, i ^ ~0x7d19);
			}
			if (i != 32024) {
				method2375((byte) -86);
			}
			for (Class296_Sub39_Sub20_Sub1 class296_sub39_sub20_sub1 = (Class296_Sub39_Sub20_Sub1) aClass145_2627.getFront(); class296_sub39_sub20_sub1 != null; class296_sub39_sub20_sub1 = (Class296_Sub39_Sub20_Sub1) aClass145_2627.getNext()) {
				message_packet.pos = 0;
				message_packet.p1(0);
				message_packet.p5(class296_sub39_sub20_sub1.queue_uid);
				aClass36_2631.method358(0, message_packet.data, message_packet.data.length, (byte) 122);
				aClass145_2628.insert(class296_sub39_sub20_sub1, -2);
			}
			for (int i_2_ = 0; i_2_ < 100; i_2_++) {
				int i_3_ = aClass36_2631.method354(false);
				if (i_3_ < 0) {
					throw new IOException();
				}
				if (i_3_ == 0) {
					break;
				}
				anInt2629 = 0;
				int current_offset = 0;
				if (aClass296_Sub39_Sub20_Sub1_2637 == null) {
					current_offset = 10;
				} else if (aClass296_Sub39_Sub20_Sub1_2637.anInt6722 == 0) {
					current_offset = 1;
				}
				if (current_offset <= 0) {
					int i_5_ = -aClass296_Sub39_Sub20_Sub1_2637.aByte6724 + aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.data.length;
					int i_6_ = -aClass296_Sub39_Sub20_Sub1_2637.anInt6722 + 512;
					if (i_6_ > -aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.pos + i_5_) {
						i_6_ = i_5_ - aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.pos;
					}
					if (i_3_ < i_6_) {
						i_6_ = i_3_;
					}
					aClass36_2631.method355(1163625227, aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.pos, i_6_, aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.data);
					if (aByte2634 != 0) {
						for (int i_7_ = 0; i_6_ > i_7_; i_7_++) {
							aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.data[i_7_ + aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.pos] = (byte) Class294.method2420(aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.data[i_7_ + aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.pos], aByte2634);
						}
					}
					aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.pos += i_6_;
					aClass296_Sub39_Sub20_Sub1_2637.anInt6722 += i_6_;
					if (aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.pos == i_5_) {
						aClass296_Sub39_Sub20_Sub1_2637.queue_unlink();
						aClass296_Sub39_Sub20_Sub1_2637.aBoolean6256 = false;
						aClass296_Sub39_Sub20_Sub1_2637 = null;
					} else if (aClass296_Sub39_Sub20_Sub1_2637.anInt6722 == 512) {
						aClass296_Sub39_Sub20_Sub1_2637.anInt6722 = 0;
					}
				} else {
					int i_8_ = current_offset - aClass296_Sub17_2636.pos;
					if (i_8_ > i_3_) {
						i_8_ = i_3_;
					}
					aClass36_2631.method355(1163625227, aClass296_Sub17_2636.pos, i_8_, aClass296_Sub17_2636.data);
					if (aByte2634 != 0) {
						for (int i_9_ = 0; i_8_ > i_9_; i_9_++) {
							aClass296_Sub17_2636.data[i_9_ + aClass296_Sub17_2636.pos] = (byte) Class294.method2420(aClass296_Sub17_2636.data[i_9_ + aClass296_Sub17_2636.pos], aByte2634);
						}
					}
					aClass296_Sub17_2636.pos += i_8_;
					if (current_offset <= aClass296_Sub17_2636.pos) {
						if (aClass296_Sub39_Sub20_Sub1_2637 == null) {
							aClass296_Sub17_2636.pos = 0;
							int i_10_ = aClass296_Sub17_2636.g1();
							int i_11_ = aClass296_Sub17_2636.g4();
							int i_12_ = aClass296_Sub17_2636.g1();
							int i_13_ = aClass296_Sub17_2636.g4();
							int i_14_ = i_12_ & 0x7f;
							boolean bool = (i_12_ & 0x80) != 0;
							long l = (long) i_10_ << 32 | i_11_;
							Class296_Sub39_Sub20_Sub1 class296_sub39_sub20_sub1;
							if (bool) {
								for (class296_sub39_sub20_sub1 = (Class296_Sub39_Sub20_Sub1) aClass145_2628.getFront(); class296_sub39_sub20_sub1 != null; class296_sub39_sub20_sub1 = (Class296_Sub39_Sub20_Sub1) aClass145_2628.getNext()) {
									if (l == class296_sub39_sub20_sub1.queue_uid) {
										break;
									}
								}
							} else {
								for (class296_sub39_sub20_sub1 = (Class296_Sub39_Sub20_Sub1) aClass145_2626.getFront(); class296_sub39_sub20_sub1 != null; class296_sub39_sub20_sub1 = (Class296_Sub39_Sub20_Sub1) aClass145_2626.getNext()) {
									if (l == class296_sub39_sub20_sub1.queue_uid) {
										break;
									}
								}
							}
							if (class296_sub39_sub20_sub1 == null) {
								throw new IOException();
							}
							int i_15_ = i_14_ != 0 ? 9 : 5;
							aClass296_Sub39_Sub20_Sub1_2637 = class296_sub39_sub20_sub1;
							aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726 = new Packet(i_15_ + i_13_ + aClass296_Sub39_Sub20_Sub1_2637.aByte6724);
							aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.p1(i_14_);
							aClass296_Sub39_Sub20_Sub1_2637.aClass296_Sub17_6726.p4(i_13_);
							aClass296_Sub39_Sub20_Sub1_2637.anInt6722 = 10;
							aClass296_Sub17_2636.pos = 0;
						} else if (aClass296_Sub39_Sub20_Sub1_2637.anInt6722 == 0) {
							if (aClass296_Sub17_2636.data[0] != -1) {
								aClass296_Sub39_Sub20_Sub1_2637 = null;
							} else {
								aClass296_Sub39_Sub20_Sub1_2637.anInt6722 = 1;
								aClass296_Sub17_2636.pos = 0;
							}
						} else {
							throw new IOException();
						}
					}
				}
			}
			return true;
		} catch (IOException ioexception) {
			try {
				aClass36_2631.method361(false);
			} catch (Exception exception) {
				/* empty */
			}
			anInt2635 = -2;
			anInt2633++;
			aClass36_2631 = null;
			if (method2375((byte) -109) == 0 && method2374((byte) 123) == 0) {
				return true;
			}
			return false;
		}
	}

	final boolean method2371(int i) {
		if (i != 23192) {
			return true;
		}
		if (method2375((byte) -109) < 20) {
			return false;
		}
		return true;
	}

	private final void method2372(int i) {
		if (aClass36_2631 != null) {
			try {
				message_packet.pos = i;
				message_packet.p1(6);
				message_packet.write24BitInt(3);
				message_packet.p2(0);
				aClass36_2631.method358(0, message_packet.data, message_packet.data.length, (byte) 122);
			} catch (IOException ioexception) {
				try {
					aClass36_2631.method361(false);
				} catch (Exception exception) {
					/* empty */
				}
				aClass36_2631 = null;
				anInt2633++;
				anInt2635 = -2;
			}
		}
	}

	final Class296_Sub39_Sub20_Sub1 method2373(int i, byte i_16_, int i_17_, boolean bool, boolean bool_18_) {
		long l = (long) i_17_ << 32 | i;
		Class296_Sub39_Sub20_Sub1 class296_sub39_sub20_sub1 = new Class296_Sub39_Sub20_Sub1();
		class296_sub39_sub20_sub1.aByte6724 = i_16_;
		class296_sub39_sub20_sub1.queue_uid = l;
		class296_sub39_sub20_sub1.aBoolean6253 = bool_18_;
		if (!bool_18_) {
			if (method2374((byte) 106) >= 20) {
				throw new RuntimeException();
			}
			aClass145_2627.insert(class296_sub39_sub20_sub1, -2);
		} else {
			if (method2375((byte) -109) >= 20) {
				throw new RuntimeException();
			}
			aClass145_2623.insert(class296_sub39_sub20_sub1, -2);
		}
		return class296_sub39_sub20_sub1;
	}

	private final int method2374(byte i) {
		if (i < 100) {
			method2376(-75);
		}
		return aClass145_2627.getSize() + aClass145_2628.getSize();
	}

	final int method2375(byte i) {
		if (i != -109) {
			return -119;
		}
		return aClass145_2623.getSize() + aClass145_2626.getSize();
	}

	final void method2376(int i) {
		if (i != 49) {
			method2367(-57);
		}
		if (aClass36_2631 != null) {
			aClass36_2631.method359(112);
		}
	}

	public static void method2377(int i) {
		aClass231_2622 = null;
		int i_19_ = 54 % ((i + 23) / 53);
		anIntArray2624 = null;
	}

	final void method2378(int i, boolean bool) {
		if (aClass36_2631 != null) {
			try {
				message_packet.pos = 0;
				message_packet.p1(!bool ? 3 : 2);
				message_packet.write24BitInt(0);
				message_packet.p2(0);
				if (i > -30) {
					method2377(-65);
				}
				aClass36_2631.method358(0, message_packet.data, message_packet.data.length, (byte) 124);
			} catch (IOException ioexception) {
				try {
					aClass36_2631.method361(false);
				} catch (Exception exception) {
					/* empty */
				}
				anInt2633++;
				anInt2635 = -2;
				aClass36_2631 = null;
			}
		}
	}

	final void method2379(int i) {
		try {
			aClass36_2631.method361(false);
		} catch (Exception exception) {
			/* empty */
		}
		anInt2633++;
		anInt2635 = -1;
		aByte2634 = (byte) (int) (i + Math.random() * 255.0);
		aClass36_2631 = null;
	}

	final boolean method2380(int i) {
		if (i != -1) {
			return false;
		}
		if (method2374((byte) 120) < 20) {
			return false;
		}
		return true;
	}

	final void method2381(byte i, Class36 class36, boolean bool) {
		if (aClass36_2631 != null) {
			try {
				aClass36_2631.method361(false);
			} catch (Exception exception) {
				/* empty */
			}
			aClass36_2631 = null;
		}
		aClass36_2631 = class36;
		method2372(0);
		method2378(i ^ 0x2e, bool);
		aClass296_Sub39_Sub20_Sub1_2637 = null;
		aClass296_Sub17_2636.pos = 0;
		for (;;) {
			Class296_Sub39_Sub20_Sub1 class296_sub39_sub20_sub1 = (Class296_Sub39_Sub20_Sub1) aClass145_2626.remove();
			if (class296_sub39_sub20_sub1 == null) {
				break;
			}
			aClass145_2623.insert(class296_sub39_sub20_sub1, -2);
		}
		for (;;) {
			Class296_Sub39_Sub20_Sub1 class296_sub39_sub20_sub1 = (Class296_Sub39_Sub20_Sub1) aClass145_2628.remove();
			if (class296_sub39_sub20_sub1 == null) {
				break;
			}
			aClass145_2627.insert(class296_sub39_sub20_sub1, -2);
		}
		if (i != -128) {
			aClass231_2622 = null;
		}
		if (aByte2634 != 0) {
			try {
				message_packet.pos = 0;
				message_packet.p1(4);
				message_packet.p1(aByte2634);
				message_packet.p4(0);
				aClass36_2631.method358(0, message_packet.data, message_packet.data.length, (byte) 126);
			} catch (IOException ioexception) {
				try {
					aClass36_2631.method361(false);
				} catch (Exception exception) {
					/* empty */
				}
				aClass36_2631 = null;
				anInt2633++;
				anInt2635 = -2;
			}
		}
		anInt2629 = 0;
		aLong2630 = Class72.method771(-108);
	}

	public Class285() {
		anInt2633 = 0;
		anInt2635 = 0;
		aClass296_Sub17_2636 = new Packet(10);
	}
}
