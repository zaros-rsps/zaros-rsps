package net.zaros.client;
/* Class95 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class95 {
	int anInt1020;
	static int[] anIntArray1021 = new int[200];
	int anInt1022 = 0;
	private int anInt1023;
	int anInt1024;
	private boolean aBoolean1025;
	int anInt1026;
	int anInt1027;
	int anInt1028;
	long aLong1029;
	int anInt1030;
	static IncomingPacket aClass231_1031 = new IncomingPacket(22, 6);
	int anInt1032;
	int anInt1033;
	int anInt1034 = 0;
	int anInt1035;
	static int anInt1036;
	static int[] anIntArray1037 = new int[1];

	final void method862(boolean bool) {
		if (!bool) {
			anInt1020 = Class296_Sub4.anIntArray4618[anInt1023 << 3];
			long l = (long) anInt1028;
			long l_0_ = (long) anInt1026;
			long l_1_ = (long) anInt1032;
			anInt1033 = (int) Math.sqrt((double) (l_1_ * l_1_ + (l_0_ * l_0_ + l * l)));
			if (anInt1024 == 0)
				anInt1024 = 1;
			if (anInt1035 != 0) {
				if (anInt1035 != 1) {
					if (anInt1035 == 2)
						aLong1029 = (long) (anInt1033 * 8 / anInt1024);
				} else {
					aLong1029 = (long) (anInt1033 * 8 / anInt1024);
					aLong1029 *= aLong1029;
				}
			} else
				aLong1029 = 2147483647L;
			if (aBoolean1025)
				anInt1033 *= -1;
		}
	}

	final void method863(int i, Packet class296_sub17) {
		if (i != 0)
			anInt1034 = 69;
		for (;;) {
			int i_2_ = class296_sub17.g1();
			if (i_2_ == 0)
				break;
			method865(i_2_, i ^ 0x200, class296_sub17);
		}
	}

	public static void method864(int i) {
		anIntArray1037 = null;
		anIntArray1021 = null;
		aClass231_1031 = null;
		if (i >= -46)
			aClass231_1031 = null;
	}

	private final void method865(int i, int i_3_, Packet class296_sub17) {
		if (i == 1)
			anInt1023 = class296_sub17.g2();
		else if (i == 2)
			class296_sub17.g1();
		else if (i != 3) {
			if (i == 4) {
				anInt1035 = class296_sub17.g1();
				anInt1024 = class296_sub17.g4();
			} else if (i != 6) {
				if (i != 8) {
					if (i != 9) {
						if (i == 10)
							aBoolean1025 = true;
					} else
						anInt1022 = 1;
				} else
					anInt1034 = 1;
			} else
				anInt1030 = class296_sub17.g1();
		} else {
			anInt1028 = class296_sub17.g4();
			anInt1026 = class296_sub17.g4();
			anInt1032 = class296_sub17.g4();
		}
		if (i_3_ != 512)
			anInt1033 = -108;
	}

	static final void method866(int i, int i_4_, int i_5_, boolean bool, int i_6_, int i_7_) {
		if (i < 1)
			i = 1;
		if (i_4_ < 1)
			i_4_ = 1;
		int i_8_ = i_4_ - 334;
		if (i_8_ >= 0) {
			if (i_8_ > 100)
				i_8_ = 100;
		} else
			i_8_ = 0;
		int i_9_ = (ModeWhere.aShort2350 + i_8_ * (-ModeWhere.aShort2350 + NodeDeque.aShort1589) / 100);
		if (i_9_ < Class296_Sub27.aShort4784)
			i_9_ = Class296_Sub27.aShort4784;
		else if (i_9_ > Class125.aShort1286)
			i_9_ = Class125.aShort1286;
		int i_10_ = i_9_ * i_4_ * 512 / (i * 334);
		if (BillboardRaw.aShort1074 <= i_10_) {
			if (i_10_ > Class296_Sub14.aShort4659) {
				i_10_ = Class296_Sub14.aShort4659;
				i_9_ = i * i_10_ * 334 / (i_4_ * 512);
				if (Class296_Sub27.aShort4784 > i_9_) {
					i_9_ = Class296_Sub27.aShort4784;
					int i_11_ = i * i_10_ * 334 / (i_9_ * 512);
					int i_12_ = (-i_11_ + i_4_) / 2;
					if (bool) {
						Class41_Sub13.aHa3774.la();
						Class41_Sub13.aHa3774.method1088(i_6_, i, -16777216, i_5_ - 28884, i_7_, i_12_);
						Class41_Sub13.aHa3774.method1088(i_6_, i, -16777216, i_5_ - 28884, -i_12_ + i_7_ + i_4_, i_12_);
					}
					i_4_ -= i_12_ * 2;
					i_7_ += i_12_;
				}
			}
		} else {
			i_10_ = BillboardRaw.aShort1074;
			i_9_ = i_10_ * (i * 334) / (i_4_ * 512);
			if (i_9_ > Class125.aShort1286) {
				i_9_ = Class125.aShort1286;
				int i_13_ = i_4_ * i_9_ * 512 / (i_10_ * 334);
				int i_14_ = (-i_13_ + i) / 2;
				if (bool) {
					Class41_Sub13.aHa3774.la();
					Class41_Sub13.aHa3774.method1088(i_6_, i_14_, -16777216, 1, i_7_, i_4_);
					Class41_Sub13.aHa3774.method1088(i + i_6_ - i_14_, i_14_, -16777216, 1, i_7_, i_4_);
				}
				i -= i_14_ * 2;
				i_6_ += i_14_;
			}
		}
		Class157.anInt1598 = i_6_;
		NodeDeque.anInt1592 = i_7_;
		if (i_5_ == 28885) {
			Class344.anInt3007 = (short) i;
			Class41_Sub17.anInt3783 = i_4_ * i_9_ / 334;
			Class296_Sub29.anInt4814 = (short) i_4_;
		}
	}

	public Class95() {
		aBoolean1025 = false;
	}
}
