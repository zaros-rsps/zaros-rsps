package net.zaros.client;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* Class70 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class ObjectDefinition {
	int anInt759;
	private int anInt760;
	int anInt761;
	short[] aShortArray762;
	int anInt763;
	int anInt764;
	int anInt765;
	int[] anIntArray766;
	ObjectDefinitionLoader loader;
	int anInt768 = 0;
	boolean aBoolean769;
	private int anInt770;
	static int[] anIntArray771;
	int anInt772;
	int anInt773 = 0;
	int[] transforms;
	boolean aBoolean775;
	int anInt776;
	boolean aBoolean777;
	private int anInt778;
	boolean aBoolean779;
	private int anInt780;
	boolean aBoolean781;
	private int anInt782 = 0;
	int[] anIntArray783;
	private int[] anIntArray784;
	boolean membersOnly;
	private int anInt786 = 128;
	private byte aByte787;
	int anInt788;
	boolean aBoolean789;
	private byte aByte790;
	private short[] originalColors;
	private int anInt792;
	boolean aBoolean793;
	private int anInt794;
	String[] options;
	private int[] anIntArray796;
	private HashTable aClass263_797;
	private byte aByte798;
	private int anInt799;
	private int anInt800;
	boolean projectileClipped;
	byte[] shapes;
	boolean aBoolean803;
	int anInt804;
	int interactonType;
	boolean aBoolean806;
	private int anInt807;
	int anInt808;
	static int anInt809 = 0;
	boolean aBoolean810;
	int anInt811;
	boolean aBoolean812;
	int anInt813;
	int[][] models;
	int anInt815;
	private byte[] aByteArray816;
	String name;
	int sizeY;
	short[] modifiedColors;
	int sizeX;
	private byte aByte821;
	int anInt822;
	int anInt823;
	int blockingType;
	int anInt825;
	private byte aByte826;
	int anInt827;
	int anInt828;
	boolean notClipped;
	private int anInt830;
	private short[] aShortArray831;
	int anInt832;
	int anInt833;
	private int anInt834;
	boolean aBoolean835;
	private int anInt836;
	int ID;
	boolean aBoolean838;

	final boolean method748(int i, int i_0_) {
		if (models == null) {
			return true;
		}
		synchronized (loader.aClass138_368) {
			for (int i_1_ = i_0_; i_1_ < shapes.length; i_1_++) {
				if (i == shapes[i_1_]) {
					for (int i_2_ = 0; i_2_ < models[i_1_].length; i_2_++) {
						if (!loader.aClass138_368.fileExists(models[i_1_][i_2_], 0)) {
							return false;
						}
					}
					return true;
				}
			}
		}
		return true;
	}

	final boolean method749(byte i) {
		if (models == null) {
			return true;
		}
		boolean bool = true;
		synchronized (loader.aClass138_368) {
			for (int i_4_ = 0; i_4_ < models.length; i_4_++) {
				for (int i_5_ = 0; i_5_ < models[i_4_].length; i_5_++) {
					boolean result = loader.aClass138_368.fileExists(models[i_4_][i_5_], 0);
					bool &= result;

				}
			}
		}
		return bool;
	}

	final Class188 method750(s var_s, s var_s_6_, ha var_ha, boolean bool, int i, int i_7_, int i_8_, int i_9_, Class375 class375, int i_10_, int i_11_, int i_12_) {
		if (Class296_Sub56.method3218(8800, i_12_)) {
			i_12_ = 4;
		}
		long l = (ID << 10) + (i_12_ << 3) + i_10_;
		l |= var_ha.anInt1295 << 29;
		if (class375 != null) {
			l |= class375.aLong3180 << 32;
		}
		int i_13_ = i_9_;
		if (aByte798 == 3) {
			i_13_ |= 0x7;
		} else {
			if (aByte798 != 0 || anInt830 != 0) {
				i_13_ |= 0x2;
			}
			if (anInt782 != 0) {
				i_13_ |= 0x1;
			}
			if (anInt778 != 0) {
				i_13_ |= 0x4;
			}
		}
		if (bool) {
			i_13_ |= 0x40000;
		}
		Class188 class188;
		synchronized (loader.aClass113_378) {
			class188 = (Class188) loader.aClass113_378.get(l);
			if (i != -954198435) {
				anIntArray784 = null;
			}
		}
		Model class178 = class188 == null ? null : class188.aClass178_1926;
		r var_r = null;
		if (class178 == null || var_ha.e(class178.ua(), i_13_) != 0) {
			if (class178 != null) {
				i_13_ = var_ha.d(i_13_, class178.ua());
			}
			int i_14_ = i_13_;
			if (i_12_ == 10 && i_10_ > 3) {
				i_14_ |= 0x5;
			}
			class178 = method762(var_ha, class375, i_12_, i ^ ~0x38dff9ad, i_14_, i_10_);
			if (class178 == null) {
				return null;
			}
			if (i_12_ == 10 && i_10_ > 3) {
				class178.a(2048);
			}
			if (bool) {
				var_r = class178.ba(null);
			}
			class178.s(i_13_);
			class188 = new Class188();
			class188.aClass178_1926 = class178;
			class188.aR1925 = var_r;
			synchronized (loader.aClass113_378) {
				loader.aClass113_378.put(class188, l);
			}
		} else {
			class178 = class188.aClass178_1926;
			var_r = class188.aR1925;
			if (bool && var_r == null) {
				var_r = class188.aR1925 = class178.ba(null);
			}
		}
		boolean bool_15_ = aByte798 != 0 && (var_s != null || var_s_6_ != null);
		boolean bool_16_ = anInt782 != 0 || anInt830 != 0 || anInt778 != 0;
		if (!bool_15_ && !bool_16_) {
			class178 = class178.method1728((byte) 0, i_9_, true);
		} else {
			class178 = class178.method1728((byte) 0, i_13_, true);
			if (bool_15_) {
				class178.p(aByte798, anInt780, var_s, var_s_6_, i_8_, i_11_, i_7_);
			}
			if (bool_16_) {
				class178.H(anInt782, anInt830, anInt778);
			}
			class178.s(i_9_);
		}
		Class306.aClass188_2743.aClass178_1926 = class178;
		Class306.aClass188_2743.aR1925 = var_r;
		return Class306.aClass188_2743;
	}

	final int method751(boolean bool) {
		if (bool != true) {
			return -118;
		}
		if (anIntArray796 != null) {
			if (anIntArray796.length > 1) {
				int i = (int) (Math.random() * 65535.0);
				for (int i_17_ = 0; anIntArray796.length > i_17_; i_17_++) {
					if (anIntArray784[i_17_] >= i) {
						return anIntArray796[i_17_];
					}
					i -= anIntArray784[i_17_];
				}
			} else {
				return anIntArray796[0];
			}
		}
		return -1;
	}

	final boolean method752(int i) {
		if (i > -113) {
			method763((byte) -115);
		}
		if (anIntArray796 == null) {
			return false;
		}
		return true;
	}

	private final void parseOpcode(Packet buffer, int opcode, boolean newObject) {
		if (opcode == 1 || opcode == 5) {
			if (opcode == 5 && loader.aBoolean369) {
				skip_models(buffer);
			}
			int num_shapes = buffer.g1();
			shapes = new byte[num_shapes];
			models = new int[num_shapes][];
			for (int shape_index = 0; num_shapes > shape_index; shape_index++) {
				shapes[shape_index] = buffer.g1b();
				int num_models = buffer.g1();
				models[shape_index] = new int[num_models];
				for (int model_index = 0; model_index < num_models; model_index++) {
					models[shape_index][model_index] = newObject ? buffer.gSmart2or4s() : buffer.g2();
				}
			}
			if (opcode == 5 && !loader.aBoolean369) {
				skip_models(buffer);
			}
		} else if (opcode == 2) {
			name = buffer.gstr();
		} else if (opcode == 14) {
			sizeX = buffer.g1();
		} else if (opcode == 15) {
			sizeY = buffer.g1();
		} else if (opcode == 17) {
			blockingType = 0;
			projectileClipped = false;
		} else if (opcode == 18) {
			projectileClipped = false;
		} else if (opcode == 19) {
			interactonType = buffer.g1();
		} else if (opcode == 21) {
			aByte798 = (byte) 1;
		} else if (opcode == 22) {
			aBoolean775 = true;
		} else if (opcode == 23) {
			anInt832 = 1;
		} else if (opcode == 24) {
			int seqid;
			if (newObject) {
				seqid = buffer.gSmart2or4s();
			} else {
				seqid = buffer.g2();
				if (seqid == 65535) {
					seqid = -1;
				}
			}
			if (seqid != -1) {
				anIntArray796 = new int[] { seqid };
			}
		} else if (opcode == 27) {
			blockingType = 1;
		} else if (opcode == 28) {
			anInt822 = buffer.g1() << 2;
		} else if (opcode == 29) {
			anInt807 = buffer.g1b();
		} else if (opcode == 39) {
			anInt799 = buffer.g1b() * 5;
		} else if (opcode >= 30 && opcode < 35) {
			options[opcode - 30] = buffer.gstr();
		} else if (opcode == 40) {
			int length = buffer.g1();
			modifiedColors = new short[length];
			originalColors = new short[length];
			for (int i = 0; length > i; i++) {
				originalColors[i] = (short) buffer.g2();
				modifiedColors[i] = (short) buffer.g2();
			}
		} else if (opcode == 41) {
			int i_20_ = buffer.g1();
			aShortArray831 = new short[i_20_];
			aShortArray762 = new short[i_20_];
			for (int i_21_ = 0; i_20_ > i_21_; i_21_++) {
				aShortArray831[i_21_] = (short) buffer.g2();
				aShortArray762[i_21_] = (short) buffer.g2();
			}
		} else if (opcode == 42) {
			int i_37_ = buffer.g1();
			aByteArray816 = new byte[i_37_];
			for (int i_38_ = 0; i_37_ > i_38_; i_38_++) {
				aByteArray816[i_38_] = buffer.g1b();
			}
		} else if (opcode == 44) {
			buffer.g2();
		} else if (opcode == 45) {
			buffer.g2();
		} else if (opcode == 62) {
			aBoolean812 = true;
		} else if (opcode == 64) {
			aBoolean769 = false;
		} else if (opcode == 65) {
			anInt792 = buffer.g2();
		} else if (opcode == 66) {
			anInt786 = buffer.g2();
		} else if (opcode == 67) {
			anInt836 = buffer.g2();
		} else if (opcode == 69) {
			anInt772 = buffer.g1();
		} else if (opcode == 70) {
			anInt794 = buffer.g2b() << 2;
		} else if (opcode == 71) {
			anInt760 = buffer.g2b() << 2;
		} else if (opcode == 72) {
			anInt770 = buffer.g2b() << 2;
		} else if (opcode == 73) {
			aBoolean789 = true;
		} else if (opcode == 74) {
			notClipped = true;
		} else if (opcode == 75) {
			anInt804 = buffer.g1();
		} else if (opcode == 77 || opcode == 92) {
			anInt800 = buffer.g2();
			if (anInt800 == 65535) {
				anInt800 = -1;
			}
			anInt834 = buffer.g2();
			if (anInt834 == 65535) {
				anInt834 = -1;
			}
			int id = -1;
			if (opcode == 92) {
				if (newObject) {
					id = buffer.gSmart2or4s();
				} else {
					id = buffer.g2();
					if (id == 65535) {
						id = -1;
					}
				}
			}
			int i_35_ = buffer.g1();
			transforms = new int[i_35_ + 2];
			for (int i_36_ = 0; i_36_ <= i_35_; i_36_++) {
				if (newObject) {
					transforms[i_36_] = buffer.gSmart2or4s();
				} else {
					transforms[i_36_] = buffer.g2();
					if (transforms[i_36_] == 65535) {
						transforms[i_36_] = -1;
					}
				}
			}
			transforms[i_35_ + 1] = id;
		} else if (opcode == 78) {
			anInt811 = buffer.g2();
			anInt761 = buffer.g1();
		} else if (opcode == 79) {
			anInt833 = buffer.g2();
			anInt768 = buffer.g2();
			anInt761 = buffer.g1();
			int i_32_ = buffer.g1();
			anIntArray766 = new int[i_32_];
			for (int i_33_ = 0; i_32_ > i_33_; i_33_++) {
				anIntArray766[i_33_] = buffer.g2();
			}
		} else if (opcode == 81) {
			aByte798 = (byte) 2;
			anInt780 = buffer.g1() * 256;
		} else if (opcode == 82) {
			aBoolean777 = true;
		} else if (opcode == 88) {
			aBoolean779 = false;
		} else if (opcode == 89) {
			aBoolean838 = false;
		} else if (opcode == 90) {
			// from 614
		} else if (opcode == 91) {
			membersOnly = true;
		} else if (opcode == 93) {
			aByte798 = (byte) 3;
			anInt780 = buffer.g2();
		} else if (opcode == 94) {
			aByte798 = (byte) 4;
		} else if (opcode == 95) {
			aByte798 = (byte) 5;
			anInt780 = buffer.g2b();
		} else if (opcode == 97) {
			aBoolean793 = true;
		} else if (opcode == 98) {
			aBoolean803 = true;
		} else if (opcode == 99) {
			anInt788 = buffer.g1();
			anInt827 = buffer.g2();
		} else if (opcode == 100) {
			anInt764 = buffer.g1();
			anInt828 = buffer.g2();
		} else if (opcode == 101) {
			anInt763 = buffer.g1();
		} else if (opcode == 102) {
			anInt765 = buffer.g2();
		} else if (opcode == 103) {
			anInt832 = 0;
		} else if (opcode == 104) {
			anInt815 = buffer.g1();
		} else if (opcode == 105) {
			aBoolean806 = true;
		} else if (opcode == 106) {
			int i_22_ = buffer.g1();
			int i_23_ = 0;
			anIntArray784 = new int[i_22_];
			anIntArray796 = new int[i_22_];
			for (int i_24_ = 0; i_22_ > i_24_; i_24_++) {
				anIntArray796[i_24_] = buffer.g2();
				if (anIntArray796[i_24_] == 65535) {
					anIntArray796[i_24_] = -1;
				}
				i_23_ += anIntArray784[i_24_] = buffer.g1();
			}
			for (int i_25_ = 0; i_22_ > i_25_; i_25_++) {
				anIntArray784[i_25_] = anIntArray784[i_25_] * 65535 / i_23_;
			}
		} else if (opcode == 107) {
			anInt759 = buffer.g2();
		} else if (opcode >= 150 && opcode < 155) {
			options[opcode - 150] = buffer.gstr();
			if (!loader.membersOnly) {
				options[opcode - 150] = null;
			}
		} else if (opcode == 160) {
			int i_26_ = buffer.g1();
			anIntArray783 = new int[i_26_];
			for (int i_27_ = 0; i_27_ < i_26_; i_27_++) {
				anIntArray783[i_27_] = buffer.g2();
			}
		} else if (opcode == 162) {
			aByte798 = (byte) 3;
			anInt780 = buffer.g4();
		} else if (opcode == 163) {
			aByte826 = buffer.g1b();
			aByte790 = buffer.g1b();
			aByte821 = buffer.g1b();
			aByte787 = buffer.g1b();
		} else if (opcode == 164) {
			anInt782 = buffer.g2b();
		} else if (opcode == 165) {
			anInt830 = buffer.g2b();
		} else if (opcode == 166) {
			anInt778 = buffer.g2b();
		} else if (opcode == 167) {
			anInt776 = buffer.g2();
		} else if (opcode == 168) {
			aBoolean810 = true;
		} else if (opcode == 169) {
			aBoolean781 = true;
		} else if (opcode == 170) {
			anInt823 = buffer.readSmart();
		} else if (opcode == 171) {
			anInt773 = buffer.readSmart();
		} else if (opcode == 173) {
			anInt825 = buffer.g2();
			anInt808 = buffer.g2();
		} else if (opcode == 177) {
			aBoolean835 = true;
		} else if (opcode == 178) {
			anInt813 = buffer.g1();
		} else if (opcode >= 190 && opcode < 196) {
			buffer.g2();
			// TODO: mouse icons, delegate
		} else if (opcode == 249) {
			int i_28_ = buffer.g1();
			if (aClass263_797 == null) {
				int i_29_ = Class8.get_next_high_pow2(i_28_);
				aClass263_797 = new HashTable(i_29_);
			}
			for (int i_30_ = 0; i_28_ > i_30_; i_30_++) {
				boolean bool = buffer.g1() == 1;
				int i_31_ = buffer.readUnsignedMedInt();
				Node class296;
				if (!bool) {
					class296 = new IntegerNode(buffer.g4());
				} else {
					class296 = new StringNode(buffer.gstr());
				}
				aClass263_797.put(i_31_, class296);
			}
		} else {
			throw new RuntimeException("Misisng opcode: " + Arrays.toString(opcodes_history.toArray()));
		}
	}

	private final void skip_models(Packet class296_sub17) {
		int i_45_ = class296_sub17.g1();
		for (int i_46_ = 0; i_45_ > i_46_; i_46_++) {
			class296_sub17.pos++;
			int i_47_ = class296_sub17.g1();
			class296_sub17.pos += i_47_ * 2;
		}
	}

	final int method755(int i, byte i_48_, int i_49_) {
		if (i_48_ != -82) {
			aByte826 = (byte) 23;
		}
		if (aClass263_797 == null) {
			return i_49_;
		}
		IntegerNode class296_sub16 = (IntegerNode) aClass263_797.get(i);
		if (class296_sub16 == null) {
			return i_49_;
		}
		return class296_sub16.value;
	}

	final boolean method756(byte i, int i_50_) {
		if (i != -70) {
			return false;
		}
		if (anIntArray796 != null && i_50_ != -1) {
			for (int i_51_ = 0; anIntArray796.length > i_51_; i_51_++) {
				if (anIntArray796[i_51_] == i_50_) {
					return true;
				}
			}
		}
		return false;
	}

	final ObjectDefinition method757(IConfigsRegister interface17, boolean bool) {
		if (bool) {
			modifiedColors = null;
		}
		int i = -1;
		if (anInt800 != -1) {
			i = interface17.getBITConfig(anInt800, (byte) -108);
		} else if (anInt834 != -1) {
			i = interface17.getConfig(true, anInt834);
		}
		if (i < 0 || transforms.length - 1 <= i || transforms[i] == -1) {
			int i_52_ = transforms[transforms.length - 1];
			if (i_52_ != -1) {
				return loader.getObjectDefinition(i_52_);
			}
			return null;
		}
		return loader.getObjectDefinition(transforms[i]);
	}

	private static final List<Integer> opcodes_history = new ArrayList<Integer>();

	public void init(Packet buff, boolean newObject) {
		if (newObject) {
			buff.pos += 3;
		}
		opcodes_history.clear();
		for (;;) {
			int opcode = buff.g1();
			if (opcode == 0) {
				break;
			}
			opcodes_history.add(opcode);
			parseOpcode(buff, opcode, newObject);
		}
	}

	public static void method759(byte i) {
		int i_53_ = -31 / ((i - 63) / 57);
		anIntArray771 = null;
	}

	final boolean method760(byte i) {
		int i_54_ = -56 % ((5 - i) / 42);
		if (anIntArray796 == null || anIntArray796.length <= 1) {
			return false;
		}
		return true;
	}

	static final boolean method761(byte i, int i_55_) {
		if (i != -10) {
			method761((byte) -60, 59);
		}
		if (i_55_ != 0 && i_55_ != 1 && i_55_ != 2) {
			return false;
		}
		return true;
	}

	private final Model method762(ha var_ha, Class375 class375, int i, int i_56_, int i_57_, int i_58_) {
		int i_59_ = anInt807 + 64;
		int i_60_ = anInt799 + 850;
		int i_61_ = i_57_;
		boolean bool = aBoolean812 || i == 2 && i_58_ > 3;
		if (bool) {
			i_57_ |= 0x10;
		}
		if (i_58_ != 0) {
			i_57_ |= 0xd;
		} else {
			if (anInt792 != 128 || anInt794 != 0) {
				i_57_ |= 0x1;
			}
			if (anInt836 != 128 || anInt770 != 0) {
				i_57_ |= 0x4;
			}
		}
		if (anInt786 != 128 || anInt760 != 0) {
			i_57_ |= 0x2;
		}
		if (originalColors != null) {
			i_57_ |= 0x4000;
		}
		if (aShortArray831 != null) {
			i_57_ |= 0x8000;
		}
		if (aByte787 != 0) {
			i_57_ |= 0x80000;
		}
		if (i_56_ != 4111) {
			return null;
		}
		Model class178 = null;
		if (shapes == null) {
			return null;
		}
		int i_62_ = -1;
		for (int i_63_ = 0; i_63_ < shapes.length; i_63_++) {
			if (i == shapes[i_63_]) {
				i_62_ = i_63_;
				break;
			}
		}
		if (i_62_ == -1) {
			return null;
		}
		int[] is = class375 != null && class375.anIntArray3184 != null ? class375.anIntArray3184 : models[i_62_];
		int i_64_ = is.length;
		if (i_64_ > 0) {
			long l = var_ha.anInt1295;
			for (int i_65_ = 0; i_65_ < i_64_; i_65_++) {
				l = is[i_65_] + l * 67783L;
			}
			synchronized (loader.aClass113_377) {
				class178 = (Model) loader.aClass113_377.get(l);
			}
			if (class178 != null) {
				if (class178.WA() != i_59_) {
					i_57_ |= 0x1000;
				}
				if (i_60_ != class178.da()) {
					i_57_ |= 0x2000;
				}
			}
			if (class178 == null || var_ha.e(class178.ua(), i_57_) != 0) {
				int i_66_ = i_57_ | 0x1f01f;
				if (class178 != null) {
					i_66_ = var_ha.d(i_66_, class178.ua());
				}
				Mesh class132 = null;
				synchronized (Class296_Sub51_Sub8.aClass132Array6385) {
					for (int i_67_ = 0; i_64_ > i_67_; i_67_++) {
						synchronized (loader.aClass138_368) {
							class132 = Class296_Sub51_Sub1.fromJs5(loader.aClass138_368, is[i_67_], 0);
						}
						if (class132 == null) {
							return null;
						}
						if (class132.version_number < 13) {
							class132.scale(2);
						}
						if (i_64_ > 1) {
							Class296_Sub51_Sub8.aClass132Array6385[i_67_] = class132;
						}
					}
					if (i_64_ > 1) {
						class132 = new Mesh(Class296_Sub51_Sub8.aClass132Array6385, i_64_);
					}
				}
				class178 = var_ha.a(class132, i_66_, loader.anInt382, i_59_, i_60_);
				synchronized (loader.aClass113_377) {
					loader.aClass113_377.put(class178, l);
				}
			}
		}
		if (class178 == null) {
			return null;
		}
		Model class178_68_ = class178.method1728((byte) 0, i_57_, true);
		if (i_59_ != class178.WA()) {
			class178_68_.C(i_59_);
		}
		if (class178.da() != i_60_) {
			class178_68_.LA(i_60_);
		}
		if (bool) {
			class178_68_.v();
		}
		if (i == 4 && i_58_ > 3) {
			class178_68_.k(2048);
			class178_68_.H(180, 0, -180);
		}
		i_58_ &= 0x3;
		if (i_58_ == 1) {
			class178_68_.k(4096);
		} else if (i_58_ == 2) {
			class178_68_.k(8192);
		} else if (i_58_ == 3) {
			class178_68_.k(12288);
		}
		if (originalColors != null) {
			short[] is_69_;
			if (class375 == null || class375.aShortArray3179 == null) {
				is_69_ = modifiedColors;
			} else {
				is_69_ = class375.aShortArray3179;
			}
			for (int i_70_ = 0; originalColors.length > i_70_; i_70_++) {
				if (aByteArray816 == null || aByteArray816.length <= i_70_) {
					class178_68_.ia(originalColors[i_70_], is_69_[i_70_]);
				} else {
					class178_68_.ia(originalColors[i_70_], Class366_Sub5.aShortArray5381[aByteArray816[i_70_] & 0xff]);
				}
			}
		}
		if (aShortArray831 != null) {
			short[] is_71_;
			if (class375 != null && class375.aShortArray3183 != null) {
				is_71_ = class375.aShortArray3183;
			} else {
				is_71_ = aShortArray762;
			}
			for (int i_72_ = 0; aShortArray831.length > i_72_; i_72_++) {
				class178_68_.aa(aShortArray831[i_72_], is_71_[i_72_]);
			}
		}
		if (aByte787 != 0) {
			class178_68_.method1717(aByte826, aByte790, aByte821, aByte787 & 0xff);
		}
		if (anInt792 != 128 || anInt786 != 128 || anInt836 != 128) {
			class178_68_.O(anInt792, anInt786, anInt836);
		}
		if (anInt794 != 0 || anInt760 != 0 || anInt770 != 0) {
			class178_68_.H(anInt794, anInt760, anInt770);
		}
		class178_68_.s(i_61_);
		return class178_68_;
	}

	final void method763(byte i) {
		if (interactonType == -1) {
			interactonType = 0;
			if (shapes != null && shapes.length == 1 && shapes[0] == 10) {
				interactonType = 1;
			}
			for (int i_73_ = 0; i_73_ < 5; i_73_++) {
				if (options[i_73_] != null) {
					interactonType = 1;
					break;
				}
			}
		}
		if (anInt804 == -1) {
			anInt804 = blockingType == 0 ? 0 : 1;
		}
		if (method752(-115) || aBoolean803 || transforms != null) {
			aBoolean835 = true;
		}
	}

	final String method764(int i, int i_74_, String string) {
		if (aClass263_797 == null) {
			return string;
		}
		if (i != 64) {
			method749((byte) 77);
		}
		StringNode class296_sub5 = (StringNode) aClass263_797.get(i_74_);
		if (class296_sub5 == null) {
			return string;
		}
		return class296_sub5.value;
	}

	final Model method765(int i, Animator class44, s var_s, int i_75_, byte i_76_, int i_77_, ha var_ha, int i_78_, s var_s_79_, int i_80_, Class375 class375, int i_81_) {
		if (Class296_Sub56.method3218(8800, i_77_)) {
			i_77_ = 4;
		}
		long l = i_80_ + (i_77_ << 3) + (ID << 10);
		l |= var_ha.anInt1295 << 29;
		int i_82_ = i_75_;
		if (class375 != null) {
			l |= class375.aLong3180 << 32;
		}
		if (class44 != null) {
			i_75_ |= class44.method568(0);
		}
		if (aByte798 == 3) {
			i_75_ |= 0x7;
		} else {
			if (aByte798 != 0 || anInt830 != 0) {
				i_75_ |= 0x2;
			}
			if (anInt782 != 0) {
				i_75_ |= 0x1;
			}
			if (anInt778 != 0) {
				i_75_ |= 0x4;
			}
		}
		if (i_77_ == 10 && i_80_ > 3) {
			i_75_ |= 0x5;
		}
		Model class178;
		synchronized (loader.aClass113_381) {
			class178 = (Model) loader.aClass113_381.get(l);
		}
		int i_83_ = -5 % ((i_76_ - 38) / 34);
		if (class178 == null || var_ha.e(class178.ua(), i_75_) != 0) {
			if (class178 != null) {
				i_75_ = var_ha.d(i_75_, class178.ua());
			}
			class178 = method762(var_ha, class375, i_77_, 4111, i_75_, i_80_);
			if (class178 == null) {
				return null;
			}
			synchronized (loader.aClass113_381) {
				loader.aClass113_381.put(class178, l);
			}
		}
		boolean bool = false;
		if (class44 != null) {
			class178 = class178.method1728((byte) 1, i_75_, true);
			bool = true;
			class44.method556(class178, i_80_ & 0x3, (byte) -63);
		}
		if (i_77_ == 10 && i_80_ > 3) {
			if (!bool) {
				bool = true;
				class178 = class178.method1728((byte) 3, i_75_, true);
			}
			class178.a(2048);
		}
		if (aByte798 != 0) {
			if (!bool) {
				class178 = class178.method1728((byte) 3, i_75_, true);
				bool = true;
			}
			class178.p(aByte798, anInt780, var_s_79_, var_s, i_78_, i_81_, i);
		}
		if (anInt782 != 0 || anInt830 != 0 || anInt778 != 0) {
			if (!bool) {
				class178 = class178.method1728((byte) 3, i_75_, true);
				bool = true;
			}
			class178.H(anInt782, anInt830, anInt778);
		}
		if (bool) {
			class178.s(i_82_);
		}
		return class178;
	}

	final boolean method766(byte i) {
		if (transforms == null) {
			if (anInt811 == -1 && anIntArray766 == null) {
				return false;
			}
			return true;
		}
		if (i != -94) {
			method764(103, 41, null);
		}
		for (int i_84_ = 0; i_84_ < transforms.length; i_84_++) {
			if (transforms[i_84_] != -1) {
				ObjectDefinition class70_85_ = loader.getObjectDefinition(transforms[i_84_]);
				if (class70_85_.anInt811 != -1 || class70_85_.anIntArray766 != null) {
					return true;
				}
			}
		}
		return false;
	}

	public ObjectDefinition() {
		aBoolean781 = false;
		anInt763 = 0;
		aBoolean775 = false;
		anInt772 = 0;
		anInt764 = -1;
		interactonType = -1;
		projectileClipped = true;
		anInt776 = 0;
		anInt794 = 0;
		anInt811 = -1;
		membersOnly = false;
		anInt799 = 0;
		aBoolean789 = false;
		anInt761 = 0;
		anInt788 = -1;
		aBoolean810 = false;
		anInt759 = -1;
		anInt792 = 128;
		aBoolean806 = false;
		anInt813 = 0;
		anInt770 = 0;
		anInt780 = -1;
		anInt808 = 256;
		anIntArray784 = null;
		anInt778 = 0;
		aByte798 = (byte) 0;
		aBoolean793 = false;
		anInt760 = 0;
		anIntArray796 = null;
		anInt823 = 960;
		anInt800 = -1;
		sizeY = 1;
		anInt825 = 256;
		anInt765 = -1;
		anInt822 = 64;
		anInt828 = -1;
		name = "null";
		aBoolean812 = false;
		notClipped = false;
		anInt832 = -1;
		blockingType = 2;
		aBoolean769 = true;
		anInt833 = 0;
		aByte787 = (byte) 0;
		anInt807 = 0;
		anInt804 = -1;
		aBoolean777 = false;
		aBoolean835 = false;
		aBoolean803 = false;
		anInt827 = -1;
		sizeX = 1;
		aBoolean779 = true;
		anInt815 = 255;
		anInt830 = 0;
		aBoolean838 = true;
		anInt834 = -1;
		anInt836 = 128;
	}
}
