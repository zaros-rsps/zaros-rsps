package net.zaros.client;

import jaclib.memory.Buffer;
import jaggl.OpenGL;

abstract class Class355 {
	private boolean aBoolean3061;
	int anInt3062;
	private int anInt3063;
	static int anInt3064 = Class296_Sub29_Sub2.method2694((byte) -65, 1600);
	private int anInt3065;
	ha_Sub3 aHa_Sub3_3066;
	static Class55 aClass55_5909;
	static Class394 aClass394_3067;
	static int[] anIntArray3068 = new int[4];

	static final void method3694(int i, String[] strings) {
		if (strings.length <= 1) {
			Class355_Sub2.aString3503 += strings[0];
			OutputStream_Sub2.anInt44 += strings[0].length();
		} else {
			for (int i_0_ = 0; strings.length > i_0_; i_0_++) {
				if (strings[i_0_].startsWith("pause")) {
					int i_1_ = 5;
					try {
						i_1_ = Integer.parseInt(strings[i_0_].substring(6));
					} catch (Exception exception) {
						/* empty */
					}
					Class41_Sub18.writeConsoleMessage("Pausing for " + i_1_ + " seconds...");
					Class296_Sub51_Sub3.anInt6347 = i_0_ + 1;
					Class199.aStringArray2004 = strings;
					Class230.aLong2208 = Class72.method771(-106) + i_1_ * 1000;
					return;
				}
				Class355_Sub2.aString3503 = strings[i_0_];
				Class144.method1483(false, i ^ ~0x88a6);
			}
		}
		if (i != 35044) {
			anIntArray3068 = null;
		}
	}

	abstract void method3695(byte i);

	final void method3696(byte[] is, int i, int i_2_) {
		method3695((byte) 109);
		if (i != 0) {
			aHa_Sub3_3066 = null;
		}
		if (anInt3063 >= i_2_) {
			OpenGL.glBufferSubDataARBub(anInt3065, 0, i_2_, is, 0);
		} else {
			OpenGL.glBufferDataARBub(anInt3065, i_2_, is, 0, !aBoolean3061 ? 35044 : 35040);
			aHa_Sub3_3066.anInt4151 += -anInt3063 + i_2_;
			anInt3063 = i_2_;
		}
	}

	@Override
	protected final void finalize() throws Throwable {
		aHa_Sub3_3066.method1301(anInt3062, -92, anInt3063);
		super.finalize();
	}

	Class355(ha_Sub3 var_ha_Sub3, int i, byte[] is, int i_3_, boolean bool) {
		anInt3063 = i_3_;
		anInt3065 = i;
		aHa_Sub3_3066 = var_ha_Sub3;
		aBoolean3061 = bool;
		OpenGL.glGenBuffersARB(1, Class95.anIntArray1037, 0);
		anInt3062 = Class95.anIntArray1037[0];
		method3695((byte) 98);
		OpenGL.glBufferDataARBub(i, anInt3063, is, 0, !aBoolean3061 ? 35044 : 35040);
		aHa_Sub3_3066.anInt4151 += anInt3063;
	}

	static final Class95 method3697(int i, int i_4_) {
		Class95 class95 = (Class95) Class42_Sub1.aClass113_3833.get(i_4_);
		if (class95 != null) {
			return class95;
		}
		byte[] is = Class366_Sub4.aClass138_5377.getFile(1, i_4_);
		if (i != 35044) {
			return null;
		}
		class95 = new Class95();
		class95.anInt1027 = i_4_;
		if (is != null) {
			class95.method863(0, new Packet(is));
		}
		class95.method862(false);
		if (class95.anInt1030 == 2 && Class41_Sub24.aClass263_3802.get(i_4_) == null) {
			Class41_Sub24.aClass263_3802.put(i_4_, new IntegerNode(Class179.anInt1855));
			Class144.aClass95Array1435[Class179.anInt1855++] = class95;
		}
		Class42_Sub1.aClass113_3833.put(class95, i_4_);
		return class95;
	}

	Class355(ha_Sub3 var_ha_Sub3, int i, Buffer buffer, int i_5_, boolean bool) {
		aHa_Sub3_3066 = var_ha_Sub3;
		anInt3065 = i;
		aBoolean3061 = bool;
		anInt3063 = i_5_;
		OpenGL.glGenBuffersARB(1, Class95.anIntArray1037, 0);
		anInt3062 = Class95.anIntArray1037[0];
		method3695((byte) 90);
		OpenGL.glBufferDataARBa(i, anInt3063, buffer.getAddress(), !aBoolean3061 ? 35044 : 35040);
		aHa_Sub3_3066.anInt4151 += anInt3063;
	}

	static final void method3698(int i, int i_6_, int i_7_, InterfaceComponent class51) {
		Class194.aClass51_1975 = class51;
		ModeWhat.anInt1189 = i_7_;
		int i_8_ = 7 % ((i_6_ + 24) / 35);
		Class41_Sub5.anInt3754 = i;
	}

	public static void method3699(int i) {
		if (i != 16903) {
			method3698(56, -21, 91, null);
		}
		anIntArray3068 = null;
		aClass394_3067 = null;
	}
}
