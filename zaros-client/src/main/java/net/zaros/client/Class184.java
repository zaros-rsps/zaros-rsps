package net.zaros.client;

/* Class184 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class184 {
	private Interface6_Impl1[] anInterface6_Impl1Array1881;
	static Js5 fs7;
	boolean aBoolean1883;
	private ha_Sub1 aHa_Sub1_1884;
	Interface6_Impl2 anInterface6_Impl2_1885;
	Interface6_Impl2 anInterface6_Impl2_1886 = null;
	Interface6_Impl1[] anInterface6_Impl1Array1887;

	static final boolean method1857(int i, int i_0_, byte i_1_) {
		if (!SubInPacket.method2242(i_0_, 86, i))
			return false;
		if ((i_0_ & 0x9000) != 0 | Class296_Sub51_Sub27_Sub1.method3161(-19214, i, i_0_) | Class367.method3803(i_0_, -88, i))
			return true;
		int i_2_ = -87 / ((-34 - i_1_) / 42);
		return (((i_0_ & 0x2000) != 0 | Class157.method1589(i_0_, -12205, i) | Class241_Sub2_Sub2.method2165(65536, i_0_, i)) & (i & 0x37) == 0);
	}

	final boolean method1858(byte i) {
		if (anInterface6_Impl2_1885 == null) {
			if (Class368_Sub5.anObject5445 == null) {
				byte[] is = Class241_Sub2_Sub1.method2161(0.6F, 128, 16.0F, 16, new Class226_Sub1(419684), 128, 4.0F, 0.5F, 4.0F, (byte) -127, 8);
				Class368_Sub5.anObject5445 = Class166.wrapBuffer(false, -72, is);
			}
			byte[] is = Class325.unwrapBuffer(false, -1, Class368_Sub5.anObject5445);
			byte[] is_3_ = new byte[is.length * 4];
			int i_4_ = 0;
			for (int i_5_ = 0; i_5_ < 16; i_5_++) {
				int i_6_ = i_5_ * 128 * 128;
				int i_7_ = i_6_;
				for (int i_8_ = 0; i_8_ < 128; i_8_++) {
					int i_9_ = i_7_ + i_8_ * 128;
					int i_10_ = i_7_ + (i_8_ - 1 & 0x7f) * 128;
					int i_11_ = i_7_ + (i_8_ + 1 & 0x7f) * 128;
					for (int i_12_ = 0; i_12_ < 128; i_12_++) {
						float f = (float) ((is[i_10_ + i_12_] & 0xff) - (is[i_12_ + i_11_] & 0xff));
						float f_13_ = (float) (-(is[(i_12_ + 1 & 0x7f) + i_9_] & 0xff) + (is[i_9_ + (i_12_ - 1 & 0x7f)] & 0xff));
						float f_14_ = (float) (128.0 / Math.sqrt((double) (f * f + (f_13_ * f_13_ + 16384.0F))));
						is_3_[i_4_++] = (byte) (int) (f_13_ * f_14_ + 127.0F);
						is_3_[i_4_++] = (byte) (int) (f_14_ * 128.0F + 127.0F);
						is_3_[i_4_++] = (byte) (int) (f_14_ * f + 127.0F);
						is_3_[i_4_++] = is[i_6_++];
					}
				}
			}
			anInterface6_Impl2_1885 = aHa_Sub1_1884.method1159(true, 128, za_Sub2.aClass202_6555, 16, 128, is_3_);
		}
		if (i >= -77)
			anInterface6_Impl1Array1887 = null;
		if (anInterface6_Impl2_1885 == null)
			return false;
		return true;
	}

	final boolean method1859(int i) {
		if (i != 16)
			aBoolean1883 = true;
		if (!aBoolean1883) {
			if (anInterface6_Impl1Array1887 == null)
				return false;
			return true;
		}
		if (anInterface6_Impl2_1886 == null)
			return false;
		return true;
	}

	public static void method1861(int i) {
		if (i == 0)
			fs7 = null;
	}

	Class184(ha_Sub1 var_ha_Sub1) {
		anInterface6_Impl2_1885 = null;
		anInterface6_Impl1Array1887 = null;
		anInterface6_Impl1Array1881 = null;
		aHa_Sub1_1884 = var_ha_Sub1;
		aBoolean1883 = aHa_Sub1_1884.aBoolean3968;
		if (aBoolean1883 && !aHa_Sub1_1884.method1147(Class67.aClass67_745, Class125.aClass202_1285, (byte) -14))
			aBoolean1883 = false;
		if (aBoolean1883 || aHa_Sub1_1884.method1200(Class67.aClass67_745, Class125.aClass202_1285, (byte) -8)) {
			Class166.method1642(128);
			if (aBoolean1883) {
				byte[] is = Class325.unwrapBuffer(false, -1, Class41_Sub1.anObject3738);
				anInterface6_Impl2_1886 = aHa_Sub1_1884.method1159(true, 128, Class125.aClass202_1285, 16, 128, is);
				is = Class325.unwrapBuffer(false, -1, Class363.anObject3109);
				aHa_Sub1_1884.method1159(true, 128, Class125.aClass202_1285, 16, 128, is);
			} else {
				anInterface6_Impl1Array1887 = new Interface6_Impl1[16];
				for (int i = 0; i < 16; i++) {
					byte[] is = Class366_Sub9.method3799(i * 128 * 128 * 2, 0, Class41_Sub1.anObject3738, 32768);
					anInterface6_Impl1Array1887[i] = aHa_Sub1_1884.method1180(true, 7, Class125.aClass202_1285, is, 128, 128);
				}
				anInterface6_Impl1Array1881 = new Interface6_Impl1[16];
				for (int i = 0; i < 16; i++) {
					byte[] is = Class366_Sub9.method3799(i * 2 * 16384, 0, Class363.anObject3109, 32768);
					anInterface6_Impl1Array1881[i] = aHa_Sub1_1884.method1180(true, 7, Class125.aClass202_1285, is, 128, 128);
				}
			}
		}
	}
}
