package net.zaros.client;

/* Class296_Sub51_Sub38 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub38 extends TextureOperation {
	private int anInt6537;
	static int[] anIntArray6538;
	private int anInt6539 = 1365;
	static OutgoingPacket aClass311_6540 = new OutgoingPacket(19, -1);
	private int anInt6541;
	static Class405 aClass405_6542;
	private int anInt6543;

	static final void method3194(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		if (i > -76)
			anIntArray6538 = null;
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4995.method489(126) != 0 && i_3_ != 0 && Class296_Sub51_Sub1.anInt6335 < 50 && i_0_ != -1)
			Class336.aClass391Array2956[Class296_Sub51_Sub1.anInt6335++] = new Class391((byte) 1, i_0_, i_3_, i_4_, i_2_, 0, i_1_, null);
	}

	static final int method3195(byte i) {
		if (i != 111)
			aClass311_6540 = null;
		if (InvisiblePlayer.aClass51_1982 == null) {
			if (Class318.aBoolean2814 || Class216.aClass296_Sub39_Sub9_2115 == null) {
				int i_5_ = Class84.aClass189_924.method1895((byte) -55);
				int i_6_ = Class84.aClass189_924.method1897(i ^ 0x6f);
				if (Class262.aBoolean1324) {
					if (i_5_ <= Class252.anInt2382 || i_5_ >= Class252.anInt2382 + Class81.anInt3666) {
						if (Class241.aClass296_Sub39_Sub1_2302 != null && Class41_Sub5.anInt3753 < i_5_ && i_5_ < (StaticMethods.anInt3152 + Class41_Sub5.anInt3753)) {
							int i_7_ = -1;
							for (int i_8_ = 0; i_8_ < (Class241.aClass296_Sub39_Sub1_2302.anInt6117); i_8_++) {
								if (!SeekableFile.aBoolean2397) {
									int i_9_ = Class270.anInt2510 + 31 + i_8_ * 16;
									if (i_6_ > i_9_ - 13 && i_6_ <= i_9_ + 3)
										i_7_ = i_8_;
								} else {
									int i_10_ = i_8_ * 16 + 33 + Class270.anInt2510;
									if (i_6_ > i_10_ - 13 && i_10_ + 3 >= i_6_)
										i_7_ = i_8_;
								}
							}
							if (i_7_ != -1) {
								int i_11_ = 0;
								Class83 class83 = new Class83(Class241.aClass296_Sub39_Sub1_2302.aClass145_6119);
								for (Class296_Sub39_Sub9 class296_sub39_sub9 = ((Class296_Sub39_Sub9) class83.method812(i - 111)); class296_sub39_sub9 != null; class296_sub39_sub9 = ((Class296_Sub39_Sub9) class83.method810((byte) 126))) {
									if (i_7_ == i_11_++)
										return class296_sub39_sub9.anInt6174;
								}
							}
						}
					} else {
						int i_12_ = -1;
						for (int i_13_ = 0; Class239.anInt2254 > i_13_; i_13_++) {
							if (SeekableFile.aBoolean2397) {
								int i_14_ = i_13_ * 16 + (Class100.anInt1067 + 33);
								if (i_6_ > i_14_ - 13 && i_6_ <= i_14_ + 3)
									i_12_ = i_13_;
							} else {
								int i_15_ = Class100.anInt1067 + 31 + i_13_ * 16;
								if (i_6_ > i_15_ - 13 && i_6_ <= i_15_ + 3)
									i_12_ = i_13_;
							}
						}
						if (i_12_ != -1) {
							int i_16_ = 0;
							Class83 class83 = new Class83(Class152.aClass145_1558);
							for (Class296_Sub39_Sub1 class296_sub39_sub1 = ((Class296_Sub39_Sub1) class83.method812(0)); class296_sub39_sub1 != null; class296_sub39_sub1 = ((Class296_Sub39_Sub1) class83.method810((byte) 96))) {
								if (i_12_ == i_16_++)
									return ((Class296_Sub39_Sub9) (class296_sub39_sub1.aClass145_6119.head.queue_next)).anInt6174;
							}
						}
					}
				} else if (i_5_ > Class252.anInt2382 && i_5_ < Class252.anInt2382 + Class81.anInt3666) {
					int i_17_ = -1;
					for (int i_18_ = 0; i_18_ < Class230.anInt2210; i_18_++) {
						if (SeekableFile.aBoolean2397) {
							int i_19_ = (Class100.anInt1067 + 33 + (-i_18_ + Class230.anInt2210 - 1) * 16);
							if (i_19_ - 13 < i_6_ && i_6_ <= i_19_ + 3)
								i_17_ = i_18_;
						} else {
							int i_20_ = ((-i_18_ + (Class230.anInt2210 - 1)) * 16 + Class100.anInt1067 + 31);
							if (i_6_ > i_20_ - 13 && i_6_ <= i_20_ + 3)
								i_17_ = i_18_;
						}
					}
					if (i_17_ != -1) {
						int i_21_ = 0;
						Class298 class298 = new Class298(HardReferenceWrapper.aClass155_6698);
						for (Class296_Sub39_Sub9 class296_sub39_sub9 = ((Class296_Sub39_Sub9) class298.method3236((byte) 64)); class296_sub39_sub9 != null; class296_sub39_sub9 = ((Class296_Sub39_Sub9) class298.method3235(true))) {
							if (i_21_++ == i_17_)
								return class296_sub39_sub9.anInt6174;
						}
					}
				}
			} else
				return Class216.aClass296_Sub39_Sub9_2115.anInt6174;
		}
		return -1;
	}

	public Class296_Sub51_Sub38() {
		super(0, true);
		anInt6537 = 0;
		anInt6541 = 0;
		anInt6543 = 20;
	}

	public static void method3196(int i) {
		anIntArray6538 = null;
		aClass405_6542 = null;
		aClass311_6540 = null;
		if (i != -28490)
			method3195((byte) 95);
	}

	static final void method3197(int i, int i_22_, int i_23_, int[] is, long[] ls) {
		if (i_22_ == 1) {
			if (i < i_23_) {
				int i_24_ = (i + i_23_) / 2;
				int i_25_ = i;
				long l = ls[i_24_];
				ls[i_24_] = ls[i_23_];
				ls[i_23_] = l;
				int i_26_ = is[i_24_];
				is[i_24_] = is[i_23_];
				is[i_23_] = i_26_;
				int i_27_ = l == 9223372036854775807L ? 0 : 1;
				for (int i_28_ = i; i_28_ < i_23_; i_28_++) {
					if ((long) (i_28_ & i_27_) + l > ls[i_28_]) {
						long l_29_ = ls[i_28_];
						ls[i_28_] = ls[i_25_];
						ls[i_25_] = l_29_;
						int i_30_ = is[i_28_];
						is[i_28_] = is[i_25_];
						is[i_25_++] = i_30_;
					}
				}
				ls[i_23_] = ls[i_25_];
				ls[i_25_] = l;
				is[i_23_] = is[i_25_];
				is[i_25_] = i_26_;
				method3197(i, i_22_, i_25_ - 1, is, ls);
				method3197(i_25_ + 1, 1, i_23_, is, ls);
			}
		}
	}

	static final void method3198(int i, int i_31_, int i_32_, int i_33_, int i_34_, int i_35_, int i_36_, ha var_ha, int i_37_, int i_38_, Class296_Sub39_Sub9 class296_sub39_sub9, int i_39_) {
		if (i_32_ >= -79)
			aClass311_6540 = null;
		if (i_38_ > i_37_ && i + i_37_ > i_38_ && i_34_ > i_35_ - 13 && i_35_ + 3 > i_34_ && class296_sub39_sub9.aBoolean6175)
			i_33_ = i_39_;
		int[] is = null;
		if (!Class295_Sub1.method2424(class296_sub39_sub9.anInt6165, (byte) 101)) {
			if (class296_sub39_sub9.anInt6167 == -1) {
				if (Class50.method609(true, class296_sub39_sub9.anInt6165)) {
					NPCNode class296_sub7 = ((NPCNode) (Class41_Sub18.localNpcs.get((long) (int) class296_sub39_sub9.aLong6162)));
					if (class296_sub7 != null) {
						NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
						NPCDefinition class147 = class338_sub3_sub1_sub3_sub2.definition;
						if (class147.configData != null)
							class147 = class147.getNPCDefinitionByConfigID((Class16_Sub3_Sub1.configsRegister));
						if (class147 != null)
							is = class147.anIntArray1499;
					}
				} else if (Class234.method2124((byte) 51, (class296_sub39_sub9.anInt6165))) {
					ObjectDefinition class70 = (Class379.objectDefinitionLoader.getObjectDefinition((int) (class296_sub39_sub9.aLong6162 >>> 32 & 0x7fffffffL)));
					if (class70.transforms != null)
						class70 = class70.method757((Class16_Sub3_Sub1.configsRegister), false);
					if (class70 != null)
						is = class70.anIntArray783;
				}
			} else
				is = (Class296_Sub39_Sub1.itemDefinitionLoader.list(class296_sub39_sub9.anInt6167).quests);
		} else
			is = (Class296_Sub39_Sub1.itemDefinitionLoader.list((int) class296_sub39_sub9.aLong6162).quests);
		String string = Class241_Sub1_Sub1.method2155(false, class296_sub39_sub9);
		if (is != null)
			string += Class338_Sub3_Sub1_Sub1.method3484((byte) 66, is);
		Class49.aClass55_461.a(i_33_, 0, i_37_ + 3, Class123_Sub1_Sub2.anIntArray5817, string, 0, i_35_, Class44_Sub1_Sub1.aClass397Array5811);
		if (class296_sub39_sub9.aBoolean6170)
			Class296_Sub2.aClass397_4589.method4096(Class304.aClass92_2729.method851(-107, string) + (i_37_ + 5), i_35_ - 12);
	}

	final void method3071(int i, Packet class296_sub17, int i_40_) {
		if (i > -84)
			anInt6537 = 6;
		int i_41_ = i_40_;
		while_269_ : do {
			while_268_ : do {
				do {
					if (i_41_ != 0) {
						if (i_41_ != 1) {
							if (i_41_ != 2) {
								if (i_41_ != 3)
									break while_269_;
							} else
								break;
							break while_268_;
						}
					} else {
						anInt6539 = class296_sub17.g2();
						break while_269_;
					}
					anInt6543 = class296_sub17.g2();
					break while_269_;
				} while (false);
				anInt6537 = class296_sub17.g2();
				break while_269_;
			} while (false);
			anInt6541 = class296_sub17.g2();
		} while (false);
	}

	final int[] get_monochrome_output(int i, int i_42_) {
		int[] is = aClass318_5035.method3335(i_42_, (byte) 28);
		if (i != 0)
			return null;
		if (aClass318_5035.aBoolean2819) {
			for (int i_43_ = 0; i_43_ < Class41_Sub10.anInt3769; i_43_++) {
				int i_44_ = ((Class33.anIntArray334[i_43_] << 12) / anInt6539 + anInt6537);
				int i_45_ = (anInt6541 + (Class294.anIntArray2686[i_42_] << 12) / anInt6539);
				int i_46_ = i_44_;
				int i_47_ = i_45_;
				int i_48_ = i_44_;
				int i_49_ = i_45_;
				int i_50_ = i_44_ * i_44_ >> 12;
				int i_51_ = i_45_ * i_45_ >> 12;
				int i_52_;
				for (i_52_ = 0; i_50_ + i_51_ < 16384 && i_52_ < anInt6543; i_50_ = i_48_ * i_48_ >> 12) {
					i_49_ = i_47_ + (i_48_ * i_49_ >> 12) * 2;
					i_48_ = i_46_ + (-i_51_ + i_50_);
					i_52_++;
					i_51_ = i_49_ * i_49_ >> 12;
				}
				is[i_43_] = anInt6543 - 1 > i_52_ ? (i_52_ << 12) / anInt6543 : 0;
			}
		}
		return is;
	}
}
