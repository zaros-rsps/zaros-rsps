package net.zaros.client;

/**
 * Holds all the constants for our GameClient.
 * 
 * @author Walied K. Yassen
 */
public interface Constants {

	/**
	 * Whether the client is currently running locally or not.
	 */
	boolean RUNNING_LOCAL = isJarFile();

	/**
	 * The current server host address.
	 */
	String HOST_ADDRESS = RUNNING_LOCAL ? "168.235.70.191" : "127.0.0.1";

	/**
	 * Whether or not should we enable ISAAC encryption layer.
	 */
	boolean ENABLE_ISAAC = false;

	/**
	 * Checks whether or not the client is running from a JAR file or not.
	 * 
	 * @return <code>true</code> if it is otherwise <code>false</code>.
	 */
	static boolean isJarFile() {
		String className = Constants.class.getName().replace('.', '/');
		String classJar = Constants.class.getResource("/" + className + ".class").toString();
		return classJar.startsWith("jar:");
	}
}
