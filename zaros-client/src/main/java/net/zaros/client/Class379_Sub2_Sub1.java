package net.zaros.client;

/* Class379_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class379_Sub2_Sub1 extends Class379_Sub2 {
	static int[] anIntArray6605;
	static int anInt6606;
	static int anInt6607 = 0;
	int anInt6608;
	static int anInt6609;

	public static void method3970(boolean bool) {
		if (bool != true)
			method3972(74);
		anIntArray6605 = null;
	}

	Class379_Sub2_Sub1(Class252 class252, Class357 class357, int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_) {
		super(class252, class357, i, i_0_, i_1_, i_2_, i_3_, i_4_, i_5_, i_6_, i_7_, i_8_, i_9_, i_10_, i_11_);
		anInt6608 = i_12_;
	}

	static final void method3971(Packet class296_sub17, int i) {
		int i_13_ = class296_sub17.readSmart();
		Class296_Sub51_Sub28.aClass303Array6488 = new Class303[i_13_];
		if (i != 6889)
			method3972(123);
		for (int i_14_ = 0; i_14_ < i_13_; i_14_++) {
			Class296_Sub51_Sub28.aClass303Array6488[i_14_] = new Class303();
			Class296_Sub51_Sub28.aClass303Array6488[i_14_].anInt2727 = class296_sub17.readSmart();
			Class296_Sub51_Sub28.aClass303Array6488[i_14_].aString2724 = class296_sub17.gjstr2();
		}
		Class368_Sub15.anInt5520 = class296_sub17.readSmart();
		Class338_Sub3_Sub3_Sub1.anInt6617 = class296_sub17.readSmart();
		Class296_Sub15_Sub1.anInt5998 = class296_sub17.readSmart();
		SubCache.aClass210_Sub1Array2713 = new Class210_Sub1[(Class338_Sub3_Sub3_Sub1.anInt6617 - Class368_Sub15.anInt5520 + 1)];
		for (int i_15_ = 0; i_15_ < Class296_Sub15_Sub1.anInt5998; i_15_++) {
			int i_16_ = class296_sub17.readSmart();
			Class210_Sub1 class210_sub1 = (SubCache.aClass210_Sub1Array2713[i_16_] = new Class210_Sub1());
			class210_sub1.anInt2095 = class296_sub17.g1();
			class210_sub1.anInt2098 = class296_sub17.g4();
			class210_sub1.anInt4540 = i_16_ + Class368_Sub15.anInt5520;
			class210_sub1.aString4542 = class296_sub17.gjstr2();
			class210_sub1.aString4543 = class296_sub17.gjstr2();
		}
		Class68.anInt758 = class296_sub17.g4();
		Class404.aBoolean3386 = true;
	}

	static final void method3972(int i) {
		if (i != 0)
			anInt6606 = -119;
		Class355_Sub1.itemsNodes.clear();
	}

	public final Class294 method45(byte i) {
		int i_17_ = -41 / ((i + 6) / 44);
		return Class83.aClass294_917;
	}

	static {
		anInt6606 = 0;
		anInt6609 = 0;
	}
}
