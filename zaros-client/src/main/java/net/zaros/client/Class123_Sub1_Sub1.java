package net.zaros.client;

/* Class123_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class123_Sub1_Sub1 extends Class123_Sub1 {
	private byte[] aByteArray5812;
	static int anInt5813 = -1;
	static Class92 aClass92_5814;

	public Class123_Sub1_Sub1() {
		super(8, 5, 8, 8, 2, 0.1F, 0.55F, 3.0F);
	}

	final byte[] method1061(int i, int i_0_, int i_1_, byte i_2_) {
		aByteArray5812 = new byte[i * (i_1_ * i_0_ * 2)];
		this.method1051(i_0_, i, i_2_ - 139, i_1_);
		if (i_2_ != 17)
			return null;
		return aByteArray5812;
	}

	final void method1058(byte i, byte i_3_, int i_4_) {
		int i_5_ = i_4_ * 2;
		aByteArray5812[i_5_++] = (byte) -1;
		int i_6_ = i_3_ & 0xff;
		int i_7_ = 92 / ((i + 24) / 42);
		aByteArray5812[i_5_] = (byte) (i_6_ * 3 >> 5);
	}

	public static void method1062(byte i) {
		aClass92_5814 = null;
		int i_8_ = 73 / ((i + 22) / 48);
	}

	static final void method1063(int i, Object[] objects, int i_9_, int[] is, int i_10_) {
		if (i != 2)
			aClass92_5814 = null;
		if (i_10_ < i_9_) {
			int i_11_ = (i_9_ + i_10_) / 2;
			int i_12_ = i_10_;
			int i_13_ = is[i_11_];
			is[i_11_] = is[i_9_];
			is[i_9_] = i_13_;
			Object object = objects[i_11_];
			objects[i_11_] = objects[i_9_];
			objects[i_9_] = object;
			int i_14_ = i_13_ == 2147483647 ? 0 : 1;
			for (int i_15_ = i_10_; i_15_ < i_9_; i_15_++) {
				if ((i_15_ & i_14_) + i_13_ > is[i_15_]) {
					int i_16_ = is[i_15_];
					is[i_15_] = is[i_12_];
					is[i_12_] = i_16_;
					Object object_17_ = objects[i_15_];
					objects[i_15_] = objects[i_12_];
					objects[i_12_++] = object_17_;
				}
			}
			is[i_9_] = is[i_12_];
			is[i_12_] = i_13_;
			objects[i_9_] = objects[i_12_];
			objects[i_12_] = object;
			method1063(2, objects, i_12_ - 1, is, i_10_);
			method1063(2, objects, i_9_, is, i_12_ + 1);
		}
	}
}
