package net.zaros.client;
import jaclib.memory.Stream;
import jaclib.memory.heap.NativeHeapBuffer;

import jaggl.OpenGL;

final class Class296_Sub32 extends Node {
	private int[] anIntArray4828;
	private s_Sub2 aS_Sub2_4829;
	int anInt4830;
	float aFloat4831;
	private ha_Sub3 aHa_Sub3_4832;
	int anInt4833;
	private NativeHeapBuffer aNativeHeapBuffer4834;
	private Class272 aClass272_4835;
	static OutgoingPacket aClass311_4836 = new OutgoingPacket(24, 7);
	int anInt4837;
	private Class295_Sub1 aClass295_Sub1_4838;
	int anInt4839;
	static GameType stellardawn = new GameType("stellardawn", "Stellar Dawn", 1);
	private Stream aStream4841;

	final void method2710(int i, int i_0_) {
		aStream4841.f(i * 4 + 3);
		aStream4841.e(-1);
		if (i_0_ > -9)
			method2712(-62);
	}

	final void method2711(byte i, int i_1_, int i_2_, int i_3_) {
		if (i > -7)
			aStream4841 = null;
		anIntArray4828[aS_Sub2_4829.anInt2832 * i_2_ + i_3_] = Class48.bitOR((anIntArray4828[aS_Sub2_4829.anInt2832 * i_2_ + i_3_]), 1 << i_1_);
	}

	static final void method2712(int i_4_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_4_, 20);
		class296_sub39_sub5.insertIntoQueue();
	}

	final void method2713(int i_5_) {
		aNativeHeapBuffer4834 = aHa_Sub3_4832.aNativeHeap4138.a(i_5_ * 4, true);
		aStream4841 = new Stream(aNativeHeapBuffer4834);
	}

	public static void method2715(int i) {
		stellardawn = null;
		if (i <= 20)
			aClass311_4836 = null;
		aClass311_4836 = null;
	}

	final void method2716(int i, int i_8_, float f, int i_9_, int i_10_) {
		if (anInt4833 != -1) {
			MaterialRaw class170 = aHa_Sub3_4832.aD1299.method14(anInt4833, -9412);
			int i_11_ = class170.aByte1773 & 0xff;
			if (i_11_ != 0 && class170.aByte1774 != 4) {
				int i_12_;
				if (i < 0)
					i_12_ = 0;
				else if (i <= 127)
					i_12_ = i * 131586;
				else
					i_12_ = 16777215;
				if (i_11_ != 256) {
					int i_13_ = i_11_;
					int i_14_ = -i_11_ + 256;
					i_9_ = (((i_12_ & 0xff00) * i_13_ + i_14_ * (i_9_ & 0xff00) & 0xff0000) + (((i_9_ & 0xff00ff) * i_14_ + (i_12_ & 0xff00ff) * i_13_) & ~0xff00ff)) >> 8;
				} else
					i_9_ = i_12_;
			}
			int i_15_ = class170.aByte1793 & 0xff;
			if (i_15_ != 0) {
				i_15_ += 256;
				int i_16_ = ((i_9_ & 0xff0000) >> 16) * i_15_;
				if (i_16_ > 65535)
					i_16_ = 65535;
				int i_17_ = i_15_ * ((i_9_ & 0xff00) >> 8);
				if (i_17_ > 65535)
					i_17_ = 65535;
				int i_18_ = (i_9_ & 0xff) * i_15_;
				if (i_18_ > 65535)
					i_18_ = 65535;
				i_9_ = (i_17_ & 0xff00) + (i_16_ << 8 & 0xff00c0) + (i_18_ >> 8);
			}
		}
		if (f != 1.0F) {
			int i_19_ = i_9_ >> 16 & 0xff;
			int i_20_ = i_9_ >> 8 & 0xff;
			i_19_ *= f;
			int i_21_ = i_9_ & 0xff;
			if (i_19_ < 0)
				i_19_ = 0;
			else if (i_19_ > 255)
				i_19_ = 255;
			i_20_ *= f;
			i_21_ *= f;
			if (i_20_ < 0)
				i_20_ = 0;
			else if (i_20_ > 255)
				i_20_ = 255;
			if (i_21_ >= 0) {
				if (i_21_ > 255)
					i_21_ = 255;
			} else
				i_21_ = 0;
			i_9_ = i_19_ << 16 | i_20_ << 8 | i_21_;
		}
		aStream4841.f(i_10_ * 4);
		aStream4841.e((byte) (i_9_ >> 16));
		aStream4841.e((byte) (i_9_ >> 8));
		if (i_8_ != -16022)
			method2711((byte) -67, 60, 81, 104);
		aStream4841.e((byte) i_9_);
	}

	final void method2717(int i, int i_22_) {
		aStream4841.b();
		Interface5 interface5 = aHa_Sub3_4832.method1347(-1, aNativeHeapBuffer4834, false, i_22_, i * 4);
		aClass272_4835 = new Class272(interface5, 5121, 4, 0);
		aNativeHeapBuffer4834 = null;
		aStream4841 = null;
	}

	final void method2718(int[] is, int i, int i_23_) {
		if (i == 256) {
			int i_24_ = 0;
			Class296_Sub17_Sub2 class296_sub17_sub2 = aHa_Sub3_4832.aClass296_Sub17_Sub2_4194;
			class296_sub17_sub2.pos = 0;
			if (!aHa_Sub3_4832.aBoolean4184) {
				for (int i_25_ = 0; i_23_ > i_25_; i_25_++) {
					int i_26_ = is[i_25_];
					short[] is_27_ = aS_Sub2_4829.aShortArrayArray5111[i_26_];
					int i_28_ = anIntArray4828[i_26_];
					if (i_28_ != 0 && is_27_ != null) {
						int i_29_ = 0;
						int i_30_ = 0;
						while (is_27_.length > i_30_) {
							if ((1 << i_29_++ & i_28_) == 0)
								i_30_ += 3;
							else {
								class296_sub17_sub2.method2590((is_27_[i_30_++] & 0xffff));
								i_24_++;
								i_24_++;
								class296_sub17_sub2.method2590((is_27_[i_30_++] & 0xffff));
								class296_sub17_sub2.method2590((is_27_[i_30_++] & 0xffff));
								i_24_++;
							}
						}
					}
				}
			} else {
				for (int i_31_ = 0; i_31_ < i_23_; i_31_++) {
					int i_32_ = is[i_31_];
					short[] is_33_ = aS_Sub2_4829.aShortArrayArray5111[i_32_];
					int i_34_ = anIntArray4828[i_32_];
					if (i_34_ != 0 && is_33_ != null) {
						int i_35_ = 0;
						int i_36_ = 0;
						while (i_36_ < is_33_.length) {
							if ((i_34_ & 1 << i_35_++) != 0) {
								class296_sub17_sub2.p2(is_33_[i_36_++] & 0xffff);
								i_24_++;
								class296_sub17_sub2.p2(is_33_[i_36_++] & 0xffff);
								i_24_++;
								i_24_++;
								class296_sub17_sub2.p2(is_33_[i_36_++] & 0xffff);
							} else
								i_36_ += 3;
						}
					}
				}
			}
			if (i_24_ > 0) {
				aClass295_Sub1_4838.method75(class296_sub17_sub2.pos, (class296_sub17_sub2.data), 5123, false);
				aHa_Sub3_4832.method1323(aS_Sub2_4829.aClass272_5133, aClass272_4835, aS_Sub2_4829.aClass272_5130, false, aS_Sub2_4829.aClass272_5139);
				aHa_Sub3_4832.method1297((aS_Sub2_4829.anInt5122 & 0x7) != 0, (byte) -76, (aS_Sub2_4829.anInt5122 & 0x8) != 0, anInt4833);
				if (aHa_Sub3_4832.underwater_mode)
					aHa_Sub3_4832.EA(2147483647, anInt4839, anInt4837, anInt4830);
				OpenGL.glMatrixMode(5890);
				OpenGL.glPushMatrix();
				OpenGL.glScalef(1.0F / aFloat4831, 1.0F / aFloat4831, 1.0F);
				OpenGL.glMatrixMode(5888);
				aHa_Sub3_4832.method1323(aS_Sub2_4829.aClass272_5133, aClass272_4835, aS_Sub2_4829.aClass272_5130, false, aS_Sub2_4829.aClass272_5139);
				aHa_Sub3_4832.method1348(i_24_, aClass295_Sub1_4838, 4, (byte) -17, 0);
				OpenGL.glMatrixMode(5890);
				OpenGL.glPopMatrix();
				OpenGL.glMatrixMode(5888);
			}
		}
	}

	Class296_Sub32(s_Sub2 var_s_Sub2, int i, int i_37_, int i_38_, int i_39_, int i_40_) {
		aS_Sub2_4829 = var_s_Sub2;
		aHa_Sub3_4832 = aS_Sub2_4829.aHa_Sub3_5126;
		anInt4839 = i_38_;
		aFloat4831 = (float) i_37_;
		anIntArray4828 = new int[aS_Sub2_4829.anInt2832 * aS_Sub2_4829.anInt2834];
		anInt4833 = i;
		anInt4837 = i_39_;
		anInt4830 = i_40_;
		aClass295_Sub1_4838 = new Class295_Sub1(aHa_Sub3_4832, 5123, null, 1);
	}
}
