package net.zaros.client;

/* Class296_Sub26 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class AnimBase extends Node {
	boolean[] aBooleanArray4764;
	int baseid;
	int anInt4766;
	static int anInt4767 = 0;
	int[] anIntArray4768;
	static Class190 aClass190_4769;
	int[] anIntArray4770;
	int[][] anIntArrayArray4771;

	static final void method2670(long l, int i, int i_0_, int i_1_, byte i_2_, int i_3_, boolean bool, Js5 class138) {
		Class296_Sub51_Sub7.anInt6371 = i_0_;
		if (i_2_ != -79) {
			method2671(-31, -71, -57, 122, 69, -79, -109);
		}
		Class296_Sub30.anInt4816 = 10000;
		Class368_Sub1.anInt5423 = i_1_;
		Class249.aClass296_Sub45_Sub4_2357 = null;
		Class392.anInt3488 = i_3_;
		TextureOperation.aClass138_5040 = class138;
		Class296_Sub50.aBoolean5025 = bool;
		ReferenceWrapper.anInt6128 = 1;
		Class287.anInt2644 = i;
		Class205_Sub4.aLong5635 = l;
	}

	static final void method2671(int i, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_) {
		Class338_Sub3_Sub2.method3553((byte) -44, i_8_);
		int i_10_ = 0;
		int i_11_ = i_8_ - i_9_;
		if (i_7_ > i_11_) {
			i_11_ = 0;
		}
		int i_12_ = i_8_;
		int i_13_ = -i_8_;
		int i_14_ = i_11_;
		int i_15_ = -i_11_;
		int i_16_ = -1;
		int i_17_ = -1;
		int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_5_];
		int i_18_ = -i_11_ + i_4_;
		Class296_Sub14.method2511(is, i_18_, (byte) 117, i, -i_8_ + i_4_);
		int i_19_ = i_11_ + i_4_;
		Class296_Sub14.method2511(is, i_19_, (byte) 112, i_6_, i_18_);
		Class296_Sub14.method2511(is, i_8_ + i_4_, (byte) 112, i, i_19_);
		while (i_10_ < i_12_) {
			i_16_ += 2;
			i_17_ += 2;
			i_15_ += i_17_;
			i_13_ += i_16_;
			if (i_15_ >= 0 && i_14_ >= 1) {
				Class358.anIntArray3087[i_14_] = i_10_;
				i_14_--;
				i_15_ -= i_14_ << 1;
			}
			i_10_++;
			if (i_13_ >= 0) {
				i_12_--;
				i_13_ -= i_12_ << 1;
				if (i_12_ >= i_11_) {
					int[] is_20_ = Class296_Sub51_Sub37.anIntArrayArray6536[i_12_ + i_5_];
					int[] is_21_ = Class296_Sub51_Sub37.anIntArrayArray6536[-i_12_ + i_5_];
					int i_22_ = i_4_ + i_10_;
					int i_23_ = -i_10_ + i_4_;
					Class296_Sub14.method2511(is_20_, i_22_, (byte) -120, i, i_23_);
					Class296_Sub14.method2511(is_21_, i_22_, (byte) -33, i, i_23_);
				} else {
					int[] is_24_ = Class296_Sub51_Sub37.anIntArrayArray6536[i_12_ + i_5_];
					int[] is_25_ = Class296_Sub51_Sub37.anIntArrayArray6536[i_5_ - i_12_];
					int i_26_ = Class358.anIntArray3087[i_12_];
					int i_27_ = i_4_ + i_10_;
					int i_28_ = i_4_ - i_10_;
					int i_29_ = i_26_ + i_4_;
					int i_30_ = -i_26_ + i_4_;
					Class296_Sub14.method2511(is_24_, i_30_, (byte) 126, i, i_28_);
					Class296_Sub14.method2511(is_24_, i_29_, (byte) -90, i_6_, i_30_);
					Class296_Sub14.method2511(is_24_, i_27_, (byte) -21, i, i_29_);
					Class296_Sub14.method2511(is_25_, i_30_, (byte) 122, i, i_28_);
					Class296_Sub14.method2511(is_25_, i_29_, (byte) 122, i_6_, i_30_);
					Class296_Sub14.method2511(is_25_, i_27_, (byte) -31, i, i_29_);
				}
			}
			int[] is_31_ = Class296_Sub51_Sub37.anIntArrayArray6536[i_10_ + i_5_];
			int[] is_32_ = Class296_Sub51_Sub37.anIntArrayArray6536[i_5_ - i_10_];
			int i_33_ = i_4_ + i_12_;
			int i_34_ = i_4_ - i_12_;
			if (i_11_ <= i_10_) {
				Class296_Sub14.method2511(is_31_, i_33_, (byte) -11, i, i_34_);
				Class296_Sub14.method2511(is_32_, i_33_, (byte) 121, i, i_34_);
			} else {
				int i_35_ = i_10_ > i_14_ ? Class358.anIntArray3087[i_10_] : i_14_;
				int i_36_ = i_4_ + i_35_;
				int i_37_ = -i_35_ + i_4_;
				Class296_Sub14.method2511(is_31_, i_37_, (byte) 2, i, i_34_);
				Class296_Sub14.method2511(is_31_, i_36_, (byte) 113, i_6_, i_37_);
				Class296_Sub14.method2511(is_31_, i_33_, (byte) 113, i, i_36_);
				Class296_Sub14.method2511(is_32_, i_37_, (byte) -34, i, i_34_);
				Class296_Sub14.method2511(is_32_, i_36_, (byte) 120, i_6_, i_37_);
				Class296_Sub14.method2511(is_32_, i_33_, (byte) -103, i, i_36_);
			}
		}
	}

	public static void method2672(byte i) {
		aClass190_4769 = null;
		if (i != 73) {
			method2671(-16, 46, 95, 104, 125, 104, 102);
		}
	}

	AnimBase(int i, byte[] data) {
		baseid = i;
		// boolean new_format = data[0] == 'N' && data[1] == 'E' && data[2] == 'W';
		Packet buffer = new Packet(data);
		// if (new_format) {
		// buffer.position += 3;
		// }
		anInt4766 = buffer.g1();
		aBooleanArray4764 = new boolean[anInt4766];
		anIntArrayArray4771 = new int[anInt4766][];
		anIntArray4768 = new int[anInt4766];
		anIntArray4770 = new int[anInt4766];
		for (int i_38_ = 0; i_38_ < anInt4766; i_38_++) {
			anIntArray4768[i_38_] = buffer.g1();
			if (anIntArray4768[i_38_] == 6) {
				anIntArray4768[i_38_] = 2;
			}
		}
		for (int i_39_ = 0; i_39_ < anInt4766; i_39_++) {
			aBooleanArray4764[i_39_] = buffer.g1() == 1;
		}
		for (int i_40_ = 0; anInt4766 > i_40_; i_40_++) {
			anIntArray4770[i_40_] = buffer.g2();
		}
		for (int i_41_ = 0; anInt4766 > i_41_; i_41_++) {
			anIntArrayArray4771[i_41_] = new int[buffer.g1()];
		}
		for (int i_42_ = 0; i_42_ < anInt4766; i_42_++) {
			for (int i_43_ = 0; i_43_ < anIntArrayArray4771[i_42_].length; i_43_++) {
				anIntArrayArray4771[i_42_][i_43_] = buffer.g1();
			}
		}
		if (buffer.data.length - buffer.pos > 0) {
			System.out.println("Mismatch base remaining: " + buffer.data.length + "," + buffer.pos);
		}
	}
}
