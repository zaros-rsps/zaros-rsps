package net.zaros.client;
/* Class90 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Rectangle;

final class ReferenceTable {
	int revision;
	int enryIndexCount;
	int crc;
	int[] entryVersions;
	int[] entryCRCS;
	int[] childIndexCounts;
	int[][] childIdentifiers;
	LookupTable entryIdentTable;
	int[] entryIdentifiers;
	static Class252 aClass252_975;
	int entryCount;
	int[][] childIndices;
	static int[] npcIndexes = new int[1024];
	int[] entryIndices;
	static Rectangle[] aRectangleArray980;
	int[] entryChildCounts;
	LookupTable[] childIdentTables;
	byte[][] whirlpoolDigests;
	static int[] anIntArray984 = {28, 35, 40, 44};
	private byte[] aByteArray985;
	static AdvancedMemoryCache aClass113_986;

	static final int method834(byte i) {
		if (Class106.aFloat1103 == 3.0) {
			return 37;
		}
		if (Class106.aFloat1103 == 4.0) {
			return 50;
		}
		if (Class106.aFloat1103 == 6.0) {
			return 75;
		}
		if (Class106.aFloat1103 == 8.0) {
			return 100;
		}
		if (i != 97) {
			method834((byte) -35);
		}
		return 200;
	}

	public static void method835(byte i) {
		anIntArray984 = null;
		npcIndexes = null;
		if (i < 43) {
			aClass113_986 = null;
		}
		aClass252_975 = null;
		aClass113_986 = null;
		aRectangleArray980 = null;
	}

	static final void method836(int i, int i_0_, int i_1_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask(0L, 15);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.anInt6145 = i_1_;
		class296_sub39_sub5.intParam = i;
		int i_2_ = -95 % ((i_0_ + 25) / 45);
	}

	private final void decode(byte[] data, byte i) {
		Packet in = new Packet(Class296_Sub51_Sub9.unpackContainer(false, data));
		int protocol = in.g1();
		if (protocol < 5 || protocol > 7) {
			throw new RuntimeException();
		}
		if (protocol >= 6) {
			revision = in.g4();
		} else {
			revision = 0;
		}
		int props = in.g1();
		boolean named = (props & 0x1) != 0;
		boolean hasWhirlpool = (props & 0x2) != 0;
		if (protocol < 7) {
			entryCount = in.g2();
		} else {
			entryCount = in.gSmart2or4();
		}
		int i_6_ = 0;
		entryIndices = new int[entryCount];
		int i_7_ = -1;
		if (protocol >= 7) {
			for (int i_8_ = 0; entryCount > i_8_; i_8_++) {
				entryIndices[i_8_] = i_6_ += in.gSmart2or4();
				if (entryIndices[i_8_] > i_7_) {
					i_7_ = entryIndices[i_8_];
				}
			}
		} else {
			for (int i_9_ = 0; entryCount > i_9_; i_9_++) {
				entryIndices[i_9_] = i_6_ += in.g2();
				if (i_7_ < entryIndices[i_9_]) {
					i_7_ = entryIndices[i_9_];
				}
			}
		}
		enryIndexCount = i_7_ + 1;
		entryCRCS = new int[enryIndexCount];
		entryChildCounts = new int[enryIndexCount];
		if (hasWhirlpool) {
			whirlpoolDigests = new byte[enryIndexCount][];
		}
		childIndexCounts = new int[enryIndexCount];
		childIndices = new int[enryIndexCount][];
		entryVersions = new int[enryIndexCount];
		if (named) {
			entryIdentifiers = new int[enryIndexCount];
			for (int i_10_ = 0; enryIndexCount > i_10_; i_10_++) {
				entryIdentifiers[i_10_] = -1;
			}
			for (int i_11_ = 0; entryCount > i_11_; i_11_++) {
				entryIdentifiers[entryIndices[i_11_]] = in.g4();
			}
			entryIdentTable = new LookupTable(entryIdentifiers);
		}
		for (int i_12_ = 0; i_12_ < entryCount; i_12_++) {
			entryCRCS[entryIndices[i_12_]] = in.g4();
		}
		if (hasWhirlpool) {
			for (int i_13_ = 0; entryCount > i_13_; i_13_++) {
				byte[] is_14_ = new byte[64];
				in.readBytes(is_14_, 0, 64);
				whirlpoolDigests[entryIndices[i_13_]] = is_14_;
			}
		}
		int i_15_ = 0;
		for (/**/; entryCount > i_15_; i_15_++) {
			entryVersions[entryIndices[i_15_]] = in.g4();
		}
		if (protocol >= 7) {
			for (int i_17_ = 0; i_17_ < entryCount; i_17_++) {
				entryChildCounts[entryIndices[i_17_]] = in.gSmart2or4();
			}
			for (int i_18_ = 0; i_18_ < entryCount; i_18_++) {
				int i_19_ = entryIndices[i_18_];
				int i_20_ = entryChildCounts[i_19_];
				i_6_ = 0;
				childIndices[i_19_] = new int[i_20_];
				int i_21_ = -1;
				for (int i_22_ = 0; i_20_ > i_22_; i_22_++) {
					int i_23_ = childIndices[i_19_][i_22_] = i_6_ += in.gSmart2or4();
					if (i_23_ > i_21_) {
						i_21_ = i_23_;
					}
				}
				childIndexCounts[i_19_] = i_21_ + 1;
				if (i_20_ == i_21_ + 1) {
					childIndices[i_19_] = null;
				}
			}
		} else {
			for (int i_24_ = 0; i_24_ < entryCount; i_24_++) {
				entryChildCounts[entryIndices[i_24_]] = in.g2();
			}
			for (int i_25_ = 0; entryCount > i_25_; i_25_++) {
				int i_26_ = entryIndices[i_25_];
				i_6_ = 0;
				int i_27_ = entryChildCounts[i_26_];
				int i_28_ = -1;
				childIndices[i_26_] = new int[i_27_];
				for (int i_29_ = 0; i_29_ < i_27_; i_29_++) {
					int i_30_ = childIndices[i_26_][i_29_] = i_6_ += in.g2();
					if (i_28_ < i_30_) {
						i_28_ = i_30_;
					}
				}
				childIndexCounts[i_26_] = i_28_ + 1;
				if (i_27_ == i_28_ + 1) {
					childIndices[i_26_] = null;
				}
			}
		}
		if (named) {
			childIdentifiers = new int[i_7_ + 1][];
			childIdentTables = new LookupTable[i_7_ + 1];
			for (int i_31_ = 0; i_31_ < entryCount; i_31_++) {
				int i_32_ = entryIndices[i_31_];
				int i_33_ = entryChildCounts[i_32_];
				childIdentifiers[i_32_] = new int[childIndexCounts[i_32_]];
				for (int i_34_ = 0; i_34_ < childIndexCounts[i_32_]; i_34_++) {
					childIdentifiers[i_32_][i_34_] = -1;
				}
				for (int i_35_ = 0; i_33_ > i_35_; i_35_++) {
					int i_36_;
					if (childIndices[i_32_] == null) {
						i_36_ = i_35_;
					} else {
						i_36_ = childIndices[i_32_][i_35_];
					}
					childIdentifiers[i_32_][i_36_] = in.g4();
				}
				childIdentTables[i_32_] = new LookupTable(childIdentifiers[i_32_]);
			}
		}
	}

	static final void method838(byte i, Mobile class338_sub3_sub1_sub3) {
		if (i < 14) {
			method835((byte) 76);
		}
		int i_37_ = -Class29.anInt307 + class338_sub3_sub1_sub3.anInt6809;
		int i_38_ = class338_sub3_sub1_sub3.anInt6813 * 512 + class338_sub3_sub1_sub3.getSize() * 256;
		int i_39_ = class338_sub3_sub1_sub3.anInt6811 * 512 + class338_sub3_sub1_sub3.getSize() * 256;
		class338_sub3_sub1_sub3.anInt6836 = 0;
		class338_sub3_sub1_sub3.tileY += (-class338_sub3_sub1_sub3.tileY + i_39_) / i_37_;
		class338_sub3_sub1_sub3.tileX += (-class338_sub3_sub1_sub3.tileX + i_38_) / i_37_;
		if (class338_sub3_sub1_sub3.anInt6819 == 0) {
			class338_sub3_sub1_sub3.method3499(8192);
		}
		if (class338_sub3_sub1_sub3.anInt6819 == 1) {
			class338_sub3_sub1_sub3.method3499(12288);
		}
		if (class338_sub3_sub1_sub3.anInt6819 == 2) {
			class338_sub3_sub1_sub3.method3499(0);
		}
		if (class338_sub3_sub1_sub3.anInt6819 == 3) {
			class338_sub3_sub1_sub3.method3499(4096);
		}
	}

	static final void method839(int i, int i_40_, int i_41_, int i_42_, int i_43_, int i_44_, int i_45_, int i_46_, int i_47_, int i_48_) {
		if (i_40_ >= 118) {
			if (i_48_ >= ConfigurationDefinition.anInt676 && i_48_ <= Class288.anInt2652 && ConfigurationDefinition.anInt676 <= i_41_ && Class288.anInt2652 >= i_41_ && i_46_ >= ConfigurationDefinition.anInt676 && i_46_ <= Class288.anInt2652 && i_43_ >= ConfigurationDefinition.anInt676 && i_43_ <= Class288.anInt2652 && i_42_ >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i_42_ && i_44_ >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i_44_ && i_47_ >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i_47_ && i >= EmissiveTriangle.anInt952 && i <= RuntimeException_Sub1.anInt3391) {
				Class48.method602(i_43_, i_47_, i_45_, i_44_, i_48_, 3, i, i_41_, i_42_, i_46_);
			} else {
				Class41_Sub4.method406(i_45_, i_42_, i_46_, i_41_, i_43_, (byte) 124, i_47_, i_48_, i, i_44_);
			}
		}
	}

	ReferenceTable(byte[] is, int i, byte[] is_49_) {
		crc = Class277.method2328(-81, is, is.length);
		if (crc != i) {
			throw new RuntimeException();
		}
		if (is_49_ != null) {
			if (is_49_.length != 64) {
				throw new RuntimeException();
			}
			aByteArray985 = Class163.method1629((byte) -32, is, is.length, 0);
			for (int i_50_ = 0; i_50_ < 64; i_50_++) {
				if (is_49_[i_50_] != aByteArray985[i_50_]) {
					throw new RuntimeException();
				}
			}
		}
		decode(is, (byte) -87);
	}

	static {
		aRectangleArray980 = new Rectangle[100];
		aClass252_975 = new Class252();
		aClass113_986 = new AdvancedMemoryCache(30);
	}
}
