package net.zaros.client;

/* Class18 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class18 {
	String aString191;
	private int anInt192;
	private int anInt193;
	byte[] aByteArray194;
	boolean aBoolean195;
	int anInt196 = -1;
	int anInt197;
	Class245 aClass245_198;
	int anInt199;
	private int anInt200;
	int anInt201;
	private int anInt202;
	int anInt203;
	int anInt204;
	int anInt205;
	int anInt206;
	boolean aBoolean207;
	private int anInt208;
	private int anInt209;
	int anInt210;
	int[] anIntArray211;
	String aString212;
	int anInt213;
	int anInt214;
	private int anInt215;
	private HashTable aClass263_216;
	int anInt217;
	int anInt218;
	int anInt219;
	private int anInt220;
	boolean aBoolean221;
	int anInt222;
	int anInt223;
	String[] aStringArray224;
	int anInt225;
	int anInt226;
	int anInt227;
	boolean aBoolean228;
	int[] anIntArray229;
	static OutgoingPacket aClass311_230 = new OutgoingPacket(76, 4);
	private int anInt231;
	static int anInt6160;
	static Class81 aClass81_6158 = new Class81("", 12);

	final String method271(String string, int i, byte i_0_) {
		if (aClass263_216 == null)
			return string;
		if (i_0_ != 127)
			return null;
		StringNode class296_sub5 = (StringNode) aClass263_216.get((long) i);
		if (class296_sub5 == null)
			return string;
		return class296_sub5.value;
	}

	final int method272(int i, int i_1_, int i_2_) {
		if (i != 13821)
			return -3;
		if (aClass263_216 == null)
			return i_2_;
		IntegerNode class296_sub16 = (IntegerNode) aClass263_216.get((long) i_1_);
		if (class296_sub16 == null)
			return i_2_;
		return class296_sub16.value;
	}

	private final void method273(int i, int i_3_, Packet class296_sub17) {
		if (i_3_ != 1) {
			if (i_3_ == 2)
				anInt197 = class296_sub17.g2();
			else if (i_3_ == 3)
				aString191 = class296_sub17.gstr();
			else if (i_3_ != 4) {
				if (i_3_ == 5)
					anInt204 = class296_sub17.readUnsignedMedInt();
				else if (i_3_ != 6) {
					if (i_3_ == 7) {
						int i_4_ = class296_sub17.g1();
						if ((i_4_ & 0x1) == 0)
							aBoolean207 = false;
						if ((i_4_ & 0x2) == 2)
							aBoolean195 = true;
					} else if (i_3_ == 8)
						aBoolean221 = class296_sub17.g1() == 1;
					else if (i_3_ == 9) {
						anInt193 = class296_sub17.g2();
						if (anInt193 == 65535)
							anInt193 = -1;
						anInt220 = class296_sub17.g2();
						if (anInt220 == 65535)
							anInt220 = -1;
						anInt231 = class296_sub17.g4();
						anInt209 = class296_sub17.g4();
					} else if (i_3_ < 10 || i_3_ > 14) {
						if (i_3_ != 15) {
							if (i_3_ == 16)
								aBoolean228 = false;
							else if (i_3_ == 17)
								aString212 = class296_sub17.gstr();
							else if (i_3_ != 18) {
								if (i_3_ != 19) {
									if (i_3_ == 20) {
										anInt208 = class296_sub17.g2();
										if (anInt208 == 65535)
											anInt208 = -1;
										anInt215 = class296_sub17.g2();
										if (anInt215 == 65535)
											anInt215 = -1;
										anInt200 = class296_sub17.g4();
										anInt192 = class296_sub17.g4();
									} else if (i_3_ != 21) {
										if (i_3_ == 22)
											anInt206 = class296_sub17.g4();
										else if (i_3_ == 23) {
											anInt196 = class296_sub17.g1();
											anInt226 = class296_sub17.g1();
											anInt227 = class296_sub17.g1();
										} else if (i_3_ != 24) {
											if (i_3_ == 249) {
												int i_5_ = class296_sub17.g1();
												if (aClass263_216 == null) {
													int i_6_ = (Class8.get_next_high_pow2(i_5_));
													aClass263_216 = new HashTable(i_6_);
												}
												for (int i_7_ = 0; i_7_ < i_5_; i_7_++) {
													boolean bool = (class296_sub17.g1() == 1);
													int i_8_ = (class296_sub17.readUnsignedMedInt());
													Node class296;
													if (bool)
														class296 = (new StringNode(class296_sub17.gstr()));
													else
														class296 = (new IntegerNode(class296_sub17.g4()));
													aClass263_216.put((long) i_8_, class296);
												}
											}
										} else {
											anInt199 = class296_sub17.g2b();
											anInt218 = class296_sub17.g2b();
										}
									} else
										anInt201 = class296_sub17.g4();
								} else
									anInt217 = class296_sub17.g2();
							} else
								anInt202 = class296_sub17.g2();
						} else {
							int i_9_ = class296_sub17.g1();
							anIntArray211 = new int[i_9_ * 2];
							for (int i_10_ = 0; i_10_ < i_9_ * 2; i_10_++)
								anIntArray211[i_10_] = class296_sub17.g2b();
							anInt213 = class296_sub17.g4();
							int i_11_ = class296_sub17.g1();
							anIntArray229 = new int[i_11_];
							for (int i_12_ = 0; i_12_ < anIntArray229.length; i_12_++)
								anIntArray229[i_12_] = class296_sub17.g4();
							aByteArray194 = new byte[i_9_];
							for (int i_13_ = 0; i_9_ > i_13_; i_13_++)
								aByteArray194[i_13_] = class296_sub17.g1b();
						}
					} else
						aStringArray224[i_3_ - 10] = class296_sub17.gstr();
				} else
					anInt214 = class296_sub17.g1();
			} else
				anInt225 = class296_sub17.readUnsignedMedInt();
		} else
			anInt203 = class296_sub17.g2();
		if (i > -34)
			method272(2, -1, -24);
	}

	final void method274(int i) {
		if (i != -2)
			anInt208 = -91;
		if (anIntArray211 != null) {
			for (int i_14_ = 0; anIntArray211.length > i_14_; i_14_ += 2) {
				if (anInt219 <= anIntArray211[i_14_]) {
					if (anIntArray211[i_14_] > anInt210)
						anInt210 = anIntArray211[i_14_];
				} else
					anInt219 = anIntArray211[i_14_];
				if (anInt223 <= anIntArray211[i_14_ + 1]) {
					if (anIntArray211[i_14_ + 1] > anInt222)
						anInt222 = anIntArray211[i_14_ + 1];
				} else
					anInt223 = anIntArray211[i_14_ + 1];
			}
		}
	}

	final Sprite method275(boolean bool, byte i, ha var_ha) {
		int i_15_ = bool ? anInt197 : anInt203;
		int i_16_ = var_ha.anInt1295 << 29 | i_15_;
		Sprite class397 = ((Sprite) aClass245_198.aClass113_2328.get((long) i_16_));
		if (i != 111)
			method280(null, false);
		if (class397 != null)
			return class397;
		if (!aClass245_198.aClass138_2323.hasEntryBuffer(i_15_))
			return null;
		Class186 class186 = Class186.method1876(aClass245_198.aClass138_2323, i_15_, 0);
		if (class186 != null) {
			class397 = var_ha.a(class186, true);
			aClass245_198.aClass113_2328.put(class397, (long) i_16_);
		}
		return class397;
	}

	final Sprite method276(ha var_ha, int i) {
		Sprite class397 = ((Sprite) aClass245_198.aClass113_2328.get((long) ((var_ha.anInt1295 << 29) | (anInt202 | 0x20000))));
		if (class397 != null)
			return class397;
		if (i != 17)
			anInt222 = -79;
		aClass245_198.aClass138_2323.hasEntryBuffer(anInt202);
		Class186 class186 = Class186.method1876(aClass245_198.aClass138_2323, anInt202, 0);
		if (class186 != null) {
			class397 = var_ha.a(class186, true);
			aClass245_198.aClass113_2328.put(class397, (long) ((var_ha.anInt1295 << 29) | (anInt202 | 0x20000)));
		}
		return class397;
	}

	static final int method277(int i, String string, int i_17_, boolean bool) {
		if (i_17_ < 2 || i_17_ > 36)
			throw new IllegalArgumentException("Invalid radix:" + i_17_);
		boolean bool_18_ = false;
		boolean bool_19_ = false;
		int i_20_ = 0;
		int i_21_ = string.length();
		for (int i_22_ = 0; i_22_ < i_21_; i_22_++) {
			int i_23_ = string.charAt(i_22_);
			if (i_22_ == 0) {
				if (i_23_ == 45) {
					bool_18_ = true;
					continue;
				}
				if (i_23_ == 43 && bool)
					continue;
			}
			if (i_23_ >= 48 && i_23_ <= 57)
				i_23_ -= 48;
			else if (i_23_ < 65 || i_23_ > 90) {
				if (i_23_ >= 97 && i_23_ <= 122)
					i_23_ -= 87;
				else
					throw new NumberFormatException();
			} else
				i_23_ -= 55;
			if (i_23_ >= i_17_)
				throw new NumberFormatException();
			if (bool_18_)
				i_23_ = -i_23_;
			int i_24_ = i_17_ * i_20_ + i_23_;
			if (i_24_ / i_17_ != i_20_)
				throw new NumberFormatException();
			bool_19_ = true;
			i_20_ = i_24_;
		}
		int i_25_ = 60 % ((62 - i) / 49);
		if (!bool_19_)
			throw new NumberFormatException();
		return i_20_;
	}

	final void method278(Packet class296_sub17, int i) {
		for (;;) {
			int i_26_ = class296_sub17.g1();
			if (i_26_ == 0)
				break;
			method273(-44, i_26_, class296_sub17);
		}
		if (i != -1)
			anInt196 = 91;
	}

	public static void method279(int i) {
		if (i < 29)
			method279(108);
		aClass311_230 = null;
	}

	final boolean method280(IConfigsRegister interface17, boolean bool) {
		if (bool != true)
			return true;
		int i;
		if (anInt220 == -1) {
			if (anInt193 == -1)
				return true;
			i = interface17.getBITConfig(anInt193, (byte) 62);
		} else
			i = interface17.getConfig(true, anInt220);
		if (anInt231 > i || anInt209 < i)
			return false;
		boolean bool_27_ = false;
		int i_28_;
		if (anInt215 != -1)
			i_28_ = interface17.getConfig(true, anInt215);
		else if (anInt208 != -1)
			i_28_ = interface17.getBITConfig(anInt208, (byte) 123);
		else
			return true;
		if (i_28_ < anInt200 || anInt192 < i_28_)
			return false;
		return true;
	}

	static final int method2825(int i, int i_6_) {
		if (Mobile.aByteArrayArray6776 != null)
			return Mobile.aByteArrayArray6776[i][i_6_] & 0xff;
		return 0;
	}

	public Class18() {
		anInt193 = -1;
		anInt210 = -2147483648;
		anInt204 = -1;
		anInt217 = -1;
		anInt208 = -1;
		anInt197 = -1;
		aBoolean221 = true;
		anInt222 = -2147483648;
		aStringArray224 = new String[5];
		anInt202 = -1;
		anInt219 = 2147483647;
		anInt220 = -1;
		aBoolean195 = false;
		anInt203 = -1;
		anInt223 = 2147483647;
		anInt214 = 0;
		aBoolean207 = true;
		aBoolean228 = true;
		anInt226 = -1;
		anInt227 = -1;
		anInt215 = -1;
	}
}
