package net.zaros.client;

import jaggl.OpenGL;

final class Class241_Sub2_Sub2 extends Class241_Sub2 {
	private ha_Sub3 aHa_Sub3_5904;
	private Class69_Sub3 aClass69_Sub3_5905;
	static ModeWhat wipModeWhat = new ModeWhat("WIP", 2);
	static boolean[][][] aBooleanArrayArrayArray5907;
	static int anInt5908 = 0;

	static final void method2162(int i, int i_0_, int i_1_, byte i_2_, int i_3_) {
		if (i_1_ > i_0_) {
			Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i], i_1_, (byte) -12, i_3_, i_0_);
		} else {
			Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i], i_0_, (byte) -58, i_3_, i_1_);
		}
		int i_4_ = 122 / ((i_2_ + 70) / 39);
	}

	static final byte[] method2163(int i, String string) {
		int i_5_ = string.length();
		if (i_5_ == 0) {
			return new byte[0];
		}
		int i_6_ = i_5_ + 3 & ~0x3;
		int i_7_ = i_6_ / 4 * 3;
		if (i < 58) {
			aBooleanArrayArrayArray5907 = null;
		}
		if (i_5_ > i_6_ - 2 && Class386.method4029(string.charAt(i_6_ - 2), -1) != -1) {
			if (i_5_ <= i_6_ - 1 || Class386.method4029(string.charAt(i_6_ - 1), -1) == -1) {
				i_7_--;
			}
		} else {
			i_7_ -= 2;
		}
		byte[] is = new byte[i_7_];
		ParticleEmitterRaw.method1663(-1, is, 0, string);
		return is;
	}

	final int method2164(byte i) {
		if (i >= -81) {
			return 25;
		}
		return aClass69_Sub3_5905.anInt5725;
	}

	static final boolean method2165(int i, int i_8_, int i_9_) {
		if (i != 65536) {
			return true;
		}
		if ((i_8_ & 0x10000) == 0) {
			return false;
		}
		return true;
	}

	public static void method2166(byte i) {
		aBooleanArrayArrayArray5907 = null;
		int i_10_ = -29 / ((7 - i) / 34);
		wipModeWhat = null;
	}

	@Override
	final Class69_Sub3 method2157(boolean bool) {
		if (bool) {
			anInt5908 = 113;
		}
		return aClass69_Sub3_5905;
	}

	final boolean method2168(Class69_Sub3 class69_sub3, Class69_Sub3 class69_sub3_11_, float f, int i) {
		boolean bool = true;
		Class392 class392 = aHa_Sub3_5904.aClass392_4260;
		aHa_Sub3_5904.K(Class30.anIntArray321);
		aHa_Sub3_5904.la();
		aHa_Sub3_5904.method1284(-31628);
		OpenGL.glMatrixMode(5889);
		OpenGL.glLoadIdentity();
		OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
		OpenGL.glMatrixMode(5888);
		OpenGL.glLoadIdentity();
		OpenGL.glPushAttrib(2048);
		OpenGL.glViewport(0, 0, aClass69_Sub3_5905.anInt5725, aClass69_Sub3_5905.anInt5725);
		aHa_Sub3_5904.method1311(false, 8577);
		aHa_Sub3_5904.method1278(1553921480, false);
		aHa_Sub3_5904.method1345(false, 54);
		aHa_Sub3_5904.method1285(-106, false);
		aHa_Sub3_5904.method1340(-2, (byte) 115);
		aHa_Sub3_5904.method1330(114, 1);
		aHa_Sub3_5904.method1275(0.0F, (byte) 126, 0.0F, f, 0.0F);
		aHa_Sub3_5904.method1306(34165, 34165, i ^ 0xa887);
		aHa_Sub3_5904.method1316(class69_sub3, (byte) -128);
		aHa_Sub3_5904.method1330(127, 0);
		aHa_Sub3_5904.method1272((byte) -107, 1);
		aHa_Sub3_5904.method1316(class69_sub3_11_, (byte) -109);
		aHa_Sub3_5904.method1351(class392, (byte) -27);
		if (i != -65535) {
			wipModeWhat = null;
		}
		for (int i_12_ = 0; i_12_ < 6; i_12_++) {
			int i_13_ = i_12_ + 34069;
			class392.method4049(0, i_13_, -1, aClass69_Sub3_5905);
			class392.method4045(0, 0);
			if (class392.method4053((byte) 118)) {
				OpenGL.glBegin(7);
				int i_14_ = i_13_;
				while_105_: do {
					while_104_: do {
						while_103_: do {
							while_102_: do {
								do {
									if (i_14_ != 34069) {
										if (i_14_ != 34070) {
											if (i_14_ != 34071) {
												if (i_14_ != 34072) {
													if (i_14_ != 34073) {
														if (i_14_ != 34074) {
															break while_105_;
														}
													} else {
														break while_103_;
													}
													break while_104_;
												}
											} else {
												break;
											}
											break while_102_;
										}
									} else {
										OpenGL.glTexCoord3i(65535, 65534, 65534);
										OpenGL.glMultiTexCoord3i(33985, 65535, 65534, 65534);
										OpenGL.glVertex2f(0.0F, 0.0F);
										OpenGL.glTexCoord3i(65535, 65534, -65534);
										OpenGL.glMultiTexCoord3i(33985, 65535, 65534, -65534);
										OpenGL.glVertex2f(1.0F, 0.0F);
										OpenGL.glTexCoord3i(65535, -65534, -65534);
										OpenGL.glMultiTexCoord3i(33985, 65535, -65534, -65534);
										OpenGL.glVertex2f(1.0F, 1.0F);
										OpenGL.glTexCoord3i(65535, -65534, 65534);
										OpenGL.glMultiTexCoord3i(33985, 65535, -65534, 65534);
										OpenGL.glVertex2f(0.0F, 1.0F);
										break while_105_;
									}
									OpenGL.glTexCoord3i(-65535, 65534, -65534);
									OpenGL.glMultiTexCoord3i(33985, -65535, 65534, -65534);
									OpenGL.glVertex2f(0.0F, 0.0F);
									OpenGL.glTexCoord3i(-65535, 65534, 65534);
									OpenGL.glMultiTexCoord3i(33985, -65535, 65534, 65534);
									OpenGL.glVertex2f(1.0F, 0.0F);
									OpenGL.glTexCoord3i(-65535, -65534, 65534);
									OpenGL.glMultiTexCoord3i(33985, -65535, -65534, 65534);
									OpenGL.glVertex2f(1.0F, 1.0F);
									OpenGL.glTexCoord3i(-65535, -65534, -65534);
									OpenGL.glMultiTexCoord3i(33985, -65535, -65534, -65534);
									OpenGL.glVertex2f(0.0F, 1.0F);
									break while_105_;
								} while (false);
								OpenGL.glTexCoord3i(-65534, 65535, -65534);
								OpenGL.glMultiTexCoord3i(33985, -65534, 65535, -65534);
								OpenGL.glVertex2f(0.0F, 0.0F);
								OpenGL.glTexCoord3i(65534, 65535, -65534);
								OpenGL.glMultiTexCoord3i(33985, 65534, 65535, -65534);
								OpenGL.glVertex2f(1.0F, 0.0F);
								OpenGL.glTexCoord3i(65534, 65535, 65534);
								OpenGL.glMultiTexCoord3i(33985, 65534, 65535, 65534);
								OpenGL.glVertex2f(1.0F, 1.0F);
								OpenGL.glTexCoord3i(-65534, 65535, 65534);
								OpenGL.glMultiTexCoord3i(33985, -65534, 65535, 65534);
								OpenGL.glVertex2f(0.0F, 1.0F);
								break while_105_;
							} while (false);
							OpenGL.glTexCoord3i(-65534, -65535, 65534);
							OpenGL.glMultiTexCoord3i(33985, -65534, -65535, 65534);
							OpenGL.glVertex2f(0.0F, 0.0F);
							OpenGL.glTexCoord3i(65534, -65535, 65534);
							OpenGL.glMultiTexCoord3i(33985, 65534, -65535, 65534);
							OpenGL.glVertex2f(1.0F, 0.0F);
							OpenGL.glTexCoord3i(65534, -65535, -65534);
							OpenGL.glMultiTexCoord3i(33985, 65534, -65535, -65534);
							OpenGL.glVertex2f(1.0F, 1.0F);
							OpenGL.glTexCoord3i(-65534, -65535, -65534);
							OpenGL.glMultiTexCoord3i(33985, -65534, -65535, -65534);
							OpenGL.glVertex2f(0.0F, 1.0F);
							break while_105_;
						} while (false);
						OpenGL.glTexCoord3i(-65534, 65534, 65535);
						OpenGL.glMultiTexCoord3i(33985, -65534, 65534, 65535);
						OpenGL.glVertex2f(0.0F, 0.0F);
						OpenGL.glTexCoord3i(65534, 65534, 65535);
						OpenGL.glMultiTexCoord3i(33985, 65534, 65534, 65535);
						OpenGL.glVertex2f(1.0F, 0.0F);
						OpenGL.glTexCoord3i(65534, -65534, 65535);
						OpenGL.glMultiTexCoord3i(33985, 65534, -65534, 65535);
						OpenGL.glVertex2f(1.0F, 1.0F);
						OpenGL.glTexCoord3i(-65534, -65534, 65535);
						OpenGL.glMultiTexCoord3i(33985, -65534, -65534, 65535);
						OpenGL.glVertex2f(0.0F, 1.0F);
						break while_105_;
					} while (false);
					OpenGL.glTexCoord3i(65534, 65534, -65535);
					OpenGL.glMultiTexCoord3i(33985, 65534, 65534, -65535);
					OpenGL.glVertex2f(0.0F, 0.0F);
					OpenGL.glTexCoord3i(-65534, 65534, -65535);
					OpenGL.glMultiTexCoord3i(33985, -65534, 65534, -65535);
					OpenGL.glVertex2f(1.0F, 0.0F);
					OpenGL.glTexCoord3i(-65534, -65534, -65535);
					OpenGL.glMultiTexCoord3i(33985, -65534, -65534, -65535);
					OpenGL.glVertex2f(1.0F, 1.0F);
					OpenGL.glTexCoord3i(65534, -65534, -65535);
					OpenGL.glMultiTexCoord3i(33985, 65534, -65534, -65535);
					OpenGL.glVertex2f(0.0F, 1.0F);
				} while (false);
				OpenGL.glEnd();
			} else {
				bool = false;
				break;
			}
		}
		class392.method4047((byte) -127, 0);
		aHa_Sub3_5904.method1280(class392, i ^ 0xfffe);
		aHa_Sub3_5904.method1330(i + 65658, 1);
		aHa_Sub3_5904.method1316(null, (byte) -122);
		aHa_Sub3_5904.method1306(8448, 8448, -22394);
		aHa_Sub3_5904.method1330(123, 0);
		aHa_Sub3_5904.method1316(null, (byte) -114);
		OpenGL.glPopAttrib();
		aHa_Sub3_5904.KA(Class30.anIntArray321[0], Class30.anIntArray321[1], Class30.anIntArray321[2], Class30.anIntArray321[3]);
		if (bool && !aHa_Sub3_5904.aBoolean4175) {
			aClass69_Sub3_5905.method719(10240);
		}
		return bool;
	}

	Class241_Sub2_Sub2(ha_Sub3 var_ha_Sub3, int i) {
		aHa_Sub3_5904 = var_ha_Sub3;
		aClass69_Sub3_5905 = new Class69_Sub3(var_ha_Sub3, 6408, i);
	}
}
