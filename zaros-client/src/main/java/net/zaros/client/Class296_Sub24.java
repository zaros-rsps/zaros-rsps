package net.zaros.client;

/* Class296_Sub24 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub24 extends Node {
	Class278[] aClass278Array4749;
	int[] anIntArray4750;
	byte[][][] aByteArrayArrayArray4751;
	static int[][] anIntArrayArray4752 = {{0, 2, 4, 6}, {6, 0, 2, 3, 5, 3}, {6, 0, 2, 4}, {2, 5, 6, 1}, {0, 2, 6}, {6, 0, 2}, {5, 6, 0, 1, 2, 4}, {7, 7, 1, 2, 4, 6}, {2, 4, 4, 7}, {6, 6, 4, 0, 1, 1, 3, 3}, {0, 2, 2, 6, 6, 4}, {0, 2, 2, 3, 7, 0, 4, 3}, {0, 2, 4, 6}};
	static SubInPacket aClass260_4753 = new SubInPacket(5, 8);
	int[] anIntArray4754;
	Class278[] aClass278Array4755;
	int anInt4756;
	int[] anIntArray4757;
	int anInt4758;
	static Class252 aClass252_4760 = new Class252();
	static String[][] aStringArrayArray4761 = {{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}, {"Jan", "Feb", "M\u00e4r", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"}, {"jan", "f\u00e9v", "mars", "avr", "mai", "juin", "juil", "ao\u00fbt", "sept", "oct", "nov", "d\u00e9c"}, {"jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago", "set", "out", "nov", "dez"}, {"jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"}};
	static int anInt4762;

	public static void method2669(int i) {
		IncomingPacket.CLANSETTINGS_FULL = null;
		if (i != 0)
			method2669(74);
		aClass260_4753 = null;
		anIntArrayArray4752 = null;
		aClass252_4760 = null;
		aStringArrayArray4761 = null;
	}

	public Class296_Sub24() {
		/* empty */
	}
}
