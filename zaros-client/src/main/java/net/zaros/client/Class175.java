package net.zaros.client;
/* Class175 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.net.InetAddress;

import jagex3.jagmisc.jagmisc;

final class Class175 implements Runnable {
	private Thread aThread1836;
	private NodeDeque aClass155_1837 = new NodeDeque();

	static final boolean method1701(byte i, int i_0_, int i_1_) {
		if (i != -9)
			return true;
		if ((i_0_ & 0x21) == 0)
			return false;
		return true;
	}

	public final void run() {
		for (;;) {
			Class296_Sub48 class296_sub48;
			synchronized (aClass155_1837) {
				Node class296;
				for (class296 = aClass155_1837.method1573(1); class296 == null; class296 = aClass155_1837.method1573(1)) {
					try {
						aClass155_1837.wait();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
				if (!(class296 instanceof Class296_Sub48))
					break;
				class296_sub48 = (Class296_Sub48) class296;
			}
			int i;
			try {
				byte[] is = InetAddress.getByName(class296_sub48.aString4974).getAddress();
				i = jagmisc.ping(is[0], is[1], is[2], is[3], 1000L);
			} catch (Throwable throwable) {
				i = 1000;
			}
			class296_sub48.anInt4976 = i;
		}
	}

	static final boolean method1702(int i, boolean bool, int i_2_) {
		if (bool != true)
			method1703((byte) -107);
		if ((i_2_ & 0x180) == 0)
			return false;
		return true;
	}

	public Class175() {
		aThread1836 = new Thread(this);
		aThread1836.setDaemon(true);
		aThread1836.start();
	}

	static final String method1703(byte i) {
		if (i != 105)
			return null;
		if (Class318.aBoolean2814 || Class216.aClass296_Sub39_Sub9_2115 == null)
			return "";
		if ((Class216.aClass296_Sub39_Sub9_2115.aString6166 == null || Class216.aClass296_Sub39_Sub9_2115.aString6166.length() == 0) && Class216.aClass296_Sub39_Sub9_2115.aString6171 != null && Class216.aClass296_Sub39_Sub9_2115.aString6171.length() > 0)
			return Class216.aClass296_Sub39_Sub9_2115.aString6171;
		return Class216.aClass296_Sub39_Sub9_2115.aString6166;
	}

	static final void method1704(int i, byte i_3_) {
		if (i_3_ <= -102) {
			Class296_Sub39_Sub13.aClass113_6205.clean(i);
			Class338_Sub3_Sub1_Sub4.aClass113_6641.clean(i);
			Class153.aClass113_1576.clean(i);
			Class41_Sub20.aClass113_3794.clean(i);
		}
	}

	final Class296_Sub48 method1705(byte i, String string) {
		if (aThread1836 == null)
			throw new IllegalStateException("");
		if (string == null)
			throw new IllegalArgumentException("");
		Class296_Sub48 class296_sub48 = new Class296_Sub48(string);
		method1707(0, class296_sub48);
		if (i != -91)
			aThread1836 = null;
		return class296_sub48;
	}

	final void method1706(int i) {
		if (i != -1)
			method1705((byte) 32, null);
		if (aThread1836 != null) {
			method1707(0, new Node());
			try {
				aThread1836.join();
			} catch (InterruptedException interruptedexception) {
				/* empty */
			}
			aThread1836 = null;
		}
	}

	private final void method1707(int i, Node class296) {
		if (i == 0) {
			synchronized (aClass155_1837) {
				aClass155_1837.addLast((byte) -127, class296);
				aClass155_1837.notify();
			}
		}
	}
}
