package net.zaros.client;
/* Class381_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;

final class Class381_Sub1 extends Class381 {
	private static Interface8 anInterface8_5620;
	private int anInt5621;

	final void method4000() {
		anInterface8_5620.method37(anInt5621, anIntArray3222);
	}

	final void method4001() {
		anInterface8_5620.method40(65536, anInt5621);
	}

	final void method3999(int i) throws Exception {
		if (i > 32768)
			throw new IllegalArgumentException();
		anInterface8_5620.method39(2, anInt5621, i);
	}

	final void method3997() {
		anInterface8_5620.method41(anInt5621, (byte) -111);
	}

	final void method4003(Component component) throws Exception {
		anInterface8_5620.method36(component, HardReferenceWrapper.aBoolean6695, Class298.anInt2699, -75);
	}

	Class381_Sub1(Class398 class398, int i) {
		anInterface8_5620 = (Interface8) class398.method4116(-34);
		anInt5621 = i;
	}

	public static void method4004() {
		anInterface8_5620 = null;
	}

	final int method3995() {
		return anInterface8_5620.method38(anInt5621, (byte) 94);
	}
}
