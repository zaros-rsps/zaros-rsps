package net.zaros.client;

/* Class306 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class306 {
	static Class306 aClass306_2739 = new Class306();
	static Class306 aClass306_2740 = new Class306();
	static Class306 aClass306_2741 = new Class306();
	static int[] anIntArray2742 = {1, -1, -1, 1};
	static Class188 aClass188_2743 = new Class188();
	static Class154 aClass154_2744;
	static IncomingPacket aClass231_2745 = new IncomingPacket(88, -1);

	public final String toString() {
		throw new IllegalStateException();
	}

	public static void method3282(int i) {
		aClass231_2745 = null;
		int i_0_ = 83 % ((31 - i) / 40);
		aClass154_2744 = null;
		aClass306_2741 = null;
		aClass306_2739 = null;
		aClass306_2740 = null;
		anIntArray2742 = null;
		aClass188_2743 = null;
	}

	static final void method3283(int i, int i_1_, int i_2_, int i_3_, Class338_Sub3_Sub2 class338_sub3_sub2) {
		Class247 class247 = Node.method2428(i, i_1_, i_2_);
		if (class247 != null) {
			class338_sub3_sub2.tileX = (i_1_ << Class313.anInt2779) + Class304.anInt2737;
			class338_sub3_sub2.anInt5213 = i_3_;
			class338_sub3_sub2.tileY = (i_2_ << Class313.anInt2779) + Class304.anInt2737;
			class247.aClass338_Sub3_Sub2_2339 = class338_sub3_sub2;
			int i_4_ = Class360_Sub2.aSArray5304 == Class52.aSArray636 ? 1 : 0;
			if (class338_sub3_sub2.method3459(0)) {
				if (class338_sub3_sub2.method3469(120)) {
					class338_sub3_sub2.aClass338_Sub3_5202 = Class368_Sub16.aClass338_Sub3Array5525[i_4_];
					Class368_Sub16.aClass338_Sub3Array5525[i_4_] = class338_sub3_sub2;
				} else {
					class338_sub3_sub2.aClass338_Sub3_5202 = Class368_Sub10.aClass338_Sub3Array5481[i_4_];
					Class368_Sub10.aClass338_Sub3Array5481[i_4_] = class338_sub3_sub2;
					Class41.aBoolean388 = true;
				}
			} else {
				class338_sub3_sub2.aClass338_Sub3_5202 = Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_4_];
				Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_4_] = class338_sub3_sub2;
			}
		}
	}

	public Class306() {
		/* empty */
	}

	static final void method3285() {
		for (int i = 0; i < StaticMethods.aClass142Array5951.length; i++)
			StaticMethods.aClass142Array5951[i].method1474();
		StaticMethods.aClass142Array5951 = null;
	}
}
