package net.zaros.client;

/* Class296_Sub45_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub45_Sub1 extends Class296_Sub45 {
	private int anInt6258;
	private int anInt6259;
	private int anInt6260;
	private int anInt6261;
	private boolean aBoolean6262;
	private int anInt6263;
	private int anInt6264;
	private int anInt6265;
	private int anInt6266;
	private int anInt6267;
	private int anInt6268;
	private int anInt6269;
	private int anInt6270;
	private int anInt6271;
	private int anInt6272;

	private static final int method2936(int i, int i_0_, byte[] is, int[] is_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, Class296_Sub45_Sub1 class296_sub45_sub1, int i_9_, int i_10_) {
		if (i_9_ == 0 || (i_6_ = i_3_ + (i_8_ - i_2_ + i_9_ - 257) / i_9_) > i_7_)
			i_6_ = i_7_;
		i_3_ <<= 1;
		i_6_ <<= 1;
		while (i_3_ < i_6_) {
			i_0_ = i_2_ >> 8;
			i = is[i_0_];
			i = (i << 8) + (is[i_0_ + 1] - i) * (i_2_ & 0xff);
			is_1_[i_3_++] += i * i_4_ >> 6;
			is_1_[i_3_++] += i * i_5_ >> 6;
			i_2_ += i_9_;
		}
		if (i_9_ == 0 || (i_6_ = (i_3_ >> 1) + (i_8_ - i_2_ + i_9_ - 1) / i_9_) > i_7_)
			i_6_ = i_7_;
		i_6_ <<= 1;
		i_0_ = i_10_;
		while (i_3_ < i_6_) {
			i = is[i_2_ >> 8];
			i = (i << 8) + (i_0_ - i) * (i_2_ & 0xff);
			is_1_[i_3_++] += i * i_4_ >> 6;
			is_1_[i_3_++] += i * i_5_ >> 6;
			i_2_ += i_9_;
		}
		class296_sub45_sub1.anInt6269 = i_2_;
		return i_3_ >> 1;
	}

	private static final int method2937(int i, int i_11_, byte[] is, int[] is_12_, int i_13_, int i_14_, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_, Class296_Sub45_Sub1 class296_sub45_sub1, int i_22_, int i_23_) {
		class296_sub45_sub1.anInt6265 -= class296_sub45_sub1.anInt6268 * i_14_;
		if (i_22_ == 0 || (i_19_ = i_14_ + (i_21_ + 256 - i_13_ + i_22_) / i_22_) > i_20_)
			i_19_ = i_20_;
		i_14_ <<= 1;
		i_19_ <<= 1;
		while (i_14_ < i_19_) {
			i_11_ = i_13_ >> 8;
			i = is[i_11_ - 1];
			i = (i << 8) + (is[i_11_] - i) * (i_13_ & 0xff);
			is_12_[i_14_++] += i * i_15_ >> 6;
			i_15_ += i_17_;
			is_12_[i_14_++] += i * i_16_ >> 6;
			i_16_ += i_18_;
			i_13_ += i_22_;
		}
		if (i_22_ == 0 || ((i_19_ = (i_14_ >> 1) + (i_21_ - i_13_ + i_22_) / i_22_) > i_20_))
			i_19_ = i_20_;
		i_19_ <<= 1;
		i_11_ = i_23_;
		while (i_14_ < i_19_) {
			i = (i_11_ << 8) + (is[i_13_ >> 8] - i_11_) * (i_13_ & 0xff);
			is_12_[i_14_++] += i * i_15_ >> 6;
			i_15_ += i_17_;
			is_12_[i_14_++] += i * i_16_ >> 6;
			i_16_ += i_18_;
			i_13_ += i_22_;
		}
		i_14_ >>= 1;
		class296_sub45_sub1.anInt6265 += class296_sub45_sub1.anInt6268 * i_14_;
		class296_sub45_sub1.anInt6260 = i_15_;
		class296_sub45_sub1.anInt6261 = i_16_;
		class296_sub45_sub1.anInt6269 = i_13_;
		return i_14_;
	}

	private final int method2938(int[] is, int i, int i_24_, int i_25_, int i_26_) {
		while (anInt6263 > 0) {
			int i_27_ = i + anInt6263;
			if (i_27_ > i_25_)
				i_27_ = i_25_;
			anInt6263 += i;
			if (anInt6266 == -256 && (anInt6269 & 0xff) == 0) {
				if (HardReferenceWrapper.aBoolean6695)
					i = method2953(0, ((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051, is, anInt6269, i, anInt6260, anInt6261, anInt6270, anInt6259, 0, i_27_, i_24_, this);
				else
					i = method2968(((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051, is, anInt6269, i, anInt6265, anInt6268, 0, i_27_, i_24_, this);
			} else if (HardReferenceWrapper.aBoolean6695)
				i = method2937(0, 0, (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6260, anInt6261, anInt6270, anInt6259, 0, i_27_, i_24_, this, anInt6266, i_26_);
			else
				i = method2947(0, 0, (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6265, anInt6268, 0, i_27_, i_24_, this, anInt6266, i_26_);
			anInt6263 -= i;
			if (anInt6263 != 0)
				return i;
			if (method2955())
				return i_25_;
		}
		if (anInt6266 == -256 && (anInt6269 & 0xff) == 0) {
			if (HardReferenceWrapper.aBoolean6695)
				return method2973(0, (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6260, anInt6261, 0, i_25_, i_24_, this);
			return method2965((((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6265, 0, i_25_, i_24_, this);
		}
		if (HardReferenceWrapper.aBoolean6695)
			return method2975(0, 0, (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6260, anInt6261, 0, i_25_, i_24_, this, anInt6266, i_26_);
		return method2944(0, 0, (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6265, 0, i_25_, i_24_, this, anInt6266, i_26_);
	}

	private static final int method2939(int i, int i_28_) {
		if (i_28_ < 0)
			return -i;
		return (int) ((double) i * Math.sqrt((double) i_28_ * 1.220703125E-4) + 0.5);
	}

	final Class296_Sub45 method2930() {
		return null;
	}

	final int method2935() {
		int i = anInt6265 * 3 >> 6;
		i = (i ^ i >> 31) + (i >>> 31);
		if (anInt6271 == 0)
			i -= i * anInt6269 / ((((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051).length << 8);
		else if (anInt6271 >= 0)
			i -= i * anInt6258 / (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051).length;
		if (i > 255)
			return 255;
		return i;
	}

	final synchronized void method2934(int[] is, int i, int i_29_) {
		if (anInt6264 == 0 && anInt6263 == 0)
			method2933(i_29_);
		else {
			Class296_Sub19_Sub1 class296_sub19_sub1 = (Class296_Sub19_Sub1) aClass296_Sub19_4951;
			int i_30_ = anInt6258 << 8;
			int i_31_ = anInt6267 << 8;
			int i_32_ = class296_sub19_sub1.aByteArray6051.length << 8;
			int i_33_ = i_31_ - i_30_;
			if (i_33_ <= 0)
				anInt6271 = 0;
			int i_34_ = i;
			i_29_ += i;
			if (anInt6269 < 0) {
				if (anInt6266 > 0)
					anInt6269 = 0;
				else {
					method2948();
					this.unlink();
					return;
				}
			}
			if (anInt6269 >= i_32_) {
				if (anInt6266 < 0)
					anInt6269 = i_32_ - 1;
				else {
					method2948();
					this.unlink();
					return;
				}
			}
			if (anInt6271 < 0) {
				if (aBoolean6262) {
					if (anInt6266 < 0) {
						i_34_ = method2938(is, i_34_, i_30_, i_29_, (class296_sub19_sub1.aByteArray6051[anInt6258]));
						if (anInt6269 >= i_30_)
							return;
						anInt6269 = i_30_ + i_30_ - 1 - anInt6269;
						anInt6266 = -anInt6266;
					}
					for (;;) {
						i_34_ = method2950(is, i_34_, i_31_, i_29_, (class296_sub19_sub1.aByteArray6051[anInt6267 - 1]));
						if (anInt6269 < i_31_)
							break;
						anInt6269 = i_31_ + i_31_ - 1 - anInt6269;
						anInt6266 = -anInt6266;
						i_34_ = method2938(is, i_34_, i_30_, i_29_, (class296_sub19_sub1.aByteArray6051[anInt6258]));
						if (anInt6269 >= i_30_)
							break;
						anInt6269 = i_30_ + i_30_ - 1 - anInt6269;
						anInt6266 = -anInt6266;
					}
				} else if (anInt6266 < 0) {
					for (;;) {
						i_34_ = method2938(is, i_34_, i_30_, i_29_, (class296_sub19_sub1.aByteArray6051[anInt6267 - 1]));
						if (anInt6269 >= i_30_)
							break;
						anInt6269 = i_31_ - 1 - (i_31_ - 1 - anInt6269) % i_33_;
					}
				} else {
					for (;;) {
						i_34_ = method2950(is, i_34_, i_31_, i_29_, (class296_sub19_sub1.aByteArray6051[anInt6258]));
						if (anInt6269 < i_31_)
							break;
						anInt6269 = i_30_ + (anInt6269 - i_30_) % i_33_;
					}
				}
			} else {
				do {
					if (anInt6271 > 0) {
						if (aBoolean6262) {
							if (anInt6266 < 0) {
								i_34_ = method2938(is, i_34_, i_30_, i_29_, (class296_sub19_sub1.aByteArray6051[anInt6258]));
								if (anInt6269 >= i_30_)
									return;
								anInt6269 = i_30_ + i_30_ - 1 - anInt6269;
								anInt6266 = -anInt6266;
								if (--anInt6271 == 0)
									break;
							}
							do {
								i_34_ = method2950(is, i_34_, i_31_, i_29_, (class296_sub19_sub1.aByteArray6051[anInt6267 - 1]));
								if (anInt6269 < i_31_)
									return;
								anInt6269 = i_31_ + i_31_ - 1 - anInt6269;
								anInt6266 = -anInt6266;
								if (--anInt6271 == 0)
									break;
								i_34_ = method2938(is, i_34_, i_30_, i_29_, (class296_sub19_sub1.aByteArray6051[anInt6258]));
								if (anInt6269 >= i_30_)
									return;
								anInt6269 = i_30_ + i_30_ - 1 - anInt6269;
								anInt6266 = -anInt6266;
							} while (--anInt6271 != 0);
						} else if (anInt6266 < 0) {
							for (;;) {
								i_34_ = method2938(is, i_34_, i_30_, i_29_, (class296_sub19_sub1.aByteArray6051[anInt6267 - 1]));
								if (anInt6269 >= i_30_)
									return;
								int i_35_ = (i_31_ - 1 - anInt6269) / i_33_;
								if (i_35_ >= anInt6271) {
									anInt6269 += i_33_ * anInt6271;
									anInt6271 = 0;
									break;
								}
								anInt6269 += i_33_ * i_35_;
								anInt6271 -= i_35_;
							}
						} else {
							for (;;) {
								i_34_ = method2950(is, i_34_, i_31_, i_29_, (class296_sub19_sub1.aByteArray6051[anInt6258]));
								if (anInt6269 < i_31_)
									return;
								int i_36_ = (anInt6269 - i_30_) / i_33_;
								if (i_36_ >= anInt6271) {
									anInt6269 -= i_33_ * anInt6271;
									anInt6271 = 0;
									break;
								}
								anInt6269 -= i_33_ * i_36_;
								anInt6271 -= i_36_;
							}
						}
					}
				} while (false);
				if (anInt6266 < 0) {
					method2938(is, i_34_, 0, i_29_, 0);
					if (anInt6269 < 0) {
						anInt6269 = -1;
						method2948();
						this.unlink();
					}
				} else {
					method2950(is, i_34_, i_32_, i_29_, 0);
					if (anInt6269 >= i_32_) {
						anInt6269 = i_32_;
						method2948();
						this.unlink();
					}
				}
			}
		}
	}

	private static final int method2940(int i, int i_37_, byte[] is, int[] is_38_, int i_39_, int i_40_, int i_41_, int i_42_, int i_43_, int i_44_, Class296_Sub45_Sub1 class296_sub45_sub1, int i_45_, int i_46_) {
		if (i_45_ == 0 || (i_42_ = i_40_ + (i_44_ - i_39_ + i_45_ - 257) / i_45_) > i_43_)
			i_42_ = i_43_;
		while (i_40_ < i_42_) {
			i_37_ = i_39_ >> 8;
			i = is[i_37_];
			is_38_[i_40_++] += (((i << 8) + (is[i_37_ + 1] - i) * (i_39_ & 0xff)) * i_41_ >> 6);
			i_39_ += i_45_;
		}
		if (i_45_ == 0 || (i_42_ = i_40_ + (i_44_ - i_39_ + i_45_ - 1) / i_45_) > i_43_)
			i_42_ = i_43_;
		i_37_ = i_46_;
		while (i_40_ < i_42_) {
			i = is[i_39_ >> 8];
			is_38_[i_40_++] += ((i << 8) + (i_37_ - i) * (i_39_ & 0xff)) * i_41_ >> 6;
			i_39_ += i_45_;
		}
		class296_sub45_sub1.anInt6269 = i_39_;
		return i_40_;
	}

	private static final int method2941(int i, int i_47_, byte[] is, int[] is_48_, int i_49_, int i_50_, int i_51_, int i_52_, int i_53_, int i_54_, int i_55_, Class296_Sub45_Sub1 class296_sub45_sub1, int i_56_, int i_57_) {
		class296_sub45_sub1.anInt6260 -= class296_sub45_sub1.anInt6270 * i_50_;
		class296_sub45_sub1.anInt6261 -= class296_sub45_sub1.anInt6259 * i_50_;
		if (i_56_ == 0 || (i_53_ = i_50_ + (i_55_ - i_49_ + i_56_ - 257) / i_56_) > i_54_)
			i_53_ = i_54_;
		while (i_50_ < i_53_) {
			i_47_ = i_49_ >> 8;
			i = is[i_47_];
			is_48_[i_50_++] += (((i << 8) + (is[i_47_ + 1] - i) * (i_49_ & 0xff)) * i_51_ >> 6);
			i_51_ += i_52_;
			i_49_ += i_56_;
		}
		if (i_56_ == 0 || (i_53_ = i_50_ + (i_55_ - i_49_ + i_56_ - 1) / i_56_) > i_54_)
			i_53_ = i_54_;
		i_47_ = i_57_;
		while (i_50_ < i_53_) {
			i = is[i_49_ >> 8];
			is_48_[i_50_++] += ((i << 8) + (i_47_ - i) * (i_49_ & 0xff)) * i_51_ >> 6;
			i_51_ += i_52_;
			i_49_ += i_56_;
		}
		class296_sub45_sub1.anInt6260 += class296_sub45_sub1.anInt6270 * i_50_;
		class296_sub45_sub1.anInt6261 += class296_sub45_sub1.anInt6259 * i_50_;
		class296_sub45_sub1.anInt6265 = i_51_;
		class296_sub45_sub1.anInt6269 = i_49_;
		return i_50_;
	}

	private final synchronized void method2942(int i) {
		method2959(i, method2954());
	}

	final synchronized void method2943(int i) {
		method2959(method2957(), i);
	}

	private static final int method2944(int i, int i_58_, byte[] is, int[] is_59_, int i_60_, int i_61_, int i_62_, int i_63_, int i_64_, int i_65_, Class296_Sub45_Sub1 class296_sub45_sub1, int i_66_, int i_67_) {
		if (i_66_ == 0 || (i_63_ = i_61_ + (i_65_ + 256 - i_60_ + i_66_) / i_66_) > i_64_)
			i_63_ = i_64_;
		while (i_61_ < i_63_) {
			i_58_ = i_60_ >> 8;
			i = is[i_58_ - 1];
			is_59_[i_61_++] += ((i << 8) + (is[i_58_] - i) * (i_60_ & 0xff)) * i_62_ >> 6;
			i_60_ += i_66_;
		}
		if (i_66_ == 0 || (i_63_ = i_61_ + (i_65_ - i_60_ + i_66_) / i_66_) > i_64_)
			i_63_ = i_64_;
		i = i_67_;
		i_58_ = i_66_;
		while (i_61_ < i_63_) {
			is_59_[i_61_++] += (((i << 8) + (is[i_60_ >> 8] - i) * (i_60_ & 0xff)) * i_62_ >> 6);
			i_60_ += i_58_;
		}
		class296_sub45_sub1.anInt6269 = i_60_;
		return i_61_;
	}

	private final void method2945() {
		anInt6265 = anInt6264;
		anInt6260 = method2949(anInt6264, anInt6272);
		anInt6261 = method2939(anInt6264, anInt6272);
	}

	final synchronized void method2946(int i, int i_68_) {
		method2956(i, i_68_, method2954());
	}

	private static final int method2947(int i, int i_69_, byte[] is, int[] is_70_, int i_71_, int i_72_, int i_73_, int i_74_, int i_75_, int i_76_, int i_77_, Class296_Sub45_Sub1 class296_sub45_sub1, int i_78_, int i_79_) {
		class296_sub45_sub1.anInt6260 -= class296_sub45_sub1.anInt6270 * i_72_;
		class296_sub45_sub1.anInt6261 -= class296_sub45_sub1.anInt6259 * i_72_;
		if (i_78_ == 0 || (i_75_ = i_72_ + (i_77_ + 256 - i_71_ + i_78_) / i_78_) > i_76_)
			i_75_ = i_76_;
		while (i_72_ < i_75_) {
			i_69_ = i_71_ >> 8;
			i = is[i_69_ - 1];
			is_70_[i_72_++] += ((i << 8) + (is[i_69_] - i) * (i_71_ & 0xff)) * i_73_ >> 6;
			i_73_ += i_74_;
			i_71_ += i_78_;
		}
		if (i_78_ == 0 || (i_75_ = i_72_ + (i_77_ - i_71_ + i_78_) / i_78_) > i_76_)
			i_75_ = i_76_;
		i = i_79_;
		i_69_ = i_78_;
		while (i_72_ < i_75_) {
			is_70_[i_72_++] += (((i << 8) + (is[i_71_ >> 8] - i) * (i_71_ & 0xff)) * i_73_ >> 6);
			i_73_ += i_74_;
			i_71_ += i_69_;
		}
		class296_sub45_sub1.anInt6260 += class296_sub45_sub1.anInt6270 * i_72_;
		class296_sub45_sub1.anInt6261 += class296_sub45_sub1.anInt6259 * i_72_;
		class296_sub45_sub1.anInt6265 = i_73_;
		class296_sub45_sub1.anInt6269 = i_71_;
		return i_72_;
	}

	private final void method2948() {
		if (anInt6263 != 0) {
			if (anInt6264 == -2147483648)
				anInt6264 = 0;
			anInt6263 = 0;
			method2945();
		}
	}

	private static final int method2949(int i, int i_80_) {
		if (i_80_ < 0)
			return i;
		return (int) (((double) i * Math.sqrt((double) (16384 - i_80_) * 1.220703125E-4)) + 0.5);
	}

	private final int method2950(int[] is, int i, int i_81_, int i_82_, int i_83_) {
		while (anInt6263 > 0) {
			int i_84_ = i + anInt6263;
			if (i_84_ > i_82_)
				i_84_ = i_82_;
			anInt6263 += i;
			if (anInt6266 == 256 && (anInt6269 & 0xff) == 0) {
				if (HardReferenceWrapper.aBoolean6695)
					i = method2962(0, ((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051, is, anInt6269, i, anInt6260, anInt6261, anInt6270, anInt6259, 0, i_84_, i_81_, this);
				else
					i = method2969(((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051, is, anInt6269, i, anInt6265, anInt6268, 0, i_84_, i_81_, this);
			} else if (HardReferenceWrapper.aBoolean6695)
				i = method2972(0, 0, (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6260, anInt6261, anInt6270, anInt6259, 0, i_84_, i_81_, this, anInt6266, i_83_);
			else
				i = method2941(0, 0, (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6265, anInt6268, 0, i_84_, i_81_, this, anInt6266, i_83_);
			anInt6263 -= i;
			if (anInt6263 != 0)
				return i;
			if (method2955())
				return i_82_;
		}
		if (anInt6266 == 256 && (anInt6269 & 0xff) == 0) {
			if (HardReferenceWrapper.aBoolean6695)
				return method2966(0, (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6260, anInt6261, 0, i_82_, i_81_, this);
			return method2963((((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6265, 0, i_82_, i_81_, this);
		}
		if (HardReferenceWrapper.aBoolean6695)
			return method2936(0, 0, (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6260, anInt6261, 0, i_82_, i_81_, this, anInt6266, i_83_);
		return method2940(0, 0, (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051), is, anInt6269, i, anInt6265, 0, i_82_, i_81_, this, anInt6266, i_83_);
	}

	final synchronized void method2951(boolean bool) {
		anInt6266 = (anInt6266 ^ anInt6266 >> 31) + (anInt6266 >>> 31);
		if (bool)
			anInt6266 = -anInt6266;
	}

	final synchronized void method2952(int i) {
		if (anInt6266 < 0)
			anInt6266 = -i;
		else
			anInt6266 = i;
	}

	private static final int method2953(int i, byte[] is, int[] is_85_, int i_86_, int i_87_, int i_88_, int i_89_, int i_90_, int i_91_, int i_92_, int i_93_, int i_94_, Class296_Sub45_Sub1 class296_sub45_sub1) {
		i_86_ >>= 8;
		i_94_ >>= 8;
		i_88_ <<= 2;
		i_89_ <<= 2;
		i_90_ <<= 2;
		i_91_ <<= 2;
		if ((i_92_ = i_87_ + i_86_ - (i_94_ - 1)) > i_93_)
			i_92_ = i_93_;
		class296_sub45_sub1.anInt6265 += class296_sub45_sub1.anInt6268 * (i_92_ - i_87_);
		i_87_ <<= 1;
		i_92_ <<= 1;
		i_92_ -= 6;
		while (i_87_ < i_92_) {
			i = is[i_86_--];
			is_85_[i_87_++] += i * i_88_;
			i_88_ += i_90_;
			is_85_[i_87_++] += i * i_89_;
			i_89_ += i_91_;
			i = is[i_86_--];
			is_85_[i_87_++] += i * i_88_;
			i_88_ += i_90_;
			is_85_[i_87_++] += i * i_89_;
			i_89_ += i_91_;
			i = is[i_86_--];
			is_85_[i_87_++] += i * i_88_;
			i_88_ += i_90_;
			is_85_[i_87_++] += i * i_89_;
			i_89_ += i_91_;
			i = is[i_86_--];
			is_85_[i_87_++] += i * i_88_;
			i_88_ += i_90_;
			is_85_[i_87_++] += i * i_89_;
			i_89_ += i_91_;
		}
		i_92_ += 6;
		while (i_87_ < i_92_) {
			i = is[i_86_--];
			is_85_[i_87_++] += i * i_88_;
			i_88_ += i_90_;
			is_85_[i_87_++] += i * i_89_;
			i_89_ += i_91_;
		}
		class296_sub45_sub1.anInt6260 = i_88_ >> 2;
		class296_sub45_sub1.anInt6261 = i_89_ >> 2;
		class296_sub45_sub1.anInt6269 = i_86_ << 8;
		return i_87_ >> 1;
	}

	final synchronized int method2954() {
		if (anInt6272 < 0)
			return -1;
		return anInt6272;
	}

	private final boolean method2955() {
		int i = anInt6264;
		int i_95_;
		int i_96_;
		if (i == -2147483648)
			i = i_95_ = i_96_ = 0;
		else {
			i_95_ = method2949(i, anInt6272);
			i_96_ = method2939(i, anInt6272);
		}
		if (anInt6265 != i || anInt6260 != i_95_ || anInt6261 != i_96_) {
			if (anInt6265 < i) {
				anInt6268 = 1;
				anInt6263 = i - anInt6265;
			} else if (anInt6265 > i) {
				anInt6268 = -1;
				anInt6263 = anInt6265 - i;
			} else
				anInt6268 = 0;
			if (anInt6260 < i_95_) {
				anInt6270 = 1;
				if (anInt6263 == 0 || anInt6263 > i_95_ - anInt6260)
					anInt6263 = i_95_ - anInt6260;
			} else if (anInt6260 > i_95_) {
				anInt6270 = -1;
				if (anInt6263 == 0 || anInt6263 > anInt6260 - i_95_)
					anInt6263 = anInt6260 - i_95_;
			} else
				anInt6270 = 0;
			if (anInt6261 < i_96_) {
				anInt6259 = 1;
				if (anInt6263 == 0 || anInt6263 > i_96_ - anInt6261)
					anInt6263 = i_96_ - anInt6261;
			} else if (anInt6261 > i_96_) {
				anInt6259 = -1;
				if (anInt6263 == 0 || anInt6263 > anInt6261 - i_96_)
					anInt6263 = anInt6261 - i_96_;
			} else
				anInt6259 = 0;
			return false;
		}
		if (anInt6264 == -2147483648) {
			anInt6264 = 0;
			anInt6265 = anInt6260 = anInt6261 = 0;
			this.unlink();
			return true;
		}
		method2945();
		return false;
	}

	final int method2929() {
		if (anInt6264 == 0 && anInt6263 == 0)
			return 0;
		return 1;
	}

	final synchronized void method2956(int i, int i_97_, int i_98_) {
		if (i == 0)
			method2959(i_97_, i_98_);
		else {
			int i_99_ = method2949(i_97_, i_98_);
			int i_100_ = method2939(i_97_, i_98_);
			if (anInt6260 == i_99_ && anInt6261 == i_100_)
				anInt6263 = 0;
			else {
				int i_101_ = i_97_ - anInt6265;
				if (anInt6265 - i_97_ > i_101_)
					i_101_ = anInt6265 - i_97_;
				if (i_99_ - anInt6260 > i_101_)
					i_101_ = i_99_ - anInt6260;
				if (anInt6260 - i_99_ > i_101_)
					i_101_ = anInt6260 - i_99_;
				if (i_100_ - anInt6261 > i_101_)
					i_101_ = i_100_ - anInt6261;
				if (anInt6261 - i_100_ > i_101_)
					i_101_ = anInt6261 - i_100_;
				if (i > i_101_)
					i = i_101_;
				anInt6263 = i;
				anInt6264 = i_97_;
				anInt6272 = i_98_;
				anInt6268 = (i_97_ - anInt6265) / i;
				anInt6270 = (i_99_ - anInt6260) / i;
				anInt6259 = (i_100_ - anInt6261) / i;
			}
		}
	}

	final synchronized int method2957() {
		if (anInt6264 == -2147483648)
			return 0;
		return anInt6264;
	}

	final synchronized void method2958(int i) {
		method2959(i << 6, method2954());
	}

	private final synchronized void method2959(int i, int i_102_) {
		anInt6264 = i;
		anInt6272 = i_102_;
		anInt6263 = 0;
		method2945();
	}

	final synchronized void method2933(int i) {
		if (anInt6263 > 0) {
			if (i >= anInt6263) {
				if (anInt6264 == -2147483648) {
					anInt6264 = 0;
					anInt6265 = anInt6260 = anInt6261 = 0;
					this.unlink();
					i = anInt6263;
				}
				anInt6263 = 0;
				method2945();
			} else {
				anInt6265 += anInt6268 * i;
				anInt6260 += anInt6270 * i;
				anInt6261 += anInt6259 * i;
				anInt6263 -= i;
			}
		}
		Class296_Sub19_Sub1 class296_sub19_sub1 = (Class296_Sub19_Sub1) aClass296_Sub19_4951;
		int i_103_ = anInt6258 << 8;
		int i_104_ = anInt6267 << 8;
		int i_105_ = class296_sub19_sub1.aByteArray6051.length << 8;
		int i_106_ = i_104_ - i_103_;
		if (i_106_ <= 0)
			anInt6271 = 0;
		if (anInt6269 < 0) {
			if (anInt6266 > 0)
				anInt6269 = 0;
			else {
				method2948();
				this.unlink();
				return;
			}
		}
		if (anInt6269 >= i_105_) {
			if (anInt6266 < 0)
				anInt6269 = i_105_ - 1;
			else {
				method2948();
				this.unlink();
				return;
			}
		}
		anInt6269 += anInt6266 * i;
		if (anInt6271 < 0) {
			if (aBoolean6262) {
				if (anInt6266 < 0) {
					if (anInt6269 >= i_103_)
						return;
					anInt6269 = i_103_ + i_103_ - 1 - anInt6269;
					anInt6266 = -anInt6266;
				}
				while (anInt6269 >= i_104_) {
					anInt6269 = i_104_ + i_104_ - 1 - anInt6269;
					anInt6266 = -anInt6266;
					if (anInt6269 >= i_103_)
						break;
					anInt6269 = i_103_ + i_103_ - 1 - anInt6269;
					anInt6266 = -anInt6266;
				}
			} else if (anInt6266 < 0) {
				if (anInt6269 < i_103_)
					anInt6269 = i_104_ - 1 - (i_104_ - 1 - anInt6269) % i_106_;
			} else if (anInt6269 >= i_104_)
				anInt6269 = i_103_ + (anInt6269 - i_103_) % i_106_;
		} else {
			do {
				if (anInt6271 > 0) {
					if (aBoolean6262) {
						if (anInt6266 < 0) {
							if (anInt6269 >= i_103_)
								return;
							anInt6269 = i_103_ + i_103_ - 1 - anInt6269;
							anInt6266 = -anInt6266;
							if (--anInt6271 == 0)
								break;
						}
						do {
							if (anInt6269 < i_104_)
								return;
							anInt6269 = i_104_ + i_104_ - 1 - anInt6269;
							anInt6266 = -anInt6266;
							if (--anInt6271 == 0)
								break;
							if (anInt6269 >= i_103_)
								return;
							anInt6269 = i_103_ + i_103_ - 1 - anInt6269;
							anInt6266 = -anInt6266;
						} while (--anInt6271 != 0);
					} else if (anInt6266 < 0) {
						if (anInt6269 >= i_103_)
							return;
						int i_107_ = (i_104_ - 1 - anInt6269) / i_106_;
						if (i_107_ >= anInt6271) {
							anInt6269 += i_106_ * anInt6271;
							anInt6271 = 0;
						} else {
							anInt6269 += i_106_ * i_107_;
							anInt6271 -= i_107_;
							return;
						}
					} else {
						if (anInt6269 < i_104_)
							return;
						int i_108_ = (anInt6269 - i_103_) / i_106_;
						if (i_108_ >= anInt6271) {
							anInt6269 -= i_106_ * anInt6271;
							anInt6271 = 0;
						} else {
							anInt6269 -= i_106_ * i_108_;
							anInt6271 -= i_108_;
							return;
						}
					}
				}
			} while (false);
			if (anInt6266 < 0) {
				if (anInt6269 < 0) {
					anInt6269 = -1;
					method2948();
					this.unlink();
				}
			} else if (anInt6269 >= i_105_) {
				anInt6269 = i_105_;
				method2948();
				this.unlink();
			}
		}
	}

	final synchronized void method2960(int i) {
		anInt6271 = i;
	}

	final synchronized void method2961(int i) {
		int i_109_ = ((((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051).length << 8);
		if (i < -1)
			i = -1;
		if (i > i_109_)
			i = i_109_;
		anInt6269 = i;
	}

	private static final int method2962(int i, byte[] is, int[] is_110_, int i_111_, int i_112_, int i_113_, int i_114_, int i_115_, int i_116_, int i_117_, int i_118_, int i_119_, Class296_Sub45_Sub1 class296_sub45_sub1) {
		i_111_ >>= 8;
		i_119_ >>= 8;
		i_113_ <<= 2;
		i_114_ <<= 2;
		i_115_ <<= 2;
		i_116_ <<= 2;
		if ((i_117_ = i_112_ + i_119_ - i_111_) > i_118_)
			i_117_ = i_118_;
		class296_sub45_sub1.anInt6265 += class296_sub45_sub1.anInt6268 * (i_117_ - i_112_);
		i_112_ <<= 1;
		i_117_ <<= 1;
		i_117_ -= 6;
		while (i_112_ < i_117_) {
			i = is[i_111_++];
			is_110_[i_112_++] += i * i_113_;
			i_113_ += i_115_;
			is_110_[i_112_++] += i * i_114_;
			i_114_ += i_116_;
			i = is[i_111_++];
			is_110_[i_112_++] += i * i_113_;
			i_113_ += i_115_;
			is_110_[i_112_++] += i * i_114_;
			i_114_ += i_116_;
			i = is[i_111_++];
			is_110_[i_112_++] += i * i_113_;
			i_113_ += i_115_;
			is_110_[i_112_++] += i * i_114_;
			i_114_ += i_116_;
			i = is[i_111_++];
			is_110_[i_112_++] += i * i_113_;
			i_113_ += i_115_;
			is_110_[i_112_++] += i * i_114_;
			i_114_ += i_116_;
		}
		i_117_ += 6;
		while (i_112_ < i_117_) {
			i = is[i_111_++];
			is_110_[i_112_++] += i * i_113_;
			i_113_ += i_115_;
			is_110_[i_112_++] += i * i_114_;
			i_114_ += i_116_;
		}
		class296_sub45_sub1.anInt6260 = i_113_ >> 2;
		class296_sub45_sub1.anInt6261 = i_114_ >> 2;
		class296_sub45_sub1.anInt6269 = i_111_ << 8;
		return i_112_ >> 1;
	}

	private static final int method2963(byte[] is, int[] is_120_, int i, int i_121_, int i_122_, int i_123_, int i_124_, int i_125_, Class296_Sub45_Sub1 class296_sub45_sub1) {
		i >>= 8;
		i_125_ >>= 8;
		i_122_ <<= 2;
		if ((i_123_ = i_121_ + i_125_ - i) > i_124_)
			i_123_ = i_124_;
		i_123_ -= 3;
		while (i_121_ < i_123_) {
			is_120_[i_121_++] += is[i++] * i_122_;
			is_120_[i_121_++] += is[i++] * i_122_;
			is_120_[i_121_++] += is[i++] * i_122_;
			is_120_[i_121_++] += is[i++] * i_122_;
		}
		i_123_ += 3;
		while (i_121_ < i_123_)
			is_120_[i_121_++] += is[i++] * i_122_;
		class296_sub45_sub1.anInt6269 = i << 8;
		return i_121_;
	}

	final boolean method2964() {
		if (anInt6269 >= 0 && anInt6269 < (((Class296_Sub19_Sub1) aClass296_Sub19_4951).aByteArray6051).length << 8)
			return false;
		return true;
	}

	private static final int method2965(byte[] is, int[] is_126_, int i, int i_127_, int i_128_, int i_129_, int i_130_, int i_131_, Class296_Sub45_Sub1 class296_sub45_sub1) {
		i >>= 8;
		i_131_ >>= 8;
		i_128_ <<= 2;
		if ((i_129_ = i_127_ + i - (i_131_ - 1)) > i_130_)
			i_129_ = i_130_;
		i_129_ -= 3;
		while (i_127_ < i_129_) {
			is_126_[i_127_++] += is[i--] * i_128_;
			is_126_[i_127_++] += is[i--] * i_128_;
			is_126_[i_127_++] += is[i--] * i_128_;
			is_126_[i_127_++] += is[i--] * i_128_;
		}
		i_129_ += 3;
		while (i_127_ < i_129_)
			is_126_[i_127_++] += is[i--] * i_128_;
		class296_sub45_sub1.anInt6269 = i << 8;
		return i_127_;
	}

	private static final int method2966(int i, byte[] is, int[] is_132_, int i_133_, int i_134_, int i_135_, int i_136_, int i_137_, int i_138_, int i_139_, Class296_Sub45_Sub1 class296_sub45_sub1) {
		i_133_ >>= 8;
		i_139_ >>= 8;
		i_135_ <<= 2;
		i_136_ <<= 2;
		if ((i_137_ = i_134_ + i_139_ - i_133_) > i_138_)
			i_137_ = i_138_;
		i_134_ <<= 1;
		i_137_ <<= 1;
		i_137_ -= 6;
		while (i_134_ < i_137_) {
			i = is[i_133_++];
			is_132_[i_134_++] += i * i_135_;
			is_132_[i_134_++] += i * i_136_;
			i = is[i_133_++];
			is_132_[i_134_++] += i * i_135_;
			is_132_[i_134_++] += i * i_136_;
			i = is[i_133_++];
			is_132_[i_134_++] += i * i_135_;
			is_132_[i_134_++] += i * i_136_;
			i = is[i_133_++];
			is_132_[i_134_++] += i * i_135_;
			is_132_[i_134_++] += i * i_136_;
		}
		i_137_ += 6;
		while (i_134_ < i_137_) {
			i = is[i_133_++];
			is_132_[i_134_++] += i * i_135_;
			is_132_[i_134_++] += i * i_136_;
		}
		class296_sub45_sub1.anInt6269 = i_133_ << 8;
		return i_134_ >> 1;
	}

	final synchronized void method2967(int i) {
		if (i == 0) {
			method2942(0);
			this.unlink();
		} else if (anInt6260 == 0 && anInt6261 == 0) {
			anInt6263 = 0;
			anInt6264 = 0;
			anInt6265 = 0;
			this.unlink();
		} else {
			int i_140_ = -anInt6265;
			if (anInt6265 > i_140_)
				i_140_ = anInt6265;
			if (-anInt6260 > i_140_)
				i_140_ = -anInt6260;
			if (anInt6260 > i_140_)
				i_140_ = anInt6260;
			if (-anInt6261 > i_140_)
				i_140_ = -anInt6261;
			if (anInt6261 > i_140_)
				i_140_ = anInt6261;
			if (i > i_140_)
				i = i_140_;
			anInt6263 = i;
			anInt6264 = -2147483648;
			anInt6268 = -anInt6265 / i;
			anInt6270 = -anInt6260 / i;
			anInt6259 = -anInt6261 / i;
		}
	}

	private static final int method2968(byte[] is, int[] is_141_, int i, int i_142_, int i_143_, int i_144_, int i_145_, int i_146_, int i_147_, Class296_Sub45_Sub1 class296_sub45_sub1) {
		i >>= 8;
		i_147_ >>= 8;
		i_143_ <<= 2;
		i_144_ <<= 2;
		if ((i_145_ = i_142_ + i - (i_147_ - 1)) > i_146_)
			i_145_ = i_146_;
		class296_sub45_sub1.anInt6260 += class296_sub45_sub1.anInt6270 * (i_145_ - i_142_);
		class296_sub45_sub1.anInt6261 += class296_sub45_sub1.anInt6259 * (i_145_ - i_142_);
		i_145_ -= 3;
		while (i_142_ < i_145_) {
			is_141_[i_142_++] += is[i--] * i_143_;
			i_143_ += i_144_;
			is_141_[i_142_++] += is[i--] * i_143_;
			i_143_ += i_144_;
			is_141_[i_142_++] += is[i--] * i_143_;
			i_143_ += i_144_;
			is_141_[i_142_++] += is[i--] * i_143_;
			i_143_ += i_144_;
		}
		i_145_ += 3;
		while (i_142_ < i_145_) {
			is_141_[i_142_++] += is[i--] * i_143_;
			i_143_ += i_144_;
		}
		class296_sub45_sub1.anInt6265 = i_143_ >> 2;
		class296_sub45_sub1.anInt6269 = i << 8;
		return i_142_;
	}

	private static final int method2969(byte[] is, int[] is_148_, int i, int i_149_, int i_150_, int i_151_, int i_152_, int i_153_, int i_154_, Class296_Sub45_Sub1 class296_sub45_sub1) {
		i >>= 8;
		i_154_ >>= 8;
		i_150_ <<= 2;
		i_151_ <<= 2;
		if ((i_152_ = i_149_ + i_154_ - i) > i_153_)
			i_152_ = i_153_;
		class296_sub45_sub1.anInt6260 += class296_sub45_sub1.anInt6270 * (i_152_ - i_149_);
		class296_sub45_sub1.anInt6261 += class296_sub45_sub1.anInt6259 * (i_152_ - i_149_);
		i_152_ -= 3;
		while (i_149_ < i_152_) {
			is_148_[i_149_++] += is[i++] * i_150_;
			i_150_ += i_151_;
			is_148_[i_149_++] += is[i++] * i_150_;
			i_150_ += i_151_;
			is_148_[i_149_++] += is[i++] * i_150_;
			i_150_ += i_151_;
			is_148_[i_149_++] += is[i++] * i_150_;
			i_150_ += i_151_;
		}
		i_152_ += 3;
		while (i_149_ < i_152_) {
			is_148_[i_149_++] += is[i++] * i_150_;
			i_150_ += i_151_;
		}
		class296_sub45_sub1.anInt6265 = i_150_ >> 2;
		class296_sub45_sub1.anInt6269 = i << 8;
		return i_149_;
	}

	static final Class296_Sub45_Sub1 method2970(Class296_Sub19_Sub1 class296_sub19_sub1, int i, int i_155_, int i_156_) {
		if (class296_sub19_sub1.aByteArray6051 == null || class296_sub19_sub1.aByteArray6051.length == 0)
			return null;
		return new Class296_Sub45_Sub1(class296_sub19_sub1, i, i_155_, i_156_);
	}

	final boolean method2971() {
		if (anInt6263 == 0)
			return false;
		return true;
	}

	private static final int method2972(int i, int i_157_, byte[] is, int[] is_158_, int i_159_, int i_160_, int i_161_, int i_162_, int i_163_, int i_164_, int i_165_, int i_166_, int i_167_, Class296_Sub45_Sub1 class296_sub45_sub1, int i_168_, int i_169_) {
		class296_sub45_sub1.anInt6265 -= class296_sub45_sub1.anInt6268 * i_160_;
		if (i_168_ == 0 || ((i_165_ = i_160_ + (i_167_ - i_159_ + i_168_ - 257) / i_168_) > i_166_))
			i_165_ = i_166_;
		i_160_ <<= 1;
		i_165_ <<= 1;
		while (i_160_ < i_165_) {
			i_157_ = i_159_ >> 8;
			i = is[i_157_];
			i = (i << 8) + (is[i_157_ + 1] - i) * (i_159_ & 0xff);
			is_158_[i_160_++] += i * i_161_ >> 6;
			i_161_ += i_163_;
			is_158_[i_160_++] += i * i_162_ >> 6;
			i_162_ += i_164_;
			i_159_ += i_168_;
		}
		if (i_168_ == 0 || (i_165_ = (i_160_ >> 1) + (i_167_ - i_159_ + i_168_ - 1) / i_168_) > i_166_)
			i_165_ = i_166_;
		i_165_ <<= 1;
		i_157_ = i_169_;
		while (i_160_ < i_165_) {
			i = is[i_159_ >> 8];
			i = (i << 8) + (i_157_ - i) * (i_159_ & 0xff);
			is_158_[i_160_++] += i * i_161_ >> 6;
			i_161_ += i_163_;
			is_158_[i_160_++] += i * i_162_ >> 6;
			i_162_ += i_164_;
			i_159_ += i_168_;
		}
		i_160_ >>= 1;
		class296_sub45_sub1.anInt6265 += class296_sub45_sub1.anInt6268 * i_160_;
		class296_sub45_sub1.anInt6260 = i_161_;
		class296_sub45_sub1.anInt6261 = i_162_;
		class296_sub45_sub1.anInt6269 = i_159_;
		return i_160_;
	}

	private Class296_Sub45_Sub1(Class296_Sub19_Sub1 class296_sub19_sub1, int i, int i_170_, int i_171_) {
		aClass296_Sub19_4951 = class296_sub19_sub1;
		anInt6258 = class296_sub19_sub1.anInt6049;
		anInt6267 = class296_sub19_sub1.anInt6050;
		aBoolean6262 = class296_sub19_sub1.aBoolean6053;
		anInt6266 = i;
		anInt6264 = i_170_;
		anInt6272 = i_171_;
		anInt6269 = 0;
		method2945();
	}

	private static final int method2973(int i, byte[] is, int[] is_172_, int i_173_, int i_174_, int i_175_, int i_176_, int i_177_, int i_178_, int i_179_, Class296_Sub45_Sub1 class296_sub45_sub1) {
		i_173_ >>= 8;
		i_179_ >>= 8;
		i_175_ <<= 2;
		i_176_ <<= 2;
		if ((i_177_ = i_174_ + i_173_ - (i_179_ - 1)) > i_178_)
			i_177_ = i_178_;
		i_174_ <<= 1;
		i_177_ <<= 1;
		i_177_ -= 6;
		while (i_174_ < i_177_) {
			i = is[i_173_--];
			is_172_[i_174_++] += i * i_175_;
			is_172_[i_174_++] += i * i_176_;
			i = is[i_173_--];
			is_172_[i_174_++] += i * i_175_;
			is_172_[i_174_++] += i * i_176_;
			i = is[i_173_--];
			is_172_[i_174_++] += i * i_175_;
			is_172_[i_174_++] += i * i_176_;
			i = is[i_173_--];
			is_172_[i_174_++] += i * i_175_;
			is_172_[i_174_++] += i * i_176_;
		}
		i_177_ += 6;
		while (i_174_ < i_177_) {
			i = is[i_173_--];
			is_172_[i_174_++] += i * i_175_;
			is_172_[i_174_++] += i * i_176_;
		}
		class296_sub45_sub1.anInt6269 = i_173_ << 8;
		return i_174_ >> 1;
	}

	final Class296_Sub45 method2932() {
		return null;
	}

	final synchronized int method2974() {
		if (anInt6266 < 0)
			return -anInt6266;
		return anInt6266;
	}

	private static final int method2975(int i, int i_180_, byte[] is, int[] is_181_, int i_182_, int i_183_, int i_184_, int i_185_, int i_186_, int i_187_, int i_188_, Class296_Sub45_Sub1 class296_sub45_sub1, int i_189_, int i_190_) {
		if (i_189_ == 0 || ((i_186_ = i_183_ + (i_188_ + 256 - i_182_ + i_189_) / i_189_) > i_187_))
			i_186_ = i_187_;
		i_183_ <<= 1;
		i_186_ <<= 1;
		while (i_183_ < i_186_) {
			i_180_ = i_182_ >> 8;
			i = is[i_180_ - 1];
			i = (i << 8) + (is[i_180_] - i) * (i_182_ & 0xff);
			is_181_[i_183_++] += i * i_184_ >> 6;
			is_181_[i_183_++] += i * i_185_ >> 6;
			i_182_ += i_189_;
		}
		if (i_189_ == 0 || ((i_186_ = (i_183_ >> 1) + (i_188_ - i_182_ + i_189_) / i_189_) > i_187_))
			i_186_ = i_187_;
		i_186_ <<= 1;
		i_180_ = i_190_;
		while (i_183_ < i_186_) {
			i = (i_180_ << 8) + (is[i_182_ >> 8] - i_180_) * (i_182_ & 0xff);
			is_181_[i_183_++] += i * i_184_ >> 6;
			is_181_[i_183_++] += i * i_185_ >> 6;
			i_182_ += i_189_;
		}
		class296_sub45_sub1.anInt6269 = i_182_;
		return i_183_ >> 1;
	}
}
