package net.zaros.client;

/* Class16_Sub3_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class16_Sub3_Sub1 extends Class16_Sub3 {
	static Class209 aClass209_5805 = new Class209(51);
	static float aFloat5806;
	static ConfigsRegister configsRegister;

	static final void method251(ha var_ha, int i) {
		Class41_Sub2.aClass55_3742 = Class296_Sub51_Sub30.method3168(var_ha, (byte) -96, true, Class368_Sub15.anInt5518, true);
		Class154_Sub1.aClass92_4292 = Class157.method1587(i + 4032, var_ha, Class368_Sub15.anInt5518);
		Class205_Sub1.aClass55_5642 = Class296_Sub51_Sub30.method3168(var_ha, (byte) -117, true, Mesh.anInt1364, true);
		Class123_Sub1_Sub1.aClass92_5814 = Class157.method1587(i ^ 0xfdb, var_ha, Mesh.anInt1364);
		if (i == -4096) {
			Class49.aClass55_461 = Class296_Sub51_Sub30.method3168(var_ha, (byte) -90, true, Class190.anInt1936, true);
			Class304.aClass92_2729 = Class157.method1587(-66, var_ha, Class190.anInt1936);
		}
	}

	public static void method252(int i) {
		if (i >= 70) {
			configsRegister = null;
			aClass209_5805 = null;
		}
	}

	static final void method253(int i, int i_0_, int i_1_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_0_][i_1_];
		if (class247 != null) {
			Class230.method2106(class247.aClass338_Sub3_Sub4_2348);
			Class230.method2106(class247.aClass338_Sub3_Sub4_2343);
			if (class247.aClass338_Sub3_Sub4_2348 != null)
				class247.aClass338_Sub3_Sub4_2348 = null;
			if (class247.aClass338_Sub3_Sub4_2343 != null)
				class247.aClass338_Sub3_Sub4_2343 = null;
		}
	}
}
