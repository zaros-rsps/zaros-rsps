package net.zaros.client;

/* Class296 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.File;

public class Node {
	public static int[] anIntArray2693 = new int[8];
	public Node prev;
	public Node next;
	public static OutgoingPacket aClass311_2696 = new OutgoingPacket(91, 3);
	public long uid;

	static final Class247 method2428(int i, int i_0_, int i_1_) {
		if (Class338_Sub2.aClass247ArrayArrayArray5195[i][i_0_][i_1_] == null) {
			boolean bool = ((Class338_Sub2.aClass247ArrayArrayArray5195[0][i_0_][i_1_] != null)
					&& (Class338_Sub2.aClass247ArrayArrayArray5195[0][i_0_][i_1_].aClass247_2338) != null);
			if (bool && i >= Class368_Sub9.anInt5477 - 1)
				return null;
			Class320.method3340(i, i_0_, i_1_);
		}
		return Class338_Sub2.aClass247ArrayArrayArray5195[i][i_0_][i_1_];
	}

	public void unlink() {
		if (prev != null) {
			prev.next = next;
			next.prev = prev;
			next = null;
			prev = null;
		}
	}

	final boolean isLinked(byte i) {
		if (prev == null)
			return false;
		if (i >= -49)
			return true;
		return true;
	}

	static final byte[] method2431(int i, File file, int i_2_) {
		if (i != 1)
			aClass311_2696 = null;
		try {
			byte[] is = new byte[i_2_];
			Class41_Sub9.method429(i_2_, file, is, false);
			return is;
		} catch (java.io.IOException ioexception) {
			return null;
		}
	}

	public static void method2432(boolean bool) {
		aClass311_2696 = null;
		anIntArray2693 = null;
		if (bool != true)
			method2432(true);
	}

	public Node() {
		/* empty */
	}
}
