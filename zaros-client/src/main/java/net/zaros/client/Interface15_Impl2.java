package net.zaros.client;
/* Interface15_Impl2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jaclib.memory.Buffer;
import jaclib.memory.Source;

interface Interface15_Impl2 extends Interface15 {
	public boolean method46(int i, int i_0_, int i_1_);

	public Buffer method47(boolean bool, int i);

	public void method31(byte i);

	public boolean method48(boolean bool, Source source, int i, int i_2_);

	public boolean method49(int i);
}
