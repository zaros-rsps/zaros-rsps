package net.zaros.client;
import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;

final class Class296_Sub15_Sub4 extends Class296_Sub15 {
	private float aFloat6027;
	static Class22 aClass22_6028;
	private int anInt6029;
	private String aString6030;
	static NodeDeque aClass155_6031 = new NodeDeque();
	private String aString6032;
	private int anInt6033;
	private String aString6034;
	private float aFloat6035;
	static Js5 aClass138_6036;
	static int anInt6037 = 1338;
	static int anInt6038 = 0;

	Class296_Sub15_Sub4(OggStreamState oggstreamstate) {
		super(oggstreamstate);
	}

	final String method2544(int i) {
		int i_0_ = 49 / ((-17 - i) / 51);
		return aString6030;
	}

	final float method2545(boolean bool) {
		if (bool != true)
			return 1.2944983F;
		return aFloat6027;
	}

	final void method2517(OggPacket oggpacket, int i) {
		if (anInt4670 <= 0 || "SUB".equals(aString6032)) {
			Packet class296_sub17 = new Packet(oggpacket.getData());
			if (i != 16)
				method2548(60);
			int i_1_ = class296_sub17.g1();
			do {
				if (anInt4670 > 8) {
					if (i_1_ == 0) {
						long l = class296_sub17.method2581();
						long l_2_ = class296_sub17.method2581();
						long l_3_ = class296_sub17.method2581();
						if (l < 0L || l_2_ < 0L || l_3_ < 0L || l < l_3_)
							throw new IllegalStateException();
						aFloat6035 = ((float) ((l + l_2_) * (long) anInt6029) / (float) anInt6033);
						aFloat6027 = ((float) (l * (long) anInt6029) / (float) anInt6033);
						int i_4_ = class296_sub17.method2574();
						if (i_4_ < 0 || (-class296_sub17.pos + class296_sub17.data.length) < i_4_)
							throw new IllegalStateException();
						aString6034 = Class211.method2012(192, (class296_sub17.data), i_4_, class296_sub17.pos);
					}
					if ((i_1_ | 0x80) != 0)
						break;
				} else {
					if ((i_1_ | 0x80) == 0)
						throw new IllegalStateException();
					if (anInt4670 != 0)
						break;
					class296_sub17.pos += 23;
					anInt6033 = class296_sub17.method2574();
					anInt6029 = class296_sub17.method2574();
					if (anInt6033 == 0 || anInt6029 == 0)
						throw new IllegalStateException();
					Packet class296_sub17_5_ = new Packet(16);
					class296_sub17.readBytes(class296_sub17_5_.data, 0, 16);
					aString6030 = class296_sub17_5_.gstr();
					class296_sub17_5_.pos = 0;
					class296_sub17.readBytes(class296_sub17_5_.data, 0, 16);
					aString6032 = class296_sub17_5_.gstr();
				}
				break;
			} while (false);
		}
	}

	final float method2546(int i) {
		if (i != 0)
			method2544(-43);
		return aFloat6035;
	}

	final String method2547(byte i) {
		if (i != -21)
			anInt6033 = -22;
		return aString6034;
	}

	public static void method2548(int i) {
		aClass155_6031 = null;
		aClass22_6028 = null;
		int i_6_ = -51 / ((-68 - i) / 55);
		aClass138_6036 = null;
	}

	final void method2519(int i) {
		if (i != 16)
			aString6032 = null;
	}
}
