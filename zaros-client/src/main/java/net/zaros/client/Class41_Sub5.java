package net.zaros.client;

/* Class41_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub5 extends Class41 {
	static int anInt3751 = -1;
	static boolean aBoolean3752;
	static int anInt3753;
	static int anInt3754 = -1;
	static Class209 aClass209_3755;
	static int[] anIntArray3756;
	static int anInt3757;

	public static void method409(int i) {
		if (i != -1) {
			anInt3757 = 58;
		}
		anIntArray3756 = null;
		aClass209_3755 = null;
	}

	Class41_Sub5(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	@Override
	final int method383(byte i) {
		if (i != 110) {
			method383((byte) 92);
		}
		return 0;
	}

	static final int method410(Player class338_sub3_sub1_sub3_sub1, int i) {
		if (i != 11736) {
			method412((byte) 63);
		}
		int i_0_ = class338_sub3_sub1_sub3_sub1.anInt6894;
		Class280 class280 = class338_sub3_sub1_sub3_sub1.method3516(false);
		int i_1_ = class338_sub3_sub1_sub3_sub1.aClass44_6777.method557((byte) -88);
		if (i_1_ != -1 && !class338_sub3_sub1_sub3_sub1.aBoolean6783) {
			if (class280.anInt2554 == i_1_ || class280.anInt2562 == i_1_ || class280.anInt2565 == i_1_ || class280.anInt2566 == i_1_) {
				i_0_ = class338_sub3_sub1_sub3_sub1.anInt6875;
			} else if (i_1_ == class280.anInt2564 || class280.anInt2598 == i_1_ || class280.anInt2583 == i_1_ || i_1_ == class280.anInt2577) {
				i_0_ = class338_sub3_sub1_sub3_sub1.anInt6890;
			}
		} else {
			i_0_ = class338_sub3_sub1_sub3_sub1.anInt6869;
		}
		return i_0_;
	}

	static final float method411(int i, float f, float f_2_, float f_3_, float f_4_, float f_5_, boolean bool, float f_6_) {
		float f_7_ = 0.0F;
		float f_8_ = -f_6_ + f_5_;
		float f_9_ = f - f_2_;
		float f_10_ = f_4_ - f_3_;
		float f_11_ = 0.0F;
		float f_12_ = 0.0F;
		if (bool) {
			anInt3754 = -8;
		}
		float f_13_ = 0.0F;
		while (f_7_ < 1.1F) {
			float f_14_ = f_8_ * f_7_ + f_6_;
			float f_15_ = f_2_ + f_9_ * f_7_;
			float f_16_ = f_3_ + f_10_ * f_7_;
			int i_17_ = (int) f_14_ >> 9;
			int i_18_ = (int) f_16_ >> 9;
			if (i_17_ > 0 && i_18_ > 0 && Class198.currentMapSizeX > i_17_ && i_18_ < Class296_Sub38.currentMapSizeY) {
				int i_19_ = Class296_Sub51_Sub11.localPlayer.z;
				if (i_19_ < 3 && (Class41_Sub18.aByteArrayArrayArray3786[1][i_17_][i_18_] & 0x2) != 0) {
					i_19_++;
				}
				int i_20_ = Class244.aSArray2320[i_19_].method3349(0, (int) f_16_, (int) f_14_);
				if (f_15_ > i_20_) {
					if (i >= 2) {
						return f_7_ + -0.1F + method411(i - 1, f_15_, f_12_, f_13_, f_16_, f_14_, bool, f_11_) * 0.1F;
					}
					return f_7_;
				}
			}
			f_11_ = f_14_;
			f_7_ += 0.1F;
			f_13_ = f_16_;
			f_12_ = f_15_;
		}
		return -1.0F;
	}

	@Override
	final void method386(int i) {
		if (i != 2) {
			anInt3754 = 104;
		}
		if (anInt389 != 1 && anInt389 != 0) {
			anInt389 = method383((byte) 110);
		}
	}

	static final boolean method412(byte i) {
		try {
			Class134 class134 = new Class134();
			byte[] is = class134.method1401(Class275.aByteArray2525, 11147);
			Class205_Sub4.method1987(i ^ 0x7b, is);
			return true;
		} catch (Throwable exception) {
			return false;
		}
	}

	@Override
	final int method380(int i, byte i_21_) {
		if (i_21_ != 41) {
			method386(-89);
		}
		return 1;
	}

	Class41_Sub5(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	static final boolean method413(int i, int i_22_, int i_23_) {
		if (i_23_ != 3709) {
			aBoolean3752 = false;
		}
		if ((i_22_ & 0x20) == 0) {
			return false;
		}
		return true;
	}

	@Override
	final void method381(int i, byte i_24_) {
		anInt389 = i;
		if (i_24_ != -110) {
			anInt3753 = 101;
		}
	}

	final int method414(int i) {
		if (i <= 114) {
			method413(-66, 126, 92);
		}
		return anInt389;
	}

	static {
		aBoolean3752 = false;
		aClass209_3755 = new Class209(52);
		anIntArray3756 = new int[4096];
	}
}
