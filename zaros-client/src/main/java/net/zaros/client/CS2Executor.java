package net.zaros.client;
/* Class150 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;

import net.zaros.client.configs.objtype.ObjType;
import net.zaros.client.logic.clans.channel.ClanChannel;
import net.zaros.client.logic.clans.settings.ClanSettings;

final class CS2Executor {

	private static String[] stringStack = new String[1000];

	private static int[] arrayLengths = new int[5];

	private static int stringStackSize = 0;

	private static long[] longLocals;

	private static InterfaceComponent aClass51_1528;

	private static String[] stringLocals;

	static int anInt1530;

	static AdvancedMemoryCache aClass113_1531;

	private static int[][] arrays;

	private static int[] intStack;

	private static int longStackSize = 0;

	private static CS2Stack[] callStack;

	private static Class270 aClass270_1536;

	private static InterfaceComponent aClass51_1537;

	private static int callStackSize;

	private static int[] anIntArray1539;

	private static ClanSettings aClass371_1540;

	private static int[] intLocals;

	static int anInt1542;

	private static long[] longStack;

	private static int intStackSize;

	private static ClanChannel aClass296_Sub54_1545;

	private static int anInt1546;

	static boolean aBoolean1547;

	static String aString1548;

	static final void method1520(Class81 class81, int i, int i_0_) {
		CS2Script class296_sub39_sub3 = StaticMethods.method1608(-1, i_0_, i, class81);
		if (class296_sub39_sub3 != null) {
			intLocals = new int[class296_sub39_sub3.intLocalsCount];
			stringLocals = new String[class296_sub39_sub3.stringLocalsCount];
			if (class296_sub39_sub3.aClass81_6135 == Class338_Sub3_Sub2_Sub1.aClass81_6849 || class296_sub39_sub3.aClass81_6135 == LookupTable.aClass81_51 || class296_sub39_sub3.aClass81_6135 == Class384.aClass81_3253) {
				int i_1_ = 0;
				int i_2_ = 0;
				if (Class359.aClass51_3092 != null) {
					i_1_ = Class359.aClass51_3092.anInt614;
					i_2_ = Class359.aClass51_3092.anInt605;
				}
				intLocals[0] = Class84.aClass189_924.method1895((byte) -55) - i_1_;
				intLocals[1] = Class84.aClass189_924.method1897(0) - i_2_;
			}
			execute(class296_sub39_sub3, 200000);
		}
	}

	static final void method1521(int i) {
		if (i != -1 && InterfaceComponent.loadInterface(i)) {
			InterfaceComponent[] class51s = Class192.openedInterfaceComponents[i];
			for (InterfaceComponent class51 : class51s) {
				if (class51.anObjectArray492 != null) {
					CS2Call class296_sub46 = new CS2Call();
					class296_sub46.callerInterface = class51;
					class296_sub46.callArgs = class51.anObjectArray492;
					runCS2(class296_sub46, 2000000);
				}
			}
		}
	}

	private static final int method1522(int i) {
		Class7 class7 = HashTable.aClass149_2456.method1515(i, -15156);
		if (class7 == null) {
			throw new RuntimeException("sr-c112");
		}
		Integer integer = aClass371_1540.getVarInt(Class296_Sub50.game.anInt340 << 16 | i);
		if (integer == null) {
			if (class7.aChar90 == 'i' || class7.aChar90 == '1') {
				return 0;
			}
			return -1;
		}
		return integer.intValue();
	}

	private static final void parseHigh(int i, boolean bool) {
		// if (i != 5420 && i != 5617 && i != 5609) {
		// System.out.println("CS2Executor.parseHigh");
		// System.out.println("i = [" + i + "], bool = [" + bool + "]");
		// }
		if (i < 5100) {
			if (i == 5000) {
				intStack[intStackSize++] = StaticMethods.anInt5970;
				return;
			}
			if (i == 5001) {
				intStackSize -= 3;
				StaticMethods.anInt5970 = intStack[intStackSize];
				Class368_Sub15.aClass213_5522 = aa_Sub3.method159(intStack[intStackSize + 1], -8);
				if (Class368_Sub15.aClass213_5522 == null) {
					Class368_Sub15.aClass213_5522 = Class296_Sub51_Sub31.aClass213_6503;
				}
				Class296_Sub51_Sub18.anInt6436 = intStack[intStackSize + 2];
				Connection class204 = Class296_Sub51_Sub13.method3111(true);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 120, Class14.aClass311_170);
				class296_sub1.out.p1(StaticMethods.anInt5970);
				class296_sub1.out.p1(Class368_Sub15.aClass213_5522.anInt2111);
				class296_sub1.out.p1(Class296_Sub51_Sub18.anInt6436);
				class204.sendPacket(class296_sub1, (byte) 119);
				return;
			}
			if (i == 5002) {
				stringStackSize -= 2;
				String string = stringStack[stringStackSize];
				String string_4_ = stringStack[stringStackSize + 1];
				intStackSize -= 2;
				int i_5_ = intStack[intStackSize];
				int i_6_ = intStack[intStackSize + 1];
				if (string_4_ == null) {
					string_4_ = "";
				}
				if (string_4_.length() > 80) {
					string_4_ = string_4_.substring(0, 80);
				}
				Connection class204 = Class296_Sub51_Sub13.method3111(true);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 116, Class84.aClass311_921);
				class296_sub1.out.p1(Class117.method1015((byte) -43, string) + 2 + Class117.method1015((byte) -36, string_4_));
				class296_sub1.out.writeString(string);
				class296_sub1.out.p1(i_5_ - 1);
				class296_sub1.out.p1(i_6_);
				class296_sub1.out.writeString(string_4_);
				class204.sendPacket(class296_sub1, (byte) 119);
				return;
			}
			if (i == 5003) {
				int i_7_ = intStack[--intStackSize];
				Class228 class228 = Class242.method2169(-121, i_7_);
				String string = "";
				if (class228 != null && class228.aString2188 != null) {
					string = class228.aString2188;
				}
				stringStack[stringStackSize++] = string;
				return;
			}
			if (i == 5004) {
				int i_8_ = intStack[--intStackSize];
				Class228 class228 = Class242.method2169(-112, i_8_);
				int i_9_ = -1;
				if (class228 != null) {
					i_9_ = class228.anInt2192;
				}
				intStack[intStackSize++] = i_9_;
				return;
			}
			if (i == 5005) {
				if (Class368_Sub15.aClass213_5522 == null) {
					intStack[intStackSize++] = -1;
				} else {
					intStack[intStackSize++] = Class368_Sub15.aClass213_5522.anInt2111;
					return;
				}
				return;
			}
			if (i == 5006) {
				int i_10_ = intStack[--intStackSize];
				Connection class204 = Class296_Sub51_Sub13.method3111(true);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 96, Class123_Sub2.aClass311_3916);
				class296_sub1.out.p1(i_10_);
				class204.sendPacket(class296_sub1, (byte) 119);
				return;
			}
			if (i == 5008) {
				String string = stringStack[--stringStackSize];
				method1528(string, i);
				return;
			}
			if (i == 5009) {
				stringStackSize -= 2;
				String string = stringStack[stringStackSize];
				String string_11_ = stringStack[stringStackSize + 1];
				if (Class338_Sub3_Sub5.rights != 0 || (!Class296_Sub40.somethingWithIgnore0 || Class41_Sub17.somethingWithIgnore1) && !Class123.somethingWithIgnore2) {
					Connection class204 = Class296_Sub51_Sub13.method3111(true);
					Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 114, InterfaceComponentSettings.aClass311_4823);
					class296_sub1.out.p1(0);
					int i_12_ = class296_sub1.out.pos;
					class296_sub1.out.writeString(string);
					Class86.method826(string_11_, 0, class296_sub1.out);
					class296_sub1.out.method2604(class296_sub1.out.pos - i_12_);
					class204.sendPacket(class296_sub1, (byte) 119);
					return;
				}
				return;
			}
			if (i == 5010) {
				int i_13_ = intStack[--intStackSize];
				Class228 class228 = Class242.method2169(4, i_13_);
				String string = "";
				if (class228 != null && class228.aString2194 != null) {
					string = class228.aString2194;
				}
				stringStack[stringStackSize++] = string;
				return;
			}
			if (i == 5011) {
				int i_14_ = intStack[--intStackSize];
				Class228 class228 = Class242.method2169(89, i_14_);
				String string = "";
				if (class228 != null && class228.aString2198 != null) {
					string = class228.aString2198;
				}
				stringStack[stringStackSize++] = string;
				return;
			}
			if (i == 5012) {
				int i_15_ = intStack[--intStackSize];
				Class228 class228 = Class242.method2169(-115, i_15_);
				int i_16_ = -1;
				if (class228 != null) {
					i_16_ = class228.anInt2189;
				}
				intStack[intStackSize++] = i_16_;
				return;
			}
			if (i == 5015) {
				String string;
				if (Class296_Sub51_Sub11.localPlayer != null && Class296_Sub51_Sub11.localPlayer.name != null) {
					string = Class296_Sub51_Sub11.localPlayer.getFullName(true);
				} else {
					string = "";
				}
				stringStack[stringStackSize++] = string;
				return;
			}
			if (i == 5016) {
				intStack[intStackSize++] = Class296_Sub51_Sub18.anInt6436;
				return;
			}
			if (i == 5017) {
				intStack[intStackSize++] = Class296_Sub39_Sub14.method2878((byte) 66);
				return;
			}
			if (i == 5018) {
				int i_17_ = intStack[--intStackSize];
				Class228 class228 = Class242.method2169(-10, i_17_);
				int i_18_ = 0;
				if (class228 != null) {
					i_18_ = class228.anInt2190;
				}
				intStack[intStackSize++] = i_18_;
				return;
			}
			if (i == 5019) {
				int i_19_ = intStack[--intStackSize];
				Class228 class228 = Class242.method2169(-123, i_19_);
				String string = "";
				if (class228 != null && class228.aString2196 != null) {
					string = class228.aString2196;
				}
				stringStack[stringStackSize++] = string;
				return;
			}
			if (i == 5020) {
				String string;
				if (Class296_Sub51_Sub11.localPlayer != null && Class296_Sub51_Sub11.localPlayer.name != null) {
					string = Class296_Sub51_Sub11.localPlayer.getName(false);
				} else {
					string = "";
				}
				stringStack[stringStackSize++] = string;
				return;
			}
			if (i == 5023) {
				int i_20_ = intStack[--intStackSize];
				Class228 class228 = Class242.method2169(-76, i_20_);
				int i_21_ = -1;
				if (class228 != null) {
					i_21_ = class228.anInt2199;
				}
				intStack[intStackSize++] = i_21_;
				return;
			}
			if (i == 5024) {
				int i_22_ = intStack[--intStackSize];
				Class228 class228 = Class242.method2169(-108, i_22_);
				int i_23_ = -1;
				if (class228 != null) {
					i_23_ = class228.anInt2195;
				}
				intStack[intStackSize++] = i_23_;
				return;
			}
			if (i == 5025) {
				int i_24_ = intStack[--intStackSize];
				Class228 class228 = Class242.method2169(24, i_24_);
				String string = "";
				if (class228 != null && class228.aString2193 != null) {
					string = class228.aString2193;
				}
				stringStack[stringStackSize++] = string;
				return;
			}
			if (i == 5050) {
				int i_25_ = intStack[--intStackSize];
				stringStack[stringStackSize++] = Class165.aClass52_1689.method639(i_25_, (byte) -106).aString6193;
				return;
			}
			if (i == 5051) {
				int i_26_ = intStack[--intStackSize];
				Class296_Sub39_Sub12 class296_sub39_sub12 = Class165.aClass52_1689.method639(i_26_, (byte) -85);
				if (class296_sub39_sub12.anIntArray6201 == null) {
					intStack[intStackSize++] = 0;
				} else {
					intStack[intStackSize++] = class296_sub39_sub12.anIntArray6201.length;
					return;
				}
				return;
			}
			if (i == 5052) {
				intStackSize -= 2;
				int i_27_ = intStack[intStackSize];
				int i_28_ = intStack[intStackSize + 1];
				Class296_Sub39_Sub12 class296_sub39_sub12 = Class165.aClass52_1689.method639(i_27_, (byte) -103);
				int i_29_ = class296_sub39_sub12.anIntArray6201[i_28_];
				intStack[intStackSize++] = i_29_;
				return;
			}
			if (i == 5053) {
				int i_30_ = intStack[--intStackSize];
				Class296_Sub39_Sub12 class296_sub39_sub12 = Class165.aClass52_1689.method639(i_30_, (byte) -110);
				if (class296_sub39_sub12.anIntArray6196 == null) {
					intStack[intStackSize++] = 0;
				} else {
					intStack[intStackSize++] = class296_sub39_sub12.anIntArray6196.length;
					return;
				}
				return;
			}
			if (i == 5054) {
				intStackSize -= 2;
				int i_31_ = intStack[intStackSize];
				int i_32_ = intStack[intStackSize + 1];
				intStack[intStackSize++] = Class165.aClass52_1689.method639(i_31_, (byte) -109).anIntArray6196[i_32_];
				return;
			}
			if (i == 5055) {
				int i_33_ = intStack[--intStackSize];
				stringStack[stringStackSize++] = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_33_, -127).method2834(0);
				return;
			}
			if (i == 5056) {
				int i_34_ = intStack[--intStackSize];
				Class296_Sub39_Sub10 class296_sub39_sub10 = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_34_, -127);
				if (class296_sub39_sub10.anIntArray6186 == null) {
					intStack[intStackSize++] = 0;
				} else {
					intStack[intStackSize++] = class296_sub39_sub10.anIntArray6186.length;
					return;
				}
				return;
			}
			if (i == 5057) {
				intStackSize -= 2;
				int i_35_ = intStack[intStackSize];
				int i_36_ = intStack[intStackSize + 1];
				intStack[intStackSize++] = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_35_, -128).anIntArray6186[i_36_];
				return;
			}
			if (i == 5058) {
				aClass270_1536 = new Class270();
				aClass270_1536.anInt2506 = intStack[--intStackSize];
				aClass270_1536.aClass296_Sub39_Sub10_2509 = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(aClass270_1536.anInt2506, -125);
				aClass270_1536.anIntArray2508 = new int[aClass270_1536.aClass296_Sub39_Sub10_2509.method2833(-22579)];
				return;
			}
			if (i == 5059) {
				anInt1542++;
				Connection class204 = Class296_Sub51_Sub13.method3111(true);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 123, Class41_Sub4.aClass311_3747);
				class296_sub1.out.p1(0);
				int i_37_ = class296_sub1.out.pos;
				class296_sub1.out.p1(0);
				class296_sub1.out.p2(aClass270_1536.anInt2506);
				aClass270_1536.aClass296_Sub39_Sub10_2509.method2835((byte) -109, aClass270_1536.anIntArray2508, class296_sub1.out);
				class296_sub1.out.method2604(class296_sub1.out.pos - i_37_);
				class204.sendPacket(class296_sub1, (byte) 119);
				return;
			}
			if (i == 5060) {
				String string = stringStack[--stringStackSize];
				Connection class204 = Class296_Sub51_Sub13.method3111(true);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 97, Class368_Sub5_Sub2.aClass311_6599);
				class296_sub1.out.p1(0);
				int i_38_ = class296_sub1.out.pos;
				class296_sub1.out.writeString(string);
				class296_sub1.out.p2(aClass270_1536.anInt2506);
				aClass270_1536.aClass296_Sub39_Sub10_2509.method2835((byte) -109, aClass270_1536.anIntArray2508, class296_sub1.out);
				class296_sub1.out.method2604(class296_sub1.out.pos - i_38_);
				class204.sendPacket(class296_sub1, (byte) 119);
				return;
			}
			if (i == 5061) {
				anInt1542++;
				Connection class204 = Class296_Sub51_Sub13.method3111(true);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 110, Class41_Sub4.aClass311_3747);
				class296_sub1.out.p1(0);
				int i_39_ = class296_sub1.out.pos;
				class296_sub1.out.p1(1);
				class296_sub1.out.p2(aClass270_1536.anInt2506);
				aClass270_1536.aClass296_Sub39_Sub10_2509.method2835((byte) -109, aClass270_1536.anIntArray2508, class296_sub1.out);
				class296_sub1.out.method2604(class296_sub1.out.pos - i_39_);
				class204.sendPacket(class296_sub1, (byte) 119);
				return;
			}
			if (i == 5062) {
				intStackSize -= 2;
				int i_40_ = intStack[intStackSize];
				int i_41_ = intStack[intStackSize + 1];
				intStack[intStackSize++] = Class165.aClass52_1689.method639(i_40_, (byte) -81).aCharArray6194[i_41_];
				return;
			}
			if (i == 5063) {
				intStackSize -= 2;
				int i_42_ = intStack[intStackSize];
				int i_43_ = intStack[intStackSize + 1];
				intStack[intStackSize++] = Class165.aClass52_1689.method639(i_42_, (byte) -99).aCharArray6202[i_43_];
				return;
			}
			if (i == 5064) {
				intStackSize -= 2;
				int i_44_ = intStack[intStackSize];
				int i_45_ = intStack[intStackSize + 1];
				if (i_45_ == -1) {
					intStack[intStackSize++] = -1;
				} else {
					intStack[intStackSize++] = Class165.aClass52_1689.method639(i_44_, (byte) -63).method2851((byte) -56, (char) i_45_);
					return;
				}
				return;
			}
			if (i == 5065) {
				intStackSize -= 2;
				int i_46_ = intStack[intStackSize];
				int i_47_ = intStack[intStackSize + 1];
				if (i_47_ == -1) {
					intStack[intStackSize++] = -1;
				} else {
					intStack[intStackSize++] = Class165.aClass52_1689.method639(i_46_, (byte) -91).method2846(15118, (char) i_47_);
					return;
				}
				return;
			}
			if (i == 5066) {
				int i_48_ = intStack[--intStackSize];
				intStack[intStackSize++] = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_48_, -126).method2833(-22579);
				return;
			}
			if (i == 5067) {
				intStackSize -= 2;
				int i_49_ = intStack[intStackSize];
				int i_50_ = intStack[intStackSize + 1];
				int i_51_ = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_49_, -126).method2841(i_50_, (byte) -8).anInt1675;
				intStack[intStackSize++] = i_51_;
				return;
			}
			if (i == 5068) {
				intStackSize -= 2;
				int i_52_ = intStack[intStackSize];
				int i_53_ = intStack[intStackSize + 1];
				aClass270_1536.anIntArray2508[i_52_] = i_53_;
				return;
			}
			if (i == 5069) {
				intStackSize -= 2;
				int i_54_ = intStack[intStackSize];
				int i_55_ = intStack[intStackSize + 1];
				aClass270_1536.anIntArray2508[i_54_] = i_55_;
				return;
			}
			if (i == 5070) {
				intStackSize -= 3;
				int i_56_ = intStack[intStackSize];
				int i_57_ = intStack[intStackSize + 1];
				int i_58_ = intStack[intStackSize + 2];
				Class296_Sub39_Sub10 class296_sub39_sub10 = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_56_, -128);
				if (class296_sub39_sub10.method2841(i_57_, (byte) -8).anInt1675 != 0) {
					throw new RuntimeException("bad command");
				}
				intStack[intStackSize++] = class296_sub39_sub10.method2832(i_58_, i_57_, false);
				return;
			}
			if (i == 5071) {
				String string = stringStack[--stringStackSize];
				boolean bool_59_ = intStack[--intStackSize] == 1;
				Class335.method3425(16, bool_59_, string);
				intStack[intStackSize++] = Class342.anInt2989;
				return;
			}
			if (i == 5072) {
				if (StaticMethods.aShortArray5932 == null || Class203.anInt3569 >= Class342.anInt2989) {
					intStack[intStackSize++] = -1;
				} else {
					intStack[intStackSize++] = StaticMethods.aShortArray5932[Class203.anInt3569++] & 0xffff;
					return;
				}
				return;
			}
			if (i == 5073) {
				Class203.anInt3569 = 0;
				return;
			}
			if (i == 5074) {
				Connection class204 = Class296_Sub51_Sub13.method3111(true);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 90, Class41_Sub4.aClass311_3747);
				class296_sub1.out.p1(0);
				int i_60_ = class296_sub1.out.pos;
				class296_sub1.out.p1(2);
				class296_sub1.out.p2(aClass270_1536.anInt2506);
				aClass270_1536.aClass296_Sub39_Sub10_2509.method2835((byte) -109, aClass270_1536.anIntArray2508, class296_sub1.out);
				class296_sub1.out.method2604(class296_sub1.out.pos - i_60_);
				class204.sendPacket(class296_sub1, (byte) 119);
				return;
			}
			if (i == 5075) {
				Connection class204 = Class296_Sub51_Sub13.method3111(true);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 123, Class41_Sub4.aClass311_3747);
				class296_sub1.out.p1(0);
				int i_61_ = class296_sub1.out.pos;
				class296_sub1.out.p1(3);
				class296_sub1.out.p2(aClass270_1536.anInt2506);
				aClass270_1536.aClass296_Sub39_Sub10_2509.method2835((byte) -109, aClass270_1536.anIntArray2508, class296_sub1.out);
				class296_sub1.out.method2604(class296_sub1.out.pos - i_61_);
				class204.sendPacket(class296_sub1, (byte) 119);
				return;
			}
		} else if (i < 5200) {
			if (i == 5100) {
				if (Class2.aClass166_66.method1637(86, 121)) {
					intStack[intStackSize++] = 1;
				} else {
					intStack[intStackSize++] = 0;
					return;
				}
				return;
			}
			if (i == 5101) {
				if (Class2.aClass166_66.method1637(82, 119)) {
					intStack[intStackSize++] = 1;
				} else {
					intStack[intStackSize++] = 0;
					return;
				}
				return;
			}
			if (i == 5102) {
				if (Class2.aClass166_66.method1637(81, 70)) {
					intStack[intStackSize++] = 1;
				} else {
					intStack[intStackSize++] = 0;
					return;
				}
				return;
			}
		} else if (i < 5300) {
			if (i == 5200) {
				Class296_Sub15_Sub1.method2526((byte) 79, intStack[--intStackSize]);
				return;
			}
			if (i == 5201) {
				intStack[intStackSize++] = ReferenceTable.method834((byte) 97);
				return;
			}
			if (i == 5205) {
				Class368_Sub13.method3848(-1, intStack[--intStackSize], -1, true, false);
				return;
			}
			if (i == 5206) {
				int i_62_ = intStack[--intStackSize];
				Class296_Sub39_Sub14 class296_sub39_sub14 = Class106.method936(i_62_ >> 14 & 0x3fff, i_62_ & 0x3fff);
				if (class296_sub39_sub14 == null) {
					intStack[intStackSize++] = -1;
				} else {
					intStack[intStackSize++] = class296_sub39_sub14.anInt6214;
					return;
				}
				return;
			}
			if (i == 5207) {
				Class296_Sub39_Sub14 class296_sub39_sub14 = Class106.method923(intStack[--intStackSize]);
				if (class296_sub39_sub14 == null || class296_sub39_sub14.aString6213 == null) {
					stringStack[stringStackSize++] = "";
				} else {
					stringStack[stringStackSize++] = class296_sub39_sub14.aString6213;
					return;
				}
				return;
			}
			if (i == 5208) {
				intStack[intStackSize++] = Class368_Sub5.anInt5450;
				intStack[intStackSize++] = Class269.anInt2503;
				return;
			}
			if (i == 5209) {
				intStack[intStackSize++] = Class69.anInt3688 + Class106.anInt1111;
				intStack[intStackSize++] = Class219_Sub1.anInt4569 + Class106.anInt1117;
				return;
			}
			if (i == 5210) {
				int i_63_ = intStack[--intStackSize];
				Class296_Sub39_Sub14 class296_sub39_sub14 = Class106.method923(i_63_);
				if (class296_sub39_sub14 == null) {
					intStack[intStackSize++] = 0;
					intStack[intStackSize++] = 0;
				} else {
					intStack[intStackSize++] = class296_sub39_sub14.anInt6222 >> 14 & 0x3fff;
					intStack[intStackSize++] = class296_sub39_sub14.anInt6222 & 0x3fff;
					return;
				}
				return;
			}
			if (i == 5211) {
				int i_64_ = intStack[--intStackSize];
				Class296_Sub39_Sub14 class296_sub39_sub14 = Class106.method923(i_64_);
				if (class296_sub39_sub14 == null) {
					intStack[intStackSize++] = 0;
					intStack[intStackSize++] = 0;
				} else {
					intStack[intStackSize++] = class296_sub39_sub14.anInt6215 - class296_sub39_sub14.anInt6221;
					intStack[intStackSize++] = class296_sub39_sub14.anInt6219 - class296_sub39_sub14.anInt6217;
					return;
				}
				return;
			}
			if (i == 5212) {
				Class296_Sub53 class296_sub53 = IncomingPacket.method2111(-73);
				if (class296_sub53 == null) {
					intStack[intStackSize++] = -1;
					intStack[intStackSize++] = -1;
				} else {
					intStack[intStackSize++] = class296_sub53.anInt5046;
					int i_65_ = class296_sub53.anInt5045 << 28 | class296_sub53.anInt5047 + Class106.anInt1111 << 14 | class296_sub53.anInt5042 + Class106.anInt1117;
					intStack[intStackSize++] = i_65_;
					return;
				}
				return;
			}
			if (i == 5213) {
				Class296_Sub53 class296_sub53 = Class368_Sub4.method3819((byte) -78);
				if (class296_sub53 == null) {
					intStack[intStackSize++] = -1;
					intStack[intStackSize++] = -1;
				} else {
					intStack[intStackSize++] = class296_sub53.anInt5046;
					int i_66_ = class296_sub53.anInt5045 << 28 | class296_sub53.anInt5047 + Class106.anInt1111 << 14 | class296_sub53.anInt5042 + Class106.anInt1117;
					intStack[intStackSize++] = i_66_;
					return;
				}
				return;
			}
			if (i == 5214) {
				int i_67_ = intStack[--intStackSize];
				Class296_Sub39_Sub14 class296_sub39_sub14 = Class296_Sub51_Sub37.method3193(-109);
				if (class296_sub39_sub14 != null) {
					boolean bool_68_ = class296_sub39_sub14.method2877(i_67_ & 0x3fff, anIntArray1539, i_67_ >> 14 & 0x3fff, (byte) -57, i_67_ >> 28 & 0x3);
					if (bool_68_) {
						Class351.method3681(anIntArray1539[2], anIntArray1539[1], (byte) 10);
					}
				}
				return;
			}
			if (i == 5215) {
				intStackSize -= 2;
				int i_69_ = intStack[intStackSize];
				int i_70_ = intStack[intStackSize + 1];
				Queue class145 = Class106.method932(i_69_ >> 14 & 0x3fff, i_69_ & 0x3fff);
				boolean bool_71_ = false;
				for (Class296_Sub39_Sub14 class296_sub39_sub14 = (Class296_Sub39_Sub14) class145.getFront(); class296_sub39_sub14 != null; class296_sub39_sub14 = (Class296_Sub39_Sub14) class145.getNext()) {
					if (class296_sub39_sub14.anInt6214 == i_70_) {
						bool_71_ = true;
						break;
					}
				}
				if (bool_71_) {
					intStack[intStackSize++] = 1;
				} else {
					intStack[intStackSize++] = 0;
					return;
				}
				return;
			}
			if (i == 5218) {
				int i_72_ = intStack[--intStackSize];
				Class296_Sub39_Sub14 class296_sub39_sub14 = Class106.method923(i_72_);
				if (class296_sub39_sub14 == null) {
					intStack[intStackSize++] = -1;
				} else {
					intStack[intStackSize++] = class296_sub39_sub14.anInt6212;
					return;
				}
				return;
			}
			if (i == 5220) {
				intStack[intStackSize++] = Class41_Sub13.anInt3773 == 100 ? 1 : 0;
				return;
			}
			if (i == 5221) {
				int i_73_ = intStack[--intStackSize];
				Class351.method3681(i_73_ & 0x3fff, i_73_ >> 14 & 0x3fff, (byte) 10);
				return;
			}
			if (i == 5222) {
				Class296_Sub39_Sub14 class296_sub39_sub14 = Class296_Sub51_Sub37.method3193(-106);
				if (class296_sub39_sub14 != null) {
					boolean bool_74_ = class296_sub39_sub14.method2875(anIntArray1539, Class219_Sub1.anInt4569 + Class106.anInt1117, (byte) 127, Class69.anInt3688 + Class106.anInt1111);
					if (bool_74_) {
						intStack[intStackSize++] = anIntArray1539[1];
						intStack[intStackSize++] = anIntArray1539[2];
					} else {
						intStack[intStackSize++] = -1;
						intStack[intStackSize++] = -1;
					}
				} else {
					intStack[intStackSize++] = -1;
					intStack[intStackSize++] = -1;
					return;
				}
				return;
			}
			if (i == 5223) {
				intStackSize -= 2;
				int i_75_ = intStack[intStackSize];
				int i_76_ = intStack[intStackSize + 1];
				Class368_Sub13.method3848(i_76_ & 0x3fff, i_75_, i_76_ >> 14 & 0x3fff, true, false);
				return;
			}
			if (i == 5224) {
				int i_77_ = intStack[--intStackSize];
				Class296_Sub39_Sub14 class296_sub39_sub14 = Class296_Sub51_Sub37.method3193(-111);
				if (class296_sub39_sub14 != null) {
					boolean bool_78_ = class296_sub39_sub14.method2877(i_77_ & 0x3fff, anIntArray1539, i_77_ >> 14 & 0x3fff, (byte) -57, i_77_ >> 28 & 0x3);
					if (bool_78_) {
						intStack[intStackSize++] = anIntArray1539[1];
						intStack[intStackSize++] = anIntArray1539[2];
					} else {
						intStack[intStackSize++] = -1;
						intStack[intStackSize++] = -1;
					}
				} else {
					intStack[intStackSize++] = -1;
					intStack[intStackSize++] = -1;
					return;
				}
				return;
			}
			if (i == 5225) {
				int i_79_ = intStack[--intStackSize];
				Class296_Sub39_Sub14 class296_sub39_sub14 = Class296_Sub51_Sub37.method3193(-125);
				if (class296_sub39_sub14 != null) {
					boolean bool_80_ = class296_sub39_sub14.method2875(anIntArray1539, i_79_ & 0x3fff, (byte) 121, i_79_ >> 14 & 0x3fff);
					if (bool_80_) {
						intStack[intStackSize++] = anIntArray1539[1];
						intStack[intStackSize++] = anIntArray1539[2];
					} else {
						intStack[intStackSize++] = -1;
						intStack[intStackSize++] = -1;
					}
				} else {
					intStack[intStackSize++] = -1;
					intStack[intStackSize++] = -1;
					return;
				}
				return;
			}
			if (i == 5226) {
				Class296_Sub15_Sub2.method2529(intStack[--intStackSize], -126);
				return;
			}
			if (i == 5227) {
				intStackSize -= 2;
				int i_81_ = intStack[intStackSize];
				int i_82_ = intStack[intStackSize + 1];
				Class368_Sub13.method3848(i_82_ & 0x3fff, i_81_, i_82_ >> 14 & 0x3fff, true, true);
				return;
			}
			if (i == 5228) {
				Class83.aBoolean914 = intStack[--intStackSize] == 1;
				return;
			}
			if (i == 5229) {
				intStack[intStackSize++] = Class83.aBoolean914 ? 1 : 0;
				return;
			}
			if (i == 5230) {
				int i_83_ = intStack[--intStackSize];
				Class107.method944(i_83_, (byte) -34);
				return;
			}
			if (i == 5231) {
				intStackSize -= 2;
				int i_84_ = intStack[intStackSize];
				boolean bool_85_ = intStack[intStackSize + 1] == 1;
				if (OutputStream_Sub1.aClass263_39 != null) {
					Node class296 = OutputStream_Sub1.aClass263_39.get(i_84_);
					if (class296 != null && !bool_85_) {
						class296.unlink();
					} else if (class296 == null && bool_85_) {
						class296 = new Node();
						OutputStream_Sub1.aClass263_39.put(i_84_, class296);
					}
				}
				return;
			}
			if (i == 5232) {
				int i_86_ = intStack[--intStackSize];
				if (OutputStream_Sub1.aClass263_39 != null) {
					Node class296 = OutputStream_Sub1.aClass263_39.get(i_86_);
					intStack[intStackSize++] = class296 != null ? 1 : 0;
				} else {
					intStack[intStackSize++] = 0;
					return;
				}
				return;
			}
			if (i == 5233) {
				intStackSize -= 2;
				int i_87_ = intStack[intStackSize];
				boolean bool_88_ = intStack[intStackSize + 1] == 1;
				if (Class296_Sub51_Sub31.aClass263_6506 != null) {
					Node class296 = Class296_Sub51_Sub31.aClass263_6506.get(i_87_);
					if (class296 != null && !bool_88_) {
						class296.unlink();
					} else if (class296 == null && bool_88_) {
						class296 = new Node();
						Class296_Sub51_Sub31.aClass263_6506.put(i_87_, class296);
					}
				}
				return;
			}
			if (i == 5234) {
				int i_89_ = intStack[--intStackSize];
				if (Class296_Sub51_Sub31.aClass263_6506 != null) {
					Node class296 = Class296_Sub51_Sub31.aClass263_6506.get(i_89_);
					intStack[intStackSize++] = class296 != null ? 1 : 0;
				} else {
					intStack[intStackSize++] = 0;
					return;
				}
				return;
			}
			if (i == 5235) {
				intStack[intStackSize++] = Class106.aClass296_Sub39_Sub14_1097 != null ? Class106.aClass296_Sub39_Sub14_1097.anInt6214 : -1;
				return;
			}
			if (i == 5236) {
				intStackSize -= 2;
				int i_90_ = intStack[intStackSize];
				int i_91_ = intStack[intStackSize + 1];
				int i_92_ = i_91_ >> 14 & 0x3fff;
				int i_93_ = i_91_ & 0x3fff;
				int i_94_ = Class274.method2314(2147483647, i_90_, i_93_, i_92_);
				if (i_94_ < 0) {
					intStack[intStackSize++] = -1;
				} else {
					intStack[intStackSize++] = i_94_;
					return;
				}
				return;
			}
			if (i == 5237) {
				Class137.method1423((byte) 124);
				return;
			}
		} else if (i < 5400) {
			if (i == 5300) {
				intStackSize -= 2;
				int i_95_ = intStack[intStackSize];
				int i_96_ = intStack[intStackSize + 1];
				Class104.method904(3, false, 0, i_95_, i_96_);
				intStack[intStackSize++] = Animator.aFrame435 != null ? 1 : 0;
				return;
			}
			if (i == 5301) {
				if (Animator.aFrame435 != null) {
					Class104.method904(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_5003.method444(122), false, 0, -1, -1);
				}
				return;
			}
			if (i == 5302) {
				Class224[] class224s = StaticMethods.method1011(10672);
				intStack[intStackSize++] = class224s.length;
				return;
			}
			if (i == 5303) {
				int i_97_ = intStack[--intStackSize];
				Class224[] class224s = StaticMethods.method1011(10672);
				intStack[intStackSize++] = class224s[i_97_].anInt2167;
				intStack[intStackSize++] = class224s[i_97_].anInt2171;
				return;
			}
			if (i == 5305) {
				int i_98_ = Applet_Sub1.anInt11;
				int i_99_ = Class221.anInt2155;
				int i_100_ = -1;
				Class224[] class224s = StaticMethods.method1011(10672);
				for (int i_101_ = 0; i_101_ < class224s.length; i_101_++) {
					Class224 class224 = class224s[i_101_];
					if (class224.anInt2167 == i_98_ && class224.anInt2171 == i_99_) {
						i_100_ = i_101_;
						break;
					}
				}
				intStack[intStackSize++] = i_100_;
				return;
			}
			if (i == 5306) {
				intStack[intStackSize++] = Class8.method192((byte) 101);
				return;
			}
			if (i == 5307) {
				int i_102_ = intStack[--intStackSize];
				if (i_102_ >= 1 && i_102_ <= 2) {
					Class104.method904(i_102_, false, 0, -1, -1);
					return;
				}
				return;
			}
			if (i == 5308) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_5003.method444(121);
				return;
			}
			if (i == 5309) {
				int i_103_ = intStack[--intStackSize];
				if (i_103_ >= 1 && i_103_ <= 2) {
					Class343_Sub1.aClass296_Sub50_5282.method3060(i_103_, 45, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_5003);
					Class343_Sub1.aClass296_Sub50_5282.method3060(i_103_, 120, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_4989);
					Class368_Sub4.method3820(1);
					return;
				}
				return;
			}
		} else if (i < 5500) {
			if (i == 5400) {
				stringStackSize -= 2;
				String string = stringStack[stringStackSize];
				String string_104_ = stringStack[stringStackSize + 1];
				int i_105_ = intStack[--intStackSize];
				Connection class204 = Class296_Sub51_Sub13.method3111(true);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 92, Class107.lobbyWebsitePacket);
				class296_sub1.out.p1(Class117.method1015((byte) -101, string) + Class117.method1015((byte) -124, string_104_) + 1);
				class296_sub1.out.writeString(string);
				class296_sub1.out.writeString(string_104_);
				class296_sub1.out.p1(i_105_);
				class204.sendPacket(class296_sub1, (byte) 119);
				return;
			}
			if (i == 5401) {
				intStackSize -= 2;
				Class12.aShortArray137[intStack[intStackSize]] = (short) StaticMethods.method2727(intStack[intStackSize + 1], (byte) 13);
				Class296_Sub39_Sub1.itemDefinitionLoader.uncacheModels();
				Class296_Sub39_Sub1.itemDefinitionLoader.uncacheRequests();
				Class352.npcDefinitionLoader.method1421(12409);
				Class366_Sub8.method3794(true);
				return;
			}
			if (i == 5405) {
				intStackSize -= 2;
				int i_106_ = intStack[intStackSize];
				int i_107_ = intStack[intStackSize + 1];
				if (i_106_ >= 0 && i_106_ < 2) {
					Class123_Sub1.anIntArrayArrayArray3909[i_106_] = new int[i_107_ << 1][4];
				}
				return;
			}
			if (i == 5406) {
				intStackSize -= 7;
				int i_108_ = intStack[intStackSize];
				int i_109_ = intStack[intStackSize + 1] << 1;
				int i_110_ = intStack[intStackSize + 2];
				int i_111_ = intStack[intStackSize + 3];
				int i_112_ = intStack[intStackSize + 4];
				int i_113_ = intStack[intStackSize + 5];
				int i_114_ = intStack[intStackSize + 6];
				if (i_108_ >= 0 && i_108_ < 2 && Class123_Sub1.anIntArrayArrayArray3909[i_108_] != null && i_109_ >= 0 && i_109_ < Class123_Sub1.anIntArrayArrayArray3909[i_108_].length) {
					Class123_Sub1.anIntArrayArrayArray3909[i_108_][i_109_] = new int[] { (i_110_ >> 14 & 0x3fff) << 9, i_111_ << 2, (i_110_ & 0x3fff) << 9, i_114_ };
					Class123_Sub1.anIntArrayArrayArray3909[i_108_][i_109_ + 1] = new int[] { (i_112_ >> 14 & 0x3fff) << 9, i_113_ << 2, (i_112_ & 0x3fff) << 9 };
				}
				return;
			}
			if (i == 5407) {
				int i_115_ = Class123_Sub1.anIntArrayArrayArray3909[intStack[--intStackSize]].length >> 1;
				intStack[intStackSize++] = i_115_;
				return;
			}
			if (i == 5411) {
				if (Animator.aFrame435 != null) {
					Class104.method904(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_5003.method444(121), false, 0, -1, -1);
				}
				if (Class340.aFrame3707 != null) {
					Class188.method1891(3746);
					System.exit(0);
				} else {
					String string = Class296_Sub27.quitURL != null ? Class296_Sub27.quitURL : LookupTable.method163(27006);
					Class296_Sub13.method2507(false, string, Class252.aClass398_2383, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(121) == 1, 37);
					return;
				}
				return;
			}
			if (i == 5419) {
				String string = "";
				if (Class296_Sub44.aClass278_4949 != null) {
					if (Class296_Sub44.aClass278_4949.anObject2539 != null) {
						string = (String) Class296_Sub44.aClass278_4949.anObject2539;
					} else {
						string = Class189_Sub1.method1908(Class296_Sub44.aClass278_4949.anInt2545, (byte) 86);
					}
				}
				stringStack[stringStackSize++] = string;
				return;
			}
			if (i == 5420) {
				intStack[intStackSize++] = Class252.aClass398_2383.aBoolean3332 ? 0 : 1;
				return;
			}
			if (i == 5421) {
				if (Animator.aFrame435 != null) {
					Class104.method904(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_5003.method444(116), false, 0, -1, -1);
				}
				String string = stringStack[--stringStackSize];
				boolean bool_116_ = intStack[--intStackSize] == 1;
				String string_117_ = LookupTable.method163(27006) + string;
				Class296_Sub13.method2507(bool_116_, string_117_, Class252.aClass398_2383, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(120) == 1, 37);
				return;
			}
			if (i == 5422) {
				stringStackSize -= 2;
				String string = stringStack[stringStackSize];
				String string_118_ = stringStack[stringStackSize + 1];
				int i_119_ = intStack[--intStackSize];
				if (string.length() > 0) {
					if (BITConfigDefinition.aStringArray2605 == null) {
						BITConfigDefinition.aStringArray2605 = new String[Class296_Sub39_Sub17.anIntArray6234[Class296_Sub50.game.anInt340]];
					}
					BITConfigDefinition.aStringArray2605[i_119_] = string;
				}
				if (string_118_.length() > 0) {
					if (Mobile.aStringArray6793 == null) {
						Mobile.aStringArray6793 = new String[Class296_Sub39_Sub17.anIntArray6234[Class296_Sub50.game.anInt340]];
					}
					Mobile.aStringArray6793[i_119_] = string_118_;
				}
				return;
			}
			if (i == 5423) {
				System.out.println(stringStack[--stringStackSize]);
				return;
			}
			if (i == 5424) {
				intStackSize -= 11;
				Class277.anInt2535 = intStack[intStackSize];
				Class314.anInt2786 = intStack[intStackSize + 1];
				Class314.anInt2781 = intStack[intStackSize + 2];
				Class279.anInt2547 = intStack[intStackSize + 3];
				Class366_Sub3.anInt5374 = intStack[intStackSize + 4];
				Class93.anInt1003 = intStack[intStackSize + 5];
				Class181.anInt1861 = intStack[intStackSize + 6];
				CS2Script.anInt6144 = intStack[intStackSize + 7];
				Class245.anInt2330 = intStack[intStackSize + 8];
				Class25.anInt285 = intStack[intStackSize + 9];
				Class5.anInt80 = intStack[intStackSize + 10];
				Class205_Sub2.fs8.hasEntryBuffer(Class366_Sub3.anInt5374);
				Class205_Sub2.fs8.hasEntryBuffer(Class93.anInt1003);
				Class205_Sub2.fs8.hasEntryBuffer(Class181.anInt1861);
				Class205_Sub2.fs8.hasEntryBuffer(CS2Script.anInt6144);
				Class205_Sub2.fs8.hasEntryBuffer(Class245.anInt2330);
				Class41_Sub27.aClass397_3811 = Class130.aClass397_1334 = Class227.aClass397_2187 = null;
				Class363.aClass397_3107 = Class49.aClass397_463 = Class296_Sub39_Sub17.aClass397_6244 = null;
				Class368_Sub15.aClass397_5521 = Class395.aClass397_3316 = null;
				SeekableFile.aBoolean2397 = true;
				return;
			}
			if (i == 5425) {
				Class296_Sub51_Sub4.method3087(21097);
				SeekableFile.aBoolean2397 = false;
				return;
			}
			if (i == 5426) {
				intStackSize -= 2;
				Class296_Sub51_Sub32.anInt6512 = intStack[intStackSize];
				Class106_Sub1.anInt3903 = intStack[intStackSize + 1];
				return;
			}
			if (i == 5427) {
				intStackSize -= 2;
				return;
			}
			if (i == 5428) {
				intStackSize -= 2;
				int i_120_ = intStack[intStackSize];
				int i_121_ = intStack[intStackSize + 1];
				intStack[intStackSize++] = Class296_Sub56.method3217(i_121_, -1007, i_120_) ? 1 : 0;
				return;
			}
			if (i == 5429) {
				Class41_Sub28.method513(stringStack[--stringStackSize], false, false, -112);
				return;
			}
			if (i == 5430) {
				try {
					Class297.method3231("accountcreated", CS2Script.anApplet6140, false);
				} catch (Throwable throwable) {
					/* empty */
				}
				return;
			}
			if (i == 5431) {
				try {
					Class297.method3231("accountcreatestarted", CS2Script.anApplet6140, false);
				} catch (Throwable throwable) {
					/* empty */
				}
				return;
			}
			if (i == 5432) {
				String string = "";
				if (Class296_Sub51_Sub10.aClipboard6394 != null) {
					Transferable transferable = Class296_Sub51_Sub10.aClipboard6394.getContents(null);
					if (transferable != null) {
						try {
							string = (String) transferable.getTransferData(DataFlavor.stringFlavor);
							if (string == null) {
								string = "";
							}
						} catch (Exception exception) {
							/* empty */
						}
					}
				}
				stringStack[stringStackSize++] = string;
				return;
			}
			if (i == 5433) {
				Class296_Sub45_Sub2.anInt6285 = intStack[--intStackSize];
				return;
			}
			if (i == 5435) {
				intStack[intStackSize++] = NodeDeque.js ? 1 : 0;
				return;
			}
			if (i == 5436) {
				if (Class360_Sub3.aClass296_Sub28_5309.anInt4802 < 6) {
					intStack[intStackSize++] = 0;
				} else {
					if (Class360_Sub3.aClass296_Sub28_5309.anInt4802 == 6 && Class360_Sub3.aClass296_Sub28_5309.anInt4806 < 10) {
						intStack[intStackSize++] = 0;
					} else {
						intStack[intStackSize++] = 1;
						return;
					}
					return;
				}
				return;
			}
		} else if (i < 5600) {
			if (i == 5500) {
				intStackSize -= 4;
				int i_122_ = intStack[intStackSize];
				int i_123_ = intStack[intStackSize + 1];
				int i_124_ = intStack[intStackSize + 2];
				int i_125_ = intStack[intStackSize + 3];
				Class296_Sub51_Sub32.method3178((i_122_ >> 14 & 0x3fff) - Class206.worldBaseX, (i_122_ & 0x3fff) - Class41_Sub26.worldBaseY, i_123_ << 2, i_124_, i_125_, false, false);
				return;
			}
			if (i == 5501) {
				intStackSize -= 4;
				int i_126_ = intStack[intStackSize];
				int i_127_ = intStack[intStackSize + 1];
				int i_128_ = intStack[intStackSize + 2];
				int i_129_ = intStack[intStackSize + 3];
				aa_Sub3.cameraLookat((i_126_ >> 14 & 0x3fff) - Class206.worldBaseX, (i_126_ & 0x3fff) - Class41_Sub26.worldBaseY, i_127_ << 2, i_128_, i_129_);
				return;
			}
			if (i == 5502) {
				intStackSize -= 6;
				int i_130_ = intStack[intStackSize];
				if (i_130_ >= 2) {
					throw new RuntimeException();
				}
				Class42_Sub3.anInt3844 = i_130_;
				int i_131_ = intStack[intStackSize + 1];
				if (i_131_ + 1 >= Class123_Sub1.anIntArrayArrayArray3909[Class42_Sub3.anInt3844].length >> 1) {
					throw new RuntimeException();
				}
				Class296_Sub22.anInt4739 = i_131_;
				Class296_Sub45_Sub4.anInt6316 = 0;
				Class156.anInt3593 = intStack[intStackSize + 2];
				StaticMethods.anInt3196 = intStack[intStackSize + 3];
				int i_132_ = intStack[intStackSize + 4];
				if (i_132_ >= 2) {
					throw new RuntimeException();
				}
				ClotchesLoader.anInt279 = i_132_;
				int i_133_ = intStack[intStackSize + 5];
				if (i_133_ + 1 >= Class123_Sub1.anIntArrayArrayArray3909[ClotchesLoader.anInt279].length >> 1) {
					throw new RuntimeException();
				}
				Class296_Sub51_Sub28.anInt6489 = i_133_;
				Class361.anInt3103 = 3;
				Class368_Sub11.anInt5486 = Class242.anInt2309 = -1;
				return;
			}
			if (i == 5503) {
				BillboardRaw.method883((byte) -128);
				return;
			}
			if (i == 5504) {
				intStackSize -= 2;
				Class373_Sub1.method3925(intStack[intStackSize + 1], 0, intStack[intStackSize], -30302);
				return;
			}
			if (i == 5505) {
				intStack[intStackSize++] = (int) Class153.aFloat1572 >> 3;
				return;
			}
			if (i == 5506) {
				intStack[intStackSize++] = (int) Class41_Sub26.aFloat3806 >> 3;
				return;
			}
			if (i == 5507) {
				Class296_Sub51_Sub33.method3182(40);
				return;
			}
			if (i == 5508) {
				Class360.method3721((byte) 63);
				return;
			}
			if (i == 5509) {
				Class366_Sub5.method3784(-127);
				return;
			}
			if (i == 5510) {
				Class268.method2296(3628);
				return;
			}
			if (i == 5511) {
				int i_134_ = intStack[--intStackSize];
				int i_135_ = i_134_ >> 14 & 0x3fff;
				int i_136_ = i_134_ & 0x3fff;
				i_135_ -= Class206.worldBaseX;
				if (i_135_ < 0) {
					i_135_ = 0;
				} else if (i_135_ >= Class198.currentMapSizeX) {
					i_135_ = Class198.currentMapSizeX;
				}
				i_136_ -= Class41_Sub26.worldBaseY;
				if (i_136_ < 0) {
					i_136_ = 0;
				} else if (i_136_ >= Class296_Sub38.currentMapSizeY) {
					i_136_ = Class296_Sub38.currentMapSizeY;
				}
				Class338_Sub3_Sub1.anInt6563 = (i_135_ << 9) + 256;
				Class127_Sub1.anInt4287 = (i_136_ << 9) + 256;
				Class361.anInt3103 = 4;
				Class368_Sub11.anInt5486 = Class242.anInt2309 = -1;
				return;
			}
			if (i == 5512) {
				ParamTypeList.method1653((byte) 116);
				return;
			}
			if (i == 5514) {
				ModeWhat.anInt1193 = intStack[--intStackSize];
				return;
			}
			if (i == 5516) {
				intStack[intStackSize++] = ModeWhat.anInt1193;
				return;
			}
			if (i == 5517) {
				int i_137_ = intStack[--intStackSize];
				if (i_137_ == -1) {
					int i_138_ = i_137_ >> 14 & 0x3fff;
					int i_139_ = i_137_ & 0x3fff;
					i_138_ -= Class206.worldBaseX;
					if (i_138_ < 0) {
						i_138_ = 0;
					} else if (i_138_ >= Class198.currentMapSizeX) {
						i_138_ = Class198.currentMapSizeX;
					}
					i_139_ -= Class41_Sub26.worldBaseY;
					if (i_139_ < 0) {
						i_139_ = 0;
					} else if (i_139_ >= Class296_Sub38.currentMapSizeY) {
						i_139_ = Class296_Sub38.currentMapSizeY;
					}
					Class368_Sub11.anInt5486 = (i_138_ << 9) + 256;
					Class242.anInt2309 = (i_139_ << 9) + 256;
				} else {
					Class368_Sub11.anInt5486 = -1;
					Class242.anInt2309 = -1;
					return;
				}
				return;
			}
			if (i == 5547) {
				intStack[intStackSize++] = Class361.anInt3103;
				return;
			}
		} else if (i < 5700) {
			if (i == 5600) {
				stringStackSize -= 2;
				String string = stringStack[stringStackSize];
				String string_140_ = stringStack[stringStackSize + 1];
				int i_141_ = intStack[--intStackSize];
				Class296_Sub51_Sub21.method3130(string_140_, string, -2103592604, i_141_);
				return;
			}
			if (i == 5601) {
				Class296_Sub35_Sub2.method2756(30);
				return;
			}
			if (i == 5602) {
				if (!Class135.method1412(-1)) {
					Class368_Sub21.method3864(-100);
				}
				return;
			}
			if (i == 5604) {
				stringStackSize--;
				if (Class366_Sub6.anInt5392 == 3 && !Class135.method1412(-1) && Class397_Sub3.anInt5799 == 0) {
					Class41_Sub2.writeEmailVerification(stringStack[stringStackSize], (byte) -110);
					return;
				}
				return;
			}
			if (i == 5605) {
				stringStackSize -= 2;
				intStackSize -= 2;
				if (Class366_Sub6.anInt5392 == 3 && !Class135.method1412(-1) && Class397_Sub3.anInt5799 == 0) {
					StaticMethods.writeAccountCreation(666, intStack[intStackSize + 1] == 1, intStack[intStackSize], stringStack[stringStackSize + 1], stringStack[stringStackSize]);
					return;
				}
				return;
			}
			if (i == 5606) {
				if (Class397_Sub3.anInt5799 == 0) {
					Class368_Sub4.anInt5441 = -2;
				}
				return;
			}
			if (i == 5607) {
				intStack[intStackSize++] = Class296_Sub39_Sub12.anInt6200;
				return;
			}
			if (i == 5608) {
				intStack[intStackSize++] = Class296_Sub40.anInt4914;
				return;
			}
			if (i == 5609) {
				intStack[intStackSize++] = Class368_Sub4.anInt5441;
				return;
			}
			if (i == 5611) {
				intStack[intStackSize++] = ParticleEmitterRaw.anInt1771;
				return;
			}
			if (i == 5612) {
				int i_142_ = intStack[--intStackSize];
				InputStream_Sub1.method127(0, i_142_);
				return;
			}
			if (i == 5613) {
				intStack[intStackSize++] = Class296_Sub39_Sub12.anInt6200;
				return;
			}
			if (i == 5615) {
				stringStackSize -= 2;
				String string = stringStack[stringStackSize];
				String string_143_ = stringStack[stringStackSize + 1];
				InterfaceComponentSettings.method2704(string, true, string_143_);
				return;
			}
			if (i == 5616) {
				StringNode.logout(3, false);
				return;
			}
			if (i == 5617) {
				intStack[intStackSize++] = Class389.anInt3277;
				return;
			}
			if (i == 5618) {
				intStackSize--;
				return;
			}
			if (i == 5619) {
				intStackSize--;
				return;
			}
			if (i == 5620) {
				intStack[intStackSize++] = 0;
				return;
			}
			if (i == 5621) {
				stringStackSize -= 2;
				intStackSize -= 2;
				return;
			}
			if (i == 5622) {
				return;
			}
			if (i == 5623) {
				if (Class294.sessionKey != null) {
					intStack[intStackSize++] = 1;
				} else {
					intStack[intStackSize++] = 0;
					return;
				}
				return;
			}
			if (i == 5624) {
				intStack[intStackSize++] = (int) (Class189.userflow >> 32);
				intStack[intStackSize++] = (int) (Class189.userflow & 0xffffffffffffffffL);
				return;
			}
			if (i == 5625) {
				intStack[intStackSize++] = Class28.under ? 1 : 0;
				return;
			}
			if (i == 5626) {
				Class28.under = true;
				Class42_Sub1.method532(6231);
				return;
			}
			if (i == 5627) {
				intStack[intStackSize++] = Class360_Sub10.anInt5349;
				intStack[intStackSize++] = AdvancedMemoryCache.anInt1171;
				intStack[intStackSize++] = Class355_Sub1.anInt3693;
				Class360_Sub10.anInt5349 = -2;
				AdvancedMemoryCache.anInt1171 = -1;
				Class355_Sub1.anInt3693 = -1;
				return;
			}
			if (i == 5628) {
				intStack[intStackSize++] = Class135.method1412(-1) ? 1 : 0;
				return;
			}
			if (i == 5629) {
				intStack[intStackSize++] = Class139.anInt1422;
				return;
			}
			if (i == 5630) {
				Js5.method1440(-101);
				return;
			}
			if (i == 5631) {
				intStackSize -= 2;
				int i_144_ = intStack[intStackSize];
				int i_145_ = intStack[intStackSize + 1];
				Class252.method2203(i_145_, i_144_, -63);
				return;
			}
			if (i == 5632) {
				int i_146_ = intStack[--intStackSize];
				Class156.method1582(i_146_, false);
				return;
			}
			if (i == 5633) {
				intStack[intStackSize++] = AdvancedMemoryCache.anInt1172;
				return;
			}
		} else if (i < 6100) {
			if (i == 6001) {
				int i_147_ = intStack[--intStackSize];
				Class343_Sub1.aClass296_Sub50_5282.method3060(i_147_, 99, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub6_5014);
				CS2Stack.method2137(70);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6002) {
				boolean bool_148_ = intStack[--intStackSize] == 1;
				Class343_Sub1.aClass296_Sub50_5282.method3060(bool_148_ ? 1 : 0, 107, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5001);
				Class343_Sub1.aClass296_Sub50_5282.method3060(bool_148_ ? 1 : 0, 53, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026);
				CS2Stack.method2137(77);
				Class296_Sub51_Sub21.method3133(100);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6003) {
				boolean bool_149_ = intStack[--intStackSize] == 1;
				Class343_Sub1.aClass296_Sub50_5282.method3060(bool_149_ ? 2 : 1, 46, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5018);
				Class343_Sub1.aClass296_Sub50_5282.method3060(bool_149_ ? 2 : 1, 87, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5029);
				Class296_Sub51_Sub21.method3133(79);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6005) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] == 1 ? 1 : 0, 74, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997);
				CS2Stack.method2137(34);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6007) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 110, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub11_5023);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6008) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] == 1 ? 1 : 0, 74, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub9_4999);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6010) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] == 1 ? 1 : 0, 95, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub16_5019);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6011) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 60, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012);
				CS2Stack.method2137(94);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6012) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] == 1 ? 1 : 0, 91, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub7_5004);
				Class368_Sub4.method3817(-125);
				Class368_Sub22.method3866(11);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6014) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] == 1 ? 2 : 0, 84, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013);
				CS2Stack.method2137(101);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6015) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] == 1 ? 1 : 0, 107, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub1_5008);
				CS2Stack.method2137(110);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6016) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 96, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5015);
				Class33.method348(false, false, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(124));
				Class368_Sub4.method3820(1);
				return;
			}
			if (i == 6017) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] == 1 ? 1 : 0, 83, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub24_5016);
				Class110.method968(false);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6018) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 74, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4995);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6019) {
				int i_150_ = intStack[--intStackSize];
				int i_151_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5017.method489(125);
				if (i_150_ != i_151_) {
					if (Class130.method1376(Class366_Sub6.anInt5392, -14723)) {
						if (i_151_ == 0 && Class30.anInt318 != -1) {
							Class338_Sub8.method3602(0, Class42_Sub4.fs6, false, -123, Class30.anInt318, i_150_);
							Class296_Sub34.method2734(255);
							Class124.aBoolean1282 = false;
						} else if (i_150_ == 0) {
							Class208.method2003(-116);
							Class124.aBoolean1282 = false;
						} else {
							Class368_Sub7.method3833(i_150_, 0);
						}
					}
					Class343_Sub1.aClass296_Sub50_5282.method3060(i_150_, 52, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5017);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
				}
				return;
			}
			if (i == 6020) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 79, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5005);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6021) {
				int i_152_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5018.method449(117);
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] == 1 ? 0 : i_152_, 49, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5029);
				Class296_Sub51_Sub21.method3133(-102);
				return;
			}
			if (i == 6023) {
				int i_153_ = intStack[--intStackSize];
				Class343_Sub1.aClass296_Sub50_5282.method3060(i_153_, 47, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub8_5027);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6024) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 125, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5020);
				Class368_Sub4.method3820(1);
				return;
			}
			if (i == 6025) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 124, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6027) {
				int i_154_ = intStack[--intStackSize];
				if (i_154_ < 0 || i_154_ > 1) {
					i_154_ = 0;
				}
				AdvancedMemoryCache.method984(-120, i_154_ == 1);
				return;
			}
			if (i == 6028) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] != 0 ? 1 : 0, 99, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub20_4996);
				Class368_Sub4.method3820(1);
				return;
			}
			if (i == 6029) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 85, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub11_5023);
				Class368_Sub4.method3820(1);
				return;
			}
			if (i == 6030) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] != 0 ? 1 : 0, 57, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub29_5002);
				Class368_Sub4.method3820(1);
				CS2Stack.method2137(105);
				return;
			}
			if (i == 6031) {
				int i_155_ = intStack[--intStackSize];
				if (i_155_ < 0 || i_155_ > 5) {
					i_155_ = 2;
				}
				Class33.method348(false, false, i_155_);
				return;
			}
			if (i == 6032) {
				intStackSize -= 2;
				int i_156_ = intStack[intStackSize];
				boolean bool_157_ = intStack[intStackSize + 1] == 1;
				Class343_Sub1.aClass296_Sub50_5282.method3060(i_156_, 79, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988);
				if (!bool_157_) {
					Class343_Sub1.aClass296_Sub50_5282.method3060(0, 68, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub15_4990);
				}
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6033) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 101, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub12_4992);
				Class368_Sub4.method3820(1);
				return;
			}
			if (i == 6034) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] == 1 ? 1 : 0, 52, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991);
				Class368_Sub4.method3820(1);
				Class368_Sub4.method3817(-128);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6035) {
				int i_158_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5001.method496(120);
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize] == 1 ? 1 : i_158_, 44, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026);
				CS2Stack.method2137(100);
				Class296_Sub51_Sub21.method3133(69);
				return;
			}
			if (i == 6036) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 63, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub10_5009);
				Class368_Sub4.method3820(1);
				Class380.aBoolean3210 = true;
				return;
			}
			if (i == 6037) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(intStack[--intStackSize], 101, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4998);
				Class368_Sub4.method3820(1);
				Class348.aBoolean3033 = false;
				return;
			}
			if (i == 6038) {
				int i_159_ = intStack[--intStackSize];
				int i_160_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5021.method489(117);
				if (i_159_ != i_160_ && Class30.anInt318 == Class122_Sub1.anInt5657) {
					if (!Class130.method1376(Class366_Sub6.anInt5392, -14723)) {
						if (i_160_ == 0) {
							Class338_Sub8.method3602(0, Class42_Sub4.fs6, false, -127, Class30.anInt318, i_159_);
							Class296_Sub34.method2734(255);
							Class124.aBoolean1282 = false;
						} else if (i_159_ == 0) {
							Class208.method2003(-128);
							Class124.aBoolean1282 = false;
						} else {
							Class368_Sub7.method3833(i_159_, 0);
						}
					}
					Class343_Sub1.aClass296_Sub50_5282.method3060(i_159_, 82, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5021);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
				}
				return;
			}
			if (i == 6039) {
				int i_161_ = intStack[--intStackSize];
				if (i_161_ > 255 || i_161_ < 0) {
					i_161_ = 0;
				}
				if (i_161_ != Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub28_4994.method511(122)) {
					Class343_Sub1.aClass296_Sub50_5282.method3060(i_161_, 49, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub28_4994);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
				}
				return;
			}
			if (i == 6040) {
				int i_162_ = intStack[--intStackSize];
				if (i_162_ != Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022.method456(127)) {
					Class343_Sub1.aClass296_Sub50_5282.method3060(i_162_, 60, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
					Class41_Sub29.method520(-96);
				}
				return;
			}
			if (i == 6041) {
				int i_163_ = intStack[--intStackSize];
				if (i_163_ != Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011.method468(115)) {
					Class343_Sub1.aClass296_Sub50_5282.method3060(i_163_, 67, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
				}
				return;
			}
		} else if (i < 6200) {
			if (i == 6101) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub6_5014.method416(122);
				return;
			}
			if (i == 6102) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5001.method496(118) == 1 ? 1 : 0;
				return;
			}
			if (i == 6103) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5018.method449(118) == 2 ? 1 : 0;
				return;
			}
			if (i == 6105) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997.method484(115) == 1 ? 1 : 0;
				return;
			}
			if (i == 6107) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub11_5023.method437(127);
				return;
			}
			if (i == 6108) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub9_4999.method430(125) == 1 ? 1 : 0;
				return;
			}
			if (i == 6110) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub16_5019.method453(118) == 1 ? 1 : 0;
				return;
			}
			if (i == 6111) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method465(127);
				return;
			}
			if (i == 6112) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub7_5004.method420(126) == 1 ? 1 : 0;
				return;
			}
			if (i == 6114) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013.method393(116) == 2 ? 1 : 0;
				return;
			}
			if (i == 6115) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub1_5008.method389(126) == 1 ? 1 : 0;
				return;
			}
			if (i == 6116) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5015.method475(122);
				return;
			}
			if (i == 6117) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub24_5016.method494(123) == 1 ? 1 : 0;
				return;
			}
			if (i == 6118) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4995.method489(125);
				return;
			}
			if (i == 6119) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5017.method489(124);
				return;
			}
			if (i == 6120) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5005.method489(120);
				return;
			}
			if (i == 6123) {
				intStack[intStackSize++] = SeekableFile.method2223(0);
				return;
			}
			if (i == 6124) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5020.method475(125);
				return;
			}
			if (i == 6125) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987.method402(120);
				return;
			}
			if (i == 6127) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub27_5010.method505(127) == 1 ? 1 : 0;
				return;
			}
			if (i == 6128) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub20_4996.method470(123) == 1 ? 1 : 0;
				return;
			}
			if (i == 6129) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub11_5023.method437(127);
				return;
			}
			if (i == 6130) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub29_5002.method515(125) == 1 ? 1 : 0;
				return;
			}
			if (i == 6131) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(115);
				return;
			}
			if (i == 6132) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988.method521(116);
				return;
			}
			if (i == 6133) {
				intStack[intStackSize++] = Class252.aClass398_2383.aBoolean3332 && !Class252.aClass398_2383.aBoolean3321 ? 1 : 0;
				return;
			}
			if (i == 6135) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub12_4992.method440(127);
				return;
			}
			if (i == 6136) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991.method497(115) == 1 ? 1 : 0;
				return;
			}
			if (i == 6138) {
				intStack[intStackSize++] = Class296_Sub51_Sub21.method3134(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(117), 200, (byte) -121);
				return;
			}
			if (i == 6139) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub10_5009.method435(125);
				return;
			}
			if (i == 6142) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4998.method489(125);
				return;
			}
			if (i == 6143) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5021.method489(124);
				return;
			}
			if (i == 6144) {
				intStack[intStackSize++] = Class382.aBoolean3241 ? 1 : 0;
				return;
			}
			if (i == 6145) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub28_4994.method511(123);
				return;
			}
			if (i == 6146) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022.method456(118);
				return;
			}
			if (i == 6147) {
				intStack[intStackSize++] = Class360_Sub3.aClass296_Sub28_5309.anInt4797 < 512 || Class382.aBoolean3241 || Class296_Sub51_Sub30.aBoolean6498 ? 1 : 0;
				return;
			}
			if (i == 6148) {
				intStack[intStackSize++] = Class309.aBoolean2758 ? 1 : 0;
				return;
			}
			if (i == 6149) {
				intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011.method468(117);
				return;
			}
		} else if (i < 6300) {
			if (i == 6200) {
				intStackSize -= 2;
				ModeWhere.aShort2350 = (short) intStack[intStackSize];
				if (ModeWhere.aShort2350 <= 0) {
					ModeWhere.aShort2350 = (short) 256;
				}
				NodeDeque.aShort1589 = (short) intStack[intStackSize + 1];
				if (NodeDeque.aShort1589 <= 0) {
					NodeDeque.aShort1589 = (short) 205;
				}
				return;
			}
			if (i == 6201) {
				intStackSize -= 2;
				Class105.aShort3661 = (short) intStack[intStackSize];
				if (Class105.aShort3661 <= 0) {
					Class105.aShort3661 = (short) 256;
				}
				Class366_Sub7.aShort5396 = (short) intStack[intStackSize + 1];
				if (Class366_Sub7.aShort5396 <= 0) {
					Class366_Sub7.aShort5396 = (short) 320;
				}
				return;
			}
			if (i == 6202) {
				intStackSize -= 4;
				Class296_Sub27.aShort4784 = (short) intStack[intStackSize];
				if (Class296_Sub27.aShort4784 <= 0) {
					Class296_Sub27.aShort4784 = (short) 1;
				}
				Class125.aShort1286 = (short) intStack[intStackSize + 1];
				if (Class125.aShort1286 <= 0) {
					Class125.aShort1286 = (short) 32767;
				} else if (Class125.aShort1286 < Class296_Sub27.aShort4784) {
					Class125.aShort1286 = Class296_Sub27.aShort4784;
				}
				BillboardRaw.aShort1074 = (short) intStack[intStackSize + 2];
				if (BillboardRaw.aShort1074 <= 0) {
					BillboardRaw.aShort1074 = (short) 1;
				}
				Class296_Sub14.aShort4659 = (short) intStack[intStackSize + 3];
				if (Class296_Sub14.aShort4659 <= 0) {
					Class296_Sub14.aShort4659 = (short) 32767;
				} else {
					if (Class296_Sub14.aShort4659 < BillboardRaw.aShort1074) {
						Class296_Sub14.aShort4659 = BillboardRaw.aShort1074;
					}
					return;
				}
				return;
			}
			if (i == 6203) {
				Class95.method866(Class187.aClass51_1923.anInt578, Class187.aClass51_1923.anInt623, 28885, false, 0, 0);
				intStack[intStackSize++] = Class344.anInt3007;
				intStack[intStackSize++] = Class296_Sub29.anInt4814;
				return;
			}
			if (i == 6204) {
				intStack[intStackSize++] = Class105.aShort3661;
				intStack[intStackSize++] = Class366_Sub7.aShort5396;
				return;
			}
			if (i == 6205) {
				intStack[intStackSize++] = ModeWhere.aShort2350;
				intStack[intStackSize++] = NodeDeque.aShort1589;
				return;
			}
		} else if (i < 6400) {
			if (i == 6300) {
				intStack[intStackSize++] = (int) (Class72.method771(-125) / 60000L);
				return;
			}
			if (i == 6301) {
				intStack[intStackSize++] = (int) (Class72.method771(-127) / 86400000L) - 11745;
				return;
			}
			if (i == 6302) {
				intStackSize -= 3;
				int i_164_ = intStack[intStackSize];
				int i_165_ = intStack[intStackSize + 1];
				int i_166_ = intStack[intStackSize + 2];
				long l = StaticMethods.method2466(i_165_, 0, 0, -55, i_166_, 12, i_164_);
				int i_167_ = Class293.method2416(83, l);
				if (i_166_ < 1970) {
					i_167_--;
				}
				intStack[intStackSize++] = i_167_;
				return;
			}
			if (i == 6303) {
				intStack[intStackSize++] = GraphicsLoader.method2280(-111, Class72.method771(-116));
				return;
			}
			if (i == 6304) {
				int i_168_ = intStack[--intStackSize];
				boolean bool_169_ = true;
				if (i_168_ < 0) {
					bool_169_ = (i_168_ + 1) % 4 == 0;
				} else if (i_168_ < 1582) {
					bool_169_ = i_168_ % 4 == 0;
				} else if (i_168_ % 4 != 0) {
					bool_169_ = false;
				} else if (i_168_ % 100 != 0) {
					bool_169_ = true;
				} else if (i_168_ % 400 != 0) {
					bool_169_ = false;
				}
				intStack[intStackSize++] = bool_169_ ? 1 : 0;
				return;
			}
			if (i == 6305) {
				int i_170_ = intStack[--intStackSize];
				int[] is = Class286.method2384(20746, i_170_);
				ArrayTools.removeElements(is, 0, intStack, intStackSize, 3);
				intStackSize += 3;
				return;
			}
			if (i == 6306) {
				int i_171_ = intStack[--intStackSize];
				intStack[intStackSize++] = (int) (Class296_Sub28.method2685((byte) 41, i_171_) / 60000L);
				return;
			}
		} else if (i < 6500) {
			if (i == 6405) {
				intStack[intStackSize++] = Class240.method2147(-10564) ? 1 : 0;
				return;
			}
			if (i == 6406) {
				intStack[intStackSize++] = Class32.method344((byte) 115) ? 1 : 0;
				return;
			}
		} else if (i < 6600) {
			if (i == 6500) {
				if (Class366_Sub6.anInt5392 != 7 || Class135.method1412(-1) || Class397_Sub3.anInt5799 != 0) {
					intStack[intStackSize++] = 1;
				} else {
					if (Class296_Sub39_Sub17.aBoolean6238) {
						intStack[intStackSize++] = 0;
					} else {
						if (Class99.aLong1065 > Class72.method771(-109) - 1000L) {
							intStack[intStackSize++] = 1;
						} else {
							Class296_Sub39_Sub17.aBoolean6238 = true;
							Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6276.aClass185_2053, (byte) 120, GraphicsLoader.aClass311_2479);
							class296_sub1.out.p4(Class68.anInt758);
							Class296_Sub45_Sub2.aClass204_6276.sendPacket(class296_sub1, (byte) 119);
							intStack[intStackSize++] = 0;
							return;
						}
						return;
					}
					return;
				}
				return;
			}
			if (i == 6501) {
				Class210_Sub1 class210_sub1 = Class296_Sub51_Sub16.method3121(true);
				if (class210_sub1 != null) {
					intStack[intStackSize++] = class210_sub1.anInt4540;
					intStack[intStackSize++] = class210_sub1.anInt2098;
					stringStack[stringStackSize++] = class210_sub1.aString4542;
					Class303 class303 = class210_sub1.method2010(true);
					intStack[intStackSize++] = class303.anInt2727;
					stringStack[stringStackSize++] = class303.aString2724;
					intStack[intStackSize++] = class210_sub1.anInt2096;
					intStack[intStackSize++] = class210_sub1.anInt4538;
					stringStack[stringStackSize++] = class210_sub1.aString4543;
				} else {
					intStack[intStackSize++] = -1;
					intStack[intStackSize++] = 0;
					stringStack[stringStackSize++] = "";
					intStack[intStackSize++] = 0;
					stringStack[stringStackSize++] = "";
					intStack[intStackSize++] = 0;
					intStack[intStackSize++] = 0;
					stringStack[stringStackSize++] = "";
					return;
				}
				return;
			}
			if (i == 6502) {
				Class210_Sub1 class210_sub1 = Class338_Sub8_Sub1.method3607((byte) 108);
				if (class210_sub1 != null) {
					intStack[intStackSize++] = class210_sub1.anInt4540;
					intStack[intStackSize++] = class210_sub1.anInt2098;
					stringStack[stringStackSize++] = class210_sub1.aString4542;
					Class303 class303 = class210_sub1.method2010(true);
					intStack[intStackSize++] = class303.anInt2727;
					stringStack[stringStackSize++] = class303.aString2724;
					intStack[intStackSize++] = class210_sub1.anInt2096;
					intStack[intStackSize++] = class210_sub1.anInt4538;
					stringStack[stringStackSize++] = class210_sub1.aString4543;
				} else {
					intStack[intStackSize++] = -1;
					intStack[intStackSize++] = 0;
					stringStack[stringStackSize++] = "";
					intStack[intStackSize++] = 0;
					stringStack[stringStackSize++] = "";
					intStack[intStackSize++] = 0;
					intStack[intStackSize++] = 0;
					stringStack[stringStackSize++] = "";
					return;
				}
				return;
			}
			if (i == 6503) {
				int i_172_ = intStack[--intStackSize];
				String string = stringStack[--stringStackSize];
				if (Class366_Sub6.anInt5392 != 7 || Class135.method1412(-1) || Class397_Sub3.anInt5799 != 0) {
					intStack[intStackSize++] = 0;
				} else {
					intStack[intStackSize++] = Class45.method582(i_172_, 7000, string) ? 1 : 0;
					return;
				}
				return;
			}
			if (i == 6506) {
				int i_173_ = intStack[--intStackSize];
				Class210_Sub1 class210_sub1 = GraphicsLoader.method2284(i_173_, -88);
				if (class210_sub1 != null) {
					intStack[intStackSize++] = class210_sub1.anInt2098;
					stringStack[stringStackSize++] = class210_sub1.aString4542;
					Class303 class303 = class210_sub1.method2010(true);
					intStack[intStackSize++] = class303.anInt2727;
					stringStack[stringStackSize++] = class303.aString2724;
					intStack[intStackSize++] = class210_sub1.anInt2096;
					intStack[intStackSize++] = class210_sub1.anInt4538;
					stringStack[stringStackSize++] = class210_sub1.aString4543;
				} else {
					intStack[intStackSize++] = -1;
					stringStack[stringStackSize++] = "";
					intStack[intStackSize++] = 0;
					stringStack[stringStackSize++] = "";
					intStack[intStackSize++] = 0;
					intStack[intStackSize++] = 0;
					stringStack[stringStackSize++] = "";
					return;
				}
				return;
			}
			if (i == 6507) {
				intStackSize -= 4;
				int i_174_ = intStack[intStackSize];
				boolean bool_175_ = intStack[intStackSize + 1] == 1;
				int i_176_ = intStack[intStackSize + 2];
				boolean bool_177_ = intStack[intStackSize + 3] == 1;
				Class296_Sub17_Sub2.method2635(13647, bool_175_, i_174_, bool_177_, i_176_);
				return;
			}
			if (i == 6508) {
				NPCDefinition.method1495(-15477);
				return;
			}
			if (i == 6509) {
				if (Class366_Sub6.anInt5392 == 7) {
					Class116.aBoolean3679 = intStack[--intStackSize] == 1;
					return;
				}
				return;
			}
			if (i == 6510) {
				intStack[intStackSize++] = Class83.worldFlags;
				return;
			}
		} else if (i >= 6700) {
			if (i < 6800 && Class296_Sub17_Sub2.modeWhat == Class241_Sub2_Sub2.wipModeWhat) {
				if (i == 6700) {
					int i_178_ = Class386.aClass263_3264.size((byte) -4);
					if (Class99.anInt1064 != -1) {
						i_178_++;
					}
					intStack[intStackSize++] = i_178_;
					return;
				}
				if (i == 6701) {
					int i_179_ = intStack[--intStackSize];
					if (Class99.anInt1064 != -1) {
						if (i_179_ == 0) {
							intStack[intStackSize++] = Class99.anInt1064;
							return;
						}
						i_179_--;
					}
					Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.getFirst(true);
					while (i_179_-- > 0) {
						class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.getNext(0);
					}
					intStack[intStackSize++] = class296_sub13.anInt4657;
					return;
				}
				if (i == 6702) {
					int i_180_ = intStack[--intStackSize];
					if (Class192.openedInterfaceComponents[i_180_] == null) {
						stringStack[stringStackSize++] = "";
					} else {
						String string = Class192.openedInterfaceComponents[i_180_][0].aString602;
						if (string == null) {
							stringStack[stringStackSize++] = "";
						} else {
							stringStack[stringStackSize++] = string.substring(0, string.indexOf(':'));
							return;
						}
						return;
					}
					return;
				}
				if (i == 6703) {
					int i_181_ = intStack[--intStackSize];
					if (Class192.openedInterfaceComponents[i_181_] == null) {
						intStack[intStackSize++] = 0;
					} else {
						intStack[intStackSize++] = Class192.openedInterfaceComponents[i_181_].length;
						return;
					}
					return;
				}
				if (i == 6704) {
					intStackSize -= 2;
					int i_182_ = intStack[intStackSize];
					int i_183_ = intStack[intStackSize + 1];
					if (Class192.openedInterfaceComponents[i_182_] == null) {
						stringStack[stringStackSize++] = "";
					} else {
						String string = Class192.openedInterfaceComponents[i_182_][i_183_].aString602;
						if (string == null) {
							stringStack[stringStackSize++] = "";
						} else {
							stringStack[stringStackSize++] = string;
							return;
						}
						return;
					}
					return;
				}
				if (i == 6705) {
					intStackSize -= 2;
					int i_184_ = intStack[intStackSize];
					int i_185_ = intStack[intStackSize + 1];
					if (Class192.openedInterfaceComponents[i_184_] == null) {
						intStack[intStackSize++] = 0;
					} else {
						intStack[intStackSize++] = Class192.openedInterfaceComponents[i_184_][i_185_].anInt529;
						return;
					}
					return;
				}
				if (i == 6706) {
					return;
				}
				if (i == 6707) {
					intStackSize -= 3;
					int i_186_ = intStack[intStackSize];
					int i_187_ = intStack[intStackSize + 1];
					int i_188_ = intStack[intStackSize + 2];
					Class296_Sub39_Sub18.interfaceClicked(1, i_186_ << 16 | i_187_, "", i_188_);
					return;
				}
				if (i == 6708) {
					intStackSize -= 3;
					int i_189_ = intStack[intStackSize];
					int i_190_ = intStack[intStackSize + 1];
					int i_191_ = intStack[intStackSize + 2];
					Class296_Sub39_Sub18.interfaceClicked(2, i_189_ << 16 | i_190_, "", i_191_);
					return;
				}
				if (i == 6709) {
					intStackSize -= 3;
					int i_192_ = intStack[intStackSize];
					int i_193_ = intStack[intStackSize + 1];
					int i_194_ = intStack[intStackSize + 2];
					Class296_Sub39_Sub18.interfaceClicked(3, i_192_ << 16 | i_193_, "", i_194_);
					return;
				}
				if (i == 6710) {
					intStackSize -= 3;
					int i_195_ = intStack[intStackSize];
					int i_196_ = intStack[intStackSize + 1];
					int i_197_ = intStack[intStackSize + 2];
					Class296_Sub39_Sub18.interfaceClicked(4, i_195_ << 16 | i_196_, "", i_197_);
					return;
				}
				if (i == 6711) {
					intStackSize -= 3;
					int i_198_ = intStack[intStackSize];
					int i_199_ = intStack[intStackSize + 1];
					int i_200_ = intStack[intStackSize + 2];
					Class296_Sub39_Sub18.interfaceClicked(5, i_198_ << 16 | i_199_, "", i_200_);
					return;
				}
				if (i == 6712) {
					intStackSize -= 3;
					int i_201_ = intStack[intStackSize];
					int i_202_ = intStack[intStackSize + 1];
					int i_203_ = intStack[intStackSize + 2];
					Class296_Sub39_Sub18.interfaceClicked(6, i_201_ << 16 | i_202_, "", i_203_);
					return;
				}
				if (i == 6713) {
					intStackSize -= 3;
					int i_204_ = intStack[intStackSize];
					int i_205_ = intStack[intStackSize + 1];
					int i_206_ = intStack[intStackSize + 2];
					Class296_Sub39_Sub18.interfaceClicked(7, i_204_ << 16 | i_205_, "", i_206_);
					return;
				}
				if (i == 6714) {
					intStackSize -= 3;
					int i_207_ = intStack[intStackSize];
					int i_208_ = intStack[intStackSize + 1];
					int i_209_ = intStack[intStackSize + 2];
					Class296_Sub39_Sub18.interfaceClicked(8, i_207_ << 16 | i_208_, "", i_209_);
					return;
				}
				if (i == 6715) {
					intStackSize -= 3;
					int i_210_ = intStack[intStackSize];
					int i_211_ = intStack[intStackSize + 1];
					int i_212_ = intStack[intStackSize + 2];
					Class296_Sub39_Sub18.interfaceClicked(9, i_210_ << 16 | i_211_, "", i_212_);
					return;
				}
				if (i == 6716) {
					intStackSize -= 3;
					int i_213_ = intStack[intStackSize];
					int i_214_ = intStack[intStackSize + 1];
					int i_215_ = intStack[intStackSize + 2];
					Class296_Sub39_Sub18.interfaceClicked(10, i_213_ << 16 | i_214_, "", i_215_);
					return;
				}
				if (i == 6717) {
					intStackSize -= 3;
					int i_216_ = intStack[intStackSize];
					int i_217_ = intStack[intStackSize + 1];
					int i_218_ = intStack[intStackSize + 2];
					InterfaceComponent class51 = Class103.method894(0, i_216_ << 16 | i_217_, i_218_);
					Class285.method2368(0);
					InterfaceComponentSettings class296_sub31 = GameClient.method115(class51);
					Class3.method172(class296_sub31.originalHash, class296_sub31.method2707(-92), class51, 22635);
					return;
				}
			} else if (i < 6900) {
				if (i == 6800) {
					int i_219_ = intStack[--intStackSize];
					Class18 class18 = Class31.aClass245_324.method2179(11702, i_219_);
					if (class18.aString191 == null) {
						stringStack[stringStackSize++] = "";
					} else {
						stringStack[stringStackSize++] = class18.aString191;
						return;
					}
					return;
				}
				if (i == 6801) {
					int i_220_ = intStack[--intStackSize];
					Class18 class18 = Class31.aClass245_324.method2179(11702, i_220_);
					intStack[intStackSize++] = class18.anInt203;
					return;
				}
				if (i == 6802) {
					int i_221_ = intStack[--intStackSize];
					Class18 class18 = Class31.aClass245_324.method2179(11702, i_221_);
					intStack[intStackSize++] = class18.anInt214;
					return;
				}
				if (i == 6803) {
					int i_222_ = intStack[--intStackSize];
					Class18 class18 = Class31.aClass245_324.method2179(11702, i_222_);
					intStack[intStackSize++] = class18.anInt217;
					return;
				}
				if (i == 6804) {
					intStackSize -= 2;
					int i_223_ = intStack[intStackSize];
					int i_224_ = intStack[intStackSize + 1];
					ParamType class383 = Class296_Sub22.itemExtraDataDefinitionLoader.list(i_224_);
					if (class383.method4013(-126)) {
						stringStack[stringStackSize++] = Class31.aClass245_324.method2179(11702, i_223_).method271(class383.name, i_224_, (byte) 127);
					} else {
						intStack[intStackSize++] = Class31.aClass245_324.method2179(11702, i_223_).method272(13821, i_224_, class383.defaultValue);
						return;
					}
					return;
				}
			} else if (i < 7000) {
				if (i == 6900) {
					intStack[intStackSize++] = Class296_Sub40.somethingWithIgnore0 && !Class41_Sub17.somethingWithIgnore1 ? 1 : 0;
					return;
				}
				if (i == 6901) {
					intStack[intStackSize++] = (int) (Class165.aLong1688 / 60000L);
					intStack[intStackSize++] = (int) ((Class165.aLong1688 - Class72.method771(-110) - Class340.aLong3708) / 60000L);
					intStack[intStackSize++] = Class268.aBoolean2495 ? 1 : 0;
					return;
				}
				if (i == 6902) {
					intStack[intStackSize++] = Class296_Sub51_Sub3.anInt6343;
					return;
				}
				if (i == 6903) {
					intStack[intStackSize++] = Js5TextureLoader.anInt3442;
					return;
				}
				if (i == 6904) {
					intStack[intStackSize++] = Class206.anInt2068;
					return;
				}
				if (i == 6905) {
					String string = "";
					if (Class296_Sub44.aClass278_4949 != null) {
						if (Class296_Sub44.aClass278_4949.anObject2539 != null) {
							string = (String) Class296_Sub44.aClass278_4949.anObject2539;
						} else {
							string = Class189_Sub1.method1908(Class296_Sub44.aClass278_4949.anInt2545, (byte) 86);
						}
					}
					stringStack[stringStackSize++] = string;
					return;
				}
				if (i == 6906) {
					intStack[intStackSize++] = Class87.anInt942;
					return;
				}
				if (i == 6907) {
					intStack[intStackSize++] = StaticMethods.anInt5058;
					return;
				}
				if (i == 6908) {
					intStack[intStackSize++] = StaticMethods.anInt5968;
					return;
				}
				if (i == 6909) {
					intStack[intStackSize++] = ModeWhere.aBoolean2354 ? 1 : 0;
					return;
				}
				if (i == 6910) {
					intStack[intStackSize++] = Class268.anInt2486;
					return;
				}
				if (i == 6911) {
					intStack[intStackSize++] = Class42_Sub1.anInt3834;
					return;
				}
				if (i == 6912) {
					intStack[intStackSize++] = Class296_Sub35_Sub1.anInt6109;
					return;
				}
				if (i == 6913) {
					intStack[intStackSize++] = Class366_Sub2.anInt5369;
					return;
				}
				if (i == 6914) {
					intStack[intStackSize++] = Class366_Sub7.aBoolean5402 ? 1 : 0;
					return;
				}
				if (i == 6915) {
					intStack[intStackSize++] = Class104.anInt1088;
					return;
				}
			} else if (i < 7100) {
				if (i == 7000) {
					int i_225_ = Class351.method3682(-513);
					intStack[intStackSize++] = Class226.anInt2185 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(122);
					intStack[intStackSize++] = i_225_;
					CS2Stack.method2137(46);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
					return;
				}
				if (i == 7001) {
					Class230.method2108(-105);
					CS2Stack.method2137(58);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
					return;
				}
				if (i == 7002) {
					Class205_Sub4.method1986((byte) 62);
					CS2Stack.method2137(82);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
					return;
				}
				if (i == 7003) {
					Class276.method2321(1);
					CS2Stack.method2137(85);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
					return;
				}
				if (i == 7004) {
					Class368_Sub20.method3857(true, (byte) 125);
					CS2Stack.method2137(30);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
					return;
				}
				if (i == 7005) {
					Class343_Sub1.aClass296_Sub50_5282.method3060(0, 118, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub15_4990);
					Class368_Sub4.method3820(1);
					Class348.aBoolean3033 = false;
					return;
				}
				if (i == 7006) {
					if (Class226.anInt2185 == 2) {
						ha.aBoolean1294 = true;
					} else {
						if (Class226.anInt2185 == 1) {
							Class338_Sub3_Sub3_Sub1.aBoolean6621 = true;
						} else {
							if (Class226.anInt2185 == 3) {
								Class42_Sub3.aBoolean3845 = true;
							}
							return;
						}
						return;
					}
					return;
				}
				if (i == 7007) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub15_4990.method452(122);
					return;
				}
			} else if (i < 7200) {
				if (i == 7100) {
					intStackSize -= 2;
					int i_226_ = intStack[intStackSize];
					int i_227_ = intStack[intStackSize + 1];
					if (i_226_ != -1) {
						if (i_227_ > 255) {
							i_227_ = 255;
						} else if (i_227_ < 0) {
							i_227_ = 0;
						}
						Class368_Sub20.method3859(false, (byte) -98, i_227_, i_226_);
					}
					return;
				}
				if (i == 7101) {
					int i_228_ = intStack[--intStackSize];
					if (i_228_ != -1) {
						Class108.method949(i_228_, 11771);
					}
					return;
				}
				if (i == 7102) {
					int i_229_ = intStack[--intStackSize];
					if (i_229_ != -1) {
						Class270.method2306(i_229_, (byte) -120);
					}
					return;
				}
				if (i == 7103) {
					intStack[intStackSize++] = Class194.method1930("jagtheora", -105) ? 1 : 0;
					return;
				}
			} else if (i < 7300) {
				if (i == 7201) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997.method483(-25952) ? 1 : 0;
					return;
				}
				if (i == 7202) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub16_5019.method454(-25952) ? 1 : 0;
					return;
				}
				if (i == 7203) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method463(-25952) ? 1 : 0;
					return;
				}
				if (i == 7204) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013.method394(-25952) ? 1 : 0;
					return;
				}
				if (i == 7205) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5020.method476(-25952) && Class41_Sub13.aHa3774.d() ? 1 : 0;
					return;
				}
				if (i == 7206) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub8_5027.method424(-25952) ? 1 : 0;
					return;
				}
				if (i == 7207) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987.method408(-25952) ? 1 : 0;
					return;
				}
				if (i == 7208) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub27_5010.method506(-25952) && Class41_Sub13.aHa3774.n() ? 1 : 0;
					return;
				}
				if (i == 7209) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub29_5002.method517(-25952) ? 1 : 0;
					return;
				}
				if (i == 7210) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991.method501(-25952) ? 1 : 0;
					return;
				}
				if (i == 7211) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub10_5009.method432(-25952) ? 1 : 0;
					return;
				}
				if (i == 7212) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub1_5008.method390(-25952) ? 1 : 0;
					return;
				}
				if (i == 7213) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022.method461(-25952) ? 1 : 0;
					return;
				}
				if (i == 7214) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988.method525(-25952) ? 1 : 0;
					return;
				}
				if (i == 7215) {
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011.method469(-25952) ? 1 : 0;
					return;
				}
			} else if (i < 7400) {
				if (i == 7301) {
					int i_230_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997.method380(i_230_, (byte) 41);
					return;
				}
				if (i == 7302) {
					int i_231_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub16_5019.method380(i_231_, (byte) 41);
					return;
				}
				if (i == 7303) {
					int i_232_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method380(i_232_, (byte) 41);
					return;
				}
				if (i == 7304) {
					int i_233_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013.method380(i_233_, (byte) 41);
					return;
				}
				if (i == 7305) {
					int i_234_ = intStack[--intStackSize];
					if (!Class41_Sub13.aHa3774.d()) {
						intStack[intStackSize++] = 3;
					} else {
						intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5020.method380(i_234_, (byte) 41);
						return;
					}
					return;
				}
				if (i == 7306) {
					int i_235_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub8_5027.method380(i_235_, (byte) 41);
					return;
				}
				if (i == 7307) {
					int i_236_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub4_4987.method380(i_236_, (byte) 41);
					return;
				}
				if (i == 7308) {
					int i_237_ = intStack[--intStackSize];
					if (!Class41_Sub13.aHa3774.n()) {
						intStack[intStackSize++] = 3;
					} else {
						intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub27_5010.method380(i_237_, (byte) 41);
						return;
					}
					return;
				}
				if (i == 7309) {
					int i_238_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub29_5002.method380(i_238_, (byte) 41);
					return;
				}
				if (i == 7310) {
					int i_239_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991.method380(i_239_, (byte) 41);
					return;
				}
				if (i == 7311) {
					int i_240_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub10_5009.method380(i_240_, (byte) 41);
					return;
				}
				if (i == 7312) {
					int i_241_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub1_5008.method380(i_241_, (byte) 41);
					return;
				}
				if (i == 7313) {
					int i_242_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub17_5022.method380(i_242_, (byte) 41);
					return;
				}
				if (i == 7314) {
					int i_243_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988.method380(i_243_, (byte) 41);
					return;
				}
				if (i == 7315) {
					int i_244_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011.method380(i_244_, (byte) 41);
					return;
				}
			}
		}
		throw new IllegalStateException(String.valueOf(i));
	}

	private static final void runCS2(CS2Call call, int maxOpcodes) {
		Object[] args = call.callArgs;
		int cs2ID = ((Integer) args[0]).intValue();
		CS2Script stack = Class338_Sub3_Sub1_Sub4.loadCS2(cs2ID, 0);
		if (stack != null) {
			intLocals = new int[stack.intLocalsCount];
			int i_246_ = 0;
			stringLocals = new String[stack.stringLocalsCount];
			int i_247_ = 0;
			longLocals = new long[stack.longLocalsCount];
			int i_248_ = 0;
			for (int i_249_ = 1; i_249_ < args.length; i_249_++) {
				if (args[i_249_] instanceof Integer) {
					int i_250_ = ((Integer) args[i_249_]).intValue();
					if (i_250_ == -2147483647) {
						i_250_ = call.anInt4958;
					}
					if (i_250_ == -2147483646) {
						i_250_ = call.anInt4964;
					}
					if (i_250_ == -2147483645) {
						i_250_ = call.callerInterface != null ? call.callerInterface.uid : -1;
					}
					if (i_250_ == -2147483644) {
						i_250_ = call.anInt4965;
					}
					if (i_250_ == -2147483643) {
						i_250_ = call.callerInterface != null ? call.callerInterface.anInt592 : -1;
					}
					if (i_250_ == -2147483642) {
						i_250_ = call.aClass51_4962 != null ? call.aClass51_4962.uid : -1;
					}
					if (i_250_ == -2147483641) {
						i_250_ = call.aClass51_4962 != null ? call.aClass51_4962.anInt592 : -1;
					}
					if (i_250_ == -2147483640) {
						i_250_ = call.anInt4966;
					}
					if (i_250_ == -2147483639) {
						i_250_ = call.anInt4969;
					}
					intLocals[i_246_++] = i_250_;
				} else if (args[i_249_] instanceof String) {
					String string = (String) args[i_249_];
					if (string.equals("event_opbase")) {
						string = call.aString4968;
					}
					stringLocals[i_247_++] = string;
				} else if (args[i_249_] instanceof Long) {
					long l = ((Long) args[i_249_]).longValue();
					longLocals[i_248_++] = l;
				}
			}
			anInt1546 = call.anInt4955;
			execute(stack, maxOpcodes);
		}
	}

	private static final int method1525(char c) {
		if (Class30.method331(c, -110)) {
			return 1;
		}
		return 0;
	}

	public static void method1526() {
		intLocals = null;
		stringLocals = null;
		longLocals = null;
		arrayLengths = null;
		arrays = null;
		intStack = null;
		stringStack = null;
		longStack = null;
		callStack = null;
		aClass51_1528 = null;
		aClass51_1537 = null;
		aClass270_1536 = null;
		aClass371_1540 = null;
		aClass296_Sub54_1545 = null;
		anIntArray1539 = null;
		aClass113_1531 = null;
		aString1548 = null;
	}

	private static final long method1527(int i) {
		Long var_long = aClass371_1540.getVarLong(Class296_Sub50.game.anInt340 << 16 | i);
		if (var_long == null) {
			return -1L;
		}
		return var_long.longValue();
	}

	private static final void method1528(String string, int i) {
		if (Class338_Sub3_Sub5.rights != 0 || (!Class296_Sub40.somethingWithIgnore0 || Class41_Sub17.somethingWithIgnore1) && !Class123.somethingWithIgnore2) {
			String string_251_ = string.toLowerCase();
			int i_252_ = 0;
			if (string_251_.startsWith(TranslatableString.aClass120_1243.getTranslation(0))) {
				i_252_ = 0;
				string = string.substring(TranslatableString.aClass120_1243.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1244.getTranslation(0))) {
				i_252_ = 1;
				string = string.substring(TranslatableString.aClass120_1244.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1245.getTranslation(0))) {
				i_252_ = 2;
				string = string.substring(TranslatableString.aClass120_1245.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1246.getTranslation(0))) {
				i_252_ = 3;
				string = string.substring(TranslatableString.aClass120_1246.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1247.getTranslation(0))) {
				i_252_ = 4;
				string = string.substring(TranslatableString.aClass120_1247.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1248.getTranslation(0))) {
				i_252_ = 5;
				string = string.substring(TranslatableString.aClass120_1248.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1249.getTranslation(0))) {
				i_252_ = 6;
				string = string.substring(TranslatableString.aClass120_1249.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1250.getTranslation(0))) {
				i_252_ = 7;
				string = string.substring(TranslatableString.aClass120_1250.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1251.getTranslation(0))) {
				i_252_ = 8;
				string = string.substring(TranslatableString.aClass120_1251.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1252.getTranslation(0))) {
				i_252_ = 9;
				string = string.substring(TranslatableString.aClass120_1252.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1253.getTranslation(0))) {
				i_252_ = 10;
				string = string.substring(TranslatableString.aClass120_1253.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1254.getTranslation(0))) {
				i_252_ = 11;
				string = string.substring(TranslatableString.aClass120_1254.getTranslation(0).length());
			} else if (Class394.langID != 0) {
				if (string_251_.startsWith(TranslatableString.aClass120_1243.getTranslation(Class394.langID))) {
					i_252_ = 0;
					string = string.substring(TranslatableString.aClass120_1243.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1244.getTranslation(Class394.langID))) {
					i_252_ = 1;
					string = string.substring(TranslatableString.aClass120_1244.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1245.getTranslation(Class394.langID))) {
					i_252_ = 2;
					string = string.substring(TranslatableString.aClass120_1245.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1246.getTranslation(Class394.langID))) {
					i_252_ = 3;
					string = string.substring(TranslatableString.aClass120_1246.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1247.getTranslation(Class394.langID))) {
					i_252_ = 4;
					string = string.substring(TranslatableString.aClass120_1247.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1248.getTranslation(Class394.langID))) {
					i_252_ = 5;
					string = string.substring(TranslatableString.aClass120_1248.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1249.getTranslation(Class394.langID))) {
					i_252_ = 6;
					string = string.substring(TranslatableString.aClass120_1249.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1250.getTranslation(Class394.langID))) {
					i_252_ = 7;
					string = string.substring(TranslatableString.aClass120_1250.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1251.getTranslation(Class394.langID))) {
					i_252_ = 8;
					string = string.substring(TranslatableString.aClass120_1251.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1252.getTranslation(Class394.langID))) {
					i_252_ = 9;
					string = string.substring(TranslatableString.aClass120_1252.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1253.getTranslation(Class394.langID))) {
					i_252_ = 10;
					string = string.substring(TranslatableString.aClass120_1253.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1254.getTranslation(Class394.langID))) {
					i_252_ = 11;
					string = string.substring(TranslatableString.aClass120_1254.getTranslation(Class394.langID).length());
				}
			}
			string_251_ = string.toLowerCase();
			int i_253_ = 0;
			if (string_251_.startsWith(TranslatableString.aClass120_1255.getTranslation(0))) {
				i_253_ = 1;
				string = string.substring(TranslatableString.aClass120_1255.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1256.getTranslation(0))) {
				i_253_ = 2;
				string = string.substring(TranslatableString.aClass120_1256.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1257.getTranslation(0))) {
				i_253_ = 3;
				string = string.substring(TranslatableString.aClass120_1257.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1258.getTranslation(0))) {
				i_253_ = 4;
				string = string.substring(TranslatableString.aClass120_1258.getTranslation(0).length());
			} else if (string_251_.startsWith(TranslatableString.aClass120_1259.getTranslation(0))) {
				i_253_ = 5;
				string = string.substring(TranslatableString.aClass120_1259.getTranslation(0).length());
			} else if (Class394.langID != 0) {
				if (string_251_.startsWith(TranslatableString.aClass120_1255.getTranslation(Class394.langID))) {
					i_253_ = 1;
					string = string.substring(TranslatableString.aClass120_1255.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1256.getTranslation(Class394.langID))) {
					i_253_ = 2;
					string = string.substring(TranslatableString.aClass120_1256.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1257.getTranslation(Class394.langID))) {
					i_253_ = 3;
					string = string.substring(TranslatableString.aClass120_1257.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1258.getTranslation(Class394.langID))) {
					i_253_ = 4;
					string = string.substring(TranslatableString.aClass120_1258.getTranslation(Class394.langID).length());
				} else if (string_251_.startsWith(TranslatableString.aClass120_1259.getTranslation(Class394.langID))) {
					i_253_ = 5;
					string = string.substring(TranslatableString.aClass120_1259.getTranslation(Class394.langID).length());
				}
			}
			Connection class204 = Class296_Sub51_Sub13.method3111(true);
			Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 120, Class296_Sub51_Sub38.aClass311_6540);
			class296_sub1.out.p1(0);
			int i_254_ = class296_sub1.out.pos;
			class296_sub1.out.p1(i_252_);
			class296_sub1.out.p1(i_253_);
			Class86.method826(string, 0, class296_sub1.out);
			class296_sub1.out.method2604(class296_sub1.out.pos - i_254_);
			class204.sendPacket(class296_sub1, (byte) 119);
		}
	}

	static final void method1529(int i, boolean bool) {
		/* empty */
	}

	private static final void method1530(InterfaceComponent class51) {
		if (class51 != null) {
			if (class51.anInt592 != -1) {
				InterfaceComponent class51_255_ = InterfaceComponent.getInterfaceComponent(class51.parentId);
				if (class51_255_ != null) {
					if (class51_255_.aClass51Array538 == class51_255_.aClass51Array555) {
						class51_255_.aClass51Array538 = new InterfaceComponent[class51_255_.aClass51Array555.length];
						class51_255_.aClass51Array538[class51_255_.aClass51Array538.length - 1] = class51;
						ArrayTools.removeElement(class51_255_.aClass51Array555, 0, class51_255_.aClass51Array538, 0, class51.anInt592);
						ArrayTools.removeElement(class51_255_.aClass51Array555, class51.anInt592 + 1, class51_255_.aClass51Array538, class51.anInt592, class51_255_.aClass51Array555.length - class51.anInt592 - 1);
					} else {
						int i = 0;
						InterfaceComponent[] class51s;
						for (class51s = class51_255_.aClass51Array538; i < class51s.length; i++) {
							if (class51s[i] == class51) {
								break;
							}
						}
						if (i < class51s.length) {
							ArrayTools.removeElement(class51s, i + 1, class51s, i, class51s.length - i - 1);
							class51s[class51_255_.aClass51Array538.length - 1] = class51;
						}
					}
				}
			} else {
				int i = class51.uid >>> 16;
				InterfaceComponent[] class51s = Class338_Sub10.aClass51ArrayArray5272[i];
				if (class51s == null) {
					InterfaceComponent[] class51s_256_ = Class192.openedInterfaceComponents[i];
					int i_257_ = class51s_256_.length;
					class51s = Class338_Sub10.aClass51ArrayArray5272[i] = new InterfaceComponent[i_257_];
					ArrayTools.removeElement(class51s_256_, 0, class51s, 0, class51s_256_.length);
				}
				int i_258_;
				for (i_258_ = 0; i_258_ < class51s.length; i_258_++) {
					if (class51s[i_258_] == class51) {
						break;
					}
				}
				if (i_258_ < class51s.length) {
					ArrayTools.removeElement(class51s, i_258_ + 1, class51s, i_258_, class51s.length - i_258_ - 1);
					class51s[class51s.length - 1] = class51;
				}
			}
		}
	}

	private static final void parseLow(int i, boolean bool) {
		if (i < 300) {
			if (i == 150) {
				intStackSize -= 3;
				int i_259_ = intStack[intStackSize];
				int i_260_ = intStack[intStackSize + 1];
				int i_261_ = intStack[intStackSize + 2];
				if (i_260_ == 0) {
					throw new RuntimeException();
				}
				InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(i_259_);
				if (class51.aClass51Array555 == null) {
					class51.aClass51Array555 = new InterfaceComponent[i_261_ + 1];
					class51.aClass51Array538 = class51.aClass51Array555;
				}
				if (class51.aClass51Array555.length <= i_261_) {
					if (class51.aClass51Array555 == class51.aClass51Array538) {
						InterfaceComponent[] class51s = new InterfaceComponent[i_261_ + 1];
						for (int i_262_ = 0; i_262_ < class51.aClass51Array555.length; i_262_++) {
							class51s[i_262_] = class51.aClass51Array555[i_262_];
						}
						class51.aClass51Array555 = class51.aClass51Array538 = class51s;
					} else {
						InterfaceComponent[] class51s = new InterfaceComponent[i_261_ + 1];
						InterfaceComponent[] class51s_263_ = new InterfaceComponent[i_261_ + 1];
						for (int i_264_ = 0; i_264_ < class51.aClass51Array555.length; i_264_++) {
							class51s[i_264_] = class51.aClass51Array555[i_264_];
							class51s_263_[i_264_] = class51.aClass51Array538[i_264_];
						}
						class51.aClass51Array555 = class51s;
						class51.aClass51Array538 = class51s_263_;
					}
				}
				if (i_261_ > 0 && class51.aClass51Array555[i_261_ - 1] == null) {
					throw new RuntimeException("Gap at:" + (i_261_ - 1));
				}
				InterfaceComponent class51_265_ = new InterfaceComponent();
				class51_265_.type = i_260_;
				class51_265_.parentId = class51_265_.uid = class51.uid;
				class51_265_.anInt592 = i_261_;
				class51.aClass51Array555[i_261_] = class51_265_;
				if (class51.aClass51Array538 != class51.aClass51Array555) {
					class51.aClass51Array538[i_261_] = class51_265_;
				}
				if (bool) {
					aClass51_1537 = class51_265_;
				} else {
					aClass51_1528 = class51_265_;
				}
				Class332.method3416(class51, (byte) 89);
				return;
			}
			if (i == 151) {
				InterfaceComponent class51 = bool ? aClass51_1537 : aClass51_1528;
				if (class51.anInt592 == -1) {
					if (bool) {
						throw new RuntimeException("Tried to .cc_delete static .active-component!");
					}
					throw new RuntimeException("Tried to cc_delete static active-component!");
				}
				InterfaceComponent class51_266_ = InterfaceComponent.getInterfaceComponent(class51.uid);
				class51_266_.aClass51Array555[class51.anInt592] = null;
				Class332.method3416(class51_266_, (byte) 114);
				return;
			}
			if (i == 152) {
				InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
				class51.aClass51Array555 = null;
				class51.aClass51Array538 = null;
				Class332.method3416(class51, (byte) 112);
				return;
			}
			if (i == 200) {
				intStackSize -= 2;
				int i_267_ = intStack[intStackSize];
				int i_268_ = intStack[intStackSize + 1];
				InterfaceComponent class51 = Class103.method894(0, i_267_, i_268_);
				if (class51 != null && i_268_ != -1) {
					intStack[intStackSize++] = 1;
					if (bool) {
						aClass51_1537 = class51;
					} else {
						aClass51_1528 = class51;
					}
				} else {
					intStack[intStackSize++] = 0;
					return;
				}
				return;
			}
			if (i == 201) {
				int i_269_ = intStack[--intStackSize];
				InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(i_269_);
				if (class51 != null) {
					intStack[intStackSize++] = 1;
					if (bool) {
						aClass51_1537 = class51;
					} else {
						aClass51_1528 = class51;
					}
				} else {
					intStack[intStackSize++] = 0;
					return;
				}
				return;
			}
			if (i == 202 || i == 204) {
				InterfaceComponent class51;
				if (i == 202) {
					int i_270_ = intStack[--intStackSize];
					class51 = InterfaceComponent.getInterfaceComponent(i_270_);
				} else {
					class51 = bool ? aClass51_1537 : aClass51_1528;
				}
				method1530(class51);
				return;
			}
			if (i == 203 || i == 205) {
				InterfaceComponent class51;
				if (i == 203) {
					int i_271_ = intStack[--intStackSize];
					class51 = InterfaceComponent.getInterfaceComponent(i_271_);
				} else {
					class51 = bool ? aClass51_1537 : aClass51_1528;
				}
				method1538(class51);
				return;
			}
		} else if (i < 500) {
			if (i == 403) {
				intStackSize -= 2;
				int i_272_ = intStack[intStackSize];
				int i_273_ = intStack[intStackSize + 1];
				if (Class296_Sub51_Sub11.localPlayer.composite != null) {
					for (int i_274_ = 0; i_274_ < Class163.anIntArray1678.length; i_274_++) {
						if (Class163.anIntArray1678[i_274_] == i_272_) {
							Class296_Sub51_Sub11.localPlayer.composite.setBodyAccessoryOnBody(Class296_Sub51_Sub13.aClass24_6420, i_274_, i_273_);
							return;
						}
					}
					for (int i_275_ = 0; i_275_ < Class212.anIntArray2103.length; i_275_++) {
						if (Class212.anIntArray2103[i_275_] == i_272_) {
							Class296_Sub51_Sub11.localPlayer.composite.setBodyAccessoryOnBody(Class296_Sub51_Sub13.aClass24_6420, i_275_, i_273_);
							break;
						}
					}
					return;
				}
				return;
			}
			if (i == 404) {
				intStackSize -= 2;
				int i_276_ = intStack[intStackSize];
				int i_277_ = intStack[intStackSize + 1];
				if (Class296_Sub51_Sub11.localPlayer.composite != null) {
					Class296_Sub51_Sub11.localPlayer.composite.setBodyColor(i_276_, i_277_);
					return;
				}
				return;
			}
			if (i == 410) {
				boolean bool_278_ = intStack[--intStackSize] != 0;
				if (Class296_Sub51_Sub11.localPlayer.composite != null) {
					Class296_Sub51_Sub11.localPlayer.composite.setGender(bool_278_);
					return;
				}
				return;
			}
			if (i == 411) {
				intStackSize -= 2;
				int i_279_ = intStack[intStackSize];
				int i_280_ = intStack[intStackSize + 1];
				if (Class296_Sub51_Sub11.localPlayer.composite != null) {
					Class296_Sub51_Sub11.localPlayer.composite.setItemOnBody(i_279_, i_280_, Class296_Sub39_Sub1.itemDefinitionLoader);
					return;
				}
				return;
			}
		} else if (i >= 1000 && i < 1100 || i >= 2000 && i < 2100) {
			InterfaceComponent class51;
			if (i >= 2000) {
				i -= 1000;
				class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
			} else {
				class51 = bool ? aClass51_1537 : aClass51_1528;
			}
			if (i == 1000) {
				intStackSize -= 4;
				class51.locX = intStack[intStackSize];
				class51.locY = intStack[intStackSize + 1];
				int i_281_ = intStack[intStackSize + 2];
				if (i_281_ < 0) {
					i_281_ = 0;
				} else if (i_281_ > 5) {
					i_281_ = 5;
				}
				int i_282_ = intStack[intStackSize + 3];
				if (i_282_ < 0) {
					i_282_ = 0;
				} else if (i_282_ > 5) {
					i_282_ = 5;
				}
				class51.aByte626 = (byte) i_281_;
				class51.aByte595 = (byte) i_282_;
				Class332.method3416(class51, (byte) 81);
				Class189.method1894(true, class51);
				if (class51.anInt592 == -1) {
					Class220.method2074(class51.uid, true);
				}
				return;
			}
			if (i == 1001) {
				intStackSize -= 4;
				class51.width = intStack[intStackSize];
				class51.height = intStack[intStackSize + 1];
				class51.anInt516 = 0;
				class51.anInt486 = 0;
				int i_283_ = intStack[intStackSize + 2];
				if (i_283_ < 0) {
					i_283_ = 0;
				} else if (i_283_ > 4) {
					i_283_ = 4;
				}
				int i_284_ = intStack[intStackSize + 3];
				if (i_284_ < 0) {
					i_284_ = 0;
				} else if (i_284_ > 4) {
					i_284_ = 4;
				}
				class51.aByte552 = (byte) i_283_;
				class51.aByte604 = (byte) i_284_;
				Class332.method3416(class51, (byte) 109);
				Class189.method1894(true, class51);
				if (class51.type == 0) {
					Class61.method695(false, -126, class51);
				}
				return;
			}
			if (i == 1003) {
				boolean bool_285_ = intStack[--intStackSize] == 1;
				if (class51.hiden != bool_285_) {
					class51.hiden = bool_285_;
					Class332.method3416(class51, (byte) 114);
				}
				if (class51.anInt592 == -1) {
					Class238.method2139(7, class51.uid);
				}
				return;
			}
			if (i == 1004) {
				intStackSize -= 2;
				class51.anInt566 = intStack[intStackSize];
				class51.anInt530 = intStack[intStackSize + 1];
				Class332.method3416(class51, (byte) 126);
				Class189.method1894(true, class51);
				if (class51.type == 0) {
					Class61.method695(false, -47, class51);
				}
				return;
			}
			if (i == 1005) {
				class51.aBoolean610 = intStack[--intStackSize] == 1;
				return;
			}
		} else if (i >= 1100 && i < 1200 || i >= 2100 && i < 2200) {
			InterfaceComponent component;
			if (i >= 2000) {
				i -= 1000;
				component = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
			} else {
				component = bool ? aClass51_1537 : aClass51_1528;
			}
			if (i == 1100) {
				intStackSize -= 2;
				component.anInt624 = intStack[intStackSize];
				if (component.anInt624 > component.scrollMaxH - component.anInt578) {
					component.anInt624 = component.scrollMaxH - component.anInt578;
				}
				if (component.anInt624 < 0) {
					component.anInt624 = 0;
				}
				component.anInt632 = intStack[intStackSize + 1];
				if (component.anInt632 > component.scrollMaxW - component.anInt623) {
					component.anInt632 = component.scrollMaxW - component.anInt623;
				}
				if (component.anInt632 < 0) {
					component.anInt632 = 0;
				}
				Class332.method3416(component, (byte) 90);
				if (component.anInt592 == -1) {
					Class182.method1844(12, component.uid);
				}
				return;
			}
			if (i == 1101) {
				component.textColour = intStack[--intStackSize];
				Class332.method3416(component, (byte) 108);
				if (component.anInt592 == -1) {
					Class246.method2187(component.uid, 0);
				}
				return;
			}
			if (i == 1102) {
				component.isFilled = intStack[--intStackSize] == 1;
				Class332.method3416(component, (byte) 79);
				return;
			}
			if (i == 1103) {
				component.anInt517 = intStack[--intStackSize];
				Class332.method3416(component, (byte) 76);
				return;
			}
			if (i == 1104) {
				component.anInt476 = intStack[--intStackSize];
				Class332.method3416(component, (byte) 82);
				return;
			}
			if (i == 1105) {
				int i_286_ = intStack[--intStackSize];
				if (component.textureID != i_286_) {
					component.textureID = i_286_;
					Class332.method3416(component, (byte) 99);
				}
				if (component.anInt592 == -1) {
					StaticMethods.method2470(component.uid, -1);
				}
				return;
			}
			if (i == 1106) {
				component.anInt573 = intStack[--intStackSize];
				Class332.method3416(component, (byte) 125);
				return;
			}
			if (i == 1107) {
				component.aBoolean562 = intStack[--intStackSize] == 1;
				Class332.method3416(component, (byte) 94);
				return;
			}
			if (i == 1108) {
				component.modelType = 1;
				component.modelID = intStack[--intStackSize];
				Class332.method3416(component, (byte) 84);
				if (component.anInt592 == -1) {
					Class198.method1945(4, component.uid);
				}
				return;
			}
			if (i == 1109) {
				intStackSize -= 6;
				component.anInt588 = intStack[intStackSize];
				component.anInt581 = intStack[intStackSize + 1];
				component.anInt612 = intStack[intStackSize + 2];
				component.anInt515 = intStack[intStackSize + 3];
				component.anInt548 = intStack[intStackSize + 4];
				component.anInt520 = intStack[intStackSize + 5];
				Class332.method3416(component, (byte) 78);
				if (component.anInt592 == -1) {
					Class338_Sub2.method3456(true, component.uid);
					Class355_Sub1.method3700(component.uid, true);
				}
				return;
			}
			if (i == 1110) {
				int i_287_ = intStack[--intStackSize];
				if (i_287_ != component.anInt497) {
					if (i_287_ != -1) {
						if (component.aClass44_554 == null) {
							component.aClass44_554 = new Class44_Sub2();
						}
						component.aClass44_554.method549((byte) 115, i_287_);
					} else {
						component.aClass44_554 = null;
					}
					component.anInt497 = i_287_;
					Class332.method3416(component, (byte) 87);
				}
				if (component.anInt592 == -1) {
					Class224.method2080(-25672, component.uid);
				}
				return;
			}
			if (i == 1111) {
				component.aBoolean507 = intStack[--intStackSize] == 1;
				Class332.method3416(component, (byte) 111);
				return;
			}
			if (i == 1112) {
				String string = stringStack[--stringStackSize];
				// System.out.println("1112 string = " + string);
				if (!string.equals(component.text)) {
					component.text = string;
					Class332.method3416(component, (byte) 105);
					method2527(component);
				}
				if (component.anInt592 == -1) {
					Class286.method2382(component.uid, 1);
				}
				return;
			}
			if (i == 1113) {
				component.fontID = intStack[--intStackSize];
				Class332.method3416(component, (byte) 73);
				if (component.anInt592 == -1) {
					Class48.method601(component.uid, 100);
				}
				return;
			}
			if (i == 1114) {
				intStackSize -= 3;
				component.anInt480 = intStack[intStackSize];
				component.anInt600 = intStack[intStackSize + 1];
				component.anInt589 = intStack[intStackSize + 2];
				Class332.method3416(component, (byte) 77);
				return;
			}
			if (i == 1115) {
				component.isInventory = intStack[--intStackSize] == 1;
				Class332.method3416(component, (byte) 101);
				return;
			}
			if (i == 1116) {
				component.borderThickness = intStack[--intStackSize];
				Class332.method3416(component, (byte) 124);
				return;
			}
			if (i == 1117) {
				component.shadowColor = intStack[--intStackSize];
				Class332.method3416(component, (byte) 93);
				return;
			}
			if (i == 1118) {
				component.flippedVertically = intStack[--intStackSize] == 1;
				Class332.method3416(component, (byte) 105);
				return;
			}
			if (i == 1119) {
				component.flippedHorizontally = intStack[--intStackSize] == 1;
				Class332.method3416(component, (byte) 103);
				return;
			}
			if (i == 1120) {
				intStackSize -= 2;
				component.scrollMaxH = intStack[intStackSize];
				component.scrollMaxW = intStack[intStackSize + 1];
				Class332.method3416(component, (byte) 74);
				if (component.type == 0) {
					Class61.method695(false, -100, component);
				}
				return;
			}
			if (i == 1122) {
				component.aBoolean546 = intStack[--intStackSize] == 1;
				Class332.method3416(component, (byte) 105);
				return;
			}
			if (i == 1123) {
				component.anInt520 = intStack[--intStackSize];
				Class332.method3416(component, (byte) 83);
				if (component.anInt592 == -1) {
					Class338_Sub2.method3456(true, component.uid);
				}
				return;
			}
			if (i == 1124) {
				int i_288_ = intStack[--intStackSize];
				component.aBoolean514 = i_288_ == 1;
				Class332.method3416(component, (byte) 127);
				return;
			}
			if (i == 1125) {
				intStackSize -= 2;
				component.anInt591 = intStack[intStackSize];
				component.anInt487 = intStack[intStackSize + 1];
				Class332.method3416(component, (byte) 93);
				return;
			}
			if (i == 1126) {
				component.anInt510 = intStack[--intStackSize];
				Class332.method3416(component, (byte) 111);
				return;
			}
			if (i == 1127) {
				intStackSize -= 2;
				int i_289_ = intStack[intStackSize];
				int i_290_ = intStack[intStackSize + 1];
				ParamType class383 = Class296_Sub22.itemExtraDataDefinitionLoader.list(i_289_);
				if (i_290_ != class383.defaultValue) {
					component.method628(20, i_289_, i_290_);
				} else {
					component.method618(0, i_289_);
					return;
				}
				return;
			}
			if (i == 1128) {
				int i_291_ = intStack[--intStackSize];
				String string = stringStack[--stringStackSize];
				ParamType class383 = Class296_Sub22.itemExtraDataDefinitionLoader.list(i_291_);
				if (!class383.name.equals(string)) {
					component.method624(i_291_, string, (byte) 64);
				} else {
					component.method618(0, i_291_);
					return;
				}
				return;
			}
			if (i == 1129 || i == 1130) {
				int i_292_ = intStack[--intStackSize];
				if ((component.type == 5 || i != 1129) && (component.type == 4 || i != 1130)) {
					if (component.anInt558 != i_292_) {
						component.anInt558 = i_292_;
						Class332.method3416(component, (byte) 93);
					}
					if (component.anInt592 == -1) {
						Class288.method2396(component.uid, 114);
					}
					return;
				}
				return;
			}
			if (i == 1131) {
				intStackSize -= 3;
				int i_293_ = intStack[intStackSize];
				short i_294_ = (short) intStack[intStackSize + 1];
				short i_295_ = (short) intStack[intStackSize + 2];
				if (i_293_ >= 0 && i_293_ < 5) {
					component.method619(i_293_, (byte) 20, i_295_, i_294_);
					Class332.method3416(component, (byte) 126);
					if (component.anInt592 == -1) {
						Class29.method322(component.uid, i_293_, 0);
					}
					return;
				}
				return;
			}
			if (i == 1132) {
				intStackSize -= 3;
				int i_296_ = intStack[intStackSize];
				short i_297_ = (short) intStack[intStackSize + 1];
				short i_298_ = (short) intStack[intStackSize + 2];
				if (i_296_ >= 0 && i_296_ < 5) {
					component.method638(i_298_, i_297_, (byte) -34, i_296_);
					Class332.method3416(component, (byte) 119);
					if (component.anInt592 == -1) {
						BillboardRaw.method882(i_296_, (byte) -96, component.uid);
					}
					return;
				}
				return;
			}
			if (i == 1133) {
				component.aBoolean617 = intStack[--intStackSize] == 1;
				Class332.method3416(component, (byte) 82);
				if (component.anInt592 == -1) {
					Class296_Sub32.method2712(component.uid);
				}
				return;
			}
			if (i == 1134) {
				intStackSize -= 2;
				int i_299_ = intStack[intStackSize];
				int i_300_ = intStack[intStackSize + 1];
				ParamType class383 = Class296_Sub22.itemExtraDataDefinitionLoader.list(i_299_);
				if (i_300_ != class383.defaultValue) {
					component.method628(33, i_299_, i_300_);
				} else {
					component.method618(0, i_299_);
					return;
				}
				return;
			}
			if (i == 1135) {
				component.aBoolean550 = intStack[--intStackSize] == 1;
				Class332.method3416(component, (byte) 91);
				if (component.anInt592 == -1) {
					Class31.method338(component.uid, -2);
				}
				return;
			}
		} else if (i >= 1200 && i < 1300 || i >= 2200 && i < 2300) {
			InterfaceComponent class51;
			if (i >= 2000) {
				i -= 1000;
				class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
			} else {
				class51 = bool ? aClass51_1537 : aClass51_1528;
			}
			Class332.method3416(class51, (byte) 93);
			if (i == 1200 || i == 1205 || i == 1208 || i == 1209 || i == 1212 || i == 1213) {
				intStackSize -= 2;
				int i_301_ = intStack[intStackSize];
				int i_302_ = intStack[intStackSize + 1];
				if (class51.anInt592 == -1) {
					ISAACCipher.method1862(class51.uid, -120);
					Class338_Sub2.method3456(true, class51.uid);
					Class355_Sub1.method3700(class51.uid, true);
				}
				if (i_301_ == -1) {
					class51.modelType = 1;
					class51.modelID = -1;
					class51.clickedItem = -1;
				} else {
					class51.clickedItem = i_301_;
					class51.anInt511 = i_302_;
					if (i == 1208 || i == 1209) {
						class51.aBoolean613 = true;
					} else {
						class51.aBoolean613 = false;
					}
					ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_301_);
					class51.anInt612 = class158.xAngle2d;
					class51.anInt515 = class158.yAngle2d;
					class51.anInt548 = class158.zAngle2d;
					class51.anInt588 = class158.xOffest2d;
					class51.anInt581 = class158.yOffest2d;
					class51.anInt520 = class158.zoom2d;
					if (i == 1205 || i == 1209) {
						class51.anInt490 = 0;
					} else if (i == 1212 || i == 1213) {
						class51.anInt490 = 1;
					} else {
						class51.anInt490 = 2;
					}
					if (class51.anInt516 > 0) {
						class51.anInt520 = class51.anInt520 * 32 / class51.anInt516;
					} else {
						if (class51.width > 0) {
							class51.anInt520 = class51.anInt520 * 32 / class51.width;
						}
						return;
					}
					return;
				}
				return;
			}
			if (i == 1201) {
				class51.modelType = 2;
				class51.modelID = intStack[--intStackSize];
				if (class51.anInt592 == -1) {
					Class198.method1945(4, class51.uid);
				}
				return;
			}
			if (i == 1202) {
				class51.modelType = 3;
				class51.modelID = -1;
				if (class51.anInt592 == -1) {
					Class198.method1945(4, class51.uid);
				}
				return;
			}
			if (i == 1203) {
				class51.modelType = 6;
				class51.modelID = intStack[--intStackSize];
				if (class51.anInt592 == -1) {
					Class198.method1945(4, class51.uid);
				}
				return;
			}
			if (i == 1204) {
				class51.modelType = 5;
				class51.modelID = intStack[--intStackSize];
				if (class51.anInt592 == -1) {
					Class198.method1945(4, class51.uid);
				}
				return;
			}
			if (i == 1206) {
				intStackSize -= 4;
				class51.anInt542 = intStack[intStackSize];
				class51.anInt571 = intStack[intStackSize + 1];
				class51.anInt488 = intStack[intStackSize + 2];
				class51.anInt606 = intStack[intStackSize + 3];
				Class332.method3416(class51, (byte) 98);
				return;
			}
			if (i == 1207) {
				intStackSize -= 2;
				class51.anInt536 = intStack[intStackSize];
				class51.anInt590 = intStack[intStackSize + 1];
				Class332.method3416(class51, (byte) 122);
				return;
			}
			if (i == 1210) {
				intStackSize -= 4;
				class51.modelID = intStack[intStackSize];
				class51.anInt545 = intStack[intStackSize + 1];
				if (intStack[intStackSize + 2] == 1) {
					class51.modelType = 9;
				} else {
					class51.modelType = 8;
				}
				if (intStack[intStackSize + 3] == 1) {
					class51.aBoolean613 = true;
				} else {
					class51.aBoolean613 = false;
				}
				if (class51.anInt592 == -1) {
					Class198.method1945(4, class51.uid);
				}
				return;
			}
			if (i == 1211) {
				class51.modelType = 5;
				class51.modelID = Class362.myPlayerIndex;
				class51.anInt545 = 0;
				if (class51.anInt592 == -1) {
					Class198.method1945(4, class51.uid);
				}
				return;
			}
		} else if (i >= 1300 && i < 1400 || i >= 2300 && i < 2400) {
			InterfaceComponent class51;
			if (i >= 2000) {
				i -= 1000;
				class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
			} else {
				class51 = bool ? aClass51_1537 : aClass51_1528;
			}
			if (i == 1300) {
				int i_303_ = intStack[--intStackSize] - 1;
				if (i_303_ < 0 || i_303_ > 9) {
					stringStackSize--;
				} else {
					class51.method621(i_303_, stringStack[--stringStackSize], (byte) -88);
					return;
				}
				return;
			}
			if (i == 1301) {
				intStackSize -= 2;
				int i_304_ = intStack[intStackSize];
				int i_305_ = intStack[intStackSize + 1];
				if (i_304_ == -1 && i_305_ == -1) {
					class51.aClass51_607 = null;
				} else {
					class51.aClass51_607 = Class103.method894(0, i_304_, i_305_);
					return;
				}
				return;
			}
			if (i == 1302) {
				int i_306_ = intStack[--intStackSize];
				if (i_306_ == Class225.anInt2181 || i_306_ == Class296_Sub51_Sub31.anInt6504 || i_306_ == Class303.anInt2725) {
					class51.anInt622 = i_306_;
					return;
				}
				return;
			}
			if (i == 1303) {
				class51.anInt579 = intStack[--intStackSize];
				return;
			}
			if (i == 1304) {
				class51.anInt523 = intStack[--intStackSize];
				return;
			}
			if (i == 1305) {
				class51.componentName = stringStack[--stringStackSize];
				return;
			}
			if (i == 1306) {
				class51.selectedAction = stringStack[--stringStackSize];
				return;
			}
			if (i == 1307) {
				class51.actions = null;
				return;
			}
			if (i == 1308) {
				class51.anInt501 = intStack[--intStackSize];
				class51.anInt519 = intStack[--intStackSize];
				return;
			}
			if (i == 1309) {
				int i_307_ = intStack[--intStackSize];
				int i_308_ = intStack[--intStackSize];
				if (i_308_ >= 1 && i_308_ <= 10) {
					class51.method635(i_308_ - 1, i_307_, (byte) 64);
				}
				return;
			}
			if (i == 1310) {
				class51.aString577 = stringStack[--stringStackSize];
				return;
			}
			if (i == 1311) {
				class51.anInt576 = intStack[--intStackSize];
				return;
			}
			if (i == 1312 || i == 1313) {
				int i_309_;
				int i_310_;
				int i_311_;
				if (i == 1312) {
					intStackSize -= 3;
					i_309_ = intStack[intStackSize] - 1;
					i_310_ = intStack[intStackSize + 1];
					i_311_ = intStack[intStackSize + 2];
					if (i_309_ < 0 || i_309_ > 9) {
						throw new RuntimeException("IOR13121313");
					}
				} else {
					intStackSize -= 2;
					i_309_ = 10;
					i_310_ = intStack[intStackSize];
					i_311_ = intStack[intStackSize + 1];
				}
				if (class51.aByteArray508 == null) {
					if (i_310_ != 0) {
						class51.aByteArray508 = new byte[11];
						class51.aByteArray495 = new byte[11];
						class51.anIntArray572 = new int[11];
					} else {
						return;
					}
				}
				class51.aByteArray508[i_309_] = (byte) i_310_;
				if (i_310_ != 0) {
					class51.aBoolean482 = true;
				} else {
					class51.aBoolean482 = false;
					for (byte element : class51.aByteArray508) {
						if (element != 0) {
							class51.aBoolean482 = true;
							break;
						}
					}
				}
				class51.aByteArray495[i_309_] = (byte) i_311_;
				return;
			}
			if (i == 1314) {
				class51.anInt611 = intStack[--intStackSize];
				return;
			}
		} else {
			if (i >= 1400 && i < 1500 || i >= 2400 && i < 2500) {
				InterfaceComponent class51;
				if (i >= 2000) {
					i -= 1000;
					class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
				} else {
					class51 = bool ? aClass51_1537 : aClass51_1528;
				}
				if (i == 1499) {
					class51.method630(4);
				} else {
					String string = stringStack[--stringStackSize];
					int[] is = null;
					if (string.length() > 0 && string.charAt(string.length() - 1) == 'Y') {
						int i_313_ = intStack[--intStackSize];
						if (i_313_ > 0) {
							is = new int[i_313_];
							while (i_313_-- > 0) {
								is[i_313_] = intStack[--intStackSize];
							}
						}
						string = string.substring(0, string.length() - 1);
					}
					Object[] objects = new Object[string.length() + 1];
					for (int i_314_ = objects.length - 1; i_314_ >= 1; i_314_--) {
						if (string.charAt(i_314_ - 1) == 's') {
							objects[i_314_] = stringStack[--stringStackSize];
						} else if (string.charAt(i_314_ - 1) == '\u00a7') {
							objects[i_314_] = new Long(longStack[--longStackSize]);
						} else {
							objects[i_314_] = new Integer(intStack[--intStackSize]);
						}
					}
					int i_315_ = intStack[--intStackSize];
					if (i_315_ != -1) {
						objects[0] = new Integer(i_315_);
					} else {
						objects = null;
					}
					if (i == 1400) {
						class51.anObjectArray563 = objects;
					} else if (i == 1401) {
						class51.anObjectArray582 = objects;
					} else if (i == 1402) {
						class51.anObjectArray537 = objects;
					} else if (i == 1403) {
						class51.anObjectArray584 = objects;
					} else if (i == 1404) {
						class51.anObjectArray503 = objects;
					} else if (i == 1405) {
						class51.anObjectArray570 = objects;
					} else if (i == 1406) {
						class51.anObjectArray557 = objects;
					} else if (i == 1407) {
						class51.anObjectArray594 = objects;
						class51.anIntArray484 = is;
					} else if (i == 1408) {
						class51.anObjectArray540 = objects;
					} else if (i == 1409) {
						class51.anObjectArray585 = objects;
					} else if (i == 1410) {
						class51.anObjectArray500 = objects;
					} else if (i == 1411) {
						class51.anObjectArray531 = objects;
					} else if (i == 1412) {
						class51.anObjectArray608 = objects;
					} else if (i == 1414) {
						class51.anObjectArray541 = objects;
						class51.anIntArray526 = is;
					} else if (i == 1415) {
						class51.anObjectArray477 = objects;
						class51.anIntArray625 = is;
					} else if (i == 1416) {
						class51.anObjectArray512 = objects;
					} else if (i == 1417) {
						class51.anObjectArray527 = objects;
					} else if (i == 1418) {
						class51.anObjectArray568 = objects;
					} else if (i == 1419) {
						class51.anObjectArray504 = objects;
					} else if (i == 1420) {
						class51.anObjectArray491 = objects;
					} else if (i == 1421) {
						class51.anObjectArray549 = objects;
					} else if (i == 1422) {
						class51.anObjectArray509 = objects;
					} else if (i == 1423) {
						class51.anObjectArray502 = objects;
					} else if (i == 1424) {
						class51.anObjectArray539 = objects;
					} else if (i == 1425) {
						class51.anObjectArray583 = objects;
					} else if (i == 1426) {
						class51.anObjectArray627 = objects;
					} else if (i == 1427) {
						class51.anObjectArray485 = objects;
					} else if (i == 1428) {
						class51.anObjectArray544 = objects;
						class51.anIntArray564 = is;
					} else if (i == 1429) {
						class51.anObjectArray525 = objects;
						class51.anIntArray619 = is;
					} else if (i == 1430) {
						class51.anObjectArray494 = objects;
					} else if (i == 1431) {
						class51.anObjectArray518 = objects;
					} else if (i == 1432) {
						class51.anObjectArray506 = objects;
					} else if (i == 1433) {
						class51.anObjectArray597 = objects;
					}
					class51.hasScripts = true;
					return;
				}
				return;
			}
			if (i < 1600) {
				InterfaceComponent class51 = bool ? aClass51_1537 : aClass51_1528;
				if (i == 1500) {
					intStack[intStackSize++] = class51.anInt614;
					return;
				}
				if (i == 1501) {
					intStack[intStackSize++] = class51.anInt605;
					return;
				}
				if (i == 1502) {
					intStack[intStackSize++] = class51.anInt578;
					return;
				}
				if (i == 1503) {
					intStack[intStackSize++] = class51.anInt623;
					return;
				}
				if (i == 1504) {
					intStack[intStackSize++] = class51.hiden ? 1 : 0;
					return;
				}
				if (i == 1505) {
					intStack[intStackSize++] = class51.parentId;
					return;
				}
				if (i == 1506) {
					InterfaceComponent class51_316_ = Class181_Sub1.method1834(class51, -2);
					intStack[intStackSize++] = class51_316_ == null ? -1 : class51_316_.uid;
					return;
				}
				if (i == 1507) {
					intStack[intStackSize++] = class51.textColour;
					return;
				}
			} else if (i < 1700) {
				InterfaceComponent class51 = bool ? aClass51_1537 : aClass51_1528;
				if (i == 1600) {
					intStack[intStackSize++] = class51.anInt624;
					return;
				}
				if (i == 1601) {
					intStack[intStackSize++] = class51.anInt632;
					return;
				}
				if (i == 1602) {
					stringStack[stringStackSize++] = class51.text;
					return;
				}
				if (i == 1603) {
					intStack[intStackSize++] = class51.scrollMaxH;
					return;
				}
				if (i == 1604) {
					intStack[intStackSize++] = class51.scrollMaxW;
					return;
				}
				if (i == 1605) {
					intStack[intStackSize++] = class51.anInt520;
					return;
				}
				if (i == 1606) {
					intStack[intStackSize++] = class51.anInt612;
					return;
				}
				if (i == 1607) {
					intStack[intStackSize++] = class51.anInt548;
					return;
				}
				if (i == 1608) {
					intStack[intStackSize++] = class51.anInt515;
					return;
				}
				if (i == 1609) {
					intStack[intStackSize++] = class51.anInt517;
					return;
				}
				if (i == 1610) {
					intStack[intStackSize++] = class51.anInt588;
					return;
				}
				if (i == 1611) {
					intStack[intStackSize++] = class51.anInt581;
					return;
				}
				if (i == 1612) {
					intStack[intStackSize++] = class51.textureID;
					return;
				}
				if (i == 1613) {
					int i_317_ = intStack[--intStackSize];
					ParamType class383 = Class296_Sub22.itemExtraDataDefinitionLoader.list(i_317_);
					if (class383.method4013(-125)) {
						stringStack[stringStackSize++] = class51.method632(i_317_, class383.name, (byte) 20);
					} else {
						intStack[intStackSize++] = class51.method631(i_317_, 82, class383.defaultValue);
						return;
					}
					return;
				}
				if (i == 1614) {
					intStack[intStackSize++] = class51.anInt573;
					return;
				}
				if (i == 2614) {
					intStack[intStackSize++] = class51.modelType == 1 ? class51.modelID : -1;
					return;
				}
			} else if (i < 1800) {
				InterfaceComponent class51 = bool ? aClass51_1537 : aClass51_1528;
				if (i == 1700) {
					intStack[intStackSize++] = class51.clickedItem;
					return;
				}
				if (i == 1701) {
					if (class51.clickedItem != -1) {
						intStack[intStackSize++] = class51.anInt511;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 1702) {
					intStack[intStackSize++] = class51.anInt592;
					return;
				}
			} else if (i < 1900) {
				InterfaceComponent class51 = bool ? aClass51_1537 : aClass51_1528;
				if (i == 1800) {
					intStack[intStackSize++] = GameClient.method115(class51).method2707(-102);
					return;
				}
				if (i == 1801) {
					int i_318_ = intStack[--intStackSize];
					i_318_--;
					if (class51.actions == null || i_318_ >= class51.actions.length || class51.actions[i_318_] == null) {
						stringStack[stringStackSize++] = "";
					} else {
						stringStack[stringStackSize++] = class51.actions[i_318_];
						return;
					}
					return;
				}
				if (i == 1802) {
					if (class51.componentName == null) {
						stringStack[stringStackSize++] = "";
					} else {
						stringStack[stringStackSize++] = class51.componentName;
						return;
					}
					return;
				}
			} else if (i < 2000 || i >= 2900 && i < 3000) {
				InterfaceComponent class51;
				if (i >= 2000) {
					class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
					i -= 1000;
				} else {
					class51 = bool ? aClass51_1537 : aClass51_1528;
				}
				if (anInt1546 >= 10) {
					throw new RuntimeException("C29xx-1");
				}
				if (i == 1927) {
					if (class51.anObjectArray485 != null) {
						CS2Call class296_sub46 = new CS2Call();
						class296_sub46.callerInterface = class51;
						class296_sub46.callArgs = class51.anObjectArray485;
						class296_sub46.anInt4955 = anInt1546 + 1;
						Class179.aClass155_1854.addLast((byte) 113, class296_sub46);
						return;
					}
					return;
				}
			} else if (i < 2600) {
				InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
				if (i == 2500) {
					intStack[intStackSize++] = class51.anInt614;
					return;
				}
				if (i == 2501) {
					intStack[intStackSize++] = class51.anInt605;
					return;
				}
				if (i == 2502) {
					intStack[intStackSize++] = class51.anInt578;
					return;
				}
				if (i == 2503) {
					intStack[intStackSize++] = class51.anInt623;
					return;
				}
				if (i == 2504) {
					intStack[intStackSize++] = class51.hiden ? 1 : 0;
					return;
				}
				if (i == 2505) {
					intStack[intStackSize++] = class51.parentId;
					return;
				}
				if (i == 2506) {
					InterfaceComponent class51_319_ = Class181_Sub1.method1834(class51, -2);
					intStack[intStackSize++] = class51_319_ == null ? -1 : class51_319_.uid;
					return;
				}
				if (i == 2507) {
					intStack[intStackSize++] = class51.textColour;
					return;
				}
			} else if (i < 2700) {
				InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
				if (i == 2600) {
					intStack[intStackSize++] = class51.anInt624;
					return;
				}
				if (i == 2601) {
					intStack[intStackSize++] = class51.anInt632;
					return;
				}
				if (i == 2602) {
					stringStack[stringStackSize++] = class51.text;
					return;
				}
				if (i == 2603) {
					intStack[intStackSize++] = class51.scrollMaxH;
					return;
				}
				if (i == 2604) {
					intStack[intStackSize++] = class51.scrollMaxW;
					return;
				}
				if (i == 2605) {
					intStack[intStackSize++] = class51.anInt520;
					return;
				}
				if (i == 2606) {
					intStack[intStackSize++] = class51.anInt612;
					return;
				}
				if (i == 2607) {
					intStack[intStackSize++] = class51.anInt548;
					return;
				}
				if (i == 2608) {
					intStack[intStackSize++] = class51.anInt515;
					return;
				}
				if (i == 2609) {
					intStack[intStackSize++] = class51.anInt517;
					return;
				}
				if (i == 2610) {
					intStack[intStackSize++] = class51.anInt588;
					return;
				}
				if (i == 2611) {
					intStack[intStackSize++] = class51.anInt581;
					return;
				}
				if (i == 2612) {
					intStack[intStackSize++] = class51.textureID;
					return;
				}
				if (i == 2613) {
					intStack[intStackSize++] = class51.anInt573;
					return;
				}
				if (i == 2614) {
					intStack[intStackSize++] = class51.modelType == 1 ? class51.modelID : -1;
					return;
				}
			} else if (i < 2800) {
				if (i == 2700) {
					InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
					intStack[intStackSize++] = class51.clickedItem;
					return;
				}
				if (i == 2701) {
					InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
					if (class51.clickedItem != -1) {
						intStack[intStackSize++] = class51.anInt511;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 2702) {
					int i_320_ = intStack[--intStackSize];
					Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.get(i_320_);
					if (class296_sub13 != null) {
						intStack[intStackSize++] = 1;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 2703) {
					InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
					if (class51.aClass51Array555 == null) {
						intStack[intStackSize++] = 0;
					} else {
						int i_321_ = class51.aClass51Array555.length;
						for (int i_322_ = 0; i_322_ < class51.aClass51Array555.length; i_322_++) {
							if (class51.aClass51Array555[i_322_] == null) {
								i_321_ = i_322_;
								break;
							}
						}
						intStack[intStackSize++] = i_321_;
						return;
					}
					return;
				}
				if (i == 2704 || i == 2705) {
					intStackSize -= 2;
					int i_323_ = intStack[intStackSize];
					int i_324_ = intStack[intStackSize + 1];
					Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.get(i_323_);
					if (class296_sub13 != null && class296_sub13.anInt4657 == i_324_) {
						intStack[intStackSize++] = 1;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
			} else if (i < 2900) {
				InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(intStack[--intStackSize]);
				if (i == 2800) {
					intStack[intStackSize++] = GameClient.method115(class51).method2707(-77);
					return;
				}
				if (i == 2801) {
					int i_325_ = intStack[--intStackSize];
					i_325_--;
					if (class51.actions == null || i_325_ >= class51.actions.length || class51.actions[i_325_] == null) {
						stringStack[stringStackSize++] = "";
					} else {
						stringStack[stringStackSize++] = class51.actions[i_325_];
						return;
					}
					return;
				}
				if (i == 2802) {
					if (class51.componentName == null) {
						stringStack[stringStackSize++] = "";
					} else {
						stringStack[stringStackSize++] = class51.componentName;
						return;
					}
					return;
				}
			} else if (i < 3200) {
				if (i == 3100) {
					String string = stringStack[--stringStackSize];
					StaticMethods.method2120(string, 65);
					return;
				}
				if (i == 3101) {
					intStackSize -= 2;
					Class41_Sub27.method507(intStack[intStackSize + 1], 1, Class296_Sub51_Sub11.localPlayer, intStack[intStackSize]);
					return;
				}
				if (i == 3103) {
					Class368_Sub23.method3869(true, -24);
					return;
				}
				if (i == 3104) {
					String string = stringStack[--stringStackSize];
					int i_326_ = 0;
					if (EquipmentData.method3305((byte) 94, string)) {
						i_326_ = Class366_Sub2.method3774(-104, string);
					}
					anInt1530++;
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 123, Class41_Sub4.aClass311_3750);
					class296_sub1.out.p4(i_326_);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					return;
				}
				if (i == 3105) {
					String string = stringStack[--stringStackSize];
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 107, Class366_Sub7.aClass311_5401);
					class296_sub1.out.p1(string.length() + 1);
					class296_sub1.out.writeString(string);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					return;
				}
				if (i == 3106) {
					String string = stringStack[--stringStackSize];
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 127, Class262.aClass311_2439);
					class296_sub1.out.p1(string.length() + 1);
					class296_sub1.out.writeString(string);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					return;
				}
				if (i == 3107) {
					int i_327_ = intStack[--intStackSize];
					String string = stringStack[--stringStackSize];
					Class295_Sub2.method2425(string, i_327_, (byte) -83);
					return;
				}
				if (i == 3108) {
					intStackSize -= 3;
					int i_328_ = intStack[intStackSize];
					int i_329_ = intStack[intStackSize + 1];
					int i_330_ = intStack[intStackSize + 2];
					InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(i_330_);
					Class296_Sub39_Sub4.method2800(i_329_, 0, i_328_, class51);
					return;
				}
				if (i == 3109) {
					intStackSize -= 2;
					int i_331_ = intStack[intStackSize];
					int i_332_ = intStack[intStackSize + 1];
					InterfaceComponent class51 = bool ? aClass51_1537 : aClass51_1528;
					Class296_Sub39_Sub4.method2800(i_332_, 0, i_331_, class51);
					return;
				}
				if (i == 3110) {
					int i_333_ = intStack[--intStackSize];
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 98, Class49.aClass311_458);
					class296_sub1.out.p2(i_333_);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					return;
				}
				if (i == 3111) {
					intStackSize -= 2;
					int i_334_ = intStack[intStackSize];
					int i_335_ = intStack[intStackSize + 1];
					Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.get(i_334_);
					if (class296_sub13 != null) {
						Class47.method596(class296_sub13, class296_sub13.anInt4657 != i_335_, true, (byte) 69);
					}
					Class368.method3809(3, 2642, i_334_, i_335_, true);
					return;
				}
				if (i == 3112) {
					intStackSize--;
					int i_336_ = intStack[intStackSize];
					Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.get(i_336_);
					if (class296_sub13 != null && class296_sub13.anInt4656 == 3) {
						Class47.method596(class296_sub13, true, true, (byte) 55);
					}
					return;
				}
				if (i == 3113) {
					NPCNode.method2448(stringStack[--stringStackSize]);
					return;
				}
				if (i == 3114) {
					intStackSize -= 2;
					int i_337_ = intStack[intStackSize];
					int i_338_ = intStack[intStackSize + 1];
					String string = stringStack[--stringStackSize];
					Class181.method1827(i_338_, "", 0, "", string, "", i_337_);
					return;
				}
				if (i == 3115) {
					intStackSize -= 11;
					Class252[] class252s = StaticMethods.method312((byte) -33);
					Class357[] class357s = Class110.method970(0);
					Class404.method4164(intStack[intStackSize + 7], intStack[intStackSize + 3], intStack[intStackSize + 5], intStack[intStackSize + 10], class252s[intStack[intStackSize]], intStack[intStackSize + 4], class357s[intStack[intStackSize + 1]], intStack[intStackSize + 9], intStack[intStackSize + 2], intStack[intStackSize + 8], intStack[intStackSize + 6], 1728789256);
					return;
				}
				if (i == 3116) {
					int i_339_ = intStack[--intStackSize];
					anInt1530++;
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 117, Class41_Sub28.aClass311_3818);
					class296_sub1.out.p2(i_339_);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					return;
				}
				if (i == 3117) {
					String string = stringStack[--stringStackSize];
					Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 93, Class366_Sub9.aClass311_5417);
					class296_sub1.out.p1(string.length() + 1);
					class296_sub1.out.writeString(string);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					return;
				}
			} else if (i < 3300) {
				if (i == 3200) {
					intStackSize -= 3;
					Class296_Sub51_Sub38.method3194(-121, intStack[intStackSize], 256, 255, intStack[intStackSize + 1], intStack[intStackSize + 2]);
					return;
				}
				if (i == 3201) {
					Class221.playSong(intStack[--intStackSize], (byte) 70, 50, 255);
					return;
				}
				if (i == 3202) {
					intStackSize -= 2;
					Class317.playJingle((byte) -97, 255, intStack[intStackSize + 1], intStack[intStackSize]);
					return;
				}
				if (i == 3203) {
					intStackSize -= 4;
					Class296_Sub51_Sub38.method3194(-102, intStack[intStackSize], 256, intStack[intStackSize + 3], intStack[intStackSize + 1], intStack[intStackSize + 2]);
					return;
				}
				if (i == 3204) {
					intStackSize -= 3;
					Class221.playSong(intStack[intStackSize], (byte) -18, intStack[intStackSize + 2], intStack[intStackSize + 1]);
					return;
				}
				if (i == 3205) {
					intStackSize -= 3;
					Class317.playJingle((byte) -108, intStack[intStackSize + 2], intStack[intStackSize + 1], intStack[intStackSize]);
					return;
				}
				if (i == 3206) {
					intStackSize -= 4;
					Class338_Sub3_Sub4.method3571(intStack[intStackSize + 3], false, 256, intStack[intStackSize + 2], intStack[intStackSize], 112, intStack[intStackSize + 1]);
					return;
				}
				if (i == 3207) {
					intStackSize -= 4;
					Class338_Sub3_Sub4.method3571(intStack[intStackSize + 3], true, 256, intStack[intStackSize + 2], intStack[intStackSize], 85, intStack[intStackSize + 1]);
					return;
				}
				if (i == 3208) {
					intStackSize -= 5;
					Class296_Sub51_Sub38.method3194(-80, intStack[intStackSize], intStack[intStackSize + 4], intStack[intStackSize + 3], intStack[intStackSize + 1], intStack[intStackSize + 2]);
					return;
				}
				if (i == 3209) {
					intStackSize -= 5;
					Class338_Sub3_Sub4.method3571(intStack[intStackSize + 3], false, intStack[intStackSize + 4], intStack[intStackSize + 2], intStack[intStackSize], 106, intStack[intStackSize + 1]);
					return;
				}
			} else if (i < 3400) {
				if (i == 3300) {
					intStack[intStackSize++] = Class29.anInt307;
					return;
				}
				if (i == 3301) {
					intStackSize -= 2;
					int i_340_ = intStack[intStackSize];
					int i_341_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = ItemArrayMethods.getItemIDFromItemsArray(i_340_, i_341_, false);
					return;
				}
				if (i == 3302) {
					intStackSize -= 2;
					int i_342_ = intStack[intStackSize];
					int i_343_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = ItemArrayMethods.getItemAmountInItemsArray(i_342_, i_343_, false);
					return;
				}
				if (i == 3303) {
					intStackSize -= 2;
					int i_344_ = intStack[intStackSize];
					int i_345_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = ItemArrayMethods.getItemsCountInItemsArray(i_344_, i_345_, false);
					return;
				}
				if (i == 3304) {
					int i_346_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class296_Sub14.itemArraysDefinitionLoader.list(i_346_).capacity;
					return;
				}
				if (i == 3305) {
					int i_347_ = intStack[--intStackSize];
					intStack[intStackSize++] = StaticMethods.anIntArray5930[i_347_];
					return;
				}
				if (i == 3306) {
					int i_348_ = intStack[--intStackSize];
					intStack[intStackSize++] = EffectiveVertex.anIntArray2215[i_348_];
					return;
				}
				if (i == 3307) {
					int i_349_ = intStack[--intStackSize];
					intStack[intStackSize++] = ClothDefinition.anIntArray3016[i_349_];
					return;
				}
				if (i == 3308) {
					int i_350_ = Class296_Sub51_Sub11.localPlayer.z;
					int i_351_ = (Class296_Sub51_Sub11.localPlayer.tileX >> 9) + Class206.worldBaseX;
					int i_352_ = (Class296_Sub51_Sub11.localPlayer.tileY >> 9) + Class41_Sub26.worldBaseY;
					intStack[intStackSize++] = (i_350_ << 28) + (i_351_ << 14) + i_352_;
					return;
				}
				if (i == 3309) {
					int i_353_ = intStack[--intStackSize];
					intStack[intStackSize++] = i_353_ >> 14 & 0x3fff;
					return;
				}
				if (i == 3310) {
					int i_354_ = intStack[--intStackSize];
					intStack[intStackSize++] = i_354_ >> 28;
					return;
				}
				if (i == 3311) {
					int i_355_ = intStack[--intStackSize];
					intStack[intStackSize++] = i_355_ & 0x3fff;
					return;
				}
				if (i == 3312) {
					intStack[intStackSize++] = Class172.member ? 1 : 0;
					return;
				}
				if (i == 3313) {
					intStackSize -= 2;
					int i_356_ = intStack[intStackSize];
					int i_357_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = ItemArrayMethods.getItemIDFromItemsArray(i_356_, i_357_, true);
					return;
				}
				if (i == 3314) {
					intStackSize -= 2;
					int i_358_ = intStack[intStackSize];
					int i_359_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = ItemArrayMethods.getItemAmountInItemsArray(i_358_, i_359_, true);
					return;
				}
				if (i == 3315) {
					intStackSize -= 2;
					int i_360_ = intStack[intStackSize];
					int i_361_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = ItemArrayMethods.getItemsCountInItemsArray(i_360_, i_361_, true);
					return;
				}
				if (i == 3316) {
					if (Class338_Sub3_Sub5.rights >= 2) {
						intStack[intStackSize++] = Class338_Sub3_Sub5.rights;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3317) {
					intStack[intStackSize++] = Class379_Sub2_Sub1.anInt6607;
					return;
				}
				if (i == 3318) {
					intStack[intStackSize++] = Class296_Sub51_Sub27_Sub1.aClass112_6733.worldId;
					return;
				}
				if (i == 3321) {
					intStack[intStackSize++] = Class123_Sub2_Sub2.anInt5822;
					return;
				}
				if (i == 3322) {
					intStack[intStackSize++] = Class388.anInt3275;
					return;
				}
				if (i == 3323) {
					if (RuntimeException_Sub1.anInt3395 >= 5 && RuntimeException_Sub1.anInt3395 <= 9) {
						intStack[intStackSize++] = 1;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3324) {
					if (RuntimeException_Sub1.anInt3395 >= 5 && RuntimeException_Sub1.anInt3395 <= 9) {
						intStack[intStackSize++] = RuntimeException_Sub1.anInt3395;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3325) {
					intStack[intStackSize++] = aa_Sub1.memb1 ? 1 : 0;
					return;
				}
				if (i == 3326) {
					intStack[intStackSize++] = Class296_Sub51_Sub11.localPlayer.combatLevel;
					return;
				}
				if (i == 3327) {
					intStack[intStackSize++] = Class296_Sub51_Sub11.localPlayer.composite != null && Class296_Sub51_Sub11.localPlayer.composite.gender ? 1 : 0;
					return;
				}
				if (i == 3329) {
					intStack[intStackSize++] = Class123.somethingWithIgnore2 ? 1 : 0;
					return;
				}
				if (i == 3330) {
					int i_362_ = intStack[--intStackSize];
					intStack[intStackSize++] = ItemArrayMethods.method1028(i_362_, false);
					return;
				}
				if (i == 3331) {
					intStackSize -= 2;
					int i_363_ = intStack[intStackSize];
					int i_364_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = ItemArrayMethods.method481(i_363_, i_364_, false, false);
					return;
				}
				if (i == 3332) {
					intStackSize -= 2;
					int i_365_ = intStack[intStackSize];
					int i_366_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = ItemArrayMethods.method481(i_365_, i_366_, false, true);
					return;
				}
				if (i == 3333) {
					intStack[intStackSize++] = Class296_Sub51_Sub39.method3202(-113);
					return;
				}
				if (i == 3335) {
					intStack[intStackSize++] = Class394.langID;
					return;
				}
				if (i == 3336) {
					intStackSize -= 4;
					int i_367_ = intStack[intStackSize];
					int i_368_ = intStack[intStackSize + 1];
					int i_369_ = intStack[intStackSize + 2];
					int i_370_ = intStack[intStackSize + 3];
					i_367_ += i_368_ << 14;
					i_367_ += i_369_ << 28;
					i_367_ += i_370_;
					intStack[intStackSize++] = i_367_;
					return;
				}
				if (i == 3337) {
					intStack[intStackSize++] = Class209.affliateID;
					return;
				}
				if (i == 3338) {
					intStack[intStackSize++] = Class368_Sub5_Sub1.method3828((byte) 121);
					return;
				}
				if (i == 3339) {
					intStack[intStackSize++] = 0;
					return;
				}
				if (i == 3340) {
					intStack[intStackSize++] = Class41.aBoolean390 ? 1 : 0;
					return;
				}
				if (i == 3341) {
					intStack[intStackSize++] = Class296_Sub37.fromBilling ? 1 : 0;
					return;
				}
				if (i == 3342) {
					intStack[intStackSize++] = Class84.aClass189_924.method1895((byte) -55);
					return;
				}
				if (i == 3343) {
					intStack[intStackSize++] = Class84.aClass189_924.method1897(0);
					return;
				}
				if (i == 3344) {
					stringStack[stringStackSize++] = Class175.method1703((byte) 105);
					return;
				}
				if (i == 3345) {
					stringStack[stringStackSize++] = Class360.method3729(-115);
					return;
				}
				if (i == 3346) {
					intStack[intStackSize++] = Class379_Sub2.method3968(52);
					return;
				}
				if (i == 3347) {
					intStack[intStackSize++] = Class379.anInt3618;
					return;
				}
				if (i == 3349) {
					intStack[intStackSize++] = Class296_Sub51_Sub11.localPlayer.aClass5_6804.method175((byte) -81) >> 3;
					return;
				}
				if (i == 3350) {
					String string = stringStack[--stringStackSize];
					if (Class296_Sub38.aString4886 != null && Class296_Sub38.aString4886.equalsIgnoreCase(string)) {
						intStack[intStackSize++] = 1;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
			} else if (i < 3500) {
				if (i == 3400) {
					intStackSize -= 2;
					int i_371_ = intStack[intStackSize];
					int i_372_ = intStack[intStackSize + 1];
					Class277 class277 = Class85.aClass350_927.method3677(i_371_, -6115);
					stringStack[stringStackSize++] = class277.method2325((byte) 121, i_372_);
					return;
				}
				if (i == 3408) {
					intStackSize -= 4;
					int i_373_ = intStack[intStackSize];
					int i_374_ = intStack[intStackSize + 1];
					int i_375_ = intStack[intStackSize + 2];
					int i_376_ = intStack[intStackSize + 3];
					Class277 class277 = Class85.aClass350_927.method3677(i_375_, -6115);
					if (class277.aChar2533 != i_373_ || class277.aChar2534 != i_374_) {
						throw new RuntimeException("C3408-1 " + i_375_ + "-" + i_376_);
					}
					if (i_374_ == 115) {
						stringStack[stringStackSize++] = class277.method2325((byte) 120, i_376_);
					} else {
						intStack[intStackSize++] = class277.method2326(false, i_376_);
						return;
					}
					return;
				}
				if (i == 3409) {
					intStackSize -= 3;
					int i_377_ = intStack[intStackSize];
					int i_378_ = intStack[intStackSize + 1];
					int i_379_ = intStack[intStackSize + 2];
					if (i_378_ == -1) {
						throw new RuntimeException("C3409-2");
					}
					Class277 class277 = Class85.aClass350_927.method3677(i_378_, -6115);
					if (class277.aChar2534 != i_377_) {
						throw new RuntimeException("C3409-1");
					}
					intStack[intStackSize++] = class277.method2330(28341, i_379_) ? 1 : 0;
					return;
				}
				if (i == 3410) {
					int i_380_ = intStack[--intStackSize];
					String string = stringStack[--stringStackSize];
					if (i_380_ == -1) {
						throw new RuntimeException("C3410-2");
					}
					Class277 class277 = Class85.aClass350_927.method3677(i_380_, -6115);
					if (class277.aChar2534 != 's') {
						throw new RuntimeException("C3410-1");
					}
					intStack[intStackSize++] = class277.method2331(0, string) ? 1 : 0;
					return;
				}
				if (i == 3411) {
					int i_381_ = intStack[--intStackSize];
					Class277 class277 = Class85.aClass350_927.method3677(i_381_, -6115);
					intStack[intStackSize++] = class277.method2334(-1);
					return;
				}
				if (i == 3412) {
					intStackSize -= 3;
					int i_382_ = intStack[intStackSize];
					int i_383_ = intStack[intStackSize + 1];
					int i_384_ = intStack[intStackSize + 2];
					if (i_383_ == -1) {
						throw new RuntimeException();
					}
					Class277 class277 = Class85.aClass350_927.method3677(i_383_, -6115);
					if (class277.aChar2534 != i_382_) {
						throw new RuntimeException();
					}
					Class296_Sub43 class296_sub43 = class277.method2323(i_384_, -117);
					int i_385_ = 0;
					if (class296_sub43 != null) {
						i_385_ = class296_sub43.anIntArray4938.length;
					}
					intStack[intStackSize++] = i_385_;
					return;
				}
				if (i == 3413) {
					int i_386_ = intStack[--intStackSize];
					String string = stringStack[--stringStackSize];
					if (i_386_ == -1) {
						throw new RuntimeException();
					}
					Class277 class277 = Class85.aClass350_927.method3677(i_386_, -6115);
					if (class277.aChar2534 != 's') {
						throw new RuntimeException();
					}
					Class296_Sub30 class296_sub30 = class277.method2336(string, -8470);
					int i_387_ = 0;
					if (class296_sub30 != null) {
						i_387_ = class296_sub30.anIntArray4819.length;
					}
					intStack[intStackSize++] = i_387_;
					return;
				}
				if (i == 3414) {
					intStackSize -= 5;
					int i_388_ = intStack[intStackSize];
					int i_389_ = intStack[intStackSize + 1];
					int i_390_ = intStack[intStackSize + 2];
					int i_391_ = intStack[intStackSize + 3];
					int i_392_ = intStack[intStackSize + 4];
					if (i_390_ == -1) {
						throw new RuntimeException();
					}
					Class277 class277 = Class85.aClass350_927.method3677(i_390_, -6115);
					if (class277.aChar2533 != i_389_) {
						throw new RuntimeException();
					}
					if (class277.aChar2534 != i_388_) {
						throw new RuntimeException();
					}
					Class296_Sub43 class296_sub43 = class277.method2323(i_391_, -127);
					if (i_392_ < 0 || class296_sub43 == null || class296_sub43.anIntArray4938.length <= i_392_) {
						throw new RuntimeException();
					}
					intStack[intStackSize++] = class296_sub43.anIntArray4938[i_392_];
					return;
				}
				if (i == 3415) {
					intStackSize -= 3;
					int i_393_ = intStack[intStackSize];
					int i_394_ = intStack[intStackSize + 1];
					int i_395_ = intStack[intStackSize + 2];
					String string = stringStack[--stringStackSize];
					if (i_394_ == -1) {
						throw new RuntimeException();
					}
					Class277 class277 = Class85.aClass350_927.method3677(i_394_, -6115);
					if (class277.aChar2533 != i_393_) {
						throw new RuntimeException();
					}
					if (class277.aChar2534 != 's') {
						throw new RuntimeException();
					}
					Class296_Sub30 class296_sub30 = class277.method2336(string, -8470);
					if (i_395_ < 0 || class296_sub30 == null || class296_sub30.anIntArray4819.length <= i_395_) {
						throw new RuntimeException();
					}
					intStack[intStackSize++] = class296_sub30.anIntArray4819[i_395_];
					return;
				}
			} else if (i < 3700) {
				if (i == 3600) {
					if (Class78.anInt3431 == 0) {
						intStack[intStackSize++] = -2;
					} else {
						if (Class78.anInt3431 == 1) {
							intStack[intStackSize++] = -1;
						} else {
							intStack[intStackSize++] = Class285.anInt2625;
							return;
						}
						return;
					}
					return;
				}
				if (i == 3601) {
					int i_396_ = intStack[--intStackSize];
					if (Class78.anInt3431 == 2 && i_396_ < Class285.anInt2625) {
						stringStack[stringStackSize++] = Js5TextureLoader.aStringArray3443[i_396_];
						if (Class338_Sub9.aStringArray5268[i_396_] != null) {
							stringStack[stringStackSize++] = Class338_Sub9.aStringArray5268[i_396_];
						} else {
							stringStack[stringStackSize++] = "";
						}
					} else {
						stringStack[stringStackSize++] = "";
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 3602) {
					int i_397_ = intStack[--intStackSize];
					if (Class78.anInt3431 == 2 && i_397_ < Class285.anInt2625) {
						intStack[intStackSize++] = Class95.anIntArray1021[i_397_];
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3603) {
					int i_398_ = intStack[--intStackSize];
					if (Class78.anInt3431 == 2 && i_398_ < Class285.anInt2625) {
						intStack[intStackSize++] = Class338_Sub3_Sub4_Sub1.anIntArray6653[i_398_];
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3604) {
					String string = stringStack[--stringStackSize];
					int i_399_ = intStack[--intStackSize];
					Class368_Sub21.method3863(string, i_399_, -1);
					return;
				}
				if (i == 3605) {
					String string = stringStack[--stringStackSize];
					Class321.method3344(-87, string);
					return;
				}
				if (i == 3606) {
					String string = stringStack[--stringStackSize];
					Class373.method3913(105, string);
					return;
				}
				if (i == 3607) {
					String string = stringStack[--stringStackSize];
					Class110.method971(string, 0, false);
					return;
				}
				if (i == 3608) {
					String string = stringStack[--stringStackSize];
					Class293.deleteIgnore(string);
					return;
				}
				if (i == 3609) {
					String string = stringStack[--stringStackSize];
					if (string.startsWith("<img=0>") || string.startsWith("<img=1>")) {
						string = string.substring(7);
					}
					intStack[intStackSize++] = Class97.method872(0, string) ? 1 : 0;
					return;
				}
				if (i == 3610) {
					int i_400_ = intStack[--intStackSize];
					if (Class78.anInt3431 == 2 && i_400_ < Class285.anInt2625) {
						stringStack[stringStackSize++] = StaticMethods.aStringArray5940[i_400_];
					} else {
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 3611) {
					if (Exception_Sub1.aString45 != null) {
						stringStack[stringStackSize++] = ha_Sub1.method1206(Exception_Sub1.aString45, -116);
					} else {
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 3612) {
					if (Exception_Sub1.aString45 != null) {
						intStack[intStackSize++] = Class271.anInt2512;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3613) {
					int i_401_ = intStack[--intStackSize];
					if (Exception_Sub1.aString45 != null && i_401_ < Class271.anInt2512) {
						stringStack[stringStackSize++] = Class338_Sub8.aClass94Array5257[i_401_].aString1018;
					} else {
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 3614) {
					int i_402_ = intStack[--intStackSize];
					if (Exception_Sub1.aString45 != null && i_402_ < Class271.anInt2512) {
						intStack[intStackSize++] = Class338_Sub8.aClass94Array5257[i_402_].anInt1011;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3615) {
					int i_403_ = intStack[--intStackSize];
					if (Exception_Sub1.aString45 != null && i_403_ < Class271.anInt2512) {
						intStack[intStackSize++] = Class338_Sub8.aClass94Array5257[i_403_].aByte1014;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3616) {
					intStack[intStackSize++] = Class97.aByte1051;
					return;
				}
				if (i == 3617) {
					String string = stringStack[--stringStackSize];
					Class241_Sub1_Sub1.method2156((byte) -29, string);
					return;
				}
				if (i == 3618) {
					intStack[intStackSize++] = Class296_Sub39_Sub10.aByte6178;
					return;
				}
				if (i == 3619) {
					String string = stringStack[--stringStackSize];
					Class338_Sub3_Sub4.method3570(string, (byte) 75);
					return;
				}
				if (i == 3620) {
					Class181.method1819((byte) -43);
					return;
				}
				if (i == 3621) {
					if (Class78.anInt3431 == 0) {
						intStack[intStackSize++] = -1;
					} else {
						intStack[intStackSize++] = Class317.ignoresSize;
						return;
					}
					return;
				}
				if (i == 3622) {
					int i_404_ = intStack[--intStackSize];
					if (Class78.anInt3431 != 0 && i_404_ < Class317.ignoresSize) {
						stringStack[stringStackSize++] = Class362.ignoreNames1[i_404_];
						if (Class296_Sub39_Sub11.ignoreNames3[i_404_] != null) {
							stringStack[stringStackSize++] = Class296_Sub39_Sub11.ignoreNames3[i_404_];
						} else {
							stringStack[stringStackSize++] = "";
						}
					} else {
						stringStack[stringStackSize++] = "";
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 3623) {
					String string = stringStack[--stringStackSize];
					if (string.startsWith("<img=0>") || string.startsWith("<img=1>")) {
						string = string.substring(7);
					}
					intStack[intStackSize++] = NPCNode.playerIgnored(string) ? 1 : 0;
					return;
				}
				if (i == 3624) {
					int i_405_ = intStack[--intStackSize];
					if (Class338_Sub8.aClass94Array5257 != null && i_405_ < Class271.anInt2512 && Class338_Sub8.aClass94Array5257[i_405_].aString1019.equalsIgnoreCase(Class296_Sub51_Sub11.localPlayer.displayName)) {
						intStack[intStackSize++] = 1;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3625) {
					if (StaticMethods.aString6087 != null) {
						stringStack[stringStackSize++] = StaticMethods.aString6087;
					} else {
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 3626) {
					int i_406_ = intStack[--intStackSize];
					if (Exception_Sub1.aString45 != null && i_406_ < Class271.anInt2512) {
						stringStack[stringStackSize++] = Class338_Sub8.aClass94Array5257[i_406_].aString1013;
					} else {
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 3627) {
					int i_407_ = intStack[--intStackSize];
					if (Class78.anInt3431 == 2 && i_407_ >= 0 && i_407_ < Class285.anInt2625) {
						intStack[intStackSize++] = StaticMethods.aBooleanArray5945[i_407_] ? 1 : 0;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3628) {
					String string = stringStack[--stringStackSize];
					if (string.startsWith("<img=0>") || string.startsWith("<img=1>")) {
						string = string.substring(7);
					}
					intStack[intStackSize++] = StaticMethods.method2722(-1, string);
					return;
				}
				if (i == 3629) {
					intStack[intStackSize++] = Class139.countryID;
					return;
				}
				if (i == 3630) {
					String string = stringStack[--stringStackSize];
					Class110.method971(string, 0, true);
					return;
				}
				if (i == 3631) {
					int i_408_ = intStack[--intStackSize];
					intStack[intStackSize++] = StaticMethods.freeIgnoreSlots[i_408_] ? 1 : 0;
					return;
				}
				if (i == 3632) {
					int i_409_ = intStack[--intStackSize];
					if (Exception_Sub1.aString45 != null && i_409_ < Class271.anInt2512) {
						stringStack[stringStackSize++] = Class338_Sub8.aClass94Array5257[i_409_].aString1019;
					} else {
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 3633) {
					int i_410_ = intStack[--intStackSize];
					if (Class78.anInt3431 != 0 && i_410_ < Class317.ignoresSize) {
						stringStack[stringStackSize++] = Class296_Sub51_Sub20.ignoreNames2[i_410_];
					} else {
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 3634) {
					int i_411_ = intStack[--intStackSize];
					if (Class78.anInt3431 == 2 && i_411_ < Class285.anInt2625) {
						intStack[intStackSize++] = Class296_Sub34_Sub2.aBooleanArray6101[i_411_] ? 1 : 0;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
			} else if (i < 3800) {
				if (i == 3700) {
					if (Class368_Sub2.unaffinedClanSettings != null) {
						intStack[intStackSize++] = 1;
						aClass371_1540 = Class368_Sub2.unaffinedClanSettings;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3701) {
					if (Class296_Sub51_Sub2.affinedClanSettings != null) {
						intStack[intStackSize++] = 1;
						aClass371_1540 = Class296_Sub51_Sub2.affinedClanSettings;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3702) {
					stringStack[stringStackSize++] = aClass371_1540.name;
					return;
				}
				if (i == 3703) {
					intStack[intStackSize++] = aClass371_1540.allow_guests ? 1 : 0;
					return;
				}
				if (i == 3704) {
					intStack[intStackSize++] = aClass371_1540.rank_talk;
					return;
				}
				if (i == 3705) {
					intStack[intStackSize++] = aClass371_1540.rank_kick;
					return;
				}
				if (i == 3706) {
					intStack[intStackSize++] = aClass371_1540.rank_lootshare;
					return;
				}
				if (i == 3707) {
					intStack[intStackSize++] = aClass371_1540.rank_unknown;
					return;
				}
				if (i == 3709) {
					intStack[intStackSize++] = aClass371_1540.members_count;
					return;
				}
				if (i == 3710) {
					int i_412_ = intStack[--intStackSize];
					stringStack[stringStackSize++] = aClass371_1540.members_displayname[i_412_];
					return;
				}
				if (i == 3711) {
					int i_413_ = intStack[--intStackSize];
					intStack[intStackSize++] = aClass371_1540.members_rank[i_413_];
					return;
				}
				if (i == 3712) {
					intStack[intStackSize++] = aClass371_1540.banneds_count;
					return;
				}
				if (i == 3713) {
					int i_414_ = intStack[--intStackSize];
					stringStack[stringStackSize++] = aClass371_1540.banneds_displayname[i_414_];
					return;
				}
				if (i == 3714) {
					intStackSize -= 3;
					int i_415_ = intStack[intStackSize];
					int i_416_ = intStack[intStackSize + 1];
					int i_417_ = intStack[intStackSize + 2];
					intStack[intStackSize++] = aClass371_1540.getMemberExtraInfo(i_415_, i_416_, i_417_);
					return;
				}
				if (i == 3715) {
					intStack[intStackSize++] = aClass371_1540.owner_slot;
					return;
				}
				if (i == 3716) {
					intStack[intStackSize++] = aClass371_1540.replacement_owner_slot;
					return;
				}
				if (i == 3717) {
					intStack[intStackSize++] = aClass371_1540.getAffinedSlot(stringStack[--stringStackSize]);
					return;
				}
				if (i == 3718) {
					intStack[intStackSize - 1] = aClass371_1540.sortAffined()[intStack[intStackSize - 1]];
					return;
				}
				if (i == 3719) {
					Class366_Sub3.method3778((byte) 66, intStack[--intStackSize]);
					return;
				}
				if (i == 3720) {
					int i_418_ = intStack[--intStackSize];
					intStack[intStackSize++] = aClass371_1540.members_joinday[i_418_];
					return;
				}
				if (i == 3750) {
					if (Class166.aClass296_Sub54_1692 != null) {
						intStack[intStackSize++] = 1;
						aClass296_Sub54_1545 = Class166.aClass296_Sub54_1692;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3751) {
					if (EmissiveTriangle.affinedClanChannel != null) {
						intStack[intStackSize++] = 1;
						aClass296_Sub54_1545 = EmissiveTriangle.affinedClanChannel;
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 3752) {
					stringStack[stringStackSize++] = aClass296_Sub54_1545.name;
					return;
				}
				if (i == 3753) {
					intStack[intStackSize++] = aClass296_Sub54_1545.rank_kick;
					return;
				}
				if (i == 3754) {
					intStack[intStackSize++] = aClass296_Sub54_1545.rank_talk;
					return;
				}
				if (i == 3755) {
					intStack[intStackSize++] = aClass296_Sub54_1545.members_count;
					return;
				}
				if (i == 3756) {
					int i_419_ = intStack[--intStackSize];
					stringStack[stringStackSize++] = aClass296_Sub54_1545.members[i_419_].name;
					return;
				}
				if (i == 3757) {
					int i_420_ = intStack[--intStackSize];
					intStack[intStackSize++] = aClass296_Sub54_1545.members[i_420_].rank;
					return;
				}
				if (i == 3758) {
					int i_421_ = intStack[--intStackSize];
					intStack[intStackSize++] = aClass296_Sub54_1545.members[i_421_].world;
					return;
				}
				if (i == 3759) {
					int i_422_ = intStack[--intStackSize];
					Class296_Sub14.method2509(true, aClass296_Sub54_1545 == EmissiveTriangle.affinedClanChannel, i_422_);
					return;
				}
				if (i == 3760) {
					intStack[intStackSize++] = aClass296_Sub54_1545.getMemberSlot(stringStack[--stringStackSize]);
					return;
				}
				if (i == 3761) {
					intStack[intStackSize - 1] = aClass296_Sub54_1545.sort()[intStack[intStackSize - 1]];
					return;
				}
				if (i == 3790) {
					intStack[intStackSize++] = Class276.anObjectArray2527 != null ? 1 : 0;
					return;
				}
			} else if (i < 4000) {
				if (i == 3903) {
					int i_423_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class360_Sub5.aClass208Array5326[i_423_].method2002(101);
					return;
				}
				if (i == 3904) {
					int i_424_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class360_Sub5.aClass208Array5326[i_424_].anInt2085;
					return;
				}
				if (i == 3905) {
					int i_425_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class360_Sub5.aClass208Array5326[i_425_].anInt2086;
					return;
				}
				if (i == 3906) {
					int i_426_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class360_Sub5.aClass208Array5326[i_426_].anInt2087;
					return;
				}
				if (i == 3907) {
					int i_427_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class360_Sub5.aClass208Array5326[i_427_].anInt2083;
					return;
				}
				if (i == 3908) {
					int i_428_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class360_Sub5.aClass208Array5326[i_428_].anInt2084;
					return;
				}
				if (i == 3910) {
					int i_429_ = intStack[--intStackSize];
					int i_430_ = Class360_Sub5.aClass208Array5326[i_429_].method2001(-85);
					intStack[intStackSize++] = i_430_ == 0 ? 1 : 0;
					return;
				}
				if (i == 3911) {
					int i_431_ = intStack[--intStackSize];
					int i_432_ = Class360_Sub5.aClass208Array5326[i_431_].method2001(-126);
					intStack[intStackSize++] = i_432_ == 2 ? 1 : 0;
					return;
				}
				if (i == 3912) {
					int i_433_ = intStack[--intStackSize];
					int i_434_ = Class360_Sub5.aClass208Array5326[i_433_].method2001(-99);
					intStack[intStackSize++] = i_434_ == 5 ? 1 : 0;
					return;
				}
				if (i == 3913) {
					int i_435_ = intStack[--intStackSize];
					int i_436_ = Class360_Sub5.aClass208Array5326[i_435_].method2001(56);
					intStack[intStackSize++] = i_436_ == 1 ? 1 : 0;
					return;
				}
			} else if (i < 4100) {
				if (i == 4000) {
					intStackSize -= 2;
					int i_437_ = intStack[intStackSize];
					int i_438_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_437_ + i_438_;
					return;
				}
				if (i == 4001) {
					intStackSize -= 2;
					int i_439_ = intStack[intStackSize];
					int i_440_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_439_ - i_440_;
					return;
				}
				if (i == 4002) {
					intStackSize -= 2;
					int i_441_ = intStack[intStackSize];
					int i_442_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_441_ * i_442_;
					return;
				}
				if (i == 4003) {
					intStackSize -= 2;
					int i_443_ = intStack[intStackSize];
					int i_444_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_443_ / i_444_;
					return;
				}
				if (i == 4004) {
					int i_445_ = intStack[--intStackSize];
					intStack[intStackSize++] = (int) (Math.random() * i_445_);
					return;
				}
				if (i == 4005) {
					int i_446_ = intStack[--intStackSize];
					intStack[intStackSize++] = (int) (Math.random() * (i_446_ + 1));
					return;
				}
				if (i == 4006) {
					intStackSize -= 5;
					int i_447_ = intStack[intStackSize];
					int i_448_ = intStack[intStackSize + 1];
					int i_449_ = intStack[intStackSize + 2];
					int i_450_ = intStack[intStackSize + 3];
					int i_451_ = intStack[intStackSize + 4];
					intStack[intStackSize++] = i_447_ + (i_448_ - i_447_) * (i_451_ - i_449_) / (i_450_ - i_449_);
					return;
				}
				if (i == 4007) {
					intStackSize -= 2;
					long l = intStack[intStackSize];
					long l_452_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = (int) (l + l * l_452_ / 100L);
					return;
				}
				if (i == 4008) {
					intStackSize -= 2;
					int i_453_ = intStack[intStackSize];
					int i_454_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_453_ | 1 << i_454_;
					return;
				}
				if (i == 4009) {
					intStackSize -= 2;
					int i_455_ = intStack[intStackSize];
					int i_456_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_455_ & -1 - (1 << i_456_);
					return;
				}
				if (i == 4010) {
					intStackSize -= 2;
					int i_457_ = intStack[intStackSize];
					int i_458_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = (i_457_ & 1 << i_458_) != 0 ? 1 : 0;
					return;
				}
				if (i == 4011) {
					intStackSize -= 2;
					int i_459_ = intStack[intStackSize];
					int i_460_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_459_ % i_460_;
					return;
				}
				if (i == 4012) {
					intStackSize -= 2;
					int i_461_ = intStack[intStackSize];
					int i_462_ = intStack[intStackSize + 1];
					if (i_461_ == 0) {
						intStack[intStackSize++] = 0;
					} else {
						intStack[intStackSize++] = (int) Math.pow(i_461_, i_462_);
						return;
					}
					return;
				}
				if (i == 4013) {
					intStackSize -= 2;
					int i_463_ = intStack[intStackSize];
					int i_464_ = intStack[intStackSize + 1];
					if (i_463_ == 0) {
						intStack[intStackSize++] = 0;
					} else {
						if (i_464_ == 0) {
							intStack[intStackSize++] = 2147483647;
						} else {
							intStack[intStackSize++] = (int) Math.pow(i_463_, 1.0 / i_464_);
							return;
						}
						return;
					}
					return;
				}
				if (i == 4014) {
					intStackSize -= 2;
					int i_465_ = intStack[intStackSize];
					int i_466_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_465_ & i_466_;
					return;
				}
				if (i == 4015) {
					intStackSize -= 2;
					int i_467_ = intStack[intStackSize];
					int i_468_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_467_ | i_468_;
					return;
				}
				if (i == 4016) {
					intStackSize -= 2;
					int i_469_ = intStack[intStackSize];
					int i_470_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_469_ < i_470_ ? i_469_ : i_470_;
					return;
				}
				if (i == 4017) {
					intStackSize -= 2;
					int i_471_ = intStack[intStackSize];
					int i_472_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = i_471_ > i_472_ ? i_471_ : i_472_;
					return;
				}
				if (i == 4018) {
					intStackSize -= 3;
					long l = intStack[intStackSize];
					long l_473_ = intStack[intStackSize + 1];
					long l_474_ = intStack[intStackSize + 2];
					intStack[intStackSize++] = (int) (l * l_474_ / l_473_);
					return;
				}
				if (i == 4019) {
					intStackSize -= 2;
					int i_475_ = intStack[intStackSize];
					int i_476_ = intStack[intStackSize + 1];
					if (i_475_ > 700 || i_476_ > 700) {
						intStack[intStackSize++] = 256;
					}
					double d = (Math.random() * (i_476_ + i_475_) - i_475_ + 800.0) / 100.0;
					intStack[intStackSize++] = (int) (Math.pow(2.0, d) + 0.5);
					return;
				}
				if (i == 4020) {
					int i_477_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class295_Sub1.anIntArray3691[Class338_Sub3_Sub4_Sub1.method3576(i_477_, (byte) -76) & 0xffff];
					return;
				}
			} else if (i < 4200) {
				if (i == 4100) {
					String string = stringStack[--stringStackSize];
					int i_478_ = intStack[--intStackSize];
					stringStack[stringStackSize++] = string + i_478_;
					return;
				}
				if (i == 4101) {
					stringStackSize -= 2;
					String string = stringStack[stringStackSize];
					String string_479_ = stringStack[stringStackSize + 1];
					stringStack[stringStackSize++] = string + string_479_;
					return;
				}
				if (i == 4102) {
					String string = stringStack[--stringStackSize];
					int i_480_ = intStack[--intStackSize];
					stringStack[stringStackSize++] = string + InputStream_Sub1.method129(i_480_, true, -1);
					return;
				}
				if (i == 4103) {
					String string = stringStack[--stringStackSize];
					stringStack[stringStackSize++] = string.toLowerCase();
					return;
				}
				if (i == 4104) {
					stringStack[stringStackSize++] = Class296_Sub39_Sub9.method2828(Class296_Sub28.method2685((byte) 41, intStack[--intStackSize]), Class394.langID, (byte) 125);
					return;
				}
				if (i == 4105) {
					stringStackSize -= 2;
					String string = stringStack[stringStackSize];
					String string_481_ = stringStack[stringStackSize + 1];
					if (Class296_Sub51_Sub11.localPlayer.composite != null && Class296_Sub51_Sub11.localPlayer.composite.gender) {
						stringStack[stringStackSize++] = string_481_;
					} else {
						stringStack[stringStackSize++] = string;
						return;
					}
					return;
				}
				if (i == 4106) {
					int i_482_ = intStack[--intStackSize];
					stringStack[stringStackSize++] = Integer.toString(i_482_);
					return;
				}
				if (i == 4107) {
					stringStackSize -= 2;
					intStack[intStackSize++] = Class106_Sub1.method941(stringStack[stringStackSize + 1], Class394.langID, 112, stringStack[stringStackSize]);
					return;
				}
				if (i == 4108) {
					String string = stringStack[--stringStackSize];
					intStackSize -= 2;
					int i_483_ = intStack[intStackSize];
					int i_484_ = intStack[intStackSize + 1];
					Class92 class92 = Class92.method2821(i_484_, 0, -49, LookupTable.aClass138_53);
					intStack[intStackSize++] = class92.method848(Class85.aClass397Array929, i_483_, string, true);
					return;
				}
				if (i == 4109) {
					String string = stringStack[--stringStackSize];
					intStackSize -= 2;
					int i_485_ = intStack[intStackSize];
					int i_486_ = intStack[intStackSize + 1];
					Class92 class92 = Class92.method2821(i_486_, 0, 92, LookupTable.aClass138_53);
					intStack[intStackSize++] = class92.method845((byte) 123, Class85.aClass397Array929, string, i_485_);
					return;
				}
				if (i == 4110) {
					stringStackSize -= 2;
					String string = stringStack[stringStackSize];
					String string_487_ = stringStack[stringStackSize + 1];
					if (intStack[--intStackSize] == 1) {
						stringStack[stringStackSize++] = string;
					} else {
						stringStack[stringStackSize++] = string_487_;
						return;
					}
					return;
				}
				if (i == 4111) {
					String string = stringStack[--stringStackSize];
					stringStack[stringStackSize++] = ha_Sub1_Sub1.escapeMessage(string);
					return;
				}
				if (i == 4112) {
					String string = stringStack[--stringStackSize];
					int i_488_ = intStack[--intStackSize];
					if (i_488_ == -1) {
						throw new RuntimeException("null char");
					}
					stringStack[stringStackSize++] = string + (char) i_488_;
					return;
				}
				if (i == 4113) {
					int i_489_ = intStack[--intStackSize];
					intStack[intStackSize++] = method1525((char) i_489_);
					return;
				}
				if (i == 4114) {
					int i_490_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class350.method3679((byte) 102, (char) i_490_) ? 1 : 0;
					return;
				}
				if (i == 4115) {
					int i_491_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class122_Sub1.method1044((char) i_491_, (byte) -81) ? 1 : 0;
					return;
				}
				if (i == 4116) {
					int i_492_ = intStack[--intStackSize];
					intStack[intStackSize++] = MaterialRaw.method1670(-100, (char) i_492_) ? 1 : 0;
					return;
				}
				if (i == 4117) {
					String string = stringStack[--stringStackSize];
					if (string != null) {
						intStack[intStackSize++] = string.length();
					} else {
						intStack[intStackSize++] = 0;
						return;
					}
					return;
				}
				if (i == 4118) {
					String string = stringStack[--stringStackSize];
					intStackSize -= 2;
					int i_493_ = intStack[intStackSize];
					int i_494_ = intStack[intStackSize + 1];
					stringStack[stringStackSize++] = string.substring(i_493_, i_494_);
					return;
				}
				if (i == 4119) {
					String string = stringStack[--stringStackSize];
					StringBuffer stringbuffer = new StringBuffer(string.length());
					boolean bool_495_ = false;
					for (int i_496_ = 0; i_496_ < string.length(); i_496_++) {
						char c = string.charAt(i_496_);
						if (c == '<') {
							bool_495_ = true;
						} else if (c == '>') {
							bool_495_ = false;
						} else if (!bool_495_) {
							stringbuffer.append(c);
						}
					}
					stringStack[stringStackSize++] = stringbuffer.toString();
					return;
				}
				if (i == 4120) {
					String string = stringStack[--stringStackSize];
					intStackSize -= 2;
					int i_497_ = intStack[intStackSize];
					int i_498_ = intStack[intStackSize + 1];
					intStack[intStackSize++] = string.indexOf(i_497_, i_498_);
					return;
				}
				if (i == 4121) {
					stringStackSize -= 2;
					String string = stringStack[stringStackSize];
					String string_499_ = stringStack[stringStackSize + 1];
					int i_500_ = intStack[--intStackSize];
					intStack[intStackSize++] = string.indexOf(string_499_, i_500_);
					return;
				}
				if (i == 4122) {
					int i_501_ = intStack[--intStackSize];
					intStack[intStackSize++] = Character.toLowerCase((char) i_501_);
					return;
				}
				if (i == 4123) {
					int i_502_ = intStack[--intStackSize];
					intStack[intStackSize++] = Character.toUpperCase((char) i_502_);
					return;
				}
				if (i == 4124) {
					boolean bool_503_ = intStack[--intStackSize] != 0;
					int i_504_ = intStack[--intStackSize];
					stringStack[stringStackSize++] = za_Sub2.method3225(bool_503_, Class394.langID, 0, -104, i_504_);
					return;
				}
				if (i == 4125) {
					String string = stringStack[--stringStackSize];
					int i_505_ = intStack[--intStackSize];
					Class92 class92 = Class92.method2821(i_505_, 0, 106, LookupTable.aClass138_53);
					intStack[intStackSize++] = class92.method847(Class85.aClass397Array929, string, 0);
					return;
				}
				if (i == 4126) {
					stringStack[stringStackSize++] = ParamType.method4010(Class394.langID, true, intStack[--intStackSize] * 60000L, -119) + " UTC";
					return;
				}
				if (i == 4127) {
					long l = longStack[--longStackSize];
					stringStack[stringStackSize++] = l == -1L ? "" : Long.toString(l, 36).toUpperCase();
					return;
				}
			} else if (i < 4300) {
				if (i == 4200) {
					int i_506_ = intStack[--intStackSize];
					stringStack[stringStackSize++] = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_506_).name;
					return;
				}
				if (i == 4201) {
					intStackSize -= 2;
					int i_507_ = intStack[intStackSize];
					int i_508_ = intStack[intStackSize + 1];
					ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_507_);
					if (i_508_ >= 1 && i_508_ <= 5 && class158.groundOptions[i_508_ - 1] != null) {
						stringStack[stringStackSize++] = class158.groundOptions[i_508_ - 1];
					} else {
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 4202) {
					intStackSize -= 2;
					int i_509_ = intStack[intStackSize];
					int i_510_ = intStack[intStackSize + 1];
					ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_509_);
					if (i_510_ >= 1 && i_510_ <= 5 && class158.inventoryOptions[i_510_ - 1] != null) {
						stringStack[stringStackSize++] = class158.inventoryOptions[i_510_ - 1];
					} else {
						stringStack[stringStackSize++] = "";
						return;
					}
					return;
				}
				if (i == 4203) {
					int i_511_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_511_).cost;
					return;
				}
				if (i == 4204) {
					int i_512_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_512_).stackability == 1 ? 1 : 0;
					return;
				}
				if (i == 4205) {
					int i_513_ = intStack[--intStackSize];
					ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_513_);
					if (class158.certTemplate == -1 && class158.certLink >= 0) {
						intStack[intStackSize++] = class158.certLink;
					} else {
						intStack[intStackSize++] = i_513_;
						return;
					}
					return;
				}
				if (i == 4206) {
					int i_514_ = intStack[--intStackSize];
					ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_514_);
					if (class158.certTemplate >= 0 && class158.certLink >= 0) {
						intStack[intStackSize++] = class158.certLink;
					} else {
						intStack[intStackSize++] = i_514_;
						return;
					}
					return;
				}
				if (i == 4207) {
					int i_515_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_515_).members ? 1 : 0;
					return;
				}
				if (i == 4208) {
					intStackSize -= 2;
					int i_516_ = intStack[intStackSize];
					int i_517_ = intStack[intStackSize + 1];
					ParamType class383 = Class296_Sub22.itemExtraDataDefinitionLoader.list(i_517_);
					if (class383.method4013(-126)) {
						stringStack[stringStackSize++] = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_516_).getStringParam(i_517_, class383.name);
					} else {
						intStack[intStackSize++] = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_516_).getIntParam(i_517_, class383.defaultValue);
						return;
					}
					return;
				}
				if (i == 4209) {
					intStackSize -= 2;
					int i_518_ = intStack[intStackSize];
					int i_519_ = intStack[intStackSize + 1] - 1;
					ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_518_);
					if (class158.cursor3 == i_519_) {
						intStack[intStackSize++] = class158.cursor3op;
					} else {
						if (class158.cursor4 == i_519_) {
							intStack[intStackSize++] = class158.cursor4op;
						} else {
							intStack[intStackSize++] = -1;
							return;
						}
						return;
					}
					return;
				}
				if (i == 4210) {
					String string = stringStack[--stringStackSize];
					int i_520_ = intStack[--intStackSize];
					Class296_Sub39_Sub7.method2816((byte) -108, i_520_ == 1, string);
					intStack[intStackSize++] = Class342.anInt2989;
					return;
				}
				if (i == 4211) {
					if (StaticMethods.aShortArray5932 == null || Class203.anInt3569 >= Class342.anInt2989) {
						intStack[intStackSize++] = -1;
					} else {
						intStack[intStackSize++] = StaticMethods.aShortArray5932[Class203.anInt3569++] & 0xffff;
						return;
					}
					return;
				}
				if (i == 4212) {
					Class203.anInt3569 = 0;
					return;
				}
				if (i == 4213) {
					int i_521_ = intStack[--intStackSize];
					intStack[intStackSize++] = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_521_).multiStackSize;
					return;
				}
				if (i == 4214) {
					String string = stringStack[--stringStackSize];
					intStackSize -= 3;
					int i_522_ = intStack[intStackSize];
					int i_523_ = intStack[intStackSize + 1];
					int i_524_ = intStack[intStackSize + 2];
					Class234.method2123((byte) -27, i_523_, i_522_ == 1, i_524_, string);
					intStack[intStackSize++] = Class342.anInt2989;
					return;
				}
				if (i == 4215) {
					stringStackSize -= 2;
					intStackSize -= 2;
					String string = stringStack[stringStackSize];
					int i_525_ = intStack[intStackSize];
					int i_526_ = intStack[intStackSize + 1];
					String string_527_ = stringStack[stringStackSize + 1];
					Class71.method769(0, string, i_526_, i_525_ == 1, string_527_);
					intStack[intStackSize++] = Class342.anInt2989;
					return;
				}
			} else if (i < 4400) {
				if (i == 4300) {
					intStackSize -= 2;
					int i_528_ = intStack[intStackSize];
					int i_529_ = intStack[intStackSize + 1];
					ParamType class383 = Class296_Sub22.itemExtraDataDefinitionLoader.list(i_529_);
					if (class383.method4013(-117)) {
						stringStack[stringStackSize++] = Class352.npcDefinitionLoader.getDefinition(i_528_).getStringExtraData(i_529_, class383.name);
					} else {
						intStack[intStackSize++] = Class352.npcDefinitionLoader.getDefinition(i_528_).getIntExtraData(i_529_, class383.defaultValue);
						return;
					}
					return;
				}
			} else if (i < 4500) {
				if (i == 4400) {
					intStackSize -= 2;
					int i_530_ = intStack[intStackSize];
					int i_531_ = intStack[intStackSize + 1];
					ParamType class383 = Class296_Sub22.itemExtraDataDefinitionLoader.list(i_531_);
					if (class383.method4013(-128)) {
						stringStack[stringStackSize++] = Class379.objectDefinitionLoader.getObjectDefinition(i_530_).method764(64, i_531_, class383.name);
					} else {
						intStack[intStackSize++] = Class379.objectDefinitionLoader.getObjectDefinition(i_530_).method755(i_531_, (byte) -82, class383.defaultValue);
						return;
					}
					return;
				}
			} else if (i < 4600) {
				if (i == 4500) {
					intStackSize -= 2;
					int i_532_ = intStack[intStackSize];
					int i_533_ = intStack[intStackSize + 1];
					ParamType class383 = Class296_Sub22.itemExtraDataDefinitionLoader.list(i_533_);
					if (class383.method4013(-115)) {
						stringStack[stringStackSize++] = Class338_Sub9.aClass76_5266.method786((byte) 99, i_532_).method2856(class383.name, 106, i_533_);
					} else {
						intStack[intStackSize++] = Class338_Sub9.aClass76_5266.method786((byte) 101, i_532_).method2853(class383.defaultValue, i_533_, 8);
						return;
					}
					return;
				}
			} else if (i < 4700) {
				if (i == 4600) {
					int i_534_ = intStack[--intStackSize];
					Class280 class280 = Class41_Sub10.aClass62_3768.method700(i_534_, 0);
					if (class280.anIntArray2574 != null && class280.anIntArray2574.length > 0) {
						int i_535_ = 0;
						int i_536_ = class280.anIntArray2570[0];
						for (int i_537_ = 1; i_537_ < class280.anIntArray2574.length; i_537_++) {
							if (class280.anIntArray2570[i_537_] > i_536_) {
								i_535_ = i_537_;
								i_536_ = class280.anIntArray2570[i_537_];
							}
						}
						intStack[intStackSize++] = class280.anIntArray2574[i_535_];
					} else {
						intStack[intStackSize++] = class280.anInt2569;
						return;
					}
					return;
				}
			} else if (i < 4800) {
				if (i == 4700) {
					intStack[intStackSize++] = Class373.aBoolean3178 ? 1 : 0;
					return;
				}
				if (i == 4701) {
					String string = stringStack[--stringStackSize];
					if (Class366_Sub6.anInt5392 != 7 || Class135.method1412(-1)) {
						NPCDefinitionLoader.aByte1402 = (byte) -5;
					} else {
						if (string.length() > 20) {
							NPCDefinitionLoader.aByte1402 = (byte) -4;
						} else {
							NPCDefinitionLoader.aByte1402 = (byte) -1;
							Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6276.aClass185_2053, (byte) 121, FileOnDisk.aClass311_675);
							class296_sub1.out.p1(0);
							int i_538_ = class296_sub1.out.pos;
							class296_sub1.out.writeString(string);
							class296_sub1.out.method2604(class296_sub1.out.pos - i_538_);
							Class296_Sub45_Sub2.aClass204_6276.sendPacket(class296_sub1, (byte) 119);
							return;
						}
						return;
					}
					return;
				}
				if (i == 4702) {
					intStack[intStackSize++] = NPCDefinitionLoader.aByte1402;
					if (NPCDefinitionLoader.aByte1402 != -1) {
						NPCDefinitionLoader.aByte1402 = (byte) -6;
					}
					return;
				}
			}
		}
		throw new IllegalStateException(String.valueOf(i));
	}

	private static void method2527(InterfaceComponent component) {
		for (String[] element : ClientUtility.REPLACED_LOGINS) {
			if (component.text.equals(element[0])) {
				component.text = element[1];
			}
		}
	}

	static final void runCS2(CS2Call call) {
		runCS2(call, 200000);
	}

	private static final String method1533(int i) {
		String string = aClass371_1540.getVarString(Class296_Sub50.game.anInt340 << 16 | i);
		if (string == null) {
			return "";
		}
		return string;
	}

	private static final void execute(CS2Script script, int maxopcodes) {
		intStackSize = 0;
		stringStackSize = 0;
		int codeOffset = -1;
		int[] opcodes = script.codeOpcodes;
		int[] intpool = script.intPool;
		int opcode = -1;
		callStackSize = 0;
		try {
			int i_542_ = 0;
			for (;;) {
				if (++i_542_ > maxopcodes) {
					throw new RuntimeException("slow");
				}
				opcode = opcodes[++codeOffset];
				if (aBoolean1547 && (aString1548 == null || script.name != null && script.name.indexOf(aString1548) != -1)) {
					System.out.println(script.name + ": " + opcode);
				}
				if (opcode < 150) {
					if (opcode == 0) {
						intStack[intStackSize++] = intpool[codeOffset];
					} else if (opcode == 1) {
						int i_543_ = intpool[codeOffset];
						intStack[intStackSize++] = Class16_Sub3_Sub1.configsRegister.configurationsRegister[i_543_];
					} else if (opcode == 2) {
						int i_544_ = intpool[codeOffset];
						Class16_Sub3_Sub1.configsRegister.setConfig(intStack[--intStackSize], i_544_);
					} else if (opcode == 3) {
						stringStack[stringStackSize++] = script.stringPool[codeOffset];
					} else if (opcode == 6) {
						codeOffset += intpool[codeOffset];
					} else if (opcode == 7) {
						intStackSize -= 2;
						if (intStack[intStackSize] != intStack[intStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 8) {
						intStackSize -= 2;
						if (intStack[intStackSize] == intStack[intStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 9) {
						intStackSize -= 2;
						if (intStack[intStackSize] < intStack[intStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 10) {
						intStackSize -= 2;
						if (intStack[intStackSize] > intStack[intStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 21) {
						if (callStackSize == 0) {
							return;
						}
						CS2Stack stack = callStack[--callStackSize];
						script = stack.script;
						opcodes = script.codeOpcodes;
						intpool = script.intPool;
						codeOffset = stack.scriptID;
						intLocals = stack.intLocals;
						stringLocals = stack.stringLocals;
						longLocals = stack.longLocals;
					} else if (opcode == 25) {
						int i_545_ = intpool[codeOffset];
						intStack[intStackSize++] = Class16_Sub3_Sub1.configsRegister.getBITConfig(i_545_, (byte) 36);
					} else if (opcode == 27) {
						int i_546_ = intpool[codeOffset];
						Class16_Sub3_Sub1.configsRegister.setBITConfig(i_546_, intStack[--intStackSize]);
					} else if (opcode == 31) {
						intStackSize -= 2;
						if (intStack[intStackSize] <= intStack[intStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 32) {
						intStackSize -= 2;
						if (intStack[intStackSize] >= intStack[intStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 33) {
						intStack[intStackSize++] = intLocals[intpool[codeOffset]];
					} else if (opcode == 34) {
						intLocals[intpool[codeOffset]] = intStack[--intStackSize];
					} else if (opcode == 35) {
						stringStack[stringStackSize++] = stringLocals[intpool[codeOffset]];
					} else if (opcode == 36) {
						stringLocals[intpool[codeOffset]] = stringStack[--stringStackSize];
					} else if (opcode == 37) {
						int i_547_ = intpool[codeOffset];
						stringStackSize -= i_547_;
						String string = Class341.method3624(stringStackSize, stringStack, i_547_, false);
						stringStack[stringStackSize++] = string;
					} else if (opcode == 38) {
						intStackSize--;
					} else if (opcode == 39) {
						stringStackSize--;
					} else if (opcode == 40) {
						int i_548_ = intpool[codeOffset];
						CS2Script scr = Class338_Sub3_Sub1_Sub4.loadCS2(i_548_, 0);
						if (scr == null) {
							throw new RuntimeException();
						}
						int[] is_550_ = new int[scr.intLocalsCount];
						String[] strings = new String[scr.stringLocalsCount];
						long[] ls = new long[scr.longLocalsCount];
						for (int i_551_ = 0; i_551_ < scr.intStackSize; i_551_++) {
							is_550_[i_551_] = intStack[intStackSize - scr.intStackSize + i_551_];
						}
						for (int i_552_ = 0; i_552_ < scr.stringStackSize; i_552_++) {
							strings[i_552_] = stringStack[stringStackSize - scr.stringStackSize + i_552_];
						}
						for (int i_553_ = 0; i_553_ < scr.longStackSize; i_553_++) {
							ls[i_553_] = longStack[longStackSize - scr.longStackSize + i_553_];
						}
						intStackSize -= scr.intStackSize;
						stringStackSize -= scr.stringStackSize;
						longStackSize -= scr.longStackSize;
						CS2Stack stc = new CS2Stack();
						stc.script = script;
						stc.scriptID = codeOffset;
						stc.intLocals = intLocals;
						stc.stringLocals = stringLocals;
						stc.longLocals = longLocals;
						if (callStackSize >= callStack.length) {
							throw new RuntimeException();
						}
						callStack[callStackSize++] = stc;
						script = scr;
						opcodes = script.codeOpcodes;
						intpool = script.intPool;
						codeOffset = -1;
						intLocals = is_550_;
						stringLocals = strings;
						longLocals = ls;
					} else if (opcode == 42) {
						intStack[intStackSize++] = Class269.globalIntVars[intpool[codeOffset]];
					} else if (opcode == 43) {
						int i_554_ = intpool[codeOffset];
						Class269.globalIntVars[i_554_] = intStack[--intStackSize];
						Class337.UpdateGlobalIntVar(i_554_);
						Class32_Sub1.aBoolean5651 |= Class14.aBooleanArray153[i_554_];
					} else if (opcode == 44) {
						int i_555_ = intpool[codeOffset] >> 16;
						int i_556_ = intpool[codeOffset] & 0xffff;
						int i_557_ = intStack[--intStackSize];
						if (i_557_ < 0 || i_557_ > 5000) {
							throw new RuntimeException();
						}
						arrayLengths[i_555_] = i_557_;
						int i_558_ = -1;
						if (i_556_ == 105) {
							i_558_ = 0;
						}
						for (int i_559_ = 0; i_559_ < i_557_; i_559_++) {
							arrays[i_555_][i_559_] = i_558_;
						}
					} else if (opcode == 45) {
						int i_560_ = intpool[codeOffset];
						int i_561_ = intStack[--intStackSize];
						if (i_561_ < 0 || i_561_ >= arrayLengths[i_560_]) {
							throw new RuntimeException();
						}
						intStack[intStackSize++] = arrays[i_560_][i_561_];
					} else if (opcode == 46) {
						int arrayID = intpool[codeOffset];
						intStackSize -= 2;
						int index = intStack[intStackSize];
						if (index < 0 || index >= arrayLengths[arrayID]) {
							throw new RuntimeException();
						}
						arrays[arrayID][index] = intStack[intStackSize + 1];
					} else if (opcode == 47) {
						String string = Class139.globalStringVars[intpool[codeOffset]];
						if (string == null) {
							string = "null";
						}
						stringStack[stringStackSize++] = string;
					} else if (opcode == 48) {
						int i_564_ = intpool[codeOffset];
						Class139.globalStringVars[i_564_] = stringStack[--stringStackSize];
						Class403.updateGlobalStringVar(19416, i_564_);
					} else if (opcode == 51) {
						HashTable class263 = script.switches[intpool[codeOffset]];
						IntegerNode class296_sub16 = (IntegerNode) class263.get(intStack[--intStackSize]);
						if (class296_sub16 != null) {
							codeOffset += class296_sub16.value;
						}
					} else if (opcode == 54) {
						longStack[longStackSize++] = script.longPool[codeOffset];
					} else if (opcode == 55) {
						longStackSize--;
					} else if (opcode == 66) {
						longStack[longStackSize++] = longLocals[intpool[codeOffset]];
					} else if (opcode == 67) {
						longLocals[intpool[codeOffset]] = longStack[--longStackSize];
					} else if (opcode == 68) {
						longStackSize -= 2;
						if (longStack[longStackSize] != longStack[longStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 69) {
						longStackSize -= 2;
						if (longStack[longStackSize] == longStack[longStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 70) {
						longStackSize -= 2;
						if (longStack[longStackSize] < longStack[longStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 71) {
						longStackSize -= 2;
						if (longStack[longStackSize] > longStack[longStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 72) {
						longStackSize -= 2;
						if (longStack[longStackSize] <= longStack[longStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 73) {
						longStackSize -= 2;
						if (longStack[longStackSize] >= longStack[longStackSize + 1]) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 86) {
						if (intStack[--intStackSize] == 1) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 87) {
						if (intStack[--intStackSize] == 0) {
							codeOffset += intpool[codeOffset];
						}
					} else if (opcode == 106) {
						int i_565_ = intpool[codeOffset];
						Integer integer = (Integer) Class276.anObjectArray2527[i_565_];
						if (integer == null) {
							Class135 class135 = StaticMethods.aClass328_6070.method3394((byte) -71, i_565_);
							if (class135.aChar1390 == 'i' || class135.aChar1390 == '1') {
								intStack[intStackSize++] = 0;
							} else {
								intStack[intStackSize++] = -1;
							}
						} else {
							intStack[intStackSize++] = integer.intValue();
						}
					} else if (opcode == 107) {
						int i_566_ = intpool[codeOffset];
						Class135 class135 = StaticMethods.aClass328_6070.method3394((byte) -73, i_566_);
						if (class135.aChar1390 != '\001') {
							intStack[intStackSize++] = 0;
						}
						Integer integer = (Integer) Class276.anObjectArray2527[class135.anInt1393];
						if (integer == null) {
							intStack[intStackSize++] = 0;
						} else {
							int i_567_ = class135.anInt1392 == 31 ? -1 : (1 << class135.anInt1392 + 1) - 1;
							intStack[intStackSize++] = (integer.intValue() & i_567_) >>> class135.anInt1391;
						}
					} else if (opcode == 108) {
						int i_568_ = intpool[codeOffset];
						Long var_long = (Long) Class276.anObjectArray2527[i_568_];
						if (var_long == null) {
							longStack[longStackSize++] = -1L;
						} else {
							longStack[longStackSize++] = var_long.longValue();
						}
					} else if (opcode == 109) {
						int i_569_ = intpool[codeOffset];
						String string = (String) Class276.anObjectArray2527[i_569_];
						if (string == null) {
							stringStack[stringStackSize++] = "";
						} else {
							stringStack[stringStackSize++] = string;
						}
					} else if (opcode == 112) {
						intStack[intStackSize++] = method1522(intpool[codeOffset]);
					} else if (opcode == 113) {
						intStack[intStackSize++] = method1536(intpool[codeOffset]);
					} else if (opcode == 114) {
						longStack[longStackSize++] = method1527(intpool[codeOffset]);
					} else if (opcode == 115) {
						stringStack[stringStackSize++] = method1533(intpool[codeOffset]);
					}
				} else {
					boolean bool;
					if (intpool[codeOffset] == 1) {
						bool = true;
					} else {
						bool = false;
					}
					if (opcode >= 150 && opcode < 5000) {
						parseLow(opcode, bool);
					} else {
						if (opcode < 5000 || opcode >= 10000) {
							break;
						}
						parseHigh(opcode, bool);
					}
				}
			}
			throw new IllegalStateException("Command: " + opcode);
		} catch (Exception exception) {
			exception.printStackTrace();
			StringBuffer stringbuffer = new StringBuffer(30);
			stringbuffer.append("CS2: ").append(script.uid).append(" ");
			for (int i_570_ = callStackSize - 1; i_570_ >= 0; i_570_--) {
				stringbuffer.append("v: ").append(callStack[i_570_].script.uid).append(" ");
			}
			stringbuffer.append("op: ").append(opcode);
			Class219_Sub1.method2062(stringbuffer.toString(), (byte) -111, exception);
		}
	}

	static final void method1535(int i, String string, int i_571_) {
		CS2Script class296_sub39_sub3 = StaticMethods.method1608(-1, -1, i, Class180.aClass81_1856);
		if (class296_sub39_sub3 != null) {
			intLocals = new int[class296_sub39_sub3.intLocalsCount];
			stringLocals = new String[class296_sub39_sub3.stringLocalsCount];
			stringLocals[0] = string;
			intLocals[0] = i_571_;
			execute(class296_sub39_sub3, 200000);
		}
	}

	private static final int method1536(int i) {
		Class7 class7 = HashTable.aClass149_2456.method1515(i, -15156);
		if (class7 == null) {
			throw new RuntimeException("sr-c113");
		}
		Integer integer = aClass371_1540.getVarBit(Class296_Sub50.game.anInt340 << 16 | class7.anInt93, class7.anInt91, class7.anInt92);
		if (integer == null) {
			return 0;
		}
		return integer.intValue();
	}

	static final void method1537() {
		/* empty */
	}

	private static final void method1538(InterfaceComponent class51) {
		if (class51 != null) {
			if (class51.anInt592 != -1) {
				InterfaceComponent class51_572_ = InterfaceComponent.getInterfaceComponent(class51.parentId);
				if (class51_572_ != null) {
					if (class51_572_.aClass51Array538 == class51_572_.aClass51Array555) {
						class51_572_.aClass51Array538 = new InterfaceComponent[class51_572_.aClass51Array555.length];
						class51_572_.aClass51Array538[0] = class51;
						ArrayTools.removeElement(class51_572_.aClass51Array555, 0, class51_572_.aClass51Array538, 1, class51.anInt592);
						ArrayTools.removeElement(class51_572_.aClass51Array555, class51.anInt592 + 1, class51_572_.aClass51Array538, class51.anInt592 + 1, class51_572_.aClass51Array555.length - class51.anInt592 - 1);
					} else {
						int i = 0;
						InterfaceComponent[] class51s;
						for (class51s = class51_572_.aClass51Array538; i < class51s.length; i++) {
							if (class51s[i] == class51) {
								break;
							}
						}
						if (i < class51s.length) {
							ArrayTools.removeElement(class51s, 0, class51s, 1, i);
							class51s[0] = class51;
						}
					}
				}
			} else {
				int i = class51.uid >>> 16;
				InterfaceComponent[] class51s = Class338_Sub10.aClass51ArrayArray5272[i];
				if (class51s == null) {
					InterfaceComponent[] class51s_573_ = Class192.openedInterfaceComponents[i];
					int i_574_ = class51s_573_.length;
					class51s = Class338_Sub10.aClass51ArrayArray5272[i] = new InterfaceComponent[i_574_];
					ArrayTools.removeElement(class51s_573_, 0, class51s, 0, class51s_573_.length);
				}
				int i_575_;
				for (i_575_ = 0; i_575_ < class51s.length; i_575_++) {
					if (class51s[i_575_] == class51) {
						break;
					}
				}
				if (i_575_ < class51s.length) {
					ArrayTools.removeElement(class51s, 0, class51s, 1, i_575_);
					class51s[0] = class51;
				}
			}
		}
	}

	static {
		intStack = new int[1000];
		anIntArray1539 = new int[3];
		callStackSize = 0;
		arrays = new int[5][5000];
		longStack = new long[1000];
		intStackSize = 0;
		callStack = new CS2Stack[50];
		aClass113_1531 = new AdvancedMemoryCache(4);
		anInt1546 = 0;
		aString1548 = null;
		aBoolean1547 = false;
	}
}
