package net.zaros.client;

/* Class296_Sub13 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class296_Sub13 extends Node {
	static Js5 aClass138_4653;
	static int anInt4654 = 0;
	static short aShort4655;
	int anInt4656;
	int anInt4657;

	static final Class213[] method2505(int i) {
		if (i != -10406) {
			return null;
		}
		return new Class213[]{StaticMethods.aClass213_5917, Class296_Sub51_Sub31.aClass213_6503, Class388.aClass213_3276};
	}

	public static final String decodeBase37(long l) {
		if (l <= 0L || l >= 6582952005840035281L) {
			return null;
		}
		if (l % 37L == 0L) {
			return null;
		}
		int i_1_ = 0;
		long l_2_ = l;
		while (l_2_ != 0L) {
			l_2_ /= 37L;
			i_1_++;
		}
		StringBuffer stringbuffer = new StringBuffer(i_1_);
		while (l != 0L) {
			long l_3_ = l;
			l /= 37L;
			stringbuffer.append(Class296_Sub51_Sub2.aCharArray6339[(int) (-(l * 37L) + l_3_)]);
		}
		return stringbuffer.reverse().toString();
	}

	static final void method2507(boolean bool, String string, Class398 class398, boolean bool_4_, int i) {
		if (i == 37) {
			Class127.method1355("openjs", class398, string, bool_4_, bool, 112);
		}
	}

	public static void method2508(int i) {
		aClass138_4653 = null;
		if (i != 6075) {
			method2507(false, null, null, true, 28);
		}
	}

	public Class296_Sub13() {
		/* empty */
	}
}
