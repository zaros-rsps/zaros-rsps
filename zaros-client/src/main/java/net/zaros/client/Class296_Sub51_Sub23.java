package net.zaros.client;

/* Class296_Sub51_Sub23 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub23 extends TextureOperation {
	private short[] aShortArray6455;
	private int anInt6456 = 0;
	static int anInt6457;
	static Class241 aClass241_6458;
	private int[] anIntArray6459;
	private int[][] anIntArrayArray6460;
	static int anInt6461 = 0;
	private int[] anIntArray6462;

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		if (i_0_ == 0) {
			anInt6456 = class296_sub17.g1();
			anIntArrayArray6460 = new int[class296_sub17.g1()][2];
			for (int i_1_ = 0; i_1_ < anIntArrayArray6460.length; i_1_++) {
				anIntArrayArray6460[i_1_][0] = class296_sub17.g2();
				anIntArrayArray6460[i_1_][1] = class296_sub17.g2();
			}
		}
		if (i > -84)
			anInt6456 = 107;
	}

	public static void method3137(byte i) {
		int i_2_ = -47 % ((-50 - i) / 63);
		aClass241_6458 = null;
	}

	private final void method3138(boolean bool) {
		if (bool != true)
			method3141(-53);
		int[] is = anIntArrayArray6460[0];
		int[] is_3_ = anIntArrayArray6460[1];
		int[] is_4_ = anIntArrayArray6460[anIntArrayArray6460.length - 2];
		int[] is_5_ = anIntArrayArray6460[anIntArrayArray6460.length - 1];
		anIntArray6462 = new int[]{-is_3_[0] + (is[0] + is[0]), -is_3_[1] + is[1] + is[1]};
		anIntArray6459 = new int[]{is_4_[0] - is_5_[0] + is_4_[0], is_4_[1] - (-is_4_[1] + is_5_[1])};
	}

	private final int[] method3139(int i, byte i_6_) {
		if (i_6_ != -36)
			return null;
		if (i < 0)
			return anIntArray6462;
		if (anIntArrayArray6460.length <= i)
			return anIntArray6459;
		return anIntArrayArray6460[i];
	}

	final void method3076(byte i) {
		if (anIntArrayArray6460 == null)
			anIntArrayArray6460 = new int[][]{new int[2], {4096, 4096}};
		if (anIntArrayArray6460.length < 2)
			throw new RuntimeException("Curve operation requires at least two markers");
		if (anInt6456 == 2)
			method3138(true);
		Class367.method3800(4096);
		method3141(-30873);
		int i_7_ = 7 / ((i + 58) / 40);
	}

	final int[] get_monochrome_output(int i, int i_8_) {
		int[] is = aClass318_5035.method3335(i_8_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int[] is_9_ = this.method3064(0, i, i_8_);
			for (int i_10_ = 0; i_10_ < Class41_Sub10.anInt3769; i_10_++) {
				int i_11_ = is_9_[i_10_] >> 4;
				if (i_11_ < 0)
					i_11_ = 0;
				if (i_11_ > 256)
					i_11_ = 256;
				is[i_10_] = aShortArray6455[i_11_];
			}
		}
		if (i != 0)
			return null;
		return is;
	}

	static final int method3140(byte i, int i_12_) {
		if (i != 75)
			method3140((byte) -65, 88);
		return (i_12_ & 0x3f966) >> 11;
	}

	public Class296_Sub51_Sub23() {
		super(1, true);
		aShortArray6455 = new short[257];
	}

	private final void method3141(int i) {
		if (i != -30873)
			anInt6456 = 23;
		int i_13_ = anInt6456;
		while_139_ : do {
			do {
				if (i_13_ != 2) {
					if (i_13_ != 1)
						break;
				} else {
					for (i_13_ = 0; i_13_ < 257; i_13_++) {
						int i_14_ = i_13_ << 4;
						int i_15_;
						for (i_15_ = 1; anIntArrayArray6460.length - 1 > i_15_; i_15_++) {
							if (anIntArrayArray6460[i_15_][0] > i_14_)
								break;
						}
						int[] is = anIntArrayArray6460[i_15_ - 1];
						int[] is_16_ = anIntArrayArray6460[i_15_];
						int i_17_ = method3139(i_15_ - 2, (byte) -36)[1];
						int i_18_ = is[1];
						int i_19_ = is_16_[1];
						int i_20_ = method3139(i_15_ + 1, (byte) -36)[1];
						int i_21_ = (i_14_ - is[0] << 12) / (is_16_[0] - is[0]);
						int i_22_ = i_21_ * i_21_ >> 12;
						int i_23_ = i_18_ - i_17_ + (i_20_ - i_19_);
						int i_24_ = i_17_ - i_18_ - i_23_;
						int i_25_ = i_19_ - i_17_;
						int i_26_ = i_18_;
						int i_27_ = i_22_ * (i_21_ * i_23_ >> 12) >> 12;
						int i_28_ = i_22_ * i_24_ >> 12;
						int i_29_ = i_21_ * i_25_ >> 12;
						int i_30_ = i_29_ + i_27_ + (i_28_ + i_26_);
						if (i_30_ <= -32768)
							i_30_ = -32767;
						if (i_30_ >= 32768)
							i_30_ = 32767;
						aShortArray6455[i_13_] = (short) i_30_;
					}
					break while_139_;
				}
				for (i_13_ = 0; i_13_ < 257; i_13_++) {
					int i_31_ = i_13_ << 4;
					int i_32_;
					for (i_32_ = 1; i_32_ < anIntArrayArray6460.length - 1; i_32_++) {
						if (i_31_ < anIntArrayArray6460[i_32_][0])
							break;
					}
					int[] is = anIntArrayArray6460[i_32_ - 1];
					int[] is_33_ = anIntArrayArray6460[i_32_];
					int i_34_ = (i_31_ - is[0] << 12) / (-is[0] + is_33_[0]);
					int i_35_ = ((-Class259.anIntArray2419[(i_34_ & 0x1ffb) >> 5] + 4096) >> 1);
					int i_36_ = -i_35_ + 4096;
					int i_37_ = is_33_[1] * i_35_ + i_36_ * is[1] >> 12;
					if (i_37_ <= -32768)
						i_37_ = -32767;
					if (i_37_ >= 32768)
						i_37_ = 32767;
					aShortArray6455[i_13_] = (short) i_37_;
				}
				break while_139_;
			} while (false);
			for (i_13_ = 0; i_13_ < 257; i_13_++) {
				int i_38_ = i_13_ << 4;
				int i_39_;
				for (i_39_ = 1; i_39_ < anIntArrayArray6460.length - 1; i_39_++) {
					if (anIntArrayArray6460[i_39_][0] > i_38_)
						break;
				}
				int[] is = anIntArrayArray6460[i_39_ - 1];
				int[] is_40_ = anIntArrayArray6460[i_39_];
				int i_41_ = (-is[0] + i_38_ << 12) / (-is[0] + is_40_[0]);
				int i_42_ = -i_41_ + 4096;
				int i_43_ = i_42_ * is[1] + is_40_[1] * i_41_ >> 12;
				if (i_43_ <= -32768)
					i_43_ = -32767;
				if (i_43_ >= 32768)
					i_43_ = 32767;
				aShortArray6455[i_13_] = (short) i_43_;
			}
		} while (false);
		anInt6457++;
	}
}
