package net.zaros.client;

/* Class295_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class295_Sub1 extends Class295 implements Interface20 {
	static int[] anIntArray3691;
	private int anInt3692;

	public final void method75(int i, byte[] is, int i_0_, boolean bool) {
		if (bool)
			anIntArray3691 = null;
		this.method2422(is, i);
		anInt3692 = i_0_;
	}

	public final int method77(byte i) {
		int i_1_ = -7 / ((i + 19) / 34);
		return 0;
	}

	public static void method2423(int i) {
		if (i > -121)
			anIntArray3691 = null;
		anIntArray3691 = null;
	}

	static final boolean method2424(int i, byte i_2_) {
		if (i_2_ != 101)
			anIntArray3691 = null;
		if (i == 2 || i == 15 || i == 58 || i == 11 || i == 13 || i == 1003)
			return true;
		if (i == 10)
			return true;
		return false;
	}

	public final int method76(boolean bool) {
		if (bool != true)
			method75(77, null, -7, true);
		return anInt3692;
	}

	Class295_Sub1(ha_Sub3 var_ha_Sub3, int i, byte[] is, int i_3_) {
		super(var_ha_Sub3, is, i_3_);
		anInt3692 = i;
	}

	public final long method74(byte i) {
		if (i < 61)
			return -99L;
		return aBuffer2692.getAddress();
	}
}
