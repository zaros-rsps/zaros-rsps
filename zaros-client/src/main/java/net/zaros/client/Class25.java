package net.zaros.client;

/* Class25 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class25 {
	private Js5 aClass138_280;
	private Js5 aClass138_281;
	static OutgoingPacket aClass311_282 = new OutgoingPacket(50, -1);
	private HashTable aClass263_283 = new HashTable(256);
	private HashTable aClass263_284 = new HashTable(256);
	static int anInt285;

	static final void method304(byte i) {
		if (Class296_Sub51_Sub15.aClass54_6426 != null)
			Class296_Sub51_Sub15.aClass54_6426.method649(0);
		if (Class373_Sub3.aThread5609 != null) {
			for (;;) {
				try {
					Class373_Sub3.aThread5609.join();
					break;
				} catch (InterruptedException interruptedexception) {
					/* empty */
				}
			}
		}
		if (i <= 123)
			aClass311_282 = null;
	}

	private final Class296_Sub19_Sub1 method305(int i, int i_0_, int[] is, int i_1_) {
		int i_2_ = i_1_ ^ (i >>> 12 | (i & 0x30000fff) << 4);
		i_2_ |= i << 16;
		long l = (long) i_2_;
		Class296_Sub19_Sub1 class296_sub19_sub1 = (Class296_Sub19_Sub1) aClass263_284.get(l);
		if (class296_sub19_sub1 != null)
			return class296_sub19_sub1;
		if (is != null && is[0] <= 0)
			return null;
		if (i_0_ != 0)
			return null;
		Class171 class171 = Class171.method1675(aClass138_281, i, i_1_);
		if (class171 == null)
			return null;
		class296_sub19_sub1 = class171.method1676();
		aClass263_284.put(l, class296_sub19_sub1);
		if (is != null)
			is[0] -= class296_sub19_sub1.aByteArray6051.length;
		return class296_sub19_sub1;
	}

	public static void method306(byte i) {
		aClass311_282 = null;
		if (i >= -68)
			method306((byte) -82);
	}

	final Class296_Sub19_Sub1 method307(int i, int i_3_, int[] is) {
		if (aClass138_280.getLastGroupId() == 1)
			return method308(is, 0, (byte) -103, i_3_);
		if (i == aClass138_280.getLastFileId(i_3_))
			return method308(is, i_3_, (byte) 44, 0);
		throw new RuntimeException();
	}

	private final Class296_Sub19_Sub1 method308(int[] is, int i, byte i_4_, int i_5_) {
		int i_6_ = i_5_ ^ (i << 4 & 0xfff9 | i >>> 12);
		i_6_ |= i << 16;
		long l = 0x100000000L ^ (long) i_6_;
		Class296_Sub19_Sub1 class296_sub19_sub1 = (Class296_Sub19_Sub1) aClass263_284.get(l);
		if (class296_sub19_sub1 != null)
			return class296_sub19_sub1;
		if (is != null && is[0] <= 0)
			return null;
		Class296_Sub18 class296_sub18 = (Class296_Sub18) aClass263_283.get(l);
		if (class296_sub18 == null) {
			class296_sub18 = Class296_Sub18.method2647(aClass138_280, i, i_5_);
			if (class296_sub18 == null)
				return null;
			aClass263_283.put(l, class296_sub18);
		}
		class296_sub19_sub1 = class296_sub18.method2644(is);
		if (class296_sub19_sub1 == null)
			return null;
		class296_sub18.unlink();
		aClass263_284.put(l, class296_sub19_sub1);
		int i_7_ = 3 % ((i_4_ + 12) / 48);
		return class296_sub19_sub1;
	}

	final Class296_Sub19_Sub1 method309(int i, int i_8_, int[] is) {
		int i_9_ = -104 / ((-82 - i) / 34);
		if (aClass138_281.getLastGroupId() == 1)
			return method305(0, 0, is, i_8_);
		if (aClass138_281.getLastFileId(i_8_) == 1)
			return method305(i_8_, 0, is, 0);
		throw new RuntimeException();
	}

	Class25(Js5 class138, Js5 class138_10_) {
		aClass138_281 = class138;
		aClass138_280 = class138_10_;
	}
}
