package net.zaros.client;

/* Class55_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class55_Sub4 extends Class55 {
	private int[] anIntArray3880;
	private ha_Sub2 aHa_Sub2_3881;
	private byte[][] aByteArrayArray3882;
	private int[] anIntArray3883;
	private int[] anIntArray3884;
	private int[] anIntArray3885;

	private final void method661(byte[] is, int[] is_0_, int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_,
			int i_6_, int i_7_, int i_8_, int i_9_, aa var_aa, int i_10_, int i_11_) {
		aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
		int[] is_12_ = var_aa_Sub2.anIntArray3726;
		int[] is_13_ = var_aa_Sub2.anIntArray3729;
		int i_14_ = i_8_;
		if (i_11_ > i_14_) {
			i_14_ = i_11_;
			i_2_ += (i_11_ - i_8_) * aHa_Sub2_3881.anInt4084;
			i_1_ += (i_11_ - i_8_) * i_9_;
		}
		int i_15_ = (i_11_ + is_12_.length < i_8_ + i_4_ ? i_11_ + is_12_.length : i_8_ + i_4_);
		for (int i_16_ = i_14_; i_16_ < i_15_; i_16_++) {
			int i_17_ = i_10_ + is_12_[i_16_ - i_11_];
			int i_18_ = is_13_[i_16_ - i_11_];
			int i_19_ = i_3_;
			if (i_7_ > i_17_) {
				int i_20_ = i_7_ - i_17_;
				if (i_20_ >= i_18_) {
					i_1_ += i_3_ + i_6_;
					i_2_ += i_3_ + i_5_;
					continue;
				}
				i_18_ -= i_20_;
			} else {
				int i_21_ = i_17_ - i_7_;
				if (i_21_ >= i_3_) {
					i_1_ += i_3_ + i_6_;
					i_2_ += i_3_ + i_5_;
					continue;
				}
				i_1_ += i_21_;
				i_19_ -= i_21_;
				i_2_ += i_21_;
			}
			int i_22_ = 0;
			if (i_19_ < i_18_)
				i_18_ = i_19_;
			else
				i_22_ = i_19_ - i_18_;
			for (int i_23_ = 0; i_23_ < i_18_; i_23_++) {
				if (is[i_1_++] != 0)
					is_0_[i_2_++] = i;
				else
					i_2_++;
			}
			i_1_ += i_22_ + i_6_;
			i_2_ += i_22_ + i_5_;
		}
	}

	private final void method662(byte[] is, int[] is_24_, int i, int i_25_, int i_26_, int i_27_, int i_28_, int i_29_,
			int i_30_, int i_31_, int i_32_, int i_33_, aa var_aa, int i_34_, int i_35_) {
		aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
		int[] is_36_ = var_aa_Sub2.anIntArray3726;
		int[] is_37_ = var_aa_Sub2.anIntArray3729;
		int i_38_ = i_31_ - aHa_Sub2_3881.anInt4079;
		int i_39_ = i_32_;
		if (i_35_ > i_39_) {
			i_39_ = i_35_;
			i_26_ += (i_35_ - i_32_) * aHa_Sub2_3881.anInt4084;
			i_25_ += (i_35_ - i_32_) * i_33_;
		}
		int i_40_ = (i_35_ + is_36_.length < i_32_ + i_28_ ? i_35_ + is_36_.length : i_32_ + i_28_);
		int i_41_ = i >>> 24;
		int i_42_ = 255 - i_41_;
		for (int i_43_ = i_39_; i_43_ < i_40_; i_43_++) {
			int i_44_ = is_36_[i_43_ - i_35_] + i_34_;
			int i_45_ = is_37_[i_43_ - i_35_];
			int i_46_ = i_27_;
			if (i_38_ > i_44_) {
				int i_47_ = i_38_ - i_44_;
				if (i_47_ >= i_45_) {
					i_25_ += i_27_ + i_30_;
					i_26_ += i_27_ + i_29_;
					continue;
				}
				i_45_ -= i_47_;
			} else {
				int i_48_ = i_44_ - i_38_;
				if (i_48_ >= i_27_) {
					i_25_ += i_27_ + i_30_;
					i_26_ += i_27_ + i_29_;
					continue;
				}
				i_25_ += i_48_;
				i_46_ -= i_48_;
				i_26_ += i_48_;
			}
			int i_49_ = 0;
			if (i_46_ < i_45_)
				i_45_ = i_46_;
			else
				i_49_ = i_46_ - i_45_;
			for (int i_50_ = -i_45_; i_50_ < 0; i_50_++) {
				if (is[i_25_++] != 0) {
					int i_51_ = ((((i & 0xff00ff) * i_41_ & ~0xff00ff) + ((i & 0xff00) * i_41_ & 0xff0000)) >> 8);
					int i_52_ = is_24_[i_26_];
					is_24_[i_26_++] = ((((i_52_ & 0xff00ff) * i_42_ & ~0xff00ff)
							+ ((i_52_ & 0xff00) * i_42_ & 0xff0000)) >> 8) + i_51_;
				} else
					i_26_++;
			}
			i_25_ += i_49_ + i_30_;
			i_26_ += i_49_ + i_29_;
		}
	}

	Class55_Sub4(ha_Sub2 var_ha_Sub2, Class92 class92, Class186[] class186s, int[] is, int[] is_53_) {
		super(var_ha_Sub2, class92);
		aHa_Sub2_3881 = var_ha_Sub2;
		anIntArray3885 = is;
		anIntArray3880 = is_53_;
		aByteArrayArray3882 = new byte[class186s.length][];
		anIntArray3884 = new int[class186s.length];
		anIntArray3883 = new int[class186s.length];
		for (int i = 0; i < class186s.length; i++) {
			aByteArrayArray3882[i] = class186s[i].aByteArray1901;
			anIntArray3884[i] = class186s[i].anInt1906;
			anIntArray3883[i] = class186s[i].anInt1903;
		}
	}

	private final void method663(byte[] is, int[] is_54_, int i, int i_55_, int i_56_, int i_57_, int i_58_, int i_59_,
			int i_60_) {
		int i_61_ = i >>> 24;
		int i_62_ = 255 - i_61_;
		for (int i_63_ = -i_58_; i_63_ < 0; i_63_++) {
			for (int i_64_ = -i_57_; i_64_ < 0; i_64_++) {
				if (is[i_55_++] != 0) {
					int i_65_ = ((((i & 0xff00ff) * i_61_ & ~0xff00ff) + ((i & 0xff00) * i_61_ & 0xff0000)) >> 8);
					int i_66_ = is_54_[i_56_];
					is_54_[i_56_++] = ((((i_66_ & 0xff00ff) * i_62_ & ~0xff00ff)
							+ ((i_66_ & 0xff00) * i_62_ & 0xff0000)) >> 8) + i_65_;
				} else
					i_56_++;
			}
			i_56_ += i_59_;
			i_55_ += i_60_;
		}
	}

	public void fa(char c, int i, int i_67_, int i_68_, boolean bool) {
		i += anIntArray3883[c];
		i_67_ += anIntArray3884[c];
		int i_69_ = anIntArray3885[c];
		int i_70_ = anIntArray3880[c];
		int i_71_ = aHa_Sub2_3881.anInt4084;
		int i_72_ = i + i_67_ * i_71_;
		int i_73_ = i_71_ - i_69_;
		int i_74_ = 0;
		int i_75_ = 0;
		if (i_67_ < aHa_Sub2_3881.anInt4085) {
			int i_76_ = aHa_Sub2_3881.anInt4085 - i_67_;
			i_70_ -= i_76_;
			i_67_ = aHa_Sub2_3881.anInt4085;
			i_75_ += i_76_ * i_69_;
			i_72_ += i_76_ * i_71_;
		}
		if (i_67_ + i_70_ > aHa_Sub2_3881.anInt4078)
			i_70_ -= i_67_ + i_70_ - aHa_Sub2_3881.anInt4078;
		if (i < aHa_Sub2_3881.anInt4079) {
			int i_77_ = aHa_Sub2_3881.anInt4079 - i;
			i_69_ -= i_77_;
			i = aHa_Sub2_3881.anInt4079;
			i_75_ += i_77_;
			i_72_ += i_77_;
			i_74_ += i_77_;
			i_73_ += i_77_;
		}
		if (i + i_69_ > aHa_Sub2_3881.anInt4104) {
			int i_78_ = i + i_69_ - aHa_Sub2_3881.anInt4104;
			i_69_ -= i_78_;
			i_74_ += i_78_;
			i_73_ += i_78_;
		}
		if (i_69_ > 0 && i_70_ > 0) {
			if ((i_68_ & ~0xffffff) == -16777216)
				method664(aByteArrayArray3882[c], aHa_Sub2_3881.anIntArray4108, i_68_, i_75_, i_72_, i_69_, i_70_,
						i_73_, i_74_);
			else if ((i_68_ & ~0xffffff) != 0)
				method663(aByteArrayArray3882[c], aHa_Sub2_3881.anIntArray4108, i_68_, i_75_, i_72_, i_69_, i_70_,
						i_73_, i_74_);
		}
	}

	private final void method664(byte[] is, int[] is_79_, int i, int i_80_, int i_81_, int i_82_, int i_83_, int i_84_,
			int i_85_) {
		int i_86_ = -(i_82_ >> 2);
		i_82_ = -(i_82_ & 0x3);
		for (int i_87_ = -i_83_; i_87_ < 0; i_87_++) {
			for (int i_88_ = i_86_; i_88_ < 0; i_88_++) {
				if (is[i_80_++] != 0)
					is_79_[i_81_++] = i;
				else
					i_81_++;
				if (is[i_80_++] != 0)
					is_79_[i_81_++] = i;
				else
					i_81_++;
				if (is[i_80_++] != 0)
					is_79_[i_81_++] = i;
				else
					i_81_++;
				if (is[i_80_++] != 0)
					is_79_[i_81_++] = i;
				else
					i_81_++;
			}
			for (int i_89_ = i_82_; i_89_ < 0; i_89_++) {
				if (is[i_80_++] != 0)
					is_79_[i_81_++] = i;
				else
					i_81_++;
			}
			i_81_ += i_84_;
			i_80_ += i_85_;
		}
	}

	public void a(char c, int i, int i_90_, int i_91_, boolean bool, aa var_aa, int i_92_, int i_93_) {
		if (var_aa == null)
			fa(c, i, i_90_, i_91_, bool);
		else {
			i += anIntArray3883[c];
			i_90_ += anIntArray3884[c];
			int i_94_ = anIntArray3885[c];
			int i_95_ = anIntArray3880[c];
			int i_96_ = aHa_Sub2_3881.anInt4084;
			int i_97_ = i + i_90_ * i_96_;
			int i_98_ = i_96_ - i_94_;
			int i_99_ = 0;
			int i_100_ = 0;
			if (i_90_ < aHa_Sub2_3881.anInt4085) {
				int i_101_ = aHa_Sub2_3881.anInt4085 - i_90_;
				i_95_ -= i_101_;
				i_90_ = aHa_Sub2_3881.anInt4085;
				i_100_ += i_101_ * i_94_;
				i_97_ += i_101_ * i_96_;
			}
			if (i_90_ + i_95_ > aHa_Sub2_3881.anInt4078)
				i_95_ -= i_90_ + i_95_ - aHa_Sub2_3881.anInt4078;
			if (i < aHa_Sub2_3881.anInt4079) {
				int i_102_ = aHa_Sub2_3881.anInt4079 - i;
				i_94_ -= i_102_;
				i = aHa_Sub2_3881.anInt4079;
				i_100_ += i_102_;
				i_97_ += i_102_;
				i_99_ += i_102_;
				i_98_ += i_102_;
			}
			if (i + i_94_ > aHa_Sub2_3881.anInt4104) {
				int i_103_ = i + i_94_ - aHa_Sub2_3881.anInt4104;
				i_94_ -= i_103_;
				i_99_ += i_103_;
				i_98_ += i_103_;
			}
			if (i_94_ > 0 && i_95_ > 0) {
				if ((i_91_ & ~0xffffff) == -16777216)
					method661(aByteArrayArray3882[c], aHa_Sub2_3881.anIntArray4108, i_91_, i_100_, i_97_, i_94_, i_95_,
							i_98_, i_99_, i, i_90_, anIntArray3885[c], var_aa, i_92_, i_93_);
				else
					method662(aByteArrayArray3882[c], aHa_Sub2_3881.anIntArray4108, i_91_, i_100_, i_97_, i_94_, i_95_,
							i_98_, i_99_, i, i_90_, anIntArray3885[c], var_aa, i_92_, i_93_);
			}
		}
	}
}
