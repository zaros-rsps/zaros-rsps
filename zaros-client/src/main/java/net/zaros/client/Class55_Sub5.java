package net.zaros.client;

import jaggl.OpenGL;

final class Class55_Sub5 extends Class55 {
	private ha_Sub3 aHa_Sub3_3886;
	private Class69_Sub1_Sub1 aClass69_Sub1_Sub1_3887;
	private boolean aBoolean3888;
	private Class341 aClass341_3889;

	public void fa(char c, int i, int i_0_, int i_1_, boolean bool) {
		aHa_Sub3_3886.method1337((byte) 37);
		aHa_Sub3_3886.method1316(aClass69_Sub1_Sub1_3887, (byte) -111);
		if (aBoolean3888 || bool) {
			aHa_Sub3_3886.method1306(7681, 8448, -22394);
			aHa_Sub3_3886.method1283(0, 34168, 768, (byte) -114);
		} else
			aHa_Sub3_3886.method1306(7681, 7681, -22394);
		OpenGL.glColor4ub((byte) (i_1_ >> 16), (byte) (i_1_ >> 8), (byte) i_1_, (byte) (i_1_ >> 24));
		OpenGL.glTranslatef((float) i, (float) i_0_, 0.0F);
		aClass341_3889.method3625((byte) 117, c);
		OpenGL.glLoadIdentity();
		if (aBoolean3888 || bool)
			aHa_Sub3_3886.method1283(0, 5890, 768, (byte) -124);
	}

	public void a(char c, int i, int i_2_, int i_3_, boolean bool, aa var_aa, int i_4_, int i_5_) {
		aa_Sub1 var_aa_Sub1 = (aa_Sub1) var_aa;
		Class69_Sub1_Sub1 class69_sub1_sub1 = var_aa_Sub1.aClass69_Sub1_Sub1_3720;
		aHa_Sub3_3886.method1337((byte) 37);
		aHa_Sub3_3886.method1316(aClass69_Sub1_Sub1_3887, (byte) -106);
		if (aBoolean3888 || bool) {
			aHa_Sub3_3886.method1306(7681, 8448, -22394);
			aHa_Sub3_3886.method1283(0, 34168, 768, (byte) -114);
		} else
			aHa_Sub3_3886.method1306(7681, 7681, -22394);
		aHa_Sub3_3886.method1330(125, 1);
		aHa_Sub3_3886.method1316(class69_sub1_sub1, (byte) -113);
		aHa_Sub3_3886.method1306(7681, 8448, -22394);
		aHa_Sub3_3886.method1283(0, 34168, 768, (byte) -116);
		OpenGL.glTexGeni(8192, 9472, 9216);
		OpenGL.glTexGeni(8193, 9472, 9216);
		float f = (class69_sub1_sub1.aFloat6687 / (float) class69_sub1_sub1.anInt6683);
		float f_6_ = (class69_sub1_sub1.aFloat6684 / (float) class69_sub1_sub1.anInt6688);
		OpenGL.glTexGenfv(8192, 9474, new float[] { f, 0.0F, 0.0F, (float) -i_4_ * f }, 0);
		OpenGL.glTexGenfv(8193, 9474, new float[] { 0.0F, f_6_, 0.0F, (float) -i_5_ * f_6_ }, 0);
		OpenGL.glEnable(3168);
		OpenGL.glEnable(3169);
		OpenGL.glColor4ub((byte) (i_3_ >> 16), (byte) (i_3_ >> 8), (byte) i_3_, (byte) (i_3_ >> 24));
		OpenGL.glTranslatef((float) i, (float) i_2_, 0.0F);
		aClass341_3889.method3625((byte) -108, c);
		OpenGL.glLoadIdentity();
		OpenGL.glDisable(3168);
		OpenGL.glDisable(3169);
		aHa_Sub3_3886.method1283(0, 5890, 768, (byte) -115);
		aHa_Sub3_3886.method1306(8448, 8448, -22394);
		aHa_Sub3_3886.method1316(null, (byte) -111);
		aHa_Sub3_3886.method1330(122, 0);
		if (aBoolean3888 || bool)
			aHa_Sub3_3886.method1283(0, 5890, 768, (byte) -110);
	}

	Class55_Sub5(ha_Sub3 var_ha_Sub3, Class92 class92, Class186[] class186s, boolean bool) {
		super(var_ha_Sub3, class92);
		aHa_Sub3_3886 = var_ha_Sub3;
		int i = 0;
		for (int i_7_ = 0; i_7_ < 256; i_7_++) {
			Class186 class186 = class186s[i_7_];
			if (class186.anInt1904 > i)
				i = class186.anInt1904;
			if (class186.anInt1899 > i)
				i = class186.anInt1899;
		}
		int i_8_ = i * 16;
		if (bool) {
			byte[] is = new byte[i_8_ * i_8_];
			for (int i_9_ = 0; i_9_ < 256; i_9_++) {
				Class186 class186 = class186s[i_9_];
				int i_10_ = class186.anInt1904;
				int i_11_ = class186.anInt1899;
				int i_12_ = i_9_ % 16 * i;
				int i_13_ = i_9_ / 16 * i;
				int i_14_ = i_13_ * i_8_ + i_12_;
				int i_15_ = 0;
				if (class186.aByteArray1905 == null) {
					byte[] is_16_ = class186.aByteArray1901;
					for (int i_17_ = 0; i_17_ < i_10_; i_17_++) {
						for (int i_18_ = 0; i_18_ < i_11_; i_18_++)
							is[i_14_++] = (byte) (is_16_[i_15_++] == 0 ? 0 : -1);
						i_14_ += i_8_ - i_11_;
					}
				} else {
					byte[] is_19_ = class186.aByteArray1905;
					for (int i_20_ = 0; i_20_ < i_10_; i_20_++) {
						for (int i_21_ = 0; i_21_ < i_11_; i_21_++)
							is[i_14_++] = is_19_[i_15_++];
						i_14_ += i_8_ - i_11_;
					}
				}
			}
			aClass69_Sub1_Sub1_3887 = Class338_Sub3_Sub5.method3583(is, var_ha_Sub3, i_8_, 6406, i_8_, -111, false,
					6406);
			aBoolean3888 = true;
		} else {
			int[] is = new int[i_8_ * i_8_];
			for (int i_22_ = 0; i_22_ < 256; i_22_++) {
				Class186 class186 = class186s[i_22_];
				int[] is_23_ = class186.anIntArray1900;
				byte[] is_24_ = class186.aByteArray1905;
				byte[] is_25_ = class186.aByteArray1901;
				int i_26_ = class186.anInt1904;
				int i_27_ = class186.anInt1899;
				int i_28_ = i_22_ % 16 * i;
				int i_29_ = i_22_ / 16 * i;
				int i_30_ = i_29_ * i_8_ + i_28_;
				int i_31_ = 0;
				if (is_24_ != null) {
					for (int i_32_ = 0; i_32_ < i_26_; i_32_++) {
						for (int i_33_ = 0; i_33_ < i_27_; i_33_++) {
							is[i_30_++] = (is_24_[i_31_] << 24 | is_23_[is_25_[i_31_] & 0xff]);
							i_31_++;
						}
						i_30_ += i_8_ - i_27_;
					}
				} else {
					for (int i_34_ = 0; i_34_ < i_26_; i_34_++) {
						for (int i_35_ = 0; i_35_ < i_27_; i_35_++) {
							int i_36_;
							if ((i_36_ = is_25_[i_31_++]) != 0)
								is[i_30_++] = is_23_[i_36_ & 0xff] | ~0xffffff;
							else
								i_30_++;
						}
						i_30_ += i_8_ - i_27_;
					}
				}
			}
			aClass69_Sub1_Sub1_3887 = Class187.method1885(is, false, 0, var_ha_Sub3, 0, (byte) -27, i_8_, i_8_);
			aBoolean3888 = false;
		}
		aClass69_Sub1_Sub1_3887.method723(88, false);
		aClass341_3889 = new Class341(var_ha_Sub3, 256);
		float f = (aClass69_Sub1_Sub1_3887.aFloat6687 / (float) aClass69_Sub1_Sub1_3887.anInt6683);
		float f_37_ = (aClass69_Sub1_Sub1_3887.aFloat6684 / (float) aClass69_Sub1_Sub1_3887.anInt6688);
		for (int i_38_ = 0; i_38_ < 256; i_38_++) {
			Class186 class186 = class186s[i_38_];
			int i_39_ = class186.anInt1904;
			int i_40_ = class186.anInt1899;
			int i_41_ = class186.anInt1906;
			int i_42_ = class186.anInt1903;
			float f_43_ = (float) (i_38_ % 16 * i);
			float f_44_ = (float) (i_38_ / 16 * i);
			float f_45_ = f_43_ * f;
			float f_46_ = f_44_ * f_37_;
			float f_47_ = (f_43_ + (float) i_40_) * f;
			float f_48_ = (f_44_ + (float) i_39_) * f_37_;
			aClass341_3889.method3627((byte) -103, i_38_);
			OpenGL.glBegin(7);
			OpenGL.glTexCoord2f(f_45_, aClass69_Sub1_Sub1_3887.aFloat6684 - f_46_);
			OpenGL.glVertex2i(i_42_, i_41_);
			OpenGL.glTexCoord2f(f_45_, aClass69_Sub1_Sub1_3887.aFloat6684 - f_48_);
			OpenGL.glVertex2i(i_42_, i_41_ + i_39_);
			OpenGL.glTexCoord2f(f_47_, aClass69_Sub1_Sub1_3887.aFloat6684 - f_48_);
			OpenGL.glVertex2i(i_42_ + i_40_, i_41_ + i_39_);
			OpenGL.glTexCoord2f(f_47_, aClass69_Sub1_Sub1_3887.aFloat6684 - f_46_);
			OpenGL.glVertex2i(i_42_ + i_40_, i_41_);
			OpenGL.glEnd();
			aClass341_3889.method3622(-120);
		}
	}
}
