package net.zaros.client;
/* Class296_Sub51_Sub8 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Random;

final class Class296_Sub51_Sub8 extends TextureOperation {
	private int anInt6372;
	private int anInt6373;
	private int anInt6374;
	private int[][] anIntArrayArray6375;
	private int anInt6376;
	private int anInt6377 = 1024;
	private int[] anIntArray6378;
	private int anInt6379;
	private int anInt6380;
	private int anInt6381;
	private int anInt6382;
	private int anInt6383;
	private int[][] anIntArrayArray6384;
	static Mesh[] aClass132Array6385 = new Mesh[4];
	private int anInt6386;

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		if (i > -84)
			method3076((byte) 45);
		int i_1_ = i_0_;
		while_101_ : do {
			while_100_ : do {
				while_99_ : do {
					while_98_ : do {
						while_97_ : do {
							while_96_ : do {
								do {
									if (i_1_ != 0) {
										if (i_1_ != 1) {
											if (i_1_ != 2) {
												if (i_1_ != 3) {
													if (i_1_ != 4) {
														if (i_1_ != 5) {
															if (i_1_ != 6) {
																if (i_1_ != 7)
																	break while_101_;
															} else
																break while_99_;
															break while_100_;
														}
													} else
														break while_97_;
													break while_98_;
												}
											} else
												break;
											break while_96_;
										}
									} else {
										anInt6380 = class296_sub17.g1();
										return;
									}
									anInt6382 = class296_sub17.g1();
									return;
								} while (false);
								anInt6381 = class296_sub17.g2();
								return;
							} while (false);
							anInt6374 = class296_sub17.g2();
							return;
						} while (false);
						anInt6386 = class296_sub17.g2();
						return;
					} while (false);
					anInt6373 = class296_sub17.g2();
					return;
				} while (false);
				anInt6372 = class296_sub17.g2();
				return;
			} while (false);
			anInt6377 = class296_sub17.g2();
			break;
		} while (false);
	}

	final int[] get_monochrome_output(int i, int i_2_) {
		int[] is = aClass318_5035.method3335(i_2_, (byte) 28);
		if (i != 0)
			get_monochrome_output(-123, -78);
		if (aClass318_5035.aBoolean2819) {
			int i_3_ = 0;
			int i_4_;
			for (i_4_ = Class294.anIntArray2686[i_2_] + anInt6373; i_4_ < 0; i_4_ += 4096) {
				/* empty */
			}
			for (/**/; i_4_ > 4096; i_4_ -= 4096) {
				/* empty */
			}
			for (/**/; anInt6382 > i_3_; i_3_++) {
				if (i_4_ < anIntArray6378[i_3_])
					break;
			}
			int i_5_ = i_3_ - 1;
			boolean bool = (i_3_ & 0x1) == 0;
			int i_6_ = anIntArray6378[i_3_];
			int i_7_ = anIntArray6378[i_3_ - 1];
			if (i_7_ + anInt6379 < i_4_ && i_6_ - anInt6379 > i_4_) {
				for (int i_8_ = 0; Class41_Sub10.anInt3769 > i_8_; i_8_++) {
					int i_9_ = 0;
					int i_10_ = !bool ? -anInt6386 : anInt6386;
					int i_11_;
					for (i_11_ = ((anInt6376 * i_10_ >> 12) + Class33.anIntArray334[i_8_]); i_11_ < 0; i_11_ += 4096) {
						/* empty */
					}
					for (/**/; i_11_ > 4096; i_11_ -= 4096) {
						/* empty */
					}
					for (/**/; i_9_ < anInt6380; i_9_++) {
						if (i_11_ < anIntArrayArray6375[i_5_][i_9_])
							break;
					}
					int i_12_ = i_9_ - 1;
					int i_13_ = anIntArrayArray6375[i_5_][i_12_];
					int i_14_ = anIntArrayArray6375[i_5_][i_9_];
					if (i_13_ + anInt6379 >= i_11_ || i_11_ >= -anInt6379 + i_14_)
						is[i_8_] = 0;
					else
						is[i_8_] = anIntArrayArray6384[i_5_][i_12_];
				}
			} else
				Class291.method2407(is, 0, Class41_Sub10.anInt3769, 0);
		}
		return is;
	}

	private final void method3096(byte i) {
		if (i > -106)
			method3097(true);
		Random random = new Random((long) anInt6382);
		anInt6379 = anInt6372 / 2;
		anInt6376 = 4096 / anInt6380;
		anInt6383 = 4096 / anInt6382;
		int i_15_ = anInt6376 / 2;
		anIntArrayArray6384 = new int[anInt6382][anInt6380];
		int i_16_ = anInt6383 / 2;
		anIntArray6378 = new int[anInt6382 + 1];
		anIntArrayArray6375 = new int[anInt6382][anInt6380 + 1];
		anIntArray6378[0] = 0;
		for (int i_17_ = 0; i_17_ < anInt6382; i_17_++) {
			if (i_17_ > 0) {
				int i_18_ = anInt6383;
				int i_19_ = (((s_Sub3.method3373(4096, random, 6445) - 2048) * anInt6374) >> 12);
				i_18_ += i_19_ * i_16_ >> 12;
				anIntArray6378[i_17_] = i_18_ + anIntArray6378[i_17_ - 1];
			}
			anIntArrayArray6375[i_17_][0] = 0;
			for (int i_20_ = 0; i_20_ < anInt6380; i_20_++) {
				if (i_20_ > 0) {
					int i_21_ = anInt6376;
					int i_22_ = ((s_Sub3.method3373(4096, random, 6445) - 2048) * anInt6381 >> 12);
					i_21_ += i_22_ * i_15_ >> 12;
					anIntArrayArray6375[i_17_][i_20_] = anIntArrayArray6375[i_17_][i_20_ - 1] + i_21_;
				}
				anIntArrayArray6384[i_17_][i_20_] = (anInt6377 <= 0 ? 4096 : -s_Sub3.method3373(anInt6377, random, 6445) + 4096);
			}
			anIntArrayArray6375[i_17_][anInt6380] = 4096;
		}
		anIntArray6378[anInt6382] = 4096;
	}

	public static void method3097(boolean bool) {
		aClass132Array6385 = null;
		if (bool != true)
			aClass132Array6385 = null;
	}

	final void method3076(byte i) {
		int i_23_ = -95 % ((i + 58) / 40);
		method3096((byte) -122);
	}

	static final void method3098(int i, byte i_24_, Js5 class138, Js5 class138_25_) {
		Class41_Sub26.aClass138_3808 = class138;
		Class127.aClass138_1302 = class138_25_;
		if (i_24_ != -65)
			aClass132Array6385 = null;
	}

	public Class296_Sub51_Sub8() {
		super(0, true);
		anInt6374 = 204;
		anInt6373 = 0;
		anInt6380 = 4;
		anInt6381 = 409;
		anInt6386 = 1024;
		anInt6372 = 81;
		anInt6382 = 8;
	}
}
