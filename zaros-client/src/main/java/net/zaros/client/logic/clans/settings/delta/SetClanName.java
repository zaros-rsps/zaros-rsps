package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class SetClanName extends ClanSettingsAction {

	private String name;

	@Override
	public void decode(Packet buffer) {
		name = buffer.gstr();
		buffer.g4();
	}

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.name = name;
	}
}
