package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

public class UpdateBaseSettings extends ClanSettingsAction {

	private boolean allow_guests;
	private byte rank_unknown;
	private byte rank_talk;
	private byte rank_kick;
	private byte rank_lootshare;

	@Override
	public void decode(Packet buffer) {
		allow_guests = buffer.g1() == 1;
		rank_talk = buffer.g1b();
		rank_kick = buffer.g1b();
		rank_lootshare = buffer.g1b();
		rank_unknown = buffer.g1b();
	}

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.allow_guests = allow_guests;
		settings.rank_talk = rank_talk;
		settings.rank_kick = rank_kick;
		settings.rank_lootshare = rank_lootshare;
		settings.rank_unknown = rank_unknown;
	}
}
