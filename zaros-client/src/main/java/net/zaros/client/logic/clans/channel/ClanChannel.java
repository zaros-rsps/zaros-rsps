package net.zaros.client.logic.clans.channel;

import net.zaros.client.ArrayTools;
import net.zaros.client.Node;
import net.zaros.client.Packet;

/**
 * @author Walied K. Yassen
 */
public class ClanChannel extends Node {

	public String name;
	public long updateNum;
	private boolean use_memberhashes;
	private boolean use_displaynames;
	public byte rank_kick;
	public byte rank_talk;
	public int members_count;
	public ClanChannelMember[] members;
	private int[] sorted_members;

	public ClanChannel(Packet buffer) {
		name = null;
		use_displaynames = true;
		decode(buffer);
	}

	private void decode(Packet buffer) {
		int flag = buffer.g1();
		if ((flag & 0x1) != 0) {
			use_memberhashes = true;
		}
		if ((flag & 0x2) != 0) {
			use_displaynames = true;
		}
		uid = buffer.g8();
		updateNum = buffer.g8();
		name = buffer.gstr();
		buffer.g1();
		rank_kick = buffer.g1b();
		rank_talk = buffer.g1b();
		members_count = buffer.g2();
		if (members_count > 0) {
			members = new ClanChannelMember[members_count];
			for (int slot = 0; slot < members_count; slot++) {
				ClanChannelMember member = new ClanChannelMember();
				if (use_memberhashes) {
					buffer.g8();
				}
				if (use_displaynames) {
					member.name = buffer.gstr();
				}
				member.rank = buffer.g1b();
				member.world = buffer.g2();
				members[slot] = member;
			}
		}
	}

	public void addMember(ClanChannelMember member) {
		if (members == null || members_count >= members.length) {
			extendMembersList(members_count + 5);
		}
		members[members_count++] = member;
		sorted_members = null;
	}

	public void deleteMember(int i_4_) {
		members_count--;
		if (members_count == 0) {
			members = null;
		} else {
			ArrayTools.removeElement(members, i_4_ + 1, members, i_4_, -i_4_ + members_count);
		}
		sorted_members = null;
	}

	private void extendMembersList(int count) {
		if (members != null) {
			ArrayTools.removeElement(members, 0, members = new ClanChannelMember[count], 0, members_count);
		} else {
			members = new ClanChannelMember[count];
		}
	}

	public int[] sort() {
		if (sorted_members == null) {
			sorted_members = new int[members_count];
			String[] strings = new String[members_count];
			for (int i_0_ = 0; i_0_ < members_count; i_0_++) {
				strings[i_0_] = members[i_0_].name;
				sorted_members[i_0_] = i_0_;
			}
			ArrayTools.quicksort(strings, sorted_members);
		}
		return sorted_members;
	}

	public int getMemberSlot(String display_name) {
		for (int slot = 0; slot < members_count; slot++) {
			if (members[slot].name.equalsIgnoreCase(display_name)) {
				return slot;
			}
		}
		return -1;
	}

}
