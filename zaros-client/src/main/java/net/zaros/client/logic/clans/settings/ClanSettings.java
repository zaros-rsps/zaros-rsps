package net.zaros.client.logic.clans.settings;

import net.zaros.client.ArrayTools;
import net.zaros.client.Class8;
import net.zaros.client.HashTable;
import net.zaros.client.IntegerNode;
import net.zaros.client.LongNode;
import net.zaros.client.Node;
import net.zaros.client.Packet;
import net.zaros.client.StringNode;

/**
 * @author Walied K. Yassen
 */
public class ClanSettings {

	public String name;
	private int creationHour;
	public long owner;
	public int updateNum;
	public int replacement_owner_slot;
	public int owner_slot;
	private boolean use_userhashes;
	private boolean use_displaynames;
	public boolean allow_guests;
	public byte rank_talk;
	public byte rank_kick;
	public byte rank_lootshare;
	public byte rank_unknown;
	public int members_count;
	public byte[] members_rank;
	public String[] members_displayname;
	private long[] members_userhash;
	public int[] members_joinday;
	private int[] members_extrainfo;
	public int banneds_count;
	public String[] banneds_displayname;
	private long[] banneds_userhash;
	private int[] affined_slots;
	private HashTable settings_vars;

	public ClanSettings(Packet buffer) {
		owner_slot = -1;
		replacement_owner_slot = -1;
		decode(buffer);
	}

	private void decode(Packet buffer) {
		int version = buffer.g1();
		if (version < 1 || version > 5) {
			throw new RuntimeException("Unsupported ClanSettings version: " + version);
		}
		int flags = buffer.g1();
		if ((flags & 0x1) != 0) {
			use_userhashes = true;
		}
		if ((flags & 0x2) != 0) {
			use_displaynames = true;
		}
		if (!use_displaynames) {
			banneds_displayname = null;
			members_displayname = null;
		}
		if (!use_userhashes) {
			members_userhash = null;
			banneds_userhash = null;
		}
		updateNum = buffer.g4();
		creationHour = buffer.g4();
		if (version <= 3 && creationHour != 0) {
			creationHour += 16912800;
		}
		members_count = buffer.g2();
		banneds_count = buffer.g1();
		name = buffer.gstr();
		if (version >= 4) {
			buffer.g4();
		}
		allow_guests = buffer.g1() == 1;
		rank_talk = buffer.g1b();
		rank_kick = buffer.g1b();
		rank_lootshare = buffer.g1b();
		rank_unknown = buffer.g1b();
		if (members_count > 0) {
			if (use_userhashes && (members_userhash == null || members_userhash.length < members_count)) {
				members_userhash = new long[members_count];
			}
			if (use_displaynames && (members_displayname == null || members_count > members_displayname.length)) {
				members_displayname = new String[members_count];
			}
			if (members_rank == null || members_rank.length < members_count) {
				members_rank = new byte[members_count];
			}
			if (members_extrainfo == null || members_count > members_extrainfo.length) {
				members_extrainfo = new int[members_count];
			}
			if (members_joinday == null || members_joinday.length < members_count) {
				members_joinday = new int[members_count];
			}
			for (int slot = 0; slot < members_count; slot++) {
				if (use_userhashes) {
					members_userhash[slot] = buffer.g8();
				}
				if (use_displaynames) {
					members_displayname[slot] = buffer.gstrnull();
				}
				members_rank[slot] = buffer.g1b();
				if (version >= 2) {
					members_extrainfo[slot] = buffer.g4();
				}
				if (version < 5) {
					members_joinday[slot] = 0;
				} else {
					members_joinday[slot] = buffer.g2();
				}
			}
			updateRanks();
		}
		if (banneds_count > 0) {
			if (use_userhashes && (banneds_userhash == null || banneds_count > banneds_userhash.length)) {
				banneds_userhash = new long[banneds_count];
			}
			if (use_displaynames && (banneds_displayname == null || banneds_count > banneds_displayname.length)) {
				banneds_displayname = new String[banneds_count];
			}
			for (int slot = 0; banneds_count > slot; slot++) {
				if (use_userhashes) {
					banneds_userhash[slot] = buffer.g8();
				}
				if (use_displaynames) {
					banneds_displayname[slot] = buffer.gstrnull();
				}
			}
		}
		if (version >= 3) {
			int vars_count = buffer.g2();
			if (vars_count > 0) {
				settings_vars = new HashTable(vars_count < 16 ? Class8.get_next_high_pow2(vars_count) : 16);
				while (vars_count-- > 0) {
					int packed = buffer.g4();
					int varid = packed & 0x3fffffff;
					int type = packed >>> 30;
					if (type == 0) {
						int value = buffer.g4();
						settings_vars.put(varid, new IntegerNode(value));
					} else if (type == 1) {
						long value = buffer.g8();
						settings_vars.put(varid, new LongNode(value));
					} else if (type == 2) {
						String value = buffer.gstr();
						settings_vars.put(varid, new StringNode(value));
					}
				}
			}
		}
	}

	public void addMember(int extrainfo, long user_hash, String display_name, int joinday) {
		if (display_name != null && display_name.length() == 0) {
			display_name = null;
		}
		if (!use_userhashes != user_hash <= 0L) {
			throw new RuntimeException("Invalid UserHash arg to this method - useUserHashes:" + use_userhashes + " but userhash:" + user_hash);
		}
		if (display_name != null == !use_displaynames) {
			throw new RuntimeException("Invalid DisplayName arg to this method - useDisplayNames:" + use_displaynames + " but displayname:" + display_name);
		}
		if (user_hash > 0L && (members_userhash == null || members_count >= members_userhash.length) || display_name != null && (members_displayname == null || members_displayname.length <= members_count)) {
			extendMembersList(members_count + 5);
		}
		if (members_userhash != null) {
			members_userhash[members_count] = user_hash;
		}
		if (members_displayname != null) {
			members_displayname[members_count] = display_name;
		}
		if (owner_slot == -1) {
			owner_slot = members_count;
			members_rank[members_count] = (byte) 126;
		} else {
			members_rank[members_count] = (byte) 0;
		}
		members_extrainfo[members_count] = extrainfo;
		members_joinday[members_count] = joinday;
		affined_slots = null;
		members_count++;
	}

	public void deleteMember(int member) {
		if (member < 0 || members_count <= member) {
			throw new RuntimeException("Invalid pos in doDeleteMember - pos:" + member + " memberCount:" + members_count);
		}
		affined_slots = null;
		members_count--;
		if (members_count == 0) {
			replacement_owner_slot = -1;
			owner_slot = -1;
			members_userhash = null;
			members_joinday = null;
			members_extrainfo = null;
			members_displayname = null;
			members_rank = null;
		} else {
			ArrayTools.removeElement(members_rank, member + 1, members_rank, member, -member + members_count);
			ArrayTools.removeElements(members_extrainfo, member + 1, members_extrainfo, member, members_count - member);
			ArrayTools.removeElements(members_joinday, member + 1, members_joinday, member, -member + members_count);
			if (members_userhash != null) {
				ArrayTools.removeElement(members_userhash, member + 1, members_userhash, member, -member + members_count);
			}
			if (members_displayname != null) {
				ArrayTools.removeElement(members_displayname, member + 1, members_displayname, member, members_count - member);
			}
			if (member == owner_slot || member == replacement_owner_slot) {
				updateRanks();
			}
		}
	}

	private void extendMembersList(int count) {
		if (use_userhashes) {
			if (members_userhash != null) {
				ArrayTools.removeElement(members_userhash, 0, members_userhash = new long[count], 0, members_count);
			} else {
				members_userhash = new long[count];
			}
		}
		if (use_displaynames) {
			if (members_displayname != null) {
				ArrayTools.removeElement(members_displayname, 0, members_displayname = new String[count], 0, members_count);
			} else {
				members_displayname = new String[count];
			}
		}
		if (members_rank == null) {
			members_rank = new byte[count];
		} else {
			ArrayTools.removeElement(members_rank, 0, members_rank = new byte[count], 0, members_count);
		}
		if (members_extrainfo != null) {
			ArrayTools.removeElements(members_extrainfo, 0, members_extrainfo = new int[count], 0, members_count);
		} else {
			members_extrainfo = new int[count];
		}
		if (members_joinday == null) {
			members_joinday = new int[count];
		} else {
			ArrayTools.removeElements(members_joinday, 0, members_joinday = new int[count], 0, members_count);
		}
	}

	public void addBanned(String display_name, long user_hash) {
		if (display_name != null && display_name.length() == 0) {
			display_name = null;
		}
		if (user_hash > 0 != use_userhashes) {
			throw new RuntimeException("Invalid UserHash arg to this method - useUserHashes:" + use_userhashes + " but userhash:" + user_hash);
		}
		if (display_name != null != use_displaynames) {
			throw new RuntimeException("Invalid DisplayName arg to this method - useDisplayNames:" + use_displaynames + " but displayname:" + display_name);
		}
		if (user_hash > 0L && (banneds_userhash == null || banneds_count >= banneds_userhash.length) || display_name != null && (banneds_displayname == null || banneds_displayname.length <= banneds_count)) {
			extendBannedList(banneds_count + 5);
		}
		if (banneds_userhash != null) {
			banneds_userhash[banneds_count] = user_hash;
		}
		if (banneds_displayname != null) {
			banneds_displayname[banneds_count] = display_name;
		}
		banneds_count++;
	}

	public void deleteBanned(int banned_id) {
		banneds_count--;
		if (banneds_count == 0) {
			banneds_displayname = null;
			banneds_userhash = null;
		} else {
			if (banneds_userhash != null) {
				ArrayTools.removeElement(banneds_userhash, banned_id + 1, banneds_userhash, banned_id, banneds_count - banned_id);
			}
			if (banneds_displayname != null) {
				ArrayTools.removeElement(banneds_displayname, banned_id + 1, banneds_displayname, banned_id, -banned_id + banneds_count);
			}
		}
	}

	private void extendBannedList(int count) {
		if (use_userhashes) {
			if (banneds_userhash == null) {
				banneds_userhash = new long[count];
			} else {
				ArrayTools.removeElement(banneds_userhash, 0, banneds_userhash = new long[count], 0, banneds_count);
			}
		}
		if (use_displaynames) {
			if (banneds_displayname != null) {
				ArrayTools.removeElement(banneds_displayname, 0, banneds_displayname = new String[count], 0, banneds_count);
			} else {
				banneds_displayname = new String[count];
			}
		}
	}

	public int setMemberRank(int member, byte rank) {
		if (rank == 126 || rank == 127) {
			return -1;
		}
		if (owner_slot == member && (replacement_owner_slot == -1 || members_rank[replacement_owner_slot] < 125)) {
			return -1;
		}
		if (members_rank[member] == rank) {
			return -1;
		}
		members_rank[member] = rank;
		updateRanks();
		return member;
	}

	private void updateRanks() {
		if (members_count == 0) {
			owner_slot = -1;
			replacement_owner_slot = -1;
		} else {
			replacement_owner_slot = -1;
			owner_slot = -1;
			int highest_slot = 0;
			byte highest_rank = members_rank[0];
			for (int slot = 1; members_count > slot; slot++) {
				if (members_rank[slot] > highest_rank) {
					if (highest_rank == 125) {
						replacement_owner_slot = highest_slot;
					}
					highest_rank = members_rank[slot];
					highest_slot = slot;
				} else {
					if (replacement_owner_slot == -1 && members_rank[slot] == 125) {
						replacement_owner_slot = slot;
					}
				}
			}
			owner_slot = highest_slot;
			if (owner_slot != -1) {
				members_rank[owner_slot] = (byte) 126;
			}
		}
	}

	public int[] sortAffined() {
		if (affined_slots == null) {
			String[] names = new String[members_count];
			affined_slots = new int[members_count];
			for (int index = 0; index < members_count; index++) {
				names[index] = members_displayname[index];
				affined_slots[index] = index;
			}
			ArrayTools.quicksort(names, affined_slots);
		}
		return affined_slots;
	}

	public Integer getVarInt(int var) {
		if (settings_vars == null) {
			return null;
		}
		Node value = settings_vars.get(var);
		if (value == null || !(value instanceof IntegerNode)) {
			return null;
		}
		return new Integer(((IntegerNode) value).value);
	}

	public boolean setVarInt(int var, int value) {
		if (settings_vars == null) {
			settings_vars = new HashTable(4);
		} else {
			Node previous_node = settings_vars.get(var);
			if (previous_node != null) {
				if (previous_node instanceof IntegerNode) {
					IntegerNode previous_Value = (IntegerNode) previous_node;
					if (value == previous_Value.value) {
						return false;
					}
					previous_Value.value = value;
					return true;
				}
				previous_node.unlink();
			}
		}
		settings_vars.put(var, new IntegerNode(value));
		return true;
	}

	public Integer getVarBit(int var, int start_bit, int end_bit) {
		if (settings_vars == null) {
			return null;
		}
		Node value = settings_vars.get(var);
		if (value == null || !(value instanceof IntegerNode)) {
			return null;
		}
		int bit_mask = end_bit != 31 ? (1 << end_bit + 1) - 1 : -1;
		return new Integer((((IntegerNode) value).value & bit_mask) >>> start_bit);
	}

	public boolean setVarBit(int var, int value, int start_bit, int end_bit) {
		int range_start = (1 << start_bit) - 1;
		int range_end = end_bit != 31 ? (1 << end_bit + 1) - 1 : -1;
		int bit_mask = range_end ^ range_start;
		value <<= start_bit;
		value &= bit_mask;
		if (settings_vars == null) {
			settings_vars = new HashTable(4);
		} else {
			Node previous_node = settings_vars.get(var);
			if (previous_node != null) {
				if (previous_node instanceof IntegerNode) {
					IntegerNode previous_value = (IntegerNode) previous_node;
					if (value == (bit_mask & previous_value.value)) {
						return false;
					}
					previous_value.value &= bit_mask ^ 0xffffffff;
					previous_value.value |= value;
					return true;
				}
				previous_node.unlink();
			}
		}
		settings_vars.put(var, new IntegerNode(value));
		return true;
	}

	public String getVarString(int var) {
		if (settings_vars == null) {
			return null;
		}
		Node value = settings_vars.get(var);
		if (value == null || !(value instanceof StringNode)) {
			return null;
		}
		return ((StringNode) value).value;
	}

	public boolean setVarString(int var, String value) {
		if (value == null) {
			value = "";
		} else if (value.length() > 80) {
			value = value.substring(0, 80);
		}
		if (settings_vars == null) {
			settings_vars = new HashTable(4);
		} else {
			Node previous_node = settings_vars.get(var);
			if (previous_node != null) {
				if (previous_node instanceof StringNode) {
					StringNode previous_value = (StringNode) previous_node;
					if (previous_value.value.equals(value)) {
						return false;
					}
					previous_value.value = value;
					return true;
				}
				previous_node.unlink();
			}
		}
		settings_vars.put(var, new StringNode(value));
		return true;
	}

	public Long getVarLong(int var) {
		if (settings_vars == null) {
			return null;
		}
		Node value = settings_vars.get(var);
		if (value == null || !(value instanceof LongNode)) {
			return null;
		}
		return new Long(((LongNode) value).value);
	}

	public boolean setVarLong(int var, long value) {
		if (settings_vars == null) {
			settings_vars = new HashTable(4);
		} else {
			Node previous_node = settings_vars.get(var);
			if (previous_node != null) {
				if (previous_node instanceof LongNode) {
					LongNode previous_value = (LongNode) previous_node;
					if (previous_value.value == value) {
						return false;
					}
					previous_value.value = value;
					return true;
				}
				previous_node.unlink();
			}
		}
		settings_vars.put(var, new LongNode(value));
		return true;
	}

	public int getMemberExtraInfo(int member, int start_bit, int end_bit) {
		int bit_mask = end_bit != 31 ? (1 << end_bit + 1) - 1 : -1;
		return (bit_mask & members_extrainfo[member]) >>> start_bit;
	}

	public int setMemberExtraInfo(int member, int value, int start_bit, int end_bit) {
		int range_start = (1 << start_bit) - 1;
		int range_end = end_bit != 31 ? (1 << end_bit + 1) - 1 : -1;
		int bit_mask = range_end ^ range_start;
		value <<= start_bit;
		value &= bit_mask;
		int previous_value = members_extrainfo[member];
		if ((previous_value & bit_mask) == value) {
			return -1;
		}
		previous_value &= bit_mask ^ 0xffffffff;
		members_extrainfo[member] = previous_value | value;
		return member;
	}

	public int getAffinedSlot(String displayname) {
		if (displayname == null || displayname.length() == 0) {
			return -1;
		}
		for (int index = 0; index < members_count; index++) {
			if (members_displayname[index].equals(displayname)) {
				return index;
			}
		}
		return -1;
	}
}
