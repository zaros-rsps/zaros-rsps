package net.zaros.client.logic.clans.channel.delta;

import net.zaros.client.Class296_Sub51_Sub32;
import net.zaros.client.Packet;
import net.zaros.client.logic.clans.channel.ClanChannel;
import net.zaros.client.logic.clans.channel.ClanChannelAction;
import net.zaros.client.logic.clans.channel.ClanChannelMember;

/**
 * @author Walied K. Yassen
 */
public class AddMember extends ClanChannelAction {

	private long member_hash = -1L;
	private int member_world;
	private String member_name = null;
	private byte member_rank;

	@Override
	public void applyToClanChannel(ClanChannel channel) {
		ClanChannelMember member = new ClanChannelMember();
		member.name = member_name;
		member.world = member_world;
		member.rank = member_rank;
		channel.addMember(member);
	}

	@Override
	public void decode(Packet buffer) {
		if (buffer.g1() != 255) {
			buffer.pos--;
			member_hash = buffer.g8();
		}
		member_name = buffer.gstrnull();
		member_world = buffer.g2();
		member_rank = buffer.g1b();
		buffer.g8();
		if (Class296_Sub51_Sub32.aBoolean6515) {
			System.out.println("memberhash:" + member_hash + " membername:" + member_name);
		}
	}
}
