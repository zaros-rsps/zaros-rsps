package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class SetExtraSettingVarbit extends ClanSettingsAction {

	private int var;
	private int value;
	private int start_bit;
	private int end_bit;

	@Override
	public void decode(Packet buffer) {
		var = buffer.g4();
		value = buffer.g4();
		start_bit = buffer.g1();
		end_bit = buffer.g1();
	}

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.setVarBit(var, value, start_bit, end_bit);
	}
}
