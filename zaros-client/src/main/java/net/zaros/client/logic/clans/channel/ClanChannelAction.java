package net.zaros.client.logic.clans.channel;

import net.zaros.client.Node;
import net.zaros.client.Packet;

public abstract class ClanChannelAction extends Node {

	public abstract void decode(Packet buffer);

	public abstract void applyToClanChannel(ClanChannel channel);
}
