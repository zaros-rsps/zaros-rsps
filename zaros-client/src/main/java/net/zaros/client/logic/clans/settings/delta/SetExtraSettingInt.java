package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class SetExtraSettingInt extends ClanSettingsAction {

	private int var;
	private int value;

	@Override
	public void decode(Packet buffer) {
		var = buffer.g4();
		value = buffer.g4();
	}

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.setVarInt(var, value);
	}
}
