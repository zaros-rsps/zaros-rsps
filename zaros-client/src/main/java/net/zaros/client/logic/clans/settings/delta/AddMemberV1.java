package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class AddMemberV1 extends ClanSettingsAction {

	private long member_hash = -1L;
	private String member_name = null;

	@Override
	public void decode(Packet buffer) {
		if (buffer.g1() != 255) {
			buffer.pos--;
			member_hash = buffer.g8();
		}
		member_name = buffer.gstrnull();
		if (Packet.debug_packets) {
			System.out.println("memberhash:" + member_hash + " membername:" + member_name);
		}
	}

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.addMember(0, member_hash, member_name, 0);
	}
}
