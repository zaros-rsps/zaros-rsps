package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class SetMemberExtraInfo extends ClanSettingsAction {

	private int member = -1;
	private int value;
	private int start_bit;
	private int end_bit;

	@Override
	public void decode(Packet buffer) {
		member = buffer.g2();
		value = buffer.g4();
		start_bit = buffer.g1();
		end_bit = buffer.g1();
	}

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.setMemberExtraInfo(member, value, start_bit, end_bit);
	}
}
