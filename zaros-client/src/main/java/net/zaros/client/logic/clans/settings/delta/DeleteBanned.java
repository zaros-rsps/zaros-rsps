package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class DeleteBanned extends ClanSettingsAction {

	private int banned_id = -1;

	@Override
	public final void decode(Packet buffer) {
		banned_id = buffer.g2();
	}

	@Override
	public final void applyToClanSettings(ClanSettings settings) {
		settings.deleteBanned(banned_id);
	}

}
