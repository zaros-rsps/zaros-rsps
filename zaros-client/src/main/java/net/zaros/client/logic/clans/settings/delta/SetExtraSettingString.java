package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class SetExtraSettingString extends ClanSettingsAction {

	private int var;
	private String value;

	@Override
	public void decode(Packet buffer) {
		var = buffer.g4();
		value = buffer.gstr();
	}

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.setVarString(var, value);
	}
}
