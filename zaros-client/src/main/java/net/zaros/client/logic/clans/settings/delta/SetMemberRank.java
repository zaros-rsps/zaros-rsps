package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class SetMemberRank extends ClanSettingsAction {

	private int member = -1;
	private byte rank;

	@Override
	public void decode(Packet buffer) {
		member = buffer.g2();
		rank = buffer.g1b();
	}

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.setMemberRank(member, rank);
	}
}
