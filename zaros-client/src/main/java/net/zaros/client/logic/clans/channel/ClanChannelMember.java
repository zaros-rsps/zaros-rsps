package net.zaros.client.logic.clans.channel;

/**
 * @author Walied K. Yassen
 */
public class ClanChannelMember {

	public String name;
	public int world;
	public byte rank;
}
