package net.zaros.client.logic.clans.channel.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.channel.ClanChannel;
import net.zaros.client.logic.clans.channel.ClanChannelAction;
import net.zaros.client.logic.clans.channel.ClanChannelMember;

/**
 * @author Walied K. Yassen
 */
public class UpdateMember extends ClanChannelAction {

	private int world;
	private byte rank;
	private int member = -1;
	private String name;

	@Override
	public void decode(Packet buffer) {
		member = buffer.g2();
		rank = buffer.g1b();
		world = buffer.g2();
		buffer.g8();
		name = buffer.gstr();
	}

	@Override
	public void applyToClanChannel(ClanChannel channel) {
		ClanChannelMember class329 = channel.members[member];
		class329.world = world;
		class329.rank = rank;
		class329.name = name;
	}

}
