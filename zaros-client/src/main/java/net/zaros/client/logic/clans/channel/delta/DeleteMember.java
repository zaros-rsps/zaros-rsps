package net.zaros.client.logic.clans.channel.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.channel.ClanChannel;
import net.zaros.client.logic.clans.channel.ClanChannelAction;

/**
 * @author Walied K. Yassen
 */
public class DeleteMember extends ClanChannelAction {

	private int member = -1;

	@Override
	public void decode(Packet buffer) {
		member = buffer.g2();
		buffer.g1();
		if (buffer.g1() != 255) {
			buffer.pos--;
			buffer.g8();
		}
	}

	@Override
	public void applyToClanChannel(ClanChannel channel) {
		channel.deleteMember(member);
	}
}
