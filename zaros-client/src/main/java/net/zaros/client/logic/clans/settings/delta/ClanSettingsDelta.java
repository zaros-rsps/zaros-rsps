package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Class296_Sub13;
import net.zaros.client.NodeDeque;
import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

public class ClanSettingsDelta {

	private final NodeDeque pending_deltas = new NodeDeque();
	private long owner;
	private int updateNum;

	public ClanSettingsDelta(Packet buffer) {
		updateNum = -1;
		decode(buffer);
	}

	private void decode(Packet buffer) {
		owner = buffer.g8();
		updateNum = buffer.g4();
		for (int type = buffer.g1(); type != 0; type = buffer.g1()) {
			if (Packet.debug_packets) {
				System.out.println("t:" + type);
			}
			ClanSettingsAction delta;
			if (type == 3) {
				delta = new AddBanned();
			} else if (type == 1) {
				delta = new AddMemberV1();
			} else if (type == 13) {
				delta = new AddMemberV2();
			} else if (type == 4) {
				delta = new UpdateBaseSettings();
			} else if (type == 6) {
				delta = new DeleteBanned();
			} else if (type == 5) {
				delta = new DeleteMember();
			} else if (type == 2) {
				delta = new SetMemberRank();
			} else if (type == 7) {
				delta = new SetMemberExtraInfo();
			} else if (type == 8) {
				delta = new SetExtraSettingInt();
			} else if (type == 9) {
				delta = new SetExtraSettingLong();
			} else if (type == 10) {
				delta = new SetExtraSettingString();
			} else if (type == 11) {
				delta = new SetExtraSettingVarbit();
			} else if (type == 12) {
				delta = new SetClanName();
			} else {
				throw new RuntimeException("Unrecognised ClanSettingsDelta type in decode()");
			}
			delta.decode(buffer);
			pending_deltas.addLast((byte) 119, delta);
		}
	}

	public void applyToClanSettings(ClanSettings settings) {
		if (owner != settings.owner || updateNum != settings.updateNum) {
			throw new RuntimeException("ClanSettingsDelta.applyToClanSettings(): Credentials do not match! Settings.owner:" + Class296_Sub13.decodeBase37(settings.owner) + " updateNum:" + settings.updateNum + " delta.owner:" + Class296_Sub13.decodeBase37(owner) + " updateNum:" + updateNum);
		}
		for (ClanSettingsAction delta = (ClanSettingsAction) pending_deltas.removeFirst((byte) 114); delta != null; delta = (ClanSettingsAction) pending_deltas.removeNext(1001)) {
			delta.applyToClanSettings(settings);
		}
		settings.updateNum++;
	}
}
