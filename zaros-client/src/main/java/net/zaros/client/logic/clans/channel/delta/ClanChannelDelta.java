package net.zaros.client.logic.clans.channel.delta;

import net.zaros.client.NodeDeque;
import net.zaros.client.Packet;
import net.zaros.client.logic.clans.channel.ClanChannel;
import net.zaros.client.logic.clans.channel.ClanChannelAction;

/**
 * @author Walied K. Yassen
 */
public class ClanChannelDelta {

	private final NodeDeque pending_actions = new NodeDeque();
	private long updateNum = -1L;
	private long clanHash;

	public ClanChannelDelta(Packet buffer) {
		decode(buffer);
	}

	private void decode(Packet buffer) {
		clanHash = buffer.g8();
		updateNum = buffer.g8();
		for (int type = buffer.g1(); type != 0; type = buffer.g1()) {
			ClanChannelAction action;
			if (type == 1) {
				action = new AddMember();
			} else if (type == 2) {
				action = new UpdateMember();
			} else if (type == 3) {
				action = new DeleteMember();
			} else if (type == 4) {
				action = new UpdateBaseSettings();
			} else {
				throw new RuntimeException("Unrecognised ClanChannelDelta type in decode()");
			}
			action.decode(buffer);
			pending_actions.addLast((byte) 104, action);
		}
	}

	public void applyToClanChannel(ClanChannel channel) {
		if (clanHash != channel.uid || channel.updateNum != updateNum) {
			throw new RuntimeException("ClanChannelDelta.applyToClanChannel(): Credentials do not match! cc.clanHash:" + channel.uid + " updateNum:" + channel.updateNum + " delta.clanHash:" + clanHash + " updateNum:" + updateNum);
		}
		for (ClanChannelAction action = (ClanChannelAction) pending_actions.removeFirst((byte) 119); action != null; action = (ClanChannelAction) pending_actions.removeNext(1001)) {
			action.applyToClanChannel(channel);
		}
		channel.updateNum++;
	}

}
