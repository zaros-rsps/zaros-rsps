package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class SetExtraSettingLong extends ClanSettingsAction {

	private int var;
	private long value;

	@Override
	public void decode(Packet buffer) {
		var = buffer.g4();
		value = buffer.g8();
	}

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.setVarLong(var, value);
	}
}
