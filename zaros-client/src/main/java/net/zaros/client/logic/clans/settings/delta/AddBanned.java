package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class AddBanned extends ClanSettingsAction {

	private String aString5926 = null;
	private long aLong5931 = -1L;

	@Override
	public void decode(Packet buffer) {
		if (buffer.g1() != 255) {
			buffer.pos--;
			aLong5931 = buffer.g8();
		}
		aString5926 = buffer.gstrnull();
	}

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.addBanned(aString5926, aLong5931);
	}
}
