package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

/**
 * @author Walied K. Yassen
 */
public class DeleteMember extends ClanSettingsAction {

	private int member = -1;

	@Override
	public void applyToClanSettings(ClanSettings settings) {
		settings.deleteMember(member);
	}

	@Override
	public void decode(Packet buffer) {
		member = buffer.g2();
	}
}
