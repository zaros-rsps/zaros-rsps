package net.zaros.client.logic.clans.settings.delta;

import net.zaros.client.Node;
import net.zaros.client.Packet;
import net.zaros.client.logic.clans.settings.ClanSettings;

public abstract class ClanSettingsAction extends Node {

	public abstract void decode(Packet buffer);

	public abstract void applyToClanSettings(ClanSettings settings);
}
