package net.zaros.client.logic.clans.channel.delta;

import net.zaros.client.Packet;
import net.zaros.client.logic.clans.channel.ClanChannel;
import net.zaros.client.logic.clans.channel.ClanChannelAction;

/**
 * @author Walied K. Yassen
 */
public class UpdateBaseSettings extends ClanChannelAction {

	private String name;
	private byte rank_talk;
	private byte rank_kick;

	@Override
	public void decode(Packet buffer) {
		name = buffer.gstrnull();
		if (name != null) {
			buffer.g1();
			rank_talk = buffer.g1b();
			rank_kick = buffer.g1b();
		}
	}

	@Override
	public void applyToClanChannel(ClanChannel channel) {
		channel.name = name;
		if (name != null) {
			channel.rank_talk = rank_talk;
			channel.rank_kick = rank_kick;
		}
	}
}
