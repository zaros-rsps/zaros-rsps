package net.zaros.client;
import jaclib.memory.Buffer;
import jaclib.memory.Source;

final class Class105_Sub1 extends Class105 implements Interface15_Impl2 {
	static int anInt5689 = 0;
	private byte aByte5690;
	static Class81 aClass81_5691 = new Class81("", 13);

	public final boolean method49(int i) {
		if (i != 2968)
			method911((byte) 42);
		return super.method909(-10218, aHa_Sub1_Sub1_3657.aMapBuffer5842);
	}

	public final boolean method46(int i, int i_0_, int i_1_) {
		aByte5690 = (byte) i_1_;
		int i_2_ = 18 % ((i_0_ + 24) / 40);
		super.method32(i, -21709);
		return true;
	}

	public final boolean method48(boolean bool, Source source, int i, int i_3_) {
		aByte5690 = (byte) i;
		super.method905(i_3_, source, -1);
		if (bool)
			method911((byte) -68);
		return true;
	}

	public final Buffer method47(boolean bool, int i) {
		if (i != -8102)
			return null;
		return super.method907(aHa_Sub1_Sub1_3657.aMapBuffer5842, (byte) -16, bool);
	}

	final int method911(byte i) {
		if (i < 122)
			anInt5689 = -62;
		return aByte5690;
	}

	public final int method61(byte i) {
		if (i <= 115)
			aByte5690 = (byte) 11;
		return super.method61((byte) 119);
	}

	public final void method31(byte i) {
		super.method31(i);
	}

	public static void method912(int i) {
		aClass81_5691 = null;
		if (i > -80)
			anInt5689 = 71;
	}

	Class105_Sub1(ha_Sub1_Sub1 var_ha_Sub1_Sub1, boolean bool) {
		super(var_ha_Sub1_Sub1, 34962, bool);
	}
}
