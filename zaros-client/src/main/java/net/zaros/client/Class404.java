package net.zaros.client;
import jaggl.OpenGL;

final class Class404 {
	private Class338 aClass338_3381 = new Class338();
	static int[] anIntArray3382 = new int[2];
	static int[] anIntArray3383 = new int[4];
	private Class338 aClass338_3384;
	static int anInt3385;
	static boolean aBoolean3386 = false;

	final void method4158(Class338 class338, int i) {
		if (class338.aClass338_2972 != null)
			class338.method3438(false);
		class338.aClass338_2971 = aClass338_3381;
		class338.aClass338_2972 = aClass338_3381.aClass338_2972;
		class338.aClass338_2972.aClass338_2971 = class338;
		class338.aClass338_2971.aClass338_2972 = class338;
		if (i != 1)
			method4160((byte) -3);
	}

	final int method4159(byte i) {
		int i_0_ = 0;
		Class338 class338 = aClass338_3381.aClass338_2971;
		if (i > -15)
			return 9;
		for (/**/; class338 != aClass338_3381; class338 = class338.aClass338_2971)
			i_0_++;
		return i_0_;
	}

	final Class338 method4160(byte i) {
		Class338 class338 = aClass338_3381.aClass338_2971;
		if (aClass338_3381 == class338) {
			aClass338_3384 = null;
			return null;
		}
		int i_1_ = -71 % ((i - 42) / 41);
		aClass338_3384 = class338.aClass338_2971;
		return class338;
	}

	final void method4161(byte i) {
		for (;;) {
			Class338 class338 = aClass338_3381.aClass338_2971;
			if (aClass338_3381 == class338)
				break;
			class338.method3438(false);
		}
		aClass338_3384 = null;
		if (i <= 24)
			method4169((byte) 44);
	}

	final Class338 method4162(int i) {
		if (i != 255)
			aClass338_3384 = null;
		Class338 class338 = aClass338_3381.aClass338_2971;
		if (aClass338_3381 == class338)
			return null;
		class338.method3438(false);
		return class338;
	}

	final Class338 method4163(int i) {
		Class338 class338 = aClass338_3384;
		if (aClass338_3381 == class338) {
			aClass338_3384 = null;
			return null;
		}
		if (i != -24917)
			anIntArray3383 = null;
		aClass338_3384 = class338.aClass338_2971;
		return class338;
	}

	static final void method4164(int i, int i_2_, int i_3_, int i_4_, Class252 class252, int i_5_, Class357 class357, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_) {
		Class360_Sub5.anInt5327 = i_7_;
		Class18.anInt6160 = i_8_;
		Class69_Sub2.aClass252_5719 = class252;
		if (i_10_ != 1728789256)
			anInt3385 = -88;
		Class296_Sub39_Sub10.anInt6176 = i_5_;
		Class238.aClass186_3629 = null;
		Class41_Sub28.aClass357_3815 = class357;
		RuntimeException_Sub1.anInt3397 = i_2_;
		Class241_Sub1.anInt4578 = i_6_;
		Class368_Sub22.anInt5559 = i_3_;
		Class122_Sub3.anInt5666 = i_4_;
		Class41_Sub18.aClass186_3788 = null;
		Class288.aClass186_2654 = null;
		Class338_Sub3_Sub1_Sub2.anInt6759 = i_9_;
		StaticMethods.anInt5910 = i;
		Class67.method711((byte) -115);
		Class122_Sub2.aBoolean5662 = true;
	}

	static final void method4165(int i, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_, int[] is, int i_16_) {
		if (i > i_14_ && !Class30.method329(i_14_ + 2844, i))
			throw new IllegalArgumentException("");
		if (i_12_ > 0 && !Class30.method329(i_14_ + 2844, i_12_))
			throw new IllegalArgumentException("");
		if (i_13_ != 32993)
			throw new IllegalArgumentException("");
		int i_17_ = 0;
		int i_18_ = i >= i_12_ ? i_12_ : i;
		int i_19_ = i >> 1;
		int i_20_ = i_12_ >> 1;
		int[] is_21_ = is;
		int[] is_22_ = new int[i_19_ * i_20_];
		for (;;) {
			OpenGL.glTexImage2Di(i_15_, i_17_, i_16_, i, i_12_, 0, i_13_, i_11_, is_21_, 0);
			if (i_18_ <= 1)
				break;
			int i_23_ = 0;
			int i_24_ = 0;
			int i_25_ = i_24_ + i;
			for (int i_26_ = 0; i_20_ > i_26_; i_26_++) {
				for (int i_27_ = 0; i_27_ < i_19_; i_27_++) {
					int i_28_ = is_21_[i_24_++];
					int i_29_ = is_21_[i_25_++];
					int i_30_ = is_21_[i_24_++];
					int i_31_ = i_28_ >> 24 & 0xff;
					int i_32_ = i_28_ & 0xff;
					int i_33_ = is_21_[i_25_++];
					int i_34_ = (i_28_ & 0xffb067) >> 16;
					int i_35_ = i_28_ >> 8 & 0xff;
					i_31_ += i_30_ >> 24 & 0xff;
					i_34_ += i_30_ >> 16 & 0xff;
					i_32_ += i_30_ & 0xff;
					i_35_ += i_30_ >> 8 & 0xff;
					i_31_ += i_29_ >> 24 & 0xff;
					i_34_ += i_29_ >> 16 & 0xff;
					i_32_ += i_29_ & 0xff;
					i_35_ += i_29_ >> 8 & 0xff;
					i_35_ += (i_33_ & 0xffda) >> 8;
					i_31_ += i_33_ >> 24 & 0xff;
					i_32_ += i_33_ & 0xff;
					i_34_ += i_33_ >> 16 & 0xff;
					is_22_[i_23_++] = (Class48.bitOR((i_32_ & 1020) >> 2, (Class48.bitOR(Class48.bitOR(((1020 & i_31_) << 22), ((1020 & i_34_) << 14)), (65280 & i_35_ << 6)))));
				}
				i_24_ += i;
				i_25_ += i;
			}
			int[] is_36_ = is_22_;
			is_22_ = is_21_;
			i = i_19_;
			is_21_ = is_36_;
			i_12_ = i_20_;
			i_17_++;
			i_18_ >>= 1;
			i_20_ >>= 1;
			i_19_ >>= 1;
		}
	}

	final boolean method4166(byte i) {
		if (i != -90)
			method4167((byte) -10, -25, 112);
		if (aClass338_3381 != aClass338_3381.aClass338_2971)
			return false;
		return true;
	}

	static final boolean method4167(byte i, int i_37_, int i_38_) {
		if (i < 115)
			return false;
		return ((Class205_Sub2.method1982(544, i_37_, i_38_) | (i_38_ & 0x2000) != 0 | ParamTypeList.method1657((byte) -105, i_37_, i_38_)) & ReferenceWrapper.method2789(-1, i_37_, i_38_));
	}

	public static void method4168(boolean bool) {
		if (!bool) {
			anIntArray3382 = null;
			anIntArray3383 = null;
		}
	}

	final Class338 method4169(byte i) {
		if (i != -70)
			method4163(-70);
		Class338 class338 = aClass338_3381.aClass338_2972;
		if (class338 == aClass338_3381) {
			aClass338_3384 = null;
			return null;
		}
		aClass338_3384 = class338.aClass338_2972;
		return class338;
	}

	public Class404() {
		aClass338_3381.aClass338_2971 = aClass338_3381;
		aClass338_3381.aClass338_2972 = aClass338_3381;
	}
}
