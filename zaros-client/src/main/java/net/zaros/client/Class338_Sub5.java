package net.zaros.client;
/* Class338_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class338_Sub5 extends Class338 {
	public int anInt5221;
	public int anInt5222;
	public int anInt5223;
	public boolean aBoolean5224 = false;
	public int anInt5225;
	public int anInt5226;

	final boolean method3591(int i, int i_0_) {
		if (!aBoolean5224)
			return false;
		int i_1_ = anInt5222 - anInt5225;
		int i_2_ = anInt5221 - anInt5223;
		int i_3_ = i_1_ * i_1_ + i_2_ * i_2_;
		int i_4_ = i * i_1_ + i_0_ * i_2_ - (anInt5225 * i_1_ + anInt5223 * i_2_);
		if (i_4_ <= 0) {
			int i_5_ = anInt5225 - i;
			int i_6_ = anInt5223 - i_0_;
			if (i_5_ * i_5_ + i_6_ * i_6_ >= anInt5226 * anInt5226)
				return false;
			return true;
		}
		if (i_4_ > i_3_) {
			int i_7_ = anInt5222 - i;
			int i_8_ = anInt5221 - i_0_;
			if (i_7_ * i_7_ + i_8_ * i_8_ >= anInt5226 * anInt5226)
				return false;
			return true;
		}
		i_4_ = (i_4_ << 10) / i_3_;
		int i_9_ = anInt5225 + (i_1_ * i_4_ >> 10) - i;
		int i_10_ = anInt5223 + (i_2_ * i_4_ >> 10) - i_0_;
		if (i_9_ * i_9_ + i_10_ * i_10_ >= anInt5226 * anInt5226)
			return false;
		return true;
	}

	public Class338_Sub5() {
		/* empty */
	}
}
