package net.zaros.client;

/* Class116 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class116 implements IConfigsRegister {
	static int anInt3676;
	static OutgoingPacket aClass311_3677 = new OutgoingPacket(32, 4);
	static Class324[] aClass324Array3678;
	static boolean aBoolean3679 = false;

	public final int getConfig(boolean bool, int i) {
		if (bool != true)
			return 65;
		IntegerNode class296_sub16 = ((IntegerNode) Class352.aClass263_3043.get((long) i));
		if (class296_sub16 == null)
			return Class16_Sub3_Sub1.configsRegister.getConfig(true, i);
		return class296_sub16.value;
	}

	public final int getBITConfig(int i, byte i_0_) {
		IntegerNode class296_sub16 = ((IntegerNode) Class352.aClass263_3043.get((long) i | 0x100000000L));
		if (class296_sub16 == null)
			return Class16_Sub3_Sub1.configsRegister.getBITConfig(i, (byte) 120);
		int i_1_ = 35 / ((-20 - i_0_) / 54);
		return class296_sub16.value;
	}

	public static void method1013(int i) {
		if (i == -11412) {
			aClass311_3677 = null;
			aClass324Array3678 = null;
		}
	}

	static final boolean method1014(int i, int i_2_, byte i_3_) {
		if (i_3_ != 104)
			method1013(-18);
		if (!(Class296_Sub44.method2928(255, i, i_2_) | (i & 0x70000) != 0) && !Class296_Sub34_Sub2.method2741(i, i_2_, (byte) -89))
			return false;
		return true;
	}
}
