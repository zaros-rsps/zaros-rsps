package net.zaros.client;
/* Class381 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;

class Class381 {
	private long aLong3221;
	int[] anIntArray3222;
	private int anInt3223 = 32;
	private boolean aBoolean3224 = false;
	private Class296_Sub45 aClass296_Sub45_3225;
	static Js5 fs0;
	private int anInt3227;
	private boolean aBoolean3228;
	private int anInt3229;
	private int anInt3230;
	private int anInt3231;
	private long aLong3232;
	private Class296_Sub45[] aClass296_Sub45Array3233;
	int anInt3234;
	int anInt3235;
	private long aLong3236;
	private int anInt3237;
	private Class296_Sub45[] aClass296_Sub45Array3238;

	final synchronized void method3989(byte i) {
		if (!aBoolean3224) {
			long l = Class72.method771(-116);
			try {
				if (l > aLong3221 + 6000L)
					aLong3221 = l + -6000L;
				for (/**/; l > aLong3221 + 5000L; l = Class72.method771(-111)) {
					method3998(256, 0);
					aLong3221 += (long) (256000 / Class298.anInt2699);
				}
			} catch (Exception exception) {
				aLong3221 = l;
			}
			if (anIntArray3222 != null) {
				do {
					try {
						if (aLong3236 != 0L) {
							if (l < aLong3236)
								break;
							method3999(anInt3235);
							aBoolean3228 = true;
							aLong3236 = 0L;
						}
						int i_0_ = method3995();
						if (anInt3231 - i_0_ > anInt3237)
							anInt3237 = -i_0_ + anInt3231;
						int i_1_ = anInt3227 + anInt3234;
						if (i_1_ + 256 > 16384)
							i_1_ = 16128;
						if (i_1_ + 256 > anInt3235) {
							anInt3235 += 1024;
							if (anInt3235 > 16384)
								anInt3235 = 16384;
							method4001();
							i_0_ = 0;
							method3999(anInt3235);
							aBoolean3228 = true;
							if (anInt3235 < i_1_ + 256) {
								i_1_ = anInt3235 - 256;
								anInt3227 = i_1_ - anInt3234;
							}
						}
						while (i_0_ < i_1_) {
							method3991(anIntArray3222, 256);
							i_0_ += 256;
							method4000();
						}
						if (l > aLong3232) {
							if (!aBoolean3228) {
								if (anInt3237 == 0 && anInt3230 == 0) {
									method4001();
									aLong3236 = l + 2000L;
									break;
								}
								anInt3227 = Math.min(anInt3230, anInt3237);
								anInt3230 = anInt3237;
							} else
								aBoolean3228 = false;
							aLong3232 = l - -2000L;
							anInt3237 = 0;
						}
						anInt3231 = i_0_;
						if (i == -49)
							break;
						aLong3232 = 89L;
					} catch (Exception exception) {
						method4001();
						aLong3236 = l + 2000L;
					}
					break;
				} while (false);
			}
		}
	}

	static final void method3990(boolean bool, AnimationsLoader class310) {
		if (bool != true)
			method3992((byte) -94);
		Class377.aClass310_3188 = class310;
	}

	private final void method3991(int[] is, int i) {
		int i_2_ = i;
		if (HardReferenceWrapper.aBoolean6695)
			i_2_ <<= 1;
		Class291.method2404(is, 0, i_2_);
		anInt3229 -= i;
		if (aClass296_Sub45_3225 != null && anInt3229 <= 0) {
			anInt3229 += Class298.anInt2699 >> 4;
			Class338_Sub3_Sub5_Sub1.method3584((byte) -52, aClass296_Sub45_3225);
			method3993(0, aClass296_Sub45_3225, aClass296_Sub45_3225.method2935());
			int i_3_ = 0;
			int i_4_ = 255;
			int i_5_ = 7;
			while_264_ : while (i_4_ != 0) {
				int i_6_;
				int i_7_;
				if (i_5_ < 0) {
					i_6_ = i_5_ & 0x3;
					i_7_ = -(i_5_ >> 2);
				} else {
					i_6_ = i_5_;
					i_7_ = 0;
				}
				for (int i_8_ = i_4_ >>> i_6_ & 0x11111111; i_8_ != 0; i_8_ >>>= 4) {
					if ((i_8_ & 0x1) != 0) {
						i_4_ &= 1 << i_6_ ^ 0xffffffff;
						Class296_Sub45 class296_sub45 = null;
						Class296_Sub45 class296_sub45_9_ = aClass296_Sub45Array3238[i_6_];
						while (class296_sub45_9_ != null) {
							Class296_Sub19 class296_sub19 = class296_sub45_9_.aClass296_Sub19_4951;
							if (class296_sub19 != null && class296_sub19.anInt4714 > i_7_) {
								i_4_ |= 1 << i_6_;
								class296_sub45 = class296_sub45_9_;
								class296_sub45_9_ = class296_sub45_9_.aClass296_Sub45_4953;
							} else {
								class296_sub45_9_.aBoolean4952 = true;
								int i_10_ = class296_sub45_9_.method2929();
								i_3_ += i_10_;
								if (class296_sub19 != null)
									class296_sub19.anInt4714 += i_10_;
								if (i_3_ >= anInt3223)
									break while_264_;
								Class296_Sub45 class296_sub45_11_ = class296_sub45_9_.method2930();
								if (class296_sub45_11_ != null) {
									int i_12_ = class296_sub45_9_.anInt4950;
									for (/**/; class296_sub45_11_ != null; class296_sub45_11_ = class296_sub45_9_.method2932())
										method3993(0, class296_sub45_11_, (i_12_ * class296_sub45_11_.method2935() >> 8));
								}
								Class296_Sub45 class296_sub45_13_ = class296_sub45_9_.aClass296_Sub45_4953;
								class296_sub45_9_.aClass296_Sub45_4953 = null;
								if (class296_sub45 == null)
									aClass296_Sub45Array3238[i_6_] = class296_sub45_13_;
								else
									class296_sub45.aClass296_Sub45_4953 = class296_sub45_13_;
								if (class296_sub45_13_ == null)
									aClass296_Sub45Array3233[i_6_] = class296_sub45;
								class296_sub45_9_ = class296_sub45_13_;
							}
						}
					}
					i_6_ += 4;
					i_7_++;
				}
				i_5_--;
			}
			for (int i_14_ = 0; i_14_ < 8; i_14_++) {
				Class296_Sub45 class296_sub45 = aClass296_Sub45Array3238[i_14_];
				aClass296_Sub45Array3238[i_14_] = aClass296_Sub45Array3233[i_14_] = null;
				Class296_Sub45 class296_sub45_15_;
				for (/**/; class296_sub45 != null; class296_sub45 = class296_sub45_15_) {
					class296_sub45_15_ = class296_sub45.aClass296_Sub45_4953;
					class296_sub45.aClass296_Sub45_4953 = null;
				}
			}
		}
		if (anInt3229 < 0)
			anInt3229 = 0;
		if (aClass296_Sub45_3225 != null)
			aClass296_Sub45_3225.method2934(is, 0, i);
		aLong3221 = Class72.method771(-118);
	}

	public static void method3992(byte i) {
		fs0 = null;
		if (i != -98)
			fs0 = null;
	}

	private final void method3993(int i, Class296_Sub45 class296_sub45, int i_16_) {
		int i_17_ = i_16_ >> 5;
		Class296_Sub45 class296_sub45_18_ = aClass296_Sub45Array3233[i_17_];
		if (class296_sub45_18_ == null)
			aClass296_Sub45Array3238[i_17_] = class296_sub45;
		else
			class296_sub45_18_.aClass296_Sub45_4953 = class296_sub45;
		aClass296_Sub45Array3233[i_17_] = class296_sub45;
		if (i != 0)
			anInt3237 = -79;
		class296_sub45.anInt4950 = i_16_;
	}

	final synchronized void method3994(int i) {
		aBoolean3228 = true;
		if (i <= 88)
			method3998(114, -8);
		try {
			method3997();
		} catch (Exception exception) {
			method4001();
			aLong3236 = Class72.method771(-124) + 2000L;
		}
	}

	int method3995() throws Exception {
		return anInt3235;
	}

	final synchronized void method3996(int i, Class296_Sub45 class296_sub45) {
		aClass296_Sub45_3225 = class296_sub45;
		int i_19_ = 12 / ((i - 43) / 39);
	}

	void method3997() throws Exception {
		/* empty */
	}

	private final void method3998(int i, int i_20_) {
		anInt3229 -= i;
		if (i_20_ > anInt3229)
			anInt3229 = 0;
		if (aClass296_Sub45_3225 != null)
			aClass296_Sub45_3225.method2933(i);
	}

	void method3999(int i) throws Exception {
		/* empty */
	}

	void method4000() throws Exception {
		/* empty */
	}

	void method4001() {
		/* empty */
	}

	final synchronized void method4002(byte i) {
		if (Class296_Sub15_Sub4.aClass22_6028 != null) {
			boolean bool = true;
			for (int i_21_ = 0; i_21_ < 2; i_21_++) {
				if (this == (Class296_Sub15_Sub4.aClass22_6028.aClass381Array250[i_21_]))
					Class296_Sub15_Sub4.aClass22_6028.aClass381Array250[i_21_] = null;
				if (Class296_Sub15_Sub4.aClass22_6028.aClass381Array250[i_21_] != null)
					bool = false;
			}
			if (bool) {
				Class296_Sub15_Sub4.aClass22_6028.aBoolean253 = true;
				while (Class296_Sub15_Sub4.aClass22_6028.aBoolean254)
					Class106_Sub1.method942(50L, 0);
				Class296_Sub15_Sub4.aClass22_6028 = null;
			}
		}
		if (i != 22)
			method3992((byte) 10);
		method4001();
		anIntArray3222 = null;
		aBoolean3224 = true;
	}

	void method4003(Component component) throws Exception {
		/* empty */
	}

	public Class381() {
		aLong3221 = Class72.method771(-120);
		aBoolean3228 = true;
		aLong3232 = 0L;
		anInt3237 = 0;
		anInt3229 = 0;
		aLong3236 = 0L;
		anInt3231 = 0;
		aClass296_Sub45Array3233 = new Class296_Sub45[8];
		aClass296_Sub45Array3238 = new Class296_Sub45[8];
		anInt3230 = 0;
	}
}
