package net.zaros.client;

/* Class21 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class21 {
	static int anInt248 = -1;

	static final String method288(int i, int i_0_, int i_1_) {
		if (i > -88)
			anInt248 = -58;
		int i_2_ = -i_1_ + i_0_;
		if (i_2_ < -9)
			return "<col=ff0000>";
		if (i_2_ < -6)
			return "<col=ff3000>";
		if (i_2_ < -3)
			return "<col=ff7000>";
		if (i_2_ < 0)
			return "<col=ffb000>";
		if (i_2_ > 9)
			return "<col=00ff00>";
		if (i_2_ > 6)
			return "<col=40ff00>";
		if (i_2_ > 3)
			return "<col=80ff00>";
		if (i_2_ > 0)
			return "<col=c0ff00>";
		return "<col=ffff00>";
	}

	static final Class296_Sub45_Sub4 method289(int i) {
		if (i >= -100)
			anInt248 = 85;
		return Class235.aClass296_Sub45_Sub4_2229;
	}
}
