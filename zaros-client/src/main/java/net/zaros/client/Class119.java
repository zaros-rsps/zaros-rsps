package net.zaros.client;

/* Class119 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class119 {
	Interface20 anInterface20_1194;
	static double aDouble1195;

	static final void method1025(int i, boolean bool, int i_0_) {
		if (i_0_ < 38)
			method1025(13, false, -44);
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i, 21);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.intParam = bool ? 1 : 0;
	}

	static final void readBuildMapDynamic() {
		ByteStream in = Class296_Sub45_Sub2.aClass204_6277.in_stream;
		Class338_Sub2.mapLoadType = in.readUnsignedByteS();
		int mapChunkY = in.g2();
		int mapSize = in.readUnsignedLEShort();
		boolean forceRefresh = in.readUnsignedByteS() == 1;
		int mapSizeX = in.g1();
		Class384.clearNpcsOnCondition();
		Class41_Sub28.setMapSize(mapSizeX);
		in.prepareBITStream();
		for (int z = 0; z < 4; z++) {
			for (int x = 0; x < Class198.currentMapSizeX >> 3; x++) {
				for (int y = 0; Class296_Sub38.currentMapSizeY >> 3 > y; y++) {
					int notEmpty = in.readBITS(1);
					if (notEmpty != 1)
						Class181.buildedMapParts[z][x][y] = -1;
					else
						Class181.buildedMapParts[z][x][y] = in.readBITS(26);
				}
			}
		}
		in.endBITStream();
		int numRegions = ((Class296_Sub45_Sub2.aClass204_6277.protSize - in.pos) / 16);
		Class355_Sub2.xteaKeys = new int[numRegions][4];
		for (int a = 0; numRegions > a; a++) {
			for (int i = 0; i < 4; i++)
				Class355_Sub2.xteaKeys[a][i] = in.g4();
		}
		Class4.anIntArray70 = new int[numRegions];
		Class188.anIntArray1924 = new int[numRegions];
		StaticMethods.anIntArray1844 = new int[numRegions];
		Class296_Sub43.anIntArray4940 = new int[numRegions];
		Class56.anIntArray659 = new int[numRegions];
		Class166.aByteArrayArray1691 = new byte[numRegions][];
		Class243.anIntArray2317 = null;
		Class107_Sub1.aByteArrayArray5667 = new byte[numRegions][];
		Class296_Sub51_Sub31.aByteArrayArray6509 = new byte[numRegions][];
		ParticleEmitterRaw.aByteArrayArray1772 = null;
		StaticMethods.aByteArrayArray3167 = new byte[numRegions][];
		numRegions = 0;
		for (int i_11_ = 0; i_11_ < 4; i_11_++) {
			for (int i_12_ = 0; Class198.currentMapSizeX >> 3 > i_12_; i_12_++) {
				for (int i_13_ = 0; i_13_ < Class296_Sub38.currentMapSizeY >> 3; i_13_++) {
					int i_14_ = (Class181.buildedMapParts[i_11_][i_12_][i_13_]);
					if (i_14_ != -1) {
						int i_15_ = (i_14_ & 0xffd1ab) >> 14;
						int i_16_ = (i_14_ & 0x3ffd) >> 3;
						int i_17_ = (i_15_ / 8 << 8) + i_16_ / 8;
						for (int i_18_ = 0; numRegions > i_18_; i_18_++) {
							if (Class296_Sub43.anIntArray4940[i_18_] == i_17_) {
								i_17_ = -1;
								break;
							}
						}
						if (i_17_ != -1) {
							Class296_Sub43.anIntArray4940[numRegions] = i_17_;
							int i_19_ = (i_17_ & 0xff21) >> 8;
							int i_20_ = i_17_ & 0xff;
							Class56.anIntArray659[numRegions] = (Class324.fs5.getFileIndex("m" + i_19_ + "_" + i_20_));
							Class4.anIntArray70[numRegions] = (Class324.fs5.getFileIndex("l" + i_19_ + "_" + i_20_));
							Class188.anIntArray1924[numRegions] = (Class324.fs5.getFileIndex("um" + i_19_ + "_" + i_20_));
							StaticMethods.anIntArray1844[numRegions] = (Class324.fs5.getFileIndex("ul" + i_19_ + "_" + i_20_));
							numRegions++;
						}
					}
				}
			}
		}
		StaticMethods.method1010(0, 12, mapChunkY, forceRefresh, mapSize);
	}

	static final void method1027(int i) {
		Class250.aClass15_2360.method230(59);
		if (i != 16)
			aDouble1195 = 0.748873847697846;
		Class262.aClass117_2449.method1018((byte) 118);
		Class296_Sub51_Sub13.aClass24_6420.clearSoftReferences();
		Class379.objectDefinitionLoader.method375(false);
		Class352.npcDefinitionLoader.method1418((byte) -100);
		Class296_Sub39_Sub1.itemDefinitionLoader.removeSoftReferences();
		Class296_Sub51_Sub13.animationsLoader.method3300(2);
		Class157.graphicsLoader.method2287(-124);
		Class296_Sub43.bitConfigsLoader.clearSoftReferences(-102);
		ConfigurationsLoader.configsLoader.clearSoftReferences(-30476);
		StaticMethods.aClass328_6070.method3396(18);
		HashTable.aClass149_2456.method1516(17474);
		Class41_Sub10.aClass62_3768.method696((byte) 81);
		ConfigurationsLoader.aClass401_86.method4139(-79);
		Class31.aClass245_324.method2180(-103);
		Class296_Sub51_Sub38.aClass405_6542.method4175((byte) -12);
		Class296_Sub22.itemExtraDataDefinitionLoader.method1655((byte) -100);
		InvisiblePlayer.aClass279_1977.method2341((byte) -107);
		Class49.aClass182_457.method1851(-121);
		Class386.aClass335_3268.method3429((byte) -86);
		Class355.aClass394_3067.method4063(i - 99);
		Class338_Sub9.aClass76_5266.method785((byte) -79);
		ParamType.aClass164_3248.method1632(50560);
		Class154_Sub1.method1563(i ^ 0xf4250);
		Class41_Sub13.method443(76);
		StaticMethods.method3898((byte) -89);
		Class338_Sub3_Sub3_Sub1.method3560(-90);
		Class61.method693(2779);
		Class121.aClass113_1269.clearSoftReferences();
		Class296_Sub15_Sub1.aClass113_5994.clearSoftReferences();
		Class264.aClass113_2471.clearSoftReferences();
		Class366_Sub6.aClass113_5390.clearSoftReferences();
		CS2Executor.aClass113_1531.clearSoftReferences();
	}
}
