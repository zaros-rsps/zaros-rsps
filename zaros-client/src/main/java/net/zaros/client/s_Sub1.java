package net.zaros.client;

/* s_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class s_Sub1 extends s {
	private float aFloat5089;
	private float aFloat5090;
	private float aFloat5091;
	private float aFloat5092;
	private Class80[][] aClass80ArrayArray5093;
	private float aFloat5094;
	private float aFloat5095;
	private float aFloat5096;
	private Class323[][] aClass323ArrayArray5097;
	private byte[][] aByteArrayArray5098;
	private float aFloat5099;
	private Class73[][] aClass73ArrayArray5100;
	private byte[][] aByteArrayArray5101;
	private ha_Sub2 aHa_Sub2_5102;
	private int anInt5103;
	private float aFloat5104;
	private float aFloat5105;
	private float aFloat5106;
	private float aFloat5107;
	private int anInt5108 = -1;
	private Class82[][] aClass82ArrayArray5109;
	private Class372[][] aClass372ArrayArray5110;

	public void wa(r var_r, int i, int i_0_, int i_1_, int i_2_, boolean bool) {
		/* empty */
	}

	public void method3354(int i, int i_3_, int i_4_, boolean[][] bools, boolean bool, int i_5_) {
		Class373_Sub3 class373_sub3 = aHa_Sub2_5102.aClass373_Sub3_4090;
		anInt5108 = -1;
		aFloat5106 = class373_sub3.aFloat5606;
		aFloat5090 = class373_sub3.aFloat5613;
		aFloat5099 = class373_sub3.aFloat5610;
		aFloat5095 = class373_sub3.aFloat5611;
		aFloat5096 = class373_sub3.aFloat5607;
		aFloat5107 = class373_sub3.aFloat5615;
		aFloat5091 = class373_sub3.aFloat5616;
		aFloat5105 = class373_sub3.aFloat5619;
		aFloat5104 = class373_sub3.aFloat5614;
		aFloat5089 = class373_sub3.aFloat5617;
		aFloat5092 = class373_sub3.aFloat5604;
		aFloat5094 = class373_sub3.aFloat5618;
		for (int i_6_ = 0; i_6_ < i_4_ + i_4_; i_6_++) {
			for (int i_7_ = 0; i_7_ < i_4_ + i_4_; i_7_++) {
				if (bools[i_6_][i_7_]) {
					int i_8_ = i - i_4_ + i_6_;
					int i_9_ = i_3_ - i_4_ + i_7_;
					if (i_8_ >= 0 && i_8_ < anInt2832 && i_9_ >= 0 && i_9_ < anInt2834)
						method3366(i_8_, i_9_, i_5_);
				}
			}
		}
	}

	public r fa(int i, int i_10_, r var_r) {
		return null;
	}

	private static int method3359(int i, int i_11_) {
		int i_12_ = (i & 0xff0000) * i_11_ >> 23;
		if (i_12_ < 2)
			i_12_ = 2;
		else if (i_12_ > 253)
			i_12_ = 253;
		int i_13_ = (i & 0xff00) * i_11_ >> 15;
		if (i_13_ < 2)
			i_13_ = 2;
		else if (i_13_ > 253)
			i_13_ = 253;
		int i_14_ = (i & 0xff) * i_11_ >> 7;
		if (i_14_ < 2)
			i_14_ = 2;
		else if (i_14_ > 253)
			i_14_ = 253;
		return i_12_ << 16 | i_13_ << 8 | i_14_;
	}

	private void method3360(int i, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_, int i_20_, boolean[][] bools,
			Class240 class240, Class11 class11, int[] is, int[] is_21_) {
		int i_22_ = (i_20_ - i_18_) * i_16_ / 256;
		int i_23_ = i_16_ >> 8;
		boolean bool = class240.aBoolean2262;
		aHa_Sub2_5102.C(false);
		class11.aBoolean102 = false;
		class11.aBoolean106 = false;
		int i_24_ = i;
		int i_25_ = i_15_ + i_22_;
		for (int i_26_ = i_17_; i_26_ < i_19_; i_26_++) {
			for (int i_27_ = i_18_; i_27_ < i_20_; i_27_++) {
				if (bools[i_26_ - i_17_][i_27_ - i_18_]) {
					if (aClass372ArrayArray5110[i_26_][i_27_] != null) {
						Class372 class372 = aClass372ArrayArray5110[i_26_][i_27_];
						if (class372.aShort3170 != -1 && (class372.aByte3173 & 0x2) == 0 && class372.anInt3174 == -1) {
							int i_28_ = aHa_Sub2_5102.method1269(class372.aShort3170);
							class11.method205((float) (i_25_ - i_23_), (float) (i_25_ - i_23_), (float) i_25_,
									(float) (i_24_ + i_23_), (float) i_24_, (float) (i_24_ + i_23_), 100.0F, 100.0F,
									100.0F, (float) (ByteStream.method2632(25051, class372.aShort3172 & 0xffff, i_28_)),
									(float) (ByteStream.method2632(25051, class372.aShort3175 & 0xffff, i_28_)),
									(float) (ByteStream.method2632(25051, class372.aShort3171 & 0xffff, i_28_)));
							class11.method205((float) i_25_, (float) i_25_, (float) (i_25_ - i_23_), (float) i_24_,
									(float) (i_24_ + i_23_), (float) i_24_, 100.0F, 100.0F, 100.0F,
									(float) (ByteStream.method2632(25051, class372.aShort3176 & 0xffff, i_28_)),
									(float) (ByteStream.method2632(25051, class372.aShort3171 & 0xffff, i_28_)),
									(float) (ByteStream.method2632(25051, class372.aShort3175 & 0xffff, i_28_)));
						} else if (class372.anInt3174 == -1) {
							class11.method205((float) (i_25_ - i_23_), (float) (i_25_ - i_23_), (float) i_25_,
									(float) (i_24_ + i_23_), (float) i_24_, (float) (i_24_ + i_23_), 100.0F, 100.0F,
									100.0F, (float) (class372.aShort3172 & 0xffff),
									(float) (class372.aShort3175 & 0xffff), (float) (class372.aShort3171 & 0xffff));
							class11.method205((float) i_25_, (float) i_25_, (float) (i_25_ - i_23_), (float) i_24_,
									(float) (i_24_ + i_23_), (float) i_24_, 100.0F, 100.0F, 100.0F,
									(float) (class372.aShort3176 & 0xffff), (float) (class372.aShort3171 & 0xffff),
									(float) (class372.aShort3175 & 0xffff));
						} else {
							int i_29_ = class372.anInt3174;
							class11.method205((float) (i_25_ - i_23_), (float) (i_25_ - i_23_), (float) i_25_,
									(float) (i_24_ + i_23_), (float) i_24_, (float) (i_24_ + i_23_), 100.0F, 100.0F,
									100.0F, (float) i_29_, (float) i_29_, (float) i_29_);
							class11.method205((float) i_25_, (float) i_25_, (float) (i_25_ - i_23_), (float) i_24_,
									(float) (i_24_ + i_23_), (float) i_24_, 100.0F, 100.0F, 100.0F, (float) i_29_,
									(float) i_29_, (float) i_29_);
						}
					} else if (aClass82ArrayArray5109[i_26_][i_27_] != null) {
						Class82 class82 = aClass82ArrayArray5109[i_26_][i_27_];
						for (int i_30_ = 0; i_30_ < class82.aShort902; i_30_++) {
							is[i_30_] = i_24_ + (class82.aShortArray909[i_30_] * i_23_ / anInt2836);
							is_21_[i_30_] = i_25_ - (class82.aShortArray908[i_30_] * i_23_ / anInt2836);
						}
						for (int i_31_ = 0; i_31_ < class82.aShort904; i_31_++) {
							short i_32_ = class82.aShortArray911[i_31_];
							short i_33_ = class82.aShortArray910[i_31_];
							short i_34_ = class82.aShortArray900[i_31_];
							int i_35_ = is[i_32_];
							int i_36_ = is[i_33_];
							int i_37_ = is[i_34_];
							int i_38_ = is_21_[i_32_];
							int i_39_ = is_21_[i_33_];
							int i_40_ = is_21_[i_34_];
							if (class82.anIntArray912 != null && class82.anIntArray912[i_31_] != -1) {
								int i_41_ = class82.anIntArray912[i_31_];
								class11.method205((float) i_38_, (float) i_39_, (float) i_40_, (float) i_35_,
										(float) i_36_, (float) i_37_, 100.0F, 100.0F, 100.0F,
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_32_], i_41_)),
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_33_], i_41_)),
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_34_], i_41_)));
							} else if (class82.aShortArray913 != null && (class82.aShortArray913[i_31_] != -1)) {
								int i_42_ = aHa_Sub2_5102.method1269(class82.aShortArray913[i_31_]);
								class11.method205((float) i_38_, (float) i_39_, (float) i_40_, (float) i_35_,
										(float) i_36_, (float) i_37_, 100.0F, 100.0F, 100.0F,
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_32_], i_42_)),
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_33_], i_42_)),
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_34_], i_42_)));
							} else {
								int i_43_ = class82.anIntArray906[i_31_];
								class11.method205((float) i_38_, (float) i_39_, (float) i_40_, (float) i_35_,
										(float) i_36_, (float) i_37_, 100.0F, 100.0F, 100.0F,
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_32_], i_43_)),
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_33_], i_43_)),
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_34_], i_43_)));
							}
						}
					}
				}
				i_25_ -= i_23_;
			}
			i_25_ = i_15_ + i_22_;
			i_24_ += i_23_;
		}
		class11.aBoolean102 = true;
		aHa_Sub2_5102.C(bool);
	}

	private void method3361(int i, int i_44_, boolean bool, Class240 class240, Class11 class11, int[] is, int[] is_45_,
			int[] is_46_, int[] is_47_, int i_48_) {
		Class80 class80 = aClass80ArrayArray5093[i][i_44_];
		if (i_48_ == 0 || (i_48_ & 0x2) == 0) {
			if (class80 != null) {
				if (anInt5108 == -1) {
					for (int i_49_ = 0; i_49_ < class80.aShort895; i_49_++) {
						int i_50_ = class80.aShortArray897[i_49_] + (i << anInt2835);
						int i_51_ = class80.aShortArray892[i_49_];
						int i_52_ = (class80.aShortArray891[i_49_] + (i_44_ << anInt2835));
						float f = aFloat5094 + (aFloat5104 * (float) i_50_ + aFloat5089 * (float) i_51_
								+ aFloat5092 * (float) i_52_);
						if (f <= (float) aHa_Sub2_5102.anInt4107)
							return;
						is_47_[i_49_] = 0;
						if (bool) {
							int i_53_ = (int) (f - (float) class240.anInt2267);
							if (i_53_ > 255)
								i_53_ = 255;
							if (i_53_ > 0) {
								is_47_[i_49_] = i_53_;
								int i_54_ = (class80.aShortArray894[i_49_] * i_53_ / 255);
								if (i_54_ > 0)
									i_51_ -= i_54_;
							}
						} else if (class240.aBoolean2259) {
							int i_55_ = (int) (f - (float) class240.anInt2267);
							if (i_55_ > 0) {
								is_47_[i_49_] = i_55_;
								if (is_47_[i_49_] > 255)
									is_47_[i_49_] = 255;
							}
						}
						float f_56_ = aFloat5095 + (aFloat5106 * (float) i_50_ + aFloat5090 * (float) i_51_
								+ aFloat5099 * (float) i_52_);
						float f_57_ = aFloat5105 + (aFloat5096 * (float) i_50_ + aFloat5107 * (float) i_51_
								+ aFloat5091 * (float) i_52_);
						is[i_49_] = (class11.anInt109 + (int) (f_56_ * (float) aHa_Sub2_5102.anInt4086 / f));
						is_45_[i_49_] = (class11.anInt95 + (int) (f_57_ * (float) aHa_Sub2_5102.anInt4072 / f));
						is_46_[i_49_] = (int) f;
					}
				} else {
					for (int i_58_ = 0; i_58_ < class80.aShort895; i_58_++) {
						int i_59_ = class80.aShortArray897[i_58_] + (i << anInt2835);
						int i_60_ = class80.aShortArray892[i_58_];
						int i_61_ = (class80.aShortArray891[i_58_] + (i_44_ << anInt2835));
						float f = aFloat5094 + (aFloat5104 * (float) i_59_ + aFloat5089 * (float) i_60_
								+ aFloat5092 * (float) i_61_);
						is_47_[i_58_] = 0;
						if (bool) {
							int i_62_ = anInt5108 - class240.anInt2267;
							if (i_62_ > 255)
								i_62_ = 255;
							if (i_62_ > 0) {
								is_47_[i_58_] = i_62_;
								int i_63_ = (class80.aShortArray894[i_58_] * i_62_ / 255);
								if (i_63_ > 0)
									i_60_ -= i_63_;
							}
						} else if (class240.aBoolean2259) {
							int i_64_ = anInt5108 - class240.anInt2267;
							if (i_64_ > 0) {
								is_47_[i_58_] = i_64_;
								if (is_47_[i_58_] > 255)
									is_47_[i_58_] = 255;
							}
						}
						float f_65_ = aFloat5095 + (aFloat5106 * (float) i_59_ + aFloat5090 * (float) i_60_
								+ aFloat5099 * (float) i_61_);
						float f_66_ = aFloat5105 + (aFloat5096 * (float) i_59_ + aFloat5107 * (float) i_60_
								+ aFloat5091 * (float) i_61_);
						is[i_58_] = (class11.anInt109
								+ (int) (f_65_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						is_45_[i_58_] = (class11.anInt95
								+ (int) (f_66_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						is_46_[i_58_] = (int) f;
					}
				}
				float f = (float) anInt2836;
				for (int i_67_ = 0; i_67_ < class80.aShort898; i_67_++) {
					int i_68_ = i_67_ * 3;
					int i_69_ = i_68_ + 1;
					int i_70_ = i_69_ + 1;
					int i_71_ = is[i_68_];
					int i_72_ = is[i_69_];
					int i_73_ = is[i_70_];
					int i_74_ = is_45_[i_68_];
					int i_75_ = is_45_[i_69_];
					int i_76_ = is_45_[i_70_];
					if (((i_71_ - i_72_) * (i_76_ - i_75_) - (i_74_ - i_75_) * (i_73_ - i_72_)) > 0) {
						class11.aBoolean103 = (i_71_ < 0 || i_72_ < 0 || i_73_ < 0 || i_71_ > class11.anInt107
								|| i_72_ > class11.anInt107 || i_73_ > class11.anInt107);
						if (is_47_[i_68_] + is_47_[i_69_] + is_47_[i_70_] < 765) {
							int i_77_ = i << anInt2835;
							int i_78_ = i_44_ << anInt2835;
							if ((class80.anIntArray889[i_68_] & 0xffffff) != 0) {
								if ((class80.aShortArray893[i_68_] == class80.aShortArray893[i_69_])
										&& (class80.aShortArray893[i_68_] == class80.aShortArray893[i_70_])
										&& (class80.aShortArray896[i_68_] == class80.aShortArray896[i_69_])
										&& (class80.aShortArray896[i_68_] == class80.aShortArray896[i_70_]))
									class11.method214((float) i_74_, (float) i_75_, (float) i_76_, (float) i_71_,
											(float) i_72_, (float) i_73_, (float) is_46_[i_68_], (float) is_46_[i_69_],
											(float) is_46_[i_70_],
											((float) (i_77_ + (class80.aShortArray897[i_68_]))
													/ (float) (class80.aShortArray896[i_68_])),
											((float) (i_77_ + (class80.aShortArray897[i_69_]))
													/ (float) (class80.aShortArray896[i_69_])),
											((float) (i_77_ + (class80.aShortArray897[i_70_]))
													/ (float) (class80.aShortArray896[i_70_])),
											((float) (i_78_ + (class80.aShortArray891[i_68_]))
													/ (float) (class80.aShortArray896[i_68_])),
											((float) (i_78_ + (class80.aShortArray891[i_69_]))
													/ (float) (class80.aShortArray896[i_69_])),
											((float) (i_78_ + (class80.aShortArray891[i_70_]))
													/ (float) (class80.aShortArray896[i_70_])),
											class80.anIntArray889[i_68_], class80.anIntArray889[i_69_],
											class80.anIntArray889[i_70_], class240.anInt2265, is_47_[i_68_],
											is_47_[i_69_], is_47_[i_70_], class80.aShortArray893[i_68_]);
								else
									class11.method217((float) i_74_, (float) i_75_, (float) i_76_, (float) i_71_,
											(float) i_72_, (float) i_73_, (float) is_46_[i_68_], (float) is_46_[i_69_],
											(float) is_46_[i_70_],
											(float) (i_77_ + (class80.aShortArray897[i_68_])) / f,
											(float) (i_77_ + (class80.aShortArray897[i_69_])) / f,
											(float) (i_77_ + (class80.aShortArray897[i_70_])) / f,
											(float) (i_78_ + (class80.aShortArray891[i_68_])) / f,
											(float) (i_78_ + (class80.aShortArray891[i_69_])) / f,
											(float) (i_78_ + (class80.aShortArray891[i_70_])) / f,
											class80.anIntArray889[i_68_], class80.anIntArray889[i_69_],
											class80.anIntArray889[i_70_], class240.anInt2265, is_47_[i_68_],
											is_47_[i_69_], is_47_[i_70_], class80.aShortArray893[i_68_],
											f / (float) (class80.aShortArray896[i_68_]), class80.aShortArray893[i_69_],
											f / (float) (class80.aShortArray896[i_69_]), class80.aShortArray893[i_70_],
											f / (float) (class80.aShortArray896[i_70_]));
							}
						} else
							class11.method206((float) i_74_, (float) i_75_, (float) i_76_, (float) i_71_, (float) i_72_,
									(float) i_73_, (float) is_46_[i_68_], (float) is_46_[i_69_], (float) is_46_[i_70_],
									class240.anInt2265);
					}
				}
			}
		}
	}

	public boolean method3351(r var_r, int i, int i_79_, int i_80_, int i_81_, boolean bool) {
		return false;
	}

	private void method3362(int i, int i_82_, int i_83_, int i_84_, int i_85_, int i_86_, int i_87_, boolean[][] bools,
			Class240 class240, Class11 class11, int[] is, int[] is_88_) {
		int i_89_ = (i_87_ - i_85_) * i_83_ / 256;
		int i_90_ = i_83_ >> 8;
		boolean bool = class240.aBoolean2262;
		aHa_Sub2_5102.C(false);
		class11.aBoolean102 = false;
		class11.aBoolean106 = false;
		int i_91_ = i;
		int i_92_ = i_82_ + i_89_;
		for (int i_93_ = i_84_; i_93_ < i_86_; i_93_++) {
			for (int i_94_ = i_85_; i_94_ < i_87_; i_94_++) {
				if (bools[i_93_ - i_84_][i_94_ - i_85_]) {
					if (aClass73ArrayArray5100 != null) {
						if (aClass73ArrayArray5100[i_93_][i_94_] != null) {
							Class73 class73 = aClass73ArrayArray5100[i_93_][i_94_];
							if (class73.aShort848 != -1 && (class73.aByte853 & 0x2) == 0 && class73.anInt852 == 0) {
								int i_95_ = aHa_Sub2_5102.method1269(class73.aShort848);
								class11.method205((float) (i_92_ - i_90_), (float) (i_92_ - i_90_), (float) i_92_,
										(float) (i_91_ + i_90_), (float) i_91_, (float) (i_91_ + i_90_), 100.0F, 100.0F,
										100.0F, (float) (ByteStream.method2632(25051, class73.anInt849, i_95_)),
										(float) (ByteStream.method2632(25051, class73.anInt855, i_95_)),
										(float) (ByteStream.method2632(25051, class73.anInt850, i_95_)));
								class11.method205((float) i_92_, (float) i_92_, (float) (i_92_ - i_90_), (float) i_91_,
										(float) (i_91_ + i_90_), (float) i_91_, 100.0F, 100.0F, 100.0F,
										(float) (ByteStream.method2632(25051, class73.anInt854, i_95_)),
										(float) (ByteStream.method2632(25051, class73.anInt850, i_95_)),
										(float) (ByteStream.method2632(25051, class73.anInt855, i_95_)));
							} else if (class73.anInt852 == 0) {
								class11.method199((float) (i_92_ - i_90_), (float) (i_92_ - i_90_), (float) i_92_,
										(float) (i_91_ + i_90_), (float) i_91_, (float) (i_91_ + i_90_), 100.0F, 100.0F,
										100.0F, class73.anInt849, class73.anInt855, class73.anInt850);
								class11.method199((float) i_92_, (float) i_92_, (float) (i_92_ - i_90_), (float) i_91_,
										(float) (i_91_ + i_90_), (float) i_91_, 100.0F, 100.0F, 100.0F,
										class73.anInt854, class73.anInt850, class73.anInt855);
							} else {
								int i_96_ = class73.anInt852;
								class11.method199((float) (i_92_ - i_90_), (float) (i_92_ - i_90_), (float) i_92_,
										(float) (i_91_ + i_90_), (float) i_91_, (float) (i_91_ + i_90_), 100.0F, 100.0F,
										100.0F, Class69_Sub4.method745(i_96_, 16711680, (class73.anInt849 & ~0xffffff)),
										Class69_Sub4.method745(i_96_, 16711680, (class73.anInt855 & ~0xffffff)),
										Class69_Sub4.method745(i_96_, 16711680, (class73.anInt850 & ~0xffffff)));
								class11.method199((float) i_92_, (float) i_92_, (float) (i_92_ - i_90_), (float) i_91_,
										(float) (i_91_ + i_90_), (float) i_91_, 100.0F, 100.0F, 100.0F,
										Class69_Sub4.method745(i_96_, 16711680, (class73.anInt854 & ~0xffffff)),
										Class69_Sub4.method745(i_96_, 16711680, (class73.anInt850 & ~0xffffff)),
										Class69_Sub4.method745(i_96_, 16711680, (class73.anInt855 & ~0xffffff)));
							}
						} else if (aClass323ArrayArray5097[i_93_][i_94_] != null) {
							Class323 class323 = aClass323ArrayArray5097[i_93_][i_94_];
							for (int i_97_ = 0; i_97_ < class323.aShort2845; i_97_++) {
								is[i_97_] = i_91_ + (class323.aShortArray2846[i_97_] * i_90_ / anInt2836);
								is_88_[i_97_] = i_92_ - (class323.aShortArray2842[i_97_] * i_90_ / anInt2836);
							}
							for (int i_98_ = 0; i_98_ < class323.aShort2844; i_98_++) {
								int i_99_ = i_98_ * 3;
								int i_100_ = i_99_ + 1;
								int i_101_ = i_100_ + 1;
								int i_102_ = is[i_99_];
								int i_103_ = is[i_100_];
								int i_104_ = is[i_101_];
								int i_105_ = is_88_[i_99_];
								int i_106_ = is_88_[i_100_];
								int i_107_ = is_88_[i_101_];
								if (class323.anIntArray2838 != null && class323.anIntArray2838[i_98_] != 0
										&& (class323.aShortArray2841 == null || (class323.aShortArray2841 != null
												&& (class323.aShortArray2841[i_98_] == -1)))) {
									int i_108_ = class323.anIntArray2838[i_98_];
									class11.method199((float) i_105_, (float) i_106_, (float) i_107_, (float) i_102_,
											(float) i_103_, (float) i_104_, 100.0F, 100.0F, 100.0F,
											(Class69_Sub4.method745(i_108_, 16711680,
													(-16777216 - (class323.anIntArray2840[i_99_] & ~0xffffff)))),
											(Class69_Sub4.method745(i_108_, 16711680,
													(-16777216 - (class323.anIntArray2840[i_100_] & ~0xffffff)))),
											(Class69_Sub4.method745(i_108_, 16711680,
													(-16777216 - (class323.anIntArray2840[i_101_] & ~0xffffff)))));
								} else if (class323.aShortArray2841 != null
										&& (class323.aShortArray2841[i_98_] != -1)) {
									int i_109_ = (aHa_Sub2_5102.method1269(class323.aShortArray2841[i_98_]));
									class11.method205((float) i_105_, (float) i_106_, (float) i_107_, (float) i_102_,
											(float) i_103_, (float) i_104_, 100.0F, 100.0F, 100.0F, (float) i_109_,
											(float) i_109_, (float) i_109_);
								} else
									class11.method199((float) i_105_, (float) i_106_, (float) i_107_, (float) i_102_,
											(float) i_103_, (float) i_104_, 100.0F, 100.0F, 100.0F,
											class323.anIntArray2840[i_99_], class323.anIntArray2840[i_100_],
											class323.anIntArray2840[i_101_]);
							}
						}
					} else if (aClass80ArrayArray5093[i_93_][i_94_] != null) {
						Class80 class80 = aClass80ArrayArray5093[i_93_][i_94_];
						for (int i_110_ = 0; i_110_ < class80.aShort895; i_110_++) {
							is[i_110_] = i_91_ + (class80.aShortArray897[i_110_] * i_90_ / anInt2836);
							is_88_[i_110_] = i_92_ - (class80.aShortArray891[i_110_] * i_90_ / anInt2836);
						}
						for (int i_111_ = 0; i_111_ < class80.aShort898; i_111_++) {
							int i_112_ = i_111_ * 3;
							int i_113_ = i_112_ + 1;
							int i_114_ = i_113_ + 1;
							int i_115_ = is[i_112_];
							int i_116_ = is[i_113_];
							int i_117_ = is[i_114_];
							int i_118_ = is_88_[i_112_];
							int i_119_ = is_88_[i_113_];
							int i_120_ = is_88_[i_114_];
							if (class80.anIntArray890 != null && class80.anIntArray890[i_111_] != 0) {
								int i_121_ = class80.anIntArray890[i_111_];
								class11.method199((float) i_118_, (float) i_119_, (float) i_120_, (float) i_115_,
										(float) i_116_, (float) i_117_, 100.0F, 100.0F, 100.0F, i_121_, i_121_, i_121_);
							} else
								class11.method199((float) i_118_, (float) i_119_, (float) i_120_, (float) i_115_,
										(float) i_116_, (float) i_117_, 100.0F, 100.0F, 100.0F,
										class80.anIntArray889[i_112_], class80.anIntArray889[i_113_],
										class80.anIntArray889[i_114_]);
						}
					}
				}
				i_92_ -= i_90_;
			}
			i_92_ = i_82_ + i_89_;
			i_91_ += i_90_;
		}
		class11.aBoolean102 = true;
		aHa_Sub2_5102.C(bool);
	}

	public void method3356(int i, int i_122_, int[] is, int[] is_123_, int[] is_124_, int[] is_125_, int[] is_126_,
			int[] is_127_, int[] is_128_, int[] is_129_, int[] is_130_, int[] is_131_, int[] is_132_, int i_133_,
			int i_134_, int i_135_, boolean bool) {
		if (aClass372ArrayArray5110 == null) {
			aClass372ArrayArray5110 = new Class372[anInt2832][anInt2834];
			aClass82ArrayArray5109 = new Class82[anInt2832][anInt2834];
		} else if (aClass73ArrayArray5100 != null || aClass80ArrayArray5093 != null)
			throw new IllegalStateException();
		boolean bool_136_ = false;
		if (is_129_.length == 2 && is_126_.length == 2
				&& (is_129_[0] == is_129_[1] || is_131_[0] != -1 && is_131_[0] == is_131_[1])) {
			bool_136_ = true;
			for (int i_137_ = 1; i_137_ < 2; i_137_++) {
				int i_138_ = is[is_126_[i_137_]];
				int i_139_ = is_124_[is_126_[i_137_]];
				if (i_138_ != 0 && i_138_ != anInt2836 || i_139_ != 0 && i_139_ != anInt2836) {
					bool_136_ = false;
					break;
				}
			}
		}
		if (!bool_136_) {
			Class82 class82 = new Class82();
			short i_140_ = (short) is.length;
			int i_141_ = (short) is_129_.length;
			class82.aShort902 = i_140_;
			class82.aShortArray907 = new short[i_140_];
			class82.aShortArray909 = new short[i_140_];
			class82.aShortArray901 = new short[i_140_];
			class82.aShortArray908 = new short[i_140_];
			for (int i_142_ = 0; i_142_ < i_140_; i_142_++) {
				int i_143_ = is[i_142_];
				int i_144_ = is_124_[i_142_];
				if (i_143_ == 0 && i_144_ == 0)
					class82.aShortArray907[i_142_] = (short) (aByteArrayArray5098[i][i_122_]
							- aByteArrayArray5101[i][i_122_]);
				else if (i_143_ == 0 && i_144_ == anInt2836)
					class82.aShortArray907[i_142_] = (short) (aByteArrayArray5098[i][i_122_ + 1]
							- aByteArrayArray5101[i][i_122_ + 1]);
				else if (i_143_ == anInt2836 && i_144_ == anInt2836)
					class82.aShortArray907[i_142_] = (short) (aByteArrayArray5098[i + 1][i_122_ + 1]
							- aByteArrayArray5101[i + 1][i_122_ + 1]);
				else if (i_143_ == anInt2836 && i_144_ == 0)
					class82.aShortArray907[i_142_] = (short) (aByteArrayArray5098[i + 1][i_122_]
							- aByteArrayArray5101[i + 1][i_122_]);
				else {
					int i_145_ = (((aByteArrayArray5098[i][i_122_] - aByteArrayArray5101[i][i_122_])
							* (anInt2836 - i_143_))
							+ (aByteArrayArray5098[i + 1][i_122_] - aByteArrayArray5101[i + 1][i_122_]) * i_143_);
					int i_146_ = (((aByteArrayArray5098[i][i_122_ + 1] - aByteArrayArray5101[i][i_122_ + 1])
							* (anInt2836 - i_143_))
							+ ((aByteArrayArray5098[i + 1][i_122_ + 1] - aByteArrayArray5101[i + 1][i_122_ + 1])
									* i_143_));
					class82.aShortArray907[i_142_] = (short) ((i_145_ * (anInt2836 - i_144_)
							+ i_146_ * i_144_) >> anInt2835 * 2);
				}
				int i_147_ = (i << anInt2835) + i_143_;
				int i_148_ = (i_122_ << anInt2835) + i_144_;
				class82.aShortArray909[i_142_] = (short) i_143_;
				class82.aShortArray908[i_142_] = (short) i_144_;
				class82.aShortArray901[i_142_] = (short) (this.method3349(0, i_148_, i_147_)
						+ (is_123_ != null ? is_123_[i_142_] : 0));
				if (class82.aShortArray907[i_142_] < 2)
					class82.aShortArray907[i_142_] = (short) 2;
			}
			boolean bool_149_ = false;
			int i_150_ = 0;
			for (int i_151_ = 0; i_151_ < i_141_; i_151_++) {
				if (is_129_[i_151_] >= 0 || is_130_ != null && is_130_[i_151_] >= 0)
					i_150_++;
				int i_152_ = is_131_[i_151_];
				if (i_152_ != -1) {
					MaterialRaw class170 = aHa_Sub2_5102.aD1299.method14(i_152_, -9412);
					if (!class170.aBoolean1792) {
						bool_149_ = true;
						if (method3363(class170.aByte1774) || class170.speed_u != 0 || class170.speed_v != 0)
							class82.aByte905 |= 0x4;
					}
				}
			}
			class82.anIntArray906 = new int[i_150_];
			if (is_130_ != null)
				class82.anIntArray912 = new int[i_150_];
			class82.aShortArray911 = new short[i_150_];
			class82.aShortArray910 = new short[i_150_];
			class82.aShortArray900 = new short[i_150_];
			if (bool_149_) {
				class82.aShortArray913 = new short[i_150_];
				class82.aShortArray903 = new short[i_150_];
			}
			for (int i_153_ = 0; i_153_ < i_141_; i_153_++) {
				if (is_129_[i_153_] >= 0 || is_130_ != null && is_130_[i_153_] >= 0) {
					if (is_129_[i_153_] >= 0)
						class82.anIntArray906[class82.aShort904] = Class338_Sub3_Sub4_Sub1.method3576(is_129_[i_153_],
								(byte) -105);
					else
						class82.anIntArray906[class82.aShort904] = -1;
					if (is_130_ != null) {
						if (is_130_[i_153_] != -1)
							class82.anIntArray912[class82.aShort904] = (Class338_Sub3_Sub4_Sub1
									.method3576(is_130_[i_153_], (byte) -118));
						else
							class82.anIntArray912[class82.aShort904] = -1;
					}
					class82.aShortArray911[class82.aShort904] = (short) is_126_[i_153_];
					class82.aShortArray910[class82.aShort904] = (short) is_127_[i_153_];
					class82.aShortArray900[class82.aShort904] = (short) is_128_[i_153_];
					if (bool_149_) {
						if (is_131_[i_153_] != -1
								&& !(aHa_Sub2_5102.aD1299.method14(is_131_[i_153_], -9412).aBoolean1792)) {
							class82.aShortArray913[class82.aShort904] = (short) is_131_[i_153_];
							class82.aShortArray903[class82.aShort904] = (short) is_132_[i_153_];
						} else
							class82.aShortArray913[class82.aShort904] = (short) -1;
					}
					class82.aShort904++;
				}
			}
			aClass82ArrayArray5109[i][i_122_] = class82;
		} else if (is_129_[0] >= 0 || is_130_ != null && is_130_[0] >= 0) {
			Class372 class372 = new Class372();
			int i_154_ = is_129_[0];
			int i_155_ = is_131_[0];
			if (is_130_ != null) {
				class372.anInt3174 = (ByteStream.method2632(25051,
						(aByteArrayArray5098[i][i_122_] - aByteArrayArray5101[i][i_122_]),
						Class338_Sub3_Sub4_Sub1.method3576(is_130_[0], (byte) -106)));
				if (i_154_ == -1)
					class372.aByte3173 |= 0x2;
			}
			if ((anIntArrayArray2831[i][i_122_] == anIntArrayArray2831[i + 1][i_122_])
					&& (anIntArrayArray2831[i][i_122_] == anIntArrayArray2831[i + 1][i_122_ + 1])
					&& (anIntArrayArray2831[i][i_122_] == anIntArrayArray2831[i][i_122_ + 1]))
				class372.aByte3173 |= 0x1;
			MaterialRaw class170 = null;
			if (i_155_ != -1)
				class170 = aHa_Sub2_5102.aD1299.method14(i_155_, -9412);
			if (class170 != null && (class372.aByte3173 & 0x2) == 0 && !class170.aBoolean1792) {
				class372.aShort3176 = (short) (aByteArrayArray5098[i][i_122_] - aByteArrayArray5101[i][i_122_]);
				class372.aShort3171 = (short) (aByteArrayArray5098[i + 1][i_122_] - aByteArrayArray5101[i + 1][i_122_]);
				class372.aShort3172 = (short) (aByteArrayArray5098[i + 1][i_122_ + 1]
						- aByteArrayArray5101[i + 1][i_122_ + 1]);
				class372.aShort3175 = (short) (aByteArrayArray5098[i][i_122_ + 1] - aByteArrayArray5101[i][i_122_ + 1]);
				class372.aShort3170 = (short) i_155_;
				if (method3363(class170.aByte1774) || class170.speed_u != 0 || class170.speed_v != 0)
					class372.aByte3173 |= 0x4;
			} else {
				short i_156_ = Class338_Sub3_Sub4_Sub1.method3576(i_154_, (byte) -125);
				class372.aShort3176 = (short) (ByteStream.method2632(25051,
						(aByteArrayArray5098[i][i_122_] - aByteArrayArray5101[i][i_122_]), i_156_));
				class372.aShort3171 = (short) (ByteStream.method2632(25051,
						(aByteArrayArray5098[i + 1][i_122_] - aByteArrayArray5101[i + 1][i_122_]), i_156_));
				class372.aShort3172 = (short) (ByteStream.method2632(25051,
						(aByteArrayArray5098[i + 1][i_122_ + 1] - aByteArrayArray5101[i + 1][i_122_ + 1]), i_156_));
				class372.aShort3175 = (short) (ByteStream.method2632(25051,
						(aByteArrayArray5098[i][i_122_ + 1] - aByteArrayArray5101[i][i_122_ + 1]), i_156_));
				class372.aShort3170 = (short) -1;
			}
			aClass372ArrayArray5110[i][i_122_] = class372;
		}
	}

	public void method3350(int i, int i_157_, int i_158_, int i_159_, int i_160_, int i_161_, int i_162_,
			boolean[][] bools) {
		Class240 class240 = aHa_Sub2_5102.method1270(Thread.currentThread());
		Class11 class11 = class240.aClass11_2285;
		class11.anInt108 = 0;
		class11.aBoolean103 = true;
		aHa_Sub2_5102.ya();
		if (aClass73ArrayArray5100 != null || aClass80ArrayArray5093 != null)
			method3362(i, i_157_, i_158_, i_159_, i_160_, i_161_, i_162_, bools, class240, class11,
					class240.anIntArray2277, class240.anIntArray2281);
		else if (aClass372ArrayArray5110 != null)
			method3360(i, i_157_, i_158_, i_159_, i_160_, i_161_, i_162_, bools, class240, class11,
					class240.anIntArray2277, class240.anIntArray2281);
	}

	private boolean method3363(int i) {
		if ((anInt5103 & 0x8) == 0)
			return false;
		if (i == 4)
			return true;
		if (i == 8)
			return true;
		if (i == 9)
			return true;
		return false;
	}

	s_Sub1(ha_Sub2 var_ha_Sub2, int i, int i_163_, int i_164_, int i_165_, int[][] is, int[][] is_166_, int i_167_) {
		super(i_164_, i_165_, i_167_, is);
		aHa_Sub2_5102 = var_ha_Sub2;
		anInt5103 = i_163_;
		aByteArrayArray5098 = new byte[i_164_ + 1][i_165_ + 1];
		int i_168_ = aHa_Sub2_5102.anInt4075 >> 9;
		for (int i_169_ = 1; i_169_ < i_165_; i_169_++) {
			for (int i_170_ = 1; i_170_ < i_164_; i_170_++) {
				int i_171_ = i_168_;
				int i_172_ = (is_166_[i_170_ + 1][i_169_] - is_166_[i_170_ - 1][i_169_]);
				int i_173_ = (is_166_[i_170_][i_169_ + 1] - is_166_[i_170_][i_169_ - 1]);
				int i_174_ = (int) Math.sqrt((double) (i_172_ * i_172_ + i_167_ * 512 + i_173_ * i_173_));
				int i_175_ = (i_172_ << 8) / i_174_;
				int i_176_ = i_167_ * -512 / i_174_;
				int i_177_ = (i_173_ << 8) / i_174_;
				i_171_ += (aHa_Sub2_5102.anInt4093 * i_175_ + aHa_Sub2_5102.anInt4109 * i_176_
						+ aHa_Sub2_5102.anInt4091 * i_177_) >> 17;
				i_171_ >>= 1;
				if (i_171_ < 2)
					i_171_ = 2;
				else if (i_171_ > 126)
					i_171_ = 126;
				aByteArrayArray5098[i_170_][i_169_] = (byte) i_171_;
			}
		}
		aByteArrayArray5101 = new byte[i_164_ + 1][i_165_ + 1];
	}

	public void YA() {
		aByteArrayArray5098 = null;
		aByteArrayArray5101 = null;
	}

	public void method3352(int i, int i_178_) {
		method3366(i, i_178_, 0);
	}

	public void U(int i, int i_179_, int[] is, int[] is_180_, int[] is_181_, int[] is_182_, int[] is_183_,
			int[] is_184_, int[] is_185_, int[] is_186_, int i_187_, int i_188_, int i_189_, boolean bool) {
		boolean bool_190_ = (anInt5103 & 0x20) == 0;
		if (aClass73ArrayArray5100 == null && !bool_190_) {
			aClass73ArrayArray5100 = new Class73[anInt2832][anInt2834];
			aClass323ArrayArray5097 = new Class323[anInt2832][anInt2834];
		} else if (aClass80ArrayArray5093 == null && bool_190_)
			aClass80ArrayArray5093 = new Class80[anInt2832][anInt2834];
		else if (aClass372ArrayArray5110 != null)
			throw new IllegalStateException();
		if (is != null && is.length != 0) {
			for (int i_191_ = 0; i_191_ < is_183_.length; i_191_++) {
				if (is_183_[i_191_] == -1)
					is_183_[i_191_] = 0;
				else
					is_183_[i_191_] = (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1.method3576(is_183_[i_191_],
							(byte) -90) & 0xffff)]) << 8 | 0xff;
			}
			if (is_184_ != null) {
				for (int i_192_ = 0; i_192_ < is_184_.length; i_192_++) {
					if (is_184_[i_192_] == -1)
						is_184_[i_192_] = 0;
					else
						is_184_[i_192_] = (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1
								.method3576(is_184_[i_192_], (byte) -124) & 0xffff)]) << 8 | 0xff;
				}
			}
			if (bool_190_) {
				Class80 class80 = new Class80();
				class80.aShort895 = (short) is.length;
				class80.aShort898 = (short) (is.length / 3);
				class80.aShortArray897 = new short[class80.aShort895];
				class80.aShortArray892 = new short[class80.aShort895];
				class80.aShortArray891 = new short[class80.aShort895];
				class80.anIntArray889 = new int[class80.aShort895];
				class80.aShortArray893 = new short[class80.aShort895];
				class80.aShortArray896 = new short[class80.aShort895];
				class80.aByteArray899 = new byte[class80.aShort895];
				if (is_182_ != null)
					class80.aShortArray894 = new short[class80.aShort895];
				for (int i_193_ = 0; i_193_ < class80.aShort895; i_193_++) {
					int i_194_ = is[i_193_];
					int i_195_ = is_181_[i_193_];
					boolean bool_196_ = false;
					int i_197_;
					if (i_194_ == 0 && i_195_ == 0)
						i_197_ = (aByteArrayArray5098[i][i_179_] - aByteArrayArray5101[i][i_179_]);
					else if (i_194_ == 0 && i_195_ == anInt2836)
						i_197_ = (aByteArrayArray5098[i][i_179_ + 1] - aByteArrayArray5101[i][i_179_ + 1]);
					else if (i_194_ == anInt2836 && i_195_ == anInt2836)
						i_197_ = (aByteArrayArray5098[i + 1][i_179_ + 1] - aByteArrayArray5101[i + 1][i_179_ + 1]);
					else if (i_194_ == anInt2836 && i_195_ == 0)
						i_197_ = (aByteArrayArray5098[i + 1][i_179_] - aByteArrayArray5101[i + 1][i_179_]);
					else {
						int i_198_ = (((aByteArrayArray5098[i][i_179_] - aByteArrayArray5101[i][i_179_])
								* (anInt2836 - i_194_))
								+ ((aByteArrayArray5098[i + 1][i_179_] - aByteArrayArray5101[i + 1][i_179_]) * i_194_));
						int i_199_ = (((aByteArrayArray5098[i][i_179_ + 1] - aByteArrayArray5101[i][i_179_ + 1])
								* (anInt2836 - i_194_))
								+ ((aByteArrayArray5098[i + 1][i_179_ + 1] - aByteArrayArray5101[i + 1][i_179_ + 1])
										* i_194_));
						i_197_ = (i_198_ * (anInt2836 - i_195_) + i_199_ * i_195_ >> anInt2835 * 2);
					}
					int i_200_ = (i << anInt2835) + i_194_;
					int i_201_ = (i_179_ << anInt2835) + i_195_;
					class80.aShortArray897[i_193_] = (short) i_194_;
					class80.aShortArray891[i_193_] = (short) i_195_;
					class80.aShortArray892[i_193_] = (short) (this.method3349(0, i_201_, i_200_)
							+ (is_180_ != null ? is_180_[i_193_] : 0));
					if (i_197_ < 0)
						i_197_ = 0;
					if (is_183_[i_193_] == 0) {
						class80.anIntArray889[i_193_] = 0;
						if (is_184_ != null)
							class80.aByteArray899[i_193_] = (byte) i_197_;
					} else {
						int i_202_ = 0;
						if (is_182_ != null) {
							int i_203_ = (class80.aShortArray894[i_193_] = (short) is_182_[i_193_]);
							if (i_188_ != 0) {
								i_202_ = i_203_ * 255 / i_188_;
								if (i_202_ < 0)
									i_202_ = 0;
								else if (i_202_ > 255)
									i_202_ = 255;
							}
						}
						int i_204_ = -16777216;
						if (is_185_[i_193_] != -1
								&& method3363(aHa_Sub2_5102.aD1299.method14(is_185_[i_193_], -9412).aByte1774))
							i_204_ = -1694498816;
						class80.anIntArray889[i_193_] = (i_204_ | BITConfigsLoader.method2102(i_202_, 115,
								method3359(((is_183_[i_193_]) >> 8), i_197_), i_187_));
						if (is_184_ != null)
							class80.aByteArray899[i_193_] = (byte) i_197_;
					}
					class80.aShortArray893[i_193_] = (short) is_185_[i_193_];
					class80.aShortArray896[i_193_] = (short) is_186_[i_193_];
				}
				if (is_184_ != null)
					class80.anIntArray890 = new int[class80.aShort898];
				for (int i_205_ = 0; i_205_ < class80.aShort898; i_205_++) {
					int i_206_ = i_205_ * 3;
					if (is_184_ != null && is_184_[i_206_] != 0)
						class80.anIntArray890[i_205_] = is_184_[i_206_] >> 8 | ~0xffffff;
				}
				aClass80ArrayArray5093[i][i_179_] = class80;
			} else {
				boolean bool_207_ = true;
				int i_208_ = -1;
				int i_209_ = -1;
				int i_210_ = -1;
				int i_211_ = -1;
				if (is.length == 6) {
					for (int i_212_ = 0; i_212_ < 6; i_212_++) {
						if (is[i_212_] == 0 && is_181_[i_212_] == 0) {
							if (i_208_ != -1 && is_183_[i_208_] != is_183_[i_212_]) {
								bool_207_ = false;
								break;
							}
							i_208_ = i_212_;
						} else if (is[i_212_] == anInt2836 && is_181_[i_212_] == 0) {
							if (i_209_ != -1 && is_183_[i_209_] != is_183_[i_212_]) {
								bool_207_ = false;
								break;
							}
							i_209_ = i_212_;
						} else if (is[i_212_] == anInt2836 && is_181_[i_212_] == anInt2836) {
							if (i_210_ != -1 && is_183_[i_210_] != is_183_[i_212_]) {
								bool_207_ = false;
								break;
							}
							i_210_ = i_212_;
						} else if (is[i_212_] == 0 && is_181_[i_212_] == anInt2836) {
							if (i_211_ != -1 && is_183_[i_211_] != is_183_[i_212_]) {
								bool_207_ = false;
								break;
							}
							i_211_ = i_212_;
						}
					}
					if (i_208_ == -1 || i_209_ == -1 || i_210_ == -1 || i_211_ == -1)
						bool_207_ = false;
					if (bool_207_) {
						if (is_180_ != null) {
							for (int i_213_ = 0; i_213_ < 4; i_213_++) {
								if (is_180_[i_213_] != 0) {
									bool_207_ = false;
									break;
								}
							}
						}
						if (bool_207_) {
							for (int i_214_ = 1; i_214_ < 4; i_214_++) {
								if (is[i_214_] != is[0] && is[i_214_] != is[0] + anInt2836
										&& is[i_214_] != is[0] - anInt2836) {
									bool_207_ = false;
									break;
								}
								if (is_181_[i_214_] != is_181_[0] && (is_181_[i_214_] != is_181_[0] + anInt2836)
										&& (is_181_[i_214_] != is_181_[0] - anInt2836)) {
									bool_207_ = false;
									break;
								}
							}
						}
					}
				} else
					bool_207_ = false;
				if (bool_207_) {
					Class73 class73 = new Class73();
					int i_215_ = is_183_[0];
					int i_216_ = is_185_[0];
					if (is_184_ != null) {
						class73.anInt852 = is_184_[0] >> 8;
						if (i_215_ == 0)
							class73.aByte853 |= 0x2;
					} else if (i_215_ == 0)
						return;
					if ((anIntArrayArray2831[i][i_179_] == anIntArrayArray2831[i + 1][i_179_])
							&& (anIntArrayArray2831[i][i_179_] == anIntArrayArray2831[i + 1][i_179_ + 1])
							&& (anIntArrayArray2831[i][i_179_] == anIntArrayArray2831[i][i_179_ + 1]))
						class73.aByte853 |= 0x1;
					if (i_216_ != -1 && (class73.aByte853 & 0x2) == 0
							&& !(aHa_Sub2_5102.aD1299.method14(i_216_, -9412).aBoolean1792)) {
						int i_217_;
						if (is_182_ != null && i_188_ != 0) {
							i_217_ = is_182_[i_208_] * 255 / i_188_;
							if (i_217_ < 0)
								i_217_ = 0;
							else if (i_217_ > 255)
								i_217_ = 255;
						} else
							i_217_ = 0;
						class73.anInt854 = (BITConfigsLoader.method2102(i_217_, 110, method3359(is_183_[i_208_] >> 8,
								(aByteArrayArray5098[i][i_179_] - aByteArrayArray5101[i][i_179_])), i_187_));
						if (class73.anInt852 != 0)
							class73.anInt854 |= (255
									- (aByteArrayArray5098[i][i_179_] - aByteArrayArray5101[i][i_179_]) << 25);
						if (is_182_ != null && i_188_ != 0) {
							i_217_ = is_182_[i_209_] * 255 / i_188_;
							if (i_217_ < 0)
								i_217_ = 0;
							else if (i_217_ > 255)
								i_217_ = 255;
						} else
							i_217_ = 0;
						class73.anInt850 = (BITConfigsLoader.method2102(i_217_, 120,
								method3359(is_183_[i_209_] >> 8,
										(aByteArrayArray5098[i + 1][i_179_] - (aByteArrayArray5101[i + 1][i_179_]))),
								i_187_));
						if (class73.anInt852 != 0)
							class73.anInt850 |= 255
									- (aByteArrayArray5098[i + 1][i_179_] - (aByteArrayArray5101[i + 1][i_179_])) << 25;
						if (is_182_ != null && i_188_ != 0) {
							i_217_ = is_182_[i_210_] * 255 / i_188_;
							if (i_217_ < 0)
								i_217_ = 0;
							else if (i_217_ > 255)
								i_217_ = 255;
						} else
							i_217_ = 0;
						class73.anInt849 = (BITConfigsLoader.method2102(i_217_, -60, method3359(is_183_[i_210_] >> 8,
								((aByteArrayArray5098[i + 1][i_179_ + 1]) - (aByteArrayArray5101[i + 1][i_179_ + 1]))),
								i_187_));
						if (class73.anInt852 != 0)
							class73.anInt849 |= (255 - (aByteArrayArray5098[i + 1][i_179_ + 1]
									- (aByteArrayArray5101[i + 1][i_179_ + 1]))) << 25;
						if (is_182_ != null && i_188_ != 0) {
							i_217_ = is_182_[i_211_] * 255 / i_188_;
							if (i_217_ < 0)
								i_217_ = 0;
							else if (i_217_ > 255)
								i_217_ = 255;
						} else
							i_217_ = 0;
						class73.anInt855 = (BITConfigsLoader.method2102(i_217_, 126,
								method3359(is_183_[i_211_] >> 8,
										(aByteArrayArray5098[i][i_179_ + 1] - (aByteArrayArray5101[i][i_179_ + 1]))),
								i_187_));
						class73.aShort848 = (short) i_216_;
					} else {
						int i_218_;
						if (is_182_ != null && i_188_ != 0) {
							i_218_ = is_182_[i_208_] * 255 / i_188_;
							if (i_218_ < 0)
								i_218_ = 0;
							else if (i_218_ > 255)
								i_218_ = 255;
						} else
							i_218_ = 0;
						class73.anInt854 = (BITConfigsLoader.method2102(i_218_, 123, method3359(is_183_[i_208_] >> 8,
								(aByteArrayArray5098[i][i_179_] - aByteArrayArray5101[i][i_179_])), i_187_));
						if (class73.anInt852 != 0)
							class73.anInt854 |= (255
									- (aByteArrayArray5098[i][i_179_] - aByteArrayArray5101[i][i_179_]) << 25);
						if (is_182_ != null && i_188_ != 0) {
							i_218_ = is_182_[i_209_] * 255 / i_188_;
							if (i_218_ < 0)
								i_218_ = 0;
							else if (i_218_ > 255)
								i_218_ = 255;
						} else
							i_218_ = 0;
						class73.anInt850 = (BITConfigsLoader.method2102(i_218_, -97,
								method3359(is_183_[i_209_] >> 8,
										(aByteArrayArray5098[i + 1][i_179_] - (aByteArrayArray5101[i + 1][i_179_]))),
								i_187_));
						if (class73.anInt852 != 0)
							class73.anInt850 |= 255
									- (aByteArrayArray5098[i + 1][i_179_] - (aByteArrayArray5101[i + 1][i_179_])) << 25;
						if (is_182_ != null && i_188_ != 0) {
							i_218_ = is_182_[i_210_] * 255 / i_188_;
							if (i_218_ < 0)
								i_218_ = 0;
							else if (i_218_ > 255)
								i_218_ = 255;
						} else
							i_218_ = 0;
						class73.anInt849 = (BITConfigsLoader.method2102(i_218_, 117, method3359(is_183_[i_210_] >> 8,
								((aByteArrayArray5098[i + 1][i_179_ + 1]) - (aByteArrayArray5101[i + 1][i_179_ + 1]))),
								i_187_));
						if (class73.anInt852 != 0)
							class73.anInt849 |= (255 - (aByteArrayArray5098[i + 1][i_179_ + 1]
									- (aByteArrayArray5101[i + 1][i_179_ + 1]))) << 25;
						if (is_182_ != null && i_188_ != 0) {
							i_218_ = is_182_[i_211_] * 255 / i_188_;
							if (i_218_ < 0)
								i_218_ = 0;
							else if (i_218_ > 255)
								i_218_ = 255;
						} else
							i_218_ = 0;
						class73.anInt855 = (BITConfigsLoader.method2102(i_218_, 116,
								method3359(is_183_[i_211_] >> 8,
										(aByteArrayArray5098[i][i_179_ + 1] - (aByteArrayArray5101[i][i_179_ + 1]))),
								i_187_));
						if (class73.anInt852 != 0)
							class73.anInt855 |= 255
									- (aByteArrayArray5098[i][i_179_ + 1] - (aByteArrayArray5101[i][i_179_ + 1])) << 25;
						class73.aShort848 = (short) -1;
					}
					if (is_182_ != null) {
						class73.aShort856 = (short) is_182_[i_210_];
						class73.aShort851 = (short) is_182_[i_211_];
						class73.aShort846 = (short) is_182_[i_209_];
						class73.aShort847 = (short) is_182_[i_208_];
					}
					aClass73ArrayArray5100[i][i_179_] = class73;
				} else {
					Class323 class323 = new Class323();
					class323.aShort2845 = (short) is.length;
					class323.aShort2844 = (short) (is.length / 3);
					class323.aShortArray2846 = new short[class323.aShort2845];
					class323.aShortArray2839 = new short[class323.aShort2845];
					class323.aShortArray2842 = new short[class323.aShort2845];
					class323.anIntArray2840 = new int[class323.aShort2845];
					if (is_182_ != null)
						class323.aShortArray2847 = new short[class323.aShort2845];
					for (int i_219_ = 0; i_219_ < class323.aShort2845; i_219_++) {
						int i_220_ = is[i_219_];
						int i_221_ = is_181_[i_219_];
						boolean bool_222_ = false;
						int i_223_;
						if (i_220_ == 0 && i_221_ == 0)
							i_223_ = (aByteArrayArray5098[i][i_179_] - aByteArrayArray5101[i][i_179_]);
						else if (i_220_ == 0 && i_221_ == anInt2836)
							i_223_ = (aByteArrayArray5098[i][i_179_ + 1] - aByteArrayArray5101[i][i_179_ + 1]);
						else if (i_220_ == anInt2836 && i_221_ == anInt2836)
							i_223_ = (aByteArrayArray5098[i + 1][i_179_ + 1] - aByteArrayArray5101[i + 1][i_179_ + 1]);
						else if (i_220_ == anInt2836 && i_221_ == 0)
							i_223_ = (aByteArrayArray5098[i + 1][i_179_] - aByteArrayArray5101[i + 1][i_179_]);
						else {
							int i_224_ = (((aByteArrayArray5098[i][i_179_] - aByteArrayArray5101[i][i_179_])
									* (anInt2836 - i_220_))
									+ ((aByteArrayArray5098[i + 1][i_179_] - aByteArrayArray5101[i + 1][i_179_])
											* i_220_));
							int i_225_ = (((aByteArrayArray5098[i][i_179_ + 1] - aByteArrayArray5101[i][i_179_ + 1])
									* (anInt2836 - i_220_))
									+ (aByteArrayArray5098[i + 1][i_179_ + 1]
											- (aByteArrayArray5101[i + 1][i_179_ + 1])) * i_220_);
							i_223_ = (i_224_ * (anInt2836 - i_221_) + i_225_ * i_221_) >> anInt2835 * 2;
						}
						int i_226_ = (i << anInt2835) + i_220_;
						int i_227_ = (i_179_ << anInt2835) + i_221_;
						class323.aShortArray2846[i_219_] = (short) i_220_;
						class323.aShortArray2842[i_219_] = (short) i_221_;
						class323.aShortArray2839[i_219_] = (short) (this.method3349(0, i_227_, i_226_)
								+ (is_180_ != null ? is_180_[i_219_] : 0));
						if (i_223_ < 0)
							i_223_ = 0;
						if (is_183_[i_219_] == 0) {
							if (is_184_ != null)
								class323.anIntArray2840[i_219_] = i_223_ << 25;
							else
								class323.anIntArray2840[i_219_] = 0;
						} else {
							int i_228_ = 0;
							if (is_182_ != null) {
								int i_229_ = (class323.aShortArray2847[i_219_] = (short) is_182_[i_219_]);
								if (i_188_ != 0) {
									i_228_ = i_229_ * 255 / i_188_;
									if (i_228_ < 0)
										i_228_ = 0;
									else if (i_228_ > 255)
										i_228_ = 255;
								}
							}
							class323.anIntArray2840[i_219_] = BITConfigsLoader.method2102(i_228_, 109,
									method3359(((is_183_[i_219_]) >> 8), i_223_), i_187_);
							if (is_184_ != null)
								class323.anIntArray2840[i_219_] |= i_223_ << 25;
						}
					}
					boolean bool_230_ = false;
					for (int i_231_ = 0; i_231_ < class323.aShort2844; i_231_++) {
						if (is_185_[i_231_ * 3] != -1
								&& !(aHa_Sub2_5102.aD1299.method14(is_185_[i_231_ * 3], -9412).aBoolean1792))
							bool_230_ = true;
					}
					if (is_184_ != null)
						class323.anIntArray2838 = new int[class323.aShort2844];
					if (bool_230_) {
						class323.aShortArray2841 = new short[class323.aShort2844];
						class323.aShortArray2843 = new short[class323.aShort2844];
					}
					for (int i_232_ = 0; i_232_ < class323.aShort2844; i_232_++) {
						int i_233_ = i_232_ * 3;
						if (is_184_ != null && is_184_[i_233_] != 0)
							class323.anIntArray2838[i_232_] = is_184_[i_233_] >> 8;
						if (bool_230_) {
							int i_234_ = i_233_ + 1;
							int i_235_ = i_234_ + 1;
							boolean bool_236_ = false;
							boolean bool_237_ = true;
							int i_238_ = is_185_[i_233_];
							if (i_238_ == -1 || (aHa_Sub2_5102.aD1299.method14(i_238_, -9412).aBoolean1792))
								bool_237_ = false;
							else
								bool_236_ = true;
							i_238_ = is_185_[i_234_];
							if (i_238_ == -1 || (aHa_Sub2_5102.aD1299.method14(i_238_, -9412).aBoolean1792))
								bool_237_ = false;
							else
								bool_236_ = true;
							i_238_ = is_185_[i_235_];
							if (i_238_ == -1 || (aHa_Sub2_5102.aD1299.method14(i_238_, -9412).aBoolean1792))
								bool_237_ = false;
							else
								bool_236_ = true;
							if (bool_237_) {
								class323.aShortArray2841[i_232_] = (short) i_238_;
								class323.aShortArray2843[i_232_] = (short) is_186_[i_233_];
							} else {
								if (bool_236_) {
									i_238_ = is_185_[i_233_];
									if (i_238_ != -1 && !(aHa_Sub2_5102.aD1299.method14(i_238_, -9412).aBoolean1792))
										class323.anIntArray2840[i_233_] = (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1
												.method3576(((aHa_Sub2_5102.aD1299.method14(i_238_, -9412).aShort1775)
														& 0xffff), (byte) -85))
												& 0xffff]);
									i_238_ = is_185_[i_234_];
									if (i_238_ != -1 && !(aHa_Sub2_5102.aD1299.method14(i_238_, -9412).aBoolean1792))
										class323.anIntArray2840[i_234_] = (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1
												.method3576(((aHa_Sub2_5102.aD1299.method14(i_238_, -9412).aShort1775)
														& 0xffff), (byte) -83))
												& 0xffff]);
									i_238_ = is_185_[i_235_];
									if (i_238_ != -1 && !(aHa_Sub2_5102.aD1299.method14(i_238_, -9412).aBoolean1792))
										class323.anIntArray2840[i_235_] = (Class295_Sub1.anIntArray3691[(Class338_Sub3_Sub4_Sub1
												.method3576(((aHa_Sub2_5102.aD1299.method14(i_238_, -9412).aShort1775)
														& 0xffff), (byte) -95))
												& 0xffff]);
								}
								class323.aShortArray2841[i_232_] = (short) -1;
							}
						}
					}
					aClass323ArrayArray5097[i][i_179_] = class323;
				}
			}
		}
	}

	public void method3357(Class296_Sub35 class296_sub35, int[] is) {
		/* empty */
	}

	private void method3364(int i, int i_239_, boolean bool, Class240 class240, Class11 class11, int[] is,
			int[] is_240_, int[] is_241_, int[] is_242_, int i_243_) {
		Class73 class73 = aClass73ArrayArray5100[i][i_239_];
		if (class73 != null) {
			if ((class73.aByte853 & 0x2) == 0) {
				if (i_243_ != 0) {
					if ((class73.aByte853 & 0x4) != 0) {
						if ((i_243_ & 0x1) != 0)
							return;
					} else if ((i_243_ & 0x2) != 0)
						return;
				}
				int i_244_ = i * anInt2836;
				int i_245_ = i_244_ + anInt2836;
				int i_246_ = i_239_ * anInt2836;
				int i_247_ = i_246_ + anInt2836;
				int i_248_ = 0;
				int i_249_ = 0;
				int i_250_ = 0;
				int i_251_ = 0;
				float f;
				float f_252_;
				float f_253_;
				float f_254_;
				int i_255_;
				int i_256_;
				int i_257_;
				int i_258_;
				int i_259_;
				int i_260_;
				int i_261_;
				int i_262_;
				if ((class73.aByte853 & 0x1) != 0 && !bool) {
					int i_263_ = anIntArrayArray2831[i][i_239_];
					float f_264_ = aFloat5089 * (float) i_263_;
					if (anInt5108 == -1) {
						f = aFloat5094 + (aFloat5104 * (float) i_244_ + f_264_ + aFloat5092 * (float) i_246_);
						if (f <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_252_ = aFloat5094 + (aFloat5104 * (float) i_245_ + f_264_ + aFloat5092 * (float) i_246_);
						if (f_252_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_253_ = aFloat5094 + (aFloat5104 * (float) i_245_ + f_264_ + aFloat5092 * (float) i_247_);
						if (f_253_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_254_ = aFloat5094 + (aFloat5104 * (float) i_244_ + f_264_ + aFloat5092 * (float) i_247_);
						if (f_254_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
					} else {
						f = aFloat5094 + (aFloat5104 * (float) i_244_ + f_264_ + aFloat5092 * (float) i_246_);
						f_252_ = aFloat5094 + (aFloat5104 * (float) i_245_ + f_264_ + aFloat5092 * (float) i_246_);
						f_253_ = aFloat5094 + (aFloat5104 * (float) i_245_ + f_264_ + aFloat5092 * (float) i_247_);
						f_254_ = aFloat5094 + (aFloat5104 * (float) i_244_ + f_264_ + aFloat5092 * (float) i_247_);
					}
					if (class240.aBoolean2259) {
						int i_265_ = (int) (f - (float) class240.anInt2267);
						if (i_265_ > 0) {
							i_248_ = i_265_;
							if (i_248_ > 255)
								i_248_ = 255;
						}
						i_265_ = (int) (f_252_ - (float) class240.anInt2267);
						if (i_265_ > 0) {
							i_249_ = i_265_;
							if (i_249_ > 255)
								i_249_ = 255;
						}
						i_265_ = (int) (f_253_ - (float) class240.anInt2267);
						if (i_265_ > 0) {
							i_250_ = i_265_;
							if (i_250_ > 255)
								i_250_ = 255;
						}
						i_265_ = (int) (f_254_ - (float) class240.anInt2267);
						if (i_265_ > 0) {
							i_251_ = i_265_;
							if (i_251_ > 255)
								i_251_ = 255;
						}
					}
					float f_266_ = aFloat5090 * (float) i_263_;
					float f_267_ = aFloat5107 * (float) i_263_;
					if (anInt5108 == -1) {
						float f_268_ = (aFloat5095
								+ (aFloat5106 * (float) i_244_ + f_266_ + aFloat5099 * (float) i_246_));
						i_255_ = (class11.anInt109 + (int) (f_268_ * (float) aHa_Sub2_5102.anInt4086 / f));
						float f_269_ = (aFloat5105
								+ (aFloat5096 * (float) i_244_ + f_267_ + aFloat5091 * (float) i_246_));
						i_256_ = (class11.anInt95 + (int) (f_269_ * (float) aHa_Sub2_5102.anInt4072 / f));
						float f_270_ = (aFloat5095
								+ (aFloat5106 * (float) i_245_ + f_266_ + aFloat5099 * (float) i_246_));
						i_257_ = (class11.anInt109 + (int) (f_270_ * (float) aHa_Sub2_5102.anInt4086 / f_252_));
						float f_271_ = (aFloat5105
								+ (aFloat5096 * (float) i_245_ + f_267_ + aFloat5091 * (float) i_246_));
						i_258_ = (class11.anInt95 + (int) (f_271_ * (float) aHa_Sub2_5102.anInt4072 / f_252_));
						float f_272_ = (aFloat5095
								+ (aFloat5106 * (float) i_245_ + f_266_ + aFloat5099 * (float) i_247_));
						i_259_ = (class11.anInt109 + (int) (f_272_ * (float) aHa_Sub2_5102.anInt4086 / f_253_));
						float f_273_ = (aFloat5105
								+ (aFloat5096 * (float) i_245_ + f_267_ + aFloat5091 * (float) i_247_));
						i_260_ = (class11.anInt95 + (int) (f_273_ * (float) aHa_Sub2_5102.anInt4072 / f_253_));
						float f_274_ = (aFloat5095
								+ (aFloat5106 * (float) i_244_ + f_266_ + aFloat5099 * (float) i_247_));
						i_261_ = (class11.anInt109 + (int) (f_274_ * (float) aHa_Sub2_5102.anInt4086 / f_254_));
						float f_275_ = (aFloat5105
								+ (aFloat5096 * (float) i_244_ + f_267_ + aFloat5091 * (float) i_247_));
						i_262_ = (class11.anInt95 + (int) (f_275_ * (float) aHa_Sub2_5102.anInt4072 / f_254_));
					} else {
						float f_276_ = (aFloat5095
								+ (aFloat5106 * (float) i_244_ + f_266_ + aFloat5099 * (float) i_246_));
						i_255_ = (class11.anInt109
								+ (int) (f_276_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_277_ = (aFloat5105
								+ (aFloat5096 * (float) i_244_ + f_267_ + aFloat5091 * (float) i_246_));
						i_256_ = (class11.anInt95
								+ (int) (f_277_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_278_ = (aFloat5095
								+ (aFloat5106 * (float) i_245_ + f_266_ + aFloat5099 * (float) i_246_));
						i_257_ = (class11.anInt109
								+ (int) (f_278_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_279_ = (aFloat5105
								+ (aFloat5096 * (float) i_245_ + f_267_ + aFloat5091 * (float) i_246_));
						i_258_ = (class11.anInt95
								+ (int) (f_279_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_280_ = (aFloat5095
								+ (aFloat5106 * (float) i_245_ + f_266_ + aFloat5099 * (float) i_247_));
						i_259_ = (class11.anInt109
								+ (int) (f_280_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_281_ = (aFloat5105
								+ (aFloat5096 * (float) i_245_ + f_267_ + aFloat5091 * (float) i_247_));
						i_260_ = (class11.anInt95
								+ (int) (f_281_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_282_ = (aFloat5095
								+ (aFloat5106 * (float) i_244_ + f_266_ + aFloat5099 * (float) i_247_));
						i_261_ = (class11.anInt109
								+ (int) (f_282_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_283_ = (aFloat5105
								+ (aFloat5096 * (float) i_244_ + f_267_ + aFloat5091 * (float) i_247_));
						i_262_ = (class11.anInt95
								+ (int) (f_283_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
					}
				} else {
					int i_284_ = anIntArrayArray2831[i][i_239_];
					int i_285_ = anIntArrayArray2831[i + 1][i_239_];
					int i_286_ = anIntArrayArray2831[i + 1][i_239_ + 1];
					int i_287_ = anIntArrayArray2831[i][i_239_ + 1];
					if (anInt5108 == -1) {
						f = aFloat5094 + (aFloat5104 * (float) i_244_ + aFloat5089 * (float) i_284_
								+ aFloat5092 * (float) i_246_);
						if (f <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_252_ = aFloat5094 + (aFloat5104 * (float) i_245_ + aFloat5089 * (float) i_285_
								+ aFloat5092 * (float) i_246_);
						if (f_252_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_253_ = aFloat5094 + (aFloat5104 * (float) i_245_ + aFloat5089 * (float) i_286_
								+ aFloat5092 * (float) i_247_);
						if (f_253_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_254_ = aFloat5094 + (aFloat5104 * (float) i_244_ + aFloat5089 * (float) i_287_
								+ aFloat5092 * (float) i_247_);
						if (f_254_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
					} else {
						f = aFloat5094 + (aFloat5104 * (float) i_244_ + aFloat5089 * (float) i_284_
								+ aFloat5092 * (float) i_246_);
						f_252_ = aFloat5094 + (aFloat5104 * (float) i_245_ + aFloat5089 * (float) i_285_
								+ aFloat5092 * (float) i_246_);
						f_253_ = aFloat5094 + (aFloat5104 * (float) i_245_ + aFloat5089 * (float) i_286_
								+ aFloat5092 * (float) i_247_);
						f_254_ = aFloat5094 + (aFloat5104 * (float) i_244_ + aFloat5089 * (float) i_287_
								+ aFloat5092 * (float) i_247_);
					}
					if (bool) {
						int i_288_ = (int) (f - (float) class240.anInt2267);
						if (i_288_ > 255)
							i_288_ = 255;
						if (i_288_ > 0) {
							i_248_ = i_288_;
							int i_289_ = class73.aShort847 * i_288_ / 255;
							if (i_289_ > 0)
								i_284_ -= i_289_;
						}
						i_288_ = (int) (f_252_ - (float) class240.anInt2267);
						if (i_288_ > 255)
							i_288_ = 255;
						if (i_288_ > 0) {
							i_249_ = i_288_;
							int i_290_ = class73.aShort846 * i_288_ / 255;
							if (i_290_ > 0)
								i_285_ -= i_290_;
						}
						i_288_ = (int) (f_253_ - (float) class240.anInt2267);
						if (i_288_ > 255)
							i_288_ = 255;
						if (i_288_ > 0) {
							i_250_ = i_288_;
							int i_291_ = class73.aShort856 * i_288_ / 255;
							if (i_291_ > 0)
								i_286_ -= i_291_;
						}
						i_288_ = (int) (f_254_ - (float) class240.anInt2267);
						if (i_288_ > 255)
							i_288_ = 255;
						if (i_288_ > 0) {
							i_251_ = i_288_;
							int i_292_ = class73.aShort851 * i_288_ / 255;
							if (i_292_ > 0)
								i_287_ -= i_292_;
						}
					} else if (class240.aBoolean2259) {
						int i_293_ = (int) (f - (float) class240.anInt2267);
						if (i_293_ > 0) {
							i_248_ = i_293_;
							if (i_248_ > 255)
								i_248_ = 255;
						}
						i_293_ = (int) (f_252_ - (float) class240.anInt2267);
						if (i_293_ > 0) {
							i_249_ = i_293_;
							if (i_249_ > 255)
								i_249_ = 255;
						}
						i_293_ = (int) (f_253_ - (float) class240.anInt2267);
						if (i_293_ > 0) {
							i_250_ = i_293_;
							if (i_250_ > 255)
								i_250_ = 255;
						}
						i_293_ = (int) (f_254_ - (float) class240.anInt2267);
						if (i_293_ > 0) {
							i_251_ = i_293_;
							if (i_251_ > 255)
								i_251_ = 255;
						}
					}
					if (anInt5108 == -1) {
						float f_294_ = aFloat5095 + (aFloat5106 * (float) i_244_ + aFloat5090 * (float) i_284_
								+ aFloat5099 * (float) i_246_);
						i_255_ = (class11.anInt109 + (int) (f_294_ * (float) aHa_Sub2_5102.anInt4086 / f));
						float f_295_ = aFloat5105 + (aFloat5096 * (float) i_244_ + aFloat5107 * (float) i_284_
								+ aFloat5091 * (float) i_246_);
						i_256_ = (class11.anInt95 + (int) (f_295_ * (float) aHa_Sub2_5102.anInt4072 / f));
						float f_296_ = aFloat5095 + (aFloat5106 * (float) i_245_ + aFloat5090 * (float) i_285_
								+ aFloat5099 * (float) i_246_);
						i_257_ = (class11.anInt109 + (int) (f_296_ * (float) aHa_Sub2_5102.anInt4086 / f_252_));
						float f_297_ = aFloat5105 + (aFloat5096 * (float) i_245_ + aFloat5107 * (float) i_285_
								+ aFloat5091 * (float) i_246_);
						i_258_ = (class11.anInt95 + (int) (f_297_ * (float) aHa_Sub2_5102.anInt4072 / f_252_));
						float f_298_ = aFloat5095 + (aFloat5106 * (float) i_245_ + aFloat5090 * (float) i_286_
								+ aFloat5099 * (float) i_247_);
						i_259_ = (class11.anInt109 + (int) (f_298_ * (float) aHa_Sub2_5102.anInt4086 / f_253_));
						float f_299_ = aFloat5105 + (aFloat5096 * (float) i_245_ + aFloat5107 * (float) i_286_
								+ aFloat5091 * (float) i_247_);
						i_260_ = (class11.anInt95 + (int) (f_299_ * (float) aHa_Sub2_5102.anInt4072 / f_253_));
						float f_300_ = aFloat5095 + (aFloat5106 * (float) i_244_ + aFloat5090 * (float) i_287_
								+ aFloat5099 * (float) i_247_);
						i_261_ = (class11.anInt109 + (int) (f_300_ * (float) aHa_Sub2_5102.anInt4086 / f_254_));
						float f_301_ = aFloat5105 + (aFloat5096 * (float) i_244_ + aFloat5107 * (float) i_287_
								+ aFloat5091 * (float) i_247_);
						i_262_ = (class11.anInt95 + (int) (f_301_ * (float) aHa_Sub2_5102.anInt4072 / f_254_));
					} else {
						float f_302_ = aFloat5095 + (aFloat5106 * (float) i_244_ + aFloat5090 * (float) i_284_
								+ aFloat5099 * (float) i_246_);
						i_255_ = (class11.anInt109
								+ (int) (f_302_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_303_ = aFloat5105 + (aFloat5096 * (float) i_244_ + aFloat5107 * (float) i_284_
								+ aFloat5091 * (float) i_246_);
						i_256_ = (class11.anInt95
								+ (int) (f_303_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_304_ = aFloat5095 + (aFloat5106 * (float) i_245_ + aFloat5090 * (float) i_285_
								+ aFloat5099 * (float) i_246_);
						i_257_ = (class11.anInt109
								+ (int) (f_304_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_305_ = aFloat5105 + (aFloat5096 * (float) i_245_ + aFloat5107 * (float) i_285_
								+ aFloat5091 * (float) i_246_);
						i_258_ = (class11.anInt95
								+ (int) (f_305_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_306_ = aFloat5095 + (aFloat5106 * (float) i_245_ + aFloat5090 * (float) i_286_
								+ aFloat5099 * (float) i_247_);
						i_259_ = (class11.anInt109
								+ (int) (f_306_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_307_ = aFloat5105 + (aFloat5096 * (float) i_245_ + aFloat5107 * (float) i_286_
								+ aFloat5091 * (float) i_247_);
						i_260_ = (class11.anInt95
								+ (int) (f_307_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_308_ = aFloat5095 + (aFloat5106 * (float) i_244_ + aFloat5090 * (float) i_287_
								+ aFloat5099 * (float) i_247_);
						i_261_ = (class11.anInt109
								+ (int) (f_308_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_309_ = aFloat5105 + (aFloat5096 * (float) i_244_ + aFloat5107 * (float) i_287_
								+ aFloat5091 * (float) i_247_);
						i_262_ = (class11.anInt95
								+ (int) (f_309_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
					}
				}
				boolean bool_310_ = (class73.aShort848 != -1
						&& method3363(aHa_Sub2_5102.aD1299.method14(class73.aShort848, -9412).aByte1774));
				if (anInt5108 == -1) {
					int i_311_ = i_249_ + i_250_ + i_251_;
					if (((i_259_ - i_261_) * (i_258_ - i_262_) - (i_260_ - i_262_) * (i_257_ - i_261_)) > 0) {
						class11.aBoolean103 = (i_259_ < 0 || i_261_ < 0 || i_257_ < 0 || i_259_ > class11.anInt107
								|| i_261_ > class11.anInt107 || i_257_ > class11.anInt107);
						if (i_311_ < 765) {
							if (i_311_ > 0) {
								if (class73.aShort848 >= 0) {
									int i_312_ = -16777216;
									if (bool_310_)
										i_312_ = -1694498816;
									class11.method214((float) i_260_, (float) i_262_, (float) i_258_, (float) i_259_,
											(float) i_261_, (float) i_257_, f_253_, f_254_, f_252_, 1.0F, 0.0F, 1.0F,
											1.0F, 1.0F, 0.0F, i_312_ | class73.anInt849 & 0xffffff,
											i_312_ | class73.anInt855 & 0xffffff, i_312_ | class73.anInt850 & 0xffffff,
											class240.anInt2265, i_250_, i_251_, i_249_, class73.aShort848);
								} else {
									if (bool_310_)
										class11.anInt108 = 100;
									class11.method199((float) i_260_, (float) i_262_, (float) i_258_, (float) i_259_,
											(float) i_261_, (float) i_257_, (float) (int) f_253_, (float) (int) f_254_,
											(float) (int) f_252_,
											(Class69_Sub4.method745(class73.anInt849, 16711680,
													i_250_ << 24 | class240.anInt2265)),
											(Class69_Sub4.method745(class73.anInt855, 16711680,
													i_251_ << 24 | class240.anInt2265)),
											(Class69_Sub4.method745(class73.anInt850, 16711680,
													(i_249_ << 24 | class240.anInt2265))));
									class11.anInt108 = 0;
								}
							} else if (class73.aShort848 >= 0) {
								int i_313_ = -16777216;
								if (bool_310_)
									i_313_ = -1694498816;
								class11.method214((float) i_260_, (float) i_262_, (float) i_258_, (float) i_259_,
										(float) i_261_, (float) i_257_, f_253_, f_254_, f_252_, 1.0F, 0.0F, 1.0F, 1.0F,
										1.0F, 0.0F, i_313_ | class73.anInt849 & 0xffffff,
										i_313_ | class73.anInt855 & 0xffffff, i_313_ | class73.anInt850 & 0xffffff, 0,
										0, 0, 0, class73.aShort848);
							} else {
								if (bool_310_)
									class11.anInt108 = 100;
								class11.method199((float) i_260_, (float) i_262_, (float) i_258_, (float) i_259_,
										(float) i_261_, (float) i_257_, (float) (int) f_253_, (float) (int) f_254_,
										(float) (int) f_252_, class73.anInt849, class73.anInt855, class73.anInt850);
								class11.anInt108 = 0;
							}
						} else
							class11.method206((float) i_260_, (float) i_262_, (float) i_258_, (float) i_259_,
									(float) i_261_, (float) i_257_, (float) (int) f_253_, (float) (int) f_254_,
									(float) (int) f_252_, class240.anInt2265);
					}
					i_311_ = i_248_ + i_249_ + i_251_;
					if (((i_255_ - i_257_) * (i_262_ - i_258_) - (i_256_ - i_258_) * (i_261_ - i_257_)) > 0) {
						class11.aBoolean103 = (i_255_ < 0 || i_257_ < 0 || i_261_ < 0 || i_255_ > class11.anInt107
								|| i_257_ > class11.anInt107 || i_261_ > class11.anInt107);
						if (i_311_ < 765) {
							if (bool_310_)
								class11.anInt108 = -1694498816;
							if (i_311_ > 0) {
								if (class73.aShort848 >= 0) {
									int i_314_ = -16777216;
									if (bool_310_)
										i_314_ = -1694498816;
									class11.method214((float) i_256_, (float) i_258_, (float) i_262_, (float) i_255_,
											(float) i_257_, (float) i_261_, f, f_252_, f_254_, 0.0F, 1.0F, 1.0F, 0.0F,
											0.0F, 1.0F, i_314_ | class73.anInt854 & 0xffffff,
											i_314_ | class73.anInt850 & 0xffffff, i_314_ | class73.anInt855 & 0xffffff,
											class240.anInt2265, i_248_, i_249_, i_251_, class73.aShort848);
								} else {
									if (bool_310_)
										class11.anInt108 = 100;
									class11.method199((float) i_256_, (float) i_258_, (float) i_262_, (float) i_255_,
											(float) i_257_, (float) i_261_, (float) (int) f, (float) (int) f_252_,
											(float) (int) f_254_,
											(Class69_Sub4.method745(class73.anInt854, 16711680,
													i_248_ << 24 | class240.anInt2265)),
											(Class69_Sub4.method745(class73.anInt850, 16711680,
													i_249_ << 24 | class240.anInt2265)),
											(Class69_Sub4.method745(class73.anInt855, 16711680,
													(i_251_ << 24 | class240.anInt2265))));
									class11.anInt108 = 0;
								}
							} else if (class73.aShort848 >= 0) {
								int i_315_ = -16777216;
								if (bool_310_)
									i_315_ = -1694498816;
								class11.method214((float) i_256_, (float) i_258_, (float) i_262_, (float) i_255_,
										(float) i_257_, (float) i_261_, f, f_252_, f_254_, 0.0F, 1.0F, 1.0F, 0.0F, 0.0F,
										1.0F, i_315_ | class73.anInt854 & 0xffffff,
										i_315_ | class73.anInt850 & 0xffffff, i_315_ | class73.anInt855 & 0xffffff, 0,
										0, 0, 0, class73.aShort848);
							} else {
								if (bool_310_)
									class11.anInt108 = 100;
								class11.method199((float) i_256_, (float) i_258_, (float) i_262_, (float) i_255_,
										(float) i_257_, (float) i_261_, (float) (int) f, (float) (int) f_252_,
										(float) (int) f_254_, class73.anInt854, class73.anInt850, class73.anInt855);
								class11.anInt108 = 0;
							}
						} else
							class11.method206((float) i_256_, (float) i_258_, (float) i_262_, (float) i_255_,
									(float) i_257_, (float) i_261_, (float) (int) f, (float) (int) f_252_,
									(float) (int) f_254_, class240.anInt2265);
					}
				} else {
					int i_316_ = i_249_ + i_250_ + i_251_;
					if (((i_259_ - i_261_) * (i_258_ - i_262_) - (i_260_ - i_262_) * (i_257_ - i_261_)) > 0) {
						class11.aBoolean103 = (i_259_ < 0 || i_261_ < 0 || i_257_ < 0 || i_259_ > class11.anInt107
								|| i_261_ > class11.anInt107 || i_257_ > class11.anInt107);
						if (i_316_ < 765) {
							if (bool_310_)
								class11.anInt108 = -1694498816;
							if (i_316_ > 0) {
								if (class73.aShort848 >= 0) {
									int i_317_ = -16777216;
									if (bool_310_)
										i_317_ = -1694498816;
									class11.method214((float) i_260_, (float) i_262_, (float) i_258_, (float) i_259_,
											(float) i_261_, (float) i_257_, f_253_, f_254_, f_252_, 1.0F, 0.0F, 1.0F,
											1.0F, 1.0F, 0.0F, i_317_ | class73.anInt849 & 0xffffff,
											i_317_ | class73.anInt855 & 0xffffff, i_317_ | class73.anInt850 & 0xffffff,
											class240.anInt2265, i_250_, i_251_, i_249_, class73.aShort848);
								} else {
									if (bool_310_)
										class11.anInt108 = 100;
									class11.method199((float) i_260_, (float) i_262_, (float) i_258_, (float) i_259_,
											(float) i_261_, (float) i_257_, (float) (int) f_253_, (float) (int) f_254_,
											(float) (int) f_252_,
											(Class69_Sub4.method745(class73.anInt849, 16711680,
													i_250_ << 24 | class240.anInt2265)),
											(Class69_Sub4.method745(class73.anInt855, 16711680,
													i_251_ << 24 | class240.anInt2265)),
											(Class69_Sub4.method745(class73.anInt850, 16711680,
													(i_249_ << 24 | class240.anInt2265))));
									class11.anInt108 = 0;
								}
							} else if (class73.aShort848 >= 0) {
								int i_318_ = -16777216;
								if (bool_310_)
									i_318_ = -1694498816;
								class11.method214((float) i_260_, (float) i_262_, (float) i_258_, (float) i_259_,
										(float) i_261_, (float) i_257_, f_253_, f_254_, f_252_, 1.0F, 0.0F, 1.0F, 1.0F,
										1.0F, 0.0F, i_318_ | class73.anInt849 & 0xffffff,
										i_318_ | class73.anInt855 & 0xffffff, i_318_ | class73.anInt850 & 0xffffff, 0,
										0, 0, 0, class73.aShort848);
							} else {
								if (bool_310_)
									class11.anInt108 = 100;
								class11.method199((float) i_260_, (float) i_262_, (float) i_258_, (float) i_259_,
										(float) i_261_, (float) i_257_, (float) (int) f_253_, (float) (int) f_254_,
										(float) (int) f_252_, class73.anInt849, class73.anInt855, class73.anInt850);
								class11.anInt108 = 0;
							}
						} else
							class11.method206((float) i_260_, (float) i_262_, (float) i_258_, (float) i_259_,
									(float) i_261_, (float) i_257_, (float) (int) f_253_, (float) (int) f_254_,
									(float) (int) f_252_, class240.anInt2265);
					}
					i_316_ = i_248_ + i_249_ + i_251_;
					if (((i_255_ - i_257_) * (i_262_ - i_258_) - (i_256_ - i_258_) * (i_261_ - i_257_)) > 0) {
						class11.aBoolean103 = (i_255_ < 0 || i_257_ < 0 || i_261_ < 0 || i_255_ > class11.anInt107
								|| i_257_ > class11.anInt107 || i_261_ > class11.anInt107);
						if (i_316_ < 765) {
							if (bool_310_)
								class11.anInt108 = -1694498816;
							if (i_316_ > 0) {
								if (class73.aShort848 >= 0) {
									int i_319_ = -16777216;
									if (bool_310_)
										i_319_ = -1694498816;
									class11.method214((float) i_256_, (float) i_258_, (float) i_262_, (float) i_255_,
											(float) i_257_, (float) i_261_, f, f_252_, f_254_, 0.0F, 1.0F, 1.0F, 0.0F,
											0.0F, 1.0F, i_319_ | class73.anInt854 & 0xffffff,
											i_319_ | class73.anInt850 & 0xffffff, i_319_ | class73.anInt855 & 0xffffff,
											class240.anInt2265, i_248_, i_249_, i_251_, class73.aShort848);
								} else {
									if (bool_310_)
										class11.anInt108 = 100;
									class11.method199((float) i_256_, (float) i_258_, (float) i_262_, (float) i_255_,
											(float) i_257_, (float) i_261_, (float) (int) f, (float) (int) f_252_,
											(float) (int) f_254_,
											(Class69_Sub4.method745(class73.anInt854, 16711680,
													i_248_ << 24 | class240.anInt2265)),
											(Class69_Sub4.method745(class73.anInt850, 16711680,
													i_249_ << 24 | class240.anInt2265)),
											(Class69_Sub4.method745(class73.anInt855, 16711680,
													(i_251_ << 24 | class240.anInt2265))));
									class11.anInt108 = 0;
								}
							} else if (class73.aShort848 >= 0) {
								int i_320_ = -16777216;
								if (bool_310_)
									i_320_ = -1694498816;
								class11.method214((float) i_256_, (float) i_258_, (float) i_262_, (float) i_255_,
										(float) i_257_, (float) i_261_, f, f_252_, f_254_, 0.0F, 1.0F, 1.0F, 0.0F, 0.0F,
										1.0F, i_320_ | class73.anInt854 & 0xffffff,
										i_320_ | class73.anInt850 & 0xffffff, i_320_ | class73.anInt855 & 0xffffff, 0,
										0, 0, 0, class73.aShort848);
							} else {
								if (bool_310_)
									class11.anInt108 = 100;
								class11.method199((float) i_256_, (float) i_258_, (float) i_262_, (float) i_255_,
										(float) i_257_, (float) i_261_, (float) (int) f, (float) (int) f_252_,
										(float) (int) f_254_, class73.anInt854, class73.anInt850, class73.anInt855);
								class11.anInt108 = 0;
							}
						} else
							class11.method206((float) i_256_, (float) i_258_, (float) i_262_, (float) i_255_,
									(float) i_257_, (float) i_261_, (float) (int) f, (float) (int) f_252_,
									(float) (int) f_254_, class240.anInt2265);
					}
				}
			}
		} else {
			Class323 class323 = aClass323ArrayArray5097[i][i_239_];
			if (class323 != null) {
				if (i_243_ != 0) {
					if ((class323.aByte2848 & 0x4) != 0) {
						if ((i_243_ & 0x1) != 0)
							return;
					} else if ((i_243_ & 0x2) != 0)
						return;
				}
				if (anInt5108 == -1) {
					for (int i_321_ = 0; i_321_ < class323.aShort2845; i_321_++) {
						int i_322_ = (class323.aShortArray2846[i_321_] + (i << anInt2835));
						int i_323_ = class323.aShortArray2839[i_321_];
						int i_324_ = (class323.aShortArray2842[i_321_] + (i_239_ << anInt2835));
						float f = aFloat5094 + (aFloat5104 * (float) i_322_ + aFloat5089 * (float) i_323_
								+ aFloat5092 * (float) i_324_);
						if (f <= (float) aHa_Sub2_5102.anInt4107)
							return;
						is_242_[i_321_] = 0;
						if (bool) {
							int i_325_ = (int) (f - (float) class240.anInt2267);
							if (i_325_ > 255)
								i_325_ = 255;
							if (i_325_ > 0) {
								is_242_[i_321_] = i_325_;
								int i_326_ = (class323.aShortArray2847[i_321_] * i_325_ / 255);
								if (i_326_ > 0)
									i_323_ -= i_326_;
							}
						} else if (class240.aBoolean2259) {
							int i_327_ = (int) (f - (float) class240.anInt2267);
							if (i_327_ > 0) {
								is_242_[i_321_] = i_327_;
								if (is_242_[i_321_] > 255)
									is_242_[i_321_] = 255;
							}
						}
						float f_328_ = aFloat5095 + (aFloat5106 * (float) i_322_ + aFloat5090 * (float) i_323_
								+ aFloat5099 * (float) i_324_);
						float f_329_ = aFloat5105 + (aFloat5096 * (float) i_322_ + aFloat5107 * (float) i_323_
								+ aFloat5091 * (float) i_324_);
						is[i_321_] = (class11.anInt109 + (int) (f_328_ * (float) aHa_Sub2_5102.anInt4086 / f));
						is_240_[i_321_] = (class11.anInt95 + (int) (f_329_ * (float) aHa_Sub2_5102.anInt4072 / f));
						is_241_[i_321_] = (int) f;
					}
				} else {
					for (int i_330_ = 0; i_330_ < class323.aShort2845; i_330_++) {
						int i_331_ = (class323.aShortArray2846[i_330_] + (i << anInt2835));
						int i_332_ = class323.aShortArray2839[i_330_];
						int i_333_ = (class323.aShortArray2842[i_330_] + (i_239_ << anInt2835));
						float f = aFloat5094 + (aFloat5104 * (float) i_331_ + aFloat5089 * (float) i_332_
								+ aFloat5092 * (float) i_333_);
						is_242_[i_330_] = 0;
						if (bool) {
							int i_334_ = anInt5108 - class240.anInt2267;
							if (i_334_ > 255)
								i_334_ = 255;
							if (i_334_ > 0) {
								is_242_[i_330_] = i_334_;
								int i_335_ = (class323.aShortArray2847[i_330_] * i_334_ / 255);
								if (i_335_ > 0)
									i_332_ -= i_335_;
							}
						} else if (class240.aBoolean2259) {
							int i_336_ = anInt5108 - class240.anInt2267;
							if (i_336_ > 0) {
								is_242_[i_330_] = i_336_;
								if (is_242_[i_330_] > 255)
									is_242_[i_330_] = 255;
							}
						}
						float f_337_ = aFloat5095 + (aFloat5106 * (float) i_331_ + aFloat5090 * (float) i_332_
								+ aFloat5099 * (float) i_333_);
						float f_338_ = aFloat5105 + (aFloat5096 * (float) i_331_ + aFloat5107 * (float) i_332_
								+ aFloat5091 * (float) i_333_);
						is[i_330_] = (class11.anInt109
								+ (int) (f_337_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						is_240_[i_330_] = (class11.anInt95
								+ (int) (f_338_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						is_241_[i_330_] = (int) f;
					}
				}
				if (class323.aShortArray2841 != null) {
					if (anInt5108 == -1) {
						for (int i_339_ = 0; i_339_ < class323.aShort2844; i_339_++) {
							int i_340_ = i_339_ * 3;
							int i_341_ = i_340_ + 1;
							int i_342_ = i_341_ + 1;
							int i_343_ = is[i_340_];
							int i_344_ = is[i_341_];
							int i_345_ = is[i_342_];
							int i_346_ = is_240_[i_340_];
							int i_347_ = is_240_[i_341_];
							int i_348_ = is_240_[i_342_];
							int i_349_ = (is_242_[i_340_] + is_242_[i_341_] + is_242_[i_342_]);
							if (((i_343_ - i_344_) * (i_348_ - i_347_) - (i_346_ - i_347_) * (i_345_ - i_344_)) > 0) {
								class11.aBoolean103 = (i_343_ < 0 || i_344_ < 0 || i_345_ < 0
										|| i_343_ > class11.anInt107 || i_344_ > class11.anInt107
										|| i_345_ > class11.anInt107);
								short i_350_ = class323.aShortArray2841[i_339_];
								if (i_349_ < 765) {
									if (i_349_ > 0) {
										if (i_350_ != -1) {
											int i_351_ = -16777216;
											if (i_350_ != -1 && method3363(
													aHa_Sub2_5102.aD1299.method14(i_350_, -9412).aByte1774))
												i_351_ = -1694498816;
											class11.method214((float) i_346_, (float) i_347_, (float) i_348_,
													(float) i_343_, (float) i_344_, (float) i_345_,
													(float) is_241_[i_340_], (float) is_241_[i_341_],
													(float) is_241_[i_342_],
													((float) (class323.aShortArray2846[i_340_]) / (float) anInt2836),
													((float) (class323.aShortArray2846[i_341_]) / (float) anInt2836),
													((float) (class323.aShortArray2846[i_342_]) / (float) anInt2836),
													((float) (class323.aShortArray2842[i_340_]) / (float) anInt2836),
													((float) (class323.aShortArray2842[i_341_]) / (float) anInt2836),
													((float) (class323.aShortArray2842[i_342_]) / (float) anInt2836),
													(i_351_ | (class323.anIntArray2840[i_340_]) & 0xffffff),
													(i_351_ | (class323.anIntArray2840[i_341_]) & 0xffffff),
													(i_351_ | (class323.anIntArray2840[i_342_]) & 0xffffff),
													class240.anInt2265, is_242_[i_340_], is_242_[i_341_],
													is_242_[i_342_], i_350_);
										} else if (((class323.anIntArray2840[i_340_]) & 0xffffff) != 0) {
											if (i_350_ != -1 && method3363(
													aHa_Sub2_5102.aD1299.method14(i_350_, -9412).aByte1774))
												class11.anInt108 = -1694498816;
											class11.method199((float) i_346_, (float) i_347_, (float) i_348_,
													(float) i_343_, (float) i_344_, (float) i_345_,
													(float) is_241_[i_340_], (float) is_241_[i_341_],
													(float) is_241_[i_342_],
													(Class69_Sub4.method745((class323.anIntArray2840[i_340_]), 16711680,
															(is_242_[i_340_] << 24 | class240.anInt2265))),
													(Class69_Sub4.method745((class323.anIntArray2840[i_341_]), 16711680,
															(is_242_[i_341_] << 24 | class240.anInt2265))),
													(Class69_Sub4.method745((class323.anIntArray2840[i_342_]), 16711680,
															(is_242_[i_342_] << 24 | class240.anInt2265))));
											class11.anInt108 = 0;
										}
									} else if (i_350_ != -1) {
										int i_352_ = -16777216;
										if (i_350_ != -1
												&& method3363(aHa_Sub2_5102.aD1299.method14(i_350_, -9412).aByte1774))
											i_352_ = -1694498816;
										class11.method214((float) i_346_, (float) i_347_, (float) i_348_,
												(float) i_343_, (float) i_344_, (float) i_345_, (float) is_241_[i_340_],
												(float) is_241_[i_341_], (float) is_241_[i_342_],
												((float) (class323.aShortArray2846[i_340_]) / (float) anInt2836),
												((float) (class323.aShortArray2846[i_341_]) / (float) anInt2836),
												((float) (class323.aShortArray2846[i_342_]) / (float) anInt2836),
												((float) (class323.aShortArray2842[i_340_]) / (float) anInt2836),
												((float) (class323.aShortArray2842[i_341_]) / (float) anInt2836),
												((float) (class323.aShortArray2842[i_342_]) / (float) anInt2836),
												i_352_ | (class323.anIntArray2840[i_340_]) & 0xffffff,
												i_352_ | (class323.anIntArray2840[i_341_]) & 0xffffff,
												i_352_ | (class323.anIntArray2840[i_342_]) & 0xffffff, 0, 0, 0, 0,
												i_350_);
									} else if ((class323.anIntArray2840[i_340_] & 0xffffff) != 0) {
										if (i_350_ != -1
												&& method3363(aHa_Sub2_5102.aD1299.method14(i_350_, -9412).aByte1774))
											class11.anInt108 = -1694498816;
										class11.method199((float) i_346_, (float) i_347_, (float) i_348_,
												(float) i_343_, (float) i_344_, (float) i_345_, (float) is_241_[i_340_],
												(float) is_241_[i_341_], (float) is_241_[i_342_],
												class323.anIntArray2840[i_340_], class323.anIntArray2840[i_341_],
												class323.anIntArray2840[i_342_]);
										class11.anInt108 = 0;
									}
								} else
									class11.method206((float) i_346_, (float) i_347_, (float) i_348_, (float) i_343_,
											(float) i_344_, (float) i_345_, (float) is_241_[i_340_],
											(float) is_241_[i_341_], (float) is_241_[i_342_], class240.anInt2265);
							}
						}
					} else {
						for (int i_353_ = 0; i_353_ < class323.aShort2844; i_353_++) {
							int i_354_ = i_353_ * 3;
							int i_355_ = i_354_ + 1;
							int i_356_ = i_355_ + 1;
							int i_357_ = is[i_354_];
							int i_358_ = is[i_355_];
							int i_359_ = is[i_356_];
							int i_360_ = is_240_[i_354_];
							int i_361_ = is_240_[i_355_];
							int i_362_ = is_240_[i_356_];
							int i_363_ = (is_242_[i_354_] + is_242_[i_355_] + is_242_[i_356_]);
							if (((i_357_ - i_358_) * (i_362_ - i_361_) - (i_360_ - i_361_) * (i_359_ - i_358_)) > 0) {
								class11.aBoolean103 = (i_357_ < 0 || i_358_ < 0 || i_359_ < 0
										|| i_357_ > class11.anInt107 || i_358_ > class11.anInt107
										|| i_359_ > class11.anInt107);
								short i_364_ = class323.aShortArray2841[i_353_];
								if (i_363_ < 765) {
									if (i_364_ != -1
											&& method3363(aHa_Sub2_5102.aD1299.method14(i_364_, -9412).aByte1774))
										class11.anInt108 = -1694498816;
									if (i_363_ > 0) {
										if (i_364_ != -1) {
											int i_365_ = -16777216;
											if (i_364_ != -1 && method3363(
													aHa_Sub2_5102.aD1299.method14(i_364_, -9412).aByte1774))
												i_365_ = -1694498816;
											class11.method214((float) i_360_, (float) i_361_, (float) i_362_,
													(float) i_357_, (float) i_358_, (float) i_359_,
													(float) is_241_[i_354_], (float) is_241_[i_355_],
													(float) is_241_[i_356_],
													((float) (class323.aShortArray2846[i_354_]) / (float) anInt2836),
													((float) (class323.aShortArray2846[i_355_]) / (float) anInt2836),
													((float) (class323.aShortArray2846[i_356_]) / (float) anInt2836),
													((float) (class323.aShortArray2842[i_354_]) / (float) anInt2836),
													((float) (class323.aShortArray2842[i_355_]) / (float) anInt2836),
													((float) (class323.aShortArray2842[i_356_]) / (float) anInt2836),
													(i_365_ | (class323.anIntArray2840[i_354_]) & 0xffffff),
													(i_365_ | (class323.anIntArray2840[i_355_]) & 0xffffff),
													(i_365_ | (class323.anIntArray2840[i_356_]) & 0xffffff),
													class240.anInt2265, is_242_[i_354_], is_242_[i_355_],
													is_242_[i_356_], i_364_);
										} else if (((class323.anIntArray2840[i_354_]) & 0xffffff) != 0) {
											if (i_364_ != -1 && method3363(
													aHa_Sub2_5102.aD1299.method14(i_364_, -9412).aByte1774))
												class11.anInt108 = -1694498816;
											class11.method199((float) i_360_, (float) i_361_, (float) i_362_,
													(float) i_357_, (float) i_358_, (float) i_359_,
													(float) is_241_[i_354_], (float) is_241_[i_355_],
													(float) is_241_[i_356_],
													(Class69_Sub4.method745((class323.anIntArray2840[i_354_]), 16711680,
															(is_242_[i_354_] << 24 | class240.anInt2265))),
													(Class69_Sub4.method745((class323.anIntArray2840[i_355_]), 16711680,
															(is_242_[i_355_] << 24 | class240.anInt2265))),
													(Class69_Sub4.method745((class323.anIntArray2840[i_356_]), 16711680,
															(is_242_[i_356_] << 24 | class240.anInt2265))));
											class11.anInt108 = 0;
										}
									} else if (i_364_ != -1) {
										int i_366_ = -16777216;
										if (i_364_ != -1
												&& method3363(aHa_Sub2_5102.aD1299.method14(i_364_, -9412).aByte1774))
											i_366_ = -1694498816;
										class11.method214((float) i_360_, (float) i_361_, (float) i_362_,
												(float) i_357_, (float) i_358_, (float) i_359_, (float) is_241_[i_354_],
												(float) is_241_[i_355_], (float) is_241_[i_356_],
												((float) (class323.aShortArray2846[i_354_]) / (float) anInt2836),
												((float) (class323.aShortArray2846[i_355_]) / (float) anInt2836),
												((float) (class323.aShortArray2846[i_356_]) / (float) anInt2836),
												((float) (class323.aShortArray2842[i_354_]) / (float) anInt2836),
												((float) (class323.aShortArray2842[i_355_]) / (float) anInt2836),
												((float) (class323.aShortArray2842[i_356_]) / (float) anInt2836),
												i_366_ | (class323.anIntArray2840[i_354_]) & 0xffffff,
												i_366_ | (class323.anIntArray2840[i_355_]) & 0xffffff,
												i_366_ | (class323.anIntArray2840[i_356_]) & 0xffffff, 0, 0, 0, 0,
												i_364_);
									} else if ((class323.anIntArray2840[i_354_] & 0xffffff) != 0) {
										if (i_364_ != -1
												&& method3363(aHa_Sub2_5102.aD1299.method14(i_364_, -9412).aByte1774))
											class11.anInt108 = -1694498816;
										class11.method199((float) i_360_, (float) i_361_, (float) i_362_,
												(float) i_357_, (float) i_358_, (float) i_359_, (float) is_241_[i_354_],
												(float) is_241_[i_355_], (float) is_241_[i_356_],
												class323.anIntArray2840[i_354_], class323.anIntArray2840[i_355_],
												class323.anIntArray2840[i_356_]);
										class11.anInt108 = 0;
									}
									class11.anInt108 = 0;
								} else
									class11.method206((float) i_360_, (float) i_361_, (float) i_362_, (float) i_357_,
											(float) i_358_, (float) i_359_, (float) is_241_[i_354_],
											(float) is_241_[i_355_], (float) is_241_[i_356_], class240.anInt2265);
							}
						}
					}
				} else {
					for (int i_367_ = 0; i_367_ < class323.aShort2844; i_367_++) {
						int i_368_ = i_367_ * 3;
						int i_369_ = i_368_ + 1;
						int i_370_ = i_369_ + 1;
						int i_371_ = is[i_368_];
						int i_372_ = is[i_369_];
						int i_373_ = is[i_370_];
						int i_374_ = is_240_[i_368_];
						int i_375_ = is_240_[i_369_];
						int i_376_ = is_240_[i_370_];
						int i_377_ = (is_242_[i_368_] + is_242_[i_369_] + is_242_[i_370_]);
						if (((i_371_ - i_372_) * (i_376_ - i_375_) - (i_374_ - i_375_) * (i_373_ - i_372_)) > 0) {
							class11.aBoolean103 = (i_371_ < 0 || i_372_ < 0 || i_373_ < 0 || i_371_ > class11.anInt107
									|| i_372_ > class11.anInt107 || i_373_ > class11.anInt107);
							if (i_377_ < 765) {
								if (i_377_ > 0) {
									if ((class323.anIntArray2840[i_368_] & 0xffffff) != 0)
										class11.method199((float) i_374_, (float) i_375_, (float) i_376_,
												(float) i_371_, (float) i_372_, (float) i_373_, (float) is_241_[i_368_],
												(float) is_241_[i_369_], (float) is_241_[i_370_],
												(BITConfigsLoader.method2102(is_242_[i_368_], -80,
														class323.anIntArray2840[i_368_], class240.anInt2265)),
												(BITConfigsLoader.method2102(is_242_[i_369_], 107,
														class323.anIntArray2840[i_369_], class240.anInt2265)),
												(BITConfigsLoader.method2102(is_242_[i_370_], 122,
														class323.anIntArray2840[i_370_], class240.anInt2265)));
								} else if ((class323.anIntArray2840[i_368_] & 0xffffff) != 0)
									class11.method199((float) i_374_, (float) i_375_, (float) i_376_, (float) i_371_,
											(float) i_372_, (float) i_373_, (float) is_241_[i_368_],
											(float) is_241_[i_369_], (float) is_241_[i_370_],
											class323.anIntArray2840[i_368_], class323.anIntArray2840[i_369_],
											class323.anIntArray2840[i_370_]);
							} else
								class11.method206((float) i_374_, (float) i_375_, (float) i_376_, (float) i_371_,
										(float) i_372_, (float) i_373_, (float) is_241_[i_368_],
										(float) is_241_[i_369_], (float) is_241_[i_370_], class240.anInt2265);
						}
					}
				}
			}
		}
	}

	public void method3348(int i, int i_378_, int i_379_, boolean[][] bools, boolean bool, int i_380_, int i_381_) {
		Class373_Sub3 class373_sub3 = aHa_Sub2_5102.aClass373_Sub3_4090;
		anInt5108 = i_380_;
		aFloat5106 = class373_sub3.aFloat5606;
		aFloat5090 = class373_sub3.aFloat5613;
		aFloat5099 = class373_sub3.aFloat5610;
		aFloat5095 = class373_sub3.aFloat5611;
		aFloat5096 = class373_sub3.aFloat5607;
		aFloat5107 = class373_sub3.aFloat5615;
		aFloat5091 = class373_sub3.aFloat5616;
		aFloat5105 = class373_sub3.aFloat5619;
		aFloat5104 = class373_sub3.aFloat5614;
		aFloat5089 = class373_sub3.aFloat5617;
		aFloat5092 = class373_sub3.aFloat5604;
		aFloat5094 = class373_sub3.aFloat5618;
		for (int i_382_ = 0; i_382_ < i_379_ + i_379_; i_382_++) {
			for (int i_383_ = 0; i_383_ < i_379_ + i_379_; i_383_++) {
				if (bools[i_382_][i_383_]) {
					int i_384_ = i - i_379_ + i_382_;
					int i_385_ = i_378_ - i_379_ + i_383_;
					if (i_384_ >= 0 && i_384_ < anInt2832 && i_385_ >= 0 && i_385_ < anInt2834)
						method3366(i_384_, i_385_, i_381_);
				}
			}
		}
	}

	private void method3365(int i, int i_386_, Class11 class11, int[] is, int[] is_387_, int[] is_388_, int[] is_389_,
			int i_390_) {
		Class372 class372 = aClass372ArrayArray5110[i][i_386_];
		if (class372 != null) {
			if ((class372.aByte3173 & 0x2) == 0) {
				if (i_390_ != 0) {
					if ((class372.aByte3173 & 0x4) != 0) {
						if ((i_390_ & 0x1) != 0)
							return;
					} else if ((i_390_ & 0x2) != 0)
						return;
				}
				int i_391_ = i * anInt2836;
				int i_392_ = i_391_ + anInt2836;
				int i_393_ = i_386_ * anInt2836;
				int i_394_ = i_393_ + anInt2836;
				float f;
				float f_395_;
				float f_396_;
				float f_397_;
				int i_398_;
				int i_399_;
				int i_400_;
				int i_401_;
				int i_402_;
				int i_403_;
				int i_404_;
				int i_405_;
				if ((class372.aByte3173 & 0x1) != 0) {
					int i_406_ = anIntArrayArray2831[i][i_386_];
					float f_407_ = aFloat5089 * (float) i_406_;
					if (anInt5108 == -1) {
						f = aFloat5094 + (aFloat5104 * (float) i_391_ + f_407_ + aFloat5092 * (float) i_393_);
						if (f <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_395_ = aFloat5094 + (aFloat5104 * (float) i_392_ + f_407_ + aFloat5092 * (float) i_393_);
						if (f_395_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_396_ = aFloat5094 + (aFloat5104 * (float) i_392_ + f_407_ + aFloat5092 * (float) i_394_);
						if (f_396_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_397_ = aFloat5094 + (aFloat5104 * (float) i_391_ + f_407_ + aFloat5092 * (float) i_394_);
						if (f_397_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
					} else {
						f = aFloat5094 + (aFloat5104 * (float) i_391_ + f_407_ + aFloat5092 * (float) i_393_);
						f_395_ = aFloat5094 + (aFloat5104 * (float) i_392_ + f_407_ + aFloat5092 * (float) i_393_);
						f_396_ = aFloat5094 + (aFloat5104 * (float) i_392_ + f_407_ + aFloat5092 * (float) i_394_);
						f_397_ = aFloat5094 + (aFloat5104 * (float) i_391_ + f_407_ + aFloat5092 * (float) i_394_);
					}
					float f_408_ = aFloat5090 * (float) i_406_;
					float f_409_ = aFloat5107 * (float) i_406_;
					if (anInt5108 == -1) {
						float f_410_ = (aFloat5095
								+ (aFloat5106 * (float) i_391_ + f_408_ + aFloat5099 * (float) i_393_));
						i_398_ = (class11.anInt109 + (int) (f_410_ * (float) aHa_Sub2_5102.anInt4086 / f));
						float f_411_ = (aFloat5105
								+ (aFloat5096 * (float) i_391_ + f_409_ + aFloat5091 * (float) i_393_));
						i_399_ = (class11.anInt95 + (int) (f_411_ * (float) aHa_Sub2_5102.anInt4072 / f));
						float f_412_ = (aFloat5095
								+ (aFloat5106 * (float) i_392_ + f_408_ + aFloat5099 * (float) i_393_));
						i_400_ = (class11.anInt109 + (int) (f_412_ * (float) aHa_Sub2_5102.anInt4086 / f_395_));
						float f_413_ = (aFloat5105
								+ (aFloat5096 * (float) i_392_ + f_409_ + aFloat5091 * (float) i_393_));
						i_401_ = (class11.anInt95 + (int) (f_413_ * (float) aHa_Sub2_5102.anInt4072 / f_395_));
						float f_414_ = (aFloat5095
								+ (aFloat5106 * (float) i_392_ + f_408_ + aFloat5099 * (float) i_394_));
						i_402_ = (class11.anInt109 + (int) (f_414_ * (float) aHa_Sub2_5102.anInt4086 / f_396_));
						float f_415_ = (aFloat5105
								+ (aFloat5096 * (float) i_392_ + f_409_ + aFloat5091 * (float) i_394_));
						i_403_ = (class11.anInt95 + (int) (f_415_ * (float) aHa_Sub2_5102.anInt4072 / f_396_));
						float f_416_ = (aFloat5095
								+ (aFloat5106 * (float) i_391_ + f_408_ + aFloat5099 * (float) i_394_));
						i_404_ = (class11.anInt109 + (int) (f_416_ * (float) aHa_Sub2_5102.anInt4086 / f_397_));
						float f_417_ = (aFloat5105
								+ (aFloat5096 * (float) i_391_ + f_409_ + aFloat5091 * (float) i_394_));
						i_405_ = (class11.anInt95 + (int) (f_417_ * (float) aHa_Sub2_5102.anInt4072 / f_397_));
					} else {
						float f_418_ = (aFloat5095
								+ (aFloat5106 * (float) i_391_ + f_408_ + aFloat5099 * (float) i_393_));
						i_398_ = (class11.anInt109
								+ (int) (f_418_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_419_ = (aFloat5105
								+ (aFloat5096 * (float) i_391_ + f_409_ + aFloat5091 * (float) i_393_));
						i_399_ = (class11.anInt95
								+ (int) (f_419_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_420_ = (aFloat5095
								+ (aFloat5106 * (float) i_392_ + f_408_ + aFloat5099 * (float) i_393_));
						i_400_ = (class11.anInt109
								+ (int) (f_420_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_421_ = (aFloat5105
								+ (aFloat5096 * (float) i_392_ + f_409_ + aFloat5091 * (float) i_393_));
						i_401_ = (class11.anInt95
								+ (int) (f_421_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_422_ = (aFloat5095
								+ (aFloat5106 * (float) i_392_ + f_408_ + aFloat5099 * (float) i_394_));
						i_402_ = (class11.anInt109
								+ (int) (f_422_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_423_ = (aFloat5105
								+ (aFloat5096 * (float) i_392_ + f_409_ + aFloat5091 * (float) i_394_));
						i_403_ = (class11.anInt95
								+ (int) (f_423_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_424_ = (aFloat5095
								+ (aFloat5106 * (float) i_391_ + f_408_ + aFloat5099 * (float) i_394_));
						i_404_ = (class11.anInt109
								+ (int) (f_424_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_425_ = (aFloat5105
								+ (aFloat5096 * (float) i_391_ + f_409_ + aFloat5091 * (float) i_394_));
						i_405_ = (class11.anInt95
								+ (int) (f_425_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
					}
				} else {
					int i_426_ = anIntArrayArray2831[i][i_386_];
					int i_427_ = anIntArrayArray2831[i + 1][i_386_];
					int i_428_ = anIntArrayArray2831[i + 1][i_386_ + 1];
					int i_429_ = anIntArrayArray2831[i][i_386_ + 1];
					if (anInt5108 == -1) {
						f = aFloat5094 + (aFloat5104 * (float) i_391_ + aFloat5089 * (float) i_426_
								+ aFloat5092 * (float) i_393_);
						if (f <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_395_ = aFloat5094 + (aFloat5104 * (float) i_392_ + aFloat5089 * (float) i_427_
								+ aFloat5092 * (float) i_393_);
						if (f_395_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_396_ = aFloat5094 + (aFloat5104 * (float) i_392_ + aFloat5089 * (float) i_428_
								+ aFloat5092 * (float) i_394_);
						if (f_396_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
						f_397_ = aFloat5094 + (aFloat5104 * (float) i_391_ + aFloat5089 * (float) i_429_
								+ aFloat5092 * (float) i_394_);
						if (f_397_ <= (float) aHa_Sub2_5102.anInt4107)
							return;
						float f_430_ = aFloat5095 + (aFloat5106 * (float) i_391_ + aFloat5090 * (float) i_426_
								+ aFloat5099 * (float) i_393_);
						i_398_ = (class11.anInt109 + (int) (f_430_ * (float) aHa_Sub2_5102.anInt4086 / f));
						float f_431_ = aFloat5105 + (aFloat5096 * (float) i_391_ + aFloat5107 * (float) i_426_
								+ aFloat5091 * (float) i_393_);
						i_399_ = (class11.anInt95 + (int) (f_431_ * (float) aHa_Sub2_5102.anInt4072 / f));
						float f_432_ = aFloat5095 + (aFloat5106 * (float) i_392_ + aFloat5090 * (float) i_427_
								+ aFloat5099 * (float) i_393_);
						i_400_ = (class11.anInt109 + (int) (f_432_ * (float) aHa_Sub2_5102.anInt4086 / f_395_));
						float f_433_ = aFloat5105 + (aFloat5096 * (float) i_392_ + aFloat5107 * (float) i_427_
								+ aFloat5091 * (float) i_393_);
						i_401_ = (class11.anInt95 + (int) (f_433_ * (float) aHa_Sub2_5102.anInt4072 / f_395_));
						float f_434_ = aFloat5095 + (aFloat5106 * (float) i_392_ + aFloat5090 * (float) i_428_
								+ aFloat5099 * (float) i_394_);
						i_402_ = (class11.anInt109 + (int) (f_434_ * (float) aHa_Sub2_5102.anInt4086 / f_396_));
						float f_435_ = aFloat5105 + (aFloat5096 * (float) i_392_ + aFloat5107 * (float) i_428_
								+ aFloat5091 * (float) i_394_);
						i_403_ = (class11.anInt95 + (int) (f_435_ * (float) aHa_Sub2_5102.anInt4072 / f_396_));
						float f_436_ = aFloat5095 + (aFloat5106 * (float) i_391_ + aFloat5090 * (float) i_429_
								+ aFloat5099 * (float) i_394_);
						i_404_ = (class11.anInt109 + (int) (f_436_ * (float) aHa_Sub2_5102.anInt4086 / f_397_));
						float f_437_ = aFloat5105 + (aFloat5096 * (float) i_391_ + aFloat5107 * (float) i_429_
								+ aFloat5091 * (float) i_394_);
						i_405_ = (class11.anInt95 + (int) (f_437_ * (float) aHa_Sub2_5102.anInt4072 / f_397_));
					} else {
						f = aFloat5094 + (aFloat5104 * (float) i_391_ + aFloat5089 * (float) i_426_
								+ aFloat5092 * (float) i_393_);
						f_395_ = aFloat5094 + (aFloat5104 * (float) i_392_ + aFloat5089 * (float) i_427_
								+ aFloat5092 * (float) i_393_);
						f_396_ = aFloat5094 + (aFloat5104 * (float) i_392_ + aFloat5089 * (float) i_428_
								+ aFloat5092 * (float) i_394_);
						f_397_ = aFloat5094 + (aFloat5104 * (float) i_391_ + aFloat5089 * (float) i_429_
								+ aFloat5092 * (float) i_394_);
						float f_438_ = aFloat5095 + (aFloat5106 * (float) i_391_ + aFloat5090 * (float) i_426_
								+ aFloat5099 * (float) i_393_);
						i_398_ = (class11.anInt109
								+ (int) (f_438_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_439_ = aFloat5105 + (aFloat5096 * (float) i_391_ + aFloat5107 * (float) i_426_
								+ aFloat5091 * (float) i_393_);
						i_399_ = (class11.anInt95
								+ (int) (f_439_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_440_ = aFloat5095 + (aFloat5106 * (float) i_392_ + aFloat5090 * (float) i_427_
								+ aFloat5099 * (float) i_393_);
						i_400_ = (class11.anInt109
								+ (int) (f_440_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_441_ = aFloat5105 + (aFloat5096 * (float) i_392_ + aFloat5107 * (float) i_427_
								+ aFloat5091 * (float) i_393_);
						i_401_ = (class11.anInt95
								+ (int) (f_441_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_442_ = aFloat5095 + (aFloat5106 * (float) i_392_ + aFloat5090 * (float) i_428_
								+ aFloat5099 * (float) i_394_);
						i_402_ = (class11.anInt109
								+ (int) (f_442_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_443_ = aFloat5105 + (aFloat5096 * (float) i_392_ + aFloat5107 * (float) i_428_
								+ aFloat5091 * (float) i_394_);
						i_403_ = (class11.anInt95
								+ (int) (f_443_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						float f_444_ = aFloat5095 + (aFloat5106 * (float) i_391_ + aFloat5090 * (float) i_429_
								+ aFloat5099 * (float) i_394_);
						i_404_ = (class11.anInt109
								+ (int) (f_444_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						float f_445_ = aFloat5105 + (aFloat5096 * (float) i_391_ + aFloat5107 * (float) i_429_
								+ aFloat5091 * (float) i_394_);
						i_405_ = (class11.anInt95
								+ (int) (f_445_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
					}
				}
				if (anInt5108 == -1) {
					if (((i_402_ - i_404_) * (i_401_ - i_405_) - (i_403_ - i_405_) * (i_400_ - i_404_)) > 0) {
						class11.aBoolean103 = (i_402_ < 0 || i_404_ < 0 || i_400_ < 0 || i_402_ > class11.anInt107
								|| i_404_ > class11.anInt107 || i_400_ > class11.anInt107);
						if (class372.aShort3170 >= 0)
							class11.method214((float) i_403_, (float) i_405_, (float) i_401_, (float) i_402_,
									(float) i_404_, (float) i_400_, f_396_, f_397_, f_395_, 1.0F, 0.0F, 1.0F, 1.0F,
									1.0F, 0.0F,
									((Class295_Sub1.anIntArray3691[class372.aShort3172 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									((Class295_Sub1.anIntArray3691[class372.aShort3175 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									((Class295_Sub1.anIntArray3691[class372.aShort3171 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									0, 0, 0, 0, class372.aShort3170);
						else
							class11.method205((float) i_403_, (float) i_405_, (float) i_401_, (float) i_402_,
									(float) i_404_, (float) i_400_, (float) (int) f_396_, (float) (int) f_397_,
									(float) (int) f_395_, (float) (class372.aShort3172 & 0xffff),
									(float) (class372.aShort3175 & 0xffff), (float) (class372.aShort3171 & 0xffff));
					}
					if (((i_398_ - i_400_) * (i_405_ - i_401_) - (i_399_ - i_401_) * (i_404_ - i_400_)) > 0) {
						class11.aBoolean103 = (i_398_ < 0 || i_400_ < 0 || i_404_ < 0 || i_398_ > class11.anInt107
								|| i_400_ > class11.anInt107 || i_404_ > class11.anInt107);
						if (class372.aShort3170 >= 0)
							class11.method214((float) i_399_, (float) i_401_, (float) i_405_, (float) i_398_,
									(float) i_400_, (float) i_404_, f, f_395_, f_397_, 0.0F, 1.0F, 1.0F, 0.0F, 0.0F,
									1.0F,
									((Class295_Sub1.anIntArray3691[class372.aShort3176 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									((Class295_Sub1.anIntArray3691[class372.aShort3171 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									((Class295_Sub1.anIntArray3691[class372.aShort3175 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									0, 0, 0, 0, class372.aShort3170);
						else
							class11.method205((float) i_399_, (float) i_401_, (float) i_405_, (float) i_398_,
									(float) i_400_, (float) i_404_, (float) (int) f, (float) (int) f_395_,
									(float) (int) f_397_, (float) (class372.aShort3176 & 0xffff),
									(float) (class372.aShort3171 & 0xffff), (float) (class372.aShort3175 & 0xffff));
					}
				} else {
					if (((i_402_ - i_404_) * (i_401_ - i_405_) - (i_403_ - i_405_) * (i_400_ - i_404_)) > 0) {
						class11.aBoolean103 = (i_402_ < 0 || i_404_ < 0 || i_400_ < 0 || i_402_ > class11.anInt107
								|| i_404_ > class11.anInt107 || i_400_ > class11.anInt107);
						if (class372.aShort3170 >= 0)
							class11.method214((float) i_403_, (float) i_405_, (float) i_401_, (float) i_402_,
									(float) i_404_, (float) i_400_, f_396_, f_397_, f_395_, 1.0F, 0.0F, 1.0F, 1.0F,
									1.0F, 0.0F,
									((Class295_Sub1.anIntArray3691[class372.aShort3172 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									((Class295_Sub1.anIntArray3691[class372.aShort3175 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									((Class295_Sub1.anIntArray3691[class372.aShort3171 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									0, 0, 0, 0, class372.aShort3170);
						else
							class11.method205((float) i_403_, (float) i_405_, (float) i_401_, (float) i_402_,
									(float) i_404_, (float) i_400_, (float) (int) f_396_, (float) (int) f_397_,
									(float) (int) f_395_, (float) (class372.aShort3172 & 0xffff),
									(float) (class372.aShort3175 & 0xffff), (float) (class372.aShort3171 & 0xffff));
					}
					if (((i_398_ - i_400_) * (i_405_ - i_401_) - (i_399_ - i_401_) * (i_404_ - i_400_)) > 0) {
						class11.aBoolean103 = (i_398_ < 0 || i_400_ < 0 || i_404_ < 0 || i_398_ > class11.anInt107
								|| i_400_ > class11.anInt107 || i_404_ > class11.anInt107);
						if (class372.aShort3170 >= 0)
							class11.method214((float) i_399_, (float) i_401_, (float) i_405_, (float) i_398_,
									(float) i_400_, (float) i_404_, f, f_395_, f_397_, 0.0F, 1.0F, 1.0F, 0.0F, 0.0F,
									1.0F,
									((Class295_Sub1.anIntArray3691[class372.aShort3176 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									((Class295_Sub1.anIntArray3691[class372.aShort3171 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									((Class295_Sub1.anIntArray3691[class372.aShort3175 & 0xffff]) & 0xffffff)
											| ~0xffffff,
									0, 0, 0, 0, class372.aShort3170);
						else
							class11.method205((float) i_399_, (float) i_401_, (float) i_405_, (float) i_398_,
									(float) i_400_, (float) i_404_, (float) (int) f, (float) (int) f_395_,
									(float) (int) f_397_, (float) (class372.aShort3176 & 0xffff),
									(float) (class372.aShort3171 & 0xffff), (float) (class372.aShort3175 & 0xffff));
					}
				}
			}
		} else {
			Class82 class82 = aClass82ArrayArray5109[i][i_386_];
			if (class82 != null) {
				if (i_390_ != 0) {
					if ((class82.aByte905 & 0x4) != 0) {
						if ((i_390_ & 0x1) != 0)
							return;
					} else if ((i_390_ & 0x2) != 0)
						return;
				}
				if (anInt5108 == -1) {
					for (int i_446_ = 0; i_446_ < class82.aShort902; i_446_++) {
						int i_447_ = (class82.aShortArray909[i_446_] + (i << anInt2835));
						short i_448_ = class82.aShortArray901[i_446_];
						int i_449_ = (class82.aShortArray908[i_446_] + (i_386_ << anInt2835));
						float f = aFloat5094 + (aFloat5104 * (float) i_447_ + aFloat5089 * (float) i_448_
								+ aFloat5092 * (float) i_449_);
						if (f <= (float) aHa_Sub2_5102.anInt4107)
							return;
						float f_450_ = aFloat5095 + (aFloat5106 * (float) i_447_ + aFloat5090 * (float) i_448_
								+ aFloat5099 * (float) i_449_);
						float f_451_ = aFloat5105 + (aFloat5096 * (float) i_447_ + aFloat5107 * (float) i_448_
								+ aFloat5091 * (float) i_449_);
						is[i_446_] = (class11.anInt109 + (int) (f_450_ * (float) aHa_Sub2_5102.anInt4086 / f));
						is_387_[i_446_] = (class11.anInt95 + (int) (f_451_ * (float) aHa_Sub2_5102.anInt4072 / f));
						is_388_[i_446_] = (int) f;
					}
				} else {
					for (int i_452_ = 0; i_452_ < class82.aShort902; i_452_++) {
						int i_453_ = (class82.aShortArray909[i_452_] + (i << anInt2835));
						short i_454_ = class82.aShortArray901[i_452_];
						int i_455_ = (class82.aShortArray908[i_452_] + (i_386_ << anInt2835));
						float f = aFloat5094 + (aFloat5104 * (float) i_453_ + aFloat5089 * (float) i_454_
								+ aFloat5092 * (float) i_455_);
						float f_456_ = aFloat5095 + (aFloat5106 * (float) i_453_ + aFloat5090 * (float) i_454_
								+ aFloat5099 * (float) i_455_);
						float f_457_ = aFloat5105 + (aFloat5096 * (float) i_453_ + aFloat5107 * (float) i_454_
								+ aFloat5091 * (float) i_455_);
						is[i_452_] = (class11.anInt109
								+ (int) (f_456_ * (float) aHa_Sub2_5102.anInt4086 / (float) anInt5108));
						is_387_[i_452_] = (class11.anInt95
								+ (int) (f_457_ * (float) aHa_Sub2_5102.anInt4072 / (float) anInt5108));
						is_388_[i_452_] = (int) f;
					}
				}
				if (class82.aShortArray913 != null) {
					if (anInt5108 == -1) {
						for (int i_458_ = 0; i_458_ < class82.aShort904; i_458_++) {
							short i_459_ = class82.aShortArray911[i_458_];
							short i_460_ = class82.aShortArray910[i_458_];
							short i_461_ = class82.aShortArray900[i_458_];
							int i_462_ = is[i_459_];
							int i_463_ = is[i_460_];
							int i_464_ = is[i_461_];
							int i_465_ = is_387_[i_459_];
							int i_466_ = is_387_[i_460_];
							int i_467_ = is_387_[i_461_];
							if (((i_462_ - i_463_) * (i_467_ - i_466_) - (i_465_ - i_466_) * (i_464_ - i_463_)) > 0) {
								class11.aBoolean103 = (i_462_ < 0 || i_463_ < 0 || i_464_ < 0
										|| i_462_ > class11.anInt107 || i_463_ > class11.anInt107
										|| i_464_ > class11.anInt107);
								short i_468_ = class82.aShortArray913[i_458_];
								if (i_468_ != -1)
									class11.method214((float) i_465_, (float) i_466_, (float) i_467_, (float) i_462_,
											(float) i_463_, (float) i_464_, (float) is_388_[i_459_],
											(float) is_388_[i_460_], (float) is_388_[i_461_],
											((float) (class82.aShortArray909[i_459_]) / (float) anInt2836),
											((float) (class82.aShortArray909[i_460_]) / (float) anInt2836),
											((float) (class82.aShortArray909[i_461_]) / (float) anInt2836),
											((float) (class82.aShortArray908[i_459_]) / (float) anInt2836),
											((float) (class82.aShortArray908[i_460_]) / (float) anInt2836),
											((float) (class82.aShortArray908[i_461_]) / (float) anInt2836),
											(Class295_Sub1.anIntArray3691[(class82.aShortArray907[i_459_] & 0xffff)])
													& 0xffffff | ~0xffffff,
											(Class295_Sub1.anIntArray3691[(class82.aShortArray907[i_460_] & 0xffff)])
													& 0xffffff | ~0xffffff,
											(Class295_Sub1.anIntArray3691[(class82.aShortArray907[i_461_] & 0xffff)])
													& 0xffffff | ~0xffffff,
											0, 0, 0, 0, i_468_);
								else {
									int i_469_ = class82.anIntArray906[i_458_];
									if (i_469_ != -1)
										class11.method205((float) i_465_, (float) i_466_, (float) i_467_,
												(float) i_462_, (float) i_463_, (float) i_464_, (float) is_388_[i_459_],
												(float) is_388_[i_460_], (float) is_388_[i_461_],
												(float) (ByteStream.method2632(25051, (class82.aShortArray907[i_459_]),
														i_469_)),
												(float) (ByteStream.method2632(25051, (class82.aShortArray907[i_460_]),
														i_469_)),
												(float) (ByteStream.method2632(25051, (class82.aShortArray907[i_461_]),
														i_469_)));
								}
							}
						}
					} else {
						for (int i_470_ = 0; i_470_ < class82.aShort904; i_470_++) {
							short i_471_ = class82.aShortArray911[i_470_];
							short i_472_ = class82.aShortArray910[i_470_];
							short i_473_ = class82.aShortArray900[i_470_];
							int i_474_ = is[i_471_];
							int i_475_ = is[i_472_];
							int i_476_ = is[i_473_];
							int i_477_ = is_387_[i_471_];
							int i_478_ = is_387_[i_472_];
							int i_479_ = is_387_[i_473_];
							if (((i_474_ - i_475_) * (i_479_ - i_478_) - (i_477_ - i_478_) * (i_476_ - i_475_)) > 0) {
								class11.aBoolean103 = (i_474_ < 0 || i_475_ < 0 || i_476_ < 0
										|| i_474_ > class11.anInt107 || i_475_ > class11.anInt107
										|| i_476_ > class11.anInt107);
								short i_480_ = class82.aShortArray913[i_470_];
								if (i_480_ != -1)
									class11.method214((float) i_477_, (float) i_478_, (float) i_479_, (float) i_474_,
											(float) i_475_, (float) i_476_, (float) is_388_[i_471_],
											(float) is_388_[i_472_], (float) is_388_[i_473_],
											((float) (class82.aShortArray909[i_471_]) / (float) anInt2836),
											((float) (class82.aShortArray909[i_472_]) / (float) anInt2836),
											((float) (class82.aShortArray909[i_473_]) / (float) anInt2836),
											((float) (class82.aShortArray908[i_471_]) / (float) anInt2836),
											((float) (class82.aShortArray908[i_472_]) / (float) anInt2836),
											((float) (class82.aShortArray908[i_473_]) / (float) anInt2836),
											(Class295_Sub1.anIntArray3691[(class82.aShortArray907[i_471_] & 0xffff)])
													& 0xffffff | ~0xffffff,
											(Class295_Sub1.anIntArray3691[(class82.aShortArray907[i_472_] & 0xffff)])
													& 0xffffff | ~0xffffff,
											(Class295_Sub1.anIntArray3691[(class82.aShortArray907[i_473_] & 0xffff)])
													& 0xffffff | ~0xffffff,
											0, 0, 0, 0, i_480_);
								else {
									int i_481_ = class82.anIntArray906[i_470_];
									if (i_481_ != -1)
										class11.method205((float) i_477_, (float) i_478_, (float) i_479_,
												(float) i_474_, (float) i_475_, (float) i_476_, (float) is_388_[i_471_],
												(float) is_388_[i_472_], (float) is_388_[i_473_],
												(float) (ByteStream.method2632(25051, (class82.aShortArray907[i_471_]),
														i_481_)),
												(float) (ByteStream.method2632(25051, (class82.aShortArray907[i_472_]),
														i_481_)),
												(float) (ByteStream.method2632(25051, (class82.aShortArray907[i_473_]),
														i_481_)));
								}
							}
						}
					}
				} else {
					for (int i_482_ = 0; i_482_ < class82.aShort904; i_482_++) {
						short i_483_ = class82.aShortArray911[i_482_];
						short i_484_ = class82.aShortArray910[i_482_];
						short i_485_ = class82.aShortArray900[i_482_];
						int i_486_ = is[i_483_];
						int i_487_ = is[i_484_];
						int i_488_ = is[i_485_];
						int i_489_ = is_387_[i_483_];
						int i_490_ = is_387_[i_484_];
						int i_491_ = is_387_[i_485_];
						if (((i_486_ - i_487_) * (i_491_ - i_490_) - (i_489_ - i_490_) * (i_488_ - i_487_)) > 0) {
							int i_492_ = class82.anIntArray906[i_482_];
							if (i_492_ != -1) {
								class11.aBoolean103 = (i_486_ < 0 || i_487_ < 0 || i_488_ < 0
										|| i_486_ > class11.anInt107 || i_487_ > class11.anInt107
										|| i_488_ > class11.anInt107);
								class11.method205((float) i_489_, (float) i_490_, (float) i_491_, (float) i_486_,
										(float) i_487_, (float) i_488_, (float) is_388_[i_483_],
										(float) is_388_[i_484_], (float) is_388_[i_485_],
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_483_], i_492_)),
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_484_], i_492_)),
										(float) (ByteStream.method2632(25051, class82.aShortArray907[i_485_], i_492_)));
							}
						}
					}
				}
			}
		}
	}

	private void method3366(int i, int i_493_, int i_494_) {
		Class240 class240 = aHa_Sub2_5102.method1270(Thread.currentThread());
		class240.aClass11_2285.anInt108 = 0;
		if (aClass73ArrayArray5100 != null)
			method3364(i, i_493_, class240.aBoolean2269, class240, class240.aClass11_2285, class240.anIntArray2277,
					class240.anIntArray2281, class240.anIntArray2284, class240.anIntArray2293, i_494_);
		else if (aClass372ArrayArray5110 != null)
			method3365(i, i_493_, class240.aClass11_2285, class240.anIntArray2277, class240.anIntArray2281,
					class240.anIntArray2284, class240.anIntArray2293, i_494_);
		else if (aClass80ArrayArray5093 != null)
			method3361(i, i_493_, class240.aBoolean2269, class240, class240.aClass11_2285, class240.anIntArray2277,
					class240.anIntArray2281, class240.anIntArray2284, class240.anIntArray2293, i_494_);
	}

	public void ka(int i, int i_495_, int i_496_) {
		if (aByteArrayArray5101[i][i_495_] < i_496_)
			aByteArrayArray5101[i][i_495_] = (byte) i_496_;
	}

	public void CA(r var_r, int i, int i_497_, int i_498_, int i_499_, boolean bool) {
		/* empty */
	}
}
