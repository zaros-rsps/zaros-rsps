package net.zaros.client;
/* Class41_Sub9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

final class Class41_Sub9 extends Class41 {
	static Class209 aClass209_3766 = new Class209(43);

	static final void method427(byte i) {
		StaticMethods.method2465(111);
		if (i != -110)
			method428(-8);
		Class318.aBoolean2814 = false;
		Class375.method3953((byte) -106, Class296_Sub39_Sub20.anInt6254, Class81.anInt3666, Class252.anInt2382, Class100.anInt1067);
	}

	Class41_Sub9(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final int method383(byte i) {
		if (i != 110)
			return -18;
		return 1;
	}

	public static void method428(int i) {
		if (i < 102)
			method427((byte) 95);
		aClass209_3766 = null;
	}

	static final void method429(int i, File file, byte[] is, boolean bool) throws IOException {
		DataInputStream datainputstream = (new DataInputStream(new BufferedInputStream(new FileInputStream(file))));
		try {
			if (bool)
				return;
			datainputstream.readFully(is, 0, i);
		} catch (java.io.EOFException eofexception) {
			/* empty */
		}
		datainputstream.close();
	}

	final void method381(int i, byte i_0_) {
		anInt389 = i;
		if (i_0_ != -110)
			aClass209_3766 = null;
	}

	final int method430(int i) {
		if (i <= 114)
			method428(2);
		return anInt389;
	}

	final void method386(int i) {
		if (anInt389 != 1 && anInt389 != 0)
			anInt389 = method383((byte) 110);
		if (i != 2) {
			/* empty */
		}
	}

	Class41_Sub9(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final int method380(int i, byte i_1_) {
		if (i_1_ != 41)
			aClass209_3766 = null;
		return 1;
	}
}
