package net.zaros.client;
import java.util.HashSet;
import java.util.Set;

/* Class143 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Js5TextureLoader implements d {
	private Js5 spritesJs5;
	static ModeWhat releaseCandidateModeWhat = new ModeWhat("RC", 1);
	private MaterialRaw[] materials;
	private Js5 texturesJs5;
	static int anInt3440;
	private SubCache cached_textures = new SubCache(256);
	static int anInt3442;
	static String[] aStringArray3443 = new String[200];
	private int num_materials;
	static IncomingPacket aClass231_3445;
	static long[] aLongArray3446 = new long[32];

	@Override
	public final float[] method16(int i, float f, boolean bool, int i_0_, int i_1_, int i_2_) {
		if (i_2_ <= 69) {
			method17(-0.3596177F, -34, true, (byte) -50, 81, 80);
		}
		return get_texture(i_0_).method2891(i, this, spritesJs5, (byte) -123, materials[i_0_].aBoolean1789, i_1_);
	}

	private static final Set<Integer> missingMaterials = new HashSet<>();

	@Override
	public final MaterialRaw method14(int materialid, int i_3_) {
		if (materialid >= materials.length) {
			if (missingMaterials.add(materialid)) {
				System.out.println("Missing material: " + materialid);
			}
			materialid = 0;
		}
		return materials[materialid];
	}

	private final Texture get_texture(int textureid) {
		Queuable class296_sub39 = cached_textures.get(textureid);
		if (class296_sub39 != null) {
			return (Texture) class296_sub39;
		}
		byte[] data = texturesJs5.get(textureid);
		if (data == null) {
			return null;
		}
		Texture class296_sub39_sub16 = new Texture(textureid, new Packet(data));
		cached_textures.put(textureid, class296_sub39_sub16);
		return class296_sub39_sub16;
	}

	@Override
	public final int method13(byte i) {
		return num_materials;
	}

	@Override
	public boolean is_ready(int materialid) {
		Texture texture = get_texture(materialid);
		if (texture == null || !texture.is_ready(this, spritesJs5)) {
			return false;
		}
		return true;
	}

	public static void method1480(int i) {
		aStringArray3443 = null;
		releaseCandidateModeWhat = null;
		aLongArray3446 = null;
		aClass231_3445 = null;
		if (i != -2434) {
			method1480(45);
		}
	}

	@Override
	public final int[] method17(float f, int i, boolean bool, byte i_6_, int i_7_, int i_8_) {
		if (i_6_ != -104) {
			return null;
		}
		return get_texture(i_8_).method2890(materials[i_8_].aBoolean1789, this, spritesJs5, i, f, i_6_ ^ ~0x67, i_7_);
	}

	@Override
	public final int[] get_transparent_pixels(int i, int i_9_, boolean bool, byte i_10_, float f, int i_11_) {
		if (i_10_ < 82) {
			get_texture(60);
		}
		return get_texture(i_11_).method2889(this, bool, f, -29699, i, materials[i_11_].aBoolean1789, spritesJs5, i_9_);
	}

	Js5TextureLoader(Js5 class138, Js5 class138_12_, Js5 class138_13_) {
		texturesJs5 = class138_12_;
		spritesJs5 = class138_13_;
		Packet buffer = new Packet(class138.getFile(0, 0));
		num_materials = buffer.g2();
		materials = new MaterialRaw[num_materials];
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (buffer.g1() == 1) {
				materials[materialid] = new MaterialRaw();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aBoolean1792 = buffer.g1() == 0;
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].small_sized = buffer.g1() == 1;
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aBoolean1786 = buffer.g1() == 1;
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aByte1793 = buffer.g1b();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aByte1773 = buffer.g1b();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aByte1774 = buffer.g1b();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aByte1784 = buffer.g1b();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aShort1775 = (short) buffer.g2();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].speed_u = buffer.g1b();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].speed_v = buffer.g1b();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aBoolean1785 = buffer.g1() == 1;
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aBoolean1789 = buffer.g1() == 1;
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aByte1795 = buffer.g1b();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aBoolean1779 = buffer.g1() == 1;
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aBoolean1780 = buffer.g1() == 1;
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].aBoolean1790 = buffer.g1() == 1;
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].anInt1782 = buffer.g1();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].anInt1794 = buffer.g4();
			}
		}
		for (int materialid = 0; materialid < num_materials; materialid++) {
			if (materials[materialid] != null) {
				materials[materialid].anInt1788 = buffer.g1();
			}
		}
		// for (int materialid = 0; materialid < num_materials; materialid++) {
		// if (!is_ready(materialid)) {
		// Texture texture = get_texture(materialid);
		// if (texture == null) {
		// System.out.println("Being null: " + materialid);
		// continue;
		// }
		// for (int psrite : texture.texture_sprites_diffuse) {
		// list.add(psrite);
		// }
		// for (int psrite : texture.texture_sprites_hdr) {
		// list.add(psrite);
		// }
		// }
		// }
		// System.out.println(Arrays.toString(list.toArray()));
	}

	static Set<Integer> list = new HashSet<>();

	static {
		aClass231_3445 = new IncomingPacket(105, 6);
	}
}
