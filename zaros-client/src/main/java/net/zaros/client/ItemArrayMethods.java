package net.zaros.client;

import net.zaros.client.configs.objtype.ObjType;

public class ItemArrayMethods {

	static final ItemsNode getItemsNode(boolean split, int arrayID) {
		long l = (long) ((split ? -2147483648 : 0) | arrayID);
		return ((ItemsNode) Class355_Sub1.itemsNodes.get(l));
	}
	
	static final void resetItemArray(int arrayID, boolean split) {
		ItemsNode node = ItemArrayMethods.getItemsNode(split, arrayID);
		if (node != null) {
			for (int i = 0; i < node.itemIDS.length; i++) {
				node.itemIDS[i] = -1;
				node.itemAmounts[i] = 0;
			}
		}
	}

	static final void updateItemsRegister(int arrayID, int index, int itemID, int itemAmount, boolean split) {
		long l = (long) ((!split ? 0 : -2147483648) | arrayID);
		ItemsNode node = (ItemsNode) Class355_Sub1.itemsNodes.get(l);
		if (node == null) {
			node = new ItemsNode();
			Class355_Sub1.itemsNodes.put(l, node);
		}
		if (index >= node.itemIDS.length) {
			int[] idsExpands = new int[index + 1];
			int[] amountExpands = new int[index + 1];
			for (int i = 0; node.itemIDS.length > i; i++) {
				idsExpands[i] = node.itemIDS[i];
				amountExpands[i] = node.itemAmounts[i];
			}
			for (int i = node.itemIDS.length; i < index; i++) {
				idsExpands[i] = -1;
				amountExpands[i] = 0;
			}
			node.itemAmounts = amountExpands;
			node.itemIDS = idsExpands;
		}
		node.itemIDS[index] = itemID;
		node.itemAmounts[index] = itemAmount;
	}

	static final void deleteItemsArray(int arrayID, boolean split) {
		ItemsNode node = getItemsNode(split, arrayID);
		if (node != null) {
			node.unlink();
		}
	}

	static final int getItemIDFromItemsArray(int arrayID, int index, boolean split) {
		ItemsNode node = getItemsNode(split, arrayID);
		if (node == null)
			return -1;
		if (index < 0 || node.itemIDS.length <= index)
			return -1;
		return node.itemIDS[index];
	}
	
	static final int getItemsCountInItemsArray(int arrayID, int itemID, boolean split) {
		ItemsNode node = getItemsNode(split, arrayID);
		if (node == null)
			return 0;
		if (itemID == -1)
			return 0;
		int total = 0;
		for (int i = 0; i < node.itemAmounts.length; i++) {
			if (itemID == node.itemIDS[i])
				total += node.itemAmounts[i];
		}
		return total;
	}

	static final int getItemAmountInItemsArray(int arrayID, int index, boolean split) {
		ItemsNode n = getItemsNode(split, arrayID);
		if (n == null)
			return 0;
		if (index < 0 || n.itemAmounts.length <= index)
			return 0;
		return n.itemAmounts[index];
	}

	static final int method481(int arrayID, int extraDataIndex, boolean split, boolean multiply) {
		ItemsNode node = getItemsNode(split, arrayID);
		if (node == null)
			return 0;
		int someTotal = 0;
		for (int i = 0; node.itemIDS.length > i; i++) {
			if (node.itemIDS[i] >= 0 && (node.itemIDS[i] < Class296_Sub39_Sub1.itemDefinitionLoader.size)) {
				ObjType definition = Class296_Sub39_Sub1.itemDefinitionLoader.list(node.itemIDS[i]);
				int value = definition.getIntParam(extraDataIndex, (Class296_Sub22.itemExtraDataDefinitionLoader.list(extraDataIndex).defaultValue));
				if (multiply)
					someTotal += node.itemAmounts[i] * value;
				else
					someTotal += value;
			}
		}
		return someTotal;
	}

	static final int method1028(int arrayID, boolean split) {
		if (split)
			return 0;
		ItemsNode node = getItemsNode(split, arrayID);
		if (node == null)
			return Class296_Sub14.itemArraysDefinitionLoader.list(arrayID).capacity;
		int amount = 0;
		for (int i = 0; node.itemIDS.length > i; i++) {
			if (node.itemIDS[i] == -1)
				amount++;
		}
		amount += (Class296_Sub14.itemArraysDefinitionLoader.list(arrayID).capacity) - node.itemIDS.length;
		return amount;
	}

}
