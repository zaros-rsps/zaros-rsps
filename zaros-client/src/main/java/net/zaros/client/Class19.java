package net.zaros.client;
/* Class19 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.File;
import java.io.RandomAccessFile;
import java.util.Hashtable;

public class Class19 {
	private static String aString232;
	private static Hashtable aHashtable233;
	private static String aString234;
	private static boolean aBoolean235 = false;
	private static int anInt236;

	public static File method281(String string, int i, String string_0_, int i_1_) {
		if (!aBoolean235)
			throw new RuntimeException("");
		File file = (File) aHashtable233.get(string);
		if (file != null)
			return file;
		String[] strings = {"c:/rscache/", "/rscache/", "c:/windows/", "c:/winnt/", "c:/", aString234, "/tmp/", ""};
		String[] strings_2_ = {".jolt_cache_" + i, ".jolt_store_" + i};
		if (i_1_ > -64)
			method281(null, -99, null, -122);
		for (int i_3_ = 0; i_3_ < 2; i_3_++) {
			for (int i_4_ = 0; strings_2_.length > i_4_; i_4_++) {
				for (int i_5_ = 0; i_5_ < strings.length; i_5_++) {
					String string_6_ = (strings[i_5_] + strings_2_[i_4_] + "/" + (string_0_ != null ? string_0_ + "/" : "") + string);
					RandomAccessFile randomaccessfile = null;
					try {
						File file_7_ = new File(string_6_);
						if (i_3_ != 0 || file_7_.exists()) {
							String string_8_ = strings[i_5_];
							if (i_3_ != 1 || string_8_.length() <= 0 || new File(string_8_).exists()) {
								new File(strings[i_5_] + strings_2_[i_4_]).mkdir();
								if (string_0_ != null)
									new File(strings[i_5_] + strings_2_[i_4_] + "/" + string_0_).mkdir();
								randomaccessfile = new RandomAccessFile(file_7_, "rw");
								int i_9_ = randomaccessfile.read();
								randomaccessfile.seek(0L);
								randomaccessfile.write(i_9_);
								randomaccessfile.seek(0L);
								randomaccessfile.close();
								aHashtable233.put(string, file_7_);
								return file_7_;
							}
						}
					} catch (Exception exception) {
						try {
							if (randomaccessfile != null) {
								randomaccessfile.close();
								Object object = null;
							}
						} catch (Exception exception_10_) {
							/* empty */
						}
					}
				}
			}
		}
		throw new RuntimeException();
	}

	public static File method282(String string, int i) {
		if (i != -2)
			return null;
		return method281(string, anInt236, aString232, -104);
	}

	public static void method283(String string, byte i, int i_11_) {
		aString232 = string;
		anInt236 = i_11_;
		try {
			if (i != -122)
				method282(null, 2);
			aString234 = System.getProperty("user.home");
			if (aString234 != null)
				aString234 += "/";
		} catch (Exception exception) {
			/* empty */
		}
		if (aString234 == null)
			aString234 = "~/";
		aBoolean235 = true;
	}

	static {
		aHashtable233 = new Hashtable(16);
	}
}
