package net.zaros.client;

/* Class239 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class239 {
	static OutgoingPacket aClass311_2253 = new OutgoingPacket(53, 18);
	static int anInt2254 = 0;
	static Js5 aClass138_2255;
	public static void method2141(int i) {
		aClass138_2255 = null;
		aClass311_2253 = null;
		if (i != 53)
			method2142(null, -31);
		PlayerUpdate.playerWalkingTypes = null;
	}

	static final long method2142(String string, int i) {
		int i_0_ = string.length();
		long l = 0L;
		for (int i_1_ = 0; i_0_ > i_1_; i_1_++)
			l = -l + ((l << 5) + (long) string.charAt(i_1_));
		if (i != -681688315)
			aClass138_2255 = null;
		return l;
	}

	static final Sprite method2143(int i, int i_2_, ha var_ha) {
		if (i_2_ < 44)
			return null;
		Class296_Sub14 class296_sub14 = ((Class296_Sub14) Class296_Sub51_Sub11.aClass263_6399.get((long) i));
		if (class296_sub14 != null) {
			Class296_Sub15_Sub3 class296_sub15_sub3 = class296_sub14.aClass219_Sub1_4667.method2052(-1);
			class296_sub14.aBoolean4669 = true;
			if (class296_sub15_sub3 != null)
				return class296_sub15_sub3.method2535((byte) 108, var_ha);
		}
		return null;
	}

	static final void method2144(int i, int i_3_, int i_4_, Class338_Sub3_Sub4 class338_sub3_sub4, Class338_Sub3_Sub4 class338_sub3_sub4_5_) {
		Class247 class247 = Node.method2428(i, i_3_, i_4_);
		if (class247 != null) {
			class247.aClass338_Sub3_Sub4_2348 = class338_sub3_sub4;
			class247.aClass338_Sub3_Sub4_2343 = class338_sub3_sub4_5_;
			int i_6_ = Class360_Sub2.aSArray5304 == Class52.aSArray636 ? 1 : 0;
			if (class338_sub3_sub4.method3459(0)) {
				if (class338_sub3_sub4.method3469(97)) {
					class338_sub3_sub4.aClass338_Sub3_5202 = Class368_Sub16.aClass338_Sub3Array5525[i_6_];
					Class368_Sub16.aClass338_Sub3Array5525[i_6_] = class338_sub3_sub4;
				} else {
					class338_sub3_sub4.aClass338_Sub3_5202 = Class368_Sub10.aClass338_Sub3Array5481[i_6_];
					Class368_Sub10.aClass338_Sub3Array5481[i_6_] = class338_sub3_sub4;
					Class41.aBoolean388 = true;
				}
			} else {
				class338_sub3_sub4.aClass338_Sub3_5202 = Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_6_];
				Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_6_] = class338_sub3_sub4;
			}
			if (class338_sub3_sub4_5_ != null) {
				if (class338_sub3_sub4_5_.method3459(0)) {
					if (class338_sub3_sub4_5_.method3469(111)) {
						class338_sub3_sub4_5_.aClass338_Sub3_5202 = Class368_Sub16.aClass338_Sub3Array5525[i_6_];
						Class368_Sub16.aClass338_Sub3Array5525[i_6_] = class338_sub3_sub4_5_;
					} else {
						class338_sub3_sub4_5_.aClass338_Sub3_5202 = Class368_Sub10.aClass338_Sub3Array5481[i_6_];
						Class368_Sub10.aClass338_Sub3Array5481[i_6_] = class338_sub3_sub4_5_;
						Class41.aBoolean388 = true;
					}
				} else {
					class338_sub3_sub4_5_.aClass338_Sub3_5202 = Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_6_];
					Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_6_] = class338_sub3_sub4_5_;
				}
			}
		}
	}
}
