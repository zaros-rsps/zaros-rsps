package net.zaros.client;

import net.zaros.client.configs.objtype.ObjTypeList;

/* Class338_Sub3_Sub4_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub3_Sub4_Sub1 extends Class338_Sub3_Sub4 implements Interface14 {
	static long aLong6651 = -1L;
	private boolean aBoolean6652;
	static int[] anIntArray6653 = new int[200];
	private Class96 aClass96_6654;
	private boolean aBoolean6655 = true;
	Class380 aClass380_6656;
	static IncomingPacket aClass231_6657 = new IncomingPacket(129, 2);
	static float aFloat6658;
	static Class292 aClass292_6659;

	Class338_Sub3_Sub4_Sub1(ha var_ha, ObjectDefinition class70, int i, int i_0_, int i_1_, int i_2_, int i_3_, boolean bool, int i_4_, int i_5_, int i_6_) {
		super(i_1_, i_2_, i_3_, i, i_0_, za_Sub1.method3222(i_4_, 15071, i_5_));
		aClass380_6656 = new Class380(var_ha, class70, i_4_, i_5_, z, i_0_, this, bool, i_6_);
		aBoolean6652 = class70.interactonType != 0 && !bool;
	}

	static final void method3572(Class338_Sub3 class338_sub3, Class296_Sub35[] class296_sub35s) {
		if (Class368_Sub2.aBoolean5427) {
			int i = class338_sub3.method3463(-1, class296_sub35s);
			Class16_Sub2.aHa3733.a(i, class296_sub35s);
		}
		if (Class52.aSArray636 == Class360_Sub2.aSArray5304) {
			boolean bool = false;
			boolean bool_7_ = false;
			int i;
			int i_8_;
			if (class338_sub3 instanceof Class338_Sub3_Sub1) {
				i = ((Class338_Sub3_Sub1) class338_sub3).aShort6560;
				i_8_ = ((Class338_Sub3_Sub1) class338_sub3).aShort6564;
			} else {
				i = class338_sub3.tileX >> Class313.anInt2779;
				i_8_ = class338_sub3.tileY >> Class313.anInt2779;
			}
			Class16_Sub2.aHa3733.EA(Class244.aSArray2320[0].method3349(0, class338_sub3.tileY, class338_sub3.tileX), Class162.method1624(i, i_8_), Class249.method2195(i, i_8_), Class18.method2825(i, i_8_));
		}
		Class338_Sub2 class338_sub2 = class338_sub3.method3473(Class16_Sub2.aHa3733, (byte) -125);
		if (class338_sub2 != null) {
			if (class338_sub3.aBoolean5206) {
				Class338_Sub5[] class338_sub5s = class338_sub2.aClass338_Sub5Array5194;
				for (int i = 0; i < class338_sub5s.length; i++) {
					Class338_Sub5 class338_sub5 = class338_sub5s[i];
					if (class338_sub5.aBoolean5224)
						Class368_Sub8.method3838(false, class338_sub5.anInt5225 - class338_sub5.anInt5226, class338_sub5.anInt5223 - class338_sub5.anInt5226, class338_sub5.anInt5221 + class338_sub5.anInt5226, (class338_sub5.anInt5222 + class338_sub5.anInt5226));
				}
			}
			if (class338_sub2.aBoolean5196) {
				class338_sub2.aClass338_Sub3_5193 = class338_sub3;
				if (Class338_Sub3_Sub2.aBoolean6566) {
					synchronized (CS2Stack.aClass264_2249) {
						CS2Stack.aClass264_2249.method2278(class338_sub2, -88);
					}
				} else
					CS2Stack.aClass264_2249.method2278(class338_sub2, -49);
			} else
				SubCache.method3258(class338_sub2, 5362);
		}
	}

	final void method3573(int i, Class375 class375) {
		if (i == 9418)
			aClass380_6656.method3984(-5581, class375);
	}

	final int method3462(byte i) {
		if (i != 28)
			aLong6651 = -111L;
		return aClass380_6656.method3982(14356);
	}

	final boolean method3469(int i) {
		if (i < 82)
			return false;
		return aBoolean6655;
	}

	final int method3466(byte i) {
		if (i <= 77)
			aClass96_6654 = null;
		return aClass380_6656.method3980(true);
	}

	static final boolean method3574(byte i, int i_9_, int i_10_) {
		if (i < 90)
			aFloat6658 = -0.014721939F;
		if (!(Class366_Sub1.method3771(i_10_, -1, i_9_) | (i_10_ & 0x60000) != 0) && !Class338_Sub3_Sub2_Sub1.method3556((byte) 81, i_10_, i_9_))
			return false;
		return true;
	}

	final Class338_Sub2 method3473(ha var_ha, byte i) {
		Model class178 = aClass380_6656.method3975(true, false, var_ha, 2048, (byte) 39);
		if (class178 == null)
			return null;
		Class373 class373 = var_ha.f();
		if (i >= -84)
			aClass380_6656 = null;
		class373.method3902(tileX, anInt5213, tileY);
		Class338_Sub2 class338_sub2 = Class144.method1481(1, true, aBoolean6652);
		int i_11_ = tileX >> 9;
		int i_12_ = tileY >> 9;
		aClass380_6656.method3977(class373, true, class178, i_11_, false, var_ha, i_12_, i_11_, i_12_);
		if (Class296_Sub39_Sub10.aBoolean6177)
			class178.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
		else
			class178.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
		if (aClass380_6656.aClass338_Sub1_3202 != null) {
			Class390 class390 = aClass380_6656.aClass338_Sub1_3202.method3444();
			if (!Class296_Sub39_Sub10.aBoolean6177)
				var_ha.a(class390);
			else
				var_ha.a(class390, ModeWhat.anInt1192);
		}
		aBoolean6655 = class178.F() || aClass380_6656.aClass338_Sub1_3202 != null;
		if (aClass96_6654 != null)
			Billboard.method4025(class178, 122, tileY, anInt5213, aClass96_6654, tileX);
		else
			aClass96_6654 = Class41_Sub21.method478(anInt5213, class178, tileX, tileY, (byte) 104);
		return class338_sub2;
	}

	static final void method3575(int i, InterfaceComponent class51, int i_13_, byte i_14_, ha var_ha) {
		Class98 class98 = class51.method633((byte) -72, var_ha);
		if (i_14_ >= -49)
			aClass292_6659 = null;
		if (class98 != null) {
			aa var_aa = class98.anAa1056;
			var_ha.KA(i_13_, i, class51.anInt578 + i_13_, class51.anInt623 + i);
			if (Class338_Sub3_Sub5_Sub1.anInt6645 == 2 || Class338_Sub3_Sub5_Sub1.anInt6645 == 5 || Class69_Sub3.aClass397_5722 == null)
				var_ha.A(-16777216, var_aa, i_13_, i);
			else {
				int i_15_;
				int i_16_;
				int i_17_;
				int i_18_;
				if (Class361.anInt3103 == 4) {
					i_16_ = Class338_Sub3_Sub1.anInt6563;
					i_15_ = Class127_Sub1.anInt4287;
					i_17_ = 4096;
					i_18_ = (int) -Class41_Sub26.aFloat3806 & 0x3fff;
				} else {
					i_15_ = (Class296_Sub51_Sub11.localPlayer.tileY);
					i_16_ = (Class296_Sub51_Sub11.localPlayer.tileX);
					i_17_ = 4096 - ObjTypeList.anInt1320 * 16;
					i_18_ = ((int) -Class41_Sub26.aFloat3806 + StaticMethods.anInt6075) & 0x3fff;
				}
				int i_19_ = i_16_ / 128 + 48 - Class198.currentMapSizeX * 2 + 208;
				int i_20_ = (-(Class296_Sub38.currentMapSizeY * 2) - (-208 - (-(i_15_ / 128) + 48) - Class296_Sub38.currentMapSizeY * 4));
				Class69_Sub3.aClass397_5722.method4082((float) class51.anInt578 / 2.0F + (float) i_13_, (float) i + (float) class51.anInt623 / 2.0F, (float) i_19_, (float) i_20_, i_17_, i_18_ << 2, var_aa, i_13_, i);
				for (IntegerNode class296_sub16 = ((IntegerNode) Class290.aClass155_2655.removeFirst((byte) 121)); class296_sub16 != null; class296_sub16 = (IntegerNode) Class290.aClass155_2655.removeNext(1001)) {
					int i_21_ = class296_sub16.value;
					int i_22_ = (-Class206.worldBaseX + (((Class296_Sub51_Sub19.aClass259_6439.anIntArray2418[i_21_]) & 0xffff9d3) >> 14));
					int i_23_ = (-Class41_Sub26.worldBaseY + ((Class296_Sub51_Sub19.aClass259_6439.anIntArray2418[i_21_]) & 0x3fff));
					int i_24_ = -(i_16_ / 128) + (i_22_ * 4 + 2);
					int i_25_ = -(i_15_ / 128) + (i_23_ * 4 + 2);
					Class212.method2021((Class296_Sub51_Sub19.aClass259_6439.anIntArray2420[i_21_]), i_13_, class51, var_aa, i_24_, i_25_, i, -28123, var_ha);
				}
				for (int i_26_ = 0; i_26_ < Class41_Sub1.anInt3739; i_26_++) {
					int i_27_ = (Class106_Sub1.anIntArray3902[i_26_] * 4 + 2 - i_16_ / 128);
					int i_28_ = (-(i_15_ / 128) + 2 + Class366_Sub9.anIntArray5421[i_26_] * 4);
					ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition((StaticMethods.anIntArray6089[i_26_]));
					if (class70.transforms != null) {
						class70 = class70.method757((Class16_Sub3_Sub1.configsRegister), false);
						if (class70 == null || class70.anInt759 == -1)
							continue;
					}
					Class212.method2021(class70.anInt759, i_13_, class51, var_aa, i_27_, i_28_, i, -28123, var_ha);
				}
				for (Class296_Sub56 class296_sub56 = ((Class296_Sub56) Class296_Sub11.aClass263_4647.getFirst(true)); class296_sub56 != null; class296_sub56 = ((Class296_Sub56) Class296_Sub11.aClass263_4647.getNext(0))) {
					int i_29_ = (int) (class296_sub56.uid >> 28 & 0x3L);
					if (i_29_ == Class296_Sub39_Sub19.anInt6250) {
						int i_30_ = ((int) (class296_sub56.uid & 0x3fffL) - Class206.worldBaseX);
						int i_31_ = ((int) (class296_sub56.uid >> 14 & 0x3fffL) - Class41_Sub26.worldBaseY);
						int i_32_ = -(i_16_ / 128) + 2 + i_30_ * 4;
						int i_33_ = -(i_15_ / 128) + (i_31_ * 4 + 2);
						Class331.method3409(i, var_aa, class51, Class205.aClass397Array3505[0], i_32_, i_33_, i_13_, false);
					}
				}
				for (int i_34_ = 0; Class367.npcsCount > i_34_; i_34_++) {
					NPCNode class296_sub7 = ((NPCNode) (Class41_Sub18.localNpcs.get((long) ReferenceTable.npcIndexes[i_34_])));
					if (class296_sub7 != null) {
						NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
						if (class338_sub3_sub1_sub3_sub2.hasDefinition() && ((Class296_Sub51_Sub11.localPlayer.z) == class338_sub3_sub1_sub3_sub2.z)) {
							NPCDefinition class147 = class338_sub3_sub1_sub3_sub2.definition;
							if (class147 != null && class147.configData != null)
								class147 = class147.getNPCDefinitionByConfigID((Class16_Sub3_Sub1.configsRegister));
							if (class147 != null && class147.visibleOnMinimap && class147.isClickable) {
								int i_35_ = ((class338_sub3_sub1_sub3_sub2.tileX / 128) - i_16_ / 128);
								int i_36_ = (-(i_15_ / 128) + (class338_sub3_sub1_sub3_sub2.tileY) / 128);
								if (class147.anInt1448 == -1)
									Class331.method3409(i, var_aa, class51, (Class205.aClass397Array3505[1]), i_35_, i_36_, i_13_, false);
								else
									Class212.method2021(class147.anInt1448, i_13_, class51, var_aa, i_35_, i_36_, i, -28123, var_ha);
							}
						}
					}
				}
				int i_37_ = PlayerUpdate.visiblePlayersCount;
				int[] is = PlayerUpdate.visiblePlayersIndexes;
				for (int i_38_ = 0; i_37_ > i_38_; i_38_++) {
					Player class338_sub3_sub1_sub3_sub1 = (PlayerUpdate.visiblePlayers[is[i_38_]]);
					if (class338_sub3_sub1_sub3_sub1 != null && class338_sub3_sub1_sub3_sub1.hasComposite() && !class338_sub3_sub1_sub3_sub1.somethingWithIgnores && ((Class296_Sub51_Sub11.localPlayer) != class338_sub3_sub1_sub3_sub1) && (class338_sub3_sub1_sub3_sub1.z == (Class296_Sub51_Sub11.localPlayer.z))) {
						int i_39_ = (class338_sub3_sub1_sub3_sub1.tileX / 128 - i_16_ / 128);
						int i_40_ = (-(i_15_ / 128) + class338_sub3_sub1_sub3_sub1.tileY / 128);
						boolean bool = false;
						for (int i_41_ = 0; i_41_ < Class285.anInt2625; i_41_++) {
							if (class338_sub3_sub1_sub3_sub1.displayName.equals(Js5TextureLoader.aStringArray3443[i_41_]) && Class95.anIntArray1021[i_41_] != 0) {
								bool = true;
								break;
							}
						}
						boolean bool_42_ = false;
						for (int i_43_ = 0; Class271.anInt2512 > i_43_; i_43_++) {
							if (class338_sub3_sub1_sub3_sub1.displayName.equals(Class338_Sub8.aClass94Array5257[i_43_].aString1019)) {
								bool_42_ = true;
								break;
							}
						}
						boolean bool_44_ = false;
						if (((Class296_Sub51_Sub11.localPlayer.team) != 0) && class338_sub3_sub1_sub3_sub1.team != 0 && (class338_sub3_sub1_sub3_sub1.team == (Class296_Sub51_Sub11.localPlayer.team)))
							bool_44_ = true;
						if (class338_sub3_sub1_sub3_sub1.aBoolean6879)
							Class331.method3409(i, var_aa, class51, Class205.aClass397Array3505[6], i_39_, i_40_, i_13_, false);
						else if (bool_44_)
							Class331.method3409(i, var_aa, class51, Class205.aClass397Array3505[4], i_39_, i_40_, i_13_, false);
						else if (!class338_sub3_sub1_sub3_sub1.aBoolean6868) {
							if (!bool) {
								if (bool_42_)
									Class331.method3409(i, var_aa, class51, (Class205.aClass397Array3505[5]), i_39_, i_40_, i_13_, false);
								else
									Class331.method3409(i, var_aa, class51, (Class205.aClass397Array3505[2]), i_39_, i_40_, i_13_, false);
							} else
								Class331.method3409(i, var_aa, class51, (Class205.aClass397Array3505[3]), i_39_, i_40_, i_13_, false);
						} else
							Class331.method3409(i, var_aa, class51, Class205.aClass397Array3505[7], i_39_, i_40_, i_13_, false);
					}
				}
				Class225[] class225s = Class338_Sub2.aClass225Array5200;
				for (int i_45_ = 0; i_45_ < class225s.length; i_45_++) {
					Class225 class225 = class225s[i_45_];
					if (class225 != null && class225.iconType != 0 && Class29.anInt307 % 20 < 10) {
						if (class225.iconType == 1) {
							NPCNode class296_sub7 = ((NPCNode) (Class41_Sub18.localNpcs.get((long) class225.targetIndex)));
							if (class296_sub7 != null) {
								NPC class338_sub3_sub1_sub3_sub2 = (class296_sub7.value);
								int i_46_ = ((class338_sub3_sub1_sub3_sub2.tileX / 128) - i_16_ / 128);
								int i_47_ = ((class338_sub3_sub1_sub3_sub2.tileY / 128) - i_15_ / 128);
								FileOnDisk.method679(class225.modelId, 360000L, i_47_, 71, var_aa, class51, i, i_13_, i_46_);
							}
						}
						if (class225.iconType == 2) {
							int i_48_ = -(i_16_ / 128) + class225.anInt2183 / 128;
							int i_49_ = -(i_15_ / 128) + class225.anInt2176 / 128;
							long l = (long) (class225.anInt2179 << 7);
							l *= l;
							FileOnDisk.method679(class225.modelId, l, i_49_, 93, var_aa, class51, i, i_13_, i_48_);
						}
						if (class225.iconType == 10 && class225.targetIndex >= 0 && ((PlayerUpdate.visiblePlayers).length > class225.targetIndex)) {
							Player class338_sub3_sub1_sub3_sub1 = (PlayerUpdate.visiblePlayers[class225.targetIndex]);
							if (class338_sub3_sub1_sub3_sub1 != null) {
								int i_50_ = ((class338_sub3_sub1_sub3_sub1.tileX / 128) - i_16_ / 128);
								int i_51_ = ((class338_sub3_sub1_sub3_sub1.tileY / 128) - i_15_ / 128);
								FileOnDisk.method679(class225.modelId, 360000L, i_51_, 40, var_aa, class51, i, i_13_, i_50_);
							}
						}
					}
				}
				if (Class361.anInt3103 != 4) {
					if (Class210_Sub1.foundX != 0) {
						int i_52_ = (-(i_16_ / 128) + (Class210_Sub1.foundX * 4 + 2) - (-(Class296_Sub51_Sub11.localPlayer.getSize() * 2) + 2));
						int i_53_ = (Class205.foundY * 4 - i_15_ / 128 + 2 - (-(Class296_Sub51_Sub11.localPlayer.getSize() * 2) + 2));
						Class331.method3409(i, var_aa, class51, (StaticMethods.aClass397Array6073[Class377.aBoolean3189 ? 1 : 0]), i_52_, i_53_, i_13_, false);
					}
					if (!Class296_Sub51_Sub11.localPlayer.somethingWithIgnores)
						var_ha.method1088(class51.anInt578 / 2 - 1 + i_13_, 3, -1, 1, class51.anInt623 / 2 + i - 1, 3);
				}
			}
		}
	}

	final boolean method3468(int i) {
		if (i >= -29)
			return false;
		return false;
	}

	static final short method3576(int i, byte i_54_) {
		int i_55_ = i >> 10 & 0x3f;
		if (i_54_ > -4)
			aFloat6658 = -0.120201804F;
		int i_56_ = (i & 0x384) >> 3;
		int i_57_ = i & 0x7f;
		i_56_ = i_57_ <= 64 ? i_57_ * i_56_ >> 7 : i_56_ * (127 - i_57_) >> 7;
		int i_58_ = i_56_ + i_57_;
		int i_59_;
		if (i_58_ != 0)
			i_59_ = (i_56_ << 8) / i_58_;
		else
			i_59_ = i_56_ << 1;
		int i_60_ = i_58_;
		return (short) (i_60_ | (i_59_ >> 4 << 7 | i_55_ << 10));
	}

	public final void method60(byte i, ha var_ha) {
		aClass380_6656.method3987(var_ha, (byte) -53);
		int i_61_ = 72 / ((-23 - i) / 35);
	}

	final boolean method3475(int i, int i_62_, ha var_ha, int i_63_) {
		Model class178 = aClass380_6656.method3975(false, false, var_ha, 131072, (byte) 39);
		if (class178 == null)
			return false;
		Class373 class373 = var_ha.f();
		class373.method3902(tileX, anInt5213, tileY);
		if (i_62_ >= -48)
			method3574((byte) 88, 19, 91);
		if (Class296_Sub39_Sub10.aBoolean6177)
			return class178.method1731(i_63_, i, class373, false, 0, ModeWhat.anInt1192);
		return class178.method1732(i_63_, i, class373, false, 0);
	}

	static final boolean method3577(int i) {
		if (i != 24060)
			return true;
		if (Class230.anInt2210 <= 0)
			return false;
		return true;
	}

	final Class96 method3461(ha var_ha, int i) {
		int i_64_ = 84 / ((i - 79) / 44);
		return aClass96_6654;
	}

	final void method3460(int i, ha var_ha) {
		int i_65_ = -124 % ((i + 41) / 62);
		Model class178 = aClass380_6656.method3975(true, true, var_ha, 262144, (byte) 39);
		if (class178 != null) {
			int i_66_ = tileX >> 9;
			int i_67_ = tileY >> 9;
			Class373 class373 = var_ha.f();
			class373.method3902(tileX, anInt5213, tileY);
			aClass380_6656.method3977(class373, false, class178, i_66_, false, var_ha, i_67_, i_66_, i_67_);
		}
	}

	final void method3472(byte i) {
		int i_68_ = -4 / ((-56 - i) / 38);
		throw new IllegalStateException();
	}

	public final void method58(int i) {
		if (i != -19727) {
			/* empty */
		}
	}

	public final int method57(byte i) {
		if (i <= 83)
			aBoolean6652 = true;
		return aClass380_6656.anInt3216;
	}

	public final int method54(int i) {
		if (i != -11077)
			aClass292_6659 = null;
		return aClass380_6656.anInt3203;
	}

	public final void method56(ha var_ha, byte i) {
		if (i != -117)
			aBoolean6652 = true;
		aClass380_6656.method3976(262144, var_ha);
	}

	final boolean method3459(int i) {
		if (i != 0)
			method3469(38);
		return false;
	}

	public final boolean method55(byte i) {
		if (i != -57)
			aClass96_6654 = null;
		return aClass380_6656.method3988(4);
	}

	final void method3467(int i, int i_69_, Class338_Sub3 class338_sub3, boolean bool, int i_70_, int i_71_, ha var_ha) {
		int i_72_ = 67 % ((i_71_ - 20) / 48);
		throw new IllegalStateException();
	}

	public final int method59(int i) {
		if (i < 16)
			method60((byte) 26, null);
		return aClass380_6656.anInt3200;
	}

	public static void method3578(int i) {
		aClass292_6659 = null;
		aClass231_6657 = null;
		int i_73_ = 109 / ((-17 - i) / 52);
		anIntArray6653 = null;
	}
}
