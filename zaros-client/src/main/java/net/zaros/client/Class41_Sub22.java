package net.zaros.client;

/* Class41_Sub22 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub22 extends Class41 {
	static int anInt3800;

	static final int method480(int i, int i_0_, int i_1_) {
		i_0_ = i_0_ * (i & 0x7f) >> 7;
		if (i_1_ != -2145109337)
			return -86;
		if (i_0_ >= 2) {
			if (i_0_ > 126)
				i_0_ = 126;
		} else
			i_0_ = 2;
		return i_0_ + (i & 0xff80);
	}

	final void method381(int i, byte i_8_) {
		anInt389 = i;
		if (i_8_ != -110)
			anInt3800 = -122;
	}

	final void method386(int i) {
		if (i != 2)
			anInt3800 = -127;
		if (aClass296_Sub50_392.method3050(28520) != Class363.runescape)
			anInt389 = 1;
		else if (aClass296_Sub50_392.method3054(104))
			anInt389 = 0;
		if (anInt389 != 0 && anInt389 != 1)
			anInt389 = method383((byte) 110);
	}

	static final void method482(int i, int i_9_, int i_10_, Class18 class18, ha var_ha, Class296_Sub53 class296_sub53, Class256 class256, int i_11_, int i_12_) {
		int i_13_ = 105 / ((-86 - i_12_) / 34);
		int i_14_ = -(i_10_ / 2) + i_9_ - 5;
		int i_15_ = i_11_ + 2;
		if (class18.anInt206 != 0)
			var_ha.method1088(i_14_, i_10_ + 10, class18.anInt206, 1, i_15_, -i_15_ + (i * class256.method2230() + i_11_ + 1));
		if (class18.anInt201 != 0)
			var_ha.method1086(i_15_, i_14_, class18.anInt201, (byte) 125, i * class256.method2230() + i_11_ - i_15_ + 1, i_10_ + 10);
		int i_16_ = class18.anInt225;
		if (class296_sub53.aBoolean5044 && class18.anInt204 != -1)
			i_16_ = class18.anInt204;
		for (int i_17_ = 0; i > i_17_; i_17_++) {
			String string = Class57.aStringArray666[i_17_];
			if (i - 1 > i_17_)
				string = string.substring(0, string.length() - 4);
			class256.method2233(var_ha, string, i_9_, i_11_, i_16_, true);
			i_11_ += class256.method2230();
		}
	}

	Class41_Sub22(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	Class41_Sub22(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final boolean method483(int i) {
		if (i != -25952)
			anInt3800 = -23;
		if (aClass296_Sub50_392.method3054(96))
			return false;
		if (aClass296_Sub50_392.method3050(28520) == Class363.runescape)
			return true;
		return false;
	}

	final int method484(int i) {
		if (i < 114)
			anInt3800 = -64;
		return anInt389;
	}

	final int method380(int i, byte i_18_) {
		if (aClass296_Sub50_392.method3054(102))
			return 3;
		if (i_18_ != 41)
			method483(-2);
		if (aClass296_Sub50_392.method3050(i_18_ + 28479) == Class363.runescape)
			return 1;
		return 3;
	}

	final int method383(byte i) {
		if (i != 110)
			anInt3800 = 65;
		return 1;
	}
}
