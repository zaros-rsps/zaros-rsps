package net.zaros.client;

/* Class272 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class272 {
	byte aByte2518;
	static IncomingPacket aClass231_2519 = new IncomingPacket(33, 6);
	byte aByte2520;
	Interface5 anInterface5_2521;
	short aShort2522;

	public static void method2310(byte i) {
		aClass231_2519 = null;
		if (i < 11)
			method2310((byte) 2);
	}

	Class272(Interface5 interface5, int i, int i_0_, int i_1_) {
		aShort2522 = (short) i;
		anInterface5_2521 = interface5;
		aByte2520 = (byte) i_1_;
		aByte2518 = (byte) i_0_;
	}

	static final byte[] method2311(byte[] is, int i, int i_2_) {
		int i_3_ = -91 / ((42 - i_2_) / 53);
		byte[] is_4_ = new byte[i];
		ArrayTools.removeElement(is, 0, is_4_, 0, i);
		return is_4_;
	}

	static final Class259 method2312(boolean bool, Js5 class138, String string, byte i) {
		int i_5_ = class138.getFileIndex(string);
		if (i_5_ == -1)
			return new Class259(0);
		int[] is = class138.getChildIndicies(i_5_);
		if (i != 91)
			aClass231_2519 = null;
		Class259 class259 = new Class259(is.length);
		int i_6_ = 0;
		int i_7_ = 0;
		while (class259.anInt2417 > i_6_) {
			Packet class296_sub17 = new Packet(class138.getFile(i_5_, is[i_7_++]));
			int i_8_ = class296_sub17.g4();
			int i_9_ = class296_sub17.g2();
			int i_10_ = class296_sub17.g1();
			if (!bool && i_10_ == 1)
				class259.anInt2417--;
			else {
				class259.anIntArray2418[i_6_] = i_8_;
				class259.anIntArray2420[i_6_] = i_9_;
				i_6_++;
			}
		}
		return class259;
	}
}
