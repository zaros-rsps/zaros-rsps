package net.zaros.client;

/* Class41_Sub17 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub17 extends Class41 {
	static SubInPacket aClass260_3782 = new SubInPacket(4, 7);
	static int anInt3783 = 0;
	static int anInt3784 = 500;
	static boolean somethingWithIgnore1 = false;

	Class41_Sub17(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final int method456(int i) {
		if (i <= 114)
			anInt3783 = 75;
		return anInt389;
	}

	static final void method457(Class296_Sub53 class296_sub53, byte i, ha var_ha, Class18 class18) {
		Sprite class397 = class18.method276(var_ha, 17);
		if (class397 != null) {
			int i_0_ = class397.method4087();
			if (class397.method4092() > i_0_)
				i_0_ = class397.method4092();
			int i_1_ = 10;
			int i_2_ = class296_sub53.anInt5041;
			int i_3_ = class296_sub53.anInt5043;
			int i_4_ = 0;
			int i_5_ = 0;
			int i_6_ = 0;
			if (class18.aString191 != null) {
				i_4_ = Class154_Sub1.aClass92_4292.method846(null, (Class57.aStringArray666), (byte) -75, class18.aString191, null);
				for (int i_7_ = 0; i_4_ > i_7_; i_7_++) {
					String string = Class57.aStringArray666[i_7_];
					if (i_4_ - 1 > i_7_)
						string = string.substring(0, string.length() - 4);
					int i_8_ = Class134.aClass256_1386.method2234(string);
					if (i_8_ > i_5_)
						i_5_ = i_8_;
				}
				i_6_ = (i_4_ * Class134.aClass256_1386.method2230() + Class134.aClass256_1386.method2232() / 2);
			}
			int i_9_ = i_0_ / 2 + class296_sub53.anInt5041;
			if (i > -29)
				aClass260_3782 = null;
			int i_10_ = class296_sub53.anInt5043;
			if (Class106.anInt1130 + i_0_ <= i_2_) {
				if (-i_0_ + Class106.anInt1123 < i_2_) {
					i_2_ = Class106.anInt1123 - i_0_;
					i_9_ = -i_1_ + (-(i_0_ / 2) + Class106.anInt1123 - i_5_ / 2 - 5);
				}
			} else {
				i_2_ = Class106.anInt1130;
				i_9_ = Class106.anInt1130 + i_0_ / 2 - (-i_1_ - i_5_ / 2) + 5;
			}
			if (i_3_ >= i_0_ + Class106.anInt1126) {
				if (i_3_ > -i_0_ + Class106.anInt1129) {
					i_3_ = -i_0_ + Class106.anInt1129;
					i_10_ = -(i_0_ / 2) + (Class106.anInt1129 - (i_1_ + i_6_));
				}
			} else {
				i_3_ = Class106.anInt1126;
				i_10_ = i_1_ + (Class106.anInt1126 + i_0_ / 2);
			}
			int i_11_ = (int) (Math.atan2((double) (i_2_ - class296_sub53.anInt5041), (double) (i_3_ - class296_sub53.anInt5043)) / 3.141592653589793 * 32767.0) & 0xffff;
			class397.method4081((float) i_2_ + (float) i_0_ / 2.0F, (float) i_0_ / 2.0F + (float) i_3_, 4096, i_11_);
			int i_12_ = -2;
			int i_13_ = -2;
			int i_14_ = -2;
			int i_15_ = -2;
			if (class18.aString191 != null) {
				i_13_ = i_10_;
				i_12_ = -(i_5_ / 2) + i_9_ - 5;
				i_14_ = i_12_ + i_5_ + 10;
				i_15_ = i_13_ + i_4_ * Class134.aClass256_1386.method2230() + 3;
				if (class18.anInt206 != 0)
					var_ha.method1088(i_12_, -i_12_ + i_14_, class18.anInt206, 1, i_13_, i_15_ - i_13_);
				if (class18.anInt201 != 0)
					var_ha.method1086(i_13_, i_12_, class18.anInt201, (byte) 124, -i_13_ + i_15_, i_14_ - i_12_);
				for (int i_16_ = 0; i_16_ < i_4_; i_16_++) {
					String string = Class57.aStringArray666[i_16_];
					if (i_16_ < i_4_ - 1)
						string = string.substring(0, string.length() - 4);
					Class134.aClass256_1386.method2233(var_ha, string, i_9_, i_10_, class18.anInt225, true);
					i_10_ += Class134.aClass256_1386.method2230();
				}
			}
			if (class18.anInt203 != -1 || class18.aString191 != null) {
				Class296_Sub38 class296_sub38 = new Class296_Sub38(class296_sub53);
				i_0_ >>= 1;
				class296_sub38.anInt4896 = i_3_ - i_0_;
				class296_sub38.anInt4890 = i_2_ + i_0_;
				class296_sub38.anInt4897 = i_2_ - i_0_;
				class296_sub38.anInt4895 = i_13_;
				class296_sub38.anInt4894 = i_0_ + i_3_;
				class296_sub38.anInt4888 = i_12_;
				class296_sub38.anInt4893 = i_15_;
				class296_sub38.anInt4887 = i_14_;
				Class366_Sub8.aClass155_5409.addLast((byte) -90, class296_sub38);
			}
		}
	}

	static final void method458(int i, int i_17_, int i_18_, int i_19_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_17_, i_18_);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.anInt6145 = i;
		class296_sub39_sub5.intParam = i_19_;
	}

	final boolean method459(boolean bool) {
		if (bool != true)
			return true;
		return CS2Script.method2797(106, anInt389);
	}

	final int method380(int i, byte i_20_) {
		if (CS2Script.method2797(119, i)) {
			if (aClass296_Sub50_392.aClass41_Sub30_5006.method527(i_20_ - 167) && !Class368_Sub11.method3844(90, aClass296_Sub50_392.aClass41_Sub30_5006.method521(120)))
				return 3;
			if (aClass296_Sub50_392.aClass41_Sub13_4989.method444(118) == 1)
				return 3;
		}
		if (i_20_ != 41)
			return 101;
		if (i == 3)
			return 3;
		if (CS2Script.method2797(106, i))
			return 2;
		return 1;
	}

	public static void method460(int i) {
		aClass260_3782 = null;
		if (i != 24346)
			method457(null, (byte) -37, null, null);
	}

	final void method381(int i, byte i_21_) {
		anInt389 = i;
		if (i_21_ != -110)
			method381(112, (byte) -121);
	}

	final boolean method461(int i) {
		if (i != -25952)
			method459(true);
		return true;
	}

	final void method386(int i) {
		if (i != 2)
			somethingWithIgnore1 = false;
		if (method459(true)) {
			if (aClass296_Sub50_392.aClass41_Sub30_5006.method527(-88) && !Class368_Sub11.method3844(111, aClass296_Sub50_392.aClass41_Sub30_5006.method521(116)))
				anInt389 = 1;
			if (aClass296_Sub50_392.aClass41_Sub13_4989.method444(116) == 1)
				anInt389 = 1;
		}
		if (anInt389 == 3)
			anInt389 = 2;
		if (anInt389 < 0 || anInt389 > 3)
			anInt389 = method383((byte) 110);
	}

	Class41_Sub17(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final int method383(byte i) {
		if (i != 110)
			return -49;
		return 0;
	}
}
