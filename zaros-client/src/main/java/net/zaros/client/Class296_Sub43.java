package net.zaros.client;

/* Class296_Sub43 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub43 extends Node {
	static BITConfigsLoader bitConfigsLoader;
	int[] anIntArray4938;
	static IncomingPacket aClass231_4939 = new IncomingPacket(37, 6);
	static int[] anIntArray4940;

	static final int method2921(int i, byte i_0_, int i_1_) {
		int i_2_ = i + i_1_ * 57;
		i_2_ = i_2_ << 13 ^ i_2_;
		if (i_0_ != -11)
			return -3;
		int i_3_ = i_2_ * (i_2_ * 15731 * i_2_ + 789221) + 1376312589 & 0x7fffffff;
		return i_3_ >> 19 & 0xff;
	}

	static final String method2922(byte i, long l) {
		if (l <= 0L || l >= 6582952005840035281L)
			return null;
		if (l % 37L == 0L)
			return null;
		int i_4_ = 0;
		for (long l_5_ = l; l_5_ != 0L; l_5_ /= 37L)
			i_4_++;
		StringBuffer stringbuffer = new StringBuffer(i_4_);
		while (l != 0L) {
			long l_6_ = l;
			l /= 37L;
			char c = (Class296_Sub51_Sub2.aCharArray6339[(int) (l_6_ + -(l * 37L))]);
			if (c == '_') {
				int i_7_ = stringbuffer.length() - 1;
				stringbuffer.setCharAt(i_7_, Character.toUpperCase(stringbuffer.charAt(i_7_)));
				c = '\u00a0';
			}
			stringbuffer.append(c);
		}
		stringbuffer.reverse();
		int i_8_ = -52 / ((i + 84) / 41);
		stringbuffer.setCharAt(0, Character.toUpperCase(stringbuffer.charAt(0)));
		return stringbuffer.toString();
	}

	public static void method2923(int i) {
		if (i == 1376312589) {
			aClass231_4939 = null;
			bitConfigsLoader = null;
			anIntArray4940 = null;
		}
	}

	Class296_Sub43(int i) {
		anIntArray4938 = new int[i];
	}
}
