package net.zaros.client;

import net.zaros.client.logic.clans.settings.ClanSettings;

/* Class296_Sub51_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub2 extends TextureOperation {
	static boolean aBoolean6338 = false;
	static char[] aCharArray6339 = {'_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	static ClanSettings affinedClanSettings;
	private int anInt6341 = 585;
	static Class343_Sub1[] cacheFileWorkers = new Class343_Sub1[37];

	final int[] get_monochrome_output(int i, int i_0_) {
		if (i != 0)
			aBoolean6338 = false;
		int[] is = aClass318_5035.method3335(i_0_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int i_1_ = Class294.anIntArray2686[i_0_];
			for (int i_2_ = 0; Class41_Sub10.anInt3769 > i_2_; i_2_++) {
				int i_3_ = Class33.anIntArray334[i_2_];
				if (i_3_ > anInt6341 && i_3_ < 4096 - anInt6341 && i_1_ > 2048 - anInt6341 && i_1_ < anInt6341 + 2048) {
					int i_4_ = 2048 - i_3_;
					i_4_ = i_4_ < 0 ? -i_4_ : i_4_;
					i_4_ <<= 12;
					i_4_ /= -anInt6341 + 2048;
					is[i_2_] = -i_4_ + 4096;
				} else if (-anInt6341 + 2048 < i_3_ && 2048 + anInt6341 > i_3_) {
					int i_5_ = i_1_ - 2048;
					i_5_ = i_5_ < 0 ? -i_5_ : i_5_;
					i_5_ -= anInt6341;
					i_5_ <<= 12;
					is[i_2_] = i_5_ / (-anInt6341 + 2048);
				} else if (anInt6341 > i_1_ || i_1_ > -anInt6341 + 4096) {
					int i_6_ = i_3_ - 2048;
					i_6_ = i_6_ < 0 ? -i_6_ : i_6_;
					i_6_ -= anInt6341;
					i_6_ <<= 12;
					is[i_2_] = i_6_ / (-anInt6341 + 2048);
				} else if (i_3_ < anInt6341 || 4096 - anInt6341 < i_3_) {
					int i_7_ = 2048 - i_1_;
					i_7_ = i_7_ < 0 ? -i_7_ : i_7_;
					i_7_ <<= 12;
					i_7_ /= -anInt6341 + 2048;
					is[i_2_] = -i_7_ + 4096;
				} else
					is[i_2_] = 0;
			}
		}
		return is;
	}

	final void method3071(int i, Packet class296_sub17, int i_8_) {
		if (i > -84)
			anInt6341 = 69;
		int i_9_ = i_8_;
		if (i_9_ == 0)
			anInt6341 = class296_sub17.g2();
	}

	public static void method3081(int i) {
		int i_10_ = 95 % ((i + 71) / 37);
		cacheFileWorkers = null;
		affinedClanSettings = null;
		aCharArray6339 = null;
	}

	public Class296_Sub51_Sub2() {
		super(0, true);
	}
}
