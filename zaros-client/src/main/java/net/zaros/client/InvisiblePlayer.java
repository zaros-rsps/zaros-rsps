package net.zaros.client;

/* Class195 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class InvisiblePlayer {
	static float aFloat1976;
	static Class279 aClass279_1977;
	static int[] anIntArray1978 = new int[500];
	boolean aBoolean1979;
	boolean aBoolean1980;
	int locationHash;
	static InterfaceComponent aClass51_1982 = null;
	int someFacingDirection;
	int interactingWithIndex;

	public static void method1931(int i) {
		aClass279_1977 = null;
		aClass51_1982 = null;
		if (i < 20) {
			aFloat1976 = 0.07152912F;
		}
		anIntArray1978 = null;
	}

	public InvisiblePlayer() {
		/* empty */
	}

	static final ParticleEmitterRaw method1932(int i, int emitterid) {
		ParticleEmitterRaw emitter_raw = (ParticleEmitterRaw) aa.aClass113_49.get(emitterid);
		if (emitter_raw != null) {
			return emitter_raw;
		}
		byte[] is = Class296_Sub39_Sub11.aClass138_6189.getFile(0, emitterid);
		emitter_raw = new ParticleEmitterRaw();
		if (is != null) {
			emitter_raw.method1666(new Packet(is));
		}
		emitter_raw.method1667(24);
		aa.aClass113_49.put(emitter_raw, emitterid);
		return emitter_raw;
	}
}
