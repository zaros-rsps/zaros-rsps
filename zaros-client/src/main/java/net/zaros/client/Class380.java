package net.zaros.client;

/* Class380 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class380 {
	private boolean[] aBooleanArray3198;
	private Class375 aClass375_3199;
	int anInt3200;
	static Class338_Sub1[] aClass338_Sub1Array3201;
	Class338_Sub1 aClass338_Sub1_3202;
	int anInt3203;
	private boolean aBoolean3204 = false;
	private r aR3205;
	static int anInt3206 = -1;
	private Model aClass178_3207;
	private boolean aBoolean3208;
	private byte aByte3209;
	static boolean aBoolean3210 = false;
	private boolean aBoolean3211;
	private int anInt3212 = -1;
	private boolean aBoolean3213;
	private int anInt3214;
	private int anInt3215;
	int anInt3216;
	private byte aByte3217;
	private Animator aClass44_3218;
	private int anInt3219;
	private Class338_Sub3 aClass338_Sub3_3220;

	Class380(ha var_ha, ObjectDefinition class70, int i, int i_37_, int i_38_, int i_39_, Class338_Sub3 class338_sub3, boolean bool, int i_40_) {
		aBoolean3208 = false;
		aBoolean3213 = false;
		anInt3214 = 0;
		anInt3219 = 0;
		aClass338_Sub3_3220 = class338_sub3;
		anInt3200 = i_37_;
		anInt3203 = i;
		aByte3209 = (byte) i_38_;
		aBoolean3208 = bool;
		aBoolean3204 = i_40_ != -1;
		aByte3217 = (byte) i_39_;
		anInt3216 = class70.ID;
		aBoolean3211 = var_ha.B() && class70.aBoolean779 && !aBoolean3208;
		aClass44_3218 = new Class44_Sub1(class338_sub3, false);
		method3986(false, i_40_, 1, false);
	}

	final Model method3975(boolean bool, boolean bool_0_, ha var_ha, int i, byte i_1_) {
		ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(anInt3216);
		if (class70.transforms != null) {
			class70 = class70.method757(za_Sub1.anInt6554 == 3 ? (IConfigsRegister) Class205_Sub1.anInterface17_5640 : Class16_Sub3_Sub1.configsRegister, false);
		}
		if (class70 == null) {
			method3987(var_ha, (byte) -53);
			anInt3212 = -1;
			return null;
		}
		if (!aBoolean3204 && anInt3212 != class70.ID) {
			method3986(false, -1, 0, true);
			aBoolean3213 = false;
			aClass178_3207 = null;
		}
		method3983(aClass338_Sub3_3220, (byte) 85);
		if (bool) {
			bool = bool & aBoolean3211 & !aBoolean3213 & Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method465(124) != 0;
		}
		if (bool_0_ && !bool) {
			anInt3212 = class70.ID;
			return null;
		}
		if (bool) {
			Class296_Sub45_Sub4.method3017(aR3205, aByte3217, aClass338_Sub3_3220.tileX, aClass338_Sub3_3220.tileY, aBooleanArray3198);
			aBoolean3213 = false;
		}
		s var_s = Class360_Sub2.aSArray5304[aByte3217];
		s var_s_2_;
		if (aBoolean3208) {
			var_s_2_ = Class244.aSArray2320[0];
		} else {
			var_s_2_ = aByte3217 < 3 ? Class360_Sub2.aSArray5304[aByte3217 + 1] : null;
		}
		Model class178 = null;
		if (aClass44_3218.method570((byte) 40)) {
			if (bool) {
				i |= 0x40000;
			}
			class178 = class70.method765(aClass338_Sub3_3220.tileY, aClass44_3218, var_s_2_, i, (byte) -123, anInt3203 == 11 ? 10 : anInt3203, var_ha, aClass338_Sub3_3220.tileX, var_s, anInt3203 != 11 ? anInt3200 : anInt3200 + 4, aClass375_3199, var_s.method3349(0, aClass338_Sub3_3220.tileY, aClass338_Sub3_3220.tileX));
			if (class178 == null) {
				anInt3214 = 0;
				aR3205 = null;
				anInt3219 = 0;
				aBooleanArray3198 = null;
			} else {
				if (bool) {
					if (aBooleanArray3198 == null) {
						aBooleanArray3198 = new boolean[4];
					}
					aR3205 = class178.ba(aR3205);
					Class296_Sub39_Sub20_Sub2.method2910(aR3205, aByte3217, aClass338_Sub3_3220.tileX, aClass338_Sub3_3220.tileY, aBooleanArray3198);
					aBoolean3213 = true;
				}
				anInt3219 = class178.fa();
				anInt3214 = class178.ma();
			}
			aClass178_3207 = null;
		} else if (aClass178_3207 != null && (i & aClass178_3207.ua()) == i && anInt3212 == class70.ID) {
			class178 = aClass178_3207;
		} else {
			if (aClass178_3207 != null) {
				i |= aClass178_3207.ua();
			}
			Class188 class188 = class70.method750(var_s, var_s_2_, var_ha, bool, -954198435, aClass338_Sub3_3220.tileY, aClass338_Sub3_3220.tileX, i, aClass375_3199, anInt3203 != 11 ? anInt3200 : 4 + anInt3200, var_s.method3349(0, aClass338_Sub3_3220.tileY, aClass338_Sub3_3220.tileX), anInt3203 == 11 ? 10 : anInt3203);
			if (class188 != null) {
				aClass178_3207 = class178 = class188.aClass178_1926;
				if (bool) {
					aR3205 = class188.aR1925;
					aBooleanArray3198 = null;
					Class296_Sub39_Sub20_Sub2.method2910(aR3205, aByte3217, aClass338_Sub3_3220.tileX, aClass338_Sub3_3220.tileY, null);
					aBoolean3213 = true;
				}
				anInt3219 = class178.fa();
				anInt3214 = class178.ma();
			} else {
				anInt3219 = 0;
				aR3205 = null;
				aClass178_3207 = null;
				anInt3214 = 0;
				aBooleanArray3198 = null;
			}
		}
		anInt3212 = class70.ID;
		return class178;
	}

	final void method3976(int i, ha var_ha) {
		method3975(true, true, var_ha, i, (byte) 39);
	}

	final void method3977(Class373 class373, boolean bool, Model class178, int i, boolean bool_3_, ha var_ha, int i_4_, int i_5_, int i_6_) {
		if (bool_3_) {
			method3979(-93, 96, -45, null, -69, -32, 72, 127, null);
		}
		EmissiveTriangle[] class89s = class178.method1735();
		EffectiveVertex[] class232s = class178.method1729();
		if ((aClass338_Sub1_3202 == null || aClass338_Sub1_3202.aBoolean5177) && (class89s != null || class232s != null)) {
			ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(anInt3216);
			if (class70.transforms != null) {
				class70 = class70.method757(za_Sub1.anInt6554 == 3 ? (IConfigsRegister) Class205_Sub1.anInterface17_5640 : Class16_Sub3_Sub1.configsRegister, bool_3_);
			}
			if (class70 != null) {
				aClass338_Sub1_3202 = Class338_Sub1.method3442(Class29.anInt307, true);
			}
		}
		if (aClass338_Sub1_3202 != null) {
			class178.method1718(class373);
			if (!bool) {
				aClass338_Sub1_3202.method3441(Class29.anInt307);
			} else {
				aClass338_Sub1_3202.method3452(var_ha, Class29.anInt307, class89s, class232s, false);
			}
			aClass338_Sub1_3202.method3450(aByte3209, i_5_, i, i_6_, i_4_);
		}
	}

	public static void method3978(int i) {
		int i_7_ = -67 / ((55 - i) / 63);
		aClass338_Sub1Array3201 = null;
	}

	static final void method3979(int i, int i_8_, int i_9_, byte[] is, int i_10_, int i_11_, int i_12_, int i_13_, byte[] is_14_) {
		int i_15_ = -119 / ((4 - i_10_) / 40);
		int i_16_ = -(i_8_ >> 2);
		i_8_ = -(i_8_ & 0x3);
		for (int i_17_ = -i_9_; i_17_ < 0; i_17_++) {
			for (int i_18_ = i_16_; i_18_ < 0; i_18_++) {
				is_14_[i_13_++] -= is[i_11_++];
				is_14_[i_13_++] -= is[i_11_++];
				is_14_[i_13_++] -= is[i_11_++];
				is_14_[i_13_++] -= is[i_11_++];
			}
			for (int i_19_ = i_8_; i_19_ < 0; i_19_++) {
				is_14_[i_13_++] -= is[i_11_++];
			}
			i_13_ += i_12_;
			i_11_ += i;
		}
	}

	final int method3980(boolean bool) {
		if (bool != true) {
			anInt3215 = 4;
		}
		return anInt3219;
	}

	final void method3981(int i, byte i_20_) {
		aBoolean3204 = true;
		if (i_20_ >= -68) {
			anInt3215 = 78;
		}
		method3986(false, i, 1, false);
	}

	final int method3982(int i) {
		if (i != 14356) {
			aR3205 = null;
		}
		return anInt3214;
	}

	private final void method3983(Class338_Sub3 class338_sub3, byte i) {
		if (!aClass44_3218.method570((byte) 40)) {
			method3986(false, -1, 0, false);
		} else if (aClass44_3218.method559(-anInt3215 + Class29.anInt307, (byte) 127)) {
			if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method465(117) == 2) {
				aBoolean3213 = false;
			}
			if (aClass44_3218.method546(57)) {
				aClass44_3218.method549((byte) 115, -1);
				aBoolean3204 = false;
				method3986(false, -1, 0, false);
			}
		}
		anInt3215 = Class29.anInt307;
		if (i < 70) {
			anInt3203 = -79;
		}
	}

	final void method3984(int i, Class375 class375) {
		if (i != -5581) {
			aBoolean3204 = false;
		}
		aClass375_3199 = class375;
		aClass178_3207 = null;
	}

	static final boolean method3985(int i, int i_21_, int i_22_, int i_23_, int i_24_, byte i_25_, byte[] is) {
		int i_26_ = i % i_23_;
		int i_27_;
		if (i_26_ != 0) {
			i_27_ = -i_26_ + i_23_;
		} else {
			i_27_ = 0;
		}
		int i_28_ = -((i_24_ - 1 + i_23_) / i_23_);
		int i_29_ = -((i_23_ - 1 + i) / i_23_);
		if (i_25_ != 41) {
			anInt3206 = -128;
		}
		for (int i_30_ = i_28_; i_30_ < 0; i_30_++) {
			for (int i_31_ = i_29_; i_31_ < 0; i_31_++) {
				if (is[i_21_] == 0) {
					return true;
				}
				i_21_ += i_23_;
			}
			i_21_ -= i_27_;
			if (is[i_21_ - 1] == 0) {
				return true;
			}
			i_21_ += i_22_;
		}
		return false;
	}

	private final void method3986(boolean bool, int i, int i_32_, boolean bool_33_) {
		int i_34_ = i;
		boolean bool_35_ = bool;
		if (i_34_ == -1) {
			ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(anInt3216);
			ObjectDefinition class70_36_ = class70;
			if (class70.transforms != null) {
				class70 = class70.method757(za_Sub1.anInt6554 == 3 ? (IConfigsRegister) Class205_Sub1.anInterface17_5640 : Class16_Sub3_Sub1.configsRegister, false);
			}
			if (class70 == null) {
				return;
			}
			if (class70 == class70_36_) {
				class70_36_ = null;
			}
			if (class70.method752(-119)) {
				if (bool_33_ && aClass44_3218.method570((byte) 40) && class70.method756((byte) -70, aClass44_3218.method557((byte) -109))) {
					return;
				}
				if (anInt3212 != class70.ID) {
					bool_35_ = class70.aBoolean838;
				}
				i_34_ = class70.method751(true);
				if (!class70.method760((byte) 106)) {
					i_32_ = 1;
				} else {
					i_32_ = 0;
				}
			} else if (class70_36_ != null && class70_36_.method752(-122)) {
				if (bool_33_ && aClass44_3218.method570((byte) 40) && class70_36_.method756((byte) -70, aClass44_3218.method557((byte) -121))) {
					return;
				}
				if (anInt3212 != class70.ID) {
					bool_35_ = class70_36_.aBoolean838;
				}
				i_34_ = class70_36_.method751(!bool);
				if (class70_36_.method760((byte) -89)) {
					i_32_ = 0;
				} else {
					i_32_ = 1;
				}
			}
		}
		if (i_34_ == -1) {
			aClass44_3218.method553(-117, -1, false);
		} else {
			aClass44_3218.method548(0, i_32_, i_34_, bool_35_, 21847);
			aClass178_3207 = null;
			aBoolean3213 = false;
			anInt3215 = Class29.anInt307;
		}
	}

	@Override
	protected final void finalize() {
		if (aClass338_Sub1_3202 != null) {
			aClass338_Sub1_3202.method3439();
		}
	}

	final void method3987(ha var_ha, byte i) {
		if (i == -53) {
			if (aR3205 != null) {
				Class296_Sub45_Sub4.method3017(aR3205, aByte3217, aClass338_Sub3_3220.tileX, aClass338_Sub3_3220.tileY, aBooleanArray3198);
				aR3205 = null;
				aBooleanArray3198 = null;
			}
		}
	}

	final boolean method3988(int i) {
		if (i != 4) {
			method3980(true);
		}
		return aBoolean3211;
	}

}
