package net.zaros.client;

/* Class296_Sub39_Sub15 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class296_Sub39_Sub15 extends Queuable {
	static float aFloat6223;
	static OutgoingPacket aClass311_6224 = new OutgoingPacket(75, 9);
	Interface12 anInterface12_6225;
	int anInt6226;

	public static void method2881(byte i) {
		if (i >= 97) {
			aClass311_6224 = null;
		}
	}

	abstract boolean method2882(int i);

	abstract Object method2883(byte i);

	Class296_Sub39_Sub15(Interface12 interface12, int i) {
		anInterface12_6225 = interface12;
		anInt6226 = i;
	}
}
