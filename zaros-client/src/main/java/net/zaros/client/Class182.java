package net.zaros.client;
import jaggl.OpenGL;

final class Class182 {
	private AdvancedMemoryCache aClass113_1875 = new AdvancedMemoryCache(16);
	static int anInt1876;
	static int[] bitMasks = new int[32];
	private Js5 aClass138_1878;

	public static void method1842(int i) {
		if (i <= 98)
			method1852(18, 69, 51);
		bitMasks = null;
	}

	static final void method1843(int i) {
		if (i == 71) {
			if (Class398.aString3343.toLowerCase().indexOf("microsoft") == -1) {
				Class92.anIntArray994[61] = 27;
				Class92.anIntArray994[91] = 42;
				Class92.anIntArray994[45] = 26;
				Class92.anIntArray994[93] = 43;
				Class92.anIntArray994[59] = 57;
				Class92.anIntArray994[46] = 72;
				if (Class398.aMethod3347 != null) {
					Class92.anIntArray994[520] = 59;
					Class92.anIntArray994[192] = 28;
					Class92.anIntArray994[222] = 58;
				} else {
					Class92.anIntArray994[222] = 59;
					Class92.anIntArray994[192] = 58;
				}
				Class92.anIntArray994[92] = 74;
				Class92.anIntArray994[44] = 71;
				Class92.anIntArray994[47] = 73;
			} else {
				Class92.anIntArray994[222] = 59;
				Class92.anIntArray994[186] = 57;
				Class92.anIntArray994[188] = 71;
				Class92.anIntArray994[223] = 28;
				Class92.anIntArray994[191] = 73;
				Class92.anIntArray994[187] = 27;
				Class92.anIntArray994[220] = 74;
				Class92.anIntArray994[190] = 72;
				Class92.anIntArray994[221] = 43;
				Class92.anIntArray994[219] = 42;
				Class92.anIntArray994[192] = 58;
				Class92.anIntArray994[189] = 26;
			}
		}
	}

	static final void method1844(int i, int i_0_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_0_, i);
		class296_sub39_sub5.insertIntoQueue();
	}

	final void method1845(int i, int i_1_) {
		if (i != 58)
			anInt1876 = 72;
		synchronized (aClass113_1875) {
			aClass113_1875.clean(i_1_);
		}
	}

	static final String method1846(byte[] is, int i) {
		if (i <= 90)
			method1848(null, -37, -123);
		return Class360.method3722(0, is, is.length, (byte) 75);
	}

	final void method1847(boolean bool) {
		synchronized (aClass113_1875) {
			aClass113_1875.clear();
			if (bool != true)
				anInt1876 = 79;
		}
	}

	static final short[] method1848(short[] is, int i, int i_2_) {
		short[] is_3_ = new short[i_2_];
		if (i != 74)
			bitMasks = null;
		Class291.copy(is, 0, is_3_, 0, i_2_);
		return is_3_;
	}

	static final Class179 method1849(ha_Sub1_Sub1 var_ha_Sub1_Sub1, byte[] is, int i, byte i_4_) {
		if (is == null || is.length == 0)
			return null;
		long l = OpenGL.glCreateShaderObjectARB(i);
		OpenGL.glShaderSourceRawARB(l, is);
		OpenGL.glCompileShaderARB(l);
		OpenGL.glGetObjectParameterivARB(l, 35713, CS2Call.anIntArray4957, 0);
		if (i_4_ != -115)
			return null;
		if (CS2Call.anIntArray4957[0] == 0) {
			if (CS2Call.anIntArray4957[0] == 0)
				System.out.println("Shader compile failed:");
			OpenGL.glGetObjectParameterivARB(l, 35716, CS2Call.anIntArray4957, 1);
			if (CS2Call.anIntArray4957[1] > 1) {
				byte[] is_5_ = new byte[CS2Call.anIntArray4957[1]];
				OpenGL.glGetInfoLogARB(l, CS2Call.anIntArray4957[1], CS2Call.anIntArray4957, 0, is_5_, 0);
				System.out.println(new String(is_5_));
			}
			if (CS2Call.anIntArray4957[0] == 0) {
				OpenGL.glDeleteObjectARB(l);
				return null;
			}
		}
		return new Class179(var_ha_Sub1_Sub1, l, i);
	}

	final Class187 method1850(int i, boolean bool) {
		Class187 class187;
		synchronized (aClass113_1875) {
			class187 = (Class187) aClass113_1875.get((long) i);
			if (bool)
				method1849(null, null, 5, (byte) -40);
		}
		if (class187 != null)
			return class187;
		byte[] is;
		synchronized (aClass138_1878) {
			is = aClass138_1878.getFile(30, i);
		}
		class187 = new Class187();
		if (is != null)
			class187.method1887((byte) 119, new Packet(is));
		synchronized (aClass113_1875) {
			aClass113_1875.put(class187, (long) i);
		}
		return class187;
	}

	final void method1851(int i) {
		synchronized (aClass113_1875) {
			int i_6_ = -34 % ((i + 60) / 53);
			aClass113_1875.clearSoftReferences();
		}
	}

	static final int method1852(int i, int i_7_, int i_8_) {
		if (i_8_ >= -41)
			method1842(66);
		int i_9_ = i_7_ >>> 31;
		return (i_9_ + i_7_) / i - i_9_;
	}

	Class182(GameType class35, int i, Js5 class138) {
		aClass138_1878 = class138;
		aClass138_1878.getLastFileId(30);
	}

	static {
		int i = 2;
		for (int i_10_ = 0; i_10_ < 32; i_10_++) {
			bitMasks[i_10_] = i - 1;
			i += i;
		}
	}
}
