package net.zaros.client;

public class LongNode extends Node {
	public long value;

	public LongNode() {
		/* empty */
	}

	public LongNode(long l) {
		value = l;
	}

	static final float method2657(float f, float f_0_, float f_1_, int i, int i_2_) {
		int i_3_ = -34 / ((-29 - i_2_) / 35);
		float[] fs = Class125.aFloatArrayArray1288[i];
		return f_0_ * fs[2] + (fs[1] * f_1_ + fs[0] * f);
	}

	static final int method2658(int i) {
		if (i >= -50) {
			return -120;
		}
		return Class131.anInt1342;
	}
}
