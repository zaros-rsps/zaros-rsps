package net.zaros.client;

/* Class147 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class NPCDefinition {
	byte aByte1445;
	private byte[] aByteArray1446;
	byte aByte1447;
	int anInt1448;
	private int[][] anIntArrayArray1449;
	private HashTable extraData;
	int[] configData;
	String name = "null";
	int anInt1453;
	int anInt1454;
	String[] options;
	int anInt1456;
	boolean aBoolean1457;
	boolean isClickable;
	int anInt1459;
	private int bitConfigID;
	private int configID;
	int anInt1462;
	int anInt1463;
	int anInt1464;
	int anInt1465;
	int npcID;
	boolean visibleOnMinimap;
	private int anInt1468;
	byte aByte1469;
	boolean aBoolean1470;
	private byte aByte1471;
	private int anInt1472;
	int renderEmote;
	private byte aByte1474;
	int anInt1475;
	private byte aByte1476;
	int combatLevel;
	static Class268 aClass268_1478;
	private int anInt1479;
	int anInt1480;
	boolean aBoolean1481;
	NPCDefinitionLoader loader;
	boolean aBoolean1483;
	int anInt1484;
	int anInt1485;
	int anInt1486;
	byte aByte1487;
	int size;
	private short[] originalColours;
	short aShort1490;
	short[] aShortArray1491;
	int anInt1492;
	byte aByte1493;
	private byte aByte1494;
	short aShort1495;
	int[] if_models;
	int anInt1497;
	private int anInt1498;
	int[] anIntArray1499;
	int anInt1500;
	int anInt1501;
	int anInt1502;
	boolean aBoolean1503;
	int anInt1504;
	short[] modifiedColours;
	int headIcon;
	int anInt1507;
	private short[] aShortArray1508;
	int[] models;
	int anInt1510;
	boolean aBoolean1511;

	final boolean method1493(boolean bool) {
		if (configData == null) {
			if (anInt1486 == -1 && anInt1500 == -1 && anInt1484 == -1) {
				return false;
			}
			return true;
		}
		for (int i = 0; configData.length > i; i++) {
			if (configData[i] != -1) {
				NPCDefinition class147_0_ = loader.getDefinition(configData[i]);
				if (class147_0_.anInt1486 != -1 || class147_0_.anInt1500 != -1 || class147_0_.anInt1484 != -1) {
					return true;
				}
			}
		}
		if (bool != true) {
			return false;
		}
		return false;
	}

	private final void parseOpcode(int opcode, Packet buffer, boolean newNpc) {
		if (opcode != 1) {
			if (opcode == 2) {
				name = buffer.gstr();
			} else if (opcode != 12) {
				if (opcode >= 30 && opcode < 35) {
					options[opcode - 30] = buffer.gstr();
				} else if (opcode == 40) {
					int i_2_ = buffer.g1();
					modifiedColours = new short[i_2_];
					originalColours = new short[i_2_];
					for (int i_3_ = 0; i_2_ > i_3_; i_3_++) {
						originalColours[i_3_] = (short) buffer.g2();
						modifiedColours[i_3_] = (short) buffer.g2();
					}
				} else if (opcode != 41) {
					if (opcode == 42) {
						int i_4_ = buffer.g1();
						aByteArray1446 = new byte[i_4_];
						for (int i_5_ = 0; i_4_ > i_5_; i_5_++) {
							aByteArray1446[i_5_] = buffer.g1b();
						}
					} else if (44 == opcode) {
						int flag = buffer.g2();
						int bits = 0;
						for (int n = flag; n > 0; n >>= 1) {
							bits++;
						}
						byte[] recolourDstIndices = new byte[bits];
						byte index = 0;
						for (int bit = 0; bit < bits; bit++) {
							if ((flag & 1 << bit) > 0) {
								recolourDstIndices[bit] = index;
								index++;
							} else {
								recolourDstIndices[bit] = (byte) -1;
							}
						}
					} else if (45 == opcode) {
						int flag = buffer.g2();
						int bits = 0;
						for (int n = flag; n > 0; n >>= 1) {
							bits++;
						}
						byte[] retextureDstIndices = new byte[bits];
						byte index = 0;
						for (int bit = 0; bit < bits; bit++) {
							if ((flag & 1 << bit) > 0) {
								retextureDstIndices[bit] = index;
								index++;
							} else {
								retextureDstIndices[bit] = (byte) -1;
							}
						}
					} else if (opcode == 60) {
						int i_6_ = buffer.g1();
						if_models = new int[i_6_];
						for (int index = 0; i_6_ > index; index++) {
							if_models[index] = newNpc ? buffer.gSmart2or4s() : buffer.g2();
						}
					} else if (opcode == 93) {
						visibleOnMinimap = false;
					} else if (opcode != 95) {
						if (opcode == 97) {
							anInt1498 = buffer.g2();
						} else if (opcode != 98) {
							if (opcode != 99) {
								if (opcode != 100) {
									if (opcode == 101) {
										anInt1479 = buffer.g1b() * 5;
									} else if (opcode == 102) {
										headIcon = buffer.g2();
									} else if (opcode != 103) {
										if (opcode == 106 || opcode == 118) {
											bitConfigID = buffer.g2();
											if (bitConfigID == 65535) {
												bitConfigID = -1;
											}
											configID = buffer.g2();
											if (configID == 65535) {
												configID = -1;
											}
											int i_8_ = -1;
											if (opcode == 118) {
												i_8_ = buffer.g2();
												if (i_8_ == 65535) {
													i_8_ = -1;
												}
											}
											int i_9_ = buffer.g1();
											configData = new int[i_9_ + 2];
											for (int i_10_ = 0; i_9_ >= i_10_; i_10_++) {
												configData[i_10_] = buffer.g2();
												if (configData[i_10_] == 65535) {
													configData[i_10_] = -1;
												}
											}
											configData[i_9_ + 1] = i_8_;
										} else if (opcode != 107) {
											if (opcode == 109) {
												aBoolean1470 = false;
											} else if (opcode == 111) {
												aBoolean1457 = false;
											} else if (opcode != 113) {
												if (opcode != 114) {
													if (opcode == 119) {
														aByte1493 = buffer.g1b();
													} else if (opcode == 121) {
														anIntArrayArray1449 = new int[models.length][];
														int i_11_ = buffer.g1();
														for (int i_12_ = 0; i_12_ < i_11_; i_12_++) {
															int i_13_ = buffer.g1();
															int[] is = anIntArrayArray1449[i_13_] = new int[3];
															is[0] = buffer.g1b();
															is[1] = buffer.g1b();
															is[2] = buffer.g1b();
														}
													} else if (opcode != 122) {
														if (opcode != 123) {
															if (opcode != 125) {
																if (opcode == 127) {
																	renderEmote = buffer.g2();
																} else if (opcode != 128) {
																	if (opcode == 134) {
																		anInt1486 = buffer.g2();
																		if (anInt1486 == 65535) {
																			anInt1486 = -1;
																		}
																		anInt1501 = buffer.g2();
																		if (anInt1501 == 65535) {
																			anInt1501 = -1;
																		}
																		anInt1500 = buffer.g2();
																		if (anInt1500 == 65535) {
																			anInt1500 = -1;
																		}
																		anInt1484 = buffer.g2();
																		if (anInt1484 == 65535) {
																			anInt1484 = -1;
																		}
																		anInt1504 = buffer.g1();
																	} else if (opcode == 135) {
																		anInt1480 = buffer.g1();
																		anInt1453 = buffer.g2();
																	} else if (opcode == 136) {
																		anInt1510 = buffer.g1();
																		anInt1475 = buffer.g2();
																	} else if (opcode != 137) {
																		if (opcode != 138) {
																			if (opcode == 139) {
																				anInt1465 = buffer.g2();
																			} else if (opcode == 140) {
																				anInt1456 = buffer.g1();
																			} else if (opcode != 141) {
																				if (opcode != 142) {
																					if (opcode == 143) {
																						aBoolean1483 = true;
																					} else if (opcode >= 150 && opcode < 155) {
																						options[opcode - 150] = buffer.gstr();
																						if (!loader.aBoolean1400) {
																							options[opcode - 150] = null;
																						}
																					} else if (opcode == 155) {
																						aByte1476 = buffer.g1b();
																						aByte1471 = buffer.g1b();
																						aByte1474 = buffer.g1b();
																						aByte1494 = buffer.g1b();
																					} else if (opcode == 158) {
																						aByte1487 = (byte) 1;
																					} else if (opcode != 159) {
																						if (opcode != 160) {
																							if (opcode == 162) {
																								aBoolean1481 = true;
																							} else if (opcode != 163) {
																								if (opcode == 164) {
																									anInt1502 = buffer.g2();
																									anInt1463 = buffer.g2();
																								} else if (opcode == 165) {
																									anInt1497 = buffer.g1();
																								} else if (opcode != 168) {
																									if (opcode == 249) {
																										int i_14_ = buffer.g1();
																										if (extraData == null) {
																											int i_15_ = Class8.get_next_high_pow2(i_14_);
																											extraData = new HashTable(i_15_);
																										}
																										for (int i_16_ = 0; i_14_ > i_16_; i_16_++) {
																											boolean bool = buffer.g1() == 1;
																											int i_17_ = buffer.readUnsignedMedInt();
																											Node class296;
																											if (!bool) {
																												class296 = new IntegerNode(buffer.g4());
																											} else {
																												class296 = new StringNode(buffer.gstr());
																											}
																											extraData.put(i_17_, class296);
																										}
																									}
																								} else {
																									anInt1464 = buffer.g1();
																								}
																							} else {
																								anInt1454 = buffer.g1();
																							}
																						} else {
																							int i_18_ = buffer.g1();
																							anIntArray1499 = new int[i_18_];
																							for (int i_19_ = 0; i_19_ < i_18_; i_19_++) {
																								anIntArray1499[i_19_] = buffer.g2();
																							}
																						}
																					} else {
																						aByte1487 = (byte) 0;
																					}
																				} else {
																					anInt1448 = buffer.g2();
																				}
																			} else {
																				aBoolean1511 = true;
																			}
																		} else {
																			anInt1507 = buffer.g2();
																		}
																	} else {
																		anInt1492 = buffer.g2();
																	}
																} else {
																	buffer.g1();
																}
															} else {
																aByte1469 = buffer.g1b();
															}
														} else {
															anInt1462 = buffer.g2();
														}
													} else {
														anInt1485 = buffer.g2();
													}
												} else {
													aByte1447 = buffer.g1b();
													aByte1445 = buffer.g1b();
												}
											} else {
												aShort1495 = (short) buffer.g2();
												aShort1490 = (short) buffer.g2();
											}
										} else {
											isClickable = false;
										}
									} else {
										anInt1459 = buffer.g2();
									}
								} else {
									anInt1472 = buffer.g1b();
								}
							} else {
								aBoolean1503 = true;
							}
						} else {
							anInt1468 = buffer.g2();
						}
					} else {
						combatLevel = buffer.g2();
					}
				} else {
					int i_20_ = buffer.g1();
					aShortArray1508 = new short[i_20_];
					aShortArray1491 = new short[i_20_];
					for (int i_21_ = 0; i_20_ > i_21_; i_21_++) {
						aShortArray1508[i_21_] = (short) buffer.g2();
						aShortArray1491[i_21_] = (short) buffer.g2();
					}
				}
			} else {
				size = buffer.g1();
			}
		} else {
			int i_22_ = buffer.g1();
			models = new int[i_22_];
			for (int i_23_ = 0; i_22_ > i_23_; i_23_++) {
				if (newNpc) {
					models[i_23_] = buffer.gSmart2or4s();
				} else {
					models[i_23_] = buffer.g2();
					if (models[i_23_] == 65535) {
						models[i_23_] = -1;
					}
				}
			}
		}
	}

	static final void method1495(int i) {
		if (i != -15477) {
			aClass268_1478 = null;
		}
		if (Class252.aClass398_2383.aBoolean3332 && Class10.aClass112_3533.worldId != -1) {
			Class45.method582(Class10.aClass112_3533.worldId, i + 22477, Class10.aClass112_3533.ipAddress);
		}
	}

	final Model method1496(int[] is, int i, ha var_ha, int i_24_, Animator[] class44s, int i_25_, IConfigsRegister interface17, Animator class44, Class62 class62, Class286 class286, Animator class44_26_) {
		if (configData != null) {
			NPCDefinition class147_27_ = getNPCDefinitionByConfigID(interface17);
			if (class147_27_ == null) {
				return null;
			}
			return class147_27_.method1496(is, i, var_ha, i_24_, class44s, i_25_, interface17, class44, class62, class286, class44_26_);
		}
		int i_28_ = i;
		if (anInt1468 != 128) {
			i_28_ |= 0x2;
		}
		if (anInt1498 != 128) {
			i_28_ |= 0x5;
		}
		boolean bool = false;
		int i_29_ = class44s != null ? class44s.length : 0;
		for (int i_30_ = 0; i_29_ > i_30_; i_30_++) {
			if (class44s[i_30_] != null) {
				bool = true;
				i_28_ |= class44s[i_30_].method568(0);
			}
		}
		if (i_24_ != -144) {
			aBoolean1481 = true;
		}
		if (class44 != null) {
			i_28_ |= class44.method568(0);
			bool = true;
		}
		if (class44_26_ != null) {
			bool = true;
			i_28_ |= class44_26_.method568(0);
		}
		long l = var_ha.anInt1295 << 16 | npcID;
		if (class286 != null) {
			l |= class286.aLong2642 << 24;
		}
		Model class178;
		synchronized (loader.aClass113_1403) {
			class178 = (Model) loader.aClass113_1403.get(l);
		}
		Class280 class280 = null;
		if (renderEmote != -1) {
			class280 = class62.method700(renderEmote, 0);
		}
		if (class178 == null || i_28_ != (i_28_ & class178.ua())) {
			if (class178 != null) {
				i_28_ |= class178.ua();
			}
			int i_31_ = i_28_;
			if (originalColours != null) {
				i_31_ |= 0x4000;
			}
			if (aShortArray1508 != null) {
				i_31_ |= 0x8000;
			}
			if (aByte1494 != 0) {
				i_31_ |= 0x80000;
			}
			int[] is_32_ = class286 != null && class286.anIntArray2638 != null ? class286.anIntArray2638 : models;
			boolean bool_33_ = false;
			synchronized (loader.aClass138_1397) {
				for (int i_34_ = 0; i_34_ < is_32_.length; i_34_++) {
					if (is_32_[i_34_] != -1 && !loader.aClass138_1397.fileExists(is_32_[i_34_], 0)) {
						bool_33_ = true;
					}
				}
			}
			if (bool_33_) {
				return null;
			}
			Mesh[] class132s = new Mesh[is_32_.length];
			for (int i_35_ = 0; is_32_.length > i_35_; i_35_++) {
				if (is_32_[i_35_] != -1) {
					synchronized (loader.aClass138_1397) {
						class132s[i_35_] = Class296_Sub51_Sub1.fromJs5(loader.aClass138_1397, is_32_[i_35_], 0);
					}
					if (class132s[i_35_] != null) {
						if (class132s[i_35_].version_number < 13) {
							class132s[i_35_].scale(2);
						}
						if (anIntArrayArray1449 != null && anIntArrayArray1449[i_35_] != null) {
							class132s[i_35_].translate(anIntArrayArray1449[i_35_][0], anIntArrayArray1449[i_35_][2], anIntArrayArray1449[i_35_][1]);
						}
					}
				}
			}
			if (class280 != null && class280.anIntArrayArray2558 != null) {
				for (int i_36_ = 0; class280.anIntArrayArray2558.length > i_36_; i_36_++) {
					if (i_36_ < class132s.length && class132s[i_36_] != null) {
						int i_37_ = 0;
						int i_38_ = 0;
						int i_39_ = 0;
						int i_40_ = 0;
						int i_41_ = 0;
						int i_42_ = 0;
						if (class280.anIntArrayArray2558[i_36_] != null) {
							i_42_ = class280.anIntArrayArray2558[i_36_][5] << 3;
							i_38_ = class280.anIntArrayArray2558[i_36_][1];
							i_40_ = class280.anIntArrayArray2558[i_36_][3] << 3;
							i_39_ = class280.anIntArrayArray2558[i_36_][2];
							i_41_ = class280.anIntArrayArray2558[i_36_][4] << 3;
							i_37_ = class280.anIntArrayArray2558[i_36_][0];
						}
						if (i_40_ != 0 || i_41_ != 0 || i_42_ != 0) {
							class132s[i_36_].method1392(i_41_, i_42_, i_40_, (byte) 84);
						}
						if (i_37_ != 0 || i_38_ != 0 || i_39_ != 0) {
							class132s[i_36_].translate(i_37_, i_39_, i_38_);
						}
					}
				}
			}
			Mesh class132;
			if (class132s.length != 1) {
				class132 = new Mesh(class132s, class132s.length);
			} else {
				class132 = class132s[0];
			}
			class178 = var_ha.a(class132, i_31_, loader.anInt1406, anInt1472 + 64, anInt1479 + 850);
			if (originalColours != null) {
				short[] is_43_;
				if (class286 == null || class286.aShortArray2640 == null) {
					is_43_ = modifiedColours;
				} else {
					is_43_ = class286.aShortArray2640;
				}
				for (int i_44_ = 0; i_44_ < originalColours.length; i_44_++) {
					if (aByteArray1446 == null || i_44_ >= aByteArray1446.length) {
						class178.ia(originalColours[i_44_], is_43_[i_44_]);
					} else {
						class178.ia(originalColours[i_44_], Class41_Sub7.aShortArray3762[aByteArray1446[i_44_] & 0xff]);
					}
				}
			}
			if (aShortArray1508 != null) {
				short[] is_45_;
				if (class286 == null || class286.aShortArray2641 == null) {
					is_45_ = aShortArray1491;
				} else {
					is_45_ = class286.aShortArray2641;
				}
				for (int i_46_ = 0; i_46_ < aShortArray1508.length; i_46_++) {
					class178.aa(aShortArray1508[i_46_], is_45_[i_46_]);
				}
			}
			if (aByte1494 != 0) {
				class178.method1717(aByte1476, aByte1471, aByte1474, aByte1494 & 0xff);
			}
			class178.s(i_28_);
			synchronized (loader.aClass113_1403) {
				loader.aClass113_1403.put(class178, l);
			}
		}
		Model class178_47_ = class178.method1728((byte) 4, i_28_, true);
		boolean bool_48_ = false;
		if (is != null) {
			for (int i_49_ = 0; i_49_ < 12; i_49_++) {
				if (is[i_49_] != -1) {
					bool_48_ = true;
				}
			}
		}
		if (!bool && !bool_48_) {
			return class178_47_;
		}
		Class373[] class373s = null;
		if (class280 != null) {
			class373s = class280.method2344(var_ha, (byte) 41);
		}
		if (bool_48_ && class373s != null) {
			for (int i_50_ = 0; i_50_ < 12; i_50_++) {
				if (class373s[i_50_] != null) {
					class178_47_.method1726(class373s[i_50_], 1 << i_50_, true);
				}
			}
		}
		int i_51_ = 0;
		int i_52_ = 1;
		while (i_51_ < i_29_) {
			if (class44s[i_51_] != null) {
				class44s[i_51_].method551(0, i_52_, 24529, class178_47_);
			}
			i_51_++;
			i_52_ <<= 1;
		}
		if (bool_48_) {
			for (int i_53_ = 0; i_53_ < 12; i_53_++) {
				if (is[i_53_] != -1) {
					int i_54_ = is[i_53_] - i_25_;
					i_54_ &= 0x3fff;
					Class373 class373 = var_ha.m();
					class373.method3906(i_54_);
					class178_47_.method1726(class373, 1 << i_53_, false);
				}
			}
		}
		if (bool_48_ && class373s != null) {
			for (int i_55_ = 0; i_55_ < 12; i_55_++) {
				if (class373s[i_55_] != null) {
					class178_47_.method1726(class373s[i_55_], 1 << i_55_, false);
				}
			}
		}
		if (class44 == null || class44_26_ == null) {
			if (class44 != null) {
				class44.method556(class178_47_, 0, (byte) -63);
			} else if (class44_26_ != null) {
				class44_26_.method556(class178_47_, 0, (byte) -63);
			}
		} else {
			Class16_Sub2.method242(class44_26_, i_24_ + 68, class178_47_, class44);
		}
		if (anInt1498 != 128 || anInt1468 != 128) {
			class178_47_.O(anInt1498, anInt1468, anInt1498);
		}
		class178_47_.s(i);
		return class178_47_;
	}

	final NPCDefinition getNPCDefinitionByConfigID(IConfigsRegister cfgRegister) {
		int configValue = -1;
		if (bitConfigID != -1) {
			configValue = cfgRegister.getBITConfig(bitConfigID, (byte) -93);
		} else if (configID != -1) {
			configValue = cfgRegister.getConfig(true, configID);
		}
		if (configValue < 0 || configValue >= configData.length - 1 || configData[configValue] == -1) {
			int npcID = configData[configData.length - 1];
			if (npcID == -1) {
				return null;
			}
			return loader.getDefinition(npcID);
		}
		return loader.getDefinition(configData[configValue]);
	}

	final int getIntExtraData(int key, int defaultValue) {
		if (extraData == null) {
			return defaultValue;
		}
		IntegerNode intn = (IntegerNode) extraData.get(key);
		if (intn == null) {
			return defaultValue;
		}
		return intn.value;
	}

	final String getStringExtraData(int key, String defaultValue) {
		if (extraData == null) {
			return defaultValue;
		}
		StringNode strn = (StringNode) extraData.get(key);
		if (strn == null) {
			return defaultValue;
		}
		return strn.value;
	}

	final void method1500(int i) {
		if (models == null) {
			models = new int[0];
		}
		if (i == -6061) {
			if (aByte1487 == -1) {
				if (loader.game == Class363.runescape) {
					aByte1487 = (byte) 1;
				} else {
					aByte1487 = (byte) 0;
				}
			}
		}
	}

	final boolean method1501(IConfigsRegister interface17, byte i) {
		if (configData == null) {
			return true;
		}
		if (i >= -108) {
			method1503(126);
		}
		int i_61_ = -1;
		if (bitConfigID != -1) {
			i_61_ = interface17.getBITConfig(bitConfigID, (byte) 45);
		} else if (configID != -1) {
			i_61_ = interface17.getConfig(true, configID);
		}
		if (i_61_ < 0 || configData.length - 1 <= i_61_ || configData[i_61_] == -1) {
			int i_62_ = configData[configData.length - 1];
			if (i_62_ == -1) {
				return false;
			}
			return true;
		}
		return true;
	}

	public void init(Packet buffer, boolean newNpc) {
		if (newNpc) {
			buffer.pos += 3;
		}
		for (;;) {
			int i = buffer.g1();
			if (i == 0) {
				break;
			}
			parseOpcode(i, buffer, newNpc);
		}
	}

	public static void method1503(int i) {
		aClass268_1478 = null;
	}

	final Model method1504(ha var_ha, Animator class44, Class286 class286, int i, IConfigsRegister interface17, int i_64_) {
		if (configData != null) {
			NPCDefinition class147_65_ = getNPCDefinitionByConfigID(interface17);
			if (class147_65_ == null) {
				return null;
			}
			return class147_65_.method1504(var_ha, class44, class286, i, interface17, 0);
		}
		if (if_models == null && (class286 == null || class286.anIntArray2638 == null)) {
			return null;
		}
		int i_66_ = i;
		if (class44 != null) {
			i_66_ |= class44.method568(0);
		}
		long l = var_ha.anInt1295 << 16 | npcID;
		if (class286 != null) {
			l |= class286.aLong2642 << 24;
		}
		Model class178;
		synchronized (loader.aClass113_1404) {
			class178 = (Model) loader.aClass113_1404.get(l);
		}
		if (class178 == null || (class178.ua() & i_66_) != i_66_) {
			if (class178 != null) {
				i_66_ |= class178.ua();
			}
			int i_67_ = i_66_;
			if (originalColours != null) {
				i_67_ |= 0x4000;
			}
			if (aShortArray1508 != null) {
				i_67_ |= 0x8000;
			}
			if (aByte1494 != 0) {
				i_67_ |= 0x80000;
			}
			int[] is = class286 != null && class286.anIntArray2638 != null ? class286.anIntArray2638 : if_models;
			boolean bool = false;
			synchronized (loader.aClass138_1397) {
				for (int i_68_ = 0; is.length > i_68_; i_68_++) {
					if (!loader.aClass138_1397.fileExists(is[i_68_], 0)) {
						bool = true;
					}
				}
			}
			if (bool) {
				return null;
			}
			Mesh[] class132s = new Mesh[is.length];
			synchronized (loader.aClass138_1397) {
				for (int i_69_ = 0; i_69_ < is.length; i_69_++) {
					class132s[i_69_] = Class296_Sub51_Sub1.fromJs5(loader.aClass138_1397, is[i_69_], 0);
				}
			}
			for (int i_70_ = 0; is.length > i_70_; i_70_++) {
				if (class132s[i_70_] != null && class132s[i_70_].version_number < 13) {
					class132s[i_70_].scale(2);
				}
			}
			Mesh class132;
			if (class132s.length == 1) {
				class132 = class132s[0];
			} else {
				class132 = new Mesh(class132s, class132s.length);
			}
			class178 = var_ha.a(class132, i_67_, loader.anInt1406, 64, 768);
			if (originalColours != null) {
				short[] is_71_;
				if (class286 == null || class286.aShortArray2640 == null) {
					is_71_ = modifiedColours;
				} else {
					is_71_ = class286.aShortArray2640;
				}
				for (int i_72_ = 0; originalColours.length > i_72_; i_72_++) {
					if (aByteArray1446 != null && aByteArray1446.length > i_72_) {
						class178.ia(originalColours[i_72_], Class41_Sub7.aShortArray3762[aByteArray1446[i_72_] & 0xff]);
					} else {
						class178.ia(originalColours[i_72_], is_71_[i_72_]);
					}
				}
			}
			if (aShortArray1508 != null) {
				short[] is_73_;
				if (class286 == null || class286.aShortArray2641 == null) {
					is_73_ = aShortArray1491;
				} else {
					is_73_ = class286.aShortArray2641;
				}
				for (int i_74_ = 0; i_74_ < aShortArray1508.length; i_74_++) {
					class178.aa(aShortArray1508[i_74_], is_73_[i_74_]);
				}
			}
			if (aByte1494 != 0) {
				class178.method1717(aByte1476, aByte1471, aByte1474, aByte1494 & 0xff);
			}
			class178.s(i_66_);
			synchronized (loader.aClass113_1404) {
				loader.aClass113_1404.put(class178, l);
			}
		}
		if (i_64_ != 0) {
			method1493(true);
		}
		if (class44 != null) {
			class178 = class178.method1728((byte) 1, i_66_, true);
			class44.method556(class178, 0, (byte) -63);
		}
		class178.s(i);
		return class178;
	}

	public NPCDefinition() {
		aByte1445 = (byte) -16;
		configID = -1;
		isClickable = true;
		anInt1465 = -1;
		anInt1453 = -1;
		anInt1459 = 32;
		bitConfigID = -1;
		anInt1456 = 255;
		anInt1463 = 256;
		aBoolean1470 = true;
		anInt1462 = -1;
		aByte1447 = (byte) -96;
		aBoolean1483 = false;
		anInt1468 = 128;
		anInt1480 = -1;
		anInt1448 = -1;
		renderEmote = -1;
		anInt1472 = 0;
		aByte1469 = (byte) 4;
		aByte1487 = (byte) -1;
		size = 1;
		anInt1454 = -1;
		anInt1485 = -1;
		anInt1486 = -1;
		aBoolean1457 = true;
		aByte1493 = (byte) 0;
		aShort1490 = (short) 0;
		anInt1492 = -1;
		anInt1464 = 0;
		anInt1504 = 0;
		anInt1498 = 128;
		anInt1475 = -1;
		anInt1484 = -1;
		headIcon = -1;
		anInt1497 = 0;
		visibleOnMinimap = true;
		aBoolean1503 = false;
		anInt1502 = 256;
		anInt1500 = -1;
		aByte1494 = (byte) 0;
		anInt1479 = 0;
		anInt1507 = -1;
		anInt1501 = -1;
		aShort1495 = (short) 0;
		combatLevel = -1;
		aBoolean1511 = false;
		anInt1510 = -1;
	}
}
