package net.zaros.client;

/* Class368_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub4 extends Class368 {
	static int anInt5440;
	static int anInt5441;
	static int anInt5442 = 0;
	static AdvancedMemoryCache aClass113_5443;
	private int anInt5444;

	Class368_Sub4(Packet class296_sub17) {
		super(class296_sub17);
		anInt5444 = class296_sub17.g2();
	}

	static final void method3817(int i) {
		int i_0_ = 0;
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub7_5004.method420(116) == 1) {
			i_0_ |= 0x1;
			i_0_ |= 0x10;
			i_0_ |= 0x20;
			i_0_ |= 0x2;
			i_0_ |= 0x4;
		}
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991.method497(118) == 0)
			i_0_ |= 0x40;
		HardReferenceWrapper.method2792((byte) 75, i_0_);
		Class379.objectDefinitionLoader.method374(0, i_0_);
		int i_1_ = -28 / ((-62 - i) / 63);
		Class296_Sub39_Sub1.itemDefinitionLoader.update_details(i_0_);
		Class352.npcDefinitionLoader.method1417(i_0_, -27707);
		Class157.graphicsLoader.method2288(i_0_, 0);
		Class296_Sub51_Sub35.method3188(23, i_0_);
		Class268.method2295(i_0_, true);
		Class300.method3250(i_0_, -99);
		Class395.method4069(1000, i_0_);
		CS2Stack.method2137(105);
	}

	final void method3807(byte i) {
		int i_2_ = -109 / ((52 - i) / 52);
		Class379.aClass302Array3624[anInt5444].method3259(-88);
	}

	public static void method3818(boolean bool) {
		aClass113_5443 = null;
		if (bool != true)
			method3821((byte) -98);
	}

	static final Class296_Sub53 method3819(byte i) {
		if (Class106.aClass155_1101 == null || Class405.aClass298_3389 == null)
			return null;
		for (Class296_Sub53 class296_sub53 = (Class296_Sub53) Class405.aClass298_3389.method3235(true); class296_sub53 != null; class296_sub53 = (Class296_Sub53) Class405.aClass298_3389.method3235(true)) {
			Class18 class18 = Class106.aClass245_1094.method2179(11702, class296_sub53.anInt5046);
			if (class18 != null && class18.aBoolean228 && class18.method280(Class106.anInterface17_1090, true))
				return class296_sub53;
		}
		if (i != -78)
			return null;
		return null;
	}

	static final void method3820(int i) {
		FileOnDisk class58 = null;
		try {
			Class278 class278 = Class252.aClass398_2383.method4117("", (byte) 98, true);
			while (class278.anInt2540 == 0)
				Class106_Sub1.method942(1L, 0);
			if (class278.anInt2540 == 1) {
				class58 = (FileOnDisk) class278.anObject2539;
				Packet class296_sub17 = Class343_Sub1.aClass296_Sub50_5282.method3057(25);
				class58.write(class296_sub17.pos, class296_sub17.data, 0, true);
			}
		} catch (Exception exception) {
			/* empty */
		}
		try {
			if (class58 != null)
				class58.close(0);
		} catch (Exception exception) {
			/* empty */
		}
		if (i != 1)
			anInt5441 = -116;
	}

	static final Class296_Sub1 method3821(byte i) {
		if (i != -68)
			anInt5441 = 54;
		if (Class88.anInt945 == 0)
			return new Class296_Sub1();
		return Class303.aClass296_Sub1Array2723[--Class88.anInt945];
	}

	static final int method3822(Mobile m, byte i) {
		if (m.anInt6815 == 0)
			return 0;
		if (m.interactingWithIndex != -1) {
			Mobile interactingWith = null;
			if (m.interactingWithIndex < 32768) {
				NPCNode n = ((NPCNode) (Class41_Sub18.localNpcs.get((long) m.interactingWithIndex)));
				if (n != null)
					interactingWith = n.value;
			} else if (m.interactingWithIndex >= 32768)
				interactingWith = (PlayerUpdate.visiblePlayers[m.interactingWithIndex - 32768]);
			if (interactingWith != null) {
				int i_4_ = (-interactingWith.tileX + m.tileX);
				int i_5_ = (-interactingWith.tileY + m.tileY);
				if (i_4_ != 0 || i_5_ != 0)
					m.method3499((int) (Math.atan2((double) i_4_, (double) i_5_) * 2607.5945876176133) & 0x3fff);
			}
		}
		if (m instanceof Player) {
			Player p = (Player) m;
			if (p.someFacingDirection != -1 && (p.currentWayPoint == 0 || p.anInt6836 > 0)) {
				p.method3499(p.someFacingDirection);
				p.someFacingDirection = -1;
			}
		} else if (m instanceof NPC) {
			NPC n = (NPC) m;
			if (n.faceX != -1 && (n.currentWayPoint == 0 || n.anInt6836 > 0)) {
				int i_6_ = (n.tileX - (n.faceX + (-Class206.worldBaseX - Class206.worldBaseX)) * 256);
				int i_7_ = (-((-Class41_Sub26.worldBaseY + n.faceY - Class41_Sub26.worldBaseY) * 256) + n.tileY);
				if (i_6_ != 0 || i_7_ != 0)
					n.method3499((int) (Math.atan2((double) i_6_, (double) i_7_) * 2607.5945876176133) & 0x3fff);
				n.faceX = -1;
			}
		}
		if (i != -67)
			method3821((byte) -42);
		return m.method3508((byte) 65);
	}

	static {
		anInt5441 = -2;
		aClass113_5443 = new AdvancedMemoryCache(64);
	}
}
