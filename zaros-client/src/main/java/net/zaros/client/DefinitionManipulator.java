package net.zaros.client;
/**
 * @author Tyluur <itstyluur@gmail.com>
 * @since 7/28/2017
 */
final class DefinitionManipulator {
	static void manipulateObject(int objectId, ObjectDefinition definition) {
		switch (objectId) {
			case 53739:
				definition.options = new String[] { "Pray-at", null, null, null, null, "Examine" };
				break;
		}
	}
	
	static void manipulateNPC(int npcId, NPCDefinition definition) {
		if (definition.options.length > 0 && definition.options[0] != null && definition.options[0].equalsIgnoreCase("interact")) {
			definition.options[0] = null;
		}
		switch (npcId) {
			case 14332:
				definition.name = "Wizard";
				definition.options = new String[] { "Talk-to", null, "Previous", null, null, "Examine" };
				break;
			case 6537:
				definition.options = new String[] { "Talk-to", null, "Skull", null, null, "Examine" };
				break;
		}
	}
	
}
