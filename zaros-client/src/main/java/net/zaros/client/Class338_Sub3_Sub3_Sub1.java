package net.zaros.client;

/* Class338_Sub3_Sub3_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub3_Sub3_Sub1 extends Class338_Sub3_Sub3 implements Interface14 {
	static EquipmentData equipmentData;
	private boolean aBoolean6611;
	private r aR6612;
	private Model aClass178_6613;
	private byte aByte6614;
	private Class96 aClass96_6615;
	static int anInt6616 = 1;
	static int anInt6617;
	private int locid;
	static World lobbyWorld;
	private boolean aBoolean6620;
	static boolean aBoolean6621 = false;
	private boolean aBoolean6622;
	private byte aByte6623;

	@Override
	public final boolean method55(byte i) {
		if (i != -57) {
			aR6612 = null;
		}
		return aBoolean6622;
	}

	@Override
	public final int method59(int i) {
		if (i <= 16) {
			equipmentData = null;
		}
		return aByte6623;
	}

	static final Class107 method3559(Packet class296_sub17, byte i) {
		if (i != -80) {
			return null;
		}
		int i_0_ = class296_sub17.g2();
		Class252 class252 = StaticMethods.method312((byte) -33)[class296_sub17.g1()];
		Class357 class357 = Class110.method970(0)[class296_sub17.g1()];
		int i_1_ = class296_sub17.g2b();
		int i_2_ = class296_sub17.g2b();
		return new Class107(i_0_, class252, class357, i_1_, i_2_);
	}

	@Override
	final boolean method3475(int i, int i_3_, ha var_ha, int i_4_) {
		Model class178 = method3561(131072, 2900, var_ha);
		if (class178 != null) {
			Class373 class373 = var_ha.f();
			class373.method3902(tileX, anInt5213, tileY);
			if (!Class296_Sub39_Sub10.aBoolean6177) {
				return class178.method1732(i_4_, i, class373, false, 0);
			}
			return class178.method1731(i_4_, i, class373, false, 0, ModeWhat.anInt1192);
		}
		if (i_3_ >= -48) {
			return false;
		}
		return false;
	}

	@Override
	public final int method54(int i) {
		if (i != -11077) {
			method58(23);
		}
		return aByte6614;
	}

	@Override
	public final void method58(int i) {
		if (i == -19727) {
			if (aClass178_6613 != null) {
				aClass178_6613.method1722();
			}
		}
	}

	Class338_Sub3_Sub3_Sub1(ha var_ha, ObjectDefinition class70, int i, int i_5_, int i_6_, int i_7_, int i_8_, boolean bool, int i_9_, int i_10_, int i_11_, int i_12_) {
		super(i_6_, i_7_, i_8_, i, i_5_, i_9_, i_10_);
		aBoolean6611 = class70.interactonType != 0 && !bool;
		tileY = i_8_;
		aBoolean6620 = bool;
		aByte6614 = (byte) i_11_;
		tileX = i_6_;
		aByte6623 = (byte) i_12_;
		locid = class70.ID;
		aBoolean6622 = var_ha.B() && class70.aBoolean779 && !aBoolean6620 && Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method465(119) != 0;
		Class188 class188 = method3562(2048, false, var_ha, aBoolean6622);
		if (class188 != null) {
			aR6612 = class188.aR1925;
			aClass178_6613 = class188.aClass178_1926;
		}
	}

	static final void method3560(int i) {
		World.aClass113_1160.clearSoftReferences();
		if (i > -49) {
			method3559(null, (byte) 55);
		}
	}

	@Override
	final int method3462(byte i) {
		if (i != 28) {
			aByte6623 = (byte) 71;
		}
		if (aClass178_6613 != null) {
			return aClass178_6613.ma();
		}
		return 0;
	}

	@Override
	public final void method56(ha var_ha, byte i) {
		if (i != -117) {
			equipmentData = null;
		}
		Object object = null;
		r var_r;
		if (aR6612 == null && aBoolean6622) {
			Class188 class188 = method3562(262144, false, var_ha, true);
			var_r = class188 != null ? class188.aR1925 : null;
		} else {
			var_r = aR6612;
			aR6612 = null;
		}
		if (var_r != null) {
			Class296_Sub39_Sub20_Sub2.method2910(var_r, aByte5203, tileX, tileY, null);
		}
	}

	@Override
	public final void method60(byte i, ha var_ha) {
		Object object = null;
		r var_r;
		if (aR6612 == null && aBoolean6622) {
			Class188 class188 = method3562(262144, false, var_ha, true);
			var_r = class188 != null ? class188.aR1925 : null;
		} else {
			var_r = aR6612;
			aR6612 = null;
		}
		if (var_r != null) {
			Class296_Sub45_Sub4.method3017(var_r, aByte5203, tileX, tileY, null);
		}
		int i_13_ = -59 / ((-23 - i) / 35);
	}

	@Override
	final boolean method3469(int i) {
		if (i < 82) {
			aByte6614 = (byte) 117;
		}
		if (aClass178_6613 == null) {
			return false;
		}
		return aClass178_6613.F();
	}

	private final Model method3561(int i, int i_14_, ha var_ha) {
		if (aClass178_6613 != null && var_ha.e(aClass178_6613.ua(), i) == 0) {
			return aClass178_6613;
		}
		if (i_14_ != 2900) {
			aByte6614 = (byte) 36;
		}
		Class188 class188 = method3562(i, false, var_ha, false);
		if (class188 != null) {
			return class188.aClass178_1926;
		}
		return null;
	}

	@Override
	public final int method57(byte i) {
		return locid;
	}

	@Override
	final int method3466(byte i) {
		if (i < 77) {
			aClass96_6615 = null;
		}
		if (aClass178_6613 == null) {
			return 0;
		}
		return aClass178_6613.fa();
	}

	@Override
	final void method3460(int i, ha var_ha) {
		int i_15_ = -37 % ((i + 41) / 62);
	}

	@Override
	final Class96 method3461(ha var_ha, int i) {
		if (aClass96_6615 == null) {
			aClass96_6615 = Class41_Sub21.method478(anInt5213, method3561(0, 2900, var_ha), tileX, tileY, (byte) 119);
		}
		int i_16_ = -120 / ((79 - i) / 44);
		return aClass96_6615;
	}

	@Override
	final Class338_Sub2 method3473(ha var_ha, byte i) {
		if (aClass178_6613 == null) {
			return null;
		}
		if (i >= -84) {
			return null;
		}
		Class373 class373 = var_ha.f();
		class373.method3902(aShort6567 + tileX, anInt5213, tileY + aShort6569);
		Class338_Sub2 class338_sub2 = Class144.method1481(1, true, aBoolean6611);
		if (Class296_Sub39_Sub10.aBoolean6177) {
			aClass178_6613.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
		} else {
			aClass178_6613.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
		}
		return class338_sub2;
	}

	private final Class188 method3562(int i, boolean bool, ha var_ha, boolean bool_17_) {
		ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(locid);
		if (bool) {
			aClass96_6615 = null;
		}
		s var_s;
		s var_s_18_;
		if (aBoolean6620) {
			var_s_18_ = Class244.aSArray2320[0];
			var_s = Class52.aSArray636[aByte5203];
		} else {
			var_s = Class244.aSArray2320[aByte5203];
			if (aByte5203 < 3) {
				var_s_18_ = Class244.aSArray2320[aByte5203 + 1];
			} else {
				var_s_18_ = null;
			}
		}
		return class70.method750(var_s, var_s_18_, var_ha, bool_17_, -954198435, tileY, tileX, i, null, aByte6623, anInt5213, aByte6614);
	}

	public static void method3563(int i) {
		equipmentData = null;
		if (i < 80) {
			method3560(-38);
		}
		lobbyWorld = null;
	}

	@Override
	final boolean method3459(int i) {
		if (i != 0) {
			return true;
		}
		if (aClass178_6613 != null) {
			if (aClass178_6613.r()) {
				return false;
			}
			return true;
		}
		return true;
	}
}
