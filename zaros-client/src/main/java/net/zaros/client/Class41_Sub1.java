package net.zaros.client;

import net.zaros.client.configs.objtype.ObjTypeList;

/* Class41_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub1 extends Class41 {
	static Class125 aClass125_3735 = new Class125();
	static int anInt3736 = 0;
	static Class256 aClass256_3737;
	static Object anObject3738;
	static int anInt3739 = 0;

	Class41_Sub1(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final void method386(int i) {
		if (anInt389 != 0 && aClass296_Sub50_392.aClass41_Sub29_5002.method515(118) != 1)
			anInt389 = 0;
		if (i != 2)
			method383((byte) -6);
		if (anInt389 < 0 || anInt389 > 1)
			anInt389 = method383((byte) 110);
	}

	Class41_Sub1(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final int method389(int i) {
		if (i <= 114)
			aClass125_3735 = null;
		return anInt389;
	}

	final int method380(int i, byte i_0_) {
		if (i == 0 || aClass296_Sub50_392.aClass41_Sub29_5002.method515(121) == 1)
			return 1;
		if (i_0_ != 41)
			return 66;
		return 2;
	}

	final void method381(int i, byte i_1_) {
		anInt389 = i;
		if (i_1_ != -110)
			method390(114);
	}

	final boolean method390(int i) {
		if (i != -25952)
			return false;
		return true;
	}

	final int method383(byte i) {
		if (i != 110)
			aClass125_3735 = null;
		return 1;
	}

	static final void method391(int i) {
		Class296_Sub39_Sub1.fs2.discardUnpacked_ = 1;
		if (Class366_Sub6.anInt5392 == 15)
			Class368_Sub2.method3812(0);
		Class207.method1993(-1);
		Class296_Sub45_Sub2.aClass204_6276.method1966(320);
		ClipData.anInt188 = 0;
		Mobile.anInt6786 = 0;
		Class296_Sub51_Sub23.anInt6457 = 0;
		CS2Executor.anInt1542 = 0;
		LookupTable.anInt54 = 0;
		Class219.anInt2127 = 0;
		Class228.anInt2191 = 0;
		CS2Executor.anInt1530 = 0;
		Class296_Sub51_Sub16.anInt6427 = 0;
		if (i != -28107)
			method391(43);
		Class123_Sub1_Sub2.anInt5816 = 0;
		Class314.anInt2782 = 0;
		Class122.anInt3549 = 0;
		Class178_Sub2.anInt4391 = 0;
		SubCache.aBoolean2707 = true;
		Class41.aBoolean390 = true;
		Class391.method4042(0);
		for (int i_2_ = 0; Class338_Sub2.aClass225Array5200.length > i_2_; i_2_++)
			Class338_Sub2.aClass225Array5200[i_2_] = null;
		Class127.aBoolean1304 = false;
		Class302.method3261(3);
		ObjTypeList.anInt1320 = (int) (Math.random() * 30.0) - 20;
		Class41_Sub8.anInt3763 = (int) (Math.random() * 110.0) - 55;
		Class134.anInt1387 = (int) (Math.random() * 80.0) - 40;
		StaticMethods.anInt6075 = (int) (Math.random() * 120.0) - 60;
		Class41_Sub26.aFloat3806 = (float) ((int) (Math.random() * 160.0) - 80 & 0x3fff);
		Class4.anInt71 = (int) (Math.random() * 100.0) - 50;
		Class224.method2082((byte) 112);
		for (int i_3_ = 0; i_3_ < 2048; i_3_++)
			PlayerUpdate.visiblePlayers[i_3_] = null;
		Class368_Sub23.npcNodeCount = 0;
		Class367.npcsCount = 0;
		Class41_Sub18.localNpcs.clear();
		Class72.aClass155_844.method1581(327680);
		Class134.aClass263_1385.clear();
		Class296_Sub51_Sub39.aClass404_6546.method4161((byte) 62);
		Class296_Sub11.aClass263_4647.clear();
		aa.aClass155_50 = new NodeDeque();
		ParticleEmitterRaw.aClass155_1770 = new NodeDeque();
		Class16_Sub3_Sub1.configsRegister.resetConfigurations(i + 28107);
		Class296_Sub50.method3053((byte) -56);
		Class44_Sub1.cameraDestX = 0;
		Class338_Sub8_Sub2.lookatHeight = 0;
		Class5.cameraDestZ = 0;
		Class53.lookatX = 0;
		Class373_Sub3.anInt5605 = 0;
		HashTable.anInt2459 = 0;
		Class32_Sub1.basedSpeed = 0;
		Class366_Sub2.lookatVelocity = 0;
		StaticMethods.lookatSpeed = 0;
		Graphic.lookatY = 0;
		for (int i_4_ = 0; Class269.globalIntVars.length > i_4_; i_4_++) {
			if (!Class14.aBooleanArray153[i_4_])
				Class269.globalIntVars[i_4_] = -1;
		}
		if (Class99.anInt1064 != -1)
			Class100.method877(-99, Class99.anInt1064);
		for (Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.getFirst(true); class296_sub13 != null; class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.getNext(i ^ ~0x6dca)) {
			if (!class296_sub13.isLinked((byte) -67)) {
				class296_sub13 = ((Class296_Sub13) Class386.aClass263_3264.getFirst(true));
				if (class296_sub13 == null)
					break;
			}
			Class47.method596(class296_sub13, true, false, (byte) 67);
		}
		Class99.anInt1064 = -1;
		Class386.aClass263_3264 = new HashTable(8);
		Class187.method1888((byte) -111);
		Player.aClass51_6872 = null;
		for (int i_5_ = 0; i_5_ < 8; i_5_++) {
			NodeDeque.aStringArray1591[i_5_] = null;
			Class294.aBooleanArray2690[i_5_] = false;
			Class296_Sub39_Sub19.anIntArray6252[i_5_] = -1;
		}
		Class379_Sub2_Sub1.method3972(0);
		Class164.aBoolean1687 = true;
		for (int i_6_ = 0; i_6_ < 100; i_6_++)
			Class360_Sub9.aBooleanArray5345[i_6_] = true;
		for (int i_7_ = 0; i_7_ < 6; i_7_++)
			Class360_Sub5.aClass208Array5326[i_7_] = new Class208();
		for (int i_8_ = 0; i_8_ < 25; i_8_++) {
			StaticMethods.anIntArray5930[i_8_] = 0;
			EffectiveVertex.anIntArray2215[i_8_] = 0;
			ClothDefinition.anIntArray3016[i_8_] = 0;
		}
		Class368_Sub22.method3866(11);
		Mesh.aBoolean1359 = true;
		Class12.aShortArray137 = Class366_Sub5.aShortArray5381 = Class41_Sub7.aShortArray3762 = Class296_Sub9.aShortArray4630 = new short[256];
		Class296_Sub51_Sub37.aString6532 = TranslatableString.aClass120_1222.getTranslation(Class394.langID);
		Class343_Sub1.aClass296_Sub50_5282.method3060(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5018.method449(i ^ ~0x6dbd), 45, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub14_5029);
		Class343_Sub1.aClass296_Sub50_5282.method3060(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5001.method496(i + 28229), 97, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026);
		Class296_Sub45_Sub2.interfaceCounter = 0;
		Class88.method829((byte) -99);
		Class300.method3248((byte) 19);
		GameClient.aClass296_Sub48_3717 = null;
		Class338_Sub3_Sub3_Sub2.aLong6628 = 0L;
		Class296_Sub39_Sub1.fs2.discardUnpacked_ = 2;
	}

	public static void method392(byte i) {
		anObject3738 = null;
		aClass256_3737 = null;
		aClass125_3735 = null;
		if (i != -31)
			method391(20);
	}
}
