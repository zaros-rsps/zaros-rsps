package net.zaros.client;
/* Class46 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.File;

final class ConfigsRegister implements IConfigsRegister {
	static Object anObject3669;
	int[] configurationsRegister;
	static int anInt3671;
	static String aString3672 = null;
	private int[] delayedConfigurationsRegister;
	static int anInt3674;
	private HashTable configChanges = new HashTable(128);

	static final int method584(byte i, String string) {
		if (!Class366_Sub9.aClass398_5420.aBoolean3332)
			return -1;
		if (Class154.aHashtable1581.containsKey(string))
			return 100;
		String string_0_ = ParamTypeList.method1654(string, 124);
		if (string_0_ == null)
			return -1;
		String string_1_ = Class344.aString3008 + string_0_;
		if (!Class268.aClass138_2494.hasFile("", string_1_))
			return -1;
		if (!Class268.aClass138_2494.hasFileBuffer_(string_1_))
			return Class268.aClass138_2494.getFileCompletion_(string_1_);
		byte[] is = Class268.aClass138_2494.getFile_("", string_1_);
		Object object = null;
		File file;
		try {
			if (i != -116)
				anInt3674 = 8;
			file = Class19.method282(string_0_, -2);
		} catch (RuntimeException runtimeexception) {
			return -1;
		}
		if (is != null && file != null) {
			boolean bool = true;
			byte[] is_2_ = Class178_Sub3.method1794((byte) 85, file);
			if (is_2_ != null && is.length == is_2_.length) {
				for (int i_3_ = 0; i_3_ < is_2_.length; i_3_++) {
					if (is[i_3_] != is_2_[i_3_]) {
						bool = false;
						break;
					}
				}
			} else
				bool = false;
			try {
				if (!bool)
					Class366_Sub9.aClass398_5420.method4129(file, is, -18301);
			} catch (Throwable throwable) {
				return -1;
			}
			Class296_Sub39_Sub12.method2852(i ^ ~0x48, file, string);
			return 100;
		}
		return -1;
	}

	final void resetConfigurations(int i) {
		for (int i_4_ = i; i_4_ < ConfigurationsLoader.configsLoader.configsCount; i_4_++) {
			ConfigurationDefinition class59 = ConfigurationsLoader.configsLoader.getDefinition(i_4_);
			if (class59 != null && class59.anInt677 == 0) {
				delayedConfigurationsRegister[i_4_] = 0;
				configurationsRegister[i_4_] = 0;
			}
		}
		configChanges = new HashTable(128);
	}

	static final Class196 method586(byte i) {
		int i_5_ = 10 / ((i + 68) / 50);
		return Class207.method1995(1, (byte) 15);
	}

	final void setConfig(int configValue, int configID) {
		configurationsRegister[configID] = configValue;
		LongNode ch = (LongNode) configChanges.get((long) configID);
		if (ch == null) {
			ch = new LongNode(Class72.method771(-122) + 500L);
			configChanges.put((long) configID, ch);
		} else
			ch.value = Class72.method771(-118) - -500L;
	}

	static final void method588(int i, int i_7_, int i_8_) {
		int i_9_ = (Class304.aClass92_2729.method851(-106, TranslatableString.aClass120_1220.getTranslation(Class394.langID)));
		int i_10_;
		if (!Class262.aBoolean1324) {
			for (Class296_Sub39_Sub9 class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeFirst((byte) 113); class296_sub39_sub9 != null; class296_sub39_sub9 = (Class296_Sub39_Sub9) HardReferenceWrapper.aClass155_6698.removeNext(1001)) {
				int i_11_ = BITConfigsLoader.method2100(class296_sub39_sub9, 0);
				if (i_9_ < i_11_)
					i_9_ = i_11_;
			}
			i_10_ = Class230.anInt2210 * 16 + 21;
			Class296_Sub39_Sub20.anInt6254 = Class230.anInt2210 * 16 + (SeekableFile.aBoolean2397 ? 26 : 22);
			i_9_ += 8;
		} else {
			for (Class296_Sub39_Sub1 class296_sub39_sub1 = ((Class296_Sub39_Sub1) Class152.aClass145_1558.getFront()); class296_sub39_sub1 != null; class296_sub39_sub1 = ((Class296_Sub39_Sub1) Class152.aClass145_1558.getNext())) {
				int i_12_;
				if (class296_sub39_sub1.anInt6117 != 1)
					i_12_ = Class296_Sub15.method2521(i ^ ~0x16, class296_sub39_sub1);
				else
					i_12_ = BITConfigsLoader.method2100(((Class296_Sub39_Sub9) (class296_sub39_sub1.aClass145_6119.head.queue_next)), i ^ 0x16);
				if (i_12_ > i_9_)
					i_9_ = i_12_;
			}
			Class296_Sub39_Sub20.anInt6254 = Class239.anInt2254 * 16 + (!SeekableFile.aBoolean2397 ? 22 : 26);
			i_10_ = Class239.anInt2254 * 16 + 21;
			i_9_ += 8;
		}
		int i_13_ = -(i_9_ / 2) + i_8_;
		if (i_13_ + i_9_ > Class241.anInt2301)
			i_13_ = Class241.anInt2301 - i_9_;
		if (i_13_ < 0)
			i_13_ = 0;
		if (i != 22)
			anInt3674 = -37;
		int i_14_ = i_7_;
		if (Class384.anInt3254 < i_14_ + i_10_)
			i_14_ = Class384.anInt3254 - i_10_;
		Class252.anInt2382 = i_13_;
		if (i_14_ < 0)
			i_14_ = 0;
		Class81.anInt3666 = i_9_;
		Class318.aBoolean2814 = true;
		Class100.anInt1067 = i_14_;
	}

	final void setDelayedConfiguration(int value, int id, byte i_16_) {
		delayedConfigurationsRegister[id] = value;
		LongNode n = (LongNode) configChanges.get((long) id);
		if (n != null) {
			if (n.value != 4611686018427387905L)
				n.value = (Class72.method771(i_16_ ^ ~0x59) + 500L | 0x4000000000000000L);
		} else {
			n = new LongNode(4611686018427387905L);
			configChanges.put((long) id, n);
		}
	}

	final void setBITConfig(int configID, int configValue) {
		BITConfigDefinition definition = Class296_Sub43.bitConfigsLoader.load(configID);
		int cfgID = definition.configID;
		int bitOffs = definition.bitOffs;
		int bitLength = definition.bitLength;
		int mask = Class182.bitMasks[-bitOffs + bitLength];
		if (configValue < 0 || mask < configValue)
			configValue = 0;
		mask <<= bitOffs;
		setConfig(mask & configValue << bitOffs | configurationsRegister[cfgID] & (mask ^ 0xffffffff), cfgID);
	}

	public final int getBITConfig(int i, byte i_24_) {
		BITConfigDefinition class282 = Class296_Sub43.bitConfigsLoader.load(i);
		int i_25_ = 126 / ((-20 - i_24_) / 54);
		int i_26_ = class282.configID;
		int i_27_ = class282.bitOffs;
		int i_28_ = class282.bitLength;
		int i_29_ = Class182.bitMasks[-i_27_ + i_28_];
		return configurationsRegister[i_26_] >> i_27_ & i_29_;
	}

	static final ClipData makeClipData(int mapSizeX, int mapSizeY) {
		ClipData clip = new ClipData();
		clip.sizeY = mapSizeY + 1 + 5;
		clip.offsetY = -1;
		clip.offsetX = -1;
		clip.sizeX = mapSizeX + 6;
		clip.clip = new int[clip.sizeX][clip.sizeY];
		clip.setup();
		return clip;
	}

	public static void method592(int i) {
		anObject3669 = null;
		aString3672 = null;
		if (i != 0)
			anInt3674 = -12;
	}

	final int nextConfigChange(boolean bool, byte i) {
		long l = Class72.method771(-126);
		for (LongNode n = (bool ? (LongNode) configChanges.getFirst(true) : (LongNode) configChanges.getNext(0)); n != null; n = (LongNode) configChanges.getNext(i - 66)) {
			if ((n.value & 0x3fffffffffffffffL) < l) {
				if ((n.value & 0x4000000000000000L) != 0L) {
					int i_32_ = (int) n.uid;
					configurationsRegister[i_32_] = delayedConfigurationsRegister[i_32_];
					n.unlink();
					return i_32_;
				}
				n.unlink();
			}
		}
		if (i != 66)
			delayedConfigurationsRegister = null;
		return -1;
	}

	static final int method594(int i, Js5 class138) {
		int i_33_ = 0;
		if (i != 22635)
			return 107;
		if (class138.hasEntryBuffer(Class41_Sub25.anInt3804))
			i_33_++;
		if (class138.hasEntryBuffer(Class368_Sub5.anInt5446))
			i_33_++;
		if (class138.hasEntryBuffer(Class379_Sub1.anInt5672))
			i_33_++;
		if (class138.hasEntryBuffer(Class16.anInt183))
			i_33_++;
		if (class138.hasEntryBuffer(Class116.anInt3676))
			i_33_++;
		if (class138.hasEntryBuffer(Class296_Sub11.anInt4650))
			i_33_++;
		if (class138.hasEntryBuffer(Class307.anInt2749))
			i_33_++;
		if (class138.hasEntryBuffer(Class206.anInt2067))
			i_33_++;
		if (class138.hasEntryBuffer(Class389.anInt3278))
			i_33_++;
		if (class138.hasEntryBuffer(Class366_Sub2.anInt5370))
			i_33_++;
		if (class138.hasEntryBuffer(Class284.anInt2619))
			i_33_++;
		if (class138.hasEntryBuffer(Class292.anInt2659))
			i_33_++;
		if (class138.hasEntryBuffer(Class296_Sub39_Sub15_Sub1.anInt6718))
			i_33_++;
		if (class138.hasEntryBuffer(Class205_Sub2.anInt5630))
			i_33_++;
		if (class138.hasEntryBuffer(ISAACCipher.anInt1897))
			i_33_++;
		return i_33_;
	}

	public final int getConfig(boolean bool, int i) {
		if (bool != true)
			return -122;
		return configurationsRegister[i];
	}

	final void setDelayedBITConfig(int configID, int configValue) {
		BITConfigDefinition class282 = Class296_Sub43.bitConfigsLoader.load(configID);
		int i_36_ = class282.configID;
		int i_37_ = class282.bitOffs;
		int i_38_ = class282.bitLength;
		int i_39_ = Class182.bitMasks[-i_37_ + i_38_];
		if (configValue < 0 || i_39_ < configValue)
			configValue = 0;
		i_39_ <<= i_37_;
		setDelayedConfiguration((configValue << i_37_ & i_39_ | delayedConfigurationsRegister[i_36_] & (i_39_ ^ 0xffffffff)), i_36_, (byte) 38);
	}

	public ConfigsRegister() {
		delayedConfigurationsRegister = new int[ConfigurationsLoader.configsLoader.configsCount];
		configurationsRegister = new int[ConfigurationsLoader.configsLoader.configsCount];
	}
}
