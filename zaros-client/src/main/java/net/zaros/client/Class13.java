package net.zaros.client;

import net.zaros.client.configs.objtype.ObjType;

/* Class13 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class13 implements Interface7 {
	static Class246 aClass246_3515 = new Class246();
	static Class202 aClass202_3516 = new Class202(1);

	public static void method223(int i) {
		if (i == -1) {
			aClass246_3515 = null;
			aClass202_3516 = null;
		}
	}

	static final void method224(int i, boolean bool, boolean bool_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		if (i_1_ <= -14) {
			if (i_3_ < i_2_) {
				int i_5_ = (i_3_ + i_2_) / 2;
				int i_6_ = i_3_;
				Class210_Sub1 class210_sub1 = AnimationsLoader.aClass210_Sub1Array2760[i_5_];
				AnimationsLoader.aClass210_Sub1Array2760[i_5_] = AnimationsLoader.aClass210_Sub1Array2760[i_2_];
				AnimationsLoader.aClass210_Sub1Array2760[i_2_] = class210_sub1;
				for (int i_7_ = i_3_; i_7_ < i_2_; i_7_++) {
					if (Class44_Sub1.method575((AnimationsLoader.aClass210_Sub1Array2760[i_7_]), bool_0_, i_4_, 40, bool, i, class210_sub1) <= 0) {
						Class210_Sub1 class210_sub1_8_ = AnimationsLoader.aClass210_Sub1Array2760[i_7_];
						AnimationsLoader.aClass210_Sub1Array2760[i_7_] = AnimationsLoader.aClass210_Sub1Array2760[i_6_];
						AnimationsLoader.aClass210_Sub1Array2760[i_6_++] = class210_sub1_8_;
					}
				}
				AnimationsLoader.aClass210_Sub1Array2760[i_2_] = AnimationsLoader.aClass210_Sub1Array2760[i_6_];
				AnimationsLoader.aClass210_Sub1Array2760[i_6_] = class210_sub1;
				method224(i, bool, bool_0_, -127, i_6_ - 1, i_3_, i_4_);
				method224(i, bool, bool_0_, -96, i_2_, i_6_ + 1, i_4_);
			}
		}
	}

	public final String method35(int[] is, long l, Class161 class161, int i) {
		if (i <= 110)
			return null;
		if (Class332.aClass161_2941 == class161) {
			Class277 class277 = Class85.aClass350_927.method3677(is[0], -6115);
			return class277.method2325((byte) 122, (int) l);
		}
		if (class161 == Class296_Sub51_Sub11.aClass161_6401 || RuntimeException_Sub1.aClass161_3392 == class161) {
			ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list((int) l);
			return class158.name;
		}
		if (class161 == Class288.aClass161_2653 || Class163.aClass161_1677 == class161 || Class161.aClass161_3193 == class161)
			return Class85.aClass350_927.method3677(is[0], -6115).method2325((byte) 126, (int) l);
		return null;
	}

	public Class13() {
		/* empty */
	}

	static final void method225(int i) {
		if (i != 11878)
			method223(-90);
		Class207.method1993(-1);
	}
}
