package net.zaros.client;

/* Class33 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class33 {
	int anInt327;
	String aString328;
	static Js5 aClass138_329;
	long aLong330;
	static Class404 aClass404_331 = new Class404();
	String aString332;
	int anInt333;
	static int[] anIntArray334;
	static OutgoingPacket aClass311_335 = new OutgoingPacket(39, 9);

	static final void method348(boolean bool, boolean bool_0_, int i) {
		if (!bool) {
			Class84.method819((byte) -93, TranslatableString.aClass120_1208.getTranslation(Class394.langID), bool_0_, i);
		}
	}

	public Class33(int i, String string, int i_1_, String string_2_, long l) {
		aString332 = string;
		aLong330 = l;
		anInt327 = i_1_;
		aString328 = string_2_;
		anInt333 = i;
	}

	public static void method349(int i) {
		anIntArray334 = null;
		aClass311_335 = null;
		aClass138_329 = null;
		aClass404_331 = null;
		if (i != 39) {
			aClass138_329 = null;
		}
	}
}
