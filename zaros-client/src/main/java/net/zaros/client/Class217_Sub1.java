package net.zaros.client;
/* Class217_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class217_Sub1 extends Class217 {
	private long aLong4544 = 0L;
	private long aLong4545 = 0L;
	private int anInt4546;
	private long aLong4547;
	private long[] aLongArray4548 = new long[10];
	private int anInt4549;

	final long method2036(boolean bool) {
		if (bool != true)
			anInt4549 = -4;
		return aLong4544;
	}

	final long method2032(byte i) {
		aLong4544 += method2037(true);
		if (aLong4544 < aLong4545)
			return (-aLong4544 + aLong4545) / 1000000L;
		int i_0_ = 38 / ((i + 54) / 35);
		return 0L;
	}

	private final long method2037(boolean bool) {
		long l = System.nanoTime();
		long l_1_ = -aLong4547 + l;
		if (bool != true)
			return 6L;
		aLong4547 = l;
		if (l_1_ > -5000000000L && l_1_ < 5000000000L) {
			aLongArray4548[anInt4549] = l_1_;
			if (anInt4546 < 1)
				anInt4546++;
			anInt4549 = (anInt4549 + 1) % 10;
		}
		long l_2_ = 0L;
		for (int i = 1; anInt4546 >= i; i++)
			l_2_ += aLongArray4548[(-i + anInt4549 + 10) % 10];
		return l_2_ / (long) anInt4546;
	}

	final void method2033(boolean bool) {
		aLong4547 = 0L;
		if (aLong4544 < aLong4545)
			aLong4544 += -aLong4544 + aLong4545;
		if (bool)
			method2034(true, -33L);
	}

	final int method2034(boolean bool, long l) {
		if (bool)
			return -117;
		if (aLong4545 <= aLong4544) {
			int i = 0;
			do {
				i++;
				aLong4545 += l;
			} while (i < 10 && aLong4544 > aLong4545);
			if (aLong4545 < aLong4544)
				aLong4545 = aLong4544;
			return i;
		}
		aLong4547 += aLong4545 - aLong4544;
		aLong4544 += -aLong4544 + aLong4545;
		aLong4545 += l;
		return 1;
	}

	Class217_Sub1() {
		aLong4547 = 0L;
		anInt4546 = 1;
		anInt4549 = 0;
		aLong4544 = System.nanoTime();
		aLong4545 = System.nanoTime();
	}
}
