package net.zaros.client;
/* Class219 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;

import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggPage;
import jagtheora.ogg.OggStreamState;
import jagtheora.ogg.OggSyncState;

abstract class Class219 {
	private boolean aBoolean2126;
	static int anInt2127;
	private OggSyncState anOggSyncState2128;
	private Class296_Sub15_Sub3 aClass296_Sub15_Sub3_2129;
	private OggPacket anOggPacket2130;
	static int[] anIntArray2131 = new int[32];
	private boolean aBoolean2132;
	private Class296_Sub15_Sub4 aClass296_Sub15_Sub4_2133;
	private byte[] aByteArray2134;
	private boolean aBoolean2135;
	private boolean aBoolean2136;
	private Class296_Sub15_Sub2 aClass296_Sub15_Sub2_2137;
	private OggPage anOggPage2138;
	static int camPosX;
	private String aString2140;
	private HashTable aClass263_2141;

	private final boolean method2043(byte i) {
		if (i >= -13)
			method2051(11, null);
		if (aClass296_Sub15_Sub2_2137 == null) {
			double d = (double) aClass296_Sub15_Sub3_2129.method2536((byte) 36);
			if (d != 0.0 && !((double) Class72.method771(-125) >= ((double) aClass296_Sub15_Sub3_2129.method2543(-115) + 1000.0 / d)))
				return false;
			return true;
		}
		if (aClass296_Sub15_Sub3_2129.method2542((byte) 78) && !(method2048(false) > aClass296_Sub15_Sub3_2129.method2538(0)))
			return false;
		return true;
	}

	private final void method2044(int i) {
		Class296_Sub15 class296_sub15 = (Class296_Sub15) aClass263_2141.getFirst(true);
		if (i == 1) {
			for (/**/; class296_sub15 != null; class296_sub15 = (Class296_Sub15) aClass263_2141.getNext(0)) {
				if (class296_sub15 != aClass296_Sub15_Sub3_2129) {
					while (class296_sub15.anOggStreamState4672.packetOut() == 1)
						class296_sub15.method2514(anOggPacket2130, -67);
				}
			}
			if (aClass296_Sub15_Sub3_2129 != null) {
				int i_0_ = 0;
				while_125_ : do {
					for (;;) {
						if (i_0_ >= 10)
							break while_125_;
						if (!method2043((byte) -34))
							break;
						if (aClass296_Sub15_Sub3_2129.anOggStreamState4672.packetOut() == 1)
							aClass296_Sub15_Sub3_2129.method2514(anOggPacket2130, i ^ ~0x45);
						else {
							method2045((byte) -56);
							break;
						}
						i_0_++;
					}
					break;
				} while (false);
			}
		}
	}

	final void method2045(byte i) {
		if (!aBoolean2126) {
			for (Class296_Sub15 class296_sub15 = (Class296_Sub15) aClass263_2141.getFirst(true); class296_sub15 != null; class296_sub15 = (Class296_Sub15) aClass263_2141.getNext(0)) {
				class296_sub15.method2519(16);
				class296_sub15.anOggStreamState4672.b();
			}
			if (i >= 0)
				aClass296_Sub15_Sub4_2133 = null;
			anOggPacket2130.b();
			anOggPage2138.b();
			anOggSyncState2128.b();
			aBoolean2126 = true;
		}
	}

	private final Class296_Sub15 method2046(int i) throws IOException {
		int i_1_ = 55 % ((i + 24) / 57);
		if (aBoolean2126)
			throw new IllegalStateException();
		if (aBoolean2132)
			return null;
		while (anOggSyncState2128.pageOut(anOggPage2138) <= 0) {
			int i_2_ = method2049((byte) -73, aByteArray2134);
			if (i_2_ == -1) {
				aBoolean2132 = true;
				return null;
			}
			if (i_2_ == 0)
				return null;
			if (!anOggSyncState2128.write(aByteArray2134, i_2_))
				throw new RuntimeException("");
		}
		int i_3_ = anOggPage2138.getSerialNumber();
		if (!anOggPage2138.isBOS()) {
			Class296_Sub15 class296_sub15 = (Class296_Sub15) aClass263_2141.get((long) i_3_);
			if (!class296_sub15.anOggStreamState4672.pageIn(anOggPage2138))
				throw new IllegalStateException();
			return class296_sub15;
		}
		OggStreamState oggstreamstate = new OggStreamState(i_3_);
		if (!oggstreamstate.pageIn(anOggPage2138))
			throw new IllegalStateException();
		if (oggstreamstate.packetPeek(anOggPacket2130) != 1)
			throw new IllegalStateException();
		Class296_Sub15 class296_sub15;
		if (aClass296_Sub15_Sub3_2129 == null && anOggPacket2130.isTheora()) {
			aClass296_Sub15_Sub3_2129 = new Class296_Sub15_Sub3(oggstreamstate);
			class296_sub15 = aClass296_Sub15_Sub3_2129;
		} else if (aClass296_Sub15_Sub2_2137 == null && anOggPacket2130.isVorbis()) {
			aClass296_Sub15_Sub2_2137 = new Class296_Sub15_Sub2(oggstreamstate);
			class296_sub15 = aClass296_Sub15_Sub2_2137;
		} else {
			byte[] is = anOggPacket2130.getData();
			StringBuffer stringbuffer = new StringBuffer();
			for (int i_4_ = 1; is.length > i_4_; i_4_++) {
				if (!Character.isLetterOrDigit((char) is[i_4_]))
					break;
				stringbuffer.append((char) is[i_4_]);
			}
			String string = stringbuffer.toString();
			if (string.equals("kate"))
				class296_sub15 = new Class296_Sub15_Sub4(oggstreamstate);
			else
				class296_sub15 = new Class296_Sub15_Sub1(oggstreamstate);
		}
		aClass263_2141.put((long) i_3_, class296_sub15);
		return class296_sub15;
	}

	final void method2047(int i, boolean bool) {
		if (aClass296_Sub15_Sub2_2137 != null) {
			Class296_Sub45_Sub2 class296_sub45_sub2 = aClass296_Sub15_Sub2_2137.method2533((byte) -81);
			if (class296_sub45_sub2 != null)
				class296_sub45_sub2.method2979((byte) -44, bool);
		}
		if (i > -83)
			aBoolean2132 = true;
		aBoolean2135 = !aBoolean2135;
	}

	final double method2048(boolean bool) {
		if (aClass296_Sub15_Sub2_2137 != null)
			return aClass296_Sub15_Sub2_2137.method2532((byte) -32);
		if (aClass296_Sub15_Sub3_2129 != null)
			return aClass296_Sub15_Sub3_2129.method2538(0);
		if (bool)
			aClass296_Sub15_Sub2_2137 = null;
		return 0.0;
	}

	abstract int method2049(byte i, byte[] is) throws IOException;

	final Class296_Sub15_Sub4 method2050(int i) {
		if (i != 4095)
			aClass296_Sub15_Sub2_2137 = null;
		return aClass296_Sub15_Sub4_2133;
	}

	static final Class266 method2051(int i, Packet class296_sub17) {
		String string = class296_sub17.gstr();
		Class252 class252 = StaticMethods.method312((byte) -33)[class296_sub17.g1()];
		Class357 class357 = Class110.method970(0)[class296_sub17.g1()];
		int i_5_ = class296_sub17.g2b();
		int i_6_ = class296_sub17.g2b();
		int i_7_ = class296_sub17.g1();
		int i_8_ = class296_sub17.g1();
		int i_9_ = class296_sub17.g1();
		int i_10_ = class296_sub17.g2();
		int i_11_ = class296_sub17.g2();
		int i_12_ = 114 % ((i - 46) / 56);
		int i_13_ = class296_sub17.g4();
		int i_14_ = class296_sub17.g4();
		int i_15_ = class296_sub17.g4();
		return new Class266(string, class252, class357, i_5_, i_6_, i_7_, i_8_, i_9_, i_10_, i_11_, i_13_, i_14_, i_15_);
	}

	final Class296_Sub15_Sub3 method2052(int i) {
		if (i != -1)
			return null;
		return aClass296_Sub15_Sub3_2129;
	}

	final Class296_Sub15_Sub2 method2053(int i) {
		if (i != -1828716544)
			return null;
		return aClass296_Sub15_Sub2_2137;
	}

	private final void method2054(int i) throws IOException {
		if (i != -1)
			aByteArray2134 = null;
		while (aClass296_Sub15_Sub3_2129.anOggStreamState4672.packetOut(anOggPacket2130) != 1) {
			Class296_Sub15 class296_sub15 = method2046(123);
			if (class296_sub15 == null) {
				if (aBoolean2132)
					method2044(i ^ ~0x1);
				return;
			}
			if (class296_sub15 == aClass296_Sub15_Sub4_2133)
				method2060((byte) 71);
		}
		aClass296_Sub15_Sub3_2129.method2514(anOggPacket2130, -66);
	}

	final boolean method2055(byte i) {
		if (!aBoolean2126 && !aBoolean2132)
			return false;
		if (i != -22)
			aBoolean2136 = true;
		if (aClass296_Sub15_Sub2_2137 != null && aClass296_Sub15_Sub2_2137.method2531(i + 20) > 0)
			return false;
		return true;
	}

	public static void method2056(boolean bool) {
		if (bool == true)
			anIntArray2131 = null;
	}

	final void method2057(byte i) throws IOException {
		if (i <= -103 && !aBoolean2135) {
			while (!aBoolean2126) {
				Class296_Sub15 class296_sub15;
				if (!aBoolean2136) {
					class296_sub15 = method2046(-82);
					if (class296_sub15 == null) {
						if (aBoolean2132)
							method2044(1);
						break;
					}
					if (class296_sub15 == null)
						throw new IllegalStateException();
					aBoolean2136 = true;
				} else
					class296_sub15 = ((Class296_Sub15) (aClass263_2141.get((long) anOggPage2138.getSerialNumber())));
				if (aClass296_Sub15_Sub2_2137 != class296_sub15) {
					if (!(class296_sub15 instanceof Class296_Sub15_Sub4)) {
						if (class296_sub15 == aClass296_Sub15_Sub3_2129) {
							if (aClass296_Sub15_Sub2_2137 == null && !aBoolean2135) {
								for (int i_16_ = 0; i_16_ < 10; i_16_++) {
									if (!method2043((byte) -83))
										break;
									method2054(-1);
									if (aBoolean2126)
										break;
								}
								break;
							}
						} else {
							while (class296_sub15.anOggStreamState4672.packetOut(anOggPacket2130) == 1) {
								if (class296_sub15.anInt4670 == 1 && (class296_sub15 instanceof Class296_Sub15_Sub4))
									method2059(aString2140, (byte) -93);
								class296_sub15.method2514(anOggPacket2130, -75);
							}
						}
					} else
						method2060((byte) -114);
				} else {
					if (aClass296_Sub15_Sub2_2137.method2531(-2) >= 50)
						break;
					while (aClass296_Sub15_Sub2_2137.anOggStreamState4672.packetOut(anOggPacket2130) == 1) {
						aClass296_Sub15_Sub2_2137.method2514(anOggPacket2130, -107);
						method2060((byte) -123);
						if (aClass296_Sub15_Sub3_2129 != null) {
							double d = aClass296_Sub15_Sub3_2129.method2538(0);
							for (int i_17_ = 0; i_17_ < 10; i_17_++) {
								if (!method2043((byte) -100))
									break;
								method2054(-1);
								if (aBoolean2126)
									return;
							}
							if (aClass296_Sub15_Sub3_2129.method2538(0) > d)
								return;
						}
						if (aClass296_Sub15_Sub2_2137.method2531(-2) >= 50)
							return;
					}
				}
				aBoolean2136 = false;
			}
		}
	}

	static final boolean method2058(int i, Class247[][][] class247s, byte i_18_, int i_19_, boolean bool, int i_20_) {
		byte i_21_ = bool ? (byte) 1 : (byte) (Class296_Sub2.anInt4590 & 0xff);
		if (i_18_ != 1)
			method2051(100, null);
		if (Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i][i_19_] == i_21_)
			return false;
		if (((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i][i_19_]) & 0x4) == 0)
			return false;
		int i_22_ = 0;
		int i_23_ = 0;
		Class41_Sub5.anIntArray3756[i_22_] = i;
		Class338_Sub3_Sub4_Sub2.anIntArray6672[i_22_++] = i_19_;
		Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i][i_19_] = i_21_;
		while (i_23_ != i_22_) {
			int i_24_ = Class41_Sub5.anIntArray3756[i_23_] & 0xffff;
			int i_25_ = (Class41_Sub5.anIntArray3756[i_23_] & 0xffe88d) >> 16;
			int i_26_ = Class41_Sub5.anIntArray3756[i_23_] >> 24 & 0xff;
			int i_27_ = Class338_Sub3_Sub4_Sub2.anIntArray6672[i_23_] & 0xffff;
			int i_28_ = Class338_Sub3_Sub4_Sub2.anIntArray6672[i_23_] >> 16 & 0xff;
			i_23_ = i_23_ + 1 & 0xfff;
			boolean bool_29_ = false;
			if (((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_][i_27_]) & 0x4) == 0)
				bool_29_ = true;
			boolean bool_30_ = false;
			if (class247s != null) {
				int i_31_ = FileWorker.anInt3005 + 1;
				while_127_ : for (/**/; i_31_ <= 3; i_31_++) {
					if (class247s[i_31_] != null && ((Class41_Sub18.aByteArrayArrayArray3786[i_31_][i_24_][i_27_]) & 0x8) == 0) {
						if (bool_29_ && class247s[i_31_][i_24_][i_27_] != null) {
							if ((class247s[i_31_][i_24_][i_27_].aClass338_Sub3_Sub4_2348) != null) {
								int i_32_ = Class220.method2068(-30724, i_25_);
								if (i_32_ == (class247s[i_31_][i_24_][i_27_].aClass338_Sub3_Sub4_2348.aShort6570) || ((class247s[i_31_][i_24_][i_27_].aClass338_Sub3_Sub4_2343) != null && i_32_ == (class247s[i_31_][i_24_][i_27_].aClass338_Sub3_Sub4_2343.aShort6570)))
									continue;
								if (i_26_ != 0) {
									int i_33_ = Class220.method2068(-30724, i_26_);
									if (((class247s[i_31_][i_24_][i_27_].aClass338_Sub3_Sub4_2348.aShort6570) == i_33_) || ((class247s[i_31_][i_24_][i_27_].aClass338_Sub3_Sub4_2343) != null && (i_33_ == (class247s[i_31_][i_24_][i_27_].aClass338_Sub3_Sub4_2343.aShort6570))))
										continue;
								}
								if (i_28_ != 0) {
									int i_34_ = Class220.method2068(-30724, i_28_);
									if (((class247s[i_31_][i_24_][i_27_].aClass338_Sub3_Sub4_2348.aShort6570) == i_34_) || ((class247s[i_31_][i_24_][i_27_].aClass338_Sub3_Sub4_2343) != null && (i_34_ == (class247s[i_31_][i_24_][i_27_].aClass338_Sub3_Sub4_2343.aShort6570))))
										continue;
								}
							}
							Class247 class247 = class247s[i_31_][i_24_][i_27_];
							if (class247.aClass234_2341 != null) {
								for (Class234 class234 = class247.aClass234_2341; class234 != null; class234 = class234.aClass234_2224) {
									Class338_Sub3_Sub1 class338_sub3_sub1 = class234.aClass338_Sub3_Sub1_2226;
									if (class338_sub3_sub1 instanceof Interface14) {
										Interface14 interface14 = (Interface14) class338_sub3_sub1;
										int i_35_ = interface14.method54(-11077);
										if (i_35_ == 21)
											i_35_ = 19;
										int i_36_ = interface14.method59(33);
										int i_37_ = i_36_ << 6 | i_35_;
										if (i_37_ == i_25_ || i_26_ != 0 && i_37_ == i_26_ || i_28_ != 0 && i_28_ == i_37_)
											continue while_127_;
									}
								}
							}
						}
						Class247 class247 = class247s[i_31_][i_24_][i_27_];
						if (class247 != null && class247.aClass234_2341 != null) {
							for (Class234 class234 = class247.aClass234_2341; class234 != null; class234 = class234.aClass234_2224) {
								Class338_Sub3_Sub1 class338_sub3_sub1 = class234.aClass338_Sub3_Sub1_2226;
								if ((class338_sub3_sub1.aShort6559 != class338_sub3_sub1.aShort6560) || (class338_sub3_sub1.aShort6558 != class338_sub3_sub1.aShort6564)) {
									for (int i_38_ = class338_sub3_sub1.aShort6560; (i_38_ <= class338_sub3_sub1.aShort6559); i_38_++) {
										for (int i_39_ = (class338_sub3_sub1.aShort6564); (class338_sub3_sub1.aShort6558 >= i_39_); i_39_++)
											Class198.aByteArrayArrayArray2003[i_31_][i_38_][i_39_] = i_21_;
									}
								}
							}
						}
						Class198.aByteArrayArrayArray2003[i_31_][i_24_][i_27_] = i_21_;
						bool_30_ = true;
					}
				}
			}
			if (bool_30_) {
				int i_40_ = Class360_Sub2.aSArray5304[FileWorker.anInt3005 + 1].method3355(i_27_, (byte) -116, i_24_);
				if (i_40_ > Class41_Sub23.anIntArray3801[i_20_])
					Class41_Sub23.anIntArray3801[i_20_] = i_40_;
				int i_41_ = i_24_ << 9;
				int i_42_ = i_27_ << 9;
				if (Class368_Sub9.anIntArray5473[i_20_] <= i_41_) {
					if (i_41_ > Class309.anIntArray2754[i_20_])
						Class309.anIntArray2754[i_20_] = i_41_;
				} else
					Class368_Sub9.anIntArray5473[i_20_] = i_41_;
				if (i_42_ >= Class404.anIntArray3382[i_20_]) {
					if (i_42_ > StaticMethods.anIntArray3143[i_20_])
						StaticMethods.anIntArray3143[i_20_] = i_42_;
				} else
					Class404.anIntArray3382[i_20_] = i_42_;
			}
			if (!bool_29_) {
				if (i_24_ >= 1 && i_21_ != (Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ - 1][i_27_])) {
					Class41_Sub5.anIntArray3756[i_22_] = Class48.bitOR(-754974720, Class48.bitOR(1179648, i_24_ - 1));
					Class338_Sub3_Sub4_Sub2.anIntArray6672[i_22_] = Class48.bitOR(i_27_, 1245184);
					Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ - 1][i_27_] = i_21_;
					i_22_ = i_22_ + 1 & 0xfff;
				}
				if (++i_27_ < Class296_Sub38.currentMapSizeY) {
					if (i_24_ - 1 >= 0 && i_21_ != (Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ - 1][i_27_]) && ((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_][i_27_]) & 0x4) == 0 && ((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_ - 1][i_27_ - 1]) & 0x4) == 0) {
						Class41_Sub5.anIntArray3756[i_22_] = Class48.bitOR(Class48.bitOR(i_24_ - 1, 1179648), 1375731712);
						Class338_Sub3_Sub4_Sub2.anIntArray6672[i_22_] = Class48.bitOR(1245184, i_27_);
						Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ - 1][i_27_] = i_21_;
						i_22_ = i_22_ + 1 & 0xfff;
					}
					if ((Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_][i_27_]) != i_21_) {
						Class41_Sub5.anIntArray3756[i_22_] = Class48.bitOR(Class48.bitOR(i_24_, 5373952), 318767104);
						Class338_Sub3_Sub4_Sub2.anIntArray6672[i_22_] = Class48.bitOR(i_27_, 5439488);
						i_22_ = i_22_ + 1 & 0xfff;
						Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_][i_27_] = i_21_;
					}
					if (Class198.currentMapSizeX > i_24_ + 1 && i_21_ != (Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ + 1][i_27_]) && ((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_][i_27_]) & 0x4) == 0 && ((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_ + 1][i_27_ - 1]) & 0x4) == 0) {
						Class41_Sub5.anIntArray3756[i_22_] = Class48.bitOR(Class48.bitOR(5373952, i_24_ + 1), -1845493760);
						Class338_Sub3_Sub4_Sub2.anIntArray6672[i_22_] = Class48.bitOR(i_27_, 5439488);
						Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ + 1][i_27_] = i_21_;
						i_22_ = i_22_ + 1 & 0xfff;
					}
				}
				i_27_--;
				if (i_24_ + 1 < Class198.currentMapSizeX && (Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ + 1][i_27_]) != i_21_) {
					Class41_Sub5.anIntArray3756[i_22_] = Class48.bitOR(1392508928, Class48.bitOR(9568256, i_24_ + 1));
					Class338_Sub3_Sub4_Sub2.anIntArray6672[i_22_] = Class48.bitOR(9633792, i_27_);
					i_22_ = i_22_ + 1 & 0xfff;
					Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ + 1][i_27_] = i_21_;
				}
				if (--i_27_ >= 0) {
					if (i_24_ - 1 >= 0 && i_21_ != (Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ - 1][i_27_]) && ((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_][i_27_]) & 0x4) == 0 && ((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_ - 1][i_27_ + 1]) & 0x4) == 0) {
						Class41_Sub5.anIntArray3756[i_22_] = Class48.bitOR(301989888, Class48.bitOR(13762560, i_24_ - 1));
						Class338_Sub3_Sub4_Sub2.anIntArray6672[i_22_] = Class48.bitOR(i_27_, 13828096);
						Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ - 1][i_27_] = i_21_;
						i_22_ = i_22_ + 1 & 0xfff;
					}
					if (i_21_ != (Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_][i_27_])) {
						Class41_Sub5.anIntArray3756[i_22_] = Class48.bitOR(Class48.bitOR(13762560, i_24_), -1828716544);
						Class338_Sub3_Sub4_Sub2.anIntArray6672[i_22_] = Class48.bitOR(i_27_, 13828096);
						i_22_ = i_22_ + 1 & 0xfff;
						Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_][i_27_] = i_21_;
					}
					if (Class198.currentMapSizeX > i_24_ + 1 && i_21_ != (Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ + 1][i_27_]) && ((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_][i_27_]) & 0x4) == 0 && ((Class41_Sub18.aByteArrayArrayArray3786[FileWorker.anInt3005][i_24_ + 1][i_27_ + 1]) & 0x4) == 0) {
						Class41_Sub5.anIntArray3756[i_22_] = Class48.bitOR(Class48.bitOR(i_24_ + 1, 9568256), -771751936);
						Class338_Sub3_Sub4_Sub2.anIntArray6672[i_22_] = Class48.bitOR(9633792, i_27_);
						i_22_ = i_22_ + 1 & 0xfff;
						Class198.aByteArrayArrayArray2003[FileWorker.anInt3005][i_24_ + 1][i_27_] = i_21_;
					}
				}
			}
		}
		if (Class41_Sub23.anIntArray3801[i_20_] != -1000000) {
			Class41_Sub23.anIntArray3801[i_20_] += 40;
			Class368_Sub9.anIntArray5473[i_20_] -= 512;
			Class309.anIntArray2754[i_20_] += 512;
			StaticMethods.anIntArray3143[i_20_] += 512;
			Class404.anIntArray3382[i_20_] -= 512;
		}
		return true;
	}

	final void method2059(String string, byte i) {
		aString2140 = string;
		int i_43_ = -38 % ((i + 22) / 57);
		if (aString2140 == null)
			aClass296_Sub15_Sub4_2133 = null;
		else {
			if (aClass296_Sub15_Sub4_2133 != null && !aString2140.equals(aClass296_Sub15_Sub4_2133.method2544(125)))
				aClass296_Sub15_Sub4_2133 = null;
			if (aClass296_Sub15_Sub4_2133 == null) {
				for (Class296_Sub15 class296_sub15 = (Class296_Sub15) aClass263_2141.getFirst(true); class296_sub15 != null; class296_sub15 = (Class296_Sub15) aClass263_2141.getNext(0)) {
					if (class296_sub15 instanceof Class296_Sub15_Sub4) {
						Class296_Sub15_Sub4 class296_sub15_sub4 = (Class296_Sub15_Sub4) class296_sub15;
						if (aString2140.equals(class296_sub15_sub4.method2544(-124))) {
							aClass296_Sub15_Sub4_2133 = class296_sub15_sub4;
							break;
						}
					}
				}
			}
		}
	}

	Class219(int i) {
		if (!Class366_Sub4.method3779("jagtheora", (byte) -17))
			throw new RuntimeException("Failed to load jagtheora library");
		aByteArray2134 = new byte[i];
		anOggSyncState2128 = new OggSyncState();
		anOggPage2138 = new OggPage();
		anOggPacket2130 = new OggPacket();
		aClass263_2141 = new HashTable(8);
	}

	private final void method2060(byte i) {
		int i_44_ = -34 % ((i + 35) / 49);
		for (Class296_Sub15 class296_sub15 = (Class296_Sub15) aClass263_2141.getFirst(true); class296_sub15 != null; class296_sub15 = (Class296_Sub15) aClass263_2141.getNext(0)) {
			if (class296_sub15 instanceof Class296_Sub15_Sub4) {
				Class296_Sub15_Sub4 class296_sub15_sub4 = (Class296_Sub15_Sub4) class296_sub15;
				while (class296_sub15_sub4.anInt4670 <= 8 || (method2048(false) > (double) class296_sub15_sub4.method2546(0))) {
					if (class296_sub15_sub4.anOggStreamState4672.packetOut(anOggPacket2130) != 1)
						break;
					class296_sub15_sub4.method2514(anOggPacket2130, -105);
				}
			}
		}
		method2059(aString2140, (byte) 120);
	}
}
