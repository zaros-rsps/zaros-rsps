package net.zaros.client;
/* Class140 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Color;

final class Class140 implements Interface10 {
	boolean aBoolean3579;
	int anInt3580;
	Class357 aClass357_3581;
	Class252 aClass252_3582;
	int anInt3583;
	int anInt3584;
	int anInt3585;
	int anInt3586;
	static Color[] aColorArray3587 = {new Color(9179409), new Color(16777215), new Color(16726277), new Color(16726277)};
	int anInt3588;
	int anInt3589;
	int anInt3590;

	static final void method1467(int i, int i_0_, int i_1_, boolean bool, int i_2_) {
		if (i_2_ <= i) {
			for (int i_3_ = i_2_; i > i_3_; i_3_++)
				Class296_Sub51_Sub37.anIntArrayArray6536[i_3_][i_1_] = i_0_;
		} else {
			for (int i_4_ = i; i_4_ < i_2_; i_4_++)
				Class296_Sub51_Sub37.anIntArrayArray6536[i_4_][i_1_] = i_0_;
		}
		if (bool)
			aColorArray3587 = null;
	}

	static final void method1468(int i, int i_5_, int i_6_, int i_7_, byte[][][] is, int[] is_8_, int[] is_9_, int[] is_10_, int[] is_11_, int[] is_12_, int i_13_, byte i_14_, int i_15_, int i_16_, boolean bool, boolean bool_17_, int i_18_, int i_19_, boolean bool_20_) {
		Class12.aBoolean133 = true;
		Class368_Sub2.aBoolean5427 = Class16_Sub2.aHa3733.q() > 0;
		Class208.aBoolean2089 = bool_17_;
		Class296_Sub45_Sub2.anInt6288 = i_5_ >> Class313.anInt2779;
		Class296_Sub39_Sub20_Sub2.anInt6728 = i_7_ >> Class313.anInt2779;
		Class395.anInt3315 = i_5_;
		StaticMethods.anInt5942 = i_7_;
		Class41_Sub19.anInt3791 = i_6_;
		Class296_Sub39_Sub4.anInt5744 = Class296_Sub45_Sub2.anInt6288 - Class379_Sub2.anInt5684;
		if (Class296_Sub39_Sub4.anInt5744 < 0) {
			Class56.anInt662 = -Class296_Sub39_Sub4.anInt5744;
			Class296_Sub39_Sub4.anInt5744 = 0;
		} else
			Class56.anInt662 = 0;
		AdvancedMemoryCache.anInt1166 = Class296_Sub39_Sub20_Sub2.anInt6728 - Class379_Sub2.anInt5684;
		if (AdvancedMemoryCache.anInt1166 < 0) {
			Class69_Sub3.anInt5729 = -AdvancedMemoryCache.anInt1166;
			AdvancedMemoryCache.anInt1166 = 0;
		} else
			Class69_Sub3.anInt5729 = 0;
		Class124.anInt1281 = Class296_Sub45_Sub2.anInt6288 + Class379_Sub2.anInt5684;
		if (Class124.anInt1281 > Class228.anInt2201)
			Class124.anInt1281 = Class228.anInt2201;
		Class41_Sub22.anInt3800 = Class296_Sub39_Sub20_Sub2.anInt6728 + Class379_Sub2.anInt5684;
		if (Class41_Sub22.anInt3800 > Class368_Sub12.anInt5488)
			Class41_Sub22.anInt3800 = Class368_Sub12.anInt5488;
		boolean[][] bools = Class296_Sub15_Sub2.aBooleanArrayArray6006;
		boolean[][] bools_21_ = Class370.aBooleanArrayArray3140;
		if (Class208.aBoolean2089) {
			for (int i_22_ = 0; i_22_ < Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 + 2; i_22_++) {
				int i_23_ = 0;
				int i_24_ = 0;
				for (int i_25_ = 0; (i_25_ < Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 + 2); i_25_++) {
					if (i_25_ > 1)
						Class285.anIntArray2624[i_25_ - 2] = i_23_;
					i_23_ = i_24_;
					int i_26_ = (Class296_Sub45_Sub2.anInt6288 - Class379_Sub2.anInt5684 + i_22_);
					int i_27_ = (Class296_Sub39_Sub20_Sub2.anInt6728 - Class379_Sub2.anInt5684 + i_25_);
					if (i_26_ >= 0 && i_27_ >= 0 && i_26_ < Class228.anInt2201 && i_27_ < Class368_Sub12.anInt5488) {
						int i_28_ = i_26_ << Class313.anInt2779;
						int i_29_ = i_27_ << Class313.anInt2779;
						int i_30_ = (Class244.aSArray2320[Class244.aSArray2320.length - 1].method3355(i_27_, (byte) -106, i_26_) - (1000 << Class313.anInt2779 - 7));
						int i_31_ = ((Class52.aSArray636 != null ? (Class52.aSArray636[0].method3355(i_27_, (byte) -110, i_26_) + Js5TextureLoader.anInt3440) : (Class244.aSArray2320[0].method3355(i_27_, (byte) -110, i_26_) + Js5TextureLoader.anInt3440)) + (1000 << Class313.anInt2779 - 7));
						i_24_ = (i_18_ >= 0 ? Class16_Sub2.aHa3733.r(i_28_, i_30_, i_29_, i_28_, i_31_, i_29_, i_18_) : Class16_Sub2.aHa3733.JA(i_28_, i_30_, i_29_, i_28_, i_31_, i_29_));
						Class370.aBooleanArrayArray3140[i_22_][i_25_] = i_24_ == 0;
					} else {
						i_24_ = -1;
						Class370.aBooleanArrayArray3140[i_22_][i_25_] = false;
					}
					if (i_22_ > 0 && i_25_ > 0) {
						int i_32_ = (Class285.anIntArray2624[i_25_ - 1] & Class285.anIntArray2624[i_25_] & i_23_ & i_24_);
						Class296_Sub15_Sub2.aBooleanArrayArray6006[i_22_ - 1][i_25_ - 1] = i_32_ == 0;
					}
				}
				Class285.anIntArray2624[(Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684)] = i_23_;
				Class285.anIntArray2624[(Class379_Sub2.anInt5684 + Class379_Sub2.anInt5684 + 1)] = i_24_;
			}
			if (i_18_ >= 0)
				Class12.aBoolean133 = false;
			else {
				Class96.anIntArray1049 = is_8_;
				TextureOperation.anIntArray5039 = is_9_;
				Class262.anIntArray2447 = is_10_;
				Class368_Sub23.anIntArray5575 = is_11_;
				Class360_Sub6.anIntArray5329 = is_12_;
				Class41_Sub21.method479(Class16_Sub2.aHa3733, (byte) -62, i_13_);
			}
		} else {
			if (Class146.aBooleanArrayArray1444 == null)
				Class146.aBooleanArrayArray1444 = (new boolean[Class228.anInt2201 + Class228.anInt2201 + 1][Class368_Sub12.anInt5488 + Class228.anInt2201 + 1]);
			for (int i_33_ = 0; i_33_ < Class146.aBooleanArrayArray1444.length; i_33_++) {
				for (int i_34_ = 0; i_34_ < Class146.aBooleanArrayArray1444[0].length; i_34_++)
					Class146.aBooleanArrayArray1444[i_33_][i_34_] = true;
			}
			Class370.aBooleanArrayArray3140 = Class146.aBooleanArrayArray1444;
			Class296_Sub15_Sub2.aBooleanArrayArray6006 = Class146.aBooleanArrayArray1444;
			Class296_Sub39_Sub4.anInt5744 = 0;
			AdvancedMemoryCache.anInt1166 = 0;
			Class124.anInt1281 = Class228.anInt2201;
			Class41_Sub22.anInt3800 = Class368_Sub12.anInt5488;
			Class12.aBoolean133 = false;
		}
		Class15.method231(104, Class16_Sub2.aHa3733);
		if (!CS2Stack.aClass264_2249.aBoolean2470) {
			Class404 class404 = CS2Stack.aClass264_2249.aClass404_2468;
			for (Class338_Sub2 class338_sub2 = (Class338_Sub2) class404.method4160((byte) -87); class338_sub2 != null; class338_sub2 = (Class338_Sub2) class404.method4163(-24917)) {
				class338_sub2.method3438(false);
				SubCache.method3258(class338_sub2, 5362);
			}
		}
		if (Class368_Sub2.aBoolean5427) {
			for (int i_35_ = 0; i_35_ < Class368_Sub4.anInt5440; i_35_++)
				Class302.aClass93Array2721[i_35_].method854(bool, false, i);
		}
		if (Class338_Sub3_Sub2.aBoolean6566) {
			Class296_Sub39_Sub19.anIntArray6248 = Class16_Sub2.aHa3733.Y();
			Class16_Sub2.aHa3733.K(Class404.anIntArray3383);
			int i_36_ = ((Class404.anIntArray3383[2] - Class404.anIntArray3383[0]) / Class247.anInt2336);
			for (int i_37_ = 0; i_37_ < Class247.anInt2336 - 1; i_37_++)
				Class379_Sub2_Sub1.anIntArray6605[i_37_] = i_36_ * (i_37_ + 1) + Class377.anIntArray3192[i_37_];
			for (int i_38_ = 0; i_38_ < StaticMethods.aClass142Array5951.length; i_38_++)
				StaticMethods.aClass142Array5951[i_38_].method1476();
		}
		if (Class351.aClass247ArrayArrayArray3041 != null) {
			if (Class338_Sub3_Sub2.aBoolean6566)
				Class122.method1037(0);
			Class69_Sub1.method732(true);
			Class16_Sub2.aHa3733.ra(-1, 1583160, 40, 127);
			ReferenceWrapper.method2790(true, is, i_13_, i_14_, i_18_, i_19_, bool_20_);
			if (Class338_Sub3_Sub2.aBoolean6566)
				SubCache.method3253();
			Class16_Sub2.aHa3733.pa();
			Class69_Sub1.method732(false);
		}
		ReferenceWrapper.method2790(false, is, i_13_, i_14_, i_18_, i_19_, bool_20_);
		if (Class338_Sub3_Sub2.aBoolean6566) {
			for (int i_39_ = 0; i_39_ < Class368_Sub9.anInt5477; i_39_++)
				Class241_Sub2_Sub2.aBooleanArrayArrayArray5907[i_39_] = Class121.aBooleanArrayArrayArray1268[i_39_];
			Class122.method1037(0);
			for (int i_40_ = 0; i_40_ < StaticMethods.aClass142Array5951.length; i_40_++)
				StaticMethods.aClass142Array5951[i_40_].method1476();
		}
		if (Class338_Sub3_Sub2.aBoolean6566) {
			SubCache.method3253();
			for (int i_41_ = 0; i_41_ < Class368_Sub9.anInt5477; i_41_++)
				Class121.aBooleanArrayArrayArray1268[i_41_] = Class241_Sub2_Sub2.aBooleanArrayArrayArray5907[i_41_];
			if (Class377.anInt3190 == 2) {
				if (aa.aLongArray47[0] < aa.aLongArray47[1]) {
					if ((Class379_Sub2_Sub1.anIntArray6605[0] + Class377.anIntArray3192[0]) > Class404.anIntArray3383[0])
						Class377.anIntArray3192[0]++;
				} else if (aa.aLongArray47[0] > aa.aLongArray47[1] && ((Class379_Sub2_Sub1.anIntArray6605[0] + Class377.anIntArray3192[0]) < Class404.anIntArray3383[2]))
					Class377.anIntArray3192[0]--;
			}
		}
		if (!Class208.aBoolean2089) {
			Class296_Sub15_Sub2.aBooleanArrayArray6006 = bools;
			Class370.aBooleanArrayArray3140 = bools_21_;
		}
		Class122_Sub3.method1048();
	}

	public static void method1469(byte i) {
		if (i > -127)
			aColorArray3587 = null;
		aColorArray3587 = null;
	}

	public final Class294 method45(byte i) {
		int i_42_ = -10 % ((i + 6) / 44);
		return Animation.aClass294_422;
	}

	Class140(int i, Class252 class252, Class357 class357, int i_43_, int i_44_, int i_45_, int i_46_, int i_47_, int i_48_, int i_49_, boolean bool) {
		anInt3580 = i_49_;
		anInt3584 = i_48_;
		anInt3586 = i_43_;
		anInt3588 = i;
		anInt3583 = i_45_;
		anInt3585 = i_44_;
		aClass357_3581 = class357;
		anInt3590 = i_47_;
		aBoolean3579 = bool;
		anInt3589 = i_46_;
		aClass252_3582 = class252;
	}

	static final boolean method1470(char c, byte i) {
		try {
			int i_50_ = -108 % ((-31 - i) / 61);
			if (c != '\u00a0' && c != ' ' && c != '_' && c != '-')
				return false;
			return true;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "ho.E(" + c + ',' + i + ')');
		}
	}
}
