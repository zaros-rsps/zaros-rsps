package net.zaros.client;
/* Class358 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.net.URL;

final class Class358 implements Runnable {
	private volatile boolean aBoolean3084;
	private Class4[] aClass4Array3085;
	private Class278 aClass278_3086;
	static int[] anIntArray3087;
	private Thread aThread3088;

	static final void method3712(int i) {
		Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6277.aClass185_2053, (byte) 106, TranslatableString.aClass311_1262);
		class296_sub1.out.p4(Class296_Sub45_Sub2.interfaceCounter);
		Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
	}

	final Class4 method3713(int i, boolean bool) {
		if (bool != true) {
			method3713(-118, true);
		}
		if (aClass4Array3085 == null || i < 0 || aClass4Array3085.length <= i) {
			return null;
		}
		return aClass4Array3085[i];
	}

	final boolean method3714(int i) {
		if (aBoolean3084) {
			return true;
		}
		if (aClass278_3086 == null) {
			try {
				int i_0_ = BITConfigsLoader.liveModeWhere != Class41_Sub29.modeWhere ? Class338_Sub8.gameWorld.worldId + 7000 : 80;
				aClass278_3086 = Class252.aClass398_2383.method4130((byte) -121, new URL("http://" + Class338_Sub8.gameWorld.ipAddress + ":" + i_0_ + "/news.ws?game=" + Class296_Sub50.game.anInt340));
			} catch (java.net.MalformedURLException malformedurlexception) {
				return true;
			}
		}
		if (aClass278_3086 == null || aClass278_3086.anInt2540 == 2) {
			return true;
		}
		if (aClass278_3086.anInt2540 != 1) {
			return false;
		}
		if (aThread3088 == null) {
			aThread3088 = new Thread(this);
			aThread3088.start();
		}
		if (i != 80) {
			modeWhats(-14);
		}
		return aBoolean3084;
	}

	@Override
	public final void run() {
		try {
			BufferedReader bufferedreader = new BufferedReader(new InputStreamReader((DataInputStream) aClass278_3086.anObject2539));
			String string = bufferedreader.readLine();
			Class196 class196 = ConfigsRegister.method586((byte) 53);
			for (/**/; string != null; string = bufferedreader.readLine()) {
				class196.method1937(0, string);
			}
			String[] strings = class196.method1936(true);
			if (strings.length % 3 != 0) {
				return;
			}
			aClass4Array3085 = new Class4[strings.length / 3];
			for (int i = 0; strings.length > i; i += 3) {
				aClass4Array3085[i / 3] = new Class4(strings[i], strings[i + 1], strings[i + 2]);
			}
		} catch (java.io.IOException ioexception) {
			/* empty */
		}
		aBoolean3084 = true;
	}

	public static void method3715(int i) {
		anIntArray3087 = null;
		if (i < 91) {
			method3712(50);
		}
	}

	public Class358() {
		/* empty */
	}

	static final ModeWhat[] modeWhats(int i) {
		int i_1_ = 2 / ((i - 33) / 41);
		return new ModeWhat[]{Class296_Sub51_Sub27.liveModeWhat, Js5TextureLoader.releaseCandidateModeWhat, Class241_Sub2_Sub2.wipModeWhat};
	}
}
