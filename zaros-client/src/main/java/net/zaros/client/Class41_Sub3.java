package net.zaros.client;
/* Class41_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub3 extends Class41 {
	static int[] anIntArray3743;
	static boolean aBoolean3744 = false;

	Class41_Sub3(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final void method386(int i) {
		anInt389 = method383((byte) 110);
		if (i != 2)
			method401(45);
	}

	final int method400(int i) {
		if (i <= 114)
			return 20;
		return anInt389;
	}

	final int method383(byte i) {
		if (i != 110)
			aBoolean3744 = false;
		if (aClass296_Sub50_392.method3054(75))
			return 1;
		return 0;
	}

	Class41_Sub3(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	public static void method401(int i) {
		anIntArray3743 = null;
		if (i != 1)
			method401(-68);
	}

	final int method380(int i, byte i_0_) {
		if (i_0_ != 41)
			method401(-109);
		return 3;
	}

	final void method381(int i, byte i_1_) {
		if (i_1_ != -110)
			aBoolean3744 = false;
		anInt389 = i;
	}
}
