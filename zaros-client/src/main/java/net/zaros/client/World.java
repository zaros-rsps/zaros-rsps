package net.zaros.client;

/* Class112 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class World {
	String ipAddress;
	private boolean usingWeb = true;
	int worldId;
	private boolean aBoolean1155;
	int port;
	int webPort = 43594;
	static IncomingPacket aClass231_1158 = new IncomingPacket(132, 5);
	static int anInt1159;
	static AdvancedMemoryCache aClass113_1160 = new AdvancedMemoryCache(4);
	static float aFloat1161;

	public static void method977(int i) {
		int i_0_ = 76 % ((50 - i) / 44);
		aClass113_1160 = null;
		aClass231_1158 = null;
	}

	final void method978(byte i) {
		if (i <= 114) {
			anInt1159 = 95;
		}
		if (usingWeb) {
			if (aBoolean1155) {
				aBoolean1155 = false;
			} else {
				usingWeb = false;
			}
		} else {
			usingWeb = true;
			aBoolean1155 = true;
		}
	}
	
	final Class278 createSocket(int i, Class398 class398) {
		if (i != 43594) {
			method978((byte) -73);
		}
		String ipAddress = Constants.HOST_ADDRESS;
		return class398.method4122(aBoolean1155, 0, !usingWeb ? port : webPort, ipAddress);
	}
	
	final Class278 createWorldSocket(int i, Class398 class398) {
		String ipAddress = Constants.HOST_ADDRESS;
		int worldId = Class296_Sub51_Sub27_Sub1.aClass112_6733.worldId;
		int port = (!usingWeb ? this.port : webPort) + worldId;
		return class398.method4122(aBoolean1155, 0, port, ipAddress);
	}

	final boolean method980(byte i, World class112_1_) {
		if (i != 122) {
			method977(-26);
		}
		if (class112_1_ == null) {
			return false;
		}
		if (worldId != class112_1_.worldId || !ipAddress.equals(class112_1_.ipAddress)) {
			return false;
		}
		return true;
	}

	public World() {
		aBoolean1155 = false;
		port = 43594;
	}
}
