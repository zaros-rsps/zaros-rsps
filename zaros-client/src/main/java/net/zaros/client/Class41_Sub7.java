package net.zaros.client;

/* Class41_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub7 extends Class41 {
	static AdvancedMemoryCache aClass113_3761 = new AdvancedMemoryCache(8);
	static short[] aShortArray3762 = new short[256];

	final void method381(int i, byte i_0_) {
		if (i_0_ != -110)
			method418(29);
		anInt389 = i;
	}

	static final int method418(int i) {
		if (i != 256)
			return 113;
		if (ConfigsRegister.anInt3674 == 1)
			return Class359.anInt3089;
		return Class261.anInt2424;
	}

	final void method386(int i) {
		if (anInt389 != 1 && anInt389 != 0)
			anInt389 = method383((byte) 110);
		if (i != 2)
			aClass113_3761 = null;
	}

	final int method383(byte i) {
		if (i != 110)
			method418(-82);
		return 1;
	}

	Class41_Sub7(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	Class41_Sub7(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	public static void method419(int i) {
		aClass113_3761 = null;
		aShortArray3762 = null;
		if (i != 9591)
			aShortArray3762 = null;
	}

	final int method380(int i, byte i_1_) {
		if (i_1_ != 41)
			method383((byte) 48);
		return 1;
	}

	final int method420(int i) {
		if (i <= 114)
			aClass113_3761 = null;
		return anInt389;
	}
}
