package net.zaros.client;
import jagdx.IDirect3DBaseTexture;
import jagdx.IDirect3DCubeTexture;
import jagdx.PixelBuffer;
import jagdx.aj;

final class Class64_Sub2 extends Class64 implements Interface6_Impl3 {
	private IDirect3DCubeTexture anIDirect3DCubeTexture3894;
	private int anInt3895;

	public final void method28(int i) {
		aHa_Sub1_Sub2_731.method1249(this, (byte) -127);
		if (i != -9994)
			anInt3895 = -102;
	}

	Class64_Sub2(ha_Sub1_Sub2 var_ha_Sub1_Sub2, int i, boolean bool, int[][] is) {
		super(var_ha_Sub1_Sub2, za_Sub2.aClass202_6555, Class67.aClass67_745, bool && var_ha_Sub1_Sub2.aBoolean5880, i * 6 * i);
		anInt3895 = i;
		if (!aBoolean732)
			anIDirect3DCubeTexture3894 = aHa_Sub1_Sub2_731.anIDirect3DDevice5865.a(anInt3895, 1, 0, 21, 1);
		else
			anIDirect3DCubeTexture3894 = aHa_Sub1_Sub2_731.anIDirect3DDevice5865.a(anInt3895, 0, 1024, 21, 1);
		PixelBuffer pixelbuffer = aHa_Sub1_Sub2_731.aPixelBuffer5867;
		for (int i_0_ = 0; i_0_ < 6; i_0_++) {
			int i_1_ = anIDirect3DCubeTexture3894.LockRect(i_0_, 0, 0, 0, anInt3895, anInt3895, 0, pixelbuffer);
			if (aj.a(i_1_, (int) 105)) {
				int i_2_ = pixelbuffer.getRowPitch();
				if (i_2_ != anInt3895 * 4) {
					for (int i_3_ = 0; i_3_ < anInt3895; i_3_++)
						pixelbuffer.b(is[i_0_], anInt3895 * i_3_, i_2_ * i_3_, anInt3895);
				} else
					pixelbuffer.b(is[i_0_], 0, 0, anInt3895 * anInt3895);
				anIDirect3DCubeTexture3894.UnlockRect(i_0_, 0);
			}
		}
	}

	public final void method29(Class131 class131, int i) {
		super.method29(class131, i);
	}

	final IDirect3DBaseTexture method706(int i) {
		if (i != 12931)
			method706(88);
		return anIDirect3DCubeTexture3894;
	}
}
