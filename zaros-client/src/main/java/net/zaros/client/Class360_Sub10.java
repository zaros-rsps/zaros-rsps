package net.zaros.client;
import jaggl.OpenGL;

final class Class360_Sub10 extends Class360 {
	private Class369 aClass369_5348;
	static int anInt5349 = -2;
	private Class369 aClass369_5350;
	private Class369 aClass369_5351;
	private Class369 aClass369_5352;
	private boolean aBoolean5353;
	private float[] aFloatArray5354 = new float[4];
	private boolean aBoolean5355 = false;
	private Interface6_Impl1 anInterface6_Impl1_5356;
	private boolean aBoolean5357;
	private boolean aBoolean5358;

	Class360_Sub10(ha_Sub1_Sub1 var_ha_Sub1_Sub1, Js5 class138) {
		super(var_ha_Sub1_Sub1);
		if (class138 == null || !var_ha_Sub1_Sub1.aBoolean5844)
			aBoolean5357 = false;
		else {
			aClass369_5352 = (Class296_Sub48.method3045(0, class138.getFile_("uw_ground_unlit", "gl"), var_ha_Sub1_Sub1, 34336));
			aClass369_5351 = (Class296_Sub48.method3045(0, class138.getFile_("uw_ground_lit", "gl"), var_ha_Sub1_Sub1, 34336));
			aClass369_5350 = (Class296_Sub48.method3045(0, class138.getFile_("uw_model_unlit", "gl"), var_ha_Sub1_Sub1, 34336));
			aClass369_5348 = Class296_Sub48.method3045(0, class138.getFile_("uw_model_lit", "gl"), var_ha_Sub1_Sub1, 34336);
			if (!(aClass369_5352 != null & aClass369_5351 != null & aClass369_5350 != null & aClass369_5348 != null))
				aBoolean5357 = false;
			else {
				anInterface6_Impl1_5356 = aHa_Sub1_3093.method1179(1, 2, false, new int[]{0, -1}, -461);
				anInterface6_Impl1_5356.method64((byte) 49, false, false);
				aBoolean5357 = true;
			}
		}
	}

	final void method3732(int i, int i_0_, int i_1_) {
		if (i_0_ >= -6)
			method3723((byte) 88);
	}

	final void method3736(byte i, Interface6 interface6, int i_2_) {
		int i_3_ = -122 % ((72 - i) / 49);
		if (interface6 == null) {
			if (!aBoolean5355) {
				aHa_Sub1_3093.method1140(aHa_Sub1_3093.anInterface6_4037, false);
				aHa_Sub1_3093.method1197((byte) 22, 1);
				aHa_Sub1_3093.method1158((byte) -103, 0, Class153.aClass287_1578);
				aHa_Sub1_3093.method1219((byte) 54, Class153.aClass287_1578, 0);
				aBoolean5355 = true;
			}
		} else {
			if (aBoolean5355) {
				aHa_Sub1_3093.method1158((byte) -110, 0, Class199.aClass287_2007);
				aHa_Sub1_3093.method1219((byte) 54, Class199.aClass287_2007, 0);
				aBoolean5355 = false;
			}
			aHa_Sub1_3093.method1140(interface6, false);
			aHa_Sub1_3093.method1197((byte) 22, i_2_);
		}
	}

	static final Class262 method3752(int i, int i_4_, int i_5_, int i_6_, int i_7_) {
		if (i_5_ > -76)
			anInt5349 = 19;
		long l = (((long) i_6_ & 0xffffL) << 32 | 65535L << 48 & (long) i_7_ << 48 | 65535L << 16 & (long) i_4_ << 16 | (long) i & 0xffffL);
		Class262 class262 = (Class262) Class41_Sub7.aClass113_3761.get(l);
		if (class262 == null) {
			class262 = InvisiblePlayer.aClass279_1977.method2340(i_7_, true, Class49.aClass182_457, i_6_, i_4_, i);
			Class41_Sub7.aClass113_3761.put(class262, l);
		}
		return class262;
	}

	final boolean method3723(byte i) {
		int i_8_ = 7 / ((i + 49) / 36);
		return aBoolean5357;
	}

	static final float[] method3753(int i, int i_9_, float[] fs) {
		float[] fs_10_ = new float[i_9_];
		Class291.method2406(fs, 0, fs_10_, i, i_9_);
		return fs_10_;
	}

	final void method3725(int i) {
		int i_11_ = -3 % ((i - 58) / 56);
		aHa_Sub1_3093.method1151(1, 16760);
		aHa_Sub1_3093.method1140(null, false);
		aHa_Sub1_3093.method1177(Js5.aClass125_1411, 9815, Js5.aClass125_1411);
		aHa_Sub1_3093.method1158((byte) -105, 0, Class199.aClass287_2007);
		aHa_Sub1_3093.method1158((byte) -112, 2, Class151.aClass287_1553);
		aHa_Sub1_3093.method1219((byte) 54, Class199.aClass287_2007, 0);
		aHa_Sub1_3093.method1151(0, 16760);
		if (aBoolean5355) {
			aHa_Sub1_3093.method1158((byte) -128, 0, Class199.aClass287_2007);
			aHa_Sub1_3093.method1219((byte) 54, Class199.aClass287_2007, 0);
			aBoolean5355 = false;
		}
		if (aBoolean5358) {
			OpenGL.glBindProgramARB(34336, 0);
			OpenGL.glDisable(34820);
			OpenGL.glDisable(34336);
			aBoolean5358 = false;
		}
	}

	final void method3726(byte i) {
		int i_12_ = aHa_Sub1_3093.method1203((byte) -60);
		Class373_Sub2 class373_sub2 = aHa_Sub1_3093.method1174(65280);
		if (!aBoolean5353)
			OpenGL.glBindProgramARB(34336, (i_12_ != 2147483647 ? aClass369_5350.anInt3137 : aClass369_5352.anInt3137));
		else
			OpenGL.glBindProgramARB(34336, (i_12_ != 2147483647 ? aClass369_5348.anInt3137 : aClass369_5351.anInt3137));
		OpenGL.glEnable(34336);
		if (i != -109)
			aFloatArray5354 = null;
		aBoolean5358 = true;
		class373_sub2.method3934(-1.0F, (float) i_12_, aFloatArray5354, 0.0F, i + 118, 0.0F);
		OpenGL.glProgramLocalParameter4fARB(34336, 1, aFloatArray5354[0], aFloatArray5354[1], aFloatArray5354[2], aFloatArray5354[3]);
		method3719((byte) 102);
	}

	final void method3719(byte i) {
		if (aBoolean5358) {
			int i_13_ = aHa_Sub1_3093.XA();
			int i_14_ = aHa_Sub1_3093.i();
			float f = -((float) (i_13_ - i_14_) * 0.125F) + (float) i_13_;
			float f_15_ = (float) i_13_ - (float) (-i_14_ + i_13_) * 0.25F;
			OpenGL.glProgramLocalParameter4fARB(34336, 0, f_15_, f, 1.0F / (float) aHa_Sub1_3093.method1169((byte) 118), (float) aHa_Sub1_3093.method1123((byte) -68) / 255.0F);
			aHa_Sub1_3093.method1151(1, 16760);
			aHa_Sub1_3093.method1139(0, aHa_Sub1_3093.method1220(121));
			aHa_Sub1_3093.method1151(0, 16760);
		}
		int i_16_ = -87 / ((-37 - i) / 43);
	}

	final void method3731(boolean bool, byte i) {
		if (i != -71) {
			/* empty */
		}
	}

	final void method3733(byte i, boolean bool) {
		aBoolean5353 = bool;
		aHa_Sub1_3093.method1151(1, 16760);
		aHa_Sub1_3093.method1140(anInterface6_Impl1_5356, false);
		aHa_Sub1_3093.method1177(Class280.aClass125_2589, 9815, Class41_Sub4.aClass125_3745);
		aHa_Sub1_3093.method1158((byte) -117, 0, Class151.aClass287_1553);
		aHa_Sub1_3093.method1170(2, Class199.aClass287_2007, false, true, (byte) -64);
		aHa_Sub1_3093.method1219((byte) 54, Class153.aClass287_1578, 0);
		aHa_Sub1_3093.method1151(0, 16760);
		method3726((byte) -109);
		if (i > -125)
			aBoolean5355 = false;
	}
}
