package net.zaros.client;

/* Class321 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class321 {
	static OutgoingPacket aClass311_2823 = new OutgoingPacket(89, -1);
	static int anInt2824;
	static SeekableFile dataFile;

	public static void method3343(byte i) {
		aClass311_2823 = null;
		if (i != 123)
			dataFile = null;
		dataFile = null;
	}

	static final void method3344(int i, String string) {
		if (string != null) {
			if (Class285.anInt2625 >= 200 && !aa_Sub1.memb1 || Class285.anInt2625 >= 200)
				Class34.method351(4, true, TranslatableString.aClass120_1207.getTranslation(Class394.langID));
			else {
				String string_0_ = Class296_Sub49.parseName(string);
				if (string_0_ != null) {
					for (int i_1_ = 0; Class285.anInt2625 > i_1_; i_1_++) {
						String string_2_ = Class296_Sub49.parseName((Js5TextureLoader.aStringArray3443[i_1_]));
						if (string_2_ != null && string_2_.equals(string_0_)) {
							Class34.method351(4, true, string + (TranslatableString.aClass120_1234.getTranslation(Class394.langID)));
							return;
						}
						if (Class338_Sub9.aStringArray5268[i_1_] != null) {
							String string_3_ = Class296_Sub49.parseName((Class338_Sub9.aStringArray5268[i_1_]));
							if (string_3_ != null && string_3_.equals(string_0_)) {
								Class34.method351(4, true, (string + (TranslatableString.aClass120_1234.getTranslation(Class394.langID))));
								return;
							}
						}
					}
					for (int i_4_ = 0; i_4_ < Class317.ignoresSize; i_4_++) {
						String string_5_ = Class296_Sub49.parseName((Class362.ignoreNames1[i_4_]));
						if (string_5_ != null && string_5_.equals(string_0_)) {
							Class34.method351(4, true, (TranslatableString.aClass120_1239.getTranslation(Class394.langID) + string + (TranslatableString.aClass120_1240.getTranslation(Class394.langID))));
							return;
						}
						if (Class296_Sub39_Sub11.ignoreNames3[i_4_] != null) {
							String string_6_ = (Class296_Sub49.parseName((Class296_Sub39_Sub11.ignoreNames3[i_4_])));
							if (string_6_ != null && string_6_.equals(string_0_)) {
								Class34.method351(4, true, ((TranslatableString.aClass120_1239.getTranslation(Class394.langID)) + string + (TranslatableString.aClass120_1240.getTranslation(Class394.langID))));
								return;
							}
						}
					}
					if (Class296_Sub49.parseName((Class296_Sub51_Sub11.localPlayer.displayName)).equals(string_0_))
						Class34.method351(4, true, (TranslatableString.aClass120_1237.getTranslation(Class394.langID)));
					else {
						if (i > -60)
							aClass311_2823 = null;
						Connection class204 = Class296_Sub51_Sub13.method3111(true);
						Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 109, (Class107_Sub1.aClass311_5668));
						class296_sub1.out.p1(Class117.method1015((byte) -109, string));
						class296_sub1.out.writeString(string);
						class204.sendPacket(class296_sub1, (byte) 119);
					}
				}
			}
		}
	}

	abstract ReferenceWrapper wrapReference(ReferenceWrapper class296_sub39_sub2, int i);
}
