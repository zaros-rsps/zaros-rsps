package net.zaros.client;

/* Class343_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class343_Sub1 extends FileWorker {
	static Class324[] aClass324Array5273;
	private ReferenceTable aClass90_5274;
	private int anInt5275;
	private Js5DiskStore aClass168_5276;
	private int anInt5277;
	private int anInt5278;
	private HashTable aClass263_5279;
	private byte[] aByteArray5280;
	private Class315 aClass315_5281;
	static Class296_Sub50 aClass296_Sub50_5282;
	private Class285 aClass285_5283;
	private int anInt5284 = 0;
	private Js5DiskStore aClass168_5285;
	private Class296_Sub39_Sub20 aClass296_Sub39_Sub20_5286;
	private byte[] aByteArray5287;
	private int anInt5288;
	private NodeDeque aClass155_5289;
	private boolean aBoolean5290;
	private boolean aBoolean5291;
	private NodeDeque aClass155_5292;
	private long aLong5293;
	private boolean aBoolean5294;

	@Override
	final byte[] getFileBuffer(int i, int i_0_) {
		Class296_Sub39_Sub20 class296_sub39_sub20 = method3650(0, -13, i_0_);
		if (i != -19808) {
			aClass263_5279 = null;
		}
		if (class296_sub39_sub20 == null) {
			return null;
		}
		byte[] is = class296_sub39_sub20.method2904(4);
		class296_sub39_sub20.unlink();
		return is;
	}

	static final void method3642(boolean bool, int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_) {
		int i_7_ = 0;
		int i_8_ = i_1_;
		int i_9_ = 0;
		int i_10_ = i_2_ - i_5_;
		int i_11_ = i_1_ - i_5_;
		int i_12_ = i_2_ * i_2_;
		int i_13_ = i_1_ * i_1_;
		int i_14_ = i_10_ * i_10_;
		int i_15_ = i_11_ * i_11_;
		int i_16_ = i_13_ << 1;
		int i_17_ = i_12_ << 1;
		int i_18_ = i_15_ << 1;
		int i_19_ = i_14_ << 1;
		int i_20_ = i_1_ << 1;
		int i_21_ = i_11_ << 1;
		int i_22_ = (1 - i_20_) * i_12_ + i_16_;
		int i_23_ = i_13_ - (i_20_ - 1) * i_17_;
		int i_24_ = i_18_ + (-i_21_ + 1) * i_14_;
		int i_25_ = -(i_19_ * (i_21_ - 1)) + i_15_;
		int i_26_ = i_12_ << 2;
		int i_27_ = i_13_ << 2;
		int i_28_ = i_14_ << 2;
		int i_29_ = i_15_ << 2;
		int i_30_ = i_16_ * 3;
		if (bool == true) {
			int i_31_ = (i_20_ - 3) * i_17_;
			int i_32_ = i_18_ * 3;
			int i_33_ = (i_21_ - 3) * i_19_;
			int i_34_ = i_27_;
			int i_35_ = (i_1_ - 1) * i_26_;
			int i_36_ = i_29_;
			int i_37_ = i_28_ * (i_11_ - 1);
			int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_6_];
			Class296_Sub14.method2511(is, -i_10_ + i_3_, (byte) 113, i_4_, -i_2_ + i_3_);
			Class296_Sub14.method2511(is, i_10_ + i_3_, (byte) -83, i, i_3_ - i_10_);
			Class296_Sub14.method2511(is, i_3_ + i_2_, (byte) -120, i_4_, i_3_ + i_10_);
			while (i_8_ > 0) {
				boolean bool_38_ = i_8_ <= i_11_;
				if (i_22_ < 0) {
					while (i_22_ < 0) {
						i_22_ += i_30_;
						i_23_ += i_34_;
						i_34_ += i_27_;
						i_7_++;
						i_30_ += i_27_;
					}
				}
				if (bool_38_) {
					if (i_24_ < 0) {
						while (i_24_ < 0) {
							i_24_ += i_32_;
							i_25_ += i_36_;
							i_9_++;
							i_36_ += i_29_;
							i_32_ += i_29_;
						}
					}
					if (i_25_ < 0) {
						i_24_ += i_32_;
						i_25_ += i_36_;
						i_36_ += i_29_;
						i_9_++;
						i_32_ += i_29_;
					}
					i_24_ -= i_37_;
					i_25_ -= i_33_;
					i_37_ -= i_28_;
					i_33_ -= i_28_;
				}
				if (i_23_ < 0) {
					i_23_ += i_34_;
					i_22_ += i_30_;
					i_7_++;
					i_30_ += i_27_;
					i_34_ += i_27_;
				}
				i_23_ -= i_31_;
				i_22_ -= i_35_;
				i_31_ -= i_26_;
				i_35_ -= i_26_;
				int i_39_ = -(--i_8_) + i_6_;
				int i_40_ = i_8_ + i_6_;
				int i_41_ = i_7_ + i_3_;
				int i_42_ = i_3_ - i_7_;
				if (bool_38_) {
					int i_43_ = i_3_ + i_9_;
					int i_44_ = -i_9_ + i_3_;
					Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_39_], i_44_, (byte) 114, i_4_, i_42_);
					Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_39_], i_43_, (byte) -47, i, i_44_);
					Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_39_], i_41_, (byte) -86, i_4_, i_43_);
					Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_40_], i_44_, (byte) -3, i_4_, i_42_);
					Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_40_], i_43_, (byte) 8, i, i_44_);
					Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_40_], i_41_, (byte) -70, i_4_, i_43_);
				} else {
					Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_39_], i_41_, (byte) 115, i_4_, i_42_);
					Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_40_], i_41_, (byte) -25, i_4_, i_42_);
				}
			}
		}
	}

	final int method3643(byte i) {
		if (aClass90_5274 == null) {
			return 0;
		}
		if (!aBoolean5290) {
			return aClass90_5274.entryCount;
		}
		Node class296 = aClass155_5292.removeFirst((byte) 113);
		if (i != 27) {
			method3648((byte) 36);
		}
		if (class296 == null) {
			return 0;
		}
		return (int) class296.uid;
	}

	final void method3644(int i) {
		if (aClass155_5292 != null) {
			if (getReferenceTable((byte) 109) == null) {
				return;
			}
			if (!aBoolean5290) {
				if (aBoolean5291) {
					boolean bool = true;
					for (Node class296 = aClass155_5292.removeFirst((byte) 113); class296 != null; class296 = aClass155_5292.removeNext(1001)) {
						int i_45_ = (int) class296.uid;
						if (aByteArray5287[i_45_] != 1) {
							method3650(2, 125, i_45_);
						}
						if (aByteArray5287[i_45_] != 1) {
							bool = false;
						} else {
							class296.unlink();
						}
					}
					while (anInt5288 < aClass90_5274.entryChildCounts.length) {
						if (aClass90_5274.entryChildCounts[anInt5288] == 0) {
							anInt5288++;
						} else {
							if (aClass285_5283.method2380(-1)) {
								bool = false;
								break;
							}
							if (aByteArray5287[anInt5288] != 1) {
								method3650(2, i - 63, anInt5288);
							}
							if (aByteArray5287[anInt5288] != 1) {
								Node class296 = new Node();
								class296.uid = anInt5288;
								bool = false;
								aClass155_5292.addLast((byte) 123, class296);
							}
							anInt5288++;
						}
					}
					if (bool) {
						aBoolean5291 = false;
						anInt5288 = 0;
					}
				} else {
					aClass155_5292 = null;
				}
			} else {
				boolean bool = true;
				for (Node class296 = aClass155_5292.removeFirst((byte) 126); class296 != null; class296 = aClass155_5292.removeNext(1001)) {
					int i_46_ = (int) class296.uid;
					if (aByteArray5287[i_46_] == 0) {
						method3650(1, 127, i_46_);
					}
					if (aByteArray5287[i_46_] == 0) {
						bool = false;
					} else {
						class296.unlink();
					}
				}
				while (anInt5288 < aClass90_5274.entryChildCounts.length) {
					if (aClass90_5274.entryChildCounts[anInt5288] == 0) {
						anInt5288++;
					} else {
						if (aClass315_5281.anInt2790 >= 250) {
							bool = false;
							break;
						}
						if (aByteArray5287[anInt5288] == 0) {
							method3650(1, -23, anInt5288);
						}
						if (aByteArray5287[anInt5288] == 0) {
							Node class296 = new Node();
							class296.uid = anInt5288;
							aClass155_5292.addLast((byte) 104, class296);
							bool = false;
						}
						anInt5288++;
					}
				}
				if (bool) {
					anInt5288 = 0;
					aBoolean5290 = false;
				}
			}
		}
		if (i == -1) {
			if (aBoolean5294 && Class72.method771(i ^ 0x6f) >= aLong5293) {
				for (Class296_Sub39_Sub20 class296_sub39_sub20 = (Class296_Sub39_Sub20) aClass263_5279.getFirst(true); class296_sub39_sub20 != null; class296_sub39_sub20 = (Class296_Sub39_Sub20) aClass263_5279.getNext(0)) {
					if (!class296_sub39_sub20.aBoolean6256) {
						if (!class296_sub39_sub20.aBoolean6257) {
							class296_sub39_sub20.aBoolean6257 = true;
						} else {
							if (!class296_sub39_sub20.aBoolean6253) {
								throw new RuntimeException();
							}
							class296_sub39_sub20.unlink();
						}
					}
				}
				aLong5293 = Class72.method771(-122) + 1000L;
			}
		}
	}

	public static void method3645(byte i) {
		aClass296_Sub50_5282 = null;
		int i_47_ = -107 / ((i - 37) / 54);
		aClass324Array5273 = null;
	}

	final void method3646(int i) {
		if (aClass155_5292 != null && getReferenceTable((byte) 109) != null) {
			if (i != -1) {
				method3649(-70);
			}
			for (Node class296 = aClass155_5289.removeFirst((byte) 116); class296 != null; class296 = aClass155_5289.removeNext(1001)) {
				int i_48_ = (int) class296.uid;
				if (i_48_ < 0 || aClass90_5274.enryIndexCount <= i_48_ || aClass90_5274.entryChildCounts[i_48_] == 0) {
					class296.unlink();
				} else {
					if (aByteArray5287[i_48_] == 0) {
						method3650(1, 127, i_48_);
					}
					if (aByteArray5287[i_48_] == -1) {
						method3650(2, i - 54, i_48_);
					}
					if (aByteArray5287[i_48_] == 1) {
						class296.unlink();
					}
				}
			}
		}
	}

	@Override
	final ReferenceTable getReferenceTable(byte i) {
		if (i != 109) {
			method3648((byte) -84);
		}
		if (aClass90_5274 != null) {
			return aClass90_5274;
		}
		if (aClass296_Sub39_Sub20_5286 == null) {
			if (aClass285_5283.method2371(23192)) {
				return null;
			}
			aClass296_Sub39_Sub20_5286 = aClass285_5283.method2373(anInt5278, (byte) 0, 255, true, true);
		}
		if (aClass296_Sub39_Sub20_5286.aBoolean6256) {
			return null;
		}
		byte[] is = aClass296_Sub39_Sub20_5286.method2904(i ^ 0x69);
		if (!(aClass296_Sub39_Sub20_5286 instanceof Class296_Sub39_Sub20_Sub2)) {
			try {
				if (is == null) {
					throw new RuntimeException();
				}
				aClass90_5274 = new ReferenceTable(is, anInt5275, aByteArray5280);
			} catch (RuntimeException runtimeexception) {
				aClass285_5283.method2379(i - 108);
				aClass90_5274 = null;
				if (!aClass285_5283.method2371(i + 23083)) {
					aClass296_Sub39_Sub20_5286 = aClass285_5283.method2373(anInt5278, (byte) 0, 255, true, true);
				} else {
					aClass296_Sub39_Sub20_5286 = null;
				}
				return null;
			}
			if (aClass168_5276 != null) {
				aClass315_5281.method3324(aClass168_5276, anInt5278, is, 123);
			}
		} else {
			try {
				if (is == null) {
					throw new RuntimeException();
				}
				aClass90_5274 = new ReferenceTable(is, anInt5275, aByteArray5280);
				if (aClass90_5274.revision != anInt5277) {
					throw new RuntimeException();
				}
			} catch (RuntimeException runtimeexception) {
				aClass90_5274 = null;
				if (!aClass285_5283.method2371(i + 23083)) {
					aClass296_Sub39_Sub20_5286 = aClass285_5283.method2373(anInt5278, (byte) 0, 255, true, true);
				} else {
					aClass296_Sub39_Sub20_5286 = null;
				}
				return null;
			}
		}
		aClass296_Sub39_Sub20_5286 = null;
		if (aClass168_5285 != null) {
			aByteArray5287 = new byte[aClass90_5274.enryIndexCount];
			anInt5284 = 0;
		}
		return aClass90_5274;
	}

	final void method3647(byte i) {
		if (i > -64) {
			method3651(-104, -82, null);
		}
		if (aClass168_5285 != null) {
			aBoolean5291 = true;
			if (aClass155_5292 == null) {
				aClass155_5292 = new NodeDeque();
			}
		}
	}

	final int method3648(byte i) {
		if (aClass90_5274 == null) {
			return 0;
		}
		int i_49_ = 62 % ((i - 49) / 61);
		return aClass90_5274.entryCount;
	}

	final int method3649(int i) {
		if (i <= 25) {
			return 54;
		}
		return anInt5284;
	}

	private final Class296_Sub39_Sub20 method3650(int i, int i_50_, int i_51_) {
		Class296_Sub39_Sub20 class296_sub39_sub20 = (Class296_Sub39_Sub20) aClass263_5279.get(i_51_);
		if (class296_sub39_sub20 != null && i == 0 && !class296_sub39_sub20.aBoolean6253 && class296_sub39_sub20.aBoolean6256) {
			class296_sub39_sub20.unlink();
			class296_sub39_sub20 = null;
		}
		boolean debug = anInt5278 == 7;

		int i_52_ = -91 % ((68 - i_50_) / 56);
		if (class296_sub39_sub20 == null) {
			if (i == 0) {
				if (aClass168_5285 == null || aByteArray5287[i_51_] == -1) {
					if (aClass285_5283.method2371(23192)) {
						return null;
					}
					class296_sub39_sub20 = aClass285_5283.method2373(i_51_, (byte) 2, anInt5278, true, true);
				} else {
					class296_sub39_sub20 = aClass315_5281.method3326(aClass168_5285, -3, i_51_);
				}
			} else if (i == 1) {
				if (aClass168_5285 == null) {
					throw new RuntimeException();
				}
				class296_sub39_sub20 = aClass315_5281.method3327(aClass168_5285, i_51_, 3);
			} else if (i == 2) {
				if (aClass168_5285 == null) {
					throw new RuntimeException();
				}
				if (aByteArray5287[i_51_] != -1) {
					throw new RuntimeException();
				}
				if (aClass285_5283.method2380(-1)) {
					return null;
				}
				class296_sub39_sub20 = aClass285_5283.method2373(i_51_, (byte) 2, anInt5278, true, false);
			} else {
				throw new RuntimeException();
			}
			aClass263_5279.put(i_51_, class296_sub39_sub20);
		}
		if (debug) {
			// System.out.println(class296_sub39_sub20);
		}
		if (class296_sub39_sub20.aBoolean6256) {
			return null;
		}
		byte[] is = class296_sub39_sub20.method2904(4);
		if (!(class296_sub39_sub20 instanceof Class296_Sub39_Sub20_Sub2)) {
			try {
				if (is == null || is.length <= 2) {
					throw new RuntimeException();
				}
				Queue.aCRC32_1441.reset();
				Queue.aCRC32_1441.update(is, 0, is.length - 2);
				int i_53_ = (int) Queue.aCRC32_1441.getValue();
				if (i_53_ != aClass90_5274.entryCRCS[i_51_]) {
					throw new RuntimeException();
				}
				if (aClass90_5274.whirlpoolDigests != null && aClass90_5274.whirlpoolDigests[i_51_] != null) {
					byte[] is_54_ = aClass90_5274.whirlpoolDigests[i_51_];
					byte[] is_55_ = Class163.method1629((byte) -60, is, is.length - 2, 0);
					for (int i_56_ = 0; i_56_ < 64; i_56_++) {
						if (is_55_[i_56_] != is_54_[i_56_]) {
							throw new RuntimeException();
						}
					}
				}
				aClass285_5283.anInt2633 = 0;
				aClass285_5283.anInt2635 = 0;
			} catch (RuntimeException runtimeexception) {
				aClass285_5283.method2379(1);
				class296_sub39_sub20.unlink();
				if (class296_sub39_sub20.aBoolean6253 && !aClass285_5283.method2371(23192)) {
					Class296_Sub39_Sub20_Sub1 class296_sub39_sub20_sub1 = aClass285_5283.method2373(i_51_, (byte) 2, anInt5278, true, true);
					aClass263_5279.put(i_51_, class296_sub39_sub20_sub1);
				}
				return null;
			}
			is[is.length - 2] = (byte) (aClass90_5274.entryVersions[i_51_] >>> 8);
			is[is.length - 1] = (byte) aClass90_5274.entryVersions[i_51_];
			if (aClass168_5285 != null) {
				aClass315_5281.method3324(aClass168_5285, i_51_, is, 109);
				if (aByteArray5287[i_51_] != 1) {
					anInt5284++;
					aByteArray5287[i_51_] = (byte) 1;
				}
			}
			if (!class296_sub39_sub20.aBoolean6253) {
				class296_sub39_sub20.unlink();
			}
			return class296_sub39_sub20;
		}
		try {
			if (is == null || is.length <= 2) {
				throw new RuntimeException();
			}
			Queue.aCRC32_1441.reset();
			Queue.aCRC32_1441.update(is, 0, is.length - 2);
			int i_57_ = (int) Queue.aCRC32_1441.getValue();
			if (aClass90_5274.entryCRCS[i_51_] != i_57_) {
				throw new RuntimeException();
			}
			if (aClass90_5274.whirlpoolDigests != null && aClass90_5274.whirlpoolDigests[i_51_] != null) {
				byte[] is_58_ = aClass90_5274.whirlpoolDigests[i_51_];
				byte[] is_59_ = Class163.method1629((byte) -108, is, is.length - 2, 0);
				for (int i_60_ = 0; i_60_ < 64; i_60_++) {
					if (is_58_[i_60_] != is_59_[i_60_]) {
						throw new RuntimeException();
					}
				}
			}
			int i_61_ = ((is[is.length - 2] & 0xff) << 8) + (is[is.length - 1] & 0xff);
			if (i_61_ != (aClass90_5274.entryVersions[i_51_] & 0xffff)) {
				throw new RuntimeException();
			}
			if (aByteArray5287[i_51_] != 1) {
				anInt5284++;
				aByteArray5287[i_51_] = (byte) 1;
			}
			if (!class296_sub39_sub20.aBoolean6253) {
				class296_sub39_sub20.unlink();
			}
			return class296_sub39_sub20;
		} catch (Exception exception) {
			aByteArray5287[i_51_] = (byte) -1;
			class296_sub39_sub20.unlink();
			if (class296_sub39_sub20.aBoolean6253 && !aClass285_5283.method2371(23192)) {
				Class296_Sub39_Sub20_Sub1 class296_sub39_sub20_sub1 = aClass285_5283.method2373(i_51_, (byte) 2, anInt5278, true, true);
				aClass263_5279.put(i_51_, class296_sub39_sub20_sub1);
			}
			return null;
		}
	}

	@Override
	final int getFileCompletion(int i, boolean bool) {
		if (bool != true) {
			return -105;
		}
		Class296_Sub39_Sub20 class296_sub39_sub20 = (Class296_Sub39_Sub20) aClass263_5279.get(i);
		if (class296_sub39_sub20 != null) {
			return class296_sub39_sub20.method2902(100);
		}
		return 0;
	}

	@Override
	final void requestFile(int i, int i_62_) {
		if (aClass168_5285 != null) {
			if (i != 0) {
				aClass168_5276 = null;
			}
			for (Node class296 = aClass155_5289.removeFirst((byte) 126); class296 != null; class296 = aClass155_5289.removeNext(1001)) {
				if (i_62_ == class296.uid) {
					return;
				}
			}
			Node class296 = new Node();
			class296.uid = i_62_;
			aClass155_5289.addLast((byte) -89, class296);
		}
	}

	static final byte[] method3651(int i, int i_63_, byte[] is) {
		byte[] is_64_ = new byte[i_63_];
		ArrayTools.removeElement(is, 0, is_64_, 0, i_63_);
		if (i != -3) {
			aClass296_Sub50_5282 = null;
		}
		return is_64_;
	}

	final int method3652(boolean bool) {
		if (bool) {
			return -76;
		}
		if (getReferenceTable((byte) 109) == null) {
			if (aClass296_Sub39_Sub20_5286 == null) {
				return 0;
			}
			return aClass296_Sub39_Sub20_5286.method2902(100);
		}
		return 100;
	}

	Class343_Sub1(int i, Js5DiskStore class168, Js5DiskStore class168_65_, Class285 class285, Class315 class315, int i_66_, byte[] is, int i_67_, boolean bool) {
		aClass263_5279 = new HashTable(16);
		anInt5288 = 0;
		aClass155_5289 = new NodeDeque();
		aLong5293 = 0L;
		aClass168_5285 = class168;
		anInt5278 = i;
		if (aClass168_5285 != null) {
			aBoolean5290 = true;
			aClass155_5292 = new NodeDeque();
		} else {
			aBoolean5290 = false;
		}
		aBoolean5294 = bool;
		anInt5277 = i_67_;
		aClass315_5281 = class315;
		anInt5275 = i_66_;
		aByteArray5280 = is;
		aClass285_5283 = class285;
		aClass168_5276 = class168_65_;
		if (aClass168_5276 != null) {
			aClass296_Sub39_Sub20_5286 = aClass315_5281.method3326(aClass168_5276, -3, anInt5278);
		}
	}
}
