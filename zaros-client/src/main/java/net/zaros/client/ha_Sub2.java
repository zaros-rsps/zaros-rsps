package net.zaros.client;

/* ha_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

public class ha_Sub2 extends ha {
	private int anInt4063;
	private int anInt4064;
	private Canvas aCanvas4065;
	private boolean aBoolean4066 = false;
	private boolean aBoolean4067 = false;
	private HashTable aClass263_4068 = new HashTable(4);
	Class296_Sub29 aClass296_Sub29_4069;
	private int anInt4070;
	private int anInt4071;
	int anInt4072;
	private AdvancedMemoryCache aClass113_4073;
	float[] aFloatArray4074;
	int anInt4075 = 75518;
	int anInt4076;
	int anInt4077;
	int anInt4078;
	int anInt4079 = 0;
	int anInt4080;
	int anInt4081;
	int anInt4082;
	private boolean aBoolean4083;
	int anInt4084;
	int anInt4085;
	int anInt4086;
	int anInt4087;
	private Class319 aClass319_4088;
	private int anInt4089;
	Class373_Sub3 aClass373_Sub3_4090;
	int anInt4091;
	private AdvancedMemoryCache aClass113_4092;
	int anInt4093;
	int anInt4094;
	private int anInt4095;
	private int anInt4096;
	private int anInt4097;
	private int anInt4098;
	float[] aFloatArray4099;
	int anInt4100;
	int anInt4101;
	private int anInt4102;
	private Class240[] aClass240Array4103;
	int anInt4104;
	int anInt4105;
	int anInt4106;
	int anInt4107;
	int[] anIntArray4108;
	int anInt4109;
	private Sprite aClass397_4110;
	private int anInt4111;

	public Class55 a(Class92 class92, Class186[] class186s, boolean bool) {
		int[] is = new int[class186s.length];
		int[] is_0_ = new int[class186s.length];
		boolean bool_1_ = false;
		for (int i = 0; i < class186s.length; i++) {
			is[i] = class186s[i].anInt1899;
			is_0_[i] = class186s[i].anInt1904;
			if (class186s[i].aByteArray1905 != null)
				bool_1_ = true;
		}
		if (bool) {
			if (bool_1_)
				return new Class55_Sub3(this, class92, class186s, is, is_0_);
			return new Class55_Sub4(this, class92, class186s, is, is_0_);
		}
		if (bool_1_)
			throw new IllegalArgumentException("");
		return new Class55_Sub1(this, class92, class186s, is, is_0_);
	}

	public void c() {
		/* empty */
	}

	public void X(int i) {
		/* empty */
	}

	public void HA(int i, int i_2_, int i_3_, int i_4_, int[] is) {
		float f = (aClass373_Sub3_4090.aFloat5618 + (aClass373_Sub3_4090.aFloat5614 * (float) i
				+ aClass373_Sub3_4090.aFloat5617 * (float) i_2_ + aClass373_Sub3_4090.aFloat5604 * (float) i_3_));
		if (f < (float) anInt4107 || f > (float) anInt4087)
			is[0] = is[1] = is[2] = -1;
		else {
			int i_5_ = (int) ((float) anInt4086 * (aClass373_Sub3_4090.aFloat5611
					+ (aClass373_Sub3_4090.aFloat5606 * (float) i + aClass373_Sub3_4090.aFloat5613 * (float) i_2_
							+ (aClass373_Sub3_4090.aFloat5610 * (float) i_3_)))
					/ (float) i_4_);
			int i_6_ = (int) ((float) anInt4072 * (aClass373_Sub3_4090.aFloat5619
					+ (aClass373_Sub3_4090.aFloat5607 * (float) i + aClass373_Sub3_4090.aFloat5615 * (float) i_2_
							+ (aClass373_Sub3_4090.aFloat5616 * (float) i_3_)))
					/ (float) i_4_);
			if (i_5_ >= anInt4082 && i_5_ <= anInt4106 && i_6_ >= anInt4094 && i_6_ <= anInt4077) {
				is[0] = i_5_ - anInt4082;
				is[1] = i_6_ - anInt4094;
				is[2] = (int) f;
			} else
				is[0] = is[1] = is[2] = -1;
		}
	}

	public void a(float f, float f_7_, float f_8_) {
		/* empty */
	}

	public void method1254(int i, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_,
			int i_17_) {
		if (i_11_ != 0 && i_12_ != 0) {
			if (i_14_ != 65535 && !aD1299.method14(i_14_, -9412).aBoolean1792) {
				if (anInt4111 != i_14_) {
					Sprite class397 = ((Sprite) aClass113_4092.get((long) i_14_));
					if (class397 == null) {
						int[] is = method1260(i_14_);
						if (is == null)
							return;
						int i_18_ = method1262(i_14_) ? 64 : anInt4081;
						class397 = this.method1096(102, 0, i_18_, i_18_, i_18_, is);
						aClass113_4092.put(class397, (long) i_14_);
					}
					anInt4111 = i_14_;
					aClass397_4110 = class397;
				}
				((Class397_Sub1) aClass397_4110).method4104(i - i_11_, i_9_ - i_12_, i_10_, i_11_ << 1, i_12_ << 1,
						i_16_, i_15_, i_17_, 1);
			} else
				method1258(i, i_9_, i_10_, i_11_, i_15_, i_17_);
		}
	}

	public int r(int i, int i_19_, int i_20_, int i_21_, int i_22_, int i_23_, int i_24_) {
		float f = (aClass373_Sub3_4090.aFloat5614 * (float) i + aClass373_Sub3_4090.aFloat5617 * (float) i_19_
				+ aClass373_Sub3_4090.aFloat5604 * (float) i_20_ + aClass373_Sub3_4090.aFloat5618);
		float f_25_ = (aClass373_Sub3_4090.aFloat5614 * (float) i_21_ + aClass373_Sub3_4090.aFloat5617 * (float) i_22_
				+ aClass373_Sub3_4090.aFloat5604 * (float) i_23_ + aClass373_Sub3_4090.aFloat5618);
		int i_26_ = 0;
		if (f < (float) anInt4107 && f_25_ < (float) anInt4107)
			i_26_ |= 0x10;
		else if (f > (float) anInt4087 && f_25_ > (float) anInt4087)
			i_26_ |= 0x20;
		int i_27_ = (int) ((float) anInt4086
				* (aClass373_Sub3_4090.aFloat5606 * (float) i + aClass373_Sub3_4090.aFloat5613 * (float) i_19_
						+ aClass373_Sub3_4090.aFloat5610 * (float) i_20_ + aClass373_Sub3_4090.aFloat5611)
				/ (float) i_24_);
		int i_28_ = (int) ((float) anInt4086
				* (aClass373_Sub3_4090.aFloat5606 * (float) i_21_ + aClass373_Sub3_4090.aFloat5613 * (float) i_22_
						+ aClass373_Sub3_4090.aFloat5610 * (float) i_23_ + aClass373_Sub3_4090.aFloat5611)
				/ (float) i_24_);
		if (i_27_ < anInt4082 && i_28_ < anInt4082)
			i_26_ |= 0x1;
		else if (i_27_ > anInt4106 && i_28_ > anInt4106)
			i_26_ |= 0x2;
		int i_29_ = (int) ((float) anInt4072
				* (aClass373_Sub3_4090.aFloat5607 * (float) i + aClass373_Sub3_4090.aFloat5615 * (float) i_19_
						+ aClass373_Sub3_4090.aFloat5616 * (float) i_20_ + aClass373_Sub3_4090.aFloat5619)
				/ (float) i_24_);
		int i_30_ = (int) ((float) anInt4072
				* (aClass373_Sub3_4090.aFloat5607 * (float) i_21_ + aClass373_Sub3_4090.aFloat5615 * (float) i_22_
						+ aClass373_Sub3_4090.aFloat5616 * (float) i_23_ + aClass373_Sub3_4090.aFloat5619)
				/ (float) i_24_);
		if (i_29_ < anInt4094 && i_30_ < anInt4094)
			i_26_ |= 0x4;
		else if (i_29_ > anInt4077 && i_30_ > anInt4077)
			i_26_ |= 0x8;
		return i_26_;
	}

	public void c(int i, int i_31_) throws Exception_Sub1 {
		if (aCanvas4065 == null || aClass296_Sub29_4069 == null)
			throw new IllegalStateException("off");
		try {
			Graphics graphics = aCanvas4065.getGraphics();
			aClass296_Sub29_4069.method2692(0, anInt4063, i, graphics, (byte) -124, 0, i_31_, anInt4070);
		} catch (Exception exception) {
			aCanvas4065.repaint();
		}
	}

	public void a(int i, Class296_Sub35[] class296_sub35s) {
		/* empty */
	}

	public void y() {
		if (aCanvas4065 != null) {
			anIntArray4108 = aClass296_Sub29_4069.anIntArray4810;
			anInt4084 = aClass296_Sub29_4069.anInt4815;
			anInt4096 = aClass296_Sub29_4069.anInt4813;
			aFloatArray4074 = aFloatArray4099;
			anInt4095 = anInt4089;
			anInt4102 = anInt4098;
		} else {
			anInt4084 = 1;
			anInt4096 = 1;
			anIntArray4108 = null;
			anInt4095 = 1;
			anInt4102 = 1;
			aFloatArray4074 = null;
		}
		aClass319_4088 = null;
		method1265();
	}

	public boolean j() {
		return true;
	}

	public void a(int[] is) {
		is[0] = anInt4084;
		is[1] = anInt4096;
	}

	public void method1255(int i, int i_32_, int i_33_, int i_34_, int i_35_, int i_36_, int i_37_, int i_38_,
			int i_39_, int i_40_) {
		if (i_34_ != 0 && i_35_ != 0) {
			if (i_37_ != 65535 && !aD1299.method14(i_37_, -9412).aBoolean1792) {
				if (anInt4111 != i_37_) {
					Sprite class397 = ((Sprite) aClass113_4092.get((long) i_37_));
					if (class397 == null) {
						int[] is = method1260(i_37_);
						if (is == null)
							return;
						int i_41_ = method1262(i_37_) ? 64 : anInt4081;
						class397 = this.method1096(119, 0, i_41_, i_41_, i_41_, is);
						aClass113_4092.put(class397, (long) i_37_);
					}
					anInt4111 = i_37_;
					aClass397_4110 = class397;
				}
				((Class397_Sub1) aClass397_4110).method4103(i - i_34_, i_32_ - i_35_, i_33_, i_34_ << 1, i_35_ << 1,
						i_39_, i_38_, i_40_, 1);
			} else
				method1258(i, i_32_, i_33_, i_34_, i_38_, i_40_);
		}
	}

	public void ra(int i, int i_42_, int i_43_, int i_44_) {
		for (int i_45_ = 0; i_45_ < aClass240Array4103.length; i_45_++) {
			aClass240Array4103[i_45_].anInt2268 = aClass240Array4103[i_45_].anInt2265;
			aClass240Array4103[i_45_].anInt2266 = i;
			aClass240Array4103[i_45_].anInt2265 = i_42_;
			aClass240Array4103[i_45_].anInt2260 = i_43_;
			aClass240Array4103[i_45_].aBoolean2269 = true;
		}
	}

	public void e() {
		aClass113_4073.clear();
		aClass113_4092.clear();
	}

	public int d(int i, int i_46_) {
		return i | i_46_;
	}

	public void b(boolean bool) {
		aBoolean4083 = bool;
		aClass113_4073.clear();
	}

	public void aa(int i, int i_47_, int i_48_, int i_49_, int i_50_, int i_51_) {
		if (i < anInt4079) {
			i_48_ -= anInt4079 - i;
			i = anInt4079;
		}
		if (i_47_ < anInt4085) {
			i_49_ -= anInt4085 - i_47_;
			i_47_ = anInt4085;
		}
		if (i + i_48_ > anInt4104)
			i_48_ = anInt4104 - i;
		if (i_47_ + i_49_ > anInt4078)
			i_49_ = anInt4078 - i_47_;
		if (i_48_ > 0 && i_49_ > 0 && i <= anInt4104 && i_47_ <= anInt4078) {
			int i_52_ = anInt4084 - i_48_;
			int i_53_ = i + i_47_ * anInt4084;
			int i_54_ = i_50_ >>> 24;
			if (i_51_ == 0 || i_51_ == 1 && i_54_ == 255) {
				int i_55_ = i_48_ >> 3;
				int i_56_ = i_48_ & 0x7;
				i_48_ = i_53_ - 1;
				for (int i_57_ = -i_49_; i_57_ < 0; i_57_++) {
					if (i_55_ > 0) {
						i = i_55_;
						do {
							anIntArray4108[++i_48_] = i_50_;
							anIntArray4108[++i_48_] = i_50_;
							anIntArray4108[++i_48_] = i_50_;
							anIntArray4108[++i_48_] = i_50_;
							anIntArray4108[++i_48_] = i_50_;
							anIntArray4108[++i_48_] = i_50_;
							anIntArray4108[++i_48_] = i_50_;
							anIntArray4108[++i_48_] = i_50_;
						} while (--i > 0);
					}
					if (i_56_ > 0) {
						i = i_56_;
						do
							anIntArray4108[++i_48_] = i_50_;
						while (--i > 0);
					}
					i_48_ += i_52_;
				}
			} else if (i_51_ == 1) {
				i_50_ = (((i_50_ & 0xff00ff) * i_54_ >> 8 & 0xff00ff)
						+ (((i_50_ & ~0xff00ff) >>> 8) * i_54_ & ~0xff00ff));
				int i_58_ = 256 - i_54_;
				for (int i_59_ = 0; i_59_ < i_49_; i_59_++) {
					for (int i_60_ = -i_48_; i_60_ < 0; i_60_++) {
						int i_61_ = anIntArray4108[i_53_];
						i_61_ = (((i_61_ & 0xff00ff) * i_58_ >> 8 & 0xff00ff)
								+ (((i_61_ & ~0xff00ff) >>> 8) * i_58_ & ~0xff00ff));
						anIntArray4108[i_53_++] = i_50_ + i_61_;
					}
					i_53_ += i_52_;
				}
			} else if (i_51_ == 2) {
				for (int i_62_ = 0; i_62_ < i_49_; i_62_++) {
					for (int i_63_ = -i_48_; i_63_ < 0; i_63_++) {
						int i_64_ = anIntArray4108[i_53_];
						int i_65_ = i_50_ + i_64_;
						int i_66_ = (i_50_ & 0xff00ff) + (i_64_ & 0xff00ff);
						i_64_ = (i_66_ & 0x1000100) + (i_65_ - i_66_ & 0x10000);
						anIntArray4108[i_53_++] = i_65_ - i_64_ | i_64_ - (i_64_ >>> 8);
					}
					i_53_ += i_52_;
				}
			} else
				throw new IllegalArgumentException();
		}
	}

	public void a(Class373 class373) {
		aClass373_Sub3_4090 = (Class373_Sub3) class373;
	}

	public int q() {
		return 0;
	}

	public void xa(float f) {
		anInt4075 = (int) (f * 65535.0F);
	}

	public void f(int i, int i_67_) {
		Class240 class240 = method1270(Thread.currentThread());
		anInt4107 = i;
		anInt4087 = i_67_;
		class240.anInt2267 = anInt4087 - 255;
	}

	public void A(int i, aa var_aa, int i_68_, int i_69_) {
		aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
		int[] is = var_aa_Sub2.anIntArray3726;
		int[] is_70_ = var_aa_Sub2.anIntArray3729;
		int i_71_;
		if (anInt4078 < i_69_ + is.length)
			i_71_ = anInt4078 - i_69_;
		else
			i_71_ = is.length;
		int i_72_;
		if (anInt4085 > i_69_) {
			i_72_ = anInt4085 - i_69_;
			i_69_ = anInt4085;
		} else
			i_72_ = 0;
		if (i_71_ - i_72_ > 0) {
			int i_73_ = i_69_ * anInt4084;
			for (int i_74_ = i_72_; i_74_ < i_71_; i_74_++) {
				int i_75_ = i_68_ + is[i_74_];
				int i_76_ = is_70_[i_74_];
				if (anInt4079 > i_75_) {
					i_76_ -= anInt4079 - i_75_;
					i_75_ = anInt4079;
				}
				if (anInt4104 < i_75_ + i_76_)
					i_76_ = anInt4104 - i_75_;
				i_75_ += i_73_;
				for (int i_77_ = -i_76_; i_77_ < 0; i_77_++)
					anIntArray4108[i_75_++] = i;
				i_73_ += anInt4084;
			}
		}
	}

	public void c(int i) {
		aClass240Array4103[i].method2145((byte) 41, null);
	}

	public void a(int i) {
		anInt4081 = i;
		aClass113_4073.clear();
	}

	public Sprite a(int i, int i_78_, int i_79_, int i_80_, boolean bool) {
		int[] is = new int[i_79_ * i_80_];
		int i_81_ = i_78_ * anInt4084 + i;
		int i_82_ = anInt4084 - i_79_;
		for (int i_83_ = 0; i_83_ < i_80_; i_83_++) {
			int i_84_ = i_83_ * i_79_;
			for (int i_85_ = 0; i_85_ < i_79_; i_85_++)
				is[i_84_ + i_85_] = anIntArray4108[i_81_++];
			i_81_ += i_82_;
		}
		if (bool)
			return new Class397_Sub1_Sub1(this, is, i_79_, i_80_);
		return new Class397_Sub1_Sub3(this, is, i_79_, i_80_);
	}

	public boolean k() {
		return false;
	}

	public boolean method1256() {
		return aBoolean4066;
	}

	public void f(int i) {
		anInt4105 = i;
		aClass240Array4103 = new Class240[anInt4105];
		for (int i_86_ = 0; i_86_ < anInt4105; i_86_++)
			aClass240Array4103[i_86_] = new Class240(this);
	}

	public void e(int i, int i_87_, int i_88_, int i_89_, int i_90_, int i_91_) {
		i_88_ -= i;
		i_89_ -= i_87_;
		if (i_89_ == 0) {
			if (i_88_ >= 0)
				U(i, i_87_, i_88_ + 1, i_90_, i_91_);
			else
				U(i + i_88_, i_87_, -i_88_ + 1, i_90_, i_91_);
		} else if (i_88_ == 0) {
			if (i_89_ >= 0)
				P(i, i_87_, i_89_ + 1, i_90_, i_91_);
			else
				P(i, i_87_ + i_89_, -i_89_ + 1, i_90_, i_91_);
		} else {
			if (i_88_ + i_89_ < 0) {
				i += i_88_;
				i_88_ = -i_88_;
				i_87_ += i_89_;
				i_89_ = -i_89_;
			}
			if (i_88_ > i_89_) {
				i_87_ <<= 16;
				i_87_ += 32768;
				i_89_ <<= 16;
				int i_92_ = (int) Math.floor((double) i_89_ / (double) i_88_ + 0.5);
				i_88_ += i;
				if (i < anInt4079) {
					i_87_ += i_92_ * (anInt4079 - i);
					i = anInt4079;
				}
				if (i_88_ >= anInt4104)
					i_88_ = anInt4104 - 1;
				int i_93_ = i_90_ >>> 24;
				if (i_91_ == 0 || i_91_ == 1 && i_93_ == 255) {
					for (/**/; i <= i_88_; i++) {
						int i_94_ = i_87_ >> 16;
						if (i_94_ >= anInt4085 && i_94_ < anInt4078)
							anIntArray4108[i + i_94_ * anInt4084] = i_90_;
						i_87_ += i_92_;
					}
					return;
				}
				if (i_91_ == 1) {
					i_90_ = (((i_90_ & 0xff00ff) * i_93_ >> 8 & 0xff00ff) + ((i_90_ & 0xff00) * i_93_ >> 8 & 0xff00)
							+ (i_93_ << 24));
					int i_95_ = 256 - i_93_;
					for (/**/; i <= i_88_; i++) {
						int i_96_ = i_87_ >> 16;
						if (i_96_ >= anInt4085 && i_96_ < anInt4078) {
							int i_97_ = i + i_96_ * anInt4084;
							int i_98_ = anIntArray4108[i_97_];
							i_98_ = (((i_98_ & 0xff00ff) * i_95_ >> 8 & 0xff00ff)
									+ ((i_98_ & 0xff00) * i_95_ >> 8 & 0xff00));
							anIntArray4108[i_97_] = i_90_ + i_98_;
						}
						i_87_ += i_92_;
					}
					return;
				}
				if (i_91_ == 2) {
					for (/**/; i <= i_88_; i++) {
						int i_99_ = i_87_ >> 16;
						if (i_99_ >= anInt4085 && i_99_ < anInt4078) {
							int i_100_ = i + i_99_ * anInt4084;
							int i_101_ = anIntArray4108[i_100_];
							int i_102_ = i_90_ + i_101_;
							int i_103_ = (i_90_ & 0xff00ff) + (i_101_ & 0xff00ff);
							i_101_ = (i_103_ & 0x1000100) + (i_102_ - i_103_ & 0x10000);
							anIntArray4108[i_100_] = i_102_ - i_101_ | i_101_ - (i_101_ >>> 8);
						}
						i_87_ += i_92_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			i <<= 16;
			i += 32768;
			i_88_ <<= 16;
			int i_104_ = (int) Math.floor((double) i_88_ / (double) i_89_ + 0.5);
			i_89_ += i_87_;
			if (i_87_ < anInt4085) {
				i += i_104_ * (anInt4085 - i_87_);
				i_87_ = anInt4085;
			}
			if (i_89_ >= anInt4078)
				i_89_ = anInt4078 - 1;
			int i_105_ = i_90_ >>> 24;
			if (i_91_ == 0 || i_91_ == 1 && i_105_ == 255) {
				for (/**/; i_87_ <= i_89_; i_87_++) {
					int i_106_ = i >> 16;
					if (i_106_ >= anInt4079 && i_106_ < anInt4104)
						anIntArray4108[i_106_ + i_87_ * anInt4084] = i_90_;
					i += i_104_;
				}
			} else if (i_91_ == 1) {
				i_90_ = (((i_90_ & 0xff00ff) * i_105_ >> 8 & 0xff00ff) + ((i_90_ & 0xff00) * i_105_ >> 8 & 0xff00)
						+ (i_105_ << 24));
				int i_107_ = 256 - i_105_;
				for (/**/; i_87_ <= i_89_; i_87_++) {
					int i_108_ = i >> 16;
					if (i_108_ >= anInt4079 && i_108_ < anInt4104) {
						int i_109_ = i_108_ + i_87_ * anInt4084;
						int i_110_ = anIntArray4108[i_109_];
						i_110_ = (((i_110_ & 0xff00ff) * i_107_ >> 8 & 0xff00ff)
								+ ((i_110_ & 0xff00) * i_107_ >> 8 & 0xff00));
						anIntArray4108[i_108_ + i_87_ * anInt4084] = i_90_ + i_110_;
					}
					i += i_104_;
				}
			} else if (i_91_ == 2) {
				for (/**/; i_87_ <= i_89_; i_87_++) {
					int i_111_ = i >> 16;
					if (i_111_ >= anInt4079 && i_111_ < anInt4104) {
						int i_112_ = i_111_ + i_87_ * anInt4084;
						int i_113_ = anIntArray4108[i_112_];
						int i_114_ = i_90_ + i_113_;
						int i_115_ = (i_90_ & 0xff00ff) + (i_113_ & 0xff00ff);
						i_113_ = (i_115_ & 0x1000100) + (i_114_ - i_115_ & 0x10000);
						anIntArray4108[i_112_] = i_114_ - i_113_ | i_113_ - (i_113_ >>> 8);
					}
					i += i_104_;
				}
			} else
				throw new IllegalArgumentException();
		}
	}

	public void L(int i, int i_116_, int i_117_) {
		for (int i_118_ = 0; i_118_ < aClass240Array4103.length; i_118_++) {
			Class240 class240 = aClass240Array4103[i_118_];
			class240.anInt2265 = i & 0xffffff;
			int i_119_ = class240.anInt2265 >>> 16 & 0xff;
			if (i_119_ < 2)
				i_119_ = 2;
			int i_120_ = class240.anInt2265 >> 8 & 0xff;
			if (i_120_ < 2)
				i_120_ = 2;
			int i_121_ = class240.anInt2265 & 0xff;
			if (i_121_ < 2)
				i_121_ = 2;
			class240.anInt2265 = i_119_ << 16 | i_120_ << 8 | i_121_;
			if (i_116_ < 0)
				class240.aBoolean2259 = false;
			else
				class240.aBoolean2259 = true;
		}
	}

	public Interface13 a(Interface19 interface19, Interface11 interface11) {
		return new Class319(this, (Sprite) interface19, (Class238) interface11);
	}

	public void a(boolean bool) {
		/* empty */
	}

	public void a(Interface13 interface13) {
		Class319 class319 = (Class319) interface13;
		anInt4084 = class319.anInt3647;
		anInt4096 = class319.anInt3643;
		anIntArray4108 = class319.anIntArray3644;
		aClass319_4088 = class319;
		anInt4095 = class319.anInt3647;
		anInt4102 = class319.anInt3643;
		aFloatArray4074 = class319.aFloatArray3645;
		method1265();
	}

	public int method1257(int i) {
		return aD1299.method14(i, -9412).anInt1788;
	}

	public boolean p() {
		return true;
	}

	public boolean a() {
		return false;
	}

	public boolean z() {
		return false;
	}

	private void method1258(int i, int i_122_, int i_123_, int i_124_, int i_125_, int i_126_) {
		if (i_124_ < 0)
			i_124_ = -i_124_;
		int i_127_ = i_122_ - i_124_;
		if (i_127_ < anInt4085)
			i_127_ = anInt4085;
		int i_128_ = i_122_ + i_124_ + 1;
		if (i_128_ > anInt4078)
			i_128_ = anInt4078;
		int i_129_ = i_127_;
		int i_130_ = i_124_ * i_124_;
		int i_131_ = 0;
		int i_132_ = i_122_ - i_129_;
		int i_133_ = i_132_ * i_132_;
		int i_134_ = i_133_ - i_132_;
		if (i_122_ > i_128_)
			i_122_ = i_128_;
		int i_135_ = i_125_ >>> 24;
		if (i_126_ == 0 || i_126_ == 1 && i_135_ == 255) {
			while (i_129_ < i_122_) {
				for (/**/; i_134_ <= i_130_ || i_133_ <= i_130_; i_134_ += i_131_++ + i_131_)
					i_133_ += i_131_ + i_131_;
				int i_136_ = i - i_131_ + 1;
				if (i_136_ < anInt4079)
					i_136_ = anInt4079;
				int i_137_ = i + i_131_;
				if (i_137_ > anInt4104)
					i_137_ = anInt4104;
				int i_138_ = i_136_ + i_129_ * anInt4084;
				for (int i_139_ = i_136_; i_139_ < i_137_; i_139_++) {
					if ((float) i_123_ < aFloatArray4074[i_138_])
						anIntArray4108[i_138_] = i_125_;
					i_138_++;
				}
				i_129_++;
				i_133_ -= i_132_-- + i_132_;
				i_134_ -= i_132_ + i_132_;
			}
			i_131_ = i_124_;
			i_132_ = i_129_ - i_122_;
			i_134_ = i_132_ * i_132_ + i_130_;
			i_133_ = i_134_ - i_131_;
			i_134_ -= i_132_;
			while (i_129_ < i_128_) {
				while (i_134_ > i_130_) {
					if (i_133_ <= i_130_)
						break;
					i_134_ -= i_131_-- + i_131_;
					i_133_ -= i_131_ + i_131_;
				}
				int i_140_ = i - i_131_;
				if (i_140_ < anInt4079)
					i_140_ = anInt4079;
				int i_141_ = i + i_131_;
				if (i_141_ > anInt4104 - 1)
					i_141_ = anInt4104 - 1;
				int i_142_ = i_140_ + i_129_ * anInt4084;
				for (int i_143_ = i_140_; i_143_ <= i_141_; i_143_++) {
					if ((float) i_123_ < aFloatArray4074[i_142_])
						anIntArray4108[i_142_] = i_125_;
					i_142_++;
				}
				i_129_++;
				i_134_ += i_132_ + i_132_;
				i_133_ += i_132_++ + i_132_;
			}
		} else if (i_126_ == 1) {
			i_125_ = (((i_125_ & 0xff00ff) * i_135_ >> 8 & 0xff00ff) + ((i_125_ & 0xff00) * i_135_ >> 8 & 0xff00)
					+ (i_135_ << 24));
			int i_144_ = 256 - i_135_;
			while (i_129_ < i_122_) {
				for (/**/; i_134_ <= i_130_ || i_133_ <= i_130_; i_134_ += i_131_++ + i_131_)
					i_133_ += i_131_ + i_131_;
				int i_145_ = i - i_131_ + 1;
				if (i_145_ < anInt4079)
					i_145_ = anInt4079;
				int i_146_ = i + i_131_;
				if (i_146_ > anInt4104)
					i_146_ = anInt4104;
				int i_147_ = i_145_ + i_129_ * anInt4084;
				for (int i_148_ = i_145_; i_148_ < i_146_; i_148_++) {
					if ((float) i_123_ < aFloatArray4074[i_147_]) {
						int i_149_ = anIntArray4108[i_147_];
						i_149_ = (((i_149_ & 0xff00ff) * i_144_ >> 8 & 0xff00ff)
								+ ((i_149_ & 0xff00) * i_144_ >> 8 & 0xff00));
						anIntArray4108[i_147_] = i_125_ + i_149_;
					}
					i_147_++;
				}
				i_129_++;
				i_133_ -= i_132_-- + i_132_;
				i_134_ -= i_132_ + i_132_;
			}
			i_131_ = i_124_;
			i_132_ = -i_132_;
			i_134_ = i_132_ * i_132_ + i_130_;
			i_133_ = i_134_ - i_131_;
			i_134_ -= i_132_;
			while (i_129_ < i_128_) {
				while (i_134_ > i_130_) {
					if (i_133_ <= i_130_)
						break;
					i_134_ -= i_131_-- + i_131_;
					i_133_ -= i_131_ + i_131_;
				}
				int i_150_ = i - i_131_;
				if (i_150_ < anInt4079)
					i_150_ = anInt4079;
				int i_151_ = i + i_131_;
				if (i_151_ > anInt4104 - 1)
					i_151_ = anInt4104 - 1;
				int i_152_ = i_150_ + i_129_ * anInt4084;
				for (int i_153_ = i_150_; i_153_ <= i_151_; i_153_++) {
					if ((float) i_123_ < aFloatArray4074[i_152_]) {
						int i_154_ = anIntArray4108[i_152_];
						i_154_ = (((i_154_ & 0xff00ff) * i_144_ >> 8 & 0xff00ff)
								+ ((i_154_ & 0xff00) * i_144_ >> 8 & 0xff00));
						anIntArray4108[i_152_] = i_125_ + i_154_;
					}
					i_152_++;
				}
				i_129_++;
				i_134_ += i_132_ + i_132_;
				i_133_ += i_132_++ + i_132_;
			}
		} else if (i_126_ == 2) {
			while (i_129_ < i_122_) {
				for (/**/; i_134_ <= i_130_ || i_133_ <= i_130_; i_134_ += i_131_++ + i_131_)
					i_133_ += i_131_ + i_131_;
				int i_155_ = i - i_131_ + 1;
				if (i_155_ < anInt4079)
					i_155_ = anInt4079;
				int i_156_ = i + i_131_;
				if (i_156_ > anInt4104)
					i_156_ = anInt4104;
				int i_157_ = i_155_ + i_129_ * anInt4084;
				for (int i_158_ = i_155_; i_158_ < i_156_; i_158_++) {
					if ((float) i_123_ < aFloatArray4074[i_157_]) {
						int i_159_ = anIntArray4108[i_157_];
						int i_160_ = i_125_ + i_159_;
						int i_161_ = (i_125_ & 0xff00ff) + (i_159_ & 0xff00ff);
						i_159_ = (i_161_ & 0x1000100) + (i_160_ - i_161_ & 0x10000);
						anIntArray4108[i_157_] = i_160_ - i_159_ | i_159_ - (i_159_ >>> 8);
					}
					i_157_++;
				}
				i_129_++;
				i_133_ -= i_132_-- + i_132_;
				i_134_ -= i_132_ + i_132_;
			}
			i_131_ = i_124_;
			i_132_ = -i_132_;
			i_134_ = i_132_ * i_132_ + i_130_;
			i_133_ = i_134_ - i_131_;
			i_134_ -= i_132_;
			while (i_129_ < i_128_) {
				while (i_134_ > i_130_) {
					if (i_133_ <= i_130_)
						break;
					i_134_ -= i_131_-- + i_131_;
					i_133_ -= i_131_ + i_131_;
				}
				int i_162_ = i - i_131_;
				if (i_162_ < anInt4079)
					i_162_ = anInt4079;
				int i_163_ = i + i_131_;
				if (i_163_ > anInt4104 - 1)
					i_163_ = anInt4104 - 1;
				int i_164_ = i_162_ + i_129_ * anInt4084;
				for (int i_165_ = i_162_; i_165_ <= i_163_; i_165_++) {
					if ((float) i_123_ < aFloatArray4074[i_164_]) {
						int i_166_ = anIntArray4108[i_164_];
						int i_167_ = i_125_ + i_166_;
						int i_168_ = (i_125_ & 0xff00ff) + (i_166_ & 0xff00ff);
						i_166_ = (i_168_ & 0x1000100) + (i_167_ - i_168_ & 0x10000);
						anIntArray4108[i_164_] = i_167_ - i_166_ | i_166_ - (i_166_ >>> 8);
					}
					i_164_++;
				}
				i_129_++;
				i_134_ += i_132_ + i_132_;
				i_133_ += i_132_++ + i_132_;
			}
		} else
			throw new IllegalArgumentException();
	}

	public void A() {
		/* empty */
	}

	public int[] method1259(int i) {
		Class296_Sub23 class296_sub23;
		synchronized (aClass113_4073) {
			class296_sub23 = (Class296_Sub23) aClass113_4073.get((long) i);
			if (class296_sub23 == null) {
				if (!aD1299.is_ready(i))
					return null;
				MaterialRaw class170 = aD1299.method14(i, -9412);
				int i_169_ = class170.small_sized || aBoolean4083 ? 64 : anInt4081;
				class296_sub23 = new Class296_Sub23(i, i_169_,
						aD1299.get_transparent_pixels(i_169_, i_169_, true, (byte) 127, 0.7F, i),
						class170.anInt1788 != 1);
				aClass113_4073.put(class296_sub23, (long) i);
			}
		}
		class296_sub23.aBoolean4744 = true;
		return class296_sub23.method2667();
	}

	public void w() {
		/* empty */
	}

	public Class241 a(Class241 class241, Class241 class241_170_, float f, Class241 class241_171_) {
		return null;
	}

	public void h() {
		/* empty */
	}

	public void ZA(int i, float f, float f_172_, float f_173_, float f_174_, float f_175_) {
		anInt4076 = (int) (f * 65535.0F);
		anInt4080 = (int) (f_172_ * 65535.0F);
		float f_176_ = (float) Math.sqrt((double) (f_173_ * f_173_ + f_174_ * f_174_ + f_175_ * f_175_));
		anInt4093 = (int) (f_173_ * 65535.0F / f_176_);
		anInt4109 = (int) (f_174_ * 65535.0F / f_176_);
		anInt4091 = (int) (f_175_ * 65535.0F / f_176_);
	}

	public void pa() {
		for (int i = 0; i < aClass240Array4103.length; i++) {
			aClass240Array4103[i].anInt2265 = aClass240Array4103[i].anInt2268;
			aClass240Array4103[i].aBoolean2269 = false;
		}
	}

	public boolean B() {
		return false;
	}

	public Class241 c(int i, int i_177_, int i_178_, int i_179_, int i_180_, int i_181_) {
		return null;
	}

	public za b(int i) {
		return null;
	}

	public void F(int i, int i_182_) {
		int i_183_ = i_182_ * anInt4084 + i;
		int i_184_ = i_182_ * anInt4095 + i;
		if (i_183_ != 0 || i_184_ != 0) {
			int[] is = anIntArray4108;
			float[] fs = aFloatArray4074;
			if (i_183_ < 0) {
				int i_185_ = is.length + i_183_;
				ArrayTools.removeElements(is, -i_183_, is, 0, i_185_);
			} else if (i_183_ > 0) {
				int i_186_ = is.length - i_183_;
				ArrayTools.removeElements(is, 0, is, i_183_, i_186_);
			}
			if (i_184_ < 0) {
				int i_187_ = fs.length + i_184_;
				Class291.method2406(fs, -i_184_, fs, 0, i_187_);
			} else if (i_184_ > 0) {
				int i_188_ = fs.length - i_184_;
				Class291.method2406(fs, 0, fs, i_184_, i_188_);
			}
		}
	}

	public int[] na(int i, int i_189_, int i_190_, int i_191_) {
		int[] is = new int[i_190_ * i_191_];
		int i_192_ = 0;
		for (int i_193_ = 0; i_193_ < i_191_; i_193_++) {
			int i_194_ = (i_189_ + i_193_) * anInt4084 + i;
			for (int i_195_ = 0; i_195_ < i_190_; i_195_++)
				is[i_192_++] = anIntArray4108[i_194_ + i_195_];
		}
		return is;
	}

	public void la() {
		anInt4079 = 0;
		anInt4085 = 0;
		anInt4104 = anInt4084;
		anInt4078 = anInt4096;
		method1261();
	}

	public void EA(int i, int i_196_, int i_197_, int i_198_) {
		Class240 class240 = method1270(Thread.currentThread());
		class240.anInt2266 = i;
		class240.anInt2265 = i_196_;
		class240.anInt2260 = i_197_;
	}

	public int I() {
		int i = anInt4071;
		anInt4071 = 0;
		return i;
	}

	public void GA(int i) {
		aa(0, 0, anInt4084, anInt4096, i, 0);
	}

	public void U(int i, int i_199_, int i_200_, int i_201_, int i_202_) {
		if (i_199_ >= anInt4085 && i_199_ < anInt4078) {
			if (i < anInt4079) {
				i_200_ -= anInt4079 - i;
				i = anInt4079;
			}
			if (i + i_200_ > anInt4104)
				i_200_ = anInt4104 - i;
			int i_203_ = i + i_199_ * anInt4084;
			int i_204_ = i_201_ >>> 24;
			if (i_202_ == 0 || i_202_ == 1 && i_204_ == 255) {
				for (int i_205_ = 0; i_205_ < i_200_; i_205_++)
					anIntArray4108[i_203_ + i_205_] = i_201_;
			} else if (i_202_ == 1) {
				i_201_ = (((i_201_ & 0xff00ff) * i_204_ >> 8 & 0xff00ff) + ((i_201_ & 0xff00) * i_204_ >> 8 & 0xff00)
						+ (i_204_ << 24));
				int i_206_ = 256 - i_204_;
				for (int i_207_ = 0; i_207_ < i_200_; i_207_++) {
					int i_208_ = anIntArray4108[i_203_ + i_207_];
					i_208_ = (((i_208_ & 0xff00ff) * i_206_ >> 8 & 0xff00ff)
							+ ((i_208_ & 0xff00) * i_206_ >> 8 & 0xff00));
					anIntArray4108[i_203_ + i_207_] = i_201_ + i_208_;
				}
			} else if (i_202_ == 2) {
				for (int i_209_ = 0; i_209_ < i_200_; i_209_++) {
					int i_210_ = anIntArray4108[i_203_ + i_209_];
					int i_211_ = i_201_ + i_210_;
					int i_212_ = (i_201_ & 0xff00ff) + (i_210_ & 0xff00ff);
					i_210_ = (i_212_ & 0x1000100) + (i_211_ - i_212_ & 0x10000);
					anIntArray4108[i_203_ + i_209_] = i_211_ - i_210_ | i_210_ - (i_210_ >>> 8);
				}
			} else
				throw new IllegalArgumentException();
		}
	}

	public void t() {
		if (aBoolean4067) {
			Class355_Sub1.method3702((byte) 78, false, true);
			aBoolean4067 = false;
		}
		aClass296_Sub29_4069 = null;
		aCanvas4065 = null;
		anInt4070 = 0;
		anInt4063 = 0;
		aClass263_4068 = null;
		aBoolean4066 = true;
	}

	public void b(int i, int i_213_, int i_214_, int i_215_, int i_216_, int i_217_) {
		U(i, i_213_, i_214_, i_216_, i_217_);
		U(i, i_213_ + i_215_ - 1, i_214_, i_216_, i_217_);
		P(i, i_213_ + 1, i_215_ - 2, i_216_, i_217_);
		P(i + i_214_ - 1, i_213_ + 1, i_215_ - 2, i_216_, i_217_);
	}

	public int JA(int i, int i_218_, int i_219_, int i_220_, int i_221_, int i_222_) {
		int i_223_ = 0;
		float f = (aClass373_Sub3_4090.aFloat5614 * (float) i + aClass373_Sub3_4090.aFloat5617 * (float) i_218_
				+ aClass373_Sub3_4090.aFloat5604 * (float) i_219_ + aClass373_Sub3_4090.aFloat5618);
		if (f < 1.0F)
			f = 1.0F;
		float f_224_ = (aClass373_Sub3_4090.aFloat5614 * (float) i_220_
				+ aClass373_Sub3_4090.aFloat5617 * (float) i_221_ + aClass373_Sub3_4090.aFloat5604 * (float) i_222_
				+ aClass373_Sub3_4090.aFloat5618);
		if (f_224_ < 1.0F)
			f_224_ = 1.0F;
		if (f < (float) anInt4107 && f_224_ < (float) anInt4107)
			i_223_ |= 0x10;
		else if (f > (float) anInt4087 && f_224_ > (float) anInt4087)
			i_223_ |= 0x20;
		int i_225_ = (int) ((float) anInt4086
				* (aClass373_Sub3_4090.aFloat5606 * (float) i + aClass373_Sub3_4090.aFloat5613 * (float) i_218_
						+ aClass373_Sub3_4090.aFloat5610 * (float) i_219_ + aClass373_Sub3_4090.aFloat5611)
				/ f);
		int i_226_ = (int) ((float) anInt4086
				* (aClass373_Sub3_4090.aFloat5606 * (float) i_220_ + aClass373_Sub3_4090.aFloat5613 * (float) i_221_
						+ aClass373_Sub3_4090.aFloat5610 * (float) i_222_ + aClass373_Sub3_4090.aFloat5611)
				/ f_224_);
		if (i_225_ < anInt4082 && i_226_ < anInt4082)
			i_223_ |= 0x1;
		else if (i_225_ > anInt4106 && i_226_ > anInt4106)
			i_223_ |= 0x2;
		int i_227_ = (int) ((float) anInt4072
				* (aClass373_Sub3_4090.aFloat5607 * (float) i + aClass373_Sub3_4090.aFloat5615 * (float) i_218_
						+ aClass373_Sub3_4090.aFloat5616 * (float) i_219_ + aClass373_Sub3_4090.aFloat5619)
				/ f);
		int i_228_ = (int) ((float) anInt4072
				* (aClass373_Sub3_4090.aFloat5607 * (float) i_220_ + aClass373_Sub3_4090.aFloat5615 * (float) i_221_
						+ aClass373_Sub3_4090.aFloat5616 * (float) i_222_ + aClass373_Sub3_4090.aFloat5619)
				/ f_224_);
		if (i_227_ < anInt4094 && i_228_ < anInt4094)
			i_223_ |= 0x4;
		else if (i_227_ > anInt4077 && i_228_ > anInt4077)
			i_223_ |= 0x8;
		return i_223_;
	}

	public Sprite a(int i, int i_229_, boolean bool) {
		if (bool)
			return new Class397_Sub1_Sub1(this, i, i_229_);
		return new Class397_Sub1_Sub3(this, i, i_229_);
	}

	public int[] method1260(int i) {
		Class296_Sub23 class296_sub23;
		synchronized (aClass113_4073) {
			class296_sub23 = ((Class296_Sub23) aClass113_4073.get((long) i | ~0x7fffffffffffffffL));
			if (class296_sub23 == null) {
				if (!aD1299.is_ready(i))
					return null;
				MaterialRaw class170 = aD1299.method14(i, -9412);
				int i_230_ = class170.small_sized || aBoolean4083 ? 64 : anInt4081;
				class296_sub23 = new Class296_Sub23(i, i_230_,
						aD1299.method17(0.7F, i_230_, true, (byte) -104, i_230_, i), class170.anInt1788 != 1);
				aClass113_4073.put(class296_sub23, (long) i | ~0x7fffffffffffffffL);
			}
		}
		class296_sub23.aBoolean4744 = true;
		return class296_sub23.method2667();
	}

	public Interface11 a(int i, int i_231_) {
		return new Class238(i, i_231_);
	}

	public void a(int i, int i_232_, int i_233_, int i_234_, int i_235_, int i_236_, aa var_aa, int i_237_,
			int i_238_) {
		aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
		int[] is = var_aa_Sub2.anIntArray3726;
		int[] is_239_ = var_aa_Sub2.anIntArray3729;
		int i_240_ = anInt4085 > i_238_ ? anInt4085 : i_238_;
		int i_241_ = anInt4078 < i_238_ + is.length ? anInt4078 : i_238_ + is.length;
		i_233_ -= i;
		i_234_ -= i_232_;
		if (i_233_ + i_234_ < 0) {
			i += i_233_;
			i_233_ = -i_233_;
			i_232_ += i_234_;
			i_234_ = -i_234_;
		}
		if (i_233_ > i_234_) {
			i_232_ <<= 16;
			i_232_ += 32768;
			i_234_ <<= 16;
			int i_242_ = (int) Math.floor((double) i_234_ / (double) i_233_ + 0.5);
			i_233_ += i;
			if (i < anInt4079) {
				i_232_ += i_242_ * (anInt4079 - i);
				i = anInt4079;
			}
			if (i_233_ >= anInt4104)
				i_233_ = anInt4104 - 1;
			int i_243_ = i_235_ >>> 24;
			if (i_236_ == 0 || i_236_ == 1 && i_243_ == 255) {
				for (/**/; i <= i_233_; i++) {
					int i_244_ = i_232_ >> 16;
					int i_245_ = i_244_ - i_238_;
					if (i_244_ >= i_240_ && i_244_ < i_241_) {
						int i_246_ = i_237_ + is[i_245_];
						if (i >= i_246_ && i < i_246_ + is_239_[i_245_])
							anIntArray4108[i + i_244_ * anInt4084] = i_235_;
					}
					i_232_ += i_242_;
				}
				return;
			}
			if (i_236_ == 1) {
				i_235_ = (((i_235_ & 0xff00ff) * i_243_ >> 8 & 0xff00ff) + ((i_235_ & 0xff00) * i_243_ >> 8 & 0xff00)
						+ (i_243_ << 24));
				int i_247_ = 256 - i_243_;
				for (/**/; i <= i_233_; i++) {
					int i_248_ = i_232_ >> 16;
					int i_249_ = i_248_ - i_238_;
					if (i_248_ >= i_240_ && i_248_ < i_241_) {
						int i_250_ = i_237_ + is[i_249_];
						if (i >= i_250_ && i < i_250_ + is_239_[i_249_]) {
							int i_251_ = i + i_248_ * anInt4084;
							int i_252_ = anIntArray4108[i_251_];
							i_252_ = (((i_252_ & 0xff00ff) * i_247_ >> 8 & 0xff00ff)
									+ ((i_252_ & 0xff00) * i_247_ >> 8 & 0xff00));
							anIntArray4108[i_251_] = i_235_ + i_252_;
						}
					}
					i_232_ += i_242_;
				}
				return;
			}
			if (i_236_ == 2) {
				for (/**/; i <= i_233_; i++) {
					int i_253_ = i_232_ >> 16;
					int i_254_ = i_253_ - i_238_;
					if (i_253_ >= i_240_ && i_253_ < i_241_) {
						int i_255_ = i_237_ + is[i_254_];
						if (i >= i_255_ && i < i_255_ + is_239_[i_254_]) {
							int i_256_ = i + i_253_ * anInt4084;
							int i_257_ = anIntArray4108[i_256_];
							int i_258_ = i_235_ + i_257_;
							int i_259_ = (i_235_ & 0xff00ff) + (i_257_ & 0xff00ff);
							i_257_ = (i_259_ & 0x1000100) + (i_258_ - i_259_ & 0x10000);
							anIntArray4108[i_256_] = i_258_ - i_257_ | i_257_ - (i_257_ >>> 8);
						}
					}
					i_232_ += i_242_;
				}
				return;
			}
			throw new IllegalArgumentException();
		}
		i <<= 16;
		i += 32768;
		i_233_ <<= 16;
		int i_260_ = (int) Math.floor((double) i_233_ / (double) i_234_ + 0.5);
		i_234_ += i_232_;
		if (i_232_ < i_240_) {
			i += i_260_ * (i_240_ - i_232_);
			i_232_ = i_240_;
		}
		if (i_234_ >= i_241_)
			i_234_ = i_241_ - 1;
		int i_261_ = i_235_ >>> 24;
		if (i_236_ == 0 || i_236_ == 1 && i_261_ == 255) {
			for (/**/; i_232_ <= i_234_; i_232_++) {
				int i_262_ = i >> 16;
				int i_263_ = i_232_ - i_238_;
				int i_264_ = i_237_ + is[i_263_];
				if (i_262_ >= anInt4079 && i_262_ < anInt4104 && i_262_ >= i_264_ && i_262_ < i_264_ + is_239_[i_263_])
					anIntArray4108[i_262_ + i_232_ * anInt4084] = i_235_;
				i += i_260_;
			}
		} else if (i_236_ == 1) {
			i_235_ = (((i_235_ & 0xff00ff) * i_261_ >> 8 & 0xff00ff) + ((i_235_ & 0xff00) * i_261_ >> 8 & 0xff00)
					+ (i_261_ << 24));
			int i_265_ = 256 - i_261_;
			for (/**/; i_232_ <= i_234_; i_232_++) {
				int i_266_ = i >> 16;
				int i_267_ = i_232_ - i_238_;
				int i_268_ = i_237_ + is[i_267_];
				if (i_266_ >= anInt4079 && i_266_ < anInt4104 && i_266_ >= i_268_
						&& i_266_ < i_268_ + is_239_[i_267_]) {
					int i_269_ = i_266_ + i_232_ * anInt4084;
					int i_270_ = anIntArray4108[i_269_];
					i_270_ = (((i_270_ & 0xff00ff) * i_265_ >> 8 & 0xff00ff)
							+ ((i_270_ & 0xff00) * i_265_ >> 8 & 0xff00));
					anIntArray4108[i_266_ + i_232_ * anInt4084] = i_235_ + i_270_;
				}
				i += i_260_;
			}
		} else if (i_236_ == 2) {
			for (/**/; i_232_ <= i_234_; i_232_++) {
				int i_271_ = i >> 16;
				int i_272_ = i_232_ - i_238_;
				int i_273_ = i_237_ + is[i_272_];
				if (i_271_ >= anInt4079 && i_271_ < anInt4104 && i_271_ >= i_273_
						&& i_271_ < i_273_ + is_239_[i_272_]) {
					int i_274_ = i_271_ + i_232_ * anInt4084;
					int i_275_ = anIntArray4108[i_274_];
					int i_276_ = i_235_ + i_275_;
					int i_277_ = (i_235_ & 0xff00ff) + (i_275_ & 0xff00ff);
					i_275_ = (i_277_ & 0x1000100) + (i_276_ - i_277_ & 0x10000);
					anIntArray4108[i_274_] = i_276_ - i_275_ | i_275_ - (i_275_ >>> 8);
				}
				i += i_260_;
			}
		} else
			throw new IllegalArgumentException();
	}

	public void a(za var_za) {
		/* empty */
	}

	public void a(int i, int i_278_, int i_279_, int i_280_, int i_281_, int i_282_, aa var_aa, int i_283_, int i_284_,
			int i_285_, int i_286_, int i_287_) {
		aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
		int[] is = var_aa_Sub2.anIntArray3726;
		int[] is_288_ = var_aa_Sub2.anIntArray3729;
		int i_289_ = anInt4085 > i_284_ ? anInt4085 : i_284_;
		int i_290_ = anInt4078 < i_284_ + is.length ? anInt4078 : i_284_ + is.length;
		i_287_ <<= 8;
		i_285_ <<= 8;
		i_286_ <<= 8;
		int i_291_ = i_285_ + i_286_;
		i_287_ %= i_291_;
		i_279_ -= i;
		i_280_ -= i_278_;
		if (i_279_ + i_280_ < 0) {
			int i_292_ = (int) (Math.sqrt((double) (i_279_ * i_279_ + i_280_ * i_280_)) * 256.0);
			int i_293_ = i_292_ % i_291_;
			i_287_ = i_291_ + i_285_ - i_287_ - i_293_;
			i_287_ %= i_291_;
			if (i_287_ < 0)
				i_287_ += i_291_;
			i += i_279_;
			i_279_ = -i_279_;
			i_278_ += i_280_;
			i_280_ = -i_280_;
		}
		if (i_279_ > i_280_) {
			i_278_ <<= 16;
			i_278_ += 32768;
			i_280_ <<= 16;
			int i_294_ = (int) Math.floor((double) i_280_ / (double) i_279_ + 0.5);
			i_279_ += i;
			int i_295_ = i_281_ >>> 24;
			int i_296_ = (int) Math.sqrt((double) ((i_294_ >> 8) * (i_294_ >> 8) + 65536));
			if (i_282_ == 0 || i_282_ == 1 && i_295_ == 255) {
				while (i <= i_279_) {
					int i_297_ = i_278_ >> 16;
					int i_298_ = i_297_ - i_284_;
					if (i >= anInt4079 && i < anInt4104 && i_297_ >= i_289_ && i_297_ < i_290_ && i_287_ < i_285_) {
						int i_299_ = i_283_ + is[i_298_];
						if (i >= i_299_ && i < i_299_ + is_288_[i_298_])
							anIntArray4108[i + i_297_ * anInt4084] = i_281_;
					}
					i_278_ += i_294_;
					i++;
					i_287_ += i_296_;
					i_287_ %= i_291_;
				}
				return;
			}
			if (i_282_ == 1) {
				i_281_ = (((i_281_ & 0xff00ff) * i_295_ >> 8 & 0xff00ff) + ((i_281_ & 0xff00) * i_295_ >> 8 & 0xff00)
						+ (i_295_ << 24));
				int i_300_ = 256 - i_295_;
				while (i <= i_279_) {
					int i_301_ = i_278_ >> 16;
					int i_302_ = i_301_ - i_284_;
					if (i >= anInt4079 && i < anInt4104 && i_301_ >= i_289_ && i_301_ < i_290_ && i_287_ < i_285_) {
						int i_303_ = i_283_ + is[i_302_];
						if (i >= i_303_ && i < i_303_ + is_288_[i_302_]) {
							int i_304_ = i + i_301_ * anInt4084;
							int i_305_ = anIntArray4108[i_304_];
							i_305_ = (((i_305_ & 0xff00ff) * i_300_ >> 8 & 0xff00ff)
									+ ((i_305_ & 0xff00) * i_300_ >> 8 & 0xff00));
							anIntArray4108[i_304_] = i_281_ + i_305_;
						}
					}
					i_278_ += i_294_;
					i++;
					i_287_ += i_296_;
					i_287_ %= i_291_;
				}
				return;
			}
			if (i_282_ == 2) {
				while (i <= i_279_) {
					int i_306_ = i_278_ >> 16;
					int i_307_ = i_306_ - i_284_;
					if (i >= anInt4079 && i < anInt4104 && i_306_ >= i_289_ && i_306_ < i_290_ && i_287_ < i_285_) {
						int i_308_ = i_283_ + is[i_307_];
						if (i >= i_308_ && i < i_308_ + is_288_[i_307_]) {
							int i_309_ = i + i_306_ * anInt4084;
							int i_310_ = anIntArray4108[i_309_];
							int i_311_ = i_281_ + i_310_;
							int i_312_ = (i_281_ & 0xff00ff) + (i_310_ & 0xff00ff);
							i_310_ = (i_312_ & 0x1000100) + (i_311_ - i_312_ & 0x10000);
							anIntArray4108[i_309_] = i_311_ - i_310_ | i_310_ - (i_310_ >>> 8);
						}
					}
					i_278_ += i_294_;
					i++;
					i_287_ += i_296_;
					i_287_ %= i_291_;
				}
				return;
			}
			throw new IllegalArgumentException();
		}
		i <<= 16;
		i += 32768;
		i_279_ <<= 16;
		int i_313_ = (int) Math.floor((double) i_279_ / (double) i_280_ + 0.5);
		int i_314_ = (int) Math.sqrt((double) ((i_313_ >> 8) * (i_313_ >> 8) + 65536));
		i_280_ += i_278_;
		int i_315_ = i_281_ >>> 24;
		if (i_282_ == 0 || i_282_ == 1 && i_315_ == 255) {
			while (i_278_ <= i_280_) {
				int i_316_ = i >> 16;
				int i_317_ = i_278_ - i_284_;
				if (i_278_ >= i_289_ && i_278_ < i_290_ && i_316_ >= anInt4079 && i_316_ < anInt4104 && i_287_ < i_285_
						&& i_316_ >= i_283_ + is[i_317_] && i_316_ < i_283_ + is[i_317_] + is_288_[i_317_])
					anIntArray4108[i_316_ + i_278_ * anInt4084] = i_281_;
				i += i_313_;
				i_278_++;
				i_287_ += i_314_;
				i_287_ %= i_291_;
			}
		} else if (i_282_ == 1) {
			i_281_ = (((i_281_ & 0xff00ff) * i_315_ >> 8 & 0xff00ff) + ((i_281_ & 0xff00) * i_315_ >> 8 & 0xff00)
					+ (i_315_ << 24));
			int i_318_ = 256 - i_315_;
			while (i_278_ <= i_280_) {
				int i_319_ = i >> 16;
				int i_320_ = i_278_ - i_284_;
				if (i_278_ >= i_289_ && i_278_ < i_290_ && i_319_ >= anInt4079 && i_319_ < anInt4104 && i_287_ < i_285_
						&& i_319_ >= i_283_ + is[i_320_] && i_319_ < i_283_ + is[i_320_] + is_288_[i_320_]) {
					int i_321_ = i_319_ + i_278_ * anInt4084;
					int i_322_ = anIntArray4108[i_321_];
					i_322_ = (((i_322_ & 0xff00ff) * i_318_ >> 8 & 0xff00ff)
							+ ((i_322_ & 0xff00) * i_318_ >> 8 & 0xff00));
					anIntArray4108[i_319_ + i_278_ * anInt4084] = i_281_ + i_322_;
				}
				i += i_313_;
				i_278_++;
				i_287_ += i_314_;
				i_287_ %= i_291_;
			}
		} else if (i_282_ == 2) {
			while (i_278_ <= i_280_) {
				int i_323_ = i >> 16;
				int i_324_ = i_278_ - i_284_;
				if (i_278_ >= i_289_ && i_278_ < i_290_ && i_323_ >= anInt4079 && i_323_ < anInt4104 && i_287_ < i_285_
						&& i_323_ >= i_283_ + is[i_324_] && i_323_ < i_283_ + is[i_324_] + is_288_[i_324_]) {
					int i_325_ = i_323_ + i_278_ * anInt4084;
					int i_326_ = anIntArray4108[i_325_];
					int i_327_ = i_281_ + i_326_;
					int i_328_ = (i_281_ & 0xff00ff) + (i_326_ & 0xff00ff);
					i_326_ = (i_328_ & 0x1000100) + (i_327_ - i_328_ & 0x10000);
					anIntArray4108[i_325_] = i_327_ - i_326_ | i_326_ - (i_326_ >>> 8);
				}
				i += i_313_;
				i_278_++;
				i_287_ += i_314_;
				i_287_ %= i_291_;
			}
		} else
			throw new IllegalArgumentException();
	}

	public void i(int i) {
		aClass240Array4103[i].method2145((byte) 41, Thread.currentThread());
	}

	private ha_Sub2(d var_d) {
		super(var_d);
		anInt4078 = 0;
		anInt4080 = 78642;
		aBoolean4083 = false;
		anInt4081 = 128;
		anInt4076 = 45823;
		anInt4085 = 0;
		anInt4071 = 0;
		anInt4086 = 512;
		anInt4072 = 512;
		anInt4104 = 0;
		anInt4097 = 0;
		anInt4087 = 3500;
		anInt4107 = 50;
		aClass113_4092 = new AdvancedMemoryCache(16);
		anInt4111 = -1;
		try {
			aClass113_4073 = new AdvancedMemoryCache(256);
			aClass373_Sub3_4090 = new Class373_Sub3();
			f(1);
			i(0);
			Class338.method3437(true, false, true);
			aBoolean4067 = true;
			anInt4064 = (int) Class72.method771(-106);
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			this.method1091((byte) -98);
			throw new RuntimeException("");
		}
	}

	public void a(Rectangle[] rectangles, int i, int i_329_, int i_330_) throws Exception_Sub1 {
		if (aCanvas4065 == null || aClass296_Sub29_4069 == null)
			throw new IllegalStateException("off");
		try {
			Graphics graphics = aCanvas4065.getGraphics();
			for (int i_331_ = 0; i_331_ < i; i_331_++) {
				Rectangle rectangle = rectangles[i_331_];
				if (rectangle.x + i_329_ <= anInt4084 && rectangle.y + i_330_ <= anInt4096
						&& rectangle.x + i_329_ + rectangle.width > 0 && rectangle.y + i_330_ + rectangle.height > 0)
					aClass296_Sub29_4069.method2692(rectangle.x, rectangle.height, rectangle.x + i_329_, graphics,
							(byte) 111, rectangle.y, rectangle.y + i_330_, rectangle.width);
			}
		} catch (Exception exception) {
			aCanvas4065.repaint();
		}
	}

	public void T(int i, int i_332_, int i_333_, int i_334_) {
		if (anInt4079 < i)
			anInt4079 = i;
		if (anInt4085 < i_332_)
			anInt4085 = i_332_;
		if (anInt4104 > i_333_)
			anInt4104 = i_333_;
		if (anInt4078 > i_334_)
			anInt4078 = i_334_;
		method1261();
	}

	public void DA(int i, int i_335_, int i_336_, int i_337_) {
		anInt4101 = i;
		anInt4100 = i_335_;
		anInt4086 = i_336_;
		anInt4072 = i_337_;
		method1261();
	}

	public boolean d() {
		return false;
	}

	public boolean u() {
		return false;
	}

	public int[] Y() {
		return new int[] { anInt4101, anInt4100, anInt4086, anInt4072 };
	}

	public void a(Class241 class241) {
		/* empty */
	}

	public void b(int i, int i_338_, int i_339_, int i_340_, double d) {
		int i_341_ = anInt4095 - i_339_;
		int i_342_ = i_338_ * anInt4095 + i;
		float[] fs = aFloatArray4074;
		int i_343_ = 0;
		while (i_343_ < i_340_) {
			int i_344_ = 0;
			while (i_344_ < i_339_) {
				float f = fs[i_342_];
				if (f != 2.14748365E9F)
					fs[i_342_] = (float) ((double) f + d);
				i_344_++;
				i_342_++;
			}
			i_343_++;
			i_342_ += i_341_;
		}
	}

	public Class373 m() {
		return new Class373_Sub3();
	}

	private void method1261() {
		anInt4082 = anInt4079 - anInt4101;
		anInt4106 = anInt4104 - anInt4101;
		anInt4094 = anInt4085 - anInt4100;
		anInt4077 = anInt4078 - anInt4100;
		for (int i = 0; i < anInt4105; i++) {
			Class11 class11 = aClass240Array4103[i].aClass11_2285;
			class11.anInt109 = anInt4101 - anInt4079;
			class11.anInt95 = anInt4100 - anInt4085;
			class11.anInt107 = anInt4104 - anInt4079;
			class11.anInt101 = anInt4078 - anInt4085;
		}
		int i = anInt4085 * anInt4084 + anInt4079;
		for (int i_345_ = anInt4085; i_345_ < anInt4078; i_345_++) {
			for (int i_346_ = 0; i_346_ < anInt4105; i_346_++)
				aClass240Array4103[i_346_].aClass11_2285.anIntArray97[i_345_ - anInt4085] = i;
			i += anInt4084;
		}
	}

	public Class373 f() {
		Class240 class240 = method1270(Thread.currentThread());
		return class240.aClass373_Sub3_2257;
	}

	public void a(Canvas canvas, int i, int i_347_) {
		Class296_Sub29 class296_sub29 = ((Class296_Sub29) aClass263_4068.get((long) canvas.hashCode()));
		if (class296_sub29 != null) {
			class296_sub29.unlink();
			class296_sub29 = Class296_Sub22.method2663(canvas, -28485, i_347_, i);
			aClass263_4068.put((long) canvas.hashCode(), class296_sub29);
			if (aCanvas4065 == canvas && aClass319_4088 == null) {
				Dimension dimension = canvas.getSize();
				anInt4070 = dimension.width;
				anInt4063 = dimension.height;
				aClass296_Sub29_4069 = class296_sub29;
				anIntArray4108 = class296_sub29.anIntArray4810;
				anInt4084 = class296_sub29.anInt4815;
				anInt4096 = class296_sub29.anInt4813;
				if (anInt4084 != anInt4095 || anInt4096 != anInt4102) {
					anInt4089 = anInt4095 = anInt4084;
					anInt4098 = anInt4102 = anInt4096;
					aFloatArray4099 = aFloatArray4074 = new float[anInt4095 * anInt4102];
				}
				method1265();
			}
		}
	}

	public boolean method1262(int i) {
		if (aBoolean4083 || aD1299.method14(i, -9412).small_sized)
			return true;
		return false;
	}

	public void ya() {
		if (anInt4079 == 0 && anInt4104 == anInt4084 && anInt4085 == 0 && anInt4078 == anInt4096) {
			int i = aFloatArray4074.length;
			int i_348_ = i - (i & 0x7);
			int i_349_ = 0;
			while (i_349_ < i_348_) {
				aFloatArray4074[i_349_++] = 2.14748365E9F;
				aFloatArray4074[i_349_++] = 2.14748365E9F;
				aFloatArray4074[i_349_++] = 2.14748365E9F;
				aFloatArray4074[i_349_++] = 2.14748365E9F;
				aFloatArray4074[i_349_++] = 2.14748365E9F;
				aFloatArray4074[i_349_++] = 2.14748365E9F;
				aFloatArray4074[i_349_++] = 2.14748365E9F;
				aFloatArray4074[i_349_++] = 2.14748365E9F;
			}
			while (i_349_ < i)
				aFloatArray4074[i_349_++] = 2.14748365E9F;
		} else {
			int i = anInt4104 - anInt4079;
			int i_350_ = anInt4078 - anInt4085;
			int i_351_ = anInt4084 - i;
			int i_352_ = anInt4079 + anInt4085 * anInt4084;
			int i_353_ = i >> 3;
			int i_354_ = i & 0x7;
			i = i_352_ - 1;
			for (int i_355_ = -i_350_; i_355_ < 0; i_355_++) {
				if (i_353_ > 0) {
					int i_356_ = i_353_;
					do {
						aFloatArray4074[++i] = 2.14748365E9F;
						aFloatArray4074[++i] = 2.14748365E9F;
						aFloatArray4074[++i] = 2.14748365E9F;
						aFloatArray4074[++i] = 2.14748365E9F;
						aFloatArray4074[++i] = 2.14748365E9F;
						aFloatArray4074[++i] = 2.14748365E9F;
						aFloatArray4074[++i] = 2.14748365E9F;
						aFloatArray4074[++i] = 2.14748365E9F;
					} while (--i_356_ > 0);
				}
				if (i_354_ > 0) {
					int i_357_ = i_354_;
					do
						aFloatArray4074[++i] = 2.14748365E9F;
					while (--i_357_ > 0);
				}
				i += i_351_;
			}
		}
	}

	public void Q(int i, int i_358_, int i_359_, int i_360_, int i_361_, int i_362_, byte[] is, int i_363_,
			int i_364_) {
		if (i_359_ > 0 && i_360_ > 0) {
			int i_365_ = 0;
			int i_366_ = 0;
			int i_367_ = (i_363_ << 16) / i_359_;
			int i_368_ = (is.length / i_363_ << 16) / i_360_;
			int i_369_ = i + i_358_ * anInt4084;
			int i_370_ = anInt4084 - i_359_;
			if (i_358_ + i_360_ > anInt4078)
				i_360_ -= i_358_ + i_360_ - anInt4078;
			if (i_358_ < anInt4085) {
				int i_371_ = anInt4085 - i_358_;
				i_360_ -= i_371_;
				i_369_ += i_371_ * anInt4084;
				i_366_ += i_368_ * i_371_;
			}
			if (i + i_359_ > anInt4104) {
				int i_372_ = i + i_359_ - anInt4104;
				i_359_ -= i_372_;
				i_370_ += i_372_;
			}
			if (i < anInt4079) {
				int i_373_ = anInt4079 - i;
				i_359_ -= i_373_;
				i_369_ += i_373_;
				i_365_ += i_367_ * i_373_;
				i_370_ += i_373_;
			}
			int i_374_ = i_361_ >>> 24;
			int i_375_ = i_362_ >>> 24;
			if (i_364_ == 0 || i_364_ == 1 && i_374_ == 255 && i_375_ == 255) {
				int i_376_ = i_365_;
				for (int i_377_ = -i_360_; i_377_ < 0; i_377_++) {
					int i_378_ = (i_366_ >> 16) * i_363_;
					for (int i_379_ = -i_359_; i_379_ < 0; i_379_++) {
						if (is[(i_365_ >> 16) + i_378_] != 0)
							anIntArray4108[i_369_++] = i_362_;
						else
							anIntArray4108[i_369_++] = i_361_;
						i_365_ += i_367_;
					}
					i_366_ += i_368_;
					i_365_ = i_376_;
					i_369_ += i_370_;
				}
			} else if (i_364_ == 1) {
				int i_380_ = i_365_;
				for (int i_381_ = -i_360_; i_381_ < 0; i_381_++) {
					int i_382_ = (i_366_ >> 16) * i_363_;
					for (int i_383_ = -i_359_; i_383_ < 0; i_383_++) {
						int i_384_ = i_361_;
						if (is[(i_365_ >> 16) + i_382_] != 0)
							i_384_ = i_362_;
						int i_385_ = i_384_ >>> 24;
						int i_386_ = 255 - i_385_;
						int i_387_ = anIntArray4108[i_369_];
						anIntArray4108[i_369_++] = ((((i_384_ & 0xff00ff) * i_385_ + (i_387_ & 0xff00ff) * i_386_)
								& ~0xff00ff)
								+ (((i_384_ & 0xff00) * i_385_ + (i_387_ & 0xff00) * i_386_) & 0xff0000)) >> 8;
						i_365_ += i_367_;
					}
					i_366_ += i_368_;
					i_365_ = i_380_;
					i_369_ += i_370_;
				}
			} else if (i_364_ == 2) {
				int i_388_ = i_365_;
				for (int i_389_ = -i_360_; i_389_ < 0; i_389_++) {
					int i_390_ = (i_366_ >> 16) * i_363_;
					for (int i_391_ = -i_359_; i_391_ < 0; i_391_++) {
						int i_392_ = i_361_;
						if (is[(i_365_ >> 16) + i_390_] != 0)
							i_392_ = i_362_;
						if (i_392_ != 0) {
							int i_393_ = anIntArray4108[i_369_];
							int i_394_ = i_392_ + i_393_;
							int i_395_ = (i_392_ & 0xff00ff) + (i_393_ & 0xff00ff);
							i_393_ = (i_395_ & 0x1000100) + (i_394_ - i_395_ & 0x10000);
							anIntArray4108[i_369_++] = i_394_ - i_393_ | i_393_ - (i_393_ >>> 8);
						} else
							i_369_++;
						i_365_ += i_367_;
					}
					i_366_ += i_368_;
					i_365_ = i_388_;
					i_369_ += i_370_;
				}
			} else
				throw new IllegalArgumentException();
		}
	}

	public s a(int i, int i_396_, int[][] is, int[][] is_397_, int i_398_, int i_399_, int i_400_) {
		return new s_Sub1(this, i_399_, i_400_, i, i_396_, is, is_397_, i_398_);
	}

	private void method1263(Class338_Sub8_Sub2 class338_sub8_sub2, int i, int i_401_, int i_402_, int i_403_) {
		int i_404_ = class338_sub8_sub2.anInt6582;
		int i_405_ = i_403_;
		i_403_ <<= 1;
		if (i_404_ == -1)
			method1258(i, i_401_, i_402_, i_405_, class338_sub8_sub2.anInt6583, 1);
		else {
			if (anInt4111 != i_404_) {
				Sprite class397 = (Sprite) aClass113_4092.get((long) i_404_);
				if (class397 == null) {
					int[] is = method1260(i_404_);
					if (is != null) {
						int i_406_ = method1262(i_404_) ? 64 : anInt4081;
						class397 = this.method1096(117, 0, i_406_, i_406_, i_406_, is);
						aClass113_4092.put(class397, (long) i_404_);
					} else
						return;
				}
				anInt4111 = i_404_;
				aClass397_4110 = class397;
			}
			i_403_++;
			((Class397_Sub1) aClass397_4110).method4103(i - i_405_, i_401_ - i_405_, i_402_, i_403_, i_403_, 0,
					class338_sub8_sub2.anInt6583, 1, 1);
		}
	}

	public aa a(int i, int i_407_, int[] is, int[] is_408_) {
		return new aa_Sub2(i, i_407_, is, is_408_);
	}

	public boolean x() {
		return false;
	}

	public Sprite a(int[] is, int i, int i_409_, int i_410_, int i_411_, boolean bool) {
		boolean bool_412_ = false;
		int i_413_ = i;
		while_123_: for (int i_414_ = 0; i_414_ < i_411_; i_414_++) {
			for (int i_415_ = 0; i_415_ < i_410_; i_415_++) {
				int i_416_ = is[i_413_++] >>> 24;
				if (i_416_ != 0 && i_416_ != 255) {
					bool_412_ = true;
					break while_123_;
				}
			}
		}
		if (bool_412_)
			return new Class397_Sub1_Sub1(this, is, i, i_409_, i_410_, i_411_, bool);
		return new Class397_Sub1_Sub3(this, is, i, i_409_, i_410_, i_411_, bool);
	}

	private void method1264(int i, int i_417_, int i_418_, int i_419_, int i_420_, int i_421_, int i_422_, int i_423_) {
		if (i_417_ >= anInt4085 && i_417_ < anInt4078) {
			int i_424_ = i + i_417_ * anInt4084;
			int i_425_ = i_419_ >>> 24;
			int i_426_ = i_421_ + i_422_;
			int i_427_ = i_423_ % i_426_;
			if (i_420_ == 0 || i_420_ == 1 && i_425_ == 255) {
				int i_428_ = 0;
				while (i_428_ < i_418_) {
					if (i + i_428_ >= anInt4079 && i + i_428_ < anInt4104 && i_427_ < i_421_)
						anIntArray4108[i_424_ + i_428_] = i_419_;
					i_428_++;
					i_427_ = ++i_427_ % i_426_;
				}
			} else if (i_420_ == 1) {
				i_419_ = (((i_419_ & 0xff00ff) * i_425_ >> 8 & 0xff00ff) + ((i_419_ & 0xff00) * i_425_ >> 8 & 0xff00)
						+ (i_425_ << 24));
				int i_429_ = 256 - i_425_;
				int i_430_ = 0;
				while (i_430_ < i_418_) {
					if (i + i_430_ >= anInt4079 && i + i_430_ < anInt4104 && i_427_ < i_421_) {
						int i_431_ = anIntArray4108[i_424_ + i_430_];
						i_431_ = (((i_431_ & 0xff00ff) * i_429_ >> 8 & 0xff00ff)
								+ ((i_431_ & 0xff00) * i_429_ >> 8 & 0xff00));
						anIntArray4108[i_424_ + i_430_] = i_419_ + i_431_;
					}
					i_430_++;
					i_427_ = ++i_427_ % i_426_;
				}
			} else if (i_420_ == 2) {
				int i_432_ = 0;
				while (i_432_ < i_418_) {
					if (i + i_432_ >= anInt4079 && i + i_432_ < anInt4104 && i_427_ < i_421_) {
						int i_433_ = anIntArray4108[i_424_ + i_432_];
						int i_434_ = i_419_ + i_433_;
						int i_435_ = (i_419_ & 0xff00ff) + (i_433_ & 0xff00ff);
						i_433_ = (i_435_ & 0x1000100) + (i_434_ - i_435_ & 0x10000);
						anIntArray4108[i_424_ + i_432_] = i_434_ - i_433_ | i_433_ - (i_433_ >>> 8);
					}
					i_432_++;
					i_427_ = ++i_427_ % i_426_;
				}
			} else
				throw new IllegalArgumentException();
		}
	}

	public Class373 o() {
		return aClass373_Sub3_4090;
	}

	public void a(int i, int i_436_, int i_437_, int i_438_, int i_439_, int i_440_, int i_441_, int i_442_,
			int i_443_) {
		i_437_ -= i;
		i_438_ -= i_436_;
		if (i_438_ == 0) {
			if (i_437_ >= 0)
				method1264(i, i_436_, i_437_ + 1, i_439_, i_440_, i_441_, i_442_, i_443_);
			else {
				int i_444_ = i_441_ + i_442_;
				i_443_ %= i_444_;
				i_443_ = i_444_ + i_441_ - i_443_ - (-i_437_ + 1) % i_444_;
				i_443_ %= i_444_;
				if (i_443_ < 0)
					i_443_ += i_444_;
				method1264(i + i_437_, i_436_, -i_437_ + 1, i_439_, i_440_, i_441_, i_442_, i_443_);
			}
		} else if (i_437_ == 0) {
			if (i_438_ >= 0)
				method1266(i, i_436_, i_438_ + 1, i_439_, i_440_, i_441_, i_442_, i_443_);
			else {
				int i_445_ = i_441_ + i_442_;
				i_443_ %= i_445_;
				i_443_ = i_445_ + i_441_ - i_443_ - (-i_438_ + 1) % i_445_;
				i_443_ %= i_445_;
				if (i_443_ < 0)
					i_443_ += i_445_;
				method1266(i, i_436_ + i_438_, -i_438_ + 1, i_439_, i_440_, i_441_, i_442_, i_443_);
			}
		} else {
			i_443_ <<= 8;
			i_441_ <<= 8;
			i_442_ <<= 8;
			int i_446_ = i_441_ + i_442_;
			i_443_ %= i_446_;
			if (i_437_ + i_438_ < 0) {
				int i_447_ = (int) (Math.sqrt((double) (i_437_ * i_437_ + i_438_ * i_438_)) * 256.0);
				int i_448_ = i_447_ % i_446_;
				i_443_ = i_446_ + i_441_ - i_443_ - i_448_;
				i_443_ %= i_446_;
				if (i_443_ < 0)
					i_443_ += i_446_;
				i += i_437_;
				i_437_ = -i_437_;
				i_436_ += i_438_;
				i_438_ = -i_438_;
			}
			if (i_437_ > i_438_) {
				i_436_ <<= 16;
				i_436_ += 32768;
				i_438_ <<= 16;
				int i_449_ = (int) Math.floor((double) i_438_ / (double) i_437_ + 0.5);
				i_437_ += i;
				int i_450_ = i_439_ >>> 24;
				int i_451_ = (int) Math.sqrt((double) ((i_449_ >> 8) * (i_449_ >> 8) + 65536));
				if (i_440_ == 0 || i_440_ == 1 && i_450_ == 255) {
					while (i <= i_437_) {
						int i_452_ = i_436_ >> 16;
						if (i >= anInt4079 && i < anInt4104 && i_452_ >= anInt4085 && i_452_ < anInt4078
								&& i_443_ < i_441_)
							anIntArray4108[i + i_452_ * anInt4084] = i_439_;
						i_436_ += i_449_;
						i++;
						i_443_ += i_451_;
						i_443_ %= i_446_;
					}
					return;
				}
				if (i_440_ == 1) {
					i_439_ = (((i_439_ & 0xff00ff) * i_450_ >> 8 & 0xff00ff)
							+ ((i_439_ & 0xff00) * i_450_ >> 8 & 0xff00) + (i_450_ << 24));
					int i_453_ = 256 - i_450_;
					while (i <= i_437_) {
						int i_454_ = i_436_ >> 16;
						if (i >= anInt4079 && i < anInt4104 && i_454_ >= anInt4085 && i_454_ < anInt4078
								&& i_443_ < i_441_) {
							int i_455_ = i + i_454_ * anInt4084;
							int i_456_ = anIntArray4108[i_455_];
							i_456_ = (((i_456_ & 0xff00ff) * i_453_ >> 8 & 0xff00ff)
									+ ((i_456_ & 0xff00) * i_453_ >> 8 & 0xff00));
							anIntArray4108[i_455_] = i_439_ + i_456_;
						}
						i_436_ += i_449_;
						i++;
						i_443_ += i_451_;
						i_443_ %= i_446_;
					}
					return;
				}
				if (i_440_ == 2) {
					while (i <= i_437_) {
						int i_457_ = i_436_ >> 16;
						if (i >= anInt4079 && i < anInt4104 && i_457_ >= anInt4085 && i_457_ < anInt4078
								&& i_443_ < i_441_) {
							int i_458_ = i + i_457_ * anInt4084;
							int i_459_ = anIntArray4108[i_458_];
							int i_460_ = i_439_ + i_459_;
							int i_461_ = (i_439_ & 0xff00ff) + (i_459_ & 0xff00ff);
							i_459_ = (i_461_ & 0x1000100) + (i_460_ - i_461_ & 0x10000);
							anIntArray4108[i_458_] = i_460_ - i_459_ | i_459_ - (i_459_ >>> 8);
						}
						i_436_ += i_449_;
						i++;
						i_443_ += i_451_;
						i_443_ %= i_446_;
					}
					return;
				}
				throw new IllegalArgumentException();
			}
			i <<= 16;
			i += 32768;
			i_437_ <<= 16;
			int i_462_ = (int) Math.floor((double) i_437_ / (double) i_438_ + 0.5);
			i_438_ += i_436_;
			int i_463_ = i_439_ >>> 24;
			int i_464_ = (int) Math.sqrt((double) ((i_462_ >> 8) * (i_462_ >> 8) + 65536));
			if (i_440_ == 0 || i_440_ == 1 && i_463_ == 255) {
				while (i_436_ <= i_438_) {
					int i_465_ = i >> 16;
					if (i_436_ >= anInt4085 && i_436_ < anInt4078 && i_465_ >= anInt4079 && i_465_ < anInt4104
							&& i_443_ < i_441_)
						anIntArray4108[i_465_ + i_436_ * anInt4084] = i_439_;
					i += i_462_;
					i_436_++;
					i_443_ += i_464_;
					i_443_ %= i_446_;
				}
			} else if (i_440_ == 1) {
				i_439_ = (((i_439_ & 0xff00ff) * i_463_ >> 8 & 0xff00ff) + ((i_439_ & 0xff00) * i_463_ >> 8 & 0xff00)
						+ (i_463_ << 24));
				int i_466_ = 256 - i_463_;
				while (i_436_ <= i_438_) {
					int i_467_ = i >> 16;
					if (i_436_ >= anInt4085 && i_436_ < anInt4078 && i_467_ >= anInt4079 && i_467_ < anInt4104
							&& i_443_ < i_441_) {
						int i_468_ = i_467_ + i_436_ * anInt4084;
						int i_469_ = anIntArray4108[i_468_];
						i_469_ = (((i_469_ & 0xff00ff) * i_466_ >> 8 & 0xff00ff)
								+ ((i_469_ & 0xff00) * i_466_ >> 8 & 0xff00));
						anIntArray4108[i_467_ + i_436_ * anInt4084] = i_439_ + i_469_;
					}
					i += i_462_;
					i_436_++;
					i_443_ += i_464_;
					i_443_ %= i_446_;
				}
			} else if (i_440_ == 2) {
				while (i_436_ <= i_438_) {
					int i_470_ = i >> 16;
					if (i_436_ >= anInt4085 && i_436_ < anInt4078 && i_470_ >= anInt4079 && i_470_ < anInt4104
							&& i_443_ < i_441_) {
						int i_471_ = i_470_ + i_436_ * anInt4084;
						int i_472_ = anIntArray4108[i_471_];
						int i_473_ = i_439_ + i_472_;
						int i_474_ = (i_439_ & 0xff00ff) + (i_472_ & 0xff00ff);
						i_472_ = (i_474_ & 0x1000100) + (i_473_ - i_474_ & 0x10000);
						anIntArray4108[i_471_] = i_473_ - i_472_ | i_472_ - (i_472_ >>> 8);
					}
					i += i_462_;
					i_436_++;
					i_443_ += i_464_;
					i_443_ %= i_446_;
				}
			} else
				throw new IllegalArgumentException();
		}
	}

	private void method1265() {
		for (int i = 0; i < anInt4105; i++)
			aClass240Array4103[i].method2146((byte) 11);
		la();
	}

	private void method1266(int i, int i_475_, int i_476_, int i_477_, int i_478_, int i_479_, int i_480_, int i_481_) {
		if (i >= anInt4079 && i < anInt4104) {
			int i_482_ = i + i_475_ * anInt4084;
			int i_483_ = i_477_ >>> 24;
			int i_484_ = i_479_ + i_480_;
			int i_485_ = i_481_ % i_484_;
			if (i_478_ == 0 || i_478_ == 1 && i_483_ == 255) {
				int i_486_ = 0;
				while (i_486_ < i_476_) {
					if (i_475_ + i_486_ >= anInt4085 && i_475_ + i_486_ < anInt4078 && i_485_ < i_479_)
						anIntArray4108[i_482_ + i_486_ * anInt4084] = i_477_;
					i_486_++;
					i_485_ = ++i_485_ % i_484_;
				}
			} else if (i_478_ == 1) {
				i_477_ = (((i_477_ & 0xff00ff) * i_483_ >> 8 & 0xff00ff) + ((i_477_ & 0xff00) * i_483_ >> 8 & 0xff00)
						+ (i_483_ << 24));
				int i_487_ = 256 - i_483_;
				int i_488_ = 0;
				while (i_488_ < i_476_) {
					if (i_475_ + i_488_ >= anInt4085 && i_475_ + i_488_ < anInt4078 && i_485_ < i_479_) {
						int i_489_ = i_482_ + i_488_ * anInt4084;
						int i_490_ = anIntArray4108[i_489_];
						i_490_ = (((i_490_ & 0xff00ff) * i_487_ >> 8 & 0xff00ff)
								+ ((i_490_ & 0xff00) * i_487_ >> 8 & 0xff00));
						anIntArray4108[i_489_] = i_477_ + i_490_;
					}
					i_488_++;
					i_485_ = ++i_485_ % i_484_;
				}
			} else if (i_478_ == 2) {
				int i_491_ = 0;
				while (i_491_ < i_476_) {
					if (i_475_ + i_491_ >= anInt4085 && i_475_ + i_491_ < anInt4078 && i_485_ < i_479_) {
						int i_492_ = i_482_ + i_491_ * anInt4084;
						int i_493_ = anIntArray4108[i_492_];
						int i_494_ = i_477_ + i_493_;
						int i_495_ = (i_477_ & 0xff00ff) + (i_493_ & 0xff00ff);
						i_493_ = (i_495_ & 0x1000100) + (i_494_ - i_495_ & 0x10000);
						anIntArray4108[i_492_] = i_494_ - i_493_ | i_493_ - (i_493_ >>> 8);
					}
					i_491_++;
					i_485_ = ++i_485_ % i_484_;
				}
			} else
				throw new IllegalArgumentException();
		}
	}

	public boolean method1267(int i) {
		return aD1299.is_ready(i);
	}

	public void K(int[] is) {
		is[0] = anInt4079;
		is[1] = anInt4085;
		is[2] = anInt4104;
		is[3] = anInt4078;
	}

	public void KA(int i, int i_496_, int i_497_, int i_498_) {
		if (i < 0)
			i = 0;
		if (i_496_ < 0)
			i_496_ = 0;
		if (i_497_ > anInt4084)
			i_497_ = anInt4084;
		if (i_498_ > anInt4096)
			i_498_ = anInt4096;
		anInt4079 = i;
		anInt4104 = i_497_;
		anInt4085 = i_496_;
		anInt4078 = i_498_;
		method1261();
	}

	public boolean l() {
		return true;
	}

	public void b(Canvas canvas, int i, int i_499_) {
		Class296_Sub29 class296_sub29 = ((Class296_Sub29) aClass263_4068.get((long) canvas.hashCode()));
		if (class296_sub29 == null) {
			class296_sub29 = Class296_Sub22.method2663(canvas, -28485, i_499_, i);
			aClass263_4068.put((long) canvas.hashCode(), class296_sub29);
		} else if (class296_sub29.anInt4815 != i || class296_sub29.anInt4813 != i_499_)
			a(canvas, i, i_499_);
	}

	public int e(int i, int i_500_) {
		i |= 0x20800;
		return i & i_500_ ^ i_500_;
	}

	public void a(Class390 class390) {
		Class240 class240 = method1270(Thread.currentThread());
		Class338_Sub8 class338_sub8 = class390.aClass84_3282.aClass338_Sub8_923;
		for (Class338_Sub8 class338_sub8_501_ = class338_sub8.aClass338_Sub8_5253; class338_sub8_501_ != class338_sub8; class338_sub8_501_ = class338_sub8_501_.aClass338_Sub8_5253) {
			Class338_Sub8_Sub2 class338_sub8_sub2 = (Class338_Sub8_Sub2) class338_sub8_501_;
			int i = class338_sub8_sub2.anInt6580 >> 12;
			int i_502_ = class338_sub8_sub2.anInt6589 >> 12;
			int i_503_ = class338_sub8_sub2.anInt6581 >> 12;
			float f = (aClass373_Sub3_4090.aFloat5618
					+ (aClass373_Sub3_4090.aFloat5614 * (float) i + aClass373_Sub3_4090.aFloat5617 * (float) i_502_
							+ aClass373_Sub3_4090.aFloat5604 * (float) i_503_));
			if (!(f < (float) anInt4107) && !(f > (float) class240.anInt2267)) {
				int i_504_ = (anInt4101 + (int) ((float) anInt4086
						* (aClass373_Sub3_4090.aFloat5611 + ((aClass373_Sub3_4090.aFloat5606 * (float) i)
								+ (aClass373_Sub3_4090.aFloat5613 * (float) i_502_)
								+ (aClass373_Sub3_4090.aFloat5610 * (float) i_503_)))
						/ f));
				int i_505_ = (anInt4100 + (int) ((float) anInt4072
						* (aClass373_Sub3_4090.aFloat5619 + ((aClass373_Sub3_4090.aFloat5607 * (float) i)
								+ (aClass373_Sub3_4090.aFloat5615 * (float) i_502_)
								+ (aClass373_Sub3_4090.aFloat5616 * (float) i_503_)))
						/ f));
				if (i_504_ >= anInt4079 && i_504_ <= anInt4104 && i_505_ >= anInt4085 && i_505_ <= anInt4078) {
					if (f == 0.0F)
						f = 1.0F;
					method1263(class338_sub8_sub2, i_504_, i_505_, (int) f,
							(int) ((float) ((class338_sub8_sub2.anInt6579 * anInt4086) >> 12) / f));
				}
			}
		}
	}

	public void a(int i, int i_506_, int i_507_, int i_508_) {
		/* empty */
	}

	public Class296_Sub35 a(int i, int i_509_, int i_510_, int i_511_, int i_512_, float f) {
		return null;
	}

	public boolean b() {
		return false;
	}

	public void h(int i) {
		Class178_Sub1.anInt4380 = Class178_Sub1.anInt4378 = i;
		if (anInt4105 > 1)
			throw new IllegalStateException("No MT");
		f(anInt4105);
		i(0);
	}

	public Class33 s() {
		return new Class33(0, "Pure Java", 1, "CPU", 0L);
	}

	public boolean method1268(int i) {
		if (!aD1299.method14(i, -9412).aBoolean1779 && !aD1299.method14(i, -9412).aBoolean1780)
			return false;
		return true;
	}

	public int i() {
		return anInt4107;
	}

	public int E() {
		return 0;
	}

	public void g(int i) {
		int i_513_ = i - anInt4064;
		for (Object object = aClass113_4073.getFirst(); object != null; object = aClass113_4073.getNext()) {
			Class296_Sub23 class296_sub23 = (Class296_Sub23) object;
			if (class296_sub23.aBoolean4744) {
				class296_sub23.anInt4748 += i_513_;
				int i_514_ = class296_sub23.anInt4748 / 20;
				if (i_514_ > 0) {
					MaterialRaw class170 = aD1299.method14(class296_sub23.anInt4747, -9412);
					class296_sub23.method2666((class170.speed_u * i_513_ * 50 / 1000),
							(class170.speed_v * i_513_ * 50 / 1000));
					class296_sub23.anInt4748 -= i_514_ * 20;
				}
				class296_sub23.aBoolean4744 = false;
			}
		}
		anInt4064 = i;
		aClass113_4092.clean(5);
		aClass113_4073.clean(5);
	}

	public void b(Canvas canvas) {
		if (canvas != null) {
			Class296_Sub29 class296_sub29 = ((Class296_Sub29) aClass263_4068.get((long) canvas.hashCode()));
			if (class296_sub29 != null) {
				aCanvas4065 = canvas;
				Dimension dimension = canvas.getSize();
				anInt4070 = dimension.width;
				anInt4063 = dimension.height;
				aClass296_Sub29_4069 = class296_sub29;
				if (aClass319_4088 == null) {
					anIntArray4108 = class296_sub29.anIntArray4810;
					anInt4084 = class296_sub29.anInt4815;
					anInt4096 = class296_sub29.anInt4813;
					if (anInt4084 != anInt4095 || anInt4096 != anInt4102) {
						anInt4089 = anInt4095 = anInt4084;
						anInt4098 = anInt4102 = anInt4096;
						aFloatArray4099 = aFloatArray4074 = new float[anInt4095 * anInt4102];
					}
					method1265();
				}
			}
		} else {
			aCanvas4065 = null;
			aClass296_Sub29_4069 = null;
			if (aClass319_4088 == null) {
				anIntArray4108 = null;
				anInt4084 = anInt4096 = 1;
				anInt4095 = anInt4102 = 1;
				method1265();
			}
		}
	}

	public void a(Class390 class390, int i) {
		Class240 class240 = method1270(Thread.currentThread());
		Class338_Sub8 class338_sub8 = class390.aClass84_3282.aClass338_Sub8_923;
		for (Class338_Sub8 class338_sub8_515_ = class338_sub8.aClass338_Sub8_5253; class338_sub8_515_ != class338_sub8; class338_sub8_515_ = class338_sub8_515_.aClass338_Sub8_5253) {
			Class338_Sub8_Sub2 class338_sub8_sub2 = (Class338_Sub8_Sub2) class338_sub8_515_;
			int i_516_ = class338_sub8_sub2.anInt6580 >> 12;
			int i_517_ = class338_sub8_sub2.anInt6589 >> 12;
			int i_518_ = class338_sub8_sub2.anInt6581 >> 12;
			float f = (aClass373_Sub3_4090.aFloat5618
					+ (aClass373_Sub3_4090.aFloat5614 * (float) i_516_ + aClass373_Sub3_4090.aFloat5617 * (float) i_517_
							+ aClass373_Sub3_4090.aFloat5604 * (float) i_518_));
			if (!(f < (float) anInt4107) && !(f > (float) class240.anInt2267)) {
				int i_519_ = (anInt4101 + (int) ((float) anInt4086
						* (aClass373_Sub3_4090.aFloat5611 + ((aClass373_Sub3_4090.aFloat5606 * (float) i_516_)
								+ (aClass373_Sub3_4090.aFloat5613 * (float) i_517_)
								+ (aClass373_Sub3_4090.aFloat5610 * (float) i_518_)))
						/ (float) i));
				int i_520_ = (anInt4100 + (int) ((float) anInt4072
						* (aClass373_Sub3_4090.aFloat5619 + ((aClass373_Sub3_4090.aFloat5607 * (float) i_516_)
								+ (aClass373_Sub3_4090.aFloat5615 * (float) i_517_)
								+ (aClass373_Sub3_4090.aFloat5616 * (float) i_518_)))
						/ (float) i));
				if (i_519_ >= anInt4079 && i_519_ <= anInt4104 && i_520_ >= anInt4085 && i_520_ <= anInt4078) {
					if (f == 0.0F)
						f = 1.0F;
					method1263(class338_sub8_sub2, i_519_, i_520_, (int) f,
							(class338_sub8_sub2.anInt6579 * anInt4086 >> 12) / i);
				}
			}
		}
	}

	public Interface19 b(int i, int i_521_) {
		return a(i, i_521_, false);
	}

	public void H(int i, int i_522_, int i_523_, int[] is) {
		float f = (aClass373_Sub3_4090.aFloat5618 + (aClass373_Sub3_4090.aFloat5614 * (float) i
				+ aClass373_Sub3_4090.aFloat5617 * (float) i_522_ + aClass373_Sub3_4090.aFloat5604 * (float) i_523_));
		if (f == 0.0F)
			is[0] = is[1] = is[2] = -1;
		else {
			int i_524_ = (int) ((float) anInt4086 * (aClass373_Sub3_4090.aFloat5611
					+ (aClass373_Sub3_4090.aFloat5606 * (float) i + (aClass373_Sub3_4090.aFloat5613 * (float) i_522_)
							+ (aClass373_Sub3_4090.aFloat5610 * (float) i_523_)))
					/ f);
			int i_525_ = (int) ((float) anInt4072 * (aClass373_Sub3_4090.aFloat5619
					+ (aClass373_Sub3_4090.aFloat5607 * (float) i + (aClass373_Sub3_4090.aFloat5615 * (float) i_522_)
							+ (aClass373_Sub3_4090.aFloat5616 * (float) i_523_)))
					/ f);
			is[0] = i_524_ - anInt4082;
			is[1] = i_525_ - anInt4094;
			is[2] = (int) f;
		}
	}

	public void da(int i, int i_526_, int i_527_, int[] is) {
		float f = (aClass373_Sub3_4090.aFloat5618 + (aClass373_Sub3_4090.aFloat5614 * (float) i
				+ aClass373_Sub3_4090.aFloat5617 * (float) i_526_ + aClass373_Sub3_4090.aFloat5604 * (float) i_527_));
		if (f < (float) anInt4107 || f > (float) anInt4087)
			is[0] = is[1] = is[2] = -1;
		else {
			int i_528_ = (int) ((float) anInt4086 * (aClass373_Sub3_4090.aFloat5611
					+ (aClass373_Sub3_4090.aFloat5606 * (float) i + (aClass373_Sub3_4090.aFloat5613 * (float) i_526_)
							+ (aClass373_Sub3_4090.aFloat5610 * (float) i_527_)))
					/ f);
			int i_529_ = (int) ((float) anInt4072 * (aClass373_Sub3_4090.aFloat5619
					+ (aClass373_Sub3_4090.aFloat5607 * (float) i + (aClass373_Sub3_4090.aFloat5615 * (float) i_526_)
							+ (aClass373_Sub3_4090.aFloat5616 * (float) i_527_)))
					/ f);
			if (i_528_ >= anInt4082 && i_528_ <= anInt4106 && i_529_ >= anInt4094 && i_529_ <= anInt4077) {
				is[0] = i_528_ - anInt4082;
				is[1] = i_529_ - anInt4094;
				is[2] = (int) f;
			} else
				is[0] = is[1] = is[2] = -1;
		}
	}

	public boolean n() {
		return false;
	}

	public void a(int i, int i_530_, int i_531_, int i_532_, int i_533_, int i_534_, int i_535_, int i_536_, int i_537_,
			int i_538_, int i_539_, int i_540_, int i_541_) {
		Class240 class240 = method1270(Thread.currentThread());
		Class11 class11 = class240.aClass11_2285;
		class11.aBoolean102 = false;
		i -= anInt4082;
		i_532_ -= anInt4082;
		i_535_ -= anInt4082;
		i_530_ -= anInt4094;
		i_533_ -= anInt4094;
		i_536_ -= anInt4094;
		class11.aBoolean103 = (i < 0 || i > class11.anInt107 || i_532_ < 0 || i_532_ > class11.anInt107 || i_535_ < 0
				|| i_535_ > class11.anInt107);
		int i_542_ = i_538_ >>> 24;
		if (i_541_ == 0 || i_541_ == 1 && i_542_ == 255) {
			class11.anInt108 = 0;
			class11.aBoolean106 = false;
			class11.method199((float) i_530_, (float) i_533_, (float) i_536_, (float) i, (float) i_532_, (float) i_535_,
					(float) i_531_, (float) i_534_, (float) i_537_, i_538_, i_539_, i_540_);
		} else if (i_541_ == 1) {
			class11.anInt108 = 255 - i_542_;
			class11.aBoolean106 = false;
			class11.method199((float) i_530_, (float) i_533_, (float) i_536_, (float) i, (float) i_532_, (float) i_535_,
					(float) i_531_, (float) i_534_, (float) i_537_, i_538_, i_539_, i_540_);
		} else if (i_541_ == 2) {
			class11.anInt108 = 128;
			class11.aBoolean106 = true;
			class11.method199((float) i_530_, (float) i_533_, (float) i_536_, (float) i, (float) i_532_, (float) i_535_,
					(float) i_531_, (float) i_534_, (float) i_537_, i_538_, i_539_, i_540_);
		} else
			throw new IllegalArgumentException();
		class11.aBoolean102 = true;
	}

	public void C(boolean bool) {
		Class240 class240 = method1270(Thread.currentThread());
		class240.aBoolean2262 = bool;
	}

	public void P(int i, int i_543_, int i_544_, int i_545_, int i_546_) {
		if (i >= anInt4079 && i < anInt4104) {
			if (i_543_ < anInt4085) {
				i_544_ -= anInt4085 - i_543_;
				i_543_ = anInt4085;
			}
			if (i_543_ + i_544_ > anInt4078)
				i_544_ = anInt4078 - i_543_;
			int i_547_ = i + i_543_ * anInt4084;
			int i_548_ = i_545_ >>> 24;
			if (i_546_ == 0 || i_546_ == 1 && i_548_ == 255) {
				for (int i_549_ = 0; i_549_ < i_544_; i_549_++)
					anIntArray4108[i_547_ + i_549_ * anInt4084] = i_545_;
			} else if (i_546_ == 1) {
				i_545_ = (((i_545_ & 0xff00ff) * i_548_ >> 8 & 0xff00ff) + ((i_545_ & 0xff00) * i_548_ >> 8 & 0xff00)
						+ (i_548_ << 24));
				int i_550_ = 256 - i_548_;
				for (int i_551_ = 0; i_551_ < i_544_; i_551_++) {
					int i_552_ = i_547_ + i_551_ * anInt4084;
					int i_553_ = anIntArray4108[i_552_];
					i_553_ = (((i_553_ & 0xff00ff) * i_550_ >> 8 & 0xff00ff)
							+ ((i_553_ & 0xff00) * i_550_ >> 8 & 0xff00));
					anIntArray4108[i_552_] = i_545_ + i_553_;
				}
			} else if (i_546_ == 2) {
				for (int i_554_ = 0; i_554_ < i_544_; i_554_++) {
					int i_555_ = i_547_ + i_554_ * anInt4084;
					int i_556_ = anIntArray4108[i_555_];
					int i_557_ = i_545_ + i_556_;
					int i_558_ = (i_545_ & 0xff00ff) + (i_556_ & 0xff00ff);
					i_556_ = (i_558_ & 0x1000100) + (i_557_ - i_558_ & 0x10000);
					anIntArray4108[i_555_] = i_557_ - i_556_ | i_556_ - (i_556_ >>> 8);
				}
			} else
				throw new IllegalArgumentException();
		}
	}

	public int method1269(int i) {
		return aD1299.method14(i, -9412).aShort1775 & 0xffff;
	}

	public Sprite a(Class186 class186, boolean bool) {
		int[] is = class186.anIntArray1900;
		byte[] is_559_ = class186.aByteArray1901;
		int i = class186.anInt1899;
		int i_560_ = class186.anInt1904;
		Class397_Sub1 class397_sub1;
		if (bool && class186.aByteArray1905 == null) {
			int[] is_561_ = new int[is.length];
			byte[] is_562_ = new byte[i * i_560_];
			for (int i_563_ = 0; i_563_ < i_560_; i_563_++) {
				int i_564_ = i_563_ * i;
				for (int i_565_ = 0; i_565_ < i; i_565_++)
					is_562_[i_564_ + i_565_] = is_559_[i_564_ + i_565_];
			}
			for (int i_566_ = 0; i_566_ < is.length; i_566_++)
				is_561_[i_566_] = is[i_566_];
			class397_sub1 = new Class397_Sub1_Sub2(this, is_562_, is_561_, i, i_560_);
		} else {
			int[] is_567_ = new int[i * i_560_];
			byte[] is_568_ = class186.aByteArray1905;
			if (is_568_ != null) {
				for (int i_569_ = 0; i_569_ < i_560_; i_569_++) {
					int i_570_ = i_569_ * i;
					for (int i_571_ = 0; i_571_ < i; i_571_++)
						is_567_[i_570_ + i_571_] = (is[is_559_[i_570_ + i_571_] & 0xff]
								| is_568_[i_570_ + i_571_] << 24);
				}
				class397_sub1 = new Class397_Sub1_Sub1(this, is_567_, i, i_560_);
			} else {
				for (int i_572_ = 0; i_572_ < i_560_; i_572_++) {
					int i_573_ = i_572_ * i;
					for (int i_574_ = 0; i_574_ < i; i_574_++) {
						int i_575_ = is[is_559_[i_573_ + i_574_] & 0xff];
						is_567_[i_573_ + i_574_] = i_575_ != 0 ? i_575_ | ~0xffffff : 0;
					}
				}
				class397_sub1 = new Class397_Sub1_Sub3(this, is_567_, i, i_560_);
			}
		}
		class397_sub1.method4097(class186.anInt1903, class186.anInt1906, class186.anInt1907, class186.anInt1902);
		return class397_sub1;
	}

	public boolean r() {
		return true;
	}

	public Model a(Mesh class132, int i, int i_576_, int i_577_, int i_578_) {
		return new Class178_Sub1(this, class132, i, i_577_, i_578_, i_576_);
	}

	public int XA() {
		return anInt4087;
	}

	public int M() {
		int i = anInt4097;
		anInt4097 = 0;
		return i;
	}

	public boolean v() {
		return false;
	}

	public Class240 method1270(Runnable runnable) {
		for (int i = 0; i < anInt4105; i++) {
			if (aClass240Array4103[i].aRunnable2261 == runnable)
				return aClass240Array4103[i];
		}
		return null;
	}

	public void a(int i, int i_579_, int i_580_, int i_581_, int i_582_, int i_583_, int i_584_) {
		Class240 class240 = method1270(Thread.currentThread());
		Class11 class11 = class240.aClass11_2285;
		int i_585_ = i_580_ - i;
		int i_586_ = i_581_ - i_579_;
		int i_587_ = i_585_ >= 0 ? i_585_ : -i_585_;
		int i_588_ = i_586_ >= 0 ? i_586_ : -i_586_;
		int i_589_ = i_587_;
		if (i_589_ < i_588_)
			i_589_ = i_588_;
		if (i_589_ != 0) {
			int i_590_ = (i_585_ << 16) / i_589_;
			int i_591_ = (i_586_ << 16) / i_589_;
			i_585_ += i_590_ >> 16;
			i_586_ += i_591_ >> 16;
			if (i_591_ <= i_590_)
				i_590_ = -i_590_;
			else
				i_591_ = -i_591_;
			int i_592_ = i_583_ * i_591_ >> 17;
			int i_593_ = i_583_ * i_591_ + 1 >> 17;
			int i_594_ = i_583_ * i_590_ >> 17;
			int i_595_ = i_583_ * i_590_ + 1 >> 17;
			i -= class11.method210();
			i_579_ -= class11.method213();
			int i_596_ = i + i_592_;
			int i_597_ = i - i_593_;
			int i_598_ = i + i_585_ - i_593_;
			int i_599_ = i + i_585_ + i_592_;
			int i_600_ = i_579_ + i_594_;
			int i_601_ = i_579_ - i_595_;
			int i_602_ = i_579_ + i_586_ - i_595_;
			int i_603_ = i_579_ + i_586_ + i_594_;
			if (i_584_ == 0)
				class11.anInt108 = 0;
			else if (i_584_ == 1)
				class11.anInt108 = 255 - (i_582_ >>> 24);
			else
				throw new IllegalArgumentException();
			C(false);
			class11.aBoolean103 = (i_596_ < 0 || i_596_ > class11.anInt107 || i_597_ < 0 || i_597_ > class11.anInt107
					|| i_598_ < 0 || i_598_ > class11.anInt107);
			class11.method206((float) i_600_, (float) i_601_, (float) i_602_, (float) i_596_, (float) i_597_,
					(float) i_598_, 100.0F, 100.0F, 100.0F, i_582_);
			class11.aBoolean103 = (i_596_ < 0 || i_596_ > class11.anInt107 || i_598_ < 0 || i_598_ > class11.anInt107
					|| i_599_ < 0 || i_599_ > class11.anInt107);
			class11.method206((float) i_600_, (float) i_602_, (float) i_603_, (float) i_596_, (float) i_598_,
					(float) i_599_, 100.0F, 100.0F, 100.0F, i_582_);
			C(true);
		}
	}

	public void a(Canvas canvas) {
		if (aCanvas4065 == canvas)
			b((Canvas) null);
		Class296_Sub29 class296_sub29 = ((Class296_Sub29) aClass263_4068.get((long) canvas.hashCode()));
		if (class296_sub29 != null)
			class296_sub29.unlink();
	}

	ha_Sub2(Canvas canvas, d var_d, int i, int i_604_) {
		this(var_d);
		try {
			b(canvas, i, i_604_);
			b(canvas);
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			this.method1091((byte) -122);
			throw new RuntimeException("");
		}
	}

	public void za(int i, int i_605_, int i_606_, int i_607_, int i_608_) {
		if (i_606_ < 0)
			i_606_ = -i_606_;
		int i_609_ = i_605_ - i_606_;
		if (i_609_ < anInt4085)
			i_609_ = anInt4085;
		int i_610_ = i_605_ + i_606_ + 1;
		if (i_610_ > anInt4078)
			i_610_ = anInt4078;
		int i_611_ = i_609_;
		int i_612_ = i_606_ * i_606_;
		int i_613_ = 0;
		int i_614_ = i_605_ - i_611_;
		int i_615_ = i_614_ * i_614_;
		int i_616_ = i_615_ - i_614_;
		if (i_605_ > i_610_)
			i_605_ = i_610_;
		int i_617_ = i_607_ >>> 24;
		if (i_608_ == 0 || i_608_ == 1 && i_617_ == 255) {
			while (i_611_ < i_605_) {
				for (/**/; i_616_ <= i_612_ || i_615_ <= i_612_; i_616_ += i_613_++ + i_613_)
					i_615_ += i_613_ + i_613_;
				int i_618_ = i - i_613_ + 1;
				if (i_618_ < anInt4079)
					i_618_ = anInt4079;
				int i_619_ = i + i_613_;
				if (i_619_ > anInt4104)
					i_619_ = anInt4104;
				int i_620_ = i_618_ + i_611_ * anInt4084;
				for (int i_621_ = i_618_; i_621_ < i_619_; i_621_++)
					anIntArray4108[i_620_++] = i_607_;
				i_611_++;
				i_615_ -= i_614_-- + i_614_;
				i_616_ -= i_614_ + i_614_;
			}
			i_613_ = i_606_;
			i_614_ = i_611_ - i_605_;
			i_616_ = i_614_ * i_614_ + i_612_;
			i_615_ = i_616_ - i_613_;
			i_616_ -= i_614_;
			while (i_611_ < i_610_) {
				while (i_616_ > i_612_) {
					if (i_615_ <= i_612_)
						break;
					i_616_ -= i_613_-- + i_613_;
					i_615_ -= i_613_ + i_613_;
				}
				int i_622_ = i - i_613_;
				if (i_622_ < anInt4079)
					i_622_ = anInt4079;
				int i_623_ = i + i_613_;
				if (i_623_ > anInt4104 - 1)
					i_623_ = anInt4104 - 1;
				int i_624_ = i_622_ + i_611_ * anInt4084;
				for (int i_625_ = i_622_; i_625_ <= i_623_; i_625_++)
					anIntArray4108[i_624_++] = i_607_;
				i_611_++;
				i_616_ += i_614_ + i_614_;
				i_615_ += i_614_++ + i_614_;
			}
		} else if (i_608_ == 1) {
			i_607_ = (((i_607_ & 0xff00ff) * i_617_ >> 8 & 0xff00ff) + ((i_607_ & 0xff00) * i_617_ >> 8 & 0xff00)
					+ (i_617_ << 24));
			int i_626_ = 256 - i_617_;
			while (i_611_ < i_605_) {
				for (/**/; i_616_ <= i_612_ || i_615_ <= i_612_; i_616_ += i_613_++ + i_613_)
					i_615_ += i_613_ + i_613_;
				int i_627_ = i - i_613_ + 1;
				if (i_627_ < anInt4079)
					i_627_ = anInt4079;
				int i_628_ = i + i_613_;
				if (i_628_ > anInt4104)
					i_628_ = anInt4104;
				int i_629_ = i_627_ + i_611_ * anInt4084;
				for (int i_630_ = i_627_; i_630_ < i_628_; i_630_++) {
					int i_631_ = anIntArray4108[i_629_];
					i_631_ = (((i_631_ & 0xff00ff) * i_626_ >> 8 & 0xff00ff)
							+ ((i_631_ & 0xff00) * i_626_ >> 8 & 0xff00));
					anIntArray4108[i_629_++] = i_607_ + i_631_;
				}
				i_611_++;
				i_615_ -= i_614_-- + i_614_;
				i_616_ -= i_614_ + i_614_;
			}
			i_613_ = i_606_;
			i_614_ = -i_614_;
			i_616_ = i_614_ * i_614_ + i_612_;
			i_615_ = i_616_ - i_613_;
			i_616_ -= i_614_;
			while (i_611_ < i_610_) {
				while (i_616_ > i_612_) {
					if (i_615_ <= i_612_)
						break;
					i_616_ -= i_613_-- + i_613_;
					i_615_ -= i_613_ + i_613_;
				}
				int i_632_ = i - i_613_;
				if (i_632_ < anInt4079)
					i_632_ = anInt4079;
				int i_633_ = i + i_613_;
				if (i_633_ > anInt4104 - 1)
					i_633_ = anInt4104 - 1;
				int i_634_ = i_632_ + i_611_ * anInt4084;
				for (int i_635_ = i_632_; i_635_ <= i_633_; i_635_++) {
					int i_636_ = anIntArray4108[i_634_];
					i_636_ = (((i_636_ & 0xff00ff) * i_626_ >> 8 & 0xff00ff)
							+ ((i_636_ & 0xff00) * i_626_ >> 8 & 0xff00));
					anIntArray4108[i_634_++] = i_607_ + i_636_;
				}
				i_611_++;
				i_616_ += i_614_ + i_614_;
				i_615_ += i_614_++ + i_614_;
			}
		} else if (i_608_ == 2) {
			while (i_611_ < i_605_) {
				for (/**/; i_616_ <= i_612_ || i_615_ <= i_612_; i_616_ += i_613_++ + i_613_)
					i_615_ += i_613_ + i_613_;
				int i_637_ = i - i_613_ + 1;
				if (i_637_ < anInt4079)
					i_637_ = anInt4079;
				int i_638_ = i + i_613_;
				if (i_638_ > anInt4104)
					i_638_ = anInt4104;
				int i_639_ = i_637_ + i_611_ * anInt4084;
				for (int i_640_ = i_637_; i_640_ < i_638_; i_640_++) {
					int i_641_ = anIntArray4108[i_639_];
					int i_642_ = i_607_ + i_641_;
					int i_643_ = (i_607_ & 0xff00ff) + (i_641_ & 0xff00ff);
					i_641_ = (i_643_ & 0x1000100) + (i_642_ - i_643_ & 0x10000);
					anIntArray4108[i_639_++] = i_642_ - i_641_ | i_641_ - (i_641_ >>> 8);
				}
				i_611_++;
				i_615_ -= i_614_-- + i_614_;
				i_616_ -= i_614_ + i_614_;
			}
			i_613_ = i_606_;
			i_614_ = -i_614_;
			i_616_ = i_614_ * i_614_ + i_612_;
			i_615_ = i_616_ - i_613_;
			i_616_ -= i_614_;
			while (i_611_ < i_610_) {
				while (i_616_ > i_612_) {
					if (i_615_ <= i_612_)
						break;
					i_616_ -= i_613_-- + i_613_;
					i_615_ -= i_613_ + i_613_;
				}
				int i_644_ = i - i_613_;
				if (i_644_ < anInt4079)
					i_644_ = anInt4079;
				int i_645_ = i + i_613_;
				if (i_645_ > anInt4104 - 1)
					i_645_ = anInt4104 - 1;
				int i_646_ = i_644_ + i_611_ * anInt4084;
				for (int i_647_ = i_644_; i_647_ <= i_645_; i_647_++) {
					int i_648_ = anIntArray4108[i_646_];
					int i_649_ = i_607_ + i_648_;
					int i_650_ = (i_607_ & 0xff00ff) + (i_648_ & 0xff00ff);
					i_648_ = (i_650_ & 0x1000100) + (i_649_ - i_650_ & 0x10000);
					anIntArray4108[i_646_++] = i_649_ - i_648_ | i_648_ - (i_648_ >>> 8);
				}
				i_611_++;
				i_616_ += i_614_ + i_614_;
				i_615_ += i_614_++ + i_614_;
			}
		} else
			throw new IllegalArgumentException();
	}
}
