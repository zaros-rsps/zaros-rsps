package net.zaros.client;
/* Class193 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Random;

final class Class193 {
	private Class20 aClass20_1948;
	private int anInt1949;
	private static int[] anIntArray1950;
	private Class20 aClass20_1951;
	private int anInt1952 = 100;
	private int[] anIntArray1953;
	private int[] anIntArray1954;
	private Class114 aClass114_1955;
	int anInt1956;
	private int[] anIntArray1957 = new int[5];
	private Class20 aClass20_1958;
	private Class20 aClass20_1959;
	private Class20 aClass20_1960;
	private Class20 aClass20_1961;
	private Class20 aClass20_1962;
	private Class20 aClass20_1963;
	private static int[] anIntArray1964 = new int[32768];
	private static int[] anIntArray1965;
	int anInt1966;
	private static int[] anIntArray1967;
	private static int[] anIntArray1968;
	private static int[] anIntArray1969;
	private static int[] anIntArray1970;
	private static int[] anIntArray1971;
	private Class20 aClass20_1972;

	final void method1924(Packet class296_sub17) {
		aClass20_1962 = new Class20();
		aClass20_1962.method286(class296_sub17);
		aClass20_1948 = new Class20();
		aClass20_1948.method286(class296_sub17);
		int i = class296_sub17.g1();
		if (i != 0) {
			class296_sub17.pos--;
			aClass20_1961 = new Class20();
			aClass20_1961.method286(class296_sub17);
			aClass20_1963 = new Class20();
			aClass20_1963.method286(class296_sub17);
		}
		i = class296_sub17.g1();
		if (i != 0) {
			class296_sub17.pos--;
			aClass20_1951 = new Class20();
			aClass20_1951.method286(class296_sub17);
			aClass20_1959 = new Class20();
			aClass20_1959.method286(class296_sub17);
		}
		i = class296_sub17.g1();
		if (i != 0) {
			class296_sub17.pos--;
			aClass20_1972 = new Class20();
			aClass20_1972.method286(class296_sub17);
			aClass20_1958 = new Class20();
			aClass20_1958.method286(class296_sub17);
		}
		for (int i_0_ = 0; i_0_ < 10; i_0_++) {
			int i_1_ = class296_sub17.readSmart();
			if (i_1_ == 0)
				break;
			anIntArray1957[i_0_] = i_1_;
			anIntArray1954[i_0_] = class296_sub17.gSmart1or2s();
			anIntArray1953[i_0_] = class296_sub17.readSmart();
		}
		anInt1949 = class296_sub17.readSmart();
		anInt1952 = class296_sub17.readSmart();
		anInt1966 = class296_sub17.g2();
		anInt1956 = class296_sub17.g2();
		aClass114_1955 = new Class114();
		aClass20_1960 = new Class20();
		aClass114_1955.method1001(class296_sub17, aClass20_1960);
	}

	public static void method1925() {
		anIntArray1950 = null;
		anIntArray1964 = null;
		anIntArray1965 = null;
		anIntArray1968 = null;
		anIntArray1970 = null;
		anIntArray1967 = null;
		anIntArray1969 = null;
		anIntArray1971 = null;
	}

	private final int method1926(int i, int i_2_, int i_3_) {
		if (i_3_ == 1) {
			if ((i & 0x7fff) < 16384)
				return i_2_;
			return -i_2_;
		}
		if (i_3_ == 2)
			return anIntArray1965[i & 0x7fff] * i_2_ >> 14;
		if (i_3_ == 3)
			return ((i & 0x7fff) * i_2_ >> 14) - i_2_;
		if (i_3_ == 4)
			return anIntArray1964[i / 2607 & 0x7fff] * i_2_;
		return 0;
	}

	final int[] method1927(int i, int i_4_) {
		Class291.method2404(anIntArray1950, 0, i);
		if (i_4_ < 10)
			return anIntArray1950;
		double d = (double) i / ((double) i_4_ + 0.0);
		aClass20_1962.method287();
		aClass20_1948.method287();
		int i_5_ = 0;
		int i_6_ = 0;
		int i_7_ = 0;
		if (aClass20_1961 != null) {
			aClass20_1961.method287();
			aClass20_1963.method287();
			i_5_ = (int) ((double) (aClass20_1961.anInt240 - aClass20_1961.anInt237) * 32.768 / d);
			i_6_ = (int) ((double) aClass20_1961.anInt237 * 32.768 / d);
		}
		int i_8_ = 0;
		int i_9_ = 0;
		int i_10_ = 0;
		if (aClass20_1951 != null) {
			aClass20_1951.method287();
			aClass20_1959.method287();
			i_8_ = (int) ((double) (aClass20_1951.anInt240 - aClass20_1951.anInt237) * 32.768 / d);
			i_9_ = (int) ((double) aClass20_1951.anInt237 * 32.768 / d);
		}
		for (int i_11_ = 0; i_11_ < 5; i_11_++) {
			if (anIntArray1957[i_11_] != 0) {
				anIntArray1968[i_11_] = 0;
				anIntArray1970[i_11_] = (int) ((double) anIntArray1953[i_11_] * d);
				anIntArray1967[i_11_] = (anIntArray1957[i_11_] << 14) / 100;
				anIntArray1969[i_11_] = (int) ((double) (aClass20_1962.anInt240 - aClass20_1962.anInt237) * 32.768 * Math.pow(1.0057929410678534, (double) anIntArray1954[i_11_]) / d);
				anIntArray1971[i_11_] = (int) ((double) aClass20_1962.anInt237 * 32.768 / d);
			}
		}
		for (int i_12_ = 0; i_12_ < i; i_12_++) {
			int i_13_ = aClass20_1962.method284(i);
			int i_14_ = aClass20_1948.method284(i);
			if (aClass20_1961 != null) {
				int i_15_ = aClass20_1961.method284(i);
				int i_16_ = aClass20_1963.method284(i);
				i_13_ += method1926(i_7_, i_16_, aClass20_1961.anInt238) >> 1;
				i_7_ += (i_15_ * i_5_ >> 16) + i_6_;
			}
			if (aClass20_1951 != null) {
				int i_17_ = aClass20_1951.method284(i);
				int i_18_ = aClass20_1959.method284(i);
				i_14_ = i_14_ * ((method1926(i_10_, i_18_, aClass20_1951.anInt238) >> 1) + 32768) >> 15;
				i_10_ += (i_17_ * i_8_ >> 16) + i_9_;
			}
			for (int i_19_ = 0; i_19_ < 5; i_19_++) {
				if (anIntArray1957[i_19_] != 0) {
					int i_20_ = i_12_ + anIntArray1970[i_19_];
					if (i_20_ < i) {
						anIntArray1950[i_20_] += method1926(anIntArray1968[i_19_], i_14_ * anIntArray1967[i_19_] >> 15, aClass20_1962.anInt238);
						anIntArray1968[i_19_] += ((i_13_ * anIntArray1969[i_19_] >> 16) + anIntArray1971[i_19_]);
					}
				}
			}
		}
		if (aClass20_1972 != null) {
			aClass20_1972.method287();
			aClass20_1958.method287();
			int i_21_ = 0;
			boolean bool = false;
			boolean bool_22_ = true;
			for (int i_23_ = 0; i_23_ < i; i_23_++) {
				int i_24_ = aClass20_1972.method284(i);
				int i_25_ = aClass20_1958.method284(i);
				int i_26_;
				if (bool_22_)
					i_26_ = aClass20_1972.anInt237 + (((aClass20_1972.anInt240 - aClass20_1972.anInt237) * i_24_) >> 8);
				else
					i_26_ = aClass20_1972.anInt237 + (((aClass20_1972.anInt240 - aClass20_1972.anInt237) * i_25_) >> 8);
				i_21_ += 256;
				if (i_21_ >= i_26_) {
					i_21_ = 0;
					bool_22_ = !bool_22_;
				}
				if (bool_22_)
					anIntArray1950[i_23_] = 0;
			}
		}
		if (anInt1949 > 0 && anInt1952 > 0) {
			int i_27_ = (int) ((double) anInt1949 * d);
			for (int i_28_ = i_27_; i_28_ < i; i_28_++)
				anIntArray1950[i_28_] += anIntArray1950[i_28_ - i_27_] * anInt1952 / 100;
		}
		if (aClass114_1955.anIntArray1179[0] > 0 || aClass114_1955.anIntArray1179[1] > 0) {
			aClass20_1960.method287();
			int i_29_ = aClass20_1960.method284(i + 1);
			int i_30_ = aClass114_1955.method1003(0, (float) i_29_ / 65536.0F);
			int i_31_ = aClass114_1955.method1003(1, (float) i_29_ / 65536.0F);
			if (i >= i_30_ + i_31_) {
				int i_32_ = 0;
				int i_33_ = i_31_;
				if (i_33_ > i - i_30_)
					i_33_ = i - i_30_;
				for (/**/; i_32_ < i_33_; i_32_++) {
					int i_34_ = (int) (((long) anIntArray1950[i_32_ + i_30_] * (long) Class114.anInt1174) >> 16);
					for (int i_35_ = 0; i_35_ < i_30_; i_35_++)
						i_34_ += (int) (((long) (anIntArray1950[i_32_ + i_30_ - 1 - i_35_]) * (long) (Class114.anIntArrayArray1180[0][i_35_])) >> 16);
					for (int i_36_ = 0; i_36_ < i_32_; i_36_++)
						i_34_ -= (int) (((long) anIntArray1950[i_32_ - 1 - i_36_] * (long) (Class114.anIntArrayArray1180[1][i_36_])) >> 16);
					anIntArray1950[i_32_] = i_34_;
					i_29_ = aClass20_1960.method284(i + 1);
				}
				i_33_ = 128;
				for (;;) {
					if (i_33_ > i - i_30_)
						i_33_ = i - i_30_;
					for (/**/; i_32_ < i_33_; i_32_++) {
						int i_37_ = (int) (((long) anIntArray1950[i_32_ + i_30_] * (long) Class114.anInt1174) >> 16);
						for (int i_38_ = 0; i_38_ < i_30_; i_38_++)
							i_37_ += (int) (((long) (anIntArray1950[i_32_ + i_30_ - 1 - i_38_]) * (long) (Class114.anIntArrayArray1180[0][i_38_])) >> 16);
						for (int i_39_ = 0; i_39_ < i_31_; i_39_++)
							i_37_ -= (int) (((long) (anIntArray1950[i_32_ - 1 - i_39_]) * (long) (Class114.anIntArrayArray1180[1][i_39_])) >> 16);
						anIntArray1950[i_32_] = i_37_;
						i_29_ = aClass20_1960.method284(i + 1);
					}
					if (i_32_ >= i - i_30_)
						break;
					i_30_ = aClass114_1955.method1003(0, (float) i_29_ / 65536.0F);
					i_31_ = aClass114_1955.method1003(1, (float) i_29_ / 65536.0F);
					i_33_ += 128;
				}
				for (/**/; i_32_ < i; i_32_++) {
					int i_40_ = 0;
					for (int i_41_ = i_32_ + i_30_ - i; i_41_ < i_30_; i_41_++)
						i_40_ += (int) (((long) (anIntArray1950[i_32_ + i_30_ - 1 - i_41_]) * (long) (Class114.anIntArrayArray1180[0][i_41_])) >> 16);
					for (int i_42_ = 0; i_42_ < i_31_; i_42_++)
						i_40_ -= (int) (((long) anIntArray1950[i_32_ - 1 - i_42_] * (long) (Class114.anIntArrayArray1180[1][i_42_])) >> 16);
					anIntArray1950[i_32_] = i_40_;
					i_29_ = aClass20_1960.method284(i + 1);
				}
			}
		}
		for (int i_43_ = 0; i_43_ < i; i_43_++) {
			if (anIntArray1950[i_43_] < -32768)
				anIntArray1950[i_43_] = -32768;
			if (anIntArray1950[i_43_] > 32767)
				anIntArray1950[i_43_] = 32767;
		}
		return anIntArray1950;
	}

	public Class193() {
		anIntArray1954 = new int[5];
		anInt1949 = 0;
		anInt1956 = 0;
		anIntArray1953 = new int[5];
		anInt1966 = 500;
	}

	static {
		Random random = new Random(0L);
		for (int i = 0; i < 32768; i++)
			anIntArray1964[i] = (random.nextInt() & 0x2) - 1;
		anIntArray1965 = new int[32768];
		for (int i = 0; i < 32768; i++)
			anIntArray1965[i] = (int) (Math.sin((double) i / 5215.1903) * 16384.0);
		anIntArray1950 = new int[220500];
		anIntArray1968 = new int[5];
		anIntArray1967 = new int[5];
		anIntArray1971 = new int[5];
		anIntArray1969 = new int[5];
		anIntArray1970 = new int[5];
	}
}
