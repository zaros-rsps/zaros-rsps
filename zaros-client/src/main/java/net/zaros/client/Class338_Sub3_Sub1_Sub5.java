package net.zaros.client;

/* Class338_Sub3_Sub1_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub3_Sub1_Sub5 extends Class338_Sub3_Sub1 {
	private Class338_Sub1 aClass338_Sub1_6837;
	private int anInt6838;
	private int anInt6839;
	private boolean aBoolean6840 = true;
	private Animator aClass44_6841;
	private int anInt6842;
	static int[] anIntArray6843 = null;
	static float aFloat6844;
	private int anInt6845;

	final boolean method3475(int i, int i_0_, ha var_ha, int i_1_) {
		if (i_0_ > -48)
			method3467(-62, -81, null, true, -58, -127, null);
		return false;
	}

	final void method3460(int i, ha var_ha) {
		Model class178 = method3546(anInt6842, var_ha, true, 0);
		int i_2_ = -2 % ((-41 - i) / 62);
		if (class178 != null) {
			Class373 class373 = var_ha.f();
			class373.method3902(tileX, anInt5213, tileY);
			method3547(var_ha, class373, 0, class178);
		}
	}

	final void method3545(byte i) {
		if (i <= 88)
			method3545((byte) -67);
		if (aClass338_Sub1_6837 != null)
			aClass338_Sub1_6837.method3439();
	}

	private final Model method3546(int i, ha var_ha, boolean bool, int i_3_) {
		Graphic class23 = Class157.graphicsLoader.getGraphic(i);
		s var_s = Class244.aSArray2320[z];
		s var_s_4_ = aByte5203 < 3 ? Class244.aSArray2320[aByte5203 + 1] : null;
		if (bool != true)
			method3469(-50);
		if (aClass44_6841 == null || aClass44_6841.method546(2))
			return class23.method295(tileX, -22, var_s, null, anInt5213, i_3_, (byte) 2, var_ha, var_s_4_, tileY, true);
		return class23.method295(tileX, 117, var_s, aClass44_6841, anInt5213, i_3_, (byte) 2, var_ha, var_s_4_, tileY, true);
	}

	Class338_Sub3_Sub1_Sub5(int i, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_, boolean bool) {
		super(i_6_, i_7_, i_8_, i_9_, i_10_, i_11_, i_12_, i_13_, i_14_, false, (byte) 0);
		anInt6839 = 0;
		anInt6838 = 0;
		anInt6845 = 0;
		anInt6842 = i;
		anInt6839 = i_15_;
		Graphic class23 = Class157.graphicsLoader.getGraphic(anInt6842);
		int i_16_ = class23.anInt264;
		if (i_16_ != -1) {
			aClass44_6841 = new Class44_Sub1(this, false);
			int i_17_ = class23.aBoolean267 ? 0 : 2;
			if (bool)
				i_17_ = 1;
			aClass44_6841.method548(i_5_, i_17_, i_16_, false, 21847);
		}
	}

	final boolean method3468(int i) {
		if (i > -29)
			method3550(42);
		return false;
	}

	final int method3466(byte i) {
		if (i < 77)
			return 13;
		return anInt6845;
	}

	final void method3472(byte i) {
		int i_18_ = -115 / ((i + 56) / 38);
		throw new IllegalStateException();
	}

	private final void method3547(ha var_ha, Class373 class373, int i, Model class178) {
		class178.method1718(class373);
		if (i != 0)
			anInt6838 = -64;
		EmissiveTriangle[] class89s = class178.method1735();
		EffectiveVertex[] class232s = class178.method1729();
		if ((aClass338_Sub1_6837 == null || aClass338_Sub1_6837.aBoolean5177) && (class89s != null || class232s != null))
			aClass338_Sub1_6837 = Class338_Sub1.method3442(Class29.anInt307, true);
		if (aClass338_Sub1_6837 != null) {
			aClass338_Sub1_6837.method3452(var_ha, (long) Class29.anInt307, class89s, class232s, false);
			aClass338_Sub1_6837.method3450(z, aShort6560, aShort6559, aShort6564, aShort6558);
		}
	}

	final boolean method3459(int i) {
		if (i != 0)
			return true;
		return false;
	}

	final boolean method3469(int i) {
		if (i <= 82)
			aBoolean6840 = true;
		return aBoolean6840;
	}

	static final void method3548(byte i, InterfaceComponent class51) {
		if (Class127.aBoolean1304) {
			if (class51.anObjectArray494 != null) {
				InterfaceComponent class51_19_ = Class103.method894(0, Class366_Sub4.anInt5375, Class180.anInt1857);
				if (class51_19_ != null) {
					CS2Call class296_sub46 = new CS2Call();
					class296_sub46.callerInterface = class51;
					class296_sub46.aClass51_4962 = class51_19_;
					class296_sub46.callArgs = class51.anObjectArray494;
					CS2Executor.runCS2(class296_sub46);
				}
			}
			if (i != 85)
				anIntArray6843 = null;
			Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 124, Connection.aClass311_2046);
			class296_sub1.out.writeLEShortA(class51.clickedItem);
			class296_sub1.out.writeLEShortA(class51.anInt592);
			class296_sub1.out.writeLEShortA(Class180.anInt1857);
			class296_sub1.out.writeLEInt(class51.uid);
			class296_sub1.out.writeLEInt(Class366_Sub4.anInt5375);
			class296_sub1.out.writeShortA(Class69.anInt3689);
			Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
		}
	}

	final Class338_Sub2 method3473(ha var_ha, byte i) {
		Model class178 = method3546(anInt6842, var_ha, true, 0x800 | (anInt6839 == 0 ? 0 : 5));
		if (class178 == null)
			return null;
		if (anInt6839 != 0)
			class178.a(anInt6839 * 2048);
		Class373 class373 = var_ha.f();
		class373.method3902(tileX, anInt5213, tileY);
		if (i > -84)
			anInt6838 = -48;
		method3547(var_ha, class373, 0, class178);
		Class338_Sub2 class338_sub2 = Class144.method1481(1, true, false);
		if (Class296_Sub39_Sub10.aBoolean6177)
			class178.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
		else
			class178.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
		if (aClass338_Sub1_6837 != null) {
			Class390 class390 = aClass338_Sub1_6837.method3444();
			if (!Class296_Sub39_Sub10.aBoolean6177)
				var_ha.a(class390);
			else
				var_ha.a(class390, ModeWhat.anInt1192);
		}
		aBoolean6840 = class178.F();
		anInt6845 = class178.fa();
		anInt6838 = class178.ma();
		return class338_sub2;
	}

	final void method3467(int i, int i_20_, Class338_Sub3 class338_sub3, boolean bool, int i_21_, int i_22_, ha var_ha) {
		int i_23_ = -65 % ((20 - i_22_) / 48);
		throw new IllegalStateException();
	}

	public static void method3549(byte i) {
		anIntArray6843 = null;
		if (i != -26)
			anIntArray6843 = null;
	}

	final boolean method3550(int i) {
		if (i != 30690)
			anIntArray6843 = null;
		if (aClass44_6841 == null || aClass44_6841.method567(1))
			return false;
		return true;
	}

	final Class96 method3461(ha var_ha, int i) {
		int i_24_ = 26 / ((79 - i) / 44);
		return null;
	}

	final int method3462(byte i) {
		if (i != 28)
			method3462((byte) -115);
		return anInt6838;
	}

	protected final void finalize() {
		if (aClass338_Sub1_6837 != null)
			aClass338_Sub1_6837.method3439();
	}

	final void method3551(int i, int i_25_) {
		int i_26_ = -125 % ((60 - i) / 53);
		if (aClass44_6841 != null && !aClass44_6841.method546(-121))
			aClass44_6841.method559(i_25_, (byte) 124);
	}

	final boolean method3552(byte i) {
		if (i < 121)
			return false;
		if (aClass44_6841 != null && !aClass44_6841.method546(-124))
			return false;
		return true;
	}
}
