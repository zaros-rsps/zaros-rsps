package net.zaros.client;

/* Class296_Sub39_Sub16 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Texture extends Queuable {
	public TextureOperation aClass296_Sub51_6227;
	public TextureOperation[] aClass296_Sub51Array6228;
	public int[] texture_sprites_hdr;
	public TextureOperation aClass296_Sub51_6230;
	public int[] texture_sprites_diffuse;
	static Class209 aClass209_6232 = new Class209(33);
	public TextureOperation aClass296_Sub51_6233;
	public final int textureid;

	final boolean is_ready(d var_d, Js5 js5) {
		if (Class41_Sub6.texture_group < 0) {
			for (int i_0_ = 0; i_0_ < texture_sprites_diffuse.length; i_0_++) {
				if (!js5.hasEntryBuffer(texture_sprites_diffuse[i_0_])) {
					return false;
				}
			}
		} else {
			for (int i_1_ = 0; texture_sprites_diffuse.length > i_1_; i_1_++) {
				if (!js5.fileExists(Class41_Sub6.texture_group, texture_sprites_diffuse[i_1_])) {
					return false;
				}
			}
		}
		for (int i_2_ = 0; i_2_ < texture_sprites_hdr.length; i_2_++) {
			if (!var_d.is_ready(texture_sprites_hdr[i_2_])) {
				return false;
			}
		}
		return true;
	}

	public static void method2888(int i) {
		if (i != 29462) {
			aClass209_6232 = null;
		}
		aClass209_6232 = null;
	}

	final int[] method2889(d var_d, boolean bool, double d, int i, int height, boolean bool_4_, Js5 class138, int width) {
		StaticMethods.aClass138_294 = class138;
		Class134.aD1388 = var_d;
		for (int i_6_ = 0; aClass296_Sub51Array6228.length > i_6_; i_6_++) {
			aClass296_Sub51Array6228[i_6_].method3070(-256, width, height);
		}
		Class249.method2193(255, d);
		AdvancedMemoryCache.method998(width, -1, height);
		int[] is = new int[width * height];
		int i_7_;
		int i_8_;
		int i_9_;
		if (!bool) {
			i_9_ = 0;
			i_8_ = width;
			i_7_ = 1;
		} else {
			i_7_ = -1;
			i_8_ = -1;
			i_9_ = width - 1;
		}
		int i_10_ = 0;
		for (int i_11_ = 0; i_11_ < height; i_11_++) {
			if (bool_4_) {
				i_10_ = i_11_;
			}
			int[] is_12_;
			int[] is_13_;
			int[] is_14_;
			if (aClass296_Sub51_6230.monochromatic) {
				int[] is_15_ = aClass296_Sub51_6230.get_monochrome_output(i + 29699, i_11_);
				is_12_ = is_15_;
				is_13_ = is_15_;
				is_14_ = is_15_;
			} else {
				int[][] is_16_ = aClass296_Sub51_6230.get_colour_output(i_11_, i ^ ~0x30d7);
				is_12_ = is_16_[1];
				is_13_ = is_16_[0];
				is_14_ = is_16_[2];
			}
			for (int i_17_ = i_9_; i_17_ != i_8_; i_17_ += i_7_) {
				int i_18_ = is_13_[i_17_] >> 4;
				if (i_18_ > 255) {
					i_18_ = 255;
				}
				if (i_18_ < 0) {
					i_18_ = 0;
				}
				int i_19_ = is_12_[i_17_] >> 4;
				if (i_19_ > 255) {
					i_19_ = 255;
				}
				if (i_19_ < 0) {
					i_19_ = 0;
				}
				int i_20_ = is_14_[i_17_] >> 4;
				if (i_20_ > 255) {
					i_20_ = 255;
				}
				if (i_20_ < 0) {
					i_20_ = 0;
				}
				i_18_ = Class363.anIntArray3111[i_18_];
				i_19_ = Class363.anIntArray3111[i_19_];
				i_20_ = Class363.anIntArray3111[i_20_];
				int i_21_ = (i_19_ << 8) + (i_18_ << 16) + i_20_;
				if (i_21_ != 0) {
					i_21_ |= ~0xffffff;
				}
				is[i_10_++] = i_21_;
				if (bool_4_) {
					i_10_ += width - 1;
				}
			}
		}
		for (int i_22_ = 0; i_22_ < aClass296_Sub51Array6228.length; i_22_++) {
			aClass296_Sub51Array6228[i_22_].method3073(i + 29583);
		}
		return is;
	}

	final int[] method2890(boolean bool, d var_d, Js5 class138, int i, double d, int i_23_, int i_24_) {
		Class134.aD1388 = var_d;
		StaticMethods.aClass138_294 = class138;
		for (int i_25_ = 0; aClass296_Sub51Array6228.length > i_25_; i_25_++) {
			aClass296_Sub51Array6228[i_25_].method3070(-256, i, i_24_);
		}
		Class249.method2193(i_23_ ^ 0xff, d);
		AdvancedMemoryCache.method998(i, -1, i_24_);
		int[] is = new int[i * i_24_];
		int i_26_ = 0;
		for (int i_27_ = i_23_; i_24_ > i_27_; i_27_++) {
			int[] is_28_;
			int[] is_29_;
			int[] is_30_;
			if (aClass296_Sub51_6230.monochromatic) {
				int[] is_31_ = aClass296_Sub51_6230.get_monochrome_output(i_23_, i_27_);
				is_28_ = is_31_;
				is_29_ = is_31_;
				is_30_ = is_31_;
			} else {
				int[][] is_32_ = aClass296_Sub51_6230.get_colour_output(i_27_, i_23_ + 17621);
				is_28_ = is_32_[0];
				is_29_ = is_32_[1];
				is_30_ = is_32_[2];
			}
			int[] is_33_;
			if (!aClass296_Sub51_6227.monochromatic) {
				is_33_ = aClass296_Sub51_6227.get_colour_output(i_27_, 17621)[0];
			} else {
				is_33_ = aClass296_Sub51_6227.get_monochrome_output(0, i_27_);
			}
			if (bool) {
				i_26_ = i_27_;
			}
			for (int i_34_ = i - 1; i_34_ >= 0; i_34_--) {
				int i_35_ = is_28_[i_34_] >> 4;
				if (i_35_ > 255) {
					i_35_ = 255;
				}
				if (i_35_ < 0) {
					i_35_ = 0;
				}
				int i_36_ = is_29_[i_34_] >> 4;
				if (i_36_ > 255) {
					i_36_ = 255;
				}
				if (i_36_ < 0) {
					i_36_ = 0;
				}
				int i_37_ = is_30_[i_34_] >> 4;
				if (i_37_ > 255) {
					i_37_ = 255;
				}
				i_35_ = Class363.anIntArray3111[i_35_];
				if (i_37_ < 0) {
					i_37_ = 0;
				}
				i_36_ = Class363.anIntArray3111[i_36_];
				i_37_ = Class363.anIntArray3111[i_37_];
				int i_38_;
				if (i_35_ != 0 || i_36_ != 0 || i_37_ != 0) {
					i_38_ = is_33_[i_34_] >> 4;
					if (i_38_ > 255) {
						i_38_ = 255;
					}
					if (i_38_ < 0) {
						i_38_ = 0;
					}
				} else {
					i_38_ = 0;
				}
				is[i_26_++] = (i_36_ << 8) + (i_35_ << 16) + (i_38_ << 24) + i_37_;
				if (bool) {
					i_26_ += i - 1;
				}
			}
		}
		for (int i_39_ = 0; aClass296_Sub51Array6228.length > i_39_; i_39_++) {
			aClass296_Sub51Array6228[i_39_].method3073(i_23_ ^ ~0x70);
		}
		return is;
	}

	public Texture(int textureid) {
		this.textureid = textureid;
		texture_sprites_hdr = new int[0];
		texture_sprites_diffuse = new int[0];
		aClass296_Sub51_6233 = new Class296_Sub51_Sub9(0);
		aClass296_Sub51_6233.anInt5032 = 1;
		aClass296_Sub51_6230 = new Class296_Sub51_Sub9();
		aClass296_Sub51_6230.anInt5032 = 1;
		aClass296_Sub51_6227 = new Class296_Sub51_Sub9();
		aClass296_Sub51Array6228 = new TextureOperation[] { aClass296_Sub51_6230, aClass296_Sub51_6227, aClass296_Sub51_6233 };
		aClass296_Sub51_6227.anInt5032 = 1;
	}

	final float[] method2891(int i, d var_d, Js5 class138, byte i_40_, boolean bool, int i_41_) {
		Class134.aD1388 = var_d;
		StaticMethods.aClass138_294 = class138;
		for (int i_42_ = 0; aClass296_Sub51Array6228.length > i_42_; i_42_++) {
			aClass296_Sub51Array6228[i_42_].method3070(-256, i_41_, i);
		}
		AdvancedMemoryCache.method998(i_41_, -1, i);
		float[] fs = new float[i * i_41_ * 4];
		if (i_40_ > -106) {
			method2888(120);
		}
		int i_43_ = 0;
		for (int i_44_ = 0; i > i_44_; i_44_++) {
			int[] is;
			int[] is_45_;
			int[] is_46_;
			if (aClass296_Sub51_6230.monochromatic) {
				int[] is_47_ = aClass296_Sub51_6230.get_monochrome_output(0, i_44_);
				is_46_ = is_47_;
				is = is_47_;
				is_45_ = is_47_;
			} else {
				int[][] is_48_ = aClass296_Sub51_6230.get_colour_output(i_44_, 17621);
				is = is_48_[0];
				is_45_ = is_48_[2];
				is_46_ = is_48_[1];
			}
			int[] is_49_;
			if (aClass296_Sub51_6227.monochromatic) {
				is_49_ = aClass296_Sub51_6227.get_monochrome_output(0, i_44_);
			} else {
				is_49_ = aClass296_Sub51_6227.get_colour_output(i_44_, 17621)[0];
			}
			if (bool) {
				i_43_ = i_44_ << 2;
			}
			int[] is_50_;
			if (aClass296_Sub51_6233.monochromatic) {
				is_50_ = aClass296_Sub51_6233.get_monochrome_output(0, i_44_);
			} else {
				is_50_ = aClass296_Sub51_6233.get_colour_output(i_44_, 17621)[0];
			}
			for (int i_51_ = i_41_ - 1; i_51_ >= 0; i_51_--) {
				float f = is_49_[i_51_] / 4096.0F;
				if (f < 0.0F) {
					f = 0.0F;
				} else if (f > 1.0F) {
					f = 1.0F;
				}
				float f_52_ = (is_50_[i_51_] * 31.0F / 4096.0F + 1.0F) / 4096.0F;
				fs[i_43_++] = is[i_51_] * f_52_;
				fs[i_43_++] = f_52_ * is_46_[i_51_];
				fs[i_43_++] = f_52_ * is_45_[i_51_];
				fs[i_43_++] = f;
				if (bool) {
					i_43_ += (i_41_ << 2) - 4;
				}
			}
		}
		for (int i_53_ = 0; aClass296_Sub51Array6228.length > i_53_; i_53_++) {
			aClass296_Sub51Array6228[i_53_].method3073(22);
		}
		return fs;
	}

	Texture(int textureid, Packet buffer) {
		this.textureid = textureid;
		int num_operations = buffer.g1();
		int num_diffuse = 0;
		int num_hdr = 0;
		int[][] is = new int[num_operations][];
		aClass296_Sub51Array6228 = new TextureOperation[num_operations];
		for (int i_56_ = 0; num_operations > i_56_; i_56_++) {
			TextureOperation class296_sub51 = Class368_Sub12.method3846(buffer, (byte) 55);
			if (class296_sub51.method3067((byte) 105) >= 0) {
				num_diffuse++;
			}
			if (class296_sub51.method3062((byte) 2) >= 0) {
				num_hdr++;
			}
			int i_57_ = class296_sub51.childs.length;
			is[i_56_] = new int[i_57_];
			for (int i_58_ = 0; i_58_ < i_57_; i_58_++) {
				is[i_56_][i_58_] = buffer.g1();
			}
			aClass296_Sub51Array6228[i_56_] = class296_sub51;
		}
		texture_sprites_diffuse = new int[num_diffuse];
		num_diffuse = 0;
		texture_sprites_hdr = new int[num_hdr];
		num_hdr = 0;
		for (int i_59_ = 0; i_59_ < num_operations; i_59_++) {
			TextureOperation class296_sub51 = aClass296_Sub51Array6228[i_59_];
			int i_60_ = class296_sub51.childs.length;
			for (int i_61_ = 0; i_60_ > i_61_; i_61_++) {
				class296_sub51.childs[i_61_] = aClass296_Sub51Array6228[is[i_59_][i_61_]];
			}
			int diffue_sprite = class296_sub51.method3067((byte) 114);
			int hdr_sprite = class296_sub51.method3062((byte) 2);
			if (diffue_sprite > 0) {
				texture_sprites_diffuse[num_diffuse++] = diffue_sprite;
			}
			if (hdr_sprite > 0) {
				texture_sprites_hdr[num_hdr++] = hdr_sprite;
			}
			is[i_59_] = null;
		}
		aClass296_Sub51_6230 = aClass296_Sub51Array6228[buffer.g1()];
		aClass296_Sub51_6227 = aClass296_Sub51Array6228[buffer.g1()];
		aClass296_Sub51_6233 = aClass296_Sub51Array6228[buffer.g1()];
	}
}
