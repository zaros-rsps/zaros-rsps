package net.zaros.client;

import java.io.IOException;

import net.zaros.client.configs.objtype.ObjType;
import net.zaros.client.logic.clans.channel.ClanChannel;
import net.zaros.client.logic.clans.channel.delta.ClanChannelDelta;
import net.zaros.client.logic.clans.settings.ClanSettings;
import net.zaros.client.logic.clans.settings.delta.ClanSettingsDelta;

public class PacketParser {

	static final boolean method3136(Connection conn, int i) throws IOException {
		Class154 class154 = conn.aClass154_2045;
		ByteStream buff = conn.in_stream;
		if (class154 == null) {
			return false;
		}
		if (conn.incomingPacket == null) {
			if (conn.aBoolean2064) {
				if (!class154.method1556(true, 1)) {
					return false;
				}
				class154.method1559((byte) 117, 0, conn.in_stream.data, 1);
				conn.aBoolean2064 = false;
				conn.anInt2062 = 0;
				conn.totalBytesReceived++;
			}
			buff.pos = 0;
			if (buff.nextOpcodeBig()) {
				if (!class154.method1556(true, 1)) {
					return false;
				}
				class154.method1559((byte) -79, 1, conn.in_stream.data, 1);
				conn.anInt2062 = 0;
				conn.totalBytesReceived++;
			}
			conn.aBoolean2064 = true;
			IncomingPacket[] packets = Class223.method2078((byte) 34);
			int opcode = buff.readOpcode();
			if (opcode < 0 || packets.length <= opcode) {
				throw new IOException("invo:" + opcode + " ip:" + buff.pos);
			}
			conn.incomingPacket = packets[opcode];
			conn.protSize = conn.incomingPacket.length;
		}
		if (conn.protSize == -1) {
			if (!class154.method1556(true, 1)) {
				return false;
			}
			class154.method1559((byte) -49, 0, buff.data, 1);
			conn.protSize = buff.data[0] & 0xff;
			conn.totalBytesReceived++;
			conn.anInt2062 = 0;
		}
		if (conn.protSize == -2) {
			if (!class154.method1556(true, 2)) {
				return false;
			}
			class154.method1559((byte) -103, 0, buff.data, 2);
			buff.pos = 0;
			conn.protSize = buff.g2();
			conn.anInt2062 = 0;
			conn.totalBytesReceived += 2;
		}
		if (conn.protSize > 0) {
			if (!class154.method1556(true, conn.protSize)) {
				return false;
			}
			buff.pos = 0;
			class154.method1559((byte) 6, 0, buff.data, conn.protSize);
			conn.anInt2062 = 0;
			conn.totalBytesReceived += conn.protSize;
		}
		conn.incomingPacket3 = conn.incomingPacket1;
		conn.incomingPacket1 = conn.incomingPacket2;
		conn.incomingPacket2 = conn.incomingPacket;
		if (conn.incomingPacket == Class153.aClass231_1577) {
			int i_13_ = buff.g4();
			boolean bool = buff.readUnsignedByteS() == 1;
			if (Class366_Sub7.aBoolean5402 == !bool || Class104.anInt1088 != i_13_) {
				Class366_Sub7.aBoolean5402 = bool;
				Class104.anInt1088 = i_13_;
				CS2Executor.method1520(Class296_Sub37.aClass81_4883, -1, -1);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class41_Sub18.aClass231_3789) {
			Class317.parseSubpacket(Class41_Sub17.aClass260_3782, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (Class144.aClass231_1433 == conn.incomingPacket) { // item2
			int arrayID = buff.g2();
			int bitFlag = buff.g1();
			boolean split = (bitFlag & 0x1) == 1;
			while (buff.pos < conn.protSize) {
				int index = buff.readSmart();
				int itemID = buff.g2();
				int itemAmount = 0;
				if (itemID != 0) {
					itemAmount = buff.g1();
					if (itemAmount == 255) {
						itemAmount = buff.g4();
					}
				}
				ItemArrayMethods.updateItemsRegister(arrayID, index, itemID - 1, itemAmount, split);
			}
			Class250.itemChangesArray[31 & Class337.itemChanges++] = arrayID;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class330.aClass231_2922) {
			if (Animator.aFrame435 != null) {
				Class104.method904(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_5003.method444(124), false, 0, -1, -1);
			}
			byte[] is = new byte[conn.protSize];
			buff.readEncryptedData(is, 0, conn.protSize);
			String string = Class360.method3722(0, is, conn.protSize, (byte) 75);
			String string_19_ = "opensn";
			if (!NodeDeque.js || Class354.method3692(Class252.aClass398_2383, string, 1, i ^ 0x6bc13c0, string_19_).anInt2540 == 2) {
				Class127.method1355(string_19_, Class252.aClass398_2383, string, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(115) == 1, true, -122);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class16.aClass231_181) {
			String string = buff.gstr();
			int i_20_ = buff.g2();
			String string_21_ = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_20_, -125).method2839(buff, false);
			Class296_Sub51_Sub9.method3099(19, string, 0, null, string_21_, false, string, i_20_, string);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class41_Sub4.aClass231_3749) {
			int max = buff.g2();
			if (max == 65535) {
				max = -1;
			}
			int componentUID = buff.readInt_v1();
			int value = buff.readUnsignedLEShort();
			int min = buff.g2();
			if (min == 65535) {
				min = -1;
			}
			BillboardRaw.interfaceCounter();
			for (int aaa = min; aaa <= max; aaa++) {
				long l = ((long) componentUID << 32) + aaa;
				InterfaceComponentSettings existing = (InterfaceComponentSettings) Class149.interfaceSettings.get(l);
				InterfaceComponentSettings modified;
				if (existing != null) {
					modified = new InterfaceComponentSettings(existing.settingsHash, value);
					existing.unlink();
				} else if (aaa == -1) {
					modified = new InterfaceComponentSettings(InterfaceComponent.getInterfaceComponent(componentUID).settings_.settingsHash, value);
				} else {
					modified = new InterfaceComponentSettings(0, value);
				}
				Class149.interfaceSettings.put(l, modified);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (ClothDefinition.aClass231_3013 == conn.incomingPacket) {
			Class317.parseSubpacket(Class296_Sub45_Sub2.aClass260_6275, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (Class164.aClass231_1686 == conn.incomingPacket) {
			int i_28_ = buff.g4();
			String string = buff.gstr();
			BillboardRaw.interfaceCounter();
			Class390.method4039(string, (byte) 28, i_28_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class241.aClass231_2304 == conn.incomingPacket) {
			Class338_Sub3_Sub5_Sub1.anInt6645 = buff.g1();
			conn.incomingPacket = null;
			return true;
		}
		if (RuntimeException_Sub1.aClass231_3393 == conn.incomingPacket) {
			byte i_29_ = buff.g1b();
			int i_30_ = buff.g2();
			Class16_Sub3_Sub1.configsRegister.setDelayedConfiguration(i_29_, i_30_, (byte) 38);
			conn.incomingPacket = null;
			return true;
		}
		if (Class154.aClass231_1582 == conn.incomingPacket) {
			int pIndex = buff.g2();
			Player player;
			if (Class362.myPlayerIndex == pIndex) {
				player = Class296_Sub51_Sub11.localPlayer;
			} else {
				player = PlayerUpdate.visiblePlayers[pIndex];
			}
			if (player == null) {
				conn.incomingPacket = null;
				return true;
			}
			int effectsFlag = buff.g2();
			int pRights = buff.g1();
			boolean checkIgnores = (effectsFlag & 0x8000) != 0;
			if (player.displayName != null && player.composite != null) {
				boolean ignore = false;
				if (pRights <= 1) {
					if (!checkIgnores && (Class296_Sub40.somethingWithIgnore0 && !Class41_Sub17.somethingWithIgnore1 || Class123.somethingWithIgnore2)) {
						ignore = true;
					} else if (NPCNode.playerIgnored(player.displayName)) {
						ignore = true;
					}
				}
				if (!ignore && Class47.anInt452 == 0) {
					int i_35_ = -1;
					String string;
					if (!checkIgnores) {
						string = ha_Sub1_Sub1.escapeMessage(Class293.readHuffmanMessage(buff));
					} else {
						effectsFlag &= 0x7fff;
						Class270 class270 = Class296_Sub51_Sub27.method3154(buff, false);
						i_35_ = class270.anInt2506;
						string = class270.aClass296_Sub39_Sub10_2509.method2839(buff, false);
					}
					player.method3524(string.trim(), effectsFlag & 0xff, effectsFlag >> 8, 120);
					int i_36_;
					if (pRights != 1 && pRights != 2) {
						i_36_ = !checkIgnores ? 2 : 17;
					} else {
						i_36_ = checkIgnores ? 17 : 1;
					}
					if (pRights != 2) {
						if (pRights != 1) {
							Class296_Sub51_Sub9.method3099(i_36_, player.getFullName(true), 0, null, string, false, player.name, i_35_, player.getName(false));
						} else {
							Class296_Sub51_Sub9.method3099(i_36_, "<img=0>" + player.getFullName(true), 0, null, string, false, player.name, i_35_, "<img=0>" + player.getName(false));
						}
					} else {
						Class296_Sub51_Sub9.method3099(i_36_, "<img=1>" + player.getFullName(true), 0, null, string, false, player.name, i_35_, "<img=1>" + player.getName(false));
					}
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class296_Sub51_Sub35.aClass231_6527) {
			boolean bool = buff.g1() == 1;
			byte[] is = new byte[conn.protSize - 1];
			buff.readBytes(is, 0, conn.protSize - 1);
			Class10.method195(is, bool);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class317.aClass231_3702) {
			boolean bool = buff.g1() == 1;
			String string = buff.gstr();
			String string_37_ = string;
			if (bool) {
				string_37_ = buff.gstr();
			}
			int i_38_ = buff.g1();
			int i_39_ = buff.g2();
			boolean bool_40_ = false;
			if (i_38_ <= 1 && NPCNode.playerIgnored(string_37_)) {
				bool_40_ = true;
			}
			if (!bool_40_ && Class47.anInt452 == 0) {
				String string_41_ = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_39_, -126).method2839(buff, false);
				if (i_38_ == 2) {
					Class296_Sub51_Sub9.method3099(25, "<img=1>" + string, 0, null, string_41_, false, string, i_39_, "<img=1>" + string_37_);
				} else if (i_38_ == 1) {
					Class296_Sub51_Sub9.method3099(25, "<img=0>" + string, 0, null, string_41_, false, string, i_39_, "<img=0>" + string_37_);
				} else {
					Class296_Sub51_Sub9.method3099(25, string, 0, null, string_41_, false, string, i_39_, string_37_);
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (Class368_Sub13.aClass231_5496 == conn.incomingPacket) {
			Class317.parseSubpacket(Class178_Sub2.aClass260_4436, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class156.aClass231_3592) {
			Class317.parseSubpacket(Class22.aClass260_252, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (IncomingPacket.CLANSETTINGS_FULL == conn.incomingPacket) {
			Class296_Sub39_Sub20_Sub2.anInt6727 = Class338_Sub3_Sub3_Sub1.anInt6616;
			boolean affined = buff.g1() == 1;
			if (conn.protSize == 1) {
				conn.incomingPacket = null;
				if (affined) {
					Class296_Sub51_Sub2.affinedClanSettings = null;
				} else {
					Class368_Sub2.unaffinedClanSettings = null;
				}
				return true;
			}
			if (!affined) {
				Class368_Sub2.unaffinedClanSettings = new ClanSettings(buff);
			} else {
				Class296_Sub51_Sub2.affinedClanSettings = new ClanSettings(buff);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class178_Sub3.aClass231_4470) {
			StringNode.logout(3, Class342.aBoolean2990);
			conn.incomingPacket = null;
			return false;
		}
		if (conn.incomingPacket == Class306.aClass231_2745) {
			Class296_Sub51_Sub37.aString6532 = conn.protSize > 2 ? buff.gstr() : TranslatableString.aClass120_1222.getTranslation(Class394.langID);
			Class208.anInt2091 = conn.protSize > 0 ? buff.g2() : -1;
			conn.incomingPacket = null;
			if (Class208.anInt2091 == 65535) {
				Class208.anInt2091 = -1;
			}
			return true;
		}
		if (conn.incomingPacket == Class368_Sub11.aClass231_5483) {
			int i_42_ = buff.g4();
			BillboardRaw.interfaceCounter();
			if (i_42_ == -1) {
				Class242.anInt2309 = -1;
				Class368_Sub11.anInt5486 = -1;
			} else {
				int i_43_ = (i_42_ & 0xfffe348) >> 14;
				i_43_ -= Class206.worldBaseX;
				int i_44_ = i_42_ & 0x3fff;
				if (i_43_ < 0) {
					i_43_ = 0;
				} else if (Class198.currentMapSizeX <= i_43_) {
					i_43_ = Class198.currentMapSizeX;
				}
				i_44_ -= Class41_Sub26.worldBaseY;
				Class368_Sub11.anInt5486 = (i_43_ << 9) + 256;
				if (i_44_ < 0) {
					i_44_ = 0;
				} else if (i_44_ >= Class296_Sub38.currentMapSizeY) {
					i_44_ = Class296_Sub38.currentMapSizeY;
				}
				Class242.anInt2309 = (i_44_ << 9) + 256;
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class172.hintIconPacket) {
			int encodedData = buff.g1();
			int iconSlot = encodedData >> 5;
			int iconType = encodedData & 0x1f;
			if (iconType == 0) {
				Class338_Sub2.aClass225Array5200[iconSlot] = null;
				conn.incomingPacket = null;
				return true;
			}
			Class225 class225 = new Class225();
			class225.iconType = iconType;
			class225.modelId = buff.g1();
			if (class225.modelId >= 0 && class225.modelId < Class151.aClass397Array1555.length) {
				if (class225.iconType == 1 || class225.iconType == 10) {
					class225.targetIndex = buff.g2();
					class225.anInt2180 = buff.g2();
					buff.pos += 4;
				} else if (class225.iconType >= 2 && class225.iconType <= 6) {
					if (class225.iconType == 2) {
						class225.anInt2176 = 256;
						class225.anInt2183 = 256;
					}
					if (class225.iconType == 3) {
						class225.anInt2183 = 0;
						class225.anInt2176 = 256;
					}
					if (class225.iconType == 4) {
						class225.anInt2183 = 512;
						class225.anInt2176 = 256;
					}
					if (class225.iconType == 5) {
						class225.anInt2176 = 0;
						class225.anInt2183 = 256;
					}
					if (class225.iconType == 6) {
						class225.anInt2176 = 512;
						class225.anInt2183 = 256;
					}
					class225.iconType = 2;
					class225.anInt2184 = buff.g1();
					class225.anInt2183 += buff.g2() - Class206.worldBaseX << 9;
					class225.anInt2176 += buff.g2() - Class41_Sub26.worldBaseY << 9;
					class225.anInt2177 = buff.g1() << 2;
					class225.anInt2179 = buff.g2();
				}
				class225.anInt2182 = buff.g2();
				if (class225.anInt2182 == 65535) {
					class225.anInt2182 = -1;
				}
				Class338_Sub2.aClass225Array5200[iconSlot] = class225;
			}
			conn.incomingPacket = null;
			return true;
		}
		if (Class396.aClass231_3320 == conn.incomingPacket) {
			boolean bool = buff.g1() == 1;
			String string = buff.gstr();
			String string_48_ = string;
			if (bool) {
				string_48_ = buff.gstr();
			}
			long l = buff.g8();
			long l_49_ = buff.g2();
			long l_50_ = buff.readUnsignedMedInt();
			int i_51_ = buff.g1();
			long l_52_ = l_50_ + (l_49_ << 32);
			boolean bool_53_ = false;
			while_130_: do {
				for (int i_54_ = 0; i_54_ < 100; i_54_++) {
					if (l_52_ == Class67.aLongArray751[i_54_]) {
						bool_53_ = true;
						break while_130_;
					}
				}
				if (i_51_ <= 1) {
					if (Class296_Sub40.somethingWithIgnore0 && !Class41_Sub17.somethingWithIgnore1 || Class123.somethingWithIgnore2) {
						bool_53_ = true;
					} else if (NPCNode.playerIgnored(string_48_)) {
						bool_53_ = true;
					}
				}
			} while (false);
			if (!bool_53_ && Class47.anInt452 == 0) {
				Class67.aLongArray751[Class162.anInt3558] = l_52_;
				Class162.anInt3558 = (Class162.anInt3558 + 1) % 100;
				String string_55_ = ha_Sub1_Sub1.escapeMessage(Class293.readHuffmanMessage(buff));
				if (i_51_ != 2 && i_51_ != 3) {
					if (i_51_ != 1) {
						Class296_Sub51_Sub9.method3099(9, string, 0, Class296_Sub43.method2922((byte) -126, l), string_55_, false, string, -1, string_48_);
					} else {
						Class296_Sub51_Sub9.method3099(9, "<img=0>" + string, 0, Class296_Sub43.method2922((byte) 122, l), string_55_, false, string, -1, "<img=0>" + string_48_);
					}
				} else {
					Class296_Sub51_Sub9.method3099(9, "<img=1>" + string, 0, Class296_Sub43.method2922((byte) 114, l), string_55_, false, string, -1, "<img=1>" + string_48_);
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (Class122_Sub1_Sub1.aClass231_6603 == conn.incomingPacket) {
			Class268.anInt2486 = buff.method2566();
			Class296_Sub40.somethingWithIgnore0 = buff.g1() == 1;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class304.aClass231_2730) {
			int i_56_ = buff.readInt_v1();
			int i_57_ = (i_56_ & 0x3f549239) >> 28;
			int i_58_ = i_56_ >> 14 & 0x3fff;
			int i_59_ = i_56_ & 0x3fff;
			int i_60_ = buff.readUnsignedShortA();
			if (i_60_ == 65535) {
				i_60_ = -1;
			}
			int i_61_ = buff.readUnsignedByteA();
			int i_62_ = i_61_ >> 2;
			int i_63_ = i_61_ & 0x3;
			i_59_ -= Class41_Sub26.worldBaseY;
			i_58_ -= Class206.worldBaseX;
			int i_64_ = Class304.anIntArray2731[i_62_];
			Class314.method3320(i_62_, i_59_, -100, i_63_, i_64_, i_57_, i_58_, i_60_);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class194.aClass231_1974) {
			int i_65_ = buff.readLEInt();
			BillboardRaw.interfaceCounter();
			Class296_Sub29.method2688(i + 113008289, 5, 0, Class362.myPlayerIndex, i_65_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class241_Sub2_Sub1.aClass231_5903 == conn.incomingPacket) {
			boolean bool = buff.g1() == 1;
			String string = buff.gstr();
			String string_66_ = string;
			if (bool) {
				string_66_ = buff.gstr();
			}
			long l = buff.g8();
			long l_67_ = buff.g2();
			long l_68_ = buff.readUnsignedMedInt();
			int i_69_ = buff.g1();
			int i_70_ = buff.g2();
			long l_71_ = l_68_ + (l_67_ << 32);
			boolean bool_72_ = false;
			while_131_: do {
				for (int i_73_ = 0; i_73_ < 100; i_73_++) {
					if (l_71_ == Class67.aLongArray751[i_73_]) {
						bool_72_ = true;
						break while_131_;
					}
				}
				if (i_69_ <= 1 && NPCNode.playerIgnored(string_66_)) {
					bool_72_ = true;
				}
			} while (false);
			if (!bool_72_ && Class47.anInt452 == 0) {
				Class67.aLongArray751[Class162.anInt3558] = l_71_;
				Class162.anInt3558 = (Class162.anInt3558 + 1) % 100;
				String string_74_ = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_70_, -127).method2839(buff, false);
				if (i_69_ != 2) {
					if (i_69_ == 1) {
						Class296_Sub51_Sub9.method3099(20, "<img=0>" + string, 0, Class296_Sub43.method2922((byte) -126, l), string_74_, false, string, i_70_, "<img=0>" + string_66_);
					} else {
						Class296_Sub51_Sub9.method3099(20, string, 0, Class296_Sub43.method2922((byte) -126, l), string_74_, false, string, i_70_, string_66_);
					}
				} else {
					Class296_Sub51_Sub9.method3099(20, "<img=1>" + string, 0, Class296_Sub43.method2922((byte) -128, l), string_74_, false, string, i_70_, "<img=1>" + string_66_);
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class304.aClass231_2728) {
			String string = buff.gstr();
			Object[] objects = new Object[string.length() + 1];
			for (int i_75_ = string.length() - 1; i_75_ >= 0; i_75_--) {
				if (string.charAt(i_75_) != 's') {
					objects[i_75_ + 1] = new Integer(buff.g4());
				} else {
					objects[i_75_ + 1] = buff.gstr();
				}
			}
			objects[0] = new Integer(buff.g4());
			BillboardRaw.interfaceCounter();
			CS2Call cs2Call = new CS2Call();
			cs2Call.callArgs = objects;
			CS2Executor.runCS2(cs2Call);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class296_Sub39_Sub6.aClass231_6150) {
			NPCNode.method2450(conn.protSize, buff, 0, Class252.aClass398_2383);
			conn.incomingPacket = null;
			return true;
		}
		if (Class32_Sub1.aClass231_5647 == conn.incomingPacket) {
			int i_76_ = buff.readInt_v1();
			BillboardRaw.interfaceCounter();
			Class296_Sub29.method2688(4, 3, -1, -1, i_76_);
			conn.incomingPacket = null;
			return true;
		}
		if (StaticMethods.aClass231_6078 == conn.incomingPacket) {
			int i_77_ = buff.g2();
			byte i_78_ = buff.g1b();
			if (Class276.anObjectArray2527 == null) {
				Class276.anObjectArray2527 = new Object[StaticMethods.aClass328_6070.anInt2910];
			}
			Class276.anObjectArray2527[i_77_] = new Integer(i_78_);
			Class219.anIntArray2131[31 & Class366_Sub5.anInt5387++] = i_77_;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class42.aClass231_398) {
			conn.incomingPacket = null;
			Class276.anObjectArray2527 = new Object[StaticMethods.aClass328_6070.anInt2910];
			return true;
		}
		if (Class395.aClass231_3313 == conn.incomingPacket) {
			int i_79_ = buff.g2();
			int i_80_ = buff.g2();
			int i_81_ = buff.g2();
			BillboardRaw.interfaceCounter();
			if (Class192.openedInterfaceComponents[i_79_] != null) {
				for (int i_82_ = i_80_; i_82_ < i_81_; i_82_++) {
					int i_83_ = buff.readUnsignedMedInt();
					if (i_82_ < Class192.openedInterfaceComponents[i_79_].length && Class192.openedInterfaceComponents[i_79_][i_82_] != null) {
						Class192.openedInterfaceComponents[i_79_][i_82_].anInt529 = i_83_;
					}
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Js5TextureLoader.aClass231_3445) {
			int i_84_ = buff.readUnsignedLEShortA();
			int i_85_ = buff.g4();
			BillboardRaw.interfaceCounter();
			Class296_Sub51_Sub17.method3122(i_85_, 109, i_84_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class342.aClass231_2986 == conn.incomingPacket) {
			conn.incomingPacket = null;
			return false;
		}
		if (Class288.aClass231_2649 == conn.incomingPacket) {
			while (conn.protSize > buff.pos) {
				boolean ignoreWarn = buff.g1() == 1;
				String username = buff.gstr();
				String displayName = buff.gstr();
				int worldId = buff.g2();
				int clanRank = buff.g1();
				boolean bool_89_ = buff.g1() == 1;
				String string_90_ = "";
				boolean bool_91_ = false;
				if (worldId > 0) {
					string_90_ = buff.gstr();
					bool_91_ = buff.g1() == 1;
				}
				for (int i_92_ = 0; i_92_ < Class285.anInt2625; i_92_++) {
					if (!ignoreWarn) {
						if (username.equals(Js5TextureLoader.aStringArray3443[i_92_])) {
							if (Class95.anIntArray1021[i_92_] != worldId) {
								boolean bool_93_ = true;
								for (Class338_Sub8_Sub1 class338_sub8_sub1 = (Class338_Sub8_Sub1) Class33.aClass404_331.method4160((byte) 115); class338_sub8_sub1 != null; class338_sub8_sub1 = (Class338_Sub8_Sub1) Class33.aClass404_331.method4163(-24917)) {
									if (class338_sub8_sub1.aString6575.equals(username)) {
										if (worldId != 0 && class338_sub8_sub1.aShort6574 == 0) {
											bool_93_ = false;
											class338_sub8_sub1.method3438(false);
										} else if (worldId == 0 && class338_sub8_sub1.aShort6574 != 0) {
											bool_93_ = false;
											class338_sub8_sub1.method3438(false);
										}
									}
								}
								if (bool_93_) {
									Class33.aClass404_331.method4158(new Class338_Sub8_Sub1(username, worldId), 1);
								}
								Class95.anIntArray1021[i_92_] = worldId;
							}
							Class338_Sub9.aStringArray5268[i_92_] = displayName;
							StaticMethods.aStringArray5940[i_92_] = string_90_;
							Class338_Sub3_Sub4_Sub1.anIntArray6653[i_92_] = clanRank;
							StaticMethods.aBooleanArray5945[i_92_] = bool_91_;
							username = null;
							Class296_Sub34_Sub2.aBooleanArray6101[i_92_] = bool_89_;
							break;
						}
					} else if (displayName.equals(Js5TextureLoader.aStringArray3443[i_92_])) {
						Js5TextureLoader.aStringArray3443[i_92_] = username;
						Class338_Sub9.aStringArray5268[i_92_] = displayName;
						username = null;
						break;
					}
				}
				if (username != null && Class285.anInt2625 < 200) {
					Js5TextureLoader.aStringArray3443[Class285.anInt2625] = username;
					Class338_Sub9.aStringArray5268[Class285.anInt2625] = displayName;
					Class95.anIntArray1021[Class285.anInt2625] = worldId;
					StaticMethods.aStringArray5940[Class285.anInt2625] = string_90_;
					Class338_Sub3_Sub4_Sub1.anIntArray6653[Class285.anInt2625] = clanRank;
					StaticMethods.aBooleanArray5945[Class285.anInt2625] = bool_91_;
					Class296_Sub34_Sub2.aBooleanArray6101[Class285.anInt2625] = bool_89_;
					Class285.anInt2625++;
				}
			}
			Class156.anInt3595 = Class338_Sub3_Sub3_Sub1.anInt6616;
			Class78.anInt3431 = 2;
			boolean bool = false;
			int i_94_ = Class285.anInt2625;
			while (i_94_ > 0) {
				i_94_--;
				bool = true;
				for (int i_95_ = 0; i_94_ > i_95_; i_95_++) {
					boolean bool_96_ = false;
					if (Class95.anIntArray1021[i_95_] != Class296_Sub51_Sub27_Sub1.aClass112_6733.worldId && Class95.anIntArray1021[i_95_ + 1] == Class296_Sub51_Sub27_Sub1.aClass112_6733.worldId) {
						bool_96_ = true;
					}
					if (!bool_96_ && Class95.anIntArray1021[i_95_] == 0 && Class95.anIntArray1021[i_95_ + 1] != 0) {
						bool_96_ = true;
					}
					if (!bool_96_ && !Class296_Sub34_Sub2.aBooleanArray6101[i_95_] && Class296_Sub34_Sub2.aBooleanArray6101[i_95_ + 1]) {
						bool_96_ = true;
					}
					if (bool_96_) {
						int i_97_ = Class95.anIntArray1021[i_95_];
						Class95.anIntArray1021[i_95_] = Class95.anIntArray1021[i_95_ + 1];
						Class95.anIntArray1021[i_95_ + 1] = i_97_;
						String string = StaticMethods.aStringArray5940[i_95_];
						StaticMethods.aStringArray5940[i_95_] = StaticMethods.aStringArray5940[i_95_ + 1];
						StaticMethods.aStringArray5940[i_95_ + 1] = string;
						String string_98_ = Js5TextureLoader.aStringArray3443[i_95_];
						Js5TextureLoader.aStringArray3443[i_95_] = Js5TextureLoader.aStringArray3443[i_95_ + 1];
						Js5TextureLoader.aStringArray3443[i_95_ + 1] = string_98_;
						String string_99_ = Class338_Sub9.aStringArray5268[i_95_];
						Class338_Sub9.aStringArray5268[i_95_] = Class338_Sub9.aStringArray5268[i_95_ + 1];
						Class338_Sub9.aStringArray5268[i_95_ + 1] = string_99_;
						int i_100_ = Class338_Sub3_Sub4_Sub1.anIntArray6653[i_95_];
						Class338_Sub3_Sub4_Sub1.anIntArray6653[i_95_] = Class338_Sub3_Sub4_Sub1.anIntArray6653[i_95_ + 1];
						Class338_Sub3_Sub4_Sub1.anIntArray6653[i_95_ + 1] = i_100_;
						boolean bool_101_ = StaticMethods.aBooleanArray5945[i_95_];
						StaticMethods.aBooleanArray5945[i_95_] = StaticMethods.aBooleanArray5945[i_95_ + 1];
						StaticMethods.aBooleanArray5945[i_95_ + 1] = bool_101_;
						boolean bool_102_ = Class296_Sub34_Sub2.aBooleanArray6101[i_95_];
						Class296_Sub34_Sub2.aBooleanArray6101[i_95_] = Class296_Sub34_Sub2.aBooleanArray6101[i_95_ + 1];
						Class296_Sub34_Sub2.aBooleanArray6101[i_95_ + 1] = bool_102_;
						bool = false;
					}
				}
				if (bool) {
					break;
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class194.aClass231_1973) { // set friends chat channel
			Class296_Sub15_Sub3.anInt6012 = Class338_Sub3_Sub3_Sub1.anInt6616;
			if (conn.protSize == 0) {
				Exception_Sub1.aString45 = null;
				conn.incomingPacket = null;
				Class271.anInt2512 = 0;
				Class338_Sub8.aClass94Array5257 = null;
				StaticMethods.aString6087 = null;
				return true;
			}
			StaticMethods.aString6087 = buff.gstr();
			boolean bool = buff.g1() == 1;
			if (bool) {
				buff.gstr();
			}
			long l = buff.g8();
			Exception_Sub1.aString45 = Class296_Sub13.decodeBase37(l);
			Class97.aByte1051 = buff.g1b();
			int i_103_ = buff.g1();
			if (i_103_ == 255) {
				conn.incomingPacket = null;
				return true;
			}
			Class271.anInt2512 = i_103_;
			Class94[] class94s = new Class94[100];
			for (int i_104_ = 0; Class271.anInt2512 > i_104_; i_104_++) {
				class94s[i_104_] = new Class94();
				class94s[i_104_].aString1018 = buff.gstr();
				bool = buff.g1() == 1;
				if (!bool) {
					class94s[i_104_].aString1019 = class94s[i_104_].aString1018;
				} else {
					class94s[i_104_].aString1019 = buff.gstr();
				}
				class94s[i_104_].aString1015 = Class296_Sub49.parseName(class94s[i_104_].aString1019);
				class94s[i_104_].anInt1011 = buff.g2();
				class94s[i_104_].aByte1014 = buff.g1b();
				class94s[i_104_].aString1013 = buff.gstr();
				if (class94s[i_104_].aString1019.equals(Class296_Sub51_Sub11.localPlayer.displayName)) {
					Class296_Sub39_Sub10.aByte6178 = class94s[i_104_].aByte1014;
				}
			}
			boolean bool_105_ = false;
			int i_106_ = Class271.anInt2512;
			while (i_106_ > 0) {
				bool_105_ = true;
				i_106_--;
				for (int i_107_ = 0; i_106_ > i_107_; i_107_++) {
					if (class94s[i_107_].aString1015.compareTo(class94s[i_107_ + 1].aString1015) > 0) {
						Class94 class94 = class94s[i_107_];
						class94s[i_107_] = class94s[i_107_ + 1];
						class94s[i_107_ + 1] = class94;
						bool_105_ = false;
					}
				}
				if (bool_105_) {
					break;
				}
			}
			Class338_Sub8.aClass94Array5257 = class94s;
			conn.incomingPacket = null;
			return true;
		}
		if (Class16_Sub1_Sub1.aClass231_5804 == conn.incomingPacket) {
			int i_108_ = buff.readUnsignedLEShortA();
			int i_109_ = buff.readInt_v2();
			Class16_Sub3_Sub1.configsRegister.setDelayedBITConfig(i_108_, i_109_);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class8.aClass231_94) {
			int i_110_ = buff.g4();
			int i_111_ = buff.readUnsignedLEShortA();
			if (i_111_ == 65535) {
				i_111_ = -1;
			}
			BillboardRaw.interfaceCounter();
			Class296_Sub29.method2688(4, 2, -1, i_111_, i_110_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class192.aClass231_3561 == conn.incomingPacket) {
			int min = buff.readUnsignedLEShort();
			if (min == 65535) {
				min = -1;
			}
			int value = buff.readLEInt();
			int componentUID = buff.readInt_v1();
			int max = buff.readUnsignedLEShort();
			BillboardRaw.interfaceCounter();
			if (max == 65535) {
				max = -1;
			}
			for (int a11 = min; a11 <= max; a11++) {
				long cHash = a11 + ((long) componentUID << 32);
				InterfaceComponentSettings existing = (InterfaceComponentSettings) Class149.interfaceSettings.get(cHash);
				InterfaceComponentSettings modified;
				if (existing == null) {
					if (a11 != -1) {
						modified = new InterfaceComponentSettings(value, -1);
					} else {
						modified = new InterfaceComponentSettings(value, InterfaceComponent.getInterfaceComponent(componentUID).settings_.originalHash);
					}
				} else {
					modified = new InterfaceComponentSettings(value, existing.originalHash);
					existing.unlink();
				}
				Class149.interfaceSettings.put(cHash, modified);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class241_Sub2.aClass231_4579) {
			int i_118_ = buff.readUnsignedLEShort();
			String string = buff.gstr();
			BillboardRaw.interfaceCounter();
			FileOnDisk.setGlobalString(string, i_118_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class42.aClass231_396 == conn.incomingPacket) {
			int i_119_ = buff.readInt_v1();
			int i_120_ = buff.readUnsignedLEShortA();
			BillboardRaw.interfaceCounter();
			Class105_Sub2.method913(i_120_, i_119_, (byte) -106);
			conn.incomingPacket = null;
			return true;
		}
		if (ParamType.aClass231_3243 == conn.incomingPacket) {
			int i_121_ = buff.readUnsignedShortA();
			int i_122_ = buff.readInt_v1();
			int i_123_ = buff.readInt_v2();
			BillboardRaw.interfaceCounter();
			Class296_Sub29.method2688(i ^ ~0x6bc5e98, 5, i_123_, i_121_, i_122_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class296_Sub45_Sub2.aClass231_6279 == conn.incomingPacket) {
			int i_124_ = buff.g2();
			String string = buff.gstr();
			boolean bool = buff.g1() == 1;
			Class251.aBoolean2372 = bool;
			Class309.aClass112_2759 = Class296_Sub51_Sub27_Sub1.aClass112_6733;
			Class45.method582(i_124_, 7000, string);
			Object object = null;
			Class41_Sub8.method422(1, 15);
			conn.incomingPacket = null;
			return false;
		}
		if (Class296_Sub15_Sub1.aClass231_5995 == conn.incomingPacket) {
			Class213.method2022(-1);
			conn.incomingPacket = null;
			return true;
		}
		if (Class87.messagePacket == conn.incomingPacket) {
			int id = buff.readSmart();
			int i_126_ = buff.g4();
			int identifier = buff.g1();
			String string = "";
			String string_128_ = string;
			if ((identifier & 0x1) != 0) {
				string = buff.gstr();
				if ((identifier & 0x2) != 0) {
					string_128_ = buff.gstr();
				} else {
					string_128_ = string;
				}
			}
			String message = buff.gstr();
			if (id == 99) {
				Class41_Sub18.writeConsoleMessage(message);
			} else if (id == 98) {
				Class368_Sub5_Sub2.method3831(message, (byte) 103);
			} else if (id == 120) {
				ClientUtility.openURL(message);
			} else {
				if (!string_128_.equals("") && NPCNode.playerIgnored(string_128_)) {
					conn.incomingPacket = null;
					return true;
				}
				Class181.method1827(i_126_, string, i + 113008285, string, message, string_128_, id);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class349.aClass231_3035) {
			NPCUpdate.processGNP();
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class208.aClass231_2082) {
			Class123_Sub2_Sub2.anInt5822 = buff.g1();
			Class308.anInt2753 = Class338_Sub3_Sub3_Sub1.anInt6616;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class296_Sub42.aClass231_4926) {
			int i_130_ = buff.g2();
			long l = buff.g8();
			if (Class276.anObjectArray2527 == null) {
				Class276.anObjectArray2527 = new Object[StaticMethods.aClass328_6070.anInt2910];
			}
			Class276.anObjectArray2527[i_130_] = new Long(l);
			Class219.anIntArray2131[31 & Class366_Sub5.anInt5387++] = i_130_;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class45.aClass231_439) {
			Class317.parseSubpacket(Class127_Sub1.aClass260_4282, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (BillboardRaw.aClass231_1075 == conn.incomingPacket) {
			int transparency = buff.readUnsignedByteS();
			int id = buff.readUnsignedShortA();
			int slot = buff.g4();
			BillboardRaw.interfaceCounter();
			Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.get(slot);
			if (class296_sub13 != null) {
				Class47.method596(class296_sub13, id != class296_sub13.anInt4657, false, (byte) 111);
			}
			Class368.method3809(transparency, 2642, slot, id, false);
			conn.incomingPacket = null;
			return true;
		}
		if (ItemsNode.aClass231_4594 == conn.incomingPacket) {
			int i_134_ = buff.g2();
			int i_135_ = buff.readUnsignedByteA();
			int i_136_ = buff.readUnsignedByteC();
			int i_137_ = buff.g1();
			int i_138_ = buff.readUnsignedByteC();
			BillboardRaw.interfaceCounter();
			Class296_Sub51_Sub13.aBooleanArray6414[i_135_] = true;
			Class304.anIntArray2732[i_135_] = i_138_;
			Class61.anIntArray710[i_135_] = i_137_;
			Packet.anIntArray4677[i_135_] = i_136_;
			Class37.anIntArray365[i_135_] = i_134_;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == AdvancedMemoryCache.CLAN_CHANNEL_DELTA) {
			Class296_Sub15_Sub1.anInt5997 = Class338_Sub3_Sub3_Sub1.anInt6616;
			boolean bool = buff.g1() == 1;
			ClanChannelDelta class176 = new ClanChannelDelta(buff);
			ClanChannel class296_sub54;
			if (bool) {
				class296_sub54 = EmissiveTriangle.affinedClanChannel;
			} else {
				class296_sub54 = Class166.aClass296_Sub54_1692;
			}
			class176.applyToClanChannel(class296_sub54);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class236.aClass231_2240) {
			StringNode.logout(i + 113008288, false);
			conn.incomingPacket = null;
			return false;
		}
		if (Class382.MIDI_SONG == conn.incomingPacket) {
			int i_139_ = buff.readUnsignedShortA();
			if (i_139_ == 65535) {
				i_139_ = -1;
			}
			int i_140_ = buff.readUnsignedByteS();
			int i_141_ = buff.readUnsignedByteC();
			Class221.playSong(i_139_, (byte) 117, i_140_, i_141_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class296_Sub37.MIDI_JINGLE == conn.incomingPacket) {
			int delay = buff.method2554();
			int songid = buff.readUnsignedLEShort();
			if (songid == 65535) {
				songid = -1;
			}
			int volume = buff.g1();
			Class317.playJingle((byte) -58, volume, delay, songid);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class235.aClass231_2228) {
			Class317.parseSubpacket(Class367.aClass260_3127, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (Applet_Sub1.aClass231_6 == conn.incomingPacket) {
			boolean affined = buff.g1() == 1;
			String displayName = buff.gstr();
			long l = buff.g2();
			long l_145_ = buff.readUnsignedMedInt();
			int modlevel = buff.g1();
			long repeating_message = l_145_ + (l << 32);
			boolean ignored = false;
			ClanChannel channel = affined ? EmissiveTriangle.affinedClanChannel : Class166.aClass296_Sub54_1692;
			while_132_: do {
				if (channel == null) {
					ignored = true;
				} else {
					for (int i_149_ = 0; i_149_ < 100; i_149_++) {
						if (repeating_message == Class67.aLongArray751[i_149_]) {
							ignored = true;
							break while_132_;
						}
					}
					if (modlevel <= 1) {
						if (Class296_Sub40.somethingWithIgnore0 && !Class41_Sub17.somethingWithIgnore1 || Class123.somethingWithIgnore2) {
							ignored = true;
						} else if (NPCNode.playerIgnored(displayName)) {
							ignored = true;
						}
					}
				}
			} while (false);
			if (!ignored && Class47.anInt452 == 0) {
				Class67.aLongArray751[Class162.anInt3558] = repeating_message;
				Class162.anInt3558 = (Class162.anInt3558 + 1) % 100;
				String message = ha_Sub1_Sub1.escapeMessage(Class293.readHuffmanMessage(buff));
				int i_151_ = affined ? 41 : 44;
				if (modlevel == 1) {
					Class296_Sub51_Sub9.method3099(i_151_, "<img=0>" + displayName, 0, channel.name, message, false, displayName, -1, "<img=0>" + displayName);
				} else if (modlevel == 2 || modlevel == 3) {
					Class296_Sub51_Sub9.method3099(i_151_, "<img=1>" + displayName, 0, channel.name, message, false, displayName, -1, "<img=1>" + displayName);
				} else {
					Class296_Sub51_Sub9.method3099(i_151_, displayName, 0, channel.name, message, false, displayName, -1, displayName);
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class368_Sub9.aClass231_5471) {
			int i_152_ = buff.g1();
			int i_153_ = buff.readUnsignedLEShortA();
			BillboardRaw.interfaceCounter();
			Class368_Sub20.method3859(true, (byte) -103, i_152_, i_153_);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class16_Sub1.aClass231_3732) {
			int i_154_ = buff.readUnsignedByteC();
			int i_155_ = buff.readUnsignedByteC();
			if (i_154_ == 255) {
				i_154_ = -1;
				i_155_ = -1;
			}
			ReferenceTable.method836(i_154_, -111, i_155_);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == ReferenceWrapper.aClass231_6127) {
			String string = buff.gstr();
			int i_156_ = buff.readUnsignedShortA();
			BillboardRaw.interfaceCounter();
			FileOnDisk.setGlobalString(string, i_156_);
			conn.incomingPacket = null;
			return true;
		}
		if (StringNode.aClass231_4621 == conn.incomingPacket) {
			String string = buff.gstr();
			String string_157_ = ha_Sub1_Sub1.escapeMessage(Class293.readHuffmanMessage(buff));
			Class181.method1827(0, string, 0, string, string_157_, string, 6);
			conn.incomingPacket = null;
			return true;
		}
		if (Class198.SYNTH_SOUND == conn.incomingPacket) {
			int synthid = buff.g2();
			if (synthid == 65535) {
				synthid = -1;
			}
			int repeat = buff.g1();
			int delay = buff.g2();
			int volume = buff.g1();
			int seek = buff.g2();
			Class296_Sub51_Sub38.method3194(-115, synthid, seek, volume, repeat, delay);
			conn.incomingPacket = null;
			return true;
		}
		if (Class131.aClass231_1340 == conn.incomingPacket) {
			int i_163_ = buff.readUnsignedByteC();
			BillboardRaw.interfaceCounter();
			conn.incomingPacket = null;
			Class166_Sub1.anInt4301 = i_163_;
			return true;
		}
		if (Class15.CAM_LOOKAT == conn.incomingPacket) {
			int localy = buff.g1();
			int localx = buff.g1();
			int height = buff.readUnsignedShortA() << 2;
			int speed = buff.g1();
			int velocity = buff.readUnsignedByteS();
			BillboardRaw.interfaceCounter();
			aa_Sub3.cameraLookat(localx, localy, height, speed, velocity);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Animation.aClass231_420) {
			Class10.anInt3537 = buff.readSignedByteC() << 3;
			Class368_Sub15.anInt5515 = buff.g1();
			ModeWhere.anInt2352 = buff.g1b() << 3;
			for (Class296_Sub56 class296_sub56 = (Class296_Sub56) Class296_Sub11.aClass263_4647.getFirst(true); class296_sub56 != null; class296_sub56 = (Class296_Sub56) Class296_Sub11.aClass263_4647.getNext(i + 113008285)) {
				int i_169_ = (int) (class296_sub56.uid >> 28 & 0x3L);
				int i_170_ = (int) (class296_sub56.uid & 0x3fffL);
				int i_171_ = i_170_ - Class206.worldBaseX;
				int i_172_ = (int) (class296_sub56.uid >> 14 & 0x3fffL);
				int i_173_ = -Class41_Sub26.worldBaseY + i_172_;
				if (Class368_Sub15.anInt5515 == i_169_ && i_171_ >= Class10.anInt3537 && Class10.anInt3537 + 8 > i_171_ && ModeWhere.anInt2352 <= i_173_ && i_173_ < ModeWhere.anInt2352 + 8) {
					class296_sub56.unlink();
					if (i_171_ >= 0 && i_173_ >= 0 && i_171_ < Class198.currentMapSizeX && i_173_ < Class296_Sub38.currentMapSizeY) {
						Class295_Sub2.method2426(i_171_, 0, i_173_, Class368_Sub15.anInt5515);
					}
				}
			}
			for (Class296_Sub42 class296_sub42 = (Class296_Sub42) aa.aClass155_50.removeFirst((byte) 113); class296_sub42 != null; class296_sub42 = (Class296_Sub42) aa.aClass155_50.removeNext(1001)) {
				if (class296_sub42.anInt4923 >= Class10.anInt3537 && Class10.anInt3537 + 8 > class296_sub42.anInt4923 && ModeWhere.anInt2352 <= class296_sub42.anInt4928 && ModeWhere.anInt2352 + 8 > class296_sub42.anInt4928 && class296_sub42.anInt4932 == Class368_Sub15.anInt5515) {
					class296_sub42.aBoolean4929 = true;
				}
			}
			for (Class296_Sub42 class296_sub42 = (Class296_Sub42) ParticleEmitterRaw.aClass155_1770.removeFirst((byte) 112); class296_sub42 != null; class296_sub42 = (Class296_Sub42) ParticleEmitterRaw.aClass155_1770.removeNext(1001)) {
				if (class296_sub42.anInt4923 >= Class10.anInt3537 && class296_sub42.anInt4923 < Class10.anInt3537 + 8 && ModeWhere.anInt2352 <= class296_sub42.anInt4928 && class296_sub42.anInt4928 < ModeWhere.anInt2352 + 8 && class296_sub42.anInt4932 == Class368_Sub15.anInt5515) {
					class296_sub42.aBoolean4929 = true;
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (Class202.aClass231_2043 == conn.incomingPacket) {
			Class317.parseSubpacket(Class41_Sub6.aClass260_3758, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (Class95.aClass231_1031 == conn.incomingPacket) {
			int i_174_ = buff.g4();
			int i_175_ = buff.readUnsignedLEShort();
			if (i_175_ == 65535) {
				i_175_ = -1;
			}
			BillboardRaw.interfaceCounter();
			Class296_Sub29.method2688(4, 1, -1, i_175_, i_174_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class368_Sub20.aClass231_5545 == conn.incomingPacket) {
			Class317.parseSubpacket(Class335.aClass260_2953, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class41_Sub19.aClass231_3790) {
			int i_176_ = buff.g4();
			int i_177_ = buff.readUnsignedLEShort();
			Class16_Sub3_Sub1.configsRegister.setDelayedConfiguration(i_176_, i_177_, (byte) 38);
			conn.incomingPacket = null;
			return true;
		}
		if (Class368_Sub5_Sub1.aClass231_6593 == conn.incomingPacket) {
			int x = buff.g2();
			int yi_179_ = buff.readUnsignedShortA();
			BillboardRaw.interfaceCounter();
			Class373_Sub1.method3925(yi_179_, 0, x, -30302);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class366_Sub8.aClass231_5411) {
			ModeWhere.anInt2352 = buff.readSignedByteA() << 3;
			Class10.anInt3537 = buff.readSignedByteA() << 3;
			Class368_Sub15.anInt5515 = buff.readUnsignedByteC();
			while (buff.pos < conn.protSize) {
				SubInPacket class260 = CS2Stack.method2138(4)[buff.g1()];
				Class317.parseSubpacket(class260, (byte) 15);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class296_Sub43.aClass231_4939) {
			int i_180_ = buff.readInt_v2();
			int i_181_ = buff.readUnsignedLEShort();
			BillboardRaw.interfaceCounter();
			Class154_Sub1.setGlobalIntVar(i_180_, i_181_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class127.aClass231_1300 == conn.incomingPacket) {
			int i_182_ = buff.g2b();
			int i_183_ = buff.g4();
			BillboardRaw.interfaceCounter();
			Class296_Sub51_Sub10.method3105((byte) -125, i_183_, i_182_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class69.aClass231_3686 == conn.incomingPacket) {
			BillboardRaw.interfaceCounter();
			ParamTypeList.method1653((byte) 116);
			conn.incomingPacket = null;
			return true;
		}
		if (Class162.aClass231_3553 == conn.incomingPacket) {
			int i_184_ = buff.readUnsignedByteC();
			int i_185_ = buff.readInt_v1();
			int i_186_ = buff.g2();
			int i_187_ = buff.readUnsignedShortA();
			BillboardRaw.interfaceCounter();
			Class296_Sub45_Sub2.method2985(i_184_, i_186_, 62, i_187_, i_185_);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class210_Sub1.aClass231_4537) {
			int i_188_ = buff.g2();
			BillboardRaw.interfaceCounter();
			Class270.method2306(i_188_, (byte) -56);
			conn.incomingPacket = null;
			return true;
		}
		if (ClotchesLoader.aClass231_276 == conn.incomingPacket) {
			Class317.parseSubpacket(Class56.aClass260_665, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (Class117.aClass231_1185 == conn.incomingPacket) {
			if (Animator.aFrame435 != null) {
				Class104.method904(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_5003.method444(i + 113008404), false, 0, -1, -1);
			}
			byte[] is = new byte[conn.protSize];
			buff.readEncryptedData(is, 0, conn.protSize);
			String string = Class360.method3722(0, is, conn.protSize, (byte) 75);
			Class296_Sub13.method2507(true, string, Class252.aClass398_2383, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(115) == 1, 37);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class63.VORBIS_SPEECH_SOUND) {
			int speechId = buff.g2();
			if (speechId == 65535) {
				speechId = -1;
			}
			int i_190_ = buff.g1();
			int i_191_ = buff.g2();
			int i_192_ = buff.g1();
			Class338_Sub3_Sub4.method3571(i_192_, true, 256, i_191_, speechId, 90, i_190_);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class338_Sub8_Sub2.aClass231_6585) {
			Class368_Sub15.aClass213_5522 = aa_Sub3.method159(buff.g1(), -80);
			conn.incomingPacket = null;
			return true;
		}
		if (Class350.aClass231_3038 == conn.incomingPacket) {
			boolean bool = buff.g1() == 1;
			String string = buff.gstr();
			long l = buff.g2();
			long l_193_ = buff.readUnsignedMedInt();
			int i_194_ = buff.g1();
			int i_195_ = buff.g2();
			long l_196_ = (l << 32) - -l_193_;
			boolean bool_197_ = false;
			Object object = null;
			ClanChannel class296_sub54 = !bool ? Class166.aClass296_Sub54_1692 : EmissiveTriangle.affinedClanChannel;
			while_133_: do {
				if (class296_sub54 == null) {
					bool_197_ = true;
				} else {
					for (int i_198_ = 0; i_198_ < 100; i_198_++) {
						if (l_196_ == Class67.aLongArray751[i_198_]) {
							bool_197_ = true;
							break while_133_;
						}
					}
					if (i_194_ <= 1 && NPCNode.playerIgnored(string)) {
						bool_197_ = true;
					}
				}
			} while (false);
			if (!bool_197_ && Class47.anInt452 == 0) {
				Class67.aLongArray751[Class162.anInt3558] = l_196_;
				Class162.anInt3558 = (Class162.anInt3558 + 1) % 100;
				String string_199_ = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_195_, -128).method2839(buff, false);
				int i_200_ = !bool ? 45 : 42;
				if (i_194_ != 2 && i_194_ != 3) {
					if (i_194_ == 1) {
						Class296_Sub51_Sub9.method3099(i_200_, "<img=0>" + string, 0, class296_sub54.name, string_199_, false, string, i_195_, "<img=0>" + string);
					} else {
						Class296_Sub51_Sub9.method3099(i_200_, string, 0, class296_sub54.name, string_199_, false, string, i_195_, string);
					}
				} else {
					Class296_Sub51_Sub9.method3099(i_200_, "<img=1>" + string, 0, class296_sub54.name, string_199_, false, string, i_195_, "<img=1>" + string);
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (Class10.aClass231_3530 == conn.incomingPacket) {
			Class317.parseSubpacket(Class296_Sub24.aClass260_4753, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (Class296_Sub27.aClass231_4782 == conn.incomingPacket) {
			int i_201_ = buff.readUnsignedByteA();
			int i_202_ = buff.readUnsignedLEShortA();
			BillboardRaw.interfaceCounter();
			if (i_201_ == 2) {
				Class137.method1423((byte) 126);
			}
			Class99.anInt1064 = i_202_;
			Class296_Sub51_Sub9.method3101(30, i_202_);
			Animator.method569(false, (byte) 95);
			CS2Executor.method1521(Class99.anInt1064);
			for (int i_203_ = 0; i_203_ < 100; i_203_++) {
				Class360_Sub9.aBooleanArray5345[i_203_] = true;
			}
			conn.incomingPacket = null;
			return true;
		}
		if (Class69_Sub4.aClass231_5730 == conn.incomingPacket) {
			Class16_Sub3_Sub1.configsRegister.resetConfigurations(i + 113008285);
			ParamType.anInt3249 += 32;
			conn.incomingPacket = null;
			return true;
		}
		if (Class296_Sub1.aClass231_4583 == conn.incomingPacket) {
			Class317.parseSubpacket(r_Sub2.aClass260_6710, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Queue.aClass231_1436) {
			PlayerUpdate.parseUpdatePacket(buff, conn.protSize);
			conn.incomingPacket = null;
			return true;
		}
		if (Class241_Sub2_Sub1.aClass231_5897 == conn.incomingPacket) {
			Class119.readBuildMapDynamic();
			conn.incomingPacket = null;
			return false;
		}
		if (conn.incomingPacket == Class296_Sub39_Sub11.aClass231_6191) {
			StaticMethods.anInt5970 = buff.readUnsignedByteA();
			Class296_Sub51_Sub18.anInt6436 = buff.readUnsignedByteA();
			conn.incomingPacket = null;
			return true;
		}
		if (Class296_Sub39_Sub4.aClass231_5742 == conn.incomingPacket) {
			Class360_Sub2.method3737(true);
			conn.incomingPacket = null;
			return false;
		}
		if (s_Sub3.aClass231_5142 == conn.incomingPacket) {
			FileWorker.aBoolean3006 = buff.readUnsignedByteC() == 1;
			conn.incomingPacket = null;
			return true;
		}
		if (Class215.aClass231_2112 == conn.incomingPacket) {
			int i_204_ = buff.readUnsignedShortA();
			int i_205_ = buff.readInt_v2();
			int i_206_ = buff.readUnsignedLEShort();
			int i_207_ = buff.readUnsignedLEShort();
			BillboardRaw.interfaceCounter();
			Class360_Sub6.method3748(i_204_, i_207_, (byte) 42, i_205_, i_206_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class258.aClass231_2413 == conn.incomingPacket) {
			Class368_Sub15.anInt5515 = buff.readUnsignedByteS();
			Class10.anInt3537 = buff.readSignedByteC() << 3;
			ModeWhere.anInt2352 = buff.readSignedByteA() << 3;
			conn.incomingPacket = null;
			return true;
		}
		if (Class396.aClass231_3318 == conn.incomingPacket) {
			int i_208_ = buff.readUnsignedLEShortA();
			int i_209_ = buff.g4();
			BillboardRaw.interfaceCounter();
			Mobile.method3496(i_208_, 12279, i_209_);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == IncomingPacket.CLANCHANNEL_FULL) {
			Class296_Sub15_Sub1.anInt5997 = Class338_Sub3_Sub3_Sub1.anInt6616;
			boolean bool = buff.g1() == 1;
			if (conn.protSize == 1) {
				conn.incomingPacket = null;
				if (bool) {
					EmissiveTriangle.affinedClanChannel = null;
				} else {
					Class166.aClass296_Sub54_1692 = null;
				}
				return true;
			}
			if (bool) {
				EmissiveTriangle.affinedClanChannel = new ClanChannel(buff);
			} else {
				Class166.aClass296_Sub54_1692 = new ClanChannel(buff);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (Class99.aClass231_1066 == conn.incomingPacket) {
			for (int i_210_ = 0; PlayerUpdate.visiblePlayers.length > i_210_; i_210_++) {
				if (PlayerUpdate.visiblePlayers[i_210_] != null) {
					PlayerUpdate.visiblePlayers[i_210_].anIntArray6789 = null;
					PlayerUpdate.visiblePlayers[i_210_].aClass44_6802.method549((byte) 115, -1);
				}
			}
			for (int i_211_ = 0; Class368_Sub23.npcNodeCount > i_211_; i_211_++) {
				Class241.npcNodes[i_211_].value.anIntArray6789 = null;
				Class241.npcNodes[i_211_].value.aClass44_6802.method549((byte) 115, -1);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (ha_Sub3.aClass231_4112 == conn.incomingPacket) {
			int i_212_ = buff.readInt_v2();
			int i_213_ = buff.g4();
			BillboardRaw.interfaceCounter();
			Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.get(i_212_);
			Class296_Sub13 class296_sub13_214_ = (Class296_Sub13) Class386.aClass263_3264.get(i_213_);
			if (class296_sub13_214_ != null) {
				Class47.method596(class296_sub13_214_, class296_sub13 == null || class296_sub13_214_.anInt4657 != class296_sub13.anInt4657, false, (byte) 45);
			}
			if (class296_sub13 != null) {
				class296_sub13.unlink();
				Class386.aClass263_3264.put(i_213_, class296_sub13);
			}
			InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(i_212_);
			if (class51 != null) {
				Class332.method3416(class51, (byte) 123);
			}
			class51 = InterfaceComponent.getInterfaceComponent(i_213_);
			if (class51 != null) {
				Class332.method3416(class51, (byte) 123);
				Class61.method695(true, -57, class51);
			}
			if (Class99.anInt1064 != -1) {
				Class296_Sub28.method2686(1, Class99.anInt1064, (byte) 84);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (r.aClass231_6209 == conn.incomingPacket) {
			int i_215_ = buff.readInt_v2();
			BillboardRaw.interfaceCounter();
			Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.get(i_215_);
			if (class296_sub13 != null) {
				Class47.method596(class296_sub13, true, false, (byte) 71);
			}
			if (Player.aClass51_6872 != null) {
				Class332.method3416(Player.aClass51_6872, (byte) 83);
				Player.aClass51_6872 = null;
			}
			conn.incomingPacket = null;
			return true;
		}
		if (GameClient.aClass231_3716 == conn.incomingPacket) {
			int i_216_ = buff.readLEInt();
			boolean bool = buff.readUnsignedByteA() == 1;
			BillboardRaw.interfaceCounter();
			Class119.method1025(i_216_, bool, i + 113008354);
			conn.incomingPacket = null;
			return true;
		}
		if (aa.aClass231_46 == conn.incomingPacket) {
			int i_217_ = buff.readUnsignedLEShort();
			int i_218_ = buff.g1();
			Class16_Sub3_Sub1.configsRegister.setDelayedBITConfig(i_217_, i_218_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class325.aClass231_2866 == conn.incomingPacket) {
			int i_219_ = buff.readInt_v2();
			int i_220_ = buff.readUnsignedShortA();
			int i_221_ = buff.readUnsignedLEShort();
			int i_222_ = buff.readUnsignedByteA();
			BillboardRaw.interfaceCounter();
			Class296_Sub51_Sub14.method3119(1392471456, i_221_, i_219_, i_222_, i_220_);
			conn.incomingPacket = null;
			return true;
		}
		if (World.aClass231_1158 == conn.incomingPacket) {
			Class317.parseSubpacket(Class75.aClass260_869, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (Class368_Sub20.aClass231_5549 == conn.incomingPacket) {
			int i_223_ = buff.g2();
			if (i_223_ == 65535) {
				i_223_ = -1;
			}
			int i_224_ = buff.g1();
			int i_225_ = buff.g2();
			int i_226_ = buff.g1();
			int i_227_ = buff.g2();
			Class338_Sub3_Sub4.method3571(i_226_, false, i_227_, i_225_, i_223_, 93, i_224_);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class41_Sub16.aClass231_3781) {
			int i_228_ = buff.g2();
			String string = buff.gstr();
			if (Class276.anObjectArray2527 == null) {
				Class276.anObjectArray2527 = new Object[StaticMethods.aClass328_6070.anInt2910];
			}
			Class276.anObjectArray2527[i_228_] = string;
			Class219.anIntArray2131[31 & Class366_Sub5.anInt5387++] = i_228_;
			conn.incomingPacket = null;
			return true;
		}
		if (Class296_Sub51_Sub33.aClass231_6522 == conn.incomingPacket) {
			boolean bool = buff.g1() == 1;
			String string = buff.gstr();
			String string_229_ = string;
			if (bool) {
				string_229_ = buff.gstr();
			}
			long l = buff.g2();
			long l_230_ = buff.readUnsignedMedInt();
			int i_231_ = buff.g1();
			int i_232_ = buff.g2();
			long l_233_ = (l << 32) - -l_230_;
			boolean bool_234_ = false;
			while_134_: do {
				for (int i_235_ = 0; i_235_ < 100; i_235_++) {
					if (Class67.aLongArray751[i_235_] == l_233_) {
						bool_234_ = true;
						break while_134_;
					}
				}
				if (i_231_ <= 1 && NPCNode.playerIgnored(string_229_)) {
					bool_234_ = true;
				}
			} while (false);
			if (!bool_234_ && Class47.anInt452 == 0) {
				Class67.aLongArray751[Class162.anInt3558] = l_233_;
				Class162.anInt3558 = (Class162.anInt3558 + 1) % 100;
				String string_236_ = Class338_Sub3_Sub4_Sub1.aClass292_6659.method2413(i_232_, i ^ 0x6bc5ee0).method2839(buff, false);
				if (i_231_ != 2) {
					if (i_231_ != 1) {
						Class296_Sub51_Sub9.method3099(18, string, 0, null, string_236_, false, string, i_232_, string_229_);
					} else {
						Class296_Sub51_Sub9.method3099(18, "<img=0>" + string, 0, null, string_236_, false, string, i_232_, "<img=0>" + string_229_);
					}
				} else {
					Class296_Sub51_Sub9.method3099(18, "<img=1>" + string, 0, null, string_236_, false, string, i_232_, "<img=1>" + string_229_);
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class309.aClass231_2756) { // item1
			int arrayID = buff.g2();
			int bitFlag = buff.g1();
			boolean split = (bitFlag & 0x1) == 1;
			ItemArrayMethods.resetItemArray(arrayID, split);
			int size = buff.g2();
			for (int arrayIndex = 0; arrayIndex < size; arrayIndex++) {
				int itemAmount = buff.g1();
				if (itemAmount == 255) {
					itemAmount = buff.readInt_v1();
				}
				int itemID = buff.readUnsignedLEShortA();
				ItemArrayMethods.updateItemsRegister(arrayID, arrayIndex, itemID - 1, itemAmount, split);
			}
			Class250.itemChangesArray[31 & Class337.itemChanges++] = arrayID;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class316.aClass231_2800) {
			int i_243_ = buff.g1();
			boolean bool = (i_243_ & 0x1) == 1;
			String string = buff.gstr();
			String string_244_ = buff.gstr();
			if (string_244_.equals("")) {
				string_244_ = string;
			}
			String string_245_ = buff.gstr();
			String string_246_ = buff.gstr();
			if (string_246_.equals("")) {
				string_246_ = string_245_;
			}
			if (!bool) {
				Class362.ignoreNames1[Class317.ignoresSize] = string;
				Class296_Sub51_Sub20.ignoreNames2[Class317.ignoresSize] = string_244_;
				Class296_Sub39_Sub11.ignoreNames3[Class317.ignoresSize] = string_245_;
				Class123.ignoreNames4[Class317.ignoresSize] = string_246_;
				StaticMethods.freeIgnoreSlots[Class317.ignoresSize] = (i_243_ & 2) == 2;
				Class317.ignoresSize++;
			} else {
				for (int i_247_ = 0; i_247_ < Class317.ignoresSize; i_247_++) {
					if (Class296_Sub51_Sub20.ignoreNames2[i_247_].equals(string_246_)) {
						Class362.ignoreNames1[i_247_] = string;
						Class296_Sub51_Sub20.ignoreNames2[i_247_] = string_244_;
						Class296_Sub39_Sub11.ignoreNames3[i_247_] = string_245_;
						Class123.ignoreNames4[i_247_] = string_246_;
						break;
					}
				}
			}
			Class156.anInt3595 = Class338_Sub3_Sub3_Sub1.anInt6616;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class272.aClass231_2519) {
			int i_248_ = buff.g2();
			int i_249_ = buff.g4();
			if (Class276.anObjectArray2527 == null) {
				Class276.anObjectArray2527 = new Object[StaticMethods.aClass328_6070.anInt2910];
			}
			Class276.anObjectArray2527[i_248_] = new Integer(i_249_);
			Class219.anIntArray2131[31 & Class366_Sub5.anInt5387++] = i_248_;
			conn.incomingPacket = null;
			return true;
		}
		if (Connection.aClass231_2048 == conn.incomingPacket) {
			int i_250_ = buff.g1();
			if (buff.g1() == 0) {
				Class360_Sub5.aClass208Array5326[i_250_] = new Class208();
			} else {
				buff.pos--;
				Class360_Sub5.aClass208Array5326[i_250_] = new Class208(buff);
			}
			conn.incomingPacket = null;
			StaticMethods.anInt5974 = Class338_Sub3_Sub3_Sub1.anInt6616;
			return true;
		}
		if (conn.incomingPacket == Class366_Sub8.aClass231_5410) { // TODO mark (interfaceSetEnabled)
			int i_251_ = buff.g1();
			int i_252_ = buff.g4();
			BillboardRaw.interfaceCounter();
			Class396.method4072((byte) 15, i_252_, i_251_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class338_Sub9.aClass231_5267 == conn.incomingPacket) {
			String string = buff.gstr();
			int i_253_ = buff.readUnsignedByteC();
			int i_254_ = buff.g1();
			int i_255_ = buff.g2();
			if (i_255_ == 65535) {
				i_255_ = -1;
			}
			if (i_254_ >= 1 && i_254_ <= 8) {
				if (string.equalsIgnoreCase("null")) {
					string = null;
				}
				NodeDeque.aStringArray1591[i_254_ - 1] = string;
				Class296_Sub39_Sub19.anIntArray6252[i_254_ - 1] = i_255_;
				Class294.aBooleanArray2690[i_254_ - 1] = i_253_ == 0;
			}
			conn.incomingPacket = null;
			return true;
		}
		if (Class366_Sub3.aClass231_5373 == conn.incomingPacket) {
			boolean bool = buff.g1() == 1;
			String string = buff.gstr();
			String string_256_ = string;
			if (bool) {
				string_256_ = buff.gstr();
			}
			long l = buff.g2();
			long l_257_ = buff.readUnsignedMedInt();
			int i_258_ = buff.g1();
			long l_259_ = (l << 32) - -l_257_;
			boolean bool_260_ = false;
			while_135_: do {
				for (int i_261_ = 0; i_261_ < 100; i_261_++) {
					if (Class67.aLongArray751[i_261_] == l_259_) {
						bool_260_ = true;
						break while_135_;
					}
				}
				if (i_258_ <= 1) {
					if (Class296_Sub40.somethingWithIgnore0 && !Class41_Sub17.somethingWithIgnore1 || Class123.somethingWithIgnore2) {
						bool_260_ = true;
					} else if (NPCNode.playerIgnored(string_256_)) {
						bool_260_ = true;
					}
				}
			} while (false);
			if (!bool_260_ && Class47.anInt452 == 0) {
				Class67.aLongArray751[Class162.anInt3558] = l_259_;
				Class162.anInt3558 = (Class162.anInt3558 + 1) % 100;
				String string_262_ = ha_Sub1_Sub1.escapeMessage(Class293.readHuffmanMessage(buff));
				if (i_258_ != 2) {
					if (i_258_ != 1) {
						Class296_Sub51_Sub9.method3099(3, string, 0, null, string_262_, false, string, -1, string_256_);
					} else {
						Class296_Sub51_Sub9.method3099(7, "<img=0>" + string, 0, null, string_262_, false, string, -1, "<img=0>" + string_256_);
					}
				} else {
					Class296_Sub51_Sub9.method3099(7, "<img=1>" + string, 0, null, string_262_, false, string, -1, "<img=1>" + string_256_);
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class250.aClass231_2359) {
			buff.pos += 28;
			if (buff.method2618()) {
				Class41_Sub12.method439(buff, buff.pos - 28, i ^ ~0x6bc5e84);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == OutputStream_Sub2.aClass231_43) {
			NPCDefinitionLoader.aByte1402 = buff.readSignedByteC();
			conn.incomingPacket = null;
			if (NPCDefinitionLoader.aByte1402 == 0 || NPCDefinitionLoader.aByte1402 == 1) {
				Class373.aBoolean3178 = true;
			}
			return true;
		}
		if (Class296_Sub20.aClass231_4716 == conn.incomingPacket) {
			Class317.parseSubpacket(Class42_Sub1.aClass260_3832, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (Class292.aClass231_2658 == conn.incomingPacket) {
			int i_263_ = buff.g4();
			Class296_Sub44.aClass278_4949 = Class252.aClass398_2383.method4121(51, i_263_);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class285.aClass231_2622) {
			int i_264_ = buff.readUnsignedShortA();
			byte i_265_ = buff.readSignedByteC();
			BillboardRaw.interfaceCounter();
			Class154_Sub1.setGlobalIntVar(i_265_, i_264_);
			conn.incomingPacket = null;
			return true;
		}
		if (i != -113008285) {
			return true;
		}
		if (Class41_Sub25.aClass231_3805 == conn.incomingPacket) {
			conn.incomingPacket = null;
			Class156.anInt3595 = Class338_Sub3_Sub3_Sub1.anInt6616;
			Class78.anInt3431 = 1;
			return true;
		}
		if (conn.incomingPacket == IncomingPacket.aClass231_4901) { // empty items
			int bitFlag = buff.readUnsignedByteC();
			int arrayID = buff.g2();
			boolean split = (bitFlag & 0x1) == 1;
			ItemArrayMethods.deleteItemsArray(arrayID, split);
			Class250.itemChangesArray[Class337.itemChanges++ & 31] = arrayID;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == MaterialRaw.aClass231_1783) {
			int i_268_ = buff.g2();
			int i_269_ = buff.readUnsignedLEShortA();
			int i_270_ = buff.readInt_v2();
			int i_271_ = buff.readUnsignedLEShort();
			BillboardRaw.interfaceCounter();
			Class296_Sub29.method2688(4, 7, i_269_, i_268_ | i_271_ << 16, i_270_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class326.aClass231_2901 == conn.incomingPacket) {
			Class317.parseSubpacket(SubCache.aClass260_2708, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class360_Sub2.aClass231_5302) {
			int i_272_ = buff.g2();
			int[] is = new int[4];
			for (int i_273_ = 0; i_273_ < 4; i_273_++) {
				is[i_273_] = buff.readUnsignedShortA();
			}
			int i_274_ = buff.readUnsignedByteA();
			NPCNode class296_sub7 = (NPCNode) Class41_Sub18.localNpcs.get(i_272_);
			if (class296_sub7 != null) {
				Class160.method1619(i_274_, 0, true, is, class296_sub7.value);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == ObjectDefinitionLoader.aClass231_374) {
			if (BITConfigDefinition.method2353(-8, Class366_Sub6.anInt5392)) {
				Class379_Sub2_Sub1.anInt6607 = (int) (buff.g2() * 2.5F);
			} else {
				Class379_Sub2_Sub1.anInt6607 = buff.g2() * 30;
			}
			conn.incomingPacket = null;
			Class308.anInt2753 = Class338_Sub3_Sub3_Sub1.anInt6616;
			return true;
		}
		if (Class87.aClass231_943 == conn.incomingPacket) {
			boolean bool = buff.g1() == 1;
			String string = buff.gstr();
			String string_275_ = string;
			if (bool) {
				string_275_ = buff.gstr();
			}
			int i_276_ = buff.g1();
			boolean bool_277_ = false;
			if (i_276_ <= 1) {
				if (Class296_Sub40.somethingWithIgnore0 && !Class41_Sub17.somethingWithIgnore1 || Class123.somethingWithIgnore2) {
					bool_277_ = true;
				} else if (i_276_ <= 1 && NPCNode.playerIgnored(string_275_)) {
					bool_277_ = true;
				}
			}
			if (!bool_277_ && Class47.anInt452 == 0) {
				String string_278_ = ha_Sub1_Sub1.escapeMessage(Class293.readHuffmanMessage(buff));
				if (i_276_ == 2) {
					Class296_Sub51_Sub9.method3099(24, "<img=1>" + string, 0, null, string_278_, false, string, -1, "<img=1>" + string_275_);
				} else if (i_276_ != 1) {
					Class296_Sub51_Sub9.method3099(24, string, 0, null, string_278_, false, string, -1, string_275_);
				} else {
					Class296_Sub51_Sub9.method3099(24, "<img=0>" + string, 0, null, string_278_, false, string, -1, "<img=0>" + string_275_);
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == StaticMethods.aClass231_6076) {
			int i_279_ = buff.g2();
			int i_280_ = buff.readInt_v2();
			int i_281_ = buff.readUnsignedLEShortA();
			BillboardRaw.interfaceCounter();
			Class289.method2398(i_280_, -16526, i_279_ + (i_281_ << 16));
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class338_Sub3_Sub4_Sub1.aClass231_6657) {
			int i_282_ = buff.readUnsignedLEShort();
			BillboardRaw.interfaceCounter();
			Class108.method949(i_282_, 11771);
			conn.incomingPacket = null;
			return true;
		}
		if (Class252.aClass231_2384 == conn.incomingPacket) {
			String string = buff.gstr();
			boolean bool = buff.g1() == 1;
			String string_283_;
			if (bool) {
				string_283_ = buff.gstr();
			} else {
				string_283_ = string;
			}
			int i_284_ = buff.g2();
			byte i_285_ = buff.g1b();
			boolean bool_286_ = false;
			if (i_285_ == -128) {
				bool_286_ = true;
			}
			if (bool_286_) {
				if (Class271.anInt2512 == 0) {
					conn.incomingPacket = null;
					return true;
				}
				boolean bool_287_ = false;
				int i_288_;
				for (i_288_ = 0; Class271.anInt2512 > i_288_; i_288_++) {
					if (Class338_Sub8.aClass94Array5257[i_288_].aString1019.equals(string_283_) && i_284_ == Class338_Sub8.aClass94Array5257[i_288_].anInt1011) {
						break;
					}
				}
				if (i_288_ < Class271.anInt2512) {
					for (/**/; i_288_ < Class271.anInt2512 - 1; i_288_++) {
						Class338_Sub8.aClass94Array5257[i_288_] = Class338_Sub8.aClass94Array5257[i_288_ + 1];
					}
					Class271.anInt2512--;
					Class338_Sub8.aClass94Array5257[Class271.anInt2512] = null;
				}
			} else {
				String string_289_ = buff.gstr();
				Class94 class94 = new Class94();
				class94.aString1018 = string;
				class94.aString1019 = string_283_;
				class94.aString1015 = Class296_Sub49.parseName(class94.aString1019);
				class94.aString1013 = string_289_;
				class94.aByte1014 = i_285_;
				class94.anInt1011 = i_284_;
				int i_290_;
				for (i_290_ = Class271.anInt2512 - 1; i_290_ >= 0; i_290_--) {
					int i_291_ = Class338_Sub8.aClass94Array5257[i_290_].aString1015.compareTo(class94.aString1015);
					if (i_291_ == 0) {
						Class338_Sub8.aClass94Array5257[i_290_].anInt1011 = i_284_;
						Class338_Sub8.aClass94Array5257[i_290_].aByte1014 = i_285_;
						Class338_Sub8.aClass94Array5257[i_290_].aString1013 = string_289_;
						if (string_283_.equals(Class296_Sub51_Sub11.localPlayer.displayName)) {
							Class296_Sub39_Sub10.aByte6178 = i_285_;
						}
						conn.incomingPacket = null;
						Class296_Sub15_Sub3.anInt6012 = Class338_Sub3_Sub3_Sub1.anInt6616;
						return true;
					}
					if (i_291_ < 0) {
						break;
					}
				}
				if (Class271.anInt2512 >= Class338_Sub8.aClass94Array5257.length) {
					conn.incomingPacket = null;
					return true;
				}
				for (int i_292_ = Class271.anInt2512 - 1; i_290_ < i_292_; i_292_--) {
					Class338_Sub8.aClass94Array5257[i_292_ + 1] = Class338_Sub8.aClass94Array5257[i_292_];
				}
				if (Class271.anInt2512 == 0) {
					Class338_Sub8.aClass94Array5257 = new Class94[100];
				}
				Class338_Sub8.aClass94Array5257[i_290_ + 1] = class94;
				Class271.anInt2512++;
				if (string_283_.equals(Class296_Sub51_Sub11.localPlayer.displayName)) {
					Class296_Sub39_Sub10.aByte6178 = i_285_;
				}
			}
			conn.incomingPacket = null;
			Class296_Sub15_Sub3.anInt6012 = Class338_Sub3_Sub3_Sub1.anInt6616;
			return true;
		}
		if (conn.incomingPacket == Class159.aClass231_1665) {
			int i_293_ = buff.readUnsignedLEShort();
			int i_294_ = buff.readLEInt();
			int i_295_ = buff.readUnsignedByteA();
			int i_296_ = buff.readUnsignedByteC();
			int i_297_ = buff.readUnsignedShortA();
			int i_298_ = buff.g2();
			if (i_298_ == 65535) {
				i_298_ = -1;
			}
			int i_299_ = i_295_ & 0x7;
			int i_300_ = (i_295_ & 0x7f) >> 3;
			if (i_300_ == 15) {
				i_300_ = -1;
			}
			boolean bool = (i_295_ >> 7 & 0x1) == 1;
			if (i_294_ >> 30 != 0) {
				int i_301_ = i_294_ >> 28 & 0x3;
				int i_302_ = ((i_294_ & 0xfffcbb3) >> 14) - Class206.worldBaseX;
				int i_303_ = (i_294_ & 0x3fff) - Class41_Sub26.worldBaseY;
				if (i_302_ >= 0 && i_303_ >= 0 && i_302_ < Class198.currentMapSizeX && Class296_Sub38.currentMapSizeY > i_303_) {
					if (i_298_ == -1) {
						Class296_Sub39_Sub11 class296_sub39_sub11 = (Class296_Sub39_Sub11) Class134.aClass263_1385.get(i_303_ | i_302_ << 16);
						if (class296_sub39_sub11 != null) {
							class296_sub39_sub11.aClass338_Sub3_Sub1_Sub5_6190.method3545((byte) 108);
							class296_sub39_sub11.unlink();
						}
					} else {
						int i_304_ = i_302_ * 512 + 256;
						int i_305_ = i_303_ * 512 + 256;
						int i_306_ = i_301_;
						if (i_306_ < 3 && r_Sub2.method2871(i_303_, i_302_, (byte) -47)) {
							i_306_++;
						}
						Class338_Sub3_Sub1_Sub5 class338_sub3_sub1_sub5 = new Class338_Sub3_Sub1_Sub5(i_298_, i_293_, i_301_, i_306_, i_304_, aa_Sub1.method155(-1537652855, i_301_, i_304_, i_305_) - i_297_, i_305_, i_302_, i_302_, i_303_, i_303_, i_299_, bool);
						Class134.aClass263_1385.put(i_302_ << 16 | i_303_, new Class296_Sub39_Sub11(class338_sub3_sub1_sub5));
					}
				}
			} else if (i_294_ >> 29 == 0) {
				if (i_294_ >> 28 != 0) {
					int i_307_ = i_294_ & 0xffff;
					Player class338_sub3_sub1_sub3_sub1;
					if (i_307_ != Class362.myPlayerIndex) {
						class338_sub3_sub1_sub3_sub1 = PlayerUpdate.visiblePlayers[i_307_];
					} else {
						class338_sub3_sub1_sub3_sub1 = Class296_Sub51_Sub11.localPlayer;
					}
					if (class338_sub3_sub1_sub3_sub1 != null) {
						Class212 class212 = class338_sub3_sub1_sub3_sub1.aClass212Array6817[i_296_];
						if (i_298_ == 65535) {
							i_298_ = -1;
						}
						boolean bool_308_ = true;
						int i_309_ = class212.anInt2105;
						if (i_298_ != -1 && i_309_ != -1) {
							if (i_309_ == i_298_) {
								Graphic class23 = Class157.graphicsLoader.getGraphic(i_298_);
								if (class23.aBoolean267 && class23.anInt264 != -1) {
									Animation class43 = Class296_Sub51_Sub13.animationsLoader.getAnimation(class23.anInt264, (byte) -124);
									int i_310_ = class43.anInt410;
									if (i_310_ == 0 || i_310_ == 2) {
										bool_308_ = false;
									} else if (i_310_ == 1) {
										bool_308_ = true;
									}
								}
							} else {
								Graphic class23 = Class157.graphicsLoader.getGraphic(i_298_);
								Graphic class23_311_ = Class157.graphicsLoader.getGraphic(i_309_);
								if (class23.anInt264 != -1 && class23_311_.anInt264 != -1) {
									Animation class43 = Class296_Sub51_Sub13.animationsLoader.getAnimation(class23.anInt264, (byte) -124);
									Animation class43_312_ = Class296_Sub51_Sub13.animationsLoader.getAnimation(class23_311_.anInt264, (byte) -124);
									if (class43.anInt408 < class43_312_.anInt408) {
										bool_308_ = false;
									}
								}
							}
						}
						if (bool_308_) {
							class212.anInt2105 = i_298_;
							class212.anInt2104 = i_299_;
							class212.anInt2106 = i_297_;
							class212.anInt2107 = i_300_;
							if (i_298_ == -1) {
								class212.aClass44_2108.method549((byte) 115, -1);
							} else {
								Graphic class23 = Class157.graphicsLoader.getGraphic(i_298_);
								int i_313_ = !class23.aBoolean267 ? 2 : 0;
								if (bool) {
									i_313_ = 1;
								}
								class212.aClass44_2108.method548(i_293_, i_313_, class23.anInt264, false, 21847);
							}
						}
					}
				}
			} else {
				int i_314_ = i_294_ & 0xffff;
				NPCNode class296_sub7 = (NPCNode) Class41_Sub18.localNpcs.get(i_314_);
				if (class296_sub7 != null) {
					NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
					if (i_298_ == 65535) {
						i_298_ = -1;
					}
					Class212 class212 = class338_sub3_sub1_sub3_sub2.aClass212Array6817[i_296_];
					boolean bool_315_ = true;
					int i_316_ = class212.anInt2105;
					if (i_298_ != -1 && i_316_ != -1) {
						if (i_298_ == i_316_) {
							Graphic class23 = Class157.graphicsLoader.getGraphic(i_298_);
							if (class23.aBoolean267 && class23.anInt264 != -1) {
								Animation class43 = Class296_Sub51_Sub13.animationsLoader.getAnimation(class23.anInt264, (byte) -124);
								int i_317_ = class43.anInt410;
								if (i_317_ != 0 && i_317_ != 2) {
									if (i_317_ == 1) {
										bool_315_ = true;
									}
								} else {
									bool_315_ = false;
								}
							}
						} else {
							Graphic class23 = Class157.graphicsLoader.getGraphic(i_298_);
							Graphic class23_318_ = Class157.graphicsLoader.getGraphic(i_316_);
							if (class23.anInt264 != -1 && class23_318_.anInt264 != -1) {
								Animation class43 = Class296_Sub51_Sub13.animationsLoader.getAnimation(class23.anInt264, (byte) -124);
								Animation class43_319_ = Class296_Sub51_Sub13.animationsLoader.getAnimation(class23_318_.anInt264, (byte) -124);
								if (class43.anInt408 < class43_319_.anInt408) {
									bool_315_ = false;
								}
							}
						}
					}
					if (bool_315_) {
						class212.anInt2106 = i_297_;
						class212.anInt2107 = i_300_;
						class212.anInt2105 = i_298_;
						if (i_298_ == -1) {
							class212.aClass44_2108.method549((byte) 115, -1);
						} else {
							Graphic class23 = Class157.graphicsLoader.getGraphic(i_298_);
							int i_320_ = class23.aBoolean267 ? 0 : 2;
							if (bool) {
								i_320_ = 1;
							}
							class212.aClass44_2108.method548(i_293_, i_320_, class23.anInt264, false, 21847);
						}
					}
				}
			}
			conn.incomingPacket = null;
			return true;
		}
		if (Class228.aClass231_2197 == conn.incomingPacket) {
			Class317.parseSubpacket(Class338_Sub8.aClass260_5255, (byte) 15);
			conn.incomingPacket = null;
			return true;
		}
		if (IncomingPacket.CLANSETTINGS_DELTA == conn.incomingPacket) {
			Class296_Sub39_Sub20_Sub2.anInt6727 = Class338_Sub3_Sub3_Sub1.anInt6616;
			boolean affined = buff.g1() == 1;
			ClanSettingsDelta delta = new ClanSettingsDelta(buff);
			ClanSettings setings;
			if (!affined) {
				setings = Class368_Sub2.unaffinedClanSettings;
			} else {
				setings = Class296_Sub51_Sub2.affinedClanSettings;
			}
			delta.applyToClanSettings(setings);
			conn.incomingPacket = null;
			return true;
		}
		if (Class41_Sub29.aClass231_3819 == conn.incomingPacket) {
			int i_321_ = buff.g4();
			int i_322_ = buff.g2();
			BillboardRaw.interfaceCounter();
			if (i_322_ == 65535) {
				i_322_ = -1;
			}
			Class41_Sub20.method474(i_322_, 4, i_321_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class151.aClass231_1551 == conn.incomingPacket) {
			int i_323_ = buff.readInt_v2();
			int i_324_ = buff.method2557();
			int i_325_ = buff.method2557();
			BillboardRaw.interfaceCounter();
			Class41_Sub17.method458(i_325_, i_323_, 11, i_324_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class122_Sub1_Sub1.aClass231_6601 == conn.incomingPacket) {
			int i_326_ = buff.readInt_v2();
			int i_327_ = buff.g2();
			BillboardRaw.interfaceCounter();
			Class296_Sub36.method2762(i_327_, (byte) 126, i_326_);
			conn.incomingPacket = null;
			return true;
		}
		if (Class326.aClass231_2904 == conn.incomingPacket) {
			Class130.method1380(-1126756703, buff.gstr());
			conn.incomingPacket = null;
			return true;
		}
		if (Class316.aClass231_2795 == conn.incomingPacket) {
			boolean bool = buff.readUnsignedByteS() == 1;
			BillboardRaw.interfaceCounter();
			StaticMethods.aBoolean2913 = bool;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == IncomingPacket.aClass231_1609) {
			if (Class99.anInt1064 != -1) {
				Class296_Sub28.method2686(0, Class99.anInt1064, (byte) 84);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (Class368_Sub20.aClass231_5547 == conn.incomingPacket) {
			BillboardRaw.interfaceCounter();
			BillboardRaw.method883((byte) -127);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class205_Sub1.aClass231_5638) {
			int i_328_ = buff.readInt_v1();
			if (i_328_ != Class366_Sub2.anInt5369) {
				Class366_Sub2.anInt5369 = i_328_;
				CS2Executor.method1520(Class324.aClass81_2856, -1, -1);
			}
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class296_Sub20.aClass231_4717) {
			Class317.ignoresSize = buff.g1();
			for (int i_329_ = 0; Class317.ignoresSize > i_329_; i_329_++) {
				Class362.ignoreNames1[i_329_] = buff.gstr();
				Class296_Sub51_Sub20.ignoreNames2[i_329_] = buff.gstr();
				if (Class296_Sub51_Sub20.ignoreNames2[i_329_].equals("")) {
					Class296_Sub51_Sub20.ignoreNames2[i_329_] = Class362.ignoreNames1[i_329_];
				}
				Class296_Sub39_Sub11.ignoreNames3[i_329_] = buff.gstr();
				Class123.ignoreNames4[i_329_] = buff.gstr();
				if (Class123.ignoreNames4[i_329_].equals("")) {
					Class123.ignoreNames4[i_329_] = Class296_Sub39_Sub11.ignoreNames3[i_329_];
				}
				StaticMethods.freeIgnoreSlots[i_329_] = false;
			}
			conn.incomingPacket = null;
			Class156.anInt3595 = Class338_Sub3_Sub3_Sub1.anInt6616;
			return true;
		}
		if (conn.incomingPacket == Class296_Sub40.aClass231_4909) {
			conn.incomingPacket = null;
			Class276.anObjectArray2527 = null;
			return true;
		}
		if (conn.incomingPacket == Class361.aClass231_3104) {
			int i_330_ = buff.readUnsignedByteC();
			int i_331_ = buff.g1();
			int i_332_ = buff.readInt_v2();
			ClothDefinition.anIntArray3016[i_331_] = i_332_;
			StaticMethods.anIntArray5930[i_331_] = i_330_;
			EffectiveVertex.anIntArray2215[i_331_] = 1;
			int i_333_ = Class14.anIntArray173[i_331_] - 1;
			for (int i_334_ = 0; i_334_ < i_333_; i_334_++) {
				if (i_332_ >= Class338_Sub3_Sub5_Sub2.anIntArray6662[i_334_]) {
					EffectiveVertex.anIntArray2215[i_331_] = i_334_ + 2;
				}
			}
			Class65.anIntArray733[31 & Class14.anInt155++] = i_331_;
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == IncomingPacket.aClass231_5916) {
			int height = buff.g2() << 2;
			int speed = buff.readUnsignedByteS();
			int velocity = buff.g1();
			int localx = buff.readUnsignedByteA();
			int localy = buff.readUnsignedByteC();
			BillboardRaw.interfaceCounter();
			Class296_Sub51_Sub32.method3178(localx, localy, height, speed, velocity, false, true);
			conn.incomingPacket = null;
			return true;
		}
		if (Class103.aClass231_1087 == conn.incomingPacket) {
			int fileID = buff.g2();
			Class274.anInt2523 = -1;
			za_Sub1.anInt6554 = 1;
			Class399.someXTEARelatedFileID = fileID;
			Class134.fs35.hasEntryBuffer(Class399.someXTEARelatedFileID);
			int xteasCount = buff.g2();
			Class296_Sub36.someSecondaryXteaBuffer = new int[xteasCount][4];
			for (int a = 0; xteasCount > a; a++) {
				for (int b = 0; b < 4; b++) {
					Class296_Sub36.someSecondaryXteaBuffer[a][b] = buff.g4();
				}
			}
			int len = buff.g1();
			Class187.someAppereanceBuffer = new Packet(len);
			Class187.someAppereanceBuffer.writeBytes(buff.data, buff.pos, len);
			conn.incomingPacket = null;
			buff.pos += len;
			return false;
		}
		if (Class44_Sub2.aClass231_3861 == conn.incomingPacket) {
			int i_345_ = buff.g4();
			int i_346_ = buff.readUnsignedShortA();
			if (i_346_ == 65535) {
				i_346_ = -1;
			}
			int i_347_ = buff.readInt_v1();
			BillboardRaw.interfaceCounter();
			Class401.method4144((byte) 122, i_347_, i_345_, i_346_);
			ObjType class158 = Class296_Sub39_Sub1.itemDefinitionLoader.list(i_346_);
			Class360_Sub6.method3748(class158.xAngle2d, class158.yAngle2d, (byte) 73, i_347_, class158.zoom2d);
			AdvancedMemoryCache.method986(i_347_, class158.xOffest2d, class158.yOffest2d, 10, class158.zAngle2d);
			conn.incomingPacket = null;
			return true;
		}
		if (conn.incomingPacket == Class67.aClass231_750) {
			Class388.anInt3275 = buff.g2b();
			Class308.anInt2753 = Class338_Sub3_Sub3_Sub1.anInt6616;
			conn.incomingPacket = null;
			return true;
		}
		if (Class338_Sub9.aClass231_5263 == conn.incomingPacket) {
			int i_348_ = buff.g4();
			int i_349_ = buff.g4();
			Class296_Sub1 class296_sub1 = Class253.allocateOut(conn.aClass185_2053, (byte) 120, OutgoingPacket.aClass311_4628);
			class296_sub1.out.p4(i_348_);
			class296_sub1.out.p4(i_349_);
			conn.sendPacket(class296_sub1, (byte) 119);
			conn.incomingPacket = null;
			return true;
		}
		if (Class296_Sub51_Sub4.aClass231_6356 == conn.incomingPacket) {
			byte i_350_ = buff.readSignedByteC();
			int i_351_ = buff.readUnsignedByteC();
			BillboardRaw.interfaceCounter();
			StaticMethods.method1009(i_351_, i_350_, (byte) 6);
			conn.incomingPacket = null;
			return true;
		}
		Class219_Sub1.method2062("T1 - " + (conn.incomingPacket == null ? -1 : conn.incomingPacket.getOpcode()) + "," + (conn.incomingPacket1 != null ? conn.incomingPacket1.getOpcode() : -1) + "," + (conn.incomingPacket3 != null ? conn.incomingPacket3.getOpcode() : -1) + " - " + conn.protSize, (byte) 82, null);
		StringNode.logout(i ^ ~0x6bc5e9f, false);
		return true;
	}

}
