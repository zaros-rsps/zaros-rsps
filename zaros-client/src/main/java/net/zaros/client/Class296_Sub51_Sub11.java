package net.zaros.client;

/* Class296_Sub51_Sub11 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub11 extends TextureOperation {
	private int anInt6397;
	private int anInt6398 = 1;
	static HashTable aClass263_6399 = new HashTable(4);
	static Player localPlayer;
	static Class161 aClass161_6401 = new Class161(1, 2, 2, 0);
	static int anInt6402 = 0;

	public Class296_Sub51_Sub11() {
		super(1, false);
		anInt6397 = 1;
	}

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		int i_1_ = i_0_;
		while_108_ : do {
			do {
				if (i_1_ != 0) {
					if (i_1_ != 1) {
						if (i_1_ == 2)
							break;
						break while_108_;
					}
				} else {
					anInt6397 = class296_sub17.g1();
					break while_108_;
				}
				anInt6398 = class296_sub17.g1();
				break while_108_;
			} while (false);
			monochromatic = class296_sub17.g1() == 1;
		} while (false);
		if (i >= -84)
			method3071(57, null, -7);
	}

	public static void method3106(int i) {
		localPlayer = null;
		if (i == 0) {
			aClass263_6399 = null;
			aClass161_6401 = null;
		}
	}

	final int[][] get_colour_output(int i, int i_2_) {
		int[][] is = aClass86_5034.method823((byte) 59, i);
		if (i_2_ != 17621)
			anInt6397 = -101;
		if (aClass86_5034.aBoolean939) {
			int i_3_ = anInt6398 + (anInt6398 + 1);
			int i_4_ = 65536 / i_3_;
			int i_5_ = anInt6397 + 1 + anInt6397;
			int i_6_ = 65536 / i_5_;
			int[][][] is_7_ = new int[i_3_][][];
			for (int i_8_ = i - anInt6398; i + anInt6398 >= i_8_; i_8_++) {
				int[][] is_9_ = this.method3075((byte) 122, 0, Class67.anInt753 & i_8_);
				int[][] is_10_ = new int[3][Class41_Sub10.anInt3769];
				int i_11_ = 0;
				int i_12_ = 0;
				int i_13_ = 0;
				int[] is_14_ = is_9_[0];
				int[] is_15_ = is_9_[1];
				int[] is_16_ = is_9_[2];
				for (int i_17_ = -anInt6397; anInt6397 >= i_17_; i_17_++) {
					int i_18_ = i_17_ & Class41_Sub25.anInt3803;
					i_11_ += is_14_[i_18_];
					i_12_ += is_15_[i_18_];
					i_13_ += is_16_[i_18_];
				}
				int[] is_19_ = is_10_[0];
				int[] is_20_ = is_10_[1];
				int[] is_21_ = is_10_[2];
				int i_22_ = 0;
				while (i_22_ < Class41_Sub10.anInt3769) {
					is_19_[i_22_] = i_6_ * i_11_ >> 16;
					is_20_[i_22_] = i_6_ * i_12_ >> 16;
					is_21_[i_22_] = i_13_ * i_6_ >> 16;
					int i_23_ = Class41_Sub25.anInt3803 & -anInt6397 + i_22_;
					i_12_ -= is_15_[i_23_];
					i_13_ -= is_16_[i_23_];
					i_22_++;
					i_11_ -= is_14_[i_23_];
					i_23_ = i_22_ + anInt6397 & Class41_Sub25.anInt3803;
					i_12_ += is_15_[i_23_];
					i_11_ += is_14_[i_23_];
					i_13_ += is_16_[i_23_];
				}
				is_7_[anInt6398 + i_8_ - i] = is_10_;
			}
			int[] is_24_ = is[0];
			int[] is_25_ = is[1];
			int[] is_26_ = is[2];
			for (int i_27_ = 0; Class41_Sub10.anInt3769 > i_27_; i_27_++) {
				int i_28_ = 0;
				int i_29_ = 0;
				int i_30_ = 0;
				for (int i_31_ = 0; i_31_ < i_3_; i_31_++) {
					int[][] is_32_ = is_7_[i_31_];
					i_29_ += is_32_[1][i_27_];
					i_30_ += is_32_[2][i_27_];
					i_28_ += is_32_[0][i_27_];
				}
				is_24_[i_27_] = i_28_ * i_4_ >> 16;
				is_25_[i_27_] = i_29_ * i_4_ >> 16;
				is_26_[i_27_] = i_30_ * i_4_ >> 16;
			}
		}
		return is;
	}

	final int[] get_monochrome_output(int i, int i_33_) {
		if (i != 0)
			get_monochrome_output(37, -49);
		int[] is = aClass318_5035.method3335(i_33_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int i_34_ = anInt6398 + anInt6398 + 1;
			int i_35_ = 65536 / i_34_;
			int i_36_ = anInt6397 + 1 + anInt6397;
			int i_37_ = 65536 / i_36_;
			int[][] is_38_ = new int[i_34_][];
			for (int i_39_ = -anInt6398 + i_33_; i_39_ <= i_33_ + anInt6398; i_39_++) {
				int[] is_40_ = this.method3064(0, 0, Class67.anInt753 & i_39_);
				int[] is_41_ = new int[Class41_Sub10.anInt3769];
				int i_42_ = 0;
				for (int i_43_ = -anInt6397; i_43_ <= anInt6397; i_43_++)
					i_42_ += is_40_[Class41_Sub25.anInt3803 & i_43_];
				int i_44_ = 0;
				while (Class41_Sub10.anInt3769 > i_44_) {
					is_41_[i_44_] = i_37_ * i_42_ >> 16;
					i_42_ -= is_40_[Class41_Sub25.anInt3803 & i_44_ - anInt6397];
					i_44_++;
					i_42_ += is_40_[Class41_Sub25.anInt3803 & i_44_ + anInt6397];
				}
				is_38_[-i_33_ + anInt6398 + i_39_] = is_41_;
			}
			for (int i_45_ = 0; i_45_ < Class41_Sub10.anInt3769; i_45_++) {
				int i_46_ = 0;
				for (int i_47_ = 0; i_47_ < i_34_; i_47_++)
					i_46_ += is_38_[i_47_][i_45_];
				is[i_45_] = i_46_ * i_35_ >> 16;
			}
		}
		return is;
	}
}
