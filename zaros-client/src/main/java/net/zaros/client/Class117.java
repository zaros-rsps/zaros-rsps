package net.zaros.client;

/* Class117 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class117 {
	private AdvancedMemoryCache aClass113_1184 = new AdvancedMemoryCache(128);
	static IncomingPacket aClass231_1185;
	private Js5 aClass138_1186;
	static int anInt1187 = 0;

	static final int method1015(byte i, String string) {
		if (i > -25)
			aClass231_1185 = null;
		return string.length() + 1;
	}

	public static void method1016(byte i) {
		int i_0_ = 46 / ((-69 - i) / 54);
		aClass231_1185 = null;
	}

	final void method1017(int i) {
		if (i >= -44)
			method1018((byte) 50);
		synchronized (aClass113_1184) {
			aClass113_1184.clear();
		}
	}

	final void method1018(byte i) {
		synchronized (aClass113_1184) {
			int i_1_ = -63 / ((70 - i) / 47);
			aClass113_1184.clearSoftReferences();
		}
	}

	Class117(GameType class35, int i, Js5 class138) {
		aClass138_1186 = class138;
		aClass138_1186.getLastFileId(1);
	}

	final void method1019(int i, byte i_2_) {
		if (i_2_ != -124)
			method1016((byte) 2);
		synchronized (aClass113_1184) {
			aClass113_1184.clean(i);
		}
	}

	final Class236 method1020(boolean bool, int i) {
		Class236 class236;
		synchronized (aClass113_1184) {
			class236 = (Class236) aClass113_1184.get((long) i);
		}
		if (class236 != null)
			return class236;
		byte[] is;
		synchronized (aClass138_1186) {
			is = aClass138_1186.getFile(1, i);
		}
		class236 = new Class236();
		if (is != null)
			class236.method2130((byte) 121, new Packet(is));
		synchronized (aClass113_1184) {
			aClass113_1184.put(class236, (long) i);
			if (bool != true)
				method1015((byte) -77, null);
		}
		return class236;
	}

	static {
		aClass231_1185 = new IncomingPacket(41, -2);
	}
}
