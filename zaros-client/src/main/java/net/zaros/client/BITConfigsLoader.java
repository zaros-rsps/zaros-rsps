package net.zaros.client;

/* Class229 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class BITConfigsLoader {
	static ModeWhere liveModeWhere = new ModeWhere("LIVE", "", "", 0);
	private AdvancedMemoryCache cachedDefinitions = new AdvancedMemoryCache(64);
	private Js5 bitConfigsCache;
	static Class327 aClass327_2205;
	static OutgoingPacket aClass311_2206 = new OutgoingPacket(61, 3);
	static int[] anIntArray2207 = new int[32];

	final BITConfigDefinition load(int configID) {
		BITConfigDefinition definition;
		synchronized (cachedDefinitions) {
			definition = (BITConfigDefinition) cachedDefinitions.get((long) configID);
		}
		if (definition != null)
			return definition;
		byte[] data;
		synchronized (bitConfigsCache) {
			data = bitConfigsCache.getFile(Class32_Sub1.getFileID((byte) 41, configID), Class219_Sub1.getChildID((byte) -44, configID));
		}
		definition = new BITConfigDefinition();
		if (data != null)
			definition.init(new Packet(data), (byte) 84);
		synchronized (cachedDefinitions) {
			cachedDefinitions.put(definition, (long) configID);
		}
		return definition;
	}

	public static void method2098(int i) {
		anIntArray2207 = null;
		liveModeWhere = null;
		if (i != 32)
			aClass327_2205 = null;
		aClass311_2206 = null;
		aClass327_2205 = null;
	}

	final void clear(int i) {
		synchronized (cachedDefinitions) {
			if (i <= 30) {
				/* empty */
			} else
				cachedDefinitions.clear();
		}
	}

	static final int method2100(Class296_Sub39_Sub9 class296_sub39_sub9, int i) {
		String string = Class241_Sub1_Sub1.method2155(false, class296_sub39_sub9);
		int[] is = null;
		if (Class295_Sub1.method2424(class296_sub39_sub9.anInt6165, (byte) 101))
			is = (Class296_Sub39_Sub1.itemDefinitionLoader.list((int) class296_sub39_sub9.aLong6162).quests);
		else if (class296_sub39_sub9.anInt6167 == -1) {
			if (!Class50.method609(true, class296_sub39_sub9.anInt6165)) {
				if (Class234.method2124((byte) 51, class296_sub39_sub9.anInt6165)) {
					ObjectDefinition class70 = (Class379.objectDefinitionLoader.getObjectDefinition((int) (class296_sub39_sub9.aLong6162 >>> 32 & 0x7fffffffL)));
					if (class70.transforms != null)
						class70 = class70.method757((Class16_Sub3_Sub1.configsRegister), false);
					if (class70 != null)
						is = class70.anIntArray783;
				}
			} else {
				NPCNode class296_sub7 = ((NPCNode) (Class41_Sub18.localNpcs.get((long) (int) class296_sub39_sub9.aLong6162)));
				if (class296_sub7 != null) {
					NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
					NPCDefinition class147 = class338_sub3_sub1_sub3_sub2.definition;
					if (class147.configData != null)
						class147 = class147.getNPCDefinitionByConfigID((Class16_Sub3_Sub1.configsRegister));
					if (class147 != null)
						is = class147.anIntArray1499;
				}
			}
		} else
			is = (Class296_Sub39_Sub1.itemDefinitionLoader.list(class296_sub39_sub9.anInt6167).quests);
		if (is != null)
			string += Class338_Sub3_Sub1_Sub1.method3484((byte) 63, is);
		int i_1_ = Class304.aClass92_2729.method847(Class44_Sub1_Sub1.aClass397Array5811, string, i);
		if (class296_sub39_sub9.aBoolean6170)
			i_1_ += Class296_Sub2.aClass397_4589.method4087() + 4;
		return i_1_;
	}

	final void updateReferences(int i, int i_2_) {
		if (i_2_ == 255) {
			synchronized (cachedDefinitions) {
				cachedDefinitions.clean(i);
			}
		}
	}

	static final int method2102(int i, int i_3_, int i_4_, int i_5_) {
		int i_6_ = -47 / ((i_3_ - 45) / 62);
		int i_7_ = -i + 255;
		i_5_ = ((i_5_ & 0xff00ff) * i & ~0xff00ff | (i_5_ & 0xff00) * i & 0xff0000) >>> 8;
		return ((((i_4_ & 0xff00ff) * i_7_ & ~0xff00ff | (i_4_ & 0xff00) * i_7_ & 0xff0000) >>> 8) + i_5_);
	}

	final void clearSoftReferences(int i) {
		synchronized (cachedDefinitions) {
			cachedDefinitions.clearSoftReferences();
			int i_8_ = 81 / ((42 - i) / 49);
		}
	}

	BITConfigsLoader(GameType class35, int i, Js5 class138) {
		bitConfigsCache = class138;
		if (bitConfigsCache != null) {
			int i_9_ = bitConfigsCache.getLastGroupId() - 1;
			bitConfigsCache.getLastFileId(i_9_);
		}
	}

	final void method2104(int i, int i_10_) {
		if (i_10_ == 1) {
			synchronized (cachedDefinitions) {
				cachedDefinitions.clear();
				cachedDefinitions = new AdvancedMemoryCache(i);
			}
		}
	}
}
