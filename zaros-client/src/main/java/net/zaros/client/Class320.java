package net.zaros.client;

/* Class320 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class320 {
	static OutgoingPacket aClass311_2820;
	static float[] aFloatArray2821 = { 0.0F, -1.0F, 0.0F, 0.0F };
	static boolean aBoolean2822;

	public static void method3339(int i) {
		aFloatArray2821 = null;
		if (i == 16711680) {
			aClass311_2820 = null;
		}
	}

	static final void method3340(int i, int i_0_, int i_1_) {
		boolean bool = Class338_Sub2.aClass247ArrayArrayArray5195[0][i_0_][i_1_] != null && Class338_Sub2.aClass247ArrayArrayArray5195[0][i_0_][i_1_].aClass247_2338 != null;
		for (int i_2_ = i; i_2_ >= 0; i_2_--) {
			if (Class338_Sub2.aClass247ArrayArrayArray5195[i_2_][i_0_][i_1_] == null) {
				Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i_2_][i_0_][i_1_] = new Class247(i_2_);
				if (bool) {
					class247.aByte2349++;
				}
			}
		}
	}

	static final void method3341(boolean bool) {
		for (int i = 0; Class77.anInt880 > i; i++) {
			Class116.aClass324Array3678[i] = null;
		}
		if (bool) {
			method3342(null, 23, null, -29, -99, false, 124);
		}
		Class77.anInt880 = 0;
		for (int i = 0; i < Class368_Sub9.anInt5477; i++) {
			for (int i_3_ = 0; i_3_ < Class228.anInt2201; i_3_++) {
				for (int i_4_ = 0; i_4_ < Class368_Sub12.anInt5488; i_4_++) {
					Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_4_][i_3_];
					if (class247 != null) {
						if (class247.aShort2347 > 0) {
							class247.aShort2347 *= -1;
						}
						if (class247.aShort2340 > 0) {
							class247.aShort2340 *= -1;
						}
					}
				}
			}
		}
		for (int i = 0; i < Class368_Sub9.anInt5477; i++) {
			for (int i_5_ = 0; Class228.anInt2201 > i_5_; i_5_++) {
				for (int i_6_ = 0; Class368_Sub12.anInt5488 > i_6_; i_6_++) {
					Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_6_][i_5_];
					if (class247 != null) {
						boolean bool_7_ = Class338_Sub2.aClass247ArrayArrayArray5195[0][i_6_][i_5_] != null && Class338_Sub2.aClass247ArrayArrayArray5195[0][i_6_][i_5_].aClass247_2338 != null;
						if (class247.aShort2347 < 0) {
							int i_8_ = i_5_;
							int i_9_ = i_5_;
							int i_10_ = i;
							int i_11_ = i;
							Class247 class247_12_ = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_6_][i_8_ - 1];
							int i_13_ = Class244.aSArray2320[i].method3355(i_5_, (byte) -119, i_6_);
							for (/**/; i_8_ > 0 && class247_12_ != null && class247_12_.aShort2347 < 0 && class247.aShort2347 == class247_12_.aShort2347 && class247_12_.aShort2345 == class247.aShort2345; class247_12_ = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_6_][i_8_ - 1]) {
								if (i_13_ != Class244.aSArray2320[i].method3355(i_8_ - 1, (byte) -110, i_6_)) {
									break;
								}
								if (i_8_ - 1 > 0 && i_13_ != Class244.aSArray2320[i].method3355(i_8_ - 2, (byte) -114, i_6_)) {
									break;
								}
								i_8_--;
							}
							for (class247_12_ = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_6_][i_9_ + 1]; Class368_Sub12.anInt5488 > i_9_ && class247_12_ != null && class247_12_.aShort2347 < 0 && class247_12_.aShort2347 == class247.aShort2347 && class247.aShort2345 == class247_12_.aShort2345; class247_12_ = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_6_][i_9_ + 1]) {
								if (i_13_ != Class244.aSArray2320[i].method3355(i_9_ + 1, (byte) -118, i_6_)) {
									break;
								}
								if (i_9_ + 1 < Class368_Sub12.anInt5488 && i_13_ != Class244.aSArray2320[i].method3355(i_9_ + 2, (byte) -125, i_6_)) {
									break;
								}
								i_9_++;
							}
							int i_14_ = -i_10_ + i_11_ + 1;
							int i_15_ = Class244.aSArray2320[bool_7_ ? i_10_ + 1 : i_10_].method3355(i_8_, (byte) -112, i_6_);
							int i_16_ = i_15_ + class247.aShort2347 * i_14_;
							int i_17_ = Class244.aSArray2320[bool_7_ ? i_10_ + 1 : i_10_].method3355(i_9_ + 1, (byte) -108, i_6_);
							int i_18_ = i_17_ + class247.aShort2347 * i_14_;
							int i_19_ = i_6_ << Class313.anInt2779;
							int i_20_ = i_8_ << Class313.anInt2779;
							int i_21_ = (i_9_ << Class313.anInt2779) + Js5TextureLoader.anInt3440;
							Class116.aClass324Array3678[Class77.anInt880++] = new Class324(1, i_11_, i_19_ + class247.aShort2345, i_19_ + class247.aShort2345, class247.aShort2345 + i_19_, class247.aShort2345 + i_19_, i_15_, i_17_, i_18_, i_16_, i_20_, i_21_, i_21_, i_20_);
							for (int i_22_ = i_10_; i_22_ <= i_11_; i_22_++) {
								for (int i_23_ = i_8_; i_9_ >= i_23_; i_23_++) {
									Class338_Sub2.aClass247ArrayArrayArray5195[i_22_][i_6_][i_23_].aShort2347 *= -1;
								}
							}
						}
						if (class247.aShort2340 < 0) {
							int i_24_ = i_6_;
							int i_25_ = i_6_;
							int i_26_ = i;
							int i_27_ = i;
							Class247 class247_28_ = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_24_ - 1][i_5_];
							int i_29_ = Class244.aSArray2320[i].method3355(i_5_, (byte) -107, i_6_);
							for (/**/; i_24_ > 0 && class247_28_ != null && class247_28_.aShort2340 < 0 && class247_28_.aShort2340 == class247.aShort2340 && class247.aShort2344 == class247_28_.aShort2344; class247_28_ = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_24_ - 1][i_5_]) {
								if (i_29_ != Class244.aSArray2320[i].method3355(i_5_, (byte) -110, i_24_ - 1)) {
									break;
								}
								if (i_24_ - 1 > 0 && Class244.aSArray2320[i].method3355(i_5_, (byte) -109, i_24_ - 2) != i_29_) {
									break;
								}
								i_24_--;
							}
							for (class247_28_ = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_25_ + 1][i_5_]; i_25_ < Class228.anInt2201 && class247_28_ != null && class247_28_.aShort2340 < 0 && class247.aShort2340 == class247_28_.aShort2340 && class247_28_.aShort2344 == class247.aShort2344; class247_28_ = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_25_ + 1][i_5_]) {
								if (Class244.aSArray2320[i].method3355(i_5_, (byte) -105, i_25_ + 1) != i_29_) {
									break;
								}
								if (Class228.anInt2201 > i_25_ + 1 && i_29_ != Class244.aSArray2320[i].method3355(i_5_, (byte) -107, i_25_ + 2)) {
									break;
								}
								i_25_++;
							}
							int i_30_ = i_27_ + 1 - i_26_;
							int i_31_ = Class244.aSArray2320[!bool_7_ ? i_26_ : i_26_ + 1].method3355(i_5_, (byte) -128, i_24_);
							int i_32_ = i_31_ + class247.aShort2340 * i_30_;
							int i_33_ = Class244.aSArray2320[!bool_7_ ? i_26_ : i_26_ + 1].method3355(i_5_, (byte) -111, i_25_ + 1);
							int i_34_ = i_33_ + class247.aShort2340 * i_30_;
							int i_35_ = i_24_ << Class313.anInt2779;
							int i_36_ = (i_25_ << Class313.anInt2779) + Js5TextureLoader.anInt3440;
							int i_37_ = i_5_ << Class313.anInt2779;
							Class116.aClass324Array3678[Class77.anInt880++] = new Class324(2, i_27_, i_35_, i_36_, i_36_, i_35_, i_31_, i_33_, i_34_, i_32_, i_37_ + class247.aShort2344, i_37_ + class247.aShort2344, i_37_ + class247.aShort2344, i_37_ + class247.aShort2344);
							for (int i_38_ = i_26_; i_38_ <= i_27_; i_38_++) {
								for (int i_39_ = i_24_; i_25_ >= i_39_; i_39_++) {
									Class338_Sub2.aClass247ArrayArrayArray5195[i_38_][i_39_][i_5_].aShort2340 *= -1;
								}
							}
						}
					}
				}
			}
		}
		Class190.aBoolean1943 = true;
	}

	static final void method3342(ha var_ha, int i, d var_d, int i_40_, int i_41_, boolean bool, int i_42_) {
		if (Class41_Sub13.anInt3773 < 100) {
			Class16_Sub2.method246(var_ha, var_d, -7618);
		}
		var_ha.KA(i_40_, i_41_, i + i_40_, i_41_ + i_42_);
		if (Class41_Sub13.anInt3773 < 100) {
			int i_43_ = 20;
			int i_44_ = i_40_ + i / 2;
			var_ha.aa(i_40_, i_41_, i, i_42_, -16777216, 0);
			int i_45_ = -i_43_ + i_42_ / 2 + i_41_ - 18;
			var_ha.b(i_44_ - 152, i_45_, 304, 34, Class140.aColorArray3587[Class10.colorID].getRGB(), 0);
			var_ha.aa(i_44_ - 150, i_45_ + 2, Class41_Sub13.anInt3773 * 3, 30, ParticleEmitterRaw.aColorArray1756[Class10.colorID].getRGB(), 0);
			Class49.aClass55_461.a(15897, TranslatableString.aClass120_1214.getTranslation(Class394.langID), -1, Class357.aColorArray3083[Class10.colorID].getRGB(), i_44_, i_43_ + i_45_);
		} else {
			int i_46_ = -(int) (i / Class106.aFloat1100) + Class69.anInt3688;
			int i_47_ = (int) (i_42_ / Class106.aFloat1100) + Class219_Sub1.anInt4569;
			int i_48_ = Class69.anInt3688 + (int) (i / Class106.aFloat1100);
			Class269.anInt2503 = (int) (i_42_ * 2 / Class106.aFloat1100);
			int i_49_ = -(int) (i_42_ / Class106.aFloat1100) + Class219_Sub1.anInt4569;
			Class293.anInt2684 = -(int) (i / Class106.aFloat1100) + Class69.anInt3688;
			Class368_Sub5.anInt5450 = (int) (i * 2 / Class106.aFloat1100);
			Class224.anInt2169 = -(int) (i_42_ / Class106.aFloat1100) + Class219_Sub1.anInt4569;
			Class106.method928(Class106.anInt1111 + i_46_, Class106.anInt1117 + i_47_, i_48_ + Class106.anInt1111, i_49_ + Class106.anInt1117, i_40_, i_41_, i + i_40_, i_42_ + i_41_ + 1);
			Class106.method930(var_ha);
			NodeDeque class155 = Class106.method929(var_ha);
			if (bool) {
				aBoolean2822 = false;
			}
			Class41_Sub20.method473(class155, 0, var_ha, 0, 34);
			if (aa_Sub2.anInt3728 > 0) {
				Class139.anInt1421--;
				if (Class139.anInt1421 == 0) {
					aa_Sub2.anInt3728--;
					Class139.anInt1421 = 20;
				}
			}
			if (Class68.drawFPS) {
				int i_50_ = i_40_ + i - 5;
				int i_51_ = i_42_ - 8 + i_41_;
				Class205_Sub1.aClass55_5642.a(127, i_51_, i_50_, 16776960, -1, "Fps:" + Class360_Sub8.anInt5338);
				i_51_ -= 15;
				Runtime runtime = Runtime.getRuntime();
				int i_52_ = (int) ((runtime.totalMemory() + -runtime.freeMemory()) / 1024L);
				int i_53_ = 16776960;
				if (i_52_ > 65536) {
					i_53_ = 16711680;
				}
				Class205_Sub1.aClass55_5642.a(127, i_51_, i_50_, i_53_, -1, "Mem:" + i_52_ + "k");
				i_51_ -= 15;
			}
		}
	}

	static {
		aClass311_2820 = new OutgoingPacket(54, 8);
		aBoolean2822 = false;
	}
}
