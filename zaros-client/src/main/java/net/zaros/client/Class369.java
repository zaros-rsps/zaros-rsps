package net.zaros.client;

import net.zaros.client.configs.objtype.ObjTypeList;

/* Class369 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class369 {
	
	static SubCache aClass301_3133;
	
	private ha_Sub1_Sub1 aHa_Sub1_Sub1_3134;
	
	static boolean aBoolean3135 = false;
	
	static int anInt3136;
	
	int anInt3137;
	
	static int[] anIntArray3138 = new int[500];
	
	static int anInt3139;
	
	static final void method3870(int i) {
		if (Class379_Sub2_Sub1.anInt6607 > 1) {
			Class379_Sub2_Sub1.anInt6607--;
			Class308.anInt2753 = Class338_Sub3_Sub3_Sub1.anInt6616;
		}
		if (Class296_Sub45_Sub2.aClass204_6277.aBoolean2065) {
			Class296_Sub45_Sub2.aClass204_6277.aBoolean2065 = false;
			Class41_Sub8.method423(0);
		} else {
			if (!Class318.aBoolean2814) {
				Class105_Sub2.method915(0);
			}
			for (int i_0_ = 0; i_0_ < 100; i_0_++) {
				if (!Class16_Sub1.method235((byte) -126, (Class296_Sub45_Sub2.aClass204_6277))) {
					break;
				}
			}
			if (Class366_Sub6.anInt5392 == 11) {
				while (Class277.method2332((byte) 60)) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 111, (Class296_Sub39_Sub12.aClass311_6198));
					class296_sub1.out.p1(0);
					int i_1_ = class296_sub1.out.pos;
					Canvas_Sub1.method123(4472, (class296_sub1.out));
					class296_sub1.out.method2604((-i_1_ + class296_sub1.out.pos));
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (GameClient.aClass296_Sub48_3717 == null) {
					if (Class338_Sub3_Sub3_Sub2.aLong6628 <= Class72.method771(-128)) {
						GameClient.aClass296_Sub48_3717 = (SeekableFile.aClass175_2399.method1705((byte) -91, (Class296_Sub51_Sub27_Sub1.aClass112_6733.ipAddress)));
					}
				} else if (GameClient.aClass296_Sub48_3717.anInt4976 != -1) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 94, Class245.aClass311_2326);
					class296_sub1.out.p2(GameClient.aClass296_Sub48_3717.anInt4976);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					GameClient.aClass296_Sub48_3717 = null;
					Class338_Sub3_Sub3_Sub2.aLong6628 = Class72.method771(i ^ ~0x6a) + 30000L;
				}
				Class296_Sub34 class296_sub34 = ((Class296_Sub34) Class403.aClass155_3379.removeFirst((byte) 113));
				if (class296_sub34 != null || (Class72.method771(i ^ ~0x63) - 2000L > Class122_Sub3.aLong5665)) {
					Class296_Sub1 class296_sub1 = null;
					int i_2_ = 0;
					for (Class296_Sub34 class296_sub34_3_ = (Class296_Sub34) Class161.aClass155_1676.removeFirst((byte) 114); class296_sub34_3_ != null; class296_sub34_3_ = ((Class296_Sub34) Class161.aClass155_1676.removeNext(1001))) {
						if (class296_sub1 != null && (class296_sub1.out.pos) - i_2_ >= 240) {
							break;
						}
						class296_sub34_3_.unlink();
						int i_4_ = class296_sub34_3_.method2736(-6);
						if (i_4_ < -1) {
							i_4_ = -1;
						} else if (i_4_ > 65534) {
							i_4_ = 65534;
						}
						int i_5_ = class296_sub34_3_.method2732(-6);
						if (i_5_ < -1) {
							i_5_ = -1;
						} else if (i_5_ > 65534) {
							i_5_ = 65534;
						}
						if (Class81.anInt3667 != i_5_ || NPCNode.anInt4626 != i_4_) {
							if (class296_sub1 == null) {
								class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 127, (Class30.aClass311_313));
								class296_sub1.out.p1(0);
								i_2_ = (class296_sub1.out.pos);
							}
							int i_6_ = i_5_ - Class81.anInt3667;
							Class81.anInt3667 = i_5_;
							int i_7_ = -NPCNode.anInt4626 + i_4_;
							NPCNode.anInt4626 = i_4_;
							int i_8_ = (int) ((class296_sub34_3_.method2737(-3) + -Class122_Sub3.aLong5665) / 20L);
							if (i_8_ < 8 && i_6_ >= -32 && i_6_ <= 31 && i_7_ >= -32 && i_7_ <= 31) {
								i_6_ += 32;
								i_7_ += 32;
								//class296_sub1.out.writeShort((i_6_ << 6) + (i_8_ << 12) + i_7_);
							} else if (i_8_ >= 32 || i_6_ < -128 || i_6_ > 127 || i_7_ < -128 || i_7_ > 127) {
								if (i_8_ >= 32) {
									//class296_sub1.out.writeShort(i_8_ + 57344);
									if (i_5_ == 1 || i_4_ == -1) {
										//class296_sub1.out.writeInt(-2147483648);
									} else {
										//class296_sub1.out.writeInt(i_5_ | i_4_ << 16);
									}
								} else {
									class296_sub1.out.p1(i_8_ + 192);
									if (i_5_ != 1 && i_4_ != -1) {
										//class296_sub1.out.writeInt(i_5_ | i_4_ << 16);
									} else {
										//class296_sub1.out.writeInt(-2147483648);
									}
								}
							} else {
								i_7_ += 128;
								i_6_ += 128;
								//class296_sub1.out.writeByte(i_8_ + 128);
								//class296_sub1.out.writeShort((i_6_ << 8) + i_7_);
							}
							Class122_Sub3.aLong5665 = class296_sub34_3_.method2737(-3);
						}
					}
					if (class296_sub1 != null) {
						class296_sub1.out.method2604(-i_2_ + (class296_sub1.out.pos));
						Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					}
				}
				if (class296_sub34 != null) {
					long l = ((class296_sub34.method2737(-3) - Class338_Sub3_Sub4_Sub1.aLong6651) / 50L);
					Class338_Sub3_Sub4_Sub1.aLong6651 = class296_sub34.method2737(-3);
					if (l > 32767L) {
						l = 32767L;
					}
					int i_9_ = class296_sub34.method2736(-6);
					if (i_9_ >= 0) {
						if (i_9_ > 65535) {
							i_9_ = 65535;
						}
					} else {
						i_9_ = 0;
					}
					int i_10_ = class296_sub34.method2732(i ^ ~0x1a);
					if (i_10_ < 0) {
						i_10_ = 0;
					} else if (i_10_ > 65535) {
						i_10_ = 65535;
					}
					int i_11_ = 0;
					if (class296_sub34.method2735(-116) == 2) {
						i_11_ = 1;
					}
					int i_12_ = (int) l;
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 104, Class303.aClass311_2726);
					class296_sub1.out.writeLEShortA(i_11_ << 15 | i_12_);
					class296_sub1.out.writeLEInt(i_9_ << 16 | i_10_);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (Class368_Sub2.anInt5430 > 0) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 121, Class321.aClass311_2823);
					class296_sub1.out.p1(Class368_Sub2.anInt5430 * 3);
					for (int i_13_ = 0; i_13_ < Class368_Sub2.anInt5430; i_13_++) {
						Interface1 interface1 = (Class338_Sub3_Sub1_Sub2.anInterface1Array6741[i_13_]);
						long l = ((interface1.method3((byte) 124) + -CS2Call.aLong4959) / 50L);
						if (l > 65535L) {
							l = 65535L;
						}
						CS2Call.aLong4959 = interface1.method3((byte) 80);
						class296_sub1.out.p1(interface1.method5(13856));
						class296_sub1.out.p2((int) l);
					}
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (Class313.anInt2780 > 0) {
					Class313.anInt2780--;
				}
				if (Mesh.aBoolean1359 && Class313.anInt2780 <= 0) {
					Class313.anInt2780 = 20;
					Mesh.aBoolean1359 = false;
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 111, ConfigurationsLoader.aClass311_87);
					class296_sub1.out.writeLEShort((int) Class41_Sub26.aFloat3806 >> 3);
					class296_sub1.out.p2((int) Class153.aFloat1572 >> 3);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (Class41.aBoolean390 == !SubCache.aBoolean2707) {
					SubCache.aBoolean2707 = Class41.aBoolean390;
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 85, (Class338_Sub3_Sub1_Sub4.aClass311_6644));
					class296_sub1.out.p1(!Class41.aBoolean390 ? 0 : 1);
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
				}
				if (!Class348.aBoolean3033) {
					Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 120, SubCache.aClass311_2706);
					class296_sub1.out.p1(0);
					int i_14_ = class296_sub1.out.pos;
					Packet class296_sub17 = Class343_Sub1.aClass296_Sub50_5282.method3057(25);
					class296_sub1.out.writeBytes(class296_sub17.data, 0, class296_sub17.pos);
					class296_sub1.out.method2604((-i_14_ + class296_sub1.out.pos));
					Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
					Class348.aBoolean3033 = true;
				}
				if (Class338_Sub2.aClass247ArrayArrayArray5195 != null) {
					if (Class361.anInt3103 == 2) {
						Class338_Sub8.method3601((byte) -27);
					} else if (Class361.anInt3103 == 3) {
						Class296_Sub51_Sub27_Sub1.method3157(false);
					}
				}
				if (!Class296_Sub51_Sub39.aBoolean6549) {
					Class296_Sub36.aFloat4865 /= 2.0F;
				} else {
					Class296_Sub51_Sub39.aBoolean6549 = false;
				}
				if (Class296_Sub51_Sub24.aBoolean6465) {
					Class296_Sub51_Sub24.aBoolean6465 = false;
				} else {
					Class304.aFloat2733 /= 2.0F;
				}
				GameType.method353(12);
				if (Class366_Sub6.anInt5392 == 11) {
					Class296_Sub20.method2655(43);
					Class296_Sub29.method2691(i - 31);
					Class41_Sub14.method446((byte) -76);
					Class296_Sub45_Sub2.aClass204_6277.anInt2062++;
					if (Class296_Sub45_Sub2.aClass204_6277.anInt2062 > 750) {
						Class41_Sub8.method423(0);
					} else {
						if (za_Sub1.anInt6554 == 0) {
							Class296_Sub52.method3204(54);
							ha_Sub3.method1295((byte) 121);
						} else {
							if (za_Sub1.anInt6554 == 1 && Class40.method378(Class399.someXTEARelatedFileID, i + 13456)) {
								Billboard.method4023(-3423);
								za_Sub1.anInt6554 = 2;
							}
							if (za_Sub1.anInt6554 == 2 && Class366_Sub6.anInt5392 != 12) {
								Class352.aClass263_3043.clear();
								za_Sub1.anInt6554 = 3;
								Class274.anInt2523 = Class29.anInt307;
								Class162.anInt3557 = 0;
								Class41_Sub23.method486(false);
							}
							if (za_Sub1.anInt6554 == 3) {
								int i_15_ = Class29.anInt307 - Class274.anInt2523;
								if (Class162.anInt3557 < (Class296_Sub39_Sub10.aClass368Array6179).length) {
									do {
										Class368 class368 = (Class296_Sub39_Sub10.aClass368Array6179[Class162.anInt3557]);
										if (class368.anInt3128 > i_15_) {
											break;
										}
										class368.method3807((byte) 107);
										if (za_Sub1.anInt6554 != 3) {
											break;
										}
									} while (++Class162.anInt3557 < (Class296_Sub39_Sub10.aClass368Array6179).length);
								}
								if (za_Sub1.anInt6554 == 3) {
									for (int i_16_ = 0; (Class379.aClass302Array3624.length > i_16_); i_16_++) {
										Class302 class302 = (Class379.aClass302Array3624[i_16_]);
										if (class302.aBoolean2716) {
											Mobile class338_sub3_sub1_sub3 = class302.method3262(0);
											Class296_Sub39_Sub20.method2903(105, class338_sub3_sub1_sub3, true);
										}
									}
								}
							}
						}
						Class276.method2319(0);
						if (!Class296_Sub2.aBoolean4588) {
							Class50.method615(59);
							Class296_Sub2.aBoolean4588 = true;
						}
						for (int i_17_ = Class16_Sub3_Sub1.configsRegister.nextConfigChange(true, (byte) 66); i_17_ != -1; i_17_ = Class16_Sub3_Sub1.configsRegister.nextConfigChange(false, (byte) 66)) {
							TranslatableString.method1031(i_17_, (byte) -77);
							Class86.anIntArray933[(31 & ParamType.anInt3249++)] = i_17_;
						}
						for (GameLoopTask task = Class100.nextTask(); task != null; task = Class100.nextTask()) {
							int type = task.getType();
							long id = task.bigUID();
							if (type != 1) {
								if (type != 2) {
									if (type != 3) {
										if (type == 4) {
											InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
											int i_19_ = (task.intParam);
											int i_20_ = (task.anInt6145);
											int i_21_ = (task.anInt6149);
											if (i_19_ != class51.modelType || i_20_ != class51.modelID || class51.anInt545 != i_21_) {
												class51.modelType = i_19_;
												class51.anInt545 = i_21_;
												class51.modelID = i_20_;
												Class332.method3416(class51, (byte) 77);
											}
										} else if (type == 5) {
											InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
											if (task.intParam != class51.anInt497) {
												if ((task.intParam) != -1) {
													if (class51.aClass44_554 == null) {
														class51.aClass44_554 = (new Class44_Sub2());
													}
													class51.aClass44_554.method549((byte) 115, (task.intParam));
												} else {
													class51.aClass44_554 = null;
												}
												class51.anInt497 = (task.intParam);
												Class332.method3416(class51, (byte) 125);
											}
										} else if (type == 6) {
											int i_22_ = (task.intParam);
											int i_23_ = i_22_ >> 10 & 0x1f;
											int i_24_ = (i_22_ & 0x3f4) >> 5;
											int i_25_ = i_22_ & 0x1f;
											int i_26_ = ((i_25_ << 3) + (i_23_ << 19) + (i_24_ << 11));
											InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
											if (i_26_ != class51.textColour) {
												class51.textColour = i_26_;
												Class332.method3416(class51, (byte) 96);
											}
										} else if (type == 7) {
											InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
											boolean bool = (task.intParam) == 1;
											if (bool != class51.hiden) {
												class51.hiden = bool;
												Class332.method3416(class51, (byte) 79);
											}
										} else if (type == 8) {
											InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
											if ((class51.anInt612 != (task.intParam)) || (class51.anInt515 != (task.anInt6145)) || ((task.anInt6149) != class51.anInt520)) {
												class51.anInt515 = (task.anInt6145);
												class51.anInt612 = (task.intParam);
												class51.anInt520 = (task.anInt6149);
												if (class51.clickedItem != -1) {
													if (class51.anInt516 > 0) {
														class51.anInt520 = (class51.anInt520 * 32 / (class51.anInt516));
													} else if (class51.width > 0) {
														class51.anInt520 = (class51.anInt520 * 32 / (class51.width));
													}
												}
												Class332.method3416(class51, (byte) 81);
											}
										} else if (type == 9) {
											InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
											if ((class51.clickedItem != (task.intParam)) || ((task.anInt6145) != class51.anInt511)) {
												class51.anInt511 = (task.anInt6145);
												class51.clickedItem = (task.intParam);
												Class332.method3416(class51, (byte) 101);
											}
										} else if (type != 10) {
											if (type != 11) {
												if (type != 12) {
													if (type == 14) {
														InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
														class51.textureID = (task.intParam);
													} else if (type == 15) {
														Class210_Sub1.foundX = (task.intParam);
														Class377.aBoolean3189 = true;
														Class205.foundY = (task.anInt6145);
													} else if (type != 16) {
														if (type == 20) {
															InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
															class51.aBoolean617 = ((task.intParam) == 1);
														} else if (type != 21) {
															if (type != 17) {
																if (type == 18) {
																	InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
																	int i_27_ = (int) (id >> 32);
																	class51.method619(i_27_, (byte) 20, (short) task.anInt6145, (short) task.intParam);
																} else if (type == 19) {
																	InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
																	int i_28_ = (int) (id >> 32);
																	class51.method638((short) task.anInt6145, (short) task.intParam, (byte) -34, i_28_);
																}
															} else {
																InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
																class51.anInt558 = (task.intParam);
															}
														} else {
															InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
															class51.aBoolean550 = ((task.intParam) == 1);
														}
													} else {
														InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
														class51.fontID = (task.intParam);
													}
												} else {
													InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
													int i_29_ = (task.intParam);
													if (class51 != null && (class51.type == 0)) {
														if ((class51.scrollMaxW - (class51.anInt623)) < i_29_) {
															i_29_ = (-(class51.anInt623) + (class51.scrollMaxW));
														}
														if (i_29_ < 0) {
															i_29_ = 0;
														}
														if (class51.anInt632 != i_29_) {
															class51.anInt632 = i_29_;
															Class332.method3416(class51, (byte) 82);
														}
													}
												}
											} else {
												InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
												class51.anInt605 = class51.locY = (task.anInt6145);
												class51.anInt614 = class51.locX = (task.intParam);
												class51.aByte595 = (byte) 0;
												class51.aByte626 = (byte) 0;
												Class332.method3416(class51, (byte) 98);
											}
										} else {
											InterfaceComponent class51 = (InterfaceComponent.getInterfaceComponent((int) id));
											if ((task.intParam != class51.anInt588) || (class51.anInt581 != (task.anInt6145)) || (class51.anInt548 != (task.anInt6149))) {
												class51.anInt548 = (task.anInt6149);
												class51.anInt588 = (task.intParam);
												class51.anInt581 = (task.anInt6145);
												Class332.method3416(class51, (byte) 125);
											}
										}
									} else {
										InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent((int) id);
										if (!task.stringParam.equals(class51.text)) {
											class51.text = (task.stringParam);
											Class332.method3416(class51, (byte) 112);
										}
									}
								} else {
									Class139.globalStringVars[(int) id] = task.stringParam;
									Class76.anIntArray872[(Class249.anInt2356++ & 31)] = (int) id;
								}
							} else {
								Class269.globalIntVars[(int) id] = task.intParam;
								Class32_Sub1.aBoolean5651 |= Class14.aBooleanArray153[(int) id];
								BITConfigsLoader.anIntArray2207[(Class296_Sub15_Sub4.anInt6038++ & 31)] = (int) id;
							}
						}
						Class217.anInt2119++;
						if (Class183.anInt1880 != 0) {
							Class41_Sub14.anInt3775 += 20;
							if (Class41_Sub14.anInt3775 >= 400) {
								Class183.anInt1880 = 0;
							}
						}
						if (StaticMethods.aClass51_5944 != null) {
							Class246.anInt2335++;
							if (Class246.anInt2335 >= 15) {
								Class332.method3416((StaticMethods.aClass51_5944), (byte) 96);
								StaticMethods.aClass51_5944 = null;
							}
						}
						Class359.aClass51_3092 = null;
						Class41_Sub28.aClass51_3814 = null;
						aa_Sub2.aBoolean3727 = false;
						PlayerEquipment.aBoolean3370 = false;
						Class206.method1992(-1, -26396, -1, null);
						if (!Class127.aBoolean1304) {
							Class41_Sub19.anInt3793 = -1;
						}
						if (i == 31) {
							StaticMethods.method2469((byte) -83);
							Class338_Sub3_Sub3_Sub1.anInt6616++;
							if (Class360_Sub6.aBoolean5333) {
								Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 108, (TextureOperation.aClass311_5038));
								class296_sub1.out.method2570((Class41_Sub27.anInt3810 << 28 | Class41_Sub10.anInt3767 << 14 | StaticMethods.anInt3150));
								Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
								Class360_Sub6.aBoolean5333 = false;
							}
							for (; ; ) {
								CS2Call class296_sub46 = ((CS2Call) Class127_Sub1.aClass155_4284.method1573(1));
								if (class296_sub46 == null) {
									break;
								}
								InterfaceComponent class51 = class296_sub46.callerInterface;
								if (class51.anInt592 >= 0) {
									InterfaceComponent class51_30_ = InterfaceComponent.getInterfaceComponent((class51.parentId));
									if (class51_30_ == null || class51_30_.aClass51Array555 == null || (class51.anInt592 >= (class51_30_.aClass51Array555).length) || class51 != (class51_30_.aClass51Array555[class51.anInt592])) {
										continue;
									}
								}
								CS2Executor.runCS2(class296_sub46);
							}
							for (; ; ) {
								CS2Call class296_sub46 = ((CS2Call) StaticMethods.aClass155_1842.method1573(1));
								if (class296_sub46 == null) {
									break;
								}
								InterfaceComponent class51 = class296_sub46.callerInterface;
								if (class51.anInt592 >= 0) {
									InterfaceComponent class51_31_ = InterfaceComponent.getInterfaceComponent((class51.parentId));
									if (class51_31_ == null || class51_31_.aClass51Array555 == null || (class51.anInt592 >= (class51_31_.aClass51Array555).length) || class51 != (class51_31_.aClass51Array555[class51.anInt592])) {
										continue;
									}
								}
								CS2Executor.runCS2(class296_sub46);
							}
							for (; ; ) {
								CS2Call class296_sub46 = ((CS2Call) Class179.aClass155_1854.method1573(1));
								if (class296_sub46 == null) {
									break;
								}
								InterfaceComponent class51 = class296_sub46.callerInterface;
								if (class51.anInt592 >= 0) {
									InterfaceComponent class51_32_ = InterfaceComponent.getInterfaceComponent((class51.parentId));
									if (class51_32_ == null || class51_32_.aClass51Array555 == null || (class51.anInt592 >= (class51_32_.aClass51Array555).length) || (class51_32_.aClass51Array555[class51.anInt592]) != class51) {
										continue;
									}
								}
								CS2Executor.runCS2(class296_sub46);
							}
							if (Class359.aClass51_3092 == null) {
								Class2.anInt59 = 0;
							}
							if (InvisiblePlayer.aClass51_1982 != null) {
								Class264.method2276(238);
							}
							if (Class338_Sub3_Sub5.rights > 0 && Class2.aClass166_66.method1637(82, 42) && Class2.aClass166_66.method1637(81, 89) && Class359.anInt3090 != 0) {
								int i_33_ = ((Class296_Sub51_Sub11.localPlayer.z) - Class359.anInt3090);
								if (i_33_ >= 0) {
									if (i_33_ > 3) {
										i_33_ = 3;
									}
								} else {
									i_33_ = 0;
								}
								Class360_Sub3.method3741(((Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0]) + Class41_Sub26.worldBaseY), i_33_, (byte) 88, ((Class296_Sub51_Sub11.localPlayer.waypointQueueX[0]) + Class206.worldBaseX));
							}
							Class48.method600(i ^ 0x7e);
							for (int i_34_ = 0; i_34_ < 5; i_34_++) {
								Class37.anIntArray365[i_34_]++;
							}
							if (Class32_Sub1.aBoolean5651 && (Class262.aLong2453 < Class72.method771(i ^ ~0x6d) - 60000L)) {
								Class188.method1891(3746);
							}
							for (Class338_Sub8_Sub1 class338_sub8_sub1 = ((Class338_Sub8_Sub1) Class33.aClass404_331.method4160((byte) 87)); class338_sub8_sub1 != null; class338_sub8_sub1 = ((Class338_Sub8_Sub1) Class33.aClass404_331.method4163(-24917))) {
								if ((long) class338_sub8_sub1.anInt6577 < Class72.method771(-118) / 1000L - 5L) {
									if (class338_sub8_sub1.aShort6574 > 0) {
										Class181.method1827(0, "", 0, "", (class338_sub8_sub1.aString6575 + (TranslatableString.aClass120_1215.getTranslation(Class394.langID))), "", 5);
									}
									if (class338_sub8_sub1.aShort6574 == 0) {
										Class181.method1827(0, "", 0, "", (class338_sub8_sub1.aString6575 + (TranslatableString.aClass120_1216.getTranslation(Class394.langID))), "", 5);
									}
									class338_sub8_sub1.method3438(false);
								}
							}
							StaticMethods.anInt5956++;
							if (StaticMethods.anInt5956 > 500) {
								StaticMethods.anInt5956 = 0;
								int i_35_ = (int) (Math.random() * 8.0);
								if ((i_35_ & 0x2) == 2) {
									Class41_Sub8.anInt3763 += StaticMethods.anInt5920;
								}
								if ((i_35_ & 0x4) == 4) {
									Class134.anInt1387 += MaterialRaw.anInt1791;
								}
								if ((i_35_ & 0x1) == 1) {
									Class4.anInt71 += Class241_Sub1_Sub1.anInt5886;
								}
							}
							if (Class4.anInt71 < -50) {
								Class241_Sub1_Sub1.anInt5886 = 2;
							}
							if (Class4.anInt71 > 50) {
								Class241_Sub1_Sub1.anInt5886 = -2;
							}
							if (Class41_Sub8.anInt3763 < -55) {
								StaticMethods.anInt5920 = 2;
							}
							if (Class41_Sub8.anInt3763 > 55) {
								StaticMethods.anInt5920 = -2;
							}
							if (Class134.anInt1387 < -40) {
								MaterialRaw.anInt1791 = 1;
							}
							if (Class134.anInt1387 > 40) {
								MaterialRaw.anInt1791 = -1;
							}
							Class296_Sub15_Sub2.anInt6004++;
							if (Class296_Sub15_Sub2.anInt6004 > 500) {
								Class296_Sub15_Sub2.anInt6004 = 0;
								int i_36_ = (int) (Math.random() * 8.0);
								if ((i_36_ & 0x1) == 1) {
									StaticMethods.anInt6075 += Class139.anInt1423;
								}
								if ((i_36_ & 0x2) == 2) {
									ObjTypeList.anInt1320 += Class353.anInt3050;
								}
							}
							if (StaticMethods.anInt6075 < -60) {
								Class139.anInt1423 = 2;
							}
							if (ObjTypeList.anInt1320 < -20) {
								Class353.anInt3050 = 1;
							}
							if (StaticMethods.anInt6075 > 60) {
								Class139.anInt1423 = -2;
							}
							if (ObjTypeList.anInt1320 > 10) {
								Class353.anInt3050 = -1;
							}
							Class296_Sub45_Sub2.aClass204_6277.anInt2060++;
							if (Class296_Sub45_Sub2.aClass204_6277.anInt2060 > 50) {
								Class219.anInt2127++;
								Class296_Sub1 class296_sub1 = Class253.allocateOut((Class296_Sub45_Sub2.aClass204_6277.aClass185_2053), (byte) 96, (Class44_Sub2.aClass311_3860));
								Class296_Sub45_Sub2.aClass204_6277.sendPacket(class296_sub1, (byte) 119);
							}
							if (TranslatableString.aBoolean1261) {
								Class358.method3712(i - 1367);
								TranslatableString.aBoolean1261 = false;
							}
							try {
								Class296_Sub45_Sub2.aClass204_6277.method1963(true);
							} catch (java.io.IOException ioexception) {
								Class41_Sub8.method423(0);
							}
						}
					}
				}
			}
		}
	}
	
	protected final void finalize() throws Throwable {
		aHa_Sub1_Sub1_3134.method1238(7803, anInt3137);
		super.finalize();
	}
	
	Class369(ha_Sub1_Sub1 var_ha_Sub1_Sub1, int i, int i_37_) {
		aHa_Sub1_Sub1_3134 = var_ha_Sub1_Sub1;
		anInt3137 = i_37_;
	}
	
	public static void method3871(int i) {
		if (i != -1) {
			method3871(-67);
		}
		anIntArray3138 = null;
		aClass301_3133 = null;
	}
	
	static {
		aClass301_3133 = new SubCache(64);
		anInt3139 = 0;
	}
}
