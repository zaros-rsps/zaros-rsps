package net.zaros.client;

/* Class49 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class49 {
	private ha_Sub3 aHa_Sub3_456;
	static Class182 aClass182_457;
	static OutgoingPacket aClass311_458 = new OutgoingPacket(67, 2);
	private AdvancedMemoryCache aClass113_459 = new AdvancedMemoryCache(256);
	private d aD460;
	static Class55 aClass55_461;
	static long[][][] aLongArrayArrayArray462;
	static Sprite aClass397_463;
	static Class256 aClass256_464;

	final void method605(int i) {
		if (i == 3553) {
			aClass113_459.clear();
		}
	}

	final Class69_Sub1 method606(int i, int textureid) {
		Object object = aClass113_459.get(textureid);
		if (object != null) {
			return (Class69_Sub1) object;
		}
		if (!aD460.is_ready(textureid)) {
			return null;
		}
		MaterialRaw class170 = aD460.method14(textureid, -9412);
		int i_1_ = class170.small_sized ? 64 : aHa_Sub3_456.anInt4131;
		Class69_Sub1 class69_sub1;
		if (class170.aBoolean1790 && aHa_Sub3_456.b()) {
			float[] fs = aD460.method16(i_1_, 0.7F, false, textureid, i_1_, 124);
			class69_sub1 = new Class69_Sub1(aHa_Sub3_456, 3553, 34842, i_1_, i_1_, class170.aByte1795 != 0, fs, 6408);
		} else {
			int[] is;
			if (class170.anInt1788 == 2 || !Class41_Sub23.method485(-65, class170.aByte1774)) {
				is = aD460.method17(0.7F, i_1_, false, (byte) -104, i_1_, textureid);
			} else {
				is = aD460.get_transparent_pixels(i_1_, i_1_, true, (byte) 102, 0.7F, textureid);
			}
			class69_sub1 = new Class69_Sub1(aHa_Sub3_456, 3553, 6408, i_1_, i_1_, class170.aByte1795 != 0, is, 0, 0, false);
		}
		class69_sub1.method733(class170.aBoolean1780, class170.aBoolean1779, true);
		aClass113_459.put(class69_sub1, textureid);
		return class69_sub1;
	}

	public static void method607(byte i) {
		aClass256_464 = null;
		aClass397_463 = null;
		aLongArrayArrayArray462 = null;
		if (i != -70) {
			aClass397_463 = null;
		}
		aClass311_458 = null;
		aClass182_457 = null;
		aClass55_461 = null;
	}

	final void method608(int i) {
		if (i != -27366) {
			aClass182_457 = null;
		}
		aClass113_459.clean(5);
	}

	Class49(ha_Sub3 var_ha_Sub3, d var_d) {
		aD460 = var_d;
		aHa_Sub3_456 = var_ha_Sub3;
	}
}
