package net.zaros.client;

/* Class296_Sub51_Sub34 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub34 extends TextureOperation {
	static Js5 aClass138_6523;

	public static void method3184(int i) {
		int i_0_ = 30 % ((12 - i) / 36);
		aClass138_6523 = null;
	}

	public Class296_Sub51_Sub34() {
		super(1, true);
	}

	static final void method3185(int i, int i_1_, int i_2_) {
		if (i_1_ != -2)
			method3185(-67, -64, 76);
		Class235.anInt2227 = i_2_;
		Class261.anInt2424 = i;
		if (ConfigsRegister.anInt3674 == 0) {
			Class359.anInt3089 = Class41_Sub2.anInt3741 * 2 + Class261.anInt2424;
			Class296_Sub39_Sub12.anInt6197 = Class347.anInt3026 * 2 + Class235.anInt2227;
		} else if (ConfigsRegister.anInt3674 == 1) {
			Class317.anInt3705 = Class244.anInt2321 + (Class235.anInt2227 / Class290.anInt2656 + 2);
			Applet_Sub1.anInt7 = (Class277.anInt2530 + 2 + Class261.anInt2424 / Class395.anInt3317);
			Class359.anInt3089 = Class395.anInt3317 * Applet_Sub1.anInt7;
			Class296_Sub39_Sub12.anInt6197 = Class290.anInt2656 * Class317.anInt3705;
			Class347.anInt3026 = Class296_Sub39_Sub12.anInt6197 - Class235.anInt2227 >> 1;
			Class41_Sub2.anInt3741 = -Class261.anInt2424 + Class359.anInt3089 >> 1;
		} else if (ConfigsRegister.anInt3674 == 2) {
			Class296_Sub39_Sub12.anInt6197 = Class235.anInt2227;
			Class359.anInt3089 = Class261.anInt2424;
		}
	}

	final int[] get_monochrome_output(int i, int i_3_) {
		if (i != 0)
			aClass138_6523 = null;
		int[] is = aClass318_5035.method3335(i_3_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int[][] is_4_ = this.method3075((byte) -45, 0, i_3_);
			int[] is_5_ = is_4_[0];
			int[] is_6_ = is_4_[1];
			int[] is_7_ = is_4_[2];
			for (int i_8_ = 0; i_8_ < Class41_Sub10.anInt3769; i_8_++)
				is[i_8_] = (is_6_[i_8_] + is_5_[i_8_] + is_7_[i_8_]) / 3;
		}
		return is;
	}
}
