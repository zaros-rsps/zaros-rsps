package net.zaros.client;
/* Class56 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Random;

final class Class56 {
	static int[] anIntArray659;
	private int anInt660;
	private boolean[] aBooleanArray661;
	static int anInt662;
	private int[][] anIntArrayArray663;
	private Js5 aClass138_664;
	static SubInPacket aClass260_665 = new SubInPacket(11, 16);

	final boolean method665(int i) {
		int i_0_ = 20 % ((i + 68) / 50);
		if (anInt660 == -1)
			return false;
		return true;
	}

	static final boolean method666(int i, int i_1_) {
		if (i_1_ != 25824)
			method667(91, 36, null, -47, -122, (byte) -101, -8, -68, -120, 58, null, 118);
		if (i != 4 && i != 8 && i != 12 && i != 10)
			return false;
		return true;
	}

	static final void method667(int i, int i_2_, Class296_Sub39_Sub1 class296_sub39_sub1, int i_3_, int i_4_, byte i_5_, int i_6_, int i_7_, int i_8_, int i_9_, ha var_ha, int i_10_) {
		int i_11_ = -80 / ((63 - i_5_) / 56);
		if (i_7_ > i && i + i_8_ > i_7_ && i_2_ > i_9_ - 13 && i_9_ + 3 > i_2_)
			i_4_ = i_6_;
		String string = TextureOperation.method3066(-4, class296_sub39_sub1);
		Class49.aClass55_461.a(i_4_, 0, i + 3, Class123_Sub1_Sub2.anIntArray5817, string, 0, i_9_, Class44_Sub1_Sub1.aClass397Array5811);
	}

	public static void method668(byte i) {
		aClass260_665 = null;
		if (i > 73)
			anIntArray659 = null;
	}

	final Class334 method669(byte i, int i_12_) {
		if (i <= 122)
			anIntArray659 = null;
		byte[] is = aClass138_664.getFile(1, i_12_);
		Class334 class334 = new Class334();
		class334.method3421(-108, new Packet(is));
		return class334;
	}

	final int[] method670(int i, int i_13_) {
		if (i < 0 || anIntArrayArray663.length <= i) {
			if (anInt660 != -1)
				return new int[]{anInt660};
			return new int[0];
		}
		if (!aBooleanArray661[i] || anIntArrayArray663[i].length <= 1)
			return anIntArrayArray663[i];
		int i_14_ = 115 % ((i_13_ + 16) / 51);
		int i_15_ = anInt660 != -1 ? 1 : 0;
		Random random = new Random();
		int[] is = new int[anIntArrayArray663[i].length];
		ArrayTools.removeElements(anIntArrayArray663[i], 0, is, 0, is.length);
		for (int i_16_ = i_15_; is.length > i_16_; i_16_++) {
			int i_17_ = s_Sub3.method3373(-i_15_ + is.length, random, 6445) + i_15_;
			int i_18_ = is[i_16_];
			is[i_16_] = is[i_17_];
			is[i_17_] = i_18_;
		}
		return is;
	}

	Class56(GameType class35, int i, Js5 class138) {
		aClass138_664 = class138;
		aClass138_664.getLastFileId(1);
		Packet class296_sub17 = new Packet(aClass138_664.getFile(0, 0));
		int i_19_ = class296_sub17.g1();
		if (i_19_ > 3) {
			anInt660 = -1;
			aBooleanArray661 = new boolean[0];
			anIntArrayArray663 = new int[0][];
		} else {
			int i_20_ = class296_sub17.g1();
			Class294[] class294s = Class294.method2478(-4652);
			boolean bool = true;
			if (class294s.length == i_20_) {
				for (int i_21_ = 0; i_21_ < class294s.length; i_21_++) {
					int i_22_ = class296_sub17.g1();
					if (i_22_ != class294s[i_21_].anInt2689) {
						bool = false;
						break;
					}
				}
			} else
				bool = false;
			if (bool) {
				int i_23_ = class296_sub17.g1();
				int i_24_ = class296_sub17.g1();
				if (i_19_ <= 2)
					anInt660 = -1;
				else
					anInt660 = class296_sub17.g2b();
				anIntArrayArray663 = new int[i_24_ + 1][];
				aBooleanArray661 = new boolean[i_24_ + 1];
				for (int i_25_ = 0; i_23_ > i_25_; i_25_++) {
					int i_26_ = class296_sub17.g1();
					aBooleanArray661[i_26_] = class296_sub17.g1() == 1;
					int i_27_ = class296_sub17.g2();
					if (anInt660 == -1) {
						anIntArrayArray663[i_26_] = new int[i_27_];
						for (int i_28_ = 0; i_27_ > i_28_; i_28_++)
							anIntArrayArray663[i_26_][i_28_] = class296_sub17.g2();
					} else {
						anIntArrayArray663[i_26_] = new int[i_27_ + 1];
						anIntArrayArray663[i_26_][0] = anInt660;
						for (int i_29_ = 0; i_29_ < i_27_; i_29_++)
							anIntArrayArray663[i_26_][i_29_ + 1] = class296_sub17.g2();
					}
				}
				for (int i_30_ = 0; i_24_ + 1 > i_30_; i_30_++) {
					if (anIntArrayArray663[i_30_] == null) {
						if (anInt660 == -1)
							anIntArrayArray663[i_30_] = new int[0];
						else
							anIntArrayArray663[i_30_] = new int[]{anInt660};
					}
				}
			} else {
				aBooleanArray661 = new boolean[0];
				anIntArrayArray663 = new int[0][];
				anInt660 = -1;
			}
		}
	}
}
