package net.zaros.client;

/* Class16_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class16_Sub2 extends Class16 {
	static ha aHa3733;

	static final void method242(Animator class44, int i, Model class178, Animator class44_0_) {
		if (class44_0_.method545(0) && class44.method545(0)) {
			Animation class43 = class44_0_.aClass43_430;
			Animation class43_1_ = class44.aClass43_430;
			class178.method1721(class43.aBooleanArray414, class44.aClass12_434.anInt130, false, class44.aClass12_434.aClass296_Sub39_Sub6_129, class44.anInt423, class44.aClass12_434.aClass296_Sub39_Sub6_135, class44_0_.aClass12_434.anInt131, class43_1_.anIntArray405[class44.anInt432], class44_0_.aClass12_434.anInt130, class44.aClass12_434.anInt131, class44_0_.anInt423, class43.aBoolean413 | class43_1_.aBoolean413, class43.anIntArray405[class44_0_.anInt432], class44_0_.aClass12_434.aClass296_Sub39_Sub6_129, class44_0_.aClass12_434.aClass296_Sub39_Sub6_135);
		}
		if (i != -76)
			method246(null, null, -30);
	}

	static final void method243(int[] is, int[] is_2_, int[] is_3_, Mobile class338_sub3_sub1_sub3, boolean bool) {
		if (bool != true)
			method242(null, 69, null, null);
		for (int i = 0; is.length > i; i++) {
			int i_4_ = is[i];
			int i_5_ = is_3_[i];
			int i_6_ = is_2_[i];
			for (int i_7_ = 0; i_5_ != 0 && i_7_ < (class338_sub3_sub1_sub3.aClass44_Sub1_Sub1Array6814).length; i_7_++) {
				if ((i_5_ & 0x1) != 0) {
					if (i_4_ != -1) {
						Animation class43 = Class296_Sub51_Sub13.animationsLoader.getAnimation(i_4_, (byte) -124);
						int i_8_ = class43.anInt410;
						Class44_Sub1_Sub1 class44_sub1_sub1 = (class338_sub3_sub1_sub3.aClass44_Sub1_Sub1Array6814[i_7_]);
						if (class44_sub1_sub1 != null && class44_sub1_sub1.method570((byte) 40)) {
							if (class44_sub1_sub1.method557((byte) -85) != i_4_) {
								if (class43.anInt408 >= (class44_sub1_sub1.method563(-1).anInt408))
									class44_sub1_sub1 = class338_sub3_sub1_sub3.aClass44_Sub1_Sub1Array6814[i_7_] = null;
							} else if (i_8_ != 0) {
								if (i_8_ == 1) {
									class44_sub1_sub1.method547(14899);
									class44_sub1_sub1.anInt5808 = i_6_;
								} else if (i_8_ == 2)
									class44_sub1_sub1.method554((byte) -123);
							} else
								class44_sub1_sub1 = class338_sub3_sub1_sub3.aClass44_Sub1_Sub1Array6814[i_7_] = null;
						}
						if (class44_sub1_sub1 == null || !class44_sub1_sub1.method570((byte) 40)) {
							class44_sub1_sub1 = class338_sub3_sub1_sub3.aClass44_Sub1_Sub1Array6814[i_7_] = (new Class44_Sub1_Sub1(class338_sub3_sub1_sub3));
							class44_sub1_sub1.method549((byte) 115, i_4_);
							class44_sub1_sub1.anInt5808 = i_6_;
						}
					} else
						class338_sub3_sub1_sub3.aClass44_Sub1_Sub1Array6814[i_7_] = null;
				}
				i_5_ >>>= 1;
			}
		}
	}

	static final RuntimeException_Sub1 method244(Throwable throwable, String string) {
		RuntimeException_Sub1 runtimeexception_sub1;
		if (throwable instanceof RuntimeException_Sub1) {
			runtimeexception_sub1 = (RuntimeException_Sub1) throwable;
			runtimeexception_sub1.aString3394 += ' ' + (String) string;
		} else
			runtimeexception_sub1 = new RuntimeException_Sub1(throwable, string);
		return runtimeexception_sub1;
	}

	public static void method245(int i) {
		if (i <= 53)
			method242(null, 74, null, null);
		aHa3733 = null;
	}

	static final void method246(ha var_ha, d var_d, int i) {
		if (i == -7618 && Class106.aClass296_Sub39_Sub14_1097 != null) {
			if (Class41_Sub13.anInt3773 < 10) {
				if (!Class106.aClass138_1093.hasFileBuffer_(Class106.aClass296_Sub39_Sub14_1097.aString6216)) {
					Class41_Sub13.anInt3773 = ((Class205_Sub2.aClass138_5631.getFileCompletion_(Class106.aClass296_Sub39_Sub14_1097.aString6216)) / 10);
					return;
				}
				Class41_Sub2.method399(-122);
				Class41_Sub13.anInt3773 = 10;
			}
			if (Class41_Sub13.anInt3773 == 10) {
				Class106.anInt1111 = Class106.aClass296_Sub39_Sub14_1097.anInt6221 >> 6 << 6;
				Class106.anInt1117 = Class106.aClass296_Sub39_Sub14_1097.anInt6217 >> 6 << 6;
				Class106.anInt1116 = (-Class106.anInt1111 + (Class106.aClass296_Sub39_Sub14_1097.anInt6215 >> 6 << 6) + 64);
				Class106.anInt1114 = (Class106.aClass296_Sub39_Sub14_1097.anInt6219 >> 6 << 6) + 64 - Class106.anInt1117;
				int[] is = new int[3];
				int i_9_ = -1;
				int i_10_ = -1;
				if (Class106.aClass296_Sub39_Sub14_1097.method2877(((Class296_Sub51_Sub11.localPlayer.tileY) >> 9) + Class41_Sub26.worldBaseY, is, ((Class296_Sub51_Sub11.localPlayer.tileX) >> 9) + Class206.worldBaseX, (byte) -57, (Class296_Sub51_Sub11.localPlayer.z))) {
					i_9_ = -Class106.anInt1111 + is[1];
					i_10_ = -Class106.anInt1117 + is[2];
				}
				if (!Class368_Sub23.aBoolean5573 && i_9_ >= 0 && Class106.anInt1116 > i_9_ && i_10_ >= 0 && i_10_ < Class106.anInt1114) {
					i_9_ += (int) (Math.random() * 10.0) - 5;
					i_10_ += (int) (Math.random() * 10.0) - 5;
					Class69.anInt3688 = i_9_;
					Class219_Sub1.anInt4569 = i_10_;
				} else if (Class368_Sub9.anInt5476 == -1 || StaticMethods.anInt5947 == -1) {
					Class106.aClass296_Sub39_Sub14_1097.method2874((Class106.aClass296_Sub39_Sub14_1097.anInt6222 & 0x3fff), (Class106.aClass296_Sub39_Sub14_1097.anInt6222 & 0xffff9e3) >> 14, 27253, is);
					Class69.anInt3688 = -Class106.anInt1111 + is[1];
					Class219_Sub1.anInt4569 = -Class106.anInt1117 + is[2];
				} else {
					Class106.aClass296_Sub39_Sub14_1097.method2874(StaticMethods.anInt5947, Class368_Sub9.anInt5476, 27253, is);
					if (is != null) {
						Class219_Sub1.anInt4569 = -Class106.anInt1117 + is[2];
						Class69.anInt3688 = -Class106.anInt1111 + is[1];
					}
					Class368_Sub9.anInt5476 = StaticMethods.anInt5947 = -1;
					Class368_Sub23.aBoolean5573 = false;
				}
				if (Class106.aClass296_Sub39_Sub14_1097.anInt6212 == 37)
					Class106.aFloat1103 = Class106.aFloat1100 = 3.0F;
				else if (Class106.aClass296_Sub39_Sub14_1097.anInt6212 != 50) {
					if (Class106.aClass296_Sub39_Sub14_1097.anInt6212 != 75) {
						if (Class106.aClass296_Sub39_Sub14_1097.anInt6212 != 100) {
							if (Class106.aClass296_Sub39_Sub14_1097.anInt6212 == 200)
								Class106.aFloat1103 = Class106.aFloat1100 = 16.0F;
							else
								Class106.aFloat1103 = Class106.aFloat1100 = 8.0F;
						} else
							Class106.aFloat1103 = Class106.aFloat1100 = 8.0F;
					} else
						Class106.aFloat1103 = Class106.aFloat1100 = 6.0F;
				} else
					Class106.aFloat1103 = Class106.aFloat1100 = 4.0F;
				Class106.anInt1099 = (int) Class106.aFloat1100 >> 1;
				Class106.aByteArrayArrayArray1102 = Class84.method814(Class106.anInt1099, (byte) -54);
				Class296_Sub51_Sub25.method3146((byte) -10);
				Class106.method916();
				Class366_Sub8.aClass155_5409 = new NodeDeque();
				Class106.anInt1106 += (int) (Math.random() * 5.0) - 2;
				if (Class106.anInt1106 < -8)
					Class106.anInt1106 = -8;
				Class106.anInt1104 += (int) (Math.random() * 5.0) - 2;
				if (Class106.anInt1106 > 8)
					Class106.anInt1106 = 8;
				if (Class106.anInt1104 < -16)
					Class106.anInt1104 = -16;
				if (Class106.anInt1104 > 16)
					Class106.anInt1104 = 16;
				Class106.method931(var_d, Class106.anInt1106 >> 2 << 10, Class106.anInt1104 >> 1);
				Class106.aClass245_1094.method2183(256, i ^ ~0x1d41, 1024);
				Class106.aClass401_1096.method4141(256, (byte) 43, 256);
				Class106.aClass38_1091.method365(4096, (byte) 17);
				Class296_Sub43.bitConfigsLoader.method2104(256, 1);
				Class41_Sub13.anInt3773 = 20;
			} else if (Class41_Sub13.anInt3773 == 20) {
				Class106_Sub1.method943(true, i ^ ~0x1dc0);
				Class106.method933(var_ha, Class106.anInt1106, Class106.anInt1104);
				Class41_Sub13.anInt3773 = 60;
				Class106_Sub1.method943(true, 1);
				Class131.method1382(36);
			} else if (Class41_Sub13.anInt3773 == 60) {
				if (!Class106.aClass138_1093.hasFile((Class106.aClass296_Sub39_Sub14_1097.aString6216 + "_staticelements"), true))
					Class106.aClass259_1107 = new Class259(0);
				else {
					if (!Class106.aClass138_1093.hasFileBuffer_((Class106.aClass296_Sub39_Sub14_1097.aString6216 + "_staticelements")))
						return;
					Class106.aClass259_1107 = Class272.method2312(Class172.member, Class106.aClass138_1093, ((Class106.aClass296_Sub39_Sub14_1097.aString6216) + "_staticelements"), (byte) 91);
				}
				Class106.method917();
				Class41_Sub13.anInt3773 = 70;
				Class106_Sub1.method943(true, 1);
				Class131.method1382(125);
			} else if (Class41_Sub13.anInt3773 == 70) {
				Class41_Sub14.aClass256_3776 = new Class256(var_ha, 11, true, Class230.aCanvas2209);
				Class41_Sub13.anInt3773 = 73;
				Class106_Sub1.method943(true, 1);
				Class131.method1382(88);
			} else if (Class41_Sub13.anInt3773 == 73) {
				Class162.aClass256_3559 = new Class256(var_ha, 12, true, Class230.aCanvas2209);
				Class41_Sub13.anInt3773 = 76;
				Class106_Sub1.method943(true, i + 7619);
				Class131.method1382(i ^ ~0x1dde);
			} else if (Class41_Sub13.anInt3773 == 76) {
				Class134.aClass256_1386 = new Class256(var_ha, 14, true, Class230.aCanvas2209);
				Class41_Sub13.anInt3773 = 79;
				Class106_Sub1.method943(true, 1);
				Class131.method1382(i + 7697);
			} else if (Class41_Sub13.anInt3773 == 79) {
				Class41_Sub1.aClass256_3737 = new Class256(var_ha, 17, true, Class230.aCanvas2209);
				Class41_Sub13.anInt3773 = 82;
				Class106_Sub1.method943(true, i ^ ~0x1dc0);
				Class131.method1382(39);
			} else if (Class41_Sub13.anInt3773 == 82) {
				Class183.aClass256_1879 = new Class256(var_ha, 19, true, Class230.aCanvas2209);
				Class41_Sub13.anInt3773 = 85;
				Class106_Sub1.method943(true, 1);
				Class131.method1382(55);
			} else if (Class41_Sub13.anInt3773 == 85) {
				ParamTypeList.aClass256_1694 = new Class256(var_ha, 22, true, Class230.aCanvas2209);
				Class41_Sub13.anInt3773 = 88;
				Class106_Sub1.method943(true, 1);
				Class131.method1382(37);
			} else if (Class41_Sub13.anInt3773 == 88) {
				Class49.aClass256_464 = new Class256(var_ha, 26, true, Class230.aCanvas2209);
				Class41_Sub13.anInt3773 = 91;
				Class106_Sub1.method943(true, 1);
				Class131.method1382(74);
			} else {
				Class264.aClass256_2472 = new Class256(var_ha, 30, true, Class230.aCanvas2209);
				Class41_Sub13.anInt3773 = 100;
				Class106_Sub1.method943(true, 1);
				Class131.method1382(85);
				System.gc();
			}
		}
	}
}
