package net.zaros.client;

/* Class296_Sub35_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub35_Sub2 extends Class296_Sub35 {
	static Class209 aClass209_6110;
	static char[] aCharArray6111 = {'[', ']', '#'};
	static int[] anIntArray6112;
	static int anInt6113;
	static int anInt6114;

	static final int method2754(int i, int i_0_, int i_1_) {
		if (i > -106)
			return 5;
		int i_2_ = 1;
		while (i_0_ > 1) {
			if ((i_0_ & 0x1) != 0)
				i_2_ *= i_1_;
			i_0_ >>= 1;
			i_1_ *= i_1_;
		}
		if (i_0_ == 1)
			return i_1_ * i_2_;
		return i_2_;
	}

	Class296_Sub35_Sub2(int i, int i_3_, int i_4_, int i_5_, int i_6_, float f) {
		super(i, i_3_, i_4_, i_5_, i_6_, f);
	}

	static final void method2756(int i) {
		int i_14_ = -108 % ((-52 - i) / 50);
		if (Class296_Sub39_Sub12.loginState == 10)
			Class296_Sub39_Sub12.loginState = 11;
	}

	final void method2747(int i, int i_15_, int i_16_, int i_17_) {
		if (i_17_ != 16184)
			anInt6114 = -106;
		anInt4849 = i_15_;
		anInt4844 = i_16_;
		anInt4848 = i;
	}

	static final void method2757(int i, int i_18_, int i_19_, int i_20_, int i_21_, int i_22_) {
		if (i_20_ < ConfigurationDefinition.anInt676 || Class288.anInt2652 < i_18_ || i_19_ < EmissiveTriangle.anInt952 || RuntimeException_Sub1.anInt3391 < i_22_)
			Class37.method363(i_22_, i_20_, i_21_, (byte) -111, i_19_, i_18_);
		else
			Class379.method3964(i_19_, i_22_, i_18_, i_20_, i_21_, -1);
		if (i != 93)
			anInt6114 = -36;
	}

	public static void method2758(int i) {
		if (i <= -44) {
			anIntArray6112 = null;
			aClass209_6110 = null;
			aCharArray6111 = null;
		}
	}

	final void method2744(float f, int i) {
		aFloat4846 = f;
		int i_23_ = -108 / ((i - 58) / 34);
	}

	static {
		aClass209_6110 = new Class209(20);
		anIntArray6112 = new int[3];
	}
}
