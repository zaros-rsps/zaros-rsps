package net.zaros.client;

/* Class293 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class293 {
	boolean aBoolean2680;
	boolean aBoolean2681;
	Interface15_Impl1 anInterface15_Impl1_2682;
	Interface15_Impl1 anInterface15_Impl1_2683;
	static int anInt2684;

	final boolean method2414(int i) {
		if (i != -7703) {
			method2418(null, (byte) 122);
		}
		if (!aBoolean2681 || aBoolean2680) {
			return false;
		}
		return true;
	}

	static final String readHuffmanMessage(Packet buffer) {
		return Class16_Sub1.method238(32767, buffer, 110);
	}

	static final int method2416(int i, long l) {
		int i_1_ = -84 / ((i - 10) / 55);
		return (int) (l / 86400000L) - 11745;
	}

	final void method2417(byte i) {
		if (anInterface15_Impl1_2682 != null) {
			anInterface15_Impl1_2682.method31((byte) 82);
		}
		if (i > -25) {
			aBoolean2680 = true;
		}
		aBoolean2681 = false;
	}

	Class293(boolean bool) {
		aBoolean2680 = bool;
	}

	static final int method2418(Class287 class287, byte i) {
		if (class287 == Class199.aClass287_2007) {
			return 5890;
		}
		if (class287 != Class347.aClass287_3025) {
			if (Class153.aClass287_1578 != class287) {
				if (class287 == Class151.aClass287_1553) {
					return 34166;
				}
			} else {
				return 34168;
			}
		} else {
			return 34167;
		}
		if (i != -100) {
			anInt2684 = -59;
		}
		throw new IllegalArgumentException();
	}

	static final void deleteIgnore(String name) {
		if (name != null) {
			String parsedName = Class296_Sub49.parseName(name);
			if (parsedName != null) {
				for (int index = 0; index < Class317.ignoresSize; index++) {
					String name1 = Class362.ignoreNames1[index];
					String parsedName1 = Class296_Sub49.parseName(name1);
					if (Class351.method3684(name1, name, parsedName, parsedName1)) {
						Class317.ignoresSize--;
						for (int i_6_ = index; Class317.ignoresSize > i_6_; i_6_++) {
							Class362.ignoreNames1[i_6_] = Class362.ignoreNames1[i_6_ + 1];
							Class296_Sub51_Sub20.ignoreNames2[i_6_] = Class296_Sub51_Sub20.ignoreNames2[i_6_ + 1];
							Class296_Sub39_Sub11.ignoreNames3[i_6_] = Class296_Sub39_Sub11.ignoreNames3[i_6_ + 1];
							Class123.ignoreNames4[i_6_] = Class123.ignoreNames4[i_6_ + 1];
							StaticMethods.freeIgnoreSlots[i_6_] = StaticMethods.freeIgnoreSlots[i_6_ + 1];
						}
						Class156.anInt3595 = Class338_Sub3_Sub3_Sub1.anInt6616;
						Connection class204 = Class296_Sub51_Sub13.method3111(true);
						Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 125, Class296_Sub44.aClass311_4947);
						class296_sub1.out.p1(Class117.method1015((byte) -102, name));
						class296_sub1.out.writeString(name);
						class204.sendPacket(class296_sub1, (byte) 119);
						break;
					}
				}
			}
		}
	}
}
