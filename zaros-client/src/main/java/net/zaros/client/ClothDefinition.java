package net.zaros.client;
/* Class345 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class ClothDefinition {
	private short[] aShortArray3009;
	private int[] anIntArray3010 = {-1, -1, -1, -1, -1};
	ClotchesLoader loader;
	private int[] anIntArray3012;
	static IncomingPacket aClass231_3013 = new IncomingPacket(110, 2);
	private short[] aShortArray3014;
	private short[] aShortArray3015;
	static int[] anIntArray3016 = new int[25];
	private short[] aShortArray3017;

	final Mesh method3656(byte i) {
		if (i >= -114)
			return null;
		if (anIntArray3012 == null)
			return null;
		Mesh[] class132s = new Mesh[anIntArray3012.length];
		synchronized (loader.aClass138_274) {
			for (int i_0_ = 0; i_0_ < anIntArray3012.length; i_0_++)
				class132s[i_0_] = Class296_Sub51_Sub1.fromJs5((loader.aClass138_274), anIntArray3012[i_0_], 0);
		}
		for (int i_1_ = 0; i_1_ < anIntArray3012.length; i_1_++) {
			if (class132s[i_1_].version_number < 13)
				class132s[i_1_].scale(2);
		}
		Mesh class132;
		if (class132s.length != 1)
			class132 = new Mesh(class132s, class132s.length);
		else
			class132 = class132s[0];
		if (class132 == null)
			return null;
		if (aShortArray3014 != null) {
			for (int i_2_ = 0; aShortArray3014.length > i_2_; i_2_++)
				class132.recolour(aShortArray3014[i_2_], aShortArray3015[i_2_]);
		}
		if (aShortArray3009 != null) {
			for (int i_3_ = 0; aShortArray3009.length > i_3_; i_3_++)
				class132.retexture(aShortArray3009[i_3_], aShortArray3017[i_3_]);
		}
		return class132;
	}

	final boolean method3657(int i) {
		if (i != 2)
			method3660((byte) -22);
		boolean bool = true;
		synchronized (loader.aClass138_274) {
			for (int i_4_ = 0; i_4_ < 5; i_4_++) {
				if (anIntArray3010[i_4_] != -1 && !loader.aClass138_274.fileExists(anIntArray3010[i_4_], 0))
					bool = false;
			}
		}
		return bool;
	}

	final Mesh method3658(byte i) {
		Mesh[] class132s = new Mesh[5];
		int i_5_ = 0;
		synchronized (loader.aClass138_274) {
			for (int i_6_ = 0; i_6_ < 5; i_6_++) {
				if (anIntArray3010[i_6_] != -1)
					class132s[i_5_++] = Class296_Sub51_Sub1.fromJs5((loader.aClass138_274), anIntArray3010[i_6_], 0);
			}
		}
		for (int i_7_ = 0; i_7_ < 5; i_7_++) {
			if (class132s[i_7_] != null && class132s[i_7_].version_number < 13)
				class132s[i_7_].scale(2);
		}
		Mesh class132 = new Mesh(class132s, i_5_);
		if (aShortArray3014 != null) {
			for (int i_8_ = 0; aShortArray3014.length > i_8_; i_8_++)
				class132.recolour(aShortArray3014[i_8_], aShortArray3015[i_8_]);
		}
		int i_9_ = 42 / ((i - 11) / 51);
		if (aShortArray3009 != null) {
			for (int i_10_ = 0; aShortArray3009.length > i_10_; i_10_++)
				class132.retexture(aShortArray3009[i_10_], aShortArray3017[i_10_]);
		}
		return class132;
	}

	private final void parseOpcode(int opc, Packet in) {
		if (opc != 1) {
			if (opc != 2) {
				if (opc != 3) {
					if (opc == 40) {
						int i_12_ = in.g1();
						aShortArray3015 = new short[i_12_];
						aShortArray3014 = new short[i_12_];
						for (int i_13_ = 0; i_13_ < i_12_; i_13_++) {
							aShortArray3014[i_13_] = (short) in.g2();
							aShortArray3015[i_13_] = (short) in.g2();
						}
					} else if (opc != 41) {
						if (opc >= 60 && opc < 70)
							anIntArray3010[opc - 60] = in.g2();
					} else {
						int i_14_ = in.g1();
						aShortArray3017 = new short[i_14_];
						aShortArray3009 = new short[i_14_];
						for (int i_15_ = 0; i_15_ < i_14_; i_15_++) {
							aShortArray3009[i_15_] = (short) in.g2();
							aShortArray3017[i_15_] = (short) in.g2();
						}
					}
				}
			} else {
				int i_16_ = in.g1();
				anIntArray3012 = new int[i_16_];
				for (int i_17_ = 0; i_16_ > i_17_; i_17_++)
					anIntArray3012[i_17_] = in.g2();
			}
		} else
			in.g1();
	}

	final boolean method3660(byte i) {
		if (anIntArray3012 == null)
			return true;
		boolean bool = true;
		synchronized (loader.aClass138_274) {
			for (int i_18_ = 0; anIntArray3012.length > i_18_; i_18_++) {
				if (!loader.aClass138_274.fileExists(anIntArray3012[i_18_], 0))
					bool = false;
			}
		}
		if (i >= -124)
			parseOpcode(86, null);
		return bool;
	}

	public static void method3661(int i) {
		anIntArray3016 = null;
		if (i < 82)
			method3661(-83);
		aClass231_3013 = null;
	}

	static final boolean method3662(int i, int i_19_, int i_20_) {
		if (i_20_ != 0)
			method3662(-128, -15, -40);
		if (!((i_19_ & 0x800) != 0 | Class139.method1465(i, i_19_, 102)) && !Class184.method1857(i, i_19_, (byte) 78))
			return false;
		return true;
	}

	static final int method3663(int i) {
		if (Class338_Sub3_Sub1_Sub5.anIntArray6843 == null)
			return 0;
		if (i != -1)
			return 5;
		return Class338_Sub3_Sub1_Sub5.anIntArray6843.length * 2;
	}

	final void init(Packet in) {
		for (;;) {
			int opc = in.g1();
			if (opc == 0)
				break;
			parseOpcode(opc, in);
		}
	}

	public ClothDefinition() {
		/* empty */
	}
}
