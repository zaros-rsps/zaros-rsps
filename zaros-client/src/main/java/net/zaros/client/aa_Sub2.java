package net.zaros.client;

/* aa_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class aa_Sub2 extends aa {
	static Class131 aClass131_3724;
	static int anInt3725;
	int[] anIntArray3726;
	static boolean aBoolean3727 = false;
	static int anInt3728;
	int[] anIntArray3729;
	static Class289 aClass289_3730;

	public static void method157(byte i) {
		aClass289_3730 = null;
		if (i != 89)
			method157((byte) 24);
		aClass131_3724 = null;
	}

	static final void method158(long l, int i) {
		if (Class338_Sub2.aClass247ArrayArrayArray5195 != null) {
			if (Class361.anInt3103 == 1 || Class361.anInt3103 == 5)
				Connection.method1967(-1, l);
			else if (Class361.anInt3103 == 4)
				Class220.method2070(l, 101);
		}
		Class296_Sub36.method2764((long) Class29.anInt307, (byte) -56, Class41_Sub13.aHa3774);
		if (Class99.anInt1064 != -1)
			Class332.method3414(Class99.anInt1064, true);
		for (int i_0_ = 0; i_0_ < Class154_Sub1.anInt4291; i_0_++) {
			if (Class360_Sub9.aBooleanArray5345[i_0_])
				Class93.aBooleanArray1002[i_0_] = true;
			StaticMethods.aBooleanArray6066[i_0_] = Class360_Sub9.aBooleanArray5345[i_0_];
			Class360_Sub9.aBooleanArray5345[i_0_] = false;
		}
		Class295_Sub2.anInt3498 = Class29.anInt307;
		Class355.method3698(-1, -113, -1, null);
		if (Class99.anInt1064 != -1) {
			Class154_Sub1.anInt4291 = 0;
			Class68.method716((byte) 47);
		}
		Class41_Sub13.aHa3774.la();
		Class368_Sub14.method3850(Class41_Sub13.aHa3774, (byte) 109);
		int i_1_ = Class296_Sub51_Sub38.method3195((byte) 111);
		if (i_1_ == -1)
			i_1_ = Class41_Sub19.anInt3793;
		if (i_1_ == -1)
			i_1_ = Class296_Sub51_Sub32.anInt6512;
		StaticMethods.method2479(-1, i_1_);
		int i_2_ = (Class296_Sub51_Sub11.localPlayer.getSize() << 8);
		if (i < 64)
			aClass289_3730 = null;
		Class206.method1989(((Class296_Sub51_Sub11.localPlayer.tileX) + i_2_), i_2_ + (Class296_Sub51_Sub11.localPlayer.tileY), (Class296_Sub51_Sub11.localPlayer.z), Class217.anInt2119, (byte) -10);
		Class217.anInt2119 = 0;
	}

	aa_Sub2(int i, int i_3_, int[] is, int[] is_4_) {
		anIntArray3729 = is_4_;
		anIntArray3726 = is;
	}

	static {
		aClass131_3724 = new Class131();
		aClass289_3730 = new Class289();
	}
}
