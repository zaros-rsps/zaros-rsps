package net.zaros.client;
/* Class177 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Calendar;

final class Class177 {
	static int anInt1845 = 0;
	static OutgoingPacket aClass311_1846 = new OutgoingPacket(90, 3);
	static Class224[] aClass224Array1847;

	static final String method1713(long l, int i, boolean bool, byte i_0_) {
		Calendar calendar;
		if (!bool) {
			Class188.method1889(l, 26998);
			calendar = Class296_Sub38.aCalendar4889;
		} else {
			Class296_Sub51_Sub27_Sub1.method3159(112, l);
			calendar = Class296_Sub38.aCalendar4898;
		}
		int i_1_ = calendar.get(5);
		if (i_0_ <= 52)
			return null;
		int i_2_ = calendar.get(2) + 1;
		int i_3_ = calendar.get(1);
		int i_4_ = calendar.get(11);
		int i_5_ = calendar.get(12);
		return (Integer.toString(i_1_ / 10) + i_1_ % 10 + "/" + i_2_ / 10 + i_2_ % 10 + "/" + i_3_ % 100 / 10 + i_3_ % 10 + " " + i_4_ / 10 + i_4_ % 10 + ":" + i_5_ / 10 + i_5_ % 10);
	}

	public static void method1714(int i) {
		if (i != 3858)
			method1713(105L, 26, true, (byte) 121);
		aClass224Array1847 = null;
		aClass311_1846 = null;
	}
}
