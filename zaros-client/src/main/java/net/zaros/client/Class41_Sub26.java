package net.zaros.client;

/* Class41_Sub26 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub26 extends Class41 {
	static float aFloat3806 = 0.0F;
	static int anInt3807 = -1;
	static Js5 aClass138_3808;
	static int worldBaseY;

	final int method497(int i) {
		if (i <= 114)
			anInt3807 = 21;
		return anInt389;
	}

	static final boolean method498(int i, int i_0_, byte i_1_) {
		int i_2_ = -118 % ((i_1_ + 2) / 38);
		if ((i & 0x800) == 0)
			return false;
		return true;
	}

	final void method381(int i, byte i_3_) {
		if (i_3_ == -110)
			anInt389 = i;
	}

	Class41_Sub26(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	static final Class209[] method499(int i) {
		if (i != -34)
			return null;
		return (new Class209[]{Class313.aClass209_2776, Class399.aClass209_3354, Class338_Sub6.aClass209_5227, Class338_Sub2.aClass209_5192, TranslatableString.aClass209_1260, Class164.aClass209_1682, Class78.aClass209_3411, aa_Sub1.aClass209_3722, Class157.aClass209_1596, Class79.aClass209_885, Class368_Sub10.aClass209_5479, Class41_Sub21.aClass209_3798, Class296_Sub35_Sub2.aClass209_6110, Class245.aClass209_2329, Class41_Sub12.aClass209_3770, Class144.aClass209_1434, PlayerEquipment.aClass209_3376, Class296_Sub51_Sub12.aClass209_6409, Texture.aClass209_6232, Class44_Sub1.aClass209_3856, Class135.aClass209_1394, Class296_Sub45_Sub3.aClass209_6291, Class41_Sub9.aClass209_3766, r_Sub2.aClass209_6716, Class16_Sub3_Sub1.aClass209_5805, Class41_Sub5.aClass209_3755, Class379_Sub2.aClass209_5677, Class181_Sub1.aClass209_4513, Class83.aClass209_919, Class107.aClass209_3574, StaticMethods.aClass209_5941});
	}

	static final boolean method500(int i, int i_4_, int i_5_) {
		if (i_4_ <= 72)
			worldBaseY = -123;
		if ((i_5_ & 0x34) == 0)
			return false;
		return true;
	}

	final int method380(int i, byte i_6_) {
		if (i_6_ != 41)
			return 49;
		if (aClass296_Sub50_392.method3050(28520) == Class363.runescape) {
			if (aClass296_Sub50_392.method3054(84))
				return 3;
			if (i == 0 || aClass296_Sub50_392.aClass41_Sub29_5002.method515(117) == 1)
				return 1;
			return 2;
		}
		return 3;
	}

	final boolean method501(int i) {
		if (i != -25952)
			method386(60);
		if (aClass296_Sub50_392.method3050(i + 54472) == Class363.runescape) {
			if (aClass296_Sub50_392.method3054(59))
				return false;
			return true;
		}
		return false;
	}

	static final void method502(int i, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_) {
		int i_14_ = i_10_ + i_7_;
		int i_15_ = -i_10_ + i_11_;
		for (int i_16_ = i_7_; i_14_ > i_16_; i_16_++)
			Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_16_]), i_12_, (byte) 127, i_13_, i_9_);
		int i_17_ = i_10_ + i_9_;
		int i_18_ = i_12_ - i_10_;
		for (int i_19_ = i_11_; i_19_ > i_15_; i_19_--)
			Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_19_]), i_12_, (byte) -1, i_13_, i_9_);
		for (int i_20_ = i_14_; i_20_ <= i_15_; i_20_++) {
			int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_20_];
			Class296_Sub14.method2511(is, i_17_, (byte) 125, i_13_, i_9_);
			Class296_Sub14.method2511(is, i_18_, (byte) 115, i, i_17_);
			Class296_Sub14.method2511(is, i_12_, (byte) -127, i_13_, i_18_);
		}
		if (i_8_ != -1)
			method499(1);
	}

	final int method383(byte i) {
		if (i != 110)
			aFloat3806 = 1.3378646F;
		return 1;
	}

	final void method386(int i) {
		if (aClass296_Sub50_392.method3050(28520) != Class363.runescape)
			anInt389 = 1;
		else if (aClass296_Sub50_392.method3054(i + 98))
			anInt389 = 0;
		if (anInt389 != 0 && anInt389 != 1)
			anInt389 = method383((byte) 110);
		if (i != 2)
			aFloat3806 = 0.31170654F;
	}

	public static void method503(boolean bool) {
		if (!bool)
			aClass138_3808 = null;
	}

	Class41_Sub26(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}
}
