package net.zaros.client;
import jaggl.OpenGL;

final class Class366_Sub7 extends Class366 {
	static short aShort5396 = 320;
	private boolean aBoolean5397;
	private Class99 aClass99_5398;
	private boolean aBoolean5399 = false;
	private Class99 aClass99_5400;
	static OutgoingPacket aClass311_5401;
	static boolean aBoolean5402;
	private Class99 aClass99_5403;
	private Class69_Sub1 aClass69_Sub1_5404;
	private boolean aBoolean5405;
	static boolean aBoolean5406 = false;
	private boolean aBoolean5407;
	private Class99 aClass99_5408;

	final void method3770(byte i, boolean bool) {
		if (i != 33) {
			/* empty */
		}
	}

	final void method3790(int i) {
		if (i < 90)
			method3770((byte) -64, true);
		if (aBoolean5405) {
			int i_0_ = aHa_Sub3_3121.XA();
			int i_1_ = aHa_Sub3_3121.i();
			float f = (float) i_0_ - (float) (-i_1_ + i_0_) * 0.125F;
			float f_2_ = (float) i_0_ - (float) (i_0_ - i_1_) * 0.25F;
			OpenGL.glProgramLocalParameter4fARB(34336, 0, f_2_, f, 1.0F / (float) aHa_Sub3_3121.anInt4173, (float) aHa_Sub3_3121.anInt4211 / 255.0F);
			aHa_Sub3_3121.method1330(115, 1);
			aHa_Sub3_3121.method1354(false, aHa_Sub3_3121.anInt4178);
			aHa_Sub3_3121.method1330(114, 0);
		}
	}

	Class366_Sub7(ha_Sub3 var_ha_Sub3) {
		super(var_ha_Sub3);
		if (aHa_Sub3_3121.aBoolean4228) {
			aClass99_5408 = (Class296_Sub42.method2919(aHa_Sub3_3121, -7, 34336, "!!ARBvp1.0\nATTRIB  iPos         = vertex.position;\nATTRIB  iColour      = vertex.color;\nATTRIB  iTexCoord    = vertex.texcoord[0];\nOUTPUT  oPos         = result.position;\nOUTPUT  oColour      = result.color;\nOUTPUT  oTexCoord0   = result.texcoord[0];\nOUTPUT  oTexCoord1   = result.texcoord[1];\nOUTPUT  oFogCoord    = result.fogcoord;\nPARAM   fogParams    = program.local[0];\nPARAM   waterPlane   = program.local[1];\nPARAM   tMatrix[4]   = { state.matrix.texture[0] };\nPARAM   pMatrix[4]   = { state.matrix.projection };\nPARAM   mvMatrix[4]  = { state.matrix.modelview };\nTEMP    viewPos, fogFactor;\nDP4   viewPos.x, mvMatrix[0], iPos;\nDP4   viewPos.y, mvMatrix[1], iPos;\nDP4   viewPos.z, mvMatrix[2], iPos;\nDP4   viewPos.w, mvMatrix[3], iPos;\nSUB   fogFactor.x, -viewPos.z, fogParams.x;\nMUL   fogFactor.x, fogFactor.x, 0.001953125;\nMAD   fogFactor.y, iTexCoord.z, fogParams.z, fogParams.w;\nSUB   fogFactor.z, -viewPos.z, fogParams.y;\nMUL   fogFactor.z, fogFactor.z, 0.00390625;\nMUL   fogFactor.x, fogFactor.x, fogFactor.y;\nMIN   fogFactor, fogFactor, 1;\nMAX   fogFactor, fogFactor, 0;\nMUL   fogFactor.z, fogFactor.z, iTexCoord.z;\nMAD   viewPos.xyz, waterPlane.xyzw, fogFactor.zzzz, viewPos.xyzw;\nMAX   oTexCoord1.xyz, fogFactor.xxxx, fogFactor.yyyy;\nMOV   oTexCoord1.w, 1;\nMOV   oColour, iColour;\nDP4   oPos.x, pMatrix[0], viewPos;\nDP4   oPos.y, pMatrix[1], viewPos;\nDP4   oPos.z, pMatrix[2], viewPos;\nDP4   oPos.w, pMatrix[3], viewPos;\nMOV   oFogCoord.x, viewPos.z;\nDP3   oTexCoord0.x, tMatrix[0], iTexCoord;\nDP3   oTexCoord0.y, tMatrix[1], iTexCoord;\nMOV   oTexCoord0.zw, iTexCoord;\nEND\n"));
			aClass99_5403 = (Class296_Sub42.method2919(aHa_Sub3_3121, -110, 34336, "!!ARBvp1.0\nATTRIB  iPos         = vertex.position;\nATTRIB  iNormal      = vertex.normal;\nATTRIB  iColour      = vertex.color;\nATTRIB  iTexCoord    = vertex.texcoord[0];\nOUTPUT  oPos         = result.position;\nOUTPUT  oColour      = result.color;\nOUTPUT  oTexCoord0   = result.texcoord[0];\nOUTPUT  oTexCoord1   = result.texcoord[1];\nOUTPUT  oFogCoord    = result.fogcoord;\nPARAM   fogParams    = program.local[0];\nPARAM   waterPlane   = program.local[1];\nPARAM   tMatrix[4]   = { state.matrix.texture[0] };\nPARAM   pMatrix[4]   = { state.matrix.projection };\nPARAM   mvMatrix[4]  = { state.matrix.modelview };\nTEMP    viewPos, viewNormal, fogFactor, colour, ndotl;\nDP4   viewPos.x, mvMatrix[0], iPos;\nDP4   viewPos.y, mvMatrix[1], iPos;\nDP4   viewPos.z, mvMatrix[2], iPos;\nDP4   viewPos.w, mvMatrix[3], iPos;\nSUB   fogFactor.x, -viewPos.z, fogParams.x;\nMUL   fogFactor.x, fogFactor.x, 0.001953125;\nMAD   fogFactor.y, iTexCoord.z, fogParams.z, fogParams.w;\nSUB   fogFactor.z, -viewPos.z, fogParams.y;\nMUL   fogFactor.z, fogFactor.z, 0.00390625;\nMUL   fogFactor.x, fogFactor.x, fogFactor.y;\nMIN   fogFactor, fogFactor, 1;\nMAX   fogFactor, fogFactor, 0;\nMUL   fogFactor.z, fogFactor.z, iTexCoord.z;\nMAD   viewPos.xyz, waterPlane.xyzw, fogFactor.zzzz, viewPos.xyzw;\nMAX   oTexCoord1.xyz, fogFactor.xxxx, fogFactor.yyyy;\nMOV   oTexCoord1.w, 1;\nDP3   viewNormal.x, mvMatrix[0], iNormal;\nDP3   viewNormal.y, mvMatrix[1], iNormal;\nDP3   viewNormal.z, mvMatrix[2], iNormal;\nDP3   ndotl.x, viewNormal, state.light[0].position;\nDP3   ndotl.y, viewNormal, state.light[1].position;\nMAX   ndotl, ndotl, 0;\nMOV   colour, state.lightmodel.ambient;\nMAD   colour, state.light[0].diffuse, ndotl.xxxx, colour;\nMAD   colour, state.light[1].diffuse, ndotl.yyyy, colour;\nMUL   oColour, iColour, colour;\nDP4   oPos.x, pMatrix[0], viewPos;\nDP4   oPos.y, pMatrix[1], viewPos;\nDP4   oPos.z, pMatrix[2], viewPos;\nDP4   oPos.w, pMatrix[3], viewPos;\nMOV   oFogCoord.x, viewPos.z;\nDP3   oTexCoord0.x, tMatrix[0], iTexCoord;\nDP3   oTexCoord0.y, tMatrix[1], iTexCoord;\nMOV   oTexCoord0.zw, iTexCoord;\nEND\n"));
			aClass99_5400 = (Class296_Sub42.method2919(aHa_Sub3_3121, -27, 34336, "!!ARBvp1.0\nATTRIB  iPos         = vertex.position;\nATTRIB  iColour      = vertex.color;\nATTRIB  iTexCoord    = vertex.texcoord[0];\nOUTPUT  oPos         = result.position;\nOUTPUT  oColour      = result.color;\nOUTPUT  oTexCoord0   = result.texcoord[0];\nOUTPUT  oTexCoord1   = result.texcoord[1];\nOUTPUT  oFogCoord    = result.fogcoord;\nPARAM   fogParams    = program.local[0];\nPARAM   waterPlane   = program.local[1];\nPARAM   pMatrix[4]   = { state.matrix.projection };\nPARAM   mvMatrix[4]  = { state.matrix.modelview };\nPARAM   texMatrix[4] = { state.matrix.texture[0] };\nTEMP    viewPos, fogFactor, depth;\nDP4   viewPos.x, mvMatrix[0], iPos;\nDP4   viewPos.y, mvMatrix[1], iPos;\nDP4   viewPos.z, mvMatrix[2], iPos;\nDP4   viewPos.w, mvMatrix[3], iPos;\nSUB   fogFactor.x, -viewPos.z, fogParams.x;\nMUL   fogFactor.x, fogFactor.x, 0.001953125;\nDP4   depth, waterPlane, viewPos;\nMAD   fogFactor.y, -depth, fogParams.z, fogParams.w;\nSUB   fogFactor.z, -viewPos.z, fogParams.y;\nMUL   fogFactor.z, fogFactor.z, 0.00390625;\nMIN   fogFactor, fogFactor, 1;\nMAX   fogFactor, fogFactor, 0;\nMUL   fogFactor.z, fogFactor.z, -depth;\nMAD   viewPos.xyz, waterPlane.xyzw, fogFactor.zzzz, viewPos.xyzw;\nMAX   oTexCoord1.xyz, fogFactor.xxxx, fogFactor.yyyy;\nMOV   oTexCoord1.w, 1;\nMOV   oColour, iColour;\nDP4   oPos.x, pMatrix[0], viewPos;\nDP4   oPos.y, pMatrix[1], viewPos;\nDP4   oPos.z, pMatrix[2], viewPos;\nDP4   oPos.w, pMatrix[3], viewPos;\nMOV   oFogCoord.x, viewPos.z;\nDP4   oTexCoord0.x, texMatrix[0], iTexCoord;\nDP4   oTexCoord0.y, texMatrix[1], iTexCoord;\nDP4   oTexCoord0.z, texMatrix[2], iTexCoord;\nMOV   oTexCoord0.w, 1;\nEND\n"));
			aClass99_5398 = (Class296_Sub42.method2919(aHa_Sub3_3121, -92, 34336, "!!ARBvp1.0\nATTRIB  iPos         = vertex.position;\nATTRIB  iNormal      = vertex.normal;\nATTRIB  iColour      = vertex.color;\nATTRIB  iTexCoord    = vertex.texcoord[0];\nOUTPUT  oPos         = result.position;\nOUTPUT  oColour      = result.color;\nOUTPUT  oTexCoord0   = result.texcoord[0];\nOUTPUT  oTexCoord1   = result.texcoord[1];\nOUTPUT  oFogCoord    = result.fogcoord;\nPARAM   fogParams    = program.local[0];\nPARAM   waterPlane   = program.local[1];\nPARAM   pMatrix[4]   = { state.matrix.projection };\nPARAM   mvMatrix[4]  = { state.matrix.modelview };\nPARAM   texMatrix[4] = { state.matrix.texture[0] };\nTEMP    viewPos, viewNormal, fogFactor, depth, colour, ndotl;\nDP4   viewPos.x, mvMatrix[0], iPos;\nDP4   viewPos.y, mvMatrix[1], iPos;\nDP4   viewPos.z, mvMatrix[2], iPos;\nDP4   viewPos.w, mvMatrix[3], iPos;\nSUB   fogFactor.x, -viewPos.z, fogParams.x;\nMUL   fogFactor.x, fogFactor.x, 0.001953125;\nDP4   depth, waterPlane, viewPos;\nMAD   fogFactor.y, -depth, fogParams.z, fogParams.w;\nSUB   fogFactor.z, -viewPos.z, fogParams.y;\nMUL   fogFactor.z, fogFactor.z, 0.00390625;\nMIN   fogFactor, fogFactor, 1;\nMAX   fogFactor, fogFactor, 0;\nMUL   fogFactor.z, fogFactor.z, -depth;\nMAD   viewPos.xyz, waterPlane.xyzw, fogFactor.zzzz, viewPos.xyzw;\nMAX   oTexCoord1.xyz, fogFactor.xxxx, fogFactor.yyyy;\nMOV   oTexCoord1.w, 1;\nDP3   viewNormal.x, mvMatrix[0], iNormal;\nDP3   viewNormal.y, mvMatrix[1], iNormal;\nDP3   viewNormal.z, mvMatrix[2], iNormal;\nDP3   ndotl.x, viewNormal, state.light[0].position;\nDP3   ndotl.y, viewNormal, state.light[1].position;\nMAX   ndotl, ndotl, 0;\nMOV   colour, state.lightmodel.ambient;\nMAD   colour, state.light[0].diffuse, ndotl.xxxx, colour;\nMAD   colour, state.light[1].diffuse, ndotl.yyyy, colour;\nMUL   oColour, iColour, colour;\nDP4   oPos.x, pMatrix[0], viewPos;\nDP4   oPos.y, pMatrix[1], viewPos;\nDP4   oPos.z, pMatrix[2], viewPos;\nDP4   oPos.w, pMatrix[3], viewPos;\nMOV   oFogCoord.x, viewPos.z;\nDP4   oTexCoord0.x, texMatrix[0], iTexCoord;\nDP4   oTexCoord0.y, texMatrix[1], iTexCoord;\nDP4   oTexCoord0.z, texMatrix[2], iTexCoord;\nMOV   oTexCoord0.w, 1;\nEND\n"));
			if (aClass99_5398 != null & (aClass99_5403 != null & aClass99_5408 != null & aClass99_5400 != null)) {
				aClass69_Sub1_5404 = new Class69_Sub1(var_ha_Sub3, 3553, 6406, 2, 1, false, new byte[]{0, -1}, 6406, false);
				aClass69_Sub1_5404.method733(false, false, true);
				aBoolean5407 = true;
			} else
				aBoolean5407 = false;
		} else
			aBoolean5407 = false;
	}

	final void method3791(boolean bool) {
		Class373_Sub1 class373_sub1 = aHa_Sub3_3121.aClass373_Sub1_4170;
		if (aBoolean5397)
			OpenGL.glBindProgramARB(34336, (aHa_Sub3_3121.anInt4200 != 2147483647 ? aClass99_5398.anInt1061 : aClass99_5403.anInt1061));
		else
			OpenGL.glBindProgramARB(34336, (aHa_Sub3_3121.anInt4200 == 2147483647 ? aClass99_5408.anInt1061 : aClass99_5400.anInt1061));
		class373_sub1.method3921(0.0F, (float) aHa_Sub3_3121.anInt4200, false, Class320.aFloatArray2821, -1.0F, 0.0F);
		OpenGL.glProgramLocalParameter4fARB(34336, 1, Class320.aFloatArray2821[0], Class320.aFloatArray2821[1], Class320.aFloatArray2821[2], Class320.aFloatArray2821[3]);
		OpenGL.glEnable(34336);
		aBoolean5405 = bool;
		method3790(127);
	}

	final void method3766(int i) {
		if (i < 30)
			aClass311_5401 = null;
		if (aBoolean5405) {
			OpenGL.glBindProgramARB(34336, 0);
			OpenGL.glDisable(34820);
			OpenGL.glDisable(34336);
			aBoolean5405 = false;
		}
		aHa_Sub3_3121.method1330(114, 1);
		aHa_Sub3_3121.method1316(null, (byte) -103);
		aHa_Sub3_3121.method1306(8448, 8448, -22394);
		aHa_Sub3_3121.method1283(0, 5890, 768, (byte) -127);
		aHa_Sub3_3121.method1283(2, 34166, 770, (byte) -123);
		aHa_Sub3_3121.method1346(true, 770, 0, 5890);
		aHa_Sub3_3121.method1330(122, 0);
		if (aBoolean5399) {
			aHa_Sub3_3121.method1283(0, 5890, 768, (byte) -111);
			aHa_Sub3_3121.method1346(true, 770, 0, 5890);
			aBoolean5399 = false;
		}
	}

	final void method3768(byte i, boolean bool) {
		aBoolean5397 = bool;
		aHa_Sub3_3121.method1330(120, 1);
		aHa_Sub3_3121.method1316(aClass69_Sub1_5404, (byte) -127);
		aHa_Sub3_3121.method1306(34165, 7681, -22394);
		aHa_Sub3_3121.method1283(0, 34166, 768, (byte) -124);
		if (i == -88) {
			aHa_Sub3_3121.method1283(2, 5890, 770, (byte) -128);
			aHa_Sub3_3121.method1346(true, 770, 0, 34168);
			aHa_Sub3_3121.method1330(114, 0);
			method3791(true);
		}
	}

	final void method3769(int i, byte i_3_, int i_4_) {
		if (i_3_ != -81)
			aClass99_5400 = null;
	}

	static final void method3792() {
		Class360_Sub6.method3746(1, Class368_Sub9.anInt5477);
	}

	public static void method3793(int i) {
		aClass311_5401 = null;
		if (i > -60)
			aBoolean5402 = false;
	}

	final boolean method3763(int i) {
		int i_5_ = -116 / ((i - 73) / 40);
		return aBoolean5407;
	}

	final void method3764(boolean bool, int i, Class69 class69) {
		if (bool)
			aBoolean5397 = false;
		if (class69 == null) {
			if (!aBoolean5399) {
				aHa_Sub3_3121.method1316(aHa_Sub3_3121.aClass69_Sub1_4195, (byte) -117);
				aHa_Sub3_3121.method1272((byte) -107, 1);
				aHa_Sub3_3121.method1283(0, 34168, 768, (byte) -123);
				aHa_Sub3_3121.method1346(true, 770, 0, 34168);
				aBoolean5399 = true;
			}
		} else {
			if (aBoolean5399) {
				aHa_Sub3_3121.method1283(0, 5890, 768, (byte) -126);
				aHa_Sub3_3121.method1346(true, 770, 0, 5890);
				aBoolean5399 = false;
			}
			aHa_Sub3_3121.method1316(class69, (byte) -120);
			aHa_Sub3_3121.method1272((byte) -107, i);
		}
	}

	static {
		aClass311_5401 = new OutgoingPacket(59, -1);
	}
}
