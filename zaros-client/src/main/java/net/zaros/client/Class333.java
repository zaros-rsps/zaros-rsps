package net.zaros.client;

/* Class333 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class333 {
	private Js5 aClass138_2944;
	private Js5 aClass138_2945;
	private Class358 aClass358_2946;

	static final void method3417(int i) {
		Class296_Sub51_Sub36.aHa6529 = null;
		Class338_Sub9.aClass373_5269 = null;
		EmissiveTriangle.anInt958 = -1;
		IOException_Sub1.anInterface13Array38 = null;
		Class241_Sub2_Sub1.anInterface13_5901 = null;
		BillboardRaw.anIntArray1080 = null;
		Class252.anInt2386 = -1;
		ConfigsRegister.anInt3674 = -1;
		Class94.aClass373_1016 = null;
		Class368_Sub23.anInt5569 = -1;
		Class373_Sub3.aClass373_5612 = null;
		if (i <= 70)
			method3417(22);
		Class379_Sub3.aClass264_5687.method2277((byte) 121);
	}

	final Interface9 method3418(Interface10 interface10, int i) {
		if (i >= -121)
			return null;
		if (interface10 == null)
			return null;
		Class294 class294 = interface10.method45((byte) 78);
		if (Class296_Sub34_Sub2.aClass294_6098 == class294)
			return new Class192((Class156) interface10);
		if (class294 == Animation.aClass294_422)
			return new Class162(method3419(16290), (Class140) interface10);
		if (Class397_Sub3.aClass294_5798 == class294)
			return new Class32(aClass138_2945, (Class107) interface10);
		if (Class110.aClass294_1140 == class294)
			return new Class32_Sub1(aClass138_2945, (Class107_Sub1) interface10);
		if (class294 == Class122_Sub3.aClass294_5663)
			return new Class122_Sub2(aClass138_2945, aClass138_2944, (Class379_Sub1) interface10);
		if (Class391.aClass294_3291 == class294)
			return new Class122_Sub3(aClass138_2945, aClass138_2944, (Class379_Sub3) interface10);
		if (Class296_Sub51_Sub20.aClass294_6441 == class294)
			return new Class122_Sub1(aClass138_2945, aClass138_2944, (Class379_Sub2) interface10);
		if (Class227.aClass294_2186 == class294)
			return new Class10(aClass138_2945, aClass138_2944, (Class266) interface10);
		if (class294 == Class296_Sub39_Sub14.aClass294_6218)
			return new Class203(aClass138_2945, (Class254) interface10);
		if (class294 == Class83.aClass294_917)
			return new Class122_Sub1_Sub1(aClass138_2945, aClass138_2944, (Class379_Sub2_Sub1) interface10);
		return null;
	}

	private final Class358 method3419(int i) {
		if (i != 16290)
			aClass358_2946 = null;
		if (aClass358_2946 == null)
			aClass358_2946 = new Class358();
		return aClass358_2946;
	}

	Class333(Js5 class138, Js5 class138_0_) {
		aClass138_2945 = class138;
		aClass138_2944 = class138_0_;
	}
}
