package net.zaros.client;

/* Callback_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;

import com.ms.dll.Callback;
import com.ms.dll.Root;
import com.ms.win32.User32;

final class Callback_Sub1 extends Callback {

	private volatile int anInt1;
	private volatile boolean aBoolean2 = true;
	private volatile int anInt3;
	private int anInt4;
	private boolean aBoolean5;

	final void method80(boolean bool, int i, Component component) {
		Object peer = ReflectionUtil.getPeer(component);
		int i_0_ = (int) ReflectionUtil.getHwnd(peer);
		if (anInt3 != i_0_ || !bool == aBoolean2) {
			if (!aBoolean5) {
				anInt4 = User32.LoadCursor(0, 32512);
				Root.alloc(this);
				aBoolean5 = true;
			}
			if (anInt3 != i_0_) {
				if (anInt3 != 0) {
					aBoolean2 = true;
					User32.SendMessage(i_0_, 101024, 0, 0);
					synchronized (this) {
						User32.SetWindowLong(anInt3, -4, anInt1);
					}
				}
				synchronized (this) {
					anInt3 = i_0_;
					anInt1 = User32.SetWindowLong(anInt3, -4, this);
				}
			}
			aBoolean2 = bool;
			if (i == 65535)
				User32.SendMessage(i_0_, 101024, 0, 0);
		}
	}

	final void method81(int i, int i_1_, int i_2_) {
		User32.SetCursorPos(i, i_1_);
		if (i_2_ >= -43)
			method82(126, -81, 118, -39);
	}

	final synchronized int method82(int i, int i_3_, int i_4_, int i_5_) {
		if (anInt3 != i) {
			int i_6_ = User32.GetWindowLong(i, -4);
			return User32.CallWindowProc(i_6_, i, i_3_, i_4_, i_5_);
		}
		if (i_3_ == 32) {
			int i_7_ = i_5_ & 0xffff;
			if (i_7_ == 1) {
				User32.SetCursor(!aBoolean2 ? 0 : anInt4);
				return 0;
			}
		}
		if (i_3_ == 101024) {
			User32.SetCursor(aBoolean2 ? anInt4 : 0);
			return 0;
		}
		if (i_3_ == 1) {
			anInt3 = 0;
			aBoolean2 = true;
		}
		return User32.CallWindowProc(anInt1, i, i_3_, i_4_, i_5_);
	}
}
