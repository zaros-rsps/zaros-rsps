package net.zaros.client;
/* Class214 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;

import com.ms.directX.DSBufferDesc;
import com.ms.directX.DSCursors;
import com.ms.directX.DirectSound;
import com.ms.directX.DirectSoundBuffer;
import com.ms.directX.WaveFormatEx;

public final class Class214 implements Interface8 {
	private DSBufferDesc[] aDSBufferDescArray3517 = new DSBufferDesc[2];
	private byte[][] aByteArrayArray3518;
	private DSCursors[] aDSCursorsArray3519;
	private DirectSoundBuffer[] aDirectSoundBufferArray3520;
	private int anInt3521;
	private boolean[] aBooleanArray3522;
	private int[] anIntArray3523 = new int[2];
	private int[] anIntArray3524;
	private int anInt3525;
	private int[] anIntArray3526;
	private DirectSound aDirectSound3527;
	private WaveFormatEx aWaveFormatEx3528;
	private int anInt3529;

	public final void method41(int i, byte i_0_) {
		aDirectSoundBufferArray3520[i].stop();
		aBooleanArray3522[i] = false;
		aDirectSoundBufferArray3520[i].setCurrentPosition(0);
		anIntArray3526[i] = 0;
		if (i_0_ >= -12)
			aBooleanArray3522 = null;
	}

	public final void method37(int i, int[] is) {
		int i_1_ = is.length;
		if (i_1_ != anInt3529 * 256)
			throw new IllegalArgumentException();
		int i_2_ = anIntArray3526[i] * anInt3525;
		for (int i_3_ = 0; i_3_ < i_1_; i_3_++) {
			int i_4_ = is[i_3_];
			if ((i_4_ + 8388608 & ~0xffffff) != 0)
				i_4_ = 0x7fffff ^ i_4_ >> 31;
			aByteArrayArray3518[i][i_2_ + i_3_ * 2] = (byte) (i_4_ >> 8);
			aByteArrayArray3518[i][i_2_ + i_3_ * 2 + 1] = (byte) (i_4_ >> 16);
		}
		aDirectSoundBufferArray3520[i].writeBuffer(i_2_, i_1_ * 2, aByteArrayArray3518[i], 0);
		anIntArray3526[i] = anIntArray3526[i] + i_1_ / anInt3529 & 0xffff;
		if (!aBooleanArray3522[i]) {
			aDirectSoundBufferArray3520[i].play(1);
			aBooleanArray3522[i] = true;
		}
	}

	public final void method36(Component component, boolean bool, int i, int i_5_) throws Exception {
		if (anInt3521 == 0) {
			if (i < 8000 || i > 48000)
				throw new IllegalArgumentException();
			anInt3525 = bool ? 4 : 2;
			anInt3529 = bool ? 2 : 1;
			anIntArray3524 = new int[anInt3529 * 256];
			aDirectSound3527.initialize(null);
			aDirectSound3527.setCooperativeLevel(component, 2);
			for (int i_6_ = 0; i_6_ < 2; i_6_++)
				aDSBufferDescArray3517[i_6_].flags = 16384;
			if (i_5_ > -15)
				method37(-21, null);
			aWaveFormatEx3528.avgBytesPerSec = anInt3525 * i;
			anInt3521 = i;
			aWaveFormatEx3528.formatTag = 1;
			aWaveFormatEx3528.samplesPerSec = i;
			aWaveFormatEx3528.blockAlign = anInt3525;
			aWaveFormatEx3528.channels = anInt3529;
			aWaveFormatEx3528.bitsPerSample = 16;
		}
	}

	public final void method39(int i, int i_7_, int i_8_) throws Exception {
		if (anInt3521 == 0 || aDirectSoundBufferArray3520[i_7_] != null)
			throw new IllegalStateException();
		int i_9_ = anInt3525 * 65536;
		if (aByteArrayArray3518[i_7_] == null || aByteArrayArray3518[i_7_].length != i_9_) {
			aByteArrayArray3518[i_7_] = new byte[i_9_];
			aDSBufferDescArray3517[i_7_].bufferBytes = i_9_;
		}
		aDirectSoundBufferArray3520[i_7_] = aDirectSound3527.createSoundBuffer(aDSBufferDescArray3517[i_7_], aWaveFormatEx3528);
		aBooleanArray3522[i_7_] = false;
		anIntArray3526[i_7_] = 0;
		if (i == 2)
			anIntArray3523[i_7_] = i_8_;
	}

	public final int method38(int i, byte i_10_) {
		if (!aBooleanArray3522[i])
			return 0;
		aDirectSoundBufferArray3520[i].getCurrentPosition(aDSCursorsArray3519[i]);
		if (i_10_ <= 63)
			anIntArray3526 = null;
		int i_11_ = aDSCursorsArray3519[i].write / anInt3525;
		int i_12_ = -i_11_ + anIntArray3526[i] & 0xffff;
		if (anIntArray3523[i] < i_12_) {
			for (int i_13_ = -anIntArray3526[i] + i_11_ & 0xffff; i_13_ > 0; i_13_ -= 256)
				method37(i, anIntArray3524);
			i_12_ = -i_11_ + anIntArray3526[i] & 0xffff;
		}
		return i_12_;
	}

	public final void method40(int i, int i_14_) {
		if (i != 65536)
			method37(123, null);
		if (aDirectSoundBufferArray3520[i_14_] != null) {
			try {
				aDirectSoundBufferArray3520[i_14_].stop();
			} catch (com.ms.com.ComFailException comfailexception) {
				/* empty */
			}
			aDirectSoundBufferArray3520[i_14_] = null;
		}
	}

	public Class214() throws Exception {
		aBooleanArray3522 = new boolean[2];
		aDirectSoundBufferArray3520 = new DirectSoundBuffer[2];
		anIntArray3526 = new int[2];
		aDSCursorsArray3519 = new DSCursors[2];
		aByteArrayArray3518 = new byte[2][];
		aDirectSound3527 = new DirectSound();
		aWaveFormatEx3528 = new WaveFormatEx();
		for (int i = 0; i < 2; i++)
			aDSBufferDescArray3517[i] = new DSBufferDesc();
		for (int i = 0; i < 2; i++)
			aDSCursorsArray3519[i] = new DSCursors();
	}
}
