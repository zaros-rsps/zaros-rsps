package net.zaros.client;

/* Class40 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class40 {
	static final boolean method378(int i, int i_0_) {
		if (i_0_ != 13487)
			return true;
		if (PlayerEquipment.anInt3375 != i || Class41_Sub19.aClass306_3792 == null) {
			HardReferenceWrapper.method2795((byte) -124);
			Class41_Sub19.aClass306_3792 = Class306.aClass306_2739;
			PlayerEquipment.anInt3375 = i;
		}
		if (Class306.aClass306_2739 == Class41_Sub19.aClass306_3792) {
			byte[] is = Class134.fs35.get(i);
			if (is == null)
				return false;
			Packet class296_sub17 = new Packet(is);
			Class242.method2171(class296_sub17, i_0_ ^ 0x4c84);
			int i_1_ = class296_sub17.g1();
			for (int i_2_ = 0; i_1_ > i_2_; i_2_++)
				Class98.aClass155_1055.addLast((byte) -85, new Class296_Sub49(class296_sub17));
			int i_3_ = class296_sub17.readSmart();
			Class88.aClass220Array946 = new Class220[i_3_];
			for (int i_4_ = 0; i_4_ < i_3_; i_4_++)
				Class88.aClass220Array946[i_4_] = new Class220(class296_sub17);
			int i_5_ = class296_sub17.readSmart();
			Class379.aClass302Array3624 = new Class302[i_5_];
			for (int i_6_ = 0; i_6_ < i_5_; i_6_++)
				Class379.aClass302Array3624[i_6_] = new Class302(class296_sub17, i_6_);
			int i_7_ = class296_sub17.readSmart();
			Class41_Sub14.aClass45Array3777 = new Class45[i_7_];
			for (int i_8_ = 0; i_7_ > i_8_; i_8_++)
				Class41_Sub14.aClass45Array3777[i_8_] = new Class45(class296_sub17);
			int i_9_ = class296_sub17.readSmart();
			InputStream_Sub1.aClass218Array34 = new Class218[i_9_];
			for (int i_10_ = 0; i_10_ < i_9_; i_10_++)
				InputStream_Sub1.aClass218Array34[i_10_] = new Class218(class296_sub17);
			int i_11_ = class296_sub17.readSmart();
			Class296_Sub39_Sub10.aClass368Array6179 = new Class368[i_11_];
			for (int i_12_ = 0; i_11_ > i_12_; i_12_++)
				Class296_Sub39_Sub10.aClass368Array6179[i_12_] = Class296_Sub51_Sub30.method3167(0, class296_sub17);
			Class41_Sub19.aClass306_3792 = Class306.aClass306_2740;
		}
		if (Class306.aClass306_2740 == Class41_Sub19.aClass306_3792) {
			boolean bool = true;
			Class368[] class368s = Class296_Sub39_Sub10.aClass368Array6179;
			for (int i_13_ = 0; i_13_ < class368s.length; i_13_++) {
				Class368 class368 = class368s[i_13_];
				if (!class368.method3805(true))
					bool = false;
			}
			if (!bool)
				return false;
			Class41_Sub19.aClass306_3792 = Class306.aClass306_2741;
		}
		return true;
	}
}
