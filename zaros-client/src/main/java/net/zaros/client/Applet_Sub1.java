package net.zaros.client;
/* Applet_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

import jagex3.jagmisc.jagmisc;

import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.Method;
import java.net.URL;

public abstract class Applet_Sub1 extends Applet implements Runnable, FocusListener, WindowListener {
	
	static IncomingPacket aClass231_6 = new IncomingPacket(45, -1);
	
	static int anInt7;
	
	private boolean aBoolean8 = false;
	
	private boolean aBoolean9 = false;
	
	static SeekableFile aClass255_10;
	
	static int anInt11;
	
	static InterfaceComponent aClass51_12 = null;
	
	public static boolean aBoolean14;
	
	public static int anInt15;
	
	public static int anInt16;
	
	public static int anInt17;
	
	public static boolean aBoolean18;
	
	public static boolean aBoolean19;
	
	public static int anInt20;
	
	public static int anInt21;
	
	public static int anInt22;
	
	public static boolean aBoolean23;
	
	public static boolean aBoolean24;
	
	public static int anInt25;
	
	public static boolean aBoolean26;
	
	public static int anInt27;
	
	public static boolean aBoolean28;
	
	public static boolean aBoolean29;
	
	public static boolean aBoolean30;
	
	public static int anInt31;
	
	abstract void method83(int i);
	
	public final void focusGained(FocusEvent focusevent) {
		Class296_Sub14.aBoolean4663 = true;
		Class78.aBoolean3430 = true;
	}
	
	public final void focusLost(FocusEvent focusevent) {
		Class296_Sub14.aBoolean4663 = false;
	}
	
	public final void windowClosing(WindowEvent windowevent) {
		destroy();
	}
	
	final void method84(int i, boolean bool, int i_0_, int i_1_, boolean bool_2_, int i_3_, String string, int i_4_) {
		try {
			StaticMethods.anInt1838 = Class241.anInt2301 = i;
			Class241_Sub2_Sub2.anInt5908 = 0;
			Class55.anApplet_Sub1_656 = this;
			Class152.anInt1568 = Class384.anInt3254 = i_3_;
			Class122_Sub1.anApplet5656 = null;
			CS2Stack.anInt2251 = 0;
			Class338_Sub3_Sub2.anInt6565 = i_1_;
			Class340.aFrame3707 = new Frame();
			Class340.aFrame3707.setTitle("Jagex");
			Class340.aFrame3707.setResizable(bool);
			Class340.aFrame3707.addWindowListener(this);
			Class340.aFrame3707.setVisible(true);
			Class340.aFrame3707.toFront();
			Insets insets = Class340.aFrame3707.getInsets();
			Class340.aFrame3707.setSize((insets.right + StaticMethods.anInt1838 + insets.left), (insets.bottom + Class152.anInt1568 + insets.top));
			Class281.aClass398_2600 = Class252.aClass398_2383 = new Class398(i_0_, string, i_4_, true);
			Class278 class278 = Class252.aClass398_2383.method4123(this, 1, -14396);
			while (class278.anInt2540 == 0) {
				Class106_Sub1.method942(10L, 0);
			}
		} catch (Exception exception) {
			Class219_Sub1.method2062(null, (byte) 109, exception);
		}
	}
	
	private final void method85(byte i) {
		long l = Class72.method771(i ^ 0x45);
		long l_5_ = ModeWhat.aLongArray1188[Class296_Sub51_Sub27_Sub1.anInt6734];
		ModeWhat.aLongArray1188[Class296_Sub51_Sub27_Sub1.anInt6734] = l;
		Class296_Sub51_Sub27_Sub1.anInt6734 = Class296_Sub51_Sub27_Sub1.anInt6734 + 1 & 0x1f;
		if (l_5_ != 0L && l_5_ < l) {
			/* empty */
		}
		synchronized (this) {
			Class41.aBoolean390 = Class296_Sub14.aBoolean4663;
		}
		method90((byte) 62);
		if (i != -63) {
			provideLoaderApplet(null);
		}
	}
	
	public final void windowActivated(WindowEvent windowevent) {
		/* empty */
	}
	
	public static void method86(boolean bool) {
		aClass231_6 = null;
		if (bool != true) {
			anInt11 = 95;
		}
		aClass255_10 = null;
		aClass51_12 = null;
	}
	
	public final void update(Graphics graphics) {
		paint(graphics);
	}
	
	public static final void provideLoaderApplet(Applet applet) {
		CS2Script.anApplet6140 = applet;
	}
	
	final boolean method87(int i) {
		if (i >= -21) {
			return true;
		}
		return Class366_Sub4.method3779("jagmisc", (byte) -17);
	}
	
	public final void stop() {
		if (Class55.anApplet_Sub1_656 == this && !Js5DiskStore.aBoolean1699) {
			Class393.aLong3301 = Class72.method771(-111) - -4000L;
		}
	}
	
	private final void method88(boolean bool) {
		if (bool) {
			aBoolean8 = true;
		}
		long l = Class72.method771(-113);
		long l_6_ = Js5TextureLoader.aLongArray3446[Class338_Sub2.anInt5198];
		Js5TextureLoader.aLongArray3446[Class338_Sub2.anInt5198] = l;
		if (l_6_ != 0L && l > l_6_) {
			int i = (int) (l + -l_6_);
			Class360_Sub8.anInt5338 = ((i >> 1) + 32000) / i;
		}
		Class338_Sub2.anInt5198 = Class338_Sub2.anInt5198 + 1 & 0x1f;
		if (Class41_Sub17.anInt3784++ > 50) {
			Class78.aBoolean3430 = true;
			Class41_Sub17.anInt3784 -= 50;
			Class230.aCanvas2209.setSize(Class241.anInt2301, Class384.anInt3254);
			Class230.aCanvas2209.setVisible(true);
			if (Class340.aFrame3707 == null || Animator.aFrame435 != null) {
				Class230.aCanvas2209.setLocation(Class241_Sub2_Sub2.anInt5908, CS2Stack.anInt2251);
			} else {
				Insets insets = Class340.aFrame3707.getInsets();
				Class230.aCanvas2209.setLocation(insets.left + Class241_Sub2_Sub2.anInt5908, CS2Stack.anInt2251 + insets.top);
			}
		}
		method83(24063);
	}
	
	final void method89(String string, int i, boolean bool, int i_7_, int i_8_, int i_9_, int i_10_) {
		try {
			if (bool) {
				destroy();
			}
			if (Class55.anApplet_Sub1_656 != null) {
				Class105.anInt3664++;
				if (Class105.anInt3664 >= 3) {
					method94((byte) -45, "alreadyloaded");
				} else {
					getAppletContext().showDocument(getDocumentBase(), "_self");
				}
			} else {
				Class122_Sub1.anApplet5656 = CS2Script.anApplet6140;
				Class55.anApplet_Sub1_656 = this;
				StaticMethods.anInt1838 = Class241.anInt2301 = i_7_;
				CS2Stack.anInt2251 = 0;
				Class241_Sub2_Sub2.anInt5908 = 0;
				Class152.anInt1568 = Class384.anInt3254 = i_10_;
				Class338_Sub3_Sub2.anInt6565 = i_8_;
				Class281.aClass398_2600 = Class252.aClass398_2383 = new Class398(i_9_, string, i, CS2Script.anApplet6140 != null);
				Class278 class278 = Class252.aClass398_2383.method4123(this, 1, -14396);
				while (class278.anInt2540 == 0) {
					Class106_Sub1.method942(10L, 0);
				}
			}
		} catch (Throwable throwable) {
			Class219_Sub1.method2062(null, (byte) -19, throwable);
			method94((byte) -45, "crash");
		}
	}
	
	abstract void method90(byte i);
	
	abstract void method91(byte i);
	
	static final void method92(int i, int i_11_, int i_12_, int i_13_, int i_14_, Mobile class338_sub3_sub1_sub3, int i_15_, int i_16_, int i_17_, Mobile class338_sub3_sub1_sub3_18_) {
		int i_19_ = class338_sub3_sub1_sub3_18_.method3505(-107);
		if (i_19_ != -1) {
			Object object = null;
			Sprite class397 = ((Sprite) Class264.aClass113_2471.get((long) i_19_));
			if (class397 == null) {
				Class186[] class186s = Class186.method1871(Class205_Sub2.fs8, i_19_, 0);
				if (class186s == null) {
					return;
				}
				class397 = Class41_Sub13.aHa3774.a(class186s[0], true);
				Class264.aClass113_2471.put(class397, (long) i_19_);
			}
			Class300.method3247(class338_sub3_sub1_sub3.tileY, class338_sub3_sub1_sub3.getSize() * 256, class338_sub3_sub1_sub3.z, 0, 512, class338_sub3_sub1_sub3.tileX, false);
			int i_20_ = StaticMethods.anIntArray4629[0] + i_17_ - 18;
			i_20_ += i_15_ / 4 * 18;
			int i_21_ = i - 54 - (-StaticMethods.anIntArray4629[1] + 16);
			i_21_ += i_15_ % 4 * 18;
			if (i_13_ >= -118) {
				method86(false);
			}
			class397.method4096(i_20_, i_21_);
			if (class338_sub3_sub1_sub3 == class338_sub3_sub1_sub3_18_) {
				Class41_Sub13.aHa3774.method1086(i_21_ - 1, i_20_ - 1, -256, (byte) 6, 18, 18);
			}
			Class368_Sub8.method3838(false, i_20_ - 1, i_21_ - 1, i_21_ + 18, i_20_ + 18);
			Class338_Sub7 class338_sub7 = Class382.method4008((byte) 106);
			class338_sub7.aClass338_Sub3_Sub1_Sub3_5251 = class338_sub3_sub1_sub3_18_;
			class338_sub7.anInt5248 = i_21_;
			class338_sub7.anInt5247 = i_20_;
			class338_sub7.anInt5252 = i_21_ + 16;
			class338_sub7.anInt5249 = i_20_ + 16;
			Class44_Sub1_Sub1.aClass404_5809.method4158(class338_sub7, 1);
		}
	}
	
	public final URL getCodeBase() {
		if (Class340.aFrame3707 != null) {
			return null;
		}
		if (CS2Script.anApplet6140 != null && CS2Script.anApplet6140 != this) {
			return CS2Script.anApplet6140.getCodeBase();
		}
		return super.getCodeBase();
	}
	
	final boolean method93(boolean bool) {
		if (bool) {
			getAppletContext();
		}
		return Class366_Sub4.method3779("jaclib", (byte) -17);
	}
	
	public final void run() {
		do {
			try {
				if (Class398.aString3343 != null) {
					String string = Class398.aString3343.toLowerCase();
					if (string.indexOf("sun") != -1 || string.indexOf("apple") != -1) {
						String string_22_ = Class398.aString3333;
						if (string_22_.equals("1.1") || string_22_.startsWith("1.1.") || string_22_.equals("1.2") || string_22_.startsWith("1.2.")) {
							method94((byte) -45, "wrongjava");
							break;
						}
					} else if (string.indexOf("ibm") != -1 && (Class398.aString3333 == null || Class398.aString3333.equals("1.4.2"))) {
						method94((byte) -45, "wrongjava");
						break;
					}
				}
				if (Class398.aString3333 != null && Class398.aString3333.startsWith("1.")) {
					int i = 2;
					int i_23_ = 0;
					while (Class398.aString3333.length() > i) {
						int i_24_ = Class398.aString3333.charAt(i);
						if (i_24_ < 48 || i_24_ > 57) {
							break;
						}
						i++;
						i_23_ = i_23_ * 10 + (i_24_ - 48);
					}
					if (i_23_ >= 5) {
						za.aBoolean5087 = true;
					}
				}
				Applet applet = Class55.anApplet_Sub1_656;
				if (CS2Script.anApplet6140 != null) {
					applet = CS2Script.anApplet6140;
				}
				Method method = Class398.aMethod3337;
				if (method != null) {
					try {
						method.invoke(applet, new Object[] { Boolean.TRUE });
					} catch (Throwable throwable) {
						throwable.printStackTrace();
						/* empty */
					}
				}
				Class77.method790((byte) -122);
				Class296_Sub51_Sub3.method3083((byte) -100);
				method100(101);
				method91((byte) -66);
				Class368_Sub20.aClass217_5550 = Class296_Sub29.method2693(0);
				while (Class393.aLong3301 == 0L || Class72.method771(-106) < Class393.aLong3301) {
					Class36.anInt353 = (Class368_Sub20.aClass217_5550.method2035(Class296_Sub51_Sub32.aLong6513, (byte) -124));
					for (int i = 0; Class36.anInt353 > i; i++) {
						method85((byte) -63);
					}
					method88(false);
					Class362.method3758(Class252.aClass398_2383, true, Class230.aCanvas2209);
				}
			} catch (Throwable throwable) {
				Class219_Sub1.method2062(method102((byte) -47), (byte) -106, throwable);
				method94((byte) -45, "crash");
			} finally {
				method97(110, true);
			}
		} while (false);
	}
	
	public final void windowIconified(WindowEvent windowevent) {
		/* empty */
	}
	
	public final void windowDeactivated(WindowEvent windowevent) {
		/* empty */
	}
	
	public final String getParameter(String string) {
		if (Class340.aFrame3707 != null) {
			return null;
		}
		if (CS2Script.anApplet6140 != null && CS2Script.anApplet6140 != this) {
			return CS2Script.anApplet6140.getParameter(string);
		}
		return super.getParameter(string);
	}
	
	public final void windowDeiconified(WindowEvent windowevent) {
		/* empty */
	}
	
	public final void destroy() {
		if (this == Class55.anApplet_Sub1_656 && !Js5DiskStore.aBoolean1699) {
			Class393.aLong3301 = Class72.method771(-127);
			Class106_Sub1.method942(5000L, 0);
			Class281.aClass398_2600 = null;
			method97(76, false);
		}
	}
	
	public final void windowOpened(WindowEvent windowevent) {
		/* empty */
	}
	
	public final AppletContext getAppletContext() {
		if (Class340.aFrame3707 != null) {
			return null;
		}
		if (CS2Script.anApplet6140 != null && this != CS2Script.anApplet6140) {
			return CS2Script.anApplet6140.getAppletContext();
		}
		return super.getAppletContext();
	}
	
	final void method94(byte i, String string) {
		if (!aBoolean8) {
			aBoolean8 = true;
			System.out.println("error_game_" + string);
			try {
				Class297.method3231("loggedout", CS2Script.anApplet6140, false);
			} catch (Throwable throwable) {
				/* empty */
			}
			try {
				if (i == -45) {
					getAppletContext().showDocument(new URL(getCodeBase(), ("error_game_" + string + ".ws")), "_top");
				}
			} catch (Exception exception) {
				/* empty */
			}
		}
	}
	
	abstract void method95(boolean bool);
	
	abstract void method96(int i);
	
	private final void method97(int i, boolean bool) {
		synchronized (this) {
			if (i < 63) {
				aBoolean8 = false;
			}
			if (Js5DiskStore.aBoolean1699) {
				return;
			}
			Js5DiskStore.aBoolean1699 = true;
		}
		System.out.println("Shutdown start - clean:" + bool);
		if (CS2Script.anApplet6140 != null) {
			CS2Script.anApplet6140.destroy();
		}
		try {
			method96(109);
		} catch (Exception exception) {
			/* empty */
		}
		if (aBoolean9) {
			try {
				jagmisc.quit();
			} catch (Throwable throwable) {
				/* empty */
			}
			aBoolean9 = false;
		}
		Class376.method3958(true, true);
		Class151.method1540((byte) -126);
		if (Class230.aCanvas2209 != null) {
			try {
				Class230.aCanvas2209.removeFocusListener(this);
				Class230.aCanvas2209.getParent().remove(Class230.aCanvas2209);
			} catch (Exception exception) {
				/* empty */
			}
		}
		if (Class252.aClass398_2383 != null) {
			try {
				Class252.aClass398_2383.method4112((byte) -112);
			} catch (Exception exception) {
				/* empty */
			}
		}
		method95(true);
		if (Class340.aFrame3707 != null) {
			Class340.aFrame3707.setVisible(false);
			Class340.aFrame3707.dispose();
			Class340.aFrame3707 = null;
		}
		System.out.println("Shutdown complete - clean:" + bool);
		System.exit(1);
	}
	
	public final synchronized void paint(Graphics graphics) {
		if (Class55.anApplet_Sub1_656 == this && !Js5DiskStore.aBoolean1699) {
			Class78.aBoolean3430 = true;
			if (za.aBoolean5087 && Class72.method771(-111) + -EmissiveTriangle.aLong962 > 1000L) {
				Rectangle rectangle = graphics.getClipBounds();
				if (rectangle == null || (StaticMethods.anInt1838 <= rectangle.width && rectangle.height >= Class152.anInt1568)) {
					Class296_Sub51_Sub19.aBoolean6438 = true;
				}
			}
		}
	}
	
	public final void start() {
		if (this == Class55.anApplet_Sub1_656 && !Js5DiskStore.aBoolean1699) {
			Class393.aLong3301 = 0L;
		}
	}
	
	static final void method98(int i, int i_25_) {
		Class187.anInt1910 = i;
		Class377.loginConnection = Class296_Sub45_Sub2.aClass204_6277;
		Class220.anInt2150 = i_25_;
		Class41_Sub27.method508(Class384.aString3252.equals(""), Class384.aString3252, "", true, (byte) -121);
	}
	
	final boolean method99(byte i) {
	/*	String string = getDocumentBase().getHost().toLowerCase();
		if (string.equals("jagex.com") || string.endsWith(".jagex.com")) {
			return true;
		}
		if (string.equals("runescape.com") || string.endsWith(".runescape.com")) {
			return true;
		}
		int i_26_ = 39 % ((i + 36) / 58);
		if (string.equals("stellardawn.com") || string.endsWith(".stellardawn.com")) {
			return true;
		}
		if (string.endsWith("127.0.0.1")) {
			return true;
		}
		for (*//**//*; string.length() > 0 && string.charAt(string.length() - 1) >= '0'; string = string.substring(0, string.length() - 1)) {
			if (string.charAt(string.length() - 1) > '9') {
				break;
			}
		}
		if (string.endsWith("192.168.1.")) {
			return true;
		}
		method94((byte) -45, "invalidhost");*/
		return true;
//		return false;
	}
	
	synchronized void method100(int i) {
		if (Class230.aCanvas2209 != null) {
			Class230.aCanvas2209.removeFocusListener(this);
			Class230.aCanvas2209.getParent().setBackground(Color.black);
			Class230.aCanvas2209.getParent().remove(Class230.aCanvas2209);
		}
		Container container;
		if (Animator.aFrame435 == null) {
			if (Class340.aFrame3707 != null) {
				container = Class340.aFrame3707;
			} else if (CS2Script.anApplet6140 == null) {
				container = Class55.anApplet_Sub1_656;
			} else {
				container = CS2Script.anApplet6140;
			}
		} else {
			container = Animator.aFrame435;
		}
		container.setLayout(null);
		Class230.aCanvas2209 = new Canvas_Sub1(this);
		if (i >= 61) {
			container.add(Class230.aCanvas2209);
			Class230.aCanvas2209.setSize(Class241.anInt2301, Class384.anInt3254);
			Class230.aCanvas2209.setVisible(true);
			if (Class340.aFrame3707 == container) {
				Insets insets = Class340.aFrame3707.getInsets();
				Class230.aCanvas2209.setLocation(insets.left + Class241_Sub2_Sub2.anInt5908, CS2Stack.anInt2251 + insets.top);
			} else {
				Class230.aCanvas2209.setLocation(Class241_Sub2_Sub2.anInt5908, CS2Stack.anInt2251);
			}
			Class230.aCanvas2209.addFocusListener(this);
			Class230.aCanvas2209.requestFocus();
			Class41.aBoolean390 = true;
			Class296_Sub14.aBoolean4663 = true;
			Class78.aBoolean3430 = true;
			Class296_Sub51_Sub19.aBoolean6438 = false;
			EmissiveTriangle.aLong962 = Class72.method771(-110);
		}
	}
	
	public final void windowClosed(WindowEvent windowevent) {
		/* empty */
	}
	
	public abstract void init();
	
	final boolean method101(int i) {
		if (i != -1) {
			anInt7 = 43;
		}
		return Class366_Sub4.method3779("jagtheora", (byte) -17);
	}
	
	String method102(byte i) {
		if (i > -2) {
			return null;
		}
		return null;
	}
	
	public final URL getDocumentBase() {
		if (Class340.aFrame3707 != null) {
			return null;
		}
		if (CS2Script.anApplet6140 != null && this != CS2Script.anApplet6140) {
			return CS2Script.anApplet6140.getDocumentBase();
		}
		return super.getDocumentBase();
	}
}
