package net.zaros.client;

/* Class261 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class261 {
	static SeekableFile aClass255_2423;
	static int anInt2424;
	static final void method2247(int i) {
		synchronized (Class368.aClass404Array3129) {
			for (int i_0_ = 0; Class368.aClass404Array3129.length > i_0_; i_0_++) {
				Class368.aClass404Array3129[i_0_] = new Class404();
				Class69_Sub1_Sub1.anIntArray6686[i_0_] = 0;
			}
		}
		if (i != -10899)
			method2249(-11);
	}

	static final Class method2248(int i, String string) throws ClassNotFoundException {
		if (string.equals("B"))
			return Byte.TYPE;
		if (i != 0)
			return null;
		if (string.equals("I"))
			return Integer.TYPE;
		if (string.equals("S"))
			return Short.TYPE;
		if (string.equals("J"))
			return Long.TYPE;
		if (string.equals("Z"))
			return Boolean.TYPE;
		if (string.equals("F"))
			return Float.TYPE;
		if (string.equals("D"))
			return Double.TYPE;
		if (string.equals("C"))
			return Character.TYPE;
		return Class.forName(string);
	}

	public static void method2249(int i) {
		int i_1_ = -26 / ((i - 43) / 51);
		aClass255_2423 = null;
	}
}
