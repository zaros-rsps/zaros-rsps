package net.zaros.client;

import java.awt.Canvas;
import java.awt.Container;
import java.awt.Insets;
import java.awt.Point;

import jaggl.OpenGL;

/**
 * @author Walied K. Yassen
 */
public class StaticMethods {

	static boolean[] freeIgnoreSlots = new boolean[100];

	static final void method2777(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, byte[] is) {
		if (i_2_ > 0 && !Class30.method329(2844, i_2_)) {
			throw new IllegalArgumentException("");
		}
		if (i > 0 && !Class30.method329(2844, i)) {
			throw new IllegalArgumentException("");
		}
		if (i_1_ == 1) {
			int i_5_ = Mobile.method3502(i_3_, -32842);
			int i_6_ = 0;
			int i_7_ = i <= i_2_ ? i : i_2_;
			int i_8_ = i_2_ >> 1;
			int i_9_ = i >> 1;
			byte[] is_10_ = is;
			byte[] is_11_ = new byte[i_5_ * i_8_ * i_9_];
			for (;;) {
				OpenGL.glTexImage2Dub(i_0_, i_6_, i_4_, i_2_, i, 0, i_3_, 5121, is_10_, 0);
				if (i_7_ <= 1) {
					break;
				}
				int i_12_ = i_2_ * i_5_;
				for (int i_13_ = 0; i_5_ > i_13_; i_13_++) {
					int i_14_ = i_13_;
					int i_15_ = i_13_;
					int i_16_ = i_15_ + i_12_;
					for (int i_17_ = 0; i_17_ < i_9_; i_17_++) {
						for (int i_18_ = 0; i_18_ < i_8_; i_18_++) {
							int i_19_ = is_10_[i_15_];
							i_15_ += i_5_;
							i_19_ += is_10_[i_15_];
							i_15_ += i_5_;
							i_19_ += is_10_[i_16_];
							i_16_ += i_5_;
							i_19_ += is_10_[i_16_];
							is_11_[i_14_] = (byte) (i_19_ >> 2);
							i_16_ += i_5_;
							i_14_ += i_5_;
						}
						i_15_ += i_12_;
						i_16_ += i_12_;
					}
				}
				byte[] is_20_ = is_11_;
				is_11_ = is_10_;
				i = i_9_;
				i_2_ = i_8_;
				is_10_ = is_20_;
				i_9_ >>= 1;
				i_6_++;
				i_7_ >>= 1;
				i_8_ >>= 1;
			}
		}
	}

	public static void method2778(byte i) {
		int i_21_ = 56 % ((77 - i) / 42);
		IncomingPacket.aClass231_4901 = null;
		freeIgnoreSlots = null;
		ConfigurationsLoader.configsLoader = null;
	}

	public static void method2822(byte i) {
		Class18.aClass81_6158 = null;
	}

	static final void method1008(int i, boolean bool, int i_1_, int i_2_, int i_3_, String string, int i_4_, long l, int i_5_, boolean bool_6_, long l_7_, String string_8_, boolean bool_9_) {
		if (!Class318.aBoolean2814 && Class230.anInt2210 < 500) {
			i_2_ = i_2_ != -1 ? i_2_ : Class41_Sub19.anInt3793;
			Class296_Sub39_Sub9 class296_sub39_sub9 = new Class296_Sub39_Sub9(string, string_8_, i_2_, i_5_, i_3_, l, i_1_, i_4_, bool_6_, bool_9_, l_7_, bool);
			Class296_Sub11.method2498((byte) 109, class296_sub39_sub9);
			int i_10_ = 75 % ((i + 50) / 32);
		}
	}

	static final void method1009(int i, int i_11_, byte i_12_) {
		Class200.method1952(i_12_ ^ ~0x77, i_11_, i);
		if (i_12_ != 6)
			StaticMethods.method1006((byte) -127);
	}

	static final void method1010(int i, int i_13_, int i_14_, boolean bool, int i_15_) {
		if (za_Sub1.anInt6554 == 4) {
			Class399.someXTEARelatedFileID = -1;
			za_Sub1.anInt6554 = 0;
		}
		if (bool || Class41_Sub6.anInt3760 != i_15_ || i_14_ != Class97.anInt1050 || (FileWorker.anInt3005 != NPCDefinitionLoader.anInt1401 && Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026.method496(121) != 1)) {
			Class41_Sub6.anInt3760 = i_15_;
			Class97.anInt1050 = i_14_;
			NPCDefinitionLoader.anInt1401 = FileWorker.anInt3005;
			if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026.method496(122) == 1)
				NPCDefinitionLoader.anInt1401 = 0;
			Class41_Sub8.method422(i ^ 0x1, i_13_);
			ModeWhat.method1021(Class123_Sub1_Sub1.aClass92_5814, 8493, TranslatableString.aClass120_1208.getTranslation(Class394.langID), Class41_Sub13.aHa3774, true, Class205_Sub1.aClass55_5642);
			int i_16_ = Class206.worldBaseX;
			int i_17_ = Class41_Sub26.worldBaseY;
			Class206.worldBaseX = (-(Class198.currentMapSizeX >> 4) + Class41_Sub6.anInt3760) * 8;
			Class41_Sub26.worldBaseY = (-(Class296_Sub38.currentMapSizeY >> 4) + Class97.anInt1050) * 8;
			Class373.aClass296_Sub39_Sub14_3177 = Class106.method936(Class41_Sub6.anInt3760 * 8, Class97.anInt1050 * 8);
			Class296_Sub51_Sub19.aClass259_6439 = null;
			int i_18_ = -i_16_ + Class206.worldBaseX;
			int i_19_ = -i_17_ + Class41_Sub26.worldBaseY;
			if (i_13_ != 12) {
				boolean bool_20_ = false;
				Class367.npcsCount = 0;
				int i_21_ = Class198.currentMapSizeX * 512 - 512;
				int i_22_ = Class296_Sub38.currentMapSizeY * 512 - 512;
				for (int i_23_ = 0; i_23_ < Class368_Sub23.npcNodeCount; i_23_++) {
					NPCNode class296_sub7 = Class241.npcNodes[i_23_];
					if (class296_sub7 != null) {
						NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
						class338_sub3_sub1_sub3_sub2.tileX -= i_18_ * 512;
						class338_sub3_sub1_sub3_sub2.tileY -= i_19_ * 512;
						if (class338_sub3_sub1_sub3_sub2.tileX < 0 || i_21_ < class338_sub3_sub1_sub3_sub2.tileX || class338_sub3_sub1_sub3_sub2.tileY < 0 || (i_22_ < class338_sub3_sub1_sub3_sub2.tileY)) {
							class338_sub3_sub1_sub3_sub2.setDefinition(null);
							bool_20_ = true;
							class296_sub7.unlink();
						} else {
							boolean bool_24_ = true;
							for (int i_25_ = 0; i_25_ < (class338_sub3_sub1_sub3_sub2.waypointQueueX).length; i_25_++) {
								class338_sub3_sub1_sub3_sub2.waypointQueueX[i_25_] -= i_18_;
								class338_sub3_sub1_sub3_sub2.wayPointQueueY[i_25_] -= i_19_;
								if ((class338_sub3_sub1_sub3_sub2.waypointQueueX[i_25_]) < 0 || ((class338_sub3_sub1_sub3_sub2.waypointQueueX[i_25_]) >= Class198.currentMapSizeX) || (class338_sub3_sub1_sub3_sub2.wayPointQueueY[i_25_]) < 0 || ((class338_sub3_sub1_sub3_sub2.wayPointQueueY[i_25_]) >= Class296_Sub38.currentMapSizeY))
									bool_24_ = false;
							}
							if (!bool_24_) {
								class338_sub3_sub1_sub3_sub2.setDefinition(null);
								class296_sub7.unlink();
								bool_20_ = true;
							} else
								ReferenceTable.npcIndexes[Class367.npcsCount++] = class338_sub3_sub1_sub3_sub2.index;
						}
					}
				}
				if (bool_20_) {
					Class368_Sub23.npcNodeCount = Class41_Sub18.localNpcs.size((byte) -4);
					Class41_Sub18.localNpcs.toArray(5125, Class241.npcNodes);
				}
			} else {
				for (int i_26_ = 0; i_26_ < Class368_Sub23.npcNodeCount; i_26_++) {
					NPCNode class296_sub7 = Class241.npcNodes[i_26_];
					if (class296_sub7 != null) {
						NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
						for (int i_27_ = 0; i_27_ < (class338_sub3_sub1_sub3_sub2.waypointQueueX).length; i_27_++) {
							class338_sub3_sub1_sub3_sub2.waypointQueueX[i_27_] -= i_18_;
							class338_sub3_sub1_sub3_sub2.wayPointQueueY[i_27_] -= i_19_;
						}
						class338_sub3_sub1_sub3_sub2.tileX -= i_18_ * 512;
						class338_sub3_sub1_sub3_sub2.tileY -= i_19_ * 512;
					}
				}
			}
			for (int i_28_ = i; i_28_ < 2048; i_28_++) {
				Player class338_sub3_sub1_sub3_sub1 = (PlayerUpdate.visiblePlayers[i_28_]);
				if (class338_sub3_sub1_sub3_sub1 != null) {
					for (int i_29_ = 0; i_29_ < (class338_sub3_sub1_sub3_sub1.waypointQueueX).length; i_29_++) {
						class338_sub3_sub1_sub3_sub1.waypointQueueX[i_29_] -= i_18_;
						class338_sub3_sub1_sub3_sub1.wayPointQueueY[i_29_] -= i_19_;
					}
					class338_sub3_sub1_sub3_sub1.tileX -= i_18_ * 512;
					class338_sub3_sub1_sub3_sub1.tileY -= i_19_ * 512;
				}
			}
			Class225[] class225s = Class338_Sub2.aClass225Array5200;
			for (int i_30_ = 0; i_30_ < class225s.length; i_30_++) {
				Class225 class225 = class225s[i_30_];
				if (class225 != null) {
					class225.anInt2176 -= i_19_ * 512;
					class225.anInt2183 -= i_18_ * 512;
				}
			}
			for (Class296_Sub42 class296_sub42 = (Class296_Sub42) aa.aClass155_50.removeFirst((byte) 121); class296_sub42 != null; class296_sub42 = (Class296_Sub42) aa.aClass155_50.removeNext(1001)) {
				class296_sub42.anInt4928 -= i_19_;
				class296_sub42.anInt4923 -= i_18_;
				if (Class338_Sub2.mapLoadType != 4 && (class296_sub42.anInt4923 < 0 || class296_sub42.anInt4928 < 0 || Class198.currentMapSizeX <= class296_sub42.anInt4923 || (Class296_Sub38.currentMapSizeY <= class296_sub42.anInt4928)))
					class296_sub42.unlink();
			}
			for (Class296_Sub42 class296_sub42 = ((Class296_Sub42) ParticleEmitterRaw.aClass155_1770.removeFirst((byte) 117)); class296_sub42 != null; class296_sub42 = ((Class296_Sub42) ParticleEmitterRaw.aClass155_1770.removeNext(1001))) {
				class296_sub42.anInt4928 -= i_19_;
				class296_sub42.anInt4923 -= i_18_;
				if (Class338_Sub2.mapLoadType != 4 && (class296_sub42.anInt4923 < 0 || class296_sub42.anInt4928 < 0 || Class198.currentMapSizeX <= class296_sub42.anInt4923 || (Class296_Sub38.currentMapSizeY <= class296_sub42.anInt4928)))
					class296_sub42.unlink();
			}
			if (Class338_Sub2.mapLoadType != 4) {
				for (Class296_Sub56 class296_sub56 = ((Class296_Sub56) Class296_Sub11.aClass263_4647.getFirst(true)); class296_sub56 != null; class296_sub56 = ((Class296_Sub56) Class296_Sub11.aClass263_4647.getNext(0))) {
					int i_31_ = (int) (class296_sub56.uid & 0x3fffL);
					int i_32_ = i_31_ - Class206.worldBaseX;
					int i_33_ = (int) (class296_sub56.uid >> 14 & 0x3fffL);
					int i_34_ = -Class41_Sub26.worldBaseY + i_33_;
					if (i_32_ < 0 || i_34_ < 0 || Class198.currentMapSizeX <= i_32_ || Class296_Sub38.currentMapSizeY <= i_34_)
						class296_sub56.unlink();
				}
			}
			if (Class210_Sub1.foundX != 0) {
				Class210_Sub1.foundX -= i_18_;
				Class205.foundY -= i_19_;
			}
			Class302.method3261(3);
			if (i_13_ != 12) {
				Class5.cameraDestZ -= i_19_;
				Class44_Sub1.cameraDestX -= i_18_;
				Class124.camPosZ -= i_19_ * 512;
				Graphic.lookatY -= i_19_;
				Class53.lookatX -= i_18_;
				Class219.camPosX -= i_18_ * 512;
				if (Math.abs(i_18_) > Class198.currentMapSizeX || Math.abs(i_19_) > Class296_Sub38.currentMapSizeY)
					Class368_Sub22.method3866(11);
			} else if (Class361.anInt3103 != 4) {
				Class368_Sub11.anInt5486 = Class242.anInt2309 = -1;
				Class361.anInt3103 = 1;
			} else {
				Class127_Sub1.anInt4287 -= i_19_ * 512;
				Class296_Sub24.anInt4762 -= i_19_ * 512;
				Class338_Sub3_Sub1.anInt6563 -= i_18_ * 512;
				Class296_Sub14.anInt4668 -= i_18_ * 512;
			}
			Class88.method829((byte) -82);
			Class296_Sub45_Sub4.method3018(-112);
			Class134.aClass263_1385.clear();
			Class72.aClass155_844.method1581(327680);
			Class296_Sub51_Sub39.aClass404_6546.method4161((byte) 119);
			SeekableFile.method2225(23123);
		}
	}

	static final Class224[] method1011(int i) {
		if (i != 10672)
			method1009(50, -3, (byte) -103);
		if (Class177.aClass224Array1847 == null) {
			Class224[] class224s = StaticMethods.method3207(Class252.aClass398_2383, (byte) 72);
			Class224[] class224s_35_ = new Class224[class224s.length];
			int i_36_ = 0;
			int i_37_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub10_5009.method435(120);
			while_83_ : for (int i_38_ = 0; class224s.length > i_38_; i_38_++) {
				Class224 class224 = class224s[i_38_];
				if ((class224.anInt2168 <= 0 || class224.anInt2168 >= 24) && class224.anInt2167 >= 800 && class224.anInt2171 >= 600 && (i_37_ != 2 || (class224.anInt2167 <= 800 && class224.anInt2171 <= 600)) && (i_37_ != 1 || (class224.anInt2167 <= 1024 && class224.anInt2171 <= 768))) {
					for (int i_39_ = 0; i_36_ > i_39_; i_39_++) {
						Class224 class224_40_ = class224s_35_[i_39_];
						if (class224_40_.anInt2167 == class224.anInt2167 && class224_40_.anInt2171 == class224.anInt2171) {
							if (class224.anInt2168 > class224_40_.anInt2168)
								class224s_35_[i_39_] = class224;
							continue while_83_;
						}
					}
					class224s_35_[i_36_] = class224;
					i_36_++;
				}
			}
			Class177.aClass224Array1847 = new Class224[i_36_];
			ArrayTools.removeElement(class224s_35_, 0, Class177.aClass224Array1847, 0, i_36_);
			int[] is = new int[Class177.aClass224Array1847.length];
			for (int i_41_ = 0; i_41_ < Class177.aClass224Array1847.length; i_41_++) {
				Class224 class224 = Class177.aClass224Array1847[i_41_];
				is[i_41_] = class224.anInt2167 * class224.anInt2171;
			}
			Class296_Sub34_Sub2.method2740(Class177.aClass224Array1847, 4, is);
		}
		return Class177.aClass224Array1847;
	}

	static final void method1012(Js5 class138, byte i) {
		ISAACCipher.anInt1895 = 0;
		Class296_Sub51_Sub23.anInt6461 = 0;
		Class368_Sub19.aClass404_5542 = new Class404();
		Class296_Sub51_Sub36.aClass338_Sub8_Sub2_Sub1Array6528 = new Class338_Sub8_Sub2_Sub1[1024];
		Class380.aClass338_Sub1Array3201 = (new Class338_Sub1[Class316.anIntArray2801[AdvancedMemoryCache.anInt1167] + 1]);
		Model.anInt1848 = 0;
		Class379_Sub2_Sub1.anInt6606 = 0;
		NPCNode.method2447(class138, (byte) 70);
		Class259.method2241(class138, true);
		if (i >= -4)
			StaticMethods.aBoolean1183 = false;
	}

	public static boolean aBoolean1183 = false;

	static final void method1006(byte i) {
		Class296_Sub51_Sub24.aClass397Array6463 = null;
		Class85.aClass397Array929 = null;
		Class356.aClass397Array3081 = null;
		Class49.aClass55_461 = null;
		Class151.aClass397Array1555 = null;
		Class296_Sub2.aClass397_4589 = null;
		Class379_Sub1.aClass397Array5671 = null;
		StaticMethods.aClass397Array6073 = null;
		Class296_Sub39_Sub17.aClass397_6237 = null;
		Class205_Sub1.aClass55_5642 = null;
		Class205.aClass397Array3505 = null;
		if (i >= -26) {
			method1010(-113, 23, -89, true, -55);
		}
		Class296_Sub9_Sub1.aClass397Array5986 = null;
		Class100.aClass397Array1069 = null;
		Class69_Sub4.aClass397Array5732 = null;
		Class368_Sub10.aClass397Array5482 = null;
		GraphicsLoader.aClass397Array2478 = null;
		Class41_Sub2.aClass55_3742 = null;
	}

	public static void method1598(int i) {
		synchronized (Class246.aClient2332) {
			if (Animator.aFrame435 != null) {
				/* empty */
			} else {
				if (i != -17779) {
					StaticMethods.anIntArray1624 = null;
				}
				java.awt.Container container;
				if (Class340.aFrame3707 == null) {
					if (CS2Script.anApplet6140 == null) {
						container = Class55.anApplet_Sub1_656;
					} else {
						container = CS2Script.anApplet6140;
					}
				} else {
					container = Class340.aFrame3707;
				}
				StaticMethods.anInt1838 = container.getSize().width;
				Class152.anInt1568 = container.getSize().height;
				if (container == Class340.aFrame3707) {
					Insets insets = Class340.aFrame3707.getInsets();
					StaticMethods.anInt1838 -= insets.left + insets.right;
					Class152.anInt1568 -= insets.bottom + insets.top;
				}
				if (Class8.method192((byte) 101) != 1) {
					Class160.method1618(-8783);
				} else {
					Class241.anInt2301 = Class368_Sub7.anInt5463;
					CS2Stack.anInt2251 = 0;
					Class384.anInt3254 = Class296_Sub15_Sub1.anInt5996;
					Class241_Sub2_Sub2.anInt5908 = (StaticMethods.anInt1838 - Class368_Sub7.anInt5463) / 2;
				}
				if (Class41_Sub29.modeWhere != BITConfigsLoader.liveModeWhere) {
					if (Class241.anInt2301 < 1024 && Class384.anInt3254 < 768) {
						/* empty */
					}
				}
				Class230.aCanvas2209.setSize(Class241.anInt2301, Class384.anInt3254);
				if (Class41_Sub13.aHa3774 != null) {
					if (Class368_Sub5_Sub2.aBoolean6597) {
						Class303.method3268(Class230.aCanvas2209, -2);
					} else {
						Class41_Sub13.aHa3774.a(Class230.aCanvas2209, Class241.anInt2301, Class384.anInt3254);
					}
				}
				if (Class340.aFrame3707 != container) {
					Class230.aCanvas2209.setLocation(Class241_Sub2_Sub2.anInt5908, CS2Stack.anInt2251);
				} else {
					Insets insets = Class340.aFrame3707.getInsets();
					Class230.aCanvas2209.setLocation(insets.left + Class241_Sub2_Sub2.anInt5908, CS2Stack.anInt2251 + insets.top);
				}
				if (Class99.anInt1064 != -1) {
					Animator.method569(true, (byte) 103);
				}
				Class367.method3802(-1);
			}
		}
	}

	public static int[] anIntArray1624 = new int[13];

	public static void method1603(float[] fs, int i, boolean bool, int i_46_, int i_47_, int i_48_, int i_49_, int i_50_, int i_51_, int[] is, int[] is_52_, int i_53_, float[] fs_54_) {
		int i_55_ = i_50_ + i_51_ * i_47_;
		int i_56_ = i + i_53_ * i_48_;
		int i_57_ = i_47_ - i_46_;
		int i_58_ = -i_46_ + i_53_;
		if (is_52_ != null) {
			if (fs != null) {
				for (int i_59_ = 0; i_59_ < i_49_; i_59_++) {
					int i_60_ = i_55_ + i_46_;
					while (i_55_ < i_60_) {
						is[i_56_] = is_52_[i_55_];
						fs_54_[i_56_++] = fs[i_55_++];
					}
					i_55_ += i_57_;
					i_56_ += i_58_;
				}
			} else {
				for (int i_61_ = 0; i_49_ > i_61_; i_61_++) {
					int i_62_ = i_55_ + i_46_;
					while (i_55_ < i_62_) {
						is[i_56_++] = is_52_[i_55_++];
					}
					i_56_ += i_58_;
					i_55_ += i_57_;
				}
			}
		} else {
			for (int i_63_ = 0; i_63_ < i_49_; i_63_++) {
				int i_64_ = i_46_ + i_55_;
				while (i_55_ < i_64_) {
					fs_54_[i_56_++] = fs[i_55_++];
				}
				i_55_ += i_57_;
				i_56_ += i_58_;
			}
		}
	}

	static public CS2Script method1608(int i, int i_80_, int i_81_, Class81 class81) {
		int i_82_ = class81.anInt3668 | i_81_ << 10;
		CS2Script class296_sub39_sub3 = (CS2Script) StaticMethods.scriptsCache.get((long) i_82_ << 16);
		if (i != -1) {
			return null;
		}
		if (class296_sub39_sub3 != null) {
			return class296_sub39_sub3;
		}
		byte[] is = Class72.cs2FS.get(Class72.cs2FS.getFileIndex(i_82_));
		if (is != null) {
			if (is.length <= 1) {
				return null;
			}
			try {
				class296_sub39_sub3 = MaterialRaw.readScript(is);
			} catch (Exception exception) {
				throw new RuntimeException(exception.getMessage() + " S: " + i_82_);
			}
			class296_sub39_sub3.aClass81_6135 = class81;
			StaticMethods.scriptsCache.put((long) i_82_ << 16, class296_sub39_sub3);
			return class296_sub39_sub3;
		}
		i_82_ = class81.anInt3668 | i_80_ + 65536 << 10;
		class296_sub39_sub3 = (CS2Script) StaticMethods.scriptsCache.get((long) i_82_ << 16);
		if (class296_sub39_sub3 != null) {
			return class296_sub39_sub3;
		}
		is = Class72.cs2FS.get(Class72.cs2FS.getFileIndex(i_82_));
		if (is != null) {
			if (is.length <= 1) {
				return null;
			}
			try {
				class296_sub39_sub3 = MaterialRaw.readScript(is);
			} catch (Exception exception) {
				throw new RuntimeException(exception.getMessage() + " S: " + i_82_);
			}
			class296_sub39_sub3.aClass81_6135 = class81;
			StaticMethods.scriptsCache.put((long) i_82_ << 16, class296_sub39_sub3);
			return class296_sub39_sub3;
		}
		i_82_ = class81.anInt3668 | 0x3fffc00;
		class296_sub39_sub3 = (CS2Script) StaticMethods.scriptsCache.get((long) i_82_ << 16);
		if (class296_sub39_sub3 != null) {
			return class296_sub39_sub3;
		}
		is = Class72.cs2FS.get(Class72.cs2FS.getFileIndex(i_82_));
		if (is != null) {
			if (is.length <= 1) {
				return null;
			}
			try {
				class296_sub39_sub3 = MaterialRaw.readScript(is);
			} catch (Exception exception) {
				throw new RuntimeException(exception.getMessage() + " S: " + i_82_);
			}
			class296_sub39_sub3.aClass81_6135 = class81;
			StaticMethods.scriptsCache.put((long) i_82_ << 16, class296_sub39_sub3);
			return class296_sub39_sub3;
		}
		return null;
	}

	public static void method1615(byte i) {
		if (i == 25) {
			IncomingPacket.aClass231_1609 = null;
			anIntArray1624 = null;
		}
	}

	public static void method311(int i) {
		OutgoingPacket.aClass311_292 = null;
		if (i != 2)
			StaticMethods.anInt287 = 76;
		StaticMethods.aClass138_294 = null;
	}

	static final Class252[] method312(byte i) {
		if (i != -33)
			return null;
		return (new Class252[] { Class296_Sub24.aClass252_4760, ReferenceTable.aClass252_975, Class218.aClass252_2124 });
	}

	static final void method310(int i, int i_0_, int i_1_, int i_2_) {
		Class190 class190 = ClipData.aClass190ArrayArray184[i][i_0_];
		Class296_Sub27.method2677((class190 != null ? class190 : Class218.aClass190_2125), i_2_, (byte) -96);
		int i_3_ = -16 % ((44 - i_1_) / 55);
	}

	public static int anInt287;
	public static Js5 aClass138_294;
	public static float[] aFloatArray3637 = new float[16];

	public static final Class241 method2121(int i, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		if (i_4_ <= 41) {
			method2121(-38, -16, 113, -120, 13, 47, -41);
		}
		long l = i * 76724863L ^ i_2_ * 475427L ^ i_6_ * 97549L ^ i_5_ * 67481L ^ i_7_ * 986053L ^ i_3_ * 32147369L;
		Class241 class241 = (Class241) Class366_Sub9.aClass113_5419.get(l);
		if (class241 != null) {
			return class241;
		}
		class241 = Class368_Sub16.aHa5527.c(i_5_, i_6_, i_2_, i_7_, i_3_, i);
		Class366_Sub9.aClass113_5419.put(class241, l);
		return class241;
	}

	public static final void method2120(String string, int i) {
		Class181.method1827(0, "", 0, "", string, "", 0);
		int i_1_ = 105 % ((i + 54) / 40);
	}

	public static void method2122(boolean bool) {
		aFloatArray3637 = null;
	}

	static final int method2456(int i) {
		int i_2_ = 7 / ((11 - i) / 51);
		int i_3_;
		if (FileWorker.anInt3004 < 96) {
			i_3_ = 1;
			Class368_Sub20.method3857(true, (byte) 106);
		} else {
			int i_4_ = Class368_Sub5_Sub1.method3828((byte) 62);
			if (i_4_ > 100) {
				if (i_4_ > 500) {
					if (i_4_ <= 1000) {
						Class276.method2321(1);
						i_3_ = 2;
					} else {
						Class368_Sub20.method3857(true, (byte) 105);
						i_3_ = 1;
					}
				} else {
					i_3_ = 3;
					Class205_Sub4.method1986((byte) 49);
				}
			} else {
				i_3_ = 4;
				Class230.method2108(-63);
			}
		}
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(127) != 0) {
			Class343_Sub1.aClass296_Sub50_5282.method3060(0, 99, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988);
			Class33.method348(false, false, 0);
		}
		Class368_Sub4.method3820(1);
		return i_3_;
	}

	public static final boolean method2452(int i, int i_0_, int i_1_) {
		if (i != -2361)
			StaticMethods.method2453(-9);
		if (!Class296_Sub51_Sub27_Sub1.method3161(-19214, i_0_, i_1_) && !Class184.method1857(i_0_, i_1_, (byte) 121))
			return false;
		return true;
	}

	public static void method2453(int i) {
		if (i != 0)
			StaticMethods.anIntArray4629 = null;
		StaticMethods.anIntArray4629 = null;
		OutgoingPacket.aClass311_4628 = null;
	}

	public static int[] anIntArray4629 = new int[3];

	public static void method3962(int i) {
		if (i != -11) {
			method3962(66);
		}
		Class161.aClass161_3193 = null;
	}

	static int anInt3196 = 0;

	static final void method2465(int i) {
		if (Class241.aClass296_Sub39_Sub1_2302 != null) {
			if (i <= 98) {
				method2465(-2);
			}
			Class241.aClass296_Sub39_Sub1_2302 = null;
			Class375.method3953((byte) -109, Class95.anInt1036, StaticMethods.anInt3152, Class41_Sub5.anInt3753, Class270.anInt2510);
		}
	}

	public static void method2464(byte i) {
		StaticMethods.aClass138_5928 = null;
		StaticMethods.aShortArray5932 = null;
		StaticMethods.anIntArrayArray5929 = null;
		StaticMethods.aClass138_5927 = null;
		if (i <= -102) {
			StaticMethods.anIntArray5930 = null;
		}
	}

	static byte method2457(int i, int i_0_, int i_1_) {
		if (i != 9) {
			return (byte) 0;
		}
		if ((i_0_ & 0x1) == i_1_) {
			return (byte) 1;
		}
		return (byte) 2;
	}

	static int anInt5910;
	static int[] anIntArray5967 = { 19, 55, 38, 155, 255, 110, 137, 205, 76 };

	public static void method2476(byte i) {
		ModeWhere.officeModeWhere = null;
		if (i != 15) {
			ModeWhere.officeModeWhere = null;
		}
		anIntArray5967 = null;
	}

	static int anInt5970 = 0;
	static int anInt5969 = 0;
	static int anInt5968;

	static void method2479(int i, int i_3_) {
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub20_4996.method470(i ^ ~0x78) == 0) {
			i_3_ = -1;
		}
		if (i_3_ != Class379.anInt3618) {
			if (i != i_3_) {
				Class325 class325 = Class355.aClass394_3067.method4062(i_3_, i + 34);
				Class186 class186 = class325.method3381((byte) 60);
				if (class186 != null) {
					Class252.aClass398_2383.method4126(class186.method1880(), class186.method1882(), new Point(class325.anInt2865, class325.anInt2871), class186.method1869(), Class230.aCanvas2209, 63);
					Class379.anInt3618 = i_3_;
				} else {
					i_3_ = -1;
				}
			}
			if (i_3_ == -1 && Class379.anInt3618 != -1) {
				Class252.aClass398_2383.method4126(-1, -1, new Point(), null, Class230.aCanvas2209, 64);
				Class379.anInt3618 = -1;
			}
		}
	}

	static boolean method2477(int i, int i_0_, int i_1_) {
		if (i != 10) {
			method2477(-113, 44, -47);
		}
		return false;
	}

	static int lookatSpeed;
	static int anInt5974 = 0;

	static final void writeAccountCreation(int i, boolean bool, int i_0_, String password, String username) {
			Class296_Sub1 class296_sub1 = Class296_Sub51_Sub30.method3169(57);
			class296_sub1.out.p1(Class292.aClass269_2671.protocolId); // 22
			class296_sub1.out.p2(0);
			int i_2_ = class296_sub1.out.pos;
			class296_sub1.out.p2(i);
	//		int[] is = Class139.writeIsaacKeys(class296_sub1, -126);
			int i_3_ = class296_sub1.out.pos;
			class296_sub1.out.writeString(username);
			class296_sub1.out.p2(Class209.affliateID);
			class296_sub1.out.writeString(password);
			class296_sub1.out.writeLong(Class189.userflow);
			class296_sub1.out.p1(Class394.langID);
			class296_sub1.out.p1(Class296_Sub50.game.anInt340);
			Class366_Sub6.method3787(class296_sub1.out, (byte) 95);
			String string_4_ = Class368_Sub2.additionalInfo;
			class296_sub1.out.p1(string_4_ != null ? 1 : 0);
			if (string_4_ != null) {
				class296_sub1.out.writeString(string_4_);
			}
			class296_sub1.out.p1(i_0_);
			class296_sub1.out.p1(bool ? 1 : 0);
			class296_sub1.out.pos += 7;
	//		class296_sub1.out.encryptXteas(is, i_3_, class296_sub1.out.position);
			class296_sub1.out.endBytePacket(class296_sub1.out.pos - i_2_);
			Class296_Sub45_Sub2.aClass204_6276.sendPacket(class296_sub1, (byte) 119);
			Class397_Sub3.anInt5799 = 1;
			Class368_Sub4.anInt5441 = -3;
			Class162.anInt3555 = 0;
			Class41_Sub28.anInt3817 = 0;
			if (i_0_ < 13) {
				Class28.under = true;
				Class42_Sub1.method532(i ^ 0x1acd);
			}
		}

	static final void method2460(int i, int i_5_, int i_6_, int i_7_, Class296_Sub36 class296_sub36, int i_8_) {
		if (class296_sub36.anInt4851 != -1 || class296_sub36.anIntArray4853 != null) {
			int i_9_ = 0;
			int i_10_ = class296_sub36.anInt4856 * Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5005.method489(i_7_ ^ 0x107e) >> 8;
			if (i_5_ <= class296_sub36.anInt4859) {
				if (class296_sub36.anInt4880 > i_5_) {
					i_9_ += -i_5_ + class296_sub36.anInt4880;
				}
			} else {
				i_9_ += i_5_ - class296_sub36.anInt4859;
			}
			if (class296_sub36.anInt4869 >= i_6_) {
				if (class296_sub36.anInt4879 > i_6_) {
					i_9_ += -i_6_ + class296_sub36.anInt4879;
				}
			} else {
				i_9_ += -class296_sub36.anInt4869 + i_6_;
			}
			if (class296_sub36.anInt4863 == 0 || class296_sub36.anInt4863 < i_9_ - 256 || Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5005.method489(124) == 0 || class296_sub36.anInt4861 != i_8_) {
				if (class296_sub36.aClass296_Sub45_Sub1_4871 != null) {
					Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36.aClass296_Sub45_Sub1_4871);
					class296_sub36.aClass296_Sub18_4866 = null;
					class296_sub36.aClass296_Sub45_Sub1_4871 = null;
					class296_sub36.aClass296_Sub19_Sub1_4862 = null;
				}
				if (class296_sub36.aClass296_Sub45_Sub1_4860 != null) {
					Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36.aClass296_Sub45_Sub1_4860);
					class296_sub36.aClass296_Sub19_Sub1_4875 = null;
					class296_sub36.aClass296_Sub18_4852 = null;
					class296_sub36.aClass296_Sub45_Sub1_4860 = null;
				}
			} else {
				i_9_ -= 256;
				if (i_9_ < 0) {
					i_9_ = 0;
				}
				int i_11_ = -class296_sub36.anInt4876 + class296_sub36.anInt4863;
				if (i_11_ < 0) {
					i_11_ = class296_sub36.anInt4863;
				}
				if (i_7_ == 4096) {
					int i_12_ = i_10_;
					int i_13_ = -class296_sub36.anInt4876 + i_9_;
					if (i_13_ > 0 && i_11_ > 0) {
						i_12_ = (i_11_ - i_13_) * i_10_ / i_11_;
					}
					Class296_Sub51_Sub11.localPlayer.getSize();
					int i_14_ = 8192;
					int i_15_ = -i_5_ + (class296_sub36.anInt4880 + class296_sub36.anInt4859) / 2;
					int i_16_ = (class296_sub36.anInt4869 + class296_sub36.anInt4879) / 2 - i_6_;
					if (i_15_ != 0 || i_16_ != 0) {
						int i_17_ = -Class44_Sub1.camRotY + -(int) (Math.atan2(i_15_, i_16_) * 2607.5945876176133) - 4096 & 0x3fff;
						if (i_17_ > 8192) {
							i_17_ = -i_17_ + 16384;
						}
						int i_18_;
						if (i_9_ > 0) {
							if (i_9_ < 4096) {
								i_18_ = i_9_ * 8192 / 4096 + 8192;
							} else {
								i_18_ = 16384;
							}
						} else {
							i_18_ = 8192;
						}
						i_14_ = (-i_18_ + 16384 >> 1) + i_18_ * i_17_ / 8192;
					}
					if (class296_sub36.aClass296_Sub45_Sub1_4871 != null) {
						class296_sub36.aClass296_Sub45_Sub1_4871.method2958(i_12_);
						class296_sub36.aClass296_Sub45_Sub1_4871.method2943(i_14_);
					} else if (class296_sub36.anInt4851 >= 0) {
						int i_19_ = class296_sub36.anInt4872 != 256 || class296_sub36.anInt4857 != 256 ? Class220.method2072(class296_sub36.anInt4872, false, class296_sub36.anInt4857) : 256;
						if (!class296_sub36.aBoolean4850) {
							Class171 class171 = Class171.method1675(Class93.fs4, class296_sub36.anInt4851, 0);
							if (class171 != null) {
								Class296_Sub19_Sub1 class296_sub19_sub1 = class171.method1676().method2652(Class264.aClass288_2474);
								Class296_Sub45_Sub1 class296_sub45_sub1 = Class296_Sub45_Sub1.method2970(class296_sub19_sub1, i_19_, i_12_ << 6, i_14_);
								class296_sub45_sub1.method2960(-1);
								Class16_Sub3.aClass296_Sub45_Sub5_3734.method3035(class296_sub45_sub1);
								class296_sub36.aClass296_Sub45_Sub1_4871 = class296_sub45_sub1;
							}
						} else {
							if (class296_sub36.aClass296_Sub18_4866 == null) {
								class296_sub36.aClass296_Sub18_4866 = Class296_Sub18.method2640(Class296_Sub15.aClass138_4671, class296_sub36.anInt4851);
							}
							if (class296_sub36.aClass296_Sub18_4866 != null) {
								if (class296_sub36.aClass296_Sub19_Sub1_4862 == null) {
									class296_sub36.aClass296_Sub19_Sub1_4862 = class296_sub36.aClass296_Sub18_4866.method2644(new int[] { 22050 });
								}
								if (class296_sub36.aClass296_Sub19_Sub1_4862 != null) {
									Class296_Sub45_Sub1 class296_sub45_sub1 = Class296_Sub45_Sub1.method2970(class296_sub36.aClass296_Sub19_Sub1_4862, i_19_, i_12_ << 6, i_14_);
									class296_sub45_sub1.method2960(-1);
									Class16_Sub3.aClass296_Sub45_Sub5_3734.method3035(class296_sub45_sub1);
									class296_sub36.aClass296_Sub45_Sub1_4871 = class296_sub45_sub1;
								}
							}
						}
					}
					while_49_: do {
						do {
							if (class296_sub36.aClass296_Sub45_Sub1_4860 == null) {
								if (class296_sub36.anIntArray4853 != null && (class296_sub36.anInt4867 -= i) <= 0) {
									int i_20_ = class296_sub36.anInt4872 != 256 || class296_sub36.anInt4857 != 256 ? (int) (Math.random() * (class296_sub36.anInt4872 - class296_sub36.anInt4857)) + class296_sub36.anInt4857 : 256;
									if (class296_sub36.aBoolean4855) {
										if (class296_sub36.aClass296_Sub18_4852 == null) {
											int i_21_ = (int) (Math.random() * class296_sub36.anIntArray4853.length);
											class296_sub36.aClass296_Sub18_4852 = Class296_Sub18.method2640(Class296_Sub15.aClass138_4671, class296_sub36.anIntArray4853[i_21_]);
										}
										if (class296_sub36.aClass296_Sub18_4852 != null) {
											if (class296_sub36.aClass296_Sub19_Sub1_4875 == null) {
												class296_sub36.aClass296_Sub19_Sub1_4875 = class296_sub36.aClass296_Sub18_4852.method2644(new int[] { 22050 });
											}
											if (class296_sub36.aClass296_Sub19_Sub1_4875 != null) {
												Class296_Sub45_Sub1 class296_sub45_sub1 = Class296_Sub45_Sub1.method2970(class296_sub36.aClass296_Sub19_Sub1_4875, i_20_, i_12_ << 6, i_14_);
												class296_sub45_sub1.method2960(0);
												Class16_Sub3.aClass296_Sub45_Sub5_3734.method3035(class296_sub45_sub1);
												class296_sub36.anInt4867 = (int) ((class296_sub36.anInt4854 - class296_sub36.anInt4858) * Math.random()) + class296_sub36.anInt4858;
												class296_sub36.aClass296_Sub45_Sub1_4860 = class296_sub45_sub1;
											}
										}
									} else {
										int i_22_ = (int) (class296_sub36.anIntArray4853.length * Math.random());
										Class171 class171 = Class171.method1675(Class93.fs4, class296_sub36.anIntArray4853[i_22_], 0);
										if (class171 == null) {
											break;
										}
										Class296_Sub19_Sub1 class296_sub19_sub1 = class171.method1676().method2652(Class264.aClass288_2474);
										Class296_Sub45_Sub1 class296_sub45_sub1 = Class296_Sub45_Sub1.method2970(class296_sub19_sub1, i_20_, i_12_ << 6, i_14_);
										class296_sub45_sub1.method2960(0);
										Class16_Sub3.aClass296_Sub45_Sub5_3734.method3035(class296_sub45_sub1);
										class296_sub36.anInt4867 = class296_sub36.anInt4858 + (int) (Math.random() * (class296_sub36.anInt4854 - class296_sub36.anInt4858));
										class296_sub36.aClass296_Sub45_Sub1_4860 = class296_sub45_sub1;
									}
									break;
								}
							} else {
								class296_sub36.aClass296_Sub45_Sub1_4860.method2958(i_12_);
								class296_sub36.aClass296_Sub45_Sub1_4860.method2943(i_14_);
								if (class296_sub36.aClass296_Sub45_Sub1_4860.isLinked((byte) -123)) {
									break while_49_;
								}
								class296_sub36.aClass296_Sub19_Sub1_4875 = null;
								class296_sub36.aClass296_Sub18_4852 = null;
								class296_sub36.aClass296_Sub45_Sub1_4860 = null;
							}
						} while (false);
						break;
					} while (false);
				}
			}
		}
	}

	static short[][] aShortArrayArray5919;
	static SubCache scriptsCache = new SubCache(128);
	static Class213 aClass213_5917 = new Class213(0);

	static void method2461(int i, int i_24_) {
		Class296_Sub30.anInt4816 = i;
		TextureOperation.aClass138_5040 = null;
		Class296_Sub51_Sub7.anInt6371 = -1;
		Class368_Sub1.anInt5423 = -1;
		Class392.anInt3488 = i_24_;
		ReferenceWrapper.anInt6128 = 1;
		Class249.aClass296_Sub45_Sub4_2357 = null;
		Class296_Sub50.aBoolean5025 = false;
	}

	public static void method2462(int i) {
		aClass213_5917 = null;
		if (i != -479059834) {
			method2461(-110, 110);
		}
		IncomingPacket.aClass231_5916 = null;
		scriptsCache = null;
		aShortArrayArray5919 = null;
	}

	static Class36 aClass36_5938;
	static String[] aStringArray5940 = new String[200];
	static Class209 aClass209_5941 = new Class209(255);
	static int anInt5942;
	static InterfaceComponent aClass51_5944;
	static boolean[] aBooleanArray5945 = new boolean[200];

	static final void method2467(int i, int i_0_, int i_1_, int i_2_, byte i_3_, int i_4_, int i_5_) {
		if (ConfigurationDefinition.anInt676 > i_0_ - i || Class288.anInt2652 < i + i_0_ || i_4_ - i < EmissiveTriangle.anInt952 || RuntimeException_Sub1.anInt3391 < i + i_4_) {
			Class206.method1991(i_4_, (byte) 115, i_5_, i_1_, i, i_2_, i_0_);
		} else {
			AnimBase.method2671(i_2_, i_0_, i_4_, i_1_, 0, i, i_5_);
		}
		if (i_3_ != 52) {
			aClass36_5938 = null;
		}
	}

	public static void method2468(int i) {
		aClass36_5938 = null;
		aStringArray5940 = null;
		aBooleanArray5945 = null;
		aClass51_5944 = null;
		if (i != 9) {
			method2467(92, -2, 32, -111, (byte) -28, 73, 110);
		}
		aClass209_5941 = null;
	}

	static long aLong5950 = 0L;
	static Class142[] aClass142Array5951;
	static int[] anIntArray5953 = {0, 1, 2, 2, 1, 1, 2, 3, 1, 3, 3, 4, 2, 0, 4};

	public static void method2471(boolean bool) {
		anIntArray5953 = null;
		aClass142Array5951 = null;
		if (bool != true) {
			aLong5950 = 82L;
		}
	}

	static final long method2466(int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_) {
		int i_7_ = 62 % ((37 - i_3_) / 43);
		Class296_Sub38.aCalendar4898.clear();
		Class296_Sub38.aCalendar4898.set(i_4_, i, i_6_, i_5_, i_2_, i_1_);
		return Class296_Sub38.aCalendar4898.getTime().getTime();
	}

	static int anInt5935 = 1403;

	static final void method2469(byte i) {
		if (Class99.anInt1064 != -1) {
			int i_1_ = Class84.aClass189_924.method1895((byte) -55);
			if (i != -83) {
				StaticMethods.anInt5949 = 19;
			}
			int i_2_ = Class84.aClass189_924.method1897(i ^ ~0x52);
			Class296_Sub34 class296_sub34 = (Class296_Sub34) Class403.aClass155_3379.removeFirst((byte) 120);
			if (class296_sub34 != null) {
				i_1_ = class296_sub34.method2732(-6);
				i_2_ = class296_sub34.method2736(-6);
			}
			int i_3_ = 0;
			int i_4_ = 0;
			if (Class368_Sub5_Sub2.aBoolean6597) {
				i_3_ = Class387.method4034(true);
				i_4_ = GraphicsLoader.method2286(true);
			}
			Class104.method900(i_2_, i_4_ + Class384.anInt3254, Class99.anInt1064, i_4_, i_3_, i_3_ + Class241.anInt2301, i_3_ + i_1_, i_4_, i_1_, i_3_, i_4_ + i_2_, (byte) 63);
			if (Class359.aClass51_3092 != null) {
				Mobile.method3495(0, i_3_ + i_1_, i_2_ + i_4_);
			}
		}
	}

	static final void method2470(int i, int i_5_) {
		if (i_5_ != -1) {
			method2470(-12, 69);
		}
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask(i, 14);
		class296_sub39_sub5.insertIntoQueue();
	}

	static int anInt5947 = -1;
	static float aFloat5948;
	static int anInt5949;
	static int anInt5956 = 0;
	static Class258 aClass258_5957 = new Class258();
	static Class181_Sub1 aClass181_Sub1_5960;
	static boolean aBoolean5963;

	static final void method2472(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_) {
		int i_5_ = 0;
		int i_6_ = i_3_;
		int i_7_ = i_4_ * i_4_;
		int i_8_ = i_3_ * i_3_;
		int i_9_ = i_8_ << 1;
		int i_10_ = i_7_ << 1;
		int i_11_ = i_3_ << 1;
		int i_12_ = i_9_ + (-i_11_ + 1) * i_7_;
		if (i_0_ != 11012) {
			StaticMethods.method2473(-121);
		}
		int i_13_ = -(i_10_ * (i_11_ - 1)) + i_8_;
		int i_14_ = i_7_ << 2;
		int i_15_ = i_8_ << 2;
		int i_16_ = i_9_ * ((i_5_ << 1) + 3);
		int i_17_ = ((i_6_ << 1) - 3) * i_10_;
		int i_18_ = (i_5_ + 1) * i_15_;
		if (i_2_ >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i_2_) {
			int i_19_ = ParticleEmitterRaw.method1668(i_4_ + i_1_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -36);
			int i_20_ = ParticleEmitterRaw.method1668(-i_4_ + i_1_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -16);
			Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_2_], i_19_, (byte) 126, i, i_20_);
		}
		int i_21_ = (i_6_ - 1) * i_14_;
		while (i_6_ > 0) {
			if (i_12_ < 0) {
				while (i_12_ < 0) {
					i_13_ += i_18_;
					i_12_ += i_16_;
					i_16_ += i_15_;
					i_5_++;
					i_18_ += i_15_;
				}
			}
			if (i_13_ < 0) {
				i_13_ += i_18_;
				i_12_ += i_16_;
				i_18_ += i_15_;
				i_16_ += i_15_;
				i_5_++;
			}
			i_12_ -= i_21_;
			i_13_ -= i_17_;
			i_6_--;
			i_21_ -= i_14_;
			i_17_ -= i_14_;
			int i_22_ = -i_6_ + i_2_;
			int i_23_ = i_6_ + i_2_;
			if (i_23_ >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i_22_) {
				int i_24_ = ParticleEmitterRaw.method1668(i_1_ + i_5_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -84);
				int i_25_ = ParticleEmitterRaw.method1668(-i_5_ + i_1_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -50);
				if (i_22_ >= EmissiveTriangle.anInt952) {
					Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_22_], i_24_, (byte) 125, i, i_25_);
				}
				if (i_23_ <= RuntimeException_Sub1.anInt3391) {
					Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_23_], i_24_, (byte) -122, i, i_25_);
				}
			}
		}
	}

	public static void method2473(int i) {
		if (i >= -42) {
			method2473(35);
		}
		aClass181_Sub1_5960 = null;
		aClass258_5957 = null;
	}

	static final void method2474(ha var_ha, boolean bool) {
		if (bool == true && Class163.aClass155_1679.method1580(123) != 0) {
			if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(124) != 0) {
				if (Class137.aHa1408 == null) {
					Canvas canvas = new Canvas();
					canvas.setSize(36, 32);
					Class137.aHa1408 = Animator.method562(Class122_Sub1_Sub1.aClass138_6604, canvas, 127, 0, 0, Class316.aD2803);
					Class355.aClass55_5909 = Class137.aHa1408.a(Class92.method2821(Class368_Sub15.anInt5518, 0, -99, LookupTable.aClass138_53), Class186.method1871(Class205_Sub2.fs8, Class368_Sub15.anInt5518, 0), true);
				}
				for (Class296_Sub44 class296_sub44 = (Class296_Sub44) Class163.aClass155_1679.removeFirst((byte) 120); class296_sub44 != null; class296_sub44 = (Class296_Sub44) Class163.aClass155_1679.removeNext(1001)) {
					Class296_Sub39_Sub1.itemDefinitionLoader.getIcon(false, class296_sub44.anInt4946, Class137.aHa1408, 13, false, var_ha, Class355.aClass55_5909, class296_sub44.aBoolean4943 ? Class296_Sub51_Sub11.localPlayer.composite : null, class296_sub44.anInt4944, class296_sub44.anInt4945, class296_sub44.anInt4948, class296_sub44.anInt4942);
					class296_sub44.unlink();
				}
			} else {
				for (Class296_Sub44 class296_sub44 = (Class296_Sub44) Class163.aClass155_1679.removeFirst((byte) 116); class296_sub44 != null; class296_sub44 = (Class296_Sub44) Class163.aClass155_1679.removeNext(1001)) {
					Class296_Sub39_Sub1.itemDefinitionLoader.getIcon(false, class296_sub44.anInt4946, var_ha, 13, false, var_ha, Class41_Sub2.aClass55_3742, !class296_sub44.aBoolean4943 ? null : Class296_Sub51_Sub11.localPlayer.composite, class296_sub44.anInt4944, class296_sub44.anInt4945, class296_sub44.anInt4948, class296_sub44.anInt4942);
					class296_sub44.unlink();
				}
				Class366_Sub8.method3794(true);
			}
		}
	}

	static final void method2475(byte i) {
		Class366_Sub9.aClass113_5419.clear();
		Class41_Sub7.aClass113_3761.clear();
		if (i != 12) {
			aClass258_5957 = null;
		}
	}

	static int anInt5920 = 2;
	static int anInt5924;
	static boolean aBoolean5925;
	static Js5 aClass138_5927;
	static Js5 aClass138_5928;
	static int[][] anIntArrayArray5929;
	static int[] anIntArray5930 = new int[25];
	static short[] aShortArray5932;
	public static int[] anIntArray3143 = new int[2];
	public static int anInt3150;
	public static int anInt3152;
	public static int anInt3161 = 0;
	public static byte[][] aByteArrayArray3167;

	static final void method3898(byte i) {
		Class110.aClass113_1143.clearSoftReferences();
	}

	static NodeDeque aClass155_1842 = new NodeDeque();

	public static void method1709(int i) {
		StaticMethods.anIntArray1844 = null;
		aClass155_1842 = null;
		int i_0_ = 32 % ((15 - i) / 50);
	}

	static final void method1708(boolean bool) {
		Class110.aClass113_1143.clear();
		if (bool != true) {
			method1709(96);
		}
	}

	static final String method1710(char c, String string, String string_1_, int i) {
		try {
			int i_2_ = string.length();
			if (i != -9271) {
				return null;
			}
			int i_3_ = string_1_.length();
			int i_4_ = i_2_;
			int i_5_ = i_3_ - 1;
			if (i_5_ != 0) {
				int i_6_ = 0;
				for (;;) {
					i_6_ = string.indexOf(c, i_6_);
					if (i_6_ < 0) {
						break;
					}
					i_6_++;
					i_4_ += i_5_;
				}
			}
			StringBuffer stringbuffer = new StringBuffer(i_4_);
			int i_7_ = 0;
			for (;;) {
				int i_8_ = string.indexOf(c, i_7_);
				if (i_8_ < 0) {
					break;
				}
				stringbuffer.append(string.substring(i_7_, i_8_));
				stringbuffer.append(string_1_);
				i_7_ = i_8_ + 1;
			}
			stringbuffer.append(string.substring(i_7_));
			return stringbuffer.toString();
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "jw.E(" + c + ',' + (string != null ? "{...}" : "null") + ',' + (string_1_ != null ? "{...}" : "null") + ',' + i + ')');
		}
	}

	static int[] anIntArray1844;
	static int anInt1843;
	static int anInt1838;

	static final int method2722(int i, String string) {
		if (string == null) {
			return -1;
		}
		for (int i_0_ = 0; Class285.anInt2625 > i_0_; i_0_++) {
			if (string.equalsIgnoreCase(Js5TextureLoader.aStringArray3443[i_0_])) {
				return i_0_;
			}
		}
		if (i != -1) {
			StaticMethods.aClass51_4842 = null;
		}
		return -1;
	}

	public static void method2719(byte i) {
		StaticMethods.aClass51_4842 = null;
		if (i > -61) {
			StaticMethods.aClass51_4842 = null;
		}
	}

	static InterfaceComponent aClass51_4842 = null;
	static boolean aBoolean2913;
	static int anInt2915 = 0;

	static void method2729(int i) {
		Class318.aBoolean2809 = true;
		if (i != -1325) {
			StaticMethods.aString6087 = null;
		}
	}

	static Class338_Sub3_Sub4 method2730(int i, int i_0_, int i_1_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_0_][i_1_];
		if (class247 == null) {
			return null;
		}
		return class247.aClass338_Sub3_Sub4_2348;
	}

	public static void method2731(int i) {
		StaticMethods.aString6087 = null;
		StaticMethods.anIntArray6089 = null;
		StaticMethods.anIntArray6082 = null;
		StaticMethods.aClass296_Sub39_Sub9_6085 = null;
		if (i != 20905) {
			method2729(108);
		}
	}

	static int[] anIntArray6082 = { 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3 };
	static Class296_Sub39_Sub9 aClass296_Sub39_Sub9_6085;
	static int anInt6084;
	static int[] anIntArray6089 = new int[1000];
	static String aString6087 = null;

	static final int method2723(int i, int i_0_) {
		if (i_0_ == 16711935) {
			return -1;
		}
		if (i != 11588) {
			method2723(-59, 58);
		}
		return StaticMethods.method2727(i_0_, (byte) 13);
	}

	static final Class338_Sub3_Sub4 method2724(int i, int i_1_, int i_2_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_1_][i_2_];
		if (class247 == null) {
			return null;
		}
		return class247.aClass338_Sub3_Sub4_2343;
	}

	static {
		aClass231_6078 = new IncomingPacket(111, 3);
		aClass311_6080 = new OutgoingPacket(57, 11);
	}

	static IncomingPacket aClass231_6078;
	static OutgoingPacket aClass311_6080;
	static float[] aFloatArray6079 = new float[4];
	static IncomingPacket aClass231_6076 = new IncomingPacket(97, 8);
	static float aFloat6081;

	public static void method2728(int i) {
		aClass231_6076 = null;
		if (i == 3) {
			aClass231_6078 = null;
			aFloatArray6079 = null;
			aClass311_6080 = null;
		}
	}

	static boolean[] aBooleanArray6066 = new boolean[100];
	static Class202 aClass202_6068 = new Class202(1);
	static Class328 aClass328_6070;
	static OutgoingPacket aClass311_6071 = new OutgoingPacket(43, 3);
	static Sprite[] aClass397Array6073;
	static Class296_Sub57 aClass296_Sub57_6074;
	static int anInt6075 = 0;

	public static void method2725(int i) {
		aBooleanArray6066 = null;
		aClass397Array6073 = null;
		aClass311_6071 = null;
		if (i == -13950) {
			aClass328_6070 = null;
			aClass202_6068 = null;
			aClass296_Sub57_6074 = null;
		}
	}

	static final int method2727(int i, byte i_2_) {
		double d = ((i & 0xffe038) >> 16) / 256.0;
		double d_3_ = ((i & 0xff36) >> 8) / 256.0;
		double d_4_ = (i & 0xff) / 256.0;
		double d_5_ = d;
		if (d_5_ > d_3_) {
			d_5_ = d_3_;
		}
		if (d_5_ > d_4_) {
			d_5_ = d_4_;
		}
		double d_6_ = d;
		if (d_6_ < d_3_) {
			d_6_ = d_3_;
		}
		if (d_4_ > d_6_) {
			d_6_ = d_4_;
		}
		if (i_2_ != 13) {
			return -83;
		}
		double d_7_ = 0.0;
		double d_8_ = 0.0;
		double d_9_ = (d_6_ + d_5_) / 2.0;
		if (d_5_ != d_6_) {
			if (d_9_ < 0.5) {
				d_8_ = (-d_5_ + d_6_) / (d_6_ + d_5_);
			}
			if (d != d_6_) {
				if (d_6_ == d_3_) {
					d_7_ = (-d + d_4_) / (-d_5_ + d_6_) + 2.0;
				} else if (d_6_ == d_4_) {
					d_7_ = (d - d_3_) / (-d_5_ + d_6_) + 4.0;
				}
			} else {
				d_7_ = (-d_4_ + d_3_) / (-d_5_ + d_6_);
			}
			if (d_9_ >= 0.5) {
				d_8_ = (d_6_ - d_5_) / (-d_5_ + (2.0 - d_6_));
			}
		}
		d_7_ /= 6.0;
		int i_10_ = (int) (d_7_ * 256.0);
		int i_11_ = (int) (d_8_ * 256.0);
		int i_12_ = (int) (d_9_ * 256.0);
		if (i_11_ < 0) {
			i_11_ = 0;
		} else if (i_11_ > 255) {
			i_11_ = 255;
		}
		if (i_12_ >= 0) {
			if (i_12_ > 255) {
				i_12_ = 255;
			}
		} else {
			i_12_ = 0;
		}
		if (i_12_ <= 243) {
			if (i_12_ <= 217) {
				if (i_12_ > 192) {
					i_11_ >>= 2;
				} else if (i_12_ > 179) {
					i_11_ >>= 1;
				}
			} else {
				i_11_ >>= 3;
			}
		} else {
			i_11_ >>= 4;
		}
		return (i_12_ >> 1) + ((i_10_ & 0xff) >> 2 << 10) + (i_11_ >> 5 << 7);
	}

	static {
		StaticMethods.aClass296_Sub57_6074 = new Class296_Sub57(0, 0);
	}

	public static Class224[] method3207(Class398 class398, byte i) {
		if (!class398.method4131(-1343340912)) {
			return new Class224[0];
		}
		Class278 class278 = class398.method4120((byte) 3);
		while (class278.anInt2540 == 0) {
			Class106_Sub1.method942(10L, 0);
		}
		if (class278.anInt2540 == 2) {
			return new Class224[0];
		}
		int[] is = (int[]) class278.anObject2539;
		Class224[] class224s = new Class224[is.length >> 2];
		if (i < 4) {
			StaticMethods.method3210(false);
		}
		for (int i_3_ = 0; class224s.length > i_3_; i_3_++) {
			Class224 class224 = new Class224();
			class224s[i_3_] = class224;
			class224.anInt2167 = is[i_3_ << 2];
			class224.anInt2171 = is[(i_3_ << 2) + 1];
			class224.anInt2168 = is[(i_3_ << 2) + 2];
			class224.anInt2170 = is[(i_3_ << 2) + 3];
		}
		return class224s;
	}

	static {
		aClass161_5053 = new Class161(4, 1, 1, 1);
	}

	public static Class161 aClass161_5053;
	public static boolean aBoolean5054 = false;

	public static void method3210(boolean bool) {
		if (bool != true) {
			method3207(null, (byte) 69);
		}
		aClass161_5053 = null;
	}

	public static int anInt5058;

}
