package net.zaros.client;

/* Class314 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class314 {
	static int anInt2781;
	static int anInt2782;
	int anInt2783;
	int anInt2784;
	int anInt2785;
	static int anInt2786;
	/* synthetic */static Class aClass2787;

	static final short[][] method3319(int i, float[][] fs, short[][] is) {
		int i_0_ = -41 / ((-24 - i) / 50);
		for (int i_1_ = 0; i_1_ < fs.length; i_1_++) {
			for (int i_2_ = 0; is[i_1_].length > i_2_; i_2_++)
				is[i_1_][i_2_] = (short) (int) (fs[i_1_][i_2_] * 16383.0F);
		}
		anInt2782++;
		return is;
	}

	public Class314() {
		/* empty */
	}

	static final void method3320(int i, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_) {
		if (i_8_ >= 0 && i_3_ >= 0 && i_8_ < Class198.currentMapSizeX - 1 && i_3_ < Class296_Sub38.currentMapSizeY - 1) {
			if (Class338_Sub2.aClass247ArrayArrayArray5195 == null)
				return;
			if (i_6_ == 0) {
				Interface14 interface14 = ((Interface14) StaticMethods.method2730(i_7_, i_8_, i_3_));
				Interface14 interface14_10_ = ((Interface14) StaticMethods.method2724(i_7_, i_8_, i_3_));
				if (interface14 != null && i != 2) {
					if (!(interface14 instanceof Class338_Sub3_Sub4_Sub1))
						Class218.method2042(i_6_, i_8_, i_9_, i_7_, i, i_3_, i_5_, true, interface14.method57((byte) 116));
					else
						((Class338_Sub3_Sub4_Sub1) interface14).aClass380_6656.method3981(i_9_, (byte) -90);
				}
				if (interface14_10_ != null) {
					if (!(interface14_10_ instanceof Class338_Sub3_Sub4_Sub1))
						Class218.method2042(i_6_, i_8_, i_9_, i_7_, i, i_3_, i_5_, true, interface14_10_.method57((byte) 107));
					else
						((Class338_Sub3_Sub4_Sub1) interface14_10_).aClass380_6656.method3981(i_9_, (byte) -115);
				}
			} else if (i_6_ == 1) {
				Interface14 interface14 = (Interface14) Class172.method1679(i_7_, i_8_, i_3_);
				if (interface14 != null) {
					if (!(interface14 instanceof Class338_Sub3_Sub3_Sub2)) {
						int i_11_ = interface14.method57((byte) 127);
						if (i == 4 || i == 5)
							Class218.method2042(i_6_, i_8_, i_9_, i_7_, 4, i_3_, i_5_, true, i_11_);
						else if (i == 6)
							Class218.method2042(i_6_, i_8_, i_9_, i_7_, 4, i_3_, i_5_ + 4, true, i_11_);
						else if (i != 7) {
							if (i == 8) {
								Class218.method2042(i_6_, i_8_, i_9_, i_7_, 4, i_3_, i_5_ + 4, true, i_11_);
								Class218.method2042(i_6_, i_8_, i_9_, i_7_, 4, i_3_, (i_5_ + 2 & 0x3) + 4, true, i_11_);
							}
						} else
							Class218.method2042(i_6_, i_8_, i_9_, i_7_, 4, i_3_, (i_5_ + 2 & 0x3) + 4, true, i_11_);
					} else
						((Class338_Sub3_Sub3_Sub2) interface14).aClass380_6626.method3981(i_9_, (byte) -75);
				}
			} else if (i_6_ != 2) {
				if (i_6_ == 3) {
					Interface14 interface14 = ((Interface14) Class296_Sub15_Sub3.method2540(i_7_, i_8_, i_3_));
					if (interface14 != null) {
						if (!(interface14 instanceof Class338_Sub3_Sub5_Sub1))
							Class218.method2042(i_6_, i_8_, i_9_, i_7_, i, i_3_, i_5_, true, interface14.method57((byte) 94));
						else
							((Class338_Sub3_Sub5_Sub1) interface14).aClass380_6650.method3981(i_9_, (byte) -119);
					}
				}
			} else {
				Interface14 interface14 = ((Interface14) (Class123_Sub2.method1070(i_7_, i_8_, i_3_, Interface14.class)));
				if (interface14 != null) {
					if (i == 11)
						i = 10;
					if (!(interface14 instanceof Class338_Sub3_Sub1_Sub4))
						Class218.method2042(i_6_, i_8_, i_9_, i_7_, i, i_3_, i_5_, true, interface14.method57((byte) 92));
					else
						((Class338_Sub3_Sub1_Sub4) interface14).aClass380_6640.method3981(i_9_, (byte) -97);
				}
			}
		}
		if (i_4_ >= -88)
			anInt2786 = -24;
	}
}
