package net.zaros.client;

import net.zaros.client.configs.objtype.ObjCustomisation;
import net.zaros.client.configs.objtype.ObjTypeList;

public class PlayerEquipment {
	private int renderEmote;
	private long aLong3369;
	public static boolean aBoolean3370 = false;
	public int[] body_colours;
	private ObjCustomisation[] recolourData;
	private long hash;
	private int[] body_data;
	public static int anInt3375 = -1;
	public static Class209 aClass209_3376 = new Class209(31);
	public boolean gender;
	public int morphismId = -1;

	public static void method4145(int i) {
		aClass209_3376 = null;
		if (i != 19001) {
			aClass209_3376 = null;
		}
	}

	private final void recalculateHash() {
		hash = -1L;
		long[] hashTable = TextureOperation.someHashTable;
		hash = hash >>> 8 ^ hashTable[(int) ((renderEmote >> 8 ^ hash) & 0xffL)];
		hash = hashTable[(int) ((hash ^ renderEmote) & 0xffL)] ^ hash >>> 8;
		for (int element : body_data) {
			hash = hashTable[(int) ((element >> 24 ^ hash) & 0xffL)] ^ hash >>> 8;
			hash = hashTable[(int) ((element >> 16 ^ hash) & 0xffL)] ^ hash >>> 8;
			hash = hash >>> 8 ^ hashTable[(int) ((element >> 8 ^ hash) & 0xffL)];
			hash = hash >>> 8 ^ hashTable[(int) ((hash ^ element) & 0xffL)];
		}
		if (recolourData != null) {
			for (ObjCustomisation element : recolourData) {
				if (element != null) {
					int[] is;
					int[] is_2_;
					if (!gender) {
						is = element.maleEquipModel;
						is_2_ = element.maleHeadModels;
					} else {
						is = element.femaleEquipModel;
						is_2_ = element.femaleHeadModels;
					}
					if (is != null) {
						for (int element2 : is) {
							hash = hashTable[(int) ((element2 >> 8 ^ hash) & 0xffL)] ^ hash >>> 8;
							hash = hashTable[(int) ((element2 ^ hash) & 0xffL)] ^ hash >>> 8;
						}
					}
					if (is_2_ != null) {
						for (int element2 : is_2_) {
							hash = hashTable[(int) ((hash ^ element2 >> 8) & 0xffL)] ^ hash >>> 8;
							hash = hash >>> 8 ^ hashTable[(int) ((element2 ^ hash) & 0xffL)];
						}
					}
					if (element.recolourDst != null) {
						for (short element2 : element.recolourDst) {
							hash = hash >>> 8 ^ hashTable[(int) ((element2 >> 8 ^ hash) & 0xffL)];
							hash = hashTable[(int) ((element2 ^ hash) & 0xffL)] ^ hash >>> 8;
						}
					}
					if (element.retextureDst != null) {
						for (short element2 : element.retextureDst) {
							hash = hash >>> 8 ^ hashTable[(int) ((hash ^ element2 >> 8) & 0xffL)];
							hash = hashTable[(int) ((element2 ^ hash) & 0xffL)] ^ hash >>> 8;
						}
					}
				}
			}
		}
		for (int i = 0; i < 10; i++) {
			hash = hash >>> 8 ^ hashTable[(int) ((body_colours[i] ^ hash) & 0xffL)];
		}
		hash = hashTable[(int) ((hash ^ (gender ? 1 : 0)) & 0xffL)] ^ hash >>> 8;
	}

	public void setBodyColor(int colorType, int value) {
		body_colours[colorType] = value;
		recalculateHash();
	}

	public void setGender(boolean female) {
		gender = female;
		recalculateHash();
	}

	public Model method4149(ClotchesLoader class24, int i, int i_12_, int i_13_, int i_14_, Animator class44, AnimationsLoader class310, ha var_ha, byte i_15_) {
		int i_16_ = class44 != null ? class44.method568(0) | i : i;
		long l = (long) i_14_ << 32 | i_12_ << 16 | i_13_;
		Model class178;
		synchronized (ParticleEmitterRaw.aClass113_1725) {
			class178 = (Model) ParticleEmitterRaw.aClass113_1725.get(l);
		}
		if (class178 == null || var_ha.e(class178.ua(), i_16_) != 0) {
			if (class178 != null) {
				i_16_ = var_ha.d(i_16_, class178.ua());
			}
			int i_17_ = i_16_;
			Mesh[] class132s = new Mesh[3];
			int i_18_ = 0;
			if (class24.getClothDefinition(i_13_).method3657(2) && class24.getClothDefinition(i_12_).method3657(2) && class24.getClothDefinition(i_14_).method3657(2)) {
				Mesh class132 = class24.getClothDefinition(i_13_).method3658((byte) 73);
				if (class132 != null) {
					class132s[i_18_++] = class132;
				}
				class132 = class24.getClothDefinition(i_12_).method3658((byte) -53);
				if (class132 != null) {
					class132s[i_18_++] = class132;
				}
				class132 = class24.getClothDefinition(i_14_).method3658((byte) -112);
				if (class132 != null) {
					class132s[i_18_++] = class132;
				}
				class132 = new Mesh(class132s, i_18_);
				i_17_ |= 0x4000;
				class178 = var_ha.a(class132, i_17_, Class296_Sub39_Sub18.anInt6247, 64, 768);
				for (int i_19_ = 0; i_19_ < 10; i_19_++) {
					for (int i_20_ = 0; i_20_ < CS2Call.aShortArrayArray4971[i_19_].length; i_20_++) {
						if (Class42_Sub4.aShortArrayArrayArray3846[i_19_][i_20_].length > body_colours[i_19_]) {
							class178.ia(CS2Call.aShortArrayArray4971[i_19_][i_20_], Class42_Sub4.aShortArrayArrayArray3846[i_19_][i_20_][body_colours[i_19_]]);
						}
					}
				}
				class178.s(i_16_);
				synchronized (ParticleEmitterRaw.aClass113_1725) {
					ParticleEmitterRaw.aClass113_1725.put(class178, l);
				}
			} else {
				return null;
			}
		}
		if (class44 == null) {
			return class178;
		}
		if (i_15_ != 34) {
			renderEmote = 111;
		}
		class178 = class178.method1728((byte) 4, i_16_, true);
		class44.method556(class178, 0, (byte) -63);
		return class178;
	}

	final Model method4150(AnimationsLoader class310, ObjTypeList class129, ha var_ha, NPCDefinitionLoader class136, int i, Animator class44, ClotchesLoader class24, IConfigsRegister interface17, int i_21_) {
		if (morphismId != -1) {
			return class136.getDefinition(morphismId).method1504(var_ha, class44, null, i, interface17, i_21_ + 3790);
		}
		int i_22_ = class44 == null ? i : i | class44.method568(0);
		Model class178;
		synchronized (ParticleEmitterRaw.aClass113_1725) {
			class178 = (Model) ParticleEmitterRaw.aClass113_1725.get(hash);
		}
		if (class178 == null || var_ha.e(class178.ua(), i_22_) != 0) {
			if (class178 != null) {
				i_22_ = var_ha.d(i_22_, class178.ua());
			}
			int i_23_ = i_22_;
			boolean bool = false;
			for (int i_24_ = 0; i_24_ < body_data.length; i_24_++) {
				int i_25_ = body_data[i_24_];
				ObjCustomisation class26 = null;
				if ((i_25_ & 0x40000000) == 0) {
					if ((i_25_ & ~0x7fffffff) != 0 && !class24.getClothDefinition(i_25_ & 0x3fffffff).method3657(2)) {
						bool = true;
					}
				} else {
					if (recolourData != null && recolourData[i_24_] != null) {
						class26 = recolourData[i_24_];
					}
					if (!class129.list(i_25_ & 0x3fffffff).isHeadMeshReady(gender, class26)) {
						bool = true;
					}
				}
			}
			if (bool) {
				return null;
			}
			Mesh[] class132s = new Mesh[body_data.length];
			int i_26_ = 0;
			for (int i_27_ = 0; body_data.length > i_27_; i_27_++) {
				int i_28_ = body_data[i_27_];
				ObjCustomisation class26 = null;
				if ((i_28_ & 0x40000000) != 0) {
					if (recolourData != null && recolourData[i_27_] != null) {
						class26 = recolourData[i_27_];
					}
					Mesh class132 = class129.list(i_28_ & 0x3fffffff).getHeadMesh(gender, class26);
					if (class132 != null) {
						class132s[i_26_++] = class132;
					}
				} else if ((i_28_ & ~0x7fffffff) != 0) {
					Mesh class132 = class24.getClothDefinition(i_28_ & 0x3fffffff).method3658((byte) 115);
					if (class132 != null) {
						class132s[i_26_++] = class132;
					}
				}
			}
			i_23_ |= 0x4000;
			Mesh class132 = new Mesh(class132s, i_26_);
			class178 = var_ha.a(class132, i_23_, Class296_Sub39_Sub18.anInt6247, 64, 768);
			for (int i_29_ = 0; i_29_ < 10; i_29_++) {
				for (int i_30_ = 0; CS2Call.aShortArrayArray4971[i_29_].length > i_30_; i_30_++) {
					if (body_colours[i_29_] < Class42_Sub4.aShortArrayArrayArray3846[i_29_][i_30_].length) {
						class178.ia(CS2Call.aShortArrayArray4971[i_29_][i_30_], Class42_Sub4.aShortArrayArrayArray3846[i_29_][i_30_][body_colours[i_29_]]);
					}
				}
			}
			class178.s(i_22_);
			synchronized (ParticleEmitterRaw.aClass113_1725) {
				ParticleEmitterRaw.aClass113_1725.put(class178, hash);
			}
		}
		if (class44 == null) {
			return class178;
		}
		if (i_21_ != -3790) {
			recolourData = null;
		}
		Model class178_31_ = class178.method1728((byte) 4, i_22_, true);
		class44.method556(class178_31_, 0, (byte) -63);
		return class178_31_;
	}

	final void setBodyAccessoryOnBody(ClotchesLoader cLoader, int clothesType, int clothesID) {
		int bodyType = Class296_Sub51_Sub30.bodyTypes[clothesType];
		if (cLoader.getClothDefinition(clothesID) != null) {
			body_data[bodyType] = clothesID | -2147483648;
			recalculateHash();
		}
	}

	final void setItemOnBody(int bodyType, int itemID, ObjTypeList itemdefLoader) {
		if (itemID == -1) {
			body_data[bodyType] = 0;
		} else if (itemdefLoader.list(itemID) != null) {
			body_data[bodyType] = itemID | 0x40000000;
			recalculateHash();
		}
	}

	final Model method4153(IConfigsRegister cfgRegister, ObjTypeList itemDefLoader, boolean bool, Animator class44, ClotchesLoader class24, NPCDefinitionLoader class136, Animator[] class44s, int i, ha var_ha, int i_37_, int[] is, EquipmentData equData, int i_38_, Animator class44_39_, AnimationsLoader animsLoader, Class62 class62) {
		if (morphismId != -1) {
			return class136.getDefinition(morphismId).method1496(is, i_38_, var_ha, i_37_ - 30956, class44s, i, cfgRegister, class44_39_, class62, null, class44);
		}
		int i_40_ = i_38_;
		long l = hash;
		int[] equipmentData = body_data;
		boolean bool_42_ = false;
		boolean bool_43_ = false;
		if (class44_39_ != null) {
			Animation anim = class44_39_.method563(-1);
			if (anim != null && (anim.shieldDisplayed >= 0 || anim.weaponDisplayed >= 0)) {
				equipmentData = new int[body_data.length];
				for (int i_44_ = 0; i_44_ < equipmentData.length; i_44_++) {
					equipmentData[i_44_] = body_data[i_44_];
				}
				if (anim.shieldDisplayed >= 0 && equData.somethingWithShieldDisplay != -1) {
					if (anim.shieldDisplayed != 65535) {
						equipmentData[equData.somethingWithShieldDisplay] = anim.shieldDisplayed | 0x40000000;
						for (int i_45_ = 0; i_45_ < equData.somethingWithShield.length; i_45_++) {
							equipmentData[equData.somethingWithShield[i_45_]] = 0;
						}
						l ^= (long) equipmentData[equData.somethingWithShieldDisplay] << 32;
					} else {
						equipmentData[equData.somethingWithShieldDisplay] = 0;
						l ^= ~0xffffffffL;
						for (int i_46_ = 0; equData.somethingWithShield.length > i_46_; i_46_++) {
							equipmentData[equData.somethingWithShield[i_46_]] = 0;
						}
					}
					bool_42_ = true;
				}
				if (anim.weaponDisplayed >= 0 && equData.somethingWithWeaponDisplay != -1) {
					if (anim.weaponDisplayed != 65535) {
						equipmentData[equData.somethingWithWeaponDisplay] = anim.weaponDisplayed | 0x40000000;
						for (int i_47_ = 0; i_47_ < equData.somethingWithWeapon.length; i_47_++) {
							equipmentData[equData.somethingWithWeapon[i_47_]] = 0;
						}
						l ^= equipmentData[equData.somethingWithWeaponDisplay];
					} else {
						equipmentData[equData.somethingWithWeaponDisplay] = 0;
						for (int i_48_ = 0; equData.somethingWithWeapon.length > i_48_; i_48_++) {
							equipmentData[equData.somethingWithWeapon[i_48_]] = 0;
						}
						l ^= 0xffffffffL;
					}
					bool_43_ = true;
				}
			}
		}
		boolean bool_49_ = false;
		if (i_37_ != 30812) {
			return null;
		}
		int i_50_ = class44s != null ? class44s.length : 0;
		for (int i_51_ = 0; i_51_ < i_50_; i_51_++) {
			if (class44s[i_51_] != null) {
				i_40_ |= class44s[i_51_].method568(0);
				bool_49_ = true;
			}
		}
		if (class44_39_ != null) {
			bool_49_ = true;
			i_40_ |= class44_39_.method568(0);
		}
		if (class44 != null) {
			bool_49_ = true;
			i_40_ |= class44.method568(i_37_ - 30812);
		}
		boolean bool_52_ = false;
		if (is != null) {
			for (int element : is) {
				if (element != -1) {
					i_40_ |= 0x20;
					bool_52_ = true;
				}
			}
		}
		Model class178;
		synchronized (Class258.aClass113_2415) {
			class178 = (Model) Class258.aClass113_2415.get(l);
		}
		Class280 class280 = null;
		if (renderEmote != -1) {
			class280 = class62.method700(renderEmote, 0);
		}
		if (class178 == null || var_ha.e(class178.ua(), i_40_) != 0) {
			if (class178 != null) {
				i_40_ = var_ha.d(i_40_, class178.ua());
			}
			int i_54_ = i_40_;
			boolean bool_55_ = false;
			for (int i_56_ = 0; i_56_ < equipmentData.length; i_56_++) {
				int i_57_ = equipmentData[i_56_];
				ObjCustomisation customisation = null;
				boolean bool_58_ = false;
				if (bool_42_) {
					if (equData.somethingWithShieldDisplay != i_56_) {
						for (int element : equData.somethingWithShield) {
							if (element == i_56_) {
								bool_58_ = true;
								break;
							}
						}
					} else {
						bool_58_ = true;
					}
				}
				if (bool_43_) {
					if (equData.somethingWithWeaponDisplay == i_56_) {
						bool_58_ = true;
					} else {
						for (int element : equData.somethingWithWeapon) {
							if (i_56_ == element) {
								bool_58_ = true;
								break;
							}
						}
					}
				}
				if ((i_57_ & 0x40000000) != 0) {
					if (!bool_58_ && recolourData != null && recolourData[i_56_] != null) {
						customisation = recolourData[i_56_];
					}
					if (!itemDefLoader.list(i_57_ & 0x3fffffff).checkCustomize(gender, customisation)) {
						bool_55_ = true;
					}
				} else if ((i_57_ & ~0x7fffffff) != 0 && !class24.getClothDefinition(i_57_ & 0x3fffffff).method3660((byte) -128)) {
					bool_55_ = true;
				}
			}
			if (!bool_55_) {
				Mesh[] class132s = new Mesh[equipmentData.length];
				for (int i_61_ = 0; equipmentData.length > i_61_; i_61_++) {
					int i_62_ = equipmentData[i_61_];
					ObjCustomisation class26 = null;
					boolean bool_63_ = i_61_ == 5 && bool_42_ || i_61_ == 3 && bool_43_;
					if ((i_62_ & 0x40000000) != 0) {
						if (!bool_63_ && recolourData != null && recolourData[i_61_] != null) {
							class26 = recolourData[i_61_];
						}
						Mesh class132 = itemDefLoader.list(i_62_ & 0x3fffffff).getEquipMesh(gender, class26);
						if (class132 != null) {
							class132s[i_61_] = class132;
						}
					} else if ((i_62_ & ~0x7fffffff) != 0) {
						Mesh class132 = class24.getClothDefinition(i_62_ & 0x3fffffff).method3656((byte) -122);
						if (class132 != null) {
							class132s[i_61_] = class132;
						}
					}
				}
				if (class280 != null && class280.anIntArrayArray2558 != null) {
					for (int i_64_ = 0; class280.anIntArrayArray2558.length > i_64_; i_64_++) {
						if (class132s[i_64_] != null) {
							int i_65_ = 0;
							int i_66_ = 0;
							int i_67_ = 0;
							int i_68_ = 0;
							int i_69_ = 0;
							int i_70_ = 0;
							if (class280.anIntArrayArray2558[i_64_] != null) {
								i_66_ = class280.anIntArrayArray2558[i_64_][1];
								i_70_ = class280.anIntArrayArray2558[i_64_][5] << 3;
								i_68_ = class280.anIntArrayArray2558[i_64_][3] << 3;
								i_67_ = class280.anIntArrayArray2558[i_64_][2];
								i_65_ = class280.anIntArrayArray2558[i_64_][0];
								i_69_ = class280.anIntArrayArray2558[i_64_][4] << 3;
							}
							if (i_68_ != 0 || i_69_ != 0 || i_70_ != 0) {
								class132s[i_64_].method1392(i_69_, i_70_, i_68_, (byte) 84);
							}
							if (i_65_ != 0 || i_66_ != 0 || i_67_ != 0) {
								class132s[i_64_].translate(i_65_, i_67_, i_66_);
							}
						}
					}
				}
				Mesh class132 = new Mesh(class132s, class132s.length);
				i_54_ |= 0x4000;
				class178 = var_ha.a(class132, i_54_, Class296_Sub39_Sub18.anInt6247, 64, 850);
				for (int i_71_ = 0; i_71_ < 10; i_71_++) {
					for (int i_72_ = 0; CS2Call.aShortArrayArray4971[i_71_].length > i_72_; i_72_++) {
						if (body_colours[i_71_] < Class42_Sub4.aShortArrayArrayArray3846[i_71_][i_72_].length) {
							class178.ia(CS2Call.aShortArrayArray4971[i_71_][i_72_], Class42_Sub4.aShortArrayArrayArray3846[i_71_][i_72_][body_colours[i_71_]]);
						}
					}
				}
				if (bool) {
					class178.s(i_40_);
					synchronized (Class258.aClass113_2415) {
						Class258.aClass113_2415.put(class178, l);
					}
					aLong3369 = l;
				}
			} else {
				if (aLong3369 != -1L) {
					synchronized (Class258.aClass113_2415) {
						class178 = (Model) Class258.aClass113_2415.get(aLong3369);
					}
				}
				if (class178 == null || var_ha.e(class178.ua(), i_40_) != 0) {
					return null;
				}
			}
		}
		Model class178_73_ = class178.method1728((byte) 4, i_40_, true);
		if (!bool_49_ && !bool_52_) {
			return class178_73_;
		}
		Class373[] class373s = null;
		if (class280 != null) {
			class373s = class280.method2344(var_ha, (byte) 41);
		}
		if (bool_52_ && class373s != null) {
			for (int i_74_ = 0; is.length > i_74_; i_74_++) {
				if (class373s[i_74_] != null) {
					class178_73_.method1726(class373s[i_74_], 1 << i_74_, true);
				}
			}
		}
		int i_75_ = 0;
		int i_76_ = 1;
		while (i_50_ > i_75_) {
			if (class44s[i_75_] != null) {
				class44s[i_75_].method551(0, i_76_, 24529, class178_73_);
			}
			i_75_++;
			i_76_ <<= 1;
		}
		if (bool_52_) {
			for (int i_77_ = 0; i_77_ < is.length; i_77_++) {
				if (is[i_77_] != -1) {
					int i_78_ = is[i_77_] - i;
					i_78_ &= 0x3fff;
					Class373 class373 = var_ha.m();
					class373.method3906(i_78_);
					class178_73_.method1726(class373, 1 << i_77_, false);
				}
			}
		}
		if (bool_52_ && class373s != null) {
			for (int i_79_ = 0; is.length > i_79_; i_79_++) {
				if (class373s[i_79_] != null) {
					class178_73_.method1726(class373s[i_79_], 1 << i_79_, false);
				}
			}
		}
		if (class44_39_ != null && class44 != null) {
			Class16_Sub2.method242(class44, i_37_ ^ ~0x7817, class178_73_, class44_39_);
		} else if (class44_39_ == null) {
			if (class44 != null) {
				class44.method556(class178_73_, 0, (byte) -63);
			}
		} else {
			class44_39_.method556(class178_73_, 0, (byte) -63);
		}
		return class178_73_;
	}

	final void update(int[] bodyData, int[] bodyColours, boolean fem, ObjCustomisation[] rec, int renderem, int npcid) {
		body_colours = bodyColours;
		gender = fem;
		recolourData = rec;
		if (renderem != renderEmote) {
			renderEmote = renderem;
		}
		morphismId = npcid;
		body_data = bodyData;
		recalculateHash();
	}

	public PlayerEquipment() {
		/* empty */
	}
}
