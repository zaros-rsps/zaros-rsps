package net.zaros.client;

/* Class338_Sub3_Sub1_Sub3_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class NPC extends Mobile {
	String name;
	int faceX;
	int faceY = -1;
	int combatLevel;
	int anInt6899;
	Class286 aClass286_6900;
	int anInt6901;
	NPCDefinition definition;

	final void method3460(int i, ha var_ha) {
		if (definition != null && (aBoolean6835 || method3535(0, var_ha, (byte) 96))) {
			Class373 class373 = var_ha.f();
			class373.method3906(aClass5_6804.method175((byte) -81));
			class373.method3904(tileX, anInt5213 - 20, tileY);
			this.method3500(aBoolean6835, aClass178Array6828, true, var_ha, class373);
			for (int i_0_ = 0; aClass178Array6828.length > i_0_; i_0_++)
				aClass178Array6828[i_0_] = null;
			int i_1_ = -75 % ((i + 41) / 62);
		}
	}

	final Class96 method3461(ha var_ha, int i) {
		int i_2_ = 14 / ((i - 79) / 44);
		return null;
	}

	static final void method3532(int i) {
		synchronized (Class258.aClass113_2415) {
			Class258.aClass113_2415.clear();
			if (i != 26852)
				return;
		}
		synchronized (ParticleEmitterRaw.aClass113_1725) {
			ParticleEmitterRaw.aClass113_1725.clear();
		}
	}

	final int method3513(byte i) {
		if (i > -25)
			return -47;
		if (definition.configData != null) {
			NPCDefinition class147 = definition.getNPCDefinitionByConfigID(Class16_Sub3_Sub1.configsRegister);
			if (class147 != null && class147.anInt1462 != -1)
				return class147.anInt1462;
		}
		if (definition.anInt1462 != -1)
			return definition.anInt1462;
		return super.method3513((byte) -122);
	}

	final void updatePosition(int i, boolean bool, int i_3_, int i_4_, int i_5_, int i_6_) {
		z = aByte5203 = (byte) i_5_;
		if (r_Sub2.method2871(i, i_6_, (byte) -47))
			aByte5203++;
		if (i_3_ <= 32)
			method3532(39);
		if (aClass44_6802.method570((byte) 40) && aClass44_6802.method563(-1).walkingProperties == 1) {
			anIntArray6789 = null;
			aClass44_6802.method549((byte) 115, -1);
		}
		for (int i_7_ = 0; aClass212Array6817.length > i_7_; i_7_++) {
			if (aClass212Array6817[i_7_].anInt2105 != -1) {
				Graphic class23 = Class157.graphicsLoader.getGraphic((aClass212Array6817[i_7_].anInt2105));
				if (class23.aBoolean267 && class23.anInt264 != -1 && (Class296_Sub51_Sub13.animationsLoader.getAnimation(class23.anInt264, (byte) -124).walkingProperties) == 1) {
					aClass212Array6817[i_7_].aClass44_2108.method549((byte) 115, -1);
					aClass212Array6817[i_7_].anInt2105 = -1;
				}
			}
		}
		if (!bool) {
			int i_8_ = -waypointQueueX[0] + i_6_;
			int i_9_ = i - wayPointQueueY[0];
			if (i_8_ >= -8 && i_8_ <= 8 && i_9_ >= -8 && i_9_ <= 8) {
				if (waypointQueueX.length - 1 > currentWayPoint)
					currentWayPoint++;
				for (int i_10_ = currentWayPoint; i_10_ > 0; i_10_--) {
					waypointQueueX[i_10_] = waypointQueueX[i_10_ - 1];
					wayPointQueueY[i_10_] = wayPointQueueY[i_10_ - 1];
					walkingTypes[i_10_] = walkingTypes[i_10_ - 1];
				}
				waypointQueueX[0] = i_6_;
				walkingTypes[0] = (byte) 1;
				wayPointQueueY[0] = i;
				return;
			}
		}
		waypointQueueX[0] = i_6_;
		currentWayPoint = 0;
		anInt6836 = 0;
		anInt6829 = 0;
		wayPointQueueY[0] = i;
		tileY = (wayPointQueueY[0] << 9) + (i_4_ << 8);
		tileX = (waypointQueueX[0] << 9) + (i_4_ << 8);
		if (aClass338_Sub1_6831 != null)
			aClass338_Sub1_6831.method3440();
	}

	final boolean hasDefinition() {
		if (definition == null)
			return false;
		return true;
	}

	final void method3472(byte i) {
		int i_11_ = 15 / ((i + 56) / 38);
		throw new IllegalStateException();
	}

	private final boolean method3535(int i, ha var_ha, byte i_12_) {
		int i_13_ = i;
		Class280 class280 = this.method3516(false);
		Animator class44 = ((!aClass44_6802.method570((byte) 40) || aClass44_6802.method567(1)) ? null : aClass44_6802);
		Animator class44_14_ = (aClass44_6777.method570((byte) 40) && (!aBoolean6783 || class44 == null) ? aClass44_6777 : null);
		int i_15_ = class280.anInt2585;
		int i_16_ = class280.anInt2584;
		if (i_15_ != 0 || i_16_ != 0 || class280.anInt2567 != 0 || class280.anInt2587 != 0)
			i |= 0x7;
		if (i_12_ != 96)
			name = null;
		boolean bool = (aByte6806 != 0 && Class29.anInt307 >= anInt6820 && Class29.anInt307 < anInt6803);
		if (bool)
			i |= 0x80000;
		int i_17_ = aClass5_6804.method175((byte) -81);
		Model class178 = (aClass178Array6828[0] = definition.method1496(anIntArray6823, i, var_ha, -144, aClass44_Sub1_Sub1Array6814, i_17_, Class16_Sub3_Sub1.configsRegister, class44, Class41_Sub10.aClass62_3768, aClass286_6900, class44_14_));
		if (class178 == null)
			return false;
		anInt6790 = class178.fa();
		anInt6800 = class178.ma();
		this.method3503(false, class178);
		if (i_15_ == 0 && i_16_ == 0)
			this.method3518(0, true, this.getSize() << 9, 0, this.getSize() << 9, i_17_);
		else {
			this.method3518(class280.anInt2580, true, i_16_, class280.anInt2556, i_15_, i_17_);
			if (anInt6801 != 0)
				aClass178Array6828[0].FA(anInt6801);
			if (anInt6769 != 0)
				aClass178Array6828[0].VA(anInt6769);
			if (anInt6795 != 0)
				aClass178Array6828[0].H(0, anInt6795, 0);
		}
		if (bool)
			class178.method1717(aByte6808, aByte6818, aByte6822, aByte6806 & 0xff);
		this.method3504(var_ha, i_15_, i_13_, i_16_, 28149, class280, i_17_);
		return true;
	}

	final boolean method3494(byte i) {
		int i_18_ = 72 % ((i - 35) / 33);
		return NPCDefinition.aClass268_1478.aBoolean2492;
	}

	final void method3536(String string, int i, int i_19_, int i_20_) {
		int i_21_ = (Class338_Sub3_Sub4.method3566((byte) 43) * NPCDefinition.aClass268_1478.anInt2499);
		this.method3507(i, string, -120, i_19_, i_21_);
		if (i_20_ < 116)
			aClass286_6900 = null;
	}

	final int method3505(int i) {
		if (i >= -85)
			definition = null;
		if (definition.configData != null) {
			NPCDefinition class147 = definition.getNPCDefinitionByConfigID(Class16_Sub3_Sub1.configsRegister);
			if (class147 != null && class147.anInt1507 != -1)
				return class147.anInt1507;
		}
		return definition.anInt1507;
	}

	private final boolean clickable() {
		return definition.isClickable;
	}

	final Class338_Sub2 method3473(ha var_ha, byte i) {
		if (definition == null || !method3535(2048, var_ha, (byte) 96))
			return null;
		if (i > -84)
			anInt6901 = -77;
		Class373 class373 = var_ha.f();
		int i_22_ = aClass5_6804.method175((byte) -81);
		class373.method3906(i_22_);
		Class247 class247 = (Class338_Sub2.aClass247ArrayArrayArray5195[z][tileX >> Class313.anInt2779][tileY >> Class313.anInt2779]);
		if (class247 == null || class247.aClass338_Sub3_Sub5_2346 == null)
			anInt6770 -= (float) anInt6770 / 10.0F;
		else {
			int i_23_ = anInt6770 - class247.aClass338_Sub3_Sub5_2346.aShort6573;
			anInt6770 -= (float) i_23_ / 10.0F;
		}
		class373.method3904(tileX, -anInt6770 - 20 + anInt5213, tileY);
		Class280 class280 = this.method3516(false);
		NPCDefinition class147 = (definition.configData != null ? definition.getNPCDefinitionByConfigID(Class16_Sub3_Sub1.configsRegister) : definition);
		aBoolean6834 = false;
		Class338_Sub2 class338_sub2 = null;
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub16_5019.method453(122) == 1 && class147.aBoolean1457 && class280.aBoolean2591) {
			Animator class44 = ((!aClass44_6802.method570((byte) 40) || !aClass44_6802.method567(1)) ? null : aClass44_6802);
			Animator class44_24_ = ((!aClass44_6777.method570((byte) 40) || aBoolean6783 && class44 != null) ? null : aClass44_6777);
			Model class178 = Class382.method4007(var_ha, anInt6795, anInt6769, definition.aShort1490 & 0xffff, definition.size, anInt6801, (byte) 114, (class44_24_ != null ? class44_24_ : class44), aClass178Array6828[0], definition.aByte1445 & 0xff, i_22_, definition.aShort1495 & 0xffff, definition.aByte1447 & 0xff);
			if (class178 != null) {
				class338_sub2 = Class144.method1481(aClass178Array6828.length + 1, true, clickable());
				aBoolean6834 = true;
				var_ha.C(false);
				if (Class296_Sub39_Sub10.aBoolean6177)
					class178.method1719(class373, (class338_sub2.aClass338_Sub5Array5194[aClass178Array6828.length]), ModeWhat.anInt1192, 0);
				else
					class178.method1715(class373, (class338_sub2.aClass338_Sub5Array5194[aClass178Array6828.length]), 0);
				var_ha.C(true);
			}
		}
		class373.method3906(i_22_);
		class373.method3904(tileX, -anInt6770 + (anInt5213 - 5), tileY);
		if (class338_sub2 == null)
			class338_sub2 = Class144.method1481(aClass178Array6828.length, true, clickable());
		this.method3500(false, aClass178Array6828, true, var_ha, class373);
		if (!Class296_Sub39_Sub10.aBoolean6177) {
			for (int i_25_ = 0; i_25_ < aClass178Array6828.length; i_25_++) {
				if (aClass178Array6828[i_25_] != null)
					aClass178Array6828[i_25_].method1715(class373, class338_sub2.aClass338_Sub5Array5194[i_25_], 0);
			}
		} else {
			for (int i_26_ = 0; i_26_ < aClass178Array6828.length; i_26_++) {
				if (aClass178Array6828[i_26_] != null)
					aClass178Array6828[i_26_].method1719(class373, class338_sub2.aClass338_Sub5Array5194[i_26_], ModeWhat.anInt1192, 0);
			}
		}
		if (aClass338_Sub1_6831 != null) {
			Class390 class390 = aClass338_Sub1_6831.method3444();
			if (Class296_Sub39_Sub10.aBoolean6177)
				var_ha.a(class390, ModeWhat.anInt1192);
			else
				var_ha.a(class390);
		}
		for (int i_27_ = 0; aClass178Array6828.length > i_27_; i_27_++) {
			if (aClass178Array6828[i_27_] != null)
				aBoolean6834 |= aClass178Array6828[i_27_].F();
			aClass178Array6828[i_27_] = null;
		}
		anInt6781 = Class296_Sub2.anInt4590;
		return class338_sub2;
	}

	final void walk(int type, int diff) {
		int x = waypointQueueX[0];
		int y = wayPointQueueY[0];
		if (diff == 0)
			y++;
		if (diff == 1) {
			y++;
			x++;
		}
		if (diff == 2)
			x++;
		if (diff == 3) {
			y--;
			x++;
		}
		if (diff == 4)
			y--;
		if (diff == 5) {
			x--;
			y--;
		}
		if (diff == 6)
			x--;
		if (diff == 7) {
			y++;
			x--;
		}
		if (aClass44_6802.method570((byte) 40) && aClass44_6802.method563(-1).walkingProperties == 1) {
			anIntArray6789 = null;
			aClass44_6802.method549((byte) 115, -1);
		}
		for (int i_32_ = 0; i_32_ < aClass212Array6817.length; i_32_++) {
			if (aClass212Array6817[i_32_].anInt2105 != -1) {
				Graphic class23 = Class157.graphicsLoader.getGraphic(aClass212Array6817[i_32_].anInt2105);
				if (class23.aBoolean267 && class23.anInt264 != -1 && (Class296_Sub51_Sub13.animationsLoader.getAnimation(class23.anInt264, (byte) -124).walkingProperties) == 1) {
					aClass212Array6817[i_32_].aClass44_2108.method549((byte) 115, -1);
					aClass212Array6817[i_32_].anInt2105 = -1;
				}
			}
		}
		if (currentWayPoint < waypointQueueX.length - 1)
			currentWayPoint++;
		for (int i = currentWayPoint; i > 0; i--) {
			waypointQueueX[i] = waypointQueueX[i - 1];
			wayPointQueueY[i] = wayPointQueueY[i - 1];
			walkingTypes[i] = walkingTypes[i - 1];
		}
		waypointQueueX[0] = x;
		walkingTypes[0] = (byte) type;
		wayPointQueueY[0] = y;
	}

	public NPC() {
		faceX = -1;
		anInt6899 = 1;
		anInt6901 = 1;
	}

	final int getRenderEmote() {
		if (definition.configData != null) {
			NPCDefinition def = definition.getNPCDefinitionByConfigID(Class16_Sub3_Sub1.configsRegister);
			if (def != null && def.renderEmote != -1)
				return def.renderEmote;
		}
		return definition.renderEmote;
	}

	static final void method3539(byte i) {
		if (i < 73)
			method3539((byte) 101);
		if (Js5.aClass278_1418 != null) {
			if (Js5.aClass278_1418.anInt2540 == 1)
				Js5.aClass278_1418 = null;
			else if (Js5.aClass278_1418.anInt2540 == 2) {
				Class247.method2190(1, 2, Class304.aClass398_2735, Class359.aString3091);
				Js5.aClass278_1418 = null;
			}
		}
	}

	final boolean method3468(int i) {
		if (i >= -29)
			method3536(null, -75, -75, -95);
		return false;
	}

	final Class79 method3497(int i) {
		if (i <= 4)
			method3468(68);
		if (aClass79_6796 != null && aClass79_6796.aString886 == null)
			return null;
		return aClass79_6796;
	}

	NPC(int i) {
		super(i);
		faceX = -1;
		anInt6899 = 1;
		anInt6901 = 1;
	}

	final void setDefinition(NPCDefinition def) {
		if (definition != def && Class318.aBoolean2814 && Class296_Sub38.method2774((byte) 100, index))
			Class41_Sub9.method427((byte) -110);
		definition = def;
		if (definition != null) {
			combatLevel = definition.combatLevel;
			name = definition.name;
		}
		if (aClass338_Sub1_6831 != null)
			aClass338_Sub1_6831.method3440();
	}

	final void method3467(int i, int i_34_, Class338_Sub3 class338_sub3, boolean bool, int i_35_, int i_36_, ha var_ha) {
		int i_37_ = 85 % ((20 - i_36_) / 48);
		throw new IllegalStateException();
	}

	final int method3458(int i) {
		if (i != 0)
			return -6;
		if (definition == null)
			return 0;
		return definition.anInt1497;
	}

	final boolean method3475(int i, int i_38_, ha var_ha, int i_39_) {
		if (definition == null || !method3535(131072, var_ha, (byte) 96))
			return false;
		if (i_38_ > -48)
			return false;
		Class373 class373 = var_ha.f();
		int i_40_ = aClass5_6804.method175((byte) -81);
		class373.method3906(i_40_);
		class373.method3904(tileX, anInt5213, tileY);
		boolean bool = false;
		for (int i_41_ = 0; aClass178Array6828.length > i_41_; i_41_++) {
			if (aClass178Array6828[i_41_] != null) {
				boolean bool_42_ = (definition.anInt1497 > 0 || (definition.anInt1454 == -1 ? definition.size == 1 : definition.anInt1454 == 1));
				boolean bool_43_;
				if (Class296_Sub39_Sub10.aBoolean6177)
					bool_43_ = (aClass178Array6828[i_41_].method1731(i_39_, i, class373, bool_42_, definition.anInt1497, ModeWhat.anInt1192));
				else
					bool_43_ = aClass178Array6828[i_41_].method1732(i_39_, i, class373, bool_42_, (definition.anInt1497));
				if (bool_43_) {
					bool = true;
					break;
				}
			}
		}
		for (int i_44_ = 0; i_44_ < aClass178Array6828.length; i_44_++)
			aClass178Array6828[i_44_] = null;
		return bool;
	}
}
