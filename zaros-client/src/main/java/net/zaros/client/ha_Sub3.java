package net.zaros.client;

/* ha_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;

import jaclib.memory.Buffer;
import jaclib.memory.Stream;
import jaclib.memory.heap.NativeHeap;
import jaggl.OpenGL;

public class ha_Sub3 extends ha {
	static IncomingPacket aClass231_4112 = new IncomingPacket(87, 8);
	int anInt4113;
	int anInt4114;
	static int anInt4115;
	private Hashtable aHashtable4116 = new Hashtable();
	private int anInt4117;
	static OutgoingPacket aClass311_4118 = new OutgoingPacket(1, 9);
	private long aLong4119;
	private long aLong4120;
	private int anInt4121;
	private Canvas aCanvas4122;
	private OpenGL anOpenGL4123;
	private Canvas aCanvas4124;
	private Class49 aClass49_4125;
	private Class197 aClass197_4126;
	private Class342 aClass342_4127;
	int anInt4128;
	private Class173 aClass173_4129;
	private Class296_Sub9_Sub1 aClass296_Sub9_Sub1_4130;
	int anInt4131 = 128;
	private Class373_Sub1 aClass373_Sub1_4132;
	Class373_Sub1 aClass373_Sub1_4133;
	int anInt4134;
	private boolean aBoolean4135;
	private NodeDeque aClass155_4136;
	Class91 aClass91_4137;
	NativeHeap aNativeHeap4138;
	int anInt4139;
	int anInt4140;
	private int anInt4141;
	private int anInt4142;
	private Interface4[] anInterface4Array4143;
	private Interface4 anInterface4_4144;
	private Interface4 anInterface4_4145;
	private Class392 aClass392_4146;
	private int anInt4147;
	private Interface4[] anInterface4Array4148;
	private Interface4[] anInterface4Array4149;
	private Class397_Sub3 aClass397_Sub3_4150;
	int anInt4151;
	int anInt4152;
	private NodeDeque aClass155_4153;
	private int anInt4154;
	private NodeDeque aClass155_4155;
	private NodeDeque aClass155_4156;
	private NodeDeque aClass155_4157;
	private NodeDeque aClass155_4158;
	private NodeDeque aClass155_4159;
	private NodeDeque aClass155_4160;
	private boolean aBoolean4161;
	private int anInt4162;
	private int anInt4163;
	private int anInt4164;
	private long aLong4165;
	private boolean aBoolean4166;
	private boolean aBoolean4167;
	Class373_Sub1 aClass373_Sub1_4168;
	private boolean aBoolean4169;
	Class373_Sub1 aClass373_Sub1_4170;
	Class373_Sub1 aClass373_Sub1_4171;
	private float aFloat4172;
	int anInt4173;
	boolean aBoolean4174;
	boolean aBoolean4175;
	private int anInt4176;
	private boolean aBoolean4177;
	int anInt4178;
	int anInt4179;
	private float aFloat4180;
	int anInt4181;
	private float aFloat4182;
	private int anInt4183;
	boolean aBoolean4184;
	private int anInt4185;
	float aFloat4186;
	private boolean aBoolean4187;
	private String aString4188;
	boolean aBoolean4189;
	boolean aBoolean4190;
	private int anInt4191;
	private int anInt4192;
	float aFloat4193;
	Class296_Sub17_Sub2 aClass296_Sub17_Sub2_4194;
	Class69_Sub1 aClass69_Sub1_4195;
	private float aFloat4196;
	private float[] aFloatArray4197;
	float aFloat4198;
	private float[] aFloatArray4199;
	int anInt4200;
	private int anInt4201;
	float[] aFloatArray4202;
	private Interface20 anInterface20_4203;
	private Interface5 anInterface5_4204;
	boolean underwater_mode;
	private Class241_Sub2 aClass241_Sub2_4206;
	private boolean aBoolean4207;
	private boolean aBoolean4208;
	private boolean aBoolean4209;
	float aFloat4210;
	int anInt4211;
	int anInt4212;
	boolean aBoolean4213;
	Class178_Sub2[] aClass178_Sub2Array4214;
	int anInt4215;
	float aFloat4216;
	float aFloat4217;
	private Class69[] aClass69Array4218;
	boolean aBoolean4219;
	private Class296_Sub35[] aClass296_Sub35Array4220;
	private int anInt4221;
	Class178_Sub2[] aClass178_Sub2Array4222;
	int anInt4223;
	int anInt4224;
	boolean aBoolean4225;
	float aFloat4226;
	int anInt4227;
	boolean aBoolean4228;
	boolean aBoolean4229;
	private boolean aBoolean4230;
	int anInt4231;
	private boolean aBoolean4232;
	Class272 aClass272_4233;
	private float aFloat4234;
	private int anInt4235;
	private boolean aBoolean4236;
	float aFloat4237;
	private int anInt4238;
	private boolean aBoolean4239;
	boolean aBoolean4240;
	private int anInt4241;
	float aFloat4242;
	private boolean aBoolean4243;
	private int anInt4244;
	private Class69_Sub1_Sub1 aClass69_Sub1_Sub1_4245;
	int anInt4246;
	private boolean aBoolean4247;
	float aFloat4248;
	int anInt4249;
	int anInt4250;
	boolean aBoolean4251;
	private boolean aBoolean4252;
	private int anInt4253;
	private int anInt4254;
	private String aString4255;
	float aFloat4256;
	boolean aBoolean4257;
	private int anInt4258;
	private float aFloat4259;
	Class392 aClass392_4260;
	float aFloat4261;
	private float aFloat4262;
	int anInt4263;
	private int anInt4264;
	private int anInt4265;
	private int anInt4266;
	private float[] aFloatArray4267;
	boolean aBoolean4268;
	private float aFloat4269;
	Class272 aClass272_4270;
	private int anInt4271;
	float aFloat4272;
	private float[] aFloatArray4273;
	private Interface5 anInterface5_4274;
	int[] anIntArray4275;
	private int anInt4276;
	int[] anIntArray4277;
	byte[] aByteArray4278;
	private int anInt4279;
	int[] anIntArray4280;

	@Override
	public Class373 m() {
		return new Class373_Sub1();
	}

	@Override
	public void i(int i) {
		method1332((byte) 111);
	}

	private void method1271(byte i) {
		if (anInt4192 != 3) {
			anInt4192 = 3;
			method1293(true);
			method1352(3008);
			anInt4162 &= ~0x7;
		}
		if (i <= 38) {
			method1302(-117);
		}
	}

	public void method1272(byte i, int i_0_) {
		if (i != -107) {
			anInterface4Array4148 = null;
		}
		if (i_0_ == 1) {
			method1306(7681, 7681, -22394);
		} else if (i_0_ != 0) {
			if (i_0_ == 2) {
				method1306(34165, 7681, -22394);
			} else if (i_0_ != 3) {
				if (i_0_ == 4) {
					method1306(34023, 34023, -22394);
				}
			} else {
				method1306(260, 8448, -22394);
			}
		} else {
			method1306(8448, 8448, -22394);
		}
	}

	public synchronized void method1273(boolean bool, int i) {
		if (bool == true) {
			IntegerNode class296_sub16 = new IntegerNode(i);
			aClass155_4157.addLast((byte) 110, class296_sub16);
		}
	}

	@Override
	public void b(boolean bool) {
		/* empty */
	}

	@Override
	public Model a(Mesh class132, int i, int i_1_, int i_2_, int i_3_) {
		return new Class178_Sub2(this, class132, i, i_2_, i_3_, i_1_);
	}

	public static void method1274(byte i) {
		aClass311_4118 = null;
		aClass231_4112 = null;
		int i_4_ = -10 % ((40 - i) / 49);
	}

	@Override
	public Interface19 b(int i, int i_5_) {
		return null;
	}

	public void method1275(float f, byte i, float f_6_, float f_7_, float f_8_) {
		Packet.aFloatArray4675[0] = f_6_;
		Packet.aFloatArray4675[2] = f_8_;
		Packet.aFloatArray4675[1] = f;
		Packet.aFloatArray4675[3] = f_7_;
		if (i < 76) {
			a(-1, -20, true);
		}
		OpenGL.glTexEnvfv(8960, 8705, Packet.aFloatArray4675, 0);
	}

	public void method1276(Class373_Sub1 class373_sub1, byte i) {
		OpenGL.glPushMatrix();
		OpenGL.glMultMatrixf(class373_sub1.method3923(true), 0);
		if (i > -48) {
			aBoolean4228 = false;
		}
	}

	@Override
	public boolean p() {
		return false;
	}

	private void method1277(boolean bool) {
		if (anInt4162 != 1) {
			method1341(!bool);
			method1311(false, 8577);
			method1278(1553921480, false);
			method1345(false, 91);
			method1285(79, false);
			method1316(null, (byte) -106);
			method1340(-2, (byte) 61);
			method1272((byte) -107, 1);
			anInt4162 = 1;
		}
		if (bool) {
			anInt4176 = -51;
		}
	}

	@Override
	public void a(Canvas canvas, int i, int i_9_) {
		long l = 0L;
		if (canvas == null || aCanvas4122 == canvas) {
			l = aLong4120;
		} else if (aHashtable4116.containsKey(canvas)) {
			Long var_long = (Long) aHashtable4116.get(canvas);
			l = var_long.longValue();
		}
		if (l == 0L) {
			throw new RuntimeException();
		}
		anOpenGL4123.surfaceResized(l);
		if (canvas == aCanvas4124) {
			method1343((byte) -98);
		}
	}

	@Override
	public void a(int i, Class296_Sub35[] class296_sub35s) {
		for (int i_10_ = 0; i_10_ < i; i_10_++) {
			aClass296_Sub35Array4220[i_10_] = class296_sub35s[i_10_];
		}
		anInt4254 = i;
		if (anInt4192 != 1) {
			method1279(true);
		}
	}

	public void method1278(int i, boolean bool) {
		if (i != 1553921480) {
			aFloat4259 = -0.3429062F;
		}
		if (!aBoolean4187 == bool) {
			aBoolean4187 = bool;
			method1309(i - 1553921380);
			anInt4162 &= ~0x7;
		}
	}

	@Override
	public void b(int i, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_) {
		float f = i + 0.35F;
		float f_16_ = i_11_ + 0.35F;
		float f_17_ = f + i_12_ - 1.0F;
		float f_18_ = i_13_ + f_16_ + -1.0F;
		method1277(false);
		method1336((byte) 43, i_15_);
		OpenGL.glColor4ub((byte) (i_14_ >> 16), (byte) (i_14_ >> 8), (byte) i_14_, (byte) (i_14_ >> 24));
		if (aBoolean4207) {
			OpenGL.glDisable(32925);
		}
		OpenGL.glBegin(2);
		OpenGL.glVertex2f(f, f_16_);
		OpenGL.glVertex2f(f, f_18_);
		OpenGL.glVertex2f(f_17_, f_18_);
		OpenGL.glVertex2f(f_17_, f_16_);
		OpenGL.glEnd();
		if (aBoolean4207) {
			OpenGL.glEnable(32925);
		}
	}

	@Override
	public void ZA(int i, float f, float f_19_, float f_20_, float f_21_, float f_22_) {
		boolean bool = i != anInt4191;
		if (bool || aFloat4272 != f || aFloat4248 != f_19_) {
			anInt4191 = i;
			aFloat4272 = f;
			aFloat4248 = f_19_;
			if (bool) {
				aFloat4186 = (anInt4191 & 0xff00) / 65280.0F;
				aFloat4193 = (anInt4191 & 0xff) / 255.0F;
				aFloat4261 = (anInt4191 & 0xff0000) / 1.671168E7F;
				method1299((byte) 122);
			}
			method1324(4609);
		}
		if (f_20_ != aFloatArray4197[0] || f_21_ != aFloatArray4197[1] || aFloatArray4197[2] != f_22_) {
			aFloatArray4197[1] = f_21_;
			aFloatArray4197[0] = f_20_;
			aFloatArray4197[2] = f_22_;
			aFloatArray4199[1] = -f_21_;
			aFloatArray4199[0] = -f_20_;
			aFloatArray4199[2] = -f_22_;
			float f_23_ = (float) (1.0 / Math.sqrt(f_22_ * f_22_ + (f_20_ * f_20_ + f_21_ * f_21_)));
			aFloatArray4202[0] = f_20_ * f_23_;
			aFloatArray4202[2] = f_23_ * f_22_;
			aFloatArray4202[1] = f_21_ * f_23_;
			aFloatArray4273[2] = -aFloatArray4202[2];
			aFloatArray4273[0] = -aFloatArray4202[0];
			aFloatArray4273[1] = -aFloatArray4202[1];
			method1339(8705);
			anInt4179 = (int) (f_22_ * 256.0F / f_21_);
			anInt4212 = (int) (f_20_ * 256.0F / f_21_);
		}
	}

	private void method1279(boolean bool) {
		int i;
		for (i = 0; i < anInt4254; i++) {
			Class296_Sub35 class296_sub35 = aClass296_Sub35Array4220[i];
			Class224.aFloatArray2172[0] = class296_sub35.method2749(true);
			int i_24_ = i + 16386;
			Class224.aFloatArray2172[1] = class296_sub35.method2751(-28925);
			Class224.aFloatArray2172[2] = class296_sub35.method2750(-4444);
			Class224.aFloatArray2172[3] = 1.0F;
			OpenGL.glLightfv(i_24_, 4611, Class224.aFloatArray2172, 0);
			int i_25_ = class296_sub35.method2746(-24996);
			float f = class296_sub35.method2745((byte) 100) / 255.0F;
			Class224.aFloatArray2172[0] = f * (i_25_ >> 16 & 255);
			Class224.aFloatArray2172[1] = f * ((i_25_ & 65448) >> 8);
			Class224.aFloatArray2172[2] = (255 & i_25_) * f;
			OpenGL.glLightfv(i_24_, 4609, Class224.aFloatArray2172, 0);
			OpenGL.glLightf(i_24_, 4617, 1.0F / (class296_sub35.method2748(107) * class296_sub35.method2748(125)));
			OpenGL.glEnable(i_24_);
		}
		for (/**/; i < anInt4266; i++) {
			OpenGL.glDisable(16386 + i);
		}
		anInt4266 = anInt4254;
		if (bool != true) {
			EA(-35, 64, 71, -32);
		}
	}

	public void method1280(Interface4 interface4, int i) {
		if (i != -1) {
			anInt4254 = -6;
		}
		if (!aBoolean4268) {
			if (anInt4142 < 0 || interface4 != anInterface4Array4143[anInt4142]) {
				throw new RuntimeException();
			}
			anInterface4Array4143[anInt4142--] = null;
			interface4.method18((byte) -96);
			if (anInt4142 < 0) {
				anInterface4_4144 = anInterface4_4145 = null;
			} else {
				anInterface4_4144 = anInterface4_4145 = anInterface4Array4143[anInt4142];
				anInterface4_4144.method20(-30874);
			}
		} else {
			method1353(i, interface4);
			method1335(0, interface4);
		}
	}

	public void method1281(int i, int i_26_, int i_27_, int i_28_) {
		if (i != 0) {
			aBoolean4213 = false;
		}
		OpenGL.glDrawArrays(i_28_, i_27_, i_26_);
	}

	public synchronized void method1282(byte i, long l) {
		if (i != -44) {
			method1291(-43, 3);
		}
		Node class296 = new Node();
		class296.uid = l;
		aClass155_4160.addLast((byte) 127, class296);
	}

	public void method1283(int i, int i_29_, int i_30_, byte i_31_) {
		OpenGL.glTexEnvi(8960, i + 34176, i_29_);
		if (i_31_ <= -108) {
			OpenGL.glTexEnvi(8960, i + 34192, i_30_);
		}
	}

	@Override
	public boolean r() {
		return false;
	}

	@Override
	public void a(Interface13 interface13) {
		/* empty */
	}

	@Override
	public Class55 a(Class92 class92, Class186[] class186s, boolean bool) {
		return new Class55_Sub5(this, class92, class186s, bool);
	}

	public void method1284(int i) {
		if (i != -31628) {
			anInt4254 = -50;
		}
		if (anInt4192 != 0) {
			anInt4192 = 0;
			anInt4162 &= ~0x1f;
		}
	}

	@Override
	public int i() {
		return anInt4249;
	}

	public void method1285(int i, boolean bool) {
		int i_32_ = 100 / ((i - 21) / 48);
		if (!bool != !aBoolean4166) {
			aBoolean4166 = bool;
			method1328(1);
			anInt4162 &= ~0x1f;
		}
	}

	@Override
	public void t() {
		for (Node class296 = aClass155_4136.removeFirst((byte) 114); class296 != null; class296 = aClass155_4136
				.removeNext(1001)) {
			((za_Sub2) class296).method3229(-126);
		}
		if (aClass342_4127 != null) {
			aClass342_4127.method3636((byte) -112);
		}
		if (anOpenGL4123 != null) {
			method1320(-125);
			Enumeration enumeration = aHashtable4116.keys();
			while (enumeration.hasMoreElements()) {
				Canvas canvas = (Canvas) enumeration.nextElement();
				Long var_long = (Long) aHashtable4116.get(canvas);
				anOpenGL4123.releaseSurface(canvas, var_long.longValue());
			}
			anOpenGL4123.release();
			anOpenGL4123 = null;
		}
		if (aBoolean4135) {
			Class355_Sub1.method3702((byte) 78, true, false);
			aBoolean4135 = false;
		}
	}

	public void method1286(int i) {
		OpenGL.glPushMatrix();
		int i_33_ = 75 % ((i - 74) / 32);
	}

	@Override
	public void a(Rectangle[] rectangles, int i, int i_34_, int i_35_) throws Exception_Sub1 {
		c(i_34_, i_35_);
	}

	private void method1287(byte i) {
		if (anInt4185 > anInt4238 || anInt4244 < anInt4201) {
			OpenGL.glScissor(0, 0, 0, 0);
		} else {
			OpenGL.glScissor(anInt4265 + anInt4185, anInt4176 + anInt4113 - anInt4244, -anInt4185 + anInt4238,
					-anInt4201 + anInt4244);
		}
		if (i >= -4) {
			method1287((byte) -90);
		}
	}

	@Override
	public void A(int i, aa var_aa, int i_36_, int i_37_) {
		aa_Sub1 var_aa_Sub1 = (aa_Sub1) var_aa;
		Class69_Sub1_Sub1 class69_sub1_sub1 = var_aa_Sub1.aClass69_Sub1_Sub1_3720;
		method1305((byte) 98);
		method1316(var_aa_Sub1.aClass69_Sub1_Sub1_3720, (byte) -104);
		method1336((byte) -117, 1);
		method1306(7681, 8448, -22394);
		method1283(0, 34167, 768, (byte) -113);
		float f = class69_sub1_sub1.aFloat6687 / class69_sub1_sub1.anInt6683;
		float f_38_ = class69_sub1_sub1.aFloat6684 / class69_sub1_sub1.anInt6688;
		OpenGL.glColor4ub((byte) (i >> 16), (byte) (i >> 8), (byte) i, (byte) (i >> 24));
		OpenGL.glBegin(7);
		OpenGL.glTexCoord2f(f * (anInt4185 - i_36_), f_38_ * (anInt4201 - i_37_));
		OpenGL.glVertex2i(anInt4185, anInt4201);
		OpenGL.glTexCoord2f((-i_36_ + anInt4185) * f, f_38_ * (anInt4244 - i_37_));
		OpenGL.glVertex2i(anInt4185, anInt4244);
		OpenGL.glTexCoord2f(f * (anInt4238 - i_36_), f_38_ * (-i_37_ + anInt4244));
		OpenGL.glVertex2i(anInt4238, anInt4244);
		OpenGL.glTexCoord2f((anInt4238 - i_36_) * f, f_38_ * (-i_37_ + anInt4201));
		OpenGL.glVertex2i(anInt4238, anInt4201);
		OpenGL.glEnd();
		method1283(0, 5890, 768, (byte) -119);
	}

	@Override
	public int[] na(int i, int i_39_, int i_40_, int i_41_) {
		int[] is = new int[i_41_ * i_40_];
		for (int i_42_ = 0; i_42_ < i_41_; i_42_++) {
			OpenGL.glReadPixelsi(i, -i_42_ + -i_39_ + anInt4113, i_40_, 1, 32993, anInt4231, is, i_40_ * i_42_);
		}
		return is;
	}

	public void method1288(int i) {
		OpenGL.glPopMatrix();
		if (i != 7330) {
			method1334(0.06228675F, 0.72800934F, -97, 2.2210333F);
		}
	}

	@Override
	public Class33 s() {
		int i = -1;
		if (aString4255.indexOf("nvidia") == -1) {
			if (aString4255.indexOf("intel") != -1) {
				i = 32902;
			} else if (aString4255.indexOf("ati") != -1) {
				i = 4098;
			}
		} else {
			i = 4318;
		}
		return new Class33(i, "OpenGL", anInt4183, aString4188, 0L);
	}

	@Override
	public void U(int i, int i_43_, int i_44_, int i_45_, int i_46_) {
		method1277(false);
		method1336((byte) -102, i_46_);
		float f = i + 0.35F;
		float f_47_ = i_43_ + 0.35F;
		OpenGL.glColor4ub((byte) (i_45_ >> 16), (byte) (i_45_ >> 8), (byte) i_45_, (byte) (i_45_ >> 24));
		OpenGL.glBegin(1);
		OpenGL.glVertex2f(f, f_47_);
		OpenGL.glVertex2f(f + i_44_, f_47_);
		OpenGL.glEnd();
	}

	@Override
	public synchronized void g(int i) {
		int i_48_ = 0;
		i &= 0x7fffffff;
		while (!aClass155_4155.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_4155.method1573(1);
			Class296_Sub35_Sub3.anIntArray6115[i_48_++] = (int) class296_sub16.uid;
			anInt4151 -= class296_sub16.value;
			if (i_48_ == 1000) {
				OpenGL.glDeleteBuffersARB(i_48_, Class296_Sub35_Sub3.anIntArray6115, 0);
				i_48_ = 0;
			}
		}
		if (i_48_ > 0) {
			OpenGL.glDeleteBuffersARB(i_48_, Class296_Sub35_Sub3.anIntArray6115, 0);
			i_48_ = 0;
		}
		while (!aClass155_4156.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_4156.method1573(1);
			Class296_Sub35_Sub3.anIntArray6115[i_48_++] = (int) class296_sub16.uid;
			anInt4152 -= class296_sub16.value;
			if (i_48_ == 1000) {
				OpenGL.glDeleteTextures(i_48_, Class296_Sub35_Sub3.anIntArray6115, 0);
				i_48_ = 0;
			}
		}
		if (i_48_ > 0) {
			OpenGL.glDeleteTextures(i_48_, Class296_Sub35_Sub3.anIntArray6115, 0);
			i_48_ = 0;
		}
		while (!aClass155_4157.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_4157.method1573(1);
			Class296_Sub35_Sub3.anIntArray6115[i_48_++] = class296_sub16.value;
			if (i_48_ == 1000) {
				OpenGL.glDeleteFramebuffersEXT(i_48_, Class296_Sub35_Sub3.anIntArray6115, 0);
				i_48_ = 0;
			}
		}
		if (i_48_ > 0) {
			OpenGL.glDeleteFramebuffersEXT(i_48_, Class296_Sub35_Sub3.anIntArray6115, 0);
			i_48_ = 0;
		}
		while (!aClass155_4158.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_4158.method1573(1);
			Class296_Sub35_Sub3.anIntArray6115[i_48_++] = (int) class296_sub16.uid;
			anInt4154 -= class296_sub16.value;
			if (i_48_ == 1000) {
				OpenGL.glDeleteRenderbuffersEXT(i_48_, Class296_Sub35_Sub3.anIntArray6115, 0);
				i_48_ = 0;
			}
		}
		if (i_48_ > 0) {
			OpenGL.glDeleteRenderbuffersEXT(i_48_, Class296_Sub35_Sub3.anIntArray6115, 0);
			boolean bool = false;
		}
		while (!aClass155_4153.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_4153.method1573(1);
			OpenGL.glDeleteLists((int) class296_sub16.uid, class296_sub16.value);
		}
		while (!aClass155_4159.method1577((byte) -77)) {
			Node class296 = aClass155_4159.method1573(1);
			OpenGL.glDeleteProgramARB((int) class296.uid);
		}
		while (!aClass155_4160.method1577((byte) -77)) {
			Node class296 = aClass155_4160.method1573(1);
			OpenGL.glDeleteObjectARB(class296.uid);
		}
		while (!aClass155_4153.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_4153.method1573(1);
			OpenGL.glDeleteLists((int) class296_sub16.uid, class296_sub16.value);
		}
		aClass49_4125.method608(-27366);
		if (E() > 100663296 && aLong4165 + 60000L < Class72.method771(-127)) {
			System.gc();
			aLong4165 = Class72.method771(-111);
		}
		anInt4134 = i;
	}

	@Override
	public void la() {
		anInt4238 = anInt4114;
		anInt4185 = 0;
		anInt4244 = anInt4113;
		anInt4201 = 0;
		OpenGL.glDisable(3089);
		method1319(-103);
	}

	private void method1289(int i) {
		if (anInt4192 != 2) {
			anInt4192 = 2;
			method1349(5888);
			method1352(3008);
			anInt4162 &= ~0x7;
		}
		if (i != -8) {
			method1282((byte) 29, 127L);
		}
	}

	@Override
	public void Q(int i, int i_49_, int i_50_, int i_51_, int i_52_, int i_53_, byte[] is, int i_54_, int i_55_) {
		float f;
		float f_56_;
		if (aClass69_Sub1_Sub1_4245 != null && i_50_ <= aClass69_Sub1_Sub1_4245.anInt5711
				&& aClass69_Sub1_Sub1_4245.anInt5715 >= i_51_) {
			aClass69_Sub1_Sub1_4245.method731((byte) -30, 0, 0, i_50_, i_51_, is, false, 0, 6406, 0);
			f_56_ = aClass69_Sub1_Sub1_4245.aFloat6687 * i_50_ / aClass69_Sub1_Sub1_4245.anInt5711;
			f = i_51_ * aClass69_Sub1_Sub1_4245.aFloat6684 / aClass69_Sub1_Sub1_4245.anInt5715;
		} else {
			aClass69_Sub1_Sub1_4245 = Class338_Sub3_Sub5.method3583(is, this, i_51_, 6406, i_50_, -111, false, 6406);
			aClass69_Sub1_Sub1_4245.method733(false, false, true);
			f = aClass69_Sub1_Sub1_4245.aFloat6684;
			f_56_ = aClass69_Sub1_Sub1_4245.aFloat6687;
		}
		method1305((byte) -116);
		method1316(aClass69_Sub1_Sub1_4245, (byte) -101);
		method1336((byte) 18, i_55_);
		OpenGL.glColor4ub((byte) (i_52_ >> 16), (byte) (i_52_ >> 8), (byte) i_52_, (byte) (i_52_ >> 24));
		method1354(false, i_53_);
		method1306(34165, 34165, -22394);
		method1283(0, 34166, 768, (byte) -120);
		method1283(2, 5890, 770, (byte) -111);
		method1346(true, 770, 0, 34166);
		method1346(true, 770, 2, 5890);
		float f_57_ = i;
		float f_58_ = i_49_;
		float f_59_ = f_57_ + i_50_;
		float f_60_ = i_51_ + f_58_;
		OpenGL.glBegin(7);
		OpenGL.glTexCoord2f(0.0F, 0.0F);
		OpenGL.glVertex2f(f_57_, f_58_);
		OpenGL.glTexCoord2f(0.0F, f_56_);
		OpenGL.glVertex2f(f_57_, f_60_);
		OpenGL.glTexCoord2f(f, f_56_);
		OpenGL.glVertex2f(f_59_, f_60_);
		OpenGL.glTexCoord2f(f, 0.0F);
		OpenGL.glVertex2f(f_59_, f_58_);
		OpenGL.glEnd();
		method1283(0, 5890, 768, (byte) -128);
		method1283(2, 34166, 770, (byte) -127);
		method1346(true, 770, 0, 5890);
		method1346(true, 770, 2, 34166);
	}

	@Override
	public Class373 f() {
		return aClass373_Sub1_4132;
	}

	public int method1290(int i, byte i_61_) {
		int i_62_ = 9 % ((i_61_ + 87) / 32);
		if (i == 6406 || i == 6409) {
			return 1;
		}
		if (i != 6410 && i != 34846 && i != 34844) {
			if (i != 6407) {
				if (i == 6408 || i == 34847) {
					return 4;
				}
				if (i != 34843) {
					if (i == 34842) {
						return 8;
					}
					if (i == 6402) {
						return 3;
					}
					if (i == 6401) {
						return 1;
					}
				} else {
					return 6;
				}
			} else {
				return 3;
			}
		} else {
			return 2;
		}
		throw new IllegalArgumentException("");
	}

	public int method1291(int i, int i_63_) {
		if (i_63_ != 1) {
			if (i_63_ != 0) {
				if (i_63_ != 2) {
					if (i_63_ == 3) {
						return 260;
					}
					if (i_63_ == 4) {
						return 34023;
					}
				} else {
					return 34165;
				}
			} else {
				return 8448;
			}
		} else {
			return 7681;
		}
		if (i >= -91) {
			aBoolean4190 = false;
		}
		throw new IllegalArgumentException();
	}

	private void method1292(byte i) {
		if (i <= 89) {
			a(15, 11, 31, -102, 98, -76, 84, -12, -113, 48, 120, -29, 126);
		}
		if (!aBoolean4208 || anInt4181 < 0) {
			OpenGL.glDisable(2912);
		} else {
			OpenGL.glEnable(2912);
		}
	}

	private void method1293(boolean bool) {
		float f = -anInt4215 * aFloat4182 / anInt4263;
		float f_64_ = aFloat4182 * -anInt4246 / anInt4227;
		float f_65_ = (anInt4114 - anInt4215) * aFloat4182 / anInt4263;
		float f_66_ = aFloat4182 * (-anInt4246 + anInt4113) / anInt4227;
		OpenGL.glMatrixMode(5889);
		OpenGL.glLoadIdentity();
		if (bool != true) {
			aString4255 = null;
		}
		if (f_65_ != f && f_64_ != f_66_) {
			OpenGL.glOrtho(f, f_65_, -f_66_, -f_64_, anInt4249, anInt4253);
		}
		OpenGL.glMatrixMode(5888);
	}

	@Override
	public boolean u() {
		return true;
	}

	@Override
	public void a(int i, int i_67_, int i_68_, int i_69_, int i_70_, int i_71_, int i_72_, int i_73_, int i_74_) {
		if (i != i_68_ || i_67_ != i_69_) {
			method1277(false);
			method1336((byte) 33, i_71_);
			float f = (float) -i + (float) i_68_;
			float f_75_ = (float) -i_67_ + (float) i_69_;
			float f_76_ = (float) (1.0 / Math.sqrt(f * f + f_75_ * f_75_));
			i_74_ %= i_72_ + i_73_;
			f *= f_76_;
			OpenGL.glColor4ub((byte) (i_70_ >> 16), (byte) (i_70_ >> 8), (byte) i_70_, (byte) (i_70_ >> 24));
			f_75_ *= f_76_;
			float f_77_ = i_72_ * f;
			float f_78_ = f_75_ * i_72_;
			float f_79_ = 0.0F;
			float f_80_ = 0.0F;
			float f_81_ = f_77_;
			float f_82_ = f_78_;
			if (i_74_ > i_72_) {
				f_79_ = (i_72_ - (-i_73_ + i_74_)) * f;
				f_80_ = (i_73_ + i_72_ - i_74_) * f_75_;
			} else {
				f_82_ = f_75_ * (-i_74_ + i_72_);
				f_81_ = f * (-i_74_ + i_72_);
			}
			float f_83_ = f_79_ + (i + 0.35F);
			float f_84_ = i_67_ + 0.35F + f_80_;
			float f_85_ = i_73_ * f;
			float f_86_ = i_73_ * f_75_;
			for (;;) {
				if (i_68_ <= i) {
					if (i_68_ + 0.35F > f_83_) {
						break;
					}
					if (f_83_ + f_81_ < i_68_) {
						f_81_ = i_68_ - f_83_;
					}
				} else {
					if (f_83_ > i_68_ + 0.35F) {
						break;
					}
					if (f_81_ + f_83_ > i_68_) {
						f_81_ = i_68_ - f_83_;
					}
				}
				if (i_69_ > i_67_) {
					if (i_69_ + 0.35F < f_84_) {
						break;
					}
					if (f_84_ + f_82_ > i_69_) {
						f_82_ = i_69_ - f_84_;
					}
				} else {
					if (f_84_ < i_69_ + 0.35F) {
						break;
					}
					if (i_69_ > f_82_ + f_84_) {
						f_82_ = -f_84_ + i_69_;
					}
				}
				OpenGL.glBegin(1);
				OpenGL.glVertex2f(f_83_, f_84_);
				OpenGL.glVertex2f(f_81_ + f_83_, f_82_ + f_84_);
				f_84_ += f_86_ + f_82_;
				OpenGL.glEnd();
				f_83_ += f_85_ + f_81_;
				f_81_ = f_77_;
				f_82_ = f_78_;
			}
		}
	}

	@Override
	public void xa(float f) {
		if (aFloat4217 != f) {
			aFloat4217 = f;
			method1299((byte) 85);
		}
	}

	@Override
	public void w() {
		if (aBoolean4243 && anInt4114 > 0 && anInt4113 > 0) {
			int i = anInt4185;
			int i_87_ = anInt4238;
			int i_88_ = anInt4201;
			int i_89_ = anInt4244;
			la();
			OpenGL.glReadBuffer(1028);
			OpenGL.glDrawBuffer(1029);
			method1284(-31628);
			method1311(false, 8577);
			method1278(1553921480, false);
			method1345(false, 120);
			method1285(106, false);
			method1316(null, (byte) -112);
			method1340(-2, (byte) 92);
			method1272((byte) -107, 1);
			method1336((byte) 2, 0);
			OpenGL.glMatrixMode(5889);
			OpenGL.glLoadIdentity();
			OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
			OpenGL.glMatrixMode(5888);
			OpenGL.glLoadIdentity();
			OpenGL.glRasterPos2i(0, 0);
			OpenGL.glCopyPixels(0, 0, anInt4114, anInt4113, 6144);
			OpenGL.glFlush();
			OpenGL.glReadBuffer(1029);
			OpenGL.glDrawBuffer(1029);
			KA(i, i_88_, i_87_, i_89_);
		}
	}

	@Override
	public void a(boolean bool) {
		/* empty */
	}

	@Override
	public boolean b() {
		if (aClass296_Sub9_Sub1_4130 == null || !aClass296_Sub9_Sub1_4130.method2488(-17671)) {
			return false;
		}
		return true;
	}

	@Override
	public void b(Canvas canvas, int i, int i_90_) {
		if (canvas == aCanvas4122) {
			throw new RuntimeException();
		}
		if (!aHashtable4116.containsKey(canvas)) {
			if (!canvas.isShowing()) {
				throw new RuntimeException();
			}
			try {
				Class var_class = java.awt.Canvas.class;
				Method method = var_class.getMethod("setIgnoreRepaint", new Class[] { Boolean.TYPE });
				method.invoke(canvas, new Object[] { Boolean.TRUE });
			} catch (Exception exception) {
				/* empty */
			}
			long l = anOpenGL4123.prepareSurface(canvas);
			if (l == -1L) {
				throw new RuntimeException();
			}
			aHashtable4116.put(canvas, new Long(l));
		}
	}

	@Override
	public void e() {
		/* empty */
	}

	public synchronized void method1294(int i, int i_91_) {
		Node class296 = new Node();
		if (i != 1) {
			anInterface5_4274 = null;
		}
		class296.uid = i_91_;
		aClass155_4159.addLast((byte) -93, class296);
	}

	@Override
	public s a(int i, int i_92_, int[][] is, int[][] is_93_, int i_94_, int i_95_, int i_96_) {
		return new s_Sub2(this, i_95_, i_96_, i, i_92_, is, is_93_, i_94_);
	}

	@Override
	public Sprite a(int i, int i_97_, int i_98_, int i_99_, boolean bool) {
		return new Class397_Sub3(this, i, i_97_, i_98_, i_99_);
	}

	@Override
	public void P(int i, int i_100_, int i_101_, int i_102_, int i_103_) {
		method1277(false);
		method1336((byte) -4, i_103_);
		float f = i + 0.35F;
		OpenGL.glColor4ub((byte) (i_102_ >> 16), (byte) (i_102_ >> 8), (byte) i_102_, (byte) (i_102_ >> 24));
		float f_104_ = i_100_ + 0.35F;
		OpenGL.glBegin(1);
		OpenGL.glVertex2f(f, f_104_);
		OpenGL.glVertex2f(f, f_104_ + i_101_);
		OpenGL.glEnd();
	}

	static public void method1295(byte i) {
		int i_105_ = -98 % ((i - 77) / 41);
		for (int i_106_ = 0; i_106_ < Class367.npcsCount; i_106_++) {
			int i_107_ = ReferenceTable.npcIndexes[i_106_];
			NPCNode class296_sub7 = (NPCNode) Class41_Sub18.localNpcs.get(i_107_);
			if (class296_sub7 != null) {
				NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
				Class296_Sub39_Sub20.method2903(127, class338_sub3_sub1_sub3_sub2, false);
			}
		}
	}

	@Override
	public int d(int i, int i_108_) {
		return i | i_108_;
	}

	private void method1296(int i) {
		aFloatArray4267[10] = aFloat4196;
		aFloatArray4267[14] = aFloat4172;
		aFloat4226 = anInt4253;
		aFloat4210 = (-anInt4253 + aFloatArray4267[i]) / aFloatArray4267[10];
	}

	@Override
	public int E() {
		return anInt4154 + anInt4152 + anInt4151;
	}

	@Override
	public boolean a() {
		return true;
	}

	@Override
	public void a(int i, int i_109_, int i_110_, int i_111_, int i_112_, int i_113_, int i_114_) {
		OpenGL.glLineWidth(i_113_);
		e(i, i_109_, i_110_, i_111_, i_112_, i_114_);
		OpenGL.glLineWidth(1.0F);
	}

	public void method1297(boolean bool, byte i, boolean bool_115_, int i_116_) {
		if (anInt4241 != i_116_ || underwater_mode != aBoolean4230) {
			Class69_Sub1 class69_sub1 = null;
			int i_117_ = 0;
			byte i_118_ = 0;
			int i_119_ = 0;
			byte i_120_ = !underwater_mode ? (byte) 0 : (byte) 3;
			if (i_116_ < 0) {
				method1318((byte) -9);
			} else {
				class69_sub1 = aClass49_4125.method606(34842, i_116_);
				MaterialRaw class170 = aD1299.method14(i_116_, -9412);
				if (class170.speed_u == 0 && class170.speed_v == 0) {
					method1318((byte) -9);
				} else {
					int i_121_ = class170.small_sized ? 64 : 128;
					int i_122_ = i_121_ * 50;
					method1334(0.0F, (float) (class170.speed_u * (anInt4134 % i_122_)) / (float) i_122_, 112,
							(float) (anInt4134 % i_122_ * class170.speed_v) / (float) i_122_);
				}
				if (!underwater_mode) {
					i_118_ = class170.aByte1784;
					i_119_ = class170.anInt1794;
					i_120_ = class170.aByte1774;
				}
				i_117_ = class170.anInt1782;
			}
			aClass197_4126.method1941(bool_115_, bool, i_119_, i_120_, (byte) 124, i_118_);
			if (!aClass197_4126.method1943(i_117_, false, class69_sub1)) {
				method1316(class69_sub1, (byte) -127);
				method1272((byte) -107, i_117_);
			}
			aBoolean4230 = underwater_mode;
			anInt4241 = i_116_;
		}
		anInt4162 &= ~0x7;
		if (i != -76) {
			method1348(21, null, -106, (byte) -128, -107);
		}
	}

	private int method1298(int i) {
		int i_123_ = 0;
		aString4255 = OpenGL.glGetString(7936).toLowerCase();
		aString4188 = OpenGL.glGetString(7937).toLowerCase();
		if (aString4255.indexOf("microsoft") != -1) {
			i_123_ |= 0x1;
		}
		if (aString4255.indexOf("brian paul") != -1 || aString4255.indexOf("mesa") != -1) {
			i_123_ |= 0x1;
		}
		String string = OpenGL.glGetString(7938);
		String[] strings = Class41_Sub30.method522((byte) 63, string.replace('.', ' '), ' ');
		if (strings.length < 2) {
			i_123_ |= 0x4;
		} else {
			try {
				int i_124_ = Class366_Sub2.method3774(-112, strings[0]);
				int i_125_ = Class366_Sub2.method3774(-54, strings[1]);
				anInt4183 = i_124_ * 10 + i_125_;
			} catch (NumberFormatException numberformatexception) {
				i_123_ |= 0x4;
			}
		}
		if (anInt4183 < 12) {
			i_123_ |= 0x2;
		}
		if (!anOpenGL4123.a("GL_ARB_multitexture")) {
			i_123_ |= 0x8;
		}
		if (!anOpenGL4123.a("GL_ARB_texture_env_combine")) {
			i_123_ |= 0x20;
		}
		int[] is = new int[1];
		OpenGL.glGetIntegerv(34018, is, 0);
		anInt4224 = is[0];
		OpenGL.glGetIntegerv(34929, is, 0);
		anInt4235 = is[0];
		int i_126_ = -26 % ((20 - i) / 42);
		OpenGL.glGetIntegerv(34930, is, 0);
		anInt4258 = is[0];
		if (anInt4224 < 2 || anInt4235 < 2 || anInt4258 < 2) {
			i_123_ |= 0x10;
		}
		aBoolean4184 = Stream.a();
		aBoolean4209 = anOpenGL4123.arePbuffersAvailable();
		aBoolean4252 = anOpenGL4123.a("GL_ARB_vertex_buffer_object");
		aBoolean4207 = anOpenGL4123.a("GL_ARB_multisample");
		aBoolean4228 = anOpenGL4123.a("GL_ARB_vertex_program");
		anOpenGL4123.a("GL_ARB_fragment_program");
		aBoolean4229 = anOpenGL4123.a("GL_ARB_vertex_shader");
		aBoolean4174 = anOpenGL4123.a("GL_ARB_fragment_shader");
		aBoolean4225 = anOpenGL4123.a("GL_EXT_texture3D");
		aBoolean4257 = anOpenGL4123.a("GL_ARB_texture_rectangle");
		aBoolean4251 = anOpenGL4123.a("GL_ARB_texture_cube_map");
		aBoolean4189 = anOpenGL4123.a("GL_ARB_texture_float");
		aBoolean4219 = false;
		aBoolean4190 = anOpenGL4123.a("GL_EXT_framebuffer_object");
		aBoolean4268 = anOpenGL4123.a("GL_EXT_framebuffer_blit");
		aBoolean4240 = anOpenGL4123.a("GL_EXT_framebuffer_multisample");
		aBoolean4239 = aBoolean4240 & aBoolean4268;
		aBoolean4213 = Class5.aString81.startsWith("mac");
		OpenGL.glGetFloatv(2834, Packet.aFloatArray4675, 0);
		aFloat4259 = Packet.aFloatArray4675[0];
		aFloat4262 = Packet.aFloatArray4675[1];
		if (i_123_ == 0) {
			return 0;
		}
		return i_123_;
	}

	@Override
	public void K(int[] is) {
		is[2] = anInt4238;
		is[1] = anInt4201;
		is[3] = anInt4244;
		is[0] = anInt4185;
	}

	private void method1299(byte i) {
		Packet.aFloatArray4675[1] = aFloat4186 * aFloat4217;
		Packet.aFloatArray4675[2] = aFloat4217 * aFloat4193;
		Packet.aFloatArray4675[0] = aFloat4217 * aFloat4261;
		Packet.aFloatArray4675[3] = 1.0F;
		OpenGL.glLightModelfv(2899, Packet.aFloatArray4675, 0);
		int i_127_ = -116 / ((i + 34) / 52);
	}

	@Override
	public void ya() {
		method1285(-72, true);
		OpenGL.glClear(256);
	}

	private void method1300(int i) {
		float[] fs = aFloatArray4267;
		if (i > 99) {
			float f = (float) (-anInt4215 * anInt4249) / (float) anInt4263;
			float f_128_ = (float) ((-anInt4215 + anInt4114) * anInt4249) / (float) anInt4263;
			float f_129_ = (float) (anInt4249 * anInt4246) / (float) anInt4227;
			float f_130_ = (float) (anInt4249 * (anInt4246 - anInt4113)) / (float) anInt4227;
			if (f_128_ == f || f_130_ == f_129_) {
				fs[8] = 0.0F;
				fs[2] = 0.0F;
				fs[12] = 0.0F;
				fs[3] = 0.0F;
				fs[10] = 1.0F;
				fs[11] = 0.0F;
				fs[15] = 1.0F;
				fs[4] = 0.0F;
				fs[7] = 0.0F;
				fs[0] = 1.0F;
				fs[13] = 0.0F;
				fs[6] = 0.0F;
				fs[1] = 0.0F;
				fs[14] = 0.0F;
				fs[9] = 0.0F;
				fs[5] = 1.0F;
			} else {
				float f_131_ = anInt4249 * 2.0F;
				fs[7] = 0.0F;
				fs[6] = 0.0F;
				fs[12] = 0.0F;
				fs[8] = (f_128_ + f) / (-f + f_128_);
				fs[2] = 0.0F;
				fs[13] = 0.0F;
				fs[9] = (f_130_ + f_129_) / (f_129_ - f_130_);
				fs[14] = aFloat4172 = -(anInt4253 * f_131_) / (anInt4253 - anInt4249);
				fs[15] = 0.0F;
				fs[3] = 0.0F;
				fs[4] = 0.0F;
				fs[1] = 0.0F;
				fs[10] = aFloat4196 = (float) -(anInt4253 + anInt4249) / (float) (anInt4253 - anInt4249);
				fs[0] = f_131_ / (-f + f_128_);
				fs[11] = -1.0F;
				fs[5] = f_131_ / (-f_130_ + f_129_);
			}
			method1296(14);
		}
	}

	public synchronized void method1301(int i, int i_132_, int i_133_) {
		if (i_132_ < -60) {
			IntegerNode class296_sub16 = new IntegerNode(i_133_);
			class296_sub16.uid = i;
			aClass155_4155.addLast((byte) -33, class296_sub16);
		}
	}

	public void method1302(int i) {
		if (anInt4162 != 16) {
			method1271((byte) 89);
			method1311(true, i ^ 0x81);
			method1345(true, 88);
			method1285(-27, true);
			method1336((byte) -90, 1);
			anInt4162 = 16;
		}
		if (i != 8448) {
			aClass178_Sub2Array4214 = null;
		}
	}

	@Override
	public void KA(int i, int i_134_, int i_135_, int i_136_) {
		if (i_134_ < 0) {
			i_134_ = 0;
		}
		if (i < 0) {
			i = 0;
		}
		if (anInt4113 < i_136_) {
			i_136_ = anInt4113;
		}
		if (i_135_ > anInt4114) {
			i_135_ = anInt4114;
		}
		anInt4238 = i_135_;
		anInt4185 = i;
		anInt4244 = i_136_;
		anInt4201 = i_134_;
		OpenGL.glEnable(3089);
		method1319(-104);
		method1287((byte) -88);
	}

	@Override
	public boolean k() {
		if (aClass296_Sub9_Sub1_4130 != null) {
			if (!aClass296_Sub9_Sub1_4130.method2488(-17671)) {
				if (!aClass342_4127.method3634(1, aClass296_Sub9_Sub1_4130)) {
					return false;
				}
				aClass49_4125.method605(3553);
			}
			return true;
		}
		return false;
	}

	@Override
	public void a(int i, int i_137_, int i_138_, int i_139_, int i_140_, int i_141_, aa var_aa, int i_142_,
			int i_143_) {
		aa_Sub1 var_aa_Sub1 = (aa_Sub1) var_aa;
		Class69_Sub1_Sub1 class69_sub1_sub1 = var_aa_Sub1.aClass69_Sub1_Sub1_3720;
		method1305((byte) 83);
		method1316(var_aa_Sub1.aClass69_Sub1_Sub1_3720, (byte) -118);
		method1336((byte) 74, i_141_);
		method1306(7681, 8448, -22394);
		method1283(0, 34167, 768, (byte) -118);
		float f = class69_sub1_sub1.aFloat6687 / class69_sub1_sub1.anInt6683;
		float f_144_ = class69_sub1_sub1.aFloat6684 / class69_sub1_sub1.anInt6688;
		float f_145_ = (float) i_138_ - (float) i;
		float f_146_ = (float) -i_137_ + (float) i_139_;
		float f_147_ = (float) (1.0 / Math.sqrt(f_146_ * f_146_ + f_145_ * f_145_));
		OpenGL.glColor4ub((byte) (i_140_ >> 16), (byte) (i_140_ >> 8), (byte) i_140_, (byte) (i_140_ >> 24));
		f_145_ *= f_147_;
		f_146_ *= f_147_;
		OpenGL.glBegin(1);
		OpenGL.glTexCoord2f((-i_142_ + i) * f, (-i_143_ + i_137_) * f_144_);
		OpenGL.glVertex2f(i + 0.35F, i_137_ + 0.35F);
		OpenGL.glTexCoord2f((-i_142_ + i_138_) * f, f_144_ * (i_139_ - i_143_));
		OpenGL.glVertex2f(i_138_ + f_145_ + 0.35F, f_146_ + i_139_ + 0.35F);
		OpenGL.glEnd();
		method1283(0, 5890, 768, (byte) -109);
	}

	public void method1303(int i, Interface20 interface20) {
		if (interface20 != anInterface20_4203) {
			if (aBoolean4252) {
				OpenGL.glBindBufferARB(34963, interface20.method77((byte) 113));
			}
			anInterface20_4203 = interface20;
		}
		if (i > -10) {
			a((Class373) null);
		}
	}

	@Override
	public int[] Y() {
		return new int[] { anInt4215, anInt4246, anInt4263, anInt4227 };
	}

	private void method1304(boolean bool) {
		aClass69Array4218 = new Class69[anInt4224];
		aClass69_Sub1_4195 = new Class69_Sub1(this, 3553, 6408, 1, 1);
		new Class69_Sub1(this, 3553, 6408, 1, 1);
		new Class69_Sub1(this, 3553, 6408, 1, 1);
		if (bool != true) {
			anIntArray4275 = null;
		}
		for (int i = 0; i < 7; i++) {
			aClass178_Sub2Array4214[i] = new Class178_Sub2(this);
			aClass178_Sub2Array4222[i] = new Class178_Sub2(this);
		}
		if (aBoolean4190) {
			aClass392_4260 = new Class392(this);
			new Class392(this);
		}
	}

	@Override
	public void a(Class390 class390, int i) {
		aClass173_4129.method1690(i, this, class390, 11321);
	}

	public void method1305(byte i) {
		if (anInt4162 != 2) {
			method1341(true);
			method1311(false, 8577);
			method1278(1553921480, false);
			method1345(false, 114);
			method1285(-70, false);
			method1340(-2, (byte) 127);
			anInt4162 = 2;
		}
		int i_148_ = -55 / ((-49 - i) / 59);
	}

	@Override
	public void F(int i, int i_149_) {
		/* empty */
	}

	public void method1306(int i, int i_150_, int i_151_) {
		if (anInt4221 == 0) {
			boolean bool = false;
			if (i != anInt4264) {
				OpenGL.glTexEnvi(8960, 34161, i);
				bool = true;
				anInt4264 = i;
			}
			if (i_150_ != anInt4271) {
				OpenGL.glTexEnvi(8960, 34162, i_150_);
				anInt4271 = i_150_;
				bool = true;
			}
			if (bool) {
				anInt4162 &= ~0x1d;
			}
		} else {
			OpenGL.glTexEnvi(8960, 34161, i);
			OpenGL.glTexEnvi(8960, 34162, i_150_);
		}
		if (i_151_ != -22394) {
			aFloatArray4273 = null;
		}
	}

	public void method1307(byte i, Interface4 interface4) {
		if (anInt4147 >= 3) {
			throw new RuntimeException();
		}
		if (anInt4147 >= 0) {
			anInterface4Array4148[anInt4147].method23(26044);
		}
		anInterface4_4145 = anInterface4Array4148[++anInt4147] = interface4;
		if (i == -39) {
			anInterface4_4145.method21(false);
		}
	}

	public void method1308(int i, float f) {
		if (i >= -32) {
			anInt4114 = 41;
		}
		if (aFloat4182 != f) {
			aFloat4182 = f;
			if (anInt4192 == 3) {
				method1293(true);
			}
		}
	}

	private void method1309(int i) {
		if (aBoolean4187 && !aBoolean4236) {
			OpenGL.glEnable(2896);
		} else {
			OpenGL.glDisable(2896);
		}
		if (i < 57) {
			method1304(true);
		}
	}

	@Override
	public Class373 o() {
		return aClass373_Sub1_4168;
	}

	public Interface20 method1310(int i, int i_152_, int i_153_, byte[] is, boolean bool) {
		if (i != 65280) {
			anInt4223 = -8;
		}
		if (aBoolean4252 && (!bool || aBoolean4177)) {
			return new Class355_Sub1(this, i_153_, is, i_152_, bool);
		}
		return new Class295_Sub1(this, i_153_, is, i_152_);
	}

	@Override
	public void f(int i) {
		if (i != 1) {
			throw new IllegalArgumentException("");
		}
	}

	public void method1311(boolean bool, int i) {
		if (bool != aBoolean4208) {
			aBoolean4208 = bool;
			method1292((byte) 115);
			anInt4162 &= ~0x1f;
		}
		if (i != 8577) {
			v();
		}
	}

	@Override
	public boolean j() {
		return true;
	}

	@Override
	public void f(int i, int i_154_) {
		if (anInt4249 != i || anInt4253 != i_154_) {
			anInt4253 = i_154_;
			anInt4249 = i;
			method1300(112);
			method1344(0);
			if (anInt4192 == 3) {
				method1293(true);
			} else if (anInt4192 == 2) {
				method1349(5888);
			}
		}
	}

	public int method1312(int i, int i_155_) {
		if (i_155_ != -3216) {
			return 43;
		}
		if (i == 5121 || i == 5120) {
			return 1;
		}
		if (i == 5123 || i == 5122) {
			return 2;
		}
		if (i == 5125 || i == 5124 || i == 5126) {
			return 4;
		}
		throw new IllegalArgumentException("");
	}

	public void method1313(int i, Class373_Sub1 class373_sub1) {
		OpenGL.glLoadMatrixf(class373_sub1.method3923(true), 0);
		int i_156_ = -49 % ((i + 18) / 59);
	}

	public void method1314(int i, int i_157_, int i_158_) {
		if (i_157_ == 1) {
			anInt4176 = i;
			anInt4265 = i_158_;
			method1338(-4);
			method1287((byte) -56);
		}
	}

	public void method1315(int i) {
		if (anInt4162 != 8) {
			method1289(-8);
			method1311(true, 8577);
			method1345(true, i ^ 0x44);
			method1285(107, true);
			method1336((byte) 124, 1);
			anInt4162 = 8;
		}
		if (i != 1) {
			L(70, 17, -104);
		}
	}

	public void method1316(Class69 class69, byte i) {
		if (i <= -100) {
			Class69 class69_159_ = aClass69Array4218[anInt4221];
			if (class69 != class69_159_) {
				if (class69 != null) {
					if (class69_159_ != null) {
						if (class69.anInt3682 != class69_159_.anInt3682) {
							OpenGL.glDisable(class69_159_.anInt3682);
							OpenGL.glEnable(class69.anInt3682);
						}
					} else {
						OpenGL.glEnable(class69.anInt3682);
					}
					OpenGL.glBindTexture(class69.anInt3682, class69.method727(203));
				} else {
					OpenGL.glDisable(class69_159_.anInt3682);
				}
				aClass69Array4218[anInt4221] = class69;
			}
			anInt4162 &= ~0x1;
		}
	}

	static public void method1317(Class338_Sub3 class338_sub3, int i, int i_160_, int i_161_, int i_162_, int i_163_) {
		boolean bool = true;
		int i_164_ = i_160_;
		int i_165_ = i_160_ + i_162_;
		int i_166_ = i_161_ - 1;
		int i_167_ = i_161_ + i_163_;
		for (int i_168_ = i; i_168_ <= i + 1; i_168_++) {
			if (i_168_ != Class368_Sub9.anInt5477) {
				for (int i_169_ = i_164_; i_169_ <= i_165_; i_169_++) {
					if (i_169_ >= 0 && i_169_ < Class228.anInt2201) {
						for (int i_170_ = i_166_; i_170_ <= i_167_; i_170_++) {
							if (i_170_ >= 0 && i_170_ < Class368_Sub12.anInt5488 && (!bool || i_169_ >= i_165_
									|| i_170_ >= i_167_ || i_170_ < i_161_ && i_169_ != i_160_)) {
								Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i_168_][i_169_][i_170_];
								if (class247 != null) {
									int i_171_ = (Class360_Sub2.aSArray5304[i_168_].method3355(i_170_, (byte) -111,
											i_169_)
											+ Class360_Sub2.aSArray5304[i_168_].method3355(i_170_, (byte) -125,
													i_169_ + 1)
											+ Class360_Sub2.aSArray5304[i_168_].method3355(i_170_ + 1, (byte) -127,
													i_169_)
											+ Class360_Sub2.aSArray5304[i_168_].method3355(i_170_ + 1, (byte) -118,
													i_169_ + 1))
											/ 4
											- (Class360_Sub2.aSArray5304[i].method3355(i_161_, (byte) -116, i_160_)
													+ Class360_Sub2.aSArray5304[i].method3355(i_161_, (byte) -107,
															i_160_ + 1)
													+ Class360_Sub2.aSArray5304[i].method3355(i_161_ + 1, (byte) -111,
															i_160_)
													+ Class360_Sub2.aSArray5304[i].method3355(i_161_ + 1, (byte) -116,
															i_160_ + 1))
													/ 4;
									Class338_Sub3_Sub4 class338_sub3_sub4 = class247.aClass338_Sub3_Sub4_2348;
									Class338_Sub3_Sub4 class338_sub3_sub4_172_ = class247.aClass338_Sub3_Sub4_2343;
									if (class338_sub3_sub4 != null && class338_sub3_sub4.method3468(-120)) {
										class338_sub3.method3467(
												(i_170_ - i_161_) * Js5TextureLoader.anInt3440
														+ (1 - i_163_) * Class304.anInt2737,
												(i_169_ - i_160_) * Js5TextureLoader.anInt3440
														+ (1 - i_162_) * Class304.anInt2737,
												class338_sub3_sub4, bool, i_171_, -41, Class16_Sub2.aHa3733);
									}
									if (class338_sub3_sub4_172_ != null && class338_sub3_sub4_172_.method3468(-117)) {
										class338_sub3.method3467(
												(i_170_ - i_161_) * Js5TextureLoader.anInt3440
														+ (1 - i_163_) * Class304.anInt2737,
												(i_169_ - i_160_) * Js5TextureLoader.anInt3440
														+ (1 - i_162_) * Class304.anInt2737,
												class338_sub3_sub4_172_, bool, i_171_, 104, Class16_Sub2.aHa3733);
									}
									for (Class234 class234 = class247.aClass234_2341; class234 != null; class234 = class234.aClass234_2224) {
										Class338_Sub3_Sub1 class338_sub3_sub1 = class234.aClass338_Sub3_Sub1_2226;
										if (class338_sub3_sub1 != null && class338_sub3_sub1.method3468(-39)
												&& (i_169_ == class338_sub3_sub1.aShort6560 || i_169_ == i_164_)
												&& (i_170_ == class338_sub3_sub1.aShort6564 || i_170_ == i_166_)) {
											int i_173_ = class338_sub3_sub1.aShort6559 - class338_sub3_sub1.aShort6560
													+ 1;
											int i_174_ = class338_sub3_sub1.aShort6558 - class338_sub3_sub1.aShort6564
													+ 1;
											class338_sub3.method3467(
													(class338_sub3_sub1.aShort6564 - i_161_)
															* Js5TextureLoader.anInt3440
															+ (i_174_ - i_163_) * Class304.anInt2737,
													(class338_sub3_sub1.aShort6560 - i_160_)
															* Js5TextureLoader.anInt3440
															+ (i_173_ - i_162_) * Class304.anInt2737,
													class338_sub3_sub1, bool, i_171_, 118, Class16_Sub2.aHa3733);
										}
									}
								}
							}
						}
					}
				}
				i_164_--;
				bool = false;
			}
		}
	}

	@Override
	public void e(int i, int i_175_, int i_176_, int i_177_, int i_178_, int i_179_) {
		method1277(false);
		method1336((byte) -110, i_179_);
		float f = (float) i_176_ - (float) i;
		float f_180_ = (float) i_177_ - (float) i_175_;
		if (f != 0.0F || f_180_ != 0.0F) {
			float f_181_ = (float) (1.0 / Math.sqrt(f_180_ * f_180_ + f * f));
			f_180_ *= f_181_;
			f *= f_181_;
		} else {
			f = 1.0F;
		}
		OpenGL.glColor4ub((byte) (i_178_ >> 16), (byte) (i_178_ >> 8), (byte) i_178_, (byte) (i_178_ >> 24));
		OpenGL.glBegin(1);
		OpenGL.glVertex2f(i + 0.35F, i_175_ + 0.35F);
		OpenGL.glVertex2f(f + i_176_ + 0.35F, f_180_ + i_177_ + 0.35F);
		OpenGL.glEnd();
	}

	@Override
	public void a(int i) {
		if (i < 128 || i > 1024) {
			throw new IllegalArgumentException();
		}
		anInt4131 = i;
		aClass49_4125.method605(3553);
	}

	@Override
	public void GA(int i) {
		method1336((byte) -112, 0);
		OpenGL.glClearColor((i & 0xff0000) / 1.671168E7F, (i & 0xff00) / 65280.0F, (i & 0xff) / 255.0F,
				(i >>> 24) / 255.0F);
		OpenGL.glClear(16384);
	}

	@Override
	public void c(int i, int i_182_) throws Exception_Sub1 {
		try {
			anOpenGL4123.swapBuffers();
		} catch (Exception exception) {
			/* empty */
		}
	}

	@Override
	public void aa(int i, int i_183_, int i_184_, int i_185_, int i_186_, int i_187_) {
		float f = i + 0.35F;
		float f_188_ = i_183_ + 0.35F;
		float f_189_ = i_184_ + f;
		method1277(false);
		float f_190_ = f_188_ + i_185_;
		method1336((byte) 28, i_187_);
		OpenGL.glColor4ub((byte) (i_186_ >> 16), (byte) (i_186_ >> 8), (byte) i_186_, (byte) (i_186_ >> 24));
		if (aBoolean4207) {
			OpenGL.glDisable(32925);
		}
		OpenGL.glBegin(7);
		OpenGL.glVertex2f(f, f_188_);
		OpenGL.glVertex2f(f, f_190_);
		OpenGL.glVertex2f(f_189_, f_190_);
		OpenGL.glVertex2f(f_189_, f_188_);
		OpenGL.glEnd();
		if (aBoolean4207) {
			OpenGL.glEnable(32925);
		}
	}

	@Override
	public Sprite a(int[] is, int i, int i_191_, int i_192_, int i_193_, boolean bool) {
		return new Class397_Sub3(this, i_192_, i_193_, is, i, i_191_);
	}

	@Override
	public void L(int i, int i_194_, int i_195_) {
		if (anInt4250 != i || anInt4181 != i_194_ || i_195_ != anInt4223) {
			anInt4181 = i_194_;
			anInt4223 = i_195_;
			anInt4250 = i;
			method1344(0);
			method1292((byte) 97);
		}
	}

	@Override
	public void H(int i, int i_196_, int i_197_, int[] is) {
		float f = aClass373_Sub1_4168.aFloat5588 + (i_197_ * aClass373_Sub1_4168.aFloat5589
				+ (i_196_ * aClass373_Sub1_4168.aFloat5579 + aClass373_Sub1_4168.aFloat5587 * i));
		if (f == 0.0F) {
			is[0] = is[1] = is[2] = -1;
		} else {
			int i_198_ = (int) ((aClass373_Sub1_4168.aFloat5576 + (aClass373_Sub1_4168.aFloat5581 * i_197_
					+ (i * aClass373_Sub1_4168.aFloat5577 + i_196_ * aClass373_Sub1_4168.aFloat5585))) * anInt4263 / f);
			is[0] = (int) (-aFloat4216 + i_198_);
			int i_199_ = (int) ((i_197_ * aClass373_Sub1_4168.aFloat5586
					+ (i * aClass373_Sub1_4168.aFloat5584 + aClass373_Sub1_4168.aFloat5583 * i_196_)
					+ aClass373_Sub1_4168.aFloat5582) * anInt4227 / f);
			is[1] = (int) (-aFloat4242 + i_199_);
			is[2] = (int) f;
		}
	}

	@Override
	public void A() {
		OpenGL.glFinish();
	}

	private void method1318(byte i) {
		if (i != -9) {
			method1277(true);
		}
		if (aBoolean4232) {
			OpenGL.glMatrixMode(5890);
			OpenGL.glLoadIdentity();
			OpenGL.glMatrixMode(5888);
			aBoolean4232 = false;
		}
	}

	@Override
	public void c(int i) {
		method1320(-128);
	}

	private void method1319(int i) {
		aFloat4242 = -anInt4246 + anInt4201;
		aFloat4256 = anInt4244 - anInt4246;
		aFloat4198 = -anInt4215 + anInt4238;
		if (i > -84) {
			w();
		}
		aFloat4216 = -anInt4215 + anInt4185;
	}

	@Override
	public void C(boolean bool) {
		aBoolean4247 = bool;
		method1328(1);
	}

	@Override
	public void h(int i) {
		/* empty */
	}

	private void method1320(int i) {
		if (i <= -124) {
			anOpenGL4123.a();
		}
	}

	public Interface5 method1321(int i, boolean bool, int i_200_, byte[] is, int i_201_) {
		if (aBoolean4252 && (!bool || aBoolean4177)) {
			return new Class355_Sub2(this, i_200_, is, i, bool);
		}
		if (i_201_ != 5888) {
			return null;
		}
		return new Class295_Sub2(this, i_200_, is, i);
	}

	public void method1322(float f, int i, float f_202_) {
		aFloat4269 = f;
		aFloat4180 = f_202_;
		method1344(0);
		if (i <= 23) {
			aFloat4198 = 0.6412745F;
		}
	}

	public void method1323(Class272 class272, Class272 class272_203_, Class272 class272_204_, boolean bool,
			Class272 class272_205_) {
		if (class272_205_ == null) {
			OpenGL.glDisableClientState(32884);
		} else {
			method1331(76, class272_205_.anInterface5_2521);
			OpenGL.glVertexPointer(class272_205_.aByte2518, class272_205_.aShort2522, anInterface5_4274.method24(-126),
					anInterface5_4274.method27(127) + class272_205_.aByte2520);
			OpenGL.glEnableClientState(32884);
		}
		if (class272 == null) {
			OpenGL.glDisableClientState(32885);
		} else {
			method1331(95, class272.anInterface5_2521);
			OpenGL.glNormalPointer(class272.aShort2522, anInterface5_4274.method24(-124),
					anInterface5_4274.method27(127) + class272.aByte2520);
			OpenGL.glEnableClientState(32885);
		}
		if (class272_203_ != null) {
			method1331(97, class272_203_.anInterface5_2521);
			OpenGL.glColorPointer(class272_203_.aByte2518, class272_203_.aShort2522, anInterface5_4274.method24(-125),
					anInterface5_4274.method27(127) - -(long) class272_203_.aByte2520);
			OpenGL.glEnableClientState(32886);
		} else {
			OpenGL.glDisableClientState(32886);
		}
		if (bool) {
			b(null, 69, -77);
		}
		if (class272_204_ != null) {
			method1331(118, class272_204_.anInterface5_2521);
			OpenGL.glTexCoordPointer(class272_204_.aByte2518, class272_204_.aShort2522,
					anInterface5_4274.method24(-127),
					anInterface5_4274.method27(127) - -(long) class272_204_.aByte2520);
			OpenGL.glEnableClientState(32888);
		} else {
			OpenGL.glDisableClientState(32888);
		}
	}

	@Override
	public int XA() {
		return anInt4253;
	}

	@Override
	public void X(int i) {
		anInt4139 = 0;
		while (i > 1) {
			i >>= 1;
			anInt4139++;
		}
		anInt4140 = 1 << anInt4139;
	}

	@Override
	public int M() {
		int i = anInt4276;
		anInt4276 = 0;
		return i;
	}

	@Override
	public void b(Canvas canvas) {
		aCanvas4124 = null;
		aLong4119 = 0L;
		if (canvas != null && aCanvas4122 != canvas) {
			if (aHashtable4116.containsKey(canvas)) {
				Long var_long = (Long) aHashtable4116.get(canvas);
				aLong4119 = var_long.longValue();
				aCanvas4124 = canvas;
			}
		} else {
			aCanvas4124 = aCanvas4122;
			aLong4119 = aLong4120;
		}
		if (aCanvas4124 == null || aLong4119 == 0L) {
			throw new RuntimeException();
		}
		anOpenGL4123.setSurface(aLong4119);
		method1343((byte) -98);
	}

	@Override
	public Sprite a(Class186 class186, boolean bool) {
		int[] is = new int[class186.anInt1899 * class186.anInt1904];
		int i = 0;
		int i_206_ = 0;
		if (class186.aByteArray1905 != null) {
			for (int i_207_ = 0; i_207_ < class186.anInt1904; i_207_++) {
				for (int i_208_ = 0; class186.anInt1899 > i_208_; i_208_++) {
					is[i_206_++] = Class48.bitOR(class186.anIntArray1900[255 & class186.aByteArray1901[i]],
							class186.aByteArray1905[i] << 24);
					i++;
				}
			}
		} else {
			for (int i_209_ = 0; class186.anInt1904 > i_209_; i_209_++) {
				for (int i_210_ = 0; i_210_ < class186.anInt1899; i_210_++) {
					int i_211_ = class186.anIntArray1900[class186.aByteArray1901[i++] & 0xff];
					is[i_206_++] = i_211_ == 0 ? 0 : Class48.bitOR(-16777216, i_211_);
				}
			}
		}
		Sprite class397 = this.method1096(119, 0, class186.anInt1904, class186.anInt1899, class186.anInt1899, is);
		class397.method4097(class186.anInt1903, class186.anInt1906, class186.anInt1907, class186.anInt1902);
		return class397;
	}

	@Override
	public void DA(int i, int i_212_, int i_213_, int i_214_) {
		anInt4263 = i_213_;
		anInt4227 = i_214_;
		anInt4246 = i_212_;
		anInt4215 = i;
		method1300(111);
		method1319(-89);
		if (anInt4192 == 3) {
			method1293(true);
		} else if (anInt4192 == 2) {
			method1349(5888);
		}
	}

	private void method1324(int i) {
		Packet.aFloatArray4675[0] = aFloat4261 * aFloat4272;
		Packet.aFloatArray4675[2] = aFloat4193 * aFloat4272;
		Packet.aFloatArray4675[1] = aFloat4272 * aFloat4186;
		Packet.aFloatArray4675[3] = 1.0F;
		OpenGL.glLightfv(16384, i, Packet.aFloatArray4675, 0);
		Packet.aFloatArray4675[0] = aFloat4261 * -aFloat4248;
		Packet.aFloatArray4675[2] = aFloat4193 * -aFloat4248;
		Packet.aFloatArray4675[3] = 1.0F;
		Packet.aFloatArray4675[1] = -aFloat4248 * aFloat4186;
		OpenGL.glLightfv(16385, 4609, Packet.aFloatArray4675, 0);
	}

	@Override
	public boolean B() {
		return true;
	}

	@Override
	public za b(int i) {
		za_Sub2 var_za_Sub2 = new za_Sub2(i);
		aClass155_4136.addLast((byte) -61, var_za_Sub2);
		return var_za_Sub2;
	}

	@Override
	public void y() {
		if (aBoolean4190) {
			if (anInterface4_4145 != aClass392_4146) {
				throw new RuntimeException();
			}
			aClass392_4146.method4047((byte) -125, 0);
			aClass392_4146.method4047((byte) -125, 8);
			method1280(aClass392_4146, -1);
		} else {
			if (!aBoolean4209) {
				throw new RuntimeException("");
			}
			aClass397_Sub3_4150.method4090(0, 0, anInt4114, anInt4113, 0, 0);
			anOpenGL4123.setSurface(aLong4119);
		}
		anInt4113 = anInt4117;
		aClass397_Sub3_4150 = null;
		anInt4114 = anInt4121;
		method1284(-31628);
		method1338(-4);
		la();
	}

	@Override
	public void a(float f, float f_215_, float f_216_) {
		Class318.aFloat2810 = f_216_;
		GameType.aFloat342 = f_215_;
		HardReferenceWrapper.aFloat6696 = f;
	}

	@Override
	public void HA(int i, int i_217_, int i_218_, int i_219_, int[] is) {
		float f = aClass373_Sub1_4168.aFloat5589 * i_218_
				+ (aClass373_Sub1_4168.aFloat5579 * i_217_ + aClass373_Sub1_4168.aFloat5587 * i)
				+ aClass373_Sub1_4168.aFloat5588;
		if (anInt4249 > f || anInt4253 < f) {
			is[0] = is[1] = is[2] = -1;
		} else {
			int i_220_ = (int) (anInt4263
					* (i_217_ * aClass373_Sub1_4168.aFloat5585 + aClass373_Sub1_4168.aFloat5577 * i
							+ aClass373_Sub1_4168.aFloat5581 * i_218_ + aClass373_Sub1_4168.aFloat5576)
					/ i_219_);
			int i_221_ = (int) ((aClass373_Sub1_4168.aFloat5582 + (i * aClass373_Sub1_4168.aFloat5584
					+ aClass373_Sub1_4168.aFloat5583 * i_217_ + i_218_ * aClass373_Sub1_4168.aFloat5586)) * anInt4227
					/ i_219_);
			if (i_220_ >= aFloat4216 && i_220_ <= aFloat4198 && aFloat4242 <= i_221_ && i_221_ <= aFloat4256) {
				is[0] = (int) (-aFloat4216 + i_220_);
				is[1] = (int) (i_221_ - aFloat4242);
				is[2] = (int) f;
			} else {
				is[0] = is[1] = is[2] = -1;
			}
		}
	}

	@Override
	public Interface11 a(int i, int i_222_) {
		return null;
	}

	@Override
	public void b(int i, int i_223_, int i_224_, int i_225_, double d) {
		/* empty */
	}

	private void method1325(byte i) {
		method1340(-2, (byte) 92);
		for (int i_226_ = anInt4224 - 1; i_226_ >= 0; i_226_--) {
			method1330(118, i_226_);
			method1316(null, (byte) -123);
			OpenGL.glTexEnvi(8960, 8704, 34160);
		}
		method1306(8448, 8448, -22394);
		method1283(2, 34168, 770, (byte) -128);
		method1318((byte) -9);
		anInt4163 = 1;
		OpenGL.glEnable(3042);
		OpenGL.glBlendFunc(770, 771);
		anInt4164 = 1;
		OpenGL.glEnable(3008);
		OpenGL.glAlphaFunc(516, 0.0F);
		aBoolean4169 = true;
		if (i != -38) {
			b((Canvas) null);
		}
		OpenGL.glColorMask(true, true, true, true);
		aBoolean4161 = true;
		method1311(true, 8577);
		method1278(i ^ ~0x5c9ef5ed, true);
		method1345(true, 89);
		method1285(126, true);
		method1284(-31628);
		anOpenGL4123.setSwapInterval(0);
		OpenGL.glShadeModel(7425);
		OpenGL.glClearDepth(1.0F);
		OpenGL.glDepthFunc(515);
		OpenGL.glPolygonMode(1028, 6914);
		OpenGL.glEnable(2884);
		OpenGL.glCullFace(1029);
		OpenGL.glMatrixMode(5888);
		OpenGL.glLoadIdentity();
		OpenGL.glColorMaterial(1028, 5634);
		OpenGL.glEnable(2903);
		float[] fs = { 0.0F, 0.0F, 0.0F, 1.0F };
		for (int i_227_ = 0; i_227_ < 8; i_227_++) {
			int i_228_ = i_227_ + 16384;
			OpenGL.glLightfv(i_228_, 4608, fs, 0);
			OpenGL.glLightf(i_228_, 4615, 0.0F);
			OpenGL.glLightf(i_228_, 4616, 0.0F);
		}
		OpenGL.glEnable(16384);
		OpenGL.glEnable(16385);
		OpenGL.glFogf(2914, 0.95F);
		OpenGL.glFogi(2917, 9729);
		OpenGL.glHint(3156, 4353);
		anInt4191 = anInt4250 = -1;
		la();
	}

	ha_Sub3(Canvas canvas, d var_d, int i) {
		super(var_d);
		aClass173_4129 = new Class173();
		aClass373_Sub1_4132 = new Class373_Sub1();
		aClass373_Sub1_4133 = new Class373_Sub1();
		aBoolean4135 = false;
		anInt4139 = 3;
		anInt4140 = 8;
		aClass155_4136 = new NodeDeque();
		anInt4141 = -1;
		anInt4147 = -1;
		anInterface4Array4149 = new Interface4[4];
		anInterface4Array4148 = new Interface4[4];
		anInterface4Array4143 = new Interface4[4];
		anInt4142 = -1;
		new Queue();
		new HashTable(16);
		aClass155_4153 = new NodeDeque();
		aClass155_4155 = new NodeDeque();
		aClass155_4156 = new NodeDeque();
		aClass155_4157 = new NodeDeque();
		aClass155_4158 = new NodeDeque();
		aClass155_4159 = new NodeDeque();
		aClass155_4160 = new NodeDeque();
		aClass373_Sub1_4168 = new Class373_Sub1();
		aClass373_Sub1_4170 = new Class373_Sub1();
		aClass373_Sub1_4171 = new Class373_Sub1();
		anInt4191 = -1;
		aFloat4182 = 1.0F;
		anInt4181 = -1;
		aFloatArray4202 = new float[4];
		anInt4178 = -1;
		aFloat4210 = 3584.0F;
		aFloat4193 = 1.0F;
		anInt4185 = 0;
		anInt4227 = 512;
		anInt4215 = 0;
		anInt4211 = 0;
		aFloat4226 = 3584.0F;
		aClass178_Sub2Array4222 = new Class178_Sub2[7];
		aClass296_Sub35Array4220 = new Class296_Sub35[Class123_Sub2_Sub1.anInt5818];
		anInt4173 = -1;
		anInt4223 = 0;
		anInt4238 = 0;
		aFloatArray4199 = new float[4];
		aBoolean4230 = false;
		anInt4244 = 0;
		aFloat4180 = 0.0F;
		aFloatArray4197 = new float[4];
		anInt4250 = -1;
		anInt4249 = 50;
		aFloat4262 = -1.0F;
		anInt4176 = 0;
		aFloat4186 = 1.0F;
		anInt4263 = 512;
		aClass178_Sub2Array4214 = new Class178_Sub2[7];
		anInt4265 = 0;
		aFloat4259 = -1.0F;
		anInt4201 = 0;
		anInt4264 = 8448;
		aFloat4269 = 1.0F;
		aFloat4248 = -1.0F;
		aFloat4261 = 1.0F;
		aFloatArray4267 = new float[16];
		aFloat4272 = -1.0F;
		aBoolean4247 = true;
		aFloatArray4273 = new float[4];
		anInt4271 = 8448;
		anInt4246 = 0;
		anInt4253 = 3584;
		aClass296_Sub17_Sub2_4194 = new Class296_Sub17_Sub2(8192);
		anIntArray4275 = new int[1];
		aByteArray4278 = new byte[16384];
		anIntArray4280 = new int[1];
		anIntArray4277 = new int[1];
		aCanvas4124 = aCanvas4122 = canvas;
		anInt4128 = i;
		if (!Class366_Sub4.method3779("jaclib", (byte) -17)) {
			throw new RuntimeException("");
		}
		if (!Class366_Sub4.method3779("jaggl", (byte) -17)) {
			throw new RuntimeException("");
		}
		try {
			anOpenGL4123 = new OpenGL();
			aLong4119 = aLong4120 = anOpenGL4123.init(canvas, 8, 8, 8, 24, 0, anInt4128);
			if (aLong4120 == 0L) {
				throw new RuntimeException("");
			}
			method1332((byte) -4);
			int i_229_ = method1298(120);
			if (i_229_ != 0) {
				throw new RuntimeException("");
			}
			anInt4231 = !aBoolean4184 ? 5121 : 33639;
			if (aString4188.indexOf("radeon") != -1) {
				int i_230_ = 0;
				boolean bool = false;
				boolean bool_231_ = false;
				String[] strings = Class41_Sub30.method522((byte) 63, aString4188.replace('/', ' '), ' ');
				for (int i_232_ = 0; i_232_ < strings.length; i_232_++) {
					String string = strings[i_232_];
					try {
						if (string.length() > 0) {
							if (string.charAt(0) == 'x' && string.length() >= 3
									&& EquipmentData.method3305((byte) 90, string.substring(1, 3))) {
								string = string.substring(1);
								bool_231_ = true;
							}
							if (string.equals("hd")) {
								bool = true;
							} else {
								if (string.startsWith("hd")) {
									string = string.substring(2);
									bool = true;
								}
								if (string.length() >= 4
										&& EquipmentData.method3305((byte) 120, string.substring(0, 4))) {
									i_230_ = Class366_Sub2.method3774(-88, string.substring(0, 4));
									break;
								}
							}
						}
					} catch (Exception exception) {
						/* empty */
					}
				}
				if (!bool_231_ && !bool) {
					if (i_230_ >= 7000 && i_230_ <= 9250) {
						aBoolean4225 = false;
					}
					if (i_230_ >= 7000 && i_230_ <= 7999) {
						aBoolean4252 = false;
					}
				}
				if (!bool || i_230_ < 4000) {
					aBoolean4189 = false;
				}
				aBoolean4257 &= anOpenGL4123.a("GL_ARB_half_float_pixel");
				aBoolean4177 = aBoolean4252;
				aBoolean4175 = true;
			}
			if (aString4255.indexOf("intel") != -1) {
				aBoolean4190 = false;
			}
			aBoolean4243 = !aString4255.equals("s3 graphics");
			if (aBoolean4252) {
				try {
					int[] is = new int[1];
					OpenGL.glGenBuffersARB(1, is, 0);
				} catch (Throwable throwable) {
					throw new RuntimeException("");
				}
			}
			Class338.method3437(true, false, false);
			aBoolean4135 = true;
			aClass49_4125 = new Class49(this, aD1299);
			method1304(true);
			aClass91_4137 = new Class91(this);
			aClass342_4127 = new Class342(this);
			if (aClass342_4127.method3632((byte) -88)) {
				aClass296_Sub9_Sub1_4130 = new Class296_Sub9_Sub1(this);
				if (!aClass296_Sub9_Sub1_4130.method2492(11684)) {
					aClass296_Sub9_Sub1_4130.method2482((byte) -127);
					aClass296_Sub9_Sub1_4130 = null;
				}
			}
			aClass197_4126 = new Class197(this);
			method1325((byte) -38);
			method1343((byte) -98);
			w();
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			t();
			throw new RuntimeException("");
		}
	}

	@Override
	public boolean n() {
		if (aClass296_Sub9_Sub1_4130 == null || anInt4128 > 1 && !aBoolean4239) {
			return false;
		}
		return true;
	}

	public synchronized void method1326(int i, int i_233_, int i_234_) {
		IntegerNode class296_sub16 = new IntegerNode(i_233_);
		class296_sub16.uid = i;
		aClass155_4158.addLast((byte) 100, class296_sub16);
		if (i_234_ != 1485573640) {
			anInterface4Array4148 = null;
		}
	}

	@Override
	public Sprite a(int i, int i_235_, boolean bool) {
		return new Class397_Sub3(this, i, i_235_, bool);
	}

	@Override
	public int e(int i, int i_236_) {
		return i & i_236_ ^ i_236_;
	}

	@Override
	public void a(int i, int i_237_, int i_238_, int i_239_, int i_240_, int i_241_, aa var_aa, int i_242_, int i_243_,
			int i_244_, int i_245_, int i_246_) {
		if (i != i_238_ || i_237_ != i_239_) {
			aa_Sub1 var_aa_Sub1 = (aa_Sub1) var_aa;
			Class69_Sub1_Sub1 class69_sub1_sub1 = var_aa_Sub1.aClass69_Sub1_Sub1_3720;
			method1305((byte) -112);
			method1316(var_aa_Sub1.aClass69_Sub1_Sub1_3720, (byte) -101);
			method1336((byte) 20, i_241_);
			method1306(7681, 8448, -22394);
			method1283(0, 34167, 768, (byte) -114);
			float f = class69_sub1_sub1.aFloat6687 / class69_sub1_sub1.anInt6683;
			float f_247_ = class69_sub1_sub1.aFloat6684 / class69_sub1_sub1.anInt6688;
			float f_248_ = (float) -i + (float) i_238_;
			float f_249_ = (float) i_239_ - (float) i_237_;
			float f_250_ = (float) (1.0 / Math.sqrt(f_248_ * f_248_ + f_249_ * f_249_));
			f_249_ *= f_250_;
			OpenGL.glColor4ub((byte) (i_240_ >> 16), (byte) (i_240_ >> 8), (byte) i_240_, (byte) (i_240_ >> 24));
			f_248_ *= f_250_;
			i_246_ %= i_245_ + i_244_;
			float f_251_ = f_248_ * i_244_;
			float f_252_ = i_244_ * f_249_;
			float f_253_ = 0.0F;
			float f_254_ = 0.0F;
			float f_255_ = f_251_;
			float f_256_ = f_252_;
			if (i_246_ > i_244_) {
				f_254_ = f_249_ * (i_245_ + i_244_ - i_246_);
				f_253_ = (i_244_ + i_245_ - i_246_) * f_248_;
			} else {
				f_255_ = (i_244_ - i_246_) * f_248_;
				f_256_ = f_249_ * (i_244_ - i_246_);
			}
			float f_257_ = i + 0.35F + f_253_;
			float f_258_ = i_237_ + 0.35F + f_254_;
			float f_259_ = f_248_ * i_245_;
			float f_260_ = i_245_ * f_249_;
			for (;;) {
				if (i < i_238_) {
					if (i_238_ + 0.35F < f_257_) {
						break;
					}
					if (f_257_ + f_255_ > i_238_) {
						f_255_ = i_238_ - f_257_;
					}
				} else {
					if (i_238_ + 0.35F > f_257_) {
						break;
					}
					if (f_257_ + f_255_ < i_238_) {
						f_255_ = i_238_ - f_257_;
					}
				}
				if (i_239_ <= i_237_) {
					if (i_239_ + 0.35F > f_258_) {
						break;
					}
					if (f_256_ + f_258_ < i_239_) {
						f_256_ = -f_258_ + i_239_;
					}
				} else {
					if (i_239_ + 0.35F < f_258_) {
						break;
					}
					if (f_256_ + f_258_ > i_239_) {
						f_256_ = i_239_ - f_258_;
					}
				}
				OpenGL.glBegin(1);
				OpenGL.glTexCoord2f((f_257_ - i_242_) * f, (-i_243_ + f_258_) * f_247_);
				OpenGL.glVertex2f(f_257_, f_258_);
				OpenGL.glTexCoord2f((f_255_ + f_257_ - i_242_) * f, (f_258_ + f_256_ - i_243_) * f_247_);
				OpenGL.glVertex2f(f_257_ + f_255_, f_258_ + f_256_);
				f_257_ += f_255_ + f_259_;
				OpenGL.glEnd();
				f_258_ += f_256_ + f_260_;
				f_255_ = f_251_;
				f_256_ = f_252_;
			}
			method1283(0, 5890, 768, (byte) -109);
		}
	}

	public void method1327(boolean bool, boolean bool_261_) {
		if (bool_261_ != aBoolean4236) {
			aBoolean4236 = bool_261_;
			method1309(125);
		}
		if (bool != true) {
			/* empty */
		}
	}

	private void method1328(int i) {
		OpenGL.glDepthMask(aBoolean4166 && aBoolean4247);
		if (i != 1) {
			j();
		}
	}

	public Class69_Sub3 method1329(byte i) {
		if (i != 8) {
			return null;
		}
		if (aClass241_Sub2_4206 != null) {
			return aClass241_Sub2_4206.method2157(false);
		}
		return null;
	}

	@Override
	public void a(Class241 class241) {
		aClass241_Sub2_4206 = (Class241_Sub2) class241;
	}

	public void method1330(int i, int i_262_) {
		if (anInt4221 != i_262_) {
			OpenGL.glActiveTexture(i_262_ + 33984);
			anInt4221 = i_262_;
		}
		if (i <= 105) {
			anInterface5_4204 = null;
		}
	}

	public void method1331(int i, Interface5 interface5) {
		if (i < 48) {
			aFloat4216 = -0.37967762F;
		}
		if (interface5 != anInterface5_4274) {
			if (aBoolean4252) {
				OpenGL.glBindBufferARB(34962, interface5.method26(19442));
			}
			anInterface5_4274 = interface5;
		}
	}

	private void method1332(byte i) {
		int i_263_ = 0;
		while (!anOpenGL4123.b()) {
			if (i_263_++ > 5) {
				throw new RuntimeException("");
			}
			Class106_Sub1.method942(1000L, 0);
		}
		int i_264_ = 20 / ((62 - i) / 46);
	}

	public void method1333(Interface4 interface4, byte i) {
		if (anInt4141 >= 3) {
			throw new RuntimeException();
		}
		if (i != -12) {
			method1320(-69);
		}
		if (anInt4141 >= 0) {
			anInterface4Array4149[anInt4141].method19(i ^ 0x53);
		}
		anInterface4_4144 = anInterface4Array4149[++anInt4141] = interface4;
		anInterface4_4144.method22(-18449);
	}

	@Override
	public int JA(int i, int i_265_, int i_266_, int i_267_, int i_268_, int i_269_) {
		int i_270_ = 0;
		float f = aClass373_Sub1_4168.aFloat5588 + (aClass373_Sub1_4168.aFloat5587 * i
				+ i_265_ * aClass373_Sub1_4168.aFloat5579 + i_266_ * aClass373_Sub1_4168.aFloat5589);
		if (f < 1.0F) {
			f = 1.0F;
		}
		float f_271_ = aClass373_Sub1_4168.aFloat5579 * i_268_ + i_267_ * aClass373_Sub1_4168.aFloat5587
				+ i_269_ * aClass373_Sub1_4168.aFloat5589 + aClass373_Sub1_4168.aFloat5588;
		if (f_271_ < 1.0F) {
			f_271_ = 1.0F;
		}
		if (f < anInt4249 && anInt4249 > f_271_) {
			i_270_ |= 0x10;
		} else if (anInt4253 < f && anInt4253 < f_271_) {
			i_270_ |= 0x20;
		}
		int i_272_ = (int) (anInt4263 * (aClass373_Sub1_4168.aFloat5581 * i_266_
				+ (aClass373_Sub1_4168.aFloat5577 * i + i_265_ * aClass373_Sub1_4168.aFloat5585)
				+ aClass373_Sub1_4168.aFloat5576) / f);
		int i_273_ = (int) (anInt4263 * (aClass373_Sub1_4168.aFloat5581 * i_269_
				+ (i_267_ * aClass373_Sub1_4168.aFloat5577 + aClass373_Sub1_4168.aFloat5585 * i_268_)
				+ aClass373_Sub1_4168.aFloat5576) / f_271_);
		if (!(aFloat4216 > i_272_) || !(aFloat4216 > i_273_)) {
			if (i_272_ > aFloat4198 && aFloat4198 < i_273_) {
				i_270_ |= 0x2;
			}
		} else {
			i_270_ |= 0x1;
		}
		int i_274_ = (int) (anInt4227 * (i_266_ * aClass373_Sub1_4168.aFloat5586
				+ (aClass373_Sub1_4168.aFloat5584 * i + i_265_ * aClass373_Sub1_4168.aFloat5583)
				+ aClass373_Sub1_4168.aFloat5582) / f);
		int i_275_ = (int) (anInt4227 * (aClass373_Sub1_4168.aFloat5582 + (aClass373_Sub1_4168.aFloat5584 * i_267_
				+ aClass373_Sub1_4168.aFloat5583 * i_268_ + aClass373_Sub1_4168.aFloat5586 * i_269_)) / f_271_);
		if (i_274_ < aFloat4242 && i_275_ < aFloat4242) {
			i_270_ |= 0x4;
		} else if (aFloat4256 < i_274_ && aFloat4256 < i_275_) {
			i_270_ |= 0x8;
		}
		return i_270_;
	}

	@Override
	public void EA(int i, int i_276_, int i_277_, int i_278_) {
		if (!underwater_mode) {
			throw new RuntimeException("");
		}
		anInt4173 = i_277_;
		anInt4211 = i_278_;
		anInt4200 = i;
		anInt4178 = i_276_;
		if (aBoolean4230) {
			aClass197_4126.aClass366_Sub7_1994.method3791(true);
			aClass197_4126.aClass366_Sub7_1994.method3790(108);
		}
	}

	private void method1334(float f, float f_279_, int i, float f_280_) {
		OpenGL.glMatrixMode(5890);
		if (aBoolean4232) {
			OpenGL.glLoadIdentity();
		}
		OpenGL.glTranslatef(f_279_, f_280_, f);
		OpenGL.glMatrixMode(5888);
		int i_281_ = -22 / ((5 - i) / 40);
		aBoolean4232 = true;
	}

	@Override
	public boolean d() {
		if (!aBoolean4207 || b() && !aBoolean4239) {
			return false;
		}
		return true;
	}

	@Override
	public void a(Class390 class390) {
		aClass173_4129.method1690(-1, this, class390, 11321);
	}

	@Override
	public int q() {
		return 4;
	}

	@Override
	public void da(int i, int i_282_, int i_283_, int[] is) {
		float f = aClass373_Sub1_4168.aFloat5588 + (aClass373_Sub1_4168.aFloat5579 * i_282_
				+ aClass373_Sub1_4168.aFloat5587 * i + aClass373_Sub1_4168.aFloat5589 * i_283_);
		if (f < anInt4249 || f > anInt4253) {
			is[0] = is[1] = is[2] = -1;
		} else {
			int i_284_ = (int) ((aClass373_Sub1_4168.aFloat5576 + (aClass373_Sub1_4168.aFloat5581 * i_283_
					+ (aClass373_Sub1_4168.aFloat5585 * i_282_ + i * aClass373_Sub1_4168.aFloat5577))) * anInt4263 / f);
			int i_285_ = (int) ((aClass373_Sub1_4168.aFloat5582 + (aClass373_Sub1_4168.aFloat5584 * i
					+ aClass373_Sub1_4168.aFloat5583 * i_282_ + aClass373_Sub1_4168.aFloat5586 * i_283_)) * anInt4227
					/ f);
			if (i_284_ >= aFloat4216 && aFloat4198 >= i_284_ && i_285_ >= aFloat4242 && i_285_ <= aFloat4256) {
				is[2] = (int) f;
				is[0] = (int) (i_284_ - aFloat4216);
				is[1] = (int) (i_285_ - aFloat4242);
			} else {
				is[0] = is[1] = is[2] = -1;
			}
		}
	}

	@Override
	public Class241 c(int i, int i_286_, int i_287_, int i_288_, int i_289_, int i_290_) {
		if (!aBoolean4251) {
			return null;
		}
		return new Class241_Sub2_Sub1(this, i, i_286_, i_287_, i_288_, i_289_, i_290_);
	}

	@Override
	public aa a(int i, int i_291_, int[] is, int[] is_292_) {
		return Class296_Sub57.method3219(is_292_, i, -109, is, this, i_291_);
	}

	@Override
	public void h() {
		if (aClass296_Sub9_Sub1_4130 != null && aClass296_Sub9_Sub1_4130.method2488(-17671)) {
			aClass342_4127.method3630(aClass296_Sub9_Sub1_4130, (byte) 11);
			aClass49_4125.method605(3553);
		}
	}

	@Override
	public void a(Class373 class373) {
		aClass373_Sub1_4168.method3915(class373);
		aClass373_Sub1_4170.method3915(aClass373_Sub1_4168);
		aClass373_Sub1_4170.method3918(3);
		aClass373_Sub1_4171.method3924((byte) -85, aClass373_Sub1_4170);
		if (anInt4192 != 1) {
			method1352(3008);
		}
	}

	public void method1335(int i, Interface4 interface4) {
		if (i > anInt4147 || interface4 != anInterface4Array4148[anInt4147]) {
			throw new RuntimeException();
		}
		anInterface4Array4148[anInt4147--] = null;
		interface4.method23(i ^ 0x65bc);
		if (anInt4147 >= 0) {
			anInterface4_4145 = anInterface4Array4148[anInt4147];
			anInterface4_4145.method21(false);
		} else {
			anInterface4_4145 = null;
		}
	}

	@Override
	public void a(int[] is) {
		is[1] = anInt4113;
		is[0] = anInt4114;
	}

	@Override
	public void c() {
		aClass342_4127.method3629(102);
	}

	@Override
	public Class241 a(Class241 class241, Class241 class241_293_, float f, Class241 class241_294_) {
		if (class241 != null && class241_293_ != null && aBoolean4251 && aBoolean4190) {
			Class241_Sub2_Sub2 class241_sub2_sub2 = null;
			Class241_Sub2 class241_sub2 = (Class241_Sub2) class241;
			Class241_Sub2 class241_sub2_295_ = (Class241_Sub2) class241_293_;
			Class69_Sub3 class69_sub3 = class241_sub2.method2157(false);
			Class69_Sub3 class69_sub3_296_ = class241_sub2_295_.method2157(false);
			if (class69_sub3 != null && class69_sub3_296_ != null) {
				int i = class69_sub3.anInt5725 <= class69_sub3_296_.anInt5725 ? class69_sub3_296_.anInt5725
						: class69_sub3.anInt5725;
				if (class241 != class241_294_ && class241_294_ != class241_293_
						&& class241_294_ instanceof Class241_Sub2_Sub2) {
					Class241_Sub2_Sub2 class241_sub2_sub2_297_ = (Class241_Sub2_Sub2) class241_294_;
					if (i == class241_sub2_sub2_297_.method2164((byte) -119)) {
						class241_sub2_sub2 = class241_sub2_sub2_297_;
					}
				}
				if (class241_sub2_sub2 == null) {
					class241_sub2_sub2 = new Class241_Sub2_Sub2(this, i);
				}
				if (class241_sub2_sub2.method2168(class69_sub3_296_, class69_sub3, f, -65535)) {
					return class241_sub2_sub2;
				}
			}
		}
		if (!(f < 0.5F)) {
			return class241_293_;
		}
		return class241;
	}

	@Override
	public boolean x() {
		return true;
	}

	public void method1336(byte i, int i_298_) {
		if (anInt4163 != i_298_) {
			int i_299_;
			boolean bool;
			boolean bool_300_;
			if (i_298_ == 1) {
				bool_300_ = true;
				i_299_ = 1;
				bool = true;
			} else if (i_298_ != 2) {
				if (i_298_ != 128) {
					i_299_ = 0;
					bool = true;
					bool_300_ = false;
				} else {
					bool_300_ = true;
					i_299_ = 3;
					bool = true;
				}
			} else {
				bool = true;
				bool_300_ = false;
				i_299_ = 2;
			}
			if (bool == !aBoolean4161) {
				OpenGL.glColorMask(bool, bool, bool, true);
				aBoolean4161 = bool;
			}
			if (!aBoolean4169 == bool_300_) {
				if (!bool_300_) {
					OpenGL.glDisable(3008);
				} else {
					OpenGL.glEnable(3008);
				}
				aBoolean4169 = bool_300_;
			}
			if (anInt4164 != i_299_) {
				if (i_299_ != 1) {
					if (i_299_ == 2) {
						OpenGL.glEnable(3042);
						OpenGL.glBlendFunc(1, 1);
					} else if (i_299_ == 3) {
						OpenGL.glEnable(3042);
						OpenGL.glBlendFunc(774, 1);
					} else {
						OpenGL.glDisable(3042);
					}
				} else {
					OpenGL.glEnable(3042);
					OpenGL.glBlendFunc(770, 771);
				}
				anInt4164 = i_299_;
			}
			anInt4162 &= ~0x1c;
			anInt4163 = i_298_;
		}
		int i_301_ = -2 % ((-54 - i) / 33);
	}

	public void method1337(byte i) {
		if (anInt4162 != 4) {
			method1341(true);
			method1311(false, 8577);
			method1278(1553921480, false);
			method1345(false, 118);
			method1285(103, false);
			method1340(-2, (byte) 111);
			method1336((byte) 93, 1);
			anInt4162 = 4;
		}
		if (i != 37) {
			aBoolean4229 = true;
		}
	}

	private void method1338(int i) {
		OpenGL.glViewport(anInt4265, anInt4176, anInt4114, anInt4113);
		if (i != -4) {
			/* empty */
		}
	}

	public void method1339(int i) {
		OpenGL.glLightfv(16384, 4611, aFloatArray4202, 0);
		if (i != 8705) {
			aByteArray4278 = null;
		}
		OpenGL.glLightfv(16385, 4611, aFloatArray4273, 0);
	}

	public void method1340(int i, byte i_302_) {
		if (i_302_ < 41) {
			a((za) null);
		}
		method1350(true, (byte) -119, i);
	}

	@Override
	public void a(int i, int i_303_, int i_304_, int i_305_) {
		aClass342_4127.method3635(i_303_, i_304_, i, i_305_, false);
	}

	private void method1341(boolean bool) {
		if (bool != true) {
			anInt4191 = -66;
		}
		if (anInt4192 != 1) {
			OpenGL.glMatrixMode(5889);
			OpenGL.glLoadIdentity();
			if (anInt4114 > 0 && anInt4113 > 0) {
				OpenGL.glOrtho(0.0, anInt4114, anInt4113, 0.0, -1.0, 1.0);
			}
			OpenGL.glMatrixMode(5888);
			OpenGL.glLoadIdentity();
			anInt4162 &= ~0x18;
			anInt4192 = 1;
		}
	}

	public synchronized void method1342(int i, int i_306_, int i_307_) {
		IntegerNode class296_sub16 = new IntegerNode(i_307_);
		if (i > -96) {
			aa(36, 71, 112, -97, 109, -100);
		}
		class296_sub16.uid = i_306_;
		aClass155_4156.addLast((byte) 114, class296_sub16);
	}

	private void method1343(byte i) {
		if (i != -98) {
			aFloatArray4273 = null;
		}
		if (aCanvas4124 != null) {
			Dimension dimension = aCanvas4124.getSize();
			anInt4117 = dimension.height;
			anInt4121 = dimension.width;
		} else {
			anInt4121 = anInt4117 = 0;
		}
		if (anInterface4_4145 == null) {
			anInt4113 = anInt4117;
			anInt4114 = anInt4121;
			method1338(i ^ 0x62);
		}
		method1284(-31628);
		la();
	}

	@Override
	public int r(int i, int i_308_, int i_309_, int i_310_, int i_311_, int i_312_, int i_313_) {
		float f = aClass373_Sub1_4168.aFloat5588 + (aClass373_Sub1_4168.aFloat5589 * i_309_
				+ (aClass373_Sub1_4168.aFloat5579 * i_308_ + i * aClass373_Sub1_4168.aFloat5587));
		float f_314_ = aClass373_Sub1_4168.aFloat5588 + (aClass373_Sub1_4168.aFloat5579 * i_311_
				+ i_310_ * aClass373_Sub1_4168.aFloat5587 + i_312_ * aClass373_Sub1_4168.aFloat5589);
		int i_315_ = 0;
		if (anInt4249 > f && anInt4249 > f_314_) {
			i_315_ |= 0x10;
		} else if (anInt4253 < f && f_314_ > anInt4253) {
			i_315_ |= 0x20;
		}
		int i_316_ = (int) ((aClass373_Sub1_4168.aFloat5576 + (aClass373_Sub1_4168.aFloat5577 * i
				+ i_308_ * aClass373_Sub1_4168.aFloat5585 + aClass373_Sub1_4168.aFloat5581 * i_309_)) * anInt4263
				/ i_313_);
		int i_317_ = (int) (anInt4263
				* (aClass373_Sub1_4168.aFloat5576 + (aClass373_Sub1_4168.aFloat5581 * i_312_
						+ (aClass373_Sub1_4168.aFloat5585 * i_311_ + i_310_ * aClass373_Sub1_4168.aFloat5577)))
				/ i_313_);
		if (aFloat4216 > i_316_ && i_317_ < aFloat4216) {
			i_315_ |= 0x1;
		} else if (aFloat4198 < i_316_ && i_317_ > aFloat4198) {
			i_315_ |= 0x2;
		}
		int i_318_ = (int) ((aClass373_Sub1_4168.aFloat5583 * i_308_ + aClass373_Sub1_4168.aFloat5584 * i
				+ aClass373_Sub1_4168.aFloat5586 * i_309_ + aClass373_Sub1_4168.aFloat5582) * anInt4227 / i_313_);
		int i_319_ = (int) (anInt4227 * (i_312_ * aClass373_Sub1_4168.aFloat5586
				+ (i_310_ * aClass373_Sub1_4168.aFloat5584 + aClass373_Sub1_4168.aFloat5583 * i_311_)
				+ aClass373_Sub1_4168.aFloat5582) / i_313_);
		if (!(i_318_ < aFloat4242) || !(aFloat4242 > i_319_)) {
			if (i_318_ > aFloat4256 && aFloat4256 < i_319_) {
				i_315_ |= 0x8;
			}
		} else {
			i_315_ |= 0x4;
		}
		return i_315_;
	}

	@Override
	public boolean v() {
		return true;
	}

	@Override
	public void ra(int i, int i_320_, int i_321_, int i_322_) {
		anInt4173 = i_321_;
		anInt4211 = i_322_;
		anInt4178 = i_320_;
		underwater_mode = true;
		anInt4200 = i;
	}

	@Override
	public boolean z() {
		return true;
	}

	@Override
	public Class296_Sub35 a(int i, int i_323_, int i_324_, int i_325_, int i_326_, float f) {
		return new Class296_Sub35_Sub3(i, i_323_, i_324_, i_325_, i_326_, f);
	}

	private void method1344(int i) {
		aFloat4234 = -aFloat4180 + (-anInt4223 + anInt4253);
		aFloat4237 = -(aFloat4269 * anInt4181) + aFloat4234;
		if (aFloat4237 < anInt4249) {
			aFloat4237 = anInt4249;
		}
		OpenGL.glFogf(2915, aFloat4237);
		OpenGL.glFogf(2916, aFloat4234);
		Packet.aFloatArray4675[i] = (16711680 & anInt4250) / 1.671168E7F;
		Packet.aFloatArray4675[2] = (255 & anInt4250) / 255.0F;
		Packet.aFloatArray4675[1] = (65280 & anInt4250) / 65280.0F;
		OpenGL.glFogfv(2918, Packet.aFloatArray4675, 0);
	}

	@Override
	public void pa() {
		underwater_mode = false;
	}

	public void method1345(boolean bool, int i) {
		if (!aBoolean4167 != !bool) {
			if (!bool) {
				OpenGL.glDisable(2929);
			} else {
				OpenGL.glEnable(2929);
			}
			anInt4162 &= ~0x1f;
			aBoolean4167 = bool;
		}
		if (i <= 47) {
			U(-88, 61, -118, 68, -9);
		}
	}

	@Override
	public void a(za var_za) {
		aNativeHeap4138 = ((za_Sub2) var_za).aNativeHeap6556;
		if (anInterface5_4204 == null) {
			Class296_Sub17_Sub2 class296_sub17_sub2 = new Class296_Sub17_Sub2(80);
			if (aBoolean4184) {
				class296_sub17_sub2.method2636((byte) 82, -1.0F);
				class296_sub17_sub2.method2636((byte) 89, -1.0F);
				class296_sub17_sub2.method2636((byte) 110, 0.0F);
				class296_sub17_sub2.method2636((byte) 71, 0.0F);
				class296_sub17_sub2.method2636((byte) 81, 1.0F);
				class296_sub17_sub2.method2636((byte) 68, 1.0F);
				class296_sub17_sub2.method2636((byte) 107, -1.0F);
				class296_sub17_sub2.method2636((byte) 88, 0.0F);
				class296_sub17_sub2.method2636((byte) 108, 1.0F);
				class296_sub17_sub2.method2636((byte) 122, 1.0F);
				class296_sub17_sub2.method2636((byte) 103, 1.0F);
				class296_sub17_sub2.method2636((byte) 82, 1.0F);
				class296_sub17_sub2.method2636((byte) 62, 0.0F);
				class296_sub17_sub2.method2636((byte) 116, 1.0F);
				class296_sub17_sub2.method2636((byte) 96, 0.0F);
				class296_sub17_sub2.method2636((byte) 94, -1.0F);
				class296_sub17_sub2.method2636((byte) 123, 1.0F);
				class296_sub17_sub2.method2636((byte) 55, 0.0F);
				class296_sub17_sub2.method2636((byte) 54, 0.0F);
				class296_sub17_sub2.method2636((byte) 82, 0.0F);
			} else {
				class296_sub17_sub2.method2637((byte) -81, -1.0F);
				class296_sub17_sub2.method2637((byte) -101, -1.0F);
				class296_sub17_sub2.method2637((byte) -91, 0.0F);
				class296_sub17_sub2.method2637((byte) -98, 0.0F);
				class296_sub17_sub2.method2637((byte) -95, 1.0F);
				class296_sub17_sub2.method2637((byte) -120, 1.0F);
				class296_sub17_sub2.method2637((byte) -98, -1.0F);
				class296_sub17_sub2.method2637((byte) -92, 0.0F);
				class296_sub17_sub2.method2637((byte) -106, 1.0F);
				class296_sub17_sub2.method2637((byte) -96, 1.0F);
				class296_sub17_sub2.method2637((byte) -80, 1.0F);
				class296_sub17_sub2.method2637((byte) -107, 1.0F);
				class296_sub17_sub2.method2637((byte) -125, 0.0F);
				class296_sub17_sub2.method2637((byte) -87, 1.0F);
				class296_sub17_sub2.method2637((byte) -104, 0.0F);
				class296_sub17_sub2.method2637((byte) -103, -1.0F);
				class296_sub17_sub2.method2637((byte) -118, 1.0F);
				class296_sub17_sub2.method2637((byte) -99, 0.0F);
				class296_sub17_sub2.method2637((byte) -79, 0.0F);
				class296_sub17_sub2.method2637((byte) -100, 0.0F);
			}
			anInterface5_4204 = method1321(class296_sub17_sub2.pos, false, 20, class296_sub17_sub2.data, 5888);
			aClass272_4233 = new Class272(anInterface5_4204, 5126, 3, 0);
			aClass272_4270 = new Class272(anInterface5_4204, 5126, 2, 12);
			aClass173_4129.method1688(1, this);
		}
	}

	public void method1346(boolean bool, int i, int i_327_, int i_328_) {
		OpenGL.glTexEnvi(8960, i_327_ + 34184, i_328_);
		if (bool != true) {
			C(true);
		}
		OpenGL.glTexEnvi(8960, i_327_ + 34200, i);
	}

	@Override
	public void T(int i, int i_329_, int i_330_, int i_331_) {
		if (i_330_ < anInt4238) {
			anInt4238 = i_330_;
		}
		if (i_329_ > anInt4201) {
			anInt4201 = i_329_;
		}
		if (i_331_ < anInt4244) {
			anInt4244 = i_331_;
		}
		if (anInt4185 < i) {
			anInt4185 = i;
		}
		OpenGL.glEnable(3089);
		method1319(-100);
		method1287((byte) -51);
	}

	public Interface5 method1347(int i, Buffer buffer, boolean bool, int i_332_, int i_333_) {
		if (aBoolean4252 && (!bool || aBoolean4177)) {
			return new Class355_Sub2(this, i_332_, buffer, i_333_, bool);
		}
		if (i != -1) {
			method1338(51);
		}
		return new Class295_Sub2(this, i_332_, buffer);
	}

	@Override
	public void a(int i, int i_334_, int i_335_, int i_336_, int i_337_, int i_338_, int i_339_, int i_340_, int i_341_,
			int i_342_, int i_343_, int i_344_, int i_345_) {
		method1277(false);
		method1336((byte) -97, i_345_);
		OpenGL.glBegin(4);
		OpenGL.glColor4ub((byte) (i_342_ >> 16), (byte) (i_342_ >> 8), (byte) i_342_, (byte) (i_342_ >> 24));
		OpenGL.glVertex3f(i + 0.35F, i_334_ + 0.35F, i_335_);
		OpenGL.glColor4ub((byte) (i_343_ >> 16), (byte) (i_343_ >> 8), (byte) i_343_, (byte) (i_343_ >> 24));
		OpenGL.glVertex3f(i_336_ + 0.35F, i_337_ + 0.35F, i_338_);
		OpenGL.glColor4ub((byte) (i_344_ >> 16), (byte) (i_344_ >> 8), (byte) i_344_, (byte) (i_344_ >> 24));
		OpenGL.glVertex3f(i_339_ + 0.35F, i_340_ + 0.35F, i_341_);
		OpenGL.glEnd();
	}

	public void method1348(int i, Interface20 interface20, int i_346_, byte i_347_, int i_348_) {
		int i_349_ = interface20.method76(true);
		if (i_347_ == -17) {
			i_348_ *= method1312(i_349_, -3216);
			method1303(-39, interface20);
			OpenGL.glDrawElements(i_346_, i, i_349_, i_348_ + interface20.method74((byte) 81));
		}
	}

	@Override
	public void za(int i, int i_350_, int i_351_, int i_352_, int i_353_) {
		if (i_351_ < 0) {
			i_351_ = -i_351_;
		}
		if (i + i_351_ >= anInt4185 && anInt4238 >= i - i_351_ && anInt4201 <= i_350_ + i_351_
				&& -i_351_ + i_350_ <= anInt4244) {
			method1277(false);
			method1336((byte) 56, i_353_);
			OpenGL.glColor4ub((byte) (i_352_ >> 16), (byte) (i_352_ >> 8), (byte) i_352_, (byte) (i_352_ >> 24));
			float f = i + 0.35F;
			float f_354_ = i_350_ + 0.35F;
			int i_355_ = i_351_ << 1;
			if (i_355_ < aFloat4259) {
				OpenGL.glBegin(7);
				OpenGL.glVertex2f(f + 1.0F, f_354_ + 1.0F);
				OpenGL.glVertex2f(f + 1.0F, f_354_ - 1.0F);
				OpenGL.glVertex2f(f + -1.0F, f_354_ - 1.0F);
				OpenGL.glVertex2f(f - 1.0F, f_354_ + 1.0F);
				OpenGL.glEnd();
			} else if (!(i_355_ <= aFloat4262)) {
				OpenGL.glBegin(6);
				OpenGL.glVertex2f(f, f_354_);
				int i_356_ = 262144 / (i_351_ * 6);
				if (i_356_ <= 64) {
					i_356_ = 64;
				} else if (i_356_ > 512) {
					i_356_ = 512;
				}
				i_356_ = Statics.method629(false, i_356_);
				OpenGL.glVertex2f(f + i_351_, f_354_);
				for (int i_357_ = -i_356_ + 16384; i_357_ > 0; i_357_ -= i_356_) {
					OpenGL.glVertex2f(f + ha.aFloatArray1298[i_357_] * i_351_,
							f_354_ + i_351_ * ha.aFloatArray1297[i_357_]);
				}
				OpenGL.glVertex2f(f + i_351_, f_354_);
				OpenGL.glEnd();
			} else {
				OpenGL.glEnable(2832);
				OpenGL.glPointSize(i_355_);
				OpenGL.glBegin(0);
				OpenGL.glVertex2f(f, f_354_);
				OpenGL.glEnd();
				OpenGL.glDisable(2832);
			}
		}
	}

	@Override
	public void a(Canvas canvas) {
		if (aCanvas4122 == canvas) {
			throw new RuntimeException();
		}
		if (aHashtable4116.containsKey(canvas)) {
			Long var_long = (Long) aHashtable4116.get(canvas);
			anOpenGL4123.releaseSurface(canvas, var_long.longValue());
			aHashtable4116.remove(canvas);
		}
	}

	private void method1349(int i) {
		OpenGL.glMatrixMode(5889);
		OpenGL.glLoadMatrixf(aFloatArray4267, 0);
		OpenGL.glMatrixMode(i);
	}

	public void method1350(boolean bool, byte i, int i_358_) {
		int i_359_ = -92 / ((-46 - i) / 40);
		method1297(bool, (byte) -76, true, i_358_);
	}

	public void method1351(Interface4 interface4, byte i) {
		if (!aBoolean4268) {
			if (anInt4142 >= 3) {
				throw new RuntimeException();
			}
			if (anInt4142 >= 0) {
				anInterface4Array4143[anInt4142].method18((byte) -109);
			}
			anInterface4_4144 = anInterface4_4145 = anInterface4Array4143[++anInt4142] = interface4;
			anInterface4_4144.method20(-30874);
		} else {
			method1333(interface4, (byte) -12);
			method1307((byte) -39, interface4);
		}
		if (i != -27) {
			method1322(-0.1592474F, 25, 0.33301827F);
		}
	}

	@Override
	public int I() {
		int i = anInt4279;
		anInt4279 = 0;
		return i;
	}

	@Override
	public boolean l() {
		return aClass197_4126.method1942(9, 3);
	}

	private void method1352(int i) {
		OpenGL.glLoadIdentity();
		OpenGL.glMultMatrixf(aClass373_Sub1_4170.method3923(true), 0);
		if (i == 3008) {
			if (aBoolean4230) {
				aClass197_4126.aClass366_Sub7_1994.method3791(true);
			}
			method1339(8705);
			method1279(true);
		}
	}

	public void method1353(int i, Interface4 interface4) {
		if (i < (anInt4141 ^ 0xffffffff) || interface4 != anInterface4Array4149[anInt4141]) {
			throw new RuntimeException();
		}
		anInterface4Array4149[anInt4141--] = null;
		interface4.method19(-33);
		if (anInt4141 < 0) {
			anInterface4_4144 = null;
		} else {
			anInterface4_4144 = anInterface4Array4149[anInt4141];
			anInterface4_4144.method22(-18449);
		}
	}

	@Override
	public Interface13 a(Interface19 interface19, Interface11 interface11) {
		return null;
	}

	public void method1354(boolean bool, int i) {
		Packet.aFloatArray4675[0] = (i & 16711680) / 1.671168E7F;
		Packet.aFloatArray4675[1] = (i & 65280) / 65280.0F;
		Packet.aFloatArray4675[2] = (255 & i) / 255.0F;
		Packet.aFloatArray4675[3] = (i >>> 24) / 255.0F;
		OpenGL.glTexEnvfv(8960, 8705, Packet.aFloatArray4675, 0);
		if (bool) {
			aFloat4216 = -0.44322386F;
		}
	}
}
