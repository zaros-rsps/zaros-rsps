package net.zaros.client;

/* Class123_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class123_Sub2_Sub1 extends Class123_Sub2 {
	static int anInt5818 = 4;
	private byte[] aByteArray5819;

	final byte[] method1073(int i, int i_0_, int i_1_, int i_2_) {
		aByteArray5819 = new byte[i_1_ * (i_2_ * i_0_) * 2];
		if (i != -24924)
			return null;
		this.method1051(i_2_, i_1_, 81, i_0_);
		return aByteArray5819;
	}

	static final void method1074(int i) {
		int i_3_ = 1024;
		int i_4_ = 3072;
		if (Class296_Sub39_Sub10.aBoolean6177) {
			i_4_ = 4096;
			if (Class315.aBoolean2789)
				i_3_ = 2048;
		}
		if ((float) i_3_ > Class153.aFloat1572)
			Class153.aFloat1572 = (float) i_3_;
		if (Class153.aFloat1572 > (float) i_4_)
			Class153.aFloat1572 = (float) i_4_;
		for (/**/; Class41_Sub26.aFloat3806 >= 16384.0F; Class41_Sub26.aFloat3806 -= 16384.0F) {
			/* empty */
		}
		for (/**/; Class41_Sub26.aFloat3806 < 0.0F; Class41_Sub26.aFloat3806 += 16384.0F) {
			/* empty */
		}
		int i_5_ = Class296_Sub14.anInt4668 >> 9;
		int i_6_ = Class296_Sub24.anInt4762 >> 9;
		int i_7_ = aa_Sub1.method155(-1537652855, FileWorker.anInt3005, Class296_Sub14.anInt4668, Class296_Sub24.anInt4762);
		int i_8_ = 0;
		if ((i_5_ ^ 0xffffffff) < i && i_6_ > 3 && Class198.currentMapSizeX - 4 > i_5_ && i_6_ < Class296_Sub38.currentMapSizeY - 4) {
			for (int i_9_ = i_5_ - 4; i_5_ + 4 >= i_9_; i_9_++) {
				for (int i_10_ = i_6_ - 4; i_10_ <= i_6_ + 4; i_10_++) {
					int i_11_ = FileWorker.anInt3005;
					if (i_11_ < 3 && r_Sub2.method2871(i_10_, i_9_, (byte) -47))
						i_11_++;
					int i_12_ = 0;
					if ((StaticMethods.aClass181_Sub1_5960.aByteArrayArrayArray1864) != null && (StaticMethods.aClass181_Sub1_5960.aByteArrayArrayArray1864[i_11_]) != null)
						i_12_ = ((StaticMethods.aClass181_Sub1_5960.aByteArrayArrayArray1864[i_11_][i_9_][i_10_]) & 0xff) * 8 << 2;
					if (Class360_Sub2.aSArray5304 != null && Class360_Sub2.aSArray5304[i_11_] != null) {
						int i_13_ = (i_7_ + i_12_ - Class360_Sub2.aSArray5304[i_11_].method3355(i_10_, (byte) -120, i_9_));
						if (i_13_ > i_8_)
							i_8_ = i_13_;
					}
				}
			}
		}
		int i_14_ = (i_8_ >> 2) * 1536;
		if (i_14_ > 786432)
			i_14_ = 786432;
		if (i_14_ < 262144)
			i_14_ = 262144;
		if (i_14_ <= Class352.anInt3044) {
			if (i_14_ < Class352.anInt3044)
				Class352.anInt3044 += (-Class352.anInt3044 + i_14_) / 80;
		} else
			Class352.anInt3044 += (-Class352.anInt3044 + i_14_) / 24;
	}

	final void method1072(byte i, int i_15_, byte i_16_) {
		int i_17_ = i_15_ * 2;
		if (i_16_ != -45)
			aByteArray5819 = null;
		i = (byte) ((i >> 1 & 0x7f) + 127);
		aByteArray5819[i_17_++] = i;
		aByteArray5819[i_17_] = i;
	}

	public Class123_Sub2_Sub1() {
		super(12, 5, 16, 2, 2, 0.45F);
	}
}
