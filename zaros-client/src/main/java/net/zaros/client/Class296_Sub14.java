package net.zaros.client;

import net.zaros.client.configs.invtype.InvTypeList;
import net.zaros.client.logic.clans.channel.ClanChannel;
import net.zaros.client.logic.clans.channel.ClanChannelMember;

/* Class296_Sub14 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub14 extends Node {
	int anInt4658;
	static short aShort4659 = 32767;
	boolean aBoolean4660;
	static InvTypeList itemArraysDefinitionLoader;
	static int anInt4662 = 0;
	static volatile boolean aBoolean4663 = true;
	boolean aBoolean4664;
	boolean aBoolean4665;
	int anInt4666;
	Class219_Sub1 aClass219_Sub1_4667;
	static int anInt4668;
	boolean aBoolean4669;

	static final void method2509(boolean bool, boolean bool_0_, int i) {
		ClanChannel class296_sub54 = (bool_0_ ? EmissiveTriangle.affinedClanChannel : Class166.aClass296_Sub54_1692);
		if (class296_sub54 != null && i >= 0 && class296_sub54.members_count > i) {
			ClanChannelMember class329 = class296_sub54.members[i];
			if (class329.rank == -1) {
				String string = class329.name;
				Connection class204 = Class296_Sub51_Sub13.method3111(bool);
				Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 86, Class41_Sub4.aClass311_3746);
				class296_sub1.out.p1(3 + Class117.method1015((byte) -73, string));
				class296_sub1.out.p1(!bool_0_ ? 0 : 1);
				class296_sub1.out.p2(i);
				class296_sub1.out.writeString(string);
				class204.sendPacket(class296_sub1, (byte) 119);
			}
		}
	}

	public static void method2510(int i) {
		if (i != 32510)
			method2509(true, false, 122);
		itemArraysDefinitionLoader = null;
	}

	static final void method2511(int[] is, int i, byte i_1_, int i_2_, int i_3_) {
		i_3_--;
		int i_4_ = --i - 7;
		int i_5_ = 14 % ((60 - i_1_) / 51);
		while (i_3_ < i_4_) {
			is[++i_3_] = i_2_;
			is[++i_3_] = i_2_;
			is[++i_3_] = i_2_;
			is[++i_3_] = i_2_;
			is[++i_3_] = i_2_;
			is[++i_3_] = i_2_;
			is[++i_3_] = i_2_;
			is[++i_3_] = i_2_;
		}
		while (i > i_3_)
			is[++i_3_] = i_2_;
	}

	static final int method2512(boolean bool) {
		if (bool)
			aBoolean4663 = true;
		return Class307.anInt2751;
	}

	Class296_Sub14(int i, Class219_Sub1 class219_sub1, int i_6_, boolean bool) {
		aClass219_Sub1_4667 = class219_sub1;
		anInt4666 = i_6_;
		anInt4658 = i;
		aBoolean4664 = bool;
	}
}
