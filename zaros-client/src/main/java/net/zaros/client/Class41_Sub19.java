package net.zaros.client;

/* Class41_Sub19 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub19 extends Class41 {
	static IncomingPacket aClass231_3790 = new IncomingPacket(135, 6);
	static int anInt3791;
	static Class306 aClass306_3792;
	static int anInt3793 = -1;

	static final void method466(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, byte i_6_) {
		if (i_6_ == 1) {
			if (i_5_ == i_2_)
				StaticMethods.method2467(i_2_, i, i_3_, i_4_, (byte) 52, i_1_, i_0_);
			else if (ConfigurationDefinition.anInt676 <= -i_2_ + i && Class288.anInt2652 >= i + i_2_ && -i_5_ + i_1_ >= EmissiveTriangle.anInt952 && RuntimeException_Sub1.anInt3391 >= i_1_ + i_5_)
				Class343_Sub1.method3642(true, i_3_, i_5_, i_2_, i, i_4_, i_0_, i_1_);
			else
				za_Sub2.method3227(i_5_, i, i_0_, true, i_2_, i_4_, i_1_, i_3_);
		}
	}

	final int method380(int i, byte i_7_) {
		if (!Class125.method1080((byte) 98, aClass296_Sub50_392.aClass41_Sub30_5006.method521(116)))
			return 3;
		if (i_7_ != 41)
			return 53;
		return 1;
	}

	final int method383(byte i) {
		if (i != 110)
			method381(-27, (byte) -7);
		return 1;
	}

	public static void method467(byte i) {
		aClass306_3792 = null;
		aClass231_3790 = null;
		if (i != 46)
			anInt3791 = -94;
	}

	final void method381(int i, byte i_8_) {
		if (i_8_ == -110)
			anInt389 = i;
	}

	Class41_Sub19(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	final void method386(int i) {
		if (i != 2)
			method469(85);
		if (anInt389 < 0 || anInt389 > 1)
			anInt389 = method383((byte) 110);
	}

	final int method468(int i) {
		if (i < 114)
			return 46;
		return anInt389;
	}

	Class41_Sub19(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final boolean method469(int i) {
		if (!Class125.method1080((byte) 90, aClass296_Sub50_392.aClass41_Sub30_5006.method521(116)))
			return false;
		if (i != -25952)
			method386(3);
		return true;
	}
}
