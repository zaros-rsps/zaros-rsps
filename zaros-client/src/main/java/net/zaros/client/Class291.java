package net.zaros.client;
/* Class291 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class291 {
	public static void copy(short[] is, int i, short[] is_3_, int i_4_, int i_5_) {
		if (is == is_3_) {
			if (i == i_4_) {
				return;
			}
			if (i_4_ > i && i_4_ < i + i_5_) {
				i_5_--;
				i += i_5_;
				i_4_ += i_5_;
				i_5_ = i - i_5_;
				i_5_ += 7;
				while (i >= i_5_) {
					is_3_[i_4_--] = is[i--];
					is_3_[i_4_--] = is[i--];
					is_3_[i_4_--] = is[i--];
					is_3_[i_4_--] = is[i--];
					is_3_[i_4_--] = is[i--];
					is_3_[i_4_--] = is[i--];
					is_3_[i_4_--] = is[i--];
					is_3_[i_4_--] = is[i--];
				}
				i_5_ -= 7;
				while (i >= i_5_) {
					is_3_[i_4_--] = is[i--];
				}
				return;
			}
		}
		i_5_ += i;
		i_5_ -= 7;
		while (i < i_5_) {
			is_3_[i_4_++] = is[i++];
			is_3_[i_4_++] = is[i++];
			is_3_[i_4_++] = is[i++];
			is_3_[i_4_++] = is[i++];
			is_3_[i_4_++] = is[i++];
			is_3_[i_4_++] = is[i++];
			is_3_[i_4_++] = is[i++];
			is_3_[i_4_++] = is[i++];
		}
		i_5_ += 7;
		while (i < i_5_) {
			is_3_[i_4_++] = is[i++];
		}
	}

	static void method2404(int[] is, int i, int i_6_) {
		i_6_ = i + i_6_ - 7;
		while (i < i_6_) {
			is[i++] = 0;
			is[i++] = 0;
			is[i++] = 0;
			is[i++] = 0;
			is[i++] = 0;
			is[i++] = 0;
			is[i++] = 0;
			is[i++] = 0;
		}
		i_6_ += 7;
		while (i < i_6_) {
			is[i++] = 0;
		}
	}

	static void method2406(float[] fs, int i, float[] fs_10_, int i_11_, int i_12_) {
		if (fs == fs_10_) {
			if (i == i_11_) {
				return;
			}
			if (i_11_ > i && i_11_ < i + i_12_) {
				i_12_--;
				i += i_12_;
				i_11_ += i_12_;
				i_12_ = i - i_12_;
				i_12_ += 7;
				while (i >= i_12_) {
					fs_10_[i_11_--] = fs[i--];
					fs_10_[i_11_--] = fs[i--];
					fs_10_[i_11_--] = fs[i--];
					fs_10_[i_11_--] = fs[i--];
					fs_10_[i_11_--] = fs[i--];
					fs_10_[i_11_--] = fs[i--];
					fs_10_[i_11_--] = fs[i--];
					fs_10_[i_11_--] = fs[i--];
				}
				i_12_ -= 7;
				while (i >= i_12_) {
					fs_10_[i_11_--] = fs[i--];
				}
				return;
			}
		}
		i_12_ += i;
		i_12_ -= 7;
		while (i < i_12_) {
			fs_10_[i_11_++] = fs[i++];
			fs_10_[i_11_++] = fs[i++];
			fs_10_[i_11_++] = fs[i++];
			fs_10_[i_11_++] = fs[i++];
			fs_10_[i_11_++] = fs[i++];
			fs_10_[i_11_++] = fs[i++];
			fs_10_[i_11_++] = fs[i++];
			fs_10_[i_11_++] = fs[i++];
		}
		i_12_ += 7;
		while (i < i_12_) {
			fs_10_[i_11_++] = fs[i++];
		}
	}

	static void method2407(int[] is, int i, int i_13_, int i_14_) {
		i_13_ = i + i_13_ - 7;
		while (i < i_13_) {
			is[i++] = i_14_;
			is[i++] = i_14_;
			is[i++] = i_14_;
			is[i++] = i_14_;
			is[i++] = i_14_;
			is[i++] = i_14_;
			is[i++] = i_14_;
			is[i++] = i_14_;
		}
		i_13_ += 7;
		while (i < i_13_) {
			is[i++] = i_14_;
		}
	}
}
