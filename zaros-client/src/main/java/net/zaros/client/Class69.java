package net.zaros.client;
import jaggl.OpenGL;

abstract class Class69 implements Interface18 {
	int anInt3680;
	ha_Sub3 aHa_Sub3_3681;
	int anInt3682;
	private boolean aBoolean3683;
	private int anInt3684;
	int anInt3685;
	static IncomingPacket aClass231_3686 = new IncomingPacket(94, 0);
	private boolean aBoolean3687 = false;
	static int anInt3688;
	static int anInt3689 = -1;
	static float aFloat3690;

	final void method718(boolean bool, byte i) {
		if (i != 24)
			method718(false, (byte) 120);
		if (!aBoolean3683 == bool) {
			int i_0_ = method722(i ^ 0x6448);
			aBoolean3683 = true;
			method725((byte) -85);
			method720(i_0_, i ^ 0x51e5);
		}
	}

	final boolean method719(int i) {
		if (i != 10240)
			method718(true, (byte) 7);
		if (aHa_Sub3_3681.aBoolean4190) {
			int i_1_ = method722(25680);
			aHa_Sub3_3681.method1316(this, (byte) -128);
			OpenGL.glGenerateMipmapEXT(anInt3682);
			aBoolean3683 = true;
			method725((byte) -85);
			method720(i_1_, i ^ 0x79fd);
			return true;
		}
		return false;
	}

	private final void method720(int i, int i_2_) {
		if (i_2_ == 20989) {
			aHa_Sub3_3681.anInt4152 -= i;
			aHa_Sub3_3681.anInt4152 += method722(25680);
		}
	}

	public static void method721(int i) {
		if (i == -200)
			aClass231_3686 = null;
	}

	protected final void finalize() throws Throwable {
		method726(0);
		super.finalize();
	}

	private final int method722(int i) {
		if (i != 25680)
			method724('^', -11, -68);
		int i_3_ = aHa_Sub3_3681.method1290(anInt3685, (byte) -16) * anInt3684;
		if (aBoolean3683)
			return i_3_ * 4 / 3;
		return i_3_;
	}

	final void method723(int i, boolean bool) {
		if (i < 75)
			aBoolean3687 = true;
		if (!aBoolean3687 == bool) {
			aBoolean3687 = bool;
			method725((byte) -85);
		}
	}

	static final char method724(char c, int i, int i_4_) {
		try {
			if (c >= '\u00c0' && c <= '\u00ff') {
				if (c >= '\u00c0' && c <= '\u00c6')
					return 'A';
				if (c == '\u00c7')
					return 'C';
				if (c >= '\u00c8' && c <= '\u00cb')
					return 'E';
				if (c >= '\u00cc' && c <= '\u00cf')
					return 'I';
				if (c >= '\u00d2' && c <= '\u00d6')
					return 'O';
				if (c >= '\u00d9' && c <= '\u00dc')
					return 'U';
				if (c == '\u00dd')
					return 'Y';
				if (c == '\u00df')
					return 's';
				if (c >= '\u00e0' && c <= '\u00e6')
					return 'a';
				if (c == '\u00e7')
					return 'c';
				if (c >= '\u00e8' && c <= '\u00eb')
					return 'e';
				if (c >= '\u00ec' && c <= '\u00ef')
					return 'i';
				if (c >= '\u00f2' && c <= '\u00f6')
					return 'o';
				if (c >= '\u00f9' && c <= '\u00fc')
					return 'u';
				if (c == '\u00fd' || c == '\u00ff')
					return 'y';
			}
			if (c == (char) i_4_)
				return 'O';
			if (c == '\u0153')
				return 'o';
			if (c == '\u0178')
				return 'Y';
			return c;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, ("ea.M(" + c + ',' + i + ',' + i_4_ + ')'));
		}
	}

	private final void method725(byte i) {
		aHa_Sub3_3681.method1316(this, (byte) -121);
		if (aBoolean3687) {
			OpenGL.glTexParameteri(anInt3682, 10241, !aBoolean3683 ? 9729 : 9987);
			OpenGL.glTexParameteri(anInt3682, 10240, 9729);
		} else {
			OpenGL.glTexParameteri(anInt3682, 10241, aBoolean3683 ? 9984 : 9728);
			OpenGL.glTexParameteri(anInt3682, 10240, 9728);
		}
		if (i != -85)
			method726(37);
	}

	final void method726(int i) {
		if (i < anInt3680) {
			aHa_Sub3_3681.method1342(i - 113, anInt3680, method722(25680));
			anInt3680 = 0;
		}
	}

	final int method727(int i) {
		if (i != 203)
			method721(-84);
		return anInt3680;
	}

	Class69(ha_Sub3 var_ha_Sub3, int i, int i_5_, int i_6_, boolean bool) {
		anInt3685 = i_5_;
		aBoolean3683 = bool;
		anInt3682 = i;
		aHa_Sub3_3681 = var_ha_Sub3;
		anInt3684 = i_6_;
		OpenGL.glGenTextures(1, Class42_Sub4.anIntArray3854, 0);
		anInt3680 = Class42_Sub4.anIntArray3854[0];
		method720(0, 20989);
	}

	public abstract void method73(boolean bool);
}
