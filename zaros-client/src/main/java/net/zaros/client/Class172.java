package net.zaros.client;

/* Class172 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class172 {
	static IncomingPacket hintIconPacket = new IncomingPacket(116, 12);
	static boolean member = false;

	static final Class338_Sub3_Sub3 method1679(int i, int i_0_, int i_1_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_0_][i_1_];
		if (class247 == null)
			return null;
		return class247.aClass338_Sub3_Sub3_2342;
	}

	public static void method1680(int i) {
		if (i != 1004)
			hintIconPacket = null;
		hintIconPacket = null;
	}

	static final void method1681(boolean bool, NPC class338_sub3_sub1_sub3_sub2, int i) {
		if (Class230.anInt2210 < 400) {
			if (i != -15204)
				method1679(41, 104, 93);
			NPCDefinition class147 = class338_sub3_sub1_sub3_sub2.definition;
			String string = class338_sub3_sub1_sub3_sub2.name;
			if (class147.configData != null) {
				class147 = class147.getNPCDefinitionByConfigID(Class16_Sub3_Sub1.configsRegister);
				if (class147 == null)
					return;
				string = class147.name;
			}
			if (class147.isClickable) {
				if (class338_sub3_sub1_sub3_sub2.combatLevel != 0) {
					String string_2_ = ((Class296_Sub32.stellardawn == Class296_Sub50.game) ? TranslatableString.aClass120_1226.getTranslation(Class394.langID) : TranslatableString.aClass120_1224.getTranslation(Class394.langID));
					string += ((Class21.method288(-115, (Class296_Sub51_Sub11.localPlayer.combatLevel), class338_sub3_sub1_sub3_sub2.combatLevel)) + " (" + (String) string_2_ + class338_sub3_sub1_sub3_sub2.combatLevel + ")");
				}
				if (Class127.aBoolean1304 && !bool) {
					ParamType class383 = (Class353.anInt3047 == -1 ? null : Class296_Sub22.itemExtraDataDefinitionLoader.list(Class353.anInt3047));
					if ((Class321.anInt2824 & 0x2) != 0 && (class383 == null || (class147.getIntExtraData(Class353.anInt3047, class383.defaultValue) != class383.defaultValue)))
						StaticMethods.method1008(74, false, 0, IOException_Sub1.anInt36, -1, Class228.aString2200, 0, (long) class338_sub3_sub1_sub3_sub2.index, 59, true, (long) class338_sub3_sub1_sub3_sub2.index, ConfigsRegister.aString3672 + " -> <col=ffff00>" + string, false);
				}
				if (!bool) {
					String[] strings = class147.options;
					if (EffectiveVertex.aBoolean2218)
						strings = Class65.method707(strings, false);
					if (strings != null) {
						for (int i_3_ = strings.length - 1; i_3_ >= 0; i_3_--) {
							if (strings[i_3_] != null && (class147.aByte1487 == 0 || (!(strings[i_3_].equalsIgnoreCase(TranslatableString.aClass120_1219.getTranslation(Class394.langID))) && !(strings[i_3_].equalsIgnoreCase(TranslatableString.aClass120_1218.getTranslation(Class394.langID)))))) {
								int i_4_ = 0;
								if (i_3_ == 0)
									i_4_ = 51;
								int i_5_ = Class106_Sub1.anInt3903;
								if (i_3_ == 1)
									i_4_ = 9;
								if (i_3_ == 2)
									i_4_ = 17;
								if (i_3_ == 3)
									i_4_ = 12;
								if (i_3_ == 4)
									i_4_ = 60;
								if (class147.anInt1480 == i_3_)
									i_5_ = class147.anInt1453;
								if (i_3_ == 5)
									i_4_ = 1004;
								if (class147.anInt1510 == i_3_)
									i_5_ = class147.anInt1475;
								StaticMethods.method1008(-94, false, 0, ((strings[i_3_].equalsIgnoreCase(TranslatableString.aClass120_1219.getTranslation(Class394.langID))) ? class147.anInt1492 : i_5_), -1, strings[i_3_], 0, (long) (class338_sub3_sub1_sub3_sub2.index), i_4_, true, (long) (class338_sub3_sub1_sub3_sub2.index), "<col=ffff00>" + string, false);
							}
						}
						if (class147.aByte1487 == 1) {
							for (int i_6_ = 0; strings.length > i_6_; i_6_++) {
								if (strings[i_6_] != null && ((strings[i_6_].equalsIgnoreCase(TranslatableString.aClass120_1219.getTranslation(Class394.langID))) || (strings[i_6_].equalsIgnoreCase(TranslatableString.aClass120_1218.getTranslation(Class394.langID))))) {
									short i_7_ = 0;
									if ((Class296_Sub51_Sub11.localPlayer.combatLevel) < (class338_sub3_sub1_sub3_sub2.combatLevel))
										i_7_ = (short) 2000;
									short i_8_ = 0;
									int i_9_ = Class106_Sub1.anInt3903;
									if (i_6_ == 0)
										i_8_ = (short) 51;
									if (i_6_ == 1)
										i_8_ = (short) 9;
									if (i_6_ == 2)
										i_8_ = (short) 17;
									if (i_6_ == 3)
										i_8_ = (short) 12;
									if (i_6_ == 4)
										i_8_ = (short) 60;
									if (i_6_ == 5)
										i_8_ = (short) 1004;
									if (class147.anInt1480 == i_6_)
										i_9_ = class147.anInt1453;
									if (i_8_ != 0)
										i_8_ += i_7_;
									if (i_6_ == class147.anInt1510)
										i_9_ = class147.anInt1475;
									StaticMethods.method1008(31, false, 0, (!(strings[i_6_].equalsIgnoreCase(TranslatableString.aClass120_1219.getTranslation(Class394.langID))) ? i_9_ : class147.anInt1492), -1, strings[i_6_], 0, (long) (class338_sub3_sub1_sub3_sub2.index), i_8_, true, (long) (class338_sub3_sub1_sub3_sub2.index), "<col=ffff00>" + string, false);
								}
							}
						}
					}
				}
			}
		}
	}
}
