package net.zaros.client;

/* Class296_Sub39_Sub17 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub17 extends Queuable {
	static int[] anIntArray6234 = {1, 4, 1, 2, 1};
	int anInt6235;
	int anInt6236;
	static Sprite aClass397_6237;
	static boolean aBoolean6238 = false;
	int anInt6239;
	EffectiveVertex aClass232_6240;
	static Class111 aClass111_6241;
	int anInt6242;
	int anInt6243;
	static Sprite aClass397_6244;
	Class95 aClass95_6245;

	static final void method2892(int i, int i_0_, int i_1_, int i_2_, int i_3_) {
		if (i_1_ != -30124)
			aClass111_6241 = null;
		if (i_3_ >= ConfigurationDefinition.anInt676 && i_3_ <= Class288.anInt2652) {
			i_0_ = ParticleEmitterRaw.method1668(i_0_, RuntimeException_Sub1.anInt3391, EmissiveTriangle.anInt952, (byte) 120);
			i_2_ = ParticleEmitterRaw.method1668(i_2_, RuntimeException_Sub1.anInt3391, EmissiveTriangle.anInt952, (byte) 19);
			Class140.method1467(i_2_, i, i_3_, false, i_0_);
		}
	}

	final void method2893(int i) {
		anInt6243 = aClass232_6240.anInt2217;
		anInt6242 = aClass232_6240.anInt2214;
		anInt6235 = aClass232_6240.anInt2219;
		if (aClass232_6240.aClass373_2222 != null)
			aClass232_6240.aClass373_2222.method3901(aClass95_6245.anInt1028, aClass95_6245.anInt1026, aClass95_6245.anInt1032, (Class296_Sub35_Sub2.anIntArray6112));
		anInt6236 = Class296_Sub35_Sub2.anIntArray6112[0];
		anInt6239 = Class296_Sub35_Sub2.anIntArray6112[i];
	}

	static final void method2894(int i) {
		for (Class296_Sub42 class296_sub42 = (Class296_Sub42) aa.aClass155_50.removeFirst((byte) 121); class296_sub42 != null; class296_sub42 = (Class296_Sub42) aa.aClass155_50.removeNext(1001)) {
			if (class296_sub42.aBoolean4929)
				class296_sub42.unlink();
			else {
				class296_sub42.aBoolean4935 = true;
				if (class296_sub42.anInt4923 >= 0 && class296_sub42.anInt4928 >= 0 && Class198.currentMapSizeX > class296_sub42.anInt4923 && class296_sub42.anInt4928 < Class296_Sub38.currentMapSizeY)
					Class322.method3346(i ^ 0x71, class296_sub42);
			}
		}
		if (i != 1)
			aClass397_6244 = null;
		for (Class296_Sub42 class296_sub42 = ((Class296_Sub42) ParticleEmitterRaw.aClass155_1770.removeFirst((byte) 125)); class296_sub42 != null; class296_sub42 = (Class296_Sub42) ParticleEmitterRaw.aClass155_1770.removeNext(1001)) {
			if (class296_sub42.aBoolean4929)
				class296_sub42.unlink();
			else
				class296_sub42.aBoolean4935 = true;
		}
	}

	public static void method2895(int i) {
		aClass111_6241 = null;
		if (i != 0)
			aClass397_6237 = null;
		anIntArray6234 = null;
		aClass397_6237 = null;
		aClass397_6244 = null;
	}

	Class296_Sub39_Sub17(EffectiveVertex class232, Class338_Sub1 class338_sub1) {
		aClass232_6240 = class232;
		aClass95_6245 = aClass232_6240.method2119(255);
		method2893(2);
	}
}
