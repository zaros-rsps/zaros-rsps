package net.zaros.client;

/* Class368_Sub15 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub15 extends Class368 {
	private int anInt5512;
	private int anInt5513;
	private int anInt5514;
	static int anInt5515;
	private int anInt5516;
	static OutgoingPacket aClass311_5517 = new OutgoingPacket(56, 5);
	static int anInt5518;
	private int anInt5519;
	static int anInt5520;
	static Sprite aClass397_5521;
	static Class213 aClass213_5522;
	static boolean aBoolean5523 = false;
	static int anInt5524;

	final void method3807(byte i) {
		int i_0_ = 18 % ((52 - i) / 52);
		Class296_Sub51_Sub32.method3178(anInt5512, anInt5513, anInt5514, 100, 100, false, false);
		Class373_Sub1.method3925(anInt5516, 0, anInt5519, -30302);
		StaticMethods.aBoolean1183 = true;
	}

	static final Class379_Sub2_Sub1 method3851(int i, Packet class296_sub17) {
		if (i < 59)
			method3852(70);
		Class379_Sub2 class379_sub2 = Class244.method2176(class296_sub17, 2);
		int i_1_ = class296_sub17.g2b();
		return (new Class379_Sub2_Sub1(class379_sub2.aClass252_3616, class379_sub2.aClass357_3621, class379_sub2.anInt3623, class379_sub2.anInt3615, class379_sub2.anInt3613, class379_sub2.anInt3626, class379_sub2.anInt3619, class379_sub2.anInt3620, class379_sub2.anInt3617, class379_sub2.anInt5680, class379_sub2.anInt5679, class379_sub2.anInt5681, class379_sub2.anInt5682, class379_sub2.anInt5676, class379_sub2.anInt5678, i_1_));
	}

	public static void method3852(int i) {
		aClass213_5522 = null;
		if (i <= 0)
			method3851(-1, null);
		aClass397_5521 = null;
		aClass311_5517 = null;
	}

	Class368_Sub15(Packet class296_sub17) {
		super(class296_sub17);
		anInt5512 = class296_sub17.g2();
		anInt5514 = class296_sub17.g2();
		anInt5513 = class296_sub17.g2();
		anInt5519 = class296_sub17.g2();
		anInt5516 = class296_sub17.g2();
	}
}
