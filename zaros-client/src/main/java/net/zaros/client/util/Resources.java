package net.zaros.client.util;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * 
 * @author Walied K. Yassen
 */
public final class Resources {

	private static final ClassLoader CLASS_LOADER = Resources.class.getClassLoader();

	public static Image getImage(String name) {
		try {
			return ImageIO.read(getResource("images/" + name));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static InputStream getResource(String name) {
		return CLASS_LOADER.getResourceAsStream(name);
	}

	private Resources() {

	}
}
