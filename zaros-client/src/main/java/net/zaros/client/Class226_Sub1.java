package net.zaros.client;
/* Class226_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Random;

final class Class226_Sub1 extends Class226 {
	private int[] anIntArray4572 = new int[512];
	static Js5 aClass138_4573;

	public static void method2091(byte i) {
		if (i == -128)
			aClass138_4573 = null;
	}

	final void method2088(float f, float f_0_, float f_1_, int i, int i_2_, int i_3_, int i_4_, int i_5_, float[] fs, int i_6_, float f_7_) {
		int i_8_ = (int) (f_0_ * (float) i + -1.0F);
		i_8_ &= 0xff;
		int i_9_ = (int) (f_7_ * (float) i_4_ + -1.0F);
		i_9_ &= 0xff;
		int i_10_ = (int) (f_1_ * (float) i_2_ + -1.0F);
		i_10_ &= 0xff;
		float f_11_ = (float) i_5_ * f_1_;
		int i_12_ = (int) f_11_;
		int i_13_ = i_12_ + 1;
		float f_14_ = (float) -i_12_ + f_11_;
		float f_15_ = 1.0F - f_14_;
		if (i_3_ < -23) {
			i_13_ &= i_10_;
			i_12_ &= i_10_;
			float f_16_ = Class196.method1940(1618, f_14_);
			int i_17_ = anIntArray4572[i_12_];
			int i_18_ = anIntArray4572[i_13_];
			for (int i_19_ = 0; i_19_ < i_4_; i_19_++) {
				float f_20_ = f_7_ * (float) i_19_;
				int i_21_ = (int) f_20_;
				int i_22_ = i_21_ + 1;
				float f_23_ = (float) -i_21_ + f_20_;
				float f_24_ = 1.0F - f_23_;
				i_22_ &= i_9_;
				float f_25_ = Class196.method1940(1618, f_23_);
				i_21_ &= i_9_;
				int i_26_ = anIntArray4572[i_17_ + i_21_];
				int i_27_ = anIntArray4572[i_17_ + i_22_];
				int i_28_ = anIntArray4572[i_21_ + i_18_];
				int i_29_ = anIntArray4572[i_18_ + i_22_];
				for (int i_30_ = 0; i_30_ < i; i_30_++) {
					float f_31_ = f_0_ * (float) i_30_;
					int i_32_ = (int) f_31_;
					int i_33_ = i_32_ + 1;
					float f_34_ = f_31_ - (float) i_32_;
					float f_35_ = 1.0F - f_34_;
					i_32_ &= i_8_;
					i_33_ &= i_8_;
					float f_36_ = Class196.method1940(1618, f_34_);
					fs[i_6_++] = (f * (Class366.method3765(11404, (Class366.method3765(11404, (Class366.method3765(11404, (LongNode.method2657(f_35_, f_15_, f_24_, ((anIntArray4572[i_32_ + i_26_]) & 7), 106)), f_36_, (LongNode.method2657(f_34_, f_15_, f_24_, (7 & (anIntArray4572[i_26_ + i_33_])), 88)))), f_25_, (Class366.method3765(11404, (LongNode.method2657(f_35_, f_15_, f_23_, (7 & (anIntArray4572[i_27_ + i_32_])), 102)), f_36_, (LongNode.method2657(f_34_, f_15_, f_23_, ((anIntArray4572[i_33_ + i_27_]) & 7), 15)))))), f_16_, (Class366.method3765(11404, (Class366.method3765(11404, (LongNode.method2657(f_35_, f_14_, f_24_, (7 & (anIntArray4572[i_32_ + i_28_])), -127)), f_36_, (LongNode.method2657(f_34_, f_14_, f_24_, (7 & (anIntArray4572[i_33_ + i_28_])), 11)))), f_25_, (Class366.method3765(11404, (LongNode.method2657(f_35_, f_14_, f_23_, (7 & (anIntArray4572[i_29_ + i_32_])), 91)), f_36_, (LongNode.method2657(f_34_, f_14_, f_23_, (7 & (anIntArray4572[i_33_ + i_29_])), -122)))))))));
				}
			}
		}
	}

	Class226_Sub1(int i) {
		Random random = new Random((long) i);
		for (int i_37_ = 0; i_37_ < 256; i_37_++)
			anIntArray4572[i_37_] = anIntArray4572[i_37_ + 256] = i_37_;
		for (int i_38_ = 0; i_38_ < 256; i_38_++) {
			int i_39_ = random.nextInt() & 0xff;
			int i_40_ = anIntArray4572[i_39_];
			anIntArray4572[i_39_] = anIntArray4572[i_39_ + 256] = anIntArray4572[i_38_];
			anIntArray4572[i_38_] = anIntArray4572[i_38_ + 256] = i_40_;
		}
	}
}
