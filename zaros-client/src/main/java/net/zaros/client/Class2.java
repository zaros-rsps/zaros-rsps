package net.zaros.client;
/* Class2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

final class Class2 implements Runnable {
	static int[] anIntArray56;
	private Thread aThread57;
	private InputStream anInputStream58;
	static int anInt59 = 0;
	private int anInt60;
	private int anInt61 = 0;
	static Class230 aClass230_62 = new Class230();
	private int anInt63 = 0;
	private IOException anIOException64;
	static Class144 aClass144_65 = Class284.method2363(true);
	static Class166 aClass166_66;
	private byte[] aByteArray67;

	final boolean method166(int i, int i_0_) throws IOException {
		if (i_0_ > -9)
			return false;
		if (i <= 0 || anInt60 <= i)
			throw new IOException();
		synchronized (this) {
			int i_1_;
			if (anInt63 < anInt61)
				i_1_ = anInt60 - (anInt61 - anInt63);
			else
				i_1_ = -anInt61 + anInt63;
			if (i > i_1_) {
				if (anIOException64 != null)
					throw new IOException(anIOException64.toString());
				this.notifyAll();
				return false;
			}
			return true;
		}
	}

	final int method167(byte[] is, int i, int i_2_, boolean bool) throws IOException {
		if (i < 0 || i_2_ < 0 || i + i_2_ > is.length)
			throw new IOException();
		synchronized (this) {
			if (bool != true)
				method170(true);
			int i_3_;
			if (anInt63 < anInt61)
				i_3_ = anInt63 + (-anInt61 + anInt60);
			else
				i_3_ = anInt63 - anInt61;
			if (i_3_ < i)
				i = i_3_;
			if (i == 0 && anIOException64 != null)
				throw new IOException(anIOException64.toString());
			if (i + anInt61 <= anInt60)
				ArrayTools.removeElement(aByteArray67, anInt61, is, i_2_, i);
			else {
				int i_4_ = -anInt61 + anInt60;
				ArrayTools.removeElement(aByteArray67, anInt61, is, i_2_, i_4_);
				ArrayTools.removeElement(aByteArray67, 0, is, i_4_ + i_2_, i - i_4_);
			}
			anInt61 = (i + anInt61) % anInt60;
			this.notifyAll();
			return i;
		}
	}

	public final void run() {
		for (;;) {
			int i;
			synchronized (this) {
				for (;;) {
					if (anIOException64 != null)
						return;
					if (anInt61 == 0)
						i = anInt60 - anInt63 - 1;
					else if (anInt63 >= anInt61)
						i = anInt60 - anInt63;
					else
						i = anInt61 + (-anInt63 - 1);
					if (i > 0)
						break;
					try {
						this.wait();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
			}
			int i_5_;
			try {
				i_5_ = anInputStream58.read(aByteArray67, anInt63, i);
				if (i_5_ == -1)
					throw new EOFException();
			} catch (IOException ioexception) {
				synchronized (this) {
					anIOException64 = ioexception;
					break;
				}
			}
			synchronized (this) {
				anInt63 = (i_5_ + anInt63) % anInt60;
			}
		}
	}

	public static void method168(int i) {
		aClass230_62 = null;
		anIntArray56 = null;
		aClass166_66 = null;
		aClass144_65 = null;
		if (i != 4449)
			aClass230_62 = null;
	}

	final void method169(byte i) {
		anInputStream58 = new InputStream_Sub1();
		int i_6_ = -63 / ((-15 - i) / 47);
	}

	final void method170(boolean bool) {
		synchronized (this) {
			if (anIOException64 == null)
				anIOException64 = new IOException("");
			this.notifyAll();
		}
		try {
			aThread57.join();
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
		if (bool)
			method168(-13);
	}

	Class2(InputStream inputstream, int i) {
		anInputStream58 = inputstream;
		anInt60 = i + 1;
		aByteArray67 = new byte[anInt60];
		aThread57 = new Thread(this);
		aThread57.setDaemon(true);
		aThread57.start();
	}
}
