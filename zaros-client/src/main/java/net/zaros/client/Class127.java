package net.zaros.client;

/* Class127 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class127 {
	static IncomingPacket aClass231_1300 = new IncomingPacket(131, 6);
	static Class299[] aClass299Array1301;
	static Js5 aClass138_1302;
	static int anInt1303 = 0;
	static boolean aBoolean1304 = false;

	static final void method1355(String string, Class398 class398, String string_0_, boolean bool, boolean bool_1_, int i) {
		int i_2_ = 3 % ((-78 - i) / 37);
		if (bool_1_) {
			if (Class398.osName.startsWith("win") && class398.aBoolean3332) {
				String string_3_ = null;
				if (CS2Script.anApplet6140 != null)
					string_3_ = CS2Script.anApplet6140.getParameter("haveie6");
				if (string_3_ == null || !string_3_.equals("1")) {
					Class278 class278 = Class247.method2190(1, 0, class398, string_0_);
					Class359.aString3091 = string_0_;
					Class304.aClass398_2735 = class398;
					Js5.aClass278_1418 = class278;
					return;
				}
			}
			if (Class398.osName.startsWith("mac")) {
				String string_4_ = null;
				if (CS2Script.anApplet6140 != null)
					string_4_ = CS2Script.anApplet6140.getParameter("havefirefox");
				if (string_4_ != null && string_4_.equals("1") && bool) {
					Class354.method3692(class398, string_0_, 1, -19805, string);
					return;
				}
			}
			Class247.method2190(1, 2, class398, string_0_);
		} else
			Class247.method2190(1, 3, class398, string_0_);
	}

	public static void method1356(int i) {
		aClass299Array1301 = null;
		int i_5_ = -124 % ((44 - i) / 44);
		aClass231_1300 = null;
		aClass138_1302 = null;
	}

	public Class127() {
		/* empty */
	}

	static final Class69_Sub1_Sub1 method1357(ha_Sub3 var_ha_Sub3, byte i, int i_6_, int i_7_, int i_8_) {
		if (i != 122)
			aClass231_1300 = null;
		if (!var_ha_Sub3.aBoolean4219 && (!Class30.method329(i + 2722, i_6_) || !Class30.method329(2844, i_7_))) {
			if (!var_ha_Sub3.aBoolean4257)
				return new Class69_Sub1_Sub1(var_ha_Sub3, i_8_, i_6_, i_7_, Class8.get_next_high_pow2(i_6_), Class8.get_next_high_pow2(i_7_));
			return new Class69_Sub1_Sub1(var_ha_Sub3, 34037, i_8_, i_6_, i_7_);
		}
		return new Class69_Sub1_Sub1(var_ha_Sub3, 3553, i_8_, i_6_, i_7_);
	}
}
