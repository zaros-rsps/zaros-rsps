package net.zaros.client;
/* Class296_Sub51_Sub10 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.datatransfer.Clipboard;

final class Class296_Sub51_Sub10 extends TextureOperation {
	private int[] anIntArray6390;
	private int anInt6391 = 10;
	private int[] anIntArray6392;
	private int anInt6393 = 2048;
	static Clipboard aClipboard6394;
	static double aDouble6395;
	private int anInt6396 = 0;

	public static void method3102(byte i) {
		aClipboard6394 = null;
		if (i < 2)
			aDouble6395 = 3.0674496596334153;
	}

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		if (i > -84)
			method3076((byte) -108);
		int i_1_ = i_0_;
		while_106_ : do {
			do {
				if (i_1_ != 0) {
					if (i_1_ != 1) {
						if (i_1_ == 2)
							break;
						break while_106_;
					}
				} else {
					anInt6391 = class296_sub17.g1();
					return;
				}
				anInt6393 = class296_sub17.g2();
				return;
			} while (false);
			anInt6396 = class296_sub17.g1();
			break;
		} while (false);
	}

	private final void method3103(int i) {
		int i_2_ = 0;
		anIntArray6390 = new int[anInt6391 + 1];
		anIntArray6392 = new int[anInt6391 + 1];
		int i_3_ = i / anInt6391;
		int i_4_ = i_3_ * anInt6393 >> 12;
		for (int i_5_ = 0; i_5_ < anInt6391; i_5_++) {
			anIntArray6392[i_5_] = i_2_;
			anIntArray6390[i_5_] = i_2_ + i_4_;
			i_2_ += i_3_;
		}
		anIntArray6392[anInt6391] = 4096;
		anIntArray6390[anInt6391] = anIntArray6390[0] + 4096;
	}

	static final ha method3104(int i, int i_6_, int i_7_, Canvas canvas, d var_d) {
		if (i_7_ < 76)
			aDouble6395 = 0.579863273404216;
		return new ha_Sub2(canvas, var_d, i, i_6_);
	}

	final void method3076(byte i) {
		int i_8_ = 59 / ((-58 - i) / 40);
		method3103(4096);
	}

	public Class296_Sub51_Sub10() {
		super(0, true);
	}

	final int[] get_monochrome_output(int i, int i_9_) {
		if (i != 0)
			return null;
		int[] is = aClass318_5035.method3335(i_9_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int i_10_ = Class294.anIntArray2686[i_9_];
			if (anInt6396 == 0) {
				int i_11_ = 0;
				for (int i_12_ = 0; i_12_ < anInt6391; i_12_++) {
					if (anIntArray6392[i_12_] <= i_10_ && i_10_ < anIntArray6392[i_12_ + 1]) {
						if (anIntArray6390[i_12_] > i_10_)
							i_11_ = 4096;
						break;
					}
				}
				Class291.method2407(is, 0, Class41_Sub10.anInt3769, i_11_);
			} else {
				for (int i_13_ = 0; Class41_Sub10.anInt3769 > i_13_; i_13_++) {
					int i_14_ = 0;
					int i_15_ = 0;
					int i_16_ = Class33.anIntArray334[i_13_];
					int i_17_ = anInt6396;
					while_107_ : do {
						do {
							if (i_17_ != 1) {
								if (i_17_ != 2) {
									if (i_17_ == 3)
										break;
									break while_107_;
								}
							} else {
								i_14_ = i_16_;
								break while_107_;
							}
							i_14_ = (i_10_ - 4096 + i_16_ >> 1) + 2048;
							break while_107_;
						} while (false);
						i_14_ = 2048 + (i_16_ - i_10_ >> 1);
					} while (false);
					for (i_17_ = 0; anInt6391 > i_17_; i_17_++) {
						if (i_14_ >= anIntArray6392[i_17_] && anIntArray6392[i_17_ + 1] > i_14_) {
							if (anIntArray6390[i_17_] > i_14_)
								i_15_ = 4096;
							break;
						}
					}
					is[i_13_] = i_15_;
				}
			}
		}
		return is;
	}

	static final void method3105(byte i, int i_18_, int i_19_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_18_, 5);
		int i_20_ = 113 / ((-53 - i) / 41);
		class296_sub39_sub5.insertIntoQueue_2();
		class296_sub39_sub5.intParam = i_19_;
	}
}
