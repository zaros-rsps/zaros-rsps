package net.zaros.client;

public class Queuable extends Node {

	public long queue_uid;
	public Queuable queue_previous;
	public Queuable queue_next;

	public void queue_unlink() {
		if (queue_previous != null) {
			queue_previous.queue_next = queue_next;
			queue_next.queue_previous = queue_previous;
			queue_previous = null;
			queue_next = null;
		}
	}

	public final boolean is_queue_linked() {
		if (queue_previous == null) {
			return false;
		}
		return true;
	}

}
