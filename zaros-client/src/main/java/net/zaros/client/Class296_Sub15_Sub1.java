package net.zaros.client;

import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;

final class Class296_Sub15_Sub1 extends Class296_Sub15 {
	static AdvancedMemoryCache aClass113_5994 = new AdvancedMemoryCache(4);
	static IncomingPacket aClass231_5995;
	static int anInt5996;
	static int anInt5997 = 0;
	static int anInt5998;
	static int[] anIntArray5999;

	@Override
	final void method2517(OggPacket oggpacket, int i) {
		if (i != 16) {
			anInt5998 = -117;
		}
	}

	Class296_Sub15_Sub1(OggStreamState oggstreamstate) {
		super(oggstreamstate);
	}

	@Override
	final void method2519(int i) {
		if (i != 16) {
			method2517(null, -100);
		}
	}

	static final void method2526(byte i, int i_1_) {
		if (i_1_ == 37) {
			Class106.aFloat1103 = 3.0F;
		} else if (i_1_ != 50) {
			if (i_1_ == 75) {
				Class106.aFloat1103 = 6.0F;
			} else if (i_1_ == 100) {
				Class106.aFloat1103 = 8.0F;
			} else if (i_1_ == 200) {
				Class106.aFloat1103 = 16.0F;
			}
		} else {
			Class106.aFloat1103 = 4.0F;
		}
		BITConfigDefinition.anInt2604 = -1;
		if (i == 79) {
			BITConfigDefinition.anInt2604 = -1;
		}
	}

	public static void method2527(int i) {
		anIntArray5999 = null;
		aClass113_5994 = null;
		if (i == 14574) {
			aClass231_5995 = null;
		}
	}

	static {
		anInt5996 = 503;
		aClass231_5995 = new IncomingPacket(1, 0);
		anIntArray5999 = new int[1000];
	}
}
