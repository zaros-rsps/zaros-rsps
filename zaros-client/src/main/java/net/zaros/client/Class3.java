package net.zaros.client;

/* Class3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class3 {
	static ModeWhere office3ModeWhere = new ModeWhere("WTWIP", "office", "_wip", 3);
	static OutgoingPacket aClass311_69 = new OutgoingPacket(55, -1);

	public static void method171(byte i) {
		if (i != -24)
			method172(73, 16, null, 99);
		office3ModeWhere = null;
		aClass311_69 = null;
	}

	static final void method172(int i, int i_0_, InterfaceComponent class51, int i_1_) {
		if (class51 != null) {
			if (i_1_ != 22635)
				aClass311_69 = null;
			if (class51.anObjectArray512 != null) {
				CS2Call class296_sub46 = new CS2Call();
				class296_sub46.callArgs = class51.anObjectArray512;
				class296_sub46.callerInterface = class51;
				CS2Executor.runCS2(class296_sub46);
			}
			Class127.aBoolean1304 = true;
			Class321.anInt2824 = i_0_;
			Class353.anInt3047 = i;
			Class69.anInt3689 = class51.clickedItem;
			Class366_Sub4.anInt5375 = class51.uid;
			IOException_Sub1.anInt36 = class51.anInt519;
			Class180.anInt1857 = class51.anInt592;
			Class41_Sub19.anInt3793 = class51.anInt501;
			Class332.method3416(class51, (byte) 127);
		}
	}
}
