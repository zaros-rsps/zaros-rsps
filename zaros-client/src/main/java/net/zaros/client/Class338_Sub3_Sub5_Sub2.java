package net.zaros.client;

/* Class338_Sub3_Sub5_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class338_Sub3_Sub5_Sub2 extends Class338_Sub3_Sub5 implements Interface14 {
	private Class96 aClass96_6660;
	private byte aByte6661;
	static int[] anIntArray6662 = new int[120];
	private boolean aBoolean6663;
	private boolean aBoolean6664;
	private boolean aBoolean6665;
	private r aR6666;
	static int anInt6667;
	private int locid;
	static float[] aFloatArray6669 = new float[16];
	private Model aClass178_6670;
	private boolean aBoolean6671;

	@Override
	public final void method58(int i) {
		if (i == -19727) {
			if (aClass178_6670 != null) {
				aClass178_6670.method1722();
			}
		}
	}

	@Override
	final boolean method3459(int i) {
		if (i != 0) {
			return false;
		}
		if (aClass178_6670 == null) {
			return true;
		}
		if (aClass178_6670.r()) {
			return false;
		}
		return true;
	}

	Class338_Sub3_Sub5_Sub2(ha var_ha, ObjectDefinition class70, int i, int i_0_, int i_1_, int i_2_, int i_3_, boolean bool, int i_4_, boolean bool_5_) {
		super(i_1_, i_2_, i_3_, i, i_0_, class70.anInt776);
		aBoolean6671 = class70.interactonType != 0 && !bool;
		aBoolean6663 = bool;
		locid = class70.ID;
		aBoolean6664 = bool_5_;
		aByte6661 = (byte) i_4_;
		tileY = i_3_;
		tileX = i_1_;
		aBoolean6665 = var_ha.B() && class70.aBoolean779 && !aBoolean6663 && Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method465(127) != 0;
		int i_6_ = 2048;
		if (aBoolean6664) {
			i_6_ |= 0x10000;
		}
		Class188 class188 = method3587(aBoolean6665, i_6_, -1, var_ha);
		if (class188 != null) {
			aR6666 = class188.aR1925;
			aClass178_6670 = class188.aClass178_1926;
			if (aBoolean6664) {
				aClass178_6670 = aClass178_6670.method1728((byte) 0, i_6_, false);
			}
		}
	}

	private final Class188 method3587(boolean bool, int i, int i_7_, ha var_ha) {
		ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(locid);
		if (i_7_ != -1) {
			return null;
		}
		s var_s;
		s var_s_8_;
		if (!aBoolean6663) {
			var_s = Class244.aSArray2320[aByte5203];
			if (aByte5203 < 3) {
				var_s_8_ = Class244.aSArray2320[aByte5203 + 1];
			} else {
				var_s_8_ = null;
			}
		} else {
			var_s = Class52.aSArray636[aByte5203];
			var_s_8_ = Class244.aSArray2320[0];
		}
		return class70.method750(var_s, var_s_8_, var_ha, bool, -954198435, tileY, tileX, i, null, aByte6661, anInt5213, 22);
	}

	static final void method3588(int i, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_) {
		int i_15_ = ParticleEmitterRaw.method1668(i_14_, RuntimeException_Sub1.anInt3391, EmissiveTriangle.anInt952, (byte) -73);
		int i_16_ = ParticleEmitterRaw.method1668(i_12_, RuntimeException_Sub1.anInt3391, EmissiveTriangle.anInt952, (byte) 123);
		if (i_9_ != 0) {
			method3589((byte) -52);
		}
		int i_17_ = ParticleEmitterRaw.method1668(i_11_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 122);
		int i_18_ = ParticleEmitterRaw.method1668(i, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 125);
		int i_19_ = ParticleEmitterRaw.method1668(i_10_ + i_14_, RuntimeException_Sub1.anInt3391, EmissiveTriangle.anInt952, (byte) 122);
		int i_20_ = ParticleEmitterRaw.method1668(i_12_ - i_10_, RuntimeException_Sub1.anInt3391, EmissiveTriangle.anInt952, (byte) -127);
		for (int i_21_ = i_15_; i_21_ < i_19_; i_21_++) {
			Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_21_], i_18_, (byte) 118, i_13_, i_17_);
		}
		for (int i_22_ = i_16_; i_22_ > i_20_; i_22_--) {
			Class296_Sub14.method2511(Class296_Sub51_Sub37.anIntArrayArray6536[i_22_], i_18_, (byte) -13, i_13_, i_17_);
		}
		int i_23_ = ParticleEmitterRaw.method1668(i_11_ + i_10_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 127);
		int i_24_ = ParticleEmitterRaw.method1668(-i_10_ + i, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 120);
		for (int i_25_ = i_19_; i_20_ >= i_25_; i_25_++) {
			int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_25_];
			Class296_Sub14.method2511(is, i_23_, (byte) 8, i_13_, i_17_);
			Class296_Sub14.method2511(is, i_18_, (byte) -57, i_13_, i_24_);
		}
	}

	@Override
	final void method3460(int i, ha var_ha) {
		int i_26_ = -95 / ((i + 41) / 62);
	}

	@Override
	final Class338_Sub2 method3473(ha var_ha, byte i) {
		if (i > -84) {
			method3589((byte) 126);
		}
		if (aClass178_6670 == null) {
			return null;
		}
		Class373 class373 = var_ha.f();
		class373.method3902(tileX, anInt5213, tileY);
		Class338_Sub2 class338_sub2 = Class144.method1481(1, true, aBoolean6671);
		if (!Class296_Sub39_Sub10.aBoolean6177) {
			aClass178_6670.method1715(class373, class338_sub2.aClass338_Sub5Array5194[0], 0);
		} else {
			aClass178_6670.method1719(class373, class338_sub2.aClass338_Sub5Array5194[0], ModeWhat.anInt1192, 0);
		}
		return class338_sub2;
	}

	public static void method3589(byte i) {
		if (i != -54) {
			aFloatArray6669 = null;
		}
		aFloatArray6669 = null;
		anIntArray6662 = null;
	}

	@Override
	final void method3467(int i, int i_27_, Class338_Sub3 class338_sub3, boolean bool, int i_28_, int i_29_, ha var_ha) {
		int i_30_ = 114 / ((i_29_ - 20) / 48);
		if (class338_sub3 instanceof Class338_Sub3_Sub5_Sub2) {
			Class338_Sub3_Sub5_Sub2 class338_sub3_sub5_sub2_31_ = (Class338_Sub3_Sub5_Sub2) class338_sub3;
			if (aClass178_6670 != null && class338_sub3_sub5_sub2_31_.aClass178_6670 != null) {
				aClass178_6670.method1736(class338_sub3_sub5_sub2_31_.aClass178_6670, i_27_, i_28_, i, bool);
			}
		}
	}

	@Override
	public final void method56(ha var_ha, byte i) {
		if (i != -117) {
			method59(89);
		}
		Object object = null;
		r var_r;
		if (aR6666 == null && aBoolean6665) {
			Class188 class188 = method3587(true, 262144, -1, var_ha);
			var_r = class188 == null ? null : class188.aR1925;
		} else {
			var_r = aR6666;
			aR6666 = null;
		}
		if (var_r != null) {
			Class296_Sub39_Sub20_Sub2.method2910(var_r, aByte5203, tileX, tileY, null);
		}
	}

	@Override
	final int method3462(byte i) {
		if (i != 28) {
			return -61;
		}
		if (aClass178_6670 == null) {
			return 0;
		}
		return aClass178_6670.ma();
	}

	@Override
	final boolean method3469(int i) {
		if (i < 82) {
			anIntArray6662 = null;
		}
		if (aClass178_6670 == null) {
			return false;
		}
		return aClass178_6670.F();
	}

	@Override
	final boolean method3468(int i) {
		if (i >= -29) {
			method3587(false, 25, 52, null);
		}
		return aBoolean6664;
	}

	@Override
	public final boolean method55(byte i) {
		if (i != -57) {
			aBoolean6665 = true;
		}
		return aBoolean6665;
	}

	@Override
	public final void method60(byte i, ha var_ha) {
		Object object = null;
		int i_32_ = -39 % ((i + 23) / 35);
		r var_r;
		if (aR6666 != null || !aBoolean6665) {
			var_r = aR6666;
			aR6666 = null;
		} else {
			Class188 class188 = method3587(true, 262144, -1, var_ha);
			var_r = class188 == null ? null : class188.aR1925;
		}
		if (var_r != null) {
			Class296_Sub45_Sub4.method3017(var_r, aByte5203, tileX, tileY, null);
		}
	}

	@Override
	final void method3472(byte i) {
		aBoolean6664 = false;
		if (aClass178_6670 != null) {
			aClass178_6670.s(aClass178_6670.ua() & ~0x10000);
		}
		int i_33_ = 102 % ((-56 - i) / 38);
	}

	@Override
	public final int method59(int i) {
		if (i <= 16) {
			method3459(-128);
		}
		return aByte6661;
	}

	@Override
	public final int method54(int i) {
		if (i != -11077) {
			method56(null, (byte) 17);
		}
		return 22;
	}

	private final Model method3590(int i, ha var_ha, int i_34_) {
		if (i_34_ != 262144) {
			aClass96_6660 = null;
		}
		if (aClass178_6670 != null && var_ha.e(aClass178_6670.ua(), i) == 0) {
			return aClass178_6670;
		}
		Class188 class188 = method3587(false, i, -1, var_ha);
		if (class188 == null) {
			return null;
		}
		return class188.aClass178_1926;
	}

	@Override
	final int method3466(byte i) {
		if (i <= 77) {
			method3466((byte) 51);
		}
		if (aClass178_6670 == null) {
			return 0;
		}
		return aClass178_6670.fa();
	}

	@Override
	public final int method57(byte i) {
		if (i <= 83) {
			method3461(null, -108);
		}
		return locid;
	}

	@Override
	final boolean method3475(int i, int i_35_, ha var_ha, int i_36_) {
		if (i_35_ >= -48) {
			return true;
		}
		Model class178 = method3590(131072, var_ha, 262144);
		if (class178 != null) {
			Class373 class373 = var_ha.f();
			class373.method3902(tileX, anInt5213, tileY);
			if (Class296_Sub39_Sub10.aBoolean6177) {
				return class178.method1731(i_36_, i, class373, false, 0, ModeWhat.anInt1192);
			}
			return class178.method1732(i_36_, i, class373, false, 0);
		}
		return false;
	}

	@Override
	final Class96 method3461(ha var_ha, int i) {
		int i_37_ = -69 % ((79 - i) / 44);
		if (aClass96_6660 == null) {
			aClass96_6660 = Class41_Sub21.method478(anInt5213, method3590(0, var_ha, 262144), tileX, tileY, (byte) 104);
		}
		return aClass96_6660;
	}

	static {
		anInt6667 = 1;
		int i = 0;
		for (int i_38_ = 0; i_38_ < 120; i_38_++) {
			int i_39_ = i_38_ + 1;
			int i_40_ = (int) (Math.pow(2.0, i_39_ / 7.0) * 300.0 + i_39_);
			i += i_40_;
			anIntArray6662[i_38_] = i / 4;
		}
	}
}
