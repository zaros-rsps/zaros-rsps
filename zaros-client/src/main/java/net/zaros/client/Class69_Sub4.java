package net.zaros.client;
import jaggl.OpenGL;

final class Class69_Sub4 extends Class69 {
	static IncomingPacket aClass231_5730 = new IncomingPacket(103, 0);
	private int anInt5731;
	static Sprite[] aClass397Array5732;

	public final void method73(boolean bool) {
		if (bool != true)
			method745(18, 8, 10);
	}

	static final boolean method743(Class96 class96, int i, boolean bool, int i_0_, int i_1_, int i_2_, int i_3_) {
		if (!Class103.aBoolean1086 || !Class12.aBoolean133)
			return false;
		if (Class296_Sub39_Sub20.anInt6255 < 100)
			return false;
		if (i_2_ == i && i_0_ == i_3_) {
			if (!Class50.method614((byte) -118, i_2_, i_3_, i_1_))
				return false;
			if (r_Sub1.method2863(class96, (byte) -102)) {
				Class264.anInt2473++;
				return true;
			}
			return false;
		}
		if (bool != true)
			return true;
		for (int i_4_ = i_2_; i >= i_4_; i_4_++) {
			for (int i_5_ = i_3_; i_0_ >= i_5_; i_5_++) {
				if (-Class86.anInt936 == (Class296_Sub36.anIntArrayArrayArray4870[i_1_][i_4_][i_5_]))
					return false;
			}
		}
		if (!r_Sub1.method2863(class96, (byte) -24))
			return false;
		Class264.anInt2473++;
		return true;
	}

	final void method744(int i, boolean bool) {
		aHa_Sub3_3681.method1316(this, (byte) -126);
		OpenGL.glTexParameteri(anInt3682, 10242, !bool ? 33071 : 10497);
		if (i < 46)
			method744(82, false);
	}

	static final int method745(int i, int i_6_, int i_7_) {
		int i_8_ = i_7_ >>> 24;
		int i_9_ = 255 - i_8_;
		i_7_ = (i_6_ & (i_7_ & 0xff00) * i_8_ | i_8_ * (i_7_ & 0xff00ff) & ~0xff00ff) >>> 8;
		return i_7_ + (((i & 0xff00ff) * i_9_ & ~0xff00ff | (i & 0xff00) * i_9_ & 0xff0000) >>> 8);
	}

	Class69_Sub4(ha_Sub3 var_ha_Sub3, int i, int i_10_, byte[] is, int i_11_) {
		super(var_ha_Sub3, 3552, i, i_10_, false);
		anInt5731 = i_10_;
		aHa_Sub3_3681.method1316(this, (byte) -108);
		OpenGL.glPixelStorei(3317, 1);
		OpenGL.glTexImage1Dub(anInt3682, 0, anInt3685, anInt5731, 0, i_11_, 5121, is, 0);
		OpenGL.glPixelStorei(3317, 4);
		this.method723(92, true);
	}

	public static void method746(int i) {
		aClass397Array5732 = null;
		aClass231_5730 = null;
		if (i != 0)
			aClass231_5730 = null;
	}

	static final void method747(int i, int i_12_, boolean bool, int i_13_, int i_14_, int i_15_, int i_16_) {
		if (bool == true) {
			for (Class338_Sub9 class338_sub9 = (Class338_Sub9) Class296_Sub51_Sub39.aClass404_6546.method4160((byte) -57); class338_sub9 != null; class338_sub9 = (Class338_Sub9) Class296_Sub51_Sub39.aClass404_6546.method4163(-24917)) {
				if (Class29.anInt307 >= class338_sub9.anInt5259)
					class338_sub9.method3438(false);
				else {
					Class296_Sub44.method2925(i_16_, (byte) -44, class338_sub9.anInt5261 * 2, class338_sub9.anInt5260, (class338_sub9.anInt5264 << 9) + 256, i_13_ >> 1, i_14_ >> 1, (class338_sub9.anInt5262 << 9) + 256, i);
					Class49.aClass55_461.a(15897, class338_sub9.aString5265, 0, class338_sub9.anInt5258 | ~0xffffff, (i_12_ + StaticMethods.anIntArray4629[0]), i_15_ + (StaticMethods.anIntArray4629[1]));
				}
			}
		}
	}
}
