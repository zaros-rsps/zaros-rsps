package net.zaros.client;

/* Class192 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class192 implements Interface9 {
	static InterfaceComponent[][] openedInterfaceComponents;
	static IncomingPacket aClass231_3561 = new IncomingPacket(42, 12);
	private Class156 aClass156_3562;

	public final void method43(boolean bool, int i) {
		if (bool)
			Class41_Sub13.aHa3774.aa(0, 0, Class241.anInt2301, Class384.anInt3254, aClass156_3562.anInt3591, 0);
		int i_0_ = -62 / ((-32 - i) / 52);
	}

	public static void method1922(int i) {
		if (i < 28)
			method1923(72, null, null);
		openedInterfaceComponents = null;
		aClass231_3561 = null;
	}

	public final boolean method44(byte i) {
		if (i < 91)
			aClass231_3561 = null;
		return true;
	}

	public final void method42(byte i) {
		if (i != 88)
			method1923(-52, null, null);
	}

	static final void method1923(int i, int[] is, int[] is_1_) {
		if (is == null || is_1_ == null) {
			aa_Sub1.aByteArrayArrayArray3721 = null;
			Class365.anIntArray3117 = null;
			Class76.anIntArray873 = null;
		} else {
			Class76.anIntArray873 = is;
			Class365.anIntArray3117 = new int[is.length];
			aa_Sub1.aByteArrayArrayArray3721 = new byte[is.length][][];
			if (i >= -21)
				openedInterfaceComponents = null;
			for (int i_2_ = 0; Class76.anIntArray873.length > i_2_; i_2_++)
				aa_Sub1.aByteArrayArrayArray3721[i_2_] = new byte[is_1_[i_2_]][];
		}
	}

	Class192(Class156 class156) {
		aClass156_3562 = class156;
	}
}
