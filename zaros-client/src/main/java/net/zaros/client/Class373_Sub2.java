package net.zaros.client;

/* Class373_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class373_Sub2 extends Class373 {
	float aFloat5590;
	float aFloat5591;
	static Class246 aClass246_5592 = new Class246();
	float aFloat5593;
	float aFloat5594;
	float aFloat5595;
	float aFloat5596;
	float aFloat5597;
	float aFloat5598;
	float aFloat5599;
	static int[][] anIntArrayArray5600 = {{12, 12, 12, 12}, {12, 12, 12, 12, 12, 5}, {5, 5, 1, 1}, {5, 1, 1, 5}, {5, 5, 5}, {5, 5, 5}, {12, 12, 12, 12, 12, 12}, {1, 12, 12, 12, 12, 12}, {1, 1, 7, 1}, {8, 9, 9, 8, 8, 3, 1, 9}, {8, 8, 9, 8, 9, 9}, {10, 10, 11, 11, 11, 7, 3, 7}, {12, 12, 12, 12}};
	float aFloat5601;
	float aFloat5602;
	float aFloat5603;

	public final void method3914(int i) {
		float f = Class5.aFloatArray78[i & 0x3fff];
		float f_0_ = Class5.aFloatArray76[i & 0x3fff];
		float f_1_ = aFloat5603;
		float f_2_ = aFloat5596;
		float f_3_ = aFloat5597;
		aFloat5603 = -(aFloat5601 * f_0_) + f_1_ * f;
		float f_4_ = aFloat5590;
		aFloat5601 = f * aFloat5601 + f_1_ * f_0_;
		aFloat5596 = f * f_2_ - f_0_ * aFloat5599;
		aFloat5599 = f_2_ * f_0_ + aFloat5599 * f;
		aFloat5597 = -(f_0_ * aFloat5591) + f_3_ * f;
		aFloat5590 = -(aFloat5594 * f_0_) + f * f_4_;
		aFloat5591 = f * aFloat5591 + f_3_ * f_0_;
		aFloat5594 = aFloat5594 * f + f_4_ * f_0_;
	}

	final float method3926(float f, byte i, float f_5_, float f_6_) {
		if (i != 53)
			method3946(-1.387333F, -0.24632479F, -7, -1.9680147F);
		return (aFloat5594 + (f_6_ * aFloat5591 + (aFloat5601 * f_5_ + aFloat5599 * f)));
	}

	public final void method3903(int i, int i_7_, int i_8_, int[] is) {
		is[1] = (int) ((float) i_8_ * aFloat5597 + (aFloat5603 * (float) i + (float) i_7_ * aFloat5596) + aFloat5590);
		is[2] = (int) (aFloat5594 + ((float) i * aFloat5601 + aFloat5599 * (float) i_7_ + aFloat5591 * (float) i_8_));
		is[0] = (int) (aFloat5593 + ((float) i * aFloat5598 + aFloat5595 * (float) i_7_ + aFloat5602 * (float) i_8_));
	}

	final void method3927(float f, float f_9_, float f_10_, byte i) {
		aFloat5593 *= f_9_;
		aFloat5603 *= f_10_;
		aFloat5599 *= f;
		aFloat5591 *= f;
		aFloat5601 *= f;
		aFloat5598 *= f_9_;
		aFloat5597 *= f_10_;
		aFloat5594 *= f;
		aFloat5590 *= f_10_;
		aFloat5602 *= f_9_;
		aFloat5595 *= f_9_;
		if (i != 62)
			method3947(51, null);
		aFloat5596 *= f_10_;
	}

	final void method3928(int i) {
		aFloat5597 = -aFloat5597;
		if (i == 11554) {
			aFloat5599 = -aFloat5599;
			aFloat5596 = -aFloat5596;
			aFloat5591 = -aFloat5591;
			aFloat5601 = -aFloat5601;
			aFloat5594 = -aFloat5594;
			aFloat5603 = -aFloat5603;
			aFloat5590 = -aFloat5590;
		}
	}

	public final void method3915(Class373 class373) {
		Class373_Sub2 class373_sub2_11_ = (Class373_Sub2) class373;
		aFloat5597 = class373_sub2_11_.aFloat5597;
		aFloat5603 = class373_sub2_11_.aFloat5603;
		aFloat5599 = class373_sub2_11_.aFloat5599;
		aFloat5596 = class373_sub2_11_.aFloat5596;
		aFloat5591 = class373_sub2_11_.aFloat5591;
		aFloat5594 = class373_sub2_11_.aFloat5594;
		aFloat5595 = class373_sub2_11_.aFloat5595;
		aFloat5598 = class373_sub2_11_.aFloat5598;
		aFloat5602 = class373_sub2_11_.aFloat5602;
		aFloat5590 = class373_sub2_11_.aFloat5590;
		aFloat5593 = class373_sub2_11_.aFloat5593;
		aFloat5601 = class373_sub2_11_.aFloat5601;
	}

	public static void method3929(byte i) {
		if (i < 96)
			method3936(null, 102, null, 111, 30, null);
		aClass246_5592 = null;
		anIntArrayArray5600 = null;
	}

	final void method3930(float f, float f_12_, byte i, float f_13_, float[] fs) {
		if (i != -56)
			aFloat5599 = 0.2785855F;
		fs[0] = f_13_ * aFloat5598 + aFloat5595 * f + f_12_ * aFloat5602;
		fs[2] = f * aFloat5599 + aFloat5601 * f_13_ + f_12_ * aFloat5591;
		fs[1] = f_13_ * aFloat5603 + f * aFloat5596 + f_12_ * aFloat5597;
	}

	public final void method3906(int i) {
		aFloat5596 = 1.0F;
		aFloat5598 = aFloat5591 = Class5.aFloatArray78[i & 0x3fff];
		aFloat5602 = Class5.aFloatArray76[i & 0x3fff];
		aFloat5601 = -aFloat5602;
		aFloat5595 = aFloat5593 = aFloat5603 = aFloat5597 = aFloat5590 = aFloat5599 = aFloat5594 = 0.0F;
	}

	final void method3931(float f, float f_14_, float f_15_, int i) {
		aFloat5593 = 0.0F;
		aFloat5596 = f_14_;
		aFloat5598 = f_15_;
		aFloat5602 = 0.0F;
		aFloat5590 = 0.0F;
		aFloat5591 = f;
		aFloat5599 = 0.0F;
		if (i >= -10)
			anIntArrayArray5600 = null;
		aFloat5603 = 0.0F;
		aFloat5601 = 0.0F;
		aFloat5595 = 0.0F;
		aFloat5597 = 0.0F;
		aFloat5594 = 0.0F;
	}

	final float[] method3932(int i, float[] fs) {
		fs[13] = aFloat5590;
		fs[1] = aFloat5603;
		fs[2] = aFloat5601;
		fs[6] = aFloat5599;
		fs[7] = 0.0F;
		fs[15] = 1.0F;
		fs[0] = aFloat5598;
		fs[5] = aFloat5596;
		fs[10] = aFloat5591;
		fs[12] = aFloat5593;
		fs[11] = 0.0F;
		fs[4] = aFloat5595;
		fs[8] = aFloat5602;
		fs[3] = (float) i;
		fs[14] = aFloat5594;
		fs[9] = aFloat5597;
		return fs;
	}

	public final void method3908(int[] is) {
		float f = -aFloat5593 + (float) is[0];
		float f_16_ = (float) is[1] - aFloat5590;
		float f_17_ = -aFloat5594 + (float) is[2];
		is[2] = (int) (aFloat5591 * f_17_ + (aFloat5597 * f_16_ + f * aFloat5602));
		is[1] = (int) (f * aFloat5595 + aFloat5596 * f_16_ + f_17_ * aFloat5599);
		is[0] = (int) (f_17_ * aFloat5601 + (aFloat5603 * f_16_ + f * aFloat5598));
	}

	static final void method3933(int i, int i_18_, int i_19_, int i_20_) {
		if (EmissiveTriangle.anInt958 != i_18_ || Class252.anInt2386 != i_19_ || Class368_Sub23.anInt5569 != i) {
			Class368_Sub23.anInt5569 = i;
			Class41.aBoolean388 = true;
			EmissiveTriangle.anInt958 = i_18_;
			Class252.anInt2386 = i_19_;
			double d = -((double) (i_18_ * 2) * 3.141592653589793) / 16384.0;
			double d_21_ = -((double) (i_19_ * 2) * 3.141592653589793) / 16384.0;
			double d_22_ = Math.cos(d_21_);
			double d_23_ = Math.sin(d_21_);
			double d_24_ = Math.cos(d);
			double d_25_ = Math.sin(d);
			Class246.aDouble2331 = d_25_ * -d_22_;
			Class296_Sub51_Sub18.aDouble6434 = d_22_ * d_24_;
			Class251.aDouble2362 = d_22_;
			Class240.aDouble2258 = d_23_;
			Class238.aDouble3630 = (double) i_20_;
			Class69_Sub3.aDouble5726 = d_25_;
			Class119.aDouble1195 = d_25_ * d_23_;
			CS2Stack.aDouble2250 = -d_23_ * d_24_;
			Class131.aDouble1343 = d_24_;
		}
	}

	final void method3934(float f, float f_26_, float[] fs, float f_27_, int i, float f_28_) {
		float f_29_;
		float f_30_;
		float f_31_;
		if (!(f_28_ > 0.00390625F) && !(f_28_ < -0.00390625F)) {
			if (f > 0.00390625F || f < -0.00390625F) {
				float f_32_ = -f_26_ / f;
				f_30_ = f_32_ * aFloat5595 + aFloat5593;
				f_31_ = aFloat5596 * f_32_ + aFloat5590;
				f_29_ = aFloat5594 + f_32_ * aFloat5599;
			} else {
				float f_33_ = -f_26_ / f_27_;
				f_30_ = aFloat5593 + aFloat5602 * f_33_;
				f_29_ = aFloat5594 + aFloat5591 * f_33_;
				f_31_ = aFloat5590 + aFloat5597 * f_33_;
			}
		} else {
			float f_34_ = -f_26_ / f_28_;
			f_29_ = aFloat5594 + f_34_ * aFloat5601;
			f_30_ = aFloat5593 + aFloat5598 * f_34_;
			f_31_ = aFloat5590 + f_34_ * aFloat5603;
		}
		fs[1] = aFloat5603 * f_28_ + aFloat5596 * f + f_27_ * aFloat5597;
		fs[0] = f * aFloat5595 + aFloat5598 * f_28_ + f_27_ * aFloat5602;
		if (i == 9) {
			fs[2] = f_27_ * aFloat5591 + (aFloat5599 * f + f_28_ * aFloat5601);
			fs[3] = -(fs[0] * f_30_ + fs[1] * f_31_ + f_29_ * fs[2]);
		}
	}

	final void method3935(int i, Class373 class373) {
		Class373_Sub2 class373_sub2_35_ = (Class373_Sub2) class373;
		aFloat5602 = class373_sub2_35_.aFloat5601;
		if (i != 4)
			aFloat5595 = 1.2824342F;
		aFloat5598 = class373_sub2_35_.aFloat5598;
		aFloat5595 = class373_sub2_35_.aFloat5603;
		aFloat5597 = class373_sub2_35_.aFloat5599;
		aFloat5596 = class373_sub2_35_.aFloat5596;
		aFloat5603 = class373_sub2_35_.aFloat5595;
		aFloat5601 = class373_sub2_35_.aFloat5602;
		aFloat5593 = -(aFloat5602 * class373_sub2_35_.aFloat5594 + (aFloat5598 * class373_sub2_35_.aFloat5593 + aFloat5595 * class373_sub2_35_.aFloat5590));
		aFloat5591 = class373_sub2_35_.aFloat5591;
		aFloat5599 = class373_sub2_35_.aFloat5597;
		aFloat5590 = -(aFloat5603 * class373_sub2_35_.aFloat5593 + aFloat5596 * class373_sub2_35_.aFloat5590 + class373_sub2_35_.aFloat5594 * aFloat5597);
		aFloat5594 = -(class373_sub2_35_.aFloat5593 * aFloat5601 + aFloat5599 * class373_sub2_35_.aFloat5590 + class373_sub2_35_.aFloat5594 * aFloat5591);
	}

	public final void method3904(int i, int i_36_, int i_37_) {
		aFloat5593 += (float) i;
		aFloat5594 += (float) i_37_;
		aFloat5590 += (float) i_36_;
	}

	public final Class373 method3916() {
		Class373_Sub2 class373_sub2_38_ = new Class373_Sub2();
		class373_sub2_38_.aFloat5591 = aFloat5591;
		class373_sub2_38_.aFloat5603 = aFloat5603;
		class373_sub2_38_.aFloat5593 = aFloat5593;
		class373_sub2_38_.aFloat5596 = aFloat5596;
		class373_sub2_38_.aFloat5602 = aFloat5602;
		class373_sub2_38_.aFloat5590 = aFloat5590;
		class373_sub2_38_.aFloat5594 = aFloat5594;
		class373_sub2_38_.aFloat5595 = aFloat5595;
		class373_sub2_38_.aFloat5601 = aFloat5601;
		class373_sub2_38_.aFloat5598 = aFloat5598;
		class373_sub2_38_.aFloat5597 = aFloat5597;
		class373_sub2_38_.aFloat5599 = aFloat5599;
		return class373_sub2_38_;
	}

	static final aa_Sub3 method3936(int[] is, int i, int[] is_39_, int i_40_, int i_41_, ha_Sub1 var_ha_Sub1) {
		int i_42_ = 126 % ((i_41_ - 54) / 57);
		if (var_ha_Sub1.method1200(Class67.aClass67_745, Class13.aClass202_3516, (byte) -117)) {
			byte[] is_43_ = new byte[i_40_ * i];
			for (int i_44_ = 0; i_44_ < i; i_44_++) {
				int i_45_ = i_40_ * i_44_ + is_39_[i_44_];
				for (int i_46_ = 0; i_46_ < is[i_44_]; i_46_++)
					is_43_[i_45_++] = (byte) -1;
			}
			return new aa_Sub3(var_ha_Sub1, i_40_, i, is_43_);
		}
		int[] is_47_ = new int[i * i_40_];
		for (int i_48_ = 0; i > i_48_; i_48_++) {
			int i_49_ = i_48_ * i_40_ + is_39_[i_48_];
			for (int i_50_ = 0; i_50_ < is[i_48_]; i_50_++)
				is_47_[i_49_++] = -16777216;
		}
		return new aa_Sub3(var_ha_Sub1, i_40_, i, is_47_);
	}

	final void method3937(Class373 class373, boolean bool) {
		Class373_Sub2 class373_sub2_51_ = (Class373_Sub2) class373;
		aFloat5602 = class373_sub2_51_.aFloat5602;
		if (bool != true)
			method3936(null, 10, null, -54, 47, null);
		aFloat5598 = class373_sub2_51_.aFloat5598;
		aFloat5599 = class373_sub2_51_.aFloat5599;
		aFloat5594 = 0.0F;
		aFloat5591 = class373_sub2_51_.aFloat5591;
		aFloat5601 = class373_sub2_51_.aFloat5601;
		aFloat5590 = 0.0F;
		aFloat5596 = class373_sub2_51_.aFloat5596;
		aFloat5597 = class373_sub2_51_.aFloat5597;
		aFloat5603 = class373_sub2_51_.aFloat5603;
		aFloat5593 = 0.0F;
		aFloat5595 = class373_sub2_51_.aFloat5595;
	}

	public final void method3900(int i) {
		float f = Class5.aFloatArray78[i & 0x3fff];
		float f_52_ = Class5.aFloatArray76[i & 0x3fff];
		float f_53_ = aFloat5598;
		float f_54_ = aFloat5595;
		float f_55_ = aFloat5602;
		aFloat5598 = -(aFloat5603 * f_52_) + f_53_ * f;
		float f_56_ = aFloat5593;
		aFloat5603 = f * aFloat5603 + f_53_ * f_52_;
		aFloat5595 = -(aFloat5596 * f_52_) + f * f_54_;
		aFloat5596 = f_54_ * f_52_ + f * aFloat5596;
		aFloat5602 = -(aFloat5597 * f_52_) + f * f_55_;
		aFloat5593 = f * f_56_ - f_52_ * aFloat5590;
		aFloat5597 = f * aFloat5597 + f_52_ * f_55_;
		aFloat5590 = f_52_ * f_56_ + aFloat5590 * f;
	}

	public final void method3902(int i, int i_57_, int i_58_) {
		aFloat5593 = (float) i;
		aFloat5590 = (float) i_57_;
		aFloat5603 = aFloat5601 = aFloat5595 = aFloat5599 = aFloat5602 = aFloat5597 = 0.0F;
		aFloat5598 = aFloat5596 = aFloat5591 = 1.0F;
		aFloat5594 = (float) i_58_;
	}

	public final void method3909(int i) {
		aFloat5598 = 1.0F;
		aFloat5596 = aFloat5591 = Class5.aFloatArray78[i & 0x3fff];
		aFloat5599 = Class5.aFloatArray76[i & 0x3fff];
		aFloat5597 = -aFloat5599;
		aFloat5595 = aFloat5602 = aFloat5593 = aFloat5603 = aFloat5590 = aFloat5601 = aFloat5594 = 0.0F;
	}

	final void method3938(Class373 class373, Class373 class373_59_) {
		Class373_Sub2 class373_sub2_60_ = (Class373_Sub2) class373;
		Class373_Sub2 class373_sub2_61_ = (Class373_Sub2) class373_59_;
		aFloat5598 = (class373_sub2_61_.aFloat5601 * class373_sub2_60_.aFloat5602 + (class373_sub2_60_.aFloat5595 * class373_sub2_61_.aFloat5603 + (class373_sub2_60_.aFloat5598 * class373_sub2_61_.aFloat5598)));
		aFloat5603 = (class373_sub2_60_.aFloat5597 * class373_sub2_61_.aFloat5601 + (class373_sub2_60_.aFloat5596 * class373_sub2_61_.aFloat5603 + (class373_sub2_61_.aFloat5598 * class373_sub2_60_.aFloat5603)));
		aFloat5595 = (class373_sub2_61_.aFloat5599 * class373_sub2_60_.aFloat5602 + (class373_sub2_61_.aFloat5595 * class373_sub2_60_.aFloat5598 + (class373_sub2_60_.aFloat5595 * class373_sub2_61_.aFloat5596)));
		aFloat5601 = (class373_sub2_60_.aFloat5601 * class373_sub2_61_.aFloat5598 + class373_sub2_61_.aFloat5603 * class373_sub2_60_.aFloat5599 + class373_sub2_61_.aFloat5601 * class373_sub2_60_.aFloat5591);
		aFloat5596 = (class373_sub2_60_.aFloat5597 * class373_sub2_61_.aFloat5599 + (class373_sub2_60_.aFloat5596 * class373_sub2_61_.aFloat5596 + (class373_sub2_60_.aFloat5603 * class373_sub2_61_.aFloat5595)));
		aFloat5602 = (class373_sub2_61_.aFloat5591 * class373_sub2_60_.aFloat5602 + (class373_sub2_60_.aFloat5595 * class373_sub2_61_.aFloat5597 + (class373_sub2_61_.aFloat5602 * class373_sub2_60_.aFloat5598)));
		aFloat5599 = (class373_sub2_61_.aFloat5599 * class373_sub2_60_.aFloat5591 + (class373_sub2_61_.aFloat5595 * class373_sub2_60_.aFloat5601 + (class373_sub2_61_.aFloat5596 * class373_sub2_60_.aFloat5599)));
		aFloat5597 = (class373_sub2_60_.aFloat5603 * class373_sub2_61_.aFloat5602 + class373_sub2_61_.aFloat5597 * class373_sub2_60_.aFloat5596 + class373_sub2_60_.aFloat5597 * class373_sub2_61_.aFloat5591);
		aFloat5593 = (class373_sub2_60_.aFloat5593 + (class373_sub2_61_.aFloat5593 * class373_sub2_60_.aFloat5598 + class373_sub2_61_.aFloat5590 * class373_sub2_60_.aFloat5595 + (class373_sub2_61_.aFloat5594 * class373_sub2_60_.aFloat5602)));
		aFloat5591 = (class373_sub2_60_.aFloat5591 * class373_sub2_61_.aFloat5591 + (class373_sub2_60_.aFloat5601 * class373_sub2_61_.aFloat5602 + (class373_sub2_61_.aFloat5597 * class373_sub2_60_.aFloat5599)));
		aFloat5590 = (class373_sub2_60_.aFloat5590 + (class373_sub2_61_.aFloat5593 * class373_sub2_60_.aFloat5603 + class373_sub2_61_.aFloat5590 * class373_sub2_60_.aFloat5596 + (class373_sub2_61_.aFloat5594 * class373_sub2_60_.aFloat5597)));
		aFloat5594 = (class373_sub2_60_.aFloat5594 + (class373_sub2_61_.aFloat5590 * class373_sub2_60_.aFloat5599 + class373_sub2_60_.aFloat5601 * class373_sub2_61_.aFloat5593 + (class373_sub2_61_.aFloat5594 * class373_sub2_60_.aFloat5591)));
	}

	final float[] method3939(float[] fs, int i) {
		fs[i] = 0.0F;
		fs[12] = 0.0F;
		fs[4] = aFloat5595;
		fs[6] = 0.0F;
		fs[1] = aFloat5603;
		fs[0] = aFloat5598;
		fs[7] = 0.0F;
		fs[11] = 0.0F;
		fs[2] = 0.0F;
		fs[5] = aFloat5596;
		fs[9] = aFloat5590;
		fs[10] = aFloat5594;
		fs[13] = 0.0F;
		fs[8] = aFloat5593;
		fs[15] = 1.0F;
		fs[3] = 0.0F;
		return fs;
	}

	public final void method3905(int i, int i_62_, int i_63_, int[] is) {
		i_63_ -= aFloat5594;
		i_62_ -= aFloat5590;
		i -= aFloat5593;
		is[1] = (int) (aFloat5595 * (float) i + (float) i_62_ * aFloat5596 + aFloat5599 * (float) i_63_);
		is[0] = (int) (aFloat5601 * (float) i_63_ + (aFloat5598 * (float) i + aFloat5603 * (float) i_62_));
		is[2] = (int) ((float) i * aFloat5602 + (float) i_62_ * aFloat5597 + (float) i_63_ * aFloat5591);
	}

	public final void method3911(int i) {
		aFloat5591 = 1.0F;
		aFloat5598 = aFloat5596 = Class5.aFloatArray78[i & 0x3fff];
		aFloat5603 = Class5.aFloatArray76[i & 0x3fff];
		aFloat5602 = aFloat5593 = aFloat5597 = aFloat5590 = aFloat5601 = aFloat5599 = aFloat5594 = 0.0F;
		aFloat5595 = -aFloat5603;
	}

	final void method3940(float f, float f_64_, float f_65_, float f_66_, float f_67_, float f_68_, float f_69_, float f_70_, float f_71_, int i) {
		aFloat5597 = f_69_;
		aFloat5595 = f_68_;
		aFloat5590 = 0.0F;
		aFloat5596 = f_71_;
		aFloat5594 = 0.0F;
		if (i >= -88)
			method3900(56);
		aFloat5599 = f_65_;
		aFloat5593 = 0.0F;
		aFloat5603 = f_66_;
		aFloat5598 = f_67_;
		aFloat5601 = f_70_;
		aFloat5591 = f_64_;
		aFloat5602 = f;
	}

	final float method3941(float f, byte i, float f_72_, float f_73_) {
		if (i != -125)
			method3943(0.07613201F, -0.42287138F, -1.6564415F, 96);
		return aFloat5590 + (aFloat5597 * f_72_ + (aFloat5596 * f_73_ + aFloat5603 * f));
	}

	final void method3942(float f, float f_74_, int i, int i_75_, int i_76_, float f_77_, int i_78_) {
		if (i_78_ != 0) {
			float f_79_ = Class5.aFloatArray78[i_78_ & 0x3fff];
			float f_80_ = Class5.aFloatArray76[i_78_ & 0x3fff];
			aFloat5601 = aFloat5599 = aFloat5602 = aFloat5597 = 0.0F;
			aFloat5603 = (float) i_76_ * (f_80_ * 2.0F);
			aFloat5594 = f;
			aFloat5591 = 1.0F;
			aFloat5598 = (float) i_76_ * (f_79_ * 2.0F);
			aFloat5596 = (float) i * (f_79_ * 2.0F);
			aFloat5590 = (f_80_ * -0.5F - f_79_ * 0.5F) * (float) (i * 2) + f_74_;
			aFloat5595 = f_80_ * -2.0F * (float) i;
			aFloat5593 = f_77_ + (f_80_ * 0.5F - f_79_ * 0.5F) * (float) (i_76_ * 2);
		} else {
			aFloat5603 = aFloat5601 = aFloat5595 = aFloat5599 = aFloat5602 = aFloat5597 = 0.0F;
			aFloat5594 = f;
			aFloat5590 = (float) -i + f_74_;
			aFloat5591 = 1.0F;
			aFloat5593 = f_77_ - (float) i_76_;
			aFloat5596 = (float) (i * 2);
			aFloat5598 = (float) (i_76_ * 2);
		}
		if (i_75_ != 12)
			aFloat5590 = 0.8669465F;
	}

	public final void method3907(int i, int i_81_, int i_82_, int i_83_, int i_84_, int i_85_) {
		float f = Class5.aFloatArray78[i_83_ & 0x3fff];
		float f_86_ = Class5.aFloatArray76[i_83_ & 0x3fff];
		float f_87_ = Class5.aFloatArray78[i_84_ & 0x3fff];
		float f_88_ = Class5.aFloatArray76[i_84_ & 0x3fff];
		float f_89_ = Class5.aFloatArray78[i_85_ & 0x3fff];
		float f_90_ = Class5.aFloatArray76[i_85_ & 0x3fff];
		float f_91_ = f_89_ * f_86_;
		float f_92_ = f_90_ * f_86_;
		aFloat5591 = f * f_87_;
		aFloat5596 = f * f_89_;
		aFloat5597 = f_87_ * f_91_ + f_88_ * f_90_;
		aFloat5602 = f_92_ * f_87_ + f_89_ * -f_88_;
		aFloat5595 = f * f_90_;
		aFloat5598 = f_87_ * f_89_ + f_92_ * f_88_;
		aFloat5601 = f * f_88_;
		aFloat5603 = f_90_ * -f_87_ + f_91_ * f_88_;
		aFloat5599 = -f_86_;
		aFloat5593 = ((float) -i * aFloat5598 - (float) i_81_ * aFloat5595 - aFloat5602 * (float) i_82_);
		aFloat5590 = (aFloat5603 * (float) -i - aFloat5596 * (float) i_81_ - aFloat5597 * (float) i_82_);
		aFloat5594 = (-((float) i_81_ * aFloat5599) + (float) -i * aFloat5601 - aFloat5591 * (float) i_82_);
	}

	public final void method3901(int i, int i_93_, int i_94_, int[] is) {
		is[1] = (int) ((float) i_94_ * aFloat5597 + (aFloat5603 * (float) i + (float) i_93_ * aFloat5596));
		is[2] = (int) ((float) i_93_ * aFloat5599 + (float) i * aFloat5601 + (float) i_94_ * aFloat5591);
		is[0] = (int) (aFloat5602 * (float) i_94_ + ((float) i * aFloat5598 + aFloat5595 * (float) i_93_));
	}

	final void method3943(float f, float f_95_, float f_96_, int i) {
		aFloat5598 = aFloat5596 = aFloat5591 = 1.0F;
		aFloat5603 = aFloat5601 = aFloat5595 = aFloat5599 = aFloat5602 = aFloat5597 = (float) i;
		aFloat5593 = f_96_;
		aFloat5594 = f_95_;
		aFloat5590 = f;
	}

	final float[] method3944(float[] fs, byte i) {
		fs[5] = aFloat5596;
		fs[3] = aFloat5593;
		fs[6] = aFloat5597;
		fs[0] = aFloat5598;
		fs[4] = aFloat5603;
		fs[8] = aFloat5601;
		fs[14] = 0.0F;
		fs[13] = 0.0F;
		fs[10] = aFloat5591;
		fs[1] = aFloat5595;
		fs[11] = aFloat5594;
		fs[7] = aFloat5590;
		if (i != 31)
			return null;
		fs[2] = aFloat5602;
		fs[9] = aFloat5599;
		fs[12] = 0.0F;
		fs[15] = 1.0F;
		return fs;
	}

	final float[] method3945(float[] fs, byte i) {
		fs[6] = aFloat5597;
		fs[3] = aFloat5593;
		fs[2] = aFloat5602;
		int i_97_ = 91 % ((i + 33) / 44);
		fs[0] = aFloat5598;
		fs[4] = aFloat5603;
		fs[5] = aFloat5596;
		fs[1] = aFloat5595;
		fs[7] = aFloat5590;
		return fs;
	}

	public final void method3917(int i) {
		float f = Class5.aFloatArray78[i & 0x3fff];
		float f_98_ = Class5.aFloatArray76[i & 0x3fff];
		float f_99_ = aFloat5598;
		float f_100_ = aFloat5595;
		float f_101_ = aFloat5602;
		float f_102_ = aFloat5593;
		aFloat5598 = aFloat5601 * f_98_ + f * f_99_;
		aFloat5595 = f_98_ * aFloat5599 + f_100_ * f;
		aFloat5601 = -(f_99_ * f_98_) + aFloat5601 * f;
		aFloat5602 = f_98_ * aFloat5591 + f_101_ * f;
		aFloat5599 = aFloat5599 * f - f_100_ * f_98_;
		aFloat5591 = aFloat5591 * f - f_101_ * f_98_;
		aFloat5593 = f_98_ * aFloat5594 + f * f_102_;
		aFloat5594 = f * aFloat5594 - f_98_ * f_102_;
	}

	final void method3946(float f, float f_103_, int i, float f_104_) {
		if (i == 16383) {
			aFloat5590 += f;
			aFloat5593 += f_104_;
			aFloat5594 += f_103_;
		}
	}

	public final void method3910() {
		aFloat5598 = aFloat5596 = aFloat5591 = 1.0F;
		aFloat5603 = aFloat5601 = aFloat5595 = aFloat5599 = aFloat5602 = aFloat5597 = aFloat5593 = aFloat5590 = aFloat5594 = 0.0F;
	}

	final void method3947(int i, Class373 class373) {
		Class373_Sub2 class373_sub2_105_ = (Class373_Sub2) class373;
		float f = aFloat5598;
		float f_106_ = aFloat5603;
		float f_107_ = aFloat5595;
		float f_108_ = aFloat5596;
		float f_109_ = aFloat5602;
		if (i != -16988)
			method3900(88);
		float f_110_ = aFloat5597;
		float f_111_ = aFloat5593;
		float f_112_ = aFloat5590;
		aFloat5598 = (f_106_ * class373_sub2_105_.aFloat5595 + f * class373_sub2_105_.aFloat5598 + aFloat5601 * class373_sub2_105_.aFloat5602);
		aFloat5603 = (f * class373_sub2_105_.aFloat5603 + class373_sub2_105_.aFloat5596 * f_106_ + aFloat5601 * class373_sub2_105_.aFloat5597);
		aFloat5595 = (aFloat5599 * class373_sub2_105_.aFloat5602 + (class373_sub2_105_.aFloat5598 * f_107_ + class373_sub2_105_.aFloat5595 * f_108_));
		aFloat5601 = (class373_sub2_105_.aFloat5591 * aFloat5601 + (f_106_ * class373_sub2_105_.aFloat5599 + class373_sub2_105_.aFloat5601 * f));
		aFloat5596 = (aFloat5599 * class373_sub2_105_.aFloat5597 + (f_108_ * class373_sub2_105_.aFloat5596 + f_107_ * class373_sub2_105_.aFloat5603));
		aFloat5597 = (class373_sub2_105_.aFloat5603 * f_109_ + class373_sub2_105_.aFloat5596 * f_110_ + aFloat5591 * class373_sub2_105_.aFloat5597);
		aFloat5602 = (aFloat5591 * class373_sub2_105_.aFloat5602 + (f_110_ * class373_sub2_105_.aFloat5595 + class373_sub2_105_.aFloat5598 * f_109_));
		aFloat5599 = (class373_sub2_105_.aFloat5591 * aFloat5599 + (f_107_ * class373_sub2_105_.aFloat5601 + f_108_ * class373_sub2_105_.aFloat5599));
		aFloat5593 = (class373_sub2_105_.aFloat5593 + (f_112_ * class373_sub2_105_.aFloat5595 + class373_sub2_105_.aFloat5598 * f_111_ + aFloat5594 * class373_sub2_105_.aFloat5602));
		aFloat5591 = (class373_sub2_105_.aFloat5599 * f_110_ + f_109_ * class373_sub2_105_.aFloat5601 + aFloat5591 * class373_sub2_105_.aFloat5591);
		aFloat5590 = (class373_sub2_105_.aFloat5590 + (aFloat5594 * class373_sub2_105_.aFloat5597 + (f_111_ * class373_sub2_105_.aFloat5603 + class373_sub2_105_.aFloat5596 * f_112_)));
		aFloat5594 = (class373_sub2_105_.aFloat5594 + (class373_sub2_105_.aFloat5601 * f_111_ + class373_sub2_105_.aFloat5599 * f_112_ + class373_sub2_105_.aFloat5591 * aFloat5594));
	}

	final float method3948(float f, float f_113_, int i, float f_114_) {
		if (i != 12)
			aFloat5597 = -0.10920699F;
		return aFloat5593 + (f_114_ * aFloat5602 + (aFloat5595 * f_113_ + aFloat5598 * f));
	}

	final float[] method3949(float[] fs, int i) {
		fs[7] = 0.0F;
		fs[9] = aFloat5599;
		fs[6] = aFloat5597;
		fs[8] = aFloat5601;
		fs[14] = 0.0F;
		fs[5] = aFloat5596;
		fs[2] = aFloat5602;
		fs[13] = 0.0F;
		fs[11] = 0.0F;
		fs[10] = aFloat5591;
		fs[1] = aFloat5595;
		fs[12] = 0.0F;
		fs[15] = 0.0F;
		fs[3] = (float) i;
		fs[4] = aFloat5603;
		fs[0] = aFloat5598;
		return fs;
	}

	public Class373_Sub2() {
		method3910();
	}
}
