package net.zaros.client;
import jaclib.memory.Buffer;
import jaclib.memory.Stream;

final class Class152 {
	private int anInt1556;
	private Class338_Sub8_Sub2[][] aClass338_Sub8_Sub2ArrayArray1557 = new Class338_Sub8_Sub2[1600][64];
	static Queue aClass145_1558;
	private Interface15_Impl2 anInterface15_Impl2_1559;
	private Interface15_Impl2 anInterface15_Impl2_1560;
	private Interface15_Impl1 anInterface15_Impl1_1561;
	private Class127 aClass127_1562;
	private Class338_Sub8_Sub2[][] aClass338_Sub8_Sub2ArrayArray1563 = new Class338_Sub8_Sub2[64][768];
	private int[] anIntArray1564;
	static int mapSizeType = -1;
	private int[] anIntArray1566;
	private int[] anIntArray1567;
	static int anInt1568;
	static NodeDeque aClass155_1569;
	static Sprite aClass397_1570;

	final void method1543(int i, ha_Sub1 var_ha_Sub1) {
		if (i != 0)
			anIntArray1564 = null;
		anInterface15_Impl2_1559.method46(786336, -116, 24);
	}

	private final void method1544(int i, ha_Sub1 var_ha_Sub1, int i_0_) {
		if (i_0_ == -8192) {
			Class296_Sub39_Sub15.aFloat6223 = var_ha_Sub1.aFloat4010;
			var_ha_Sub1.method1117(125, (float) i);
			var_ha_Sub1.method1124(16);
			var_ha_Sub1.method1221(false, -23);
			var_ha_Sub1.method1106(127, false);
			var_ha_Sub1.method1102(-116);
		}
	}

	final void method1545(byte i) {
		anInterface15_Impl2_1559.method31((byte) 82);
		int i_1_ = -90 / ((i + 1) / 51);
	}

	public static void method1546(byte i) {
		int i_2_ = -47 / ((i + 14) / 51);
		aClass397_1570 = null;
		aClass145_1558 = null;
		aClass155_1569 = null;
	}

	final void method1547(Class390 class390, ha_Sub1 var_ha_Sub1, int i, byte i_3_) {
		if (var_ha_Sub1.aClass373_Sub2_3941 != null) {
			if (i >= 0)
				method1544(i, var_ha_Sub1, -8192);
			else
				method1549(var_ha_Sub1, -31);
			float f = var_ha_Sub1.aClass373_Sub2_3941.aFloat5601;
			float f_4_ = var_ha_Sub1.aClass373_Sub2_3941.aFloat5599;
			float f_5_ = var_ha_Sub1.aClass373_Sub2_3941.aFloat5591;
			float f_6_ = var_ha_Sub1.aClass373_Sub2_3941.aFloat5594;
			try {
				int i_7_ = 0;
				int i_8_ = 2147483647;
				int i_9_ = 0;
				Class338_Sub8 class338_sub8 = class390.aClass84_3282.aClass338_Sub8_923;
				Class338_Sub8 class338_sub8_10_ = class338_sub8.aClass338_Sub8_5253;
				if (i_3_ <= 116)
					anInterface15_Impl1_1561 = null;
				for (/**/; class338_sub8 != class338_sub8_10_; class338_sub8_10_ = class338_sub8_10_.aClass338_Sub8_5253) {
					Class338_Sub8_Sub2 class338_sub8_sub2 = (Class338_Sub8_Sub2) class338_sub8_10_;
					int i_11_ = (int) (f_6_ + ((float) (class338_sub8_sub2.anInt6580 >> 12) * f + f_4_ * (float) ((class338_sub8_sub2.anInt6589) >> 12) + f_5_ * (float) ((class338_sub8_sub2.anInt6581) >> 12)));
					if (i_11_ < i_8_)
						i_8_ = i_11_;
					if (i_9_ < i_11_)
						i_9_ = i_11_;
					anIntArray1567[i_7_++] = i_11_;
				}
				int i_12_ = -i_8_ + i_9_;
				int i_13_;
				if (i_12_ + 2 <= 1600) {
					i_12_ += 2;
					i_13_ = 0;
				} else {
					i_13_ = (Class296_Sub29_Sub2.method2694((byte) -56, i_12_) + 1 - Class355.anInt3064);
					i_12_ = (i_12_ >> i_13_) + 2;
				}
				i_7_ = 0;
				class338_sub8_10_ = class338_sub8.aClass338_Sub8_5253;
				int i_14_ = -2;
				boolean bool = true;
				boolean bool_15_ = true;
				while (class338_sub8_10_ != class338_sub8) {
					anInt1556 = 0;
					for (int i_16_ = 0; i_16_ < i_12_; i_16_++)
						anIntArray1564[i_16_] = 0;
					for (int i_17_ = 0; i_17_ < 64; i_17_++)
						anIntArray1566[i_17_] = 0;
					for (/**/; class338_sub8 != class338_sub8_10_; class338_sub8_10_ = class338_sub8_10_.aClass338_Sub8_5253) {
						Class338_Sub8_Sub2 class338_sub8_sub2 = (Class338_Sub8_Sub2) class338_sub8_10_;
						if (bool_15_) {
							bool_15_ = false;
							i_14_ = class338_sub8_sub2.anInt6582;
							bool = class338_sub8_sub2.aBoolean6578;
						}
						if (i_7_ > 0 && (class338_sub8_sub2.anInt6582 != i_14_ || bool == !class338_sub8_sub2.aBoolean6578)) {
							bool_15_ = true;
							break;
						}
						int i_18_ = anIntArray1567[i_7_++] - i_8_ >> i_13_;
						if (i_18_ < 1600) {
							if (anIntArray1564[i_18_] < 64)
								aClass338_Sub8_Sub2ArrayArray1557[i_18_][anIntArray1564[i_18_]++] = class338_sub8_sub2;
							else {
								if (anIntArray1564[i_18_] == 64) {
									if (anInt1556 == 64)
										continue;
									anIntArray1564[i_18_] += anInt1556++ + 1;
								}
								aClass338_Sub8_Sub2ArrayArray1563[anIntArray1564[i_18_] - 1 - 64][anIntArray1566[(anIntArray1564[i_18_] - 65)]++] = class338_sub8_sub2;
							}
						}
					}
					var_ha_Sub1.method1133(false, i_14_ >= 0 ? i_14_ : -1, 0, false);
					if (!bool || (Class296_Sub39_Sub15.aFloat6223 == var_ha_Sub1.aFloat4010)) {
						if (var_ha_Sub1.aFloat4010 != 1.0F)
							var_ha_Sub1.xa(1.0F);
					} else
						var_ha_Sub1.xa(Class296_Sub39_Sub15.aFloat6223);
					method1550(i_12_, var_ha_Sub1, 1);
				}
			} catch (Exception exception) {
				/* empty */
			}
			method1548(var_ha_Sub1, (byte) 76);
		}
	}

	private final void method1548(ha_Sub1 var_ha_Sub1, byte i) {
		if (i > 45) {
			var_ha_Sub1.method1106(-63, true);
			var_ha_Sub1.method1221(true, -59);
			if (var_ha_Sub1.aFloat4010 != Class296_Sub39_Sub15.aFloat6223)
				var_ha_Sub1.xa(Class296_Sub39_Sub15.aFloat6223);
		}
	}

	private final void method1549(ha_Sub1 var_ha_Sub1, int i) {
		Class296_Sub39_Sub15.aFloat6223 = var_ha_Sub1.aFloat4010;
		var_ha_Sub1.method1130(8);
		var_ha_Sub1.method1221(false, -106);
		var_ha_Sub1.method1106(-106, false);
		var_ha_Sub1.method1102(-117);
		int i_19_ = -119 / ((i - 23) / 51);
	}

	private final void method1550(int i, ha_Sub1 var_ha_Sub1, int i_20_) {
		int i_21_ = 0;
		Class373_Sub2 class373_sub2 = var_ha_Sub1.method1122(65);
		float f = class373_sub2.aFloat5598;
		float f_22_ = class373_sub2.aFloat5595;
		if (i_20_ == 1) {
			float f_23_ = class373_sub2.aFloat5602;
			float f_24_ = class373_sub2.aFloat5603;
			float f_25_ = class373_sub2.aFloat5596;
			float f_26_ = class373_sub2.aFloat5597;
			float f_27_ = f + f_24_;
			float f_28_ = f_25_ + f_22_;
			float f_29_ = f_23_ + f_26_;
			float f_30_ = -f_24_ + f;
			float f_31_ = -f_25_ + f_22_;
			float f_32_ = -f_26_ + f_23_;
			float f_33_ = f_24_ - f;
			float f_34_ = -f_22_ + f_25_;
			float f_35_ = -f_23_ + f_26_;
			Buffer buffer = anInterface15_Impl2_1559.method47(true, -8102);
			if (buffer != null) {
				Stream stream = var_ha_Sub1.method1156(buffer, i_20_ - 110);
				if (Stream.a()) {
					for (int i_36_ = i - 1; i_36_ >= 0; i_36_--) {
						int i_37_ = (anIntArray1564[i_36_] > 64 ? 64 : anIntArray1564[i_36_]);
						if (i_37_ > 0) {
							for (int i_38_ = i_37_ - 1; i_38_ >= 0; i_38_--) {
								Class338_Sub8_Sub2 class338_sub8_sub2 = (aClass338_Sub8_Sub2ArrayArray1557[i_36_][i_38_]);
								int i_39_ = class338_sub8_sub2.anInt6583;
								byte i_40_ = (byte) (i_39_ >> 16);
								byte i_41_ = (byte) (i_39_ >> 8);
								byte i_42_ = (byte) i_39_;
								byte i_43_ = (byte) (i_39_ >>> 24);
								float f_44_ = (float) (class338_sub8_sub2.anInt6580 >> 12);
								float f_45_ = (float) (class338_sub8_sub2.anInt6589 >> 12);
								float f_46_ = (float) (class338_sub8_sub2.anInt6581 >> 12);
								int i_47_ = class338_sub8_sub2.anInt6579 >> 12;
								stream.a(f_27_ * (float) -i_47_ + f_44_);
								stream.a(f_45_ + f_28_ * (float) -i_47_);
								stream.a(f_29_ * (float) -i_47_ + f_46_);
								if (var_ha_Sub1.anInt3956 != 0)
									stream.a(i_40_, i_41_, i_42_, i_43_);
								else
									stream.b(i_40_, i_41_, i_42_, i_43_);
								stream.a(0.0F);
								stream.a(0.0F);
								stream.a(f_44_ + (float) i_47_ * f_30_);
								stream.a(f_45_ + f_31_ * (float) i_47_);
								stream.a(f_46_ + f_32_ * (float) i_47_);
								if (var_ha_Sub1.anInt3956 == 0)
									stream.b(i_40_, i_41_, i_42_, i_43_);
								else
									stream.a(i_40_, i_41_, i_42_, i_43_);
								stream.a(1.0F);
								stream.a(0.0F);
								stream.a(f_44_ + (float) i_47_ * f_27_);
								stream.a(f_45_ + f_28_ * (float) i_47_);
								stream.a((float) i_47_ * f_29_ + f_46_);
								if (var_ha_Sub1.anInt3956 != 0)
									stream.a(i_40_, i_41_, i_42_, i_43_);
								else
									stream.b(i_40_, i_41_, i_42_, i_43_);
								stream.a(1.0F);
								stream.a(1.0F);
								stream.a((float) i_47_ * f_33_ + f_44_);
								stream.a(f_34_ * (float) i_47_ + f_45_);
								stream.a((float) i_47_ * f_35_ + f_46_);
								if (var_ha_Sub1.anInt3956 == 0)
									stream.b(i_40_, i_41_, i_42_, i_43_);
								else
									stream.a(i_40_, i_41_, i_42_, i_43_);
								stream.a(0.0F);
								stream.a(1.0F);
								i_21_++;
							}
							if (anIntArray1564[i_36_] > 64) {
								int i_48_ = anIntArray1564[i_36_] - 64 - 1;
								for (int i_49_ = anIntArray1566[i_48_] - 1; i_49_ >= 0; i_49_--) {
									Class338_Sub8_Sub2 class338_sub8_sub2 = (aClass338_Sub8_Sub2ArrayArray1563[i_48_][i_49_]);
									int i_50_ = class338_sub8_sub2.anInt6583;
									byte i_51_ = (byte) (i_50_ >> 16);
									byte i_52_ = (byte) (i_50_ >> 8);
									byte i_53_ = (byte) i_50_;
									byte i_54_ = (byte) (i_50_ >>> 24);
									float f_55_ = (float) (class338_sub8_sub2.anInt6580 >> 12);
									float f_56_ = (float) (class338_sub8_sub2.anInt6589 >> 12);
									float f_57_ = (float) (class338_sub8_sub2.anInt6581 >> 12);
									int i_58_ = class338_sub8_sub2.anInt6579 >> 12;
									stream.a((float) -i_58_ * f_27_ + f_55_);
									stream.a(f_56_ + (float) -i_58_ * f_28_);
									stream.a((float) -i_58_ * f_29_ + f_57_);
									if (var_ha_Sub1.anInt3956 != 0)
										stream.a(i_51_, i_52_, i_53_, i_54_);
									else
										stream.b(i_51_, i_52_, i_53_, i_54_);
									stream.a(0.0F);
									stream.a(0.0F);
									stream.a(f_55_ + (float) i_58_ * f_30_);
									stream.a(f_31_ * (float) i_58_ + f_56_);
									stream.a(f_57_ + f_32_ * (float) i_58_);
									if (var_ha_Sub1.anInt3956 == 0)
										stream.b(i_51_, i_52_, i_53_, i_54_);
									else
										stream.a(i_51_, i_52_, i_53_, i_54_);
									stream.a(1.0F);
									stream.a(0.0F);
									stream.a(f_55_ + f_27_ * (float) i_58_);
									stream.a(f_56_ + f_28_ * (float) i_58_);
									stream.a(f_57_ + (float) i_58_ * f_29_);
									if (var_ha_Sub1.anInt3956 != 0)
										stream.a(i_51_, i_52_, i_53_, i_54_);
									else
										stream.b(i_51_, i_52_, i_53_, i_54_);
									stream.a(1.0F);
									stream.a(1.0F);
									stream.a(f_55_ + f_33_ * (float) i_58_);
									stream.a((float) i_58_ * f_34_ + f_56_);
									stream.a(f_35_ * (float) i_58_ + f_57_);
									if (var_ha_Sub1.anInt3956 == 0)
										stream.b(i_51_, i_52_, i_53_, i_54_);
									else
										stream.a(i_51_, i_52_, i_53_, i_54_);
									stream.a(0.0F);
									stream.a(1.0F);
									i_21_++;
								}
							}
						}
					}
				} else {
					for (int i_59_ = i - 1; i_59_ >= 0; i_59_--) {
						int i_60_ = (anIntArray1564[i_59_] > 64 ? 64 : anIntArray1564[i_59_]);
						if (i_60_ > 0) {
							for (int i_61_ = i_60_ - 1; i_61_ >= 0; i_61_--) {
								Class338_Sub8_Sub2 class338_sub8_sub2 = (aClass338_Sub8_Sub2ArrayArray1557[i_59_][i_61_]);
								int i_62_ = class338_sub8_sub2.anInt6583;
								byte i_63_ = (byte) (i_62_ >> 16);
								byte i_64_ = (byte) (i_62_ >> 8);
								byte i_65_ = (byte) i_62_;
								byte i_66_ = (byte) (i_62_ >>> 24);
								float f_67_ = (float) (class338_sub8_sub2.anInt6580 >> 12);
								float f_68_ = (float) (class338_sub8_sub2.anInt6589 >> 12);
								float f_69_ = (float) (class338_sub8_sub2.anInt6581 >> 12);
								int i_70_ = class338_sub8_sub2.anInt6579 >> 12;
								stream.b((float) -i_70_ * f_27_ + f_67_);
								stream.b(f_28_ * (float) -i_70_ + f_68_);
								stream.b(f_69_ + f_29_ * (float) -i_70_);
								if (var_ha_Sub1.anInt3956 == 0)
									stream.b(i_63_, i_64_, i_65_, i_66_);
								else
									stream.a(i_63_, i_64_, i_65_, i_66_);
								stream.b(0.0F);
								stream.b(0.0F);
								stream.b(f_67_ + (float) i_70_ * f_30_);
								stream.b(f_68_ + (float) i_70_ * f_31_);
								stream.b(f_69_ + f_32_ * (float) i_70_);
								if (var_ha_Sub1.anInt3956 != 0)
									stream.a(i_63_, i_64_, i_65_, i_66_);
								else
									stream.b(i_63_, i_64_, i_65_, i_66_);
								stream.b(1.0F);
								stream.b(0.0F);
								stream.b(f_67_ + (float) i_70_ * f_27_);
								stream.b(f_68_ + f_28_ * (float) i_70_);
								stream.b(f_29_ * (float) i_70_ + f_69_);
								if (var_ha_Sub1.anInt3956 != 0)
									stream.a(i_63_, i_64_, i_65_, i_66_);
								else
									stream.b(i_63_, i_64_, i_65_, i_66_);
								stream.b(1.0F);
								stream.b(1.0F);
								stream.b(f_33_ * (float) i_70_ + f_67_);
								stream.b((float) i_70_ * f_34_ + f_68_);
								stream.b((float) i_70_ * f_35_ + f_69_);
								if (var_ha_Sub1.anInt3956 == 0)
									stream.b(i_63_, i_64_, i_65_, i_66_);
								else
									stream.a(i_63_, i_64_, i_65_, i_66_);
								stream.b(0.0F);
								i_21_++;
								stream.b(1.0F);
							}
							if (anIntArray1564[i_59_] > 64) {
								int i_71_ = anIntArray1564[i_59_] - 64 - 1;
								for (int i_72_ = anIntArray1566[i_71_] - 1; i_72_ >= 0; i_72_--) {
									Class338_Sub8_Sub2 class338_sub8_sub2 = (aClass338_Sub8_Sub2ArrayArray1563[i_71_][i_72_]);
									int i_73_ = class338_sub8_sub2.anInt6583;
									byte i_74_ = (byte) (i_73_ >> 16);
									byte i_75_ = (byte) (i_73_ >> 8);
									byte i_76_ = (byte) i_73_;
									byte i_77_ = (byte) (i_73_ >>> 24);
									float f_78_ = (float) (class338_sub8_sub2.anInt6580 >> 12);
									float f_79_ = (float) (class338_sub8_sub2.anInt6589 >> 12);
									float f_80_ = (float) (class338_sub8_sub2.anInt6581 >> 12);
									int i_81_ = class338_sub8_sub2.anInt6579 >> 12;
									stream.b(f_27_ * (float) -i_81_ + f_78_);
									stream.b(f_28_ * (float) -i_81_ + f_79_);
									stream.b(f_80_ + f_29_ * (float) -i_81_);
									if (var_ha_Sub1.anInt3956 == 0)
										stream.b(i_74_, i_75_, i_76_, i_77_);
									else
										stream.a(i_74_, i_75_, i_76_, i_77_);
									stream.b(0.0F);
									stream.b(0.0F);
									stream.b(f_78_ + f_30_ * (float) i_81_);
									stream.b((float) i_81_ * f_31_ + f_79_);
									stream.b(f_80_ + f_32_ * (float) i_81_);
									if (var_ha_Sub1.anInt3956 != 0)
										stream.a(i_74_, i_75_, i_76_, i_77_);
									else
										stream.b(i_74_, i_75_, i_76_, i_77_);
									stream.b(1.0F);
									stream.b(0.0F);
									stream.b(f_78_ + f_27_ * (float) i_81_);
									stream.b(f_79_ + f_28_ * (float) i_81_);
									stream.b(f_80_ + (float) i_81_ * f_29_);
									if (var_ha_Sub1.anInt3956 != 0)
										stream.a(i_74_, i_75_, i_76_, i_77_);
									else
										stream.b(i_74_, i_75_, i_76_, i_77_);
									stream.b(1.0F);
									stream.b(1.0F);
									stream.b((float) i_81_ * f_33_ + f_78_);
									stream.b((float) i_81_ * f_34_ + f_79_);
									stream.b(f_80_ + (float) i_81_ * f_35_);
									if (var_ha_Sub1.anInt3956 != 0)
										stream.a(i_74_, i_75_, i_76_, i_77_);
									else
										stream.b(i_74_, i_75_, i_76_, i_77_);
									stream.b(0.0F);
									stream.b(1.0F);
									i_21_++;
								}
							}
						}
					}
				}
				stream.b();
				if (anInterface15_Impl2_1559.method49(2968)) {
					var_ha_Sub1.method1178(anInterface15_Impl2_1559, (byte) -127, 0);
					var_ha_Sub1.method1178(anInterface15_Impl2_1560, (byte) -124, 1);
					var_ha_Sub1.method1209(aClass127_1562, (byte) -99);
					var_ha_Sub1.method1232(Class166_Sub1.aClass289_4298, anInterface15_Impl1_1561, i_21_ * 4, i_21_ * 2, 0, 64, 0);
				}
			}
		}
	}

	Class152(ha_Sub1 var_ha_Sub1) {
		anInt1556 = 0;
		anIntArray1566 = new int[64];
		anIntArray1567 = new int[8191];
		anIntArray1564 = new int[1600];
		aClass127_1562 = var_ha_Sub1.method1233((byte) -46, (new Class211[]{new Class211(new Class356[]{Class356.aClass356_3072, Class356.aClass356_3075, (Class356.aClass356_3077)}), new Class211(Class356.aClass356_3073)}));
		anInterface15_Impl2_1559 = var_ha_Sub1.method1205(true, -125);
		anInterface15_Impl2_1560 = var_ha_Sub1.method1205(false, 100);
		anInterface15_Impl2_1560.method46(393168, 20, 12);
		anInterface15_Impl1_1561 = var_ha_Sub1.method1154(false, -88);
		anInterface15_Impl1_1561.method32(49146, -21709);
		Buffer buffer = anInterface15_Impl1_1561.method30((byte) -83, true);
		if (buffer != null) {
			Stream stream = var_ha_Sub1.method1156(buffer, -102);
			if (!Stream.a()) {
				for (int i = 0; i < 8191; i++) {
					int i_82_ = i * 4;
					stream.a(i_82_);
					stream.a(i_82_ + 1);
					stream.a(i_82_ + 2);
					stream.a(i_82_ + 2);
					stream.a(i_82_ + 3);
					stream.a(i_82_);
				}
			} else {
				for (int i = 0; i < 8191; i++) {
					int i_83_ = i * 4;
					stream.d(i_83_);
					stream.d(i_83_ + 1);
					stream.d(i_83_ + 2);
					stream.d(i_83_ + 2);
					stream.d(i_83_ + 3);
					stream.d(i_83_);
				}
			}
			stream.b();
			anInterface15_Impl1_1561.method33(32185);
		}
		Buffer buffer_84_ = anInterface15_Impl2_1560.method47(true, -8102);
		if (buffer_84_ != null) {
			Stream stream = var_ha_Sub1.method1156(buffer_84_, -121);
			if (Stream.a()) {
				for (int i = 0; i < 8191; i++) {
					stream.a(0.0F);
					stream.a(-1.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(-1.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(-1.0F);
					stream.a(0.0F);
					stream.a(0.0F);
					stream.a(-1.0F);
					stream.a(0.0F);
				}
			} else {
				for (int i = 0; i < 8191; i++) {
					stream.b(0.0F);
					stream.b(-1.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(-1.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(-1.0F);
					stream.b(0.0F);
					stream.b(0.0F);
					stream.b(-1.0F);
					stream.b(0.0F);
				}
			}
			stream.b();
			anInterface15_Impl2_1560.method49(2968);
		}
	}

	static {
		aClass145_1558 = new Queue();
		aClass155_1569 = new NodeDeque();
	}
}
