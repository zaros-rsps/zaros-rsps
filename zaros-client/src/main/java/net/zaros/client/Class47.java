package net.zaros.client;

/* Class47 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class47 {
	static int anInt444 = 50;
	short[] aShortArray445;
	short[] aShortArray446;
	static Class79[] aClass79Array447 = new Class79[anInt444];
	static int[] anIntArray448 = new int[anInt444];
	static int[] anIntArray449;
	byte[] aByteArray450;
	static int[] anIntArray451;
	static int anInt452 = 0;
	short[] aShortArray453;

	static final void method596(Class296_Sub13 class296_sub13, boolean bool, boolean bool_0_, byte i) {
		int i_1_ = class296_sub13.anInt4657;
		int i_2_ = (int) class296_sub13.uid;
		class296_sub13.unlink();
		if (bool)
			Class100.method877(66, i_1_);
		if (i < 40)
			method597(0);
		ObjectDefinitionLoader.method369(-124, i_1_);
		InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(i_2_);
		if (class51 != null)
			Class332.method3416(class51, (byte) 90);
		Class315.method3322(-120);
		if (!bool_0_ && Class99.anInt1064 != -1)
			Class296_Sub28.method2686(1, Class99.anInt1064, (byte) 84);
		Class244 class244 = new Class244(Class386.aClass263_3264);
		for (Class296_Sub13 class296_sub13_3_ = (Class296_Sub13) class244.method2177(-88); class296_sub13_3_ != null; class296_sub13_3_ = (Class296_Sub13) class244.method2175(115)) {
			if (!class296_sub13_3_.isLinked((byte) -120)) {
				class296_sub13_3_ = (Class296_Sub13) class244.method2177(-86);
				if (class296_sub13_3_ == null)
					break;
			}
			if (class296_sub13_3_.anInt4656 == 3) {
				int i_4_ = (int) class296_sub13_3_.uid;
				if (i_4_ >>> 16 == i_1_)
					method596(class296_sub13_3_, true, bool_0_, (byte) 61);
			}
		}
	}

	public static void method597(int i) {
		anIntArray448 = null;
		if (i == -1) {
			anIntArray451 = null;
			aClass79Array447 = null;
			anIntArray449 = null;
		}
	}

	static final Class157 method598(int i, String string, int i_5_) {
		Class157 class157;
		try {
			class157 = (Class157) Class157_Sub1.class.newInstance();
		} catch (Throwable throwable) {
			class157 = new Class157_Sub2();
		}
		class157.aString1593 = string;
		class157.anInt1594 = i_5_;
		if (i <= 110)
			anIntArray451 = null;
		return class157;
	}

	static final boolean method599(byte i, int i_6_, int i_7_) {
		if (i != -46)
			anIntArray451 = null;
		return (ConfigurationDefinition.method687(i_6_, i_7_, (byte) 40) & ((i_6_ & 0x2000) != 0 | Class157.method1589(i_6_, -12205, i_7_) | Class241_Sub2_Sub2.method2165(65536, i_6_, i_7_)));
	}

	static {
		anIntArray451 = new int[anInt444];
		anIntArray449 = new int[anInt444];
	}
}
