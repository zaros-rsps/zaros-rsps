package net.zaros.client;

/* Class322 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class322 {
	static int anInt2826;
	static int anInt2827;
	Class92 aClass92_2828;
	Class55 aClass55_2829 = null;

	static final void method3346(int i, Class296_Sub42 class296_sub42) {
		if (i <= 13)
			method3346(-55, null);
		if (Class338_Sub2.aClass247ArrayArrayArray5195 != null) {
			Interface14 interface14 = null;
			if (class296_sub42.anInt4927 == 0)
				interface14 = ((Interface14) StaticMethods.method2730(class296_sub42.anInt4932, class296_sub42.anInt4923, (class296_sub42.anInt4928)));
			if (class296_sub42.anInt4927 == 1)
				interface14 = ((Interface14) Class172.method1679(class296_sub42.anInt4932, class296_sub42.anInt4923, class296_sub42.anInt4928));
			if (class296_sub42.anInt4927 == 2)
				interface14 = ((Interface14) (Class123_Sub2.method1070(class296_sub42.anInt4932, class296_sub42.anInt4923, class296_sub42.anInt4928, Interface14.class)));
			if (class296_sub42.anInt4927 == 3)
				interface14 = ((Interface14) Class296_Sub15_Sub3.method2540(class296_sub42.anInt4932, class296_sub42.anInt4923, (class296_sub42.anInt4928)));
			if (interface14 != null) {
				class296_sub42.anInt4925 = interface14.method57((byte) 85);
				class296_sub42.anInt4931 = interface14.method54(-11077);
				class296_sub42.anInt4924 = interface14.method59(28);
			} else {
				class296_sub42.anInt4925 = -1;
				class296_sub42.anInt4931 = 0;
				class296_sub42.anInt4924 = 0;
			}
		}
	}

	Class322(Class55 class55) {
		aClass92_2828 = null;
		aClass55_2829 = class55;
	}

	Class322(Class55 class55, Class92 class92) {
		aClass92_2828 = null;
		aClass55_2829 = class55;
		aClass92_2828 = class92;
	}
}
