package net.zaros.client;

/* Class394 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class394 {
	static int langID = 0;
	private Js5 aClass138_3306;
	Js5 aClass138_3307;
	private AdvancedMemoryCache aClass113_3308 = new AdvancedMemoryCache(64);
	AdvancedMemoryCache aClass113_3309 = new AdvancedMemoryCache(2);

	final void method4060(int i, int i_0_) {
		synchronized (aClass113_3308) {
			aClass113_3308.clean(i);
		}
		synchronized (aClass113_3309) {
			aClass113_3309.clean(i);
		}
		if (i_0_ < 24)
			aClass138_3306 = null;
	}

	final void method4061(byte i) {
		synchronized (aClass113_3308) {
			aClass113_3308.clear();
			if (i <= 62)
				aClass113_3308 = null;
		}
		synchronized (aClass113_3309) {
			aClass113_3309.clear();
		}
	}

	final Class325 method4062(int i, int i_1_) {
		Class325 class325;
		synchronized (aClass113_3308) {
			class325 = (Class325) aClass113_3308.get((long) i);
		}
		if (class325 != null)
			return class325;
		byte[] is;
		synchronized (aClass138_3306) {
			is = aClass138_3306.getFile(i_1_, i);
		}
		class325 = new Class325();
		class325.aClass394_2867 = this;
		if (is != null)
			class325.method3379((byte) -103, new Packet(is));
		synchronized (aClass113_3308) {
			aClass113_3308.put(class325, (long) i);
		}
		return class325;
	}

	final void method4063(int i) {
		synchronized (aClass113_3308) {
			aClass113_3308.clearSoftReferences();
		}
		int i_2_ = -6 % ((i + 10) / 35);
		synchronized (aClass113_3309) {
			aClass113_3309.clearSoftReferences();
		}
	}

	Class394(GameType class35, int i, Js5 class138, Js5 class138_3_) {
		aClass138_3307 = class138_3_;
		aClass138_3306 = class138;
		aClass138_3306.getLastFileId(33);
	}
}
