package net.zaros.client;
/* Class296_Sub19_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub19_Sub1 extends Class296_Sub19 {
	int anInt6049;
	int anInt6050;
	byte[] aByteArray6051;
	int anInt6052;
	boolean aBoolean6053;

	final Class296_Sub19_Sub1 method2652(Class288 class288) {
		aByteArray6051 = class288.method2394(aByteArray6051, (byte) 115);
		anInt6052 = class288.method2391(anInt6052, -2);
		if (anInt6049 == anInt6050)
			anInt6049 = anInt6050 = class288.method2392(21284, anInt6049);
		else {
			anInt6049 = class288.method2392(21284, anInt6049);
			anInt6050 = class288.method2392(21284, anInt6050);
			if (anInt6049 == anInt6050)
				anInt6049--;
		}
		return this;
	}

	Class296_Sub19_Sub1(int i, byte[] is, int i_0_, int i_1_) {
		anInt6052 = i;
		aByteArray6051 = is;
		anInt6049 = i_0_;
		anInt6050 = i_1_;
	}

	Class296_Sub19_Sub1(int i, byte[] is, int i_2_, int i_3_, boolean bool) {
		anInt6052 = i;
		aByteArray6051 = is;
		anInt6049 = i_2_;
		anInt6050 = i_3_;
		aBoolean6053 = bool;
	}
}
