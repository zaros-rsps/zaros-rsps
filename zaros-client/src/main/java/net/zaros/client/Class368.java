package net.zaros.client;

/* Class368 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class368 {
	int anInt3128;
	static Class404[] aClass404Array3129 = new Class404[5];
	static int anInt3130;
	static Class241 aClass241_3131;
	static int anInt3132;

	boolean method3805(boolean bool) {
		if (bool != true)
			return true;
		return true;
	}

	static final boolean method3806(int i, boolean bool, int i_0_) {
		if (bool != true)
			return true;
		if (!((i & 0x40000) != 0 | Class296_Sub51_Sub5.method3092(i, -24276, i_0_)) && !Class368_Sub8.method3836(i_0_, i, -1))
			return false;
		return true;
	}

	abstract void method3807(byte i);

	public static void method3808(int i) {
		aClass404Array3129 = null;
		if (i == 5)
			aClass241_3131 = null;
	}

	Class368(Packet class296_sub17) {
		anInt3128 = class296_sub17.g2();
	}

	static final Class296_Sub13 method3809(int i, int i_1_, int i_2_, int i_3_, boolean bool) {
		Class296_Sub13 class296_sub13 = new Class296_Sub13();
		class296_sub13.anInt4656 = i;
		class296_sub13.anInt4657 = i_3_;
		Class386.aClass263_3264.put((long) i_2_, class296_sub13);
		Class296_Sub51_Sub9.method3101(105, i_3_);
		InterfaceComponent class51 = InterfaceComponent.getInterfaceComponent(i_2_);
		if (class51 != null)
			Class332.method3416(class51, (byte) 85);
		if (Player.aClass51_6872 != null) {
			Class332.method3416(Player.aClass51_6872, (byte) 119);
			Player.aClass51_6872 = null;
		}
		Class315.method3322(-118);
		if (class51 != null)
			Class61.method695(!bool, -57, class51);
		if (!bool)
			CS2Executor.method1521(i_3_);
		if (i_1_ != 2642)
			return null;
		if (!bool && Class99.anInt1064 != -1)
			Class296_Sub28.method2686(1, Class99.anInt1064, (byte) 84);
		return class296_sub13;
	}

	static {
		for (int i = 0; aClass404Array3129.length > i; i++)
			aClass404Array3129[i] = new Class404();
	}
}
