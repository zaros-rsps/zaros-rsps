package net.zaros.client;
/* Class255 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.EOFException;
import java.io.File;
import java.io.IOException;

final class SeekableFile {
	private long aLong2390;
	private byte[] aByteArray2391;
	private long aLong2392;
	private long aLong2393;
	private int anInt2394;
	private long aLong2395;
	private long aLong2396 = -1L;
	static boolean aBoolean2397 = false;
	private byte[] aByteArray2398;
	static Class175 aClass175_2399 = new Class175();
	private long aLong2400;
	private int anInt2401;
	private FileOnDisk aClass58_2402;

	static final int method2215(int i) {
		if (i != 4234) {
			method2223(-98);
		}
		return Class352.method3686(0, false);
	}

	private final void method2216(byte i) throws IOException {
		if (i >= -89) {
			method2225(-91);
		}
		if (aLong2393 != -1L) {
			if (aLong2393 != aLong2400) {
				aClass58_2402.seek(aLong2393, (byte) 85);
				aLong2400 = aLong2393;
			}
			aClass58_2402.write(anInt2401, aByteArray2398, 0, true);
			aLong2400 += anInt2401;
			if (aLong2400 > aLong2392) {
				aLong2392 = aLong2400;
			}
			long l = -1L;
			if (aLong2396 <= aLong2393 && anInt2394 + aLong2396 > aLong2393) {
				l = aLong2393;
			} else if (aLong2396 >= aLong2393 && aLong2393 - -(long) anInt2401 > aLong2396) {
				l = aLong2396;
			}
			long l_0_ = -1L;
			if (aLong2396 >= aLong2393 + anInt2401 || anInt2401 + aLong2393 > aLong2396 + anInt2394) {
				if (anInt2394 + aLong2396 > aLong2393 && anInt2394 + aLong2396 <= anInt2401 + aLong2393) {
					l_0_ = anInt2394 + aLong2396;
				}
			} else {
				l_0_ = aLong2393 - -(long) anInt2401;
			}
			if (l > -1L && l_0_ > l) {
				int i_1_ = (int) (l_0_ + -l);
				ArrayTools.removeElement(aByteArray2398, (int) (l - aLong2393), aByteArray2391, (int) (-aLong2396 + l), i_1_);
			}
			anInt2401 = 0;
			aLong2393 = -1L;
		}
	}

	final void method2217(byte[] is, byte i) throws IOException {
		if (i != 58) {
			method2226(-12);
		}
		read(is, 0, is.length);
	}

	private final void method2218(int i) throws IOException {
		anInt2394 = 0;
		if (aLong2395 != aLong2400) {
			aClass58_2402.seek(aLong2395, (byte) 85);
			aLong2400 = aLong2395;
		}
		aLong2396 = aLong2395;
		int i_2_ = 99 % ((i - 16) / 49);
		while (anInt2394 < aByteArray2391.length) {
			int i_3_ = aByteArray2391.length - anInt2394;
			if (i_3_ > 200000000) {
				i_3_ = 200000000;
			}
			int i_4_ = aClass58_2402.read((byte) 9, anInt2394, aByteArray2391, i_3_);
			if (i_4_ == -1) {
				break;
			}
			anInt2394 += i_4_;
			aLong2400 += i_4_;
		}
	}

	public static void method2219(int i) {
		aClass175_2399 = null;
		if (i <= 64) {
			method2223(-57);
		}
	}

	final void read(byte[] is, int i, int i_5_) throws IOException {
		try {
			if (is.length < i_5_ + i) {
				throw new ArrayIndexOutOfBoundsException(i + i_5_ - is.length);
			}
			if (aLong2393 != -1L && aLong2393 <= aLong2395 && i_5_ + aLong2395 <= anInt2401 + aLong2393) {
				ArrayTools.removeElement(aByteArray2398, (int) (-aLong2393 + aLong2395), is, i, i_5_);
				aLong2395 += i_5_;
				return;
			}
			long l = aLong2395;
			int i_7_ = i;
			int i_8_ = i_5_;
			if (aLong2395 >= aLong2396 && aLong2395 < anInt2394 + aLong2396) {
				int i_9_ = (int) (anInt2394 - -aLong2396 - aLong2395);
				if (i_9_ > i_5_) {
					i_9_ = i_5_;
				}
				ArrayTools.removeElement(aByteArray2391, (int) (-aLong2396 + aLong2395), is, i, i_9_);
				aLong2395 += i_9_;
				i += i_9_;
				i_5_ -= i_9_;
			}
			if (aByteArray2391.length < i_5_) {
				aClass58_2402.seek(aLong2395, (byte) 85);
				aLong2400 = aLong2395;
				int i_10_;
				for (/**/; i_5_ > 0; i_5_ -= i_10_) {
					i_10_ = aClass58_2402.read((byte) 9, i, is, i_5_);
					if (i_10_ == -1) {
						break;
					}
					aLong2395 += i_10_;
					i += i_10_;
					aLong2400 += i_10_;
				}
			} else if (i_5_ > 0) {
				method2218(70);
				int i_11_ = i_5_;
				if (anInt2394 < i_11_) {
					i_11_ = anInt2394;
				}
				ArrayTools.removeElement(aByteArray2391, 0, is, i, i_11_);
				i_5_ -= i_11_;
				i += i_11_;
				aLong2395 += i_11_;
			}
			if (-1 != aLong2393) {
				if (aLong2393 > aLong2395 && i_5_ > 0) {
					int i_12_ = i + (int) (-aLong2395 + aLong2393);
					if (i_12_ > i_5_ + i) {
						i_12_ = i + i_5_;
					}
					while (i_12_ > i) {
						i_5_--;
						is[i++] = (byte) 0;
						aLong2395++;
					}
				}
				long l_13_ = -1L;
				if (aLong2393 >= l && l + i_8_ > aLong2393) {
					l_13_ = aLong2393;
				} else if (aLong2393 <= l && anInt2401 + aLong2393 > l) {
					l_13_ = l;
				}
				long l_14_ = -1L;
				if (anInt2401 + aLong2393 > l && anInt2401 + aLong2393 <= l + i_8_) {
					l_14_ = anInt2401 + aLong2393;
				} else if (aLong2393 < i_8_ + l && anInt2401 + aLong2393 >= i_8_ + l) {
					l_14_ = i_8_ + l;
				}
				if (l_13_ > -1L && l_13_ < l_14_) {
					int i_15_ = (int) (l_14_ - l_13_);
					ArrayTools.removeElement(aByteArray2398, (int) (-aLong2393 + l_13_), is, i_7_ + (int) (-l + l_13_), i_15_);
					if (l_14_ > aLong2395) {
						i_5_ -= l_14_ - aLong2395;
						aLong2395 = l_14_;
					}
				}
			}
		} catch (IOException ioexception) {
			aLong2400 = -1L;
			throw ioexception;
		}
		if (i_5_ > 0) {
			throw new EOFException();
		}
	}

	final void method2221(int i) throws IOException {
		method2216((byte) -115);
		aClass58_2402.close(i ^ i);
	}

	final void write(byte[] is, int i, int i_16_) throws IOException {
		try {
			if (aLong2395 - -(long) i_16_ > aLong2390) {
				aLong2390 = aLong2395 - -(long) i_16_;
			}
			if (aLong2393 != -1L && (aLong2395 < aLong2393 || anInt2401 + aLong2393 < aLong2395)) {
				method2216((byte) -125);
			}
			if (aLong2393 != -1L && aByteArray2398.length + aLong2393 < aLong2395 + i_16_) {
				int i_18_ = (int) (-aLong2395 - (-aLong2393 - aByteArray2398.length));
				ArrayTools.removeElement(is, i, aByteArray2398, (int) (-aLong2393 + aLong2395), i_18_);
				i_16_ -= i_18_;
				i += i_18_;
				aLong2395 += i_18_;
				anInt2401 = aByteArray2398.length;
				method2216((byte) -124);
			}
			if (aByteArray2398.length < i_16_) {
				if (aLong2400 != aLong2395) {
					aClass58_2402.seek(aLong2395, (byte) 85);
					aLong2400 = aLong2395;
				}
				aClass58_2402.write(i_16_, is, i, true);
				aLong2400 += i_16_;
				if (aLong2400 > aLong2392) {
					aLong2392 = aLong2400;
				}
				long l = -1L;
				long l_19_ = -1L;
				if (aLong2396 > aLong2395 || aLong2396 - -(long) anInt2394 <= aLong2395) {
					if (aLong2395 <= aLong2396 && i_16_ + aLong2395 > aLong2396) {
						l = aLong2396;
					}
				} else {
					l = aLong2395;
				}
				if (aLong2396 < i_16_ + aLong2395 && anInt2394 + aLong2396 >= aLong2395 + i_16_) {
					l_19_ = aLong2395 + i_16_;
				} else if (aLong2395 < anInt2394 + aLong2396 && aLong2395 - -(long) i_16_ >= anInt2394 + aLong2396) {
					l_19_ = anInt2394 + aLong2396;
				}
				if (l > -1L && l < l_19_) {
					int i_20_ = (int) (l_19_ + -l);
					ArrayTools.removeElement(is, (int) (-aLong2395 + i + l), aByteArray2391, (int) (l + -aLong2396), i_20_);
				}
				aLong2395 += i_16_;
			} else if (i_16_ > 0) {
				if (aLong2393 == -1L) {
					aLong2393 = aLong2395;
				}
				ArrayTools.removeElement(is, i, aByteArray2398, (int) (-aLong2393 + aLong2395), i_16_);
				aLong2395 += i_16_;
				if (anInt2401 < -aLong2393 + aLong2395) {
					anInt2401 = (int) (aLong2395 - aLong2393);
				}
			}
		} catch (IOException ioexception) {
			aLong2400 = -1L;
			throw ioexception;
		}
	}

	static final int method2223(int i) {
		if (i != 0) {
			return 53;
		}
		return AdvancedMemoryCache.anInt1167;
	}

	public long size() {
		return aLong2390;
	}

	static final void method2225(int i) {
		Class241_Sub1.aClass400_4577 = new Class400(8);
		Class16_Sub1_Sub1.anInt5803 = 0;
		if (i != 23123) {
			method2223(-106);
		}
		for (Class338_Sub1 class338_sub1 = (Class338_Sub1) Class368_Sub19.aClass404_5542.method4160((byte) 114); class338_sub1 != null; class338_sub1 = (Class338_Sub1) Class368_Sub19.aClass404_5542.method4163(-24917)) {
			class338_sub1.method3449();
		}
	}

	private final File method2226(int i) {
		if (i != 148446728) {
			aBoolean2397 = false;
		}
		return aClass58_2402.getFile(true);
	}

	final void seek(long l) throws IOException {
		if (0 > l) {
			throw new IOException("Invalid seek to " + l + " in file " + method2226(0 ^ 0x8d91e08));
		}
		aLong2395 = l;
	}

	SeekableFile(FileOnDisk class58, int i, int i_21_) throws IOException {
		aLong2393 = -1L;
		anInt2401 = 0;
		aClass58_2402 = class58;
		aLong2390 = aLong2392 = class58.getLength(-2);
		aLong2395 = 0L;
		aByteArray2398 = new byte[i_21_];
		aByteArray2391 = new byte[i];
	}
}
