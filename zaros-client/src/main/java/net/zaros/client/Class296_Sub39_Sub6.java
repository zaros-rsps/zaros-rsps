package net.zaros.client;

/* Class296_Sub39_Sub6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub6 extends Queuable {
	static IncomingPacket aClass231_6150 = new IncomingPacket(56, -2);
	AnimFrame[] aClass60Array6151;
	private int anInt6152;
	private byte[][] aByteArrayArray6153;
	final boolean method2810(int i, int i_0_) {
		if (i_0_ != -8657864)
			return false;
		return aClass60Array6151[i].aBoolean681;
	}

	Class296_Sub39_Sub6(int i) {
		anInt6152 = i;
	}

	final boolean method2811(byte i, int i_1_) {
		if (i != 27)
			anInt6152 = 86;
		return aClass60Array6151[i_1_].aBoolean684;
	}

	public static void method2812(byte i) {
		if (i != -126)
			method2812((byte) 110);
		aClass231_6150 = null;
		PlayerUpdate.visiblePlayers = null;
	}

	static final GameLoopTask makeGameLoopTask(long key, int type) {
		GameLoopTask task = ((GameLoopTask) Class368_Sub5.gameLoopCache.get((long) type << 56 | key));
		if (task == null) {
			task = new GameLoopTask(type, key);
			Class368_Sub5.gameLoopCache.put((task.uid), task);
		}
		return task;
	}

	final boolean method2814(int i, byte i_3_) {
		if (i_3_ >= -1)
			anInt6152 = -105;
		return aClass60Array6151[i].aBoolean688;
	}

	final boolean method2815(int i) {
		if (i != 1)
			return false;
		if (aClass60Array6151 != null)
			return true;
		if (aByteArrayArray6153 == null) {
			synchronized (Class41_Sub26.aClass138_3808) {
				if (!Class41_Sub26.aClass138_3808.hasFileBuffer((byte) -118, anInt6152))
					return false;
				int[] is = Class41_Sub26.aClass138_3808.getChildIndicies(anInt6152);
				aByteArrayArray6153 = new byte[is.length][];
				for (int i_4_ = 0; is.length > i_4_; i_4_++)
					aByteArrayArray6153[i_4_] = Class41_Sub26.aClass138_3808.getFile(anInt6152, is[i_4_]);
			}
		}
		boolean bool = true;
		for (int i_5_ = 0; aByteArrayArray6153.length > i_5_; i_5_++) {
			byte[] is = aByteArrayArray6153[i_5_];
			Packet class296_sub17 = new Packet(is);
			class296_sub17.pos = 1;
			int i_6_ = class296_sub17.g2();
			synchronized (Class127.aClass138_1302) {
				bool &= Class127.aClass138_1302.hasEntryBuffer(i_6_);
			}
		}
		if (!bool)
			return false;
		NodeDeque class155 = new NodeDeque();
		int[] is;
		synchronized (Class41_Sub26.aClass138_3808) {
			int i_7_ = Class41_Sub26.aClass138_3808.getLastFileId(anInt6152);
			aClass60Array6151 = new AnimFrame[i_7_];
			is = Class41_Sub26.aClass138_3808.getChildIndicies(anInt6152);
		}
		for (int i_8_ = 0; i_8_ < is.length; i_8_++) {
			byte[] is_9_ = aByteArrayArray6153[i_8_];
			Packet class296_sub17 = new Packet(is_9_);
			class296_sub17.pos = 1;
			int i_10_ = class296_sub17.g2();
			AnimBase class296_sub26 = null;
			for (AnimBase class296_sub26_11_ = (AnimBase) class155.removeFirst((byte) 122); class296_sub26_11_ != null; class296_sub26_11_ = (AnimBase) class155.removeNext(1001)) {
				if (i_10_ == class296_sub26_11_.baseid) {
					class296_sub26 = class296_sub26_11_;
					break;
				}
			}
			if (class296_sub26 == null) {
				synchronized (Class127.aClass138_1302) {
					class296_sub26 = new AnimBase(i_10_, (Class127.aClass138_1302.get(i_10_)));
				}
				class155.addLast((byte) 121, class296_sub26);
			}
			aClass60Array6151[is[i_8_]] = new AnimFrame(is_9_, class296_sub26);
		}
		aByteArrayArray6153 = null;
		return true;
	}
}
