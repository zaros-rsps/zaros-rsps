package net.zaros.client;

/* Class50 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class50 {
	private ha_Sub3 aHa_Sub3_465;
	private int anInt466;
	private Class295_Sub1 aClass295_Sub1_467;
	private int anInt468;
	private int anInt469 = -1;
	private Interface20 anInterface20_470;
	static int[][] anIntArrayArray471 = {{2, 4}, {2, 4}, {5, 2, 4}, {4, 5, 2}, {2, 4, 5}, {5, 2, 4}, {1, 6, 2, 5}, {1, 6, 7, 1}, {6, 7, 1, 1}, {0, 8, 9, 8, 9, 4}, {8, 9, 4, 0, 8, 9}, {2, 10, 0, 10, 11, 11}, {2, 4}, {1, 6, 7, 1}, {1, 6, 7, 1}};
	private int anInt472;
	boolean aBoolean473 = true;
	private Class69_Sub1 aClass69_Sub1_474;
	private Class207 aClass207_475;

	static final boolean method609(boolean bool, int i) {
		if (bool != true)
			method615(75);
		if (i == 51 || i == 9 || i == 17 || i == 12 || i == 60 || i == 1004)
			return true;
		if (i == 59)
			return true;
		return false;
	}

	public static void method610(byte i) {
		if (i == 60)
			anIntArrayArray471 = null;
	}

	private final void method611(int i) {
		if (aBoolean473) {
			aBoolean473 = false;
			byte[] is = aClass207_475.aByteArray2076;
			byte[] is_0_ = aHa_Sub3_465.aByteArray4278;
			int i_1_ = 0;
			int i_2_ = aClass207_475.anInt2080;
			int i_3_ = anInt466 + anInt472 * aClass207_475.anInt2080;
			for (int i_4_ = -128; i_4_ < 0; i_4_++) {
				i_1_ = -i_1_ + (i_1_ << 8);
				for (int i_5_ = -128; i_5_ < 0; i_5_++) {
					if (is[i_3_++] != 0)
						i_1_++;
				}
				i_3_ += i_2_ - 128;
			}
			if (aClass69_Sub1_474 != null && i_1_ == anInt469)
				aBoolean473 = false;
			else {
				anInt469 = i_1_;
				int i_6_ = 0;
				i_3_ = i_2_ * anInt472 + anInt466;
				for (int i_7_ = -128; i_7_ < 0; i_7_++) {
					for (int i_8_ = -128; i_8_ < 0; i_8_++) {
						if (is[i_3_] != 0)
							is_0_[i_6_++] = (byte) 68;
						else {
							int i_9_ = 0;
							if (is[i_3_ - 1] != 0)
								i_9_++;
							if (is[i_3_ + 1] != 0)
								i_9_++;
							if (is[i_3_ - i_2_] != 0)
								i_9_++;
							if (is[i_2_ + i_3_] != 0)
								i_9_++;
							is_0_[i_6_++] = (byte) (i_9_ * 17);
						}
						i_3_++;
					}
					i_3_ += aClass207_475.anInt2080 - 128;
				}
				if (aClass69_Sub1_474 != null)
					aClass69_Sub1_474.method731((byte) -30, 0, 0, 128, 128, aHa_Sub3_465.aByteArray4278, false, 0, 6406, 0);
				else {
					aClass69_Sub1_474 = new Class69_Sub1(aHa_Sub3_465, 3553, 6406, 128, 128, false, aHa_Sub3_465.aByteArray4278, 6406, false);
					aClass69_Sub1_474.method733(false, false, true);
					aClass69_Sub1_474.method723(127, true);
				}
				int i_10_ = 49 % ((i - 54) / 38);
			}
		}
	}

	final void method612(byte[] is, int i, int i_11_, int i_12_) {
		aClass295_Sub1_467.method75((aHa_Sub3_465.method1312(i_12_, -3216) * i_11_), is, i_12_, false);
		if (i != -1)
			anInt469 = 39;
		method613((byte) 99, i_11_, aClass295_Sub1_467);
	}

	private final void method613(byte i, int i_13_, Interface20 interface20) {
		if (i_13_ != 0) {
			int i_14_ = -92 % ((51 - i) / 44);
			method611(-114);
			aHa_Sub3_465.method1316(aClass69_Sub1_474, (byte) -125);
			aHa_Sub3_465.method1348(i_13_, interface20, 4, (byte) -17, 0);
		}
	}

	static final boolean method614(byte i, int i_15_, int i_16_, int i_17_) {
		if (!Class103.aBoolean1086 || !Class12.aBoolean133)
			return false;
		if (i > -103)
			anIntArrayArray471 = null;
		if (Class296_Sub39_Sub20.anInt6255 < 100)
			return false;
		int i_18_ = Class296_Sub36.anIntArrayArrayArray4870[i_17_][i_15_][i_16_];
		if (i_18_ == -Class86.anInt936)
			return false;
		if (i_18_ == Class86.anInt936)
			return true;
		if (Class360_Sub2.aSArray5304 == Class52.aSArray636)
			return false;
		int i_19_ = i_15_ << Class313.anInt2779;
		int i_20_ = i_16_ << Class313.anInt2779;
		if ((Class389.method4037(Class360_Sub2.aSArray5304[i_17_].method3355(i_16_ + 1, (byte) -111, i_15_ + 1), i_19_ + 1, 2, Class360_Sub2.aSArray5304[i_17_].method3355(i_16_, (byte) -124, i_15_), i_20_ + 1, Js5TextureLoader.anInt3440 + i_19_ - 1, Class360_Sub2.aSArray5304[i_17_].method3355(i_16_ + 1, (byte) -108, i_15_), i_19_ + 1, i_20_ + Js5TextureLoader.anInt3440 - 1, Js5TextureLoader.anInt3440 + i_20_ - 1)) && (Class389.method4037(Class360_Sub2.aSArray5304[i_17_].method3355(i_16_, (byte) -113, i_15_ + 1), i_19_ + 1, 2, Class360_Sub2.aSArray5304[i_17_].method3355(i_16_, (byte) -110, i_15_), i_20_ + 1, Js5TextureLoader.anInt3440 - 1 + i_19_, Class360_Sub2.aSArray5304[i_17_].method3355(i_16_ + 1, (byte) -123, i_15_ + 1), i_19_ + Js5TextureLoader.anInt3440 - 1, i_20_ + 1, i_20_ + Js5TextureLoader.anInt3440 - 1))) {
			Class367.anInt3126++;
			Class296_Sub36.anIntArrayArrayArray4870[i_17_][i_15_][i_16_] = Class86.anInt936;
			return true;
		}
		Class296_Sub36.anIntArrayArrayArray4870[i_17_][i_15_][i_16_] = -Class86.anInt936;
		return false;
	}

	static final void method615(int i) {
		if (i != 59)
			method610((byte) -52);
		for (Class296_Sub39_Sub11 class296_sub39_sub11 = ((Class296_Sub39_Sub11) Class134.aClass263_1385.getFirst(true)); class296_sub39_sub11 != null; class296_sub39_sub11 = ((Class296_Sub39_Sub11) Class134.aClass263_1385.getNext(0))) {
			Class338_Sub3_Sub1_Sub5 class338_sub3_sub1_sub5 = class296_sub39_sub11.aClass338_Sub3_Sub1_Sub5_6190;
			class338_sub3_sub1_sub5.method3551(-73, 1);
			if (class338_sub3_sub1_sub5.method3552((byte) 124)) {
				class296_sub39_sub11.unlink();
				class338_sub3_sub1_sub5.method3545((byte) 92);
			} else if (Class338_Sub2.aClass247ArrayArrayArray5195 != null && class338_sub3_sub1_sub5.method3550(30690))
				Class125.method1079(class338_sub3_sub1_sub5, true);
		}
	}

	final void method616(boolean bool) {
		method613((byte) 110, anInt468, anInterface20_470);
		if (bool)
			method614((byte) 38, 24, -26, -22);
	}

	Class50(ha_Sub3 var_ha_Sub3, Class207 class207, s_Sub2 var_s_Sub2, int i, int i_21_, int i_22_, int i_23_, int i_24_) {
		anInt472 = i_24_;
		anInt466 = i_23_;
		aClass207_475 = class207;
		aHa_Sub3_465 = var_ha_Sub3;
		int i_25_ = 1 << i_22_;
		int i_26_ = 0;
		int i_27_ = i << i_22_;
		int i_28_ = i_21_ << i_22_;
		for (int i_29_ = 0; i_29_ < i_25_; i_29_++) {
			int i_30_ = i_27_ + var_s_Sub2.anInt2832 * (i_28_ + i_29_);
			for (int i_31_ = 0; i_25_ > i_31_; i_31_++) {
				short[] is = var_s_Sub2.aShortArrayArray5111[i_30_++];
				if (is != null)
					i_26_ += is.length;
			}
		}
		anInt468 = i_26_;
		if (i_26_ <= 0)
			aClass69_Sub1_474 = null;
		else {
			Packet class296_sub17 = new Packet(i_26_ * 2);
			if (aHa_Sub3_465.aBoolean4184) {
				for (int i_32_ = 0; i_32_ < i_25_; i_32_++) {
					int i_33_ = (i_28_ + i_32_) * var_s_Sub2.anInt2832 + i_27_;
					for (int i_34_ = 0; i_34_ < i_25_; i_34_++) {
						short[] is = var_s_Sub2.aShortArrayArray5111[i_33_++];
						if (is != null) {
							for (int i_35_ = 0; i_35_ < is.length; i_35_++)
								class296_sub17.p2(is[i_35_] & 0xffff);
						}
					}
				}
			} else {
				for (int i_36_ = 0; i_36_ < i_25_; i_36_++) {
					int i_37_ = i_27_ + var_s_Sub2.anInt2832 * (i_36_ + i_28_);
					for (int i_38_ = 0; i_25_ > i_38_; i_38_++) {
						short[] is = var_s_Sub2.aShortArrayArray5111[i_37_++];
						if (is != null) {
							for (int i_39_ = 0; i_39_ < is.length; i_39_++)
								class296_sub17.method2590(is[i_39_] & 0xffff);
						}
					}
				}
			}
			anInterface20_470 = aHa_Sub3_465.method1310(65280, class296_sub17.pos, 5123, class296_sub17.data, false);
			aClass295_Sub1_467 = new Class295_Sub1(aHa_Sub3_465, 5123, null, 1);
		}
	}
}
