package net.zaros.client;

/* Class273 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class273 implements Interface2 {
	private Interface9[] anInterface9Array3432;
	private Class334 aClass334_3433;
	private ha aHa3434;
	private boolean aBoolean3435;

	public final void method11(boolean bool) {
		if (bool)
			aClass334_3433 = null;
	}

	public final int method6(byte i) {
		if (i < 27)
			aClass334_3433 = null;
		return aClass334_3433.anInt2947;
	}

	public final void method10(byte i) {
		if (i == -97) {
			if (Class41_Sub13.aHa3774 != aHa3434) {
				aBoolean3435 = true;
				aHa3434 = Class41_Sub13.aHa3774;
			}
			aHa3434.GA(0);
			Interface9[] interface9s = anInterface9Array3432;
			for (int i_0_ = 0; interface9s.length > i_0_; i_0_++) {
				Interface9 interface9 = interface9s[i_0_];
				if (interface9 != null)
					interface9.method42((byte) 88);
			}
		}
	}

	public final void method8(byte i, boolean bool) {
		bool = true;
		if (i != -127)
			aClass334_3433 = null;
		Interface9[] interface9s = anInterface9Array3432;
		for (int i_1_ = 0; interface9s.length > i_1_; i_1_++) {
			Interface9 interface9 = interface9s[i_1_];
			if (interface9 != null)
				interface9.method43(bool || aBoolean3435, -99);
		}
		aBoolean3435 = false;
	}

	static final boolean method2313(int i, int i_2_, int i_3_) {
		if (i < 58)
			return false;
		return (Class215.method2025((byte) -20, i_2_, i_3_) & Class379_Sub1.method3967(i_2_, true, i_3_));
	}

	public final int method9(int i) {
		int i_4_ = 0;
		Interface9[] interface9s = anInterface9Array3432;
		for (int i_5_ = 0; i_5_ < interface9s.length; i_5_++) {
			Interface9 interface9 = interface9s[i_5_];
			if (interface9 == null || interface9.method44((byte) 114))
				i_4_++;
		}
		if (i != 4739)
			anInterface9Array3432 = null;
		return i_4_ * 100 / anInterface9Array3432.length;
	}

	public final boolean method7(long l, byte i) {
		if (i > -103)
			return true;
		if (Class72.method771(-123) < (long) aClass334_3433.anInt2948 + l)
			return false;
		return true;
	}

	Class273(Class334 class334, Class333 class333) {
		aClass334_3433 = class334;
		anInterface9Array3432 = new Interface9[aClass334_3433.anInterface10Array2949.length];
		for (int i = 0; anInterface9Array3432.length > i; i++)
			anInterface9Array3432[i] = class333.method3418(aClass334_3433.anInterface10Array2949[i], -124);
	}
}
