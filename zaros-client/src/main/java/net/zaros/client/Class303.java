package net.zaros.client;
/* Class303 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Dimension;

final class Class303 {
	static Class296_Sub1[] aClass296_Sub1Array2723 = new Class296_Sub1[300];
	String aString2724;
	static int anInt2725 = 2;
	static OutgoingPacket aClass311_2726 = new OutgoingPacket(36, 6);
	int anInt2727;

	public static void method3267(int i) {
		if (i != 300)
			method3269((byte) 110);
		aClass311_2726 = null;
		aClass296_Sub1Array2723 = null;
	}

	static final void method3268(Canvas canvas, int i) {
		Dimension dimension = canvas.getSize();
		Class296_Sub15_Sub3.method2539(dimension.width, dimension.height, (byte) -116);
		if ((ConfigsRegister.anInt3674 ^ 0xffffffff) != i)
			Class296_Sub51_Sub36.aHa6529.a(canvas, Class235.anInt2227, Class261.anInt2424);
		else
			Class296_Sub51_Sub36.aHa6529.a(canvas, Class296_Sub39_Sub12.anInt6197, Class359.anInt3089);
	}

	static final void method3269(byte i) {
		World.aClass113_1160.clear();
		if (i < 97)
			aClass311_2726 = null;
	}
}
