package net.zaros.client;

/* Class62 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class62 {
	private AdvancedMemoryCache aClass113_711 = new AdvancedMemoryCache(64);
	private Js5 aClass138_712;
	EquipmentData aClass312_713;

	final void method696(byte i) {
		if (i > 42) {
			synchronized (aClass113_711) {
				aClass113_711.clearSoftReferences();
			}
		}
	}

	final void method697(int i, byte i_0_) {
		if (i_0_ != -5) {
			aClass138_712 = null;
		}
		synchronized (aClass113_711) {
			aClass113_711.clean(i);
		}
	}

	final void method698(int i) {
		synchronized (aClass113_711) {
			if (i != 0) {
				aClass113_711 = null;
			}
			aClass113_711.clear();
		}
	}

	static final void method699(int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, ha var_ha) {
		if ((Class227.aClass397_2187 == null || Class368_Sub15.aClass397_5521 == null || Class41_Sub27.aClass397_3811 == null) && Class205_Sub2.fs8.hasEntryBuffer(Class181.anInt1861) && Class205_Sub2.fs8.hasEntryBuffer(CS2Script.anInt6144) && Class205_Sub2.fs8.hasEntryBuffer(Class245.anInt2330)) {
			Class186 class186 = Class186.method1876(Class205_Sub2.fs8, CS2Script.anInt6144, 0);
			Class368_Sub15.aClass397_5521 = var_ha.a(class186, true);
			class186.method1879();
			Class395.aClass397_3316 = var_ha.a(class186, true);
			Class227.aClass397_2187 = var_ha.a(Class186.method1876(Class205_Sub2.fs8, Class181.anInt1861, 0), true);
			Class186 class186_6_ = Class186.method1876(Class205_Sub2.fs8, Class245.anInt2330, 0);
			Class41_Sub27.aClass397_3811 = var_ha.a(class186_6_, true);
			class186_6_.method1879();
			Class130.aClass397_1334 = var_ha.a(class186_6_, true);
		}
		int i_7_ = -6 / ((72 - i_2_) / 33);
		if (Class227.aClass397_2187 != null && Class368_Sub15.aClass397_5521 != null && Class41_Sub27.aClass397_3811 != null) {
			int i_8_ = (-(Class41_Sub27.aClass397_3811.method4087() * 2) + i_5_) / Class227.aClass397_2187.method4087();
			for (int i_9_ = 0; i_8_ > i_9_; i_9_++) {
				Class227.aClass397_2187.method4096(Class41_Sub27.aClass397_3811.method4087() + i + Class227.aClass397_2187.method4087() * i_9_, -Class227.aClass397_2187.method4092() + i_3_ + i_4_);
			}
			int i_10_ = (-i_1_ + i_4_ - Class41_Sub27.aClass397_3811.method4092()) / Class368_Sub15.aClass397_5521.method4092();
			for (int i_11_ = 0; i_11_ < i_10_; i_11_++) {
				Class368_Sub15.aClass397_5521.method4096(i, Class368_Sub15.aClass397_5521.method4092() * i_11_ + i_1_ + i_3_);
				Class395.aClass397_3316.method4096(i - (-i_5_ + Class395.aClass397_3316.method4087()), i_11_ * Class368_Sub15.aClass397_5521.method4092() + i_1_ + i_3_);
			}
			Class41_Sub27.aClass397_3811.method4096(i, i_3_ + i_4_ - Class41_Sub27.aClass397_3811.method4092());
			Class130.aClass397_1334.method4096(i_5_ + i - Class41_Sub27.aClass397_3811.method4087(), -Class41_Sub27.aClass397_3811.method4092() + i_3_ + i_4_);
		}
	}

	final Class280 method700(int i, int i_12_) {
		Class280 class280;
		synchronized (aClass113_711) {
			class280 = (Class280) aClass113_711.get(i);
		}
		if (class280 != null) {
			return class280;
		}
		if (i_12_ != 0) {
			return null;
		}
		byte[] is;
		synchronized (aClass138_712) {
			is = aClass138_712.getFile(32, i);
		}
		class280 = new Class280();
		class280.aClass62_2594 = this;
		if (is != null) {
			boolean newBas = is[0] == 'N' && is[1] == 'E' && is[2] == 'W';
			class280.method2343(i_12_ - 20502, new Packet(is), newBas);
		}
		synchronized (aClass113_711) {
			aClass113_711.put(class280, i);
		}
		return class280;
	}

	Class62(GameType class35, int i, Js5 class138, EquipmentData class312) {
		aClass138_712 = class138;
		aClass138_712.getLastFileId(32);
		aClass312_713 = class312;
	}
}
