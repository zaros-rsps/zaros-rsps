package net.zaros.client;
/* Class299 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Dimension;

final class Class299 {
	String aString2701;
	private Class404 aClass404_2702 = new Class404();
	private volatile int anInt2703;
	private Class142 aClass142_2704;

	final void method3238(Class338_Sub3 class338_sub3, int i) {
		class338_sub3.aBoolean5207 = false;
		synchronized (aClass404_2702) {
			aClass404_2702.method4158(class338_sub3, 1);
			anInt2703++;
		}
		if (i < 2)
			method3245((byte) -15);
		if (aClass142_2704 != null) {
			synchronized (aClass142_2704) {
				aClass142_2704.notify();
			}
		}
	}

	final boolean method3239(int i) {
		if (i > -60)
			aClass404_2702 = null;
		if (anInt2703 != 0)
			return false;
		return true;
	}

	static final void method3240(Canvas canvas, int i) {
		Dimension dimension = canvas.getSize();
		if (i != 9527)
			parseModeWhat(122, -25);
		Class296_Sub15_Sub3.method2539(dimension.width, dimension.height, (byte) -116);
		if (ConfigsRegister.anInt3674 == 1)
			Class296_Sub51_Sub36.aHa6529.b(canvas, Class296_Sub39_Sub12.anInt6197, Class359.anInt3089);
		else
			Class296_Sub51_Sub36.aHa6529.b(canvas, Class235.anInt2227, Class261.anInt2424);
	}

	final void method3241(Class338_Sub3 class338_sub3, int i) {
		if (i == 0) {
			class338_sub3.aBoolean5207 = true;
			synchronized (aClass404_2702) {
				aClass404_2702.method4158(class338_sub3, 1);
				anInt2703++;
			}
			if (aClass142_2704 != null) {
				synchronized (aClass142_2704) {
					aClass142_2704.notify();
				}
			}
		}
	}

	final void method3242(Class142 class142, int i) {
		if (i != 0)
			parseModeWhat(-107, -128);
		aClass142_2704 = class142;
	}

	static final ModeWhat parseModeWhat(int i, int i_0_) {
		if (i_0_ > -12)
			return null;
		ModeWhat[] class118s = Class358.modeWhats(-113);
		for (int i_1_ = 0; i_1_ < class118s.length; i_1_++) {
			ModeWhat class118 = class118s[i_1_];
			if (i == class118.modeID)
				return class118;
		}
		return null;
	}

	final void method3244(Class338_Sub10 class338_sub10, byte i) {
		if (i != 80)
			aClass404_2702 = null;
		synchronized (aClass404_2702) {
			aClass404_2702.method4158(class338_sub10, 1);
			anInt2703++;
		}
		if (aClass142_2704 != null) {
			synchronized (aClass142_2704) {
				aClass142_2704.notify();
			}
		}
	}

	final Class338 method3245(byte i) {
		Object object = null;
		if (i != -82)
			aClass142_2704 = null;
		Class338 class338;
		synchronized (aClass404_2702) {
			class338 = aClass404_2702.method4160((byte) 107);
			class338.method3438(false);
			anInt2703--;
		}
		return class338;
	}

	Class299(String string) {
		aString2701 = string;
	}
}
