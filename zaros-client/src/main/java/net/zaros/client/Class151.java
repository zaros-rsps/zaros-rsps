package net.zaros.client;
/* Class151 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

final class Class151 {
	int anInt1549;
	private Js5 aClass138_1550;
	static IncomingPacket aClass231_1551 = new IncomingPacket(57, 8);
	private AdvancedMemoryCache aClass113_1552 = new AdvancedMemoryCache(64);
	static Class287 aClass287_1553 = new Class287();
	static OutgoingPacket aClass311_1554 = new OutgoingPacket(45, 7);
	static Sprite[] aClass397Array1555;

	public static void method1539(int i) {
		aClass287_1553 = null;
		aClass397Array1555 = null;
		if (i == 9535) {
			aClass231_1551 = null;
			aClass311_1554 = null;
		}
	}

	static final boolean method1540(byte i) {
		Hashtable hashtable = new Hashtable();
		if (i >= -100)
			aClass231_1551 = null;
		Enumeration enumeration = Class296_Sub9.aHashtable4632.keys();
		while (enumeration.hasMoreElements()) {
			Object object = enumeration.nextElement();
			hashtable.put(object, Class296_Sub9.aHashtable4632.get(object));
		}
		try {
			Class var_class = java.lang.reflect.AccessibleObject.class;
			Class var_class_0_ = java.lang.ClassLoader.class;
			Field field = var_class_0_.getDeclaredField("nativeLibraries");
			Method method = var_class.getDeclaredMethod("setAccessible", new Class[]{Boolean.TYPE});
			method.invoke(field, new Object[]{Boolean.TRUE});
			try {
				enumeration = Class296_Sub9.aHashtable4632.keys();
				while (enumeration.hasMoreElements()) {
					String string = (String) enumeration.nextElement();
					try {
						File file = (File) Class154.aHashtable1581.get(string);
						Class var_class_1_ = (Class) Class296_Sub9.aHashtable4632.get(string);
						Vector vector = ((Vector) field.get(var_class_1_.getClassLoader()));
						for (int i_2_ = 0; vector.size() > i_2_; i_2_++) {
							try {
								Object object = vector.elementAt(i_2_);
								Field field_3_ = object.getClass().getDeclaredField("name");
								method.invoke(field_3_, new Object[]{Boolean.TRUE});
								try {
									String string_4_ = (String) field_3_.get(object);
									if (string_4_ != null && (string_4_.equalsIgnoreCase(file.getCanonicalPath()))) {
										Field field_5_ = object.getClass().getDeclaredField("handle");
										Method method_6_ = (object.getClass().getDeclaredMethod("finalize", new Class[0]));
										method.invoke(field_5_, (new Object[]{Boolean.TRUE}));
										method.invoke(method_6_, (new Object[]{Boolean.TRUE}));
										try {
											method_6_.invoke(object, new Object[0]);
											field_5_.set(object, new Integer(0));
											hashtable.remove(string);
										} catch (Throwable throwable) {
											/* empty */
										}
										method.invoke(method_6_, (new Object[]{Boolean.FALSE}));
										method.invoke(field_5_, (new Object[]{Boolean.FALSE}));
									}
								} catch (Throwable throwable) {
									/* empty */
								}
								method.invoke(field_3_, new Object[]{Boolean.FALSE});
							} catch (Throwable throwable) {
								/* empty */
							}
						}
					} catch (Throwable throwable) {
						/* empty */
					}
				}
			} catch (Throwable throwable) {
				/* empty */
			}
			method.invoke(field, new Object[]{Boolean.FALSE});
		} catch (Throwable throwable) {
			/* empty */
		}
		Class296_Sub9.aHashtable4632 = hashtable;
		return Class296_Sub9.aHashtable4632.isEmpty();
	}

	static final void method1541(int i) {
		Class47.anInt452 = 0;
		int i_7_ = (((Class296_Sub51_Sub11.localPlayer.tileX) >> 9) + Class206.worldBaseX);
		int i_8_ = (((Class296_Sub51_Sub11.localPlayer.tileY) >> 9) + Class41_Sub26.worldBaseY);
		if (i <= -5) {
			if (i_7_ >= 3053 && i_7_ <= 3156 && i_8_ >= 3056 && i_8_ <= 3136)
				Class47.anInt452 = 1;
			if (i_7_ >= 3072 && i_7_ <= 3118 && i_8_ >= 9492 && i_8_ <= 9535)
				Class47.anInt452 = 1;
			if (Class47.anInt452 == 1 && i_7_ >= 3139 && i_7_ <= 3199 && i_8_ >= 3008 && i_8_ <= 3062)
				Class47.anInt452 = 0;
		}
	}

	final Class31 method1542(byte i, int i_9_) {
		Class31 class31;
		synchronized (aClass113_1552) {
			class31 = (Class31) aClass113_1552.get((long) i_9_);
		}
		if (class31 != null)
			return class31;
		byte[] is;
		synchronized (aClass138_1550) {
			is = aClass138_1550.getFile(19, i_9_);
		}
		class31 = new Class31();
		if (i != -42)
			method1539(59);
		if (is != null)
			class31.method336(new Packet(is), 1007);
		synchronized (aClass113_1552) {
			aClass113_1552.put(class31, (long) i_9_);
		}
		return class31;
	}

	Class151(GameType class35, int i, Js5 class138) {
		aClass138_1550 = class138;
		anInt1549 = aClass138_1550.getLastFileId(19);
	}
}
