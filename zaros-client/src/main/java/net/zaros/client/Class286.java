package net.zaros.client;

/* Class286 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class286 {
	int[] anIntArray2638;
	static Js5 aClass138_2639;
	short[] aShortArray2640;
	short[] aShortArray2641;
	long aLong2642;
	static String aString2643 = "";

	static final void method2382(int i, int i_0_) {
		if (i_0_ != 1)
			aString2643 = null;
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i, 3);
		class296_sub39_sub5.insertIntoQueue();
	}

	public static void method2383(int i) {
		int i_1_ = -46 / ((i + 47) / 47);
		aClass138_2639 = null;
		aString2643 = null;
	}

	Class286(long l, int[] is, short[] is_2_, short[] is_3_) {
		aShortArray2640 = is_2_;
		aLong2642 = l;
		anIntArray2638 = is;
		aShortArray2641 = is_3_;
	}

	static final int[] method2384(int i, int i_4_) {
		Class188.method1889(Class296_Sub28.method2685((byte) 41, i_4_), 26998);
		int[] is = new int[3];
		is[0] = Class296_Sub38.aCalendar4889.get(5);
		is[1] = Class296_Sub38.aCalendar4889.get(2);
		is[2] = Class296_Sub38.aCalendar4889.get(1);
		if (i != 20746)
			aClass138_2639 = null;
		return is;
	}

	protected Class286() {
		/* empty */
	}
}
