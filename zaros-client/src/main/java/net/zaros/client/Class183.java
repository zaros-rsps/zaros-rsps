package net.zaros.client;

/* Class183 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class183 {
	static Class256 aClass256_1879;
	static int anInt1880 = 0;

	static final int method1853(int i) {
		if (i != 0)
			aClass256_1879 = null;
		return GameType.anInt341++;
	}

	public static void method1854(int i) {
		if (i != 32331)
			aClass256_1879 = null;
		aClass256_1879 = null;
	}

	static final void method1855(byte i) {
		Class296_Sub36.method2764((long) Class29.anInt307, (byte) -56, Class41_Sub13.aHa3774);
		if (Class99.anInt1064 != -1)
			Class332.method3414(Class99.anInt1064, true);
		for (int i_0_ = 0; i_0_ < Class154_Sub1.anInt4291; i_0_++) {
			if (Class360_Sub9.aBooleanArray5345[i_0_])
				Class93.aBooleanArray1002[i_0_] = true;
			StaticMethods.aBooleanArray6066[i_0_] = Class360_Sub9.aBooleanArray5345[i_0_];
			Class360_Sub9.aBooleanArray5345[i_0_] = false;
		}
		Class295_Sub2.anInt3498 = Class29.anInt307;
		int i_1_ = 98 / ((i - 30) / 34);
		if (Class99.anInt1064 != -1) {
			Class154_Sub1.anInt4291 = 0;
			Class68.method716((byte) 47);
		}
		Class41_Sub13.aHa3774.la();
		Class368_Sub14.method3850(Class41_Sub13.aHa3774, (byte) -102);
		int i_2_ = Class296_Sub51_Sub38.method3195((byte) 111);
		if (i_2_ == -1)
			i_2_ = Class41_Sub19.anInt3793;
		if (i_2_ == -1)
			i_2_ = Class296_Sub51_Sub32.anInt6512;
		StaticMethods.method2479(-1, i_2_);
		Class217.anInt2119 = 0;
	}

	static final void method1856(int i, int i_3_, int i_4_, Class var_class) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_3_][i_4_];
		if (class247 != null) {
			for (Class234 class234 = class247.aClass234_2341; class234 != null; class234 = class234.aClass234_2224) {
				Class338_Sub3_Sub1 class338_sub3_sub1 = class234.aClass338_Sub3_Sub1_2226;
				if (var_class.isAssignableFrom(class338_sub3_sub1.getClass()) && class338_sub3_sub1.aShort6560 == i_3_ && class338_sub3_sub1.aShort6564 == i_4_) {
					Class159.method1616(class338_sub3_sub1, false);
					break;
				}
			}
		}
	}
}
