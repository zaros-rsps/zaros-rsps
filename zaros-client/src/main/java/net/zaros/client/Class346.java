package net.zaros.client;
/* Class346 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class346 {
	private int[] anIntArray3018;
	private float[][] aFloatArrayArray3019;
	int anInt3020;
	private int[] anIntArray3021;
	private int anInt3022;
	private int[] anIntArray3023;

	final float[] method3665() {
		return aFloatArrayArray3019[method3667()];
	}

	private final void method3666() {
		int[] is = new int[anInt3022];
		int[] is_0_ = new int[33];
		for (int i = 0; i < anInt3022; i++) {
			int i_1_ = anIntArray3018[i];
			if (i_1_ != 0) {
				int i_2_ = 1 << 32 - i_1_;
				int i_3_ = is_0_[i_1_];
				is[i] = i_3_;
				int i_4_;
				if ((i_3_ & i_2_) != 0)
					i_4_ = is_0_[i_1_ - 1];
				else {
					i_4_ = i_3_ | i_2_;
					for (int i_5_ = i_1_ - 1; i_5_ >= 1; i_5_--) {
						int i_6_ = is_0_[i_5_];
						if (i_6_ != i_3_)
							break;
						int i_7_ = 1 << 32 - i_5_;
						if ((i_6_ & i_7_) != 0) {
							is_0_[i_5_] = is_0_[i_5_ - 1];
							break;
						}
						is_0_[i_5_] = i_6_ | i_7_;
					}
				}
				is_0_[i_1_] = i_4_;
				for (int i_8_ = i_1_ + 1; i_8_ <= 32; i_8_++) {
					int i_9_ = is_0_[i_8_];
					if (i_9_ == i_3_)
						is_0_[i_8_] = i_4_;
				}
			}
		}
		anIntArray3023 = new int[8];
		int i = 0;
		for (int i_10_ = 0; i_10_ < anInt3022; i_10_++) {
			int i_11_ = anIntArray3018[i_10_];
			if (i_11_ != 0) {
				int i_12_ = is[i_10_];
				int i_13_ = 0;
				for (int i_14_ = 0; i_14_ < i_11_; i_14_++) {
					int i_15_ = -2147483648 >>> i_14_;
					if ((i_12_ & i_15_) != 0) {
						if (anIntArray3023[i_13_] == 0)
							anIntArray3023[i_13_] = i;
						i_13_ = anIntArray3023[i_13_];
					} else
						i_13_++;
					if (i_13_ >= anIntArray3023.length) {
						int[] is_16_ = new int[anIntArray3023.length * 2];
						for (int i_17_ = 0; i_17_ < anIntArray3023.length; i_17_++)
							is_16_[i_17_] = anIntArray3023[i_17_];
						anIntArray3023 = is_16_;
					}
					i_15_ >>>= 1;
				}
				anIntArray3023[i_13_] = i_10_ ^ 0xffffffff;
				if (i_13_ >= i)
					i = i_13_ + 1;
			}
		}
	}

	final int method3667() {
		int i;
		for (i = 0; anIntArray3023[i] >= 0; i = (Class296_Sub18.method2649() != 0 ? anIntArray3023[i] : i + 1)) {
			/* empty */
		}
		return anIntArray3023[i] ^ 0xffffffff;
	}

	Class346() {
		Class296_Sub18.method2641(24);
		anInt3020 = Class296_Sub18.method2641(16);
		anInt3022 = Class296_Sub18.method2641(24);
		anIntArray3018 = new int[anInt3022];
		boolean bool = Class296_Sub18.method2649() != 0;
		if (bool) {
			int i = 0;
			int i_18_ = Class296_Sub18.method2641(5) + 1;
			while (i < anInt3022) {
				int i_19_ = (Class296_Sub18.method2641(Class296_Sub29_Sub2.method2694((byte) -38, anInt3022 - i)));
				for (int i_20_ = 0; i_20_ < i_19_; i_20_++)
					anIntArray3018[i++] = i_18_;
				i_18_++;
			}
		} else {
			boolean bool_21_ = Class296_Sub18.method2649() != 0;
			for (int i = 0; i < anInt3022; i++) {
				if (bool_21_ && Class296_Sub18.method2649() == 0)
					anIntArray3018[i] = 0;
				else
					anIntArray3018[i] = Class296_Sub18.method2641(5) + 1;
			}
		}
		method3666();
		int i = Class296_Sub18.method2641(4);
		if (i > 0) {
			float f = Class296_Sub18.method2646(Class296_Sub18.method2641(32));
			float f_22_ = Class296_Sub18.method2646(Class296_Sub18.method2641(32));
			int i_23_ = Class296_Sub18.method2641(4) + 1;
			boolean bool_24_ = Class296_Sub18.method2649() != 0;
			int i_25_;
			if (i == 1)
				i_25_ = method3668(anInt3022, anInt3020);
			else
				i_25_ = anInt3022 * anInt3020;
			anIntArray3021 = new int[i_25_];
			for (int i_26_ = 0; i_26_ < i_25_; i_26_++)
				anIntArray3021[i_26_] = Class296_Sub18.method2641(i_23_);
			aFloatArrayArray3019 = new float[anInt3022][anInt3020];
			if (i == 1) {
				for (int i_27_ = 0; i_27_ < anInt3022; i_27_++) {
					float f_28_ = 0.0F;
					int i_29_ = 1;
					for (int i_30_ = 0; i_30_ < anInt3020; i_30_++) {
						int i_31_ = i_27_ / i_29_ % i_25_;
						float f_32_ = ((float) anIntArray3021[i_31_] * f_22_ + f + f_28_);
						aFloatArrayArray3019[i_27_][i_30_] = f_32_;
						if (bool_24_)
							f_28_ = f_32_;
						i_29_ *= i_25_;
					}
				}
			} else {
				for (int i_33_ = 0; i_33_ < anInt3022; i_33_++) {
					float f_34_ = 0.0F;
					int i_35_ = i_33_ * anInt3020;
					for (int i_36_ = 0; i_36_ < anInt3020; i_36_++) {
						float f_37_ = ((float) anIntArray3021[i_35_] * f_22_ + f + f_34_);
						aFloatArrayArray3019[i_33_][i_36_] = f_37_;
						if (bool_24_)
							f_34_ = f_37_;
						i_35_++;
					}
				}
			}
		}
	}

	private static final int method3668(int i, int i_38_) {
		int i_39_;
		for (i_39_ = (int) Math.pow((double) i, 1.0 / (double) i_38_) + 1; Class296_Sub35_Sub2.method2754(-110, i_38_, i_39_) > i; i_39_--) {
			/* empty */
		}
		return i_39_;
	}
}
