package net.zaros.client;

/* Class12 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class12 {
	Class296_Sub39_Sub6 aClass296_Sub39_Sub6_129;
	int anInt130;
	int anInt131;
	static boolean aBoolean133 = true;
	boolean aBoolean134 = false;
	Class296_Sub39_Sub6 aClass296_Sub39_Sub6_135;
	static Class296_Sub48 aClass296_Sub48_136 = null;
	static short[] aShortArray137;
	int anInt138;

	final void method219(byte i) {
		if (i < -30) {
			aClass296_Sub39_Sub6_135 = aClass296_Sub39_Sub6_129 = null;
			anInt138 = 0;
			aBoolean134 = false;
		}
	}

	final boolean method220(boolean bool, int[] is, int i, AnimationsLoader class310, int i_0_, Animation class43) {
		if (bool)
			aBoolean133 = false;
		if (!aBoolean134) {
			if (is.length <= i_0_)
				return false;
			anInt130 = is[i_0_];
			aClass296_Sub39_Sub6_135 = class310.method3298(15, anInt130 >> 16);
			anInt130 &= 0xffff;
			if (aClass296_Sub39_Sub6_135 != null) {
				if (class43.isTweened && i != -1 && is.length > i) {
					anInt131 = is[i];
					aClass296_Sub39_Sub6_129 = class310.method3298(-94, anInt131 >> 16);
					anInt131 &= 0xffff;
				}
				if (class43.aBoolean413)
					anInt138 |= 0x200;
				if (aClass296_Sub39_Sub6_135.method2811((byte) 27, anInt130))
					anInt138 |= 0x80;
				if (aClass296_Sub39_Sub6_135.method2814(anInt130, (byte) -121))
					anInt138 |= 0x100;
				if (aClass296_Sub39_Sub6_135.method2810(anInt130, -8657864))
					anInt138 |= 0x400;
				if (aClass296_Sub39_Sub6_129 != null) {
					if (aClass296_Sub39_Sub6_129.method2811((byte) 27, anInt131))
						anInt138 |= 0x80;
					if (aClass296_Sub39_Sub6_129.method2814(anInt131, (byte) -111))
						anInt138 |= 0x100;
					if (aClass296_Sub39_Sub6_129.method2810(anInt131, -8657864))
						anInt138 |= 0x400;
				}
				aBoolean134 = true;
				anInt138 |= 0x20;
				return true;
			}
			return false;
		}
		return true;
	}

	static final boolean method221(Class324 class324, int i, int i_1_) {
		Class30.aClass373_317.method3903(class324.anIntArray2854[i_1_], class324.anIntArray2863[i_1_], class324.anIntArray2849[i_1_], Class210_Sub1.anIntArray4539);
		int i_2_ = Class210_Sub1.anIntArray4539[2];
		if (i_2_ < 50)
			return false;
		class324.aShortArray2851[i_1_] = (short) ((Class217.anInt2118 * Class210_Sub1.anIntArray4539[0] / i_2_) + Class128.anInt1315);
		class324.aShortArray2859[i_1_] = (short) (Class166_Sub1.anInt4302 + (Class210_Sub1.anIntArray4539[1] * Class369.anInt3136 / i_2_));
		if (i != 24790)
			method221(null, -71, 43);
		class324.aShortArray2858[i_1_] = (short) i_2_;
		return true;
	}

	public Class12() {
		/* empty */
	}

	public static void method222(int i) {
		aClass296_Sub48_136 = null;
		PlayerUpdate.visiblePlayersIndexes = null;
		aShortArray137 = null;
		if (i != 21463)
			aClass296_Sub48_136 = null;
	}

	static {
	}
}
