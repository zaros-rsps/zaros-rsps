package net.zaros.client;

/* Class296_Sub51_Sub26 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub26 extends TextureOperation {
	private int anInt6475 = 0;
	private int anInt6476 = 1;
	private int anInt6477 = 0;

	final void method3076(byte i) {
		int i_0_ = -127 / ((i + 58) / 40);
		Class367.method3800(4096);
	}

	static final void clippedWalk(Player pl, int toMapX, int toMapY, byte walkType) {
		int mapX = pl.waypointQueueX[0];
		int mapY = pl.wayPointQueueY[0];
		if (mapX >= 0 && Class198.currentMapSizeX > mapX && mapY >= 0 && mapY < Class296_Sub38.currentMapSizeY && (toMapX >= 0 && toMapX < Class198.currentMapSizeX && toMapY >= 0 && toMapY < Class296_Sub38.currentMapSizeY)) {
			int numSteps = (RouteFinder.findRoute((BITConfigDefinition.mapClips[pl.z]), -4, pl.getSize(), mapX, mapY, toMapX, toMapY, 0, 0, 0, Class32.pathBufferX, Class124.pathBufferY, true, 0));
			if (numSteps >= 1 && numSteps <= 3) {
				for (int s = 0; s < numSteps - 1; s++)
					pl.addWayPoint(walkType, Class32.pathBufferX[s], Class124.pathBufferY[s]);
			}
		}
	}

	final int[] get_monochrome_output(int i, int i_9_) {
		if (i != 0)
			method3076((byte) -94);
		int[] is = aClass318_5035.method3335(i_9_, (byte) 28);
		if (aClass318_5035.aBoolean2819) {
			int i_10_ = Class294.anIntArray2686[i_9_];
			int i_11_ = i_10_ - 2048 >> 1;
			for (int i_12_ = 0; i_12_ < Class41_Sub10.anInt3769; i_12_++) {
				int i_13_ = Class33.anIntArray334[i_12_];
				int i_14_ = i_13_ - 2048 >> 1;
				int i_15_;
				if (anInt6477 != 0) {
					int i_16_ = i_11_ * i_11_ + i_14_ * i_14_ >> 12;
					i_15_ = (int) (Math.sqrt((double) ((float) i_16_ / 4096.0F)) * 4096.0);
					i_15_ = (int) ((double) (anInt6476 * i_15_) * 3.141592653589793);
				} else
					i_15_ = (-i_10_ + i_13_) * anInt6476;
				i_15_ -= i_15_ & ~0xfff;
				if (anInt6475 == 0)
					i_15_ = Class2.anIntArray56[i_15_ >> 4 & 0xff] + 4096 >> 1;
				else if (anInt6475 == 2) {
					i_15_ -= 2048;
					if (i_15_ < 0)
						i_15_ = -i_15_;
					i_15_ = -i_15_ + 2048 << 1;
				}
				is[i_12_] = i_15_;
			}
		}
		return is;
	}

	static final boolean method3149(int i, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_, int[] is, int i_22_) {
		if (i_19_ > Class213.anInt2110)
			i_19_ = Class213.anInt2110;
		if (i_20_ < 0)
			i_20_ = 0;
		if (i_19_ <= i_20_)
			return true;
		i_22_ = -i_20_ + i_19_ >> 2;
		i_21_ += i_20_ - 1;
		int i_23_ = 31 / ((i_18_ - 24) / 50);
		i_17_ += i_20_ * i;
		if (Class296_Sub51_Sub6.anInt6368 == 1) {
			Class296_Sub39_Sub20.anInt6255 += i_22_;
			while (--i_22_ >= 0) {
				if (is[++i_21_] > i_17_)
					is[i_21_] = i_17_;
				i_17_ += i;
				if (is[++i_21_] > i_17_)
					is[i_21_] = i_17_;
				i_17_ += i;
				if (i_17_ < is[++i_21_])
					is[i_21_] = i_17_;
				i_17_ += i;
				if (is[++i_21_] > i_17_)
					is[i_21_] = i_17_;
				i_17_ += i;
			}
			i_22_ = i_19_ - i_20_ & 0x3;
			while (--i_22_ >= 0) {
				if (i_17_ < is[++i_21_])
					is[i_21_] = i_17_;
				i_17_ += i;
			}
		} else {
			i_17_ -= 38400;
			while (--i_22_ >= 0) {
				if (is[++i_21_] > i_17_)
					return false;
				i_17_ += i;
				if (i_17_ < is[++i_21_])
					return false;
				i_17_ += i;
				if (i_17_ < is[++i_21_])
					return false;
				i_17_ += i;
				if (is[++i_21_] > i_17_)
					return false;
				i_17_ += i;
			}
			i_22_ = -i_20_ + i_19_ & 0x3;
			while (--i_22_ >= 0) {
				if (is[++i_21_] > i_17_)
					return false;
				i_17_ += i;
			}
		}
		return true;
	}

	static final boolean method3150(int i, byte i_24_, int i_25_) {
		if (i_24_ < 57)
			method3150(-47, (byte) 35, 0);
		return ((Class175.method1701((byte) -9, i, i_25_) | r.method2859(i, (byte) -15, i_25_) | Class41_Sub20.method471(0, i, i_25_)) & Class230.method2105(99, i_25_, i));
	}

	public Class296_Sub51_Sub26() {
		super(0, true);
	}

	static final void method3151(int i) {
		int i_26_ = 50 / ((-9 - i) / 51);
		Class326.aFont2903 = null;
		Class387.anImage3713 = null;
	}

	final void method3071(int i, Packet class296_sub17, int i_27_) {
		int i_28_ = i_27_;
		while_145_ : do {
			do {
				if (i_28_ != 0) {
					if (i_28_ != 1) {
						if (i_28_ == 3)
							break;
						break while_145_;
					}
				} else {
					anInt6477 = class296_sub17.g1();
					break while_145_;
				}
				anInt6475 = class296_sub17.g1();
				break while_145_;
			} while (false);
			anInt6476 = class296_sub17.g1();
		} while (false);
		if (i >= -84)
			method3071(-4, null, 69);
	}
}
