package net.zaros.client;

/* Class254 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class254 implements Interface10 {
	int anInt3597;
	static OutgoingPacket aClass311_3598 = new OutgoingPacket(6, 15);
	static int[] anIntArray3599 = new int[6];

	static final void method2210(int i) {
		for (int i_0_ = i; i_0_ < 100; i_0_++)
			Class198.aClass228Array2002[i_0_] = null;
		ConfigsRegister.anInt3671 = 0;
	}

	Class254(int i) {
		anInt3597 = i;
	}

	static final boolean method2211(int i, int i_1_, int i_2_) {
		if (i_2_ != 384)
			method2210(48);
		if ((i & 0x180) == 0)
			return false;
		return true;
	}

	static final void method2212(int i, boolean bool) {
		if (bool && Class106.aClass296_Sub39_Sub14_1097 != null)
			Class154.anInt1580 = Class106.aClass296_Sub39_Sub14_1097.anInt6214;
		else
			Class154.anInt1580 = -1;
		Class366_Sub8.aClass155_5409 = null;
		Class106.aClass296_Sub39_Sub14_1097 = null;
		Class41_Sub13.anInt3773 = 0;
		Class359.aClass51_3092 = null;
		Class106.method938();
		Class106.aClass155_1101.method1581(i + 299219);
		Class264.aClass256_2472 = null;
		ParamTypeList.aClass256_1694 = null;
		Class183.aClass256_1879 = null;
		Class106.aClass259_1107 = null;
		Class41_Sub1.aClass256_3737 = null;
		Class41_Sub14.aClass256_3776 = null;
		Class134.aClass256_1386 = null;
		BITConfigDefinition.anInt2604 = -1;
		Class162.aClass256_3559 = null;
		Class49.aClass256_464 = null;
		if (i != 28461)
			method2213(54);
		Class296_Sub51_Sub5.anInt6365 = -1;
		Class152.aClass397_1570 = null;
		if (Class106.aClass245_1094 != null) {
			Class106.aClass245_1094.method2182(i ^ 0x6f2f);
			Class106.aClass245_1094.method2183(64, i ^ 0x6fad, 128);
		}
		if (Class106.aClass401_1096 != null)
			Class106.aClass401_1096.method4141(64, (byte) -65, 64);
		if (Class106.aClass38_1091 != null)
			Class106.aClass38_1091.method365(64, (byte) 101);
		Class296_Sub43.bitConfigsLoader.method2104(64, 1);
	}

	public static void method2213(int i) {
		if (i != 64)
			method2211(-12, 107, 27);
		aClass311_3598 = null;
		anIntArray3599 = null;
	}

	public final Class294 method45(byte i) {
		int i_3_ = 84 / ((i + 6) / 44);
		return Class296_Sub39_Sub14.aClass294_6218;
	}

	static final String method2214(String string, boolean bool) {
		String string_4_ = null;
		int i = string.indexOf("--> ");
		if (bool)
			return null;
		if (i >= 0) {
			string_4_ = string.substring(0, i + 4);
			string = string.substring(i + 4);
		}
		if (string.startsWith("directlogin ")) {
			int i_5_ = string.indexOf(" ", "directlogin ".length());
			if (i_5_ >= 0) {
				int i_6_ = string.length();
				string = string.substring(0, i_5_) + " ";
				for (int i_7_ = i_5_ + 1; i_7_ < i_6_; i_7_++)
					string += "*";
			}
		}
		if (string_4_ == null)
			return string;
		return string_4_ + string;
	}
}
