package net.zaros.client;

/* Class327 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class327 {
	static int[] anIntArray2905 = {0, 4, 3, 3, 1, 1, 3, 5, 1, 5, 3, 6, 4};

	static final void method3388(int i, int i_0_) {
		int i_1_ = Class29.anInt307 - ClotchesLoader.anInt273;
		if (i_1_ >= 100) {
			Class361.anInt3103 = 1;
			Class368_Sub11.anInt5486 = Class242.anInt2309 = -1;
		} else {
			int i_2_ = (int) Class153.aFloat1572;
			if (i_2_ < Class352.anInt3044 >> 8)
				i_2_ = Class352.anInt3044 >> 8;
			if (Class296_Sub51_Sub13.aBooleanArray6414[4] && Class61.anIntArray710[4] + 128 > i_2_)
				i_2_ = Class61.anIntArray710[4] + 128;
			int i_3_ = (int) Class41_Sub26.aFloat3806 + Class134.anInt1387 & 0x3fff;
			Class9.method194(i_2_, 100, Class296_Sub14.anInt4668, i, aa_Sub1.method155(-1537652855, FileWorker.anInt3005, (Class296_Sub51_Sub11.localPlayer.tileX), (Class296_Sub51_Sub11.localPlayer.tileY)) - 200, (i_2_ >> 3) * 3 + 600 << 2, Class296_Sub24.anInt4762, i_3_);
			if (i_0_ <= 30)
				method3389(-2);
			float f = 1.0F - ((float) ((-i_1_ + 100) * ((-i_1_ + 100) * (-i_1_ + 100))) / 1000000.0F);
			Class296_Sub17_Sub2.camRotX = (int) ((float) Class319.anInt3646 + (float) (-Class319.anInt3646 + Class296_Sub17_Sub2.camRotX) * f);
			Class219.camPosX = (int) ((float) Class322.anInt2826 + f * (float) (Class219.camPosX - Class322.anInt2826));
			Class124.camPosZ = (int) ((float) Class368.anInt3130 + (float) (Class124.camPosZ - Class368.anInt3130) * f);
			TranslatableString.camPosY = (int) ((float) (-StaticMethods.anInt5949 + TranslatableString.camPosY) * f + (float) StaticMethods.anInt5949);
			int i_4_ = Class44_Sub1.camRotY - Class368_Sub5.anInt5454;
			if (i_4_ > 8192)
				i_4_ -= 16384;
			else if (i_4_ < -8192)
				i_4_ += 16384;
			Class44_Sub1.camRotY = (int) (f * (float) i_4_ + (float) Class368_Sub5.anInt5454);
			Class44_Sub1.camRotY &= 0x3fff;
		}
	}

	public static void method3389(int i) {
		int i_5_ = 90 / ((i + 65) / 48);
		anIntArray2905 = null;
	}

	public Class327() {
		/* empty */
	}

	abstract long method3390(int i);

	static final void method3391(int i, int i_6_, int i_7_, int i_8_, byte[] is, int i_9_, int i_10_, byte[] is_11_, int i_12_) {
		if (i_12_ == 21245) {
			int i_13_ = -(i_8_ >> 2);
			i_8_ = -(i_8_ & 0x3);
			for (int i_14_ = -i_10_; i_14_ < 0; i_14_++) {
				for (int i_15_ = i_13_; i_15_ < 0; i_15_++) {
					is[i++] += is_11_[i_7_++];
					is[i++] += is_11_[i_7_++];
					is[i++] += is_11_[i_7_++];
					is[i++] += is_11_[i_7_++];
				}
				for (int i_16_ = i_8_; i_16_ < 0; i_16_++)
					is[i++] += is_11_[i_7_++];
				i += i_9_;
				i_7_ += i_6_;
			}
		}
	}
}
