package net.zaros.client;
/* Class203 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;

import jaggl.OpenGL;

final class Class203 implements Interface9 {
	static boolean objectTag = false;
	static int anInt3564 = 0;
	private Sprite aClass397_3565;
	static long[] aLongArray3566;
	private Js5 aClass138_3567;
	private Class254 aClass254_3568;
	static int anInt3569;

	public final boolean method44(byte i) {
		if (i <= 91)
			return true;
		return aClass138_3567.hasEntryBuffer(aClass254_3568.anInt3597);
	}

	static final ha method1961(Canvas canvas, d var_d, int i, boolean bool, Js5 class138) {
		if (!Class134.method1402(20))
			throw new RuntimeException("");
		if (!Class366_Sub4.method3779("jaggl", (byte) -17))
			throw new RuntimeException("");
		OpenGL opengl = new OpenGL();
		long l = opengl.init(canvas, 8, 8, 8, 24, 0, i);
		if (l == 0L)
			throw new RuntimeException("");
		ha_Sub1_Sub1 var_ha_Sub1_Sub1 = new ha_Sub1_Sub1(opengl, canvas, l, var_d, class138, i);
		if (bool)
			method1962((byte) -25);
		var_ha_Sub1_Sub1.method1193(-16649);
		return var_ha_Sub1_Sub1;
	}

	Class203(Js5 class138, Class254 class254) {
		aClass254_3568 = class254;
		aClass138_3567 = class138;
	}

	public final void method42(byte i) {
		if (i != 88)
			method43(true, -23);
		aClass397_3565 = CS2Script.method2798(aClass138_3567, aClass254_3568.anInt3597, i - 91);
	}

	public static void method1962(byte i) {
		aLongArray3566 = null;
		if (i < 59)
			aLongArray3566 = null;
	}

	public final void method43(boolean bool, int i) {
		if (bool) {
			int i_0_ = (Class368_Sub7.anInt5463 < Class241.anInt2301 ? Class241.anInt2301 : Class368_Sub7.anInt5463);
			int i_1_ = (Class384.anInt3254 <= Class296_Sub15_Sub1.anInt5996 ? Class296_Sub15_Sub1.anInt5996 : Class384.anInt3254);
			int i_2_ = aClass397_3565.method4099();
			int i_3_ = aClass397_3565.method4088();
			int i_4_ = 0;
			int i_5_ = i_0_;
			int i_6_ = i_3_ * i_0_ / i_2_;
			int i_7_ = (-i_6_ + i_1_) / 2;
			if (i_1_ < i_6_) {
				i_7_ = 0;
				i_5_ = i_2_ * i_1_ / i_3_;
				i_6_ = i_1_;
				i_4_ = (-i_5_ + i_0_) / 2;
			}
			aClass397_3565.method4080(i_4_, i_7_, i_5_, i_6_);
		}
		int i_8_ = 51 % ((-32 - i) / 52);
	}
}
