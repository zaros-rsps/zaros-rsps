package net.zaros.client;

/* Class296_Sub51_Sub9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub9 extends TextureOperation {
	private int anInt6387 = 4096;
	static float aFloat6388;
	static int anInt6389 = 0;

	static final void method3099(int i, String string, int i_0_, String string_1_, String string_2_, boolean bool, String string_3_, int i_4_, String string_5_) {
		Class228 class228 = Class198.aClass228Array2002[99];
		for (int i_6_ = 99; i_6_ > 0; i_6_--)
			Class198.aClass228Array2002[i_6_] = Class198.aClass228Array2002[i_6_ - 1];
		if (class228 == null)
			class228 = new Class228(i, i_0_, string, string_5_, string_3_, string_1_, i_4_, string_2_);
		else
			class228.method2094(i_0_, string_5_, string_1_, i, (byte) -127, string_3_, string_2_, i_4_, string);
		ConfigsRegister.anInt3671++;
		Class198.aClass228Array2002[0] = class228;
		if (bool)
			unpackContainer(true, null);
		Class318.anInt2808 = Class338_Sub3_Sub3_Sub1.anInt6616;
	}

	static final byte[] unpackContainer(boolean bool, byte[] is) {
		Packet class296_sub17 = new Packet(is);
		int i = class296_sub17.g1();
		if (bool)
			return null;
		int i_7_ = class296_sub17.g4();
		if (i_7_ < 0 || Class395.anInt3314 != 0 && i_7_ > Class395.anInt3314)
			throw new RuntimeException();
		if (i != 0) {
			int i_8_ = class296_sub17.g4();
			if (i_8_ < 0 || Class395.anInt3314 != 0 && Class395.anInt3314 < i_8_) {
				return new byte[100]; // throw new RuntimeException();
			}
			if (i == 2) { // GZIP
				// Check header/size
				if (is[9] != 31 || is[10] != -117 || i_8_ > 1024 * 1024) {
					System.out.println("head: " + is[9] + " " + is[10] + " size: " + i_8_);
					System.out.println("Invalid GZIP header, invalid decryption?");
					return new byte[100];
				}
			}
			byte[] is_9_ = new byte[i_8_];
			if (i != 1) {
				synchronized (Class355_Sub2.aClass134_3499) {
					Class355_Sub2.aClass134_3499.method1403(is_9_, -3651, class296_sub17);
				}
			} else
				Class305.method3273(is_9_, i_8_, is, i_7_, 9);
			return is_9_;
		}
		byte[] is_10_ = new byte[i_7_];
		class296_sub17.readBytes(is_10_, 0, i_7_);
		return is_10_;
	}

	final int[] get_monochrome_output(int i, int i_11_) {
		if (i != 0)
			anInt6389 = -61;
		int[] is = aClass318_5035.method3335(i_11_, (byte) 28);
		if (aClass318_5035.aBoolean2819)
			Class291.method2407(is, 0, Class41_Sub10.anInt3769, anInt6387);
		return is;
	}

	Class296_Sub51_Sub9(int i) {
		super(0, true);
		anInt6387 = i;
	}

	public Class296_Sub51_Sub9() {
		this(4096);
	}

	final void method3071(int i, Packet class296_sub17, int i_12_) {
		int i_13_ = i_12_;
		if (i_13_ == 0)
			anInt6387 = (class296_sub17.g1() << 12) / 255;
		if (i >= -84) {
			/* empty */
		}
	}

	static final void method3101(int i, int i_14_) {
		if (InterfaceComponent.loadInterface(i_14_)) {
			InterfaceComponent[] class51s = Class192.openedInterfaceComponents[i_14_];
			if (i < 25)
				anInt6389 = 85;
			for (InterfaceComponent class51 : class51s) {
				if (class51 != null && class51.aClass44_554 != null) {
					class51.aClass44_554.method547(14899);
				}
			}
		}
	}
}
