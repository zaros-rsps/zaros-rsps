package net.zaros.client;

/* Class41_Sub12 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub12 extends Class41 {
	static Class209 aClass209_3770 = new Class209(22);
	static ModeWhere intBetaModeWhere = new ModeWhere("INTBETA", "office", "_intbeta", 6);

	final void method381(int i, byte i_0_) {
		if (i_0_ != -110)
			aClass209_3770 = null;
		anInt389 = i;
	}

	static final boolean method438(int i, int i_1_, int i_2_) {
		int i_3_ = -64 / ((i_1_ + 52) / 45);
		if (!((i & 0x70000) != 0 | Class41_Sub26.method500(i_2_, 123, i)) && !Class296_Sub34_Sub2.method2741(i, i_2_, (byte) -79))
			return false;
		return true;
	}

	final int method383(byte i) {
		if (aClass296_Sub50_392.method3055(true).method326(-5469) > 1)
			return 4;
		if (i != 110)
			return 64;
		return 2;
	}

	static final void method439(Packet class296_sub17, int i, int i_4_) {
		if (i_4_ == 24) {
			if (Applet_Sub1.aClass255_10 != null) {
				try {
					Applet_Sub1.aClass255_10.seek(0L);
					Applet_Sub1.aClass255_10.write((class296_sub17.data), i, 24);
				} catch (Exception exception) {
					/* empty */
				}
			}
		}
	}

	Class41_Sub12(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	Class41_Sub12(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	final int method440(int i) {
		if (i <= 114)
			return -84;
		return anInt389;
	}

	public static void method441(boolean bool) {
		if (bool)
			method438(-3, 71, -25);
		intBetaModeWhere = null;
		aClass209_3770 = null;
	}

	static final void method442(int i, Js5 class138) {
		Class368_Sub15.anInt5518 = class138.getFileIndex("p11_full");
		Mesh.anInt1364 = class138.getFileIndex("p12_full");
		if (i == 14576)
			Class190.anInt1936 = class138.getFileIndex("b12_full");
	}

	final int method380(int i, byte i_5_) {
		if (i_5_ != 41)
			aClass209_3770 = null;
		return 1;
	}

	final void method386(int i) {
		if (anInt389 < 0 && anInt389 > 4)
			anInt389 = method383((byte) 110);
		if (i != 2)
			method438(65, -74, -47);
	}
}
