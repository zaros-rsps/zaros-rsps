package net.zaros.client;

/* Class355_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class355_Sub1 extends Class355 implements Interface20 {
	static int anInt3693 = -1;
	static HashTable itemsNodes = new HashTable(32);
	static float aFloat3695;
	private int anInt3696;

	public final void method75(int i, byte[] is, int i_0_, boolean bool) {
		this.method3696(is, 0, i);
		if (bool)
			method3701(false, 26);
		anInt3696 = i_0_;
	}

	static final void method3700(int i, boolean bool) {
		if (bool != true)
			itemsNodes = null;
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i, 10);
		class296_sub39_sub5.insertIntoQueue();
	}

	public final long method74(byte i) {
		if (i < 61)
			aFloat3695 = 0.18236594F;
		return 0L;
	}

	static final boolean method3701(boolean bool, int i) {
		if (bool != true)
			anInt3693 = 12;
		if (i != 3 && i != 4 && i != 5 && i != 6)
			return false;
		return true;
	}

	public static final void method3702(byte i, boolean bool, boolean bool_1_) {
		if (bool) {
			Class296_Sub34.anInt4843--;
			if (Class296_Sub34.anInt4843 == 0)
				Class166_Sub1.anIntArray4300 = null;
		}
		if (i != 78)
			itemsNodes = null;
		if (bool_1_) {
			StaticMethods.anInt2915--;
			if (StaticMethods.anInt2915 == 0)
				Class295_Sub1.anIntArray3691 = null;
		}
	}

	public static void method3703(int i) {
		if (i != 15329)
			aFloat3695 = -0.664079F;
		itemsNodes = null;
	}

	public final int method76(boolean bool) {
		if (bool != true)
			method76(false);
		return anInt3696;
	}

	public final int method77(byte i) {
		int i_2_ = -99 / ((-19 - i) / 34);
		return anInt3062;
	}

	final void method3695(byte i) {
		if (i <= 64)
			anInt3693 = 27;
		aHa_Sub3_3066.method1303(-22, this);
	}

	Class355_Sub1(ha_Sub3 var_ha_Sub3, int i, byte[] is, int i_3_, boolean bool) {
		super(var_ha_Sub3, 34963, is, i_3_, bool);
		anInt3696 = i;
	}
}
