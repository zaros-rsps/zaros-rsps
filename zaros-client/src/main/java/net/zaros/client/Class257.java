package net.zaros.client;
/* Class257 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.*;
import java.awt.image.BufferedImage;

final class Class257 {
	private Component aComponent2411;
	private Robot aRobot2412;
	private static Component aComponent2411_static;
	private static Robot aRobot2412_static;

	final void method2235(Component component, int[] is, int i, int i_0_, Point point) {
		if (is == null)
			component.setCursor(null);
		else {
			BufferedImage bufferedimage = new BufferedImage(i, i_0_, 2);
			bufferedimage.setRGB(0, 0, i, i_0_, is, 0, i);
			component.setCursor(component.getToolkit().createCustomCursor(bufferedimage, point, null));
		}
	}

	Class257() throws Exception {
		aRobot2412 = new Robot();
	}
	
	

	final void method2236(Component component, boolean bool) {
		if (bool)
			component = null;
		else if (component == null)
			throw new NullPointerException();
		if (component != aComponent2411) {
			if (aComponent2411 != null) {
				aComponent2411.setCursor(null);
				aComponent2411 = null;
			}
			if (component != null) {
				component.setCursor(component.getToolkit().createCustomCursor(new BufferedImage(1, 1, 2), new Point(0, 0), null));
				aComponent2411 = component;
			}
		}
	}

	final void method2237(int i, int i_1_) {
		aRobot2412.mouseMove(i, i_1_);
	}

	public static void method2236_static(Component aCanvas2209, boolean b) {
		if (b)
			aCanvas2209 = null;
		else if (aCanvas2209 == null)
			throw new NullPointerException();
		if (aCanvas2209 != aComponent2411_static) {
			if (aComponent2411_static != null) {
				aComponent2411_static = null;
			}
			if (aCanvas2209 != null) {
				aComponent2411_static = aCanvas2209;
			}
		}
	}
}
