package net.zaros.client;

/* Class71 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class71 {
	int anInt839 = -1;
	int anInt840;
	int anInt841;
	int anInt842 = -1;
	int[] anIntArray843;

	private final void method767(Packet class296_sub17, int i, byte i_0_) {
		int i_1_ = -64 / ((-75 - i_0_) / 32);
		if (i != 1) {
			if (i != 2) {
				if (i != 3) {
					if (i == 4)
						anInt840 = class296_sub17.g1();
					else if (i == 5)
						anInt841 = class296_sub17.g2();
				} else
					anInt839 = class296_sub17.g1();
			} else {
				anIntArray843 = new int[class296_sub17.g1()];
				for (int i_2_ = 0; i_2_ < anIntArray843.length; i_2_++)
					anIntArray843[i_2_] = class296_sub17.g2();
			}
		} else
			anInt842 = class296_sub17.g2();
	}

	static final void method768(Mobile class338_sub3_sub1_sub3, byte i, boolean bool) {
		if (i == 104) {
			Class280 class280 = class338_sub3_sub1_sub3.method3516(false);
			if (class338_sub3_sub1_sub3.currentWayPoint == 0) {
				class338_sub3_sub1_sub3.anInt6836 = 0;
				Class41_Sub5.anInt3757 = -1;
				Class45.anInt438 = 0;
			} else {
				if (class338_sub3_sub1_sub3.aClass44_6802.method570((byte) 40) && !class338_sub3_sub1_sub3.aClass44_6802.method567(1)) {
					Animation class43 = class338_sub3_sub1_sub3.aClass44_6802.method563(i - 105);
					if (class338_sub3_sub1_sub3.anInt6829 > 0 && class43.anInt404 == 0) {
						Class45.anInt438 = 0;
						class338_sub3_sub1_sub3.anInt6836++;
						Class41_Sub5.anInt3757 = -1;
						return;
					}
					if (class338_sub3_sub1_sub3.anInt6829 <= 0 && class43.walkingProperties == 0) {
						Class41_Sub5.anInt3757 = -1;
						class338_sub3_sub1_sub3.anInt6836++;
						Class45.anInt438 = 0;
						return;
					}
				}
				for (int i_3_ = 0; i_3_ < class338_sub3_sub1_sub3.aClass212Array6817.length; i_3_++) {
					if ((class338_sub3_sub1_sub3.aClass212Array6817[i_3_].anInt2105) != -1 && class338_sub3_sub1_sub3.aClass212Array6817[i_3_].aClass44_2108.method567(1)) {
						Graphic class23 = (Class157.graphicsLoader.getGraphic((class338_sub3_sub1_sub3.aClass212Array6817[i_3_].anInt2105)));
						if (class23.aBoolean267 && class23.anInt264 != -1) {
							Animation class43 = (Class296_Sub51_Sub13.animationsLoader.getAnimation(class23.anInt264, (byte) -124));
							if (class338_sub3_sub1_sub3.anInt6829 > 0 && class43.anInt404 == 0) {
								Class41_Sub5.anInt3757 = -1;
								Class45.anInt438 = 0;
								class338_sub3_sub1_sub3.anInt6836++;
								return;
							}
							if (class338_sub3_sub1_sub3.anInt6829 <= 0 && class43.walkingProperties == 0) {
								class338_sub3_sub1_sub3.anInt6836++;
								Class45.anInt438 = 0;
								Class41_Sub5.anInt3757 = -1;
								return;
							}
						}
					}
				}
				int i_4_ = class338_sub3_sub1_sub3.tileX;
				int i_5_ = class338_sub3_sub1_sub3.tileY;
				int i_6_ = ((class338_sub3_sub1_sub3.waypointQueueX[class338_sub3_sub1_sub3.currentWayPoint - 1]) * 512 + class338_sub3_sub1_sub3.getSize() * 256);
				int i_7_ = ((class338_sub3_sub1_sub3.wayPointQueueY[class338_sub3_sub1_sub3.currentWayPoint - 1]) * 512 + class338_sub3_sub1_sub3.getSize() * 256);
				if (i_6_ > i_4_) {
					if (i_5_ >= i_7_) {
						if (i_7_ >= i_5_)
							class338_sub3_sub1_sub3.method3499(12288);
						else
							class338_sub3_sub1_sub3.method3499(14336);
					} else
						class338_sub3_sub1_sub3.method3499(10240);
				} else if (i_4_ <= i_6_) {
					if (i_5_ < i_7_)
						class338_sub3_sub1_sub3.method3499(8192);
					else if (i_5_ > i_7_)
						class338_sub3_sub1_sub3.method3499(0);
				} else if (i_5_ >= i_7_) {
					if (i_5_ > i_7_)
						class338_sub3_sub1_sub3.method3499(2048);
					else
						class338_sub3_sub1_sub3.method3499(4096);
				} else
					class338_sub3_sub1_sub3.method3499(6144);
				byte i_8_ = (class338_sub3_sub1_sub3.walkingTypes[class338_sub3_sub1_sub3.currentWayPoint - 1]);
				if (!bool && (-i_4_ + i_6_ > 1024 || -i_4_ + i_6_ < -1024 || i_7_ - i_5_ > 1024 || i_7_ - i_5_ < -1024)) {
					class338_sub3_sub1_sub3.tileX = i_6_;
					class338_sub3_sub1_sub3.tileY = i_7_;
					class338_sub3_sub1_sub3.method3517(false, true, (class338_sub3_sub1_sub3.anInt6816));
					class338_sub3_sub1_sub3.currentWayPoint--;
					Class45.anInt438 = 0;
					Class41_Sub5.anInt3757 = -1;
					if (class338_sub3_sub1_sub3.anInt6829 > 0)
						class338_sub3_sub1_sub3.anInt6829--;
				} else {
					int i_9_ = 16;
					boolean bool_10_ = true;
					if (class338_sub3_sub1_sub3 instanceof NPC)
						bool_10_ = (((NPC) class338_sub3_sub1_sub3).definition.aBoolean1470);
					if (!bool_10_) {
						if (!bool && class338_sub3_sub1_sub3.currentWayPoint > 1)
							i_9_ = 24;
						if (!bool && class338_sub3_sub1_sub3.currentWayPoint > 2)
							i_9_ = 32;
					} else {
						int i_11_ = (-class338_sub3_sub1_sub3.aClass5_6804.anInt77 + class338_sub3_sub1_sub3.anInt6816);
						if (i_11_ != 0 && class338_sub3_sub1_sub3.interactingWithIndex == -1 && class338_sub3_sub1_sub3.anInt6815 != 0)
							i_9_ = 8;
						if (!bool && class338_sub3_sub1_sub3.currentWayPoint > 2)
							i_9_ = 24;
						if (!bool && class338_sub3_sub1_sub3.currentWayPoint > 3)
							i_9_ = 32;
					}
					if (class338_sub3_sub1_sub3.anInt6836 > 0 && class338_sub3_sub1_sub3.currentWayPoint > 1) {
						i_9_ = 32;
						class338_sub3_sub1_sub3.anInt6836--;
					}
					if (i_8_ != 2) {
						if (i_8_ == 0)
							i_9_ >>= 1;
					} else
						i_9_ <<= 1;
					Class45.anInt438 = 0;
					if (class280.anInt2568 != -1) {
						i_9_ <<= 9;
						if (class338_sub3_sub1_sub3.currentWayPoint == 1) {
							int i_12_ = (class338_sub3_sub1_sub3.anInt6833 * class338_sub3_sub1_sub3.anInt6833);
							int i_13_ = ((i_6_ < class338_sub3_sub1_sub3.tileX ? class338_sub3_sub1_sub3.tileX - i_6_ : i_6_ - class338_sub3_sub1_sub3.tileX) << 9);
							int i_14_ = ((class338_sub3_sub1_sub3.tileY <= i_7_ ? i_7_ - class338_sub3_sub1_sub3.tileY : (-i_7_ + class338_sub3_sub1_sub3.tileY)) << 9);
							int i_15_ = i_14_ < i_13_ ? i_13_ : i_14_;
							int i_16_ = i_15_ * class280.anInt2568 * 2;
							if (i_12_ > i_16_)
								class338_sub3_sub1_sub3.anInt6833 /= 2;
							else if (i_15_ < i_12_ / 2) {
								class338_sub3_sub1_sub3.anInt6833 -= class280.anInt2568;
								if (class338_sub3_sub1_sub3.anInt6833 < 0)
									class338_sub3_sub1_sub3.anInt6833 = 0;
							} else if (i_9_ > class338_sub3_sub1_sub3.anInt6833) {
								class338_sub3_sub1_sub3.anInt6833 += class280.anInt2568;
								if (class338_sub3_sub1_sub3.anInt6833 > i_9_)
									class338_sub3_sub1_sub3.anInt6833 = i_9_;
							}
						} else if (i_9_ <= class338_sub3_sub1_sub3.anInt6833) {
							if (class338_sub3_sub1_sub3.anInt6833 > 0) {
								class338_sub3_sub1_sub3.anInt6833 -= class280.anInt2568;
								if (class338_sub3_sub1_sub3.anInt6833 < 0)
									class338_sub3_sub1_sub3.anInt6833 = 0;
							}
						} else {
							class338_sub3_sub1_sub3.anInt6833 += class280.anInt2568;
							if (i_9_ < class338_sub3_sub1_sub3.anInt6833)
								class338_sub3_sub1_sub3.anInt6833 = i_9_;
						}
						i_9_ = class338_sub3_sub1_sub3.anInt6833 >> 9;
						if (i_9_ < 1)
							i_9_ = 1;
					}
					if (i_6_ == i_4_ && i_7_ == i_5_)
						Class41_Sub5.anInt3757 = -1;
					else {
						if (i_6_ > i_4_) {
							Class45.anInt438 |= 0x4;
							class338_sub3_sub1_sub3.tileX += i_9_;
							if (i_6_ < class338_sub3_sub1_sub3.tileX)
								class338_sub3_sub1_sub3.tileX = i_6_;
						} else if (i_4_ > i_6_) {
							class338_sub3_sub1_sub3.tileX -= i_9_;
							Class45.anInt438 |= 0x8;
							if (i_6_ > class338_sub3_sub1_sub3.tileX)
								class338_sub3_sub1_sub3.tileX = i_6_;
						}
						if (i_5_ >= i_7_) {
							if (i_7_ < i_5_) {
								Class45.anInt438 |= 0x2;
								class338_sub3_sub1_sub3.tileY -= i_9_;
								if (class338_sub3_sub1_sub3.tileY < i_7_)
									class338_sub3_sub1_sub3.tileY = i_7_;
							}
						} else {
							class338_sub3_sub1_sub3.tileY += i_9_;
							Class45.anInt438 |= 0x1;
							if (class338_sub3_sub1_sub3.tileY > i_7_)
								class338_sub3_sub1_sub3.tileY = i_7_;
						}
						if (i_9_ < 32)
							Class41_Sub5.anInt3757 = i_8_;
						else
							Class41_Sub5.anInt3757 = 2;
					}
					if (class338_sub3_sub1_sub3.tileX == i_6_ && class338_sub3_sub1_sub3.tileY == i_7_) {
						class338_sub3_sub1_sub3.currentWayPoint--;
						if (class338_sub3_sub1_sub3.anInt6829 > 0)
							class338_sub3_sub1_sub3.anInt6829--;
					}
				}
			}
		}
	}

	static final void method769(int i, String string, int i_17_, boolean bool, String string_18_) {
		Class27.method314(true, string_18_, -1, i_17_, (byte) 116, string, bool);
		if (i != 0) {
			/* empty */
		}
	}

	final void method770(Packet class296_sub17, byte i) {
		if (i != -93)
			anInt842 = -66;
		for (;;) {
			int i_19_ = class296_sub17.g1();
			if (i_19_ == 0)
				break;
			method767(class296_sub17, i_19_, (byte) 65);
		}
	}

	public Class71() {
		anInt841 = -1;
		anInt840 = 0;
	}
}
