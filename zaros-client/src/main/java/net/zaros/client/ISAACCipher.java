package net.zaros.client;

/* Class185 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class ISAACCipher {
	private int currentSEED;
	private int[] seeds;
	private int last;
	private int accumulator;
	static Class131 aClass131_1892 = new Class131();
	private int[] mix;
	private int sum;
	static int anInt1895 = 0;
	static Class289 aClass289_1896 = new Class289();
	static int anInt1897;
	static int anInt1898 = 0;

	static final void method1862(int i, int i_0_) {
		if (i_0_ < -119) {
			GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask(i, 9);
			class296_sub39_sub5.insertIntoQueue();
		}
	}

	public static void method1863(int i) {
		aClass289_1896 = null;
		aClass131_1892 = null;
		if (i != -257) {
			method1863(-55);
		}
	}

	private final void mix() {
		last += ++sum;
		for (int pos = 0; pos < 256; pos++) {
			int m = mix[pos];
			if ((pos & 0x2) != 0) {
				if ((pos & 0x1) != 0) {
					accumulator ^= accumulator >>> 16;
				} else {
					accumulator ^= accumulator << 2;
				}
			} else if ((pos & 0x1) != 0) {
				accumulator ^= accumulator >>> 6;
			} else {
				accumulator ^= accumulator << 13;
			}
			accumulator += mix[pos + 128 & 0xff];
			int i;
			mix[pos] = i = last + accumulator + mix[(1020 & m) >> 2];
			seeds[pos] = last = m + mix[(i & 261353) >> 8 >> 2];
		}
	}

	final int peek() {
		if (!Constants.ENABLE_ISAAC) {
			return 0;
		}
		if (currentSEED == 0) {
			mix();
			currentSEED = 256;
		}
		return seeds[currentSEED - 1];
	}

	private final void randomizeISAAC() {
		int i_5_;
		int i_6_;
		int i_7_;
		int i_8_;
		int i_9_;
		int i_10_;
		int i_11_;
		int i_4_ = i_5_ = i_6_ = i_7_ = i_8_ = i_9_ = i_10_ = i_11_ = -1640531527;
		for (int i_12_ = 0; i_12_ < 4; i_12_++) {
			i_4_ ^= i_5_ << 11;
			i_7_ += i_4_;
			i_5_ += i_6_;
			i_5_ ^= i_6_ >>> 2;
			i_6_ += i_7_;
			i_8_ += i_5_;
			i_6_ ^= i_7_ << 8;
			i_9_ += i_6_;
			i_7_ += i_8_;
			i_7_ ^= i_8_ >>> 16;
			i_8_ += i_9_;
			i_10_ += i_7_;
			i_8_ ^= i_9_ << 10;
			i_9_ += i_10_;
			i_11_ += i_8_;
			i_9_ ^= i_10_ >>> 4;
			i_10_ += i_11_;
			i_4_ += i_9_;
			i_10_ ^= i_11_ << 8;
			i_11_ += i_4_;
			i_5_ += i_10_;
			i_11_ ^= i_4_ >>> 9;
			i_4_ += i_5_;
			i_6_ += i_11_;
		}
		for (int i_13_ = 0; i_13_ < 256; i_13_ += 8) {
			i_8_ += seeds[i_13_ + 4];
			i_9_ += seeds[i_13_ + 5];
			i_10_ += seeds[i_13_ + 6];
			i_4_ += seeds[i_13_];
			i_5_ += seeds[i_13_ + 1];
			i_11_ += seeds[i_13_ + 7];
			i_6_ += seeds[i_13_ + 2];
			i_7_ += seeds[i_13_ + 3];
			i_4_ ^= i_5_ << 11;
			i_5_ += i_6_;
			i_7_ += i_4_;
			i_5_ ^= i_6_ >>> 2;
			i_8_ += i_5_;
			i_6_ += i_7_;
			i_6_ ^= i_7_ << 8;
			i_9_ += i_6_;
			i_7_ += i_8_;
			i_7_ ^= i_8_ >>> 16;
			i_10_ += i_7_;
			i_8_ += i_9_;
			i_8_ ^= i_9_ << 10;
			i_11_ += i_8_;
			i_9_ += i_10_;
			i_9_ ^= i_10_ >>> 4;
			i_10_ += i_11_;
			i_4_ += i_9_;
			i_10_ ^= i_11_ << 8;
			i_5_ += i_10_;
			i_11_ += i_4_;
			i_11_ ^= i_4_ >>> 9;
			i_6_ += i_11_;
			i_4_ += i_5_;
			mix[i_13_] = i_4_;
			mix[i_13_ + 1] = i_5_;
			mix[i_13_ + 2] = i_6_;
			mix[i_13_ + 3] = i_7_;
			mix[i_13_ + 4] = i_8_;
			mix[i_13_ + 5] = i_9_;
			mix[i_13_ + 6] = i_10_;
			mix[i_13_ + 7] = i_11_;
		}
		for (int i_14_ = 0; i_14_ < 256; i_14_ += 8) {
			i_4_ += mix[i_14_];
			i_10_ += mix[i_14_ + 6];
			i_6_ += mix[i_14_ + 2];
			i_7_ += mix[i_14_ + 3];
			i_11_ += mix[i_14_ + 7];
			i_5_ += mix[i_14_ + 1];
			i_9_ += mix[i_14_ + 5];
			i_8_ += mix[i_14_ + 4];
			i_4_ ^= i_5_ << 11;
			i_5_ += i_6_;
			i_7_ += i_4_;
			i_5_ ^= i_6_ >>> 2;
			i_6_ += i_7_;
			i_8_ += i_5_;
			i_6_ ^= i_7_ << 8;
			i_7_ += i_8_;
			i_9_ += i_6_;
			i_7_ ^= i_8_ >>> 16;
			i_10_ += i_7_;
			i_8_ += i_9_;
			i_8_ ^= i_9_ << 10;
			i_11_ += i_8_;
			i_9_ += i_10_;
			i_9_ ^= i_10_ >>> 4;
			i_10_ += i_11_;
			i_4_ += i_9_;
			i_10_ ^= i_11_ << 8;
			i_11_ += i_4_;
			i_5_ += i_10_;
			i_11_ ^= i_4_ >>> 9;
			i_6_ += i_11_;
			i_4_ += i_5_;
			mix[i_14_] = i_4_;
			mix[i_14_ + 1] = i_5_;
			mix[i_14_ + 2] = i_6_;
			mix[i_14_ + 3] = i_7_;
			mix[i_14_ + 4] = i_8_;
			mix[i_14_ + 5] = i_9_;
			mix[i_14_ + 6] = i_10_;
			mix[i_14_ + 7] = i_11_;
		}
		mix();
		currentSEED = 256;
	}

	public int next() {
		if (!Constants.ENABLE_ISAAC) {
			return 0;
		}
		if (currentSEED == 0) {
			mix();
			currentSEED = 256;
		}
		return seeds[--currentSEED];
	}

	private ISAACCipher() {
		/* empty */
	}

	ISAACCipher(int[] is) {
		seeds = new int[256];
		mix = new int[256];
		for (int i = 0; is.length > i; i++) {
			seeds[i] = is[i];
		}
		randomizeISAAC();
	}
}
