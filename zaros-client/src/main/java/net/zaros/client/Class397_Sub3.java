package net.zaros.client;

import jaggl.OpenGL;

public class Class397_Sub3 extends Sprite {
	private int anInt5791;
	private int anInt5792;
	private boolean aBoolean5793 = false;
	private int anInt5794;
	private int anInt5795;
	private Class69_Sub1_Sub1 aClass69_Sub1_Sub1_5796;
	private ha_Sub3 aHa_Sub3_5797;
	static Class294 aClass294_5798;
	static int anInt5799 = 0;
	private Class69_Sub1_Sub1 aClass69_Sub1_Sub1_5800;
	private int anInt5801;

	public void method4075(float f, float f_0_, float f_1_, float f_2_, float f_3_, float f_4_, int i, int i_5_,
			int i_6_, int i_7_) {
		if (aBoolean5793) {
			float f_8_ = (float) method4099();
			float f_9_ = (float) method4088();
			float f_10_ = (-f + f_1_) / f_8_;
			float f_11_ = (-f_0_ + f_2_) / f_8_;
			float f_12_ = (-f + f_3_) / f_9_;
			float f_13_ = (-f_0_ + f_4_) / f_9_;
			float f_14_ = (float) anInt5795 * f_12_;
			float f_15_ = (float) anInt5795 * f_13_;
			float f_16_ = f_10_ * (float) anInt5794;
			float f_17_ = (float) anInt5794 * f_11_;
			float f_18_ = -f_10_ * (float) anInt5801;
			float f_19_ = -f_11_ * (float) anInt5801;
			float f_20_ = (float) anInt5791 * -f_12_;
			f_0_ = f_0_ + f_17_ + f_15_;
			float f_21_ = (float) anInt5791 * -f_13_;
			f_2_ = f_15_ + (f_19_ + f_2_);
			f_1_ = f_14_ + (f_18_ + f_1_);
			f = f_14_ + (f_16_ + f);
			f_3_ = f_20_ + (f_3_ + f_16_);
			f_4_ = f_21_ + (f_4_ + f_17_);
		}
		float f_22_ = f_1_ - f + f_3_;
		aClass69_Sub1_Sub1_5800.method723(97, (i_7_ & 0x1) != 0);
		float f_23_ = f_2_ + (-f_0_ + f_4_);
		aHa_Sub3_5797.method1305((byte) 102);
		aHa_Sub3_5797.method1316(aClass69_Sub1_Sub1_5800, (byte) -122);
		aHa_Sub3_5797.method1336((byte) 106, i_6_);
		aHa_Sub3_5797.method1272((byte) -107, i);
		OpenGL.glColor4ub((byte) (i_5_ >> 16), (byte) (i_5_ >> 8), (byte) i_5_, (byte) (i_5_ >> 24));
		OpenGL.glBegin(7);
		OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
		OpenGL.glVertex2f(f, f_0_);
		OpenGL.glTexCoord2f(0.0F, 0.0F);
		OpenGL.glVertex2f(f_3_, f_4_);
		OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
		OpenGL.glVertex2f(f_22_, f_23_);
		OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
		OpenGL.glVertex2f(f_1_, f_2_);
		OpenGL.glEnd();
	}

	private void method4107(byte i) {
		aHa_Sub3_5797.method1330(116, 1);
		aHa_Sub3_5797.method1316(null, (byte) -112);
		aHa_Sub3_5797.method1306(8448, 8448, -22394);
		aHa_Sub3_5797.method1283(1, 34168, 768, (byte) -115);
		aHa_Sub3_5797.method1346(true, 770, 0, 5890);
		aHa_Sub3_5797.method1330(114, 0);
		if (i >= 120)
			aHa_Sub3_5797.method1283(1, 34168, 768, (byte) -111);
	}

	public int method4088() {
		return anInt5791 + (aClass69_Sub1_Sub1_5800.anInt6688 + anInt5795);
	}

	public void method4079(int i, int i_24_, int i_25_, int i_26_, int i_27_) {
		aClass69_Sub1_Sub1_5800.method723(127, false);
		aHa_Sub3_5797.method1305((byte) -126);
		aHa_Sub3_5797.method1336((byte) 20, i_27_);
		OpenGL.glColor4ub((byte) (i_26_ >> 16), (byte) (i_26_ >> 8), (byte) i_26_, (byte) (i_26_ >> 24));
		i_24_ += anInt5795;
		i += anInt5794;
		if (aClass69_Sub1_Sub1_5796 == null) {
			aHa_Sub3_5797.method1316(aClass69_Sub1_Sub1_5800, (byte) -108);
			aHa_Sub3_5797.method1272((byte) -107, i_25_);
			OpenGL.glBegin(7);
			OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
			OpenGL.glVertex2i(i, i_24_);
			OpenGL.glTexCoord2f(0.0F, 0.0F);
			OpenGL.glVertex2i(i, aClass69_Sub1_Sub1_5800.anInt6688 + i_24_);
			OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
			OpenGL.glVertex2i(aClass69_Sub1_Sub1_5800.anInt6683 + i, aClass69_Sub1_Sub1_5800.anInt6688 + i_24_);
			OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
			OpenGL.glVertex2i(i + aClass69_Sub1_Sub1_5800.anInt6683, i_24_);
			OpenGL.glEnd();
		} else {
			method4111((byte) 52, i_25_);
			aClass69_Sub1_Sub1_5796.method723(88, false);
			OpenGL.glBegin(7);
			OpenGL.glMultiTexCoord2f(33985, 0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
			OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
			OpenGL.glVertex2i(i, i_24_);
			OpenGL.glMultiTexCoord2f(33985, 0.0F, 0.0F);
			OpenGL.glTexCoord2f(0.0F, 0.0F);
			OpenGL.glVertex2i(i, aClass69_Sub1_Sub1_5800.anInt6688 + i_24_);
			OpenGL.glMultiTexCoord2f(33985, aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
			OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
			OpenGL.glVertex2i(aClass69_Sub1_Sub1_5800.anInt6683 + i, i_24_ + aClass69_Sub1_Sub1_5800.anInt6688);
			OpenGL.glMultiTexCoord2f(33985, aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
			OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
			OpenGL.glVertex2i(i + aClass69_Sub1_Sub1_5800.anInt6683, i_24_);
			OpenGL.glEnd();
			method4107((byte) 124);
		}
	}

	public void method4094(int i, int i_28_, int i_29_, int i_30_, int i_31_, int i_32_, int i_33_, int i_34_) {
		aClass69_Sub1_Sub1_5800.method723(90, (i_34_ & 0x1) != 0);
		aHa_Sub3_5797.method1305((byte) -118);
		aHa_Sub3_5797.method1336((byte) -118, i_33_);
		OpenGL.glColor4ub((byte) (i_32_ >> 16), (byte) (i_32_ >> 8), (byte) i_32_, (byte) (i_32_ >> 24));
		if (!aBoolean5793) {
			if (aClass69_Sub1_Sub1_5796 == null) {
				aHa_Sub3_5797.method1316(aClass69_Sub1_Sub1_5800, (byte) -102);
				aHa_Sub3_5797.method1272((byte) -107, i_31_);
				OpenGL.glBegin(7);
				OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glVertex2i(i, i_28_);
				OpenGL.glTexCoord2f(0.0F, 0.0F);
				OpenGL.glVertex2i(i, i_28_ + i_30_);
				OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
				OpenGL.glVertex2i(i + i_29_, i_28_ + i_30_);
				OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glVertex2i(i + i_29_, i_28_);
				OpenGL.glEnd();
			} else {
				method4111((byte) 52, i_31_);
				aClass69_Sub1_Sub1_5796.method723(77, true);
				OpenGL.glBegin(7);
				OpenGL.glMultiTexCoord2f(33985, 0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glVertex2i(i, i_28_);
				OpenGL.glMultiTexCoord2f(33985, 0.0F, 0.0F);
				OpenGL.glTexCoord2f(0.0F, 0.0F);
				OpenGL.glVertex2i(i, i_28_ + i_30_);
				OpenGL.glMultiTexCoord2f(33985, aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
				OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
				OpenGL.glVertex2i(i_29_ + i, i_30_ + i_28_);
				OpenGL.glMultiTexCoord2f(33985, aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glVertex2i(i_29_ + i, i_28_);
				OpenGL.glEnd();
				method4107((byte) 122);
			}
		} else {
			float f = (float) i_29_ / (float) method4099();
			float f_35_ = (float) i_30_ / (float) method4088();
			float f_36_ = f * (float) anInt5794 + (float) i;
			float f_37_ = f_35_ * (float) anInt5795 + (float) i_28_;
			float f_38_ = (float) aClass69_Sub1_Sub1_5800.anInt6683 * f + f_36_;
			float f_39_ = (float) aClass69_Sub1_Sub1_5800.anInt6688 * f_35_ + f_37_;
			if (aClass69_Sub1_Sub1_5796 != null) {
				method4111((byte) 52, i_31_);
				aClass69_Sub1_Sub1_5796.method723(106, true);
				OpenGL.glBegin(7);
				OpenGL.glMultiTexCoord2f(33985, 0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glVertex2f(f_36_, f_37_);
				OpenGL.glMultiTexCoord2f(33985, 0.0F, 0.0F);
				OpenGL.glTexCoord2f(0.0F, 0.0F);
				OpenGL.glVertex2f(f_36_, f_39_);
				OpenGL.glMultiTexCoord2f(33985, aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
				OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
				OpenGL.glVertex2f(f_38_, f_39_);
				OpenGL.glMultiTexCoord2f(33985, aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glVertex2f(f_38_, f_37_);
				OpenGL.glEnd();
				method4107((byte) 121);
			} else {
				aHa_Sub3_5797.method1316(aClass69_Sub1_Sub1_5800, (byte) -107);
				aHa_Sub3_5797.method1272((byte) -107, i_31_);
				OpenGL.glBegin(7);
				OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glVertex2f(f_36_, f_37_);
				OpenGL.glTexCoord2f(0.0F, 0.0F);
				OpenGL.glVertex2f(f_36_, f_39_);
				OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
				OpenGL.glVertex2f(f_38_, f_39_);
				OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
				OpenGL.glVertex2f(f_38_, f_37_);
				OpenGL.glEnd();
			}
		}
	}

	public int method4087() {
		return aClass69_Sub1_Sub1_5800.anInt6683;
	}

	private void method4108(int i, int i_40_, int i_41_, int i_42_, int[] is, int i_43_, int i_44_) {
		aClass69_Sub1_Sub1_5800.method730(0, i_40_, i, i_41_, true, is, i_44_, i_42_, i_43_);
	}

	public static void method4109(int i) {
		aClass294_5798 = null;
		if (i != 0)
			method4110((byte) 76, -29, null);
	}

	public int method4092() {
		return aClass69_Sub1_Sub1_5800.anInt6688;
	}

	public void method4097(int i, int i_45_, int i_46_, int i_47_) {
		anInt5791 = i_47_;
		anInt5801 = i_46_;
		anInt5794 = i;
		anInt5795 = i_45_;
		aBoolean5793 = (anInt5794 != 0 || anInt5795 != 0 || anInt5801 != 0 || anInt5791 != 0);
	}

	public void method4076(int[] is) {
		is[3] = anInt5791;
		is[1] = anInt5795;
		is[2] = anInt5801;
		is[0] = anInt5794;
	}

	public void method4091(float f, float f_48_, float f_49_, float f_50_, float f_51_, float f_52_, int i, aa var_aa,
			int i_53_, int i_54_) {
		Class69_Sub1_Sub1 class69_sub1_sub1 = ((aa_Sub1) var_aa).aClass69_Sub1_Sub1_3720;
		if (aBoolean5793) {
			float f_55_ = (float) method4099();
			float f_56_ = (float) method4088();
			float f_57_ = (-f + f_49_) / f_55_;
			float f_58_ = (-f_48_ + f_50_) / f_55_;
			float f_59_ = (f_51_ - f) / f_56_;
			float f_60_ = (f_52_ - f_48_) / f_56_;
			float f_61_ = (float) anInt5795 * f_59_;
			float f_62_ = f_60_ * (float) anInt5795;
			float f_63_ = f_57_ * (float) anInt5794;
			float f_64_ = (float) anInt5794 * f_58_;
			float f_65_ = -f_57_ * (float) anInt5801;
			float f_66_ = -f_58_ * (float) anInt5801;
			float f_67_ = (float) anInt5791 * -f_59_;
			f_50_ = f_62_ + (f_50_ + f_66_);
			f = f + f_63_ + f_61_;
			f_51_ = f_51_ + f_63_ + f_67_;
			f_49_ = f_61_ + (f_49_ + f_65_);
			float f_68_ = -f_60_ * (float) anInt5791;
			f_48_ = f_64_ + f_48_ + f_62_;
			f_52_ = f_52_ + f_64_ + f_68_;
		}
		float f_69_ = f_51_ + (-f + f_49_);
		aClass69_Sub1_Sub1_5800.method723(79, (i & 0x1) != 0);
		float f_70_ = f_50_ + (f_52_ - f_48_);
		aHa_Sub3_5797.method1305((byte) -115);
		aHa_Sub3_5797.method1316(aClass69_Sub1_Sub1_5800, (byte) -116);
		aHa_Sub3_5797.method1272((byte) -107, 1);
		aHa_Sub3_5797.method1330(115, 1);
		aHa_Sub3_5797.method1316(class69_sub1_sub1, (byte) -118);
		aHa_Sub3_5797.method1306(7681, 8448, -22394);
		aHa_Sub3_5797.method1283(0, 34168, 768, (byte) -118);
		aHa_Sub3_5797.method1336((byte) -89, 1);
		float f_71_ = (class69_sub1_sub1.aFloat6687 / (float) class69_sub1_sub1.anInt6683);
		float f_72_ = (class69_sub1_sub1.aFloat6684 / (float) class69_sub1_sub1.anInt6688);
		OpenGL.glBegin(7);
		OpenGL.glColor3f(1.0F, 1.0F, 1.0F);
		OpenGL.glMultiTexCoord2f(33984, 0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
		OpenGL.glMultiTexCoord2f(33985, f_71_ * (f - (float) i_53_),
				(-(f_72_ * ((float) -i_54_ + f_48_)) + class69_sub1_sub1.aFloat6684));
		OpenGL.glVertex2f(f, f_48_);
		OpenGL.glMultiTexCoord2f(33984, 0.0F, 0.0F);
		OpenGL.glMultiTexCoord2f(33985, (f_51_ - (float) i_53_) * f_71_,
				(class69_sub1_sub1.aFloat6684 - (f_52_ - (float) i_54_) * f_72_));
		OpenGL.glVertex2f(f_51_, f_52_);
		OpenGL.glMultiTexCoord2f(33984, aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
		OpenGL.glMultiTexCoord2f(33985, (f_69_ - (float) i_53_) * f_71_,
				(-((f_70_ - (float) i_54_) * f_72_) + class69_sub1_sub1.aFloat6684));
		OpenGL.glVertex2f(f_69_, f_70_);
		OpenGL.glMultiTexCoord2f(33984, aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
		OpenGL.glMultiTexCoord2f(33985, f_71_ * ((float) -i_53_ + f_49_),
				(class69_sub1_sub1.aFloat6684 - f_72_ * ((float) -i_54_ + f_50_)));
		OpenGL.glVertex2f(f_49_, f_50_);
		OpenGL.glEnd();
		aHa_Sub3_5797.method1283(0, 5890, 768, (byte) -113);
		aHa_Sub3_5797.method1272((byte) -107, 0);
		aHa_Sub3_5797.method1316(null, (byte) -102);
		aHa_Sub3_5797.method1330(124, 0);
	}

	public void method4093(int i, int i_73_, aa var_aa, int i_74_, int i_75_) {
		aa_Sub1 var_aa_Sub1 = (aa_Sub1) var_aa;
		Class69_Sub1_Sub1 class69_sub1_sub1 = var_aa_Sub1.aClass69_Sub1_Sub1_3720;
		aClass69_Sub1_Sub1_5800.method723(85, false);
		aHa_Sub3_5797.method1305((byte) 124);
		aHa_Sub3_5797.method1316(aClass69_Sub1_Sub1_5800, (byte) -104);
		aHa_Sub3_5797.method1272((byte) -107, 1);
		aHa_Sub3_5797.method1330(109, 1);
		aHa_Sub3_5797.method1316(class69_sub1_sub1, (byte) -107);
		aHa_Sub3_5797.method1306(7681, 8448, -22394);
		aHa_Sub3_5797.method1283(0, 34168, 768, (byte) -120);
		aHa_Sub3_5797.method1336((byte) -112, 1);
		i += anInt5794;
		i_73_ += anInt5795;
		int i_76_ = i + aClass69_Sub1_Sub1_5800.anInt6683;
		int i_77_ = i_73_ + aClass69_Sub1_Sub1_5800.anInt6688;
		float f = (class69_sub1_sub1.aFloat6687 / (float) class69_sub1_sub1.anInt6683);
		float f_78_ = (class69_sub1_sub1.aFloat6684 / (float) class69_sub1_sub1.anInt6688);
		float f_79_ = f * (float) (i - i_74_);
		float f_80_ = (float) (-i_74_ + i_76_) * f;
		float f_81_ = class69_sub1_sub1.aFloat6684 - (float) (-i_75_ + i_73_) * f_78_;
		float f_82_ = (-((float) (i_77_ - i_75_) * f_78_) + class69_sub1_sub1.aFloat6684);
		OpenGL.glBegin(7);
		OpenGL.glColor3f(1.0F, 1.0F, 1.0F);
		OpenGL.glMultiTexCoord2f(33984, 0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
		OpenGL.glMultiTexCoord2f(33985, f_79_, f_81_);
		OpenGL.glVertex2i(i, i_73_);
		OpenGL.glMultiTexCoord2f(33984, 0.0F, 0.0F);
		OpenGL.glMultiTexCoord2f(33985, f_79_, f_82_);
		OpenGL.glVertex2i(i, i_73_ + aClass69_Sub1_Sub1_5800.anInt6688);
		OpenGL.glMultiTexCoord2f(33984, aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
		OpenGL.glMultiTexCoord2f(33985, f_80_, f_82_);
		OpenGL.glVertex2i(aClass69_Sub1_Sub1_5800.anInt6683 + i, aClass69_Sub1_Sub1_5800.anInt6688 + i_73_);
		OpenGL.glMultiTexCoord2f(33984, aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
		OpenGL.glMultiTexCoord2f(33985, f_80_, f_81_);
		OpenGL.glVertex2i(aClass69_Sub1_Sub1_5800.anInt6683 + i, i_73_);
		OpenGL.glEnd();
		aHa_Sub3_5797.method1283(0, 5890, 768, (byte) -122);
		aHa_Sub3_5797.method1272((byte) -107, 0);
		aHa_Sub3_5797.method1316(null, (byte) -124);
		aHa_Sub3_5797.method1330(123, 0);
	}

	static public void method4110(byte i, int i_83_, Mobile class338_sub3_sub1_sub3) {
		if (i != 1)
			method4109(68);
		if (class338_sub3_sub1_sub3.anIntArray6789 != null) {
			int i_84_ = class338_sub3_sub1_sub3.anIntArray6789[i_83_ + 1];
			if (i_84_ != class338_sub3_sub1_sub3.aClass44_6802.method557((byte) -70)) {
				class338_sub3_sub1_sub3.aClass44_6802.method571(class338_sub3_sub1_sub3.aClass44_6802.method560(true),
						-128, i_84_);
				class338_sub3_sub1_sub3.anInt6829 = class338_sub3_sub1_sub3.currentWayPoint;
			}
		}
	}

	public void method4090(int i, int i_85_, int i_86_, int i_87_, int i_88_, int i_89_) {
		if (aHa_Sub3_5797.aBoolean4213) {
			int[] is = aHa_Sub3_5797.na(i_88_, i_89_, i_86_, i_87_);
			if (is != null) {
				for (int i_90_ = 0; is.length > i_90_; i_90_++)
					is[i_90_] = Class48.bitOR(is[i_90_], -16777216);
				method4108(i, i_85_, i_86_, i_87_, is, 0, i_86_);
			}
		} else
			aClass69_Sub1_Sub1_5800.method728(i, i_86_, i_89_, i_87_, i_88_, i_85_, (byte) 71);
	}

	private void method4111(byte i, int i_91_) {
		aHa_Sub3_5797.method1330(107, 1);
		aHa_Sub3_5797.method1316(aClass69_Sub1_Sub1_5800, (byte) -101);
		if (i == 52) {
			aHa_Sub3_5797.method1306(aHa_Sub3_5797.method1291(-113, i_91_), 7681, -22394);
			aHa_Sub3_5797.method1283(1, 34167, 768, (byte) -113);
			aHa_Sub3_5797.method1346(true, 770, 0, 34168);
			aHa_Sub3_5797.method1330(121, 0);
			aHa_Sub3_5797.method1316(aClass69_Sub1_Sub1_5796, (byte) -112);
			aHa_Sub3_5797.method1306(34479, 7681, i - 22446);
			aHa_Sub3_5797.method1283(1, 34166, 768, (byte) -114);
			if (anInt5792 != 0) {
				if (anInt5792 == 1)
					aHa_Sub3_5797.method1275(1.0F, (byte) 81, 0.5F, 0.0F, 0.5F);
				else if (anInt5792 != 2) {
					if (anInt5792 == 3)
						aHa_Sub3_5797.method1275(128.5F, (byte) 86, 128.5F, 0.0F, 128.5F);
				} else
					aHa_Sub3_5797.method1275(0.5F, (byte) 80, 0.5F, 0.0F, 1.0F);
			} else
				aHa_Sub3_5797.method1275(0.5F, (byte) 115, 1.0F, 0.0F, 0.5F);
		}
	}

	public void method4098(int i, int i_92_, int i_93_, int i_94_, int i_95_, int i_96_, int i_97_) {
		int i_98_ = i + i_93_;
		aClass69_Sub1_Sub1_5800.method723(104, false);
		int i_99_ = i_92_ + i_94_;
		aHa_Sub3_5797.method1305((byte) 95);
		aHa_Sub3_5797.method1316(aClass69_Sub1_Sub1_5800, (byte) -124);
		aHa_Sub3_5797.method1336((byte) -127, i_97_);
		aHa_Sub3_5797.method1272((byte) -107, i_95_);
		OpenGL.glColor4ub((byte) (i_96_ >> 16), (byte) (i_96_ >> 8), (byte) i_96_, (byte) (i_96_ >> 24));
		if (aClass69_Sub1_Sub1_5800.aBoolean6685 && !aBoolean5793) {
			float f = (aClass69_Sub1_Sub1_5800.aFloat6684 * (float) i_94_ / (float) aClass69_Sub1_Sub1_5800.anInt6688);
			float f_100_ = ((float) i_93_ * aClass69_Sub1_Sub1_5800.aFloat6687
					/ (float) aClass69_Sub1_Sub1_5800.anInt6683);
			OpenGL.glBegin(7);
			OpenGL.glTexCoord2f(0.0F, f);
			OpenGL.glVertex2i(i, i_92_);
			OpenGL.glTexCoord2f(0.0F, 0.0F);
			OpenGL.glVertex2i(i, i_99_);
			OpenGL.glTexCoord2f(f_100_, 0.0F);
			OpenGL.glVertex2i(i_98_, i_99_);
			OpenGL.glTexCoord2f(f_100_, f);
			OpenGL.glVertex2i(i_98_, i_92_);
			OpenGL.glEnd();
		} else {
			OpenGL.glPushMatrix();
			OpenGL.glTranslatef((float) anInt5794, (float) anInt5795, 0.0F);
			int i_101_ = method4099();
			int i_102_ = method4088();
			int i_103_ = aClass69_Sub1_Sub1_5800.anInt6688 + i_92_;
			OpenGL.glBegin(7);
			int i_104_ = i_92_;
			while (i_99_ >= i_103_) {
				int i_105_ = aClass69_Sub1_Sub1_5800.anInt6683 + i;
				int i_106_ = i;
				for (/**/; i_105_ <= i_98_; i_105_ += i_101_) {
					OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
					OpenGL.glVertex2i(i_106_, i_104_);
					OpenGL.glTexCoord2f(0.0F, 0.0F);
					OpenGL.glVertex2i(i_106_, i_103_);
					OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, 0.0F);
					OpenGL.glVertex2i(i_105_, i_103_);
					OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
					OpenGL.glVertex2i(i_105_, i_104_);
					i_106_ += i_101_;
				}
				if (i_98_ > i_106_) {
					float f = ((float) (i_98_ - i_106_) * aClass69_Sub1_Sub1_5800.aFloat6687
							/ (float) aClass69_Sub1_Sub1_5800.anInt6683);
					OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
					OpenGL.glVertex2i(i_106_, i_104_);
					OpenGL.glTexCoord2f(0.0F, 0.0F);
					OpenGL.glVertex2i(i_106_, i_103_);
					OpenGL.glTexCoord2f(f, 0.0F);
					OpenGL.glVertex2i(i_98_, i_103_);
					OpenGL.glTexCoord2f(f, aClass69_Sub1_Sub1_5800.aFloat6684);
					OpenGL.glVertex2i(i_98_, i_104_);
				}
				i_103_ += i_102_;
				i_104_ += i_102_;
			}
			if (i_104_ < i_99_) {
				float f = (aClass69_Sub1_Sub1_5800.aFloat6684
						* (float) (aClass69_Sub1_Sub1_5800.anInt6688 + i_104_ - i_99_)
						/ (float) aClass69_Sub1_Sub1_5800.anInt6688);
				int i_107_ = aClass69_Sub1_Sub1_5800.anInt6683 + i;
				int i_108_ = i;
				while (i_98_ >= i_107_) {
					OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
					OpenGL.glVertex2i(i_108_, i_104_);
					OpenGL.glTexCoord2f(0.0F, f);
					OpenGL.glVertex2i(i_108_, i_99_);
					OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, f);
					OpenGL.glVertex2i(i_107_, i_99_);
					OpenGL.glTexCoord2f(aClass69_Sub1_Sub1_5800.aFloat6687, aClass69_Sub1_Sub1_5800.aFloat6684);
					OpenGL.glVertex2i(i_107_, i_104_);
					i_107_ += i_101_;
					i_108_ += i_101_;
				}
				if (i_108_ < i_98_) {
					float f_109_ = (aClass69_Sub1_Sub1_5800.aFloat6687 * (float) (i_98_ - i_108_)
							/ (float) aClass69_Sub1_Sub1_5800.anInt6683);
					OpenGL.glTexCoord2f(0.0F, aClass69_Sub1_Sub1_5800.aFloat6684);
					OpenGL.glVertex2i(i_108_, i_104_);
					OpenGL.glTexCoord2f(0.0F, f);
					OpenGL.glVertex2i(i_108_, i_99_);
					OpenGL.glTexCoord2f(f_109_, f);
					OpenGL.glVertex2i(i_98_, i_99_);
					OpenGL.glTexCoord2f(f_109_, aClass69_Sub1_Sub1_5800.aFloat6684);
					OpenGL.glVertex2i(i_98_, i_104_);
				}
			}
			OpenGL.glEnd();
			OpenGL.glPopMatrix();
		}
	}

	public void method4084(int i, int i_110_, int i_111_) {
		OpenGL.glPixelTransferf(3348, 0.5F);
		OpenGL.glPixelTransferf(3349, 0.499F);
		OpenGL.glPixelTransferf(3352, 0.5F);
		OpenGL.glPixelTransferf(3353, 0.499F);
		OpenGL.glPixelTransferf(3354, 0.5F);
		OpenGL.glPixelTransferf(3355, 0.499F);
		aClass69_Sub1_Sub1_5796 = Class296_Sub39_Sub1.method2782(aHa_Sub3_5797, true, aClass69_Sub1_Sub1_5800.anInt6688,
				i, i_110_, (aClass69_Sub1_Sub1_5800.anInt6683));
		anInt5792 = i_111_;
		OpenGL.glPixelTransferf(3348, 1.0F);
		OpenGL.glPixelTransferf(3349, 0.0F);
		OpenGL.glPixelTransferf(3352, 1.0F);
		OpenGL.glPixelTransferf(3353, 0.0F);
		OpenGL.glPixelTransferf(3354, 1.0F);
		OpenGL.glPixelTransferf(3355, 0.0F);
	}

	Class397_Sub3(ha_Sub3 var_ha_Sub3, int i, int i_112_, boolean bool) {
		anInt5791 = 0;
		anInt5792 = 0;
		anInt5795 = 0;
		anInt5794 = 0;
		anInt5801 = 0;
		aHa_Sub3_5797 = var_ha_Sub3;
		aClass69_Sub1_Sub1_5800 = Class127.method1357(var_ha_Sub3, (byte) 122, i, i_112_, !bool ? 6407 : 6408);
	}

	Class397_Sub3(ha_Sub3 var_ha_Sub3, int i, int i_113_, int i_114_, int i_115_) {
		anInt5791 = 0;
		anInt5792 = 0;
		anInt5795 = 0;
		anInt5794 = 0;
		anInt5801 = 0;
		aHa_Sub3_5797 = var_ha_Sub3;
		aClass69_Sub1_Sub1_5800 = Class296_Sub39_Sub1.method2782(var_ha_Sub3, true, i_115_, i, i_113_, i_114_);
	}

	public int method4099() {
		return anInt5794 + (aClass69_Sub1_Sub1_5800.anInt6683 + anInt5801);
	}

	Class397_Sub3(ha_Sub3 var_ha_Sub3, int i, int i_116_, int[] is, int i_117_, int i_118_) {
		anInt5791 = 0;
		anInt5792 = 0;
		anInt5795 = 0;
		anInt5794 = 0;
		anInt5801 = 0;
		aHa_Sub3_5797 = var_ha_Sub3;
		aClass69_Sub1_Sub1_5800 = Class187.method1885(is, false, i_117_, var_ha_Sub3, i_118_, (byte) -27, i_116_, i);
	}

	static {
		aClass294_5798 = new Class294(5, 1);
	}
}
