package net.zaros.client;
import jaggl.OpenGL;

final class Class205_Sub2 extends Class205 implements Interface6_Impl3 {
	static OutgoingPacket aClass311_5628 = new OutgoingPacket(14, 3);
	static int updatableNpcsCount = 0;
	static int anInt5630;
	static Js5 aClass138_5631;
	static Js5 fs8;

	Class205_Sub2(ha_Sub1_Sub1 var_ha_Sub1_Sub1, int i, boolean bool, int[][] is) {
		super(var_ha_Sub1_Sub1, 34067, za_Sub2.aClass202_6555, Class67.aClass67_745, i * (i * 6), bool);
		aHa_Sub1_Sub1_3508.method1140(this, false);
		if (!bool) {
			for (int i_0_ = 0; i_0_ < 6; i_0_++)
				OpenGL.glTexImage2Di(i_0_ + 34069, 0, this.method1971(113), i, i, 0, BITConfigDefinition.method2350(6409, aClass202_3514), aHa_Sub1_Sub1_3508.anInt5852, is[i_0_], 0);
		} else {
			for (int i_1_ = 0; i_1_ < 6; i_1_++)
				this.method1972(i_1_ + 34069, is[i_1_], i, (byte) -94, i);
		}
	}

	public static void method1981(int i) {
		fs8 = null;
		aClass138_5631 = null;
		aClass311_5628 = null;
		if (i != -7450)
			method1981(-19);
	}

	static final boolean method1982(int i, int i_2_, int i_3_) {
		if (i != 544)
			method1981(88);
		return (i_3_ & 0x220) == 544 | (i_3_ & 0x18) != 0;
	}
}
