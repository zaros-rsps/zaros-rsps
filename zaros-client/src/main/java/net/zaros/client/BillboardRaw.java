package net.zaros.client;

public class BillboardRaw {
	public static boolean aBoolean1071 = false;
	public int anInt1072;
	public int anInt1073;
	public static short aShort1074 = 1;
	public static IncomingPacket aClass231_1075 = new IncomingPacket(139, 7);
	public int anInt1076 = 2;
	public boolean aBoolean1077;
	public int texture_id;
	public int anInt1079;
	public static int[] anIntArray1080;
	public static Js5 interfacesFS;
	public boolean aBoolean1082;

	public static void method881(int i) {
		if (i <= 126) {
			method883((byte) 120);
		}
		aClass231_1075 = null;
		anIntArray1080 = null;
		interfacesFS = null;
	}

	static final void method882(int i, byte i_0_, int i_1_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask(i_1_ | (long) i << 32, 19);
		if (i_0_ > -37) {
			method881(-79);
		}
		class296_sub39_sub5.insertIntoQueue();
	}

	static final void method883(byte i) {
		for (int i_2_ = 0; i_2_ < 5; i_2_++) {
			Class296_Sub51_Sub13.aBooleanArray6414[i_2_] = false;
		}
		Class366_Sub2.lookatVelocity = 0;
		Class361.anInt3103 = 1;
		Class42_Sub3.anInt3844 = -1;
		int i_3_ = 23 / ((-58 - i) / 59);
		Class368_Sub11.anInt5486 = Class242.anInt2309 = -1;
		StaticMethods.lookatSpeed = 0;
		ClotchesLoader.anInt279 = -1;
	}

	static final void method884(int i, byte i_4_, InterfaceComponent class51, ha var_ha, int i_5_, int i_6_) {
		if (i_4_ < 126) {
			aClass231_1075 = null;
		}
		for (int i_7_ = 7; i_7_ >= 0; i_7_--) {
			for (int i_8_ = 127; i_8_ >= 0; i_8_--) {
				int i_9_ = i_7_ << 7 & 0x380 | i_6_ << 10 & 0xfc00 | i_8_ & 0x7f;
				Class338.method3437(true, false, false);
				int i_10_ = Class166_Sub1.anIntArray4300[i_9_];
				Class355_Sub1.method3702((byte) 78, true, false);
				var_ha.aa((class51.anInt578 * i_8_ >> 7) + i_5_, (class51.anInt623 * (7 - i_7_) >> 3) + i,
						(class51.anInt578 >> 7) + 1, (class51.anInt623 >> 3) + 1, i_10_, 0);
			}
		}
	}

	private final void method885(int i, Packet bufer, int i_11_, int i_12_) {
		if (i_11_ != 1) {
			if (i_11_ == 2) {
				anInt1072 = bufer.g2() + 1;
				anInt1073 = bufer.g2() + 1;
			} else if (i_11_ != 3) {
				if (i_11_ != 4) {
					if (i_11_ == 5) {
						anInt1079 = bufer.g1();
					} else if (i_11_ != 6) {
						if (i_11_ == 7) {
							aBoolean1077 = true;
						}
					} else {
						aBoolean1082 = true;
					}
				} else {
					anInt1076 = bufer.g1();
				}
			} else {
				bufer.g1b();
			}
		} else {
			texture_id = bufer.g2();
			if (texture_id == 65535) {
				texture_id = -1;
			}
		}
	}

	static final int method886(int i, int i_13_, boolean bool) {
		if (bool != true) {
			method883((byte) 100);
		}
		int i_14_ = Class296_Sub43.method2921(i - 1, (byte) -11, i_13_ - 1)
				+ Class296_Sub43.method2921(i + 1, (byte) -11, i_13_ - 1)
				+ Class296_Sub43.method2921(i - 1, (byte) -11, i_13_ + 1)
				+ Class296_Sub43.method2921(i + 1, (byte) -11, i_13_ + 1);
		int i_15_ = Class296_Sub43.method2921(i - 1, (byte) -11, i_13_)
				+ Class296_Sub43.method2921(i + 1, (byte) -11, i_13_)
				+ Class296_Sub43.method2921(i, (byte) -11, i_13_ - 1)
				+ Class296_Sub43.method2921(i, (byte) -11, i_13_ + 1);
		int i_16_ = Class296_Sub43.method2921(i, (byte) -11, i_13_);
		return i_16_ / 4 + i_14_ / 16 + i_15_ / 8;
	}

	static final boolean interfaceCounter() {
		Class296_Sub45_Sub2.interfaceCounter++;
		TranslatableString.aBoolean1261 = true;
		return true;
	}

	final void method888(byte i, Packet class296_sub17, int i_17_) {
		if (i > -29) {
			method884(32, (byte) -25, null, null, 15, -108);
		}
		for (;;) {
			int i_18_ = class296_sub17.g1();
			if (i_18_ == 0) {
				break;
			}
			method885(-1135, class296_sub17, i_18_, i_17_);
		}
	}

	public BillboardRaw() {
		anInt1073 = 64;
		anInt1072 = 64;
		anInt1079 = 1;
		texture_id = -1;
		aBoolean1082 = false;
		aBoolean1077 = false;
	}
}
