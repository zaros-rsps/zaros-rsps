package net.zaros.client;
import jaggl.OpenGL;

final class Class69_Sub3 extends Class69 {
	static Sprite aClass397_5722;
	static int anInt5723 = -1;
	private int anInt5724 = -1;
	int anInt5725;
	static double aDouble5726;
	private int anInt5727 = -1;
	static int anInt5728 = 0;
	static int anInt5729;

	public static void method741(int i) {
		if (i != -6390)
			method741(-64);
		aClass397_5722 = null;
	}

	Class69_Sub3(ha_Sub3 var_ha_Sub3, int i, int i_0_, boolean bool, byte[][] is, int i_1_) {
		super(var_ha_Sub3, 34067, i, i_0_ * i_0_ * 6, bool);
		anInt5725 = i_0_;
		aHa_Sub3_3681.method1316(this, (byte) -128);
		for (int i_2_ = 0; i_2_ < 6; i_2_++)
			OpenGL.glTexImage2Dub(34069 + i_2_, 0, anInt3685, i_0_, i_0_, 0, i_1_, 5121, is[i_2_], 0);
		this.method723(116, true);
	}

	Class69_Sub3(ha_Sub3 var_ha_Sub3, int i, int i_3_, boolean bool, int[][] is) {
		super(var_ha_Sub3, 34067, i, i_3_ * i_3_ * 6, bool);
		anInt5725 = i_3_;
		aHa_Sub3_3681.method1316(this, (byte) -105);
		if (bool) {
			for (int i_4_ = 0; i_4_ < 6; i_4_++)
				Class404.method4165(i_3_, aHa_Sub3_3681.anInt4231, i_3_, 32993, 0, i_4_ + 34069, is[i_4_], anInt3685);
		} else {
			for (int i_5_ = 0; i_5_ < 6; i_5_++)
				OpenGL.glTexImage2Di(34069 + i_5_, 0, anInt3685, i_3_, i_3_, 0, 32993, aHa_Sub3_3681.anInt4231, is[i_5_], 0);
		}
		this.method723(127, true);
	}

	final void method742(int i, int i_6_, int i_7_, int i_8_, int i_9_) {
		if (i_8_ != -21641)
			anInt5729 = 66;
		OpenGL.glFramebufferTexture2DEXT(i_9_, i_6_, i, anInt3680, i_7_);
		anInt5727 = i_6_;
		anInt5724 = i_9_;
	}

	Class69_Sub3(ha_Sub3 var_ha_Sub3, int i, int i_10_) {
		super(var_ha_Sub3, 34067, i, i_10_ * (i_10_ * 6), false);
		anInt5725 = i_10_;
		aHa_Sub3_3681.method1316(this, (byte) -116);
		for (int i_11_ = 0; i_11_ < 6; i_11_++)
			OpenGL.glTexImage2Dub(i_11_ + 34069, 0, anInt3685, i_10_, i_10_, 0, Class296_Sub39_Sub18.method2897((byte) 61, anInt3685), 5121, null, 0);
		this.method723(126, true);
	}

	public final void method73(boolean bool) {
		OpenGL.glFramebufferTexture2DEXT(anInt5724, anInt5727, 3553, 0, 0);
		if (bool == true) {
			anInt5724 = -1;
			anInt5727 = -1;
		}
	}
}
