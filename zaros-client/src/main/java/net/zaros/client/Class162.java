package net.zaros.client;

/* Class162 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class162 implements Interface9 {
	private Class140 aClass140_3552;
	static IncomingPacket aClass231_3553;
	private Class358 aClass358_3554;
	static int anInt3555 = 0;
	static Class276 aClass276_3556;
	static int anInt3557;
	static int anInt3558;
	static Class256 aClass256_3559;

	public static void method1623(byte i) {
		if (i != -93)
			method1624(8, 120);
		aClass256_3559 = null;
		aClass231_3553 = null;
		aClass276_3556 = null;
	}

	public final void method42(byte i) {
		if (i != 88)
			method1623((byte) -120);
	}

	public final boolean method44(byte i) {
		if (i < 91)
			aClass140_3552 = null;
		return aClass358_3554.method3714(80);
	}

	static final int method1624(int i, int i_0_) {
		if (Class296_Sub51_Sub32.anIntArrayArray6514 != null)
			return (Class296_Sub51_Sub32.anIntArrayArray6514[i][i_0_] & 0xffffff);
		return 0;
	}

	private final int method1625(int i, int i_1_, int i_2_, Class55 class55, String string, int i_3_) {
		int i_4_ = 70 % ((i_3_ + 82) / 43);
		return class55.a((byte) 110, null, 0, null, aClass140_3552.anInt3584, aClass140_3552.anInt3583 - i_1_ * 2, i_1_ + i_2_, i + i_1_, string, 0, aClass140_3552.anInt3590, -(i_1_ * 2) + aClass140_3552.anInt3589, 0, 0, null, 0);
	}

	static final boolean method1626(byte i, int i_5_, int i_6_) {
		if (i < 56)
			method1624(87, 120);
		if ((i_6_ & 0x37) == 0)
			return false;
		return true;
	}

	public final void method43(boolean bool, int i) {
		int i_7_ = 8 / ((-32 - i) / 52);
		Class4 class4 = aClass358_3554.method3713(aClass140_3552.anInt3588, true);
		if (class4 != null) {
			int i_8_ = (aClass140_3552.aClass252_3582.method2204((aClass140_3552.anInt3583), 23236, (Class368_Sub7.anInt5463)) + aClass140_3552.anInt3586);
			int i_9_ = ((aClass140_3552.aClass357_3581.method3711(Class296_Sub15_Sub1.anInt5996, aClass140_3552.anInt3589, (byte) 116)) + aClass140_3552.anInt3585);
			if (aClass140_3552.aBoolean3579)
				Class41_Sub13.aHa3774.b(i_8_, i_9_, aClass140_3552.anInt3583, aClass140_3552.anInt3589, aClass140_3552.anInt3580, 0);
			i_9_ += method1625(i_8_, 5, i_9_, Class49.aClass55_461, class4.aString73, -128) * 12;
			i_9_ += 8;
			if (aClass140_3552.aBoolean3579)
				Class41_Sub13.aHa3774.e(i_8_, i_9_, i_8_ + (aClass140_3552.anInt3583 - 1), i_9_, aClass140_3552.anInt3580, 0);
			i_9_ = ++i_9_ + method1625(i_8_, 5, i_9_, Class49.aClass55_461, class4.aString74, -125) * 12;
			i_9_ += 5;
			i_9_ += method1625(i_8_, 5, i_9_, Class49.aClass55_461, class4.aString72, -128) * 12;
		}
	}

	Class162(Class358 class358, Class140 class140) {
		aClass358_3554 = class358;
		aClass140_3552 = class140;
	}

	static {
		aClass231_3553 = new IncomingPacket(108, 9);
		anInt3557 = 0;
		anInt3558 = 0;
	}
}
