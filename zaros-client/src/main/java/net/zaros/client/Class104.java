package net.zaros.client;


/* Class104 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class104 {
	static int anInt1088;
	static int loopCycle = 0;

	abstract byte[] method899(int i);

	static final void method900(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_, int i_9_, byte i_10_) {
		if (InterfaceComponent.loadInterface(i_1_)) {
			if (i_10_ != 63)
				anInt1088 = 44;
			if (Class338_Sub10.aClass51ArrayArray5272[i_1_] != null)
				GameClient.method121(Class338_Sub10.aClass51ArrayArray5272[i_1_], -1, i_3_, i_6_, i_4_, i_0_, i_8_, i_2_, i_7_, i, i_5_, i_9_);
			else
				GameClient.method121(Class192.openedInterfaceComponents[i_1_], -1, i_3_, i_6_, i_4_, i_0_, i_8_, i_2_, i_7_, i, i_5_, i_9_);
		}
	}

	abstract byte[] method901(int i, int i_11_, int i_12_);

	abstract void method903(byte[] is, byte i);

	static final void method904(int i, boolean bool, int i_78_, int i_79_, int i_80_) {
		Class41_Sub29.method520(102);
		Class368_Sub5_Sub1.aLong6594 = (long) i_78_;
		int i_81_ = Class8.method192((byte) 101);
		if (i == 3 || i_81_ == 3)
			bool = true;
		if (!Class41_Sub13.aHa3774.j())
			bool = true;
		s.method3353(bool, i_80_, i_79_, -22614, i_81_, i);
	}
}
