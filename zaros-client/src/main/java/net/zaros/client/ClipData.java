package net.zaros.client;
/* Class17 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class ClipData {
	static Class190[][] aClass190ArrayArray184;
	int[][] clip;
	int offsetX;
	int sizeY;
	static int anInt188;
	int sizeX;
	int offsetY;

	final void flagObject(int objX, int objY, int objSizeX, int objSizeY, boolean solid, boolean bool_0_) {
		int flag = 256;
		if (solid)
			flag |= 0x20000;
		objX -= offsetX;
		objY -= offsetY;

		if (bool_0_)
			flag |= 0x40000000;
		for (int x = objX; objX + objSizeX > x; x++) {
			if (x >= 0 && sizeX > x) {
				for (int y = objY; y < objY + objSizeY; y++) {
					if (y >= 0 && y < sizeY)
						flagClip(x, y, flag);
				}
			}
		}
	}

	final void flagUnmovable(int x, int y) {
		x -= offsetX;
		y -= offsetY;
		clip[x][y] = clip[x][y] | 2097152;
	}

	public static void method256(int i) {
		if (i == 27786)
			aClass190ArrayArray184 = null;
	}

	final void flagWall(int x, int y, int wallType, int wallFace, boolean solid, boolean bool_10_) {
		x -= offsetX;
		y -= offsetY;
		if (wallType == 0) {
			if (wallFace == 0) {
				flagClip(x, y, 128);
				flagClip(x - 1, y, 8);
			}
			if (wallFace == 1) {
				flagClip(x, y, 2);
				flagClip(x, y + 1, 32);
			}
			if (wallFace == 2) {
				flagClip(x, y, 8);
				flagClip(x + 1, y, 128);
			}
			if (wallFace == 3) {
				flagClip(x, y, 32);
				flagClip(x, y - 1, 2);
			}
		}
		if (wallType == 1 || wallType == 3) {
			if (wallFace == 0) {
				flagClip(x, y, 1);
				flagClip(x - 1, y + 1, 16);
			}
			if (wallFace == 1) {
				flagClip(x, y, 4);
				flagClip(x + 1, y + 1, 64);
			}
			if (wallFace == 2) {
				flagClip(x, y, 16);
				flagClip(x + 1, y - 1, 1);
			}
			if (wallFace == 3) {
				flagClip(x, y, 64);
				flagClip(x - 1, y - 1, 4);
			}
		}
		if (wallType == 2) {
			if (wallFace == 0) {
				flagClip(x, y, 130);
				flagClip(x - 1, y, 8);
				flagClip(x, y + 1, 32);
			}
			if (wallFace == 1) {
				flagClip(x, y, 10);
				flagClip(x, y + 1, 32);
				flagClip(x + 1, y, 128);
			}
			if (wallFace == 2) {
				flagClip(x, y, 40);
				flagClip(x + 1, y, 128);
				flagClip(x, y - 1, 2);
			}
			if (wallFace == 3) {
				flagClip(x, y, 160);
				flagClip(x, y - 1, 2);
				flagClip(x - 1, y, 8);
			}
		}
		if (bool_10_) {
			if (wallType == 0) {
				if (wallFace == 0) {
					flagClip(x, y, 65536);
					flagClip(x - 1, y, 4096);
				}
				if (wallFace == 1) {
					flagClip(x, y, 1024);
					flagClip(x, y + 1, 16384);
				}
				if (wallFace == 2) {
					flagClip(x, y, 4096);
					flagClip(x + 1, y, 65536);
				}
				if (wallFace == 3) {
					flagClip(x, y, 16384);
					flagClip(x, y - 1, 1024);
				}
			}
			if (wallType == 1 || wallType == 3) {
				if (wallFace == 0) {
					flagClip(x, y, 512);
					flagClip(x - 1, y + 1, 8192);
				}
				if (wallFace == 1) {
					flagClip(x, y, 2048);
					flagClip(x + 1, y + 1, 32768);
				}
				if (wallFace == 2) {
					flagClip(x, y, 8192);
					flagClip(x + 1, y - 1, 512);
				}
				if (wallFace == 3) {
					flagClip(x, y, 32768);
					flagClip(x - 1, y - 1, 2048);
				}
			}
			if (wallType == 2) {
				if (wallFace == 0) {
					flagClip(x, y, 66560);
					flagClip(x - 1, y, 4096);
					flagClip(x, y + 1, 16384);
				}
				if (wallFace == 1) {
					flagClip(x, y, 5120);
					flagClip(x, y + 1, 16384);
					flagClip(x + 1, y, 65536);
				}
				if (wallFace == 2) {
					flagClip(x, y, 20480);
					flagClip(x + 1, y, 65536);
					flagClip(x, y - 1, 1024);
				}
				if (wallFace == 3) {
					flagClip(x, y, 81920);
					flagClip(x, y - 1, 1024);
					flagClip(x - 1, y, 4096);
				}
			}
		}
		if (solid) {
			if (wallType == 0) {
				if (wallFace == 0) {
					flagClip(x, y, 536870912);
					flagClip(x - 1, y, 33554432);
				}
				if (wallFace == 1) {
					flagClip(x, y, 8388608);
					flagClip(x, y + 1, 134217728);
				}
				if (wallFace == 2) {
					flagClip(x, y, 33554432);
					flagClip(x + 1, y, 536870912);
				}
				if (wallFace == 3) {
					flagClip(x, y, 134217728);
					flagClip(x, y - 1, 8388608);
				}
			}
			if (wallType == 1 || wallType == 3) {
				if (wallFace == 0) {
					flagClip(x, y, 4194304);
					flagClip(x - 1, y + 1, 67108864);
				}
				if (wallFace == 1) {
					flagClip(x, y, 16777216);
					flagClip(x + 1, y + 1, 268435456);
				}
				if (wallFace == 2) {
					flagClip(x, y, 67108864);
					flagClip(x + 1, y - 1, 4194304);
				}
				if (wallFace == 3) {
					flagClip(x, y, 268435456);
					flagClip(x - 1, y - 1, 16777216);
				}
			}
			if (wallType == 2) {
				if (wallFace == 0) {
					flagClip(x, y, 545259520);
					flagClip(x - 1, y, 33554432);
					flagClip(x, y + 1, 134217728);
				}
				if (wallFace == 1) {
					flagClip(x, y, 41943040);
					flagClip(x, y + 1, 134217728);
					flagClip(x + 1, y, 536870912);
				}
				if (wallFace == 2) {
					flagClip(x, y, 167772160);
					flagClip(x + 1, y, 536870912);
					flagClip(x, y - 1, 8388608);
				}
				if (wallFace == 3) {
					flagClip(x, y, 671088640);
					flagClip(x, y - 1, 8388608);
					flagClip(x - 1, y, 33554432);
				}
			}
		}
	}

	final boolean checkMove_(int clipType, int toMapX, int toMapY, int currentMapX, int currentMapY, int targetFace, int selfSize) {
		if (selfSize != 1) {
			if (toMapX >= currentMapX && toMapX <= currentMapX + (selfSize - 1) && toMapY <= toMapY && toMapY <= selfSize - 1 + toMapY)
				return true;
		} else if (currentMapX == toMapX && currentMapY == toMapY)
			return true;
		currentMapY -= offsetY;
		currentMapX -= offsetX;
		toMapY -= offsetY;
		toMapX -= offsetX;
		if (selfSize != 1) {
			int someX = selfSize - 1 + currentMapX;
			int someY = selfSize - 1 + currentMapY;
			if (clipType == 0) {
				if (targetFace != 0) {
					if (targetFace == 1) {
						if (currentMapX <= toMapX && toMapX <= someX && toMapY + 1 == currentMapY)
							return true;
						if (-selfSize + toMapX == currentMapX && toMapY >= currentMapY && toMapY <= someY && (clip[someX][toMapY] & 0x2c0108) == 0)
							return true;
						if (currentMapX == toMapX + 1 && currentMapY <= toMapY && someY >= toMapY && (clip[currentMapX][toMapY] & 0x2c0180) == 0)
							return true;
					} else if (targetFace == 2) {
						if (toMapX + 1 == currentMapX && toMapY >= currentMapY && someY >= toMapY)
							return true;
						if (toMapX >= currentMapX && toMapX <= someX && toMapY + 1 == currentMapY && ((clip[toMapX][currentMapY] & 0x2c0120) == 0))
							return true;
						if (toMapX >= currentMapX && someX >= toMapX && -selfSize + toMapY == currentMapY && ((clip[toMapX][someY] & 0x2c0102) == 0))
							return true;
					} else if (targetFace == 3) {
						if (currentMapX <= toMapX && someX >= toMapX && toMapY - selfSize == currentMapY)
							return true;
						if (currentMapX == -selfSize + toMapX && currentMapY <= toMapY && toMapY <= someY && (clip[someX][toMapY] & 0x2c0108) == 0)
							return true;
						if (toMapX + 1 == currentMapX && toMapY >= currentMapY && toMapY <= someY && (clip[currentMapX][toMapY] & 0x2c0180) == 0)
							return true;
					}
				} else {
					if (toMapX - selfSize == currentMapX && currentMapY <= toMapY && someY >= toMapY)
						return true;
					if (currentMapX <= toMapX && toMapX <= someX && currentMapY == toMapY + 1 && (clip[toMapX][currentMapY] & 0x2c0120) == 0)
						return true;
					if (toMapX >= currentMapX && toMapX <= someX && currentMapY == toMapY - selfSize && (clip[toMapX][someY] & 0x2c0102) == 0)
						return true;
				}
			}
			if (clipType == 2) {
				if (targetFace != 0) {
					if (targetFace != 1) {
						if (targetFace != 2) {
							if (targetFace == 3) {
								if (currentMapX == toMapX - selfSize && toMapY >= currentMapY && toMapY <= someY)
									return true;
								if (currentMapX <= toMapX && someX >= toMapX && currentMapY == toMapY + 1 && (clip[toMapX][currentMapY] & 0x2c0120) == 0)
									return true;
								if (toMapX + 1 == currentMapX && currentMapY <= toMapY && toMapY <= someY && (clip[currentMapX][toMapY] & 0x2c0180) == 0)
									return true;
								if (toMapX >= currentMapX && someX >= toMapX && currentMapY == -selfSize + toMapY)
									return true;
							}
						} else {
							if (toMapX - selfSize == currentMapX && toMapY >= currentMapY && toMapY <= someY && ((clip[someX][toMapY] & 0x2c0108) == 0))
								return true;
							if (toMapX >= currentMapX && toMapX <= someX && currentMapY == toMapY + 1 && (clip[toMapX][currentMapY] & 0x2c0120) == 0)
								return true;
							if (currentMapX == toMapX + 1 && toMapY >= currentMapY && toMapY <= someY)
								return true;
							if (currentMapX <= toMapX && toMapX <= someX && currentMapY == -selfSize + toMapY)
								return true;
						}
					} else {
						if (currentMapX == -selfSize + toMapX && currentMapY <= toMapY && someY >= toMapY && (clip[someX][toMapY] & 0x2c0108) == 0)
							return true;
						if (toMapX >= currentMapX && toMapX <= someX && toMapY + 1 == currentMapY)
							return true;
						if (toMapX + 1 == currentMapX && currentMapY <= toMapY && someY >= toMapY)
							return true;
						if (toMapX >= currentMapX && someX >= toMapX && currentMapY == -selfSize + toMapY && ((clip[toMapX][someY] & 0x2c0102) == 0))
							return true;
					}
				} else {
					if (currentMapX == -selfSize + toMapX && toMapY >= currentMapY && toMapY <= someY)
						return true;
					if (currentMapX <= toMapX && toMapX <= someX && toMapY + 1 == currentMapY)
						return true;
					if (currentMapX == toMapX + 1 && toMapY >= currentMapY && toMapY <= someY && (clip[currentMapX][toMapY] & 0x2c0180) == 0)
						return true;
					if (currentMapX <= toMapX && toMapX <= someX && currentMapY == -selfSize + toMapY && (clip[toMapX][someY] & 0x2c0102) == 0)
						return true;
				}
			}
			if (clipType == 9) {
				if (toMapX >= currentMapX && toMapX <= someX && toMapY + 1 == currentMapY && (clip[toMapX][currentMapY] & 0x2c0120) == 0)
					return true;
				if (currentMapX <= toMapX && toMapX <= someX && currentMapY == -selfSize + toMapY && (clip[toMapX][someY] & 0x2c0102) == 0)
					return true;
				if (toMapX - selfSize == currentMapX && currentMapY <= toMapY && toMapY <= someY && (clip[someX][toMapY] & 0x2c0108) == 0)
					return true;
				if (toMapX + 1 == currentMapX && currentMapY <= toMapY && someY >= toMapY && (clip[currentMapX][toMapY] & 0x2c0180) == 0)
					return true;
			}
		} else {
			if (clipType == 0) {
				if (targetFace == 0) {
					if (toMapX - 1 == currentMapX && toMapY == currentMapY)
						return true;
					if (currentMapX == toMapX && currentMapY == toMapY + 1 && (clip[currentMapX][currentMapY] & 0x2c0120) == 0)
						return true;
					if (currentMapX == toMapX && currentMapY == toMapY - 1 && (clip[currentMapX][currentMapY] & 0x2c0102) == 0)
						return true;
				} else if (targetFace == 1) {
					if (currentMapX == toMapX && toMapY + 1 == currentMapY)
						return true;
					if (toMapX - 1 == currentMapX && toMapY == currentMapY && (clip[currentMapX][currentMapY] & 0x2c0108) == 0)
						return true;
					if (toMapX + 1 == currentMapX && currentMapY == toMapY && (clip[currentMapX][currentMapY] & 0x2c0180) == 0)
						return true;
				} else if (targetFace != 2) {
					if (targetFace == 3) {
						if (toMapX == currentMapX && toMapY - 1 == currentMapY)
							return true;
						if (currentMapX == toMapX - 1 && toMapY == currentMapY && ((clip[currentMapX][currentMapY] & 0x2c0108) == 0))
							return true;
						if (currentMapX == toMapX + 1 && toMapY == currentMapY && ((clip[currentMapX][currentMapY] & 0x2c0180) == 0))
							return true;
					}
				} else {
					if (currentMapX == toMapX + 1 && toMapY == currentMapY)
						return true;
					if (currentMapX == toMapX && toMapY + 1 == currentMapY && (clip[currentMapX][currentMapY] & 0x2c0120) == 0)
						return true;
					if (currentMapX == toMapX && toMapY - 1 == currentMapY && (clip[currentMapX][currentMapY] & 0x2c0102) == 0)
						return true;
				}
			}
			if (clipType == 2) {
				if (targetFace == 0) {
					if (currentMapX == toMapX - 1 && currentMapY == toMapY)
						return true;
					if (currentMapX == toMapX && toMapY + 1 == currentMapY)
						return true;
					if (toMapX + 1 == currentMapX && currentMapY == toMapY && (clip[currentMapX][currentMapY] & 0x2c0180) == 0)
						return true;
					if (toMapX == currentMapX && currentMapY == toMapY - 1 && (clip[currentMapX][currentMapY] & 0x2c0102) == 0)
						return true;
				} else if (targetFace != 1) {
					if (targetFace != 2) {
						if (targetFace == 3) {
							if (toMapX - 1 == currentMapX && currentMapY == toMapY)
								return true;
							if (toMapX == currentMapX && toMapY + 1 == currentMapY && (clip[currentMapX][currentMapY] & 0x2c0120) == 0)
								return true;
							if (toMapX + 1 == currentMapX && toMapY == currentMapY && (clip[currentMapX][currentMapY] & 0x2c0180) == 0)
								return true;
							if (toMapX == currentMapX && currentMapY == toMapY - 1)
								return true;
						}
					} else {
						if (currentMapX == toMapX - 1 && currentMapY == toMapY && ((clip[currentMapX][currentMapY] & 0x2c0108) == 0))
							return true;
						if (toMapX == currentMapX && currentMapY == toMapY + 1 && ((clip[currentMapX][currentMapY] & 0x2c0120) == 0))
							return true;
						if (currentMapX == toMapX + 1 && currentMapY == toMapY)
							return true;
						if (toMapX == currentMapX && toMapY - 1 == currentMapY)
							return true;
					}
				} else {
					if (toMapX - 1 == currentMapX && currentMapY == toMapY && (clip[currentMapX][currentMapY] & 0x2c0108) == 0)
						return true;
					if (toMapX == currentMapX && toMapY + 1 == currentMapY)
						return true;
					if (currentMapX == toMapX + 1 && currentMapY == toMapY)
						return true;
					if (toMapX == currentMapX && toMapY - 1 == currentMapY && (clip[currentMapX][currentMapY] & 0x2c0102) == 0)
						return true;
				}
			}
			if (clipType == 9) {
				if (currentMapX == toMapX && toMapY + 1 == currentMapY && (clip[currentMapX][currentMapY] & 0x20) == 0)
					return true;
				if (toMapX == currentMapX && toMapY - 1 == currentMapY && (clip[currentMapX][currentMapY] & 0x2) == 0)
					return true;
				if (toMapX - 1 == currentMapX && toMapY == currentMapY && (clip[currentMapX][currentMapY] & 0x8) == 0)
					return true;
				if (toMapX + 1 == currentMapX && toMapY == currentMapY && (clip[currentMapX][currentMapY] & 0x80) == 0)
					return true;
			}
		}
		return false;
	}

	final void unflagUnmovable(int x, int y) {
		y -= offsetY;
		x -= offsetX;
		clip[x][y] = clip[x][y] & ~2097152;
	}

	final void unflagDecoration(int x, int y) {
		y -= offsetY;
		x -= offsetX;
		clip[x][y] = clip[x][y] & ~262144;
	}

	final void setup() {
		for (int x = 0; sizeX > x; x++) {
			for (int y = 0; sizeY > y; y++) {
				if (x != 0 && y != 0 && x < sizeX - 5 && y < sizeY - 5)
					clip[x][y] = 2097152;
				else
					clip[x][y] = -1;
			}
		}
		anInt188++;
	}

	final void unflagObject(int x, int sizeY, int objFace, boolean bool, int sizeX, int y, boolean solid) {
		int flag = 256;
		if (solid)
			flag |= 0x20000;
		y -= offsetY;
		x -= offsetX;
		if (objFace == 1 || objFace == 3) {
			int i_37_ = sizeX;
			sizeX = sizeY;
			sizeY = i_37_;
		}
		if (bool)
			flag |= 0x40000000;
		for (int tx = x; sizeX + x > tx; tx++) {
			if (tx >= 0 && sizeX > tx) {
				for (int ty = y; ty < y + sizeY; ty++) {
					if (ty >= 0 && ty < sizeY)
						unFlagClip(tx, ty, flag);
				}
			}
		}
	}

	final void flagDecoration(int x, int y) {
		y -= offsetY;
		x -= offsetX;
		clip[x][y] = clip[x][y] | 262144;
	}

	static final int method264(int i, int i_42_, int i_43_, byte i_44_) {
		i_43_ &= 0x3;
		if (i_43_ == 0)
			return i;
		if (i_43_ == 1)
			return i_42_;
		int i_45_ = -119 / ((-52 - i_44_) / 48);
		if (i_43_ == 2)
			return -i + 7;
		return 7 - i_42_;
	}

	final boolean checkMove(int fromMapX, int fromMapY, int toMapX, int toMapY, int fromSizeX, int fromSizeY, int targetSizeX, int targetSizeY, int i_49_) {
		int fromFullX = fromSizeX + fromMapX;
		int fromFullY = fromSizeY + fromMapY;
		int toFullX = toMapX + targetSizeX;
		int toFullY = toMapY + targetSizeY;
		if (fromMapX == toFullX && (i_49_ & 0x2) == 0) {
			int i_59_ = fromMapY > toMapY ? fromMapY : toMapY;
			for (int i_60_ = toFullY > fromFullY ? fromFullY : toFullY; i_59_ < i_60_; i_59_++) {
				if ((clip[toFullX - offsetX - 1][i_59_ - offsetY] & 0x8) == 0)
					return true;
			}
		} else if (toMapX != fromFullX || (i_49_ & 0x8) != 0) {
			if (toFullY == fromMapY && (i_49_ & 0x1) == 0) {
				int i_61_ = toMapX >= fromMapX ? toMapX : fromMapX;
				for (int i_62_ = fromFullX >= toFullX ? toFullX : fromFullX; i_62_ > i_61_; i_61_++) {
					if (((clip[i_61_ - offsetX][toFullY - 1 - offsetY]) & 0x2) == 0)
						return true;
				}
			} else if (toMapY == fromFullY && (i_49_ & 0x4) == 0) {
				int i_63_ = fromMapX > toMapX ? fromMapX : toMapX;
				for (int i_64_ = fromFullX >= toFullX ? toFullX : fromFullX; i_64_ > i_63_; i_63_++) {
					if (((clip[-offsetX + i_63_][-offsetY + toMapY]) & 0x20) == 0)
						return true;
				}
			}
		} else {
			int i_65_ = fromMapY <= toMapY ? toMapY : fromMapY;
			for (int i_66_ = fromFullY >= toFullY ? toFullY : fromFullY; i_65_ < i_66_; i_65_++) {
				if ((clip[toMapX - offsetX][-offsetY + i_65_] & 0x80) == 0)
					return true;
			}
		}
		return false;
	}

	final boolean checkMove_(int fromMapX, int fromMapY, int toMapX, int toMapY, int selfSize, int targetSizeX, int targetSizeY, int i_70_) {
		if (selfSize > 1) {
			if (ClipData.checkMove(fromMapX, fromMapY, toMapX, toMapY, selfSize, selfSize, targetSizeX, targetSizeY))
				return true;
			return checkMove(fromMapX, fromMapY, toMapX, toMapY, selfSize, selfSize, targetSizeX, targetSizeY, i_70_);
		}
		int i_76_ = toMapX + targetSizeX - 1;
		int i_77_ = targetSizeY + toMapY - 1;
		if (fromMapX >= toMapX && i_76_ >= fromMapX && toMapY <= fromMapY && i_77_ >= fromMapY)
			return true;
		if (fromMapX == toMapX - 1 && toMapY <= fromMapY && fromMapY <= i_77_ && (clip[-offsetX + fromMapX][-offsetY + fromMapY] & 0x8) == 0 && (i_70_ & 0x8) == 0)
			return true;
		if (i_76_ + 1 == fromMapX && toMapY <= fromMapY && i_77_ >= fromMapY && (clip[-offsetX + fromMapX][-offsetY + fromMapY] & 0x80) == 0 && (i_70_ & 0x2) == 0)
			return true;
		if (toMapY - 1 == fromMapY && fromMapX >= toMapX && i_76_ >= fromMapX && ((clip[fromMapX - offsetX][-offsetY + fromMapY] & 0x2) == 0) && (i_70_ & 0x4) == 0)
			return true;
		if (i_77_ + 1 == fromMapY && toMapX <= fromMapX && i_76_ >= fromMapX && ((clip[fromMapX - offsetX][fromMapY - offsetY] & 0x20) == 0) && (i_70_ & 0x1) == 0)
			return true;
		return false;
	}

	final void unflagWall(int x, int y, int type, int face, boolean solid, boolean bool) {
		y -= offsetY;
		x -= offsetX;
		if (type == 0) {
			if (face == 0) {
				unFlagClip(x, y, 128);
				unFlagClip(x - 1, y, 8);
			}
			if (face == 1) {
				unFlagClip(x, y, 2);
				unFlagClip(x, y + 1, 32);
			}
			if (face == 2) {
				unFlagClip(x, y, 8);
				unFlagClip(x + 1, y, 128);
			}
			if (face == 3) {
				unFlagClip(x, y, 32);
				unFlagClip(x, y - 1, 2);
			}
		}
		if (type == 1 || type == 3) {
			if (face == 0) {
				unFlagClip(x, y, 1);
				unFlagClip(x - 1, y + 1, 16);
			}
			if (face == 1) {
				unFlagClip(x, y, 4);
				unFlagClip(x + 1, y + 1, 64);
			}
			if (face == 2) {
				unFlagClip(x, y, 16);
				unFlagClip(x + 1, y - 1, 1);
			}
			if (face == 3) {
				unFlagClip(x, y, 64);
				unFlagClip(x - 1, y - 1, 4);
			}
		}
		if (type == 2) {
			if (face == 0) {
				unFlagClip(x, y, 130);
				unFlagClip(x - 1, y, 8);
				unFlagClip(x, y + 1, 32);
			}
			if (face == 1) {
				unFlagClip(x, y, 10);
				unFlagClip(x, y + 1, 32);
				unFlagClip(x + 1, y, 128);
			}
			if (face == 2) {
				unFlagClip(x, y, 40);
				unFlagClip(x + 1, y, 128);
				unFlagClip(x, y - 1, 2);
			}
			if (face == 3) {
				unFlagClip(x, y, 160);
				unFlagClip(x, y - 1, 2);
				unFlagClip(x - 1, y, 8);
			}
		}
		if (solid) {
			if (type == 0) {
				if (face == 0) {
					unFlagClip(x, y, 65536);
					unFlagClip(x - 1, y, 4096);
				}
				if (face == 1) {
					unFlagClip(x, y, 1024);
					unFlagClip(x, y + 1, 16384);
				}
				if (face == 2) {
					unFlagClip(x, y, 4096);
					unFlagClip(x + 1, y, 65536);
				}
				if (face == 3) {
					unFlagClip(x, y, 16384);
					unFlagClip(x, y - 1, 1024);
				}
			}
			if (type == 1 || type == 3) {
				if (face == 0) {
					unFlagClip(x, y, 512);
					unFlagClip(x - 1, y + 1, 8192);
				}
				if (face == 1) {
					unFlagClip(x, y, 2048);
					unFlagClip(x + 1, y + 1, 32768);
				}
				if (face == 2) {
					unFlagClip(x, y, 8192);
					unFlagClip(x + 1, y - 1, 512);
				}
				if (face == 3) {
					unFlagClip(x, y, 32768);
					unFlagClip(x - 1, y - 1, 2048);
				}
			}
			if (type == 2) {
				if (face == 0) {
					unFlagClip(x, y, 66560);
					unFlagClip(x - 1, y, 4096);
					unFlagClip(x, y + 1, 16384);
				}
				if (face == 1) {
					unFlagClip(x, y, 5120);
					unFlagClip(x, y + 1, 16384);
					unFlagClip(x + 1, y, 65536);
				}
				if (face == 2) {
					unFlagClip(x, y, 20480);
					unFlagClip(x + 1, y, 65536);
					unFlagClip(x, y - 1, 1024);
				}
				if (face == 3) {
					unFlagClip(x, y, 81920);
					unFlagClip(x, y - 1, 1024);
					unFlagClip(x - 1, y, 4096);
				}
			}
		}
		if (bool) {
			if (type == 0) {
				if (face == 0) {
					unFlagClip(x, y, 536870912);
					unFlagClip(x - 1, y, 33554432);
				}
				if (face == 1) {
					unFlagClip(x, y, 8388608);
					unFlagClip(x, y + 1, 134217728);
				}
				if (face == 2) {
					unFlagClip(x, y, 33554432);
					unFlagClip(x + 1, y, 536870912);
				}
				if (face == 3) {
					unFlagClip(x, y, 134217728);
					unFlagClip(x, y - 1, 8388608);
				}
			}
			if (type == 1 || type == 3) {
				if (face == 0) {
					unFlagClip(x, y, 4194304);
					unFlagClip(x - 1, y + 1, 67108864);
				}
				if (face == 1) {
					unFlagClip(x, y, 16777216);
					unFlagClip(x + 1, y + 1, 268435456);
				}
				if (face == 2) {
					unFlagClip(x, y, 67108864);
					unFlagClip(x + 1, y - 1, 4194304);
				}
				if (face == 3) {
					unFlagClip(x, y, 268435456);
					unFlagClip(x - 1, y - 1, 16777216);
				}
			}
			if (type == 2) {
				if (face == 0) {
					unFlagClip(x, y, 545259520);
					unFlagClip(x - 1, y, 33554432);
					unFlagClip(x, y + 1, 134217728);
				}
				if (face == 1) {
					unFlagClip(x, y, 41943040);
					unFlagClip(x, y + 1, 134217728);
					unFlagClip(x + 1, y, 536870912);
				}
				if (face == 2) {
					unFlagClip(x, y, 167772160);
					unFlagClip(x + 1, y, 536870912);
					unFlagClip(x, y - 1, 8388608);
				}
				if (face == 3) {
					unFlagClip(x, y, 671088640);
					unFlagClip(x, y - 1, 8388608);
					unFlagClip(x - 1, y, 33554432);
				}
			}
		}
	}

	private final void flagClip(int x, int y, int flag) {
		clip[x][y] = clip[x][y] | flag;
	}

	private final void unFlagClip(int x, int y, int flag) {
		clip[x][y] = clip[x][y] & ~flag;
	}

	final boolean checkMove(int clipType, int fromMapX, int fromMapY, int toMapX, int toMapY, int selfSize, int targetFace) {
		if (selfSize != 1) {
			if (fromMapX <= toMapX && toMapX <= selfSize + fromMapX - 1 && toMapY <= toMapY && toMapY <= selfSize + toMapY - 1)
				return true;
		} else if (fromMapX == toMapX && fromMapY == toMapY)
			return true;
		fromMapX -= offsetX;
		toMapY -= offsetY;
		toMapX -= offsetX;
		fromMapY -= offsetY;
		if (selfSize == 1) {
			if (clipType == 6 || clipType == 7) {
				if (clipType == 7)
					targetFace = targetFace + 2 & 0x3;
				if (targetFace == 0) {
					if (toMapX + 1 == fromMapX && fromMapY == toMapY && (clip[fromMapX][fromMapY] & 0x80) == 0)
						return true;
					if (fromMapX == toMapX && toMapY - 1 == fromMapY && (clip[fromMapX][fromMapY] & 0x2) == 0)
						return true;
				} else if (targetFace != 1) {
					if (targetFace == 2) {
						if (fromMapX == toMapX - 1 && fromMapY == toMapY && (clip[fromMapX][fromMapY] & 0x8) == 0)
							return true;
						if (toMapX == fromMapX && fromMapY == toMapY + 1 && (clip[fromMapX][fromMapY] & 0x20) == 0)
							return true;
					} else if (targetFace == 3) {
						if (fromMapX == toMapX + 1 && fromMapY == toMapY && (clip[fromMapX][fromMapY] & 0x80) == 0)
							return true;
						if (toMapX == fromMapX && fromMapY == toMapY + 1 && (clip[fromMapX][fromMapY] & 0x20) == 0)
							return true;
					}
				} else {
					if (fromMapX == toMapX - 1 && toMapY == fromMapY && (clip[fromMapX][fromMapY] & 0x8) == 0)
						return true;
					if (toMapX == fromMapX && toMapY - 1 == fromMapY && (clip[fromMapX][fromMapY] & 0x2) == 0)
						return true;
				}
			}
			if (clipType == 8) {
				if (toMapX == fromMapX && toMapY + 1 == fromMapY && (clip[fromMapX][fromMapY] & 0x20) == 0)
					return true;
				if (toMapX == fromMapX && fromMapY == toMapY - 1 && (clip[fromMapX][fromMapY] & 0x2) == 0)
					return true;
				if (toMapX - 1 == fromMapX && toMapY == fromMapY && (clip[fromMapX][fromMapY] & 0x8) == 0)
					return true;
				if (toMapX + 1 == fromMapX && toMapY == fromMapY && (clip[fromMapX][fromMapY] & 0x80) == 0)
					return true;
			}
		} else {
			int i_97_ = selfSize + fromMapX - 1;
			int i_98_ = fromMapY - 1 + selfSize;
			if (clipType == 6 || clipType == 7) {
				if (clipType == 7)
					targetFace = targetFace + 2 & 0x3;
				if (targetFace == 0) {
					if (toMapX + 1 == fromMapX && toMapY >= fromMapY && i_98_ >= toMapY && (clip[fromMapX][toMapY] & 0x80) == 0)
						return true;
					if (fromMapX <= toMapX && toMapX <= i_97_ && fromMapY == toMapY - selfSize && (clip[toMapX][i_98_] & 0x2) == 0)
						return true;
				} else if (targetFace == 1) {
					if (toMapX - selfSize == fromMapX && fromMapY <= toMapY && i_98_ >= toMapY && (clip[i_97_][toMapY] & 0x8) == 0)
						return true;
					if (toMapX >= fromMapX && i_97_ >= toMapX && fromMapY == -selfSize + toMapY && (clip[toMapX][i_98_] & 0x2) == 0)
						return true;
				} else if (targetFace == 2) {
					if (toMapX - selfSize == fromMapX && fromMapY <= toMapY && i_98_ >= toMapY && (clip[i_97_][toMapY] & 0x8) == 0)
						return true;
					if (toMapX >= fromMapX && toMapX <= i_97_ && fromMapY == toMapY + 1 && (clip[toMapX][fromMapY] & 0x20) == 0)
						return true;
				} else if (targetFace == 3) {
					if (fromMapX == toMapX + 1 && fromMapY <= toMapY && toMapY <= i_98_ && (clip[fromMapX][toMapY] & 0x80) == 0)
						return true;
					if (fromMapX <= toMapX && toMapX <= i_97_ && toMapY + 1 == fromMapY && (clip[toMapX][fromMapY] & 0x20) == 0)
						return true;
				}
			}
			if (clipType == 8) {
				if (fromMapX <= toMapX && toMapX <= i_97_ && fromMapY == toMapY + 1 && (clip[toMapX][fromMapY] & 0x20) == 0)
					return true;
				if (fromMapX <= toMapX && toMapX <= i_97_ && -selfSize + toMapY == fromMapY && (clip[toMapX][i_98_] & 0x2) == 0)
					return true;
				if (-selfSize + toMapX == fromMapX && toMapY >= fromMapY && i_98_ >= toMapY && (clip[i_97_][toMapY] & 0x8) == 0)
					return true;
				if (fromMapX == toMapX + 1 && fromMapY <= toMapY && toMapY <= i_98_ && (clip[fromMapX][toMapY] & 0x80) == 0)
					return true;
			}
		}
		return false;
	}

	static final boolean checkMove(int fromMapX, int fromMapY, int toMapX, int toMapY, int fromSizeX, int fromSizeY, int targetSizeX, int targetSizeY) {
		if (fromMapX >= (toMapX + targetSizeX) || (fromMapX + fromSizeX) <= toMapX)
			return false;
		if (fromMapY >= toMapY + targetSizeY || toMapY >= fromMapY + fromSizeY)
			return false;
		return true;
	}
}
