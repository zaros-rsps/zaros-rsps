package net.zaros.client;
/* Class343 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class FileWorker {
	static int anInt3004 = 64;
	static int anInt3005;
	static boolean aBoolean3006 = true;

	abstract ReferenceTable getReferenceTable(byte i);

	public FileWorker() {
		/* empty */
	}

	abstract void requestFile(int i, int i_0_);

	abstract int getFileCompletion(int i, boolean bool);

	abstract byte[] getFileBuffer(int i, int i_1_);

	static final void method3641(int i) {
		if (Class166_Sub1.anIntArray4300 == null)
			Class166_Sub1.anIntArray4300 = new int[65536];
		else
			return;
		double d = Math.random() * 0.03 - 0.015 + 0.7;
		int i_2_ = -38 / ((i + 58) / 33);
		for (int i_3_ = 0; i_3_ < 65536; i_3_++) {
			double d_4_ = (double) ((i_3_ & 0xff76) >> 10) / 64.0 + 0.0078125;
			double d_5_ = (double) ((i_3_ & 0x383) >> 7) / 8.0 + 0.0625;
			double d_6_ = (double) (i_3_ & 0x7f) / 128.0;
			double d_7_ = d_6_;
			double d_8_ = d_6_;
			double d_9_ = d_6_;
			if (d_5_ != 0.0) {
				double d_10_;
				if (!(d_6_ < 0.5))
					d_10_ = d_6_ + d_5_ - d_5_ * d_6_;
				else
					d_10_ = (d_5_ + 1.0) * d_6_;
				double d_11_ = -d_10_ + d_6_ * 2.0;
				double d_12_ = d_4_ + 0.3333333333333333;
				if (d_12_ > 1.0)
					d_12_--;
				double d_13_ = d_4_;
				double d_14_ = d_4_ + -0.3333333333333333;
				if (!(d_13_ * 6.0 < 1.0)) {
					if (!(d_13_ * 2.0 < 1.0)) {
						if (d_13_ * 3.0 < 2.0)
							d_8_ = (((-d_11_ + d_10_) * (-d_13_ + 0.6666666666666666) * 6.0) + d_11_);
						else
							d_8_ = d_11_;
					} else
						d_8_ = d_10_;
				} else
					d_8_ = d_13_ * ((d_10_ - d_11_) * 6.0) + d_11_;
				if (d_12_ * 6.0 < 1.0)
					d_7_ = d_11_ + d_12_ * ((d_10_ - d_11_) * 6.0);
				else if (d_12_ * 2.0 < 1.0)
					d_7_ = d_10_;
				else if (!(d_12_ * 3.0 < 2.0))
					d_7_ = d_11_;
				else
					d_7_ = d_11_ + ((-d_11_ + d_10_) * (0.6666666666666666 - d_12_) * 6.0);
				if (d_14_ < 0.0)
					d_14_++;
				if (!(d_14_ * 6.0 < 1.0)) {
					if (d_14_ * 2.0 < 1.0)
						d_9_ = d_10_;
					else if (!(d_14_ * 3.0 < 2.0))
						d_9_ = d_11_;
					else
						d_9_ = d_11_ + ((-d_14_ + 0.6666666666666666) * (-d_11_ + d_10_) * 6.0);
				} else
					d_9_ = d_14_ * ((d_10_ - d_11_) * 6.0) + d_11_;
			}
			d_7_ = Math.pow(d_7_, d);
			d_8_ = Math.pow(d_8_, d);
			d_9_ = Math.pow(d_9_, d);
			int i_15_ = (int) (d_7_ * 256.0);
			int i_16_ = (int) (d_8_ * 256.0);
			int i_17_ = (int) (d_9_ * 256.0);
			int i_18_ = (i_15_ << 16) + (i_16_ << 8) + i_17_;
			Class166_Sub1.anIntArray4300[i_3_] = i_18_;
		}
	}
}
