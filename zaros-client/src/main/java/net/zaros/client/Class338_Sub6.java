package net.zaros.client;

import jagex3.jagmisc.jagmisc;
import net.zaros.client.configs.invtype.InvTypeList;
import net.zaros.client.configs.objtype.ObjTypeList;

final class Class338_Sub6 extends Class338 {
	static Class209 aClass209_5227 = new Class209(2);
	ParticleEmitterRaw aClass169_5228;
	Class404 aClass404_5229;
	private long aLong5230;
	boolean aBoolean5231 = false;
	Class337 aClass337_5232;
	EmissiveTriangle aClass89_5233;
	int anInt5234;
	Class338_Sub1 aClass338_Sub1_5235;
	private int anInt5236 = 0;
	static Interface2[] anInterface2Array5237;
	private Class337 aClass337_5238;
	private int anInt5239;
	private int anInt5240;
	private int anInt5241;
	private int anInt5242;
	private int anInt5243;
	private int anInt5244;
	private boolean aBoolean5245;
	private int anInt5246;

	public static void method3592(byte i) {
		anInterface2Array5237 = null;
		aClass209_5227 = null;
		if (i != 22) {
			method3592((byte) 1);
		}
	}

	static final int method3593(byte i) {
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub5_5000.method414(122) == 0) {
			for (int i_0_ = 0; Class338_Sub8_Sub1.anInt6576 > i_0_; i_0_++) {
				if (Class341.anInterface1Array2978[i_0_].method2(-23600) == 's' || Class341.anInterface1Array2978[i_0_].method2(-23600) == 'S') {
					Class343_Sub1.aClass296_Sub50_5282.method3060(1, 93, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub5_5000);
					Class309.aBoolean2758 = true;
					break;
				}
			}
		}
		if (Class253.aClass326_2389 == Class326.aClass326_2873) {
			Runtime runtime = Runtime.getRuntime();
			int i_1_ = (int) ((runtime.totalMemory() + -runtime.freeMemory()) / 1024L);
			long l = Class72.method771(-124);
			if (Class246.aLong2333 == 0L) {
				Class246.aLong2333 = l;
			}
			if (i_1_ > 16384 && l - Class246.aLong2333 < 5000L) {
				if (l - Class48.aLong455 > 1000L) {
					System.gc();
					Class48.aLong455 = l;
				}
				return 0;
			}
		}
		if (Class253.aClass326_2389 == Class326.aClass326_2877) {
			if (Class296_Sub39_Sub17.aClass111_6241 == null) {
				Class296_Sub39_Sub17.aClass111_6241 = new Class111(Class42.aClass285_395, Class296_Sub51_Sub36.aClass315_6530, Class326.aBigInteger2902, Connection.aBigInteger2052);
			}
			if (!Class296_Sub39_Sub17.aClass111_6241.method972((byte) 97)) {
				return 0;
			}
			Class296_Sub30.method2700(true, 0, null, (byte) 79);
			Class368_Sub2.aBoolean5431 = false;// !Class41_Sub5.method412((byte) 61);
			GameLoopTask.aClass138_6146 = Class154_Sub1.createFS(1, Class368_Sub2.aBoolean5431 ? 34 : 32, false);
			Class296_Sub15_Sub4.aClass138_6036 = Class154_Sub1.createFS(1, 33, false);
			LookupTable.aClass138_53 = Class154_Sub1.createFS(1, 13, false);
		}
		if (Class326.aClass326_2880 == Class253.aClass326_2389) {
			boolean bool = Class296_Sub15_Sub4.aClass138_6036.filesCompleted(-2);
			int i_2_ = Class296_Sub51_Sub2.cacheFileWorkers[33].method3652(false);
			i_2_ = i_2_ + Class296_Sub51_Sub2.cacheFileWorkers[!Class368_Sub2.aBoolean5431 ? 32 : 34].method3652(false);
			i_2_ += Class296_Sub51_Sub2.cacheFileWorkers[13].method3652(false);
			i_2_ = i_2_ + (bool ? 100 : Class296_Sub15_Sub4.aClass138_6036.getTotalCompletion(0));
			if (i_2_ != 400) {
				return i_2_ / 4;
			}
			Class307.anInt2751 = GameLoopTask.aClass138_6146.getReferenceCRC(22354);
			Class131.anInt1342 = Class296_Sub15_Sub4.aClass138_6036.getReferenceCRC(22354);
			Class41_Sub12.method442(14576, GameLoopTask.aClass138_6146);
			int i_3_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub28_4994.method511(117);
			Class304.aClass56_2734 = new Class56(Class296_Sub50.game, Class394.langID, Class296_Sub15_Sub4.aClass138_6036);
			int[] is = Class304.aClass56_2734.method670(i_3_, 37);
			if (is.length == 0) {
				is = Class304.aClass56_2734.method670(0, -121);
			}
			Class333 class333 = new Class333(GameLoopTask.aClass138_6146, LookupTable.aClass138_53);
			if (is.length > 0) {
				anInterface2Array5237 = new Interface2[is.length];
				for (int i_4_ = 0; anInterface2Array5237.length > i_4_; i_4_++) {
					anInterface2Array5237[i_4_] = new Class273(Class304.aClass56_2734.method669((byte) 125, is[i_4_]), class333);
				}
			}
		}
		if (Class253.aClass326_2389 == Class326.aClass326_2882) {
			Class134.method1406(LookupTable.aClass138_53, Class350.method3678(true), (byte) -7, GameLoopTask.aClass138_6146);
		}
		if (Class326.aClass326_2883 == Class253.aClass326_2389) {
			int i_5_ = SeekableFile.method2215(4234);
			int i_6_ = ClothDefinition.method3663(-1);
			if (i_5_ < i_6_) {
				return i_5_ * 100 / i_6_;
			}
		}
		if (Class326.aClass326_2884 == Class253.aClass326_2389) {
			if (anInterface2Array5237 != null && anInterface2Array5237.length > 0) {
				if (anInterface2Array5237[0].method9(4739) < 100) {
					return 0;
				}
				if (anInterface2Array5237.length > 1 && Class304.aClass56_2734.method665(-10) && anInterface2Array5237[1].method9(4739) < 100) {
					return 0;
				}
			}
			Class296_Sub39_Sub15_Sub1.method2886(8, Class41_Sub13.aHa3774);
			Class16_Sub3_Sub1.method251(Class41_Sub13.aHa3774, -4096);
			Class41_Sub8.method422(1, 1);
		}
		if (Class326.aClass326_2885 == Class253.aClass326_2389) {
			for (int i_7_ = 0; i_7_ < 4; i_7_++) {
				BITConfigDefinition.mapClips[i_7_] = ConfigsRegister.makeClipData(Class198.currentMapSizeX, Class296_Sub38.currentMapSizeY);
			}
		}
		if (Class253.aClass326_2389 == Class326.aClass326_2886) {
			Class205_Sub2.fs8 = Class154_Sub1.createFS(1, 8, false);
			Class381.fs0 = Class154_Sub1.createFS(1, 0, false);
			Class270.fs1 = Class154_Sub1.createFS(1, 1, false);
			Class296_Sub39_Sub1.fs2 = Class154_Sub1.createFS(1, 2, false);
			Class360_Sub6.fs3 = Class154_Sub1.createFS(1, 3, false);
			Class93.fs4 = Class154_Sub1.createFS(1, 4, false);
			Class324.fs5 = Class154_Sub1.createFS(1, 5, true);
			Class42_Sub4.fs6 = Class154_Sub1.createFS(1, 6, true);
			Class184.fs7 = Class154_Sub1.createFS(1, 7, false);
			Class205_Sub1.aClass138_5641 = Class154_Sub1.createFS(1, 9, false);
			Class296_Sub51_Sub29.aClass138_6494 = Class154_Sub1.createFS(1, 10, false);
			Class296_Sub22.aClass138_4722 = Class154_Sub1.createFS(1, 11, false);
			Class72.cs2FS = Class154_Sub1.createFS(1, 12, false);
			Class296_Sub15.aClass138_4671 = Class154_Sub1.createFS(1, 14, false);
			Class33.aClass138_329 = Class154_Sub1.createFS(1, 15, false);
			Class326.fs16 = Class154_Sub1.createFS(1, 16, false);
			Class296_Sub30.aClass138_4822 = Class154_Sub1.createFS(1, 17, false);
			Class199.aClass138_2005 = Class154_Sub1.createFS(1, 18, false);
			Class63.aClass138_726 = Class154_Sub1.createFS(1, 19, false);
			Class196.fs20 = Class154_Sub1.createFS(1, 20, false);
			Class365.gfxFS = Class154_Sub1.createFS(1, 21, false);
			Class296_Sub51_Sub17.aClass138_6429 = Class154_Sub1.createFS(1, 22, false);
			Class205_Sub2.aClass138_5631 = Class154_Sub1.createFS(1, 23, true);
			Class296_Sub51_Sub34.aClass138_6523 = Class154_Sub1.createFS(1, 24, false);
			Class296_Sub13.aClass138_4653 = Class154_Sub1.createFS(1, 25, false);
			StaticMethods.aClass138_5927 = Class154_Sub1.createFS(1, 26, true);
			Class324.aClass138_2862 = Class154_Sub1.createFS(1, 27, false);
			Class296_Sub51_Sub21.fs28 = Class154_Sub1.createFS(1, 28, true);
			Class338_Sub2.aClass138_5201 = Class154_Sub1.createFS(1, 29, false);
			Class134.fs35 = Class154_Sub1.createFS(1, 35, true);
			Class211.aClass138_2099 = Class154_Sub1.createFS(1, 30, true);
			Class122_Sub1_Sub1.aClass138_6604 = Class154_Sub1.createFS(1, 31, true);
			Class77.aClass138_879 = Class154_Sub1.createFS(2, 36, true);
		}
		if (Class253.aClass326_2389 == Class326.aClass326_2887) {
			int i_8_ = 0;
			for (int i_9_ = 0; i_9_ < 37; i_9_++) {
				if (Class296_Sub51_Sub2.cacheFileWorkers[i_9_] != null) {
					i_8_ = i_8_ + Class296_Sub51_Sub2.cacheFileWorkers[i_9_].method3652(false) * ParamType.anIntArray3242[i_9_] / 100;
				}
			}
			if (i_8_ != 100) {
				if (Class157.anInt1595 < 0) {
					Class157.anInt1595 = i_8_;
				}
				return (i_8_ - Class157.anInt1595) * 100 / (100 - Class157.anInt1595);
			}
			Class348.method3674(Class205_Sub2.fs8, -75);
			Class134.method1406(LookupTable.aClass138_53, Class350.method3678(true), (byte) -7, Class205_Sub2.fs8);
		}
		if (Class253.aClass326_2389 == Class326.aClass326_2888) {
			byte[] is = Class296_Sub51_Sub21.fs28.get(4);
			if (is == null) {
				return 0;
			}
			Class296_Sub51_Sub6.method3093(is, 1);
			Class335.method3428((byte) -120);
			Class41_Sub8.method422(1, 2);
		}
		if (Class253.aClass326_2389 == Class326.aClass326_2889) {
			Class154.method1560(true, Class211.aClass138_2099, Class252.aClass398_2383);
		}
		if (Class253.aClass326_2389 == Class326.aClass326_2890) {
			int i_10_ = Class215.method2024(false);
			if (i_10_ < 100) {
				return i_10_;
			}
			Class288.method2397((byte) 119, Class296_Sub51_Sub21.fs28.get(1));
			NPCDefinition.aClass268_1478 = new Class268(Class296_Sub51_Sub21.fs28);
			Class42_Sub4.aShortArrayArrayArray3846 = NPCDefinition.aClass268_1478.aShortArrayArrayArray2500;
			CS2Call.aShortArrayArray4971 = NPCDefinition.aClass268_1478.aShortArrayArray2496;
			Class338_Sub3_Sub3_Sub1.equipmentData = new EquipmentData(Class296_Sub51_Sub21.fs28);
		}
		if (Class253.aClass326_2389 == Class326.aClass326_2891) {
			if (NPCDefinition.aClass268_1478.anInt2497 != -1 && !Class184.fs7.fileExists(NPCDefinition.aClass268_1478.anInt2497, 0)) {
				return 99;
			}
			Class316.aD2803 = new Js5TextureLoader(StaticMethods.aClass138_5927, Class205_Sub1.aClass138_5641, Class205_Sub2.fs8);
			Class296_Sub22.itemExtraDataDefinitionLoader = new ParamTypeList(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class41_Sub10.aClass62_3768 = new Class62(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2, Class338_Sub3_Sub3_Sub1.equipmentData);
			Class355.aClass394_3067 = new Class394(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2, Class205_Sub2.fs8);
			Class85.aClass350_927 = new Class350(Class296_Sub50.game, Class394.langID, Class296_Sub30.aClass138_4822);
			Class250.aClass15_2360 = new Class15(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class262.aClass117_2449 = new Class117(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			ParamType.aClass164_3248 = new Class164(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2, Class205_Sub2.fs8);
			Class296_Sub51_Sub13.aClass24_6420 = new ClotchesLoader(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2, Class184.fs7);
			Class296_Sub14.itemArraysDefinitionLoader = new InvTypeList(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class386.aClass335_3268 = new Class335(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class379.objectDefinitionLoader = new ObjectDefinitionLoader(Class296_Sub50.game, Class394.langID, true, Class326.fs16, Class184.fs7);
			Class31.aClass245_324 = new Class245(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2, Class205_Sub2.fs8);
			ConfigurationsLoader.aClass401_86 = new Class401(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2, Class205_Sub2.fs8);
			Class352.npcDefinitionLoader = new NPCDefinitionLoader(Class296_Sub50.game, Class394.langID, true, Class199.aClass138_2005, Class184.fs7);
			Class296_Sub39_Sub1.itemDefinitionLoader = new ObjTypeList(Class296_Sub50.game, Class394.langID, true, Class296_Sub22.itemExtraDataDefinitionLoader, Class63.aClass138_726, Class184.fs7);
			Class296_Sub51_Sub38.aClass405_6542 = new Class405(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class296_Sub51_Sub13.animationsLoader = new AnimationsLoader(Class296_Sub50.game, Class394.langID, Class196.fs20, Class381.fs0, Class270.fs1);
			InvisiblePlayer.aClass279_1977 = new Class279(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class49.aClass182_457 = new Class182(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class157.graphicsLoader = new GraphicsLoader(Class296_Sub50.game, Class394.langID, Class365.gfxFS, Class184.fs7);
			Class338_Sub9.aClass76_5266 = new Class76(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class162.aClass276_3556 = new Class276(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class220.aClass151_2151 = new Class151(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class296_Sub43.bitConfigsLoader = new BITConfigsLoader(Class296_Sub50.game, Class394.langID, Class296_Sub51_Sub17.aClass138_6429);
			ConfigurationsLoader.configsLoader = new ConfigurationsLoader(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			StaticMethods.aClass328_6070 = new Class328(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			HashTable.aClass149_2456 = new Class149(Class296_Sub50.game, Class394.langID, Class296_Sub39_Sub1.fs2);
			Class296_Sub51_Sub27_Sub1.method3158(10816, LookupTable.aClass138_53, Class360_Sub6.fs3, Class184.fs7, Class205_Sub2.fs8);
			GameClient.method106(-21662, Class338_Sub2.aClass138_5201);
			Class165.aClass52_1689 = new Class52(Class394.langID, Class296_Sub51_Sub34.aClass138_6523, Class296_Sub13.aClass138_4653);
			Class338_Sub3_Sub4_Sub1.aClass292_6659 = new Class292(Class394.langID, Class296_Sub51_Sub34.aClass138_6523, Class296_Sub13.aClass138_4653, new Class13());
			Class379.objectDefinitionLoader.method373(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026.method496(118) == 0, (byte) -105);
			Class16_Sub3_Sub1.configsRegister = new ConfigsRegister();
			Class368_Sub4.method3817(-126);
			Class381.method3990(true, Class296_Sub51_Sub13.animationsLoader);
			StaticMethods.method1012(Class324.aClass138_2862, (byte) -120);
			Class393.method4057(Class184.fs7, (byte) -96, Class316.aD2803);
			Class130 class130 = new Class130(Class296_Sub51_Sub29.aClass138_6494.getFile_("", "huffman"));
			Class69_Sub2.method737(42, class130);
			try {
				jagmisc.init();
			} catch (Throwable throwable) {
				/* empty */
			}
			Class368_Sub20.aClass217_5550 = Class296_Sub29.method2693(0);
			Class360_Sub3.aClass296_Sub28_5309 = new Class296_Sub28(true, Class252.aClass398_2383);
		}
		if (Class326.aClass326_2893 == Class253.aClass326_2389) {
			int i_11_ = ConfigsRegister.method594(22635, Class205_Sub2.fs8) + Class352.method3686(0, true);
			int i_12_ = Class41_Sub30.method523((byte) 123) + ClothDefinition.method3663(-1);
			if (i_12_ > i_11_) {
				return i_11_ * 100 / i_12_;
			}
		}
		if (i <= 112) {
			return 91;
		}
		if (Class326.aClass326_2894 == Class253.aClass326_2389) {
			Class106.method922(Class205_Sub2.aClass138_5631, Class250.aClass15_2360, Class262.aClass117_2449, Class379.objectDefinitionLoader, Class31.aClass245_324, ConfigurationsLoader.aClass401_86, Class16_Sub3_Sub1.configsRegister);
		}
		if (Class326.aClass326_2895 == Class253.aClass326_2389) {
			Class139.globalStringVars = new String[Class162.aClass276_3556.anInt2528];
			Class269.globalIntVars = new int[Class220.aClass151_2151.anInt1549];
			Class14.aBooleanArray153 = new boolean[Class220.aClass151_2151.anInt1549];
			for (int i_13_ = 0; Class220.aClass151_2151.anInt1549 > i_13_; i_13_++) {
				if (Class220.aClass151_2151.method1542((byte) -42, i_13_).anInt326 == 0) {
					Class14.aBooleanArray153[i_13_] = true;
					ObjectDefinition.anInt809++;
				}
				Class269.globalIntVars[i_13_] = -1;
			}
			Class302.method3260((byte) -64);
			Class324.fs5.clearIdentifiers(true, (byte) 81, false);
			Class42_Sub4.fs6.clearIdentifiers(true, (byte) 89, true);
			Class205_Sub2.fs8.clearIdentifiers(true, (byte) 123, true);
			LookupTable.aClass138_53.clearIdentifiers(true, (byte) 101, true);
			Class296_Sub51_Sub29.aClass138_6494.clearIdentifiers(true, (byte) 79, true);
			Class296_Sub39_Sub1.fs2.discardUnpacked_ = 2;
			Class154.aBoolean1584 = true;
			Class296_Sub30.aClass138_4822.discardUnpacked_ = 2;
			Class326.fs16.discardUnpacked_ = 2;
			Class199.aClass138_2005.discardUnpacked_ = 2;
			Class63.aClass138_726.discardUnpacked_ = 2;
			Class196.fs20.discardUnpacked_ = 2;
			Class365.gfxFS.discardUnpacked_ = 2;
		}
		if (Class326.aClass326_2896 == Class253.aClass326_2389) {
			if (!InterfaceComponent.loadInterface(NPCDefinition.aClass268_1478.anInt2490)) {
				return 0;
			}
			boolean bool = true;
			for (int i_14_ = 0; i_14_ < Class192.openedInterfaceComponents[NPCDefinition.aClass268_1478.anInt2490].length; i_14_++) {
				InterfaceComponent class51 = Class192.openedInterfaceComponents[NPCDefinition.aClass268_1478.anInt2490][i_14_];
				if (class51.type == 5 && class51.textureID != -1 && !Class205_Sub2.fs8.fileExists(class51.textureID, 0)) {
					bool = false;
				}
			}
			if (!bool) {
				return 0;
			}
		}
		if (Class253.aClass326_2389 == Class326.aClass326_2897) {
			Class274.method2315(true, (byte) -69);
		}
		if (Class326.aClass326_2898 == Class253.aClass326_2389) {
			Class296_Sub51_Sub15.aClass54_6426.method649(0);
			try {
				Class373_Sub3.aThread5609.join();
			} catch (InterruptedException interruptedexception) {
				return 0;
			}
			Class296_Sub51_Sub15.aClass54_6426 = null;
			Class304.aClass56_2734 = null;
			anInterface2Array5237 = null;
			Class373_Sub3.aThread5609 = null;
			GameLoopTask.aClass138_6146 = null;
			Class296_Sub15_Sub4.aClass138_6036 = null;
			Class370.method3872(true);
			Class382.aBoolean3241 = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub5_5000.method414(126) == 1;
			Class343_Sub1.aClass296_Sub50_5282.method3060(1, 117, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub5_5000);
			if (Class382.aBoolean3241) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(0, 57, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988);
			} else if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988.aBoolean3823 && Class360_Sub3.aClass296_Sub28_5309.anInt4797 < 512 && Class360_Sub3.aClass296_Sub28_5309.anInt4797 != 0) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(0, 51, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988);
			}
			Class368_Sub4.method3820(1);
			if (Class382.aBoolean3241) {
				Class33.method348(false, false, 0);
			} else {
				Class33.method348(false, false, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_4988.method521(121));
			}
			Class104.method904(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_5003.method444(117), false, 0, -1, -1);
			Class296_Sub39_Sub15_Sub1.method2886(8, Class41_Sub13.aHa3774);
			Class16_Sub3_Sub1.method251(Class41_Sub13.aHa3774, -4096);
			Class296_Sub51_Sub13.method3112(Class205_Sub2.fs8, (byte) -103, Class41_Sub13.aHa3774);
			ha.method1087(1, Class85.aClass397Array929);
		}
		return ByteStream.method2631(93);
	}

	final void method3594(boolean bool, long l, int i, int i_15_, ha var_ha) {
		if (!aBoolean5231) {
			if (AdvancedMemoryCache.anInt1167 < aClass169_5228.anInt1744) {
				bool = false;
			} else if (Class153.anIntArray1579[AdvancedMemoryCache.anInt1167] < Class79.anInt888) {
				bool = false;
			} else if (aBoolean5245) {
				bool = false;
			} else if (aClass169_5228.anInt1724 != -1) {
				int i_16_ = (int) (l + -aLong5230);
				if (!aClass169_5228.aBoolean1710 && i_16_ > aClass169_5228.anInt1724) {
					bool = false;
				} else {
					i_16_ %= aClass169_5228.anInt1724;
				}
				if (!aClass169_5228.aBoolean1751 && aClass169_5228.anInt1709 > i_16_) {
					bool = false;
				}
				if (aClass169_5228.aBoolean1751 && aClass169_5228.anInt1709 <= i_16_) {
					bool = false;
				}
			}
		} else {
			bool = false;
		}
		if (bool) {
			Class196.anInt1991++;
			int i_17_ = (aClass337_5232.anInt2957 + aClass337_5232.anInt2970 + aClass337_5232.anInt2968) / 3;
			int i_18_ = (aClass337_5232.anInt2963 + aClass337_5232.anInt2966 + aClass337_5232.anInt2959) / 3;
			int i_19_ = (aClass337_5232.anInt2961 + aClass337_5232.anInt2967 + aClass337_5232.anInt2958) / 3;
			if (aClass337_5232.anInt2964 != i_17_ || i_18_ != aClass337_5232.anInt2962 || aClass337_5232.anInt2969 != i_19_) {
				aClass337_5232.anInt2964 = i_17_;
				aClass337_5232.anInt2962 = i_18_;
				aClass337_5232.anInt2969 = i_19_;
				int i_20_ = -aClass337_5232.anInt2970 + aClass337_5232.anInt2968;
				int i_21_ = aClass337_5232.anInt2963 - aClass337_5232.anInt2966;
				int i_22_ = aClass337_5232.anInt2967 - aClass337_5232.anInt2958;
				int i_23_ = aClass337_5232.anInt2957 - aClass337_5232.anInt2970;
				int i_24_ = -aClass337_5232.anInt2966 + aClass337_5232.anInt2959;
				int i_25_ = aClass337_5232.anInt2961 - aClass337_5232.anInt2958;
				anInt5239 = i_20_ * i_24_ - i_23_ * i_21_;
				anInt5243 = i_25_ * i_21_ - i_22_ * i_24_;
				for (anInt5246 = i_23_ * i_22_ - i_25_ * i_20_; anInt5243 > 32767 || anInt5246 > 32767 || anInt5239 > 32767 || anInt5243 < -32767 || anInt5246 < -32767 || anInt5239 < -32767; anInt5243 >>= 1) {
					anInt5246 >>= 1;
					anInt5239 >>= 1;
				}
				int i_26_ = (int) Math.sqrt(anInt5246 * anInt5246 + anInt5243 * anInt5243 + anInt5239 * anInt5239);
				if (i_26_ <= 0) {
					i_26_ = 1;
				}
				anInt5239 = anInt5239 * 32767 / i_26_;
				anInt5243 = anInt5243 * 32767 / i_26_;
				anInt5246 = anInt5246 * 32767 / i_26_;
				if (aClass169_5228.aShort1749 > 0 || aClass169_5228.aShort1736 > 0) {
					int i_27_ = (int) (Math.atan2(anInt5239, anInt5243) * 2607.5945876176133);
					int i_28_ = (int) (Math.atan2(anInt5246, Math.sqrt(anInt5243 * anInt5243 + anInt5239 * anInt5239)) * 2607.5945876176133);
					anInt5241 = aClass169_5228.aShort1749 - aClass169_5228.aShort1740;
					anInt5240 = -(anInt5241 >> 1) + aClass169_5228.aShort1740 + i_27_;
					anInt5244 = aClass169_5228.aShort1736 - aClass169_5228.aShort1750;
					anInt5242 = i_28_ + aClass169_5228.aShort1750 - (anInt5244 >> 1);
				}
			}
			anInt5236 += (int) ((aClass169_5228.anInt1752 + Math.random() * (-aClass169_5228.anInt1752 + aClass169_5228.anInt1716)) * i_15_);
			if (anInt5236 > 63) {
				int i_29_ = anInt5236 >> 6;
				anInt5236 &= 0x3f;
				for (int i_30_ = 0; i_29_ > i_30_; i_30_++) {
					int i_31_;
					int i_32_;
					int i_33_;
					if (aClass169_5228.aShort1749 <= 0 && aClass169_5228.aShort1736 <= 0) {
						i_32_ = anInt5243;
						i_31_ = anInt5246;
						i_33_ = anInt5239;
					} else {
						int i_34_ = anInt5240 + (int) (Math.random() * anInt5241);
						i_34_ &= 0x3fff;
						int i_35_ = Class296_Sub4.anIntArray4598[i_34_];
						int i_36_ = Class296_Sub4.anIntArray4618[i_34_];
						int i_37_ = anInt5242 + (int) (Math.random() * anInt5244);
						i_37_ &= 0x1fff;
						int i_38_ = Class296_Sub4.anIntArray4598[i_37_];
						int i_39_ = Class296_Sub4.anIntArray4618[i_37_];
						int i_40_ = 13;
						i_31_ = (i_39_ << 1) * -1;
						i_32_ = i_38_ * i_36_ >> i_40_;
						i_33_ = i_38_ * i_35_ >> i_40_;
					}
					float f = (float) Math.random();
					float f_41_ = (float) Math.random();
					if (f + f_41_ > 1.0F) {
						f = 1.0F - f;
						f_41_ = 1.0F - f_41_;
					}
					float f_42_ = 1.0F - (f_41_ + f);
					int i_43_ = (int) (f_41_ * aClass337_5232.anInt2968 + f * aClass337_5232.anInt2970 + aClass337_5232.anInt2957 * f_42_);
					int i_44_ = (int) (aClass337_5232.anInt2963 * f_41_ + f * aClass337_5232.anInt2966 + aClass337_5232.anInt2959 * f_42_);
					int i_45_ = (int) (f_42_ * aClass337_5232.anInt2961 + (f * aClass337_5232.anInt2958 + f_41_ * aClass337_5232.anInt2967));
					int i_46_ = (int) (f_41_ * aClass337_5238.anInt2968 + aClass337_5238.anInt2970 * f + f_42_ * aClass337_5238.anInt2957);
					int i_47_ = (int) (aClass337_5238.anInt2963 * f_41_ + f * aClass337_5238.anInt2966 + aClass337_5238.anInt2959 * f_42_);
					int i_48_ = (int) (aClass337_5238.anInt2958 * f + f_41_ * aClass337_5238.anInt2967 + aClass337_5238.anInt2961 * f_42_);
					int i_49_ = i_43_ - i_46_;
					int i_50_ = i_44_ - i_47_;
					int i_51_ = i_45_ - i_48_;
					int i_52_ = (int) (i_49_ * Math.random() + i_46_);
					int i_53_ = (int) (i_47_ + Math.random() * i_50_);
					int i_54_ = (int) (i_48_ + i_51_ * Math.random());
					int i_55_ = (int) (Math.random() * (aClass169_5228.anInt1721 - aClass169_5228.anInt1723)) + aClass169_5228.anInt1723;
					int i_56_ = aClass169_5228.anInt1743 + (int) (Math.random() * (aClass169_5228.anInt1768 - aClass169_5228.anInt1743));
					int i_57_ = (int) ((-aClass169_5228.anInt1728 + aClass169_5228.anInt1757) * Math.random()) + aClass169_5228.anInt1728;
					int i_58_;
					if (!aClass169_5228.aBoolean1715) {
						i_58_ = (int) (Math.random() * aClass169_5228.anInt1726 + aClass169_5228.anInt1755) | (int) (aClass169_5228.anInt1760 + aClass169_5228.anInt1763 * Math.random()) << 16 | (int) (Math.random() * aClass169_5228.anInt1753 + aClass169_5228.anInt1732) << 8 | (int) (Math.random() * aClass169_5228.anInt1712 + aClass169_5228.anInt1722) << 24;
					} else {
						double d = Math.random();
						i_58_ = (int) (aClass169_5228.anInt1755 + d * aClass169_5228.anInt1726) | (int) (aClass169_5228.anInt1753 * d + aClass169_5228.anInt1732) << 8 | (int) (aClass169_5228.anInt1763 * d + aClass169_5228.anInt1760) << 16 | (int) (aClass169_5228.anInt1722 + aClass169_5228.anInt1712 * Math.random()) << 24;
					}
					int i_59_ = aClass169_5228.anInt1719;
					if (!var_ha.u() && !aClass169_5228.aBoolean1759) {
						i_59_ = -1;
					}
					if (Class296_Sub51_Sub23.anInt6461 != ISAACCipher.anInt1895) {
						Class338_Sub8_Sub2_Sub1 class338_sub8_sub2_sub1 = Class296_Sub51_Sub36.aClass338_Sub8_Sub2_Sub1Array6528[Class296_Sub51_Sub23.anInt6461];
						Class296_Sub51_Sub23.anInt6461 = Class296_Sub51_Sub23.anInt6461 + 1 & 0x3ff;
						class338_sub8_sub2_sub1.method3613(this, i_52_, i_53_, i_54_, i_32_, i_31_, i_33_, i_55_, i_56_, i_58_, i_57_, i_59_, aClass169_5228.aBoolean1761, aClass169_5228.aBoolean1713);
					} else {
						Class338_Sub8_Sub2_Sub1 class338_sub8_sub2_sub1 = new Class338_Sub8_Sub2_Sub1(this, i_52_, i_53_, i_54_, i_32_, i_31_, i_33_, i_55_, i_56_, i_58_, i_57_, i_59_, aClass169_5228.aBoolean1761, aClass169_5228.aBoolean1713);
					}
				}
			}
		}
		if (!aClass337_5232.method3434(aClass337_5238, 0)) {
			Class337 class337 = aClass337_5238;
			aClass337_5238 = aClass337_5232;
			aClass337_5232 = class337;
			aClass337_5232.anInt2969 = aClass337_5238.anInt2969;
			aClass337_5232.anInt2968 = aClass89_5233.anInt961;
			aClass337_5232.anInt2958 = aClass89_5233.anInt957;
			aClass337_5232.anInt2963 = aClass89_5233.anInt963;
			aClass337_5232.anInt2962 = aClass337_5238.anInt2962;
			aClass337_5232.anInt2961 = aClass89_5233.anInt949;
			aClass337_5232.anInt2964 = aClass337_5238.anInt2964;
			aClass337_5232.anInt2959 = aClass89_5233.anInt948;
			aClass337_5232.anInt2967 = aClass89_5233.anInt960;
			aClass337_5232.anInt2957 = aClass89_5233.anInt965;
			aClass337_5232.anInt2966 = aClass89_5233.anInt950;
			aClass337_5232.anInt2970 = aClass89_5233.anInt951;
		}
		anInt5234 = 0;
		if (i >= -37) {
			aClass338_Sub1_5235 = null;
		}
		for (Class338_Sub8_Sub2_Sub1 class338_sub8_sub2_sub1 = (Class338_Sub8_Sub2_Sub1) aClass404_5229.method4160((byte) 123); class338_sub8_sub2_sub1 != null; class338_sub8_sub2_sub1 = (Class338_Sub8_Sub2_Sub1) aClass404_5229.method4163(-24917)) {
			class338_sub8_sub2_sub1.method3611(l, i_15_);
			anInt5234++;
		}
		Class246.anInt2334 += anInt5234;
	}

	final void method3595(int i, ha var_ha, long l) {
		if (i != -1) {
			anInt5239 = 89;
		}
		for (Class338_Sub8_Sub2_Sub1 class338_sub8_sub2_sub1 = (Class338_Sub8_Sub2_Sub1) aClass404_5229.method4160((byte) -36); class338_sub8_sub2_sub1 != null; class338_sub8_sub2_sub1 = (Class338_Sub8_Sub2_Sub1) aClass404_5229.method4163(i ^ 0x6154)) {
			class338_sub8_sub2_sub1.method3612(var_ha, l);
		}
	}

	final void method3596(byte i) {
		if (i > -45) {
			method3592((byte) -94);
		}
		aClass337_5232.anInt2970 = aClass89_5233.anInt951;
		aClass337_5232.anInt2958 = aClass89_5233.anInt957;
		aClass337_5232.anInt2957 = aClass89_5233.anInt965;
		aClass337_5232.anInt2959 = aClass89_5233.anInt948;
		aClass337_5232.anInt2966 = aClass89_5233.anInt950;
		aClass337_5232.anInt2963 = aClass89_5233.anInt963;
		aClass337_5232.anInt2961 = aClass89_5233.anInt949;
		aClass337_5232.anInt2968 = aClass89_5233.anInt961;
		aClass337_5232.anInt2967 = aClass89_5233.anInt960;
		if (aClass337_5232.anInt2968 != aClass337_5232.anInt2970 || aClass337_5232.anInt2968 != aClass337_5232.anInt2957 || aClass337_5232.anInt2966 != aClass337_5232.anInt2963 || aClass337_5232.anInt2959 != aClass337_5232.anInt2963 || aClass337_5232.anInt2958 != aClass337_5232.anInt2967 || aClass337_5232.anInt2961 != aClass337_5232.anInt2967) {
			if (aBoolean5245) {
				aClass337_5238.anInt2967 = aClass337_5232.anInt2967;
				aClass337_5238.anInt2959 = aClass337_5232.anInt2959;
				aClass337_5238.anInt2968 = aClass337_5232.anInt2968;
				aClass337_5238.anInt2961 = aClass337_5232.anInt2961;
				aClass337_5238.anInt2966 = aClass337_5232.anInt2966;
				aClass337_5238.anInt2957 = aClass337_5232.anInt2957;
				aBoolean5245 = false;
				aClass337_5238.anInt2963 = aClass337_5232.anInt2963;
				aClass337_5238.anInt2970 = aClass337_5232.anInt2970;
				aClass337_5238.anInt2958 = aClass337_5232.anInt2958;
			}
		} else {
			aBoolean5245 = true;
		}
	}

	Class338_Sub6(ha var_ha, EmissiveTriangle class89, Class338_Sub1 class338_sub1, long l) {
		aClass337_5232 = new Class337();
		aClass337_5238 = new Class337();
		aBoolean5245 = false;
		aClass89_5233 = class89;
		aLong5230 = l;
		aClass338_Sub1_5235 = class338_sub1;
		aClass169_5228 = aClass89_5233.method831(-11742);
		if (!var_ha.u() && aClass169_5228.anInt1765 != -1) {
			aClass169_5228 = InvisiblePlayer.method1932(-16734, aClass169_5228.anInt1765);
		}
		aClass404_5229 = new Class404();
		anInt5236 += Math.random() * 64.0;
		method3596((byte) -46);
		aClass337_5238.anInt2958 = aClass337_5232.anInt2958;
		aClass337_5238.anInt2968 = aClass337_5232.anInt2968;
		aClass337_5238.anInt2967 = aClass337_5232.anInt2967;
		aClass337_5238.anInt2970 = aClass337_5232.anInt2970;
		aClass337_5238.anInt2963 = aClass337_5232.anInt2963;
		aClass337_5238.anInt2957 = aClass337_5232.anInt2957;
		aClass337_5238.anInt2959 = aClass337_5232.anInt2959;
		aClass337_5238.anInt2961 = aClass337_5232.anInt2961;
		aClass337_5238.anInt2966 = aClass337_5232.anInt2966;
	}
}
