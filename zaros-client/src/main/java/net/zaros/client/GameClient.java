package net.zaros.client;

/* client - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Rectangle;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Vector;

import net.zaros.client.configs.objtype.ObjTypeList;

public final class GameClient extends Applet_Sub1 {
	static OutgoingPacket aClass311_3715 = new OutgoingPacket(8, 6);
	static IncomingPacket aClass231_3716 = new IncomingPacket(24, 5);
	static Class296_Sub48 aClass296_Sub48_3717;
	public static int zoom = 600;// do u use this? no clue

	@Override
	final synchronized void method100(int i) {
		if (CS2Script.anApplet6140 != null && Class230.aCanvas2209 == null && !Class252.aClass398_2383.aBoolean3321) {
			try {
				Class var_class = CS2Script.anApplet6140.getClass();
				Field field = var_class.getDeclaredField("canvas");
				Class230.aCanvas2209 = (Canvas) field.get(CS2Script.anApplet6140);
				field.set(CS2Script.anApplet6140, null);
				if (Class230.aCanvas2209 != null) {
					return;
				}
			} catch (Exception exception) {
				/* empty */
			}
		}
		if (i < 61) {
			aClass311_3715 = null;
		}
		super.method100(91);
	}

	private final void method103(byte i) {
		boolean bool = Class42.aClass285_395.method2370(32024);
		if (!bool) {
			method113(3879);
		}
	}

	private final void method104(int i) {
		if (Class366_Sub6.anInt5392 == 7 && !Class135.method1412(-1) || Class366_Sub6.anInt5392 == 9 && Class296_Sub39_Sub12.anInt6200 == 42) {
			if (Class379_Sub2_Sub1.anInt6607 > 1) {
				Class308.anInt2753 = Class338_Sub3_Sub3_Sub1.anInt6616;
				Class379_Sub2_Sub1.anInt6607--;
			}
			if (!Class318.aBoolean2814) {
				Class105_Sub2.method915(i ^ 0x6872);
			}
			for (int i_1_ = 0; i_1_ < 100; i_1_++) {
				if (!Class16_Sub1.method235((byte) -107, Class296_Sub45_Sub2.aClass204_6276)) {
					break;
				}
			}
		}
		Class217.anInt2119++;
		Class355.method3698(-1, -82, -1, null);
		Class206.method1992(-1, i ^ ~0xf69, -1, null);
		StaticMethods.method2469((byte) -83);
		Class338_Sub3_Sub3_Sub1.anInt6616++;
		for (int npcNode = 0; Class368_Sub23.npcNodeCount > npcNode; npcNode++) {
			NPC n = Class241.npcNodes[npcNode].value;
			if (n != null) {
				byte walkingProperties = n.definition.aByte1493;
				if ((walkingProperties & 0x1) != 0) {
					int npcSize = n.getSize();
					if ((walkingProperties & 0x2) != 0 && n.currentWayPoint == 0 && Math.random() * 1000.0 < 10.0) {
						int rndX = (int) Math.round(Math.random() * 10.0 - 5.0);
						int rndY = (int) Math.round(Math.random() * 10.0 - 5.0);
						if (rndX != 0 || rndY != 0) {
							int moveMapX = n.waypointQueueX[0] + rndX;
							int moveMapY = n.wayPointQueueY[0] + rndY;
							if (moveMapX < 0) {
								moveMapX = 0;
							} else if (moveMapX > Class198.currentMapSizeX - npcSize - 1) {
								moveMapX = Class198.currentMapSizeX - npcSize - 1;
							}
							if (moveMapY < 0) {
								moveMapY = 0;
							} else if (Class296_Sub38.currentMapSizeY - npcSize - 1 < moveMapY) {
								moveMapY = Class296_Sub38.currentMapSizeY + -npcSize - 1;
							}
							int steps = RouteFinder.findRoute(BITConfigDefinition.mapClips[n.z], -1, npcSize, n.waypointQueueX[0], n.wayPointQueueY[0], moveMapX, moveMapY, npcSize, npcSize, 0, Class32.pathBufferX, Class124.pathBufferY, true, 0);
							if (steps > 0) {
								if (steps > 9) {
									steps = 9;
								}
								for (int step = 0; steps > step; step++) {
									n.waypointQueueX[step] = Class32.pathBufferX[steps - step - 1];
									n.wayPointQueueY[step] = Class124.pathBufferY[steps - step - 1];
									n.walkingTypes[step] = (byte) 1;
								}
								n.currentWayPoint = steps;
							}
						}
					}
					Class71.method768(n, (byte) 104, true);
					int i_11_ = Class368_Sub4.method3822(n, (byte) -67);
					Class41_Sub10.method431(-1073741824, n);
					Class123_Sub1_Sub2.method1065(Class45.anInt438, 0, i_11_, Class41_Sub5.anInt3757, n);
					Class397_Sub3.method4110((byte) 1, Class41_Sub5.anInt3757, n);
					MaterialRaw.method1674(n, (byte) -128);
				}
			}
		}
		if ((Class366_Sub6.anInt5392 == 3 || Class366_Sub6.anInt5392 == 9 || Class366_Sub6.anInt5392 == 7) && (!Class135.method1412(i - 26739) || Class366_Sub6.anInt5392 == 9 && Class296_Sub39_Sub12.anInt6200 == 42) && Class397_Sub3.anInt5799 == 0) {
			if (Class361.anInt3103 != 2) {
				Class296_Sub51_Sub27_Sub1.method3157(false);
			} else {
				Class338_Sub8.method3601((byte) -27);
			}
			if (Class219.camPosX >> 9 < 14 || Class219.camPosX >> 9 >= Class198.currentMapSizeX - 14 || Class124.camPosZ >> 9 < 14 || Class296_Sub38.currentMapSizeY - 14 <= Class124.camPosZ >> 9) {
				Class69_Sub2.method740(4);
			}
		}
		for (;;) {
			CS2Call class296_sub46 = (CS2Call) Class127_Sub1.aClass155_4284.method1573(1);
			if (class296_sub46 == null) {
				break;
			}
			InterfaceComponent class51 = class296_sub46.callerInterface;
			if (class51.anInt592 >= 0) {
				InterfaceComponent class51_12_ = InterfaceComponent.getInterfaceComponent(class51.parentId);
				if (class51_12_ == null || class51_12_.aClass51Array555 == null || class51_12_.aClass51Array555.length <= class51.anInt592 || class51_12_.aClass51Array555[class51.anInt592] != class51) {
					continue;
				}
			}
			CS2Executor.runCS2(class296_sub46);
		}
		for (;;) {
			CS2Call class296_sub46 = (CS2Call) StaticMethods.aClass155_1842.method1573(i - 26737);
			if (class296_sub46 == null) {
				break;
			}
			InterfaceComponent class51 = class296_sub46.callerInterface;
			if (class51.anInt592 >= 0) {
				InterfaceComponent class51_13_ = InterfaceComponent.getInterfaceComponent(class51.parentId);
				if (class51_13_ == null || class51_13_.aClass51Array555 == null || class51.anInt592 >= class51_13_.aClass51Array555.length || class51 != class51_13_.aClass51Array555[class51.anInt592]) {
					continue;
				}
			}
			CS2Executor.runCS2(class296_sub46);
		}
		if (i == 26738) {
			for (;;) {
				CS2Call class296_sub46 = (CS2Call) Class179.aClass155_1854.method1573(1);
				if (class296_sub46 == null) {
					break;
				}
				InterfaceComponent class51 = class296_sub46.callerInterface;
				if (class51.anInt592 >= 0) {
					InterfaceComponent class51_14_ = InterfaceComponent.getInterfaceComponent(class51.parentId);
					if (class51_14_ == null || class51_14_.aClass51Array555 == null || class51.anInt592 >= class51_14_.aClass51Array555.length || class51 != class51_14_.aClass51Array555[class51.anInt592]) {
						continue;
					}
				}
				CS2Executor.runCS2(class296_sub46);
			}
			if (InvisiblePlayer.aClass51_1982 != null) {
				Class264.method2276(238);
			}
			if (Class29.anInt307 % 1500 == 0) {
				Class44_Sub1.method572((byte) 40);
			}
			if (Class366_Sub6.anInt5392 == 7 && !Class135.method1412(i - 26739) || Class366_Sub6.anInt5392 == 9 && Class296_Sub39_Sub12.anInt6200 == 42) {
				Class48.method600(i ^ 0x6800);
			}
			Class296_Sub17_Sub2.method2634(-123);
			if (Class32_Sub1.aBoolean5651 && Class72.method771(i ^ ~0x681d) + -60000L > Class262.aLong2453) {
				Class188.method1891(3746);
			}
			for (Class338_Sub8_Sub1 class338_sub8_sub1 = (Class338_Sub8_Sub1) Class33.aClass404_331.method4160((byte) -7); class338_sub8_sub1 != null; class338_sub8_sub1 = (Class338_Sub8_Sub1) Class33.aClass404_331.method4163(i ^ ~0x926)) {
				if (class338_sub8_sub1.anInt6577 < Class72.method771(-112) / 1000L + -5L) {
					if (class338_sub8_sub1.aShort6574 > 0) {
						Class181.method1827(0, "", 0, "", class338_sub8_sub1.aString6575 + TranslatableString.aClass120_1215.getTranslation(Class394.langID), "", 5);
					}
					if (class338_sub8_sub1.aShort6574 == 0) {
						Class181.method1827(0, "", i ^ 0x6872, "", class338_sub8_sub1.aString6575 + TranslatableString.aClass120_1216.getTranslation(Class394.langID), "", 5);
					}
					class338_sub8_sub1.method3438(false);
				}
			}
			if (Class366_Sub6.anInt5392 == 7 && !Class135.method1412(-1) || Class366_Sub6.anInt5392 == 9 && Class296_Sub39_Sub12.anInt6200 == 42) {
				if (Class366_Sub6.anInt5392 != 9 && Class296_Sub45_Sub2.aClass204_6276.aClass154_2045 == null) {
					StringNode.logout(3, false);
				} else if (Class296_Sub45_Sub2.aClass204_6276 != null) {
					Class296_Sub45_Sub2.aClass204_6276.anInt2060++;
					if (Class296_Sub45_Sub2.aClass204_6276.anInt2060 > 50) {
						Class219.anInt2127++;
						Class296_Sub1 class296_sub1 = Class253.allocateOut(Class296_Sub45_Sub2.aClass204_6276.aClass185_2053, (byte) 87, Class44_Sub2.aClass311_3860);
						Class296_Sub45_Sub2.aClass204_6276.sendPacket(class296_sub1, (byte) 119);
					}
					try {
						Class296_Sub45_Sub2.aClass204_6276.method1963(true);
					} catch (java.io.IOException ioexception) {
						if (Class366_Sub6.anInt5392 == 9) {
							Class296_Sub45_Sub2.aClass204_6276.method1966(320);
						} else {
							StringNode.logout(3, false);
						}
					}
				}
			}
		}
	}

	@Override
	final void method90(byte i) {
		if (i < 14) {
			aClass296_Sub48_3717 = null;
		}
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(115) == 2) {
			try {
				method112(26629);
			} catch (Throwable throwable) {
				Class219_Sub1.method2062(throwable.getMessage() + " (Recovered) " + method102((byte) -54), (byte) -107, throwable);
				Class296_Sub51_Sub30.aBoolean6498 = true;
				Class33.method348(false, false, 0);
			}
		} else {
			method112(26629);
		}
	}

	static final void method105() {
		for (int i = 0; i < Class198.currentMapSizeX; i++) {
			int[] is = Class31.anIntArrayArray325[i];
			for (int i_15_ = 0; i_15_ < Class296_Sub38.currentMapSizeY; i_15_++) {
				is[i_15_] = 0;
			}
		}
	}

	@Override
	final void method95(boolean bool) {
		method119((byte) -125);
		Class121.method1034(-15577);
		Class284.method2364(1);
		GameLoopTask.method2809((byte) -51);
		TranslatableString.method1030((byte) -108);
		Class166.method1641((byte) 104);
		Class189.method1898((byte) 60);
		Class396.method4074(17);
		Class377.method3959(false);
		Class302.method3265(-79);
		Class274.method2317(true);
		Class296_Sub49.method3049((byte) 125);
		Class349.method3675((byte) -97);
		Mobile.method3509(-58);
		Class200.method1954(15330);
		Class359.method3717((byte) -88);
		Animator.method552(0);
		Class330.method3403((byte) 108);
		Class164.method1631(-2);
		Class403.method4156(1610337057);
		Class181.method1828(false);
		StaticMethods.method2778((byte) 126);
		Node.method2432(true);
		Queue.method1488(-96);
		HashTable.method2267(-22705);
		Applet_Sub1.method86(true);
		ModeWhat.method1024(250);
		GameType.method352((byte) 117);
		Class373.method3912(false);
		ha.method1093(19128);
		Class296_Sub50.method3059(94);
		Class225.method2083(false);
		ConfigsRegister.method592(0);
		NodeDeque.method1579(-16777216);
		Class285.method2377(90);
		Class111.method973(-61);
		Class343_Sub1.method3645((byte) -101);
		Js5.method1463(0);
		SeekableFile.method2219(88);
		AdvancedMemoryCache.method989((byte) -97);
		Class350.method3680(126);
		Class15.method229(-67);
		Class117.method1016((byte) -5);
		ClotchesLoader.method301(108);
		Class335.method3430(42);
		ObjectDefinitionLoader.method376(-108);
		Class245.method2186(true);
		Class401.method4140((byte) 108);
		ParamTypeList.method1652(false);
		Class405.method4174((byte) 102);
		AnimationsLoader.method3299((byte) 32);
		Class182.method1842(111);
		GraphicsLoader.method2283(true);
		Class76.method787(26);
		Class276.method2320((byte) 33);
		Class151.method1539(9535);
		BITConfigsLoader.method2098(32);
		ConfigurationsLoader.method180(-128);
		Class328.method3395((byte) 73);
		Class149.method1519(0);
		Class52.method640(119);
		Class292.method2412(false);
		Class268.method2300(true);
		Class154.method1562(bool);
		Connection.method1964((byte) 123);
		Class296_Sub48.method3043(!bool);
		Packet.method2572();
		ClipData.method256(27786);
		Class181_Sub1.method1836(-73);
		Class296_Sub39_Sub14.method2880(116);
		Class259.method2239(118);
		Class404.method4168(!bool);
		Class224.method2081((byte) 119);
		Player.method3531(-25357);
		Class94.method861((byte) 17);
		StaticMethods.method3210(bool);
		Class208.method2004(94);
		Class296_Sub1.method2434((byte) -82);
		SubInPacket.method2243(false);
		Class296_Sub2.method2435(-122);
		Class296_Sub13.method2508(6075);
		InterfaceComponentSettings.method2703(21666);
		ReferenceWrapper.method2788((byte) -60);
		ISAACCipher.method1863(-257);
		ByteStream.method2626((byte) -73);
		Class304.method3271(-67);
		Class362.method3756(1001);
		Class338_Sub8.method3603(123);
		Class72.method772(-3025);
		Class199.method1946(-1500698260);
		Class210_Sub1.method2009(90);
		Class183.method1854(32331);
		World.method977(-4);
		RuntimeException_Sub1.method4176(0);
		Class281.method2349(106);
		Class253.method2207((byte) -95);
		Class100.method879((byte) 88);
		Class296_Sub39_Sub20_Sub2.method2909(0);
		Class296_Sub39_Sub20_Sub1.method2907((byte) -118);
		Class370.method3875(-1);
		Class235.method2128(-25629);
		ObjectDefinition.method759((byte) -72);
		Class188.method1890((byte) -124);
		s.method3358(-77);
		Class375.method3952(bool);
		Model.method1738(0);
		NPCDefinition.method1503(102);
		Class286.method2383(-114);
		StaticMethods.method1615((byte) 25);
		PlayerEquipment.method4145(19001);
		Class55.a((byte) -112);
		StaticMethods.method311(2);
		FileOnDisk.method682(-2);
		Class30.method330(true);
		Class41_Sub21.method477((byte) 112);
		Class41_Sub27.method509((byte) 4);
		Class41_Sub6.method415(-64);
		Class41_Sub4.method403((byte) -117);
		Class41_Sub9.method428(106);
		Class41_Sub1.method392((byte) -31);
		Class41_Sub29.method516((byte) 27);
		Class41_Sub7.method419(9591);
		Class41_Sub18.method464(2);
		Class41_Sub17.method460(24346);
		Class41_Sub8.method421((byte) 60);
		Class41_Sub14.method447(-72);
		Class41_Sub10.method433((byte) -124);
		Class41_Sub19.method467((byte) 46);
		Class41_Sub16.method455(28541);
		Class41_Sub3.method401(1);
		Class41_Sub26.method503(false);
		Class41_Sub25.method495(127);
		Class41_Sub2.method397(9991);
		Class41_Sub13.method445(-20511);
		Class41_Sub20.method472(34);
		Class41_Sub12.method441(!bool);
		Class41_Sub28.method514(-16319);
		Class41_Sub5.method409(-1);
		Class41_Sub23.method488(17478);
		Class41_Sub24.method492(16);
		Class41.method382((byte) -95);
		Class351.method3683(-20291);
		Class33.method349(39);
		Exception_Sub1.method136(-61);
		aa.method154(false);
		za.method3220(true);
		Class92.method852(17850);
		Class390.method4038((byte) -29);
		Class241.method2150(true);
		Class180.method1814(-29646);
		Class338_Sub3_Sub1.method3478((byte) -124);
		Class296_Sub45_Sub4.method3006(false);
		Class381.method3992((byte) -98);
		Class288.method2395((byte) -124);
		Class391.method4043((byte) -45);
		Animation.method543((byte) 92);
		CS2Executor.method1526();
		Class81.method809(30);
		Class296_Sub15_Sub4.method2548(44);
		Class219.method2056(bool);
		Class219_Sub1.method2066(-7523);
		Class296_Sub15_Sub2.method2534((byte) -13);
		Class296_Sub45_Sub2.method2978(-6298);
		Class261.method2249(121);
		Class139.method1464(false);
		Class384.method4017(false);
		Class326.method3387(90);
		Class172.method1680(1004);
		Js5TextureLoader.method1480(-2434);
		Class130.method1378(true);
		Class103.method895(0);
		Class13.method223(-1);
		Class56.method668((byte) 122);
		Class31.method337(-109);
		Class106.method925();
		Class336.method3432(-21850);
		Class163.method1628(126);
		Class25.method306((byte) -119);
		Class22.method290(2);
		Class215.method2023((byte) 127);
		Class227.method2093(2);
		Class191.method1921(-87);
		Class252.method2205((byte) -43);
		Class357.method3710((byte) 75);
		Class165.method1636(1);
		Class264.method2279(205);
		Class338_Sub2.method3457(-27017);
		Class96.method871(0);
		Class212.method2020(false);
		Class44_Sub1_Sub1.method577((byte) 35);
		Class5.method176(-1);
		Class338_Sub1.method3445();
		Class280.method2347(1857370566);
		Class365.method3762(63);
		ReferenceTable.method835((byte) 64);
		Class134.method1404((byte) -110);
		Class106_Sub1.method940(-1);
		Class347.method3669(13);
		Class244.method2174(0);
		Class18.method279(39);
		Class256.method2229();
		Class353.method3689(-11);
		Class269.method2303(true);
		Class388.method4036(0);
		Class262.method2253((byte) -86);
		CS2Stack.method2136((byte) -27);
		Class270.method2304(-95);
		CS2Call.method3039(2);
		CS2Script.method2796(126);
		Class275.method2318(-98);
		Class190.method1918(-16281);
		Class327.method3389(-115);
		Class93.method859((byte) -108);
		Class338_Sub3_Sub3.method3557(0);
		Class234.method2126(-8460);
		r.method2858((byte) -56);
		Class382.method4009(11800);
		Class174.method1696();
		EmissiveTriangle.method833(0);
		EffectiveVertex.method2115(0);
		AnimBase.method2672((byte) 73);
		Class296_Sub39_Sub6.method2812((byte) -126);
		AnimFrame.method688();
		Class325.method3380(60);
		Class313.method3308((byte) 80);
		Class344.method3654(896);
		ItemsNode.method2439((byte) 119);
		Class124.method1078((byte) 107);
		Class306.method3282(118);
		Class220.method2073(112);
		Class45.method580(6);
		Class218.method2041((byte) 86);
		Class368.method3808(5);
		Class206.method1988(-1);
		InvisiblePlayer.method1931(111);
		Class3.method171((byte) -24);
		Class296_Sub36.method2761(-24);
		Class296_Sub18.method2651();
		Class61.method692(9);
		Class236.method2131(-69);
		ClothDefinition.method3661(85);
		BITConfigDefinition.method2355((byte) -125);
		ConfigurationDefinition.method684(55);
		Class135.method1411(-57);
		ParamType.method4012(-25);
		Class251.method2198(39);
		Class187.method1886((byte) -10);
		Class153.method1552(-126);
		Class296_Sub39_Sub13.method2854((byte) -127);
		ParticleEmitterRaw.method1665(-119);
		Class95.method864(-119);
		Class85.method821((byte) 96);
		SubCache.method3251(-128);
		Class320.method3339(16711680);
		Class87.method828(0);
		Class228.method2096(-65536);
		Class88.method830((byte) 94);
		Class296_Sub42.method2918(10);
		Class44_Sub2.method578(true);
		Class12.method222(21463);
		Class290.method2400(0);
		Class250.method2197(!bool);
		Class44_Sub1.method573(69);
		Class338_Sub7.method3598(-98);
		Class338_Sub3_Sub1_Sub2.method3493(false);
		Class338_Sub3_Sub1_Sub5.method3549((byte) -26);
		Class338_Sub9.method3616((byte) -94);
		Class363.method3760(24);
		Class296_Sub56.method3216(-2776);
		Class65.method709((byte) 116);
		Class296_Sub39_Sub10.method2840(-68);
		Class161.method1622((byte) -57);
		Class239.method2141(53);
		Class159.method1617(37);
		Class309.method3292(126);
		StaticMethods.method1709(73);
		StaticMethods.method3962(-11);
		Class338_Sub3_Sub2_Sub1.method3554(-122);
		Class338_Sub3_Sub4_Sub1.method3578(-94);
		Class380.method3978(-31);
		Class338_Sub3_Sub1_Sub4.method3541((byte) -77);
		Class194.method1929(4);
		Class399.method4133((byte) -126);
		Class166_Sub1.method1650((byte) -58);
		Class294.method2421(!bool);
		Class379_Sub2_Sub1.method3970(bool);
		Class156.method1583((byte) 114);
		Class254.method2213(64);
		Class379_Sub2.method3969(89);
		Class140.method1469((byte) -128);
		Class107_Sub1.method946((byte) -94);
		Class379_Sub1.method3966((byte) -117);
		Class107.method945((byte) -60);
		Class379_Sub3.method3974(255);
		Class358.method3715(103);
		MaterialRaw.method1669((byte) -128);
		Texture.method2888(29462);
		StaticMethods.method2822((byte) 111);
		BillboardRaw.method881(127);
		Class296_Sub39_Sub12.method2850(-20134);
		Class296_Sub15.method2520((byte) -120);
		StringNode.method2445((byte) 52);
		Class324.method3377(9);
		Canvas_Sub1.method125(-21676);
		Class8.method190(-4);
		Class157.method1588(true);
		IOException_Sub1.method132(116);
		Class137.method1425(0);
		Class243.method2173((byte) 106);
		Class249.method2194((byte) 120);
		Class48.method604((byte) 21);
		LookupTable.method162((byte) 90);
		Class305.method3280();
		Class14.method226((byte) -4);
		HardReferenceWrapper.method2794((byte) -26);
		Class321.method3343((byte) 123);
		Class296_Sub39_Sub15.method2881((byte) 111);
		Class154_Sub1.method1564((byte) -121);
		Class338_Sub3_Sub5_Sub2.method3589((byte) -54);
		Class338_Sub3_Sub4_Sub2.method3582((byte) 92);
		Class146.method1492(2);
		Class338_Sub3_Sub3_Sub1.method3563(121);
		Class296_Sub27.method2674((byte) 57);
		Class303.method3267(300);
		Class367.method3804(-30918);
		Class84.method813(5);
		Class296_Sub4.method2444(-18269);
		MidiEvent.method955();
		Class296_Sub45_Sub3.method2991(-1);
		Class193.method1925();
		Class74.method778();
		Class381_Sub1.method4004();
		Class177.method1714(3858);
		Class296_Sub15_Sub1.method2527(14574);
		Class216.method2029((byte) 54);
		TextureOperation.method3063(-2);
		Class340.method3619(5);
		Class300.method3246(-4410);
		Class317.method3332(126);
		Class387.method4035(1);
		Class192.method1922(30);
		Class162.method1623((byte) -93);
		Class32.method341(0);
		Class32_Sub1.method347((byte) 127);
		Class379.method3965(2);
		Class122_Sub3.method1047((byte) -126);
		Class122_Sub1.method1041(false);
		Class10.method197(-32746);
		Class203.method1962((byte) 68);
		Class122_Sub1_Sub1.method1045((byte) -43);
		Class196.method1934(!bool);
		Class4.method173((byte) -125);
		Class198.method1944((byte) 109);
		Class148.method1512();
		Class78.method793(116);
		Class296_Sub40.method2911(4);
		Class296_Sub39_Sub17.method2895(0);
		Class338_Sub8_Sub2.method3609(0);
		Class338_Sub6.method3592((byte) 22);
		Class83.method811(0);
		Class338_Sub10.method3617((byte) 104);
		Class126.method1084((byte) 120);
		Class123.method1056(-5690);
		Class86.method825((byte) 119);
		Class296_Sub39_Sub15_Sub1.method2885(false);
		Class144.method1482(-121);
		Class368_Sub19.method3855(-116);
		Class368_Sub7.method3835(2);
		Class368_Sub8.method3837((byte) -107);
		Class368_Sub16.method3853(false);
		Class368_Sub5_Sub1.method3826(20552);
		Class368_Sub5.method3824(1400);
		Class368_Sub3.method3816(14926);
		Class368_Sub11.method3845(4);
		Class368_Sub23.method3868((byte) 119);
		Class368_Sub15.method3852(1);
		Class368_Sub4.method3818(true);
		Class368_Sub9.method3840(-1);
		Class368_Sub20.method3858((byte) 102);
		Class368_Sub21.method3865((byte) -110);
		Class368_Sub18.method3854((byte) -57);
		Class368_Sub5_Sub2.method3830(14274);
		Class368_Sub13.method3847(0);
		Class368_Sub22.method3867(22067);
		Class368_Sub2.method3813(24842);
		Class368_Sub10.method3842(28230);
		Class296_Sub24.method2669(0);
		Class110.method967(20524);
		Class296_Sub51_Sub27.method3152(-1678246399);
		Class296_Sub51_Sub13.method3113((byte) -97);
		Class296_Sub29.method2690(4);
		Class373_Sub3.method3950(16383);
		ha_Sub3.method1274((byte) -123);
		Class49.method607((byte) -70);
		Class342.method3628(-24);
		Class296_Sub9_Sub1.method2493(0);
		Class173.method1682(2);
		Class373_Sub1.method3920(9070);
		Class397_Sub3.method4109(0);
		Class392.method4052(0);
		Class69.method721(-200);
		Class241_Sub2.method2159(-31119);
		Class69_Sub1_Sub1.method736((byte) -91);
		Class178_Sub2.method1789(122);
		Class272.method2310((byte) 35);
		Class296_Sub17_Sub2.method2639(-9688);
		Class69_Sub3.method741(-6390);
		Class356.method3708(4);
		Class67.method714(4);
		Class184.method1861(0);
		Class373_Sub2.method3929((byte) 110);
		Class230.method2109(0);
		Class125.method1081((byte) -104);
		Class395.method4066(-13283);
		Class360.method3727((byte) -96);
		Class241_Sub1.method2153(4475);
		Class258.method2238(true);
		Class152.method1546((byte) 118);
		Class127.method1356(109);
		Class178_Sub3.method1810((byte) -70);
		Class202.method1959((byte) 102);
		Class287.method2387((byte) 121);
		Class246.method2188((byte) -84);
		Class211.method2014(118);
		ha_Sub1_Sub1.method1236(16711680);
		Class105_Sub1.method912(-119);
		Class114.method1004();
		Class296_Sub51_Sub21.method3135((byte) -105);
		Class296_Sub51_Sub24.method3142(32);
		Class296_Sub51_Sub8.method3097(true);
		Class296_Sub51_Sub11.method3106(0);
		Class296_Sub51_Sub19.method3128((byte) 31);
		Class296_Sub51_Sub1.method3077((byte) -31);
		Class296_Sub51_Sub23.method3137((byte) -127);
		Class296_Sub51_Sub29.method3164(4);
		Class296_Sub51_Sub36.method3191((byte) -126);
		Class296_Sub51_Sub2.method3081(-121);
		Class296_Sub51_Sub35.method3189(true);
		Class296_Sub51_Sub12.method3110((byte) 32);
		Class296_Sub51_Sub27_Sub1.method3156((byte) 6);
		Class296_Sub51_Sub30.method3170(2);
		Class296_Sub51_Sub37.method3192(0);
		Class296_Sub51_Sub18.method3125(0);
		Class296_Sub51_Sub7.method3094(true);
		Class296_Sub51_Sub34.method3184(-78);
		Class296_Sub51_Sub17.method3124(2);
		Class296_Sub51_Sub20.method3129((byte) 114);
		Class296_Sub51_Sub10.method3102((byte) 86);
		Class296_Sub51_Sub32.method3177(255);
		Class296_Sub51_Sub28.method3163(416591148);
		Class296_Sub51_Sub38.method3196(-28490);
		Class296_Sub51_Sub39.method3199(65977057);
		Class296_Sub51_Sub4.method3088(32547);
		Class296_Sub51_Sub15.method3120((byte) -85);
		Class296_Sub51_Sub31.method3173((byte) -2);
		Class296_Sub51_Sub33.method3183(-79);
		Class75.method782(-77);
		Class296_Sub23.method2668();
		Class238.method2140(bool);
		aa_Sub2.method157((byte) 89);
		Class316.method3330(-21994);
		Class296_Sub35_Sub1.method2752(-1);
		Class50.method610((byte) 60);
		Class295_Sub1.method2423(-128);
		Class69_Sub2.method739(0);
		Class296_Sub39_Sub4.method2803(92);
		Class296_Sub9.method2481(256);
		Class361.method3755(31372);
		Class366_Sub7.method3793(-89);
		za_Sub2.method3226(-1209185758);
		aa_Sub1.method156((byte) 57);
		Class341.method3626((byte) 71);
		Class296_Sub32.method2715(105);
		Class99.method876((byte) 83);
		Class296_Sub35_Sub3.method2759(-12090);
		Class241_Sub2_Sub1.method2160((byte) 116);
		Class241_Sub2_Sub2.method2166((byte) -59);
		Class355_Sub1.method3703(15329);
		Class355.method3699(16903);
		Class355_Sub2.method3704(true);
		Class366_Sub2.method3775((byte) -91);
		Class366_Sub5.method3783(114);
		Class366_Sub9.method3797(3);
		Class69_Sub4.method746(0);
		Class366_Sub4.method3780(bool);
		Class366_Sub3.method3777(false);
		Class366_Sub8.method3795((byte) -96);
		Class366_Sub1.method3773((byte) 61);
		Class366_Sub6.method3786((byte) 89);
		Class77.method789((byte) 11);
		Class68.method717(95);
		Class131.method1383(-33);
		Class37.method362((byte) 75);
		r_Sub2.method2867(-2813);
		Class352.method3687(87);
		Class226_Sub1.method2091((byte) -128);
		s_Sub3.method3375(90);
		Class296_Sub11.method2497((byte) -84);
		Class386.method4032(-1);
		Class296_Sub35_Sub2.method2758(-60);
		Class360_Sub5.method3743(0);
		Class360_Sub9.method3751((byte) -92);
		Class360_Sub2.method3738(0);
		Class360_Sub6.method3744(104);
		Class123_Sub1.method1059(bool);
		Class123_Sub2.method1067((byte) 51);
		Class57.method671(-13032);
		Class205_Sub3.method1984((byte) -88);
		Class205.method1976(115);
		Class205_Sub1.method1980(-6790);
		Class205_Sub2.method1981(-7450);
		Class369.method3871(-1);
		Class360_Sub3.method3739(121);
		Class105_Sub2.method914((byte) 61);
		Class127_Sub1.method1358((byte) 66);
		Class16_Sub3.method250((byte) -60);
		Class16.method233(-1);
		Class16_Sub3_Sub1.method252(103);
		Class16_Sub2.method245(63);
		Class16_Sub1_Sub1.method240((byte) -97);
		Class16_Sub1.method237((byte) -48);
		Class296_Sub20.method2654(-24695);
		Class63.method705(76);
		Class296_Sub22.method2660((byte) -47);
		Class179.method1813(-32065);
		Class296_Sub39_Sub11.method2842((byte) -10);
		Class116.method1013(-11412);
		Class296_Sub14.method2510(32510);
		Class79.method802(85);
		Class98.method875(-117);
		InputStream_Sub2.method130((byte) 39);
		OutputStream_Sub2.method135(false);
		StaticMethods.method2122(true);
		Class296_Sub39_Sub7.method2820(true);
		Class296_Sub44.method2926(117);
		Class296_Sub43.method2923(1376312589);
		Class296_Sub30.method2701(12558);
		Class296_Sub38.method2775((byte) 98);
		Class296_Sub39_Sub1.method2786(11);
		Class296_Sub39_Sub9.method2829((byte) 124);
		StaticMethods.method2719((byte) -104);
		StaticMethods.method2731(20905);
		StaticMethods.method2728(3);
		StaticMethods.method2725(-13950);
		StaticMethods.method2453(0);
		StaticMethods.method2476((byte) 15);
		StaticMethods.method2464((byte) -106);
		StaticMethods.method2462(-479059834);
		StaticMethods.method2473(-74);
		StaticMethods.method2471(true);
		StaticMethods.method2468(9);
		Class296_Sub34_Sub2.method2742(1);
		Class2.method168(4449);
		Class29.method323(100);
		OutputStream_Sub1.method134(false);
		InputStream_Sub1.method128(-125);
		Class296_Sub37.method2767(127);
		Class296_Sub39_Sub19.method2900(-98);
		Class47.method597(-1);
		Class123_Sub1_Sub1.method1062((byte) -80);
		Class332.method3415(1);
		Class223.method2077(-18350);
		Class42.method530((byte) -35);
		Class42_Sub1.method534((byte) 65);
		Class42_Sub4.method539(-2015);
		Class123_Sub1_Sub2.method1066(!bool);
		Class123_Sub2_Sub2.method1075(-24536);
	}

	static final void method106(int i, Js5 class138) {
		StaticMethods.aClass138_5928 = class138;
		if (i != -21662) {
			method114();
		}
	}

	static final void method107(int i) {
		int i_16_ = PlayerUpdate.visiblePlayersCount;
		int[] is = PlayerUpdate.visiblePlayersIndexes;
		int i_17_;
		if (za_Sub1.anInt6554 == 3) {
			i_17_ = Class379.aClass302Array3624.length;
		} else {
			i_17_ = Class366_Sub7.aBoolean5406 ? i_16_ : i_16_ + Class367.npcsCount;
		}
		for (int i_18_ = 0; i_18_ < i_17_; i_18_++) {
			Mobile class338_sub3_sub1_sub3;
			if (za_Sub1.anInt6554 == 3) {
				Class302 class302 = Class379.aClass302Array3624[i_18_];
				if (!class302.aBoolean2716) {
					continue;
				}
				class338_sub3_sub1_sub3 = class302.method3262(0);
			} else {
				if (i_18_ < i_16_) {
					class338_sub3_sub1_sub3 = PlayerUpdate.visiblePlayers[is[i_18_]];
				} else {
					class338_sub3_sub1_sub3 = ((NPCNode) Class41_Sub18.localNpcs.get(ReferenceTable.npcIndexes[i_18_ - i_16_])).value;
				}
				if (class338_sub3_sub1_sub3.z != i) {
					continue;
				}
				if (class338_sub3_sub1_sub3.anInt6766 < 0) {
					class338_sub3_sub1_sub3.aBoolean6774 = false;
					continue;
				}
			}
			class338_sub3_sub1_sub3.anInt6767 = 0;
			int i_19_ = class338_sub3_sub1_sub3.getSize();
			if ((i_19_ & 0x1) == 0) {
				if ((class338_sub3_sub1_sub3.tileX & 0x1ff) != 0 || (class338_sub3_sub1_sub3.tileY & 0x1ff) != 0) {
					class338_sub3_sub1_sub3.aBoolean6774 = false;
					continue;
				}
			} else if ((class338_sub3_sub1_sub3.tileX & 0x1ff) != 256 || (class338_sub3_sub1_sub3.tileY & 0x1ff) != 256) {
				class338_sub3_sub1_sub3.aBoolean6774 = false;
				continue;
			}
			if (za_Sub1.anInt6554 != 3) {
				if (i_19_ == 1) {
					int i_20_ = class338_sub3_sub1_sub3.tileX >> 9;
					int i_21_ = class338_sub3_sub1_sub3.tileY >> 9;
					if (class338_sub3_sub1_sub3.anInt6766 != Class31.anIntArrayArray325[i_20_][i_21_]) {
						class338_sub3_sub1_sub3.aBoolean6774 = true;
						continue;
					}
					if (StaticMethods.anIntArrayArray5929[i_20_][i_21_] > 1) {
						StaticMethods.anIntArrayArray5929[i_20_][i_21_]--;
						class338_sub3_sub1_sub3.aBoolean6774 = true;
						continue;
					}
				} else {
					int i_22_ = (i_19_ - 1) * 256 + 252;
					int i_23_ = class338_sub3_sub1_sub3.tileX - i_22_ >> 9;
					int i_24_ = class338_sub3_sub1_sub3.tileY - i_22_ >> 9;
					int i_25_ = class338_sub3_sub1_sub3.tileX + i_22_ >> 9;
					int i_26_ = class338_sub3_sub1_sub3.tileY + i_22_ >> 9;
					if (!Class67.method715(i_25_, i_24_, class338_sub3_sub1_sub3.anInt6766, 108, i_26_, i_23_)) {
						for (int i_27_ = i_23_; i_27_ <= i_25_; i_27_++) {
							for (int i_28_ = i_24_; i_28_ <= i_26_; i_28_++) {
								if (class338_sub3_sub1_sub3.anInt6766 == Class31.anIntArrayArray325[i_27_][i_28_]) {
									StaticMethods.anIntArrayArray5929[i_27_][i_28_]--;
								}
							}
						}
						class338_sub3_sub1_sub3.aBoolean6774 = true;
						continue;
					}
				}
			}
			class338_sub3_sub1_sub3.aBoolean6774 = false;
			class338_sub3_sub1_sub3.anInt5213 = aa_Sub1.method155(-1537652855, class338_sub3_sub1_sub3.z, class338_sub3_sub1_sub3.tileX, class338_sub3_sub1_sub3.tileY);
			Class125.method1079(class338_sub3_sub1_sub3, true);
		}
	}

	static final void method108() {
		int i = PlayerUpdate.visiblePlayersCount;
		int[] is = PlayerUpdate.visiblePlayersIndexes;
		int i_29_;
		if (za_Sub1.anInt6554 == 3) {
			i_29_ = Class379.aClass302Array3624.length;
		} else {
			i_29_ = Class366_Sub7.aBoolean5406 ? i : i + Class367.npcsCount;
		}
		for (int i_30_ = 0; i_30_ < i_29_; i_30_++) {
			Mobile class338_sub3_sub1_sub3;
			if (za_Sub1.anInt6554 == 3) {
				Class302 class302 = Class379.aClass302Array3624[i_30_];
				if (!class302.aBoolean2716) {
					continue;
				}
				class338_sub3_sub1_sub3 = class302.method3262(0);
			} else {
				if (i_30_ < i) {
					class338_sub3_sub1_sub3 = PlayerUpdate.visiblePlayers[is[i_30_]];
				} else {
					class338_sub3_sub1_sub3 = ((NPCNode) Class41_Sub18.localNpcs.get(ReferenceTable.npcIndexes[i_30_ - i])).value;
				}
				if (class338_sub3_sub1_sub3.anInt6766 < 0) {
					continue;
				}
			}
			int i_31_ = class338_sub3_sub1_sub3.getSize();
			if ((i_31_ & 0x1) == 0) {
				if ((class338_sub3_sub1_sub3.tileX & 0x1ff) == 0 && (class338_sub3_sub1_sub3.tileY & 0x1ff) == 0) {
					continue;
				}
			} else if ((class338_sub3_sub1_sub3.tileX & 0x1ff) == 256 && (class338_sub3_sub1_sub3.tileY & 0x1ff) == 256) {
				continue;
			}
			class338_sub3_sub1_sub3.anInt5213 = aa_Sub1.method155(-1537652855, class338_sub3_sub1_sub3.z, class338_sub3_sub1_sub3.tileX, class338_sub3_sub1_sub3.tileY);
			Class125.method1079(class338_sub3_sub1_sub3, true);
		}
	}

	static final void method109(int i) {
		int i_32_ = PlayerUpdate.visiblePlayersCount;
		int[] is = PlayerUpdate.visiblePlayersIndexes;
		int i_33_;
		if (za_Sub1.anInt6554 == 3) {
			i_33_ = Class379.aClass302Array3624.length;
		} else {
			i_33_ = i_32_ + Class367.npcsCount;
		}
		for (int i_34_ = 0; i_34_ < i_33_; i_34_++) {
			Mobile class338_sub3_sub1_sub3;
			if (za_Sub1.anInt6554 == 3) {
				Class302 class302 = Class379.aClass302Array3624[i_34_];
				if (!class302.aBoolean2716) {
					continue;
				}
				class338_sub3_sub1_sub3 = class302.method3262(0);
			} else {
				if (i_34_ < i_32_) {
					class338_sub3_sub1_sub3 = PlayerUpdate.visiblePlayers[is[i_34_]];
				} else {
					class338_sub3_sub1_sub3 = ((NPCNode) Class41_Sub18.localNpcs.get(ReferenceTable.npcIndexes[i_34_ - i_32_])).value;
				}
				if (class338_sub3_sub1_sub3.z != i || class338_sub3_sub1_sub3.anInt6766 < 0) {
					continue;
				}
			}
			int i_35_ = class338_sub3_sub1_sub3.getSize();
			if ((i_35_ & 0x1) == 0) {
				if ((class338_sub3_sub1_sub3.tileX & 0x1ff) != 0 || (class338_sub3_sub1_sub3.tileY & 0x1ff) != 0) {
					continue;
				}
			} else if ((class338_sub3_sub1_sub3.tileX & 0x1ff) != 256 || (class338_sub3_sub1_sub3.tileY & 0x1ff) != 256) {
				continue;
			}
			if (i_35_ == 1) {
				int i_36_ = class338_sub3_sub1_sub3.tileX >> 9;
				int i_37_ = class338_sub3_sub1_sub3.tileY >> 9;
				if (class338_sub3_sub1_sub3.anInt6766 > Class31.anIntArrayArray325[i_36_][i_37_]) {
					Class31.anIntArrayArray325[i_36_][i_37_] = class338_sub3_sub1_sub3.anInt6766;
					StaticMethods.anIntArrayArray5929[i_36_][i_37_] = 1;
				} else if (class338_sub3_sub1_sub3.anInt6766 == Class31.anIntArrayArray325[i_36_][i_37_]) {
					StaticMethods.anIntArrayArray5929[i_36_][i_37_]++;
				}
			} else {
				int i_38_ = (i_35_ - 1) * 256 + 60;
				int i_39_ = class338_sub3_sub1_sub3.tileX - i_38_ >> 9;
				int i_40_ = class338_sub3_sub1_sub3.tileY - i_38_ >> 9;
				int i_41_ = class338_sub3_sub1_sub3.tileX + i_38_ >> 9;
				int i_42_ = class338_sub3_sub1_sub3.tileY + i_38_ >> 9;
				for (int i_43_ = i_39_; i_43_ <= i_41_; i_43_++) {
					for (int i_44_ = i_40_; i_44_ <= i_42_; i_44_++) {
						if (class338_sub3_sub1_sub3.anInt6766 > Class31.anIntArrayArray325[i_43_][i_44_]) {
							Class31.anIntArrayArray325[i_43_][i_44_] = class338_sub3_sub1_sub3.anInt6766;
							StaticMethods.anIntArrayArray5929[i_43_][i_44_] = 1;
						} else if (class338_sub3_sub1_sub3.anInt6766 == Class31.anIntArrayArray325[i_43_][i_44_]) {
							StaticMethods.anIntArrayArray5929[i_43_][i_44_]++;
						}
					}
				}
			}
		}
	}

	static final boolean method110(InterfaceComponent class51) {
		if (BillboardRaw.aBoolean1071) {
			if (method115(class51).settingsHash != 0) {
				return false;
			}
			if (class51.type == 0) {
				return false;
			}
		}
		return class51.hiden;
	}

	@Override
	final void method96(int i) {
		if (Class32_Sub1.aBoolean5651) {
			Class188.method1891(3746);
		}
		Class25.method304((byte) 126);
		if (Class41_Sub13.aHa3774 != null) {
			Class41_Sub13.aHa3774.method1091((byte) -106);
		}
		if (Animator.aFrame435 != null) {
			Class41.method388(16549, Animator.aFrame435, Class252.aClass398_2383);
			Animator.aFrame435 = null;
		}
		Class296_Sub45_Sub2.aClass204_6277.method1966(320);
		Class296_Sub45_Sub2.aClass204_6276.method1966(320);
		Class292.method2411(-32048);
		Class42.aClass285_395.method2366((byte) 71);
		if (i <= 76) {
			method83(7);
		}
		Class296_Sub51_Sub36.aClass315_6530.method3323((byte) 117);
		if (SeekableFile.aClass175_2399 != null) {
			SeekableFile.aClass175_2399.method1706(-1);
			SeekableFile.aClass175_2399 = null;
		}
		try {
			Class321.dataFile.method2221(-1);
			for (int i_45_ = 0; i_45_ < 37; i_45_++) {
				Class392.aClass255Array3491[i_45_].method2221(-1);
			}
			Class261.aClass255_2423.method2221(-1);
			Applet_Sub1.aClass255_10.method2221(-1);
			Class296_Sub39_Sub20_Sub1.method2908((byte) -103);
		} catch (Exception exception) {
			/* empty */
		}
	}

	@Override
	final String method102(byte i) {
		String string = null;
		try {
			string = "[1)" + Class206.worldBaseX + "," + Class41_Sub26.worldBaseY + "," + Class198.currentMapSizeX + "," + Class296_Sub38.currentMapSizeY + "|";
			if (i > -2) {
				method113(-84);
			}
			if (Class296_Sub51_Sub11.localPlayer != null) {
				string += "2)" + FileWorker.anInt3005 + "," + (Class296_Sub51_Sub11.localPlayer.waypointQueueX[0] + Class206.worldBaseX) + "," + (Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0] + Class41_Sub26.worldBaseY) + "|";
			}
			string += "3)" + Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(126) + "|4)" + Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5020.method475(123) + "|5)" + Class8.method192((byte) 101) + "|6)" + Class241.anInt2301 + "," + Class384.anInt3254 + "|";
			string += "7)" + Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub7_5004.method420(123) + "|";
			string += "8)" + Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub18_5012.method465(127) + "|";
			string += "9)" + Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub2_5013.method393(125) + "|";
			string += "10)" + Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub26_4991.method497(115) + "|";
			string += "11)" + Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub27_5010.method505(124) + "|";
			string += "12)" + Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub25_5026.method496(126) + "|";
			string += "13)" + FileWorker.anInt3004 + "|";
			string += "14)" + Class366_Sub6.anInt5392;
			if (Class360_Sub3.aClass296_Sub28_5309 != null) {
				string += "|15)" + Class360_Sub3.aClass296_Sub28_5309.anInt4797;
			}
			try {
				if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(121) == 2) {
					Class var_class = java.lang.ClassLoader.class;
					Field field = var_class.getDeclaredField("nativeLibraries");
					Class var_class_46_ = java.lang.reflect.AccessibleObject.class;
					Method method = var_class_46_.getDeclaredMethod("setAccessible", new Class[] { Boolean.TYPE });
					method.invoke(field, new Object[] { Boolean.TRUE });
					Vector vector = (Vector) field.get(GameClient.class.getClassLoader());
					for (int i_47_ = 0; i_47_ < vector.size(); i_47_++) {
						try {
							Object object = vector.elementAt(i_47_);
							Field field_48_ = object.getClass().getDeclaredField("name");
							method.invoke(field_48_, new Object[] { Boolean.TRUE });
							try {
								String string_49_ = (String) field_48_.get(object);
								if (string_49_ != null && string_49_.indexOf("sw3d.dll") != -1) {
									Field field_50_ = object.getClass().getDeclaredField("handle");
									method.invoke(field_50_, new Object[] { Boolean.TRUE });
									string += "|16)" + Long.toHexString(field_50_.getLong(object));
									method.invoke(field_50_, new Object[] { Boolean.FALSE });
								}
							} catch (Throwable throwable) {
								/* empty */
							}
							method.invoke(field_48_, new Object[] { Boolean.FALSE });
						} catch (Throwable throwable) {
							/* empty */
						}
					}
				}
			} catch (Throwable throwable) {
				/* empty */
			}
			string += "]";
		} catch (Throwable throwable) {
			/* empty */
		}
		return string;
	}

	static final void method111() {
		Class296_Sub27.anInt4786 = 0;
		for (int i = 0; i < Class367.npcsCount; i++) {
			NPC class338_sub3_sub1_sub3_sub2 = ((NPCNode) Class41_Sub18.localNpcs.get(ReferenceTable.npcIndexes[i])).value;
			if (class338_sub3_sub1_sub3_sub2.aBoolean6774 && class338_sub3_sub1_sub3_sub2.method3505(-117) != -1) {
				int i_51_ = (class338_sub3_sub1_sub3_sub2.getSize() - 1) * 256 + 252;
				int i_52_ = class338_sub3_sub1_sub3_sub2.tileX - i_51_ >> 9;
				int i_53_ = class338_sub3_sub1_sub3_sub2.tileY - i_51_ >> 9;
				Mobile class338_sub3_sub1_sub3 = Class128.method1363(256, i_53_, class338_sub3_sub1_sub3_sub2.z, i_52_);
				if (class338_sub3_sub1_sub3 != null) {
					int i_54_ = class338_sub3_sub1_sub3.index;
					if (class338_sub3_sub1_sub3 instanceof NPC) {
						i_54_ += 2048;
					}
					if (class338_sub3_sub1_sub3.anInt6767 == 0 && class338_sub3_sub1_sub3.method3505(-90) != -1) {
						InvisiblePlayer.anIntArray1978[Class296_Sub27.anInt4786] = i_54_;
						Class369.anIntArray3138[Class296_Sub27.anInt4786] = i_54_;
						Class296_Sub27.anInt4786++;
						class338_sub3_sub1_sub3.anInt6767++;
					}
					InvisiblePlayer.anIntArray1978[Class296_Sub27.anInt4786] = i_54_;
					Class369.anIntArray3138[Class296_Sub27.anInt4786] = class338_sub3_sub1_sub3_sub2.index + 2048;
					Class296_Sub27.anInt4786++;
					class338_sub3_sub1_sub3.anInt6767++;
				}
			}
		}
		Class338_Sub3_Sub4.method3568(0, true, InvisiblePlayer.anIntArray1978, Class296_Sub27.anInt4786 - 1, Class369.anIntArray3138);
	}

	private final void method112(int i) {
		if (Class366_Sub6.anInt5392 != 16) {
			Class29.anInt307++;
			if (Class29.anInt307 % 1000 == 1) {
				GregorianCalendar gregoriancalendar = new GregorianCalendar();
				Class348.anInt3034 = gregoriancalendar.get(11) * 600 - (-(gregoriancalendar.get(12) * 10) - gregoriancalendar.get(13) / 6);
				Class379_Sub2.aRandom5683.setSeed(Class348.anInt3034);
			}
			Class296_Sub45_Sub2.aClass204_6277.method1969(55);
			Class296_Sub45_Sub2.aClass204_6276.method1969(i - 26722);
			method103((byte) 59);
			if (Class296_Sub39_Sub17.aClass111_6241 != null) {
				Class296_Sub39_Sub17.aClass111_6241.method975(106);
			}
			Class296_Sub51_Sub5.method3089(-11);
			Class340.method3618(110);
			Class2.aClass166_66.method1640(true);
			Class84.aClass189_924.method1893(0);
			if (Class41_Sub13.aHa3774 != null) {
				Class41_Sub13.aHa3774.g((int) Class72.method771(-126));
			}
			NPC.method3539((byte) 101);
			Class338_Sub8_Sub1.anInt6576 = 0;
			Class368_Sub2.anInt5430 = 0;
			for (Interface1 interface1 = Class2.aClass166_66.method1639(16171); interface1 != null; interface1 = Class2.aClass166_66.method1639(16171)) {
				int i_55_ = interface1.method4((byte) 70);
				if (i_55_ != 2 && i_55_ != 3) {
					if (i_55_ == 0 && Class368_Sub2.anInt5430 < 75) {
						Class338_Sub3_Sub1_Sub2.anInterface1Array6741[Class368_Sub2.anInt5430] = interface1;
						Class368_Sub2.anInt5430++;
					}
				} else {
					char c = interface1.method2(-23600);
					if (Class16_Sub3.method249(i - 26626) && (c == '`' || c == '\u00a7' || c == '\u00b2')) {
						if (Class212.method2019((byte) 118)) {
							Class296_Sub51_Sub5.method3090(true);
						} else {
							Class338_Sub8_Sub2.method3608((byte) -35);
						}
					} else if (Class338_Sub8_Sub1.anInt6576 < 128) {
						Class341.anInterface1Array2978[Class338_Sub8_Sub1.anInt6576] = interface1;
						Class338_Sub8_Sub1.anInt6576++;
					}
				}
			}
			if (i != 26629) {
				method119((byte) -31);
			}
			Class359.anInt3090 = 0;
			for (Class296_Sub34 class296_sub34 = Class84.aClass189_924.method1899(i - 26707); class296_sub34 != null; class296_sub34 = Class84.aClass189_924.method1899(-122)) {
				int i_56_ = class296_sub34.method2735(-121);
				if (i_56_ != -1) {
					if (i_56_ == 6) {
						Class359.anInt3090 += class296_sub34.method2738(4);
					} else if (Class365.method3761(i_56_, -20937)) {
						Class403.aClass155_3379.addLast((byte) 98, class296_sub34);
						if (Class403.aClass155_3379.method1580(-125) > 10) {
							Class403.aClass155_3379.method1573(1);
						}
					}
				} else {
					Class161.aClass155_1676.addLast((byte) -51, class296_sub34);
				}
			}
			if (Class212.method2019((byte) 113)) {
				Mesh.method1393((byte) 100);
			}
			if (ObjectDefinition.method761((byte) -10, Class366_Sub6.anInt5392)) {
				Packet.method2583(-98);
				Class131.method1382(51);
			} else if (Class56.method666(Class366_Sub6.anInt5392, 25824)) {
				Class295_Sub2.method2427(-125);
			}
			if (Class355_Sub1.method3701(true, Class366_Sub6.anInt5392) && !Class56.method666(Class366_Sub6.anInt5392, i - 805)) {
				method104(26738);
				Class252.method2206(2);
				Class173.method1686((byte) -74);
			} else if (!BITConfigDefinition.method2353(-8, Class366_Sub6.anInt5392) || Class56.method666(Class366_Sub6.anInt5392, 25824)) {
				if (Class366_Sub6.anInt5392 != 13) {
					if (Class130.method1376(Class366_Sub6.anInt5392, -14723) && !Class56.method666(Class366_Sub6.anInt5392, 25824)) {
						Class369.method3870(31);
					} else if (Class366_Sub6.anInt5392 == 14 || Class366_Sub6.anInt5392 == 15) {
						Class173.method1686((byte) -125);
						if (Class296_Sub39_Sub12.anInt6200 != -3 && Class296_Sub39_Sub12.anInt6200 != 2 && Class296_Sub39_Sub12.anInt6200 != 15) {
							if (Class366_Sub6.anInt5392 != 15) {
								StringNode.logout(3, false);
							} else {
								Class355_Sub1.anInt3693 = AdvancedMemoryCache.anInt1172;
								Class360_Sub10.anInt5349 = Class296_Sub39_Sub12.anInt6200;
								AdvancedMemoryCache.anInt1171 = ParticleEmitterRaw.anInt1771;
								if (Class251.aBoolean2372) {
									Class45.method582(Class309.aClass112_2759.worldId, 7000, Class309.aClass112_2759.ipAddress);
									Class296_Sub45_Sub2.aClass204_6277.aClass154_2045 = null;
									Class41_Sub8.method422(i - 26628, 14);
								} else {
									StringNode.logout(3, Class342.aBoolean2990);
								}
							}
						}
					}
				} else {
					Class173.method1686((byte) -16);
				}
			} else {
				method104(i ^ 0x77);
				Class173.method1686((byte) -77);
			}
			StaticMethods.method2474(Class41_Sub13.aHa3774, true);
			Class403.aClass155_3379.method1573(1);
		}
	}

	@Override
	public final void init() {
		if (method99((byte) -99)) {
			Class338_Sub8.gameWorld = new World();
			Class338_Sub8.gameWorld.worldId = Integer.parseInt(getParameter("worldid"));
			Class338_Sub3_Sub3_Sub1.lobbyWorld = new World();
			Class338_Sub3_Sub3_Sub1.lobbyWorld.worldId = Integer.parseInt(getParameter("lobbyid"));
			Class338_Sub3_Sub3_Sub1.lobbyWorld.ipAddress = getParameter("lobbyaddress");
			Class41_Sub29.modeWhere = InputStream_Sub1.findMode(Integer.parseInt(getParameter("modewhere")), (byte) 84);
			if (Class296_Sub34_Sub2.localModeWhere != Class41_Sub29.modeWhere) {
				if (!Class296_Sub51_Sub27_Sub1.method3160(Class41_Sub29.modeWhere, 30618) && BITConfigsLoader.liveModeWhere != Class41_Sub29.modeWhere) {
					Class41_Sub29.modeWhere = BITConfigsLoader.liveModeWhere;
				}
			} else {
				Class41_Sub29.modeWhere = Class3.office3ModeWhere;
			}
			Class296_Sub17_Sub2.modeWhat = Class299.parseModeWhat(Integer.parseInt(getParameter("modewhat")), -108);
			if (Class296_Sub17_Sub2.modeWhat != Class241_Sub2_Sub2.wipModeWhat && Class296_Sub17_Sub2.modeWhat != Js5TextureLoader.releaseCandidateModeWhat && Class296_Sub51_Sub27.liveModeWhat != Class296_Sub17_Sub2.modeWhat) {
				Class296_Sub17_Sub2.modeWhat = Class296_Sub51_Sub27.liveModeWhat;
			}
			try {
				Class394.langID = Integer.parseInt(getParameter("lang"));
			} catch (Exception exception) {
				Class394.langID = 0;
			}
			String string = getParameter("objecttag");
			if (string != null && string.equals("1")) {
				Class203.objectTag = true;
			} else {
				Class203.objectTag = false;
			}
			String string_57_ = getParameter("js");
			if (string_57_ != null && string_57_.equals("1")) {
				NodeDeque.js = true;
			} else {
				NodeDeque.js = false;
			}
			String string_58_ = getParameter("advert");
			if (string_58_ == null || !string_58_.equals("1")) {
				Class213.advert = false;
			} else {
				Class213.advert = true;
			}
			String string_59_ = getParameter("game");
			if (string_59_ != null) {
				if (string_59_.equals("0")) {
					Class296_Sub50.game = Class363.runescape;
				} else if (!string_59_.equals("1")) {
					if (!string_59_.equals("2")) {
						if (string_59_.equals("3")) {
							Class296_Sub50.game = Class220.game4;
						}
					} else {
						Class296_Sub50.game = Class379.game3;
					}
				} else {
					Class296_Sub50.game = Class296_Sub32.stellardawn;
				}
			}
			try {
				Class209.affliateID = Integer.parseInt(getParameter("affid"));
			} catch (Exception exception) {
				Class209.affliateID = 0;
			}
			Class296_Sub27.quitURL = getParameter("quiturl");
			Class347.settings = getParameter("settings");
			if (Class347.settings == null) {
				Class347.settings = "";
			}
			Class28.under = "1".equals(getParameter("under"));
			String string_60_ = getParameter("country");
			if (string_60_ != null) {
				try {
					Class139.countryID = Integer.parseInt(string_60_);
				} catch (Exception exception) {
					Class139.countryID = 0;
				}
			}
			Class10.colorID = Integer.parseInt(getParameter("colourid"));
			if (Class10.colorID < 0 || Class10.colorID >= ParticleEmitterRaw.aColorArray1756.length) {
				Class10.colorID = 0;
			}
			if (Integer.parseInt(getParameter("sitesettings_member")) == 1) {
				aa_Sub1.memb1 = Class296_Sub22.memb2 = true;
			}
			String string_61_ = getParameter("frombilling");
			if (string_61_ != null && string_61_.equals("true")) {
				Class296_Sub37.fromBilling = true;
			}
			String string_62_ = getParameter("sskey");
			if (string_62_ != null) {
				Class294.sessionKey = Class241_Sub2_Sub2.method2163(79, Class85.method822((byte) 65, string_62_));
				if (Class294.sessionKey.length < 16) {
					Class294.sessionKey = null;
				}
			}
			String string_63_ = getParameter("force64mb");
			if (string_63_ != null && string_63_.equals("true")) {
				Queue.force64MB = true;
			}
			String string_64_ = getParameter("worldflags");
			if (string_64_ != null) {
				try {
					Class83.worldFlags = Integer.parseInt(string_64_);
				} catch (Exception exception) {
					/* empty */
				}
			}
			String string_65_ = getParameter("userFlow");
			if (string_65_ != null) {
				try {
					Class189.userflow = Long.parseLong(string_65_);
				} catch (NumberFormatException numberformatexception) {
					/* empty */
				}
			}
			Class368_Sub2.additionalInfo = getParameter("additionalInfo");
			if (Class368_Sub2.additionalInfo != null && Class368_Sub2.additionalInfo.length() > 50) {
				Class368_Sub2.additionalInfo = null;
			}
			Class246.aClient2332 = this;
			if (Class296_Sub50.game != Class363.runescape) {
				if (Class296_Sub32.stellardawn == Class296_Sub50.game) {
					Class296_Sub15_Sub1.anInt5996 = 480;
					Class368_Sub7.anInt5463 = 640;
				}
			} else {
				Class368_Sub7.anInt5463 = 765;
				Class296_Sub15_Sub1.anInt5996 = 503;
			}
			method89(Class296_Sub50.game.aString338, 37, false, Class368_Sub7.anInt5463, 666, Class296_Sub17_Sub2.modeWhat.method1022((byte) 86) + 32, Class296_Sub15_Sub1.anInt5996);
		}
	}

	public static final void main(String[] strings) {
		try {
			if (strings.length != 6) {
				Class296_Sub20.method2656(true, "Argument count");
			}
			Class338_Sub8.gameWorld = new World();
			Class338_Sub8.gameWorld.worldId = Integer.parseInt(strings[0]);
			Class338_Sub3_Sub3_Sub1.lobbyWorld = new World();
			Class338_Sub3_Sub3_Sub1.lobbyWorld.worldId = Integer.parseInt(strings[1]);
			Class41_Sub29.modeWhere = Class296_Sub34_Sub2.localModeWhere;
			if (!strings[3].equals("live")) {
				if (strings[3].equals("rc")) {
					Class296_Sub17_Sub2.modeWhat = Js5TextureLoader.releaseCandidateModeWhat;
				} else if (strings[3].equals("wip")) {
					Class296_Sub17_Sub2.modeWhat = Class241_Sub2_Sub2.wipModeWhat;
				} else {
					Class296_Sub20.method2656(true, "modewhat");
				}
			} else {
				Class296_Sub17_Sub2.modeWhat = Class296_Sub51_Sub27.liveModeWhat;
			}
			Class394.langID = Class296_Sub42.method2917(strings[4], 41);
			if (Class394.langID == -1) {
				if (strings[4].equals("english")) {
					Class394.langID = 0;
				} else if (!strings[4].equals("german")) {
					Class296_Sub20.method2656(true, "language");
				} else {
					Class394.langID = 1;
				}
			}
			Class203.objectTag = false;
			NodeDeque.js = false;
			if (strings[5].equals("game0")) {
				Class296_Sub50.game = Class363.runescape;
			} else if (strings[5].equals("game1")) {
				Class296_Sub50.game = Class296_Sub32.stellardawn;
			} else if (strings[5].equals("game2")) {
				Class296_Sub50.game = Class379.game3;
			} else if (!strings[5].equals("game3")) {
				Class296_Sub20.method2656(true, "game");
			} else {
				Class296_Sub50.game = Class220.game4;
			}
			Class139.countryID = 0;
			Class209.affliateID = 0;
			Class294.sessionKey = null;
			aa_Sub1.memb1 = Class296_Sub22.memb2 = true;
			Class10.colorID = Class296_Sub50.game.anInt340;
			Class368_Sub2.additionalInfo = null;
			Class189.userflow = 0L;
			Class347.settings = "";
			Queue.force64MB = false;
			Class296_Sub37.fromBilling = false;
			Class83.worldFlags = 0;
			GameClient var_client = new GameClient();
			Class246.aClient2332 = var_client;
			var_client.method84(1024, true, Class296_Sub17_Sub2.modeWhat.method1022((byte) 110) + 32, 666, false, 768, Class296_Sub50.game.aString338, 37);
			Class340.aFrame3707.setLocation(40, 40);
		} catch (Exception exception) {
			Class219_Sub1.method2062(null, (byte) -98, exception);
		}
	}

	@Override
	final void method91(byte i) {
		if (Queue.force64MB) {
			FileWorker.anInt3004 = 64;
		}
		Frame frame = new Frame("Jagex");
		frame.pack();
		frame.dispose();
		StaticMethods.method1598(-17779);
		Class296_Sub51_Sub36.aClass315_6530 = new Class315(Class252.aClass398_2383);
		Class42.aClass285_395 = new Class285();
		Class192.method1923(-65, new int[] { 20, 260 }, new int[] { 1000, 100 });
		if (BITConfigsLoader.liveModeWhere != Class41_Sub29.modeWhere) {
			Class367.aByteArrayArray3124 = new byte[50][];
		}
		Class343_Sub1.aClass296_Sub50_5282 = FileOnDisk.method674((byte) 2);
		if (BITConfigsLoader.liveModeWhere != Class41_Sub29.modeWhere) {
			if (!Class296_Sub51_Sub27_Sub1.method3160(Class41_Sub29.modeWhere, 30618)) {
				if (Class41_Sub29.modeWhere == Class296_Sub34_Sub2.localModeWhere) {
					Class338_Sub8.gameWorld.ipAddress = Constants.HOST_ADDRESS;
					Class338_Sub8.gameWorld.port = Class338_Sub8.gameWorld.worldId + 40000;
					Class338_Sub3_Sub3_Sub1.lobbyWorld.ipAddress = Constants.HOST_ADDRESS;
					Class338_Sub3_Sub3_Sub1.lobbyWorld.port = 40000 + Class338_Sub3_Sub3_Sub1.lobbyWorld.worldId;
					Class338_Sub8.gameWorld.webPort = Class338_Sub8.gameWorld.worldId + 50000;
					Class338_Sub3_Sub3_Sub1.lobbyWorld.webPort = 50000 + Class338_Sub3_Sub3_Sub1.lobbyWorld.worldId;
				}
			} else {
				Class338_Sub8.gameWorld.ipAddress = getCodeBase().getHost();
				Class338_Sub8.gameWorld.port = Class338_Sub8.gameWorld.worldId + 40000;
				Class338_Sub8.gameWorld.webPort = Class338_Sub8.gameWorld.worldId + 50000;
				Class338_Sub3_Sub3_Sub1.lobbyWorld.port = Class338_Sub3_Sub3_Sub1.lobbyWorld.worldId + 40000;
				Class338_Sub3_Sub3_Sub1.lobbyWorld.webPort = Class338_Sub3_Sub3_Sub1.lobbyWorld.worldId + 50000;
			}
		} else {
			Class338_Sub8.gameWorld.ipAddress = getCodeBase().getHost();
		}
		Class296_Sub51_Sub27_Sub1.aClass112_6733 = Class338_Sub8.gameWorld;
		Class12.aShortArray137 = Class366_Sub5.aShortArray5381 = Class41_Sub7.aShortArray3762 = Class296_Sub9.aShortArray4630 = new short[256];
		if (Class296_Sub50.game == Class363.runescape) {
			SubCache.aBoolean2709 = false;
		}
		try {
			if (i > -62) {
				method120(7, 92);
			}
			Class296_Sub51_Sub10.aClipboard6394 = Class246.aClient2332.getToolkit().getSystemClipboard();
		} catch (Exception exception) {
			/* empty */
		}
		Class2.aClass166_66 = Class242.method2170(Class230.aCanvas2209, 24898);
		Class84.aClass189_924 = s_Sub2.method3367(true, Class230.aCanvas2209, (byte) -120);
		try {
			if (Class252.aClass398_2383.aClass58_3331 != null) {
				Class321.dataFile = new SeekableFile(Class252.aClass398_2383.aClass58_3331, 5200, 0);
				for (int i_66_ = 0; i_66_ < 37; i_66_++) {
					Class392.aClass255Array3491[i_66_] = new SeekableFile(Class252.aClass398_2383.aClass58Array3336[i_66_], 6000, 0);
				}
				Class261.aClass255_2423 = new SeekableFile(Class252.aClass398_2383.aClass58_3326, 6000, 0);
				Class368_Sub23.aClass168_5574 = new Js5DiskStore(255, Class321.dataFile, Class261.aClass255_2423, 500000);
				Applet_Sub1.aClass255_10 = new SeekableFile(Class252.aClass398_2383.aClass58_3340, 24, 0);
				Class252.aClass398_2383.aClass58Array3336 = null;
				Class252.aClass398_2383.aClass58_3340 = null;
				Class252.aClass398_2383.aClass58_3326 = null;
				Class252.aClass398_2383.aClass58_3331 = null;
			}
		} catch (java.io.IOException ioexception) {
			Applet_Sub1.aClass255_10 = null;
			Class261.aClass255_2423 = null;
			Class368_Sub23.aClass168_5574 = null;
			Class321.dataFile = null;
		}
		if (BITConfigsLoader.liveModeWhere != Class41_Sub29.modeWhere) {
			Class68.drawFPS = true;
		}
		Class55.aString657 = TranslatableString.aClass120_1208.getTranslation(Class394.langID);
	}

	private final void method113(int i) {
		if (i == 3879) {
			if (Class87.anInt940 < Class42.aClass285_395.anInt2633) {
				Class296_Sub51_Sub27_Sub1.aClass112_6733.method978((byte) 127);
				Class243.anInt2316 = (Class42.aClass285_395.anInt2633 * 50 - 50) * 5;
				if (Class243.anInt2316 > 3000) {
					Class243.anInt2316 = 3000;
				}
				if (Class42.aClass285_395.anInt2633 >= 2 && Class42.aClass285_395.anInt2635 == 6) {
					method94((byte) -45, "js5connect_outofdate");
					Class366_Sub6.anInt5392 = 16;
					return;
				}
				if (Class42.aClass285_395.anInt2633 >= 4 && Class42.aClass285_395.anInt2635 == -1) {
					method94((byte) -45, "js5crc");
					Class366_Sub6.anInt5392 = 16;
					return;
				}
				if (Class42.aClass285_395.anInt2633 >= 4 && ObjectDefinition.method761((byte) -10, Class366_Sub6.anInt5392)) {
					if (Class42.aClass285_395.anInt2635 != 7 && Class42.aClass285_395.anInt2635 != 9) {
						if (Class42.aClass285_395.anInt2635 > 0) {
							if (Class296_Sub29.aString4812 != null) {
								method94((byte) -45, "js5proxy_" + Class296_Sub29.aString4812.trim());
							} else {
								method94((byte) -45, "js5connect");
							}
						} else {
							method94((byte) -45, "js5io");
						}
					} else {
						method94((byte) -45, "js5connect_full");
					}
					Class366_Sub6.anInt5392 = 16;
					return;
				}
			}
			Class87.anInt940 = Class42.aClass285_395.anInt2633;
			if (Class243.anInt2316 > 0) {
				Class243.anInt2316--;
			} else {
				do {
					try {
						if (ParamType.anInt3246 == 0) {
							Class360_Sub3.aClass278_5311 = Class296_Sub51_Sub27_Sub1.aClass112_6733.createSocket(43594, Class252.aClass398_2383);
							ParamType.anInt3246++;
						}
						if (ParamType.anInt3246 == 1) {
							if (Class360_Sub3.aClass278_5311.anInt2540 == 2) {
								if (Class360_Sub3.aClass278_5311.anObject2539 != null) {
									Class296_Sub29.aString4812 = (String) Class360_Sub3.aClass278_5311.anObject2539;
								}
								method120(-127, 1000);
								break;
							}
							if (Class360_Sub3.aClass278_5311.anInt2540 == 1) {
								ParamType.anInt3246++;
							}
						}
						if (ParamType.anInt3246 == 2) {
							StaticMethods.aClass36_5938 = new Class36((Socket) Class360_Sub3.aClass278_5311.anObject2539, Class252.aClass398_2383, 25000);
							Packet class296_sub17 = new Packet(5);
							class296_sub17.p1(Class292.aClass269_2667.protocolId);
							class296_sub17.p4(666);
							StaticMethods.aClass36_5938.method358(0, class296_sub17.data, 5, (byte) 120);
							ParamType.anInt3246++;
							Class396.aLong3319 = Class72.method771(-121);
						}
						if (ParamType.anInt3246 == 3) {
							if (ObjectDefinition.method761((byte) -10, Class366_Sub6.anInt5392) || StaticMethods.aClass36_5938.method354(false) > 0) {
								int i_67_ = StaticMethods.aClass36_5938.method360(51);
								if (i_67_ != 0) {
									method120(63, i_67_);
									break;
								}
								ParamType.anInt3246++;
							} else if (-Class396.aLong3319 + Class72.method771(-128) > 30000L) {
								method120(66, 1001);
								break;
							}
						}
						if (ParamType.anInt3246 != 4) {
							break;
						}
						boolean bool = ObjectDefinition.method761((byte) -10, Class366_Sub6.anInt5392) || Class355_Sub1.method3701(true, Class366_Sub6.anInt5392) || BITConfigDefinition.method2353(-8, Class366_Sub6.anInt5392);
						Class200[] class200s = Class200.method1948((byte) -104);
						Packet class296_sub17 = new Packet(class200s.length * 4);
						StaticMethods.aClass36_5938.method355(1163625227, 0, class296_sub17.data.length, class296_sub17.data);
						for (Class200 class200 : class200s) {
							class200.method1949((byte) 92, class296_sub17.g4());
						}
						Class42.aClass285_395.method2381((byte) -128, StaticMethods.aClass36_5938, !bool);
						ParamType.anInt3246 = 0;
						StaticMethods.aClass36_5938 = null;
						Class360_Sub3.aClass278_5311 = null;
					} catch (java.io.IOException ioexception) {
						method120(i ^ ~0xf50, 1002);
					}
					break;
				} while (false);
			}
		}
	}

	static final void method114() {
		int i = PlayerUpdate.visiblePlayersCount;
		int[] is = PlayerUpdate.visiblePlayersIndexes;
		int i_69_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub11_5023.method437(115);
		boolean bool = i_69_ == 1 && i > 200 || i_69_ == 0 && i > 50;
		for (int i_70_ = 0; i_70_ < i; i_70_++) {
			Player class338_sub3_sub1_sub3_sub1 = PlayerUpdate.visiblePlayers[is[i_70_]];
			if (!class338_sub3_sub1_sub3_sub1.hasComposite()) {
				class338_sub3_sub1_sub3_sub1.anInt6766 = -1;
			} else if (class338_sub3_sub1_sub3_sub1.somethingWithIgnores) {
				class338_sub3_sub1_sub3_sub1.anInt6766 = -1;
			} else {
				class338_sub3_sub1_sub3_sub1.method3477(-1);
				if (class338_sub3_sub1_sub3_sub1.aShort6560 < 0 || class338_sub3_sub1_sub3_sub1.aShort6564 < 0 || class338_sub3_sub1_sub3_sub1.aShort6559 >= Class198.currentMapSizeX || class338_sub3_sub1_sub3_sub1.aShort6558 >= Class296_Sub38.currentMapSizeY) {
					class338_sub3_sub1_sub3_sub1.anInt6766 = -1;
				} else {
					class338_sub3_sub1_sub3_sub1.aBoolean6870 = class338_sub3_sub1_sub3_sub1.aBoolean6783 ? bool : false;
					if (class338_sub3_sub1_sub3_sub1 == Class296_Sub51_Sub11.localPlayer) {
						class338_sub3_sub1_sub3_sub1.anInt6766 = 2147483647;
					} else {
						int i_71_ = 0;
						if (!class338_sub3_sub1_sub3_sub1.aBoolean6774) {
							i_71_++;
						}
						if (class338_sub3_sub1_sub3_sub1.anInt6779 > Class29.anInt307) {
							i_71_ += 2;
						}
						i_71_ += 5 - class338_sub3_sub1_sub3_sub1.getSize() << 2;
						if (class338_sub3_sub1_sub3_sub1.aBoolean6879 || class338_sub3_sub1_sub3_sub1.aBoolean6868) {
							i_71_ += 512;
						} else {
							if (Class166_Sub1.anInt4301 == 0) {
								i_71_ += 32;
							} else {
								i_71_ += 128;
							}
							i_71_ += 256;
						}
						class338_sub3_sub1_sub3_sub1.anInt6766 = i_71_ + 1;
					}
				}
			}
		}
		for (int i_72_ = 0; i_72_ < Class367.npcsCount; i_72_++) {
			NPC class338_sub3_sub1_sub3_sub2 = ((NPCNode) Class41_Sub18.localNpcs.get(ReferenceTable.npcIndexes[i_72_])).value;
			if (!class338_sub3_sub1_sub3_sub2.hasDefinition() || !class338_sub3_sub1_sub3_sub2.definition.method1501(Class16_Sub3_Sub1.configsRegister, (byte) -113)) {
				class338_sub3_sub1_sub3_sub2.anInt6766 = -1;
			} else {
				class338_sub3_sub1_sub3_sub2.method3477(-1);
				if (class338_sub3_sub1_sub3_sub2.aShort6560 < 0 || class338_sub3_sub1_sub3_sub2.aShort6564 < 0 || class338_sub3_sub1_sub3_sub2.aShort6559 >= Class198.currentMapSizeX || class338_sub3_sub1_sub3_sub2.aShort6558 >= Class296_Sub38.currentMapSizeY) {
					class338_sub3_sub1_sub3_sub2.anInt6766 = -1;
				} else {
					int i_73_ = 0;
					if (!class338_sub3_sub1_sub3_sub2.aBoolean6774) {
						i_73_++;
					}
					if (class338_sub3_sub1_sub3_sub2.anInt6779 > Class29.anInt307) {
						i_73_ += 2;
					}
					i_73_ += 5 - class338_sub3_sub1_sub3_sub2.getSize() << 2;
					if (Class166_Sub1.anInt4301 == 0) {
						if (class338_sub3_sub1_sub3_sub2.definition.aBoolean1511) {
							i_73_ += 64;
						} else {
							i_73_ += 128;
						}
					} else if (Class166_Sub1.anInt4301 == 1) {
						if (class338_sub3_sub1_sub3_sub2.definition.aBoolean1511) {
							i_73_ += 32;
						} else {
							i_73_ += 64;
						}
					}
					if (class338_sub3_sub1_sub3_sub2.definition.aBoolean1503) {
						i_73_ += 1024;
					} else if (!class338_sub3_sub1_sub3_sub2.definition.aBoolean1483) {
						i_73_ += 256;
					}
					class338_sub3_sub1_sub3_sub2.anInt6766 = i_73_ + 1;
				}
			}
		}
		for (Class225 class225 : Class338_Sub2.aClass225Array5200) {
			if (class225 != null) {
				if (class225.iconType == 1) {
					NPCNode class296_sub7 = (NPCNode) Class41_Sub18.localNpcs.get(class225.targetIndex);
					if (class296_sub7 != null) {
						NPC class338_sub3_sub1_sub3_sub2 = class296_sub7.value;
						if (class338_sub3_sub1_sub3_sub2.anInt6766 >= 0) {
							class338_sub3_sub1_sub3_sub2.anInt6766 += 2048;
						}
					}
				} else if (class225.iconType == 10) {
					Player class338_sub3_sub1_sub3_sub1 = PlayerUpdate.visiblePlayers[class225.targetIndex];
					if (class338_sub3_sub1_sub3_sub1 != null && class338_sub3_sub1_sub3_sub1 != Class296_Sub51_Sub11.localPlayer && class338_sub3_sub1_sub3_sub1.anInt6766 >= 0) {
						class338_sub3_sub1_sub3_sub1.anInt6766 += 2048;
					}
				}
			}
		}
	}

	static final InterfaceComponentSettings method115(InterfaceComponent class51) {
		InterfaceComponentSettings class296_sub31 = (InterfaceComponentSettings) Class149.interfaceSettings.get(((long) class51.uid << 32) + class51.anInt592);
		if (class296_sub31 != null) {
			return class296_sub31;
		}
		return class51.settings_;
	}

	private final void method116(int i) {
		if (i != -123) {
			aClass231_3716 = null;
		}
		if (Class366_Sub6.anInt5392 != 16) {
			long l = NodeDeque.method1572(i ^ ~0x71) / 1000000L + -StaticMethods.aLong5950;
			StaticMethods.aLong5950 = NodeDeque.method1572(55) / 1000000L;
			boolean bool = ModeWhere.method2192(27651);
			if (bool && Class124.aBoolean1282 && Class274.aClass381_2524 != null) {
				Class274.aClass381_2524.method3994(103);
			}
			if (Class196.method1938(i ^ 0x16, Class366_Sub6.anInt5392)) {
				if (Class368_Sub5_Sub1.aLong6594 != 0L && Class368_Sub5_Sub1.aLong6594 < Class72.method771(-113)) {
					Class104.method904(Class8.method192((byte) 101), false, 0, Class41_Sub29.anInt3820, Class393.anInt3303);
				} else if (!Class41_Sub13.aHa3774.v() && Class296_Sub51_Sub19.aBoolean6438) {
					Class42_Sub1.method533(true);
				}
			}
			if (Animator.aFrame435 == null) {
				java.awt.Container container;
				if (Class340.aFrame3707 == null) {
					if (CS2Script.anApplet6140 == null) {
						container = Class55.anApplet_Sub1_656;
					} else {
						container = CS2Script.anApplet6140;
					}
				} else {
					container = Class340.aFrame3707;
				}
				int i_75_ = container.getSize().width;
				int i_76_ = container.getSize().height;
				if (container == Class340.aFrame3707) {
					Insets insets = Class340.aFrame3707.getInsets();
					i_75_ -= insets.right + insets.left;
					i_76_ -= insets.bottom + insets.top;
				}
				if (StaticMethods.anInt1838 != i_75_ || Class152.anInt1568 != i_76_ || Class380.aBoolean3210) {
					if (Class41_Sub13.aHa3774 == null || Class41_Sub13.aHa3774.j()) {
						StaticMethods.method1598(-17779);
					} else {
						Class152.anInt1568 = i_76_;
						StaticMethods.anInt1838 = i_75_;
					}
					Class368_Sub5_Sub1.aLong6594 = Class72.method771(i ^ 0x13) - -500L;
					Class380.aBoolean3210 = false;
				}
			}
			if (Animator.aFrame435 != null && !Class41.aBoolean390 && Class196.method1938(i - 4, Class366_Sub6.anInt5392)) {
				Class104.method904(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub13_5003.method444(115), false, i + 123, -1, -1);
			}
			boolean bool_77_ = false;
			if (Class78.aBoolean3430) {
				bool_77_ = true;
				Class78.aBoolean3430 = false;
			}
			if (bool_77_) {
				Class367.method3802(i ^ 0x7a);
			}
			if (Class41_Sub13.aHa3774 != null && Class41_Sub13.aHa3774.v() || Class8.method192((byte) 101) != 1) {
				Class366_Sub8.method3794(true);
			}
			if (ObjectDefinition.method761((byte) -10, Class366_Sub6.anInt5392)) {
				Class360_Sub6.method3747(1, bool_77_);
			} else if (Class241_Sub2.method2158(Class366_Sub6.anInt5392, -125)) {
				Class183.method1855((byte) -85);
			} else if (Class96.method870(Class366_Sub6.anInt5392, (byte) 62)) {
				Class183.method1855((byte) -97);
			} else if (!Class56.method666(Class366_Sub6.anInt5392, 25824)) {
				if (Class366_Sub6.anInt5392 == 11) {
					aa_Sub2.method158(l, 100);
				} else if (Class366_Sub6.anInt5392 != 14) {
					if (Class366_Sub6.anInt5392 == 15) {
						ModeWhat.method1021(Class123_Sub1_Sub1.aClass92_5814, 8493, TranslatableString.aClass120_1227.getTranslation(Class394.langID), Class41_Sub13.aHa3774, false, Class205_Sub1.aClass55_5642);
					}
				} else {
					ModeWhat.method1021(Class123_Sub1_Sub1.aClass92_5814, 8493, TranslatableString.aClass120_1210.getTranslation(Class394.langID) + "<br>" + TranslatableString.aClass120_1211.getTranslation(Class394.langID), Class41_Sub13.aHa3774, false, Class205_Sub1.aClass55_5642);
				}
			} else if (StaticMethods.anInt5969 != 1) {
				if (StaticMethods.anInt5969 != 2) {
					ModeWhat.method1021(Class123_Sub1_Sub1.aClass92_5814, 8493, TranslatableString.aClass120_1208.getTranslation(Class394.langID), Class41_Sub13.aHa3774, true, Class205_Sub1.aClass55_5642);
				} else {
					if (Class240.anInt2264 < Class117.anInt1187) {
						Class240.anInt2264 = Class117.anInt1187;
					}
					int i_78_ = 50 + (-Class117.anInt1187 + Class240.anInt2264) * 50 / Class240.anInt2264;
					ModeWhat.method1021(Class123_Sub1_Sub1.aClass92_5814, i + 8616, TranslatableString.aClass120_1208.getTranslation(Class394.langID) + "<br>(" + i_78_ + "%)", Class41_Sub13.aHa3774, true, Class205_Sub1.aClass55_5642);
				}
			} else {
				if (CS2Stack.anInt2252 < OutputStream_Sub1.anInt40) {
					CS2Stack.anInt2252 = OutputStream_Sub1.anInt40;
				}
				int i_79_ = (CS2Stack.anInt2252 - OutputStream_Sub1.anInt40) * 50 / CS2Stack.anInt2252;
				ModeWhat.method1021(Class123_Sub1_Sub1.aClass92_5814, 8493, TranslatableString.aClass120_1208.getTranslation(Class394.langID) + "<br>(" + i_79_ + "%)", Class41_Sub13.aHa3774, true, Class205_Sub1.aClass55_5642);
			}
			if (Class296_Sub51_Sub28.anInt6487 == 3) {
				for (int i_80_ = 0; Class154_Sub1.anInt4291 > i_80_; i_80_++) {
					Rectangle rectangle = s.aRectangleArray2837[i_80_];
					if (!StaticMethods.aBooleanArray6066[i_80_]) {
						if (Class93.aBooleanArray1002[i_80_]) {
							Class41_Sub13.aHa3774.method1086(rectangle.y, rectangle.x, -65536, (byte) 123, rectangle.height, rectangle.width);
						} else {
							Class41_Sub13.aHa3774.method1086(rectangle.y, rectangle.x, -16711936, (byte) 44, rectangle.height, rectangle.width);
						}
					} else {
						Class41_Sub13.aHa3774.method1086(rectangle.y, rectangle.x, -65281, (byte) -34, rectangle.height, rectangle.width);
					}
				}
			}
			if (Class212.method2019((byte) 83)) {
				Class100.method880(Class41_Sub13.aHa3774, -128);
			}
			if (!Class252.aClass398_2383.aBoolean3321 || !Class196.method1938(i + 8, Class366_Sub6.anInt5392) || Class296_Sub51_Sub28.anInt6487 != 0 || Class8.method192((byte) 101) != 1 || bool_77_) {
				if (!ObjectDefinition.method761((byte) -10, Class366_Sub6.anInt5392)) {
					for (int i_81_ = 0; Class154_Sub1.anInt4291 > i_81_; i_81_++) {
						Class93.aBooleanArray1002[i_81_] = false;
					}
					try {
						if (Class368_Sub5_Sub2.aBoolean6597) {
							Class296_Sub51_Sub33.method3180(false);
						} else {
							Class41_Sub13.aHa3774.method1090((byte) -87);
						}
					} catch (Exception_Sub1 exception_sub1) {
						Class219_Sub1.method2062(exception_sub1.getMessage() + " (Recovered) " + method102((byte) -37), (byte) 94, exception_sub1);
						Class33.method348(false, false, 0);
					}
				}
			} else {
				int i_82_ = 0;
				for (int i_83_ = 0; Class154_Sub1.anInt4291 > i_83_; i_83_++) {
					if (Class93.aBooleanArray1002[i_83_]) {
						Class93.aBooleanArray1002[i_83_] = false;
						ReferenceTable.aRectangleArray980[i_82_++] = s.aRectangleArray2837[i_83_];
					}
				}
				try {
					if (Class368_Sub5_Sub2.aBoolean6597) {
						Class366_Sub6.method3788(24, ReferenceTable.aRectangleArray980, i_82_);
					} else {
						Class41_Sub13.aHa3774.method1098(2, i_82_, ReferenceTable.aRectangleArray980);
					}
				} catch (Exception_Sub1 exception_sub1) {
					/* empty */
				}
			}
			Class360_Sub3.method3740(790);
			int i_84_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub12_4992.method440(125);
			if (i_84_ != 0) {
				if (i_84_ == 1) {
					Class106_Sub1.method942(10L, i + 123);
				} else if (i_84_ != 2) {
					if (i_84_ == 3) {
						Class106_Sub1.method942(2L, 0);
					}
				} else {
					Class106_Sub1.method942(5L, 0);
				}
			} else {
				Class106_Sub1.method942(15L, 0);
			}
			if (Class154.aBoolean1584) {
				Class300.method3249(-124);
			}
			if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub5_5000.method414(i ^ ~0x6) == 1 && Class366_Sub6.anInt5392 == 3 && Class99.anInt1064 != -1) {
				Class343_Sub1.aClass296_Sub50_5282.method3060(0, i ^ ~0x56, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub5_5000);
				Class368_Sub4.method3820(i ^ ~0x7b);
			}
		}
	}

	static final byte[] method117(boolean bool, int i) {
		if (bool != true) {
			aClass231_3716 = null;
		}
		Class296_Sub39_Sub19 class296_sub39_sub19 = (Class296_Sub39_Sub19) HashTable.aClass301_2465.get(i);
		if (class296_sub39_sub19 == null) {
			byte[] is = new byte[512];
			Random random = new Random(i);
			for (int i_85_ = 0; i_85_ < 255; i_85_++) {
				is[i_85_] = (byte) i_85_;
			}
			for (int i_86_ = 0; i_86_ < 255; i_86_++) {
				int i_87_ = -i_86_ + 255;
				int i_88_ = s_Sub3.method3373(i_87_, random, 6445);
				byte i_89_ = is[i_88_];
				is[i_88_] = is[i_87_];
				is[i_87_] = is[-i_86_ + 511] = i_89_;
			}
			class296_sub39_sub19 = new Class296_Sub39_Sub19(is);
			HashTable.aClass301_2465.put(i, class296_sub39_sub19);
		}
		return class296_sub39_sub19.aByteArray6249;
	}

	static final InterfaceComponent method118(InterfaceComponent class51) {
		int i = method115(class51).method2705(7);
		if (i == 0) {
			return null;
		}
		for (int i_90_ = 0; i_90_ < i; i_90_++) {
			class51 = InterfaceComponent.getInterfaceComponent(class51.parentId);
			if (class51 == null) {
				return null;
			}
		}
		return class51;
	}

	public static void method119(byte i) {
		aClass311_3715 = null;
		if (i != -125) {
			method107(34);
		}
		aClass296_Sub48_3717 = null;
		aClass231_3716 = null;
	}

	private final void method120(int i, int i_91_) {
		Class360_Sub3.aClass278_5311 = null;
		Class42.aClass285_395.anInt2633++;
		Class42.aClass285_395.anInt2635 = i_91_;
		int i_92_ = -10 % ((-83 - i) / 32);
		ParamType.anInt3246 = 0;
		StaticMethods.aClass36_5938 = null;
	}

	@Override
	final void method83(int i) {
		if (i != 24063) {
			method102((byte) -17);
		}
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(121) == 2) {
			try {
				method116(i - 24186);
			} catch (Throwable throwable) {
				Class219_Sub1.method2062(throwable.getMessage() + " (Recovered) " + method102((byte) -112), (byte) 61, throwable);
				Class296_Sub51_Sub30.aBoolean6498 = true;
				Class33.method348(false, false, 0);
			}
		} else {
			method116(i - 24186);
		}
	}

	static final void method121(InterfaceComponent[] class51s, int i, int i_93_, int i_94_, int i_95_, int i_96_, int i_97_, int i_98_, int i_99_, int i_100_, int i_101_, int i_102_) {
		for (int i_103_ = 0; i_103_ < class51s.length; i_103_++) {
			InterfaceComponent class51 = class51s[i_103_];
			if (class51 != null && class51.parentId == i) {
				int i_104_ = class51.anInt614 + i_97_;
				int i_105_ = class51.anInt605 + i_98_;
				int i_106_;
				int i_107_;
				int i_108_;
				int i_109_;
				if (class51.type == 2) {
					i_106_ = i_93_;
					i_107_ = i_94_;
					i_108_ = i_95_;
					i_109_ = i_96_;
				} else {
					int i_110_ = i_104_ + class51.anInt578;
					int i_111_ = i_105_ + class51.anInt623;
					if (class51.type == 9) {
						i_110_++;
						i_111_++;
					}
					i_106_ = i_104_ > i_93_ ? i_104_ : i_93_;
					i_107_ = i_105_ > i_94_ ? i_105_ : i_94_;
					i_108_ = i_110_ < i_95_ ? i_110_ : i_95_;
					i_109_ = i_111_ < i_96_ ? i_111_ : i_96_;
				}
				if (class51.type != 0 && !class51.hasScripts && method115(class51).settingsHash == 0 && class51 != StaticMethods.aClass51_4842 && class51.contentType != Class296_Sub15_Sub4.anInt6037 && class51.contentType != Class137.anInt1409 && class51.contentType != Class83.anInt918 && class51.contentType != StaticMethods.anInt5935) {
					if (i_106_ < i_108_ && i_107_ < i_109_) {
						Class266.method2289(class51, -29278);
					}
				} else if (!method110(class51)) {
					int i_112_ = 0;
					int i_113_ = 0;
					if (Class368_Sub5_Sub2.aBoolean6597) {
						i_112_ = Class387.method4034(true);
						i_113_ = GraphicsLoader.method2286(true);
					}
					if (class51 == InvisiblePlayer.aClass51_1982 && Class368_Sub3.method3814((byte) -53, InvisiblePlayer.aClass51_1982) != null) {
						PlayerEquipment.aBoolean3370 = true;
						ByteStream.anInt6040 = i_104_;
						Class21.anInt248 = i_105_;
					}
					if (class51.aBoolean482 || i_106_ < i_108_ && i_107_ < i_109_) {
						if (class51.aBoolean610 && i_101_ >= i_106_ && i_102_ >= i_107_ && i_101_ < i_108_ && i_102_ < i_109_) {
							for (CS2Call class296_sub46 = (CS2Call) Class179.aClass155_1854.removeFirst((byte) 122); class296_sub46 != null; class296_sub46 = (CS2Call) Class179.aClass155_1854.removeNext(1001)) {
								if (class296_sub46.aBoolean4960) {
									class296_sub46.unlink();
									class296_sub46.callerInterface.aBoolean524 = false;
								}
							}
							if (ha.anInt1293 == 0) {
								InvisiblePlayer.aClass51_1982 = null;
								StaticMethods.aClass51_4842 = null;
							}
							Class2.anInt59 = 0;
							StringNode.aBoolean4622 = false;
							Class360_Sub6.aBoolean5333 = false;
							if (!Class318.aBoolean2814) {
								Class105_Sub2.method915(0);
							}
						}
						boolean bool = class51.aBoolean550 && class51.type == 5 && class51.anInt517 == 0 && class51.anInt542 < 0 && class51.clickedItem == -1 && class51.anInt558 == -1 && !class51.aBoolean562 && class51.anInt573 == 0;
						boolean bool_114_ = false;
						if (Class84.aClass189_924.method1895((byte) -55) + i_112_ >= i_106_ && Class84.aClass189_924.method1897(0) + i_113_ >= i_107_ && Class84.aClass189_924.method1895((byte) -55) + i_112_ < i_108_ && Class84.aClass189_924.method1897(0) + i_113_ < i_109_) {
							if (bool) {
								Class98 class98 = class51.method633((byte) -87, Class41_Sub13.aHa3774);
								if (class98 == null || class98.anInt1059 != class51.anInt578 || class98.anInt1052 != class51.anInt623) {
									bool_114_ = true;
								} else {
									int i_115_ = Class84.aClass189_924.method1895((byte) -55) + i_112_ - i_104_;
									int i_116_ = Class84.aClass189_924.method1897(0) + i_113_ - i_105_;
									if (i_116_ >= 0 && i_116_ < class98.anIntArray1054.length) {
										int i_117_ = class98.anIntArray1054[i_116_];
										if (i_115_ >= i_117_ && i_115_ <= i_117_ + class98.anIntArray1053[i_116_]) {
											bool_114_ = true;
										}
									}
								}
							} else {
								bool_114_ = true;
							}
						}
						if (!Class127.aBoolean1304 && bool_114_) {
							if (class51.anInt611 >= 0) {
								Class41_Sub19.anInt3793 = class51.anInt611;
							} else if (class51.aBoolean610) {
								Class41_Sub19.anInt3793 = -1;
							}
						}
						if (!Class318.aBoolean2814 && bool_114_) {
							Class296_Sub39_Sub7.method2819(i_101_ - i_105_, -128, i_101_ - i_104_, class51);
						}
						boolean bool_118_ = false;
						if (Class84.aClass189_924.method1901((byte) -94) && bool_114_) {
							bool_118_ = true;
						}
						boolean bool_119_ = false;
						Class296_Sub34 class296_sub34 = (Class296_Sub34) Class403.aClass155_3379.removeFirst((byte) 112);
						if (class296_sub34 != null && class296_sub34.method2735(-120) == 0 && class296_sub34.method2732(-6) >= i_106_ && class296_sub34.method2736(-6) >= i_107_ && class296_sub34.method2732(-6) < i_108_ && class296_sub34.method2736(-6) < i_109_) {
							if (bool) {
								Class98 class98 = class51.method633((byte) -74, Class41_Sub13.aHa3774);
								if (class98 == null || class98.anInt1059 != class51.anInt578 || class98.anInt1052 != class51.anInt623) {
									bool_119_ = true;
								} else {
									int i_120_ = class296_sub34.method2732(-6) - i_104_;
									int i_121_ = class296_sub34.method2736(-6) - i_105_;
									if (i_121_ >= 0 && i_121_ < class98.anIntArray1054.length) {
										int i_122_ = class98.anIntArray1054[i_121_];
										if (i_120_ >= i_122_ && i_120_ <= i_122_ + class98.anIntArray1053[i_121_]) {
											bool_119_ = true;
										}
									}
								}
							} else {
								bool_119_ = true;
							}
						}
						if (class51.aByteArray508 != null && !Class212.method2019((byte) 126)) {
							for (int i_123_ = 0; i_123_ < class51.aByteArray508.length; i_123_++) {
								if (!Class2.aClass166_66.method1637(class51.aByteArray508[i_123_], 54)) {
									if (class51.anIntArray599 != null) {
										class51.anIntArray599[i_123_] = 0;
									}
								} else if (class51.anIntArray599 == null || Class29.anInt307 >= class51.anIntArray599[i_123_]) {
									byte i_124_ = class51.aByteArray495[i_123_];
									if (i_124_ == 0 || ((i_124_ & 0x8) == 0 || !Class2.aClass166_66.method1637(86, 51) && !Class2.aClass166_66.method1637(82, 101) && !Class2.aClass166_66.method1637(81, 79)) && ((i_124_ & 0x2) == 0 || Class2.aClass166_66.method1637(86, 97)) && ((i_124_ & 0x1) == 0 || Class2.aClass166_66.method1637(82, 104)) && ((i_124_ & 0x4) == 0 || Class2.aClass166_66.method1637(81, 41))) {
										if (i_123_ < 10) {
											Class296_Sub39_Sub18.interfaceClicked(i_123_ + 1, class51.uid, "", -1);
										} else if (i_123_ == 10) {
											Class285.method2368(0);
											InterfaceComponentSettings class296_sub31 = method115(class51);
											Class3.method172(class296_sub31.originalHash, class296_sub31.method2707(-83), class51, 22635);
											Class228.aString2200 = Class368_Sub19.method3856(class51, 126);
											if (Class228.aString2200 == null) {
												Class228.aString2200 = "Null";
											}
											ConfigsRegister.aString3672 = class51.componentName + "<col=ffffff>";
										}
										int i_125_ = class51.anIntArray572[i_123_];
										if (class51.anIntArray599 == null) {
											class51.anIntArray599 = new int[class51.aByteArray508.length];
										}
										if (i_125_ != 0) {
											class51.anIntArray599[i_123_] = Class29.anInt307 + i_125_;
										} else {
											class51.anIntArray599[i_123_] = 2147483647;
										}
									}
								}
							}
						}
						if (bool_119_) {
							Class296_Sub39_Sub4.method2800(i_113_ + class296_sub34.method2736(-6) - i_105_, 0, i_112_ + class296_sub34.method2732(-6) - i_104_, class51);
						}
						if (InvisiblePlayer.aClass51_1982 != null && InvisiblePlayer.aClass51_1982 != class51 && bool_114_ && method115(class51).method2702(true)) {
							Class41_Sub28.aClass51_3814 = class51;
						}
						if (class51 == StaticMethods.aClass51_4842) {
							aa_Sub2.aBoolean3727 = true;
							LookupTable.anInt52 = i_104_;
							Class296_Sub34_Sub2.anInt6096 = i_105_;
						}
						if (class51.hasScripts || class51.contentType != 0) {
							if (bool_114_ && Class359.anInt3090 != 0 && class51.anObjectArray527 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.aBoolean4960 = true;
								class296_sub46.callerInterface = class51;
								class296_sub46.anInt4964 = Class359.anInt3090;
								class296_sub46.callArgs = class51.anObjectArray527;
								Class179.aClass155_1854.addLast((byte) -93, class296_sub46);
							}
							if (InvisiblePlayer.aClass51_1982 != null) {
								bool_119_ = false;
								bool_118_ = false;
							} else if (Class318.aBoolean2814 || class51.contentType != Class368_Sub5.anInt5447 && Class2.anInt59 > 0) {
								bool_119_ = false;
								bool_118_ = false;
								bool_114_ = false;
							}
							if (class51.contentType != 0) {
								if (class51.contentType == Class83.anInt918 || class51.contentType == StaticMethods.anInt5935) {
									Class187.aClass51_1923 = class51;
									if (s_Sub3.aClass262_5164 != null) {
										s_Sub3.aClass262_5164.method2258(true, Class41_Sub13.aHa3774, class51.anInt623, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011.method468(122));
									}
									if (class51.contentType == Class83.anInt918) {
										if (!Class318.aBoolean2814 && i_101_ >= i_106_ && i_102_ >= i_107_ && i_101_ < i_108_ && i_102_ < i_109_) {
											Statics.method985(i_99_, Class41_Sub13.aHa3774, (byte) -15, i_100_);
											for (Class338_Sub7 class338_sub7 = (Class338_Sub7) Class44_Sub1_Sub1.aClass404_5809.method4160((byte) -105); class338_sub7 != null; class338_sub7 = (Class338_Sub7) Class44_Sub1_Sub1.aClass404_5809.method4163(-24917)) {
												if (i_101_ >= class338_sub7.anInt5247 && i_101_ < class338_sub7.anInt5249 && i_102_ >= class338_sub7.anInt5248 && i_102_ < class338_sub7.anInt5252) {
													Class105_Sub2.method915(0);
													Class243.method2172(-26844, class338_sub7.aClass338_Sub3_Sub1_Sub3_5251);
												}
											}
										}
										continue;
									}
								}
								if (class51.contentType == Class296_Sub15_Sub4.anInt6037) {
									Class98 class98 = class51.method633((byte) -115, Class41_Sub13.aHa3774);
									if (class98 != null && (Class338_Sub3_Sub5_Sub1.anInt6645 == 0 || Class338_Sub3_Sub5_Sub1.anInt6645 == 3) && !Class318.aBoolean2814 && i_101_ >= i_106_ && i_102_ >= i_107_ && i_101_ < i_108_ && i_102_ < i_109_) {
										int i_126_ = i_101_ - i_104_;
										int i_127_ = i_102_ - i_105_;
										int i_128_ = class98.anIntArray1054[i_127_];
										if (i_126_ >= i_128_ && i_126_ <= i_128_ + class98.anIntArray1053[i_127_]) {
											i_126_ -= class51.anInt578 / 2;
											i_127_ -= class51.anInt623 / 2;
											int i_129_;
											if (Class361.anInt3103 == 4) {
												i_129_ = (int) Class41_Sub26.aFloat3806 & 0x3fff;
											} else {
												i_129_ = (int) Class41_Sub26.aFloat3806 + StaticMethods.anInt6075 & 0x3fff;
											}
											int i_130_ = Class296_Sub4.anIntArray4598[i_129_];
											int i_131_ = Class296_Sub4.anIntArray4618[i_129_];
											if (Class361.anInt3103 != 4) {
												i_130_ = i_130_ * (ObjTypeList.anInt1320 + 256) >> 8;
												i_131_ = i_131_ * (ObjTypeList.anInt1320 + 256) >> 8;
											}
											int i_132_ = i_127_ * i_130_ + i_126_ * i_131_ >> 14;
											int i_133_ = i_127_ * i_131_ - i_126_ * i_130_ >> 14;
											int i_134_;
											int i_135_;
											if (Class361.anInt3103 == 4) {
												i_134_ = (Class338_Sub3_Sub1.anInt6563 >> 9) + (i_132_ >> 2);
												i_135_ = (Class127_Sub1.anInt4287 >> 9) - (i_133_ >> 2);
											} else {
												int i_136_ = (Class296_Sub51_Sub11.localPlayer.getSize() - 1) * 256;
												i_134_ = (Class296_Sub51_Sub11.localPlayer.tileX - i_136_ >> 9) + (i_132_ >> 2);
												i_135_ = (Class296_Sub51_Sub11.localPlayer.tileY - i_136_ >> 9) - (i_133_ >> 2);
											}
											if (Class127.aBoolean1304 && (Class321.anInt2824 & 0x40) != 0) {
												InterfaceComponent class51_137_ = Class103.method894(0, Class366_Sub4.anInt5375, Class180.anInt1857);
												if (class51_137_ != null) {
													StaticMethods.method1008(74, true, i_134_, IOException_Sub1.anInt36, class51.clickedItem, Class228.aString2200, i_135_, 1L, 49, true, class51.anInt592 << 0 | class51.uid, " ->", false);
												} else {
													Class285.method2368(0);
												}
											} else {
												if (Class296_Sub50.game == Class296_Sub32.stellardawn) {
													StaticMethods.method1008(-91, true, i_134_, -1, -1, TranslatableString.aClass120_1223.getTranslation(Class394.langID), i_135_, 1L, 18, true, 0L, "", false);
												}
												StaticMethods.method1008(-117, true, i_134_, Class208.anInt2091, -1, Class296_Sub51_Sub37.aString6532, i_135_, 1L, 44, true, 0L, "", false);
											}
										}
									}
									continue;
								}
								if (class51.contentType == Class368_Sub5.anInt5447) {
									Class359.aClass51_3092 = class51;
									if (bool_114_) {
										StringNode.aBoolean4622 = true;
									}
									if (bool_119_) {
										int i_138_ = (int) ((i_112_ + class296_sub34.method2732(-6) - i_104_ - class51.anInt578 / 2) * 2.0 / Class106.aFloat1100);
										int i_139_ = (int) -((i_113_ + class296_sub34.method2736(-6) - i_105_ - class51.anInt623 / 2) * 2.0 / Class106.aFloat1100);
										int i_140_ = Class69.anInt3688 + i_138_ + Class106.anInt1111;
										int i_141_ = Class219_Sub1.anInt4569 + i_139_ + Class106.anInt1117;
										Class296_Sub39_Sub14 class296_sub39_sub14 = Class296_Sub51_Sub37.method3193(-114);
										if (class296_sub39_sub14 != null) {
											int[] is = new int[3];
											class296_sub39_sub14.method2875(is, i_141_, (byte) 126, i_140_);
											if (is != null) {
												if (Class2.aClass166_66.method1637(82, 120) && Class338_Sub3_Sub5.rights > 0) {
													Class360_Sub3.method3741(is[2], is[0], (byte) 96, is[1]);
													continue;
												}
												Class360_Sub6.aBoolean5333 = true;
												Class41_Sub27.anInt3810 = is[0];
												Class41_Sub10.anInt3767 = is[1];
												StaticMethods.anInt3150 = is[2];
											}
											Class2.anInt59 = 1;
											AnimationsLoader.aBoolean2762 = false;
											Class296_Sub38.anInt4899 = Class84.aClass189_924.method1895((byte) -55);
											Class208.anInt2090 = Class84.aClass189_924.method1897(0);
										}
									} else if (bool_118_ && Class2.anInt59 > 0) {
										if (Class2.anInt59 == 1 && (Class296_Sub38.anInt4899 != Class84.aClass189_924.method1895((byte) -55) || Class208.anInt2090 != Class84.aClass189_924.method1897(0))) {
											Class279.anInt2549 = Class69.anInt3688;
											Class164.anInt1681 = Class219_Sub1.anInt4569;
											Class2.anInt59 = 2;
										}
										if (Class2.anInt59 == 2) {
											AnimationsLoader.aBoolean2762 = true;
											Class178_Sub2.method1785(125, Class279.anInt2549 + (int) ((Class296_Sub38.anInt4899 - Class84.aClass189_924.method1895((byte) -55)) * 2.0 / Class106.aFloat1103));
											Class225.method2085(Class164.anInt1681 - (int) ((Class208.anInt2090 - Class84.aClass189_924.method1897(0)) * 2.0 / Class106.aFloat1103), false);
										}
									} else {
										if (Class2.anInt59 > 0 && !AnimationsLoader.aBoolean2762) {
											if ((Class296_Sub9_Sub1.anInt5982 == 1 || Class189_Sub1.method1904(-7703)) && Class230.anInt2210 > 2) {
												TextureOperation.method3068(Class208.anInt2090, Class296_Sub38.anInt4899, -17902);
											} else if (Class338_Sub3_Sub4_Sub1.method3577(24060)) {
												TextureOperation.method3068(Class208.anInt2090, Class296_Sub38.anInt4899, -17902);
											}
										}
										Class2.anInt59 = 0;
									}
									continue;
								}
								if (class51.contentType == Class210.anInt2097) {
									if (bool_118_) {
										Class283.method2358(class51.anInt578, i_113_ + Class84.aClass189_924.method1897(0) - i_105_, 0, class51.anInt623, i_112_ + Class84.aClass189_924.method1895((byte) -55) - i_104_);
									}
									continue;
								}
								if (class51.contentType == Class137.anInt1409) {
									Class206.method1992(i_105_, -26396, i_104_, class51);
									continue;
								}
							}
							if (!class51.aBoolean618 && bool_119_) {
								class51.aBoolean618 = true;
								if (class51.anObjectArray563 != null) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.aBoolean4960 = true;
									class296_sub46.callerInterface = class51;
									class296_sub46.anInt4958 = i_112_ + class296_sub34.method2732(-6) - i_104_;
									class296_sub46.anInt4964 = i_113_ + class296_sub34.method2736(-6) - i_105_;
									class296_sub46.callArgs = class51.anObjectArray563;
									Class179.aClass155_1854.addLast((byte) 119, class296_sub46);
								}
							}
							if (class51.aBoolean618 && bool_118_ && class51.anObjectArray531 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.aBoolean4960 = true;
								class296_sub46.callerInterface = class51;
								class296_sub46.anInt4958 = i_112_ + Class84.aClass189_924.method1895((byte) -55) - i_104_;
								class296_sub46.anInt4964 = i_113_ + Class84.aClass189_924.method1897(0) - i_105_;
								class296_sub46.callArgs = class51.anObjectArray531;
								Class179.aClass155_1854.addLast((byte) -44, class296_sub46);
							}
							if (class51.aBoolean618 && !bool_118_) {
								class51.aBoolean618 = false;
								if (class51.anObjectArray537 != null) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.aBoolean4960 = true;
									class296_sub46.callerInterface = class51;
									class296_sub46.anInt4958 = i_112_ + Class84.aClass189_924.method1895((byte) -55) - i_104_;
									class296_sub46.anInt4964 = i_113_ + Class84.aClass189_924.method1897(0) - i_105_;
									class296_sub46.callArgs = class51.anObjectArray537;
									StaticMethods.aClass155_1842.addLast((byte) -67, class296_sub46);
								}
							}
							if (bool_118_ && class51.anObjectArray582 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.aBoolean4960 = true;
								class296_sub46.callerInterface = class51;
								class296_sub46.anInt4958 = i_112_ + Class84.aClass189_924.method1895((byte) -55) - i_104_;
								class296_sub46.anInt4964 = i_113_ + Class84.aClass189_924.method1897(0) - i_105_;
								class296_sub46.callArgs = class51.anObjectArray582;
								Class179.aClass155_1854.addLast((byte) -83, class296_sub46);
							}
							if (!class51.aBoolean524 && bool_114_) {
								class51.aBoolean524 = true;
								if (class51.anObjectArray584 != null) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.aBoolean4960 = true;
									class296_sub46.callerInterface = class51;
									class296_sub46.anInt4958 = i_112_ + Class84.aClass189_924.method1895((byte) -55) - i_104_;
									class296_sub46.anInt4964 = i_113_ + Class84.aClass189_924.method1897(0) - i_105_;
									class296_sub46.callArgs = class51.anObjectArray584;
									Class179.aClass155_1854.addLast((byte) -24, class296_sub46);
								}
							}
							if (class51.aBoolean524 && bool_114_ && class51.anObjectArray608 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.aBoolean4960 = true;
								class296_sub46.callerInterface = class51;
								class296_sub46.anInt4958 = i_112_ + Class84.aClass189_924.method1895((byte) -55) - i_104_;
								class296_sub46.anInt4964 = i_113_ + Class84.aClass189_924.method1897(0) - i_105_;
								class296_sub46.callArgs = class51.anObjectArray608;
								Class179.aClass155_1854.addLast((byte) 124, class296_sub46);
							}
							if (class51.aBoolean524 && !bool_114_) {
								class51.aBoolean524 = false;
								if (class51.anObjectArray503 != null) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.aBoolean4960 = true;
									class296_sub46.callerInterface = class51;
									class296_sub46.anInt4958 = i_112_ + Class84.aClass189_924.method1895((byte) -55) - i_104_;
									class296_sub46.anInt4964 = i_113_ + Class84.aClass189_924.method1897(0) - i_105_;
									class296_sub46.callArgs = class51.anObjectArray503;
									StaticMethods.aClass155_1842.addLast((byte) -74, class296_sub46);
								}
							}
							if (class51.anObjectArray540 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.callerInterface = class51;
								class296_sub46.callArgs = class51.anObjectArray540;
								Class127_Sub1.aClass155_4284.addLast((byte) -65, class296_sub46);
							}
							if (class51.anObjectArray544 != null && Class296_Sub15_Sub4.anInt6038 > class51.anInt560) {
								if (class51.anIntArray564 == null || Class296_Sub15_Sub4.anInt6038 - class51.anInt560 > 32) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.callerInterface = class51;
									class296_sub46.callArgs = class51.anObjectArray544;
									Class179.aClass155_1854.addLast((byte) 105, class296_sub46);
								} else {
									while_39_: for (int i_142_ = class51.anInt560; i_142_ < Class296_Sub15_Sub4.anInt6038; i_142_++) {
										int i_143_ = BITConfigsLoader.anIntArray2207[i_142_ & 0x1f];
										for (int i_144_ = 0; i_144_ < class51.anIntArray564.length; i_144_++) {
											if (class51.anIntArray564[i_144_] == i_143_) {
												CS2Call class296_sub46 = new CS2Call();
												class296_sub46.callerInterface = class51;
												class296_sub46.callArgs = class51.anObjectArray544;
												Class179.aClass155_1854.addLast((byte) -60, class296_sub46);
												break while_39_;
											}
										}
									}
								}
								class51.anInt560 = Class296_Sub15_Sub4.anInt6038;
							}
							if (class51.anObjectArray525 != null && Class249.anInt2356 > class51.anInt521) {
								if (class51.anIntArray619 == null || Class249.anInt2356 - class51.anInt521 > 32) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.callerInterface = class51;
									class296_sub46.callArgs = class51.anObjectArray525;
									Class179.aClass155_1854.addLast((byte) 112, class296_sub46);
								} else {
									while_40_: for (int i_145_ = class51.anInt521; i_145_ < Class249.anInt2356; i_145_++) {
										int i_146_ = Class76.anIntArray872[i_145_ & 0x1f];
										for (int i_147_ = 0; i_147_ < class51.anIntArray619.length; i_147_++) {
											if (class51.anIntArray619[i_147_] == i_146_) {
												CS2Call class296_sub46 = new CS2Call();
												class296_sub46.callerInterface = class51;
												class296_sub46.callArgs = class51.anObjectArray525;
												Class179.aClass155_1854.addLast((byte) -67, class296_sub46);
												break while_40_;
											}
										}
									}
								}
								class51.anInt521 = Class249.anInt2356;
							}
							if (class51.anObjectArray594 != null && ParamType.anInt3249 > class51.anInt596) {
								if (class51.anIntArray484 == null || ParamType.anInt3249 - class51.anInt596 > 32) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.callerInterface = class51;
									class296_sub46.callArgs = class51.anObjectArray594;
									Class179.aClass155_1854.addLast((byte) -66, class296_sub46);
								} else {
									while_41_: for (int i_148_ = class51.anInt596; i_148_ < ParamType.anInt3249; i_148_++) {
										int i_149_ = Class86.anIntArray933[i_148_ & 0x1f];
										for (int i_150_ = 0; i_150_ < class51.anIntArray484.length; i_150_++) {
											if (class51.anIntArray484[i_150_] == i_149_) {
												CS2Call class296_sub46 = new CS2Call();
												class296_sub46.callerInterface = class51;
												class296_sub46.callArgs = class51.anObjectArray594;
												Class179.aClass155_1854.addLast((byte) -58, class296_sub46);
												break while_41_;
											}
										}
									}
								}
								class51.anInt596 = ParamType.anInt3249;
							}
							if (class51.anObjectArray541 != null && Class337.itemChanges > class51.anInt553) {
								if (class51.anIntArray526 == null || Class337.itemChanges - class51.anInt553 > 32) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.callerInterface = class51;
									class296_sub46.callArgs = class51.anObjectArray541;
									Class179.aClass155_1854.addLast((byte) -67, class296_sub46);
								} else {
									while_42_: for (int i_151_ = class51.anInt553; i_151_ < Class337.itemChanges; i_151_++) {
										int i_152_ = Class250.itemChangesArray[i_151_ & 0x1f];
										for (int i_153_ = 0; i_153_ < class51.anIntArray526.length; i_153_++) {
											if (class51.anIntArray526[i_153_] == i_152_) {
												CS2Call class296_sub46 = new CS2Call();
												class296_sub46.callerInterface = class51;
												class296_sub46.callArgs = class51.anObjectArray541;
												Class179.aClass155_1854.addLast((byte) -58, class296_sub46);
												break while_42_;
											}
										}
									}
								}
								class51.anInt553 = Class337.itemChanges;
							}
							if (class51.anObjectArray477 != null && Class14.anInt155 > class51.anInt496) {
								if (class51.anIntArray625 == null || Class14.anInt155 - class51.anInt496 > 32) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.callerInterface = class51;
									class296_sub46.callArgs = class51.anObjectArray477;
									Class179.aClass155_1854.addLast((byte) -36, class296_sub46);
								} else {
									while_43_: for (int i_154_ = class51.anInt496; i_154_ < Class14.anInt155; i_154_++) {
										int i_155_ = Class65.anIntArray733[i_154_ & 0x1f];
										for (int i_156_ = 0; i_156_ < class51.anIntArray625.length; i_156_++) {
											if (class51.anIntArray625[i_156_] == i_155_) {
												CS2Call class296_sub46 = new CS2Call();
												class296_sub46.callerInterface = class51;
												class296_sub46.callArgs = class51.anObjectArray477;
												Class179.aClass155_1854.addLast((byte) 100, class296_sub46);
												break while_43_;
											}
										}
									}
								}
								class51.anInt496 = Class14.anInt155;
							}
							if (class51.anObjectArray597 != null && Class366_Sub5.anInt5387 > class51.anInt528) {
								if (class51.anIntArray569 == null || Class366_Sub5.anInt5387 - class51.anInt528 > 32) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.callerInterface = class51;
									class296_sub46.callArgs = class51.anObjectArray597;
									Class179.aClass155_1854.addLast((byte) -101, class296_sub46);
								} else {
									while_44_: for (int i_157_ = class51.anInt528; i_157_ < Class366_Sub5.anInt5387; i_157_++) {
										int i_158_ = Class219.anIntArray2131[i_157_ & 0x1f];
										for (int i_159_ = 0; i_159_ < class51.anIntArray569.length; i_159_++) {
											if (class51.anIntArray569[i_159_] == i_158_) {
												CS2Call class296_sub46 = new CS2Call();
												class296_sub46.callerInterface = class51;
												class296_sub46.callArgs = class51.anObjectArray597;
												Class179.aClass155_1854.addLast((byte) 111, class296_sub46);
												break while_44_;
											}
										}
									}
								}
								class51.anInt528 = Class366_Sub5.anInt5387;
							}
							if (Class318.anInt2808 > class51.anInt633 && class51.anObjectArray568 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.callerInterface = class51;
								class296_sub46.callArgs = class51.anObjectArray568;
								Class179.aClass155_1854.addLast((byte) -71, class296_sub46);
							}
							if (Class156.anInt3595 > class51.anInt633 && class51.anObjectArray491 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.callerInterface = class51;
								class296_sub46.callArgs = class51.anObjectArray491;
								Class179.aClass155_1854.addLast((byte) -26, class296_sub46);
							}
							if (Class296_Sub15_Sub3.anInt6012 > class51.anInt633 && class51.anObjectArray549 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.callerInterface = class51;
								class296_sub46.callArgs = class51.anObjectArray549;
								Class179.aClass155_1854.addLast((byte) 109, class296_sub46);
							}
							if (Class296_Sub39_Sub20_Sub2.anInt6727 > class51.anInt633 && class51.anObjectArray518 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.callerInterface = class51;
								class296_sub46.callArgs = class51.anObjectArray518;
								Class179.aClass155_1854.addLast((byte) 105, class296_sub46);
							}
							if (Class296_Sub15_Sub1.anInt5997 > class51.anInt633 && class51.anObjectArray506 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.callerInterface = class51;
								class296_sub46.callArgs = class51.anObjectArray506;
								Class179.aClass155_1854.addLast((byte) 102, class296_sub46);
							}
							if (StaticMethods.anInt5974 > class51.anInt633 && class51.anObjectArray583 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.callerInterface = class51;
								class296_sub46.callArgs = class51.anObjectArray583;
								Class179.aClass155_1854.addLast((byte) 115, class296_sub46);
							}
							if (Class308.anInt2753 > class51.anInt633 && class51.anObjectArray509 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.callerInterface = class51;
								class296_sub46.callArgs = class51.anObjectArray509;
								Class179.aClass155_1854.addLast((byte) -41, class296_sub46);
							}
							class51.anInt633 = Class338_Sub3_Sub3_Sub1.anInt6616;
							if (class51.anObjectArray504 != null) {
								for (int i_160_ = 0; i_160_ < Class338_Sub8_Sub1.anInt6576; i_160_++) {
									CS2Call class296_sub46 = new CS2Call();
									class296_sub46.callerInterface = class51;
									class296_sub46.anInt4966 = Class341.anInterface1Array2978[i_160_].method5(13856);
									class296_sub46.anInt4969 = Class341.anInterface1Array2978[i_160_].method2(-23600);
									class296_sub46.callArgs = class51.anObjectArray504;
									Class179.aClass155_1854.addLast((byte) -65, class296_sub46);
								}
							}
							if (Class241_Sub1_Sub1.aBoolean5890 && class51.anObjectArray627 != null) {
								CS2Call class296_sub46 = new CS2Call();
								class296_sub46.callerInterface = class51;
								class296_sub46.callArgs = class51.anObjectArray627;
								Class179.aClass155_1854.addLast((byte) -84, class296_sub46);
							}
						}
						if (class51.type == 5 && class51.anInt542 != -1) {
							class51.method620(17286, InvisiblePlayer.aClass279_1977, Class49.aClass182_457).method2258(true, Class41_Sub13.aHa3774, class51.anInt623, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub19_5011.method468(123));
						}
						Class266.method2289(class51, -29278);
						if (class51.type == 0) {
							method121(class51s, class51.uid, i_106_, i_107_, i_108_, i_109_, i_104_ - class51.anInt624, i_105_ - class51.anInt632, i_99_, i_100_, i_101_, i_102_);
							if (class51.aClass51Array538 != null) {
								method121(class51.aClass51Array538, class51.uid, i_106_, i_107_, i_108_, i_109_, i_104_ - class51.anInt624, i_105_ - class51.anInt632, i_99_, i_100_, i_101_, i_102_);
							}
							Class296_Sub13 class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.get(class51.uid);
							if (class296_sub13 != null) {
								if (Class296_Sub50.game == Class363.runescape && class296_sub13.anInt4656 == 0 && !Class318.aBoolean2814 && bool_114_ && !BillboardRaw.aBoolean1071) {
									Class105_Sub2.method915(0);
								}
								Class104.method900(i_100_, i_109_, class296_sub13.anInt4657, i_105_, i_106_, i_108_, i_101_, i_107_, i_99_, i_104_, i_102_, (byte) 63);
							}
						}
					}
				}
			}
		}
	}
}
