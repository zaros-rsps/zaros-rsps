package net.zaros.client;

/* Class296_Sub51_Sub24 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub24 extends TextureOperation {
	static Sprite[] aClass397Array6463;
	static int[] anIntArray6464;
	static boolean aBoolean6465 = false;

	public Class296_Sub51_Sub24() {
		super(0, true);
	}

	public static void method3142(int i) {
		anIntArray6464 = null;
		if (i != 32)
			anIntArray6464 = null;
		aClass397Array6463 = null;
	}

	final int[] get_monochrome_output(int i, int i_0_) {
		if (i != 0)
			anIntArray6464 = null;
		int[] is = aClass318_5035.method3335(i_0_, (byte) 28);
		if (aClass318_5035.aBoolean2819)
			Class291.method2407(is, 0, Class41_Sub10.anInt3769, Class294.anIntArray2686[i_0_]);
		return is;
	}

	static {
		anIntArray6464 = new int[]{16, 32, 64, 128};
	}
}
