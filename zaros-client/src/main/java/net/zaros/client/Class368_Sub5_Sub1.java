package net.zaros.client;
/* Class368_Sub5_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;

final class Class368_Sub5_Sub1 extends Class368_Sub5 {
	private int anInt6590;
	private int anInt6591;
	private int anInt6592;
	static IncomingPacket aClass231_6593 = new IncomingPacket(66, 4);
	static long aLong6594 = 0L;

	static final void method3825(boolean bool, byte i) {
		if (Class198.aByteArrayArrayArray2003 == null)
			Class198.aByteArrayArrayArray2003 = new byte[4][Class198.currentMapSizeX][Class296_Sub38.currentMapSizeY];
		for (int i_0_ = 0; i_0_ < 4; i_0_++) {
			for (int i_1_ = 0; Class198.currentMapSizeX > i_1_; i_1_++) {
				for (int i_2_ = 0; Class296_Sub38.currentMapSizeY > i_2_; i_2_++)
					Class198.aByteArrayArrayArray2003[i_0_][i_1_][i_2_] = i;
			}
		}
		if (bool != true)
			aClass231_6593 = null;
	}

	public static void method3826(int i) {
		aClass231_6593 = null;
		if (i != 20552)
			aLong6594 = -88L;
	}

	static final void method3827(int i, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_, int i_8_) {
		if (i_8_ == 23358) {
			if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_4995.method489(i_8_ - 23232) != 0 && i_6_ != 0 && Class296_Sub51_Sub1.anInt6335 < 50 && i_5_ != -1)
				Class336.aClass391Array2956[Class296_Sub51_Sub1.anInt6335++] = new Class391((byte) 1, i_5_, i_6_, i_7_, i_3_, i_4_, i, null);
		}
	}

	final void method3807(byte i) {
		int i_9_ = anInt6592 * 512 + 256;
		int i_10_ = anInt6590 * 512 + 256;
		int i_11_ = 59 % ((i - 52) / 52);
		int i_12_ = anInt6591;
		if (i_12_ < 3 && r_Sub2.method2871(anInt6590, anInt6592, (byte) -47))
			i_12_++;
		Class338_Sub3_Sub1_Sub5 class338_sub3_sub1_sub5 = new Class338_Sub3_Sub1_Sub5(anInt5449, 0, anInt6591, i_12_, i_9_, aa_Sub1.method155(-1537652855, anInt6591, i_9_, i_10_) - anInt5448, i_10_, anInt6592, anInt6592, anInt6590, anInt6590, anInt5451, false);
		Class134.aClass263_1385.put((long) (anInt6590 | anInt6592 << 16), new Class296_Sub39_Sub11(class338_sub3_sub1_sub5));
	}

	Class368_Sub5_Sub1(Packet class296_sub17) {
		super(class296_sub17);
		int i = class296_sub17.g4();
		anInt6592 = i >>> 16;
		anInt6590 = i & 0xffff;
		anInt6591 = class296_sub17.g1();
	}

	static final int method3828(byte i) {
		ha var_ha = Class41_Sub13.aHa3774;
		boolean bool = false;
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(116) != 0) {
			Canvas canvas = new Canvas();
			canvas.setSize(100, 100);
			var_ha = Animator.method562(null, canvas, 120, 0, 0, null);
			bool = true;
		}
		long l = Class72.method771(-110);
		for (int i_13_ = 0; i_13_ < 10000; i_13_++)
			var_ha.a(5, 10, 100, 75, 50, 100, 15, 90, 100, -65536, -65536, -65536, 1);
		int i_14_ = (int) (Class72.method771(-110) + -l);
		if (i <= 21)
			return -35;
		var_ha.method1088(0, 100, -16777216, 1, 0, 100);
		if (bool)
			var_ha.method1091((byte) -80);
		return i_14_;
	}
}
