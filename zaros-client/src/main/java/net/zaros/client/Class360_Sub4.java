package net.zaros.client;
import jagdx.IDirect3DDevice;
import jagdx.IDirect3DVertexShader;

final class Class360_Sub4 extends Class360 {
	private ha_Sub1_Sub2 aHa_Sub1_Sub2_5313;
	private boolean aBoolean5314;
	private IDirect3DVertexShader anIDirect3DVertexShader5315;
	private boolean aBoolean5316 = false;
	private Interface6_Impl1 anInterface6_Impl1_5317;
	private IDirect3DVertexShader anIDirect3DVertexShader5318;
	private IDirect3DVertexShader anIDirect3DVertexShader5319;
	private boolean aBoolean5320;
	private static float[] aFloatArray5321 = new float[16];
	private static float[] aFloatArray5322 = new float[4];
	private IDirect3DVertexShader anIDirect3DVertexShader5323;
	private IDirect3DVertexShader anIDirect3DVertexShader5324;

	final void method3725(int i) {
		aHa_Sub1_3093.method1151(1, 16760);
		aHa_Sub1_3093.method1140(null, false);
		aHa_Sub1_3093.method1177(Js5.aClass125_1411, 9815, Js5.aClass125_1411);
		aHa_Sub1_3093.method1158((byte) -110, 0, Class199.aClass287_2007);
		aHa_Sub1_3093.method1158((byte) -114, 2, Class151.aClass287_1553);
		int i_0_ = -83 / ((58 - i) / 56);
		aHa_Sub1_3093.method1219((byte) 54, Class199.aClass287_2007, 0);
		aHa_Sub1_3093.method1151(0, 16760);
		if (aBoolean5316) {
			aHa_Sub1_3093.method1158((byte) -113, 0, Class199.aClass287_2007);
			aHa_Sub1_3093.method1219((byte) 54, Class199.aClass287_2007, 0);
			aBoolean5316 = false;
		}
		if (anIDirect3DVertexShader5323 != null) {
			aHa_Sub1_Sub2_5313.method1248(1326468944, null);
			anIDirect3DVertexShader5323 = null;
		}
	}

	final void method3733(byte i, boolean bool) {
		aBoolean5314 = bool;
		aHa_Sub1_3093.method1151(1, 16760);
		aHa_Sub1_3093.method1140(anInterface6_Impl1_5317, false);
		aHa_Sub1_3093.method1177(Class280.aClass125_2589, 9815, Class41_Sub4.aClass125_3745);
		aHa_Sub1_3093.method1158((byte) -106, 0, Class151.aClass287_1553);
		if (i > -125)
			anIDirect3DVertexShader5323 = null;
		aHa_Sub1_3093.method1170(2, Class199.aClass287_2007, false, true, (byte) -117);
		aHa_Sub1_3093.method1219((byte) 54, Class153.aClass287_1578, 0);
		aHa_Sub1_3093.method1151(0, 16760);
		method3726((byte) -109);
	}

	Class360_Sub4(ha_Sub1_Sub2 var_ha_Sub1_Sub2, Js5 class138) {
		super(var_ha_Sub1_Sub2);
		aHa_Sub1_Sub2_5313 = var_ha_Sub1_Sub2;
		if (class138 != null && ((aHa_Sub1_Sub2_5313.aD3DCAPS5858.VertexShaderVersion & 0xffff) >= 257)) {
			anIDirect3DVertexShader5318 = (aHa_Sub1_Sub2_5313.anIDirect3DDevice5865.a(class138.getFile_("uw_ground_unlit", "dx")));
			anIDirect3DVertexShader5315 = (aHa_Sub1_Sub2_5313.anIDirect3DDevice5865.a(class138.getFile_("uw_ground_lit", "dx")));
			anIDirect3DVertexShader5319 = (aHa_Sub1_Sub2_5313.anIDirect3DDevice5865.a(class138.getFile_("uw_model_unlit", "dx")));
			anIDirect3DVertexShader5324 = aHa_Sub1_Sub2_5313.anIDirect3DDevice5865.a(class138.getFile_("uw_model_lit", "dx"));
			if (!(anIDirect3DVertexShader5324 != null & (anIDirect3DVertexShader5318 != null & anIDirect3DVertexShader5315 != null & anIDirect3DVertexShader5319 != null)))
				aBoolean5320 = false;
			else {
				anInterface6_Impl1_5317 = aHa_Sub1_3093.method1179(1, 2, false, new int[]{0, -1}, -461);
				anInterface6_Impl1_5317.method64((byte) 49, false, false);
				aBoolean5320 = true;
			}
		} else
			aBoolean5320 = false;
	}

	final void method3732(int i, int i_1_, int i_2_) {
		if (i_1_ > -6)
			aBoolean5316 = false;
	}

	final void method3736(byte i, Interface6 interface6, int i_3_) {
		if (interface6 != null) {
			if (aBoolean5316) {
				aHa_Sub1_3093.method1158((byte) -125, 0, Class199.aClass287_2007);
				aHa_Sub1_3093.method1219((byte) 54, Class199.aClass287_2007, 0);
				aBoolean5316 = false;
			}
			aHa_Sub1_3093.method1140(interface6, false);
			aHa_Sub1_3093.method1197((byte) 22, i_3_);
		} else if (!aBoolean5316) {
			aHa_Sub1_3093.method1140(aHa_Sub1_3093.anInterface6_4037, false);
			aHa_Sub1_3093.method1197((byte) 22, 1);
			aHa_Sub1_3093.method1158((byte) -116, 0, Class153.aClass287_1578);
			aHa_Sub1_3093.method1219((byte) 54, Class153.aClass287_1578, 0);
			aBoolean5316 = true;
		}
		int i_4_ = -54 % ((72 - i) / 49);
	}

	final void method3734(int i) {
		if (i == -13412) {
			if (anIDirect3DVertexShader5323 != null) {
				IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5313.anIDirect3DDevice5865;
				idirect3ddevice.a(4, aHa_Sub1_3093.method1182(i ^ ~0x3462, aFloatArray5321));
			}
		}
	}

	final void method3731(boolean bool, byte i) {
		if (i != -71)
			anIDirect3DVertexShader5315 = null;
	}

	final void method3730(boolean bool) {
		if (anIDirect3DVertexShader5323 != null) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5313.anIDirect3DDevice5865;
			Class373_Sub2 class373_sub2 = aHa_Sub1_3093.method1226(-23501);
			idirect3ddevice.SetVertexShaderConstantF(8, class373_sub2.method3945(aFloatArray5321, (byte) 95), 2);
		}
		if (bool)
			method3731(false, (byte) -46);
	}

	final boolean method3723(byte i) {
		int i_5_ = 85 % ((i + 49) / 36);
		return aBoolean5320;
	}

	final void method3719(byte i) {
		if (anIDirect3DVertexShader5323 != null) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5313.anIDirect3DDevice5865;
			int i_6_ = aHa_Sub1_3093.XA();
			int i_7_ = aHa_Sub1_3093.i();
			float f = -((float) (i_6_ - i_7_) * 0.125F) + (float) i_6_;
			float f_8_ = (float) i_6_ - (float) (-i_7_ + i_6_) * 0.25F;
			idirect3ddevice.b(10, f_8_, 1.0F / (-f_8_ + f), f, 1.0F / (-f + (float) i_6_));
			idirect3ddevice.b(11, (1.0F / (float) aHa_Sub1_3093.method1169((byte) 118)), ((float) aHa_Sub1_3093.method1123((byte) -68) / 255.0F), aHa_Sub1_3093.aFloat3964, 1.0F / (-aHa_Sub1_3093.aFloat3989 + aHa_Sub1_3093.aFloat3964));
			aHa_Sub1_3093.method1139(0, aHa_Sub1_3093.method1220(120));
		}
		int i_9_ = -11 / ((-37 - i) / 43);
	}

	private final void method3742(byte i) {
		if (anIDirect3DVertexShader5323 != null && aBoolean5314) {
			Class373_Sub2 class373_sub2 = aHa_Sub1_3093.method1113(27200);
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5313.anIDirect3DDevice5865;
			idirect3ddevice.b(13, aHa_Sub1_3093.aFloat4000 * aHa_Sub1_3093.aFloat4010, aHa_Sub1_3093.aFloat4010 * aHa_Sub1_3093.aFloat4019, aHa_Sub1_3093.aFloat4010 * aHa_Sub1_3093.aFloat4043, 1.0F);
			idirect3ddevice.b(14, aHa_Sub1_3093.aFloat4022 * aHa_Sub1_3093.aFloat4000, aHa_Sub1_3093.aFloat4019 * aHa_Sub1_3093.aFloat4022, aHa_Sub1_3093.aFloat4022 * aHa_Sub1_3093.aFloat4043, 1.0F);
			idirect3ddevice.b(16, aHa_Sub1_3093.aFloat4000 * aHa_Sub1_3093.aFloat4007, aHa_Sub1_3093.aFloat4007 * aHa_Sub1_3093.aFloat4019, aHa_Sub1_3093.aFloat4007 * aHa_Sub1_3093.aFloat4043, 1.0F);
			class373_sub2.method3930(aHa_Sub1_3093.aFloatArray4030[1], aHa_Sub1_3093.aFloatArray4030[2], (byte) -56, aHa_Sub1_3093.aFloatArray4030[0], aFloatArray5322);
			idirect3ddevice.SetVertexShaderConstantF(15, aFloatArray5322, 1);
			class373_sub2.method3930(aHa_Sub1_3093.aFloatArray4028[1], aHa_Sub1_3093.aFloatArray4028[2], (byte) -56, aHa_Sub1_3093.aFloatArray4028[0], aFloatArray5322);
			idirect3ddevice.SetVertexShaderConstantF(17, aFloatArray5322, 1);
		}
		if (i < 25)
			aFloatArray5322 = null;
	}

	final void method3726(byte i) {
		IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5313.anIDirect3DDevice5865;
		if (i != -109)
			method3726((byte) -107);
		int i_10_ = aHa_Sub1_3093.method1203((byte) -60);
		Class373_Sub2 class373_sub2 = aHa_Sub1_3093.method1174(65280);
		IDirect3DVertexShader idirect3dvertexshader;
		if (!aBoolean5314)
			idirect3dvertexshader = (i_10_ != 2147483647 ? anIDirect3DVertexShader5319 : anIDirect3DVertexShader5318);
		else
			idirect3dvertexshader = (i_10_ == 2147483647 ? anIDirect3DVertexShader5315 : anIDirect3DVertexShader5324);
		if (idirect3dvertexshader != anIDirect3DVertexShader5323) {
			anIDirect3DVertexShader5323 = idirect3dvertexshader;
			aHa_Sub1_Sub2_5313.method1248(1326468944, idirect3dvertexshader);
			method3742((byte) 49);
			method3734(-13412);
			method3730(false);
			method3724((byte) -50);
			method3720(8250);
			method3719((byte) -88);
		}
		class373_sub2.method3934(-1.0F, (float) i_10_, aFloatArray5322, 0.0F, 9, 0.0F);
		idirect3ddevice.a(12, aFloatArray5322);
	}

	final void method3724(byte i) {
		if (i == -50) {
			if (anIDirect3DVertexShader5323 != null) {
				IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5313.anIDirect3DDevice5865;
				Class373_Sub2 class373_sub2 = aHa_Sub1_Sub2_5313.method1122(i + 101);
				idirect3ddevice.a(0, class373_sub2.method3944(aFloatArray5321, (byte) 31));
			}
		}
	}

	final void method3720(int i) {
		if (anIDirect3DVertexShader5323 != null) {
			IDirect3DDevice idirect3ddevice = aHa_Sub1_Sub2_5313.anIDirect3DDevice5865;
			Class373_Sub2 class373_sub2 = aHa_Sub1_Sub2_5313.method1122(i ^ 0x205b);
			idirect3ddevice.a(0, class373_sub2.method3944(aFloatArray5321, (byte) 31));
		}
		if (i != 8250)
			method3742((byte) -40);
	}
}
