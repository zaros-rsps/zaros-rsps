package net.zaros.client;

/* Class296_Sub45_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub45_Sub5 extends Class296_Sub45 {
	private NodeDeque aClass155_6331 = new NodeDeque();
	private NodeDeque aClass155_6332 = new NodeDeque();
	private int anInt6333 = 0;
	private int anInt6334 = -1;

	final Class296_Sub45 method2932() {
		return (Class296_Sub45) aClass155_6331.removeNext(1001);
	}

	private final void method3031(Class296_Sub10 class296_sub10) {
		class296_sub10.unlink();
		class296_sub10.method2495();
		Node class296 = aClass155_6332.aClass296_1586.next;
		if (class296 == aClass155_6332.aClass296_1586)
			anInt6334 = -1;
		else
			anInt6334 = ((Class296_Sub10) class296).anInt4635;
	}

	final synchronized void method2934(int[] is, int i, int i_0_) {
		do {
			if (anInt6334 < 0) {
				method3032(is, i, i_0_);
				break;
			}
			if (anInt6333 + i_0_ < anInt6334) {
				anInt6333 += i_0_;
				method3032(is, i, i_0_);
				break;
			}
			int i_1_ = anInt6334 - anInt6333;
			method3032(is, i, i_1_);
			i += i_1_;
			i_0_ -= i_1_;
			anInt6333 += i_1_;
			method3036();
			Class296_Sub10 class296_sub10 = (Class296_Sub10) aClass155_6332.removeFirst((byte) 112);
			synchronized (class296_sub10) {
				int i_2_ = class296_sub10.method2494(this);
				if (i_2_ < 0) {
					class296_sub10.anInt4635 = 0;
					method3031(class296_sub10);
				} else {
					class296_sub10.anInt4635 = i_2_;
					method3038(class296_sub10.next, class296_sub10);
				}
			}
		} while (i_0_ != 0);
	}

	final Class296_Sub45 method2930() {
		return (Class296_Sub45) aClass155_6331.removeFirst((byte) 124);
	}

	final synchronized void method2933(int i) {
		do {
			if (anInt6334 < 0) {
				method3033(i);
				break;
			}
			if (anInt6333 + i < anInt6334) {
				anInt6333 += i;
				method3033(i);
				break;
			}
			int i_3_ = anInt6334 - anInt6333;
			method3033(i_3_);
			i -= i_3_;
			anInt6333 += i_3_;
			method3036();
			Class296_Sub10 class296_sub10 = (Class296_Sub10) aClass155_6332.removeFirst((byte) 119);
			synchronized (class296_sub10) {
				int i_4_ = class296_sub10.method2494(this);
				if (i_4_ < 0) {
					class296_sub10.anInt4635 = 0;
					method3031(class296_sub10);
				} else {
					class296_sub10.anInt4635 = i_4_;
					method3038(class296_sub10.next, class296_sub10);
				}
			}
		} while (i != 0);
	}

	private final void method3032(int[] is, int i, int i_5_) {
		for (Class296_Sub45 class296_sub45 = (Class296_Sub45) aClass155_6331.removeFirst((byte) 117); class296_sub45 != null; class296_sub45 = (Class296_Sub45) aClass155_6331.removeNext(1001))
			class296_sub45.method2931(is, i, i_5_);
	}

	private final void method3033(int i) {
		for (Class296_Sub45 class296_sub45 = (Class296_Sub45) aClass155_6331.removeFirst((byte) 121); class296_sub45 != null; class296_sub45 = (Class296_Sub45) aClass155_6331.removeNext(1001))
			class296_sub45.method2933(i);
	}

	final synchronized int method3034() {
		return aClass155_6331.method1580(90);
	}

	final synchronized void method3035(Class296_Sub45 class296_sub45) {
		aClass155_6331.method1570(true, class296_sub45);
	}

	final int method2929() {
		return 0;
	}

	private final void method3036() {
		if (anInt6333 > 0) {
			for (Class296_Sub10 class296_sub10 = (Class296_Sub10) aClass155_6332.removeFirst((byte) 113); class296_sub10 != null; class296_sub10 = (Class296_Sub10) aClass155_6332.removeNext(1001))
				class296_sub10.anInt4635 -= anInt6333;
			anInt6334 -= anInt6333;
			anInt6333 = 0;
		}
	}

	final synchronized void method3037(Class296_Sub45 class296_sub45) {
		class296_sub45.unlink();
	}

	private final void method3038(Node class296, Class296_Sub10 class296_sub10) {
		for (/**/; (class296 != aClass155_6332.aClass296_1586 && (((Class296_Sub10) class296).anInt4635 <= class296_sub10.anInt4635)); class296 = class296.next) {
			/* empty */
		}
		Class296_Sub36.method2765(0, class296_sub10, class296);
		anInt6334 = ((Class296_Sub10) aClass155_6332.aClass296_1586.next).anInt4635;
	}

	public Class296_Sub45_Sub5() {
		/* empty */
	}
}
