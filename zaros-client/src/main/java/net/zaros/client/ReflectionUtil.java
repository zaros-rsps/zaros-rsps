package net.zaros.client;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.lang.reflect.Field;

public class ReflectionUtil {

	static Field f_peer;
	static Field f_hwnd;

	static {
		new java.awt.Container();
		try {
			f_peer = Component.class.getDeclaredField("peer");
			f_peer.setAccessible(true);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		try {
			Class<?> clazz = Class.forName("sun.awt.windows.WComponentPeer");
			f_hwnd = clazz.getDeclaredField("hwnd");
			f_hwnd.setAccessible(true);
		} catch (Throwable e) {
			// NOOP
		}
	}
	public static long getHwnd(Object peer) {
		if (f_hwnd == null || peer == null) {
			return 0;
		}
		try {
			return (f_hwnd.getLong(peer));
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static Object getPeer(Object component) {
		try {
			return f_peer.get(component);
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}
}
