package net.zaros.client;
/* Class122_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.applet.Applet;

class Class122_Sub1 extends Class122 {
	private Sprite aClass397_5653;
	Sprite aClass397_5654;
	private Sprite aClass397_5655;
	static Applet anApplet5656;
	static int anInt5657 = -1;
	static Class280 aClass280_5658 = new Class280();
	private Sprite aClass397_5659;
	private Sprite aClass397_5660;
	private Sprite aClass397_5661;

	static final int method1040(int i) {
		synchronized (Class258.aClass113_2415) {
			if (i != 21077)
				method1042(53, (byte) -2);
			return Class258.aClass113_2415.getHardReferenceCount();
		}
	}

	public static void method1041(boolean bool) {
		if (bool)
			method1041(false);
		anApplet5656 = null;
		aClass280_5658 = null;
	}

	static final void method1042(int i, byte i_0_) {
		Class296_Sub51_Sub32.aLong6513 = 1000000000L / (long) i;
		if (i_0_ != 6)
			aClass280_5658 = null;
	}

	public final boolean method44(byte i) {
		if (!super.method44((byte) 107))
			return false;
		Class379_Sub2 class379_sub2 = (Class379_Sub2) aClass379_3543;
		if (!aClass138_3546.hasEntryBuffer(class379_sub2.anInt5680))
			return false;
		if (!aClass138_3546.hasEntryBuffer(class379_sub2.anInt5679))
			return false;
		if (!aClass138_3546.hasEntryBuffer(class379_sub2.anInt5681))
			return false;
		if (!aClass138_3546.hasEntryBuffer(class379_sub2.anInt5682))
			return false;
		if (i <= 91)
			return true;
		if (!aClass138_3546.hasEntryBuffer(class379_sub2.anInt5676))
			return false;
		if (!aClass138_3546.hasEntryBuffer(class379_sub2.anInt5678))
			return false;
		return true;
	}

	final void method1036(boolean bool, int i, int i_1_, byte i_2_) {
		int i_3_ = i + aClass397_5659.method4099();
		int i_4_ = aClass379_3543.anInt3613 + (i - aClass397_5661.method4099());
		int i_5_ = i_1_ + aClass397_5660.method4088();
		int i_6_ = aClass379_3543.anInt3626 + (i_1_ - aClass397_5655.method4088());
		int i_7_ = -i_3_ + i_4_;
		int i_8_ = -i_5_ + i_6_;
		int i_9_ = this.method1039(true) * i_7_ / 10000;
		int[] is = new int[4];
		Class41_Sub13.aHa3774.K(is);
		Class41_Sub13.aHa3774.KA(i_3_, i_5_, i_3_ + i_9_, i_6_);
		method1043(i_3_, i_5_, i_8_, i_7_, (byte) 17);
		Class41_Sub13.aHa3774.KA(i_3_ + i_9_, i_5_, i_4_, i_6_);
		aClass397_5653.method4085(i_3_, i_5_, i_7_, i_8_);
		if (i_2_ == 80)
			Class41_Sub13.aHa3774.KA(is[0], is[1], is[2], is[3]);
	}

	public final void method42(byte i) {
		super.method42(i);
		Class379_Sub2 class379_sub2 = (Class379_Sub2) aClass379_3543;
		aClass397_5654 = CS2Script.method2798(aClass138_3546, class379_sub2.anInt5680, i - 91);
		aClass397_5653 = CS2Script.method2798(aClass138_3546, class379_sub2.anInt5679, -3);
		aClass397_5659 = CS2Script.method2798(aClass138_3546, class379_sub2.anInt5681, -3);
		aClass397_5661 = CS2Script.method2798(aClass138_3546, class379_sub2.anInt5682, i - 91);
		aClass397_5660 = CS2Script.method2798(aClass138_3546, class379_sub2.anInt5676, -3);
		aClass397_5655 = CS2Script.method2798(aClass138_3546, class379_sub2.anInt5678, -3);
	}

	final void method1035(int i, boolean bool, int i_10_, int i_11_) {
		if (i_10_ == -24228) {
			if (bool) {
				int[] is = new int[4];
				Class41_Sub13.aHa3774.K(is);
				Class41_Sub13.aHa3774.KA(i_11_, i, i_11_ + aClass379_3543.anInt3613, aClass379_3543.anInt3626 + i);
				int i_12_ = aClass397_5659.method4099();
				int i_13_ = aClass397_5659.method4088();
				int i_14_ = aClass397_5661.method4099();
				int i_15_ = aClass397_5661.method4088();
				aClass397_5659.method4096(i_11_, i + (aClass379_3543.anInt3626 - i_13_) / 2);
				aClass397_5661.method4096((aClass379_3543.anInt3613 + i_11_ - i_14_), ((-i_15_ + aClass379_3543.anInt3626) / 2) + i);
				Class41_Sub13.aHa3774.KA(i_11_, i, aClass379_3543.anInt3613 + i_11_, i + aClass397_5660.method4088());
				aClass397_5660.method4085(i_11_ + i_12_, i, (-i_14_ - i_12_ + aClass379_3543.anInt3613), aClass379_3543.anInt3626);
				int i_16_ = aClass397_5655.method4088();
				Class41_Sub13.aHa3774.KA(i_11_, -i_16_ + i + aClass379_3543.anInt3626, aClass379_3543.anInt3613 + i_11_, i + aClass379_3543.anInt3626);
				aClass397_5655.method4085(i_11_ + i_12_, i - (-aClass379_3543.anInt3626 + i_16_), aClass379_3543.anInt3613 - (i_12_ + i_14_), aClass379_3543.anInt3626);
				Class41_Sub13.aHa3774.KA(is[0], is[1], is[2], is[3]);
			}
		}
	}

	void method1043(int i, int i_17_, int i_18_, int i_19_, byte i_20_) {
		if (i_20_ != 17)
			anApplet5656 = null;
		aClass397_5654.method4085(i, i_17_, i_19_, i_18_);
	}

	static final boolean method1044(char c, byte i) {
		try {
			int i_21_ = -87 % ((-9 - i) / 40);
			if ((c < 'A' || c > 'Z') && (c < 'a' || c > 'z'))
				return false;
			return true;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "bia.L(" + c + ',' + i + ')');
		}
	}

	Class122_Sub1(Js5 class138, Js5 class138_22_, Class379_Sub2 class379_sub2) {
		super(class138, class138_22_, (Class379) class379_sub2);
	}
}
