package net.zaros.client;
/* Class296_Sub29_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Shape;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.ImageProducer;

final class Class296_Sub29_Sub2 extends Class296_Sub29 implements ImageProducer {
	private ColorModel aColorModel6058;
	private Image anImage6059;
	private ImageConsumer anImageConsumer6060;
	private Canvas aCanvas6061;

	public final void startProduction(ImageConsumer imageconsumer) {
		addConsumer(imageconsumer);
	}

	public final void requestTopDownLeftRightResend(ImageConsumer imageconsumer) {
		/* empty */
	}

	static final int method2694(byte i, int i_0_) {
		int i_1_ = -32 % ((i - 54) / 59);
		int i_2_ = 0;
		if (i_0_ < 0 || i_0_ >= 65536) {
			i_2_ += 16;
			i_0_ >>>= 16;
		}
		if (i_0_ >= 256) {
			i_2_ += 8;
			i_0_ >>>= 8;
		}
		if (i_0_ >= 16) {
			i_2_ += 4;
			i_0_ >>>= 4;
		}
		if (i_0_ >= 4) {
			i_2_ += 2;
			i_0_ >>>= 2;
		}
		if (i_0_ >= 1) {
			i_2_++;
			i_0_ >>>= 1;
		}
		return i_0_ + i_2_;
	}

	private final synchronized void method2695(int i, int i_3_, int i_4_, int i_5_, int i_6_) {
		if (anImageConsumer6060 != null) {
			anImageConsumer6060.setPixels(i_6_, i_4_, i, i_5_, aColorModel6058, anIntArray4810, i_4_ * anInt4815 + i_6_, anInt4815);
			anImageConsumer6060.imageComplete(i_3_);
		}
	}

	private final synchronized void method2696(int i) {
		if (i == 32 && anImageConsumer6060 != null) {
			anImageConsumer6060.setPixels(0, 0, anInt4815, anInt4813, aColorModel6058, anIntArray4810, 0, anInt4815);
			anImageConsumer6060.imageComplete(2);
		}
	}

	static final void method2697(int i, int i_7_, int i_8_, int i_9_, int i_10_) {
		int i_11_ = 0;
		if (i_10_ >= -98)
			method2694((byte) -87, 9);
		int i_12_ = i_8_;
		int i_13_ = -i_8_;
		int i_14_ = -1;
		int i_15_ = ParticleEmitterRaw.method1668(i_8_ + i_7_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 120);
		int i_16_ = ParticleEmitterRaw.method1668(-i_8_ + i_7_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 127);
		Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_9_]), i_15_, (byte) 124, i, i_16_);
		while (i_11_ < i_12_) {
			i_14_ += 2;
			i_13_ += i_14_;
			if (i_13_ > 0) {
				i_12_--;
				i_13_ -= i_12_ << 1;
				int i_17_ = -i_12_ + i_9_;
				int i_18_ = i_9_ + i_12_;
				if (i_18_ >= EmissiveTriangle.anInt952 && i_17_ <= RuntimeException_Sub1.anInt3391) {
					int i_19_ = ParticleEmitterRaw.method1668(i_11_ + i_7_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 127);
					int i_20_ = ParticleEmitterRaw.method1668(-i_11_ + i_7_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) 127);
					if (i_18_ <= RuntimeException_Sub1.anInt3391)
						Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_18_]), i_19_, (byte) 119, i, i_20_);
					if (i_17_ >= EmissiveTriangle.anInt952)
						Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_17_]), i_19_, (byte) 121, i, i_20_);
				}
			}
			int i_21_ = -++i_11_ + i_9_;
			int i_22_ = i_9_ + i_11_;
			if (i_22_ >= EmissiveTriangle.anInt952 && i_21_ <= RuntimeException_Sub1.anInt3391) {
				int i_23_ = ParticleEmitterRaw.method1668(i_12_ + i_7_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -34);
				int i_24_ = ParticleEmitterRaw.method1668(i_7_ - i_12_, Class288.anInt2652, ConfigurationDefinition.anInt676, (byte) -126);
				if (RuntimeException_Sub1.anInt3391 >= i_22_)
					Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_22_]), i_23_, (byte) -83, i, i_24_);
				if (EmissiveTriangle.anInt952 <= i_21_)
					Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_21_]), i_23_, (byte) -96, i, i_24_);
			}
		}
	}

	public final synchronized boolean isConsumer(ImageConsumer imageconsumer) {
		if (anImageConsumer6060 != imageconsumer)
			return false;
		return true;
	}

	final void method2692(int i, int i_25_, int i_26_, Graphics graphics, byte i_27_, int i_28_, int i_29_, int i_30_) {
		int i_31_ = 7 / ((-70 - i_27_) / 42);
		method2695(i_30_, 2, i_29_, i_25_, i_26_);
		Shape shape = graphics.getClip();
		graphics.clipRect(i, i_28_, i_30_, i_25_);
		graphics.drawImage(anImage6059, -i_26_ + i, -i_29_ + i_28_, aCanvas6061);
		graphics.setClip(shape);
	}

	public final synchronized void addConsumer(ImageConsumer imageconsumer) {
		anImageConsumer6060 = imageconsumer;
		imageconsumer.setDimensions(anInt4815, anInt4813);
		imageconsumer.setProperties(null);
		imageconsumer.setColorModel(aColorModel6058);
		imageconsumer.setHints(14);
	}

	public final synchronized void removeConsumer(ImageConsumer imageconsumer) {
		if (anImageConsumer6060 == imageconsumer)
			anImageConsumer6060 = null;
	}

	static final void method2698(Color color, byte i, Color color_32_, String string, Color color_33_, int i_34_) {
		try {
			Graphics graphics = Class230.aCanvas2209.getGraphics();
			if (i <= 41)
				method2699((byte) 109, null);
			if (Class326.aFont2903 == null)
				Class326.aFont2903 = new Font("Helvetica", 1, 13);
			if (color_33_ == null)
				color_33_ = new Color(140, 17, 17);
			if (color == null)
				color = new Color(140, 17, 17);
			if (color_32_ == null)
				color_32_ = new Color(255, 255, 255);
			try {
				if (Class387.anImage3713 == null)
					Class387.anImage3713 = Class230.aCanvas2209.createImage(Class241.anInt2301, Class384.anInt3254);
				Graphics graphics_35_ = Class387.anImage3713.getGraphics();
				graphics_35_.setColor(Color.black);
				graphics_35_.fillRect(0, 0, Class241.anInt2301, Class384.anInt3254);
				int i_36_ = Class241.anInt2301 / 2 - 152;
				int i_37_ = Class384.anInt3254 / 2 - 18;
				graphics_35_.setColor(color);
				graphics_35_.drawRect(i_36_, i_37_, 303, 33);
				graphics_35_.setColor(color_33_);
				graphics_35_.fillRect(i_36_ + 2, i_37_ + 2, i_34_ * 3, 30);
				graphics_35_.setColor(Color.black);
				graphics_35_.drawRect(i_36_ + 1, i_37_ + 1, 301, 31);
				graphics_35_.fillRect(i_34_ * 3 + i_36_ + 2, i_37_ + 2, -(i_34_ * 3) + 300, 30);
				graphics_35_.setFont(Class326.aFont2903);
				graphics_35_.setColor(color_32_);
				graphics_35_.drawString(string, ((304 - string.length() * 6) / 2 + i_36_), i_37_ + 22);
				if (Class55.aString657 != null) {
					graphics_35_.setFont(Class326.aFont2903);
					graphics_35_.setColor(color_32_);
					graphics_35_.drawString(Class55.aString657, (Class241.anInt2301 / 2 - (Class55.aString657.length() * 6 / 2)), Class384.anInt3254 / 2 - 26);
				}
				graphics.drawImage(Class387.anImage3713, 0, 0, null);
			} catch (Exception exception) {
				graphics.setColor(Color.black);
				graphics.fillRect(0, 0, Class241.anInt2301, Class384.anInt3254);
				int i_38_ = Class241.anInt2301 / 2 - 152;
				int i_39_ = Class384.anInt3254 / 2 - 18;
				graphics.setColor(color);
				graphics.drawRect(i_38_, i_39_, 303, 33);
				graphics.setColor(color_33_);
				graphics.fillRect(i_38_ + 2, i_39_ + 2, i_34_ * 3, 30);
				graphics.setColor(Color.black);
				graphics.drawRect(i_38_ + 1, i_39_ + 1, 301, 31);
				graphics.fillRect(i_34_ * 3 + 2 + i_38_, i_39_ + 2, -(i_34_ * 3) + 300, 30);
				graphics.setFont(Class326.aFont2903);
				graphics.setColor(color_32_);
				if (Class55.aString657 != null) {
					graphics.setFont(Class326.aFont2903);
					graphics.setColor(color_32_);
					graphics.drawString(Class55.aString657, (Class241.anInt2301 / 2 - (Class55.aString657.length() * 6 / 2)), Class384.anInt3254 / 2 - 26);
				}
				graphics.drawString(string, i_38_ + (304 - string.length() * 6) / 2, i_39_ + 22);
			}
		} catch (Exception exception) {
			Class230.aCanvas2209.repaint();
		}
	}

	final void method2689(Canvas canvas, int i, int i_40_, int i_41_) {
		aCanvas6061 = canvas;
		anInt4813 = i_40_;
		anInt4815 = i;
		anIntArray4810 = new int[anInt4813 * anInt4815];
		aColorModel6058 = new DirectColorModel(32, 16711680, 65280, 255);
		anImage6059 = aCanvas6061.createImage(this);
		method2696(32);
		aCanvas6061.prepareImage(anImage6059, aCanvas6061);
		method2696(32);
		aCanvas6061.prepareImage(anImage6059, aCanvas6061);
		method2696(32);
		aCanvas6061.prepareImage(anImage6059, aCanvas6061);
		if (i_41_ < 60)
			anImageConsumer6060 = null;
	}

	static final void method2699(byte i, NPC class338_sub3_sub1_sub3_sub2) {
		int i_42_ = 43 / ((i + 29) / 45);
		for (Class296_Sub36 class296_sub36 = ((Class296_Sub36) Class296_Sub15_Sub4.aClass155_6031.removeFirst((byte) 127)); class296_sub36 != null; class296_sub36 = ((Class296_Sub36) Class296_Sub15_Sub4.aClass155_6031.removeNext(1001))) {
			if (class338_sub3_sub1_sub3_sub2 == class296_sub36.aClass338_Sub3_Sub1_Sub3_Sub2_4878) {
				if (class296_sub36.aClass296_Sub45_Sub1_4871 != null) {
					Class16_Sub3.aClass296_Sub45_Sub5_3734.method3037(class296_sub36.aClass296_Sub45_Sub1_4871);
					class296_sub36.aClass296_Sub45_Sub1_4871 = null;
				}
				class296_sub36.unlink();
				break;
			}
		}
	}
}
