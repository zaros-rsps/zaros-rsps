package net.zaros.client;

/* Class41_Sub21 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class41_Sub21 extends Class41 {
	static Class209 aClass209_3798 = new Class209(17);
	static NodeDeque aClass155_3799 = new NodeDeque();

	final int method475(int i) {
		if (i <= 114)
			method386(-71);
		return anInt389;
	}

	final boolean method476(int i) {
		if (i != -25952)
			aClass209_3798 = null;
		if (!Class125.method1080((byte) 93, aClass296_Sub50_392.aClass41_Sub30_5006.method521(i + 26074)))
			return false;
		return true;
	}

	Class41_Sub21(int i, Class296_Sub50 class296_sub50) {
		super(i, class296_sub50);
	}

	public static void method477(byte i) {
		aClass209_3798 = null;
		if (i == 112)
			aClass155_3799 = null;
	}

	Class41_Sub21(Class296_Sub50 class296_sub50) {
		super(class296_sub50);
	}

	static final Class96 method478(int i, Model class178, int i_0_, int i_1_, byte i_2_) {
		if (class178 == null)
			return null;
		int i_3_ = 99 / ((44 - i_2_) / 56);
		Class96 class96 = new Class96(i_0_, i, i_1_, class178.na(), class178.V(), class178.RA(), class178.fa(), class178.EA(), class178.HA(), class178.G());
		return class96;
	}

	final int method380(int i, byte i_4_) {
		if (i_4_ != 41)
			return 8;
		if (!Class125.method1080((byte) -115, aClass296_Sub50_392.aClass41_Sub30_5006.method521(i_4_ + 86)))
			return 3;
		return 1;
	}

	final int method383(byte i) {
		if (i != 110)
			aClass155_3799 = null;
		return 0;
	}

	final void method386(int i) {
		if (aClass296_Sub50_392.aClass41_Sub30_5006.method527(i - 124) && !Class125.method1080((byte) -67, aClass296_Sub50_392.aClass41_Sub30_5006.method521(i + 115)))
			anInt389 = 0;
		if (i == 2) {
			if (anInt389 < 0 || anInt389 > 2)
				anInt389 = method383((byte) 110);
		}
	}

	static final void method479(ha var_ha, byte i, int i_5_) {
		if (!Class103.aBoolean1086 || !Class12.aBoolean133)
			Class360_Sub9.anInt5343 = 0;
		else {
			if (Class105_Sub2.aBoolean5695)
				Class165.aLong1690 = BITConfigsLoader.aClass327_2205.method3390(28);
			Class367.anInt3126 = 0;
			Class338_Sub3_Sub5.anInt6572 = 0;
			Class264.anInt2473 = 0;
			int[] is = var_ha.Y();
			Class369.anInt3136 = (int) ((float) is[3] / 3.0F);
			Class217.anInt2118 = (int) ((float) is[2] / 3.0F);
			var_ha.a(Class296_Sub49.anIntArray4983);
			if (((int) ((float) Class296_Sub49.anIntArray4983[0] / 3.0F) != Class213.anInt2110) || (Class123_Sub1_Sub1.anInt5813 != (int) ((float) Class296_Sub49.anIntArray4983[1] / 3.0F))) {
				Class123_Sub1_Sub1.anInt5813 = (int) ((float) Class296_Sub49.anIntArray4983[1] / 3.0F);
				Class213.anInt2110 = (int) ((float) Class296_Sub49.anIntArray4983[0] / 3.0F);
				Class128.anInt1315 = Class213.anInt2110 >> 1;
				Class365.anIntArray3115 = (new int[Class213.anInt2110 * Class123_Sub1_Sub1.anInt5813]);
				Class166_Sub1.anInt4302 = Class123_Sub1_Sub1.anInt5813 >> 1;
			}
			Class30.aClass373_317 = var_ha.o();
			Class360_Sub9.anInt5343 = 0;
			if (i <= -58) {
				for (int i_6_ = 0; Class349.anInt3036 > i_6_; i_6_++)
					Class348.method3672(za.aClass324Array5088[i_6_], 0, var_ha, i_5_);
				for (int i_7_ = 0; i_7_ < Class77.anInt880; i_7_++)
					Class348.method3672(Class116.aClass324Array3678[i_7_], 0, var_ha, i_5_);
				for (int i_8_ = 0; i_8_ < Class22.anInt249; i_8_++)
					Class348.method3672(Class343_Sub1.aClass324Array5273[i_8_], 0, var_ha, i_5_);
				Class296_Sub39_Sub20.anInt6255 = 0;
				if (Class360_Sub9.anInt5343 > 0) {
					int i_9_ = Class365.anIntArray3115.length;
					int i_10_ = i_9_ - i_9_ & 0x7;
					int i_11_ = 0;
					while (i_11_ < i_10_) {
						Class365.anIntArray3115[i_11_++] = 2147483647;
						Class365.anIntArray3115[i_11_++] = 2147483647;
						Class365.anIntArray3115[i_11_++] = 2147483647;
						Class365.anIntArray3115[i_11_++] = 2147483647;
						Class365.anIntArray3115[i_11_++] = 2147483647;
						Class365.anIntArray3115[i_11_++] = 2147483647;
						Class365.anIntArray3115[i_11_++] = 2147483647;
						Class365.anIntArray3115[i_11_++] = 2147483647;
					}
					while (i_11_ < i_9_)
						Class365.anIntArray3115[i_11_++] = 2147483647;
					Class296_Sub51_Sub6.anInt6368 = 1;
					for (int i_12_ = 0; Class360_Sub9.anInt5343 > i_12_; i_12_++) {
						Class324 class324 = Class29.aClass324Array305[i_12_];
						Class296_Sub39_Sub12.method2848(class324.aShortArray2851[3], class324.aShortArray2851[1], 30946, class324.aShortArray2859[0], class324.aShortArray2851[0], class324.aShortArray2859[1], class324.aShortArray2858[0], class324.aShortArray2858[1], class324.aShortArray2859[3], class324.aShortArray2858[3]);
						Class296_Sub39_Sub12.method2848(class324.aShortArray2851[3], class324.aShortArray2851[2], 30946, class324.aShortArray2859[1], class324.aShortArray2851[1], class324.aShortArray2859[2], class324.aShortArray2858[1], class324.aShortArray2858[2], class324.aShortArray2859[3], class324.aShortArray2858[3]);
					}
					Class296_Sub51_Sub6.anInt6368 = 2;
				}
				if (Class105_Sub2.aBoolean5695)
					Class84.aLong925 = (BITConfigsLoader.aClass327_2205.method3390(94) + -Class165.aLong1690);
			}
		}
	}

	final void method381(int i, byte i_13_) {
		anInt389 = i;
		if (i_13_ != -110)
			aClass155_3799 = null;
	}
}
