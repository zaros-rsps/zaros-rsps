package net.zaros.client;
/* Class170 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.FileOutputStream;

public  class MaterialRaw {
	public byte aByte1773;
	public byte aByte1774;
	public short aShort1775;
	public static FileOutputStream aFileOutputStream1776;
	public byte speed_u;
	public boolean small_sized;
	public boolean aBoolean1779;
	public boolean aBoolean1780;
	public static int anInt1781;
	public int anInt1782;
	public static IncomingPacket aClass231_1783;
	public byte aByte1784;
	public boolean aBoolean1785;
	public boolean aBoolean1786;
	public byte speed_v;
	public int anInt1788;
	public boolean aBoolean1789;
	public boolean aBoolean1790;
	public static int anInt1791 = 1;
	public boolean aBoolean1792;
	public byte aByte1793;
	public int anInt1794;
	public byte aByte1795;

	public static void method1669(byte i) {
		aFileOutputStream1776 = null;
		aClass231_1783 = null;
		if (i > -120)
			readScript(null);
	}

	static final boolean method1670(int i, char c) {
		try {
			int i_0_ = 34 % ((-55 - i) / 33);
			if (c < '0' || c > '9')
				return false;
			return true;
		} catch (RuntimeException runtimeexception) {
			throw Class16_Sub2.method244(runtimeexception, "jm.D(" + i + ',' + c + ')');
		}
	}

	static final void method1671(byte i) {
		if (Class69_Sub3.anInt5723 >= 0) {
			long l = Class72.method771(-108);
			Class69_Sub3.anInt5723 -= -Animation.aLong411 + l;
			if (Class69_Sub3.anInt5723 > 0) {
				int i_1_ = (Class69_Sub3.anInt5723 << 8) / InterfaceComponentSettings.anInt4826;
				int i_2_ = 255 - i_1_;
				float f = (float) i_1_ / 255.0F;
				Mobile.anInt6787 = (((i_1_ * (Class42_Sub1.anInt3835 & 0xff00ff) + (AnimBase.aClass190_4769.anInt1932 & 0xff00ff) * i_2_) & ~0xff00ff) + (((Class42_Sub1.anInt3835 & 0xff00) * i_1_ + (AnimBase.aClass190_4769.anInt1932 & 0xff00) * i_2_) & 0xff0000)) >>> 8;
				float f_3_ = -f + 1.0F;
				ha_Sub3.anInt4115 = ((((Class205.anInt3512 & 0xff00) * i_1_ + i_2_ * (AnimBase.aClass190_4769.anInt1946 & 0xff00)) & 0xff0000) + ((i_2_ * (AnimBase.aClass190_4769.anInt1946 & 0xff00ff) + i_1_ * (Class205.anInt3512 & 0xff00ff)) & ~0xff00ff)) >>> 8;
				Class296_Sub28.anInt4799 = (i_1_ * ObjectDefinitionLoader.anInt379 + AnimBase.aClass190_4769.anInt1931 * i_2_) >> 8;
				OutputStream_Sub2.aFloat41 = (f_3_ * (AnimBase.aClass190_4769.aFloat1938 - Class338_Sub3_Sub1_Sub5.aFloat6844) + Class338_Sub3_Sub1_Sub5.aFloat6844);
				Animation.aFloat406 = (AnimBase.aClass190_4769.aFloat1945 - Class317.aFloat3704) * f_3_ + Class317.aFloat3704;
				Class67.aFloat752 = (Class307.aFloat2748 + f_3_ * (-Class307.aFloat2748 + AnimBase.aClass190_4769.aFloat1933));
				Class30.aFloat316 = (f_3_ * (AnimBase.aClass190_4769.aFloat1939 - StaticMethods.aFloat5948) + StaticMethods.aFloat5948);
				Class69.aFloat3690 = (Class296_Sub51_Sub9.aFloat6388 + f_3_ * (-Class296_Sub51_Sub9.aFloat6388 + AnimBase.aClass190_4769.aFloat1930));
				r.aFloat6208 = (f_3_ * (AnimBase.aClass190_4769.aFloat1941 - CS2Call.aFloat4967) + CS2Call.aFloat4967);
				if (AnimBase.aClass190_4769.aClass241_1942 != Class105_Sub2.aClass241_5692)
					Class296_Sub51_Sub23.aClass241_6458 = (Class368_Sub16.aHa5527.a(Class105_Sub2.aClass241_5692, AnimBase.aClass190_4769.aClass241_1942, f_3_, Class296_Sub51_Sub23.aClass241_6458));
				if (AnimBase.aClass190_4769.aClass262_1935 != Class368_Sub7.aClass262_5461) {
					if (Class368_Sub7.aClass262_5461 == null) {
						s_Sub3.aClass262_5164 = AnimBase.aClass190_4769.aClass262_1935;
						if (s_Sub3.aClass262_5164 != null)
							s_Sub3.aClass262_5164.method2250(0, i_2_, (byte) -86);
					} else {
						s_Sub3.aClass262_5164 = Class368_Sub7.aClass262_5461;
						if (s_Sub3.aClass262_5164 != null)
							s_Sub3.aClass262_5164.method2250(255, i_2_, (byte) 4);
					}
				}
			} else {
				Class69.aFloat3690 = AnimBase.aClass190_4769.aFloat1930;
				r.aFloat6208 = AnimBase.aClass190_4769.aFloat1941;
				Class296_Sub51_Sub23.aClass241_6458 = AnimBase.aClass190_4769.aClass241_1942;
				Class67.aFloat752 = AnimBase.aClass190_4769.aFloat1933;
				OutputStream_Sub2.aFloat41 = AnimBase.aClass190_4769.aFloat1938;
				Animation.aFloat406 = AnimBase.aClass190_4769.aFloat1945;
				Class296_Sub28.anInt4799 = AnimBase.aClass190_4769.anInt1931;
				ha_Sub3.anInt4115 = AnimBase.aClass190_4769.anInt1946;
				Class30.aFloat316 = AnimBase.aClass190_4769.aFloat1939;
				Mobile.anInt6787 = AnimBase.aClass190_4769.anInt1932;
				if (s_Sub3.aClass262_5164 != null)
					s_Sub3.aClass262_5164.method2252((byte) -90);
				Class69_Sub3.anInt5723 = -1;
				s_Sub3.aClass262_5164 = AnimBase.aClass190_4769.aClass262_1935;
			}
			Animation.aLong411 = l;
		}
		if (i > -17)
			method1670(-106, '\ufff1');
	}

	static final CS2Script readScript(byte[] buffer) {
		CS2Script stack = new CS2Script();
		Packet reader = new Packet(buffer);
		reader.pos = reader.data.length - 2;
		int switchBlocksSize = reader.g2();
		int endOffset = -switchBlocksSize + (reader.data.length - 16 - 2);
		reader.pos = endOffset;
		int codeSize = reader.g4();
		stack.intLocalsCount = reader.g2();
		stack.stringLocalsCount = reader.g2();
		stack.longLocalsCount = reader.g2();
		stack.intStackSize = reader.g2();
		stack.stringStackSize = reader.g2();
		stack.longStackSize = reader.g2();
		int switchBlocksCount = reader.g1();
		if (switchBlocksCount > 0) {
			stack.switches = new HashTable[switchBlocksCount];
			for (int i = 0; switchBlocksCount > i; i++) {
				int numCases = reader.g2();
				HashTable blockTable = new HashTable(Class8.get_next_high_pow2(numCases));
				stack.switches[i] = blockTable;
				while (numCases-- > 0) {
					int caseValue = reader.g4();
					int jumpOffset = reader.g4();
					blockTable.put((long) caseValue, new IntegerNode(jumpOffset));
				}
			}
		}
		reader.pos = 0;
		stack.name = reader.gstrnull();
		stack.codeOpcodes = new int[codeSize];
		int oWrite = 0;
		while (endOffset > reader.pos) {
			int opcode = reader.g2();
			if (opcode == 3) {
				if (stack.stringPool == null)
					stack.stringPool = new String[codeSize];
				stack.stringPool[oWrite] = reader.gstr().intern();
				
				if (stack.stringPool[oWrite].toLowerCase().contains("runescape")) {
					stack.stringPool[oWrite] = stack.stringPool[oWrite].replace("RuneScape", ClientUtility.NAME);
					stack.stringPool[oWrite] = stack.stringPool[oWrite].replace("runescape", ClientUtility.NAME);
					stack.stringPool[oWrite] = stack.stringPool[oWrite].replace("RuneScape", ClientUtility.NAME);
				}
				
				String text = stack.stringPool[oWrite];
				String newText = null;
				//System.out.println("text=" + text);
				if (text.equalsIgnoreCase("Please enter your email address again here.")) {
					newText = "Please enter your username again here.";
				} else if (text.equalsIgnoreCase("This email address is available for use.")) {
					newText = "This username is available for use.";
				} else if (text.equalsIgnoreCase("Please enter your email address here.")) {
					newText = "Please enter your username here.";
				} else if (text.equalsIgnoreCase("Please enter a valid Email address.")) {
					newText = "Please enter a valid username.";
				} else if (text.equalsIgnoreCase("Please ensure both Email addresses match.")) {
					newText = "Please ensure both usernames match.";
				} else if (text.equalsIgnoreCase("Email already in use. Try a different email or click ")) {
					newText = "Username already in use. Try a different name or click ";
				} else if (text.equalsIgnoreCase("Your password is too similar to your Email address.")) {
					newText = "Your password is too similar to your username.";
				} else if (text.equalsIgnoreCase("Both email addresses match.")) {
					newText = "Both usernames match.";
				} else if (text.equalsIgnoreCase("High-risk Wilderness World")) {
					newText = "Global-PvP World";
				} else if (text.equalsIgnoreCase("Warning: This is a High-risk Wilderness world.")) {
					newText = "Warning: This is a Global-pvp world.";
				} else if (text.equalsIgnoreCase("While you are in the Wilderness on this world, you will not be permitted to use the Protect Item prayer or curse, so you may lose ALL your items when you die.")) {
					newText = "All areas in this world are dangerous, except for banks. You must be prepared to fight when you log in.";
				}
				if (newText != null) {
					stack.stringPool[oWrite] = newText;
				}
			} else if (opcode != 54) {
				if (stack.intPool == null)
					stack.intPool = new int[codeSize];
				if (opcode >= 150 || opcode == 21 || opcode == 38 || opcode == 39)
					stack.intPool[oWrite] = reader.g1();
				else
					stack.intPool[oWrite] = reader.g4();
			} else {
				if (stack.longPool == null)
					stack.longPool = new long[codeSize];
				stack.longPool[oWrite] = reader.g8();
			}
			stack.codeOpcodes[oWrite++] = opcode;
		}
		return stack;
	}

	static final boolean method1673(int i, int i_14_, byte i_15_) {
		if (i_15_ != -5)
			readScript(null);
		if ((i & 0x22) == 0)
			return false;
		return true;
	}

	static final void method1674(Mobile class338_sub3_sub1_sub3, byte i) {
		Animator class44 = class338_sub3_sub1_sub3.aClass44_6777;
		if (class44.method570((byte) 40) && class44.method559(1, (byte) 126) && class44.method546(-126)) {
			if (class338_sub3_sub1_sub3.aBoolean6783) {
				class44.method549((byte) 115, class338_sub3_sub1_sub3.method3516(false).method2348((byte) -110));
				class338_sub3_sub1_sub3.aBoolean6783 = class44.method570((byte) 40);
			}
			class44.method547(14899);
		}
		for (int i_16_ = 0; i_16_ < class338_sub3_sub1_sub3.aClass212Array6817.length; i_16_++) {
			if (class338_sub3_sub1_sub3.aClass212Array6817[i_16_].anInt2105 != -1) {
				Animator class44_17_ = (class338_sub3_sub1_sub3.aClass212Array6817[i_16_].aClass44_2108);
				if (class44_17_.method567(1)) {
					Graphic class23 = (Class157.graphicsLoader.getGraphic((class338_sub3_sub1_sub3.aClass212Array6817[i_16_].anInt2105)));
					Animation class43 = class44_17_.method563(i ^ 0x7f);
					if (class23.aBoolean267) {
						if (class43.anInt404 != 3) {
							if (class43.anInt404 == 1 && class338_sub3_sub1_sub3.anInt6829 > 0 && (Class29.anInt307 >= class338_sub3_sub1_sub3.anInt6809) && (Class29.anInt307 > class338_sub3_sub1_sub3.anInt6807))
								continue;
						} else if (class338_sub3_sub1_sub3.anInt6829 > 0 && (Class29.anInt307 >= class338_sub3_sub1_sub3.anInt6809) && (class338_sub3_sub1_sub3.anInt6807 < Class29.anInt307)) {
							class44_17_.method549((byte) 115, -1);
							class338_sub3_sub1_sub3.aClass212Array6817[i_16_].anInt2105 = -1;
							continue;
						}
					}
				}
				if (class44_17_.method559(1, (byte) 123) && class44_17_.method546(i ^ ~0x4d)) {
					class44_17_.method549((byte) 115, -1);
					class338_sub3_sub1_sub3.aClass212Array6817[i_16_].anInt2105 = -1;
				}
			}
		}
		Animator class44_18_ = class338_sub3_sub1_sub3.aClass44_6802;
		do {
			if (class44_18_.method570((byte) 40)) {
				Animation class43 = class44_18_.method563(-1);
				if (class43.anInt404 == 3) {
					if (class338_sub3_sub1_sub3.anInt6829 > 0 && (class338_sub3_sub1_sub3.anInt6809 <= Class29.anInt307) && (class338_sub3_sub1_sub3.anInt6807 < Class29.anInt307)) {
						class338_sub3_sub1_sub3.anIntArray6789 = null;
						class44_18_.method549((byte) 115, -1);
						break;
					}
				} else if (class43.anInt404 == 1) {
					if (class338_sub3_sub1_sub3.anInt6829 <= 0 || class338_sub3_sub1_sub3.anInt6809 > Class29.anInt307 || (Class29.anInt307 <= class338_sub3_sub1_sub3.anInt6807))
						class44_18_.method558(false, 0);
					else {
						class44_18_.method558(false, 1);
						break;
					}
				}
				if (class44_18_.method559(1, (byte) 127) && class44_18_.method546(i + 8)) {
					class338_sub3_sub1_sub3.anIntArray6789 = null;
					class44_18_.method549((byte) 115, -1);
				}
			}
		} while (false);
		if (i == -128) {
			for (int i_19_ = 0; i_19_ < (class338_sub3_sub1_sub3.aClass44_Sub1_Sub1Array6814).length; i_19_++) {
				Class44_Sub1_Sub1 class44_sub1_sub1 = (class338_sub3_sub1_sub3.aClass44_Sub1_Sub1Array6814[i_19_]);
				if (class44_sub1_sub1 != null) {
					if (class44_sub1_sub1.anInt5808 > 0)
						class44_sub1_sub1.anInt5808--;
					else if (class44_sub1_sub1.method559(1, (byte) 125) && class44_sub1_sub1.method546(49))
						class338_sub3_sub1_sub3.aClass44_Sub1_Sub1Array6814[i_19_] = null;
				}
			}
		}
	}

	public MaterialRaw() {
		/* empty */
	}

	static {
		aClass231_1783 = new IncomingPacket(81, 10);
	}
}
