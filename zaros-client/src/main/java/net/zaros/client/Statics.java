package net.zaros.client;

import net.zaros.client.configs.objtype.ObjType;

public class Statics {

	static final void method985(int i, ha var_ha, byte i_5_, int i_6_) {
		if (i >= 0 && i_6_ >= 0 && Class188.anInt1927 != 0 && NPCNode.anInt4625 != 0) {
			int i_7_;
			int i_8_;
			int i_9_;
			int i_10_;
			Class373 class373;
			int i_11_;
			int i_12_;
			if (Class368_Sub5_Sub2.aBoolean6597) {
				Class41_Sub4.method407(false, i_5_ ^ 0x639f);
				class373 = var_ha.o();
				int[] is = var_ha.Y();
				i_7_ = is[2];
				i_9_ = is[0];
				i_8_ = is[1];
				i_10_ = is[3];
				i_12_ = ItemsNode.method2438(false, 0) + i;
				i_11_ = i_6_ + Class65.method708(false, (byte) 66);
			} else {
				var_ha.DA(Class360_Sub8.anInt5340, Js5.anInt1416, Class188.anInt1927, NPCNode.anInt4625);
				i_7_ = Class188.anInt1927;
				i_8_ = Js5.anInt1416;
				i_9_ = Class360_Sub8.anInt5340;
				i_10_ = NPCNode.anInt4625;
				var_ha.KA(Class41_Sub5.anInt3754, ModeWhat.anInt1189, Class188.anInt1927, NPCNode.anInt4625);
				class373 = var_ha.m();
				class373.method3907(MaterialRaw.anInt1781, Class296_Sub39_Sub12.anInt6195, Class296_Sub11.anInt4649, ParamType.anInt3251, Class27.anInt296, Class75.anInt870);
				i_11_ = i_6_;
				i_12_ = i;
				var_ha.a(class373);
			}
			if (i_10_ == 0)
				i_10_ = 1;
			if (i_7_ == 0)
				i_7_ = 1;
			Class41.method384(true, false);
			if (Class244.aSArray2320 != null && (!Class127.aBoolean1304 || (Class321.anInt2824 & 0x40) != 0)) {
				int i_13_ = -1;
				int i_14_ = -1;
				int i_15_ = var_ha.i();
				int i_16_ = var_ha.XA();
				int i_17_;
				int i_18_;
				int i_19_;
				int i_20_;
				if (!Class296_Sub39_Sub10.aBoolean6177) {
					i_20_ = i_16_ * (i_11_ - i_8_) / i_10_;
					i_19_ = (i_11_ - i_8_) * i_15_ / i_10_;
					i_18_ = i_16_ * (i_12_ - i_9_) / i_7_;
					i_17_ = (-i_9_ + i_12_) * i_15_ / i_7_;
				} else {
					i_17_ = i_18_ = (i_12_ - i_9_) * ModeWhat.anInt1192 / i_7_;
					i_19_ = i_20_ = ModeWhat.anInt1192 * (-i_8_ + i_11_) / i_10_;
				}
				int[] is = {i_17_, i_19_, i_15_};
				int[] is_21_ = {i_18_, i_20_, i_16_};
				class373.method3908(is);
				class373.method3908(is_21_);
				float f = Class41_Sub5.method411(4, (float) is_21_[1], (float) is[1], (float) is[2], (float) is_21_[2], (float) is_21_[0], false, (float) is[0]);
				if (f > 0.0F) {
					int i_22_ = -is[0] + is_21_[0];
					int i_23_ = is_21_[2] - is[2];
					int i_24_ = (int) ((float) is[0] + (float) i_22_ * f);
					int i_25_ = (int) ((float) is[2] + (float) i_23_ * f);
					i_13_ = i_24_ + (Class296_Sub51_Sub11.localPlayer.getSize() - 1 << 8) >> 9;
					i_14_ = (Class296_Sub51_Sub11.localPlayer.getSize() - 1 << 8) + i_25_ >> 9;
					int i_26_ = (Class296_Sub51_Sub11.localPlayer.z);
					if (i_26_ < 3 && ((Class41_Sub18.aByteArrayArrayArray3786[1][i_24_ >> 9][i_25_ >> 9]) & 0x2) != 0)
						i_26_++;
				}
				if (i_13_ != -1 && i_14_ != -1) {
					if (Class127.aBoolean1304 && (Class321.anInt2824 & 0x40) != 0) {
						InterfaceComponent class51 = Class103.method894(0, Class366_Sub4.anInt5375, Class180.anInt1857);
						if (class51 == null)
							Class285.method2368(i_5_ + 15);
						else
							StaticMethods.method1008(-92, true, i_13_, IOException_Sub1.anInt36, -1, Class228.aString2200, i_14_, 0L, 49, true, (long) (i_14_ | i_13_ << 0), " ->", false);
					} else {
						if (StaticMethods.aBoolean2913)
							StaticMethods.method1008(i_5_ - 80, true, i_13_, -1, -1, TranslatableString.aClass120_1223.getTranslation(Class394.langID), i_14_, 0L, 18, true, (long) (i_14_ | i_13_ << 0), "", false);
						StaticMethods.method1008(19, true, i_13_, Class208.anInt2091, -1, Class296_Sub51_Sub37.aString6532, i_14_, 0L, 44, true, (long) (i_14_ | i_13_ << 0), "", false);
					}
				}
			}
			if (Class368_Sub5_Sub2.aBoolean6597)
				Class296_Sub28.method2681(-19895);
			if (i_5_ != -15)
				AdvancedMemoryCache.anInt1166 = -83;
			for (int i_27_ = 0; i_27_ < (Class368_Sub5_Sub2.aBoolean6597 ? 2 : 1); i_27_++) {
				boolean bool = i_27_ == 0;
				Class264 class264 = (bool ? CS2Stack.aClass264_2249 : Class379_Sub3.aClass264_5687);
				int i_28_ = i;
				int i_29_ = i_6_;
				if (Class368_Sub5_Sub2.aBoolean6597) {
					Class41_Sub4.method407(bool, -25490);
					i_28_ += ItemsNode.method2438(bool, 0);
					i_29_ += Class65.method708(bool, (byte) 66);
				}
				Class404 class404 = class264.aClass404_2468;
				for (Class338_Sub2 class338_sub2 = (Class338_Sub2) class404.method4160((byte) -53); class338_sub2 != null; class338_sub2 = (Class338_Sub2) class404.method4163(-24917)) {
					if ((SubCache.aBoolean2709 || (class338_sub2.aClass338_Sub3_5193.z == (Class296_Sub51_Sub11.localPlayer.z))) && class338_sub2.method3455(i_28_, i_29_, (byte) 51, var_ha)) {
						boolean bool_30_ = false;
						boolean bool_31_ = false;
						int i_32_;
						int i_33_;
						if (!(class338_sub2.aClass338_Sub3_5193 instanceof Class338_Sub3_Sub1)) {
							i_33_ = (class338_sub2.aClass338_Sub3_5193.tileX >> 9);
							i_32_ = (class338_sub2.aClass338_Sub3_5193.tileY >> 9);
						} else {
							i_32_ = (((Class338_Sub3_Sub1) class338_sub2.aClass338_Sub3_5193).aShort6564);
							i_33_ = (((Class338_Sub3_Sub1) class338_sub2.aClass338_Sub3_5193).aShort6560);
						}
						if (class338_sub2.aClass338_Sub3_5193 instanceof Player) {
							Player class338_sub3_sub1_sub3_sub1 = ((Player) class338_sub2.aClass338_Sub3_5193);
							int i_34_ = class338_sub3_sub1_sub3_sub1.getSize();
							if (((i_34_ & 0x1) == 0 && (class338_sub3_sub1_sub3_sub1.tileX & 0x1ff) == 0 && (class338_sub3_sub1_sub3_sub1.tileY & 0x1ff) == 0) || ((i_34_ & 0x1) == 1 && (class338_sub3_sub1_sub3_sub1.tileX & 0x1ff) == 256 && (class338_sub3_sub1_sub3_sub1.tileY & 0x1ff) == 256)) {
								int i_35_ = (class338_sub3_sub1_sub3_sub1.tileX - (class338_sub3_sub1_sub3_sub1.getSize() - 1 << 8));
								int i_36_ = (class338_sub3_sub1_sub3_sub1.tileY - (class338_sub3_sub1_sub3_sub1.getSize() - 1 << 8));
								for (int i_37_ = 0; i_37_ < Class367.npcsCount; i_37_++) {
									NPCNode class296_sub7 = ((NPCNode) (Class41_Sub18.localNpcs.get((long) (ReferenceTable.npcIndexes[i_37_]))));
									if (class296_sub7 != null) {
										NPC class338_sub3_sub1_sub3_sub2 = (class296_sub7.value);
										if ((Class29.anInt307 != (class338_sub3_sub1_sub3_sub2.anInt6798)) && (class338_sub3_sub1_sub3_sub2.aBoolean6774)) {
											int i_38_ = (-((class338_sub3_sub1_sub3_sub2.definition.size) - 1 << 8) + (class338_sub3_sub1_sub3_sub2.tileX));
											int i_39_ = ((class338_sub3_sub1_sub3_sub2.tileY) - ((class338_sub3_sub1_sub3_sub2.definition.size) - 1 << 8));
											if (i_38_ >= i_35_ && ((class338_sub3_sub1_sub3_sub2.definition.size) <= (-(-i_35_ + i_38_ >> 9) + (class338_sub3_sub1_sub3_sub1.getSize()))) && i_39_ >= i_36_ && ((class338_sub3_sub1_sub3_sub2.definition.size) <= (-(-i_36_ + i_39_ >> 9) + (class338_sub3_sub1_sub3_sub1.getSize())))) {
												Class172.method1681(((Class296_Sub51_Sub11.localPlayer.z) != (class338_sub2.aClass338_Sub3_5193.z)), class338_sub3_sub1_sub3_sub2, -15204);
												class338_sub3_sub1_sub3_sub2.anInt6798 = Class29.anInt307;
											}
										}
									}
								}
								int i_40_ = PlayerUpdate.visiblePlayersCount;
								int[] is = PlayerUpdate.visiblePlayersIndexes;
								for (int i_41_ = 0; i_41_ < i_40_; i_41_++) {
									Player class338_sub3_sub1_sub3_sub1_42_ = (PlayerUpdate.visiblePlayers[is[i_41_]]);
									if ((class338_sub3_sub1_sub3_sub1_42_ != null) && (class338_sub3_sub1_sub3_sub1_42_.anInt6798) != Class29.anInt307 && (class338_sub3_sub1_sub3_sub1 != class338_sub3_sub1_sub3_sub1_42_) && (class338_sub3_sub1_sub3_sub1_42_.aBoolean6774)) {
										int i_43_ = ((class338_sub3_sub1_sub3_sub1_42_.tileX) - (class338_sub3_sub1_sub3_sub1_42_.getSize() - 1 << 8));
										int i_44_ = ((class338_sub3_sub1_sub3_sub1_42_.tileY) - (class338_sub3_sub1_sub3_sub1_42_.getSize() - 1 << 8));
										if (i_35_ <= i_43_ && (class338_sub3_sub1_sub3_sub1_42_.getSize() <= (class338_sub3_sub1_sub3_sub1.getSize() - (-i_35_ + i_43_ >> 9))) && i_44_ >= i_36_ && (class338_sub3_sub1_sub3_sub1_42_.getSize() <= (-(i_44_ - i_36_ >> 9) + (class338_sub3_sub1_sub3_sub1.getSize())))) {
											Class368_Sub6.method3832(((Class296_Sub51_Sub11.localPlayer.z) != (class338_sub2.aClass338_Sub3_5193.z)), i_5_ + 2024, class338_sub3_sub1_sub3_sub1_42_);
											class338_sub3_sub1_sub3_sub1_42_.anInt6798 = Class29.anInt307;
										}
									}
								}
							}
							if (class338_sub3_sub1_sub3_sub1.anInt6798 == Class29.anInt307)
								continue;
							Class368_Sub6.method3832(((Class296_Sub51_Sub11.localPlayer.z) != (class338_sub2.aClass338_Sub3_5193.z)), i_5_ + 2024, class338_sub3_sub1_sub3_sub1);
							class338_sub3_sub1_sub3_sub1.anInt6798 = Class29.anInt307;
						}
						if (class338_sub2.aClass338_Sub3_5193 instanceof NPC) {
							NPC class338_sub3_sub1_sub3_sub2 = ((NPC) class338_sub2.aClass338_Sub3_5193);
							if (class338_sub3_sub1_sub3_sub2.definition != null) {
								if ((((class338_sub3_sub1_sub3_sub2.definition.size) & 0x1) == 0 && (class338_sub3_sub1_sub3_sub2.tileX & 0x1ff) == 0 && (class338_sub3_sub1_sub3_sub2.tileY & 0x1ff) == 0) || (((class338_sub3_sub1_sub3_sub2.definition.size) & 0x1) == 1 && ((class338_sub3_sub1_sub3_sub2.tileX) & 0x1ff) == 256 && ((class338_sub3_sub1_sub3_sub2.tileY) & 0x1ff) == 256)) {
									int i_45_ = ((class338_sub3_sub1_sub3_sub2.tileX) - ((class338_sub3_sub1_sub3_sub2.definition.size) - 1 << 8));
									int i_46_ = ((class338_sub3_sub1_sub3_sub2.tileY) - ((class338_sub3_sub1_sub3_sub2.definition.size) - 1 << 8));
									for (int i_47_ = 0; Class367.npcsCount > i_47_; i_47_++) {
										NPCNode class296_sub7 = ((NPCNode) (Class41_Sub18.localNpcs.get((long) (ReferenceTable.npcIndexes[i_47_]))));
										if (class296_sub7 != null) {
											NPC class338_sub3_sub1_sub3_sub2_48_ = (class296_sub7.value);
											if ((Class29.anInt307 != (class338_sub3_sub1_sub3_sub2_48_.anInt6798)) && (class338_sub3_sub1_sub3_sub2_48_ != class338_sub3_sub1_sub3_sub2) && (class338_sub3_sub1_sub3_sub2_48_.aBoolean6774)) {
												int i_49_ = ((class338_sub3_sub1_sub3_sub2_48_.tileX) - ((class338_sub3_sub1_sub3_sub2_48_.definition.size) - 1 << 8));
												int i_50_ = ((class338_sub3_sub1_sub3_sub2_48_.tileY) - ((class338_sub3_sub1_sub3_sub2_48_.definition.size) - 1 << 8));
												if (i_49_ >= i_45_ && ((class338_sub3_sub1_sub3_sub2_48_.definition.size) <= ((class338_sub3_sub1_sub3_sub2.definition.size) - (-i_45_ + i_49_ >> 9))) && i_50_ >= i_46_ && ((class338_sub3_sub1_sub3_sub2_48_.definition.size) <= ((class338_sub3_sub1_sub3_sub2.definition.size) - (-i_46_ + i_50_ >> 9)))) {
													Class172.method1681(((class338_sub2.aClass338_Sub3_5193.z) != (Class296_Sub51_Sub11.localPlayer.z)), class338_sub3_sub1_sub3_sub2_48_, i_5_ - 15189);
													class338_sub3_sub1_sub3_sub2_48_.anInt6798 = Class29.anInt307;
												}
											}
										}
									}
									int i_51_ = PlayerUpdate.visiblePlayersCount;
									int[] is = PlayerUpdate.visiblePlayersIndexes;
									for (int i_52_ = 0; i_52_ < i_51_; i_52_++) {
										Player class338_sub3_sub1_sub3_sub1 = (PlayerUpdate.visiblePlayers[is[i_52_]]);
										if ((class338_sub3_sub1_sub3_sub1 != null) && (Class29.anInt307 != (class338_sub3_sub1_sub3_sub1.anInt6798)) && (class338_sub3_sub1_sub3_sub1.aBoolean6774)) {
											int i_53_ = ((class338_sub3_sub1_sub3_sub1.tileX) - (class338_sub3_sub1_sub3_sub1.getSize() - 1 << 8));
											int i_54_ = ((class338_sub3_sub1_sub3_sub1.tileY) - ((class338_sub3_sub1_sub3_sub1.getSize()) - 1 << 8));
											if (i_45_ <= i_53_ && (class338_sub3_sub1_sub3_sub1.getSize() <= (-(i_53_ - i_45_ >> 9) + (class338_sub3_sub1_sub3_sub2.definition.size))) && i_54_ >= i_46_ && (class338_sub3_sub1_sub3_sub1.getSize() <= (-(i_54_ - i_46_ >> 9) + (class338_sub3_sub1_sub3_sub2.definition.size)))) {
												Class368_Sub6.method3832(((Class296_Sub51_Sub11.localPlayer.z) != (class338_sub2.aClass338_Sub3_5193.z)), 2009, class338_sub3_sub1_sub3_sub1);
												class338_sub3_sub1_sub3_sub1.anInt6798 = Class29.anInt307;
											}
										}
									}
								}
								if (Class29.anInt307 == class338_sub3_sub1_sub3_sub2.anInt6798)
									continue;
								Class172.method1681(((Class296_Sub51_Sub11.localPlayer.z) != (class338_sub2.aClass338_Sub3_5193.z)), class338_sub3_sub1_sub3_sub2, i_5_ - 15189);
								class338_sub3_sub1_sub3_sub2.anInt6798 = Class29.anInt307;
							}
						}
						if (class338_sub2.aClass338_Sub3_5193 instanceof Class338_Sub3_Sub2_Sub1) {
							int i_55_ = Class206.worldBaseX + i_33_;
							int i_56_ = i_32_ + Class41_Sub26.worldBaseY;
							Class296_Sub56 class296_sub56 = ((Class296_Sub56) (Class296_Sub11.aClass263_4647.get((long) (i_55_ | (i_56_ << 14 | (class338_sub2.aClass338_Sub3_5193.z) << 28)))));
							if (class296_sub56 != null) {
								int i_57_ = 0;
								for (Class296_Sub2 class296_sub2 = ((Class296_Sub2) class296_sub56.aClass155_5080.method1569(127)); class296_sub2 != null; class296_sub2 = ((Class296_Sub2) class296_sub56.aClass155_5080.method1576(1725258273))) {
									ObjType class158 = (Class296_Sub39_Sub1.itemDefinitionLoader.list(class296_sub2.anInt4591));
									if (Class127.aBoolean1304 && ((class338_sub2.aClass338_Sub3_5193.z) == (Class296_Sub51_Sub11.localPlayer.z))) {
										ParamType class383 = (Class353.anInt3047 == -1 ? null : (Class296_Sub22.itemExtraDataDefinitionLoader.list(Class353.anInt3047)));
										if ((Class321.anInt2824 & 0x1) != 0 && (class383 == null || ((class158.getIntParam(Class353.anInt3047, class383.defaultValue)) != class383.defaultValue)))
											StaticMethods.method1008(18, false, i_33_, IOException_Sub1.anInt36, -1, Class228.aString2200, i_32_, (long) (class296_sub2.anInt4591), 10, true, (long) i_57_, (ConfigsRegister.aString3672 + " -> <col=ff9040>" + class158.name), false);
									}
									if ((Class296_Sub51_Sub11.localPlayer.z) == (class338_sub2.aClass338_Sub3_5193.z)) {
										String[] strings = class158.groundOptions;
										for (int i_58_ = strings.length - 1; i_58_ >= 0; i_58_--) {
											if (strings[i_58_] != null) {
												int i_59_ = 0;
												if (i_58_ == 0)
													i_59_ = 2;
												int i_60_ = Class106_Sub1.anInt3903;
												if (i_58_ == 1)
													i_59_ = 15;
												if (i_58_ == 2)
													i_59_ = 58;
												if (i_58_ == 3)
													i_59_ = 11;
												if (i_58_ == 4)
													i_59_ = 13;
												if (i_58_ == 5)
													i_59_ = 1003;
												if (i_58_ == class158.cursor1)
													i_60_ = class158.cursor1op;
												if (class158.cursor2 == i_58_)
													i_60_ = class158.cursor2op;
												StaticMethods.method1008(i_5_ - 83, false, i_33_, i_60_, -1, strings[i_58_], i_32_, (long) (class296_sub2.anInt4591), i_59_, true, (long) i_57_, ("<col=ff9040>" + class158.name), false);
											}
										}
									}
									i_57_++;
								}
							}
						}
						if (class338_sub2.aClass338_Sub3_5193 instanceof Interface14) {
							Interface14 interface14 = ((Interface14) class338_sub2.aClass338_Sub3_5193);
							ObjectDefinition class70 = (Class379.objectDefinitionLoader.getObjectDefinition(interface14.method57((byte) 115)));
							if (class70.transforms != null)
								class70 = class70.method757((Class16_Sub3_Sub1.configsRegister), false);
							if (class70 != null) {
								if (Class127.aBoolean1304 && ((Class296_Sub51_Sub11.localPlayer.z) == (class338_sub2.aClass338_Sub3_5193.z))) {
									ParamType class383 = (Class353.anInt3047 != -1 ? (Class296_Sub22.itemExtraDataDefinitionLoader.list(Class353.anInt3047)) : null);
									if ((Class321.anInt2824 & 0x4) != 0 && (class383 == null || (class70.method755((Class353.anInt3047), (byte) -82, (class383.defaultValue)) != class383.defaultValue)))
										StaticMethods.method1008(-125, false, i_33_, IOException_Sub1.anInt36, -1, Class228.aString2200, i_32_, (Class296_Sub15.method2518((byte) 97, interface14, i_32_, i_33_)), 48, true, (long) interface14.hashCode(), (ConfigsRegister.aString3672 + " -> <col=00ffff>" + class70.name), false);
								}
								if (class338_sub2.aClass338_Sub3_5193.z == (Class296_Sub51_Sub11.localPlayer.z)) {
									String[] strings = class70.options;
									if (strings != null) {
										for (int i_61_ = strings.length - 1; i_61_ >= 0; i_61_--) {
											if (strings[i_61_] != null) {
												int i_62_ = 0;
												if (i_61_ == 0)
													i_62_ = 45;
												int i_63_ = Class106_Sub1.anInt3903;
												if (i_61_ == 1)
													i_62_ = 22;
												if (i_61_ == 2)
													i_62_ = 4;
												if (i_61_ == 3)
													i_62_ = 57;
												if (i_61_ == 4)
													i_62_ = 1012;
												if (i_61_ == 5)
													i_62_ = 1009;
												if (i_61_ == class70.anInt788)
													i_63_ = class70.anInt827;
												if (class70.anInt764 == i_61_)
													i_63_ = class70.anInt828;
												StaticMethods.method1008(-90, false, i_33_, i_63_, -1, strings[i_61_], i_32_, (Class296_Sub15.method2518((byte) 70, interface14, i_32_, i_33_)), i_62_, true, (long) interface14.hashCode(), ("<col=00ffff>" + class70.name), false);
											}
										}
									}
								}
							}
						}
					}
				}
				if (Class368_Sub5_Sub2.aBoolean6597)
					Class296_Sub28.method2681(i_5_ ^ 0x4db8);
			}
			Class41.method384(false, false);
		}
	}

	static final int method626(int i, byte i_13_, int i_14_) {
		int i_15_ = (Class44_Sub2.method579(i + 45365, -1, 4, i_14_ + 91923) - 128 + (Class44_Sub2.method579(i + 10294, -1, 2, i_14_ + 37821) - 128 >> 1) + (Class44_Sub2.method579(i, -1, 1, i_14_) - 128 >> 2));
		if (i_13_ <= 8)
			return -33;
		i_15_ = (int) ((double) i_15_ * 0.3) + 35;
		if (i_15_ >= 10) {
			if (i_15_ > 60)
				i_15_ = 60;
		} else
			i_15_ = 10;
		return i_15_;
	}

	static final int method629(boolean bool, int i) {
		if (bool)
			return 90;
		int i_27_ = i >>> 1;
		i_27_ |= i_27_ >>> 1;
		i_27_ |= i_27_ >>> 2;
		i_27_ |= i_27_ >>> 4;
		i_27_ |= i_27_ >>> 8;
		i_27_ |= i_27_ >>> 16;
		return i & (i_27_ ^ 0xffffffff);
	}

	static final void method634(int i, int[][] is) {
		Class296_Sub51_Sub37.anIntArrayArray6536 = is;
		int i_39_ = -103 % ((i + 4) / 62);
	}

}
