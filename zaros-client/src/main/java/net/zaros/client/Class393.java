package net.zaros.client;

/* Class393 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class393 {
	static long aLong3301;
	static int anInt3302;
	static int anInt3303 = -1;

	static final void method4056() {
		for (int i = 0; i < Class368_Sub4.anInt5440; i++) {
			if (!Class296_Sub17_Sub2.aBooleanArray6047[i]) {
				Class93 class93 = Class302.aClass93Array2721[i];
				Class296_Sub35 class296_sub35 = class93.aClass296_Sub35_999;
				int i_0_ = class93.anInt1006;
				int i_1_ = class296_sub35.method2748(40) - Class304.anInt2737;
				int i_2_ = (i_1_ * 2 >> Class313.anInt2779) + 1;
				int i_3_ = 0;
				int[] is = new int[i_2_ * i_2_];
				int i_4_ = (class296_sub35.method2749(true) - i_1_ >> Class313.anInt2779);
				int i_5_ = (class296_sub35.method2750(-4444) - i_1_ >> Class313.anInt2779);
				int i_6_ = (class296_sub35.method2750(-4444) + i_1_ >> Class313.anInt2779);
				if (i_5_ < 0) {
					i_3_ -= i_5_;
					i_5_ = 0;
				}
				if (i_6_ >= Class368_Sub12.anInt5488)
					i_6_ = Class368_Sub12.anInt5488 - 1;
				for (int i_7_ = i_5_; i_7_ <= i_6_; i_7_++) {
					int i_8_ = class93.aShortArray1010[i_3_];
					int i_9_ = i_8_ >>> 8;
					int i_10_ = i_3_ * i_2_ + i_9_;
					int i_11_ = i_4_ + (i_8_ >>> 8);
					int i_12_ = i_11_ + (i_8_ & 0xff) - 1;
					if (i_11_ < 0) {
						i_10_ -= i_11_;
						i_11_ = 0;
					}
					if (i_12_ >= Class228.anInt2201)
						i_12_ = Class228.anInt2201 - 1;
					for (int i_13_ = i_11_; i_13_ <= i_12_; i_13_++) {
						int i_14_ = 1;
						Class338_Sub3_Sub1 class338_sub3_sub1 = (Class123_Sub2.method1070(i_0_, i_13_, i_7_, Class338_Sub3_Sub1.class));
						if (class338_sub3_sub1 != null && class338_sub3_sub1.aByte6562 != 0) {
							if (class338_sub3_sub1.aByte6562 == 1) {
								boolean bool = i_13_ - 1 >= i_11_;
								boolean bool_15_ = i_13_ + 1 <= i_12_;
								if (!bool && i_7_ + 1 <= i_6_) {
									int i_16_ = class93.aShortArray1010[i_3_ + 1];
									int i_17_ = i_4_ + (i_16_ >>> 8);
									int i_18_ = i_17_ + (i_16_ & 0xff);
									bool = i_13_ > i_17_ && i_13_ < i_18_;
								}
								if (!bool_15_ && i_7_ - 1 >= i_5_) {
									int i_19_ = class93.aShortArray1010[i_3_ - 1];
									int i_20_ = i_4_ + (i_19_ >>> 8);
									int i_21_ = i_20_ + (i_19_ & 0xff);
									bool_15_ = i_13_ > i_20_ && i_13_ < i_21_;
								}
								if (bool && !bool_15_)
									i_14_ = 4;
								else if (bool_15_ && !bool)
									i_14_ = 2;
							} else {
								boolean bool = i_13_ - 1 >= i_11_;
								boolean bool_22_ = i_13_ + 1 <= i_12_;
								if (!bool && i_7_ - 1 >= i_5_) {
									int i_23_ = class93.aShortArray1010[i_3_ - 1];
									int i_24_ = i_4_ + (i_23_ >>> 8);
									int i_25_ = i_24_ + (i_23_ & 0xff);
									bool = i_13_ > i_24_ && i_13_ < i_25_;
								}
								if (!bool_22_ && i_7_ + 1 <= i_6_) {
									int i_26_ = class93.aShortArray1010[i_3_ + 1];
									int i_27_ = i_4_ + (i_26_ >>> 8);
									int i_28_ = i_27_ + (i_26_ & 0xff);
									bool_22_ = i_13_ > i_27_ && i_13_ < i_28_;
								}
								if (bool && !bool_22_)
									i_14_ = 3;
								else if (bool_22_ && !bool)
									i_14_ = 5;
							}
						}
						is[i_10_++] = i_14_;
					}
					i_3_++;
				}
				Class296_Sub17_Sub2.aBooleanArray6047[i] = true;
				Class360_Sub2.aSArray5304[i_0_].method3357(class296_sub35, is);
			}
		}
	}

	static final void method4057(Js5 class138, byte i, d var_d) {
		Class239.aClass138_2255 = class138;
		if (i <= -68)
			Class366_Sub8.aD5412 = var_d;
	}


	static {
		aLong3301 = 0L;
		anInt3302 = -1;
	}
}
