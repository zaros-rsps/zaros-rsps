package net.zaros.client;

import net.zaros.client.configs.objtype.ObjTypeList;

/* Class296_Sub39_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub1 extends Queuable {
	String aString6116;
	int anInt6117;
	static Js5 fs2;
	Queue aClass145_6119;
	static Class81 aClass81_6120 = new Class81("", 11);
	static int[] anIntArray6121 = new int[1];
	static int anInt6122 = 0;
	static ObjTypeList itemDefinitionLoader;
	static boolean aBoolean6124 = false;
	static Class296_Sub39_Sub9 aClass296_Sub39_Sub9_6125;

	final boolean method2780(Class296_Sub39_Sub9 class296_sub39_sub9, int i) {
		boolean bool = true;
		if (i != -1)
			return false;
		class296_sub39_sub9.queue_unlink();
		for (Class296_Sub39_Sub9 class296_sub39_sub9_0_ = (Class296_Sub39_Sub9) aClass145_6119.getFront(); class296_sub39_sub9_0_ != null; class296_sub39_sub9_0_ = (Class296_Sub39_Sub9) aClass145_6119.getNext()) {
			if (Class395.method4064(-1001, class296_sub39_sub9.anInt6165, class296_sub39_sub9_0_.anInt6165)) {
				Class296_Sub48.method3044(class296_sub39_sub9, class296_sub39_sub9_0_, (byte) -69);
				anInt6117++;
				if (bool)
					return false;
				return true;
			}
			bool = false;
		}
		aClass145_6119.insert(class296_sub39_sub9, -2);
		anInt6117++;
		return bool;
	}

	static final float[] method2781(float[] fs, int i, int i_1_) {
		float[] fs_2_ = new float[i_1_];
		if (i != 26587)
			itemDefinitionLoader = null;
		Class291.method2406(fs, 0, fs_2_, 0, i_1_);
		return fs_2_;
	}

	static final Class69_Sub1_Sub1 method2782(ha_Sub3 var_ha_Sub3, boolean bool, int i, int i_3_, int i_4_, int i_5_) {
		if (bool != true)
			itemDefinitionLoader = null;
		if (!var_ha_Sub3.aBoolean4219 && (!Class30.method329(2844, i_5_) || !Class30.method329(2844, i))) {
			if (!var_ha_Sub3.aBoolean4257)
				return new Class69_Sub1_Sub1(var_ha_Sub3, i_3_, i_4_, i_5_, i, Class8.get_next_high_pow2(i_5_), Class8.get_next_high_pow2(i), true);
			return new Class69_Sub1_Sub1(var_ha_Sub3, 34037, i_3_, i_4_, i_5_, i, true);
		}
		return new Class69_Sub1_Sub1(var_ha_Sub3, 3553, i_3_, i_4_, i_5_, i, true);
	}

	final boolean method2783(int i, Class296_Sub39_Sub9 class296_sub39_sub9) {
		int i_6_ = method2785((byte) 39);
		if (i != 3553)
			itemDefinitionLoader = null;
		class296_sub39_sub9.queue_unlink();
		anInt6117--;
		if (anInt6117 == 0) {
			this.unlink();
			this.queue_unlink();
			Class239.anInt2254--;
			ReferenceTable.aClass113_986.put(this, class296_sub39_sub9.aLong6161);
			return false;
		}
		if (method2785((byte) 30) == i_6_)
			return false;
		return true;
	}

	static final Class379 method2784(Packet class296_sub17, int i) {
		Class252 class252 = StaticMethods.method312((byte) -33)[class296_sub17.g1()];
		Class357 class357 = Class110.method970(i)[class296_sub17.g1()];
		int i_7_ = class296_sub17.g2b();
		int i_8_ = class296_sub17.g2b();
		int i_9_ = class296_sub17.g2();
		int i_10_ = class296_sub17.g2();
		int i_11_ = class296_sub17.g2b();
		int i_12_ = class296_sub17.g4();
		int i_13_ = class296_sub17.g4();
		return new Class379(class252, class357, i_7_, i_8_, i_9_, i_10_, i_11_, i_12_, i_13_);
	}

	final int method2785(byte i) {
		if (aClass145_6119.head.queue_next != aClass145_6119.head)
			return (((Class296_Sub39_Sub9) aClass145_6119.head.queue_next).anInt6165);
		if (i < 14)
			return -64;
		return -1;
	}

	public static void method2786(int i) {
		fs2 = null;
		if (i != 11)
			method2786(-47);
		itemDefinitionLoader = null;
		aClass296_Sub39_Sub9_6125 = null;
		aClass81_6120 = null;
		anIntArray6121 = null;
	}

	Class296_Sub39_Sub1(String string) {
		aString6116 = string;
		aClass145_6119 = new Queue();
	}
}
