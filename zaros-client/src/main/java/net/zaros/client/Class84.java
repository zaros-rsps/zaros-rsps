package net.zaros.client;

public class Class84 {
	static OutgoingPacket aClass311_921;
	Class338_Sub8 aClass338_Sub8_923 = new Class338_Sub8();
	static Class189 aClass189_924;
	static long aLong925;
	private Class338_Sub8 aClass338_Sub8_926;

	public static void method813(int i) {
		aClass189_924 = null;
		PlayerUpdate.invisiblePlayersIndexes = null;
		aClass311_921 = null;
		if (i != 5)
			method813(-39);
	}

	public static final byte[][][] method814(int i, byte i_0_) {
		byte[][][] is = new byte[8][4][];
		int i_1_ = i;
		int i_2_ = i;
		byte[] is_3_ = new byte[i_2_ * i_1_];
		int i_4_ = 0;
		for (int i_5_ = 0; i_5_ < i_2_; i_5_++) {
			for (int i_6_ = 0; i_1_ > i_6_; i_6_++) {
				if (i_6_ <= i_5_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[0][0] = is_3_;
		is_3_ = new byte[i_2_ * i_1_];
		i_4_ = 0;
		for (int i_7_ = i_2_ - 1; i_7_ >= 0; i_7_--) {
			for (int i_8_ = 0; i_8_ < i_2_; i_8_++) {
				if (i_8_ <= i_7_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[0][1] = is_3_;
		is_3_ = new byte[i_2_ * i_1_];
		i_4_ = 0;
		for (int i_9_ = 0; i_2_ > i_9_; i_9_++) {
			for (int i_10_ = 0; i_1_ > i_10_; i_10_++) {
				if (i_9_ <= i_10_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[0][2] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_2_ * i_1_];
		for (int i_11_ = i_2_ - 1; i_11_ >= 0; i_11_--) {
			for (int i_12_ = 0; i_12_ < i_1_; i_12_++) {
				if (i_12_ >= i_11_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[0][3] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_13_ = i_2_ - 1; i_13_ >= 0; i_13_--) {
			for (int i_14_ = 0; i_14_ < i_1_; i_14_++) {
				if (i_13_ >> 1 >= i_14_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[1][0] = is_3_;
		is_3_ = new byte[i_2_ * i_1_];
		i_4_ = 0;
		for (int i_15_ = 0; i_2_ > i_15_; i_15_++) {
			for (int i_16_ = 0; i_16_ < i_1_; i_16_++) {
				if (i_4_ < 0 || i_4_ >= is_3_.length)
					i_4_++;
				else {
					if (i_16_ >= i_15_ << 1)
						is_3_[i_4_] = (byte) -1;
					i_4_++;
				}
			}
		}
		is[1][1] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_2_ * i_1_];
		for (int i_17_ = 0; i_2_ > i_17_; i_17_++) {
			for (int i_18_ = i_1_ - 1; i_18_ >= 0; i_18_--) {
				if (i_18_ <= i_17_ >> 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[1][2] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_19_ = i_2_ - 1; i_19_ >= 0; i_19_--) {
			for (int i_20_ = i_1_ - 1; i_20_ >= 0; i_20_--) {
				if (i_19_ << 1 <= i_20_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[1][3] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_1_ * i_2_];
		for (int i_21_ = i_2_ - 1; i_21_ >= 0; i_21_--) {
			for (int i_22_ = i_1_ - 1; i_22_ >= 0; i_22_--) {
				if (i_22_ <= i_21_ >> 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[2][0] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_23_ = i_2_ - 1; i_23_ >= 0; i_23_--) {
			for (int i_24_ = 0; i_24_ < i_1_; i_24_++) {
				if (i_24_ >= i_23_ << 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[2][1] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_25_ = 0; i_25_ < i_2_; i_25_++) {
			for (int i_26_ = 0; i_26_ < i_1_; i_26_++) {
				if (i_26_ <= i_25_ >> 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[2][2] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_2_ * i_1_];
		for (int i_27_ = 0; i_2_ > i_27_; i_27_++) {
			for (int i_28_ = i_1_ - 1; i_28_ >= 0; i_28_--) {
				if (i_28_ >= i_27_ << 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[2][3] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_1_ * i_2_];
		for (int i_29_ = i_2_ - 1; i_29_ >= 0; i_29_--) {
			for (int i_30_ = 0; i_30_ < i_1_; i_30_++) {
				if (i_29_ >> 1 <= i_30_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[3][0] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_31_ = 0; i_2_ > i_31_; i_31_++) {
			for (int i_32_ = 0; i_32_ < i_1_; i_32_++) {
				if (i_32_ <= i_31_ << 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[3][1] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_33_ = 0; i_2_ > i_33_; i_33_++) {
			for (int i_34_ = i_1_ - 1; i_34_ >= 0; i_34_--) {
				if (i_34_ >= i_33_ >> 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[3][2] = is_3_;
		is_3_ = new byte[i_2_ * i_1_];
		i_4_ = 0;
		for (int i_35_ = i_2_ - 1; i_35_ >= 0; i_35_--) {
			for (int i_36_ = i_1_ - 1; i_36_ >= 0; i_36_--) {
				if (i_36_ <= i_35_ << 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[3][3] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_2_ * i_1_];
		int i_37_ = i_2_ - 1;
		if (i_0_ != -54)
			method814(109, (byte) -56);
		for (/**/; i_37_ >= 0; i_37_--) {
			for (int i_38_ = i_1_ - 1; i_38_ >= 0; i_38_--) {
				if (i_38_ >= i_37_ >> 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[4][0] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_39_ = i_2_ - 1; i_39_ >= 0; i_39_--) {
			for (int i_40_ = 0; i_1_ > i_40_; i_40_++) {
				if (i_40_ <= i_39_ << 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[4][1] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_41_ = 0; i_2_ > i_41_; i_41_++) {
			for (int i_42_ = 0; i_1_ > i_42_; i_42_++) {
				if (i_42_ >= i_41_ >> 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[4][2] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_2_ * i_1_];
		for (int i_43_ = 0; i_2_ > i_43_; i_43_++) {
			for (int i_44_ = i_1_ - 1; i_44_ >= 0; i_44_--) {
				if (i_44_ <= i_43_ << 1)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[4][3] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_2_ * i_1_];
		for (int i_45_ = 0; i_45_ < i_2_; i_45_++) {
			for (int i_46_ = 0; i_46_ < i_1_; i_46_++) {
				if (i_46_ <= i_1_ / 2)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[5][0] = is_3_;
		is_3_ = new byte[i_2_ * i_1_];
		i_4_ = 0;
		for (int i_47_ = 0; i_47_ < i_2_; i_47_++) {
			for (int i_48_ = 0; i_48_ < i_1_; i_48_++) {
				if (i_2_ / 2 >= i_47_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[5][1] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_49_ = 0; i_49_ < i_2_; i_49_++) {
			for (int i_50_ = 0; i_1_ > i_50_; i_50_++) {
				if (i_1_ / 2 <= i_50_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[5][2] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_2_ * i_1_];
		for (int i_51_ = 0; i_2_ > i_51_; i_51_++) {
			for (int i_52_ = 0; i_52_ < i_1_; i_52_++) {
				if (i_51_ >= i_2_ / 2)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[5][3] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_1_ * i_2_];
		for (int i_53_ = 0; i_2_ > i_53_; i_53_++) {
			for (int i_54_ = 0; i_54_ < i_1_; i_54_++) {
				if (i_54_ <= i_53_ - i_2_ / 2)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[6][0] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_55_ = i_2_ - 1; i_55_ >= 0; i_55_--) {
			for (int i_56_ = 0; i_1_ > i_56_; i_56_++) {
				if (-(i_2_ / 2) + i_55_ >= i_56_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[6][1] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_1_ * i_2_];
		for (int i_57_ = i_2_ - 1; i_57_ >= 0; i_57_--) {
			for (int i_58_ = i_1_ - 1; i_58_ >= 0; i_58_--) {
				if (-(i_2_ / 2) + i_57_ >= i_58_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[6][2] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_2_ * i_1_];
		for (int i_59_ = 0; i_59_ < i_2_; i_59_++) {
			for (int i_60_ = i_1_ - 1; i_60_ >= 0; i_60_--) {
				if (i_60_ <= i_59_ - i_2_ / 2)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[6][3] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_2_ * i_1_];
		for (int i_61_ = 0; i_61_ < i_2_; i_61_++) {
			for (int i_62_ = 0; i_62_ < i_1_; i_62_++) {
				if (i_62_ >= i_61_ - i_2_ / 2)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[7][0] = is_3_;
		i_4_ = 0;
		is_3_ = new byte[i_2_ * i_1_];
		for (int i_63_ = i_2_ - 1; i_63_ >= 0; i_63_--) {
			for (int i_64_ = 0; i_1_ > i_64_; i_64_++) {
				if (i_63_ - i_2_ / 2 <= i_64_)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[7][1] = is_3_;
		is_3_ = new byte[i_2_ * i_1_];
		i_4_ = 0;
		for (int i_65_ = i_2_ - 1; i_65_ >= 0; i_65_--) {
			for (int i_66_ = i_1_ - 1; i_66_ >= 0; i_66_--) {
				if (i_66_ >= i_65_ - i_2_ / 2)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[7][2] = is_3_;
		is_3_ = new byte[i_1_ * i_2_];
		i_4_ = 0;
		for (int i_67_ = 0; i_2_ > i_67_; i_67_++) {
			for (int i_68_ = i_1_ - 1; i_68_ >= 0; i_68_--) {
				if (i_68_ >= i_67_ - i_2_ / 2)
					is_3_[i_4_] = (byte) -1;
				i_4_++;
			}
		}
		is[7][3] = is_3_;
		return is;
	}

	public void method815(int i, Class338_Sub8 class338_sub8) {
		if (class338_sub8.aClass338_Sub8_5254 != null)
			class338_sub8.method3600((byte) 115);
		class338_sub8.aClass338_Sub8_5254 = aClass338_Sub8_923.aClass338_Sub8_5254;
		if (i == 24150) {
			class338_sub8.aClass338_Sub8_5253 = aClass338_Sub8_923;
			class338_sub8.aClass338_Sub8_5254.aClass338_Sub8_5253 = class338_sub8;
			class338_sub8.aClass338_Sub8_5253.aClass338_Sub8_5254 = class338_sub8;
		}
	}

	public Class338_Sub8 method816(int i) {
		Class338_Sub8 class338_sub8 = aClass338_Sub8_926;
		if (aClass338_Sub8_923 == class338_sub8) {
			aClass338_Sub8_926 = null;
			return null;
		}
		aClass338_Sub8_926 = class338_sub8.aClass338_Sub8_5253;
		if (i > -118)
			return null;
		return class338_sub8;
	}

	public void method817(int i) {
		for (;;) {
			Class338_Sub8 class338_sub8 = aClass338_Sub8_923.aClass338_Sub8_5253;
			if (class338_sub8 == aClass338_Sub8_923)
				break;
			class338_sub8.method3600((byte) 112);
		}
		aClass338_Sub8_926 = null;
		if (i < 68)
			method818(59);
	}

	public Class338_Sub8 method818(int i) {
		int i_69_ = -103 / ((i + 75) / 33);
		Class338_Sub8 class338_sub8 = aClass338_Sub8_923.aClass338_Sub8_5253;
		if (aClass338_Sub8_923 == class338_sub8) {
			aClass338_Sub8_926 = null;
			return null;
		}
		aClass338_Sub8_926 = class338_sub8.aClass338_Sub8_5253;
		return class338_sub8;
	}

	public static void method819(byte i, String string, boolean bool, int i_70_) {
		Class25.method304((byte) 125);
		za_Sub1.method3224(0);
		Canvas_Sub1.method124((byte) 25);
		Class296_Sub30.method2700(bool, i_70_, string, (byte) 79);
		Class245.method2184(36);
		Class296_Sub39_Sub15_Sub1.method2886(8, Class41_Sub13.aHa3774);
		Class16_Sub3_Sub1.method251(Class41_Sub13.aHa3774, -4096);
		Class296_Sub51_Sub13.method3112(Class205_Sub2.fs8, (byte) -126, Class41_Sub13.aHa3774);
		if (i >= -14)
			method814(-29, (byte) -71);
		Class296_Sub51_Sub4.method3087(21097);
		ha.method1087(1, Class85.aClass397Array929);
		Class366_Sub8.method3794(true);
		Class368_Sub4.method3817(-127);
		if (Class366_Sub6.anInt5392 == 3)
			Class41_Sub8.method422(1, 4);
		else if (Class366_Sub6.anInt5392 != 7) {
			if (Class366_Sub6.anInt5392 != 9) {
				if (Class366_Sub6.anInt5392 != 11) {
					if (Class366_Sub6.anInt5392 == 1 || Class366_Sub6.anInt5392 == 2)
						Class53.method641(-91);
				} else
					Class41_Sub8.method422(1, 12);
			} else
				Class41_Sub8.method422(1, 10);
		} else
			Class41_Sub8.method422(1, 8);
	}

	public Class84() {
		aClass338_Sub8_923.aClass338_Sub8_5254 = aClass338_Sub8_923;
		aClass338_Sub8_923.aClass338_Sub8_5253 = aClass338_Sub8_923;
	}

	public int method820(int i) {
		int i_71_ = i;
		for (Class338_Sub8 class338_sub8 = aClass338_Sub8_923.aClass338_Sub8_5253; class338_sub8 != aClass338_Sub8_923; class338_sub8 = class338_sub8.aClass338_Sub8_5253)
			i_71_++;
		return i_71_;
	}

	static {
		aClass311_921 = new OutgoingPacket(74, -1);
	}
}
