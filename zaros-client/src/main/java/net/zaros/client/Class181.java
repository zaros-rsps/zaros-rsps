package net.zaros.client;

import net.zaros.client.configs.objtype.ObjTypeList;

/* Class181 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

class Class181 {
	private int[] anIntArray1858 = {0, 0, 0, 256, 512, 512, 512, 256, 256, 384, 128, 128, 256};
	int anInt1859;
	boolean aBoolean1860;
	static int anInt1861;
	private Class117 aClass117_1862;
	private byte[][][] aByteArrayArrayArray1863;
	byte[][][] aByteArrayArrayArray1864;
	private byte[][][] aByteArrayArrayArray1865;
	int[][][] anIntArrayArrayArray1866;
	private byte[][][] aByteArrayArrayArray1867;
	int anInt1868;
	private byte[][][] aByteArrayArrayArray1869;
	int anInt1870;
	byte[][][] aByteArrayArrayArray1871;
	static int[][][] buildedMapParts;
	private int[] anIntArray1873 = {0, 256, 512, 512, 512, 256, 0, 0, 128, 256, 128, 384, 256};
	private Class15 aClass15_1874;

	private final void method1816(int[][] is, s var_s, s var_s_0_, boolean bool, ha var_ha, s var_s_1_, int i) {
		byte[][] is_2_ = aByteArrayArrayArray1869[i];
		if (bool != true) {
			aClass15_1874 = null;
		}
		byte[][] is_3_ = aByteArrayArrayArray1863[i];
		byte[][] is_4_ = aByteArrayArrayArray1865[i];
		byte[][] is_5_ = aByteArrayArrayArray1867[i];
		for (int i_6_ = 0; anInt1859 > i_6_; i_6_++) {
			int i_7_ = i_6_ < anInt1859 - 1 ? i_6_ + 1 : i_6_;
			for (int i_8_ = 0; anInt1868 > i_8_; i_8_++) {
				int i_9_ = i_8_ < anInt1868 - 1 ? i_8_ + 1 : i_8_;
				if (Class41_Sub5.anInt3751 == -1 || Class296_Sub40.method2912(23266, i_8_, Class41_Sub5.anInt3751, i_6_, i)) {
					boolean bool_10_ = false;
					boolean bool_11_ = false;
					boolean[] bools = new boolean[4];
					byte i_12_ = is_2_[i_6_][i_8_];
					int i_13_ = is_3_[i_6_][i_8_];
					int i_14_ = is_5_[i_6_][i_8_] & 0xff;
					int i_15_ = is_4_[i_6_][i_8_] & 0xff;
					int i_16_ = is_4_[i_6_][i_9_] & 0xff;
					int i_17_ = is_4_[i_7_][i_9_] & 0xff;
					int i_18_ = is_4_[i_7_][i_8_] & 0xff;
					if (i_14_ != 0 || i_15_ != 0) {
						Class61 class61 = i_14_ == 0 ? null : aClass15_1874.method232(true, i_14_ - 1);
						if (i_12_ == 0 && class61 == null) {
							i_12_ = (byte) 12;
						}
						Class236 class236 = i_15_ != 0 ? aClass117_1862.method1020(bool, i_15_ - 1) : null;
						Class61 class61_19_ = class61;
						if (class61 != null) {
							if (class61.anInt702 == -1 && class61.anInt707 == -1) {
								class61_19_ = class61;
								class61 = null;
							} else if (class236 != null && i_12_ != 0) {
								bool_11_ = class61.aBoolean705;
							}
						}
						if ((i_12_ == 0 || i_12_ == 12) && i_6_ > 0 && i_8_ > 0 && anInt1859 > i_6_ && i_8_ < anInt1868) {
							int i_20_ = 0;
							int i_21_ = 0;
							int i_22_ = 0;
							i_21_ = i_21_ + (is_4_[i_7_][i_8_ - 1] == i_15_ ? 1 : -1);
							i_22_ = i_22_ + (i_15_ == is_4_[i_7_][i_9_] ? 1 : -1);
							i_20_ = i_20_ - (i_15_ != is_4_[i_6_ - 1][i_8_ - 1] ? 1 : 1);
							int i_23_ = 0;
							if (is_4_[i_6_][i_8_ - 1] != i_15_) {
								i_20_--;
								i_21_--;
							} else {
								i_20_++;
								i_21_++;
							}
							i_23_ = i_23_ - (is_4_[i_6_ - 1][i_9_] != i_15_ ? 1 : 1);
							if (is_4_[i_7_][i_8_] != i_15_) {
								i_21_--;
								i_22_--;
							} else {
								i_21_++;
								i_22_++;
							}
							if (is_4_[i_6_][i_9_] == i_15_) {
								i_22_++;
								i_23_++;
							} else {
								i_23_--;
								i_22_--;
							}
							if (i_15_ == is_4_[i_6_ - 1][i_8_]) {
								i_20_++;
								i_23_++;
							} else {
								i_20_--;
								i_23_--;
							}
							int i_24_ = i_20_ - i_22_;
							if (i_24_ < 0) {
								i_24_ = -i_24_;
							}
							int i_25_ = i_21_ - i_23_;
							if (i_25_ < 0) {
								i_25_ = -i_25_;
							}
							if (i_25_ == i_24_) {
								i_24_ = var_s_1_.method3355(i_8_, (byte) -128, i_6_) - var_s_1_.method3355(i_9_, (byte) -114, i_7_);
								i_25_ = var_s_1_.method3355(i_8_, (byte) -121, i_7_) - var_s_1_.method3355(i_9_, (byte) -116, i_6_);
								if (i_24_ < 0) {
									i_24_ = -i_24_;
								}
								if (i_25_ < 0) {
									i_25_ = -i_25_;
								}
							}
							i_13_ = i_24_ >= i_25_ ? 0 : 1;
						}
						for (int i_26_ = 0; i_26_ < 13; i_26_++) {
							ObjTypeList.anIntArray1321[i_26_] = -1;
							Class347.anIntArray3027[i_26_] = 1;
						}
						boolean[] bools_27_ = class61 == null || !class61.aBoolean705 ? Class41_Sub16.aBooleanArrayArray3780[i_12_] : Class300.aBooleanArrayArray3697[i_12_];
						method1829(var_ha, i_13_, is_2_, class236, anInt1859, i_12_, 3, bools, class61, i_6_, is_3_, is_5_, anInt1868, i_8_);
						boolean bool_28_ = class61 != null && class61.anInt702 != class61.anInt707;
						if (!bool_28_) {
							for (int i_29_ = 0; i_29_ < 8; i_29_++) {
								if (ObjTypeList.anIntArray1321[i_29_] >= 0 && StaticMethods.anIntArray1624[i_29_] != Class296_Sub51_Sub21.anIntArray6450[i_29_]) {
									bool_28_ = true;
									break;
								}
							}
						}
						if (!bools_27_[i_13_ + 1 & 0x3]) {
							bools[1] = Class135.method1409(bools[1], (Class347.anIntArray3027[4] & Class347.anIntArray3027[2]) == 0);
						}
						if (!bools_27_[3 + i_13_ & 0x3]) {
							bools[3] = Class135.method1409(bools[3], (Class347.anIntArray3027[6] & Class347.anIntArray3027[0]) == 0);
						}
						if (!bools_27_[i_13_ & 0x3]) {
							bools[0] = Class135.method1409(bools[0], (Class347.anIntArray3027[0] & Class347.anIntArray3027[2]) == 0);
						}
						if (!bools_27_[i_13_ + 2 & 0x3]) {
							bools[2] = Class135.method1409(bools[2], (Class347.anIntArray3027[6] & Class347.anIntArray3027[4]) == 0);
						}
						if (!bool_11_ && (i_12_ == 0 || i_12_ == 12)) {
							if (!bools[0] || bools[1] || bools[2] || !bools[3]) {
								if (!bools[0] || !bools[1] || bools[2] || bools[3]) {
									if (bools[0] || !bools[1] || !bools[2] || bools[3]) {
										if (!bools[0] && !bools[1] && bools[2] && bools[3]) {
											i_12_ = i_12_ == 0 ? (byte) 13 : (byte) 14;
											i_13_ = 1;
											bools[2] = bools[3] = false;
										}
									} else {
										i_13_ = 2;
										bools[1] = bools[2] = false;
										i_12_ = i_12_ == 0 ? (byte) 13 : (byte) 14;
									}
								} else {
									bools[0] = bools[1] = false;
									i_12_ = i_12_ != 0 ? (byte) 14 : (byte) 13;
									i_13_ = 3;
								}
							} else {
								i_13_ = 0;
								i_12_ = i_12_ == 0 ? (byte) 13 : (byte) 14;
								bools[0] = bools[3] = false;
							}
						}
						boolean bool_30_ = !bool_11_ && !bools[0] && !bools[2] && !bools[1] && !bools[3];
						int[] is_31_ = null;
						int i_32_;
						int[] is_33_;
						int i_34_;
						int[] is_35_;
						int[] is_36_;
						if (!bool_30_) {
							if (bool_11_) {
								i_32_ = class61 != null ? Class338_Sub2.anIntArray5197[i_12_] : 0;
								is_36_ = Class262.anIntArrayArray2452[i_12_];
								is_31_ = ReferenceWrapper.anIntArrayArray6129[i_12_];
								is_35_ = Class373_Sub2.anIntArrayArray5600[i_12_];
								is_33_ = Class296_Sub24.anIntArrayArray4752[i_12_];
								i_34_ = class236 != null ? Class327.anIntArray2905[i_12_] : 0;
							} else {
								is_35_ = Class340.anIntArrayArray3706[i_12_];
								is_36_ = Class123_Sub2.anIntArrayArray3919[i_12_];
								is_31_ = ha.anIntArrayArray1296[i_12_];
								i_32_ = class61 != null ? Class304.anIntArray2736[i_12_] : 0;
								i_34_ = class236 != null ? Class126.anIntArray1290[i_12_] : 0;
								is_33_ = Class296_Sub39_Sub20_Sub2.anIntArrayArray6729[i_12_];
							}
						} else {
							i_32_ = class61 == null ? 0 : Class302.anIntArray2722[i_12_];
							is_33_ = Class296_Sub40.anIntArrayArray4910[i_12_];
							i_34_ = class236 != null ? StaticMethods.anIntArray5953[i_12_] : 0;
							is_35_ = Class296_Sub39_Sub4.anIntArrayArray5735[i_12_];
							is_36_ = Class50.anIntArrayArray471[i_12_];
						}
						int i_37_ = i_34_ + i_32_;
						if (i_37_ <= 0) {
							Class320.method3340(i, i_6_, i_8_);
						} else {
							if (bools[0]) {
								i_37_++;
							}
							if (bools[2]) {
								i_37_++;
							}
							if (bools[1]) {
								i_37_++;
							}
							if (bools[3]) {
								i_37_++;
							}
							int i_38_ = 0;
							int i_39_ = 0;
							int i_40_ = i_37_ * 3;
							int[] is_41_ = !bool_28_ ? null : new int[i_40_];
							int[] is_42_ = new int[i_40_];
							int[] is_43_ = new int[i_40_];
							int[] is_44_ = new int[i_40_];
							int[] is_45_ = new int[i_40_];
							int[] is_46_ = new int[i_40_];
							int[] is_47_ = var_s_0_ != null ? new int[i_40_] : null;
							int[] is_48_ = var_s_0_ == null && var_s == null ? null : new int[i_40_];
							int i_49_ = -1;
							int i_50_ = -1;
							int i_51_ = 256;
							if (class61 == null) {
								if (bool_30_) {
									i_38_ += Class302.anIntArray2722[i_12_];
								} else if (bool_11_) {
									i_38_ += Class338_Sub2.anIntArray5197[i_12_];
								} else {
									i_38_ += Class304.anIntArray2736[i_12_];
								}
							} else {
								i_51_ = class61.anInt706;
								i_49_ = class61.anInt702;
								i_50_ = class61.anInt709;
								int i_52_ = Class368_Sub3.method3815(!bool, var_ha, class61);
								for (int i_53_ = 0; i_32_ > i_53_; i_53_++) {
									boolean bool_54_ = false;
									int i_55_;
									if (!bools[-i_13_ & 0x3] || is_31_[0] != i_38_) {
										if (!bools[2 - i_13_ & 0x3] || is_31_[2] != i_38_) {
											if (!bools[-i_13_ + 1 & 0x3] || i_38_ != is_31_[1]) {
												if (!bools[-i_13_ + 3 & 0x3] || i_38_ != is_31_[3]) {
													Class94.anIntArray1012[0] = is_33_[i_38_];
													Class94.anIntArray1012[1] = is_36_[i_38_];
													i_55_ = 3;
													Class94.anIntArray1012[2] = is_35_[i_38_];
												} else {
													Class94.anIntArray1012[0] = is_33_[i_38_];
													Class94.anIntArray1012[1] = 7;
													Class94.anIntArray1012[2] = is_35_[i_38_];
													Class94.anIntArray1012[3] = 7;
													Class94.anIntArray1012[4] = is_36_[i_38_];
													i_55_ = 6;
													Class94.anIntArray1012[5] = is_35_[i_38_];
												}
											} else {
												Class94.anIntArray1012[0] = is_33_[i_38_];
												Class94.anIntArray1012[1] = 3;
												Class94.anIntArray1012[2] = is_35_[i_38_];
												Class94.anIntArray1012[3] = 3;
												Class94.anIntArray1012[4] = is_36_[i_38_];
												i_55_ = 6;
												Class94.anIntArray1012[5] = is_35_[i_38_];
											}
										} else {
											Class94.anIntArray1012[0] = is_33_[i_38_];
											Class94.anIntArray1012[1] = 5;
											Class94.anIntArray1012[2] = is_35_[i_38_];
											Class94.anIntArray1012[3] = 5;
											Class94.anIntArray1012[4] = is_36_[i_38_];
											i_55_ = 6;
											Class94.anIntArray1012[5] = is_35_[i_38_];
										}
									} else {
										Class94.anIntArray1012[0] = is_33_[i_38_];
										Class94.anIntArray1012[1] = 1;
										Class94.anIntArray1012[2] = is_35_[i_38_];
										Class94.anIntArray1012[3] = 1;
										Class94.anIntArray1012[4] = is_36_[i_38_];
										Class94.anIntArray1012[5] = is_35_[i_38_];
										i_55_ = 6;
									}
									for (int i_56_ = 0; i_55_ > i_56_; i_56_++) {
										int i_57_ = Class94.anIntArray1012[i_56_];
										int i_58_ = -(i_13_ * 2) + i_57_ & 0x7;
										int i_59_ = anIntArray1873[i_57_];
										int i_60_ = anIntArray1858[i_57_];
										int i_61_;
										int i_62_;
										if (i_13_ == 1) {
											i_62_ = -i_59_ + 512;
											i_61_ = i_60_;
										} else if (i_13_ != 2) {
											if (i_13_ == 3) {
												i_61_ = -i_60_ + 512;
												i_62_ = i_59_;
											} else {
												i_61_ = i_59_;
												i_62_ = i_60_;
											}
										} else {
											i_62_ = 512 - i_60_;
											i_61_ = -i_59_ + 512;
										}
										is_42_[i_39_] = i_61_;
										is_43_[i_39_] = i_62_;
										if (is_47_ != null && Class52.aBooleanArrayArray635[i_12_][i_57_]) {
											int i_63_ = (i_6_ << 9) + i_61_;
											int i_64_ = (i_8_ << 9) + i_62_;
											is_47_[i_39_] = var_s_0_.method3349(0, i_64_, i_63_) - var_s_1_.method3349(0, i_64_, i_63_);
										}
										if (is_48_ != null) {
											if (var_s_0_ != null && !Class52.aBooleanArrayArray635[i_12_][i_57_]) {
												int i_65_ = i_61_ + (i_6_ << 9);
												int i_66_ = i_62_ + (i_8_ << 9);
												is_48_[i_39_] = var_s_1_.method3349(0, i_66_, i_65_) - var_s_0_.method3349(0, i_66_, i_65_);
											} else if (var_s != null && !Class110.aBooleanArrayArray1142[i_12_][i_57_]) {
												int i_67_ = (i_6_ << 9) + i_61_;
												int i_68_ = (i_8_ << 9) + i_62_;
												is_48_[i_39_] = var_s.method3349(0, i_68_, i_67_) - var_s_1_.method3349(0, i_68_, i_67_);
											}
										}
										if (i_57_ >= 8 || ObjTypeList.anIntArray1321[i_58_] <= class61.anInt708) {
											if (is_41_ != null) {
												is_41_[i_39_] = i_52_;
											}
											is_45_[i_39_] = class61.anInt709;
											is_46_[i_39_] = class61.anInt706;
											is_44_[i_39_] = i_49_;
										} else {
											if (is_41_ != null) {
												is_41_[i_39_] = Class296_Sub51_Sub21.anIntArray6450[i_58_];
											}
											is_46_[i_39_] = Class296_Sub39_Sub11.anIntArray6188[i_58_];
											is_45_[i_39_] = Class392.anIntArray3487[i_58_];
											is_44_[i_39_] = StaticMethods.anIntArray1624[i_58_];
										}
										i_39_++;
									}
									i_38_++;
								}
								if (!aBoolean1860 && i == 0) {
									Class79.method805(i_6_, i_8_, class61.anInt699, class61.anInt701 * 8, class61.anInt700);
								}
								if (i_12_ != 12 && class61.anInt702 != -1 && class61.aBoolean697) {
									bool_10_ = true;
								}
							}
							if (class236 != null) {
								if (i_16_ == 0) {
									i_16_ = i_15_;
								}
								if (i_17_ == 0) {
									i_17_ = i_15_;
								}
								if (i_18_ == 0) {
									i_18_ = i_15_;
								}
								Class236 class236_69_ = aClass117_1862.method1020(true, i_15_ - 1);
								Class236 class236_70_ = aClass117_1862.method1020(true, i_16_ - 1);
								Class236 class236_71_ = aClass117_1862.method1020(true, i_17_ - 1);
								Class236 class236_72_ = aClass117_1862.method1020(true, i_18_ - 1);
								for (int i_73_ = 0; i_34_ > i_73_; i_73_++) {
									boolean bool_74_ = false;
									int i_75_;
									if (!bools[-i_13_ & 0x3] || is_31_[0] != i_38_) {
										if (bools[-i_13_ + 2 & 0x3] && is_31_[2] == i_38_) {
											Class94.anIntArray1012[0] = is_33_[i_38_];
											Class94.anIntArray1012[1] = 5;
											Class94.anIntArray1012[2] = is_35_[i_38_];
											Class94.anIntArray1012[3] = 5;
											Class94.anIntArray1012[4] = is_36_[i_38_];
											i_75_ = 6;
											Class94.anIntArray1012[5] = is_35_[i_38_];
										} else if (bools[-i_13_ + 1 & 0x3] && i_38_ == is_31_[1]) {
											Class94.anIntArray1012[0] = is_33_[i_38_];
											Class94.anIntArray1012[1] = 3;
											Class94.anIntArray1012[2] = is_35_[i_38_];
											Class94.anIntArray1012[3] = 3;
											Class94.anIntArray1012[4] = is_36_[i_38_];
											i_75_ = 6;
											Class94.anIntArray1012[5] = is_35_[i_38_];
										} else if (bools[-i_13_ + 3 & 0x3] && i_38_ == is_31_[3]) {
											Class94.anIntArray1012[0] = is_33_[i_38_];
											Class94.anIntArray1012[1] = 7;
											Class94.anIntArray1012[2] = is_35_[i_38_];
											Class94.anIntArray1012[3] = 7;
											Class94.anIntArray1012[4] = is_36_[i_38_];
											Class94.anIntArray1012[5] = is_35_[i_38_];
											i_75_ = 6;
										} else {
											Class94.anIntArray1012[0] = is_33_[i_38_];
											Class94.anIntArray1012[1] = is_36_[i_38_];
											i_75_ = 3;
											Class94.anIntArray1012[2] = is_35_[i_38_];
										}
									} else {
										Class94.anIntArray1012[0] = is_33_[i_38_];
										Class94.anIntArray1012[1] = 1;
										Class94.anIntArray1012[2] = is_35_[i_38_];
										Class94.anIntArray1012[3] = 1;
										Class94.anIntArray1012[4] = is_36_[i_38_];
										Class94.anIntArray1012[5] = is_35_[i_38_];
										i_75_ = 6;
									}
									for (int i_76_ = 0; i_75_ > i_76_; i_76_++) {
										int i_77_ = Class94.anIntArray1012[i_76_];
										int i_78_ = i_77_ - i_13_ * 2 & 0x7;
										int i_79_ = anIntArray1873[i_77_];
										int i_80_ = anIntArray1858[i_77_];
										int i_81_;
										int i_82_;
										if (i_13_ != 1) {
											if (i_13_ != 2) {
												if (i_13_ == 3) {
													i_81_ = -i_80_ + 512;
													i_82_ = i_79_;
												} else {
													i_81_ = i_79_;
													i_82_ = i_80_;
												}
											} else {
												i_82_ = 512 - i_80_;
												i_81_ = -i_79_ + 512;
											}
										} else {
											i_81_ = i_80_;
											i_82_ = -i_79_ + 512;
										}
										is_42_[i_39_] = i_81_;
										is_43_[i_39_] = i_82_;
										if (is_47_ != null && Class52.aBooleanArrayArray635[i_12_][i_77_]) {
											int i_83_ = i_81_ + (i_6_ << 9);
											int i_84_ = i_82_ + (i_8_ << 9);
											is_47_[i_39_] = var_s_0_.method3349(0, i_84_, i_83_) - var_s_1_.method3349(0, i_84_, i_83_);
										}
										if (is_48_ != null) {
											if (var_s_0_ == null || Class52.aBooleanArrayArray635[i_12_][i_77_]) {
												if (var_s != null && !Class110.aBooleanArrayArray1142[i_12_][i_77_]) {
													int i_85_ = i_81_ + (i_6_ << 9);
													int i_86_ = (i_8_ << 9) + i_82_;
													is_48_[i_39_] = var_s.method3349(0, i_86_, i_85_) - var_s_1_.method3349(0, i_86_, i_85_);
												}
											} else {
												int i_87_ = i_81_ + (i_6_ << 9);
												int i_88_ = i_82_ + (i_8_ << 9);
												is_48_[i_39_] = var_s_1_.method3349(0, i_88_, i_87_) - var_s_0_.method3349(0, i_88_, i_87_);
											}
										}
										if (i_77_ >= 8 || ObjTypeList.anIntArray1321[i_78_] < 0) {
											if (bool_11_ && Class52.aBooleanArrayArray635[i_12_][i_77_]) {
												is_45_[i_39_] = i_50_;
												is_46_[i_39_] = i_51_;
												is_44_[i_39_] = i_49_;
											} else if (i_81_ == 0 && i_82_ == 0) {
												is_44_[i_39_] = is[i_6_][i_8_];
												is_45_[i_39_] = class236_69_.anInt2230;
												is_46_[i_39_] = class236_69_.anInt2241;
											} else if (i_81_ == 0 && i_82_ == 512) {
												is_44_[i_39_] = is[i_6_][i_9_];
												is_45_[i_39_] = class236_70_.anInt2230;
												is_46_[i_39_] = class236_70_.anInt2241;
											} else if (i_81_ != 512 || i_82_ != 512) {
												if (i_81_ == 512 && i_82_ == 0) {
													is_44_[i_39_] = is[i_7_][i_8_];
													is_45_[i_39_] = class236_72_.anInt2230;
													is_46_[i_39_] = class236_72_.anInt2241;
												} else {
													if (i_81_ < 256) {
														if (i_82_ >= 256) {
															is_45_[i_39_] = class236_70_.anInt2230;
															is_46_[i_39_] = class236_70_.anInt2241;
														} else {
															is_45_[i_39_] = class236_69_.anInt2230;
															is_46_[i_39_] = class236_69_.anInt2241;
														}
													} else if (i_82_ >= 256) {
														is_45_[i_39_] = class236_71_.anInt2230;
														is_46_[i_39_] = class236_71_.anInt2241;
													} else {
														is_45_[i_39_] = class236_72_.anInt2230;
														is_46_[i_39_] = class236_72_.anInt2241;
													}
													int i_89_ = Class344.method3653(!bool, i_81_ << 7 >> 9, is[i_6_][i_8_], is[i_7_][i_8_]);
													int i_90_ = Class344.method3653(false, i_81_ << 7 >> 9, is[i_6_][i_9_], is[i_7_][i_9_]);
													is_44_[i_39_] = Class344.method3653(false, i_82_ << 7 >> 9, i_89_, i_90_);
												}
											} else {
												is_44_[i_39_] = is[i_7_][i_9_];
												is_45_[i_39_] = class236_71_.anInt2230;
												is_46_[i_39_] = class236_71_.anInt2241;
											}
											if (is_41_ != null) {
												is_41_[i_39_] = is_44_[i_39_];
											}
										} else {
											if (is_41_ != null) {
												is_41_[i_39_] = Class296_Sub51_Sub21.anIntArray6450[i_78_];
											}
											is_46_[i_39_] = Class296_Sub39_Sub11.anIntArray6188[i_78_];
											is_45_[i_39_] = Class392.anIntArray3487[i_78_];
											is_44_[i_39_] = StaticMethods.anIntArray1624[i_78_];
										}
										i_39_++;
									}
									i_38_++;
								}
								if (i_12_ != 0 && class236.aBoolean2233) {
									bool_10_ = true;
								}
							}
							int i_91_ = var_s_1_.method3355(i_8_, (byte) -113, i_6_);
							int i_92_ = var_s_1_.method3355(i_8_, (byte) -108, i_7_);
							int i_93_ = var_s_1_.method3355(i_9_, (byte) -115, i_7_);
							int i_94_ = var_s_1_.method3355(i_9_, (byte) -115, i_6_);
							boolean bool_95_ = r_Sub2.method2871(i_8_, i_6_, (byte) -47);
							if (bool_95_ && i > 1 || !bool_95_ && i > 0) {
								boolean bool_96_ = true;
								if (class236 == null || class236.aBoolean2239) {
									if (i_15_ == 0 && i_12_ != 0) {
										bool_96_ = false;
									} else if (i_14_ > 0 && class61_19_ != null && !class61_19_.aBoolean703) {
										bool_96_ = false;
									}
								} else {
									bool_96_ = false;
								}
								if (bool_96_ && i_92_ == i_91_ && i_91_ == i_93_ && i_91_ == i_94_) {
									aByteArrayArrayArray1871[i][i_6_][i_8_] = (byte) Class48.bitOR(aByteArrayArrayArray1871[i][i_6_][i_8_], 4);
								}
							}
							int i_97_ = 0;
							int i_98_ = 0;
							int i_99_ = 0;
							if (aBoolean1860) {
								i_97_ = Class162.method1624(i_6_, i_8_);
								i_98_ = Class249.method2195(i_6_, i_8_);
								i_99_ = Class18.method2825(i_6_, i_8_);
							}
							var_s_1_.U(i_6_, i_8_, is_42_, is_47_, is_43_, is_48_, is_44_, is_41_, is_45_, is_46_, i_97_, i_98_, i_99_, bool_10_);
							Class320.method3340(i, i_6_, i_8_);
						}
					}
				}
			}
		}
	}

	static final void method1817(boolean bool) {
		Class384.aString3252 = "";
		if (!bool) {
			Class296_Sub51_Sub14.anInt6423 = -1;
			Class238.aLong3633 = 0L;
		}
	}

	final void method1818(int i, ClipData[] class17s, Packet class296_sub17, int i_100_, byte i_101_, int i_102_, int i_103_) {
		if (!aBoolean1860) {
			for (int i_104_ = 0; i_104_ < 4; i_104_++) {
				ClipData class17 = class17s[i_104_];
				for (int i_105_ = 0; i_105_ < 64; i_105_++) {
					for (int i_106_ = 0; i_106_ < 64; i_106_++) {
						int i_107_ = i_102_ + i_105_;
						int i_108_ = i_106_ + i;
						if (i_107_ >= 0 && anInt1859 > i_107_ && i_108_ >= 0 && i_108_ < anInt1868) {
							class17.unflagUnmovable(i_107_, i_108_);
						}
					}
				}
			}
		}
		int i_109_ = i_100_ + i_102_;
		int i_110_ = i + i_103_;
		for (int i_111_ = 0; anInt1870 > i_111_; i_111_++) {
			for (int i_112_ = 0; i_112_ < 64; i_112_++) {
				for (int i_113_ = 0; i_113_ < 64; i_113_++) {
					method1826(i_110_ + i_113_, i_111_, i_112_ + i_109_, (byte) 124, class296_sub17, false, 0, 0, i + i_113_, 0, i_112_ + i_102_);
				}
			}
		}
		if (i_101_ < 20) {
			method1827(-26, null, -22, null, null, null, 96);
		}
	}

	static final void method1819(byte i) {
		Class122.anInt3549++;
		Connection class204 = Class296_Sub51_Sub13.method3111(true);
		if (i > -10) {
			buildedMapParts = null;
		}
		Class296_Sub1 class296_sub1 = Class253.allocateOut(class204.aClass185_2053, (byte) 115, Class146.aClass311_1443);
		class296_sub1.out.p1(0);
		class204.sendPacket(class296_sub1, (byte) 119);
	}

	final void method1820(ha var_ha, boolean bool, s var_s, s var_s_114_) {
		int[][] is = new int[anInt1859][anInt1868];
		if (bool) {
			buildedMapParts = null;
		}
		if (Class41_Sub3.anIntArray3743 == null || anInt1868 != Class41_Sub3.anIntArray3743.length) {
			Class401.anIntArray3365 = new int[anInt1868];
			InterfaceComponentSettings.anIntArray4825 = new int[anInt1868];
			Class296_Sub4.anIntArray4615 = new int[anInt1868];
			Class296_Sub51_Sub38.anIntArray6538 = new int[anInt1868];
			Class41_Sub3.anIntArray3743 = new int[anInt1868];
		}
		for (int i = 0; i < anInt1870; i++) {
			for (int i_115_ = 0; anInt1868 > i_115_; i_115_++) {
				Class41_Sub3.anIntArray3743[i_115_] = 0;
				Class296_Sub51_Sub38.anIntArray6538[i_115_] = 0;
				InterfaceComponentSettings.anIntArray4825[i_115_] = 0;
				Class401.anIntArray3365[i_115_] = 0;
				Class296_Sub4.anIntArray4615[i_115_] = 0;
			}
			for (int i_116_ = -5; anInt1859 > i_116_; i_116_++) {
				for (int i_117_ = 0; anInt1868 > i_117_; i_117_++) {
					int i_118_ = i_116_ + 5;
					if (anInt1859 > i_118_) {
						int i_119_ = aByteArrayArrayArray1865[i][i_118_][i_117_] & 0xff;
						if (i_119_ > 0) {
							Class236 class236 = aClass117_1862.method1020(true, i_119_ - 1);
							Class41_Sub3.anIntArray3743[i_117_] += class236.anInt2234;
							Class296_Sub51_Sub38.anIntArray6538[i_117_] += class236.anInt2236;
							InterfaceComponentSettings.anIntArray4825[i_117_] += class236.anInt2237;
							Class401.anIntArray3365[i_117_] += class236.anInt2238;
							Class296_Sub4.anIntArray4615[i_117_]++;
						}
					}
					int i_120_ = i_116_ - 5;
					if (i_120_ >= 0) {
						int i_121_ = aByteArrayArrayArray1865[i][i_120_][i_117_] & 0xff;
						if (i_121_ > 0) {
							Class236 class236 = aClass117_1862.method1020(true, i_121_ - 1);
							Class41_Sub3.anIntArray3743[i_117_] -= class236.anInt2234;
							Class296_Sub51_Sub38.anIntArray6538[i_117_] -= class236.anInt2236;
							InterfaceComponentSettings.anIntArray4825[i_117_] -= class236.anInt2237;
							Class401.anIntArray3365[i_117_] -= class236.anInt2238;
							Class296_Sub4.anIntArray4615[i_117_]--;
						}
					}
				}
				if (i_116_ >= 0) {
					int i_122_ = 0;
					int i_123_ = 0;
					int i_124_ = 0;
					int i_125_ = 0;
					int i_126_ = 0;
					for (int i_127_ = -5; anInt1868 > i_127_; i_127_++) {
						int i_128_ = i_127_ + 5;
						if (i_128_ < anInt1868) {
							i_123_ += Class296_Sub51_Sub38.anIntArray6538[i_128_];
							i_122_ += Class41_Sub3.anIntArray3743[i_128_];
							i_125_ += Class401.anIntArray3365[i_128_];
							i_126_ += Class296_Sub4.anIntArray4615[i_128_];
							i_124_ += InterfaceComponentSettings.anIntArray4825[i_128_];
						}
						int i_129_ = i_127_ - 5;
						if (i_129_ >= 0) {
							i_126_ -= Class296_Sub4.anIntArray4615[i_129_];
							i_122_ -= Class41_Sub3.anIntArray3743[i_129_];
							i_124_ -= InterfaceComponentSettings.anIntArray4825[i_129_];
							i_123_ -= Class296_Sub51_Sub38.anIntArray6538[i_129_];
							i_125_ -= Class401.anIntArray3365[i_129_];
						}
						if (i_127_ >= 0 && i_125_ > 0 && i_126_ > 0) {
							is[i_116_][i_127_] = Class296_Sub51_Sub17.method3123(i_124_ / i_126_, i_122_ * 256 / i_125_, i_123_ / i_126_, -1);
						}
					}
				}
			}
			if (!Class296_Sub51_Sub2.aBoolean6338) {
				method1822(-81, i != 0 ? null : var_s_114_, is, var_ha, i, Class360_Sub2.aSArray5304[i], i == 0 ? var_s : null);
			} else {
				method1816(is, i != 0 ? null : var_s, i != 0 ? null : var_s_114_, true, var_ha, Class360_Sub2.aSArray5304[i], i);
			}
			aByteArrayArrayArray1865[i] = null;
			aByteArrayArrayArray1867[i] = null;
			aByteArrayArrayArray1869[i] = null;
			aByteArrayArrayArray1863[i] = null;
		}
		if (!aBoolean1860) {
			if (Class360_Sub6.anInt5331 != 0) {
				Class366_Sub7.method3792();
			}
			if (Class259.aBoolean2416) {
				Class393.method4056();
			}
		}
		for (int i = 0; i < anInt1870; i++) {
			Class360_Sub2.aSArray5304[i].YA();
		}
	}

	final void method1821(int i, int i_130_, Packet class296_sub17, int i_131_, int i_132_, ClipData[] class17s, int i_133_, int i_134_, int i_135_, int i_136_) {
		int i_137_ = (i_136_ & 0x7) * 8;
		int i_138_ = (i_130_ & 0x7) * 8;
		if (!aBoolean1860) {
			ClipData class17 = class17s[i_135_];
			for (int i_139_ = 0; i_139_ < 8; i_139_++) {
				for (int i_140_ = 0; i_140_ < 8; i_140_++) {
					int i_141_ = ClipData.method264(i_139_ & 0x7, i_140_ & 0x7, i_132_, (byte) 76) + i;
					int i_142_ = Class67.method713(7, i_132_, i_139_ & 0x7, i_140_ & 0x7) + i_134_;
					if (i_141_ > 0 && anInt1859 - 1 > i_141_ && i_142_ > 0 && i_142_ < anInt1868 - 1) {
						class17.unflagUnmovable(i_141_, i_142_);
					}
				}
			}
		}
		int i_143_ = i_136_ << 3 & ~0x3f;
		int i_144_ = i_130_ << 3 & ~0x3f;
		int i_145_ = 0;
		int i_146_ = i_133_;
		if (i_132_ != 1) {
			if (i_132_ != 2) {
				if (i_132_ == 3) {
					i_145_ = 1;
				}
			} else {
				i_145_ = 1;
				i_146_ = 1;
			}
		} else {
			i_146_ = 1;
		}
		for (int i_147_ = 0; anInt1870 > i_147_; i_147_++) {
			for (int i_148_ = 0; i_148_ < 64; i_148_++) {
				for (int i_149_ = 0; i_149_ < 64; i_149_++) {
					if (i_131_ == i_147_ && i_137_ <= i_148_ && i_148_ <= i_137_ + 8 && i_149_ >= i_138_ && i_138_ + 8 >= i_149_) {
						int i_150_;
						int i_151_;
						if (i_137_ + 8 == i_148_ || i_138_ + 8 == i_149_) {
							if (i_132_ != 0) {
								if (i_132_ != 1) {
									if (i_132_ != 2) {
										i_151_ = i_134_ - (-i_148_ + i_137_);
										i_150_ = i - (-8 - (-i_149_ + i_138_));
									} else {
										i_151_ = i_138_ + -i_149_ + i_134_ + 8;
										i_150_ = -i_148_ - (-i_137_ - i - 8);
									}
								} else {
									i_150_ = i_149_ - i_138_ + i;
									i_151_ = -i_148_ + i_137_ + i_134_ + 8;
								}
							} else {
								i_151_ = i_134_ + i_149_ - i_138_;
								i_150_ = i - (i_137_ - i_148_);
							}
							method1826(i_149_ + i_144_, i_135_, i_148_ + i_143_, (byte) 102, class296_sub17, true, 0, 0, i_151_, 0, i_150_);
						} else {
							i_150_ = ClipData.method264(i_148_ & 0x7, i_149_ & 0x7, i_132_, (byte) -117) + i;
							i_151_ = i_134_ + Class67.method713(i_133_ + 7, i_132_, i_148_ & 0x7, i_149_ & 0x7);
							method1826(i_144_ + i_149_, i_135_, i_143_ + i_148_, (byte) 120, class296_sub17, false, i_132_, i_146_, i_151_, i_145_, i_150_);
						}
						if (i_148_ == 63 || i_149_ == 63) {
							int i_152_ = 1;
							if (i_148_ == 63 && i_149_ == 63) {
								i_152_ = 3;
							}
							for (int i_153_ = 0; i_152_ > i_153_; i_153_++) {
								int i_154_ = i_148_;
								int i_155_ = i_149_;
								if (i_153_ == 0) {
									i_154_ = i_148_ == 63 ? 64 : i_148_;
									i_155_ = i_149_ == 63 ? 64 : i_149_;
								} else if (i_153_ != 1) {
									if (i_153_ == 2) {
										i_155_ = 64;
									}
								} else {
									i_154_ = 64;
								}
								int i_156_;
								int i_157_;
								if (i_132_ != 0) {
									if (i_132_ != 1) {
										if (i_132_ != 2) {
											i_157_ = -i_155_ + i_138_ + 8 + i;
											i_156_ = i_154_ - i_137_ + i_134_;
										} else {
											i_157_ = -i_154_ + i_137_ + i + 8;
											i_156_ = i_138_ + -i_155_ + 8 + i_134_;
										}
									} else {
										i_156_ = -i_154_ + i_137_ + 8 + i_134_;
										i_157_ = -i_138_ + i_155_ + i;
									}
								} else {
									i_156_ = i_134_ - (-i_155_ + i_138_);
									i_157_ = i_154_ - i_137_ + i;
								}
								if (i_157_ >= 0 && anInt1859 > i_157_ && i_156_ >= 0 && i_156_ < anInt1868) {
									anIntArrayArrayArray1866[i_135_][i_157_][i_156_] = anIntArrayArrayArray1866[i_135_][i_150_ + i_145_][i_151_ + i_146_];
								}
							}
						}
					} else {
						method1826(0, 0, 0, (byte) 85, class296_sub17, false, 0, 0, -1, 0, -1);
					}
				}
			}
		}
	}

	private final void method1822(int i, s var_s, int[][] is, ha var_ha, int i_158_, s var_s_159_, s var_s_160_) {
		if (i >= -59) {
			anInt1861 = 124;
		}
		for (int i_161_ = 0; anInt1859 > i_161_; i_161_++) {
			for (int i_162_ = 0; anInt1868 > i_162_; i_162_++) {
				if (Class41_Sub5.anInt3751 == -1 || Class296_Sub40.method2912(23266, i_162_, Class41_Sub5.anInt3751, i_161_, i_158_)) {
					byte i_163_ = aByteArrayArrayArray1869[i_158_][i_161_][i_162_];
					byte i_164_ = aByteArrayArrayArray1863[i_158_][i_161_][i_162_];
					int i_165_ = aByteArrayArrayArray1867[i_158_][i_161_][i_162_] & 0xff;
					int i_166_ = aByteArrayArrayArray1865[i_158_][i_161_][i_162_] & 0xff;
					Class61 class61 = i_165_ != 0 ? aClass15_1874.method232(true, i_165_ - 1) : null;
					if (i_163_ == 0 && class61 == null) {
						i_163_ = (byte) 12;
					}
					Class236 class236 = i_166_ == 0 ? null : aClass117_1862.method1020(true, i_166_ - 1);
					Class61 class61_167_ = class61;
					if (class61 != null && class61.anInt702 == -1 && class61.anInt707 == -1) {
						class61_167_ = class61;
						class61 = null;
					}
					if (class61 != null || class236 != null) {
						int i_168_ = StaticMethods.anIntArray5953[i_163_];
						int i_169_ = Class302.anIntArray2722[i_163_];
						int i_170_ = (class236 != null ? i_168_ : 0) + (class61 != null ? i_169_ : 0);
						int i_171_ = 0;
						int i_172_ = 0;
						int i_173_ = class61 != null ? class61.anInt709 : -1;
						int i_174_ = class236 != null ? class236.anInt2230 : -1;
						int[] is_175_ = new int[i_170_];
						int[] is_176_ = new int[i_170_];
						int[] is_177_ = new int[i_170_];
						int[] is_178_ = new int[i_170_];
						int[] is_179_ = new int[i_170_];
						int[] is_180_ = new int[i_170_];
						int[] is_181_ = class61 != null && class61.anInt707 != -1 ? new int[i_170_] : null;
						if (class61 == null) {
							i_172_ += i_169_;
						} else {
							for (int i_182_ = 0; i_169_ > i_182_; i_182_++) {
								is_175_[i_171_] = Class296_Sub40.anIntArrayArray4910[i_163_][i_172_];
								is_176_[i_171_] = Class50.anIntArrayArray471[i_163_][i_172_];
								is_177_[i_171_] = Class296_Sub39_Sub4.anIntArrayArray5735[i_163_][i_172_];
								is_179_[i_171_] = i_173_;
								is_180_[i_171_] = class61.anInt706;
								is_178_[i_171_] = class61.anInt702;
								if (is_181_ != null) {
									is_181_[i_171_] = class61.anInt707;
								}
								i_172_++;
								i_171_++;
							}
							if (!aBoolean1860 && i_158_ == 0) {
								Class79.method805(i_161_, i_162_, class61.anInt699, class61.anInt701 * 8, class61.anInt700);
							}
						}
						if (class236 != null) {
							for (int i_183_ = 0; i_168_ > i_183_; i_183_++) {
								is_175_[i_171_] = Class296_Sub40.anIntArrayArray4910[i_163_][i_172_];
								is_176_[i_171_] = Class50.anIntArrayArray471[i_163_][i_172_];
								is_177_[i_171_] = Class296_Sub39_Sub4.anIntArrayArray5735[i_163_][i_172_];
								is_179_[i_171_] = i_174_;
								is_180_[i_171_] = class236.anInt2241;
								is_178_[i_171_] = is[i_161_][i_162_];
								if (is_181_ != null) {
									is_181_[i_171_] = is_178_[i_171_];
								}
								i_171_++;
								i_172_++;
							}
						}
						int i_184_ = anIntArray1873.length;
						int[] is_185_ = new int[i_184_];
						int[] is_186_ = new int[i_184_];
						int[] is_187_ = var_s != null ? new int[i_184_] : null;
						int[] is_188_ = var_s == null && var_s_160_ == null ? null : new int[i_184_];
						for (int i_189_ = 0; i_184_ > i_189_; i_189_++) {
							int i_190_ = anIntArray1873[i_189_];
							int i_191_ = anIntArray1858[i_189_];
							if (i_164_ != 0) {
								if (i_164_ != 1) {
									if (i_164_ != 2) {
										if (i_164_ == 3) {
											int i_192_ = i_190_;
											is_185_[i_189_] = 512 - i_191_;
											is_186_[i_189_] = i_192_;
										}
									} else {
										is_185_[i_189_] = -i_190_ + 512;
										is_186_[i_189_] = 512 - i_191_;
									}
								} else {
									int i_193_ = i_190_;
									is_185_[i_189_] = i_191_;
									is_186_[i_189_] = 512 - i_193_;
								}
							} else {
								is_185_[i_189_] = i_190_;
								is_186_[i_189_] = i_191_;
							}
							if (is_187_ != null && Class52.aBooleanArrayArray635[i_163_][i_189_]) {
								int i_194_ = is_185_[i_189_] + (i_161_ << 9);
								int i_195_ = is_186_[i_189_] + (i_162_ << 9);
								is_187_[i_189_] = var_s.method3349(0, i_195_, i_194_) - var_s_159_.method3349(0, i_195_, i_194_);
							}
							if (is_188_ != null) {
								if (var_s != null && !Class52.aBooleanArrayArray635[i_163_][i_189_]) {
									int i_196_ = is_185_[i_189_] + (i_161_ << 9);
									int i_197_ = is_186_[i_189_] + (i_162_ << 9);
									is_188_[i_189_] = var_s_159_.method3349(0, i_197_, i_196_) - var_s.method3349(0, i_197_, i_196_);
								} else if (var_s_160_ != null && !Class110.aBooleanArrayArray1142[i_163_][i_189_]) {
									int i_198_ = is_185_[i_189_] + (i_161_ << 9);
									int i_199_ = is_186_[i_189_] + (i_162_ << 9);
									is_188_[i_189_] = var_s_160_.method3349(0, i_199_, i_198_) - var_s_159_.method3349(0, i_199_, i_198_);
								}
							}
						}
						int i_200_ = var_s_159_.method3355(i_162_, (byte) -125, i_161_);
						int i_201_ = var_s_159_.method3355(i_162_, (byte) -124, i_161_ + 1);
						int i_202_ = var_s_159_.method3355(i_162_ + 1, (byte) -116, i_161_ + 1);
						int i_203_ = var_s_159_.method3355(i_162_ + 1, (byte) -121, i_161_);
						boolean bool = r_Sub2.method2871(i_162_, i_161_, (byte) -47);
						if (bool && i_158_ > 1 || !bool && i_158_ > 0) {
							boolean bool_204_ = true;
							if (class236 != null && !class236.aBoolean2239) {
								bool_204_ = false;
							} else if (i_166_ == 0 && i_163_ != 0) {
								bool_204_ = false;
							} else if (i_165_ > 0 && class61_167_ != null && !class61_167_.aBoolean703) {
								bool_204_ = false;
							}
							if (bool_204_ && i_201_ == i_200_ && i_202_ == i_200_ && i_203_ == i_200_) {
								aByteArrayArrayArray1871[i_158_][i_161_][i_162_] = (byte) Class48.bitOR(aByteArrayArrayArray1871[i_158_][i_161_][i_162_], 4);
							}
						}
						int i_205_ = 0;
						int i_206_ = 0;
						int i_207_ = 0;
						if (aBoolean1860) {
							i_205_ = Class162.method1624(i_161_, i_162_);
							i_206_ = Class249.method2195(i_161_, i_162_);
							i_207_ = Class18.method2825(i_161_, i_162_);
						}
						var_s_159_.method3356(i_161_, i_162_, is_185_, is_187_, is_186_, is_188_, is_175_, is_176_, is_177_, is_178_, is_181_, is_179_, is_180_, i_205_, i_206_, i_207_, false);
						Class320.method3340(i_158_, i_161_, i_162_);
					}
				}
			}
		}
	}

	final void method1823(int i, int i_208_, byte i_209_, int i_210_, int i_211_, int i_212_) {
		for (int i_213_ = i_212_; i_212_ + i_211_ > i_213_; i_213_++) {
			for (int i_214_ = i_208_; i_214_ < i + i_208_; i_214_++) {
				if (i_214_ >= 0 && i_214_ < anInt1859 && i_213_ >= 0 && i_213_ < anInt1868) {
					anIntArrayArrayArray1866[i_210_][i_214_][i_213_] = i_210_ <= 0 ? 0 : anIntArrayArrayArray1866[i_210_ - 1][i_214_][i_213_] - 960;
				}
			}
		}
		if (i_209_ == -123) {
			if (i_208_ > 0 && i_208_ < anInt1859) {
				for (int i_215_ = i_212_ + 1; i_211_ + i_212_ > i_215_; i_215_++) {
					if (i_215_ >= 0 && anInt1868 > i_215_) {
						anIntArrayArrayArray1866[i_210_][i_208_][i_215_] = anIntArrayArrayArray1866[i_210_][i_208_ - 1][i_215_];
					}
				}
			}
			if (i_212_ > 0 && anInt1868 > i_212_) {
				for (int i_216_ = i_208_ + 1; i + i_208_ > i_216_; i_216_++) {
					if (i_216_ >= 0 && anInt1859 > i_216_) {
						anIntArrayArrayArray1866[i_210_][i_216_][i_212_] = anIntArrayArrayArray1866[i_210_][i_216_][i_212_ - 1];
					}
				}
			}
			do {
				if (i_208_ >= 0 && i_212_ >= 0 && i_208_ < anInt1859 && anInt1868 > i_212_) {
					if (i_210_ != 0) {
						if (i_208_ > 0 && anIntArrayArrayArray1866[i_210_][i_208_ - 1][i_212_] != anIntArrayArrayArray1866[i_210_ - 1][i_208_ - 1][i_212_]) {
							anIntArrayArrayArray1866[i_210_][i_208_][i_212_] = anIntArrayArrayArray1866[i_210_][i_208_ - 1][i_212_];
						} else if (i_212_ <= 0 || anIntArrayArrayArray1866[i_210_][i_208_][i_212_ - 1] == anIntArrayArrayArray1866[i_210_ - 1][i_208_][i_212_ - 1]) {
							if (i_208_ > 0 && i_212_ > 0 && anIntArrayArrayArray1866[i_210_][i_208_ - 1][i_212_ - 1] != anIntArrayArrayArray1866[i_210_ - 1][i_208_ - 1][i_212_ - 1]) {
								anIntArrayArrayArray1866[i_210_][i_208_][i_212_] = anIntArrayArrayArray1866[i_210_][i_208_ - 1][i_212_ - 1];
							}
						} else {
							anIntArrayArrayArray1866[i_210_][i_208_][i_212_] = anIntArrayArrayArray1866[i_210_][i_208_][i_212_ - 1];
						}
					} else if (i_208_ > 0 && anIntArrayArrayArray1866[i_210_][i_208_ - 1][i_212_] != 0) {
						anIntArrayArrayArray1866[i_210_][i_208_][i_212_] = anIntArrayArrayArray1866[i_210_][i_208_ - 1][i_212_];
					} else if (i_212_ > 0 && anIntArrayArrayArray1866[i_210_][i_208_][i_212_ - 1] != 0) {
						anIntArrayArrayArray1866[i_210_][i_208_][i_212_] = anIntArrayArrayArray1866[i_210_][i_208_][i_212_ - 1];
					} else {
						if (i_208_ <= 0 || i_212_ <= 0 || anIntArrayArrayArray1866[i_210_][i_208_ - 1][i_212_ - 1] == 0) {
							break;
						}
						anIntArrayArrayArray1866[i_210_][i_208_][i_212_] = anIntArrayArrayArray1866[i_210_][i_208_ - 1][i_212_ - 1];
					}
					break;
				}
			} while (false);
		}
	}

	final void method1824(int i, int[][] is, byte i_217_) {
		int[][] is_218_ = anIntArrayArrayArray1866[i];
		if (i_217_ >= -113) {
			method1824(98, null, (byte) 68);
		}
		for (int i_219_ = 0; i_219_ < anInt1859 + 1; i_219_++) {
			for (int i_220_ = 0; i_220_ < anInt1868 + 1; i_220_++) {
				is_218_[i_219_][i_220_] += is[i_219_][i_220_];
			}
		}
	}

	final void method1825(int i, int i_221_, int i_222_, int i_223_, int i_224_) {
		for (int i_225_ = i_223_; anInt1870 > i_225_; i_225_++) {
			method1823(i_222_, i, (byte) -123, i_225_, i_221_, i_224_);
		}
	}

	private final void method1826(int i, int i_226_, int i_227_, byte i_228_, Packet class296_sub17, boolean bool, int i_229_, int i_230_, int i_231_, int i_232_, int i_233_) {
		if (i_228_ <= 57) {
			anIntArray1873 = null;
		}
		if (i_229_ == 1) {
			i_230_ = 1;
		} else if (i_229_ == 2) {
			i_232_ = 1;
			i_230_ = 1;
		} else if (i_229_ == 3) {
			i_232_ = 1;
		}
		if (i_233_ >= 0 && anInt1859 > i_233_ && i_231_ >= 0 && anInt1868 > i_231_) {
			if (!aBoolean1860 && !bool) {
				Class41_Sub18.aByteArrayArrayArray3786[i_226_][i_233_][i_231_] = (byte) 0;
			}
			for (;;) {
				int i_234_ = class296_sub17.g1();
				if (i_234_ == 0) {
					if (!aBoolean1860) {
						if (i_226_ != 0) {
							anIntArrayArrayArray1866[i_226_][i_233_ + i_232_][i_231_ + i_230_] = anIntArrayArrayArray1866[i_226_ - 1][i_232_ + i_233_][i_230_ + i_231_] - 960;
						} else {
							anIntArrayArrayArray1866[0][i_233_ + i_232_][i_230_ + i_231_] = -Statics.method626(932731 + i_227_, (byte) 15, 556238 + i) * 8 << 2;
						}
					} else {
						anIntArrayArrayArray1866[0][i_233_ + i_232_][i_231_ + i_230_] = 0;
						break;
					}
					break;
				}
				if (i_234_ == 1) {
					int i_235_ = class296_sub17.g1();
					if (aBoolean1860) {
						anIntArrayArrayArray1866[0][i_233_ + i_232_][i_231_ + i_230_] = i_235_ * 8 << 2;
					} else {
						if (i_235_ == 1) {
							i_235_ = 0;
						}
						if (i_226_ != 0) {
							anIntArrayArrayArray1866[i_226_][i_233_ + i_232_][i_230_ + i_231_] = anIntArrayArrayArray1866[i_226_ - 1][i_233_ + i_232_][i_230_ + i_231_] - (i_235_ * 8 << 2);
						} else {
							anIntArrayArrayArray1866[0][i_232_ + i_233_][i_230_ + i_231_] = -i_235_ * 8 << 2;
							break;
						}
						break;
					}
					break;
				}
				if (i_234_ <= 49) {
					if (bool) {
						class296_sub17.g1();
					} else {
						aByteArrayArrayArray1867[i_226_][i_233_][i_231_] = class296_sub17.g1b();
						aByteArrayArrayArray1869[i_226_][i_233_][i_231_] = (byte) ((i_234_ - 2) / 4);
						aByteArrayArrayArray1863[i_226_][i_233_][i_231_] = (byte) (3 & i_234_ + i_229_ - 2);
					}
				} else if (i_234_ <= 81) {
					if (!aBoolean1860 && !bool) {
						Class41_Sub18.aByteArrayArrayArray3786[i_226_][i_233_][i_231_] = (byte) (i_234_ - 49);
					}
				} else if (!bool) {
					aByteArrayArrayArray1865[i_226_][i_233_][i_231_] = (byte) (i_234_ - 81);
				}
			}
		} else {
			for (;;) {
				int i_236_ = class296_sub17.g1();
				if (i_236_ == 0) {
					break;
				}
				if (i_236_ == 1) {
					class296_sub17.g1();
					break;
				}
				if (i_236_ <= 49) {
					class296_sub17.g1();
				}
			}
		}
	}

	static final void method1827(int i, String string, int i_237_, String string_238_, String string_239_, String string_240_, int i_241_) {
		Class296_Sub51_Sub9.method3099(i_241_, string, i, null, string_239_, false, string_238_, -1, string_240_);
		if (i_237_ != 0) {
			method1817(false);
		}
	}

	public static void method1828(boolean bool) {
		buildedMapParts = null;
		if (bool) {
			method1819((byte) -70);
		}
	}

	private final void method1829(ha var_ha, int i, byte[][] is, Class236 class236, int i_242_, int i_243_, int i_244_, boolean[] bools, Class61 class61, int i_245_, byte[][] is_246_, byte[][] is_247_, int i_248_, int i_249_) {
		boolean[] bools_250_ = class61 != null && class61.aBoolean705 ? Class300.aBooleanArrayArray3697[i_243_] : Class41_Sub16.aBooleanArrayArray3780[i_243_];
		if (i_249_ > 0) {
			if (i_245_ > 0) {
				int i_251_ = is_247_[i_245_ - 1][i_249_ - 1] & 0xff;
				if (i_251_ > 0) {
					Class61 class61_252_ = aClass15_1874.method232(true, i_251_ - 1);
					if (class61_252_.anInt702 != -1 && class61_252_.aBoolean705) {
						byte i_253_ = is[i_245_ - 1][i_249_ - 1];
						int i_254_ = is_246_[i_245_ - 1][i_249_ - 1] * 2 + 4 & 0x7;
						int i_255_ = Class368_Sub3.method3815(false, var_ha, class61_252_);
						if (Class52.aBooleanArrayArray635[i_253_][i_254_]) {
							StaticMethods.anIntArray1624[0] = class61_252_.anInt702;
							Class296_Sub51_Sub21.anIntArray6450[0] = i_255_;
							Class392.anIntArray3487[0] = class61_252_.anInt709;
							Class296_Sub39_Sub11.anIntArray6188[0] = class61_252_.anInt706;
							ObjTypeList.anIntArray1321[0] = class61_252_.anInt708;
							Class347.anIntArray3027[0] = 256;
						}
					}
				}
			}
			if (i_242_ - 1 > i_245_) {
				int i_256_ = is_247_[i_245_ + 1][i_249_ - 1] & 0xff;
				if (i_256_ > 0) {
					Class61 class61_257_ = aClass15_1874.method232(true, i_256_ - 1);
					if (class61_257_.anInt702 != -1 && class61_257_.aBoolean705) {
						byte i_258_ = is[i_245_ + 1][i_249_ - 1];
						int i_259_ = is_246_[i_245_ + 1][i_249_ - 1] * 2 + 6 & 0x7;
						int i_260_ = Class368_Sub3.method3815(false, var_ha, class61_257_);
						if (Class52.aBooleanArrayArray635[i_258_][i_259_]) {
							StaticMethods.anIntArray1624[2] = class61_257_.anInt702;
							Class296_Sub51_Sub21.anIntArray6450[2] = i_260_;
							Class392.anIntArray3487[2] = class61_257_.anInt709;
							Class296_Sub39_Sub11.anIntArray6188[2] = class61_257_.anInt706;
							ObjTypeList.anIntArray1321[2] = class61_257_.anInt708;
							Class347.anIntArray3027[2] = 512;
						}
					}
				}
			}
		}
		if (i_248_ - 1 > i_249_) {
			if (i_245_ > 0) {
				int i_261_ = is_247_[i_245_ - 1][i_249_ + 1] & 0xff;
				if (i_261_ > 0) {
					Class61 class61_262_ = aClass15_1874.method232(true, i_261_ - 1);
					if (class61_262_.anInt702 != -1 && class61_262_.aBoolean705) {
						byte i_263_ = is[i_245_ - 1][i_249_ + 1];
						int i_264_ = is_246_[i_245_ - 1][i_249_ + 1] * 2 + 2 & 0x7;
						int i_265_ = Class368_Sub3.method3815(false, var_ha, class61_262_);
						if (Class52.aBooleanArrayArray635[i_263_][i_264_]) {
							StaticMethods.anIntArray1624[6] = class61_262_.anInt702;
							Class296_Sub51_Sub21.anIntArray6450[6] = i_265_;
							Class392.anIntArray3487[6] = class61_262_.anInt709;
							Class296_Sub39_Sub11.anIntArray6188[6] = class61_262_.anInt706;
							ObjTypeList.anIntArray1321[6] = class61_262_.anInt708;
							Class347.anIntArray3027[6] = 64;
						}
					}
				}
			}
			if (i_245_ < i_242_ - 1) {
				int i_266_ = is_247_[i_245_ + 1][i_249_ + 1] & 0xff;
				if (i_266_ > 0) {
					Class61 class61_267_ = aClass15_1874.method232(true, i_266_ - 1);
					if (class61_267_.anInt702 != -1 && class61_267_.aBoolean705) {
						byte i_268_ = is[i_245_ + 1][i_249_ + 1];
						int i_269_ = is_246_[i_245_ + 1][i_249_ + 1] * 2 & 0x7;
						int i_270_ = Class368_Sub3.method3815(false, var_ha, class61_267_);
						if (Class52.aBooleanArrayArray635[i_268_][i_269_]) {
							StaticMethods.anIntArray1624[4] = class61_267_.anInt702;
							Class296_Sub51_Sub21.anIntArray6450[4] = i_270_;
							Class392.anIntArray3487[4] = class61_267_.anInt709;
							Class296_Sub39_Sub11.anIntArray6188[4] = class61_267_.anInt706;
							ObjTypeList.anIntArray1321[4] = class61_267_.anInt708;
							Class347.anIntArray3027[4] = 128;
						}
					}
				}
			}
		}
		if (i_249_ > 0) {
			int i_271_ = is_247_[i_245_][i_249_ - 1] & 0xff;
			if (i_271_ > 0) {
				Class61 class61_272_ = aClass15_1874.method232(true, i_271_ - 1);
				if (class61_272_.anInt702 != -1) {
					byte i_273_ = is[i_245_][i_249_ - 1];
					int i_274_ = is_246_[i_245_][i_249_ - 1];
					if (!class61_272_.aBoolean705) {
						if (!bools_250_[i & 0x3]) {
							bools[0] = Class41_Sub16.aBooleanArrayArray3780[i_273_][3 & i_274_ + 2];
						}
					} else {
						int i_275_ = 2;
						int i_276_ = 4 + i_274_ * 2;
						int i_277_ = Class368_Sub3.method3815(false, var_ha, class61_272_);
						for (int i_278_ = 0; i_278_ < 3; i_278_++) {
							i_276_ &= 0x7;
							i_275_ &= 0x7;
							if (Class52.aBooleanArrayArray635[i_273_][i_276_] && ObjTypeList.anIntArray1321[i_275_] <= class61_272_.anInt708) {
								StaticMethods.anIntArray1624[i_275_] = class61_272_.anInt702;
								Class296_Sub51_Sub21.anIntArray6450[i_275_] = i_277_;
								Class392.anIntArray3487[i_275_] = class61_272_.anInt709;
								Class296_Sub39_Sub11.anIntArray6188[i_275_] = class61_272_.anInt706;
								if (ObjTypeList.anIntArray1321[i_275_] != class61_272_.anInt708) {
									Class347.anIntArray3027[i_275_] = 32;
								} else {
									Class347.anIntArray3027[i_275_] = Class48.bitOR(Class347.anIntArray3027[i_275_], 32);
								}
								ObjTypeList.anIntArray1321[i_275_] = class61_272_.anInt708;
							}
							i_275_--;
							i_276_++;
						}
						if (!bools_250_[i & 0x3]) {
							bools[0] = Class300.aBooleanArrayArray3697[i_273_][2 + i_274_ & 3];
						}
					}
				}
			}
		}
		if (i_248_ - 1 > i_249_) {
			int i_279_ = is_247_[i_245_][i_249_ + 1] & 0xff;
			if (i_279_ > 0) {
				Class61 class61_280_ = aClass15_1874.method232(true, i_279_ - 1);
				if (class61_280_.anInt702 != -1) {
					byte i_281_ = is[i_245_][i_249_ + 1];
					int i_282_ = is_246_[i_245_][i_249_ + 1];
					if (!class61_280_.aBoolean705) {
						if (!bools_250_[i + 2 & 0x3]) {
							bools[2] = Class41_Sub16.aBooleanArrayArray3780[i_281_][3 & i_282_];
						}
					} else {
						int i_283_ = 4;
						int i_284_ = i_282_ * 2 + 2;
						int i_285_ = Class368_Sub3.method3815(false, var_ha, class61_280_);
						for (int i_286_ = 0; i_286_ < 3; i_286_++) {
							i_283_ &= 0x7;
							i_284_ &= 0x7;
							if (Class52.aBooleanArrayArray635[i_281_][i_284_] && class61_280_.anInt708 >= ObjTypeList.anIntArray1321[i_283_]) {
								StaticMethods.anIntArray1624[i_283_] = class61_280_.anInt702;
								Class296_Sub51_Sub21.anIntArray6450[i_283_] = i_285_;
								Class392.anIntArray3487[i_283_] = class61_280_.anInt709;
								Class296_Sub39_Sub11.anIntArray6188[i_283_] = class61_280_.anInt706;
								if (class61_280_.anInt708 != ObjTypeList.anIntArray1321[i_283_]) {
									Class347.anIntArray3027[i_283_] = 16;
								} else {
									Class347.anIntArray3027[i_283_] = Class48.bitOR(Class347.anIntArray3027[i_283_], 16);
								}
								ObjTypeList.anIntArray1321[i_283_] = class61_280_.anInt708;
							}
							i_284_--;
							i_283_++;
						}
						if (!bools_250_[i + 2 & 0x3]) {
							bools[2] = Class300.aBooleanArrayArray3697[i_281_][--i_282_ & 3];
						}
					}
				}
			}
		}
		if (i_245_ > 0) {
			int i_287_ = is_247_[i_245_ - 1][i_249_] & 0xff;
			if (i_287_ > 0) {
				Class61 class61_288_ = aClass15_1874.method232(true, i_287_ - 1);
				if (class61_288_.anInt702 != -1) {
					byte i_289_ = is[i_245_ - 1][i_249_];
					int i_290_ = is_246_[i_245_ - 1][i_249_];
					if (!class61_288_.aBoolean705) {
						if (!bools_250_[i + 3 & 0x3]) {
							bools[3] = Class41_Sub16.aBooleanArrayArray3780[i_289_][3 & 1 + i_290_];
						}
					} else {
						int i_291_ = 6;
						int i_292_ = i_290_ * 2 + 4;
						int i_293_ = Class368_Sub3.method3815(false, var_ha, class61_288_);
						for (int i_294_ = 0; i_294_ < 3; i_294_++) {
							i_292_ &= 0x7;
							i_291_ &= 0x7;
							if (Class52.aBooleanArrayArray635[i_289_][i_292_] && ObjTypeList.anIntArray1321[i_291_] <= class61_288_.anInt708) {
								StaticMethods.anIntArray1624[i_291_] = class61_288_.anInt702;
								Class296_Sub51_Sub21.anIntArray6450[i_291_] = i_293_;
								Class392.anIntArray3487[i_291_] = class61_288_.anInt709;
								Class296_Sub39_Sub11.anIntArray6188[i_291_] = class61_288_.anInt706;
								if (class61_288_.anInt708 == ObjTypeList.anIntArray1321[i_291_]) {
									Class347.anIntArray3027[i_291_] = Class48.bitOR(Class347.anIntArray3027[i_291_], 8);
								} else {
									Class347.anIntArray3027[i_291_] = 8;
								}
								ObjTypeList.anIntArray1321[i_291_] = class61_288_.anInt708;
							}
							i_291_++;
							i_292_--;
						}
						if (!bools_250_[i + 3 & 0x3]) {
							bools[3] = Class300.aBooleanArrayArray3697[i_289_][3 & i_290_ + 1];
						}
					}
				}
			}
		}
		if (i_245_ < i_242_ - 1) {
			int i_295_ = is_247_[i_245_ + 1][i_249_] & 0xff;
			if (i_295_ > 0) {
				Class61 class61_296_ = aClass15_1874.method232(true, i_295_ - 1);
				if (class61_296_.anInt702 != -1) {
					byte i_297_ = is[i_245_ + 1][i_249_];
					int i_298_ = is_246_[i_245_ + 1][i_249_];
					if (class61_296_.aBoolean705) {
						int i_299_ = 4;
						int i_300_ = i_298_ * 2 + 6;
						int i_301_ = Class368_Sub3.method3815(false, var_ha, class61_296_);
						for (int i_302_ = 0; i_302_ < 3; i_302_++) {
							i_300_ &= 0x7;
							i_299_ &= 0x7;
							if (Class52.aBooleanArrayArray635[i_297_][i_300_] && class61_296_.anInt708 >= ObjTypeList.anIntArray1321[i_299_]) {
								StaticMethods.anIntArray1624[i_299_] = class61_296_.anInt702;
								Class296_Sub51_Sub21.anIntArray6450[i_299_] = i_301_;
								Class392.anIntArray3487[i_299_] = class61_296_.anInt709;
								Class296_Sub39_Sub11.anIntArray6188[i_299_] = class61_296_.anInt706;
								if (class61_296_.anInt708 == ObjTypeList.anIntArray1321[i_299_]) {
									Class347.anIntArray3027[i_299_] = Class48.bitOR(Class347.anIntArray3027[i_299_], 4);
								} else {
									Class347.anIntArray3027[i_299_] = 4;
								}
								ObjTypeList.anIntArray1321[i_299_] = class61_296_.anInt708;
							}
							i_300_++;
							i_299_--;
						}
						if (!bools_250_[1 + i & 0x3]) {
							bools[1] = Class300.aBooleanArrayArray3697[i_297_][3 + i_298_ & 3];
						}
					} else if (!bools_250_[i + 1 & 0x3]) {
						bools[1] = Class41_Sub16.aBooleanArrayArray3780[i_297_][i_298_ + 3 & 3];
					}
				}
			}
		}
		if (class61 != null) {
			int i_303_ = Class368_Sub3.method3815(false, var_ha, class61);
			if (class61.aBoolean705) {
				for (int i_304_ = 0; i_304_ < 8; i_304_++) {
					int i_305_ = i_304_ - i * 2 & 0x7;
					if (Class52.aBooleanArrayArray635[i_243_][i_304_] && class61.anInt708 >= ObjTypeList.anIntArray1321[i_305_]) {
						StaticMethods.anIntArray1624[i_305_] = class61.anInt702;
						Class296_Sub51_Sub21.anIntArray6450[i_305_] = i_303_;
						Class392.anIntArray3487[i_305_] = class61.anInt709;
						Class296_Sub39_Sub11.anIntArray6188[i_305_] = class61.anInt706;
						if (ObjTypeList.anIntArray1321[i_305_] == class61.anInt708) {
							Class347.anIntArray3027[i_305_] = Class48.bitOR(Class347.anIntArray3027[i_305_], 2);
						} else {
							Class347.anIntArray3027[i_305_] = 2;
						}
						ObjTypeList.anIntArray1321[i_305_] = class61.anInt708;
					}
				}
			}
		}
		if (i_244_ != 3) {
			anInt1861 = 105;
		}
	}

	final void method1830(ClipData[] class17s, int[][][] is, ha var_ha, int i) {
		if (!aBoolean1860) {
			for (int i_306_ = 0; i_306_ < 4; i_306_++) {
				for (int i_307_ = 0; i_307_ < anInt1859; i_307_++) {
					for (int i_308_ = 0; i_308_ < anInt1868; i_308_++) {
						if ((Class41_Sub18.aByteArrayArrayArray3786[i_306_][i_307_][i_308_] & 0x1) != 0) {
							int i_309_ = i_306_;
							if ((Class41_Sub18.aByteArrayArrayArray3786[1][i_307_][i_308_] & 0x2) != 0) {
								i_309_--;
							}
							if (i_309_ >= 0) {
								class17s[i_309_].flagUnmovable(i_307_, i_308_);
							}
						}
					}
				}
			}
		}
		if (i == -30558) {
			for (int i_310_ = 0; i_310_ < anInt1870; i_310_++) {
				int i_311_ = 0;
				int i_312_ = 0;
				if (!aBoolean1860) {
					if (Class259.aBoolean2416) {
						i_311_ |= 0x2;
					}
					if (Class41_Sub2.aBoolean3740) {
						i_312_ |= 0x8;
					}
					if (Class360_Sub6.anInt5331 != 0) {
						if (i_310_ == 0 | TranslatableString.aBoolean1263) {
							i_312_ |= 0x10;
						}
						i_311_ |= 0x1;
					}
				}
				if (Class259.aBoolean2416) {
					i_312_ |= 0x7;
				}
				if (!ByteStream.aBoolean6044) {
					i_312_ |= 0x20;
				}
				int[][] is_313_ = is == null || i_310_ >= is.length ? anIntArrayArrayArray1866[i_310_] : is[i_310_];
				NPCDefinitionLoader.method1419(i_310_, var_ha.a(anInt1859, anInt1868, anIntArrayArrayArray1866[i_310_], is_313_, 512, i_311_, i_312_));
			}
		}
	}

	Class181(int i, int i_314_, int i_315_, boolean bool, Class15 class15, Class117 class117) {
		aClass15_1874 = class15;
		anInt1868 = i_315_;
		anInt1870 = i;
		aBoolean1860 = bool;
		aClass117_1862 = class117;
		anInt1859 = i_314_;
		aByteArrayArrayArray1871 = new byte[anInt1870][anInt1859 + 1][anInt1868 + 1];
		aByteArrayArrayArray1863 = new byte[anInt1870][anInt1859][anInt1868];
		aByteArrayArrayArray1867 = new byte[anInt1870][anInt1859][anInt1868];
		aByteArrayArrayArray1865 = new byte[anInt1870][anInt1859][anInt1868];
		anIntArrayArrayArray1866 = new int[anInt1870][anInt1859 + 1][anInt1868 + 1];
		aByteArrayArrayArray1869 = new byte[anInt1870][anInt1859][anInt1868];
	}
}
