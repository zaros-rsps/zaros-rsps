package net.zaros.client.ui.skin;

import org.pushingpixels.substance.api.ComponentState;
import org.pushingpixels.substance.api.SubstanceColorSchemeBundle;
import org.pushingpixels.substance.api.SubstanceSkin;
import org.pushingpixels.substance.api.SubstanceSlices.ColorSchemeAssociationKind;
import org.pushingpixels.substance.api.SubstanceSlices.DecorationAreaType;
import org.pushingpixels.substance.api.colorscheme.ColorSchemeSingleColorQuery;
import org.pushingpixels.substance.api.colorscheme.EbonyColorScheme;
import org.pushingpixels.substance.api.colorscheme.SubstanceColorScheme;
import org.pushingpixels.substance.api.painter.border.ClassicBorderPainter;
import org.pushingpixels.substance.api.painter.border.CompositeBorderPainter;
import org.pushingpixels.substance.api.painter.border.DelegateBorderPainter;
import org.pushingpixels.substance.api.painter.decoration.MarbleNoiseDecorationPainter;
import org.pushingpixels.substance.api.painter.highlight.ClassicHighlightPainter;
import org.pushingpixels.substance.api.painter.overlay.BottomLineOverlayPainter;
import org.pushingpixels.substance.api.shaper.ClassicButtonShaper;
import org.pushingpixels.substance.internal.painter.SimplisticFillPainter;

/**
 * @author Walied K. Yassen
 */
public final class ZarosSkin extends SubstanceSkin {

	/**
	 * Constructs a new {@link ZarosSkin} type object instance.
	 */
	public ZarosSkin() {
		final SubstanceSkin.ColorSchemes schemes = SubstanceSkin.getColorSchemes("zaros-skin.colorschemes");
		SubstanceColorScheme activeScheme = schemes.get("Zaros Active");
		SubstanceColorScheme selectedDisabledScheme = schemes.get("Zaros Selected Disabled");
		SubstanceColorScheme selectedScheme = schemes.get("Zaros Selected");
		SubstanceColorScheme disabledScheme = schemes.get("Zaros Disabled");

		SubstanceColorScheme enabledScheme = schemes.get("Zaros Enabled");
		SubstanceColorScheme backgroundScheme = schemes.get("Zaros Background");

		SubstanceColorSchemeBundle defaultSchemeBundle = new SubstanceColorSchemeBundle(activeScheme, enabledScheme, disabledScheme);

		// highlight fill scheme + custom alpha for rollover unselected state
		SubstanceColorScheme highlightScheme = schemes.get("Zaros Highlight");
		defaultSchemeBundle.registerHighlightColorScheme(highlightScheme, 0.6f, ComponentState.ROLLOVER_UNSELECTED);
		defaultSchemeBundle.registerHighlightColorScheme(highlightScheme, 0.8f, ComponentState.SELECTED);
		defaultSchemeBundle.registerHighlightColorScheme(highlightScheme, 1.0f, ComponentState.ROLLOVER_SELECTED);
		defaultSchemeBundle.registerHighlightColorScheme(highlightScheme, 0.75f, ComponentState.ARMED, ComponentState.ROLLOVER_ARMED);

		// highlight border scheme
		SubstanceColorScheme borderScheme = schemes.get("Zaros Border");
		SubstanceColorScheme separatorScheme = schemes.get("Zaros Separator");
		defaultSchemeBundle.registerColorScheme(new EbonyColorScheme(), ColorSchemeAssociationKind.HIGHLIGHT_BORDER, ComponentState.getActiveStates());
		defaultSchemeBundle.registerColorScheme(borderScheme, ColorSchemeAssociationKind.BORDER);
		defaultSchemeBundle.registerColorScheme(separatorScheme, ColorSchemeAssociationKind.SEPARATOR);

		// text highlight scheme
		SubstanceColorScheme textHighlightScheme = schemes.get("Zaros Text Highlight");
		defaultSchemeBundle.registerColorScheme(textHighlightScheme, ColorSchemeAssociationKind.HIGHLIGHT_TEXT, ComponentState.SELECTED, ComponentState.ROLLOVER_SELECTED);

		defaultSchemeBundle.registerColorScheme(highlightScheme, ComponentState.ARMED, ComponentState.ROLLOVER_ARMED);

		SubstanceColorScheme highlightMarkScheme = schemes.get("Zaros Highlight Mark");
		defaultSchemeBundle.registerColorScheme(highlightMarkScheme, ColorSchemeAssociationKind.HIGHLIGHT_MARK, ComponentState.getActiveStates());
		defaultSchemeBundle.registerColorScheme(highlightMarkScheme, ColorSchemeAssociationKind.MARK, ComponentState.ROLLOVER_SELECTED, ComponentState.ROLLOVER_UNSELECTED);
		defaultSchemeBundle.registerColorScheme(borderScheme, ColorSchemeAssociationKind.MARK, ComponentState.SELECTED);

		defaultSchemeBundle.registerColorScheme(disabledScheme, 0.5f, ComponentState.DISABLED_UNSELECTED);
		defaultSchemeBundle.registerColorScheme(selectedDisabledScheme, 0.65f, ComponentState.DISABLED_SELECTED);
		defaultSchemeBundle.registerColorScheme(disabledScheme, ColorSchemeAssociationKind.MARK, ComponentState.DISABLED_UNSELECTED, ComponentState.DISABLED_SELECTED);

		defaultSchemeBundle.registerColorScheme(highlightScheme, ComponentState.ROLLOVER_SELECTED);
		defaultSchemeBundle.registerColorScheme(selectedScheme, ComponentState.SELECTED);

		SubstanceColorScheme tabHighlightScheme = schemes.get("Zaros Tab Highlight");
		defaultSchemeBundle.registerColorScheme(tabHighlightScheme, ColorSchemeAssociationKind.TAB, ComponentState.ROLLOVER_SELECTED);

		this.registerDecorationAreaSchemeBundle(defaultSchemeBundle, backgroundScheme, DecorationAreaType.NONE);
		setTabFadeStart(0.18);
		setTabFadeEnd(0.18);
		registerAsDecorationArea(backgroundScheme, DecorationAreaType.PRIMARY_TITLE_PANE, DecorationAreaType.SECONDARY_TITLE_PANE, DecorationAreaType.HEADER);

		BottomLineOverlayPainter bottomLineOverlayPainter = new BottomLineOverlayPainter(ColorSchemeSingleColorQuery.DARK);
		addOverlayPainter(bottomLineOverlayPainter, DecorationAreaType.PRIMARY_TITLE_PANE, DecorationAreaType.SECONDARY_TITLE_PANE, DecorationAreaType.HEADER);
		buttonShaper = new ClassicButtonShaper();
		fillPainter = SimplisticFillPainter.INSTANCE;
		borderPainter = new CompositeBorderPainter("Zaros", new DelegateBorderPainter("Zaros Outer", new ClassicBorderPainter(), (SubstanceColorScheme scheme) -> scheme.shade(0.1f)), new DelegateBorderPainter("Zaros Inner", new ClassicBorderPainter(), (SubstanceColorScheme scheme) -> scheme.tint(0.8f)));
		highlightPainter = new ClassicHighlightPainter();
		MarbleNoiseDecorationPainter decorationPainter = new MarbleNoiseDecorationPainter();
		decorationPainter.setTextureAlpha(0.7f);
		this.decorationPainter = decorationPainter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.pushingpixels.substance.api.trait.SubstanceTrait#getDisplayName()
	 */
	@Override
	public String getDisplayName() {
		return "Zaros";
	}
}
