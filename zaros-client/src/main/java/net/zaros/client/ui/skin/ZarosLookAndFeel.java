package net.zaros.client.ui.skin;

import org.pushingpixels.substance.api.SubstanceLookAndFeel;

/**
 * Represents Zaros look and feel.
 * 
 * @author Walied K. Yassen
 */
public final class ZarosLookAndFeel extends SubstanceLookAndFeel {

	/**
	 * The serialisation key of the {@link ZarosLookAndFeel} type.
	 */
	private static final long serialVersionUID = 1600312882829053078L;

	/**
	 * Constructs a new {@link ZarosLookAndFeel} type object instance.
	 */
	public ZarosLookAndFeel() {
		super(new ZarosSkin());
	}
}
