package net.zaros.client.ui;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import net.zaros.client.GameClient;
import net.zaros.client.util.Resources;

/**
 * Represents the main client frame.
 * 
 * @author Walied K. Yassen
 */
public final class GameFrame extends JFrame {

	/**
	 * The serialisation key of the {@link GameFrame} class type.
	 */
	private static final long serialVersionUID = 3944274013284868677L;

	/**
	 * The client applet.
	 */
	private final Applet applet;

	/**
	 * The game client instance.
	 */
	private final GameClient client;

	/**
	 * Constructs a new {@link GameFrame} type object instance.
	 * 
	 * @param applet the client applet.
	 * @param client the game client instance.
	 */
	public GameFrame(Applet applet, GameClient client) {
		super("Zaros - Beta");
		this.applet = applet;
		this.client = client;
		initialise();
	}

	/**
	 * Initialises the frame.
	 */
	private void initialise() {
		initialise_properties();
		initialise_layout();
	}

	/**
	 * Initialises the frame properties.
	 */
	private void initialise_properties() {
		setSize(800, 600);
		setResizable(true);
		setLocationRelativeTo(null);
		setIconImages(getFavicons());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	/**
	 * Initialises the client layout.
	 */
	private void initialise_layout() {
		add(applet, BorderLayout.CENTER);
	}

	/**
	 * Starts the frame.
	 */
	public void start() {
		setVisible(true);
	}
	
	
	private final List<Image> getFavicons() {
		List<Image> images = new ArrayList<Image>();
		images.add(Resources.getImage("favicon-128.png"));
		images.add(Resources.getImage("favicon-64.png"));
		images.add(Resources.getImage("favicon-32.png"));
		return images;
	}
}
