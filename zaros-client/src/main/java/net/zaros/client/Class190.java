package net.zaros.client;

/* Class190 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class190 {
	float aFloat1930;
	int anInt1931;
	int anInt1932;
	float aFloat1933 = 1.0F;
	static int[] anIntArray1934 = new int[2];
	Class262 aClass262_1935;
	static int anInt1936;
	int anInt1937;
	float aFloat1938;
	float aFloat1939;
	int anInt1940;
	float aFloat1941 = 0.25F;
	Class241 aClass241_1942;
	static boolean aBoolean1943 = false;
	int anInt1944;
	float aFloat1945;
	int anInt1946;

	final boolean method1916(int i, Class190 class190_0_) {
		if (i != 64)
			method1917(61, null);
		if (anInt1932 != class190_0_.anInt1932 || class190_0_.aFloat1945 != aFloat1945 || class190_0_.aFloat1938 != aFloat1938 || class190_0_.aFloat1939 != aFloat1939 || class190_0_.aFloat1941 != aFloat1941 || aFloat1930 != class190_0_.aFloat1930 || class190_0_.aFloat1933 != aFloat1933 || anInt1946 != class190_0_.anInt1946 || anInt1931 != class190_0_.anInt1931 || class190_0_.aClass241_1942 != aClass241_1942 || aClass262_1935 != class190_0_.aClass262_1935)
			return false;
		return true;
	}

	final void method1917(int i, Packet class296_sub17) {
		int i_1_ = class296_sub17.g2();
		int i_2_ = class296_sub17.g2b();
		if (i != 8)
			method1916(-52, null);
		int i_3_ = class296_sub17.g2b();
		int i_4_ = class296_sub17.g2b();
		int i_5_ = class296_sub17.g2();
		Class217.anInt2120 = i_5_;
		aClass262_1935 = Class360_Sub10.method3752(i_1_, i_4_, -107, i_3_, i_2_);
	}

	public static void method1918(int i) {
		anIntArray1934 = null;
		if (i != -16281)
			method1918(-106);
	}

	final void method1919(int i, Packet class296_sub17) {
		aFloat1930 = (float) (class296_sub17.g1() * 8) / 255.0F;
		if (i == -1) {
			aFloat1941 = (float) (class296_sub17.g1() * 8) / 255.0F;
			aFloat1933 = (float) (class296_sub17.g1() * 8) / 255.0F;
		}
	}

	final void method1920(Packet class296_sub17, byte i) {
		if (i > -37)
			method1916(-29, null);
		int i_6_ = class296_sub17.g1();
		if (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub7_5004.method420(121) == 1 && Class368_Sub16.aHa5527.q() > 0) {
			if ((i_6_ & 0x1) != 0)
				anInt1932 = class296_sub17.g4();
			else
				anInt1932 = ItemsNode.anInt4593;
			if ((i_6_ & 0x2) == 0)
				aFloat1945 = 1.1523438F;
			else
				aFloat1945 = (float) class296_sub17.g2() / 256.0F;
			if ((i_6_ & 0x4) == 0)
				aFloat1938 = 0.69921875F;
			else
				aFloat1938 = (float) class296_sub17.g2() / 256.0F;
			if ((i_6_ & 0x8) != 0)
				aFloat1939 = (float) class296_sub17.g2() / 256.0F;
			else
				aFloat1939 = 1.2F;
		} else {
			if ((i_6_ & 0x1) != 0)
				class296_sub17.g4();
			if ((i_6_ & 0x2) != 0)
				class296_sub17.g2();
			if ((i_6_ & 0x4) != 0)
				class296_sub17.g2();
			if ((i_6_ & 0x8) != 0)
				class296_sub17.g2();
			aFloat1939 = 1.2F;
			anInt1932 = ItemsNode.anInt4593;
			aFloat1945 = 1.1523438F;
			aFloat1938 = 0.69921875F;
		}
		if ((i_6_ & 0x10) != 0) {
			anInt1944 = class296_sub17.g2b();
			anInt1937 = class296_sub17.g2b();
			anInt1940 = class296_sub17.g2b();
		} else {
			anInt1940 = -50;
			anInt1937 = -60;
			anInt1944 = -50;
		}
		if ((i_6_ & 0x20) == 0)
			anInt1946 = ByteStream.anInt6042;
		else
			anInt1946 = class296_sub17.g4();
		if ((i_6_ & 0x40) != 0)
			anInt1931 = class296_sub17.g2();
		else
			anInt1931 = 0;
		if ((i_6_ & 0x80) != 0) {
			int i_7_ = class296_sub17.g2();
			int i_8_ = class296_sub17.g2();
			int i_9_ = class296_sub17.g2();
			int i_10_ = class296_sub17.g2();
			int i_11_ = class296_sub17.g2();
			int i_12_ = class296_sub17.g2();
			aClass241_1942 = StaticMethods.method2121(i_12_, i_9_, i_11_, 60, i_7_, i_8_, i_10_);
		} else
			aClass241_1942 = Class368.aClass241_3131;
	}

	public Class190() {
		aFloat1930 = 1.0F;
		anInt1944 = -50;
		aClass241_1942 = Class368.aClass241_3131;
		aFloat1938 = 0.69921875F;
		anInt1931 = 0;
		aFloat1945 = 1.1523438F;
		aClass262_1935 = HardReferenceWrapper.aClass262_6700;
		aFloat1939 = 1.2F;
		anInt1937 = -60;
		anInt1940 = -50;
		anInt1932 = ItemsNode.anInt4593;
		anInt1946 = ByteStream.anInt6042;
	}

	Class190(Packet class296_sub17) {
		aFloat1930 = 1.0F;
		method1920(class296_sub17, (byte) -123);
	}
}
