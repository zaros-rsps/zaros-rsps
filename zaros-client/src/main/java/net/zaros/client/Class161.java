package net.zaros.client;

/* Class161 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class161 {
	int anInt1671;
	static float aFloat1672;
	int anInt1673;
	int anInt1674;
	int anInt1675;
	static Class161 aClass161_3193 = new Class161(11, 0, 1, 2);
	static NodeDeque aClass155_1676 = new NodeDeque();

	static final void method1620(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_) {
		if (i_0_ < ConfigurationDefinition.anInt676 || Class288.anInt2652 < i_1_ || i_6_ < EmissiveTriangle.anInt952 || RuntimeException_Sub1.anInt3391 < i)
			Class355_Sub2.method3706(i_1_, i, i_3_, i_0_, i_5_, i_6_, i_4_, -111);
		else
			Class41_Sub26.method502(i_3_, i_6_, -1, i_0_, i_5_, i, i_1_, i_4_);
		if (i_2_ >= -117)
			aClass155_1676 = null;
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	static final Class161 method2463(byte i, int i_1_) {
		int i_2_ = -38 % ((-44 - i) / 59);
		Class161[] class161s = Class41_Sub14.method450((byte) -108);
		for (Class161 class161 : class161s) {
			if (i_1_ == class161.anInt1675) {
				return class161;
			}
		}
		return null;
	}

	static final boolean method1621(int i, int i_7_, int i_8_) {
		if (i_7_ != 1884)
			return false;
		if (!StaticMethods.method2477(i_7_ - 1874, i_8_, i) && !Class16_Sub1_Sub1.method239(i_7_ - 1884, i, i_8_))
			return false;
		return true;
	}

	public static void method1622(byte i) {
		aClass155_1676 = null;
		if (i >= -46)
			method1621(-125, -24, 40);
	}

	Class161(int i, int i_9_, int i_10_, int i_11_) {
		anInt1675 = i;
		anInt1673 = i_11_;
		anInt1671 = i_10_;
		anInt1674 = i_9_;
	}
}
