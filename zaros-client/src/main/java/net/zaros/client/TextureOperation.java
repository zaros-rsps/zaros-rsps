package net.zaros.client;

/* Class296_Sub51 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class TextureOperation extends Node {
	int anInt5032;
	boolean monochromatic;
	Class86 aClass86_5034;
	Class318 aClass318_5035;
	TextureOperation[] childs;
	public static long[] someHashTable = new long[256];
	static OutgoingPacket aClass311_5038;
	static int[] anIntArray5039;
	static Js5 aClass138_5040;

	int method3062(byte i) {
		if (i != 2) {
			childs = null;
		}
		return -1;
	}

	public static void method3063(int i) {
		aClass311_5038 = null;
		anIntArray5039 = null;
		aClass138_5040 = null;
		if (i == -2) {
			someHashTable = null;
		}
	}

	final int[] method3064(int i, int i_0_, int i_1_) {
		if (i_0_ != 0) {
			return null;
		}
		if (childs[i].monochromatic) {
			return childs[i].get_monochrome_output(0, i_1_);
		}
		return childs[i].get_colour_output(i_1_, 17621)[0];
	}

	int[][] get_colour_output(int i, int i_2_) {
		throw new IllegalStateException("This operation does not have a colour output");
	}

	static final String method3066(int i, Class296_Sub39_Sub1 class296_sub39_sub1) {
		if (i != -4) {
			return null;
		}
		return class296_sub39_sub1.aString6116 + " <col=ffffff>>";
	}

	int method3067(byte i) {
		return -1;
	}

	static final void method3068(int i, int i_3_, int i_4_) {
		if (i_4_ != -17902) {
			method3068(-29, 35, 69);
		}
		if (Class399.anInt3357 == 1) {
			Class97.method873(i, i_3_, 121, Class296_Sub17_Sub2.aClass296_Sub39_Sub9_6046);
		} else if (Class399.anInt3357 == 2) {
			if (!Class368_Sub5_Sub2.aBoolean6597) {
				ConfigsRegister.method588(22, i, i_3_);
			} else {
				ConfigsRegister.method588(22, GraphicsLoader.method2286(true) + i, Class387.method4034(true) + i_3_);
			}
		}
		Class296_Sub17_Sub2.aClass296_Sub39_Sub9_6046 = null;
		Class399.anInt3357 = 0;
	}

	static final void method3069(int i) {
		Class250.aClass15_2360.method227(64);
		Class262.aClass117_2449.method1017(-95);
		Class296_Sub51_Sub13.aClass24_6420.clearCachedClothes();
		Class379.objectDefinitionLoader.clearCached();
		Class352.npcDefinitionLoader.method1414((byte) -128);
		Class296_Sub39_Sub1.itemDefinitionLoader.uncacheAll();
		Class296_Sub51_Sub13.animationsLoader.clearCachedAnimations();
		Class157.graphicsLoader.clearCached(22896);
		Class296_Sub43.bitConfigsLoader.clear(72);
		ConfigurationsLoader.configsLoader.clearCachedConfigurations((byte) -46);
		StaticMethods.aClass328_6070.method3393((byte) -118);
		HashTable.aClass149_2456.method1517((byte) 115);
		Class41_Sub10.aClass62_3768.method698(0);
		ConfigurationsLoader.aClass401_86.method4138(9);
		Class31.aClass245_324.method2182(2);
		Class296_Sub51_Sub38.aClass405_6542.method4172((byte) -99);
		Class296_Sub22.itemExtraDataDefinitionLoader.method1659(16421);
		InvisiblePlayer.aClass279_1977.method2339((byte) -111);
		Class49.aClass182_457.method1847(true);
		Class386.aClass335_3268.method3426(-101);
		Class355.aClass394_3067.method4061((byte) 79);
		Class338_Sub9.aClass76_5266.method783(26);
		ParamType.aClass164_3248.method1630(true);
		if (i > -106) {
			aClass311_5038 = null;
		}
		NPC.method3532(26852);
		Class32.method340(19055);
		StaticMethods.method1708(true);
		Class55.a(8364);
		if (BITConfigsLoader.liveModeWhere != Class41_Sub29.modeWhere) {
			for (int i_5_ = 0; i_5_ < Class367.aByteArrayArray3124.length; i_5_++) {
				Class367.aByteArrayArray3124[i_5_] = null;
			}
			ISAACCipher.anInt1898 = 0;
		}
		StaticMethods.method2475((byte) 12);
		Class303.method3269((byte) 105);
		Class296_Sub39_Sub14.method2873(122);
		Class304.method3270(false);
		Class397_Sub2.method4106(1);
		CS2Executor.aClass113_1531.clear();
		Class41_Sub13.aHa3774.e();
		Class137.method1424(0);
		Class108.method950(-32060);
		Class381.fs0.clearEntryBuffers((byte) -126);
		Class270.fs1.clearEntryBuffers((byte) -126);
		Class296_Sub39_Sub1.fs2.clearEntryBuffers((byte) -126);
		Class360_Sub6.fs3.clearEntryBuffers((byte) -126);
		Class93.fs4.clearEntryBuffers((byte) -126);
		Class324.fs5.clearEntryBuffers((byte) -126);
		Class42_Sub4.fs6.clearEntryBuffers((byte) -126);
		Class184.fs7.clearEntryBuffers((byte) -126);
		Class205_Sub2.fs8.clearEntryBuffers((byte) -126);
		Class205_Sub1.aClass138_5641.clearEntryBuffers((byte) -126);
		Class296_Sub51_Sub29.aClass138_6494.clearEntryBuffers((byte) -126);
		Class296_Sub22.aClass138_4722.clearEntryBuffers((byte) -126);
		Class72.cs2FS.clearEntryBuffers((byte) -126);
		LookupTable.aClass138_53.clearEntryBuffers((byte) -126);
		Class296_Sub15.aClass138_4671.clearEntryBuffers((byte) -126);
		Class33.aClass138_329.clearEntryBuffers((byte) -126);
		Class326.fs16.clearEntryBuffers((byte) -126);
		Class296_Sub30.aClass138_4822.clearEntryBuffers((byte) -126);
		Class199.aClass138_2005.clearEntryBuffers((byte) -126);
		Class63.aClass138_726.clearEntryBuffers((byte) -126);
		Class196.fs20.clearEntryBuffers((byte) -126);
		Class365.gfxFS.clearEntryBuffers((byte) -126);
		Class296_Sub51_Sub17.aClass138_6429.clearEntryBuffers((byte) -126);
		Class205_Sub2.aClass138_5631.clearEntryBuffers((byte) -126);
		Class296_Sub51_Sub34.aClass138_6523.clearEntryBuffers((byte) -126);
		Class296_Sub13.aClass138_4653.clearEntryBuffers((byte) -126);
		StaticMethods.aClass138_5927.clearEntryBuffers((byte) -126);
		Class324.aClass138_2862.clearEntryBuffers((byte) -126);
		Class296_Sub51_Sub21.fs28.clearEntryBuffers((byte) -126);
		Class134.fs35.clearEntryBuffers((byte) -126);
		Class338_Sub2.aClass138_5201.clearEntryBuffers((byte) -126);
		Class211.aClass138_2099.clearEntryBuffers((byte) -126);
		Class122_Sub1_Sub1.aClass138_6604.clearEntryBuffers((byte) -126);
		Class77.aClass138_879.clearEntryBuffers((byte) -126);
		Class121.aClass113_1269.clear();
		Class296_Sub15_Sub1.aClass113_5994.clear();
		Class264.aClass113_2471.clear();
		Class366_Sub6.aClass113_5390.clear();
	}

	void method3070(int i, int i_6_, int i_7_) {
		int i_8_ = i != (anInt5032 ^ 0xffffffff) ? anInt5032 : i_7_;
		if (monochromatic) {
			aClass318_5035 = new Class318(i_8_, i_7_, i_6_);
		} else {
			aClass86_5034 = new Class86(i_8_, i_7_, i_6_);
		}
	}

	void method3071(int i, Packet class296_sub17, int i_9_) {
		if (i >= -84) {
			/* empty */
		}
	}

	int[] get_monochrome_output(int i, int i_10_) {
		throw new IllegalStateException("This operation does not have a monochrome output");
	}

	void method3073(int i) {
		if (!monochromatic) {
			aClass86_5034.method827(111);
			aClass86_5034 = null;
		} else {
			aClass318_5035.method3334(107);
			aClass318_5035 = null;
		}
		int i_11_ = -119 / ((i + 58) / 39);
	}

	final int[][] method3075(byte i, int i_24_, int i_25_) {
		int i_26_ = -58 / ((i - 68) / 41);
		if (childs[i_24_].monochromatic) {
			int[] is = childs[i_24_].get_monochrome_output(0, i_25_);
			int[][] is_27_ = new int[3][];
			is_27_[1] = is;
			is_27_[0] = is;
			is_27_[2] = is;
			return is_27_;
		}
		return childs[i_24_].get_colour_output(i_25_, 17621);
	}

	void method3076(byte i) {
		int i_28_ = 19 % ((i + 58) / 40);
	}

	TextureOperation(int i, boolean bool) {
		monochromatic = bool;
		childs = new TextureOperation[i];
	}

	static {
		for (int i = 0; i < 256; i++) {
			long l = i;
			for (int i_29_ = 0; i_29_ < 8; i_29_++) {
				if ((l & 0x1L) != 1L) {
					l >>>= 1;
				} else {
					l = l >>> 1 ^ ~0x3693a86a2878f0bdL;
				}
			}
			someHashTable[i] = l;
		}
		aClass311_5038 = new OutgoingPacket(63, 4);
	}
}
