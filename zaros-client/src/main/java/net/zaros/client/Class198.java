package net.zaros.client;

/* Class198 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class198 {
	static IncomingPacket SYNTH_SOUND;
	static int currentMapSizeX = 104;
	static int anInt2001;
	static Class228[] aClass228Array2002 = new Class228[100];
	static byte[][][] aByteArrayArrayArray2003;

	public static void method1944(byte i) {
		if (i != 109)
			SYNTH_SOUND = null;
		aByteArrayArrayArray2003 = null;
		SYNTH_SOUND = null;
		aClass228Array2002 = null;
	}

	static final void method1945(int i, int i_0_) {
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_0_, i);
		class296_sub39_sub5.insertIntoQueue();
	}

	static {
		SYNTH_SOUND = new IncomingPacket(72, 8);
	}
}
