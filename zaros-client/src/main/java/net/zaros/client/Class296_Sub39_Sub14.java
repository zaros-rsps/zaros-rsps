package net.zaros.client;

/* Class296_Sub39_Sub14 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub14 extends Queuable {
	boolean aBoolean6210;
	int anInt6211 = -1;
	int anInt6212;
	String aString6213;
	int anInt6214;
	int anInt6215;
	String aString6216;
	int anInt6217;
	static Class294 aClass294_6218 = new Class294(8, 1);
	int anInt6219;
	NodeDeque aClass155_6220;
	int anInt6221;
	int anInt6222;

	final void method2872(int i) {
		if (i != 6275)
			method2877(86, null, 63, (byte) 41, -22);
		anInt6217 = 12800;
		anInt6219 = 0;
		anInt6215 = 0;
		anInt6221 = 12800;
		for (Class296_Sub27 class296_sub27 = (Class296_Sub27) aClass155_6220.removeFirst((byte) 127); class296_sub27 != null; class296_sub27 = (Class296_Sub27) aClass155_6220.removeNext(1001)) {
			if (anInt6221 > class296_sub27.anInt4773)
				anInt6221 = class296_sub27.anInt4773;
			if (anInt6219 < class296_sub27.anInt4774)
				anInt6219 = class296_sub27.anInt4774;
			if (anInt6217 > class296_sub27.anInt4775)
				anInt6217 = class296_sub27.anInt4775;
			if (anInt6215 < class296_sub27.anInt4781)
				anInt6215 = class296_sub27.anInt4781;
		}
	}

	static final void method2873(int i) {
		synchronized (Class280.aClass113_2555) {
			Class280.aClass113_2555.clear();
		}
		if (i < 5)
			aClass294_6218 = null;
	}

	final boolean method2874(int i, int i_0_, int i_1_, int[] is) {
		for (Class296_Sub27 class296_sub27 = (Class296_Sub27) aClass155_6220.removeFirst((byte) 117); class296_sub27 != null; class296_sub27 = (Class296_Sub27) aClass155_6220.removeNext(i_1_ ^ 0x699c)) {
			if (class296_sub27.method2678(i_0_, (byte) -127, i)) {
				class296_sub27.method2679(i, (byte) 11, is, i_0_);
				return true;
			}
		}
		if (i_1_ != 27253)
			method2880(106);
		return false;
	}

	final boolean method2875(int[] is, int i, byte i_2_, int i_3_) {
		if (i_2_ <= 81)
			method2878((byte) -42);
		for (Class296_Sub27 class296_sub27 = (Class296_Sub27) aClass155_6220.removeFirst((byte) 119); class296_sub27 != null; class296_sub27 = (Class296_Sub27) aClass155_6220.removeNext(1001)) {
			if (class296_sub27.method2673(i, i_3_, 23)) {
				class296_sub27.method2676(is, i_3_, 1, i);
				return true;
			}
		}
		return false;
	}

	static final boolean method2876(byte i, Interface14 interface14) {
		ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(interface14.method57((byte) 113));
		if (class70.anInt765 == -1)
			return true;
		if (i != -86)
			aClass294_6218 = null;
		Class28 class28 = ConfigurationsLoader.aClass401_86.method4143(class70.anInt765, 24180);
		if (class28.anInt300 == -1)
			return true;
		return class28.method316((byte) -21);
	}

	final boolean method2877(int i, int[] is, int i_4_, byte i_5_, int i_6_) {
		for (Class296_Sub27 class296_sub27 = (Class296_Sub27) aClass155_6220.removeFirst((byte) 126); class296_sub27 != null; class296_sub27 = (Class296_Sub27) aClass155_6220.removeNext(1001)) {
			if (class296_sub27.method2680(i, false, i_6_, i_4_)) {
				class296_sub27.method2679(i, (byte) 11, is, i_4_);
				return true;
			}
		}
		if (i_5_ != -57)
			aClass155_6220 = null;
		return false;
	}

	static final int method2878(byte i) {
		if (i < 36)
			method2880(-64);
		return ConfigsRegister.anInt3671;
	}

	final boolean method2879(int i, int i_7_, int i_8_) {
		if (i_8_ != -4)
			aBoolean6210 = false;
		for (Class296_Sub27 class296_sub27 = (Class296_Sub27) aClass155_6220.removeFirst((byte) 127); class296_sub27 != null; class296_sub27 = (Class296_Sub27) aClass155_6220.removeNext(i_8_ ^ ~0x3ea)) {
			if (class296_sub27.method2678(i_7_, (byte) -127, i))
				return true;
		}
		return false;
	}

	public static void method2880(int i) {
		if (i >= 22)
			aClass294_6218 = null;
	}

	Class296_Sub39_Sub14(int i, String string, String string_9_, int i_10_, int i_11_, boolean bool, int i_12_, int i_13_) {
		aBoolean6210 = true;
		anInt6215 = 0;
		anInt6212 = -1;
		anInt6217 = 12800;
		anInt6219 = 0;
		anInt6221 = 12800;
		anInt6212 = i_12_;
		anInt6214 = i;
		aString6213 = string_9_;
		anInt6211 = i_11_;
		aBoolean6210 = bool;
		aString6216 = string;
		anInt6222 = i_10_;
		if (anInt6212 == 255)
			anInt6212 = 0;
		aClass155_6220 = new NodeDeque();
	}
}
