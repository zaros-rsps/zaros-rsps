package net.zaros.client;

/* Class296_Sub30 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub30 extends Node {
	static int anInt4816;
	String aString4817;
	static OutgoingPacket aClass311_4818;
	int[] anIntArray4819;
	static int anInt4820 = 1;
	static int[] anIntArray4821 = new int[1];
	static Js5 aClass138_4822;

	static final void method2700(boolean bool, int i, String string, byte i_0_) {
		Class368_Sub9.method3839(2);
		if (i == 0) {
			Class41_Sub13.aHa3774 = Animator.method562(Class122_Sub1_Sub1.aClass138_6604, Class230.aCanvas2209, 117, 0, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5015.method475(125) * 2), Class316.aD2803);
			if (string != null) {
				Class41_Sub13.aHa3774.GA(0);
				Class92 class92 = Class92.method2821(Mesh.anInt1364, 0, -112, LookupTable.aClass138_53);
				Class55 class55 = (Class41_Sub13.aHa3774.a(class92, Class186.method1871(Class205_Sub2.fs8, Mesh.anInt1364, 0), true));
				Class367.method3802(-1);
				ModeWhat.method1021(class92, i_0_ + 8414, string, Class41_Sub13.aHa3774, true, class55);
			}
		} else {
			ha var_ha = null;
			if (string != null) {
				var_ha = Animator.method562(Class122_Sub1_Sub1.aClass138_6604, Class230.aCanvas2209, 84, 0, 0, Class316.aD2803);
				var_ha.GA(0);
				Class92 class92 = Class92.method2821(Mesh.anInt1364, 0, 23, LookupTable.aClass138_53);
				Class55 class55 = var_ha.a(class92, Class186.method1871((Class205_Sub2.fs8), Mesh.anInt1364, 0), true);
				Class367.method3802(-1);
				ModeWhat.method1021(class92, 8493, string, var_ha, true, class55);
			}
			try {
				Class41_Sub13.aHa3774 = Animator.method562(Class122_Sub1_Sub1.aClass138_6604, Class230.aCanvas2209, 84, i, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub21_5015.method475(126) * 2, Class316.aD2803);
				if (string != null) {
					var_ha.GA(0);
					Class92 class92 = Class92.method2821(Mesh.anInt1364, 0, -28, LookupTable.aClass138_53);
					Class55 class55 = var_ha.a(class92, Class186.method1871((Class205_Sub2.fs8), Mesh.anInt1364, 0), true);
					Class367.method3802(-1);
					ModeWhat.method1021(class92, i_0_ ^ 0x2162, string, var_ha, true, class55);
				}
				if (Class41_Sub13.aHa3774.a()) {
					boolean bool_1_ = true;
					try {
						bool_1_ = (Class360_Sub3.aClass296_Sub28_5309.anInt4797 > 256);
					} catch (Throwable throwable) {
						/* empty */
					}
					za var_za;
					if (bool_1_)
						var_za = Class41_Sub13.aHa3774.b(146800640);
					else
						var_za = Class41_Sub13.aHa3774.b(104857600);
					Class41_Sub13.aHa3774.a(var_za);
				}
			} catch (Throwable throwable) {
				int i_2_ = Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method521(125);
				if (i_2_ == 2)
					Class296_Sub51_Sub30.aBoolean6498 = true;
				Class343_Sub1.aClass296_Sub50_5282.method3060(0, 82, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006);
				method2700(bool, i_2_, string, (byte) 79);
				return;
			} finally {
				if (var_ha != null) {
					try {
						var_ha.method1091((byte) -78);
					} catch (Throwable throwable) {
						/* empty */
					}
				}
			}
		}
		Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006.method526(!bool, (byte) 125);
		Class343_Sub1.aClass296_Sub50_5282.method3060(i, i_0_ ^ 0x3c, (Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub30_5006));
		Class108.method950(-32060);
		Class41_Sub13.aHa3774.h(10000);
		Class41_Sub13.aHa3774.X(32);
		Class223.aClass373_2162 = Class41_Sub13.aHa3774.m();
		Class253.aClass373_2388 = Class41_Sub13.aHa3774.m();
		Graphic.method292((byte) 94);
		Class41_Sub13.aHa3774.b(Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub3_4993.method400(127) == 1);
		if (Class41_Sub13.aHa3774.n())
			AdvancedMemoryCache.method984(i_0_ - 190, Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub27_5010.method505(126) == 1);
		Class340.method3621(i_0_ - 85, Class198.currentMapSizeX >> 3, Class41_Sub13.aHa3774, Class296_Sub38.currentMapSizeY >> 3);
		Class368_Sub22.method3866(11);
		Class348.aBoolean3033 = false;
		Class177.aClass224Array1847 = null;
		Class380.aBoolean3210 = true;
		if (i_0_ != 79)
			method2701(35);
		Class41_Sub29.method520(107);
	}

	public static void method2701(int i) {
		aClass138_4822 = null;
		anIntArray4821 = null;
		aClass311_4818 = null;
		if (i != 12558)
			aClass138_4822 = null;
	}

	Class296_Sub30(String string, int i) {
		anIntArray4819 = new int[i];
		aString4817 = string;
	}

	static {
		aClass311_4818 = new OutgoingPacket(69, 3);
	}
}
