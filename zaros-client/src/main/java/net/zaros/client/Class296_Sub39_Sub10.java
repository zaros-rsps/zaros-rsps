package net.zaros.client;

/* Class296_Sub39_Sub10 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub39_Sub10 extends Queuable {
	static int anInt6176;
	static boolean aBoolean6177 = false;
	static byte aByte6178;
	static Class368[] aClass368Array6179;
	static int[] anIntArray6180 = new int[4096];
	Class292 aClass292_6181;
	private int[][] anIntArrayArray6182;
	boolean aBoolean6183 = true;
	private String[] aStringArray6184;
	private int[] anIntArray6185;
	int[] anIntArray6186;

	final int method2832(int i, int i_0_, boolean bool) {
		if (bool)
			anIntArray6180 = null;
		if (anIntArray6185 == null || i_0_ < 0 || i_0_ > anIntArray6185.length)
			return -1;
		if (anIntArrayArray6182[i_0_] == null || i < 0 || i > anIntArrayArray6182[i_0_].length)
			return -1;
		return anIntArrayArray6182[i_0_][i];
	}

	final int method2833(int i) {
		if (anIntArray6185 == null)
			return 0;
		if (i != -22579)
			anIntArrayArray6182 = null;
		return anIntArray6185.length;
	}

	final String method2834(int i) {
		StringBuffer stringbuffer = new StringBuffer(80);
		if (aStringArray6184 == null)
			return "";
		stringbuffer.append(aStringArray6184[i]);
		for (int i_1_ = 1; aStringArray6184.length > i_1_; i_1_++) {
			stringbuffer.append("...");
			stringbuffer.append(aStringArray6184[i_1_]);
		}
		return stringbuffer.toString();
	}

	final void method2835(byte i, int[] is, Packet class296_sub17) {
		if (i != -109)
			aBoolean6183 = true;
		if (anIntArray6185 != null) {
			int i_2_ = 0;
			while_137_ : do {
				for (;;) {
					if (i_2_ >= anIntArray6185.length)
						break while_137_;
					if (is.length <= i_2_)
						break;
					int i_3_ = method2841(i_2_, (byte) -8).anInt1674;
					if (i_3_ > 0)
						class296_sub17.method2551(i_3_, (long) is[i_2_]);
					i_2_++;
				}
				break;
			} while (false);
		}
	}

	final void method2836(byte i) {
		if (anIntArray6186 != null) {
			for (int i_4_ = 0; anIntArray6186.length > i_4_; i_4_++)
				anIntArray6186[i_4_] = Class48.bitOR(anIntArray6186[i_4_], 32768);
		}
		int i_5_ = 60 % ((48 - i) / 50);
	}

	private final void method2837(Packet class296_sub17, int i, int i_6_) {
		if (i_6_ == 1)
			aStringArray6184 = Class41_Sub30.method522((byte) 63, class296_sub17.gstr(), '<');
		else if (i_6_ == 2) {
			int i_7_ = class296_sub17.g1();
			anIntArray6186 = new int[i_7_];
			for (int i_8_ = 0; i_8_ < i_7_; i_8_++)
				anIntArray6186[i_8_] = class296_sub17.g2();
		} else if (i_6_ == 3) {
			int i_9_ = class296_sub17.g1();
			anIntArray6185 = new int[i_9_];
			anIntArrayArray6182 = new int[i_9_][];
			for (int i_10_ = 0; i_9_ > i_10_; i_10_++) {
				int i_11_ = class296_sub17.g2();
				Class161 class161 = Class161.method2463((byte) 123, i_11_);
				if (class161 != null) {
					anIntArray6185[i_10_] = i_11_;
					anIntArrayArray6182[i_10_] = new int[class161.anInt1673];
					for (int i_12_ = 0; class161.anInt1673 > i_12_; i_12_++)
						anIntArrayArray6182[i_10_][i_12_] = class296_sub17.g2();
				}
			}
		} else if (i_6_ == 4)
			aBoolean6183 = false;
		int i_13_ = 70 / ((18 - i) / 55);
	}

	final void method2838(Packet class296_sub17, int i) {
		if (i == 80) {
			for (;;) {
				int i_14_ = class296_sub17.g1();
				if (i_14_ == 0)
					break;
				method2837(class296_sub17, 116, i_14_);
			}
		}
	}

	final String method2839(Packet class296_sub17, boolean bool) {
		if (bool)
			aBoolean6183 = false;
		StringBuffer stringbuffer = new StringBuffer(80);
		if (anIntArray6185 != null) {
			for (int i = 0; anIntArray6185.length > i; i++) {
				stringbuffer.append(aStringArray6184[i]);
				stringbuffer.append(aClass292_6181.method2410(-113, method2841(i, (byte) -8), anIntArrayArray6182[i], class296_sub17.method2564((Class161.method2463((byte) 58, anIntArray6185[i]).anInt1671))));
			}
		}
		stringbuffer.append(aStringArray6184[aStringArray6184.length - 1]);
		return stringbuffer.toString();
	}

	public static void method2840(int i) {
		if (i >= -18)
			aByte6178 = (byte) 113;
		aClass368Array6179 = null;
		anIntArray6180 = null;
	}

	final Class161 method2841(int i, byte i_15_) {
		if (i_15_ != -8)
			aBoolean6183 = false;
		if (anIntArray6185 == null || i < 0 || i > anIntArray6185.length)
			return null;
		return Class161.method2463((byte) -126, anIntArray6185[i]);
	}

	public Class296_Sub39_Sub10() {
		/* empty */
	}

	static {
		for (int i = 0; i < 4096; i++)
			anIntArray6180[i] = Class296_Sub39_Sub18.method2899(i, -1184463124);
	}
}
