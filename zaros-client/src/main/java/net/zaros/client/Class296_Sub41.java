package net.zaros.client;

/* Class296_Sub41 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub41 extends Node {
	byte[] aByteArray4915;
	byte[] aByteArray4916;
	Class296_Sub19_Sub1[] aClass296_Sub19_Sub1Array4917;
	short[] aShortArray4918;
	Class242[] aClass242Array4919;
	private int[] anIntArray4920;
	byte[] aByteArray4921;
	int anInt4922;

	final void method2914(int i) {
		if (i <= 9)
			method2916(null, false, null, null);
		anIntArray4920 = null;
	}

	static final void method2915(int i, boolean bool) {
		Class220.anInt2150 = i;
		Class187.anInt1910 = 2;
		Class377.loginConnection = Class296_Sub45_Sub2.aClass204_6277;
		String string = null;
		if (Class294.sessionKey != null) {
			Packet class296_sub17 = new Packet(Class294.sessionKey);
			string = Class296_Sub13.decodeBase37(class296_sub17.g8());
			Class238.aLong3633 = class296_sub17.g8();
		}
		if (string == null)
			BITConfigDefinition.method2352(-2, 35);
		else
			Class41_Sub27.method508(bool, string, "", true, (byte) -104);
	}

	final boolean method2916(Class25 class25, boolean bool, int[] is, byte[] is_0_) {
		if (bool)
			method2914(56);
		boolean bool_1_ = true;
		int i = 0;
		Class296_Sub19_Sub1 class296_sub19_sub1 = null;
		for (int i_2_ = 0; i_2_ < 128; i_2_++) {
			if (is_0_ == null || is_0_[i_2_] != 0) {
				int i_3_ = anIntArray4920[i_2_];
				if (i_3_ != 0) {
					if (i != i_3_) {
						i = i_3_;
						if ((--i_3_ & 0x1) == 0)
							class296_sub19_sub1 = class25.method309(96, i_3_ >> 2, is);
						else
							class296_sub19_sub1 = class25.method307(1, i_3_ >> 2, is);
						if (class296_sub19_sub1 == null)
							bool_1_ = false;
					}
					if (class296_sub19_sub1 != null) {
						aClass296_Sub19_Sub1Array4917[i_2_] = class296_sub19_sub1;
						anIntArray4920[i_2_] = 0;
					}
				}
			}
		}
		return bool_1_;
	}

	public Class296_Sub41() {
		/* empty */
	}

	Class296_Sub41(byte[] is) {
		aShortArray4918 = new short[128];
		aClass242Array4919 = new Class242[128];
		aByteArray4916 = new byte[128];
		anIntArray4920 = new int[128];
		aClass296_Sub19_Sub1Array4917 = new Class296_Sub19_Sub1[128];
		aByteArray4915 = new byte[128];
		aByteArray4921 = new byte[128];
		Packet class296_sub17 = new Packet(is);
		int i;
		for (i = 0; class296_sub17.data[class296_sub17.pos + i] != 0; i++) {
			/* empty */
		}
		byte[] is_4_ = new byte[i];
		for (int i_5_ = 0; i_5_ < i; i_5_++)
			is_4_[i_5_] = class296_sub17.g1b();
		class296_sub17.pos++;
		i++;
		int i_6_ = class296_sub17.pos;
		class296_sub17.pos += i;
		int i_7_;
		for (i_7_ = 0; (class296_sub17.data[i_7_ + class296_sub17.pos] != 0); i_7_++) {
			/* empty */
		}
		byte[] is_8_ = new byte[i_7_];
		for (int i_9_ = 0; i_7_ > i_9_; i_9_++)
			is_8_[i_9_] = class296_sub17.g1b();
		class296_sub17.pos++;
		i_7_++;
		int i_10_ = class296_sub17.pos;
		class296_sub17.pos += i_7_;
		int i_11_;
		for (i_11_ = 0; (class296_sub17.data[class296_sub17.pos + i_11_] != 0); i_11_++) {
			/* empty */
		}
		byte[] is_12_ = new byte[i_11_];
		for (int i_13_ = 0; i_13_ < i_11_; i_13_++)
			is_12_[i_13_] = class296_sub17.g1b();
		class296_sub17.pos++;
		byte[] is_14_ = new byte[++i_11_];
		int i_15_;
		if (i_11_ <= 1)
			i_15_ = i_11_;
		else {
			is_14_[1] = (byte) 1;
			int i_16_ = 1;
			i_15_ = 2;
			for (int i_17_ = 2; i_17_ < i_11_; i_17_++) {
				int i_18_ = class296_sub17.g1();
				if (i_18_ == 0)
					i_16_ = i_15_++;
				else {
					if (i_16_ >= i_18_)
						i_18_--;
					i_16_ = i_18_;
				}
				is_14_[i_17_] = (byte) i_16_;
			}
		}
		Class242[] class242s = new Class242[i_15_];
		for (int i_19_ = 0; class242s.length > i_19_; i_19_++) {
			Class242 class242 = class242s[i_19_] = new Class242();
			int i_20_ = class296_sub17.g1();
			if (i_20_ > 0)
				class242.aByteArray2306 = new byte[i_20_ * 2];
			i_20_ = class296_sub17.g1();
			if (i_20_ > 0) {
				class242.aByteArray2305 = new byte[i_20_ * 2 + 2];
				class242.aByteArray2305[1] = (byte) 64;
			}
		}
		int i_21_ = class296_sub17.g1();
		byte[] is_22_ = i_21_ <= 0 ? null : new byte[i_21_ * 2];
		i_21_ = class296_sub17.g1();
		byte[] is_23_ = i_21_ > 0 ? new byte[i_21_ * 2] : null;
		int i_24_;
		for (i_24_ = 0; (class296_sub17.data[i_24_ + class296_sub17.pos] != 0); i_24_++) {
			/* empty */
		}
		byte[] is_25_ = new byte[i_24_];
		for (int i_26_ = 0; i_24_ > i_26_; i_26_++)
			is_25_[i_26_] = class296_sub17.g1b();
		class296_sub17.pos++;
		i_24_++;
		int i_27_ = 0;
		for (int i_28_ = 0; i_28_ < 128; i_28_++) {
			i_27_ += class296_sub17.g1();
			aShortArray4918[i_28_] = (short) i_27_;
		}
		i_27_ = 0;
		for (int i_29_ = 0; i_29_ < 128; i_29_++) {
			i_27_ += class296_sub17.g1();
			aShortArray4918[i_29_] += i_27_ << 8;
		}
		int i_30_ = 0;
		int i_31_ = 0;
		int i_32_ = 0;
		for (int i_33_ = 0; i_33_ < 128; i_33_++) {
			if (i_30_ == 0) {
				if (i_31_ >= is_25_.length)
					i_30_ = -1;
				else
					i_30_ = is_25_[i_31_++];
				i_32_ = class296_sub17.gvarint();
			}
			aShortArray4918[i_33_] += (2 & i_32_ - 1) << 14;
			i_30_--;
			anIntArray4920[i_33_] = i_32_;
		}
		i_30_ = 0;
		i_31_ = 0;
		int i_34_ = 0;
		for (int i_35_ = 0; i_35_ < 128; i_35_++) {
			if (anIntArray4920[i_35_] != 0) {
				if (i_30_ == 0) {
					i_34_ = class296_sub17.data[i_6_++] - 1;
					if (is_4_.length <= i_31_)
						i_30_ = -1;
					else
						i_30_ = is_4_[i_31_++];
				}
				i_30_--;
				aByteArray4915[i_35_] = (byte) i_34_;
			}
		}
		i_30_ = 0;
		i_31_ = 0;
		int i_36_ = 0;
		for (int i_37_ = 0; i_37_ < 128; i_37_++) {
			if (anIntArray4920[i_37_] != 0) {
				if (i_30_ == 0) {
					i_36_ = class296_sub17.data[i_10_++] + 16 << 2;
					if (i_31_ >= is_8_.length)
						i_30_ = -1;
					else
						i_30_ = is_8_[i_31_++];
				}
				aByteArray4921[i_37_] = (byte) i_36_;
				i_30_--;
			}
		}
		i_30_ = 0;
		i_31_ = 0;
		Class242 class242 = null;
		for (int i_38_ = 0; i_38_ < 128; i_38_++) {
			if (anIntArray4920[i_38_] != 0) {
				if (i_30_ == 0) {
					class242 = class242s[is_14_[i_31_]];
					if (i_31_ >= is_12_.length)
						i_30_ = -1;
					else
						i_30_ = is_12_[i_31_++];
				}
				i_30_--;
				aClass242Array4919[i_38_] = class242;
			}
		}
		i_30_ = 0;
		i_31_ = 0;
		int i_39_ = 0;
		for (int i_40_ = 0; i_40_ < 128; i_40_++) {
			if (i_30_ == 0) {
				if (i_31_ >= is_25_.length)
					i_30_ = -1;
				else
					i_30_ = is_25_[i_31_++];
				if (anIntArray4920[i_40_] > 0)
					i_39_ = class296_sub17.g1() + 1;
			}
			aByteArray4916[i_40_] = (byte) i_39_;
			i_30_--;
		}
		anInt4922 = class296_sub17.g1() + 1;
		for (int i_41_ = 0; i_15_ > i_41_; i_41_++) {
			Class242 class242_42_ = class242s[i_41_];
			if (class242_42_.aByteArray2306 != null) {
				for (int i_43_ = 1; i_43_ < class242_42_.aByteArray2306.length; i_43_ += 2)
					class242_42_.aByteArray2306[i_43_] = class296_sub17.g1b();
			}
			if (class242_42_.aByteArray2305 != null) {
				for (int i_44_ = 3; class242_42_.aByteArray2305.length - 2 > i_44_; i_44_ += 2)
					class242_42_.aByteArray2305[i_44_] = class296_sub17.g1b();
			}
		}
		if (is_22_ != null) {
			for (int i_45_ = 1; is_22_.length > i_45_; i_45_ += 2)
				is_22_[i_45_] = class296_sub17.g1b();
		}
		if (is_23_ != null) {
			for (int i_46_ = 1; i_46_ < is_23_.length; i_46_ += 2)
				is_23_[i_46_] = class296_sub17.g1b();
		}
		for (int i_47_ = 0; i_15_ > i_47_; i_47_++) {
			Class242 class242_48_ = class242s[i_47_];
			if (class242_48_.aByteArray2305 != null) {
				i_27_ = 0;
				for (int i_49_ = 2; i_49_ < class242_48_.aByteArray2305.length; i_49_ += 2) {
					i_27_ = i_27_ + 1 + class296_sub17.g1();
					class242_48_.aByteArray2305[i_49_] = (byte) i_27_;
				}
			}
		}
		for (int i_50_ = 0; i_15_ > i_50_; i_50_++) {
			Class242 class242_51_ = class242s[i_50_];
			if (class242_51_.aByteArray2306 != null) {
				i_27_ = 0;
				for (int i_52_ = 2; class242_51_.aByteArray2306.length > i_52_; i_52_ += 2) {
					i_27_ = class296_sub17.g1() + (i_27_ + 1);
					class242_51_.aByteArray2306[i_52_] = (byte) i_27_;
				}
			}
		}
		if (is_22_ != null) {
			i_27_ = class296_sub17.g1();
			is_22_[0] = (byte) i_27_;
			for (int i_53_ = 2; i_53_ < is_22_.length; i_53_ += 2) {
				i_27_ = class296_sub17.g1() + 1 + i_27_;
				is_22_[i_53_] = (byte) i_27_;
			}
			int i_54_ = is_22_[0];
			int i_55_ = is_22_[1];
			for (int i_56_ = 0; i_56_ < i_54_; i_56_++)
				aByteArray4916[i_56_] = (byte) (i_55_ * aByteArray4916[i_56_] + 32 >> 6);
			for (int i_57_ = 2; is_22_.length > i_57_; i_57_ += 2) {
				int i_58_ = is_22_[i_57_];
				int i_59_ = is_22_[i_57_ + 1];
				int i_60_ = (-i_54_ + i_58_) / 2 + i_55_ * (i_58_ - i_54_);
				for (int i_61_ = i_54_; i_58_ > i_61_; i_61_++) {
					int i_62_ = Class182.method1852(i_58_ - i_54_, i_60_, -84);
					aByteArray4916[i_61_] = (byte) (i_62_ * aByteArray4916[i_61_] + 32 >> 6);
					i_60_ += i_59_ - i_55_;
				}
				i_54_ = i_58_;
				i_55_ = i_59_;
			}
			for (int i_63_ = i_54_; i_63_ < 128; i_63_++)
				aByteArray4916[i_63_] = (byte) (aByteArray4916[i_63_] * i_55_ + 32 >> 6);
			Object object = null;
		}
		if (is_23_ != null) {
			i_27_ = class296_sub17.g1();
			is_23_[0] = (byte) i_27_;
			for (int i_64_ = 2; is_23_.length > i_64_; i_64_ += 2) {
				i_27_ = class296_sub17.g1() + 1 + i_27_;
				is_23_[i_64_] = (byte) i_27_;
			}
			int i_65_ = is_23_[0];
			int i_66_ = is_23_[1] << 1;
			for (int i_67_ = 0; i_67_ < i_65_; i_67_++) {
				int i_68_ = (aByteArray4921[i_67_] & 0xff) + i_66_;
				if (i_68_ < 0)
					i_68_ = 0;
				if (i_68_ > 128)
					i_68_ = 128;
				aByteArray4921[i_67_] = (byte) i_68_;
			}
			int i_69_ = 2;
			while (i_69_ < is_23_.length) {
				int i_70_ = is_23_[i_69_];
				int i_71_ = is_23_[i_69_ + 1] << 1;
				int i_72_ = (i_70_ - i_65_) * i_66_ + (i_70_ - i_65_) / 2;
				for (int i_73_ = i_65_; i_73_ < i_70_; i_73_++) {
					int i_74_ = Class182.method1852(i_70_ - i_65_, i_72_, -68);
					int i_75_ = i_74_ + (aByteArray4921[i_73_] & 0xff);
					if (i_75_ < 0)
						i_75_ = 0;
					if (i_75_ > 128)
						i_75_ = 128;
					i_72_ += i_71_ - i_66_;
					aByteArray4921[i_73_] = (byte) i_75_;
				}
				i_69_ += 2;
				i_66_ = i_71_;
				i_65_ = i_70_;
			}
			Object object = null;
			for (int i_76_ = i_65_; i_76_ < 128; i_76_++) {
				int i_77_ = i_66_ + (aByteArray4921[i_76_] & 0xff);
				if (i_77_ < 0)
					i_77_ = 0;
				if (i_77_ > 128)
					i_77_ = 128;
				aByteArray4921[i_76_] = (byte) i_77_;
			}
		}
		for (int i_78_ = 0; i_78_ < i_15_; i_78_++)
			class242s[i_78_].anInt2307 = class296_sub17.g1();
		for (int i_79_ = 0; i_79_ < i_15_; i_79_++) {
			Class242 class242_80_ = class242s[i_79_];
			if (class242_80_.aByteArray2306 != null)
				class242_80_.anInt2310 = class296_sub17.g1();
			if (class242_80_.aByteArray2305 != null)
				class242_80_.anInt2313 = class296_sub17.g1();
			if (class242_80_.anInt2307 > 0)
				class242_80_.anInt2308 = class296_sub17.g1();
		}
		for (int i_81_ = 0; i_81_ < i_15_; i_81_++)
			class242s[i_81_].anInt2311 = class296_sub17.g1();
		for (int i_82_ = 0; i_15_ > i_82_; i_82_++) {
			Class242 class242_83_ = class242s[i_82_];
			if (class242_83_.anInt2311 > 0)
				class242_83_.anInt2314 = class296_sub17.g1();
		}
		for (int i_84_ = 0; i_15_ > i_84_; i_84_++) {
			Class242 class242_85_ = class242s[i_84_];
			if (class242_85_.anInt2314 > 0)
				class242_85_.anInt2312 = class296_sub17.g1();
		}
	}
}
