package net.zaros.client;

/* Class76 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class76 {
	private Js5 aClass138_871;
	static int[] anIntArray872 = new int[32];
	static int[] anIntArray873;
	private AdvancedMemoryCache aClass113_874 = new AdvancedMemoryCache(256);
	static Packet aClass296_Sub17_875;

	final void method783(int i) {
		if (i == 26) {
			synchronized (aClass113_874) {
				aClass113_874.clear();
			}
		}
	}

	static final String method784(String string, String string_0_, String string_1_, byte i) {
		int i_2_ = -126 % ((i - 5) / 48);
		for (int i_3_ = string.indexOf(string_1_); i_3_ != -1; i_3_ = string.indexOf(string_1_, i_3_ + string_0_.length()))
			string = (string.substring(0, i_3_) + string_0_ + string.substring(string_1_.length() + i_3_));
		return string;
	}

	final void method785(byte i) {
		synchronized (aClass113_874) {
			if (i != -79)
				aClass138_871 = null;
			aClass113_874.clearSoftReferences();
		}
	}

	final Class296_Sub39_Sub13 method786(byte i, int i_4_) {
		if (i < 84)
			method786((byte) 7, 127);
		Class296_Sub39_Sub13 class296_sub39_sub13;
		synchronized (aClass113_874) {
			class296_sub39_sub13 = (Class296_Sub39_Sub13) aClass113_874.get((long) i_4_);
		}
		if (class296_sub39_sub13 != null)
			return class296_sub39_sub13;
		byte[] is;
		synchronized (aClass138_871) {
			is = aClass138_871.getFile(26, i_4_);
		}
		class296_sub39_sub13 = new Class296_Sub39_Sub13();
		if (is != null)
			class296_sub39_sub13.method2857(new Packet(is), (byte) -51);
		synchronized (aClass113_874) {
			aClass113_874.put(class296_sub39_sub13, (long) i_4_);
		}
		return class296_sub39_sub13;
	}

	public static void method787(int i) {
		aClass296_Sub17_875 = null;
		anIntArray873 = null;
		if (i != 26)
			method787(-121);
		anIntArray872 = null;
	}

	final void method788(int i, boolean bool) {
		synchronized (aClass113_874) {
			if (bool)
				method786((byte) 83, 21);
			aClass113_874.clean(i);
		}
	}

	Class76(GameType class35, int i, Js5 class138) {
		aClass138_871 = class138;
		aClass138_871.getLastFileId(26);
	}
}
