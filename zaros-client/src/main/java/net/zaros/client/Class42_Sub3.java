package net.zaros.client;

/* Class42_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class42_Sub3 extends Class42 {
	private int anInt3840;
	private int anInt3841;
	private int anInt3842;
	private int anInt3843;
	static int anInt3844 = -1;
	static boolean aBoolean3845 = false;

	static final boolean method536(int i, int i_0_, int i_1_, Class338_Sub3_Sub4 class338_sub3_sub4, int i_2_) {
		if (!Class103.aBoolean1086 || !Class12.aBoolean133)
			return false;
		if (Class296_Sub39_Sub20.anInt6255 < 100)
			return false;
		if (!Class50.method614((byte) -117, i, i_1_, i_2_))
			return false;
		int i_3_ = i << Class313.anInt2779;
		if (i_0_ < 49)
			return true;
		int i_4_ = i_1_ << Class313.anInt2779;
		int i_5_ = (Class360_Sub2.aSArray5304[i_2_].method3355(i_1_, (byte) -112, i) - 1);
		int i_6_ = i_5_ + class338_sub3_sub4.method3466((byte) 84);
		if (class338_sub3_sub4.aShort6570 == 1) {
			if (!Class389.method4037(i_6_, i_3_, 2, i_5_, i_4_, i_3_, i_6_, i_3_, i_4_, Js5TextureLoader.anInt3440 + i_4_))
				return false;
			if (!Class389.method4037(i_6_, i_3_, 2, i_5_, i_4_, i_3_, i_5_, i_3_, i_4_ + Js5TextureLoader.anInt3440, i_4_ + Js5TextureLoader.anInt3440))
				return false;
			Class338_Sub3_Sub5.anInt6572++;
			return true;
		}
		if (class338_sub3_sub4.aShort6570 == 2) {
			if (!Class389.method4037(i_6_, i_3_, 2, i_5_, Js5TextureLoader.anInt3440 + i_4_, i_3_ + Js5TextureLoader.anInt3440, i_6_, i_3_, Js5TextureLoader.anInt3440 + i_4_, i_4_ + Js5TextureLoader.anInt3440))
				return false;
			if (!Class389.method4037(i_5_, i_3_, 2, i_5_, Js5TextureLoader.anInt3440 + i_4_, i_3_ + Js5TextureLoader.anInt3440, i_6_, i_3_ + Js5TextureLoader.anInt3440, Js5TextureLoader.anInt3440 + i_4_, i_4_ + Js5TextureLoader.anInt3440))
				return false;
			Class338_Sub3_Sub5.anInt6572++;
			return true;
		}
		if (class338_sub3_sub4.aShort6570 == 4) {
			if (!Class389.method4037(i_6_, Js5TextureLoader.anInt3440 + i_3_, 2, i_5_, i_4_, Js5TextureLoader.anInt3440 + i_3_, i_6_, Js5TextureLoader.anInt3440 + i_3_, i_4_, Js5TextureLoader.anInt3440 + i_4_))
				return false;
			if (!Class389.method4037(i_6_, Js5TextureLoader.anInt3440 + i_3_, 2, i_5_, i_4_, Js5TextureLoader.anInt3440 + i_3_, i_5_, Js5TextureLoader.anInt3440 + i_3_, i_4_ + Js5TextureLoader.anInt3440, i_4_ + Js5TextureLoader.anInt3440))
				return false;
			Class338_Sub3_Sub5.anInt6572++;
			return true;
		}
		if (class338_sub3_sub4.aShort6570 == 8) {
			if (!Class389.method4037(i_6_, i_3_, 2, i_5_, i_4_, Js5TextureLoader.anInt3440 + i_3_, i_6_, i_3_, i_4_, i_4_))
				return false;
			if (!Class389.method4037(i_5_, i_3_, 2, i_5_, i_4_, Js5TextureLoader.anInt3440 + i_3_, i_6_, Js5TextureLoader.anInt3440 + i_3_, i_4_, i_4_))
				return false;
			Class338_Sub3_Sub5.anInt6572++;
			return true;
		}
		if (class338_sub3_sub4.aShort6570 == 16) {
			if (!Class236.method2132(i_4_ + Class304.anInt2737, Class304.anInt2737, i_5_, i_6_, i_3_, 0, Class304.anInt2737))
				return false;
			Class338_Sub3_Sub5.anInt6572++;
			return true;
		}
		if (class338_sub3_sub4.aShort6570 == 32) {
			if (!Class236.method2132(Class304.anInt2737 + i_4_, Class304.anInt2737, i_5_, i_6_, Class304.anInt2737 + i_3_, 0, Class304.anInt2737))
				return false;
			Class338_Sub3_Sub5.anInt6572++;
			return true;
		}
		if (class338_sub3_sub4.aShort6570 == 64) {
			if (!Class236.method2132(i_4_, Class304.anInt2737, i_5_, i_6_, i_3_ + Class304.anInt2737, 0, Class304.anInt2737))
				return false;
			Class338_Sub3_Sub5.anInt6572++;
			return true;
		}
		if (class338_sub3_sub4.aShort6570 == 128) {
			if (!Class236.method2132(i_4_, Class304.anInt2737, i_5_, i_6_, i_3_, 0, Class304.anInt2737))
				return false;
			Class338_Sub3_Sub5.anInt6572++;
			return true;
		}
		return true;
	}

	static final void method537(int i, int i_7_, int i_8_) {
		Class247 class247 = Class338_Sub2.aClass247ArrayArrayArray5195[i][i_7_][i_8_];
		if (class247 != null) {
			Class230.method2106(class247.aClass338_Sub3_Sub3_2342);
			Class230.method2106(class247.aClass338_Sub3_Sub3_2337);
			if (class247.aClass338_Sub3_Sub3_2342 != null)
				class247.aClass338_Sub3_Sub3_2342 = null;
			if (class247.aClass338_Sub3_Sub3_2337 != null)
				class247.aClass338_Sub3_Sub3_2337 = null;
		}
	}

	final void method528(boolean bool, int i, int i_9_) {
		int i_10_ = i_9_ * anInt3841 >> 12;
		int i_11_ = i_9_ * anInt3842 >> 12;
		int i_12_ = i * anInt3843 >> 12;
		if (!bool) {
			int i_13_ = anInt3840 * i >> 12;
			Class41_Sub19.method466(i_10_, anInt393, i_12_, i_11_, anInt394, anInt397, i_13_, (byte) 1);
		}
	}

	Class42_Sub3(int i, int i_14_, int i_15_, int i_16_, int i_17_, int i_18_, int i_19_) {
		super(i_17_, i_18_, i_19_);
		anInt3840 = i_16_;
		anInt3842 = i_15_;
		anInt3843 = i_14_;
		anInt3841 = i;
	}

	final void method529(int i, int i_20_, int i_21_) {
		if (i_21_ > -11)
			anInt3841 = -40;
	}

	static final Class42_Sub2 method538(Packet class296_sub17, boolean bool) {
		if (bool)
			aBoolean3845 = true;
		return new Class42_Sub2(class296_sub17.g2b(), class296_sub17.g2b(), class296_sub17.g2b(), class296_sub17.g2b(), class296_sub17.readUnsignedMedInt(), class296_sub17.g1());
	}

	final void method531(int i, int i_22_, int i_23_) {
		int i_24_ = i_23_ * anInt3841 >> 12;
		int i_25_ = anInt3842 * i_23_ >> 12;
		if (i == 68) {
			int i_26_ = anInt3843 * i_22_ >> 12;
			int i_27_ = anInt3840 * i_22_ >> 12;
			Class366.method3767(i_24_, (byte) 80, i_26_, i_27_, anInt394, i_25_);
		}
	}
}
