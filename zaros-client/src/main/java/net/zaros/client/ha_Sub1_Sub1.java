package net.zaros.client;
/* ha_Sub1_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Rectangle;

import jaclib.memory.Stream;
import jaggl.MapBuffer;
import jaggl.OpenGL;

public class ha_Sub1_Sub1 extends ha_Sub1 {
	static HashTable aClass263_5823 = new HashTable(16);
	private OpenGL anOpenGL5824;
	static int anInt5825;
	private NodeDeque aClass155_5826 = new NodeDeque();
	private NodeDeque aClass155_5827 = new NodeDeque();
	private NodeDeque aClass155_5828 = new NodeDeque();
	private NodeDeque aClass155_5829 = new NodeDeque();
	private NodeDeque aClass155_5830 = new NodeDeque();
	private NodeDeque aClass155_5831 = new NodeDeque();
	private NodeDeque aClass155_5832 = new NodeDeque();
	private int anInt5833;
	private boolean aBoolean5834;
	private boolean aBoolean5835;
	private boolean aBoolean5836;
	private boolean aBoolean5837;
	private long aLong5838;
	private Class105_Sub1[] aClass105_Sub1Array5839 = new Class105_Sub1[16];
	MapBuffer aMapBuffer5840 = new MapBuffer();
	private boolean aBoolean5841;
	MapBuffer aMapBuffer5842 = new MapBuffer();
	boolean aBoolean5843;
	boolean aBoolean5844;
	private boolean aBoolean5845;
	boolean aBoolean5846;
	private String aString5847;
	private int anInt5848 = 0;
	int[] anIntArray5849;
	private int anInt5850;
	private String aString5851;
	int anInt5852;
	boolean aBoolean5853;
	private boolean aBoolean5854;

	@Override
	public void b(int i, int i_0_, int i_1_, int i_2_, double d) {
		/* empty */
	}

	@Override
	public void a(Rectangle[] rectangles, int i, int i_3_, int i_4_) throws Exception_Sub1 {
		c(i_3_, i_4_);
	}

	@Override
	public Interface15_Impl2 method1205(boolean bool, int i) {
		int i_5_ = -102 / ((-15 - i) / 62);
		return new Class105_Sub1(this, bool);
	}

	@Override
	public boolean k() {
		return false;
	}

	@Override
	public void method1218(int i, int i_6_) {
		if (i != 11688) {
			method1104(true, -102, 91, null);
		}
	}

	public synchronized void method1234(int i, int i_7_, int i_8_) {
		if (i == 0) {
			IntegerNode class296_sub16 = new IntegerNode(i_7_);
			class296_sub16.uid = i_8_;
			aClass155_5828.addLast((byte) -124, class296_sub16);
		}
	}

	@Override
	public void a(int i, int i_9_, int i_10_, int i_11_) {
		/* empty */
	}

	@Override
	public void w() {
		if (anInt3921 > 0 || anInt3920 > 0) {
			int i = anInt3987;
			int i_12_ = anInt3982;
			int i_13_ = anInt4039;
			int i_14_ = anInt4040;
			la();
			OpenGL.glReadBuffer(1028);
			OpenGL.glDrawBuffer(1029);
			method1105((byte) 95);
			method1201(-25357, false);
			method1137(4, false);
			method1110(false, -63);
			method1106(-99, false);
			method1140(null, false);
			method1133(false, -2, 0, false);
			method1197((byte) 22, 1);
			method1161(31156, 0);
			OpenGL.glMatrixMode(5889);
			OpenGL.glLoadIdentity();
			OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
			OpenGL.glMatrixMode(5888);
			OpenGL.glLoadIdentity();
			OpenGL.glRasterPos2i(0, 0);
			OpenGL.glCopyPixels(0, 0, anInt3921, anInt3920, 6144);
			OpenGL.glFlush();
			OpenGL.glReadBuffer(1029);
			OpenGL.glDrawBuffer(1029);
			KA(i, i_13_, i_12_, i_14_);
		}
	}

	@Override
	public void method1228(int i) {
		Class124.aFloatArray1283[0] = (16711680 & anInt4033) / 1.671168E7F;
		Class124.aFloatArray1283[1] = (65280 & anInt4033) / 65280.0F;
		Class124.aFloatArray1283[3] = (anInt4033 >>> 24) / 255.0F;
		if (i != -25685) {
			aBoolean5837 = false;
		}
		Class124.aFloatArray1283[2] = (255 & anInt4033) / 255.0F;
		OpenGL.glTexEnvfv(8960, 8705, Class124.aFloatArray1283, 0);
	}

	@Override
	public Interface6_Impl1 method1187(int i, byte i_15_, int[] is, int i_16_, int i_17_, int i_18_, boolean bool) {
		if (i_15_ >= -121) {
			return null;
		}
		if (aBoolean5845 || Class30.method329(2844, i_18_) && Class30.method329(2844, i)) {
			return new Class205_Sub3(this, i_18_, i, bool, is, i_17_, i_16_);
		}
		if (aBoolean5854) {
			return new Class205_Sub1(this, i_18_, i, is, i_17_, i_16_);
		}
		Class205_Sub3 class205_sub3 = new Class205_Sub3(this, za_Sub2.aClass202_6555, Class67.aClass67_745, Class8.get_next_high_pow2(i_18_), Class8.get_next_high_pow2(i));
		class205_sub3.method68(0, i_18_, i_17_, is, -25989, i_16_, i, 0);
		return class205_sub3;
	}

	@Override
	public void y() {
		/* empty */
	}

	@Override
	public void a(boolean bool) {
		/* empty */
	}

	@Override
	public Interface6_Impl1 method1160(Class202 class202, int i, boolean bool, int i_19_, float[] fs, int i_20_, int i_21_, int i_22_) {
		if (i_22_ != 28426) {
			method1209(null, (byte) -22);
		}
		if (aBoolean5845 || Class30.method329(2844, i_20_) && Class30.method329(2844, i)) {
			return new Class205_Sub3(this, class202, i_20_, i, bool, fs, i_19_, i_21_);
		}
		if (!aBoolean5854) {
			Class205_Sub3 class205_sub3 = new Class205_Sub3(this, class202, Class67.aClass67_749, Class8.get_next_high_pow2(i_20_), Class8.get_next_high_pow2(i));
			class205_sub3.method1983(fs, 0, i_20_, i_21_, class202, 0, false, i_19_, i);
			return class205_sub3;
		}
		return new Class205_Sub1(this, class202, i_20_, i, fs, i_19_, i_21_);
	}

	static public String escapeMessage(String string) {
		int i_23_ = string.length();
		int i_24_ = 0;
		for (int i_25_ = 0; i_23_ > i_25_; i_25_++) {
			char c = string.charAt(i_25_);
			if (c == '<' || c == '>') {
				i_24_ += 3;
			}
		}
		StringBuffer stringbuffer = new StringBuffer(i_24_ + i_23_);
		for (int i_26_ = 0; i_26_ < i_23_; i_26_++) {
			char c = string.charAt(i_26_);
			if (c == '<') {
				stringbuffer.append("<lt>");
			} else if (c != '>') {
				stringbuffer.append(c);
			} else {
				stringbuffer.append("<gt>");
			}
		}
		return stringbuffer.toString();
	}

	@Override
	public void method1148(int i) {
		if (i <= 4) {
			method1185(-34, 110, null, -15);
		}
		if (!aBoolean3952 || !aBoolean3963 || anInt3990 < 0) {
			OpenGL.glDisable(2912);
		} else {
			OpenGL.glEnable(2912);
		}
	}

	@Override
	public void method1138(int i) {
		OpenGL.glLightfv(16384, 4611, aFloatArray4030, 0);
		OpenGL.glLightfv(16385, 4611, aFloatArray4028, i);
	}

	public static void method1236(int i) {
		aClass263_5823 = null;
		if (i != 16711680) {
			method1236(55);
		}
	}

	@Override
	public void method1227(int i) {
		if (i != -1) {
			aMapBuffer5840 = null;
		}
		OpenGL.glMatrixMode(5890);
		if (Class67.aClass230_754 == aClass230Array3966[anInt3991]) {
			OpenGL.glLoadIdentity();
		} else {
			OpenGL.glLoadMatrixf(aClass373_Sub2Array3965[anInt3991].method3932(0, Class338_Sub3_Sub5_Sub2.aFloatArray6669), 0);
		}
		OpenGL.glMatrixMode(5888);
	}

	@Override
	public void c(int i, int i_27_) throws Exception_Sub1 {
		anOpenGL5824.swapBuffers();
	}

	@Override
	public void method1165(int i) {
		if (i != 2147483647) {
			method1224(false);
		}
		OpenGL.glTexEnvi(8960, 34162, Class352.method3688(aClass125Array3995[anInt3991], true));
	}

	@Override
	public boolean method1147(Class67 class67, Class202 class202, byte i) {
		if (i != -14) {
			return false;
		}
		return true;
	}

	@Override
	public void method1150(boolean bool) {
		if (bool == true) {
			OpenGL.glTexEnvi(8960, 34161, Class352.method3688(aClass125Array4003[anInt3991], bool));
		}
	}

	@Override
	public void method1173(int i) {
		if (aBoolean4041) {
			OpenGL.glEnable(16384);
			OpenGL.glEnable(16385);
		} else {
			OpenGL.glDisable(16384);
			OpenGL.glDisable(16385);
		}
		if (i != -3394) {
			aBoolean5834 = true;
		}
	}

	@Override
	public void method1209(Class127 class127, byte i) {
		Class211[] class211s = ((Class127_Sub1) class127).aClass211Array4283;
		int i_28_ = 0;
		if (i < -92) {
			boolean bool = false;
			boolean bool_29_ = false;
			boolean bool_30_ = false;
			for (int i_31_ = 0; class211s.length > i_31_; i_31_++) {
				Class211 class211 = class211s[i_31_];
				Class105_Sub1 class105_sub1 = aClass105_Sub1Array5839[i_31_];
				int i_32_ = 0;
				int i_33_ = class105_sub1.method911((byte) 126);
				long l = class105_sub1.method908(-120);
				class105_sub1.method906(0);
				for (int i_34_ = 0; i_34_ < class211.method2015(192); i_34_++) {
					Class356 class356 = class211.method2018(true, i_34_);
					if (Class356.aClass356_3072 != class356) {
						if (class356 == Class356.aClass356_3073) {
							bool_29_ = true;
							OpenGL.glNormalPointer(5126, i_33_, i_32_ + l);
						} else if (class356 != Class356.aClass356_3075) {
							if (class356 != Class356.aClass356_3076) {
								if (Class356.aClass356_3077 == class356) {
									OpenGL.glClientActiveTexture(i_28_++ + 33984);
									OpenGL.glTexCoordPointer(2, 5126, i_33_, i_32_ + l);
								} else if (class356 == Class356.aClass356_3078) {
									OpenGL.glClientActiveTexture(33984 + i_28_++);
									OpenGL.glTexCoordPointer(3, 5126, i_33_, l + i_32_);
								} else if (Class356.aClass356_3079 == class356) {
									OpenGL.glClientActiveTexture(i_28_++ + 33984);
									OpenGL.glTexCoordPointer(4, 5126, i_33_, l + i_32_);
								}
							} else {
								OpenGL.glClientActiveTexture(i_28_++ + 33984);
								OpenGL.glTexCoordPointer(1, 5126, i_33_, l + i_32_);
							}
						} else {
							OpenGL.glColorPointer(4, 5121, i_33_, i_32_ + l);
							bool = true;
						}
					} else {
						bool_30_ = true;
						OpenGL.glVertexPointer(3, 5126, i_33_, l + i_32_);
					}
					i_32_ += class356.anInt3074;
				}
			}
			if (!bool_30_ != !aBoolean5841) {
				if (bool_30_) {
					OpenGL.glEnableClientState(32884);
				} else {
					OpenGL.glDisableClientState(32884);
				}
				aBoolean5841 = bool_30_;
			}
			if (aBoolean5837 == !bool_29_) {
				if (bool_29_) {
					OpenGL.glEnableClientState(32885);
				} else {
					OpenGL.glDisableClientState(32885);
				}
				aBoolean5837 = bool_29_;
			}
			if (aBoolean5834 == !bool) {
				if (!bool) {
					OpenGL.glDisableClientState(32886);
				} else {
					OpenGL.glEnableClientState(32886);
				}
				aBoolean5834 = bool;
			}
			if (i_28_ > anInt5833) {
				for (int i_35_ = anInt5833; i_28_ > i_35_; i_35_++) {
					OpenGL.glClientActiveTexture(i_35_ + 33984);
					OpenGL.glEnableClientState(32888);
				}
				anInt5833 = i_28_;
			} else if (anInt5833 > i_28_) {
				for (int i_36_ = i_28_; i_36_ < anInt5833; i_36_++) {
					OpenGL.glClientActiveTexture(i_36_ + 33984);
					OpenGL.glDisableClientState(32888);
				}
				anInt5833 = i_28_;
			}
		}
	}

	@Override
	public Object method1100(int i, Canvas canvas) {
		int i_37_ = -60 % ((9 - i) / 53);
		long l = anOpenGL5824.prepareSurface(canvas);
		if (l == -1L) {
			throw new RuntimeException();
		}
		return new Long(l);
	}

	@Override
	public void method1208(Object object, Canvas canvas, byte i) {
		Long var_long = (Long) object;
		if (i > 118) {
			anOpenGL5824.surfaceResized(var_long.longValue());
		}
	}

	@Override
	public void c() {
		/* empty */
	}

	@Override
	public boolean method1200(Class67 class67, Class202 class202, byte i) {
		int i_38_ = 69 % ((-54 - i) / 42);
		return true;
	}

	@Override
	public void A() {
		OpenGL.glFinish();
	}

	@Override
	public void method1149(int i) {
		if (!aBoolean4062) {
			OpenGL.glDisable(3089);
		} else {
			OpenGL.glEnable(3089);
		}
		if (i != 25154) {
			aBoolean5834 = false;
		}
	}

	@Override
	public void method1186(int i) {
		OpenGL.glDepthMask(aBoolean3946 && aBoolean3997);
		if (i != 0) {
			method1104(true, 42, 46, null);
		}
	}

	@Override
	public void method1216(int i) {
		if (i >= -56) {
			aClass155_5830 = null;
		}
		if (aBoolean4015) {
			OpenGL.glEnable(3042);
		} else {
			OpenGL.glDisable(3042);
		}
	}

	@Override
	public void method1178(Interface15_Impl2 interface15_impl2, byte i, int i_39_) {
		if (i < -116) {
			aClass105_Sub1Array5839[i_39_] = (Class105_Sub1) interface15_impl2;
		}
	}

	@Override
	public void method1108(boolean bool) {
		if (aBoolean3979) {
			OpenGL.glEnable(3008);
		} else {
			OpenGL.glDisable(3008);
		}
		if (bool != true) {
			method1170(-29, null, false, true, (byte) -105);
		}
	}

	@Override
	public void method1101(boolean bool, byte i) {
		if (!bool) {
			OpenGL.glDisable(32925);
		} else {
			OpenGL.glEnable(32925);
		}
		int i_40_ = 94 % ((i - 15) / 49);
	}

	public synchronized void method1237(byte i, long l) {
		Node class296 = new Node();
		class296.uid = l;
		if (i >= -43) {
			method1154(false, 73);
		}
		aClass155_5832.addLast((byte) 99, class296);
	}

	@Override
	public void method1146(byte i) {
		for (int i_41_ = anInt4031 - 1; i_41_ >= 0; i_41_--) {
			OpenGL.glActiveTexture(33984 + i_41_);
			OpenGL.glTexEnvi(8960, 8704, 34160);
			OpenGL.glTexEnvi(8960, 34161, 8448);
			OpenGL.glTexEnvi(8960, 34178, 34166);
			OpenGL.glTexEnvi(8960, 34162, 8448);
			OpenGL.glTexEnvi(8960, 34186, 34166);
		}
		OpenGL.glTexEnvi(8960, 34186, 34168);
		OpenGL.glShadeModel(7425);
		OpenGL.glClearDepth(1.0F);
		OpenGL.glDepthFunc(515);
		OpenGL.glPolygonMode(1028, 6914);
		OpenGL.glEnable(2884);
		OpenGL.glCullFace(1029);
		OpenGL.glAlphaFunc(516, 0.0F);
		OpenGL.glMatrixMode(5888);
		OpenGL.glLoadIdentity();
		OpenGL.glColorMaterial(1028, 5634);
		OpenGL.glEnable(2903);
		float[] fs = {0.0F, 0.0F, 0.0F, 1.0F};
		for (int i_42_ = 0; i_42_ < 8; i_42_++) {
			int i_43_ = 16384 + i_42_;
			OpenGL.glLightfv(i_43_, 4608, fs, 0);
			OpenGL.glLightf(i_43_, 4615, 0.0F);
			OpenGL.glLightf(i_43_, 4616, 0.0F);
		}
		OpenGL.glFogf(2914, 0.95F);
		OpenGL.glFogi(2917, 9729);
		OpenGL.glHint(3156, 4353);
		anOpenGL5824.setSwapInterval(0);
		super.method1146(i);
	}

	@Override
	public void method1194(byte i) {
		OpenGL.glMatrixMode(5889);
		OpenGL.glLoadMatrixf(aFloatArray3949, 0);
		OpenGL.glMatrixMode(5888);
		if (i != -57) {
			method1115(-6);
		}
	}

	@Override
	public void ya() {
		method1106(-75, true);
		OpenGL.glClear(256);
	}

	@Override
	public void method1217(int i, Object object, Canvas canvas) {
		if (i != 26104) {
			method1204(false);
		}
		Long var_long = (Long) object;
		anOpenGL5824.releaseSurface(canvas, var_long.longValue());
	}

	@Override
	public Class360 method1119(int i, int i_44_) {
		if (i != 22940) {
			method1134(-13);
		}
		int i_45_ = i_44_;
		while_95_ : do {
			do {
				if (i_45_ != 3) {
					if (i_45_ != 4) {
						if (i_45_ == 8) {
							break;
						}
						break while_95_;
					}
				} else {
					return new Class360_Sub10(this, aClass138_3928);
				}
				return new Class360_Sub8(this, aClass138_3928, aClass184_3929);
			} while (false);
			return new Class360_Sub3(this, aClass138_3928, aClass184_3929);
		} while (false);
		return super.method1119(22940, i_44_);
	}

	@Override
	public void F(int i, int i_46_) {
		/* empty */
	}

	@Override
	public Class241 a(Class241 class241, Class241 class241_47_, float f, Class241 class241_48_) {
		if (f < 0.5F) {
			return class241;
		}
		return class241_47_;
	}

	@Override
	public void t() {
		super.t();
		if (anOpenGL5824 != null) {
			anOpenGL5824.a();
			anOpenGL5824.release();
			anOpenGL5824 = null;
		}
	}

	@Override
	public void method1185(int i, int i_49_, Class289 class289, int i_50_) {
		int i_51_;
		int i_52_;
		if (ISAACCipher.aClass289_1896 == class289) {
			i_52_ = i_50_ * 2;
			i_51_ = 1;
		} else if (Class287.aClass289_2645 != class289) {
			if (Class166_Sub1.aClass289_4298 == class289) {
				i_51_ = 4;
				i_52_ = i_50_ * 3;
			} else if (class289 == aa_Sub2.aClass289_3730) {
				i_52_ = i_50_ + 2;
				i_51_ = 6;
			} else if (class289 == Class121.aClass289_1267) {
				i_52_ = i_50_ + 2;
				i_51_ = 5;
			} else {
				i_51_ = 0;
				i_52_ = i_50_;
			}
		} else {
			i_52_ = i_50_ + 1;
			i_51_ = 3;
		}
		if (i == 0) {
			OpenGL.glDrawArrays(i_51_, i_49_, i_52_);
		}
	}

	@Override
	public void method1171(byte i, boolean bool, int i_53_, Class287 class287) {
		OpenGL.glTexEnvi(8960, 34184 + i_53_, Class293.method2418(class287, (byte) -100));
		int i_54_ = -107 / ((-37 - i) / 38);
		OpenGL.glTexEnvi(8960, 34200 + i_53_, !bool ? 770 : 771);
	}

	@Override
	public int[] na(int i, int i_55_, int i_56_, int i_57_) {
		int[] is = new int[i_56_ * i_57_];
		for (int i_58_ = 0; i_58_ < i_57_; i_58_++) {
			OpenGL.glReadPixelsi(i, anInt3920 - i_55_ - i_58_ - 1, i_56_, 1, 32993, anInt5852, is, i_56_ * i_58_);
		}
		return is;
	}

	@Override
	public void method1170(int i, Class287 class287, boolean bool, boolean bool_59_, byte i_60_) {
		if (i_60_ >= -29) {
			anInt5825 = 32;
		}
		OpenGL.glTexEnvi(8960, i + 34176, Class293.method2418(class287, (byte) -100));
		if (bool_59_) {
			OpenGL.glTexEnvi(8960, i + 34192, !bool ? 770 : 771);
		} else {
			OpenGL.glTexEnvi(8960, i + 34192, bool ? 769 : 768);
		}
	}

	@Override
	public boolean b() {
		return false;
	}

	@Override
	public void method1211(byte i) {
		if (aBoolean3973 && !aBoolean4032) {
			OpenGL.glEnable(2896);
		} else {
			OpenGL.glDisable(2896);
		}
		if (i != 85) {
			anInt5850 = 22;
		}
	}

	@Override
	public Interface11 a(int i, int i_61_) {
		return null;
	}

	@Override
	public Interface6_Impl3 method1104(boolean bool, int i, int i_62_, int[][] is) {
		if (i != 0) {
			ya();
		}
		return new Class205_Sub2(this, i_62_, bool, is);
	}

	@Override
	public void method1152(int i) {
		if (i >= 106) {
			method1239(106);
		}
	}

	@Override
	public float method1144(int i) {
		if (i != 1) {
			return 1.8965541F;
		}
		return 0.0F;
	}

	@Override
	public Interface6_Impl1 method1107(int i, int i_63_, int i_64_, Class67 class67, Class202 class202) {
		if (i_63_ >= -58) {
			return null;
		}
		if (aBoolean5845 || Class30.method329(2844, i_64_) && Class30.method329(2844, i)) {
			return new Class205_Sub3(this, class202, class67, i_64_, i);
		}
		if (aBoolean5854) {
			return new Class205_Sub1(this, class202, class67, i_64_, i);
		}
		return new Class205_Sub3(this, class202, class67, Class8.get_next_high_pow2(i_64_), Class8.get_next_high_pow2(i));
	}

	@Override
	public Interface19 b(int i, int i_65_) {
		return null;
	}

	@Override
	public void a(Interface13 interface13) {
		/* empty */
	}

	@Override
	public Class127 method1233(byte i, Class211[] class211s) {
		if (i != -46) {
			aBoolean5836 = false;
		}
		return new Class127_Sub1(class211s);
	}

	@Override
	public void method1210(int i) {
		aBoolean5835 = false;
		if (i != -10667) {
			method1154(true, -47);
		}
		method1239(127);
	}

	@Override
	public void method1128(boolean bool) {
		if (!aBoolean3948) {
			OpenGL.glDisable(2929);
		} else {
			OpenGL.glEnable(2929);
		}
		if (bool) {
			aBoolean5845 = true;
		}
	}

	@Override
	public void method1224(boolean bool) {
		aFloat3964 = -anInt3960 + anInt3981;
		aFloat3989 = aFloat3964 - anInt3990;
		if (bool != true) {
			aBoolean5836 = true;
		}
		if (aFloat3989 < anInt4035) {
			aFloat3989 = anInt4035;
		}
		OpenGL.glFogf(2915, aFloat3989);
		OpenGL.glFogf(2916, aFloat3964);
		Class124.aFloatArray1283[1] = (65280 & anInt3947) / 65280.0F;
		Class124.aFloatArray1283[0] = (anInt3947 & 16711680) / 1.671168E7F;
		Class124.aFloatArray1283[2] = (anInt3947 & 255) / 255.0F;
		OpenGL.glFogfv(2918, Class124.aFloatArray1283, 0);
	}

	@Override
	public int I() {
		return anInt5848;
	}

	@Override
	public void method1225(int i) {
		method1102(-118);
		int i_66_ = 0;
		int i_67_ = -47 % ((i - 33) / 61);
		for (/**/; anInt4046 > i_66_; i_66_++) {
			Class296_Sub35 class296_sub35 = aClass296_Sub35Array4005[i_66_];
			int i_68_ = class296_sub35.method2746(-24996);
			int i_69_ = i_66_ + 16386;
			float f = class296_sub35.method2745((byte) 100) / 255.0F;
			Class124.aFloatArray1283[0] = class296_sub35.method2749(true);
			Class124.aFloatArray1283[1] = class296_sub35.method2751(-28925);
			Class124.aFloatArray1283[2] = class296_sub35.method2750(-4444);
			Class124.aFloatArray1283[3] = 1.0F;
			OpenGL.glLightfv(i_69_, 4611, Class124.aFloatArray1283, 0);
			Class124.aFloatArray1283[1] = f * (255 & i_68_ >> 8);
			Class124.aFloatArray1283[3] = 1.0F;
			Class124.aFloatArray1283[2] = f * (i_68_ & 255);
			Class124.aFloatArray1283[0] = f * ((i_68_ & 16716698) >> 16);
			OpenGL.glLightfv(i_69_, 4609, Class124.aFloatArray1283, 0);
			OpenGL.glLightf(i_69_, 4617, 1.0F / (class296_sub35.method2748(117) * class296_sub35.method2748(55)));
			OpenGL.glEnable(i_69_);
		}
		for (/**/; anInt3983 > i_66_; i_66_++) {
			OpenGL.glDisable(i_66_ + 16386);
		}
		super.method1225(113);
	}

	@Override
	public void method1204(boolean bool) {
		if (bool == true) {
			OpenGL.glViewport(anInt4011, anInt4042, anInt3921, anInt3920);
		}
	}

	@Override
	public void method1184(byte i) {
		if (i == -109) {
			if (aClass258_4029 != StaticMethods.aClass258_5957) {
				if (HardReferenceWrapper.aClass258_6699 == aClass258_4029) {
					OpenGL.glBlendFunc(1, 1);
				} else if (ConfigurationsLoader.aClass258_89 == aClass258_4029) {
					OpenGL.glBlendFunc(774, 1);
				}
			} else {
				OpenGL.glBlendFunc(770, 771);
			}
		}
	}

	@Override
	public void GA(int i) {
		OpenGL.glClearColor((i & 0xff0000) / 1.671168E7F, (i & 0xff00) / 65280.0F, (i & 0xff) / 255.0F, (i >>> 24) / 255.0F);
		OpenGL.glClear(16384);
	}

	@Override
	public Interface13 a(Interface19 interface19, Interface11 interface11) {
		return null;
	}

	@Override
	public void method1189(byte i) {
		Class124.aFloatArray1283[0] = aFloat4022 * aFloat4000;
		Class124.aFloatArray1283[2] = aFloat4022 * aFloat4043;
		Class124.aFloatArray1283[1] = aFloat4019 * aFloat4022;
		Class124.aFloatArray1283[3] = 1.0F;
		OpenGL.glLightfv(16384, 4609, Class124.aFloatArray1283, 0);
		if (i == 75) {
			Class124.aFloatArray1283[2] = aFloat4043 * -aFloat4007;
			Class124.aFloatArray1283[3] = 1.0F;
			Class124.aFloatArray1283[1] = aFloat4019 * -aFloat4007;
			Class124.aFloatArray1283[0] = -aFloat4007 * aFloat4000;
			OpenGL.glLightfv(16385, 4609, Class124.aFloatArray1283, 0);
		}
	}

	public synchronized void method1238(int i, int i_70_) {
		Node class296 = new Node();
		class296.uid = i_70_;
		aClass155_5831.addLast((byte) 99, class296);
		if (i != 7803) {
			method1239(103);
		}
	}

	@Override
	public Interface6_Impl1 method1195(boolean bool, int i, byte[] is, Class202 class202, int i_71_, boolean bool_72_, int i_73_, int i_74_) {
		if (bool != true) {
			return null;
		}
		if (!aBoolean5845 && (!Class30.method329(2844, i) || !Class30.method329(2844, i_71_))) {
			if (aBoolean5854) {
				return new Class205_Sub1(this, class202, i, i_71_, is, i_74_, i_73_);
			}
			Class205_Sub3 class205_sub3 = new Class205_Sub3(this, class202, Class67.aClass67_745, Class8.get_next_high_pow2(i), Class8.get_next_high_pow2(i_71_));
			class205_sub3.method67(true, i_73_, i, 0, is, 0, i_74_, i_71_, class202);
			return class205_sub3;
		}
		return new Class205_Sub3(this, class202, i, i_71_, bool_72_, is, i_74_, i_73_);
	}

	@Override
	public Class33 s() {
		int i = -1;
		if (aString5847.indexOf("nvidia") == -1) {
			if (aString5847.indexOf("intel") == -1) {
				if (aString5847.indexOf("ati") != -1) {
					i = 4098;
				}
			} else {
				i = 32902;
			}
		} else {
			i = 4318;
		}
		return new Class33(i, "OpenGL", anInt5850, aString5851, 0L);
	}

	@Override
	public void a(float f, float f_75_, float f_76_) {
		/* empty */
	}

	@Override
	public void h() {
		/* empty */
	}

	@Override
	public void method1157(boolean bool, Class246 class246) {
		if (Class373_Sub2.aClass246_5592 == class246) {
			OpenGL.glDisable(3168);
			OpenGL.glDisable(3169);
			OpenGL.glDisable(3170);
		} else {
			int i = Class354.method3690((byte) -113, class246);
			OpenGL.glTexGeni(8192, 9472, i);
			OpenGL.glEnable(3168);
			OpenGL.glTexGeni(8193, 9472, i);
			OpenGL.glEnable(3169);
			OpenGL.glTexGeni(8194, 9472, i);
			OpenGL.glEnable(3170);
		}
		if (bool) {
			method1218(-6, 107);
		}
	}

	@Override
	public void method1188(int i) {
		OpenGL.glActiveTexture(anInt3991 + 33984);
		int i_77_ = -59 / ((i + 15) / 44);
	}

	private void method1239(int i) {
		if (aBoolean5836) {
			OpenGL.glPopMatrix();
		}
		if (i < 98) {
			anOpenGL5824 = null;
		}
		if (!aClass108_3954.method947(27229)) {
			if (aBoolean3940) {
				OpenGL.glLoadIdentity();
				aBoolean5836 = false;
			} else {
				OpenGL.glLoadMatrixf(aClass373_Sub2_3939.method3932(0, Class338_Sub3_Sub5_Sub2.aFloatArray6669), 0);
				aBoolean5836 = false;
			}
		} else {
			if (!aBoolean5835) {
				OpenGL.glLoadMatrixf(aClass373_Sub2_3943.method3932(0, Class338_Sub3_Sub5_Sub2.aFloatArray6669), 0);
				aBoolean5835 = true;
				method1138(0);
				method1225(96);
			}
			if (aBoolean3940) {
				aBoolean5836 = false;
			} else {
				OpenGL.glPushMatrix();
				OpenGL.glMultMatrixf(aClass373_Sub2_3939.method3932(0, Class338_Sub3_Sub5_Sub2.aFloatArray6669), 0);
				aBoolean5836 = true;
			}
		}
	}

	@Override
	public Interface6_Impl2 method1159(boolean bool, int i, Class202 class202, int i_78_, int i_79_, byte[] is) {
		if (bool != true) {
			aMapBuffer5840 = null;
		}
		return new Class205_Sub4(this, class202, i_79_, i, i_78_, is);
	}

	@Override
	public Interface15_Impl1 method1154(boolean bool, int i) {
		int i_80_ = -115 / ((-30 - i) / 37);
		return new Class105_Sub2(this, Class67.aClass67_746, bool);
	}

	@Override
	public void method1232(Class289 class289, Interface15_Impl1 interface15_impl1, int i, int i_81_, int i_82_, int i_83_, int i_84_) {
		if (i_83_ <= 23) {
			anIntArray5849 = null;
		}
		int i_85_;
		int i_86_;
		if (ISAACCipher.aClass289_1896 == class289) {
			i_86_ = 1;
			i_85_ = i_81_ * 2;
		} else if (Class287.aClass289_2645 == class289) {
			i_85_ = i_81_ + 1;
			i_86_ = 3;
		} else if (Class166_Sub1.aClass289_4298 != class289) {
			if (class289 != aa_Sub2.aClass289_3730) {
				if (Class121.aClass289_1267 != class289) {
					i_85_ = i_81_;
					i_86_ = 0;
				} else {
					i_85_ = i_81_ + 2;
					i_86_ = 5;
				}
			} else {
				i_85_ = i_81_ + 2;
				i_86_ = 6;
			}
		} else {
			i_86_ = 4;
			i_85_ = i_81_ * 3;
		}
		Class67 class67 = interface15_impl1.method34(16839);
		Class105_Sub2 class105_sub2 = (Class105_Sub2) interface15_impl1;
		class105_sub2.method906(0);
		OpenGL.glDrawElements(i_86_, i_85_, HashTable.method2264(-123, class67), class105_sub2.method908(-127) + class67.anInt741 * i_82_);
	}

	@Override
	public synchronized void g(int i) {
		i &= 0x7fffffff;
		int i_87_ = 0;
		while (!aClass155_5827.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_5827.method1573(1);
			Class296_Sub15_Sub1.anIntArray5999[i_87_++] = (int) class296_sub16.uid;
			anInt3938 -= class296_sub16.value;
			if (i_87_ == 1000) {
				OpenGL.glDeleteBuffersARB(i_87_, Class296_Sub15_Sub1.anIntArray5999, 0);
				i_87_ = 0;
			}
		}
		if (i_87_ > 0) {
			OpenGL.glDeleteBuffersARB(i_87_, Class296_Sub15_Sub1.anIntArray5999, 0);
			i_87_ = 0;
		}
		while (!aClass155_5828.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_5828.method1573(1);
			Class296_Sub15_Sub1.anIntArray5999[i_87_++] = (int) class296_sub16.uid;
			anInt3937 -= class296_sub16.value;
			if (i_87_ == 1000) {
				OpenGL.glDeleteTextures(i_87_, Class296_Sub15_Sub1.anIntArray5999, 0);
				i_87_ = 0;
			}
		}
		if (i_87_ > 0) {
			OpenGL.glDeleteTextures(i_87_, Class296_Sub15_Sub1.anIntArray5999, 0);
			i_87_ = 0;
		}
		while (!aClass155_5829.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_5829.method1573(1);
			Class296_Sub15_Sub1.anIntArray5999[i_87_++] = class296_sub16.value;
			if (i_87_ == 1000) {
				OpenGL.glDeleteFramebuffersEXT(i_87_, Class296_Sub15_Sub1.anIntArray5999, 0);
				i_87_ = 0;
			}
		}
		if (i_87_ > 0) {
			OpenGL.glDeleteFramebuffersEXT(i_87_, Class296_Sub15_Sub1.anIntArray5999, 0);
			i_87_ = 0;
		}
		while (!aClass155_5830.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_5830.method1573(1);
			Class296_Sub15_Sub1.anIntArray5999[i_87_++] = (int) class296_sub16.uid;
			anInt3936 -= class296_sub16.value;
			if (i_87_ == 1000) {
				OpenGL.glDeleteRenderbuffersEXT(i_87_, Class296_Sub15_Sub1.anIntArray5999, 0);
				i_87_ = 0;
			}
		}
		if (i_87_ > 0) {
			OpenGL.glDeleteRenderbuffersEXT(i_87_, Class296_Sub15_Sub1.anIntArray5999, 0);
			boolean bool = false;
		}
		while (!aClass155_5826.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_5826.method1573(1);
			OpenGL.glDeleteLists((int) class296_sub16.uid, class296_sub16.value);
		}
		while (!aClass155_5831.method1577((byte) -77)) {
			Node class296 = aClass155_5831.method1573(1);
			OpenGL.glDeleteProgramARB((int) class296.uid);
		}
		while (!aClass155_5832.method1577((byte) -77)) {
			Node class296 = aClass155_5832.method1573(1);
			OpenGL.glDeleteObjectARB(class296.uid);
		}
		while (!aClass155_5826.method1577((byte) -77)) {
			IntegerNode class296_sub16 = (IntegerNode) aClass155_5826.method1573(1);
			OpenGL.glDeleteLists((int) class296_sub16.uid, class296_sub16.value);
		}
		if (E() > 100663296 && Class72.method771(-118) > aLong5838 - -60000L) {
			System.gc();
			aLong5838 = Class72.method771(-123);
		}
		super.g(i);
	}

	@Override
	public void method1134(int i) {
		if (i != 25993) {
			aClass155_5832 = null;
		}
		int i_88_ = anIntArray5849[anInt3991];
		if (i_88_ != 0) {
			anIntArray5849[anInt3991] = 0;
			OpenGL.glBindTexture(i_88_, 0);
			OpenGL.glDisable(i_88_);
		}
	}

	@Override
	public void method1112(boolean bool) {
		Class124.aFloatArray1283[2] = aFloat4010 * aFloat4043;
		Class124.aFloatArray1283[3] = 1.0F;
		Class124.aFloatArray1283[0] = aFloat4000 * aFloat4010;
		Class124.aFloatArray1283[1] = aFloat4010 * aFloat4019;
		if (bool) {
			anIntArray5849 = null;
		}
		OpenGL.glLightModelfv(2899, Class124.aFloatArray1283, 0);
	}

	public synchronized void method1240(int i, int i_89_, int i_90_) {
		IntegerNode class296_sub16 = new IntegerNode(i_89_);
		class296_sub16.uid = i_90_;
		if (i > 110) {
			aClass155_5827.addLast((byte) 119, class296_sub16);
		}
	}

	ha_Sub1_Sub1(OpenGL opengl, Canvas canvas, long l, d var_d, Js5 class138, int i) {
		super(canvas, new Long(l), var_d, class138, i, 1);
		try {
			anOpenGL5824 = opengl;
			anOpenGL5824.b();
			aString5847 = OpenGL.glGetString(7936).toLowerCase();
			aString5851 = OpenGL.glGetString(7937).toLowerCase();
			if (aString5847.indexOf("microsoft") != -1 || aString5847.indexOf("brian paul") != -1 || aString5847.indexOf("mesa") != -1) {
				throw new RuntimeException("");
			}
			String string = OpenGL.glGetString(7938);
			String[] strings = Class41_Sub30.method522((byte) 63, string.replace('.', ' '), ' ');
			if (strings.length >= 2) {
				try {
					int i_91_ = Class366_Sub2.method3774(-110, strings[0]);
					int i_92_ = Class366_Sub2.method3774(-76, strings[1]);
					anInt5850 = i_91_ * 10 + i_92_;
				} catch (NumberFormatException numberformatexception) {
					throw new RuntimeException("");
				}
			} else {
				throw new RuntimeException("");
			}
			if (anInt5850 < 12) {
				throw new RuntimeException("");
			}
			if (!anOpenGL5824.a("GL_ARB_multitexture")) {
				throw new RuntimeException("");
			}
			if (!anOpenGL5824.a("GL_ARB_texture_env_combine")) {
				throw new RuntimeException("");
			}
			int[] is = new int[1];
			OpenGL.glGetIntegerv(34018, is, 0);
			anInt4031 = is[0];
			if (anInt4031 < 2) {
				throw new RuntimeException("");
			}
			anInt3950 = 8;
			aBoolean5853 = anOpenGL5824.a("GL_ARB_vertex_buffer_object");
			aBoolean3974 = anOpenGL5824.a("GL_ARB_multisample");
			aBoolean5854 = anOpenGL5824.a("GL_ARB_texture_rectangle");
			aBoolean3977 = anOpenGL5824.a("GL_ARB_texture_cube_map");
			aBoolean5845 = anOpenGL5824.a("GL_ARB_texture_non_power_of_two");
			aBoolean3968 = anOpenGL5824.a("GL_EXT_texture3D");
			aBoolean5846 = anOpenGL5824.a("GL_ARB_vertex_shader");
			aBoolean5844 = anOpenGL5824.a("GL_ARB_vertex_program");
			aBoolean5843 = anOpenGL5824.a("GL_ARB_fragment_shader");
			anOpenGL5824.a("GL_ARB_fragment_program");
			anIntArray5849 = new int[anInt4031];
			anInt5852 = Stream.a() ? 33639 : 5121;
			if (aString5851.indexOf("radeon") != -1) {
				int i_93_ = 0;
				boolean bool = false;
				boolean bool_94_ = false;
				String[] strings_95_ = Class41_Sub30.method522((byte) 63, aString5851.replace('/', ' '), ' ');
				for (String element : strings_95_) {
					String string_97_ = element;
					try {
						if (string_97_.length() > 0) {
							if (string_97_.charAt(0) == 'x' && string_97_.length() >= 3 && EquipmentData.method3305((byte) 72, string_97_.substring(1, 3))) {
								bool_94_ = true;
								string_97_ = string_97_.substring(1);
							}
							if (string_97_.equals("hd")) {
								bool = true;
							} else {
								if (string_97_.startsWith("hd")) {
									bool = true;
									string_97_ = string_97_.substring(2);
								}
								if (string_97_.length() >= 4 && EquipmentData.method3305((byte) 80, string_97_.substring(0, 4))) {
									i_93_ = Class366_Sub2.method3774(-25, string_97_.substring(0, 4));
									break;
								}
							}
						}
					} catch (Exception exception) {
						/* empty */
					}
				}
				if (!bool_94_ && !bool) {
					if (i_93_ >= 7000 && i_93_ <= 7999) {
						aBoolean5853 = false;
					}
					if (i_93_ >= 7000 && i_93_ <= 9250) {
						aBoolean3968 = false;
					}
				}
				aBoolean5854 &= anOpenGL5824.a("GL_ARB_half_float_pixel");
			}
			aString5847.indexOf("intel");
			if (aBoolean5853) {
				try {
					int[] is_98_ = new int[1];
					OpenGL.glGenBuffersARB(1, is_98_, 0);
				} catch (Throwable throwable) {
					throw new RuntimeException("");
				}
			}
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			method1091((byte) -117);
			throw new RuntimeException("");
		}
	}

	@Override
	public void method1115(int i) {
		OpenGL.glScissor(anInt4011 + anInt3987, -anInt4040 + anInt4042 + anInt3920, -anInt3987 + anInt3982, -anInt4039 + anInt4040);
		if (i != 13) {
			aMapBuffer5842 = null;
		}
	}

	@Override
	public void method1190(Object object, byte i, Canvas canvas) {
		if (i <= -52) {
			Long var_long = (Long) object;
			if (!anOpenGL5824.setSurface(var_long.longValue())) {
				throw new RuntimeException();
			}
		}
	}

	@Override
	public void method1162(boolean bool) {
		if (bool) {
			method1210(69);
		}
	}
}
