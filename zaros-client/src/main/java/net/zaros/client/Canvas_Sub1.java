package net.zaros.client;
/* Canvas_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

final class Canvas_Sub1 extends Canvas {
	private Component aComponent32;
	static final void method123(int i, ByteStream class296_sub17_sub1) {
		Class296_Sub24 class296_sub24 = (Class296_Sub24) Class328.aClass155_2912.removeFirst((byte) 117);
		if (class296_sub24 != null) {
			boolean bool = false;
			for (int i_0_ = 0; i_0_ < class296_sub24.anInt4756; i_0_++) {
				if (class296_sub24.aClass278Array4749[i_0_] != null) {
					if (class296_sub24.aClass278Array4749[i_0_].anInt2540 == 2)
						class296_sub24.anIntArray4754[i_0_] = -5;
					if (class296_sub24.aClass278Array4749[i_0_].anInt2540 == 0)
						bool = true;
				}
				if (class296_sub24.aClass278Array4755[i_0_] != null) {
					if (class296_sub24.aClass278Array4755[i_0_].anInt2540 == 2)
						class296_sub24.anIntArray4754[i_0_] = -6;
					if (class296_sub24.aClass278Array4755[i_0_].anInt2540 == 0)
						bool = true;
				}
			}
			if (!bool) {
				int i_1_ = class296_sub17_sub1.pos;
				class296_sub17_sub1.p4(class296_sub24.anInt4758);
				for (int i_2_ = 0; class296_sub24.anInt4756 > i_2_; i_2_++) {
					if (class296_sub24.anIntArray4754[i_2_] != 0)
						class296_sub17_sub1.p1((class296_sub24.anIntArray4754[i_2_]));
					else {
						try {
							int i_3_ = class296_sub24.anIntArray4750[i_2_];
							if (i_3_ == 0) {
								Field field = (Field) (class296_sub24.aClass278Array4749[i_2_].anObject2539);
								int i_4_ = field.getInt(null);
								class296_sub17_sub1.p1(0);
								class296_sub17_sub1.p4(i_4_);
							} else if (i_3_ == 1) {
								Field field = (Field) (class296_sub24.aClass278Array4749[i_2_].anObject2539);
								field.setInt(null, (class296_sub24.anIntArray4757[i_2_]));
								class296_sub17_sub1.p1(0);
							} else if (i_3_ == 2) {
								Field field = (Field) (class296_sub24.aClass278Array4749[i_2_].anObject2539);
								int i_5_ = field.getModifiers();
								class296_sub17_sub1.p1(0);
								class296_sub17_sub1.p4(i_5_);
							}
							if (i_3_ == 3) {
								Method method = (Method) (class296_sub24.aClass278Array4755[i_2_].anObject2539);
								byte[][] is = (class296_sub24.aByteArrayArrayArray4751[i_2_]);
								Object[] objects = new Object[is.length];
								for (int i_6_ = 0; i_6_ < is.length; i_6_++) {
									ObjectInputStream objectinputstream = (new ObjectInputStream(new ByteArrayInputStream(is[i_6_])));
									objects[i_6_] = objectinputstream.readObject();
								}
								Object object = method.invoke(null, objects);
								if (object != null) {
									if (object instanceof Number) {
										class296_sub17_sub1.p1(1);
										class296_sub17_sub1.writeLong(((Number) object).longValue());
									} else if (object instanceof String) {
										class296_sub17_sub1.p1(2);
										class296_sub17_sub1.writeString((String) object);
									} else
										class296_sub17_sub1.p1(4);
								} else
									class296_sub17_sub1.p1(0);
							} else if (i_3_ == 4) {
								Method method = (Method) (class296_sub24.aClass278Array4755[i_2_].anObject2539);
								int i_7_ = method.getModifiers();
								class296_sub17_sub1.p1(0);
								class296_sub17_sub1.p4(i_7_);
							}
						} catch (ClassNotFoundException classnotfoundexception) {
							class296_sub17_sub1.p1(-10);
						} catch (java.io.InvalidClassException invalidclassexception) {
							class296_sub17_sub1.p1(-11);
						} catch (java.io.StreamCorruptedException streamcorruptedexception) {
							class296_sub17_sub1.p1(-12);
						} catch (java.io.OptionalDataException optionaldataexception) {
							class296_sub17_sub1.p1(-13);
						} catch (IllegalAccessException illegalaccessexception) {
							class296_sub17_sub1.p1(-14);
						} catch (IllegalArgumentException illegalargumentexception) {
							class296_sub17_sub1.p1(-15);
						} catch (java.lang.reflect.InvocationTargetException invocationtargetexception) {
							class296_sub17_sub1.p1(-16);
						} catch (SecurityException securityexception) {
							class296_sub17_sub1.p1(-17);
						} catch (java.io.IOException ioexception) {
							class296_sub17_sub1.p1(-18);
						} catch (NullPointerException nullpointerexception) {
							class296_sub17_sub1.p1(-19);
						} catch (Exception exception) {
							class296_sub17_sub1.p1(-20);
						} catch (Throwable throwable) {
							class296_sub17_sub1.p1(-21);
						}
					}
				}
				if (i != 4472)
					method123(19, null);
				class296_sub17_sub1.method2559(i_1_);
				class296_sub24.unlink();
			}
		}
	}

	static final void method124(byte i) {
		Class2.aClass166_66.method1645((byte) -98);
		Class84.aClass189_924.method1896(false);
		Class246.aClient2332.method100(72);
		Class230.aCanvas2209.setBackground(Color.black);
		Class379.anInt3618 = -1;
		Class2.aClass166_66 = Class242.method2170(Class230.aCanvas2209, 24898);
		Class84.aClass189_924 = s_Sub2.method3367(true, Class230.aCanvas2209, (byte) -120);
		if (i != 25)
			IncomingPacket.CLANCHANNEL_FULL = null;
	}

	public static void method125(int i) {
		IncomingPacket.CLANCHANNEL_FULL = null;
		if (i != -21676)
			method125(-98);
	}

	Canvas_Sub1(Component component) {
		aComponent32 = component;
	}

	public final void update(Graphics graphics) {
		aComponent32.update(graphics);
	}

	public final void paint(Graphics graphics) {
		aComponent32.paint(graphics);
	}
}
