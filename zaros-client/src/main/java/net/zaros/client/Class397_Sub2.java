package net.zaros.client;

/* Class397_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class397_Sub2 extends Sprite {
	private ha_Sub1 aHa_Sub1_5778;
	private boolean aBoolean5779;
	private boolean aBoolean5780;
	private int anInt5781 = 0;
	private boolean aBoolean5782 = false;
	private boolean aBoolean5783;
	private int anInt5784;
	private Interface6_Impl1 anInterface6_Impl1_5785;
	private int anInt5786;
	private int anInt5787 = 0;
	private boolean aBoolean5788;
	private int anInt5789;
	private int anInt5790 = 0;

	public void method4076(int[] is) {
		is[1] = anInt5789;
		is[3] = anInt5787;
		is[0] = anInt5790;
		is[2] = anInt5781;
	}

	public int method4099() {
		return anInt5781 + anInt5784 + anInt5790;
	}

	private void method4105(int i, int i_0_, int i_1_, int i_2_, int[] is, int i_3_, int i_4_) {
		anInterface6_Impl1_5785.method68(i, i_1_, i_3_, is, -25989, i_4_, i_2_, i_0_);
	}

	public int method4087() {
		return anInt5784;
	}

	public void method4079(int i, int i_5_, int i_6_, int i_7_, int i_8_) {
		Class373_Sub2 class373_sub2 = aHa_Sub1_5778.method1215(-125);
		Class373_Sub2 class373_sub2_9_ = aHa_Sub1_5778.method1153(115);
		i += anInt5790;
		i_5_ += anInt5789;
		anInterface6_Impl1_5785.method29(aa_Sub2.aClass131_3724, 16518);
		aHa_Sub1_5778.method1229(-3);
		aHa_Sub1_5778.method1140(anInterface6_Impl1_5785, false);
		aHa_Sub1_5778.method1161(31156, i_8_);
		aHa_Sub1_5778.method1197((byte) 22, i_6_);
		aHa_Sub1_5778.method1158((byte) -123, 1, Class151.aClass287_1553);
		aHa_Sub1_5778.method1219((byte) 54, Class151.aClass287_1553, 1);
		aHa_Sub1_5778.method1139(0, i_7_);
		class373_sub2.method3931(0.0F, (float) anInt5786, (float) anInt5784, -121);
		class373_sub2.method3904(i, i_5_, 0);
		class373_sub2_9_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) anInt5786, (byte) 115),
				anInterface6_Impl1_5785.method69(-119, (float) anInt5784), -12);
		aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
		aHa_Sub1_5778.method1202(0);
		aHa_Sub1_5778.method1142(-68);
		aHa_Sub1_5778.method1125(0);
		aHa_Sub1_5778.method1158((byte) -98, 1, Class153.aClass287_1578);
		aHa_Sub1_5778.method1219((byte) 54, Class153.aClass287_1578, 1);
	}

	public int method4088() {
		return anInt5787 + (anInt5786 + anInt5789);
	}

	public void method4098(int i, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_) {
		Class373_Sub2 class373_sub2 = aHa_Sub1_5778.method1215(-24);
		Class373_Sub2 class373_sub2_16_ = aHa_Sub1_5778.method1153(98);
		anInterface6_Impl1_5785.method29(aa_Sub2.aClass131_3724, 16518);
		aHa_Sub1_5778.method1229(-3);
		aHa_Sub1_5778.method1140(anInterface6_Impl1_5785, false);
		aHa_Sub1_5778.method1161(31156, i_15_);
		aHa_Sub1_5778.method1197((byte) 22, i_13_);
		aHa_Sub1_5778.method1158((byte) -120, 1, Class151.aClass287_1553);
		aHa_Sub1_5778.method1219((byte) 54, Class151.aClass287_1553, 1);
		aHa_Sub1_5778.method1139(0, i_14_);
		boolean bool = aBoolean5780 && anInt5789 == 0 && anInt5787 == 0;
		boolean bool_17_ = aBoolean5779 && anInt5790 == 0 && anInt5781 == 0;
		if (!(bool_17_ & bool)) {
			if (bool_17_) {
				int i_18_ = i_12_ + i_10_;
				int i_19_ = method4088();
				class373_sub2_16_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) anInt5786, (byte) 106),
						anInterface6_Impl1_5785.method69(-104, (float) i_11_), -36);
				aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
				int i_20_ = i_10_ + anInt5789;
				int i_21_ = i_20_ + anInt5786;
				while (i_18_ >= i_21_) {
					class373_sub2.method3931(0.0F, (float) anInt5786, (float) i_11_, -21);
					class373_sub2.method3904(i, i_20_, 0);
					aHa_Sub1_5778.method1202(0);
					i_21_ += i_19_;
					i_20_ += i_19_;
					aHa_Sub1_5778.method1142(-72);
				}
				if (i_18_ > i_20_) {
					int i_22_ = i_18_ - i_20_;
					class373_sub2_16_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) i_22_, (byte) 122),
							anInterface6_Impl1_5785.method69(-112, (float) i_11_), -82);
					aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
					class373_sub2.method3931(0.0F, (float) i_22_, (float) i_11_, -96);
					class373_sub2.method3904(i, i_20_, 0);
					aHa_Sub1_5778.method1202(0);
					aHa_Sub1_5778.method1142(-85);
				}
			} else if (bool) {
				int i_23_ = i + i_11_;
				int i_24_ = method4099();
				class373_sub2_16_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) i_12_, (byte) -77),
						anInterface6_Impl1_5785.method69(-104, (float) anInt5784), -102);
				aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
				int i_25_ = anInt5790 + i;
				int i_26_ = i_25_ + anInt5784;
				while (i_23_ >= i_26_) {
					class373_sub2.method3931(0.0F, (float) i_12_, (float) anInt5784, -52);
					class373_sub2.method3904(i_25_, i_10_, 0);
					aHa_Sub1_5778.method1202(0);
					i_26_ += i_24_;
					aHa_Sub1_5778.method1142(-92);
					i_25_ += i_24_;
				}
				if (i_23_ > i_25_) {
					int i_27_ = -i_25_ + i_23_;
					class373_sub2_16_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) i_12_, (byte) 111),
							anInterface6_Impl1_5785.method69(-88, (float) i_27_), -44);
					aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
					class373_sub2.method3931(0.0F, (float) i_12_, (float) i_27_, -11);
					class373_sub2.method3904(i_25_, i_10_, 0);
					aHa_Sub1_5778.method1202(0);
					aHa_Sub1_5778.method1142(-115);
				}
			} else {
				int i_28_ = i_10_ + i_12_;
				int i_29_ = i_11_ + i;
				int i_30_ = method4099();
				int i_31_ = method4088();
				int i_32_ = i_10_ + anInt5789;
				int i_33_ = anInt5786 + i_32_;
				while (i_33_ <= i_28_) {
					class373_sub2_16_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) anInt5786, (byte) 127),
							anInterface6_Impl1_5785.method69(-117, (float) anInt5784), -72);
					aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
					int i_34_ = anInt5790 + i;
					int i_35_ = i_34_ + anInt5784;
					while (i_35_ <= i_29_) {
						class373_sub2.method3931(0.0F, (float) anInt5786, (float) anInt5784, -64);
						class373_sub2.method3904(i_34_, i_32_, 0);
						aHa_Sub1_5778.method1202(0);
						i_34_ += i_30_;
						i_35_ += i_30_;
						aHa_Sub1_5778.method1142(-119);
					}
					if (i_29_ > i_34_) {
						int i_36_ = -i_34_ + i_29_;
						class373_sub2_16_.method3931(1.0F,
								anInterface6_Impl1_5785.method71((float) anInt5786, (byte) -114),
								anInterface6_Impl1_5785.method69(-99, (float) i_36_), -89);
						aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
						class373_sub2.method3931(0.0F, (float) anInt5786, (float) i_36_, -90);
						class373_sub2.method3904(i_34_, i_32_, 0);
						aHa_Sub1_5778.method1202(0);
						aHa_Sub1_5778.method1142(-127);
					}
					i_33_ += i_31_;
					i_32_ += i_31_;
				}
				if (i_32_ < i_28_) {
					int i_37_ = -i_32_ + i_28_;
					class373_sub2_16_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) i_37_, (byte) -57),
							anInterface6_Impl1_5785.method69(-94, (float) anInt5784), -84);
					aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
					int i_38_ = i + anInt5790;
					int i_39_ = i_38_ + anInt5784;
					while (i_29_ >= i_39_) {
						class373_sub2.method3931(0.0F, (float) i_37_, (float) anInt5784, -16);
						class373_sub2.method3904(i_38_, i_32_, 0);
						aHa_Sub1_5778.method1202(0);
						i_39_ += i_30_;
						i_38_ += i_30_;
						aHa_Sub1_5778.method1142(-91);
					}
					if (i_38_ < i_29_) {
						int i_40_ = -i_38_ + i_29_;
						class373_sub2_16_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) i_37_, (byte) -116),
								anInterface6_Impl1_5785.method69(-124, (float) i_40_), -108);
						aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
						class373_sub2.method3931(0.0F, (float) i_37_, (float) i_40_, -105);
						class373_sub2.method3904(i_38_, i_32_, 0);
						aHa_Sub1_5778.method1202(0);
						aHa_Sub1_5778.method1142(-83);
					}
				}
			}
		} else {
			class373_sub2_16_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) i_12_, (byte) 109),
					anInterface6_Impl1_5785.method69(-100, (float) i_11_), -110);
			class373_sub2.method3931(0.0F, (float) i_12_, (float) i_11_, -39);
			class373_sub2.method3904(i, i_10_, 0);
			aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
			aHa_Sub1_5778.method1202(0);
			aHa_Sub1_5778.method1142(-122);
		}
		aHa_Sub1_5778.method1125(0);
		aHa_Sub1_5778.method1158((byte) -124, 1, Class153.aClass287_1578);
		aHa_Sub1_5778.method1219((byte) 54, Class153.aClass287_1578, 1);
	}

	public void method4091(float f, float f_41_, float f_42_, float f_43_, float f_44_, float f_45_, int i, aa var_aa,
			int i_46_, int i_47_) {
		Class373_Sub2 class373_sub2 = aHa_Sub1_5778.method1215(88);
		Class373_Sub2 class373_sub2_48_ = aHa_Sub1_5778.method1153(98);
		aa_Sub3 var_aa_Sub3 = (aa_Sub3) var_aa;
		Interface6_Impl1 interface6_impl1 = var_aa_Sub3.anInterface6_Impl1_3731;
		anInterface6_Impl1_5785
				.method29(((!aBoolean5788 && !aBoolean5783 && (i & 0x1) != 0) ? ISAACCipher.aClass131_1892
						: aa_Sub2.aClass131_3724), 16518);
		aHa_Sub1_5778.method1229(-3);
		aHa_Sub1_5778.method1140(anInterface6_Impl1_5785, false);
		aHa_Sub1_5778.method1161(31156, 1);
		aHa_Sub1_5778.method1197((byte) 22, 1);
		if (aBoolean5782) {
			float f_49_ = (float) anInt5784 / (float) method4099();
			float f_50_ = (float) anInt5786 / (float) method4088();
			class373_sub2.method3940(0.0F, 1.0F, 0.0F, (f_43_ - f_41_) * f_49_, f_49_ * (-f + f_42_),
					(-f + f_44_) * f_50_, 0.0F, 0.0F, (-f_41_ + f_45_) * f_50_, -112);
			class373_sub2.method3946(f_50_ * ((float) anInt5789 + f_41_), 0.0F, 16383, f_49_ * ((float) anInt5790 + f));
		} else {
			class373_sub2.method3940(0.0F, 1.0F, 0.0F, -f_41_ + f_43_, -f + f_42_, -f + f_44_, 0.0F, 0.0F,
					f_45_ - f_41_, -112);
			class373_sub2.method3946(f_41_, 0.0F, 16383, f);
		}
		class373_sub2_48_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) anInt5786, (byte) -8),
				anInterface6_Impl1_5785.method69(-111, (float) anInt5784), -39);
		aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
		aHa_Sub1_5778.method1151(1, 16760);
		aHa_Sub1_5778.method1140(interface6_impl1, false);
		aHa_Sub1_5778.method1177(Class41_Sub4.aClass125_3745, 9815, Js5.aClass125_1411);
		aHa_Sub1_5778.method1158((byte) -109, 0, Class153.aClass287_1578);
		Class373_Sub2 class373_sub2_51_ = aHa_Sub1_5778.method1153(104);
		class373_sub2_51_.method3915(class373_sub2);
		class373_sub2_51_.method3904(-i_46_, -i_47_, 0);
		class373_sub2_51_.method3927(1.0F, interface6_impl1.method69(-114, 1.0F),
				interface6_impl1.method71(1.0F, (byte) 110), (byte) 62);
		aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
		aHa_Sub1_5778.method1202(0);
		aHa_Sub1_5778.method1142(-117);
		aHa_Sub1_5778.method1125(0);
		aHa_Sub1_5778.method1158((byte) -110, 0, Class199.aClass287_2007);
		aHa_Sub1_5778.method1177(Js5.aClass125_1411, 9815, Js5.aClass125_1411);
		aHa_Sub1_5778.method1140(null, false);
		aHa_Sub1_5778.method1151(0, 16760);
		aHa_Sub1_5778.method1125(0);
	}

	public void method4093(int i, int i_52_, aa var_aa, int i_53_, int i_54_) {
		aa_Sub3 var_aa_Sub3 = (aa_Sub3) var_aa;
		i_52_ += anInt5789;
		i += anInt5790;
		Interface6_Impl1 interface6_impl1 = var_aa_Sub3.anInterface6_Impl1_3731;
		anInterface6_Impl1_5785.method29(aa_Sub2.aClass131_3724, 16518);
		aHa_Sub1_5778.method1229(-3);
		aHa_Sub1_5778.method1140(anInterface6_Impl1_5785, false);
		aHa_Sub1_5778.method1161(31156, 1);
		aHa_Sub1_5778.method1197((byte) 22, 1);
		Class373_Sub2 class373_sub2 = aHa_Sub1_5778.method1215(106);
		class373_sub2.method3931(0.0F, (float) anInt5786, (float) anInt5784, -75);
		class373_sub2.method3904(i, i_52_, 0);
		aHa_Sub1_5778.method1202(0);
		Class373_Sub2 class373_sub2_55_ = aHa_Sub1_5778.method1153(98);
		class373_sub2_55_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) anInt5786, (byte) -82),
				anInterface6_Impl1_5785.method69(-103, (float) anInt5784), -22);
		aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
		aHa_Sub1_5778.method1151(1, 16760);
		aHa_Sub1_5778.method1140(interface6_impl1, false);
		aHa_Sub1_5778.method1177(Class41_Sub4.aClass125_3745, 9815, Js5.aClass125_1411);
		aHa_Sub1_5778.method1158((byte) -106, 0, Class153.aClass287_1578);
		Class373_Sub2 class373_sub2_56_ = aHa_Sub1_5778.method1153(115);
		class373_sub2_56_.method3931(1.0F, interface6_impl1.method71((float) anInt5786, (byte) 120),
				interface6_impl1.method69(-113, (float) anInt5784), -39);
		class373_sub2_56_.method3946(interface6_impl1.method71((float) (-i_54_ + i_52_), (byte) 116), 0.0F, 16383,
				interface6_impl1.method69(-126, (float) (i - i_53_)));
		aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
		aHa_Sub1_5778.method1142(-70);
		aHa_Sub1_5778.method1125(0);
		aHa_Sub1_5778.method1158((byte) -125, 0, Class199.aClass287_2007);
		aHa_Sub1_5778.method1177(Js5.aClass125_1411, 9815, Js5.aClass125_1411);
		aHa_Sub1_5778.method1140(null, false);
		aHa_Sub1_5778.method1151(0, 16760);
		aHa_Sub1_5778.method1125(0);
	}

	public void method4090(int i, int i_57_, int i_58_, int i_59_, int i_60_, int i_61_) {
		int[] is = aHa_Sub1_5778.na(i_60_, i_61_, i_58_, i_59_);
		if (is != null) {
			for (int i_62_ = 0; i_62_ < is.length; i_62_++)
				is[i_62_] = is[i_62_] | -16777216;
			method4105(i, i_57_, i_58_, i_59_, is, 0, i_58_);
		}
	}

	public void method4097(int i, int i_63_, int i_64_, int i_65_) {
		anInt5790 = i;
		anInt5787 = i_65_;
		anInt5789 = i_63_;
		anInt5781 = i_64_;
		aBoolean5782 = (anInt5790 != 0 || anInt5789 != 0 || anInt5781 != 0 || anInt5787 != 0);
	}

	public void method4084(int i, int i_66_, int i_67_) {
		int[] is = aHa_Sub1_5778.na(i, i_66_, anInt5784, anInt5786);
		int[] is_68_ = new int[anInt5784 * anInt5786];
		anInterface6_Impl1_5785.method65(0, is_68_, 0, anInt5784, anInt5786, 16438, 0);
		if (i_67_ == 0) {
			for (int i_69_ = 0; i_69_ < anInt5786; i_69_++) {
				int i_70_ = i_69_ * anInt5784;
				for (int i_71_ = 0; i_71_ < anInt5784; i_71_++)
					is_68_[i_71_
							+ i_70_] = (((is_68_[i_70_ + i_71_] & 16777215) | (is[i_70_ + i_71_] << 8 & -16776963)));
			}
		} else if (i_67_ == 1) {
			for (int i_72_ = 0; i_72_ < anInt5786; i_72_++) {
				int i_73_ = anInt5784 * i_72_;
				for (int i_74_ = 0; i_74_ < anInt5784; i_74_++)
					is_68_[i_74_ + i_73_] = (((is_68_[i_73_ + i_74_] & 16777215)
							| ((1073807104 & is[i_73_ + i_74_]) << 16)));
			}
		} else if (i_67_ != 2) {
			if (i_67_ == 3) {
				for (int i_75_ = 0; anInt5786 > i_75_; i_75_++) {
					int i_76_ = anInt5784 * i_75_;
					for (int i_77_ = 0; i_77_ < anInt5784; i_77_++)
						is_68_[i_77_ + i_76_] = (((is[i_76_ + i_77_] != 0 ? -16777216 : 0)
								| (16777215 & is_68_[i_76_ + i_77_])));
				}
			}
		} else {
			for (int i_78_ = 0; anInt5786 > i_78_; i_78_++) {
				int i_79_ = anInt5784 * i_78_;
				for (int i_80_ = 0; anInt5784 > i_80_; i_80_++)
					is_68_[i_79_
							+ i_80_] = (((is_68_[i_80_ + i_79_] & 16777215) | ((627414271 & is[i_79_ + i_80_]) << 24)));
			}
		}
		method4105(0, 0, anInt5784, anInt5786, is_68_, 0, anInt5784);
	}

	public void method4075(float f, float f_81_, float f_82_, float f_83_, float f_84_, float f_85_, int i, int i_86_,
			int i_87_, int i_88_) {
		Class373_Sub2 class373_sub2 = aHa_Sub1_5778.method1215(-92);
		Class373_Sub2 class373_sub2_89_ = aHa_Sub1_5778.method1153(119);
		anInterface6_Impl1_5785.method29(((aBoolean5788 || aBoolean5783 || (i_88_ & 0x1) == 0) ? aa_Sub2.aClass131_3724
				: ISAACCipher.aClass131_1892), 16518);
		aHa_Sub1_5778.method1229(-3);
		aHa_Sub1_5778.method1140(anInterface6_Impl1_5785, false);
		aHa_Sub1_5778.method1161(31156, i_87_);
		aHa_Sub1_5778.method1197((byte) 22, i);
		aHa_Sub1_5778.method1158((byte) -126, 1, Class151.aClass287_1553);
		aHa_Sub1_5778.method1219((byte) 54, Class151.aClass287_1553, 1);
		aHa_Sub1_5778.method1139(0, i_86_);
		if (aBoolean5782) {
			float f_90_ = (float) method4099();
			float f_91_ = (float) method4088();
			float f_92_ = (-f + f_82_) / f_90_;
			float f_93_ = (-f_81_ + f_83_) / f_90_;
			float f_94_ = (f_84_ - f) / f_91_;
			float f_95_ = (f_85_ - f_81_) / f_91_;
			float f_96_ = f_94_ * (float) anInt5789;
			float f_97_ = f_95_ * (float) anInt5789;
			float f_98_ = f_92_ * (float) anInt5790;
			float f_99_ = (float) anInt5790 * f_93_;
			float f_100_ = -f_92_ * (float) anInt5781;
			float f_101_ = -f_93_ * (float) anInt5781;
			float f_102_ = (float) anInt5787 * -f_94_;
			f_81_ = f_99_ + f_81_ + f_97_;
			f_84_ = f_84_ + f_98_ + f_102_;
			f = f_96_ + (f_98_ + f);
			f_82_ = f_96_ + (f_100_ + f_82_);
			f_83_ = f_97_ + (f_83_ + f_101_);
			float f_103_ = (float) anInt5787 * -f_95_;
			f_85_ = f_103_ + (f_85_ + f_99_);
		}
		class373_sub2.method3940(0.0F, 1.0F, 0.0F, f_83_ - f_81_, f_82_ - f, f_84_ - f, 0.0F, 0.0F, f_85_ - f_81_, -93);
		class373_sub2.method3946(f_81_, 0.0F, 16383, f);
		class373_sub2_89_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) anInt5786, (byte) 108),
				anInterface6_Impl1_5785.method69(-107, (float) anInt5784), -74);
		aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
		aHa_Sub1_5778.method1202(0);
		aHa_Sub1_5778.method1142(-97);
		aHa_Sub1_5778.method1125(0);
		aHa_Sub1_5778.method1158((byte) -118, 1, Class153.aClass287_1578);
		aHa_Sub1_5778.method1219((byte) 54, Class153.aClass287_1578, 1);
	}

	static public void method4106(int i) {
		if (i == 1)
			Class42_Sub1.aClass113_3833.clear();
	}

	public int method4092() {
		return anInt5786;
	}

	public void method4094(int i, int i_104_, int i_105_, int i_106_, int i_107_, int i_108_, int i_109_, int i_110_) {
		Class373_Sub2 class373_sub2 = aHa_Sub1_5778.method1215(-17);
		Class373_Sub2 class373_sub2_111_ = aHa_Sub1_5778.method1153(97);
		anInterface6_Impl1_5785.method29(((aBoolean5788 || aBoolean5783 || (i_110_ & 0x1) == 0) ? aa_Sub2.aClass131_3724
				: ISAACCipher.aClass131_1892), 16518);
		aHa_Sub1_5778.method1229(-3);
		aHa_Sub1_5778.method1140(anInterface6_Impl1_5785, false);
		aHa_Sub1_5778.method1161(31156, i_109_);
		aHa_Sub1_5778.method1197((byte) 22, i_107_);
		aHa_Sub1_5778.method1158((byte) -119, 1, Class151.aClass287_1553);
		aHa_Sub1_5778.method1219((byte) 54, Class151.aClass287_1553, 1);
		aHa_Sub1_5778.method1139(0, i_108_);
		class373_sub2_111_.method3931(1.0F, anInterface6_Impl1_5785.method71((float) anInt5786, (byte) -34),
				anInterface6_Impl1_5785.method69(-97, (float) anInt5784), -98);
		if (aBoolean5782) {
			i_105_ = i_105_ * anInt5784 / method4099();
			i_106_ = anInt5786 * i_106_ / method4088();
			i += i_105_ * anInt5790 / anInt5784;
			i_104_ += i_106_ * anInt5789 / anInt5786;
		}
		class373_sub2.method3931(0.0F, (float) i_106_, (float) i_105_, -120);
		class373_sub2.method3904(i, i_104_, 0);
		aHa_Sub1_5778.method1181(Class360_Sub5.aClass230_5325, (byte) 125);
		aHa_Sub1_5778.method1202(0);
		aHa_Sub1_5778.method1142(-125);
		aHa_Sub1_5778.method1125(0);
		aHa_Sub1_5778.method1158((byte) -101, 1, Class153.aClass287_1578);
		aHa_Sub1_5778.method1219((byte) 54, Class153.aClass287_1578, 1);
	}

	Class397_Sub2(ha_Sub1 var_ha_Sub1, int i, int i_112_, boolean bool) {
		anInt5789 = 0;
		aHa_Sub1_5778 = var_ha_Sub1;
		anInt5784 = i;
		anInt5786 = i_112_;
		anInterface6_Impl1_5785 = var_ha_Sub1.method1107(i_112_, -87, i, Class67.aClass67_745,
				(bool ? za_Sub2.aClass202_6555 : Class122_Sub1_Sub1.aClass202_6602));
		anInterface6_Impl1_5785.method64((byte) 49, true, true);
		aBoolean5788 = i != anInterface6_Impl1_5785.method70(125);
		aBoolean5783 = i_112_ != anInterface6_Impl1_5785.method66(3776);
		aBoolean5779 = !aBoolean5788 && anInterface6_Impl1_5785.method72(23);
		aBoolean5780 = !aBoolean5783 && anInterface6_Impl1_5785.method72(105);
	}

	Class397_Sub2(ha_Sub1 var_ha_Sub1, int i, int i_113_, int[] is, int i_114_, int i_115_) {
		anInt5789 = 0;
		anInt5784 = i;
		aHa_Sub1_5778 = var_ha_Sub1;
		anInt5786 = i_113_;
		anInterface6_Impl1_5785 = var_ha_Sub1.method1187(i_113_, (byte) -122, is, i_115_, i_114_, i, false);
		anInterface6_Impl1_5785.method64((byte) 49, true, true);
		aBoolean5788 = i != anInterface6_Impl1_5785.method70(52);
		aBoolean5783 = anInterface6_Impl1_5785.method66(3776) != i_113_;
		aBoolean5779 = !aBoolean5788 && anInterface6_Impl1_5785.method72(113);
		aBoolean5780 = !aBoolean5783 && anInterface6_Impl1_5785.method72(108);
	}
}
