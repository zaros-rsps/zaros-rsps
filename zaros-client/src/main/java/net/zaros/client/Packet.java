package net.zaros.client;

/* Class296_Sub17 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.math.BigInteger;

public class Packet extends Node {
	static float[] aFloatArray4675 = new float[4];
	public int pos;
	static int[] anIntArray4677;
	public static boolean debug_packets = false;
	byte[] data;

	public void writeLEShortA(int i_0_) {
		data[pos++] = (byte) (i_0_ + 128);
		data[pos++] = (byte) (i_0_ >> 8);
	}

	public void method2550() {
		if (data != null) {
			Class370.method3874(data, (byte) 125);
		}
		data = null;
	}

	public void method2551(int i, long l) {
		if (--i < 0 || i > 7) {
			throw new IllegalArgumentException();
		}
		for (int i_2_ = i * 8; i_2_ >= 0; i_2_ -= 8) {
			data[pos++] = (byte) (int) (l >> i_2_);
		}
	}

	public int gSmart2or4s() {
		if (data[pos] < 0) {
			return g4() & 0x7fffffff;
		}
		int i_3_ = g2();
		if (i_3_ == 32767) {
			return -1;
		}
		return i_3_;
	}

	public int readInt_v1() {
		pos += 4;
		return (data[pos - 3] & 0xff) + (data[pos - 2] << 24 & ~0xffffff) + (data[pos - 1] << 16 & 0xff0000) + (data[pos - 4] << 8 & 0xff00);
	}

	public int method2554() {
		pos += 3;
		return (data[pos - 2] << 16 & 0xff0000) - (-((data[pos - 3] & 0xff) << 8) - (data[pos - 1] & 0xff));
	}

	public byte readSignedByteA() {
		return (byte) (data[pos++] - 128);
	}

	public int g2b() {
		pos += 2;
		int i_4_ = ((data[pos - 2] & 0xff) << 8) + (data[pos - 1] & 0xff);
		if (i_4_ > 32767) {
			i_4_ -= 65536;
		}
		return i_4_;
	}

	public int method2557() {
		pos += 2;
		int i_5_ = (data[pos - 2] & 0xff) + (data[pos - 1] << 8 & 0xff00);
		if (i_5_ > 32767) {
			i_5_ -= 65536;
		}
		return i_5_;
	}

	public void encryptXteas(int[] is, int i_6_, int i_7_) {
		int i_8_ = pos;
		pos = i_6_;
		int i_9_ = (i_7_ - i_6_) / 8;
		for (int i_11_ = 0; i_11_ < i_9_; i_11_++) {
			int i_12_ = g4();
			int i_13_ = g4();
			int i_14_ = 0;
			int i_15_ = -1640531527;
			int i_16_ = 32;
			while (i_16_-- > 0) {
				i_12_ += i_13_ + (i_13_ << 4 ^ i_13_ >>> 5) ^ is[i_14_ & 0x3] + i_14_;
				i_14_ += i_15_;
				i_13_ += i_12_ + (i_12_ << 4 ^ i_12_ >>> 5) ^ is[(i_14_ & 0x1a3b) >>> 11] + i_14_;
			}
			pos -= 8;
			p4(i_12_);
			p4(i_13_);
		}
		pos = i_8_;
	}

	public int method2559(int i) {
		int i_18_ = ClotchesLoader.method303(24443, pos, data, i);
		p4(i_18_);
		return i_18_;
	}

	public int readLEInt() {
		pos += 4;
		return (data[pos - 4] & 0xff) + ((data[pos - 3] & 0xff) << 8) + ((data[pos - 1] & 0xff) << 24) + (data[pos - 2] << 16 & 0xff0000);
	}

	public int gSmart2or4() {
		if (data[pos] < 0) {
			return g4() & 0x7fffffff;
		}
		return g2();
	}

	public String gstr() {
		int i_19_ = pos;
		while (data[pos++] != 0) {
			/* empty */
		}
		int i_20_ = pos - i_19_ - 1;
		if (i_20_ == 0) {
			return "";
		}
		return Class360.method3722(i_19_, data, i_20_, (byte) 75);
	}

	public void writeLEInt(int i) {
		data[pos++] = (byte) i;
		data[pos++] = (byte) (i >> 8);
		data[pos++] = (byte) (i >> 16);
		data[pos++] = (byte) (i >> 24);
	}

	public long method2564(int i_22_) {
		if (--i_22_ < 0 || i_22_ > 7) {
			throw new IllegalArgumentException();
		}
		int i_23_ = i_22_ * 8;
		long l = 0L;
		for (/**/; i_23_ >= 0; i_23_ -= 8) {
			l |= (data[pos++] & 0xffL) << i_23_;
		}
		return l;
	}

	public int readUnsignedLEShortA() {
		pos += 2;
		return (data[pos - 1] << 8 & 0xff00) + (data[pos - 2] - 128 & 0xff);
	}

	public int method2566() {
		pos += 3;
		int i_24_ = (data[pos - 1] & 0xff) + (data[pos - 3] << 16 & 0xff0000) + ((data[pos - 2] & 0xff) << 8);
		if (i_24_ > 8388607) {
			i_24_ -= 16777216;
		}
		return i_24_;
	}

	public int readUnsignedByteS() {
		return -data[pos++] + 128 & 0xff;
	}

	public void p2(int i) {
		data[pos++] = (byte) (i >> 8);
		data[pos++] = (byte) i;
	}

	public int g4() {
		pos += 4;
		return (data[pos - 1] & 0xff) + (data[pos - 4] << 24 & ~0xffffff) - (-(data[pos - 3] << 16 & 0xff0000) - ((data[pos - 2] & 0xff) << 8));
	}

	public void method2570(int i) {
		data[pos++] = (byte) (i >> 8);
		data[pos++] = (byte) i;
		data[pos++] = (byte) (i >> 24);
		data[pos++] = (byte) (i >> 16);
	}

	public void writeString(String string) {
		int i = string.indexOf('\0');
		if (i >= 0) {
			throw new IllegalArgumentException("NUL character at " + i + " - cannot pjstr");
		}
		pos += Class360.method3735(string, 0, data, pos, -210, string.length());
		data[pos++] = (byte) 0;
	}

	public static void method2572() {
		anIntArray4677 = null;
		aFloatArray4675 = null;
	}

	public byte readSignedByteC() {
		return (byte) -data[pos++];
	}

	public int method2574() {
		pos += 4;
		return (data[pos - 4] & 0xff) + (data[pos - 3] << 8 & 0xff00) + ((data[pos - 2] & 0xff) << 16) + ((data[pos - 1] & 0xff) << 24);
	}

	public void writeInt_v2(int i_26_) {
		data[pos++] = (byte) (i_26_ >> 16);
		data[pos++] = (byte) (i_26_ >> 24);
		data[pos++] = (byte) i_26_;
		data[pos++] = (byte) (i_26_ >> 8);
	}

	public int g1() {
		return data[pos++] & 0xff;
	}

	public String gjstr2() {
		byte version = data[pos++];
		if (version != 0) {
			throw new IllegalStateException("Bad version number in gjstr2");
		}
		int start = pos;
		while (data[pos++] != 0) {
			/* empty */
		}
		int end = -1 - start + pos;
		if (end == 0) {
			return "";
		}
		return Class360.method3722(start, data, end, (byte) 75);
	}

	public int readInt_v2() {
		pos += 4;
		return (data[pos - 2] & 0xff) + ((data[pos - 4] & 0xff) << 16) + (data[pos - 3] << 24 & ~0xffffff) + (data[pos - 1] << 8 & 0xff00);
	}

	public int readUnsignedMedInt() {
		pos += 3;
		return (data[pos - 2] << 8 & 0xff00) + ((data[pos - 3] & 0xff) << 16) + (data[pos - 1] & 0xff);
	}

	public void method2580(int i_30_) {
		data[pos++] = (byte) i_30_;
		data[pos++] = (byte) (i_30_ >> 8);
		data[pos++] = (byte) (i_30_ >> 16);
		data[pos++] = (byte) (i_30_ >> 24);
	}

	public long method2581() {
		long l = method2574() & 0xffffffffL;
		long l_32_ = method2574() & 0xffffffffL;
		return l + (l_32_ << 32);
	}

	public int gSmart1or2s() {
		int i_33_ = data[pos] & 0xff;
		if (i_33_ < 128) {
			return g1() - 64;
		}
		return g2() - 49152;
	}

	static public void method2583(int i) {
		if (Class41_Sub8.aClass326Array3764 == null) {
			Class41_Sub8.aClass326Array3764 = Class326.method3385(100);
			Class253.aClass326_2389 = Class41_Sub8.aClass326Array3764[0];
			Class296_Sub39_Sub4.aLong5743 = Class72.method771(-109);
		}
		if (Class296_Sub51_Sub15.aClass54_6426 == null) {
			Class296_Sub51_Sub30.method3171((byte) -128);
		}
		Class326 class326 = Class253.aClass326_2389;
		if (i != -98) {
			method2583(-19);
		}
		int i_34_ = Class338_Sub6.method3593((byte) 116);
		if (Class253.aClass326_2389 == class326) {
			Class338_Sub7.aString5250 = Class253.aClass326_2389.aClass120_2879.getTranslation(Class394.langID);
			if (Class253.aClass326_2389.aBoolean2881) {
				Class391.anInt3297 = (Class253.aClass326_2389.anInt2876 - Class253.aClass326_2389.anInt2875) * i_34_ / 100 + Class253.aClass326_2389.anInt2875;
			}
			if (Class253.aClass326_2389.aBoolean2872) {
				Class338_Sub7.aString5250 += Class391.anInt3297 + "%";
			}
		} else if (Class253.aClass326_2389 == Class326.aClass326_2899) {
			Class296_Sub51_Sub15.aClass54_6426 = null;
			Class41_Sub8.method422(1, 3);
		} else {
			Class338_Sub7.aString5250 = class326.aClass120_2878.getTranslation(Class394.langID);
			if (Class253.aClass326_2389.aBoolean2872) {
				Class338_Sub7.aString5250 += class326.anInt2876 + "%";
			}
			Class391.anInt3297 = class326.anInt2876;
			if (Class253.aClass326_2389.aBoolean2881 || class326.aBoolean2881) {
				Class296_Sub39_Sub4.aLong5743 = Class72.method771(-107);
			}
		}
		if (Class296_Sub51_Sub15.aClass54_6426 != null) {
			Class296_Sub51_Sub15.aClass54_6426.method643(Class338_Sub7.aString5250, -109, Class253.aClass326_2389, Class391.anInt3297, Class296_Sub39_Sub4.aLong5743);
			if (Class338_Sub6.anInterface2Array5237 != null) {
				for (int i_35_ = Class367.anInt3125 + 1; Class338_Sub6.anInterface2Array5237.length > i_35_; i_35_++) {
					if (Class338_Sub6.anInterface2Array5237[i_35_].method9(4739) >= 100 && i_35_ - 1 == Class367.anInt3125 && Class366_Sub6.anInt5392 >= 1 && Class296_Sub51_Sub15.aClass54_6426.method647(0)) {
						try {
							Class338_Sub6.anInterface2Array5237[i_35_].method10((byte) -97);
						} catch (Exception exception) {
							Class338_Sub6.anInterface2Array5237 = null;
							break;
						}
						Class296_Sub51_Sub15.aClass54_6426.method644(true, Class338_Sub6.anInterface2Array5237[i_35_]);
						Class367.anInt3125++;
						if (Class367.anInt3125 >= Class338_Sub6.anInterface2Array5237.length - 1 && Class338_Sub6.anInterface2Array5237.length > 1) {
							Class367.anInt3125 = Class304.aClass56_2734.method665(i ^ 0x16) ? 0 : -1;
						}
					}
				}
			}
		}
	}

	public void writeShortA(int i_36_) {
		data[pos++] = (byte) (i_36_ >> 8);
		data[pos++] = (byte) (i_36_ + 128);
	}

	public void p4(int i_37_) {
		data[pos++] = (byte) (i_37_ >> 24);
		data[pos++] = (byte) (i_37_ >> 16);
		data[pos++] = (byte) (i_37_ >> 8);
		data[pos++] = (byte) i_37_;
	}

	static public ModeWhere[] getModeWheres(int i) {
		if (i <= 23) {
			return null;
		}
		return new ModeWhere[] { BITConfigsLoader.liveModeWhere, ModeWhere.officeModeWhere, Class121.office2ModeWhere, Class3.office3ModeWhere, Class296_Sub34_Sub2.localModeWhere, Class294.office4ModeWhere, Class41_Sub12.intBetaModeWhere };
	}

	public int gvarint() {
		int i_33_ = data[pos++];
		int i_34_ = 0;
		for (; i_33_ < 0; i_33_ = data[pos++]) {
			i_34_ = (i_34_ | i_33_ & 0x7f) << 7;
		}
		return i_34_ | i_33_;
	}

	public void writeRsaEncryption(BigInteger exponent, BigInteger modulus) {
		int i_41_ = pos;
		pos = 0;
		byte[] is = new byte[i_41_];
		readBytes(is, 0, i_41_);
		byte[] buffer = new BigInteger(is).modPow(exponent, modulus).toByteArray();
		pos = 0;
		p2(buffer.length);
		// System.out.println("wrote rsa buffer length: " + buffer.length);
		writeBytes(buffer, 0, buffer.length);
	}

	public byte readSignedByteS() {
		return (byte) (-data[pos++] + 128);
	}

	public void method2590(int i) {
		data[pos++] = (byte) i;
		data[pos++] = (byte) (i >> 8);
	}

	public void writeLEShort(int i_46_) {
		data[pos++] = (byte) i_46_;
		data[pos++] = (byte) (i_46_ >> 8);
	}

	public int readSmart() {
		int i = data[pos] & 0xff;
		if (i < 128) {
			return g1();
		}
		return g2() - 32768;
	}

	public void write24BitInt(int i) {
		data[pos++] = (byte) (i >> 16);
		data[pos++] = (byte) (i >> 8);
		data[pos++] = (byte) i;
	}

	public void p5(long i) {
		data[pos++] = (byte) (int) (i >> 32);
		data[pos++] = (byte) (int) (i >> 24);
		data[pos++] = (byte) (int) (i >> 16);
		data[pos++] = (byte) (int) (i >> 8);
		data[pos++] = (byte) (int) i;
	}

	public int readUnsignedByteA() {
		return data[pos++] - 128 & 0xff;
	}

	public int method2595() {
		int i_48_ = 0;
		int i_49_;
		for (i_49_ = readSmart(); i_49_ == 32767; i_49_ = readSmart()) {
			i_48_ += 32767;
		}
		i_48_ += i_49_;
		return i_48_;
	}

	public void writeLong(long l) {
		data[pos++] = (byte) (int) (l >> 56);
		data[pos++] = (byte) (int) (l >> 48);
		data[pos++] = (byte) (int) (l >> 40);
		data[pos++] = (byte) (int) (l >> 32);
		data[pos++] = (byte) (int) (l >> 24);
		data[pos++] = (byte) (int) (l >> 16);
		data[pos++] = (byte) (int) (l >> 8);
		data[pos++] = (byte) (int) l;
	}

	public void writeBytes(byte[] _data, int offset, int count) {
		int numWriten = offset;
		for (/**/; numWriten < count + offset; numWriten++) {
			data[pos++] = _data[numWriten];
		}
	}

	public int g2() {
		pos += 2;
		return ((data[pos - 2] & 0xff) << 8) + (data[pos - 1] & 0xff);
	}

	public void decryptXTEA(int i, int i_53_, int[] is) {
		int i_54_ = pos;
		pos = i;
		int i_55_ = (-i + i_53_) / 8;
		for (int i_56_ = 0; i_55_ > i_56_; i_56_++) {
			int i_57_ = g4();
			int i_58_ = g4();
			int i_59_ = -957401312;
			int i_60_ = -1640531527;
			int i_61_ = 32;
			while (i_61_-- > 0) {
				i_58_ -= (i_57_ >>> 5 ^ i_57_ << 4) + i_57_ ^ is[i_59_ >>> 11 & ~0x439ffffc] + i_59_;
				i_59_ -= i_60_;
				i_57_ -= (i_58_ >>> 5 ^ i_58_ << 4) + i_58_ ^ i_59_ + is[i_59_ & 0x3];
			}
			pos -= 8;
			p4(i_57_);
			p4(i_58_);
		}
		pos = i_54_;
	}

	public void writeByteC(int i_62_) {
		data[pos++] = (byte) -i_62_;
	}

	public void readBytesA(byte[] dest, int offs, int length) {
		for (int i = offs; i < length + offs; i++) {
			dest[i] = (byte) (data[pos++] - 128);
		}
	}

	public int readUnsignedLEShort() {
		pos += 2;
		return (data[pos - 1] << 8 & 0xff00) + (data[pos - 2] & 0xff);
	}

	public void method2603(int i_66_) {
		data[pos++] = (byte) (-i_66_ + 128);
	}

	public void method2604(int i) {
		data[pos - i - 1] = (byte) i;
	}

	public int readUnsignedByteC() {
		return -data[pos++] & 0xff;
	}

	public byte g1b() {
		return data[pos++];
	}

	public void psize4(int offset) {
		data[pos - offset - 4] = (byte) (offset >> 24);
		data[pos - offset - 3] = (byte) (offset >> 16);
		data[pos - offset - 2] = (byte) (offset >> 8);
		data[pos - offset - 1] = (byte) offset;
	}

	public String gstrnull() {
		if (data[pos] == 0) {
			pos++;
			return null;
		}
		return gstr();
	}

	public long g8() {
		long l = g4() & 0xffffffffL;
		long l_70_ = g4() & 0xffffffffL;
		return (l << 32) - -l_70_;
	}

	public void p1(int i) {
		data[pos++] = (byte) i;
	}

	public void method2611(int i_72_) {
		if (i_72_ >= 0 && i_72_ < 128) {
			p1(i_72_);
		} else if (i_72_ >= 0 && i_72_ < 32768) {
			p2(i_72_ + 32768);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public void method2612(int[] is) {
		int i_73_ = pos / 8;
		pos = 0;
		for (int i_74_ = 0; i_74_ < i_73_; i_74_++) {
			int i_75_ = g4();
			int i_76_ = g4();
			int i_77_ = -957401312;
			int i_78_ = -1640531527;
			int i_79_ = 32;
			while (i_79_-- > 0) {
				i_76_ -= (i_75_ >>> 5 ^ i_75_ << 4) + i_75_ ^ is[(i_77_ & 0x1d57) >>> 11] + i_77_;
				i_77_ -= i_78_;
				i_75_ -= i_77_ + is[i_77_ & 0x3] ^ (i_76_ >>> 5 ^ i_76_ << 4) + i_76_;
			}
			pos -= 8;
			p4(i_75_);
			p4(i_76_);
		}
	}

	public void endBytePacket(int i) {
		data[pos - i - 2] = (byte) (i >> 8);
		data[-i - 1 + pos] = (byte) i;
	}

	public long method2614() {
		long l = g1() & 0xffffffffL;
		long l_80_ = g4() & 0xffffffffL;
		return l_80_ + (l << 32);
	}

	public int readUnsignedShortA() {
		pos += 2;
		return (data[pos - 1] - 128 & 0xff) + ((data[pos - 2] & 0xff) << 8);
	}

	public void readBytes(byte[] is, int i_82_, int i_83_) {
		for (int i_84_ = i_82_; i_83_ + i_82_ > i_84_; i_84_++) {
			is[i_84_] = data[pos++];
		}
	}

	public void pvarint(int i) {
		if (0 != (i & ~0x7f)) {
			if ((i & ~0x3fff) != 0) {
				if ((i & ~0x1fffff) != 0) {
					if (0 != (i & ~0xfffffff)) {
						p1(i >>> 28 | 0x80);
					}
					p1(i >>> 21 | 0x80);
				}
				p1(i >>> 14 | 0x80);
			}
			p1(i >>> 7 | 0x80);
		}
		p1(i & 0x7f);
	}

	public boolean method2618() {
		pos -= 4;
		int i_86_ = ClotchesLoader.method303(24443, pos, data, 0);
		int i_87_ = g4();
		if (i_86_ == i_87_) {
			return true;
		}
		return false;
	}

	public void method2619(String string) {
		int i_88_ = string.indexOf('\0');
		if (i_88_ >= 0) {
			throw new IllegalArgumentException("NUL character at " + i_88_ + " - cannot pjstr2");
		}
		data[pos++] = (byte) 0;
		pos += Class360.method3735(string, 0, data, pos, -210, string.length());
		data[pos++] = (byte) 0;
	}

	public void method2620(int i) {
		data[pos++] = (byte) (i + 128);
	}

	public Packet(int i) {
		pos = 0;
		data = Class370.method3873(100, i);
	}

	public Packet(byte[] is) {
		data = is;
		pos = 0;
	}

	static {
		anIntArray4677 = new int[5];
	}

	public int gSmart1or2n() {
		int i_29_ = data[pos] & 0xff;
		if (i_29_ < 128) {
			return g1() - 1;
		}
		return g2() - 32769;
	}
}
