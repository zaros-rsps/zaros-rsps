package net.zaros.client;
/* Class204 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;
import java.math.BigInteger;

final class Connection {
	private NodeDeque aClass155_2044 = new NodeDeque();
	Class154 aClass154_2045;
	static OutgoingPacket aClass311_2046 = new OutgoingPacket(26, 16);
	Class278 aClass278_2047;
	static IncomingPacket aClass231_2048 = new IncomingPacket(7, 20);
	private Packet aClass296_Sub17_2049;
	private int anInt2050 = 0;
	ByteStream in_stream;
	static BigInteger aBigInteger2052 = (new BigInteger("882d31960607d44ab473d60056aecd87b436003b084ff914e107434cbe23573b0332e4a875b4ba3cd53f6c9e2d0461f442ba0ac2c4460ab84e720c61a28424881955dfa8d8ff915e57674cf5819c636c7480ca3d20a179c406238d1612b178d7e4fb27077593dbdc64c2ecdcacc52561fee8516faa2e6b69b79103858564236d6b74ef07d88ca97ccba1cfd2d1e9cf5101e4d246f6bddf56a6a21bbecf0e0e5085710e838ca5283e85e1c77ba9827233be44218df337282f1f0805321fe0ea649f7b83ecfd41eb796bd3394172e8dfd4c5ddadcd6605070dca41f95d35e3cbce8824396a830635a459cd759df8f98b8c3950bd32a22ced7063148de589f2badd338d74c094f704ccfe61ddcbab0f69df7323ce33b5152cd58896e2c42aa8953edfa0ad5260b5c653338592940cf021bca738b280e331265bfb79f7ff8952600b05d0c48bfe95958305c616b4e38fa558a31045faa229732cc525e36fec2849b1c9956074040af192ae41863351bd9585e0067e3d7a456ff4c3134492eeff0a0535e7c0da5578d7c8993581ec4ad7db3b2cc7bcac75e8d8d14c42ec33dbb3843437464461057ca1f0cf04fef343588fbed42d99d5aafe9faed4d6956cd3a2059cff36bb35f38fb11bd398d7fd67c63c0fd6dde7099b3a444eff2eea498b0bb271318f38bc6e1b27b67f61907dfef2d0a067a4f6f5c037552f451e3d2c042a751f", 16));
	ISAACCipher aClass185_2053;
	int totalBytesReceived;
	int anInt2055;
	int protSize;
	IncomingPacket incomingPacket;
	IncomingPacket incomingPacket1;
	IncomingPacket incomingPacket2;
	int anInt2060;
	private int anInt2061;
	int anInt2062;
	int anInt2063;
	boolean aBoolean2064;
	boolean aBoolean2065;
	IncomingPacket incomingPacket3;

	final void method1963(boolean bool) throws IOException {
		if (bool != true)
			method1964((byte) 104);
		if (aClass154_2045 != null && anInt2050 > 0) {
			aClass296_Sub17_2049.pos = 0;
			for (;;) {
				Class296_Sub1 class296_sub1 = (Class296_Sub1) aClass155_2044.removeFirst((byte) 121);
				if (class296_sub1 == null || (class296_sub1.anInt4587 > (-aClass296_Sub17_2049.pos + aClass296_Sub17_2049.data.length)))
					break;
				aClass296_Sub17_2049.writeBytes((class296_sub1.out.data), 0, class296_sub1.anInt4587);
				anInt2050 -= class296_sub1.anInt4587;
				class296_sub1.unlink();
				class296_sub1.out.method2550();
				class296_sub1.method2433((byte) -86);
			}
			aClass154_2045.method1558(0, aClass296_Sub17_2049.data, -7979, aClass296_Sub17_2049.pos);
			anInt2061 += aClass296_Sub17_2049.pos;
			anInt2060 = 0;
		}
	}

	public static void method1964(byte i) {
		if (i <= 27)
			method1964((byte) -6);
		aClass231_2048 = null;
		aClass311_2046 = null;
		aBigInteger2052 = null;
	}

	static final void method1965(long[] ls, int i, int[] is) {
		Class296_Sub51_Sub38.method3197(0, i + 19852, ls.length - 1, is, ls);
		if (i != -19851)
			method1964((byte) -30);
	}

	final void method1966(int i) {
		if (aClass154_2045 != null) {
			aClass154_2045.method1561(124);
			aClass154_2045 = null;
		}
		if (i != 320)
			aClass154_2045 = null;
	}

	static final void method1967(int i, long l) {
		int i_0_ = ((Class296_Sub51_Sub11.localPlayer.tileX) + Class4.anInt71);
		int i_1_ = (Class41_Sub8.anInt3763 + (Class296_Sub51_Sub11.localPlayer.tileY));
		if (Class296_Sub14.anInt4668 - i_0_ < -2000 || Class296_Sub14.anInt4668 - i_0_ > 2000 || -i_1_ + Class296_Sub24.anInt4762 < -2000 || -i_1_ + Class296_Sub24.anInt4762 > 2000) {
			Class296_Sub14.anInt4668 = i_0_;
			Class296_Sub24.anInt4762 = i_1_;
		}
		if (Class296_Sub14.anInt4668 != i_0_) {
			int i_2_ = -Class296_Sub14.anInt4668 + i_0_;
			int i_3_ = (int) (l * (long) i_2_ / 320L);
			if (i_2_ > 0) {
				if (i_3_ != 0) {
					if (i_3_ > i_2_)
						i_3_ = i_2_;
				} else
					i_3_ = 1;
			} else if (i_3_ == 0)
				i_3_ = -1;
			else if (i_2_ > i_3_)
				i_3_ = i_2_;
			Class296_Sub14.anInt4668 += i_3_;
		}
		Class153.aFloat1572 += Class304.aFloat2733 * (float) l / 6.0F;
		if (i_1_ != Class296_Sub24.anInt4762) {
			int i_4_ = i_1_ - Class296_Sub24.anInt4762;
			int i_5_ = (int) (l * (long) i_4_ / 320L);
			if (i_4_ > 0) {
				if (i_5_ != 0) {
					if (i_4_ < i_5_)
						i_5_ = i_4_;
				} else
					i_5_ = 1;
			} else if (i_5_ != 0) {
				if (i_4_ > i_5_)
					i_5_ = i_4_;
			} else
				i_5_ = -1;
			Class296_Sub24.anInt4762 += i_5_;
		}
		Class41_Sub26.aFloat3806 += Class296_Sub36.aFloat4865 * (float) l / 6.0F;
		Class123_Sub2_Sub1.method1074(i ^ 0x3);
		if (i != -1)
			aBigInteger2052 = null;
	}

	final void method1968(int i) {
		aClass155_2044.method1581(i + 327680);
		anInt2050 = i;
	}

	final void method1969(int i) {
		if (Class29.anInt307 % 50 == 0) {
			anInt2055 = anInt2061;
			anInt2061 = 0;
			anInt2063 = totalBytesReceived;
			totalBytesReceived = 0;
		}
		int i_6_ = 110 / ((i + 55) / 36);
	}

	final void sendPacket(Class296_Sub1 class296_sub1, byte i) {
		if (i == 119) {
			aClass155_2044.addLast((byte) 109, class296_sub1);
			class296_sub1.anInt4587 = class296_sub1.out.pos;
			anInt2050 += class296_sub1.anInt4587;
			class296_sub1.out.pos = 0;
		}
	}

	public Connection() {
		aClass296_Sub17_2049 = new Packet(1350);
		in_stream = new ByteStream(15000);
		protSize = 0;
		incomingPacket = null;
		anInt2060 = 0;
		aBoolean2064 = true;
		anInt2062 = 0;
		aBoolean2065 = false;
	}
}
