package net.zaros.client;
/* Class357 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Color;

final class Class357 {
	static int anInt3082 = 0;
	static Color[] aColorArray3083 = {new Color(16777215), new Color(16777215), new Color(16741381), new Color(16741381)};

	static final void method3709(int i, int i_0_, int i_1_, int i_2_, int i_3_, ha var_ha) {
		Class296_Sub51_Sub36.aHa6529 = var_ha;
		Class373_Sub3.aClass373_5612 = Class296_Sub51_Sub36.aHa6529.m();
		Class338_Sub9.aClass373_5269 = Class296_Sub51_Sub36.aHa6529.m();
		Class94.aClass373_1016 = Class296_Sub51_Sub36.aHa6529.m();
		ConfigsRegister.anInt3674 = 0;
		IOException_Sub1.anInterface13Array38 = null;
		Class347.anInt3026 = i_0_;
		BillboardRaw.anIntArray1080 = null;
		Class41_Sub2.anInt3741 = i_2_;
		Class296_Sub15_Sub3.method2539(i_1_, i_3_, (byte) -116);
		EmissiveTriangle.anInt958 = -1;
		Class368_Sub23.anInt5569 = -1;
		Class252.anInt2386 = -1;
		if (i != 16777215)
			method3710((byte) 122);
	}

	public static void method3710(byte i) {
		if (i == 75)
			aColorArray3083 = null;
	}

	public Class357() {
		/* empty */
	}

	final int method3711(int i, int i_4_, byte i_5_) {
		if (i_5_ <= 55)
			return 67;
		int i_6_ = Class384.anInt3254 <= i ? i : Class384.anInt3254;
		if (this == Class296_Sub50.aClass357_5028)
			return 0;
		if (this == Class368_Sub18.aClass357_5534)
			return -i_4_ + i_6_;
		if (r_Sub2.aClass357_6717 == this)
			return (-i_4_ + i_6_) / 2;
		return 0;
	}

	public final String toString() {
		throw new IllegalStateException();
	}
}
