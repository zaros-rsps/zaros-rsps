package net.zaros.client;

/* Class391 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class391 {
	int anInt3284;
	byte aByte3285;
	Class296_Sub18 aClass296_Sub18_3286;
	Class296_Sub19_Sub1 aClass296_Sub19_Sub1_3287;
	Class296_Sub45_Sub1 aClass296_Sub45_Sub1_3288;
	int anInt3289;
	Class338_Sub3 aClass338_Sub3_3290;
	static Class294 aClass294_3291 = new Class294(2, 2);
	int anInt3292;
	int anInt3293;
	int anInt3294;
	int anInt3295;
	static OutgoingPacket aClass311_3296 = new OutgoingPacket(84, 8);
	static int anInt3297;
	static int anInt3298;
	static int anInt3299 = 0;
	Class171 aClass171_3300;

	static final void method4040(int i) {
		if (i > -119)
			aClass311_3296 = null;
		Class296_Sub51_Sub38.anIntArray6538 = null;
		Class318.aBoolean2809 = false;
		InterfaceComponentSettings.anIntArray4825 = null;
		Class401.anIntArray3365 = null;
		Class296_Sub4.anIntArray4615 = null;
		Class41_Sub3.anIntArray3743 = null;
	}

	static final Packet method4041(byte i) {
		Packet class296_sub17 = Class368_Sub2.method3811(true);
		class296_sub17.writeLong(0L);
		int i_0_ = 28 % ((19 - i) / 40);
		class296_sub17.writeString(Class379.aString3625);
		class296_sub17.writeLong(Class238.aLong3633);
		class296_sub17.writeLong(Class368_Sub1.aLong5424);
		class296_sub17.writeRsaEncryption(RSA.EXPONENT, RSA.MODULUS);
		return class296_sub17;
	}

	static final void method4042(int i) {
		if (i != 0)
			aClass294_3291 = null;
		Class328.aClass155_2912 = new NodeDeque();
	}

	public static void method4043(byte i) {
		aClass294_3291 = null;
		if (i == -45)
			aClass311_3296 = null;
	}

	final boolean method4044(int i) {
		if (i != 372)
			method4042(-7);
		if (aByte3285 != 2 && aByte3285 != 3)
			return false;
		return true;
	}

	Class391(byte i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, Class338_Sub3 class338_sub3) {
		anInt3294 = i_4_;
		anInt3292 = i_3_;
		anInt3289 = i_2_;
		aByte3285 = i;
		anInt3284 = i_6_;
		aClass338_Sub3_3290 = class338_sub3;
		anInt3295 = i_1_;
		anInt3293 = i_5_;
	}
}
