package net.zaros.client;
/* dxVertexLayout - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jagdx.IDirect3DVertexDeclaration;
import jagdx.VertexElementCollection;

public class dxVertexLayout extends Class127 {
	IDirect3DVertexDeclaration anIDirect3DVertexDeclaration4281;

	dxVertexLayout(ha_Sub1_Sub2 var_ha_Sub1_Sub2, Class211[] class211s) {
		VertexElementCollection vertexelementcollection = new VertexElementCollection(var_ha_Sub1_Sub2.aVba5870);
		int i = 0;
		for (int i_0_ = 0; i_0_ < class211s.length; i_0_++) {
			int i_1_ = 0;
			Class211 class211 = class211s[i_0_];
			for (int i_2_ = 0; i_2_ < class211.method2015(192); i_2_++) {
				Class356 class356 = class211.method2018(true, i_2_);
				if (class356 == Class356.aClass356_3072)
					vertexelementcollection.addElement(i_0_, 2, 0, 0, 0, i_1_);
				else if (class356 != Class356.aClass356_3073) {
					if (class356 == Class356.aClass356_3075)
						vertexelementcollection.addElement(i_0_, 4, 0, 10, 0, i_1_);
					else if (Class356.aClass356_3076 == class356)
						vertexelementcollection.addElement(i_0_, 0, 0, 5, i++, i_1_);
					else if (Class356.aClass356_3077 != class356) {
						if (class356 != Class356.aClass356_3078) {
							if (class356 == Class356.aClass356_3079)
								vertexelementcollection.addElement(i_0_, 3, 0, 5, i++, i_1_);
						} else
							vertexelementcollection.addElement(i_0_, 2, 0, 5, i++, i_1_);
					} else
						vertexelementcollection.addElement(i_0_, 1, 0, 5, i++, i_1_);
				} else
					vertexelementcollection.addElement(i_0_, 2, 0, 3, 0, i_1_);
				i_1_ += class356.anInt3074;
			}
		}
		vertexelementcollection.finish();
		anIDirect3DVertexDeclaration4281 = var_ha_Sub1_Sub2.anIDirect3DDevice5865.a(vertexelementcollection, null);
	}
}
