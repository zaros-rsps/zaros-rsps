package net.zaros.client;
import jaggl.OpenGL;

final class Class366_Sub9 extends Class366 {
	private Class341 aClass341_5415;
	private Class69_Sub4 aClass69_Sub4_5416;
	static OutgoingPacket aClass311_5417 = new OutgoingPacket(46, -1);
	private Class361 aClass361_5418;
	static AdvancedMemoryCache aClass113_5419 = new AdvancedMemoryCache(8);
	static Class398 aClass398_5420;
	static int[] anIntArray5421 = new int[1000];

	private static final byte[] method3796(byte[] is, int i, int i_0_, byte i_1_) {
		byte[] is_2_ = new byte[i_0_];
		ArrayTools.removeElement(is, i, is_2_, 0, i_0_);
		if (i_1_ > -32)
			method3796(null, 60, -117, (byte) 70);
		return is_2_;
	}

	final boolean method3763(int i) {
		int i_3_ = 12 % ((i - 73) / 40);
		return true;
	}

	public static void method3797(int i) {
		anIntArray5421 = null;
		aClass311_5417 = null;
		aClass398_5420 = null;
		aClass113_5419 = null;
		if (i != 3)
			method3797(-126);
	}

	final void method3768(byte i, boolean bool) {
		if (aHa_Sub3_3121.anInt4181 > 0) {
			float f = -0.5F / (float) aHa_Sub3_3121.anInt4181;
			aHa_Sub3_3121.method1330(127, 1);
			Class296_Sub51_Sub39.aFloatArray6550[0] = 0.0F;
			Class296_Sub51_Sub39.aFloatArray6550[1] = 0.0F;
			Class296_Sub51_Sub39.aFloatArray6550[3] = aHa_Sub3_3121.aFloat4237 * f + 0.25F;
			Class296_Sub51_Sub39.aFloatArray6550[2] = f;
			OpenGL.glPushMatrix();
			OpenGL.glLoadIdentity();
			OpenGL.glTexGenfv(8192, 9474, Class296_Sub51_Sub39.aFloatArray6550, 0);
			OpenGL.glPopMatrix();
			aHa_Sub3_3121.method1322(0.5F, 65, (float) aHa_Sub3_3121.anInt4181);
			aHa_Sub3_3121.method1316(aClass69_Sub4_5416, (byte) -125);
			aHa_Sub3_3121.method1330(i + 201, 0);
		}
		if (i == -88) {
			aClass341_5415.method3625((byte) 66, '\0');
			OpenGL.glMatrixMode(5890);
			OpenGL.glPushMatrix();
			OpenGL.glScalef(0.25F, 0.25F, 1.0F);
			OpenGL.glMatrixMode(5888);
		}
	}

	final void method3770(byte i, boolean bool) {
		if (i != 33)
			method3768((byte) -58, true);
		aHa_Sub3_3121.method1306(260, 8448, i - 22427);
	}

	private final void method3798(int i) {
		aClass341_5415 = new Class341(aHa_Sub3_3121, 2);
		aClass341_5415.method3627((byte) -124, 0);
		aHa_Sub3_3121.method1330(118, 1);
		aHa_Sub3_3121.method1306(7681, 260, -22394);
		aHa_Sub3_3121.method1283(0, 34168, 768, (byte) -121);
		OpenGL.glTexGeni(8192, 9472, 9216);
		OpenGL.glEnable(3168);
		aHa_Sub3_3121.method1330(119, 0);
		if (i < 63)
			aClass398_5420 = null;
		OpenGL.glTexEnvf(8960, 34163, 2.0F);
		if (aClass361_5418.aBoolean3102) {
			OpenGL.glTexGeni(8194, 9472, 9217);
			OpenGL.glTexGeni(8195, 9472, 9217);
			OpenGL.glTexGenfv(8195, 9473, new float[]{0.0F, 0.0F, 0.0F, 1.0F}, 0);
			OpenGL.glEnable(3170);
			OpenGL.glEnable(3171);
		}
		aClass341_5415.method3622(-125);
		aClass341_5415.method3627((byte) -101, 1);
		aHa_Sub3_3121.method1330(124, 1);
		aHa_Sub3_3121.method1306(8448, 8448, -22394);
		aHa_Sub3_3121.method1283(0, 5890, 768, (byte) -127);
		OpenGL.glDisable(3168);
		aHa_Sub3_3121.method1330(117, 0);
		OpenGL.glTexEnvf(8960, 34163, 1.0F);
		if (aClass361_5418.aBoolean3102) {
			OpenGL.glDisable(3170);
			OpenGL.glDisable(3171);
		}
		aClass341_5415.method3622(-122);
	}

	Class366_Sub9(ha_Sub3 var_ha_Sub3, Class361 class361) {
		super(var_ha_Sub3);
		aClass361_5418 = class361;
		method3798(75);
		aClass69_Sub4_5416 = new Class69_Sub4(aHa_Sub3_3121, 6406, 2, new byte[]{0, -1}, 6406);
		aClass69_Sub4_5416.method744(94, false);
	}

	final void method3766(int i) {
		aClass341_5415.method3625((byte) 99, '\001');
		if (aHa_Sub3_3121.anInt4181 > 0) {
			aHa_Sub3_3121.method1330(116, 1);
			aHa_Sub3_3121.method1316(null, (byte) -124);
			aHa_Sub3_3121.method1322(1.0F, 99, 0.0F);
			aHa_Sub3_3121.method1330(118, 0);
		}
		aHa_Sub3_3121.method1306(8448, 8448, -22394);
		if (i < 30)
			method3797(90);
		OpenGL.glMatrixMode(5890);
		OpenGL.glPopMatrix();
		OpenGL.glMatrixMode(5888);
	}

	final void method3769(int i, byte i_4_, int i_5_) {
		if ((i & 0x1) != 1) {
			if (!aClass361_5418.aBoolean3102)
				aHa_Sub3_3121.method1316((aClass361_5418.aClass69_Sub1Array3095[0]), (byte) -103);
			else {
				aHa_Sub3_3121.method1316(aClass361_5418.aClass69_Sub2_3097, (byte) -123);
				Class296_Sub51_Sub39.aFloatArray6550[3] = 0.0F;
				Class296_Sub51_Sub39.aFloatArray6550[2] = 0.0F;
				Class296_Sub51_Sub39.aFloatArray6550[0] = 0.0F;
				Class296_Sub51_Sub39.aFloatArray6550[1] = 0.0F;
				OpenGL.glTexGenfv(8194, 9473, Class296_Sub51_Sub39.aFloatArray6550, 0);
			}
		} else if (aClass361_5418.aBoolean3102) {
			aHa_Sub3_3121.method1316(aClass361_5418.aClass69_Sub2_3097, (byte) -116);
			Class296_Sub51_Sub39.aFloatArray6550[1] = 0.0F;
			Class296_Sub51_Sub39.aFloatArray6550[2] = 0.0F;
			Class296_Sub51_Sub39.aFloatArray6550[0] = 0.0F;
			Class296_Sub51_Sub39.aFloatArray6550[3] = (float) (aHa_Sub3_3121.anInt4134 % 4000) / 4000.0F;
			OpenGL.glTexGenfv(8194, 9473, Class296_Sub51_Sub39.aFloatArray6550, 0);
		} else {
			int i_6_ = aHa_Sub3_3121.anInt4134 % 4000 * 16 / 4000;
			aHa_Sub3_3121.method1316((aClass361_5418.aClass69_Sub1Array3095[i_6_]), (byte) -105);
		}
		if (i_4_ != -81)
			method3763(91);
	}

	final void method3764(boolean bool, int i, Class69 class69) {
		if (bool)
			method3763(-86);
	}

	static final byte[] method3799(int i, int i_7_, Object object, int i_8_) {
		if (object == null)
			return null;
		if (object instanceof byte[]) {
			byte[] is = (byte[]) object;
			return method3796(is, i, i_8_, (byte) -89);
		}
		if (i_7_ != 0)
			method3797(-123);
		if (object instanceof Class104) {
			Class104 class104 = (Class104) object;
			return class104.method901(-123, i, i_8_);
		}
		throw new IllegalArgumentException();
	}
}
