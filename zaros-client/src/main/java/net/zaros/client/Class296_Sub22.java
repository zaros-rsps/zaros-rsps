package net.zaros.client;
/* Class296_Sub22 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;

import jaclib.memory.Buffer;
import jaclib.memory.Stream;

final class Class296_Sub22 extends Node {
	private int anInt4721;
	static Js5 aClass138_4722;
	private HashTable aClass263_4723;
	private int anInt4724;
	private float[][] aFloatArrayArray4725;
	private Class296_Sub35 aClass296_Sub35_4726;
	private Interface15_Impl2 anInterface15_Impl2_4727;
	private float[][] aFloatArrayArray4728;
	private int anInt4729;
	private int anInt4730;
	static ParamTypeList itemExtraDataDefinitionLoader;
	private Stream aStream4732;
	static Js5 aClass138_4733;
	private int anInt4734;
	static boolean memb2 = false;
	private Interface15_Impl1 anInterface15_Impl1_4736;
	private int anInt4737;
	private s_Sub3 aS_Sub3_4738;
	static int anInt4739 = 0;
	private Stream aStream4740;
	private ha_Sub1 aHa_Sub1_4741;
	private float[][] aFloatArrayArray4742;

	static final Class296_Sub41 method2659(Js5 class138, byte i, int i_0_) {
		byte[] is = class138.get(i_0_);
		if (is == null) {
			return null;
		}
		if (i != 0) {
			return null;
		}
		return new Class296_Sub41(is);
	}

	public static void method2660(byte i) {
		aClass138_4733 = null;
		if (i != -47) {
			method2662(-49, null, null, 114);
		}
		aClass138_4722 = null;
		itemExtraDataDefinitionLoader = null;
	}

	private final void method2661(int i, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_) {
		long l = -1L;
		int i_7_ = (i_5_ << aS_Sub3_4738.anInt2835) + i_2_;
		int i_8_ = (i_4_ << aS_Sub3_4738.anInt2835) + i_3_;
		int i_9_ = aS_Sub3_4738.method3349(0, i_8_, i_7_);
		if ((i_2_ & 0x7f) == 0 || (i_3_ & 0x7f) == 0) {
			l = 65535L << 16 & (long) i_8_ << 16 | i_7_ & 0xffffL;
			Node class296 = aClass263_4723.get(l);
			if (class296 != null) {
				method2665(1, ((Class296_Sub20) class296).aShort4715);
				return;
			}
		}
		short i_10_ = (short) anInt4721++;
		if (l != -1L) {
			aClass263_4723.put(l, new Class296_Sub20(i_10_));
		}
		float f;
		float f_11_;
		float f_12_;
		if (i_2_ == 0 && i_3_ == 0) {
			f_12_ = aFloatArrayArray4725[i_6_][i_1_];
			f_11_ = aFloatArrayArray4742[i_6_][i_1_];
			f = aFloatArrayArray4728[i_6_][i_1_];
		} else if (i_2_ != aS_Sub3_4738.anInt2836 || i_3_ != 0) {
			if (i_2_ != aS_Sub3_4738.anInt2836 || aS_Sub3_4738.anInt2836 != i_3_) {
				if (i_2_ == 0 && aS_Sub3_4738.anInt2836 == i_3_) {
					f = aFloatArrayArray4728[i_6_][i_1_ + 1];
					f_11_ = aFloatArrayArray4742[i_6_][i_1_ + 1];
					f_12_ = aFloatArrayArray4725[i_6_][i_1_ + 1];
				} else {
					float f_13_ = (float) i_2_ / (float) aS_Sub3_4738.anInt2836;
					float f_14_ = (float) i_3_ / (float) aS_Sub3_4738.anInt2836;
					float f_15_ = aFloatArrayArray4725[i_6_][i_1_];
					float f_16_ = aFloatArrayArray4728[i_6_][i_1_];
					float f_17_ = aFloatArrayArray4742[i_6_][i_1_];
					float f_18_ = aFloatArrayArray4725[i_6_ + 1][i_1_];
					float f_19_ = aFloatArrayArray4728[i_6_ + 1][i_1_];
					f_17_ += f_13_ * (aFloatArrayArray4742[i_6_][i_1_ + 1] - f_17_);
					float f_20_ = aFloatArrayArray4742[i_6_ + 1][i_1_];
					f_19_ += f_13_ * (aFloatArrayArray4728[i_6_ + 1][i_1_ + 1] - f_19_);
					f_18_ += (aFloatArrayArray4725[i_6_ + 1][i_1_ + 1] - f_18_) * f_13_;
					f_15_ += f_13_ * (aFloatArrayArray4725[i_6_][i_1_ + 1] - f_15_);
					f_16_ += (-f_16_ + aFloatArrayArray4728[i_6_][i_1_ + 1]) * f_13_;
					f = f_16_ + (-f_16_ + f_19_) * f_14_;
					f_20_ += f_13_ * (-f_20_ + aFloatArrayArray4742[i_6_ + 1][i_1_ + 1]);
					f_12_ = f_15_ + f_14_ * (f_18_ - f_15_);
					f_11_ = f_17_ + f_14_ * (f_20_ - f_17_);
				}
			} else {
				f_12_ = aFloatArrayArray4725[i_6_ + 1][i_1_ + 1];
				f_11_ = aFloatArrayArray4742[i_6_ + 1][i_1_ + 1];
				f = aFloatArrayArray4728[i_6_ + 1][i_1_ + 1];
			}
		} else {
			f = aFloatArrayArray4728[i_6_ + 1][i_1_];
			f_12_ = aFloatArrayArray4725[i_6_ + 1][i_1_];
			f_11_ = aFloatArrayArray4742[i_6_ + 1][i_1_];
		}
		float f_21_ = aClass296_Sub35_4726.method2749(true) - i_7_;
		float f_22_ = -i_9_ + aClass296_Sub35_4726.method2751(-28925);
		float f_23_ = -i_8_ + aClass296_Sub35_4726.method2750(-4444);
		float f_24_ = (float) Math.sqrt(f_23_ * f_23_ + (f_21_ * f_21_ + f_22_ * f_22_));
		float f_25_ = 1.0F / f_24_;
		f_21_ *= f_25_;
		f_22_ *= f_25_;
		f_23_ *= f_25_;
		float f_26_ = f_24_ / aClass296_Sub35_4726.method2748(50);
		float f_27_ = -(f_26_ * f_26_) + 1.0F;
		if (f_27_ < 0.0F) {
			f_27_ = 0.0F;
		}
		float f_28_ = f_12_ * f_21_ + f_22_ * f + f_11_ * f_23_;
		if (f_28_ < 0.0F) {
			f_28_ = 0.0F;
		}
		float f_29_ = f_28_ * f_27_ * 2.0F;
		if (f_29_ > 1.0F) {
			f_29_ = 1.0F;
		}
		int i_30_ = aClass296_Sub35_4726.method2746(-24996);
		int i_31_ = (int) (f_29_ * ((i_30_ & 0xff025b) >> 16));
		if (i_31_ > 255) {
			i_31_ = 255;
		}
		if (i <= 22) {
			aHa_Sub1_4741 = null;
		}
		int i_32_ = (int) ((i_30_ >> 8 & 0xff) * f_29_);
		if (i_32_ > 255) {
			i_32_ = 255;
		}
		int i_33_ = (int) ((i_30_ & 0xff) * f_29_);
		if (!Stream.a()) {
			aStream4740.b((float) i_7_);
			aStream4740.b((float) i_9_);
			aStream4740.b((float) i_8_);
		} else {
			aStream4740.a((float) i_7_);
			aStream4740.a((float) i_9_);
			aStream4740.a((float) i_8_);
		}
		if (i_33_ > 255) {
			i_33_ = 255;
		}
		if (aHa_Sub1_4741.anInt3956 != 0) {
			aStream4740.e(i_31_);
			aStream4740.e(i_32_);
			aStream4740.e(i_33_);
		} else {
			aStream4740.e(i_33_);
			aStream4740.e(i_32_);
			aStream4740.e(i_31_);
		}
		aStream4740.e(255);
		method2665(1, i_10_);
	}

	static final void method2662(int i, Class338_Sub3 class338_sub3, Animation class43, int i_34_) {
		if (Class296_Sub51_Sub1.anInt6335 < 50 && class43 != null && class43.anIntArrayArray418 != null && class43.anIntArrayArray418.length > i_34_ && class43.anIntArrayArray418[i_34_] != null) {
			int i_35_ = class43.anIntArrayArray418[i_34_][0];
			int i_36_ = i_35_ >> 8;
			int i_37_ = (i_35_ & 0xff) >> 5;
			if (class43.anIntArrayArray418[i_34_].length > 1) {
				int i_38_ = (int) (Math.random() * class43.anIntArrayArray418[i_34_].length);
				if (i_38_ > 0) {
					i_36_ = class43.anIntArrayArray418[i_34_][i_38_];
				}
			}
			int i_39_ = i_35_ & 0x1f;
			int i_40_ = 256;
			if (class43.anIntArray415 != null && class43.anIntArray400 != null) {
				i_40_ = (int) ((class43.anIntArray400[i_34_] - class43.anIntArray415[i_34_]) * Math.random()) + class43.anIntArray415[i_34_];
			}
			int i_41_ = class43.anIntArray416 == null ? 255 : class43.anIntArray416[i_34_];
			if (i_39_ == 0) {
				if (class338_sub3 == Class296_Sub51_Sub11.localPlayer) {
					if (!class43.aBoolean409) {
						Class296_Sub51_Sub38.method3194(-99, i_36_, i_40_, i_41_, i_37_, 0);
					} else {
						Class338_Sub3_Sub4.method3571(i_41_, false, i_40_, 0, i_36_, i + 115, i_37_);
					}
				}
			} else if ((Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub23_5005.method489(125) ^ 0xffffffff) != i) {
				int i_42_ = class338_sub3.tileX - 256 >> 9;
				int i_43_ = class338_sub3.tileY - 256 >> 9;
				int i_44_ = class338_sub3 == Class296_Sub51_Sub11.localPlayer ? 0 : i_39_ + (i_43_ << 8) + (i_42_ << 16) + (class338_sub3.z << 24);
				Class336.aClass391Array2956[Class296_Sub51_Sub1.anInt6335++] = new Class391(class43.aBoolean409 ? (byte) 2 : (byte) 1, i_36_, i_37_, 0, i_41_, i_44_, i_40_, class338_sub3);
			}
		}
	}

	static final Class296_Sub29 method2663(Canvas canvas, int i, int i_45_, int i_46_) {
		try {
			Class296_Sub29 class296_sub29 = new Class296_Sub29_Sub1();
			class296_sub29.method2689(canvas, i_46_, i_45_, 78);
			return class296_sub29;
		} catch (Throwable throwable) {
			Class296_Sub29_Sub2 class296_sub29_sub2 = new Class296_Sub29_Sub2();
			class296_sub29_sub2.method2689(canvas, i_46_, i_45_, 90);
			return class296_sub29_sub2;
		}
	}

	final void method2664(int i, int i_47_, boolean[][] bools, int i_48_, int i_49_) {
		if (anInterface15_Impl1_4736 != null && anInt4737 <= i_48_ + i_47_ && -i_48_ + i_47_ <= anInt4724 && anInt4734 <= i_48_ + i && -i_48_ + i <= anInt4730) {
			int i_50_ = anInt4734;
			if (i_49_ > 63) {
				for (/**/; i_50_ <= anInt4730; i_50_++) {
					for (int i_51_ = anInt4737; anInt4724 >= i_51_; i_51_++) {
						int i_52_ = -i_47_ + i_51_;
						int i_53_ = -i + i_50_;
						if (-i_48_ < i_52_ && i_52_ < i_48_ && i_53_ > -i_48_ && i_48_ > i_53_ && bools[i_48_ + i_52_][i_48_ + i_53_]) {
							aHa_Sub1_4741.method1145((byte) (int) (aClass296_Sub35_4726.method2745((byte) 100) * 255.0F), 1);
							aHa_Sub1_4741.method1178(anInterface15_Impl2_4727, (byte) -118, 0);
							aHa_Sub1_4741.method1209(aHa_Sub1_4741.aClass127_4057, (byte) -107);
							aHa_Sub1_4741.method1232(Class166_Sub1.aClass289_4298, anInterface15_Impl1_4736, anInt4721, anInt4729 / 3, 0, 79, 0);
							return;
						}
					}
				}
			}
		}
	}

	private final void method2665(int i, short i_54_) {
		if (i != 1) {
			method2659(null, (byte) -107, 88);
		}
		if (Stream.a()) {
			aStream4732.d(i_54_);
		} else {
			aStream4732.a(i_54_);
		}
	}

	Class296_Sub22(ha_Sub1 var_ha_Sub1, s_Sub3 var_s_Sub3, Class296_Sub35 class296_sub35, int[] is) {
		aS_Sub3_4738 = var_s_Sub3;
		aHa_Sub1_4741 = var_ha_Sub1;
		aClass296_Sub35_4726 = class296_sub35;
		int i = aClass296_Sub35_4726.method2748(111) - (var_s_Sub3.anInt2836 >> 1);
		anInt4737 = aClass296_Sub35_4726.method2749(true) - i >> var_s_Sub3.anInt2835;
		anInt4724 = aClass296_Sub35_4726.method2749(true) + i >> var_s_Sub3.anInt2835;
		anInt4734 = -i + aClass296_Sub35_4726.method2750(-4444) >> var_s_Sub3.anInt2835;
		anInt4730 = aClass296_Sub35_4726.method2750(-4444) + i >> var_s_Sub3.anInt2835;
		int i_55_ = anInt4724 - anInt4737 + 1;
		int i_56_ = anInt4730 + -anInt4734 + 1;
		aFloatArrayArray4742 = new float[i_55_ + 1][i_56_ + 1];
		aFloatArrayArray4728 = new float[i_55_ + 1][i_56_ + 1];
		aFloatArrayArray4725 = new float[i_55_ + 1][i_56_ + 1];
		for (int i_57_ = 0; i_56_ >= i_57_; i_57_++) {
			int i_58_ = anInt4734 + i_57_;
			if (i_58_ > 0 && aS_Sub3_4738.anInt2834 - 1 > i_58_) {
				for (int i_59_ = 0; i_55_ >= i_59_; i_59_++) {
					int i_60_ = anInt4737 + i_59_;
					if (i_60_ > 0 && i_60_ < aS_Sub3_4738.anInt2832 - 1) {
						int i_61_ = var_s_Sub3.method3355(i_58_, (byte) -123, i_60_ + 1) - var_s_Sub3.method3355(i_58_, (byte) -121, i_60_ - 1);
						int i_62_ = var_s_Sub3.method3355(i_58_ + 1, (byte) -122, i_60_) - var_s_Sub3.method3355(i_58_ - 1, (byte) -108, i_60_);
						float f = (float) (1.0 / Math.sqrt(i_62_ * i_62_ + i_61_ * i_61_ + 65536));
						aFloatArrayArray4725[i_59_][i_57_] = f * i_61_;
						aFloatArrayArray4728[i_59_][i_57_] = f * -256.0F;
						aFloatArrayArray4742[i_59_][i_57_] = f * i_62_;
					}
				}
			}
		}
		int i_63_ = 0;
		for (int i_64_ = anInt4734; i_64_ <= anInt4730; i_64_++) {
			if (i_64_ < 0 || i_64_ >= var_s_Sub3.anInt2834) {
				i_63_ += -anInt4737 + anInt4724;
			} else {
				for (int i_65_ = anInt4737; anInt4724 >= i_65_; i_65_++) {
					if (i_65_ >= 0 && i_65_ < var_s_Sub3.anInt2832) {
						int i_66_ = is[i_63_];
						int[] is_67_ = var_s_Sub3.anIntArrayArrayArray5155[i_65_][i_64_];
						if (is_67_ != null && i_66_ != 0) {
							if (i_66_ == 1) {
								int i_68_ = 0;
								while (is_67_.length > i_68_) {
									if (is_67_[i_68_++] != -1 && is_67_[i_68_++] != -1 && is_67_[i_68_++] != -1) {
										anInt4729 += 3;
									}
								}
							} else {
								anInt4729 += 3;
							}
						}
					}
					i_63_++;
				}
			}
		}
		if (anInt4729 > 0) {
			aClass263_4723 = new HashTable(Class8.get_next_high_pow2(anInt4729));
			anInterface15_Impl1_4736 = aHa_Sub1_4741.method1154(false, 98);
			anInterface15_Impl1_4736.method32(anInt4729, -21709);
			jaclib.memory.heap.NativeHeapBuffer nativeheapbuffer = aHa_Sub1_4741.method1109(false, (byte) -54, anInt4729 * 16);
			aStream4740 = new Stream(nativeheapbuffer);
			for (;;) {
				Buffer buffer = anInterface15_Impl1_4736.method30((byte) -123, true);
				if (buffer != null) {
					aStream4732 = new Stream(buffer);
					i_63_ = 0;
					int i_69_ = 0;
					for (int i_70_ = anInt4734; i_70_ <= anInt4730; i_70_++) {
						if (i_70_ >= 0 && var_s_Sub3.anInt2834 > i_70_) {
							int i_71_ = 0;
							for (int i_72_ = anInt4737; anInt4724 >= i_72_; i_72_++) {
								if (i_72_ >= 0 && var_s_Sub3.anInt2832 > i_72_) {
									int i_73_ = is[i_63_];
									int[] is_74_ = var_s_Sub3.anIntArrayArrayArray5155[i_72_][i_70_];
									if (is_74_ != null && i_73_ != 0) {
										if (i_73_ == 1) {
											int[] is_75_ = var_s_Sub3.anIntArrayArrayArray5145[i_72_][i_70_];
											int[] is_76_ = var_s_Sub3.anIntArrayArrayArray5154[i_72_][i_70_];
											int i_77_ = 0;
											while (is_74_.length > i_77_) {
												if (is_74_[i_77_] == -1 || is_74_[i_77_ + 1] == -1 || is_74_[i_77_ + 2] == -1) {
													i_77_ += 3;
												} else {
													method2661(109, i_69_, is_75_[i_77_], is_76_[i_77_], i_70_, i_72_, i_71_);
													i_77_++;
													method2661(119, i_69_, is_75_[i_77_], is_76_[i_77_], i_70_, i_72_, i_71_);
													i_77_++;
													method2661(85, i_69_, is_75_[i_77_], is_76_[i_77_], i_70_, i_72_, i_71_);
													i_77_++;
												}
											}
										} else if (i_73_ != 3) {
											if (i_73_ != 2) {
												if (i_73_ != 5) {
													if (i_73_ == 4) {
														method2661(51, i_69_, 0, var_s_Sub3.anInt2836, i_70_, i_72_, i_71_);
														method2661(116, i_69_, 0, 0, i_70_, i_72_, i_71_);
														method2661(122, i_69_, var_s_Sub3.anInt2836, var_s_Sub3.anInt2836, i_70_, i_72_, i_71_);
													}
												} else {
													method2661(37, i_69_, var_s_Sub3.anInt2836, var_s_Sub3.anInt2836, i_70_, i_72_, i_71_);
													method2661(75, i_69_, 0, var_s_Sub3.anInt2836, i_70_, i_72_, i_71_);
													method2661(97, i_69_, var_s_Sub3.anInt2836, 0, i_70_, i_72_, i_71_);
												}
											} else {
												method2661(127, i_69_, var_s_Sub3.anInt2836, 0, i_70_, i_72_, i_71_);
												method2661(104, i_69_, var_s_Sub3.anInt2836, var_s_Sub3.anInt2836, i_70_, i_72_, i_71_);
												method2661(118, i_69_, 0, 0, i_70_, i_72_, i_71_);
											}
										} else {
											method2661(83, i_69_, 0, 0, i_70_, i_72_, i_71_);
											method2661(55, i_69_, var_s_Sub3.anInt2836, 0, i_70_, i_72_, i_71_);
											method2661(92, i_69_, 0, var_s_Sub3.anInt2836, i_70_, i_72_, i_71_);
										}
									}
								}
								i_63_++;
								i_71_++;
							}
						} else {
							i_63_ += anInt4724 - anInt4737;
						}
						i_69_++;
					}
					aStream4732.b();
					if (anInterface15_Impl1_4736.method33(32185)) {
						break;
					}
					aStream4740.f(0);
					aClass263_4723.clear();
				}
			}
			aStream4740.b();
			anInterface15_Impl2_4727 = aHa_Sub1_4741.method1205(false, 100);
			anInterface15_Impl2_4727.method48(false, nativeheapbuffer, 16, anInt4721 * 16);
		} else {
			anInterface15_Impl2_4727 = null;
			anInterface15_Impl1_4736 = null;
		}
		aClass263_4723 = null;
		aStream4732 = null;
		aStream4740 = null;
		aFloatArrayArray4725 = aFloatArrayArray4728 = aFloatArrayArray4742 = null;
	}
}
