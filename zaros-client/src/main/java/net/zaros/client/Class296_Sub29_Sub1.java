package net.zaros.client;
/* Class296_Sub29_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.util.Hashtable;

final class Class296_Sub29_Sub1 extends Class296_Sub29 {
	private Image anImage6054;
	private Shape aShape6055;
	private Canvas aCanvas6056;
	private Rectangle aRectangle6057;

	@Override
	final void method2692(int i, int i_0_, int i_1_, Graphics graphics, byte i_2_, int i_3_, int i_4_, int i_5_) {
		aShape6055 = graphics.getClip();
		int i_6_ = 2 / ((-70 - i_2_) / 42);
		aRectangle6057.x = i;
		aRectangle6057.width = i_5_;
		aRectangle6057.height = i_0_;
		aRectangle6057.y = i_3_;
		graphics.setClip(aRectangle6057);
		graphics.drawImage(anImage6054, -i_1_ + i, -i_4_ + i_3_, aCanvas6056);
		graphics.setClip(aShape6055);
	}

	@Override
	final void method2689(Canvas canvas, int i, int i_7_, int i_8_) {
		aCanvas6056 = canvas;
		aRectangle6057 = new Rectangle();
		anInt4813 = i_7_;
		anInt4815 = i;
		anIntArray4810 = new int[anInt4813 * anInt4815];
		DataBufferInt databufferint = new DataBufferInt(anIntArray4810, anIntArray4810.length);
		DirectColorModel directcolormodel = new DirectColorModel(32, 16711680, 65280, 255);
		WritableRaster writableraster = Raster.createWritableRaster(directcolormodel.createCompatibleSampleModel(anInt4815, anInt4813), databufferint, null);
		anImage6054 = new BufferedImage(directcolormodel, writableraster, false, new Hashtable());
	}
}
