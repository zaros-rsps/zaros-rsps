package net.zaros.client;

import net.zaros.client.configs.objtype.ObjTypeList;

/* Class212 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class212 {
	static int[] anIntArray2103 = {7, 8, 9, 10, 11, 12, 13, 15};
	int anInt2104;
	int anInt2105 = -1;
	int anInt2106;
	int anInt2107;
	Animator aClass44_2108;

	static final boolean method2019(byte i) {
		if (i < 79)
			return true;
		return Class338_Sub3_Sub5_Sub1.aBoolean6647;
	}

	Class212(Mobile class338_sub3_sub1_sub3) {
		aClass44_2108 = new Class44_Sub1(class338_sub3_sub1_sub3, false);
	}

	public static void method2020(boolean bool) {
		if (!bool)
			anIntArray2103 = null;
	}

	static final void method2021(int i, int i_0_, InterfaceComponent class51, aa var_aa, int i_1_, int i_2_, int i_3_, int i_4_, ha var_ha) {
		if (i_4_ != -28123)
			anIntArray2103 = null;
		Class18 class18 = Class31.aClass245_324.method2179(i_4_ + 39825, i);
		if (class18 != null && class18.aBoolean195 && class18.method280(Class16_Sub3_Sub1.configsRegister, true)) {
			if (class18.anIntArray211 != null) {
				int[] is = new int[class18.anIntArray211.length];
				for (int i_5_ = 0; is.length / 2 > i_5_; i_5_++) {
					int i_6_;
					if (Class361.anInt3103 != 4)
						i_6_ = (StaticMethods.anInt6075 + (int) Class41_Sub26.aFloat3806) & 0x3fff;
					else
						i_6_ = (int) Class41_Sub26.aFloat3806 & 0x3fff;
					int i_7_ = Class296_Sub4.anIntArray4598[i_6_];
					int i_8_ = Class296_Sub4.anIntArray4618[i_6_];
					if (Class361.anInt3103 != 4) {
						i_8_ = i_8_ * 256 / (ObjTypeList.anInt1320 + 256);
						i_7_ = i_7_ * 256 / (ObjTypeList.anInt1320 + 256);
					}
					is[i_5_ * 2] = ((i_8_ * (class18.anIntArray211[i_5_ * 2] * 4 + i_1_) + ((class18.anIntArray211[i_5_ * 2 + 1] * 4 + i_2_) * i_7_)) >> 14) + class51.anInt578 / 2 + i_0_;
					is[i_5_ * 2 + 1] = (i_3_ + class51.anInt623 / 2 - ((-((class18.anIntArray211[i_5_ * 2] * 4 + i_1_) * i_7_) + (i_8_ * (class18.anIntArray211[i_5_ * 2 + 1] * 4 + i_2_))) >> 14));
				}
				Class98 class98 = class51.method633((byte) -107, var_ha);
				if (class98 != null)
					Class148.method1514(var_ha, is, class18.anInt213, class98.anIntArray1054, class98.anIntArray1053);
				if (class18.anInt196 > 0) {
					for (int i_9_ = 0; i_9_ < is.length / 2 - 1; i_9_++) {
						int i_10_ = is[i_9_ * 2];
						int i_11_ = is[i_9_ * 2 + 1];
						int i_12_ = is[(i_9_ + 1) * 2];
						int i_13_ = is[i_9_ * 2 + 2 + 1];
						if (i_10_ > i_12_) {
							int i_14_ = i_10_;
							i_10_ = i_12_;
							int i_15_ = i_11_;
							i_12_ = i_14_;
							i_11_ = i_13_;
							i_13_ = i_15_;
						} else if (i_12_ == i_10_ && i_11_ > i_13_) {
							int i_16_ = i_11_;
							i_11_ = i_13_;
							i_13_ = i_16_;
						}
						var_ha.a(i_10_, i_11_, i_12_, i_13_, (class18.anIntArray229[class18.aByteArray194[i_9_] & 0xff]), 1, var_aa, i_0_, i_3_, class18.anInt196, class18.anInt226, class18.anInt227);
					}
					int i_17_ = is[is.length - 2];
					int i_18_ = is[is.length - 1];
					int i_19_ = is[0];
					int i_20_ = is[1];
					if (i_19_ < i_17_) {
						int i_21_ = i_17_;
						i_17_ = i_19_;
						int i_22_ = i_18_;
						i_19_ = i_21_;
						i_18_ = i_20_;
						i_20_ = i_22_;
					} else if (i_19_ == i_17_ && i_20_ < i_18_) {
						int i_23_ = i_18_;
						i_18_ = i_20_;
						i_20_ = i_23_;
					}
					var_ha.a(i_17_, i_18_, i_19_, i_20_, (class18.anIntArray229[(class18.aByteArray194[class18.aByteArray194.length - 1]) & 0xff]), 1, var_aa, i_0_, i_3_, class18.anInt196, class18.anInt226, class18.anInt227);
				} else {
					for (int i_24_ = 0; i_24_ < is.length / 2 - 1; i_24_++)
						var_ha.a(is[i_24_ * 2], is[i_24_ * 2 + 1], is[(i_24_ + 1) * 2], is[(i_24_ + 1) * 2 + 1], (class18.anIntArray229[class18.aByteArray194[i_24_] & 0xff]), 1, var_aa, i_0_, i_3_);
					var_ha.a(is[is.length - 2], is[is.length - 1], is[0], is[1], (class18.anIntArray229[(class18.aByteArray194[class18.aByteArray194.length - 1]) & 0xff]), 1, var_aa, i_0_, i_3_);
				}
			}
			Sprite class397 = null;
			if (class18.anInt203 != -1) {
				class397 = class18.method275(false, (byte) 111, var_ha);
				if (class397 != null)
					Class331.method3409(i_3_, var_aa, class51, class397, i_1_, i_2_, i_0_, false);
			}
			if (class18.aString191 != null) {
				int i_25_ = 0;
				if (class397 != null)
					i_25_ = class397.method4092();
				Class55 class55 = Class41_Sub2.aClass55_3742;
				Class92 class92 = Class154_Sub1.aClass92_4292;
				if (class18.anInt214 == 1) {
					class92 = Class123_Sub1_Sub1.aClass92_5814;
					class55 = Class205_Sub1.aClass55_5642;
				}
				if (class18.anInt214 == 2) {
					class55 = Class49.aClass55_461;
					class92 = Class304.aClass92_2729;
				}
				Class331.method3407(i_0_, var_aa, (byte) -127, i_1_, i_2_, i_3_, class51, class18.aString191, i_25_, class18.anInt225, class92, class55);
			}
		}
	}
}
