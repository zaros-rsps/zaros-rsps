package net.zaros.client;

/* Class266 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class266 implements Interface10 {
	int anInt3600;
	int anInt3601;
	String aString3602;
	int anInt3603;
	int anInt3604;
	int anInt3605;
	int anInt3606;
	Class357 aClass357_3607;
	int anInt3608;
	int anInt3609;
	int anInt3610;
	int anInt3611;
	Class252 aClass252_3612;

	static final void method2289(InterfaceComponent class51, int i) {
		if (i == -29278) {
			if (class51.type == 5 && class51.clickedItem != -1)
				Class368_Sub13.method3849(-16777216, class51, Class41_Sub13.aHa3774);
		}
	}

	public final Class294 method45(byte i) {
		int i_0_ = 103 / ((-6 - i) / 44);
		return Class227.aClass294_2186;
	}

	static final void method2290(Class338_Sub3[] class338_sub3s, int i, int i_1_) {
		if (i < i_1_) {
			int i_2_ = (i + i_1_) / 2;
			int i_3_ = i;
			Class338_Sub3 class338_sub3 = class338_sub3s[i_2_];
			class338_sub3s[i_2_] = class338_sub3s[i_1_];
			class338_sub3s[i_1_] = class338_sub3;
			int i_4_ = class338_sub3.anInt5208;
			for (int i_5_ = i; i_5_ < i_1_; i_5_++) {
				if (class338_sub3s[i_5_].anInt5208 < i_4_ + (i_5_ & 0x1)) {
					Class338_Sub3 class338_sub3_6_ = class338_sub3s[i_5_];
					class338_sub3s[i_5_] = class338_sub3s[i_3_];
					class338_sub3s[i_3_++] = class338_sub3_6_;
				}
			}
			class338_sub3s[i_1_] = class338_sub3s[i_3_];
			class338_sub3s[i_3_] = class338_sub3;
			method2290(class338_sub3s, i, i_3_ - 1);
			method2290(class338_sub3s, i_3_ + 1, i_1_);
		}
	}

	Class266(String string, Class252 class252, Class357 class357, int i, int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_) {
		anInt3611 = i_15_;
		anInt3601 = i_12_;
		aString3602 = string;
		aClass357_3607 = class357;
		anInt3609 = i_11_;
		anInt3604 = i;
		anInt3610 = i_9_;
		anInt3600 = i_13_;
		aClass252_3612 = class252;
		anInt3606 = i_7_;
		anInt3608 = i_8_;
		anInt3605 = i_14_;
		anInt3603 = i_10_;
	}
}
