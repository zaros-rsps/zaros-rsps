package net.zaros.client;

/* Class44_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class44_Sub2 extends Animator {
	static int[] anIntArray3859 = new int[128];
	static OutgoingPacket aClass311_3860;
	static IncomingPacket aClass231_3861;
	static String[] aStringArray3862;

	final void method565(int i, Animation class43, byte i_0_) {
		if (i_0_ != -95)
			anIntArray3859 = null;
		Class370.method3876(0, i, class43);
	}

	public static void method578(boolean bool) {
		aStringArray3862 = null;
		aClass231_3861 = null;
		anIntArray3859 = null;
		if (bool != true)
			method579(-22, -63, 105, 18);
		aClass311_3860 = null;
	}

	public Class44_Sub2() {
		super(true);
	}

	static final int method579(int i, int i_1_, int i_2_, int i_3_) {
		int i_4_ = i / i_2_;
		int i_5_ = i_2_ - 1 & i;
		int i_6_ = i_3_ / i_2_;
		int i_7_ = i_3_ & i_1_ + i_2_;
		int i_8_ = BillboardRaw.method886(i_4_, i_6_, true);
		int i_9_ = BillboardRaw.method886(i_4_ + 1, i_6_, true);
		int i_10_ = BillboardRaw.method886(i_4_, i_6_ + 1, true);
		int i_11_ = BillboardRaw.method886(i_4_ + 1, i_6_ + 1, true);
		int i_12_ = Class296_Sub39_Sub4.method2802(i_9_, i_8_, i_5_, i_2_, -11202);
		int i_13_ = Class296_Sub39_Sub4.method2802(i_11_, i_10_, i_5_, i_2_, -11202);
		return Class296_Sub39_Sub4.method2802(i_13_, i_12_, i_7_, i_2_, i_1_ ^ 0x2bc1);
	}

	static {
		for (int i = 0; anIntArray3859.length > i; i++)
			anIntArray3859[i] = -1;
		for (int i = 65; i <= 90; i++)
			anIntArray3859[i] = i - 65;
		for (int i = 97; i <= 122; i++)
			anIntArray3859[i] = i - 97 + 26;
		for (int i = 48; i <= 57; i++)
			anIntArray3859[i] = i + 52 - 48;
		anIntArray3859[42] = anIntArray3859[43] = 62;
		anIntArray3859[45] = anIntArray3859[47] = 63;
		aClass311_3860 = new OutgoingPacket(33, 0);
		aStringArray3862 = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
		aClass231_3861 = new IncomingPacket(64, 10);
	}
}
