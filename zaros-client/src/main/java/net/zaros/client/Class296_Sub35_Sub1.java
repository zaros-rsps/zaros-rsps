package net.zaros.client;

public  class Class296_Sub35_Sub1 extends Class296_Sub35 {
	static int[] anIntArray6108 = new int[1];
	static int anInt6109;

	@Override
	final void method2744(float f, int i) {
		int i_0_ = 63 % ((58 - i) / 34);
		aFloat4846 = f;
	}

	@Override
	final void method2747(int i, int i_1_, int i_2_, int i_3_) {
		anInt4844 = i_2_;
		anInt4848 = i;
		if (i_3_ == 16184) {
			anInt4849 = i_1_;
		}
	}

	public Class296_Sub35_Sub1(int i, int i_4_, int i_5_, int i_6_, int i_7_, float f) {
		super(i, i_4_, i_5_, i_6_, i_7_, f);
	}

	public static void method2752(int i) {
		if (i != -1) {
			method2753(-42, -96, -18, null, -73, 26);
		}
		anIntArray6108 = null;
	}

	static final boolean method2753(int i, int i_8_, int i_9_, byte[] is, int i_10_, int i_11_) {
		boolean bool = true;
		Packet class296_sub17 = new Packet(is);
		int i_12_ = i_8_;
		for (;;) {
			int i_13_ = class296_sub17.method2595();
			if (i_13_ == 0) {
				break;
			}
			i_12_ += i_13_;
			int i_14_ = 0;
			boolean bool_15_ = false;
			for (;;) {
				if (bool_15_) {
					int i_16_ = class296_sub17.readSmart();
					if (i_16_ == 0) {
						break;
					}
					class296_sub17.g1();
				} else {
					int i_17_ = class296_sub17.readSmart();
					if (i_17_ == 0) {
						break;
					}
					i_14_ += i_17_ - 1;
					int i_18_ = i_14_ & 0x3f;
					int i_19_ = (i_14_ & 0xff5) >> 6;
					int i_20_ = class296_sub17.g1() >> 2;
					int i_21_ = i_19_ + i_10_;
					int i_22_ = i_9_ + i_18_;
					if (i_21_ > 0 && i_22_ > 0 && i_11_ - 1 > i_21_ && i - 1 > i_22_) {
						ObjectDefinition class70 = Class379.objectDefinitionLoader.getObjectDefinition(i_12_);
						if (i_20_ != 22 || Class343_Sub1.aClass296_Sub50_5282.aClass41_Sub22_4997.method484(i_8_ ^ ~0x79) != 0 || class70.interactonType != 0 || class70.blockingType == 1 || class70.aBoolean789) {
							bool_15_ = true;
							if (!class70.method749((byte) -126)) {
								Class117.anInt1187++;
								bool = false;
							}
						}
					}
				}
			}
		}
		return bool;
	}
}
