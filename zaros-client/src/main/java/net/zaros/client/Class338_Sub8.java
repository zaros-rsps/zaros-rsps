package net.zaros.client;

/* Class338_Sub8 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

class Class338_Sub8 extends Class338 {
	Class338_Sub8 aClass338_Sub8_5253;
	Class338_Sub8 aClass338_Sub8_5254;
	static SubInPacket aClass260_5255 = new SubInPacket(9, 4);
	static World gameWorld;
	static Class94[] aClass94Array5257;

	final void method3600(byte i) {
		if (aClass338_Sub8_5254 != null) {
			int i_0_ = -7 / ((58 - i) / 39);
			aClass338_Sub8_5254.aClass338_Sub8_5253 = aClass338_Sub8_5253;
			aClass338_Sub8_5253.aClass338_Sub8_5254 = aClass338_Sub8_5254;
			aClass338_Sub8_5253 = null;
			aClass338_Sub8_5254 = null;
		}
	}

	static final void method3601(byte i) {
		int i_1_ = Class44_Sub1.cameraDestX * 512 + 256;
		int i_2_ = Class5.cameraDestZ * 512 + 256;
		int i_3_ = (aa_Sub1.method155(-1537652855, FileWorker.anInt3005, i_1_, i_2_) - HashTable.anInt2459);
		if (Class32_Sub1.basedSpeed < 100) {
			if (i_1_ > Class219.camPosX) {
				Class219.camPosX += Class373_Sub3.anInt5605 + (Class32_Sub1.basedSpeed * (i_1_ - Class219.camPosX) / 1000);
				if (Class219.camPosX > i_1_)
					Class219.camPosX = i_1_;
			}
			if (TranslatableString.camPosY < i_3_) {
				TranslatableString.camPosY += ((i_3_ - TranslatableString.camPosY) * Class32_Sub1.basedSpeed / 1000) + Class373_Sub3.anInt5605;
				if (TranslatableString.camPosY > i_3_)
					TranslatableString.camPosY = i_3_;
			}
			if (Class219.camPosX > i_1_) {
				Class219.camPosX -= (Class373_Sub3.anInt5605 + (Class32_Sub1.basedSpeed * (-i_1_ + Class219.camPosX) / 1000));
				if (i_1_ > Class219.camPosX)
					Class219.camPosX = i_1_;
			}
			if (i_3_ < TranslatableString.camPosY) {
				TranslatableString.camPosY -= ((TranslatableString.camPosY - i_3_) * Class32_Sub1.basedSpeed / 1000) + Class373_Sub3.anInt5605;
				if (i_3_ > TranslatableString.camPosY)
					TranslatableString.camPosY = i_3_;
			}
			if (Class124.camPosZ < i_2_) {
				Class124.camPosZ += (Class373_Sub3.anInt5605 + (Class32_Sub1.basedSpeed * (-Class124.camPosZ + i_2_) / 1000));
				if (Class124.camPosZ > i_2_)
					Class124.camPosZ = i_2_;
			}
			if (Class124.camPosZ > i_2_) {
				Class124.camPosZ -= Class373_Sub3.anInt5605 + (Class32_Sub1.basedSpeed * (Class124.camPosZ - i_2_) / 1000);
				if (Class124.camPosZ < i_2_)
					Class124.camPosZ = i_2_;
			}
		} else {
			Class219.camPosX = Class44_Sub1.cameraDestX * 512 + 256;
			Class124.camPosZ = Class5.cameraDestZ * 512 + 256;
			TranslatableString.camPosY = (aa_Sub1.method155(-1537652855, FileWorker.anInt3005, Class219.camPosX, Class124.camPosZ) - HashTable.anInt2459);
		}
		i_1_ = Class53.lookatX * 512 + 256;
		i_2_ = Graphic.lookatY * 512 + 256;
		i_3_ = (aa_Sub1.method155(-1537652855, FileWorker.anInt3005, i_1_, i_2_) - Class338_Sub8_Sub2.lookatHeight);
		int i_4_ = i_1_ - Class219.camPosX;
		int i_5_ = i_3_ - TranslatableString.camPosY;
		int i_6_ = -Class124.camPosZ + i_2_;
		int i_7_ = (int) Math.sqrt((double) (i_6_ * i_6_ + i_4_ * i_4_));
		int i_8_ = ((int) (Math.atan2((double) i_5_, (double) i_7_) * 2607.5945876176133) & 0x3fff);
		if (i_8_ < 1024)
			i_8_ = 1024;
		int i_9_ = ((int) (Math.atan2((double) i_4_, (double) i_6_) * -2607.5945876176133) & 0x3fff);
		if (i_8_ > 3072)
			i_8_ = 3072;
		if (i_8_ > Class296_Sub17_Sub2.camRotX) {
			Class296_Sub17_Sub2.camRotX += ((Class366_Sub2.lookatVelocity * (i_8_ - Class296_Sub17_Sub2.camRotX >> 3) / 1000) + StaticMethods.lookatSpeed) << 3;
			if (Class296_Sub17_Sub2.camRotX > i_8_)
				Class296_Sub17_Sub2.camRotX = i_8_;
		}
		if (Class296_Sub17_Sub2.camRotX > i_8_) {
			Class296_Sub17_Sub2.camRotX -= (((-i_8_ + Class296_Sub17_Sub2.camRotX >> 3) * Class366_Sub2.lookatVelocity / 1000) + StaticMethods.lookatSpeed) << 3;
			if (i_8_ > Class296_Sub17_Sub2.camRotX)
				Class296_Sub17_Sub2.camRotX = i_8_;
		}
		int i_10_ = -Class44_Sub1.camRotY + i_9_;
		if (i_10_ > 8192)
			i_10_ -= 16384;
		if (i_10_ < -8192)
			i_10_ += 16384;
		if (i != -27)
			method3603(25);
		i_10_ >>= 3;
		if (i_10_ > 0) {
			Class44_Sub1.camRotY += (i_10_ * Class366_Sub2.lookatVelocity / 1000 + StaticMethods.lookatSpeed) << 3;
			Class44_Sub1.camRotY &= 0x3fff;
		}
		if (i_10_ < 0) {
			Class44_Sub1.camRotY -= (Class366_Sub2.lookatVelocity * -i_10_ / 1000 + StaticMethods.lookatSpeed) << 3;
			Class44_Sub1.camRotY &= 0x3fff;
		}
		int i_11_ = i_9_ - Class44_Sub1.camRotY;
		if (i_11_ > 8192)
			i_11_ -= 16384;
		if (i_11_ < -8192)
			i_11_ += 16384;
		Class268.anInt2488 = 0;
		if (i_11_ < 0 && i_10_ > 0 || i_11_ > 0 && i_10_ < 0)
			Class44_Sub1.camRotY = i_9_;
	}

	static final void method3602(int i, Js5 class138, boolean bool, int i_12_, int i_13_, int i_14_) {
		int i_15_ = -53 / ((-72 - i_12_) / 49);
		Class296_Sub15_Sub2.method2528(2, class138, i_14_, bool, 0L, i_13_, i);
	}

	public Class338_Sub8() {
		/* empty */
	}

	public static void method3603(int i) {
		gameWorld = null;
		aClass260_5255 = null;
		aClass94Array5257 = null;
		if (i < 121)
			aClass260_5255 = null;
	}
}
