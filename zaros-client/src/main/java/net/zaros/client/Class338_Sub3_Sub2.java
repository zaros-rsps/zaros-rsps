package net.zaros.client;

/* Class338_Sub3_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class338_Sub3_Sub2 extends Class338_Sub3 {
	static int anInt6565;
	static boolean aBoolean6566 = false;

	Class338_Sub3_Sub2(int i, int i_0_, int i_1_, int i_2_, int i_3_) {
		tileY = i_1_;
		aByte5203 = (byte) i_3_;
		z = (byte) i_2_;
		anInt5213 = i_0_;
		tileX = i;
	}

	final boolean method3470(int i, ha var_ha) {
		if (i != -30488)
			return false;
		Class234 class234 = Class296_Sub15.method2516(z, tileX >> Class313.anInt2779, tileY >> Class313.anInt2779);
		if (class234 != null && class234.aClass338_Sub3_Sub1_2226.aBoolean6557)
			return (Class296_Sub51_Sub14.method3115((byte) -127, (class234.aClass338_Sub3_Sub1_2226.method3466((byte) 97) + this.method3466((byte) 81)), z, tileY >> Class313.anInt2779, tileX >> Class313.anInt2779));
		return Class50.method614((byte) -104, tileX >> Class313.anInt2779, tileY >> Class313.anInt2779, z);
	}

	static final void method3553(byte i, int i_4_) {
		if (i != -44)
			method3553((byte) -113, -124);
		if (Class358.anIntArray3087 == null || i_4_ > Class358.anIntArray3087.length)
			Class358.anIntArray3087 = new int[i_4_];
	}

	final boolean method3468(int i) {
		if (i > -29)
			method3467(109, -27, null, true, -81, 56, null);
		return false;
	}

	final boolean method3465(int i) {
		if (i != 0)
			method3463(-8, null);
		return (Class296_Sub15_Sub2.aBooleanArrayArray6006[(Class379_Sub2.anInt5684 - Class296_Sub45_Sub2.anInt6288 + (tileX >> Class313.anInt2779))][(-Class296_Sub39_Sub20_Sub2.anInt6728 + (tileY >> Class313.anInt2779) + Class379_Sub2.anInt5684)]);
	}

	final int method3463(int i, Class296_Sub35[] class296_sub35s) {
		if (i != -1)
			return 51;
		return this.method3474(tileX >> Class313.anInt2779, class296_sub35s, tileY >> Class313.anInt2779, 4);
	}

	final void method3467(int i, int i_5_, Class338_Sub3 class338_sub3, boolean bool, int i_6_, int i_7_, ha var_ha) {
		int i_8_ = 89 % ((20 - i_7_) / 48);
		throw new IllegalStateException();
	}

	final void method3472(byte i) {
		int i_9_ = -15 % ((-56 - i) / 38);
		throw new IllegalStateException();
	}
}
