package net.zaros.client;

/* Class296_Sub52 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub52 extends Node implements Interface1 {
	char aChar3398;
	int anInt3399;
	long aLong3400;
	int anInt3401;
	int anInt3402;

	public final int method1(byte i) {
		if (i != -105)
			method3204(34);
		return anInt3402;
	}

	public final int method5(int i) {
		if (i != 13856)
			method3((byte) -41);
		return anInt3401;
	}

	public final long method3(byte i) {
		if (i < 12)
			return 62L;
		return aLong3400;
	}

	static final boolean method3203(int i, Class18 class18) {
		if (class18 == null)
			return false;
		if (!class18.aBoolean207)
			return false;
		if (!class18.method280(Class106.anInterface17_1090, true))
			return false;
		if (Class296_Sub51_Sub31.aClass263_6506.get((long) class18.anInt205) != null)
			return false;
		if (OutputStream_Sub1.aClass263_39.get((long) class18.anInt217) != null)
			return false;
		if (i >= -12)
			method3204(-105);
		return true;
	}

	public final int method4(byte i) {
		if (i != 70)
			method3((byte) -85);
		return anInt3399;
	}

	public final char method2(int i) {
		if (i != -23600)
			method3((byte) 28);
		return aChar3398;
	}

	static final void method3204(int i) {
		int i_0_ = PlayerUpdate.visiblePlayersCount;
		int[] is = PlayerUpdate.visiblePlayersIndexes;
		int i_1_ = 0;
		if (i >= 46) {
			for (/**/; i_1_ < i_0_; i_1_++) {
				Player class338_sub3_sub1_sub3_sub1 = (PlayerUpdate.visiblePlayers[is[i_1_]]);
				if (class338_sub3_sub1_sub3_sub1 != null)
					Class296_Sub39_Sub20.method2903(106, class338_sub3_sub1_sub3_sub1, false);
			}
		}
	}
}
