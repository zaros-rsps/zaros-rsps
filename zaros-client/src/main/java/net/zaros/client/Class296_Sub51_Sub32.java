package net.zaros.client;

/* Class296_Sub51_Sub32 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class296_Sub51_Sub32 extends TextureOperation {
	static int anInt6512 = -1;
	static long aLong6513 = 20000000L;
	static int[][] anIntArrayArray6514;
	public static boolean aBoolean6515 = false;
	private Class42[] aClass42Array6516;

	@Override
	final int[][] get_colour_output(int i, int i_0_) {
		if (i_0_ != 17621) {
			return null;
		}
		int[][] is = aClass86_5034.method823((byte) 81, i);
		if (aClass86_5034.aBoolean939) {
			int i_1_ = Class41_Sub10.anInt3769;
			int i_2_ = Class296_Sub35_Sub2.anInt6114;
			int[][] is_3_ = new int[i_2_][i_1_];
			int[][][] is_4_ = aClass86_5034.method824((byte) -125);
			method3175(-1972899924, is_3_);
			for (int i_5_ = 0; Class296_Sub35_Sub2.anInt6114 > i_5_; i_5_++) {
				int[] is_6_ = is_3_[i_5_];
				int[][] is_7_ = is_4_[i_5_];
				int[] is_8_ = is_7_[0];
				int[] is_9_ = is_7_[1];
				int[] is_10_ = is_7_[2];
				for (int i_11_ = 0; i_11_ < Class41_Sub10.anInt3769; i_11_++) {
					int i_12_ = is_6_[i_11_];
					is_10_[i_11_] = 4080 & i_12_ << 4;
					is_9_[i_11_] = 4080 & i_12_ >> 4;
					is_8_[i_11_] = (16711680 & i_12_) >> 12;
				}
			}
		}
		return is;
	}

	@Override
	final int[] get_monochrome_output(int i, int i_13_) {
		int[] is = aClass318_5035.method3335(i_13_, (byte) 28);
		if (i != 0) {
			anIntArrayArray6514 = null;
		}
		if (aClass318_5035.aBoolean2819) {
			method3175(i - 1972899924, aClass318_5035.method3337(i + 127));
		}
		return is;
	}

	@Override
	final void method3071(int i, Packet class296_sub17, int i_14_) {
		if (i_14_ == 0) {
			aClass42Array6516 = new Class42[class296_sub17.g1()];
			while_243_ : for (int i_15_ = 0; i_15_ < aClass42Array6516.length; i_15_++) {
				int i_16_ = class296_sub17.g1();
				int i_17_ = i_16_;
				while_241_ : do {
					do {
						if (i_17_ != 0) {
							if (i_17_ != 1) {
								if (i_17_ != 2) {
									if (i_17_ != 3) {
										continue while_243_;
									}
								} else {
									break;
								}
								break while_241_;
							}
						} else {
							aClass42Array6516[i_15_] = Class42_Sub3.method538(class296_sub17, false);
							continue while_243_;
						}
						aClass42Array6516[i_15_] = Class338_Sub7.method3597((byte) -42, class296_sub17);
						continue while_243_;
					} while (false);
					aClass42Array6516[i_15_] = Class360_Sub6.method3745(true, class296_sub17);
					continue while_243_;
				} while (false);
				aClass42Array6516[i_15_] = Class253.method2209(0, class296_sub17);
			}
		} else if (i_14_ == 1) {
			monochromatic = class296_sub17.g1() == 1;
		}
		if (i > -84) {
			aLong6513 = 26L;
		}
	}

	public Class296_Sub51_Sub32() {
		super(0, true);
	}

	private final void method3175(int i, int[][] is) {
		if (i != -1972899924) {
			anInt6512 = 39;
		}
		int i_18_ = Class41_Sub10.anInt3769;
		int i_19_ = Class296_Sub35_Sub2.anInt6114;
		Statics.method634(97, is);
		Class368_Sub5.method3823((byte) 77, Class41_Sub25.anInt3803, Class67.anInt753, 0, 0);
		if (aClass42Array6516 != null) {
			for (Class42 class42 : aClass42Array6516) {
				int i_21_ = class42.anInt394;
				int i_22_ = class42.anInt397;
				if (i_21_ >= 0) {
					if (i_22_ >= 0) {
						class42.method528(false, i_19_, i_18_);
					} else {
						class42.method531(68, i_19_, i_18_);
					}
				} else if (i_22_ >= 0) {
					class42.method529(i_19_, i_18_, -43);
				}
			}
		}
	}

	static final void method3176(ha var_ha, int i) {
		if (Class296_Sub39_Sub19.anInt6250 != Class296_Sub51_Sub11.localPlayer.z && Class338_Sub2.aClass247ArrayArrayArray5195 != null) {
			if (Class156.method1584((byte) 22, var_ha, Class296_Sub51_Sub11.localPlayer.z)) {
				Class296_Sub39_Sub19.anInt6250 = Class296_Sub51_Sub11.localPlayer.z;
			}
			if (i != -11669) {
				anInt6512 = 81;
			}
		}
	}

	public static void method3177(int i) {
		if (i == 255) {
			anIntArrayArray6514 = null;
		}
	}

	static final void method3178(int localx, int localy, int height, int speed, int velocity, boolean bool, boolean bool_23_) {
		Class5.cameraDestZ = localy;
		Class44_Sub1.cameraDestX = localx;
		HashTable.anInt2459 = height;
		Class373_Sub3.anInt5605 = speed;
		if (!bool) {
			Class32_Sub1.basedSpeed = velocity;
			if (bool_23_ && Class32_Sub1.basedSpeed >= 100) {
				Class219.camPosX = Class44_Sub1.cameraDestX * 512 + 256;
				Class124.camPosZ = Class5.cameraDestZ * 512 + 256;
				TranslatableString.camPosY = aa_Sub1.method155(-1537652855, FileWorker.anInt3005, Class219.camPosX, Class124.camPosZ) - HashTable.anInt2459;
			}
			Class361.anInt3103 = 2;
			Class368_Sub11.anInt5486 = Class242.anInt2309 = -1;
		}
	}
}
