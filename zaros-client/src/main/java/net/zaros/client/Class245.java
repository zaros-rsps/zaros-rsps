package net.zaros.client;

/* Class245 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class245 {
	Js5 aClass138_2323;
	private AdvancedMemoryCache aClass113_2324 = new AdvancedMemoryCache(128);
	private Js5 aClass138_2325;
	static OutgoingPacket aClass311_2326;
	static boolean aBoolean2327 = false;
	AdvancedMemoryCache aClass113_2328 = new AdvancedMemoryCache(64);
	static Class209 aClass209_2329;
	static int anInt2330;

	final Class18 method2179(int i, int i_0_) {
		Class18 class18;
		synchronized (aClass113_2324) {
			class18 = (Class18) aClass113_2324.get((long) i_0_);
		}
		if (class18 != null)
			return class18;
		byte[] is;
		synchronized (aClass138_2325) {
			is = aClass138_2325.getFile(36, i_0_);
		}
		class18 = new Class18();
		class18.aClass245_198 = this;
		class18.anInt205 = i_0_;
		if (i != 11702)
			return null;
		if (is != null)
			class18.method278(new Packet(is), -1);
		class18.method274(i ^ ~0x2db7);
		synchronized (aClass113_2324) {
			aClass113_2324.put(class18, (long) i_0_);
		}
		return class18;
	}

	final void method2180(int i) {
		synchronized (aClass113_2324) {
			aClass113_2324.clearSoftReferences();
		}
		synchronized (aClass113_2328) {
			aClass113_2328.clearSoftReferences();
			if (i > -55)
				aBoolean2327 = true;
		}
	}

	final void method2181(int i, int i_1_) {
		synchronized (aClass113_2324) {
			aClass113_2324.clean(i);
		}
		if (i_1_ == 36) {
			synchronized (aClass113_2328) {
				aClass113_2328.clean(i);
			}
		}
	}

	final void method2182(int i) {
		synchronized (aClass113_2324) {
			aClass113_2324.clear();
		}
		synchronized (aClass113_2328) {
			aClass113_2328.clear();
			if (i != 2)
				anInt2330 = -46;
		}
	}

	final void method2183(int i, int i_2_, int i_3_) {
		aClass113_2324 = new AdvancedMemoryCache(i_3_);
		if (i_2_ != 128)
			method2184(124);
		aClass113_2328 = new AdvancedMemoryCache(i);
	}

	static final void method2184(int i) {
		if (i != 36)
			aBoolean2327 = false;
		Class353.aClass322Array3049 = null;
	}

	static final void method2185(int i, float[] fs, int i_4_, int i_5_, int i_6_, float f, float f_7_, int i_8_, int i_9_, float f_10_, int i_11_, int i_12_, float[] fs_13_, int i_14_) {
		i_14_ -= i_9_;
		i_12_ -= i_11_;
		i_5_ -= i_6_;
		if (i_8_ < 54)
			method2186(false);
		float f_15_ = ((float) i_5_ * fs_13_[1] + fs_13_[0] * (float) i_14_ + fs_13_[2] * (float) i_12_);
		float f_16_ = (fs_13_[5] * (float) i_12_ + (fs_13_[4] * (float) i_5_ + (float) i_14_ * fs_13_[3]));
		float f_17_ = ((float) i_5_ * fs_13_[7] + fs_13_[6] * (float) i_14_ + (float) i_12_ * fs_13_[8]);
		float f_18_;
		float f_19_;
		if (i_4_ != 0) {
			if (i_4_ != 1) {
				if (i_4_ == 2) {
					f_18_ = f_10_ + -f_15_ + 0.5F;
					f_19_ = -f_16_ + f_7_ + 0.5F;
				} else if (i_4_ == 3) {
					f_19_ = -f_16_ + f_7_ + 0.5F;
					f_18_ = f_10_ + f_15_ + 0.5F;
				} else if (i_4_ != 4) {
					f_19_ = -f_16_ + f_7_ + 0.5F;
					f_18_ = -f_17_ + f + 0.5F;
				} else {
					f_19_ = -f_16_ + f_7_ + 0.5F;
					f_18_ = f + f_17_ + 0.5F;
				}
			} else {
				f_19_ = f_17_ + f + 0.5F;
				f_18_ = f_10_ + f_15_ + 0.5F;
			}
		} else {
			f_18_ = f_10_ + f_15_ + 0.5F;
			f_19_ = f + -f_17_ + 0.5F;
		}
		if (i == 1) {
			float f_20_ = f_18_;
			f_18_ = -f_19_;
			f_19_ = f_20_;
		} else if (i == 2) {
			f_19_ = -f_19_;
			f_18_ = -f_18_;
		} else if (i == 3) {
			float f_21_ = f_18_;
			f_18_ = f_19_;
			f_19_ = -f_21_;
		}
		fs[1] = f_19_;
		fs[0] = f_18_;
	}

	public static void method2186(boolean bool) {
		aClass311_2326 = null;
		aClass209_2329 = null;
		if (bool != true)
			method2186(false);
	}

	Class245(GameType class35, int i, Js5 class138, Js5 class138_22_) {
		aClass138_2325 = class138;
		aClass138_2323 = class138_22_;
		aClass138_2325.getLastFileId(36);
	}

	static {
		aClass311_2326 = new OutgoingPacket(21, 2);
		aClass209_2329 = new Class209(21);
	}
}
