package net.zaros.client;

/* Class249 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class249 {
	static AdvancedMemoryCache aClass113_2355 = new AdvancedMemoryCache(10);
	static int anInt2356 = 0;
	static Class296_Sub45_Sub4 aClass296_Sub45_Sub4_2357;

	static final void method2193(int i, double d) {
		if (i == 255) {
			if (Class252.aDouble2381 != d) {
				for (int i_0_ = 0; i_0_ < 256; i_0_++) {
					int i_1_ = (int) (Math.pow((double) i_0_ / 255.0, d) * 255.0);
					Class363.anIntArray3111[i_0_] = i_1_ <= 255 ? i_1_ : 255;
				}
				Class252.aDouble2381 = d;
			}
		}
	}

	public static void method2194(byte i) {
		aClass296_Sub45_Sub4_2357 = null;
		aClass113_2355 = null;
		if (i < 93)
			method2195(93, 105);
	}

	static final int method2195(int i, int i_2_) {
		if (StaticMethods.aShortArrayArray5919 != null)
			return StaticMethods.aShortArrayArray5919[i][i_2_] & 0xffff;
		return 0;
	}

	static final void method2196(int i, int i_3_, int i_4_, int i_5_, int i_6_) {
		int i_7_ = 0;
		int i_8_ = i;
		int i_9_ = -i;
		int i_10_ = i_4_;
		Class296_Sub14.method2511((Class296_Sub51_Sub37.anIntArrayArray6536[i_6_]), i + i_3_, (byte) -104, i_5_, -i + i_3_);
		while (i_8_ > i_7_) {
			i_10_ += 2;
			i_9_ += i_10_;
			i_7_++;
			if (i_9_ >= 0) {
				i_8_--;
				i_9_ -= i_8_ << 1;
				int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_8_ + i_6_];
				int[] is_11_ = Class296_Sub51_Sub37.anIntArrayArray6536[-i_8_ + i_6_];
				int i_12_ = i_7_ + i_3_;
				int i_13_ = i_3_ - i_7_;
				Class296_Sub14.method2511(is, i_12_, (byte) 111, i_5_, i_13_);
				Class296_Sub14.method2511(is_11_, i_12_, (byte) -32, i_5_, i_13_);
			}
			int i_14_ = i_8_ + i_3_;
			int i_15_ = -i_8_ + i_3_;
			int[] is = Class296_Sub51_Sub37.anIntArrayArray6536[i_6_ + i_7_];
			int[] is_16_ = Class296_Sub51_Sub37.anIntArrayArray6536[i_6_ - i_7_];
			Class296_Sub14.method2511(is, i_14_, (byte) 116, i_5_, i_15_);
			Class296_Sub14.method2511(is_16_, i_14_, (byte) 119, i_5_, i_15_);
		}
	}
}
