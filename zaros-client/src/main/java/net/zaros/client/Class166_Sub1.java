package net.zaros.client;
/* Class166_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Method;

final class Class166_Sub1 extends Class166 implements KeyListener, FocusListener {
	static Class289 aClass289_4298 = new Class289();
	private NodeDeque aClass155_4299 = new NodeDeque();
	static int[] anIntArray4300;
	static int anInt4301 = 0;
	static int anInt4302;
	static Class55 aClass55_4303;
	static int anInt4304 = -1;
	private NodeDeque aClass155_4305 = new NodeDeque();
	private boolean[] aBooleanArray4306 = new boolean[112];
	private Component aComponent4307;

	private final void method1646(boolean bool) {
		if (aComponent4307 != null) {
			aComponent4307.removeKeyListener(this);
			aComponent4307.removeFocusListener(this);
			aComponent4307 = null;
			for (int i = 0; i < 112; i++)
				aBooleanArray4306[i] = false;
			if (bool)
				method1637(-96, -76);
			aClass155_4299.method1581(327680);
			aClass155_4305.method1581(327680);
		}
	}

	final synchronized void method1640(boolean bool) {
		aClass155_4299.method1581(327680);
		Class296_Sub52 class296_sub52 = (Class296_Sub52) aClass155_4305.method1573(1);
		if (bool != true)
			aBooleanArray4306 = null;
		for (/**/; class296_sub52 != null; class296_sub52 = (Class296_Sub52) aClass155_4305.method1573(1)) {
			class296_sub52.anInt3402 = method1648((byte) -126);
			if (class296_sub52.anInt3399 == 0) {
				if (!aBooleanArray4306[class296_sub52.anInt3401]) {
					Class296_Sub52 class296_sub52_0_ = new Class296_Sub52();
					class296_sub52_0_.anInt3399 = 0;
					class296_sub52_0_.anInt3401 = class296_sub52.anInt3401;
					class296_sub52_0_.anInt3402 = class296_sub52.anInt3402;
					class296_sub52_0_.aChar3398 = '\0';
					class296_sub52_0_.aLong3400 = class296_sub52.aLong3400;
					aClass155_4299.addLast((byte) -29, class296_sub52_0_);
					aBooleanArray4306[class296_sub52.anInt3401] = true;
				}
				class296_sub52.anInt3399 = 2;
				aClass155_4299.addLast((byte) 98, class296_sub52);
			} else if (class296_sub52.anInt3399 == 1) {
				if (aBooleanArray4306[class296_sub52.anInt3401]) {
					aClass155_4299.addLast((byte) -23, class296_sub52);
					aBooleanArray4306[class296_sub52.anInt3401] = false;
				}
			} else if (class296_sub52.anInt3399 == -1) {
				for (int i = 0; i < 112; i++) {
					if (aBooleanArray4306[i]) {
						Class296_Sub52 class296_sub52_1_ = new Class296_Sub52();
						class296_sub52_1_.anInt3401 = i;
						class296_sub52_1_.anInt3399 = 1;
						class296_sub52_1_.aLong3400 = class296_sub52.aLong3400;
						class296_sub52_1_.aChar3398 = '\0';
						class296_sub52_1_.anInt3402 = class296_sub52.anInt3402;
						aClass155_4299.addLast((byte) -59, class296_sub52_1_);
						aBooleanArray4306[i] = false;
					}
				}
			} else if (class296_sub52.anInt3399 == 3)
				aClass155_4299.addLast((byte) 125, class296_sub52);
		}
	}

	public final void focusGained(FocusEvent focusevent) {
		/* empty */
	}

	private final void method1647(Component component, boolean bool) {
		if (bool != true)
			method1637(120, 110);
		method1646(false);
		aComponent4307 = component;
		Method method = Class398.aMethod3347;
		if (method != null) {
			try {
				method.invoke(aComponent4307, new Object[]{Boolean.FALSE});
			} catch (Throwable throwable) {
				/* empty */
			}
		}
		aComponent4307.addKeyListener(this);
		aComponent4307.addFocusListener(this);
	}

	final boolean method1637(int i, int i_2_) {
		if (i_2_ <= 36)
			return true;
		if (i < 0 || i >= 112)
			return false;
		return aBooleanArray4306[i];
	}

	public final synchronized void keyPressed(KeyEvent keyevent) {
		method1651(0, keyevent, -1);
	}

	private final int method1648(byte i) {
		int i_3_ = -22 % ((-32 - i) / 50);
		int i_4_ = 0;
		if (aBooleanArray4306[81])
			i_4_ |= 0x1;
		if (aBooleanArray4306[82])
			i_4_ |= 0x4;
		if (aBooleanArray4306[86])
			i_4_ |= 0x2;
		return i_4_;
	}

	private final void method1649(int i, char c, int i_5_, boolean bool) {
		do {
			try {
				Class296_Sub52 class296_sub52 = new Class296_Sub52();
				class296_sub52.anInt3401 = i_5_;
				class296_sub52.anInt3399 = i;
				class296_sub52.aChar3398 = c;
				class296_sub52.aLong3400 = Class72.method771(-127);
				aClass155_4305.addLast((byte) 110, class296_sub52);
				if (!bool)
					break;
				aClass289_4298 = null;
			} catch (RuntimeException runtimeexception) {
				throw Class16_Sub2.method244(runtimeexception, ("uea.D(" + i + ',' + c + ',' + i_5_ + ',' + bool + ')'));
			}
			break;
		} while (false);
	}

	public final synchronized void keyReleased(KeyEvent keyevent) {
		method1651(1, keyevent, -1);
	}

	final void method1645(byte i) {
		method1646(false);
		if (i != -98)
			anInt4304 = 3;
	}

	public final synchronized void focusLost(FocusEvent focusevent) {
		method1649(-1, '\0', 0, false);
	}

	public final synchronized void keyTyped(KeyEvent keyevent) {
		char c = keyevent.getKeyChar();
		if (c != 0 && AnimationsLoader.method3301(c, -111)) {
			method1649(3, c, -1, false);
			keyevent.consume();
		}
	}

	public static void method1650(byte i) {
		anIntArray4300 = null;
		if (i == -58) {
			aClass55_4303 = null;
			aClass289_4298 = null;
		}
	}

	private final void method1651(int i, KeyEvent keyevent, int i_6_) {
		int i_7_ = keyevent.getKeyCode();
		if (i_7_ != 0) {
			if (i_7_ >= 0 && i_7_ < Class92.anIntArray994.length) {
				i_7_ = Class92.anIntArray994[i_7_];
				if (i != 0 || (i_7_ & 0x80) == 0)
					i_7_ &= ~0x80;
				else
					i_7_ = 0;
			} else
				i_7_ = 0;
		} else
			i_7_ = 0;
		if (i_7_ != 0) {
			method1649(i, '\0', i_7_, false);
			keyevent.consume();
			if (i_6_ != -1)
				anInt4301 = 30;
		}
	}

	final Interface1 method1639(int i) {
		if (i != 16171)
			method1639(-52);
		return (Interface1) aClass155_4299.method1573(i - 16170);
	}

	Class166_Sub1(Component component) {
		Class182.method1843(71);
		method1647(component, true);
	}
}
