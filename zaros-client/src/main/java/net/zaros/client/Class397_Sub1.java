package net.zaros.client;

/* Class397_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public abstract class Class397_Sub1 extends Sprite {
	public static int anInt5745;
	public static int anInt5746;
	private static int anInt5747;
	private static int anInt5748;
	public static int anInt5749;
	public int anInt5750;
	public static int anInt5751;
	public static int anInt5752;
	public static int anInt5753 = 0;
	private static int anInt5754;
	public static int anInt5755;
	public int anInt5756;
	public int anInt5757;
	public int anInt5758;
	public static int anInt5759;
	private static int anInt5760;
	public int anInt5761;
	private static int anInt5762;
	private static int anInt5763;
	public static int anInt5764;
	public static int anInt5765;
	public static int anInt5766;
	public static int anInt5767;
	public ha_Sub2 aHa_Sub2_5768;
	public static int anInt5769;
	public static int anInt5770;
	public static int anInt5771;
	public int anInt5772;
	public static int anInt5773;
	private int[] anIntArray5774;
	public static int anInt5775;
	public static int anInt5776;
	public static int anInt5777;

	private boolean method4100(float f, float f_0_, float f_1_, float f_2_, float f_3_, float f_4_) {
		int i = anInt5772 + anInt5758 + anInt5757;
		int i_5_ = anInt5761 + anInt5756 + anInt5750;
		if (i != anInt5758 || i_5_ != anInt5756) {
			float f_6_ = (f_1_ - f) / (float) i;
			float f_7_ = (f_2_ - f_0_) / (float) i;
			float f_8_ = (f_3_ - f) / (float) i_5_;
			float f_9_ = (f_4_ - f_0_) / (float) i_5_;
			float f_10_ = f_8_ * (float) anInt5761;
			float f_11_ = f_9_ * (float) anInt5761;
			float f_12_ = f_6_ * (float) anInt5772;
			float f_13_ = f_7_ * (float) anInt5772;
			float f_14_ = -f_6_ * (float) anInt5757;
			float f_15_ = -f_7_ * (float) anInt5757;
			float f_16_ = -f_8_ * (float) anInt5750;
			float f_17_ = -f_9_ * (float) anInt5750;
			f += f_12_ + f_10_;
			f_0_ += f_13_ + f_11_;
			f_1_ += f_14_ + f_10_;
			f_2_ += f_15_ + f_11_;
			f_3_ += f_12_ + f_16_;
			f_4_ += f_13_ + f_17_;
		}
		float f_18_ = f_3_ + (f_1_ - f);
		float f_19_ = f_2_ + (f_4_ - f_0_);
		float f_20_;
		float f_21_;
		if (f < f_1_) {
			f_20_ = f;
			f_21_ = f_1_;
		} else {
			f_20_ = f_1_;
			f_21_ = f;
		}
		if (f_3_ < f_20_)
			f_20_ = f_3_;
		if (f_18_ < f_20_)
			f_20_ = f_18_;
		if (f_3_ > f_21_)
			f_21_ = f_3_;
		if (f_18_ > f_21_)
			f_21_ = f_18_;
		float f_22_;
		float f_23_;
		if (f_0_ < f_2_) {
			f_22_ = f_0_;
			f_23_ = f_2_;
		} else {
			f_22_ = f_2_;
			f_23_ = f_0_;
		}
		if (f_4_ < f_22_)
			f_22_ = f_4_;
		if (f_19_ < f_22_)
			f_22_ = f_19_;
		if (f_4_ > f_23_)
			f_23_ = f_4_;
		if (f_19_ > f_23_)
			f_23_ = f_19_;
		if (f_20_ < (float) aHa_Sub2_5768.anInt4079)
			f_20_ = (float) aHa_Sub2_5768.anInt4079;
		if (f_21_ > (float) aHa_Sub2_5768.anInt4104)
			f_21_ = (float) aHa_Sub2_5768.anInt4104;
		if (f_22_ < (float) aHa_Sub2_5768.anInt4085)
			f_22_ = (float) aHa_Sub2_5768.anInt4085;
		if (f_23_ > (float) aHa_Sub2_5768.anInt4078)
			f_23_ = (float) aHa_Sub2_5768.anInt4078;
		f_21_ = f_20_ - f_21_;
		if (f_21_ >= 0.0F)
			return false;
		f_23_ = f_22_ - f_23_;
		if (f_23_ >= 0.0F)
			return false;
		anInt5776 = aHa_Sub2_5768.anInt4084;
		anInt5759 = (int) ((float) ((int) f_22_ * anInt5776) + f_20_);
		float f_24_ = (f_1_ - f) * (f_4_ - f_0_) - (f_2_ - f_0_) * (f_3_ - f);
		float f_25_ = (f_3_ - f) * (f_2_ - f_0_) - (f_4_ - f_0_) * (f_1_ - f);
		anInt5766 = (int) ((f_4_ - f_0_) * 4096.0F * (float) anInt5758 / f_24_);
		anInt5777 = (int) ((f_2_ - f_0_) * 4096.0F * (float) anInt5756 / f_25_);
		anInt5751 = (int) ((f_3_ - f) * 4096.0F * (float) anInt5758 / f_25_);
		anInt5745 = (int) ((f_1_ - f) * 4096.0F * (float) anInt5756 / f_24_);
		anInt5763 = (int) (f_20_ * 16.0F + 8.0F - (f + f_1_ + f_3_ + f_18_) / 4.0F * 16.0F);
		anInt5747 = (int) (f_22_ * 16.0F + 8.0F - (f_0_ + f_2_ + f_4_ + f_19_) / 4.0F * 16.0F);
		anInt5765 = (anInt5758 >> 1 << 12) + (anInt5747 * anInt5751 >> 4);
		anInt5775 = (anInt5756 >> 1 << 12) + (anInt5747 * anInt5745 >> 4);
		anInt5764 = anInt5763 * anInt5766 >> 4;
		anInt5769 = anInt5763 * anInt5777 >> 4;
		anInt5762 = (int) f_20_;
		anInt5770 = (int) f_21_;
		anInt5748 = (int) f_22_;
		anInt5771 = (int) f_23_;
		return true;
	}

	public void method4076(int[] is) {
		is[0] = anInt5772;
		is[1] = anInt5761;
		is[2] = anInt5757;
		is[3] = anInt5750;
	}

	public void method4097(int i, int i_26_, int i_27_, int i_28_) {
		anInt5772 = i;
		anInt5761 = i_26_;
		anInt5757 = i_27_;
		anInt5750 = i_28_;
	}

	public abstract void method4101(int i, int i_29_);

	public abstract void method4102(int[] is, int[] is_30_, int i, int i_31_);

	public abstract void method4079(int i, int i_32_, int i_33_, int i_34_, int i_35_);

	public abstract void method4103(int i, int i_36_, int i_37_, int i_38_, int i_39_, int i_40_, int i_41_, int i_42_,
			int i_43_);

	public void method4091(float f, float f_44_, float f_45_, float f_46_, float f_47_, float f_48_, int i, aa var_aa,
			int i_49_, int i_50_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		if (method4100(f, f_44_, f_45_, f_46_, f_47_, f_48_)) {
			aa_Sub2 var_aa_Sub2 = (aa_Sub2) var_aa;
			method4102(var_aa_Sub2.anIntArray3726, var_aa_Sub2.anIntArray3729, anInt5762 - i_49_,
					-i_50_ - (anInt5771 - anInt5748));
		}
	}

	public int method4099() {
		return anInt5772 + anInt5758 + anInt5757;
	}

	public void method4075(float f, float f_51_, float f_52_, float f_53_, float f_54_, float f_55_, int i, int i_56_,
			int i_57_, int i_58_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		if (method4100(f, f_51_, f_52_, f_53_, f_54_, f_55_)) {
			anInt5767 = i_56_;
			if (i != 1) {
				anInt5755 = i_56_ >>> 24;
				anInt5749 = 256 - anInt5755;
				if (i == 0) {
					anInt5746 = (i_56_ & 0xff0000) >> 16;
					anInt5753 = (i_56_ & 0xff00) >> 8;
					anInt5752 = i_56_ & 0xff;
				} else if (i == 2) {
					anInt5754 = i_56_ >>> 24;
					anInt5760 = 256 - anInt5754;
					int i_59_ = (i_56_ & 0xff00ff) * anInt5760 & ~0xff00ff;
					int i_60_ = (i_56_ & 0xff00) * anInt5760 & 0xff0000;
					anInt5773 = (i_59_ | i_60_) >>> 8;
				}
			}
			if (i == 1) {
				if (i_57_ == 0)
					method4101(1, 0);
				else if (i_57_ == 1)
					method4101(1, 1);
				else if (i_57_ == 2)
					method4101(1, 2);
			} else if (i == 0) {
				if (i_57_ == 0)
					method4101(0, 0);
				else if (i_57_ == 1)
					method4101(0, 1);
				else if (i_57_ == 2)
					method4101(0, 2);
			} else if (i == 3) {
				if (i_57_ == 0)
					method4101(3, 0);
				else if (i_57_ == 1)
					method4101(3, 1);
				else if (i_57_ == 2)
					method4101(3, 2);
			} else if (i == 2) {
				if (i_57_ == 0)
					method4101(2, 0);
				else if (i_57_ == 1)
					method4101(2, 1);
				else if (i_57_ == 2)
					method4101(2, 2);
			}
		}
	}

	public abstract void method4094(int i, int i_61_, int i_62_, int i_63_, int i_64_, int i_65_, int i_66_, int i_67_);

	public abstract void method4104(int i, int i_68_, int i_69_, int i_70_, int i_71_, int i_72_, int i_73_, int i_74_,
			int i_75_);

	public int method4088() {
		return anInt5761 + anInt5756 + anInt5750;
	}

	public int method4092() {
		return anInt5756;
	}

	public void method4098(int i, int i_76_, int i_77_, int i_78_, int i_79_, int i_80_, int i_81_) {
		if (aHa_Sub2_5768.method1256())
			throw new IllegalStateException();
		if (anIntArray5774 == null)
			anIntArray5774 = new int[4];
		aHa_Sub2_5768.K(anIntArray5774);
		aHa_Sub2_5768.T(aHa_Sub2_5768.anInt4079, aHa_Sub2_5768.anInt4085, i + i_77_, i_76_ + i_78_);
		int i_82_ = method4099();
		int i_83_ = method4088();
		int i_84_ = (i_77_ + i_82_ - 1) / i_82_;
		int i_85_ = (i_78_ + i_83_ - 1) / i_83_;
		for (int i_86_ = 0; i_86_ < i_85_; i_86_++) {
			int i_87_ = i_86_ * i_83_;
			for (int i_88_ = 0; i_88_ < i_84_; i_88_++)
				method4079(i + i_88_ * i_82_, i_76_ + i_87_, i_79_, i_80_, i_81_);
		}
		aHa_Sub2_5768.KA(anIntArray5774[0], anIntArray5774[1], anIntArray5774[2], anIntArray5774[3]);
	}

	public abstract void method4093(int i, int i_89_, aa var_aa, int i_90_, int i_91_);

	public int method4087() {
		return anInt5758;
	}

	public Class397_Sub1(ha_Sub2 var_ha_Sub2, int i, int i_92_) {
		aHa_Sub2_5768 = var_ha_Sub2;
		anInt5758 = i;
		anInt5756 = i_92_;
	}

	static {
		anInt5746 = 0;
		anInt5749 = 0;
		anInt5752 = 0;
		anInt5760 = 0;
		anInt5754 = 0;
		anInt5773 = 0;
		anInt5755 = 0;
	}
}
