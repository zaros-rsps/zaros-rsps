package net.zaros.client;

/* Class296_Sub51_Sub31 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub51_Sub31 extends TextureOperation {
	private int anInt6500 = 2048;
	private int anInt6501;
	private int anInt6502 = 2048;
	static Class213 aClass213_6503;
	static int anInt6504 = 1;
	private int anInt6505;
	static HashTable aClass263_6506;
	private int anInt6507;
	private int anInt6508;
	static byte[][] aByteArrayArray6509;
	static Js5 aClass138_6510;
	private int anInt6511;

	final void method3071(int i, Packet class296_sub17, int i_0_) {
		int i_1_ = i_0_;
		while_197_ : do {
			while_196_ : do {
				while_195_ : do {
					while_194_ : do {
						while_193_ : do {
							do {
								if (i_1_ != 0) {
									if (i_1_ != 1) {
										if (i_1_ != 2) {
											if (i_1_ != 3) {
												if (i_1_ != 4) {
													if (i_1_ != 5) {
														if (i_1_ == 6)
															break while_196_;
														break while_197_;
													}
												} else
													break while_194_;
												break while_195_;
											}
										} else
											break;
										break while_193_;
									}
								} else {
									anInt6502 = class296_sub17.g2();
									break while_197_;
								}
								anInt6508 = class296_sub17.g2();
								break while_197_;
							} while (false);
							anInt6501 = class296_sub17.g2();
							break while_197_;
						} while (false);
						anInt6500 = class296_sub17.g2();
						break while_197_;
					} while (false);
					anInt6505 = class296_sub17.g2();
					break while_197_;
				} while (false);
				anInt6507 = class296_sub17.g2();
				break while_197_;
			} while (false);
			anInt6511 = class296_sub17.g2();
		} while (false);
		if (i >= -84)
			aClass138_6510 = null;
	}

	private final boolean method3172(byte i, int i_2_, int i_3_) {
		int i_4_ = anInt6505 * (i_2_ - i_3_) >> 12;
		int i_5_ = Class259.anIntArray2419[i_4_ * 255 >> 12 & 0xff];
		int i_6_ = -113 / ((i - 41) / 58);
		i_5_ = (i_5_ << 12) / anInt6505;
		i_5_ = (i_5_ << 12) / anInt6511;
		i_5_ = anInt6507 * i_5_ >> 12;
		if (i_3_ + i_2_ >= i_5_ || i_2_ + i_3_ <= -i_5_)
			return false;
		return true;
	}

	final int[] get_monochrome_output(int i, int i_7_) {
		int[] is = aClass318_5035.method3335(i_7_, (byte) 28);
		if (i != 0)
			aByteArrayArray6509 = null;
		if (aClass318_5035.aBoolean2819) {
			int i_8_ = Class294.anIntArray2686[i_7_] - 2048;
			for (int i_9_ = 0; i_9_ < Class41_Sub10.anInt3769; i_9_++) {
				int i_10_ = Class33.anIntArray334[i_9_] - 2048;
				int i_11_ = anInt6502 + i_10_;
				i_11_ = i_11_ < -2048 ? i_11_ + 4096 : i_11_;
				i_11_ = i_11_ > 2048 ? i_11_ - 4096 : i_11_;
				int i_12_ = i_8_ + anInt6508;
				i_12_ = i_12_ < -2048 ? i_12_ + 4096 : i_12_;
				i_12_ = i_12_ <= 2048 ? i_12_ : i_12_ - 4096;
				int i_13_ = anInt6501 + i_10_;
				i_13_ = i_13_ >= -2048 ? i_13_ : i_13_ + 4096;
				i_13_ = i_13_ <= 2048 ? i_13_ : i_13_ - 4096;
				int i_14_ = anInt6500 + i_8_;
				i_14_ = i_14_ >= -2048 ? i_14_ : i_14_ + 4096;
				i_14_ = i_14_ <= 2048 ? i_14_ : i_14_ - 4096;
				is[i_9_] = (!method3172((byte) 111, i_12_, i_11_) && !method3174(i_14_, (byte) 40, i_13_)) ? 0 : 4096;
			}
		}
		return is;
	}

	final void method3076(byte i) {
		Class367.method3800(4096);
		int i_15_ = 94 % ((-58 - i) / 40);
	}

	public Class296_Sub51_Sub31() {
		super(0, true);
		anInt6501 = 0;
		anInt6505 = 12288;
		anInt6507 = 4096;
		anInt6511 = 8192;
		anInt6508 = 0;
	}

	public static void method3173(byte i) {
		if (i == -2) {
			aClass138_6510 = null;
			aByteArrayArray6509 = null;
			aClass263_6506 = null;
			aClass213_6503 = null;
		}
	}

	private final boolean method3174(int i, byte i_16_, int i_17_) {
		if (i_16_ != 40)
			method3071(78, null, 7);
		int i_18_ = anInt6505 * (i + i_17_) >> 12;
		int i_19_ = Class259.anIntArray2419[(i_18_ * 255 & 0xffc08) >> 12];
		i_19_ = (i_19_ << 12) / anInt6505;
		i_19_ = (i_19_ << 12) / anInt6511;
		i_19_ = anInt6507 * i_19_ >> 12;
		if (-i_17_ + i >= i_19_ || -i_19_ >= i - i_17_)
			return false;
		return true;
	}

	static {
		aClass213_6503 = new Class213(1);
		aClass263_6506 = new HashTable(8);
	}
}
