package net.zaros.client;

/* Class274 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class274 {
	static int anInt2523 = -1;
	static Class381 aClass381_2524;

	static final int method2314(int i, int i_0_, int i_1_, int i_2_) {
		if (Class41_Sub13.anInt3773 < 100)
			return -2;
		int i_3_ = -2;
		int i_4_ = i;
		int i_5_ = i_2_ - Class106.anInt1111;
		int i_6_ = i_1_ - Class106.anInt1117;
		for (Class296_Sub53 class296_sub53 = ((Class296_Sub53) Class106.aClass155_1101.removeFirst((byte) 118)); class296_sub53 != null; class296_sub53 = (Class296_Sub53) Class106.aClass155_1101.removeNext(1001)) {
			if (i_0_ == class296_sub53.anInt5046) {
				int i_7_ = class296_sub53.anInt5047;
				int i_8_ = class296_sub53.anInt5042;
				int i_9_ = (i_7_ + Class106.anInt1111 << 14 | Class106.anInt1117 + i_8_);
				int i_10_ = ((-i_7_ + i_5_) * (-i_7_ + i_5_) + (-i_8_ + i_6_) * (i_6_ - i_8_));
				if (i_3_ < 0 || i_4_ > i_10_) {
					i_4_ = i_10_;
					i_3_ = i_9_;
				}
			}
		}
		return i_3_;
	}

	static final void method2315(boolean bool, byte i) {
		if (bool) {
			if (Class99.anInt1064 != -1)
				Class100.method877(-100, Class99.anInt1064);
			for (Class296_Sub13 class296_sub13 = ((Class296_Sub13) Class386.aClass263_3264.getFirst(true)); class296_sub13 != null; class296_sub13 = ((Class296_Sub13) Class386.aClass263_3264.getNext(0))) {
				if (!class296_sub13.isLinked((byte) -54)) {
					class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.getFirst(true);
					if (class296_sub13 == null)
						break;
				}
				Class47.method596(class296_sub13, true, false, (byte) 53);
			}
			Class99.anInt1064 = -1;
			Class386.aClass263_3264 = new HashTable(8);
			Class187.method1888((byte) -118);
			Class99.anInt1064 = NPCDefinition.aClass268_1478.anInt2490;
			Animator.method569(false, (byte) 99);
			Class366_Sub8.method3794(true);
			CS2Executor.method1521(Class99.anInt1064);
		}
		Class338_Sub8_Sub1.method3605(1);
		Class342.aBoolean2990 = false;
		Class308.method3291((byte) 119);
		Class41_Sub19.anInt3793 = -1;
		StaticMethods.method2479(-1, Class296_Sub51_Sub32.anInt6512);
		Class296_Sub51_Sub11.localPlayer = new Player();
		Class296_Sub51_Sub11.localPlayer.tileY = Class296_Sub38.currentMapSizeY * 512 / 2;
		Class296_Sub51_Sub11.localPlayer.waypointQueueX[0] = Class198.currentMapSizeX / 2;
		Class296_Sub51_Sub11.localPlayer.tileX = Class198.currentMapSizeX * 512 / 2;
		int i_11_ = 100 / ((i + 10) / 53);
		Class219.camPosX = Class124.camPosZ = 0;
		Class296_Sub51_Sub11.localPlayer.wayPointQueueY[0] = Class296_Sub38.currentMapSizeY / 2;
		if (Class361.anInt3103 != 2)
			Class296_Sub51_Sub27_Sub1.method3157(false);
		else {
			Class219.camPosX = Class44_Sub1.cameraDestX << 9;
			Class124.camPosZ = Class5.cameraDestZ << 9;
		}
		Class368_Sub22.method3866(11);
	}

	static final int method2316(byte i, int i_12_, int i_13_) {
		if (i != 79)
			method2317(false);
		int i_14_ = 0;
		for (/**/; i_13_ > 0; i_13_--) {
			i_14_ = i_14_ << 1 | i_12_ & 0x1;
			i_12_ >>>= 1;
		}
		return i_14_;
	}

	public static void method2317(boolean bool) {
		if (bool == true)
			aClass381_2524 = null;
	}
}
