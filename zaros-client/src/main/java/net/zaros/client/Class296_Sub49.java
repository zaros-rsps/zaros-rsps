package net.zaros.client;

/* Class296_Sub49 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class296_Sub49 extends Node {
	int anInt4977;
	int anInt4978;
	int anInt4979;
	int anInt4980;
	int anInt4981;
	int anInt4982;
	static int[] anIntArray4983 = new int[2];
	int anInt4984;
	int anInt4985;
	int anInt4986;

	static final void method3046(byte[][] is, Class181_Sub1 class181_sub1, int i) {
		for (int i_0_ = 0; class181_sub1.anInt1870 > i_0_; i_0_++) {
			Class108.method948(-2060);
			for (int i_1_ = 0; Class198.currentMapSizeX >> 3 > i_1_; i_1_++) {
				for (int i_2_ = 0; i_2_ < Class296_Sub38.currentMapSizeY >> 3; i_2_++) {
					int i_3_ = Class181.buildedMapParts[i_0_][i_1_][i_2_];
					if (i_3_ != -1) {
						int i_4_ = (i_3_ & 0x3623f47) >> 24;
						if (!class181_sub1.aBoolean1860 || i_4_ == 0) {
							int i_5_ = (i_3_ & 0x6) >> 1;
							int i_6_ = i_3_ >> 14 & 0x3ff;
							int i_7_ = (i_3_ & 0x3ffe) >> 3;
							int i_8_ = (i_6_ / 8 << 8) + i_7_ / 8;
							for (int i_9_ = 0; i_9_ < Class296_Sub43.anIntArray4940.length; i_9_++) {
								if (i_8_ == Class296_Sub43.anIntArray4940[i_9_] && is[i_9_] != null) {
									Packet class296_sub17 = new Packet(is[i_9_]);
									class181_sub1.method1821(i_1_ * 8, i_7_, class296_sub17, i_4_, i_5_, BITConfigDefinition.mapClips, 0, i_2_ * 8, i_0_, i_6_);
									class181_sub1.method1832(true, class296_sub17, Class41_Sub13.aHa3774, i_4_, i_5_, i_2_ * 8, i_0_, i_1_ * 8, i_6_, i_7_);
									break;
								}
							}
						}
					}
				}
			}
		}
		if (i < 51)
			anIntArray4983 = null;
		for (int i_10_ = 0; i_10_ < class181_sub1.anInt1870; i_10_++) {
			Class108.method948(-2060);
			for (int i_11_ = 0; i_11_ < Class198.currentMapSizeX >> 3; i_11_++) {
				for (int i_12_ = 0; Class296_Sub38.currentMapSizeY >> 3 > i_12_; i_12_++) {
					int i_13_ = (Class181.buildedMapParts[i_10_][i_11_][i_12_]);
					if (i_13_ == -1)
						class181_sub1.method1823(8, i_11_ * 8, (byte) -123, i_10_, 8, i_12_ * 8);
				}
			}
		}
	}

	static final String parseName(String string) {
		if (string == null)
			return null;
		int i = 0;
		int i_14_;
		for (i_14_ = string.length(); i < i_14_; i++) {
			if (!Class140.method1470(string.charAt(i), (byte) 31))
				break;
		}
		for (/**/; i_14_ > i; i_14_--) {
			if (!Class140.method1470(string.charAt(i_14_ - 1), (byte) -111))
				break;
		}
		int i_15_ = -i + i_14_;
		if (i_15_ < 1 || i_15_ > 12)
			return null;
		StringBuffer stringbuffer = new StringBuffer(i_15_);
		for (int i_16_ = i; i_14_ > i_16_; i_16_++) {
			char c = string.charAt(i_16_);
			if (Class29.method324((byte) 125, c)) {
				char c_17_ = Class296_Sub40.method2913(c, false);
				if (c_17_ != 0)
					stringbuffer.append(c_17_);
			}
		}
		if (stringbuffer.length() == 0)
			return null;
		return stringbuffer.toString();
	}

	static final void method3048(byte i) {
		Class320.method3341(false);
		if (i > -106)
			anIntArray4983 = null;
	}

	public static void method3049(byte i) {
		anIntArray4983 = null;
		if (i <= 85)
			parseName(null);
	}

	Class296_Sub49(Packet class296_sub17) {
		int i = class296_sub17.g4();
		anInt4982 = i >>> 28;
		anInt4986 = (i & 0xfffd63e) >>> 14;
		anInt4980 = i & 0x3fff;
		anInt4977 = class296_sub17.g1();
		anInt4985 = class296_sub17.g1();
		anInt4981 = class296_sub17.g1();
		anInt4978 = class296_sub17.g1();
		anInt4984 = class296_sub17.g1();
		anInt4979 = class296_sub17.g1();
	}
}
