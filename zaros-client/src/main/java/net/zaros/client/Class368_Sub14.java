package net.zaros.client;

/* Class368_Sub14 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class368_Sub14 extends Class368 {
	private int anInt5508;
	static int anInt5509;
	private MidiDecoder aClass296_Sub47_5510;
	private int anInt5511;

	final void method3807(byte i) {
		Class296_Sub15.method2513(aClass296_Sub47_5510, 99, anInt5508);
		int i_0_ = 91 % ((52 - i) / 52);
	}

	static final void method3850(ha var_ha, byte i) {
		if (Class318.aBoolean2814)
			Class103.method896(0, var_ha);
		else
			Class338_Sub3_Sub4_Sub2.method3581(var_ha, (byte) -119);
		int i_1_ = -65 / ((i - 55) / 33);
	}

	Class368_Sub14(Packet class296_sub17) {
		super(class296_sub17);
		anInt5511 = class296_sub17.g2();
		anInt5508 = class296_sub17.g1();
	}

	final boolean method3805(boolean bool) {
		if (bool != true)
			method3807((byte) -77);
		if (aClass296_Sub47_5510 == null)
			aClass296_Sub47_5510 = MidiDecoder.method3042(Class42_Sub4.fs6, anInt5511, 0);
		if (aClass296_Sub47_5510 == null)
			return false;
		if (!IOException_Sub1.method133(aClass296_Sub47_5510, (byte) -69))
			return false;
		return true;
	}
}
