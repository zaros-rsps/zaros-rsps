package net.zaros.client;
/* Class260 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class SubInPacket {
	static MidiDecoder aClass296_Sub47_2421;
	static float aFloat2422;

	static final boolean method2242(int i, int i_0_, int i_1_) {
		int i_2_ = -95 % ((-26 - i_0_) / 54);
		if ((i & 0x800) == 0)
			return false;
		return true;
	}

	public static void method2243(boolean bool) {
		aClass296_Sub47_2421 = null;
		if (bool)
			aFloat2422 = -0.05407866F;
	}

	static final Class140 method2245(int i, Packet class296_sub17) {
		int i_4_ = class296_sub17.g1();
		Class252 class252 = StaticMethods.method312((byte) -33)[class296_sub17.g1()];
		Class357 class357 = Class110.method970(i + 2)[class296_sub17.g1()];
		int i_5_ = class296_sub17.g2b();
		int i_6_ = class296_sub17.g2b();
		int i_7_ = class296_sub17.g2();
		int i_8_ = class296_sub17.g2();
		int i_9_ = class296_sub17.g4();
		int i_10_ = class296_sub17.g4();
		int i_11_ = class296_sub17.g4();
		boolean bool = (class296_sub17.g1() ^ 0xffffffff) == i;
		return new Class140(i_4_, class252, class357, i_5_, i_6_, i_7_, i_8_, i_9_, i_10_, i_11_, bool);
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	SubInPacket(int i, int i_32_) {
		/* empty */
	}
}
