package net.zaros.client;

/* Class67 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class67 {
	int anInt741;
	static Class67 aClass67_742 = new Class67(1);
	static Class67 aClass67_743 = new Class67(2);
	static Class67 aClass67_744 = new Class67(4);
	static Class67 aClass67_745 = new Class67(1);
	static Class67 aClass67_746 = new Class67(2);
	static Class67 aClass67_747 = new Class67(4);
	static Class67 aClass67_748 = new Class67(2);
	static Class67 aClass67_749 = new Class67(4);
	static IncomingPacket aClass231_750 = new IncomingPacket(0, 2);
	static long[] aLongArray751 = new long[100];
	static float aFloat752;
	static int anInt753;
	static Class230 aClass230_754 = new Class230();
	static Class181_Sub1 aClass181_Sub1_755;

	static final boolean method711(byte i) {
		boolean bool = true;
		if (Class238.aClass186_3629 == null) {
			if (!Class205_Sub2.fs8.hasEntryBuffer(Class338_Sub3_Sub1_Sub2.anInt6759))
				bool = false;
			else
				Class238.aClass186_3629 = Class186.method1872(Class205_Sub2.fs8, Class338_Sub3_Sub1_Sub2.anInt6759);
		}
		if (i >= -85)
			return true;
		if (Class41_Sub18.aClass186_3788 == null) {
			if (!Class205_Sub2.fs8.hasEntryBuffer(StaticMethods.anInt5910))
				bool = false;
			else
				Class41_Sub18.aClass186_3788 = Class186.method1872(Class205_Sub2.fs8, StaticMethods.anInt5910);
		}
		if (Class288.aClass186_2654 == null) {
			if (!Class205_Sub2.fs8.hasEntryBuffer(Class18.anInt6160))
				bool = false;
			else
				Class288.aClass186_2654 = Class186.method1872(Class205_Sub2.fs8, Class18.anInt6160);
		}
		if (Class296_Sub51_Sub21.aClass92_6447 == null) {
			if (!LookupTable.aClass138_53.hasEntryBuffer(Class122_Sub3.anInt5666))
				bool = false;
			else
				Class296_Sub51_Sub21.aClass92_6447 = Class259.method2240(LookupTable.aClass138_53, (byte) 10, Class122_Sub3.anInt5666);
		}
		if (Class154.aClass186Array1583 == null) {
			if (Class205_Sub2.fs8.hasEntryBuffer(Class122_Sub3.anInt5666))
				Class154.aClass186Array1583 = Class186.method1868(Class205_Sub2.fs8, Class122_Sub3.anInt5666);
			else
				bool = false;
		}
		return bool;
	}

	public final String toString() {
		throw new IllegalStateException();
	}

	private Class67(int i) {
		anInt741 = i;
	}

	static final void method712(byte i) {
		if (i != 43)
			method714(4);
		Class41_Sub8.anIntArray3765 = Class366_Sub1.method3772((byte) 98, 8, 0.4F, 2048, true, 8, 4, 35);
	}

	static final int method713(int i, int i_0_, int i_1_, int i_2_) {
		i_0_ &= 0x3;
		if (i_0_ == 0)
			return i_2_;
		if (i != 7)
			method714(97);
		if (i_0_ == 1)
			return -i_1_ + 7;
		if (i_0_ == 2)
			return -i_2_ + 7;
		return i_1_;
	}

	public static void method714(int i) {
		aClass67_748 = null;
		aClass67_742 = null;
		aClass67_745 = null;
		aClass67_747 = null;
		aClass231_750 = null;
		aClass67_743 = null;
		aLongArray751 = null;
		aClass230_754 = null;
		if (i == 4) {
			aClass67_749 = null;
			aClass67_744 = null;
			aClass67_746 = null;
			aClass181_Sub1_755 = null;
		}
	}

	static final boolean method715(int i, int i_3_, int i_4_, int i_5_, int i_6_, int i_7_) {
		for (int i_8_ = i_7_; i_8_ <= i; i_8_++) {
			for (int i_9_ = i_3_; i_6_ >= i_9_; i_9_++) {
				if (Class31.anIntArrayArray325[i_8_][i_9_] == i_4_ && StaticMethods.anIntArrayArray5929[i_8_][i_9_] <= 1)
					return true;
			}
		}
		int i_10_ = 75 / ((i_5_ + 1) / 36);
		return false;
	}
}
