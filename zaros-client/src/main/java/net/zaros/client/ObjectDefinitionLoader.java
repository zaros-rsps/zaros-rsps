package net.zaros.client;

/* Class38 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.File;
import java.lang.reflect.Method;

final class ObjectDefinitionLoader {
	Js5 aClass138_368;
	boolean aBoolean369 = false;
	private AdvancedMemoryCache cachedDefinitions = new AdvancedMemoryCache(64);
	boolean membersOnly;
	private Js5 objectsDefsFS;
	private GameType GameType;
	static IncomingPacket aClass231_374 = new IncomingPacket(10, 2);
	private int languageID;
	static Queue gameTaskQueue = new Queue();
	AdvancedMemoryCache aClass113_377 = new AdvancedMemoryCache(500);
	AdvancedMemoryCache aClass113_378 = new AdvancedMemoryCache(30);
	static int anInt379;
	static int[] anIntArray380 = new int[3];
	AdvancedMemoryCache aClass113_381 = new AdvancedMemoryCache(50);
	int anInt382;
	private String[] defaultActions;

	final void method365(int i, byte i_0_) {
		if (i_0_ >= 12) {
			cachedDefinitions = new AdvancedMemoryCache(i);
		}
	}

	static final void method366(byte i) {
		if (i != 36) {
			method376(-122);
		}
		if (Class294.sessionKey != null) {
			Class296_Sub41.method2915(Class220.anInt2150, false);
		} else if (Class296_Sub51_Sub14.anInt6423 == -1) {
			Class368_Sub20.method3860((byte) -64, Class220.anInt2150, Class379.aString3625, Class286.aString2643);
		} else {
			Applet_Sub1.method98(2, Class220.anInt2150);
		}
	}

	final void method367(byte i, int i_1_) {
		synchronized (cachedDefinitions) {
			cachedDefinitions.clean(i_1_);
		}
		synchronized (aClass113_377) {
			aClass113_377.clean(i_1_);
			if (i < 99) {
				method365(-43, (byte) -63);
			}
		}
		synchronized (aClass113_378) {
			aClass113_378.clean(i_1_);
		}
		synchronized (aClass113_381) {
			aClass113_381.clean(i_1_);
		}
	}

	final ObjectDefinition getObjectDefinition(int objectID) {
		ObjectDefinition def;
		synchronized (cachedDefinitions) {
			def = (ObjectDefinition) cachedDefinitions.get(objectID);
		}
		if (def != null) {
			return def;
		}
		byte[] data;
		synchronized (objectsDefsFS) {
			data = objectsDefsFS.getFile(ObjectDefinitionLoader.objectFileID(objectID), ObjectDefinitionLoader.objectSubfileID(objectID));
		}
		def = new ObjectDefinition();
		def.loader = this;
		def.ID = objectID;
		def.options = defaultActions.clone();
		if (data != null) {
			boolean newObject = data[0] == 'N' && data[1] == 'E' && data[2] == 'W';
			def.init(new Packet(data), newObject);
		}
		def.method763((byte) -115);
		if (!membersOnly && def.membersOnly) {
			def.options = null;
			def.anIntArray783 = null;
		}
		if (def.notClipped) {
			def.projectileClipped = false;
			def.blockingType = 0;
		}
		DefinitionManipulator.manipulateObject(objectID, def);
		synchronized (cachedDefinitions) {
			cachedDefinitions.put(def, objectID);
		}
		return def;
	}

	static final void method369(int i, int i_2_) {
		int i_3_ = 52 % ((-7 - i) / 51);
		for (Node class296 = Class149.interfaceSettings.getFirst(true); class296 != null; class296 = Class149.interfaceSettings.getNext(0)) {
			if ((class296.uid >> 48 & 0xffffL) == i_2_) {
				class296.unlink();
			}
		}
	}

	final void setMembersOnly(boolean only) {
		if (only != membersOnly) {
			membersOnly = only;
			clearCached();
		}
	}

	static final boolean method371(Class var_class, String string, int i) {
		Class var_class_4_ = (Class) Class296_Sub9.aHashtable4632.get(string);
		if (var_class_4_ != null) {
			if (var_class_4_.getClassLoader() != var_class.getClassLoader()) {
				return false;
			}
			return true;
		}
		File file = null;
		if (i != 65535) {
			anInt379 = 54;
		}
		if (file == null) {
			file = (File) Class154.aHashtable1581.get(string);
		}
		if (file != null) {
			try {
				file = new File(file.getCanonicalPath());
				System.load(file.getPath());
				Class var_class_5_ = java.lang.Runtime.class;
				Class var_class_6_ = java.lang.reflect.AccessibleObject.class;
				Method method = var_class_6_.getDeclaredMethod("setAccessible", new Class[] { Boolean.TYPE });
				Method method_7_ = var_class_5_.getDeclaredMethod("load0", new Class[] { java.lang.Class.class, java.lang.String.class });
				method.invoke(method_7_, new Object[] { Boolean.TRUE });
				method_7_.invoke(Runtime.getRuntime(), new Object[] { var_class, file.getPath() });
				method.invoke(method_7_, new Object[] { Boolean.FALSE });
				Class296_Sub9.aHashtable4632.put(string, var_class);
				return true;
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
				Class296_Sub9.aHashtable4632.put(string, Class351.class);
				return true;
			} catch (Throwable e) {
				e.printStackTrace();
				/* empty */
			}
		}
		return false;
	}

	final void clearCached() {
		synchronized (cachedDefinitions) {
			cachedDefinitions.clear();
		}
		synchronized (aClass113_377) {
			aClass113_377.clear();
		}
		synchronized (aClass113_378) {
			aClass113_378.clear();
		}
		synchronized (aClass113_381) {
			aClass113_381.clear();
		}
	}

	final void method373(boolean bool, byte i) {
		if (aBoolean369 == !bool && i <= -42) {
			aBoolean369 = bool;
			clearCached();
		}
	}

	final void method374(int i, int i_8_) {
		if (i != 0) {
			method376(103);
		}
		anInt382 = i_8_;
		synchronized (aClass113_377) {
			aClass113_377.clear();
		}
		synchronized (aClass113_378) {
			aClass113_378.clear();
		}
		synchronized (aClass113_381) {
			aClass113_381.clear();
		}
	}

	final void method375(boolean bool) {
		synchronized (cachedDefinitions) {
			cachedDefinitions.clearSoftReferences();
		}
		synchronized (aClass113_377) {
			aClass113_377.clearSoftReferences();
		}
		synchronized (aClass113_378) {
			aClass113_378.clearSoftReferences();
		}
		synchronized (aClass113_381) {
			aClass113_381.clearSoftReferences();
		}
		if (bool) {
			anIntArray380 = null;
		}
	}

	static final int objectSubfileID(int objectSubfileID) {
		return objectSubfileID & 0xff;
	}

	static final int objectFileID(int objectID) {
		return objectID >>> 8;
	}

	public static void method376(int i) {
		anIntArray380 = null;
		gameTaskQueue = null;
		aClass231_374 = null;
		if (i > -97) {
			method369(115, 118);
		}
	}

	ObjectDefinitionLoader(GameType class35, int i, boolean bool, Js5 class138, Js5 class138_9_) {
		aClass138_368 = class138_9_;
		objectsDefsFS = class138;
		languageID = i;
		membersOnly = bool;
		GameType = class35;
		if (objectsDefsFS != null) {
			int i_10_ = objectsDefsFS.getLastGroupId() - 1;
			objectsDefsFS.getLastFileId(i_10_);
		}
		if (GameType != Class363.runescape) {
			defaultActions = new String[] { null, null, null, null, null, null };
		} else {
			defaultActions = new String[] { null, null, null, null, null, TranslatableString.aClass120_1218.getTranslation(languageID) };
		}
	}
}
