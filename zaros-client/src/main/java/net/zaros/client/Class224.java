package net.zaros.client;

/* Class224 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class224 {
	int anInt2167;
	int anInt2168;
	static int anInt2169;
	int anInt2170;
	int anInt2171;
	static float[] aFloatArray2172 = new float[4];

	static final void method2079(boolean bool, int i) {
		if (i != -19621)
			anInt2169 = 75;
		if (bool) {
			if (Class99.anInt1064 != -1)
				Class100.method877(-116, Class99.anInt1064);
			for (Class296_Sub13 class296_sub13 = ((Class296_Sub13) Class386.aClass263_3264.getFirst(true)); class296_sub13 != null; class296_sub13 = ((Class296_Sub13) Class386.aClass263_3264.getNext(0))) {
				if (!class296_sub13.isLinked((byte) -111)) {
					class296_sub13 = (Class296_Sub13) Class386.aClass263_3264.getFirst(true);
					if (class296_sub13 == null)
						break;
				}
				Class47.method596(class296_sub13, true, false, (byte) 54);
			}
			Class99.anInt1064 = -1;
			Class386.aClass263_3264 = new HashTable(8);
			Class187.method1888((byte) -48);
			Class99.anInt1064 = NPCDefinition.aClass268_1478.anInt2493;
			Animator.method569(false, (byte) 86);
			Class366_Sub8.method3794(true);
			CS2Executor.method1521(Class99.anInt1064);
		}
		Class342.aBoolean2990 = true;
	}

	static final void method2080(int i, int i_0_) {
		if (i != -25672)
			aFloatArray2172 = null;
		GameLoopTask class296_sub39_sub5 = Class296_Sub39_Sub6.makeGameLoopTask((long) i_0_, 5);
		class296_sub39_sub5.insertIntoQueue();
	}

	public static void method2081(byte i) {
		aFloatArray2172 = null;
		if (i < 5)
			method2080(11, -26);
	}

	public Class224() {
		/* empty */
	}

	static final void method2082(byte i) {
		Class338_Sub3_Sub5_Sub1.anInt6645 = 0;
		Class210_Sub1.foundX = -1;
		if (i <= 49)
			anInt2169 = 24;
		Class205.foundY = -1;
		Class296_Sub39_Sub19.anInt6250 = -1;
	}
}
