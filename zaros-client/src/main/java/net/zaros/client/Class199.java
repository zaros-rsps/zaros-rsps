package net.zaros.client;

/* Class199 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class199 {
	static String[] aStringArray2004;
	static Js5 aClass138_2005;
	static boolean aBoolean2006 = false;
	static Class287 aClass287_2007 = new Class287();

	public static void method1946(int i) {
		aClass138_2005 = null;
		aStringArray2004 = null;
		aClass287_2007 = null;
		if (i != -1500698260)
			method1947(31, false, -16, 45, -91, -55);
	}

	static final void method1947(int i, boolean bool, int i_0_, int i_1_, int i_2_, int i_3_) {
		int i_4_ = i_0_ - i_2_;
		int i_5_ = i_3_ - i_1_;
		if (i_4_ != 0) {
			if (i_5_ == 0) {
				Class54.method652(i_2_, (byte) 84, i_1_, i_0_, i);
				return;
			}
		} else {
			if (i_5_ != 0)
				Class296_Sub39_Sub17.method2892(i, i_1_, -30124, i_3_, i_2_);
			return;
		}
		int i_6_ = (i_5_ << 12) / i_4_;
		int i_7_ = -(i_2_ * i_6_ >> 12) + i_1_;
		if (bool)
			method1947(29, true, -21, -114, -111, 121);
		int i_8_;
		int i_9_;
		if (i_0_ < ConfigurationDefinition.anInt676) {
			i_8_ = (i_6_ * ConfigurationDefinition.anInt676 >> 12) + i_7_;
			i_9_ = ConfigurationDefinition.anInt676;
		} else if (Class288.anInt2652 < i_0_) {
			i_8_ = i_7_ + (i_6_ * Class288.anInt2652 >> 12);
			i_9_ = Class288.anInt2652;
		} else {
			i_8_ = i_3_;
			i_9_ = i_0_;
		}
		int i_10_;
		int i_11_;
		if (i_2_ < ConfigurationDefinition.anInt676) {
			i_10_ = i_7_ + (ConfigurationDefinition.anInt676 * i_6_ >> 12);
			i_11_ = ConfigurationDefinition.anInt676;
		} else if (Class288.anInt2652 >= i_2_) {
			i_10_ = i_1_;
			i_11_ = i_2_;
		} else {
			i_10_ = i_7_ + (i_6_ * Class288.anInt2652 >> 12);
			i_11_ = Class288.anInt2652;
		}
		if (i_10_ >= EmissiveTriangle.anInt952) {
			if (i_10_ > RuntimeException_Sub1.anInt3391) {
				i_10_ = RuntimeException_Sub1.anInt3391;
				i_11_ = (RuntimeException_Sub1.anInt3391 - i_7_ << 12) / i_6_;
			}
		} else {
			i_11_ = (-i_7_ + EmissiveTriangle.anInt952 << 12) / i_6_;
			i_10_ = EmissiveTriangle.anInt952;
		}
		if (EmissiveTriangle.anInt952 > i_8_) {
			i_8_ = EmissiveTriangle.anInt952;
			i_9_ = (-i_7_ + EmissiveTriangle.anInt952 << 12) / i_6_;
		} else if (i_8_ > RuntimeException_Sub1.anInt3391) {
			i_9_ = (-i_7_ + RuntimeException_Sub1.anInt3391 << 12) / i_6_;
			i_8_ = RuntimeException_Sub1.anInt3391;
		}
		Class32.method342(i_9_, i_8_, i, (byte) -119, i_11_, i_10_);
	}
}
