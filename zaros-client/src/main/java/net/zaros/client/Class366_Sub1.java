package net.zaros.client;
/* Class366_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Calendar;
import java.util.TimeZone;

import jaggl.OpenGL;

final class Class366_Sub1 extends Class366 {
	private boolean aBoolean5360;
	private boolean aBoolean5361 = false;
	private Class361 aClass361_5362;
	static Calendar aCalendar5363 = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
	private Class389 aClass389_5364;

	final void method3768(byte i, boolean bool) {
		if (i != -88)
			aBoolean5360 = true;
		Class69_Sub3 class69_sub3 = aHa_Sub3_3121.method1329((byte) 8);
		if (aBoolean5360 && class69_sub3 != null) {
			aHa_Sub3_3121.method1330(123, 1);
			aHa_Sub3_3121.method1316(class69_sub3, (byte) -114);
			aHa_Sub3_3121.method1330(106, 0);
			aHa_Sub3_3121.method1316(aClass361_5362.aClass69_Sub2_3100, (byte) -110);
			long l = aClass389_5364.aLong3280;
			OpenGL.glUseProgramObjectARB(l);
			OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l, "normalSampler"), 0);
			OpenGL.glUniform1iARB(OpenGL.glGetUniformLocationARB(l, "envMapSampler"), 1);
			OpenGL.glUniform3fARB(OpenGL.glGetUniformLocationARB(l, "sunDir"), -aHa_Sub3_3121.aFloatArray4202[0], -aHa_Sub3_3121.aFloatArray4202[1], -aHa_Sub3_3121.aFloatArray4202[2]);
			OpenGL.glUniform4fARB(OpenGL.glGetUniformLocationARB(l, "sunColour"), aHa_Sub3_3121.aFloat4261, aHa_Sub3_3121.aFloat4186, aHa_Sub3_3121.aFloat4193, 1.0F);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "sunExponent"), Math.abs(aHa_Sub3_3121.aFloatArray4202[1]) * 928.0F + 96.0F);
			aBoolean5361 = true;
		}
	}

	final void method3766(int i) {
		if (aBoolean5361) {
			aHa_Sub3_3121.method1330(123, 1);
			aHa_Sub3_3121.method1316(null, (byte) -115);
			aHa_Sub3_3121.method1330(119, 0);
			aHa_Sub3_3121.method1316(null, (byte) -126);
			OpenGL.glUseProgramObjectARB(0L);
			aBoolean5361 = false;
		}
		if (i <= 30)
			method3763(-127);
	}

	static final boolean method3771(int i, int i_0_, int i_1_) {
		if (i_0_ != -1)
			aCalendar5363 = null;
		if ((i & 0x10) == 0)
			return false;
		return true;
	}

	final void method3764(boolean bool, int i, Class69 class69) {
		if (bool)
			aClass389_5364 = null;
		if (!aBoolean5361) {
			aHa_Sub3_3121.method1316(class69, (byte) -124);
			aHa_Sub3_3121.method1272((byte) -107, i);
		}
	}

	final void method3770(byte i, boolean bool) {
		if (i != 33)
			method3764(true, 4, null);
	}

	final boolean method3763(int i) {
		int i_2_ = -124 / ((i - 73) / 40);
		return false;
	}

	static final int[] method3772(byte i, int i_3_, float f, int i_4_, boolean bool, int i_5_, int i_6_, int i_7_) {
		int i_8_ = -85 / ((14 - i) / 63);
		int[] is = new int[i_4_];
		Class296_Sub51_Sub25 class296_sub51_sub25 = new Class296_Sub51_Sub25();
		class296_sub51_sub25.anInt6471 = (int) (f * 4096.0F);
		class296_sub51_sub25.anInt6467 = i_3_;
		class296_sub51_sub25.aBoolean6469 = bool;
		class296_sub51_sub25.anInt6474 = i_7_;
		class296_sub51_sub25.anInt6472 = i_6_;
		class296_sub51_sub25.anInt6470 = i_5_;
		class296_sub51_sub25.method3076((byte) 22);
		AdvancedMemoryCache.method998(i_4_, -1, 1);
		class296_sub51_sub25.method3147(0, (byte) 80, is);
		return is;
	}

	public static void method3773(byte i) {
		if (i <= 53)
			aCalendar5363 = null;
		aCalendar5363 = null;
	}

	final void method3769(int i, byte i_9_, int i_10_) {
		if (aBoolean5361) {
			int i_11_ = 1 << (i & 0x3);
			float f = (float) (1 << (i >> 3 & 0x7)) / 32.0F;
			int i_12_ = i_10_ & 0xffff;
			float f_13_ = (float) (i_10_ >> 16 & 0x3) / 8.0F;
			long l = aClass389_5364.aLong3280;
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "time"), (float) (aHa_Sub3_3121.anInt4134 * i_11_ % 40000) / 40000.0F);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "scale"), f);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "breakWaterDepth"), (float) i_12_);
			OpenGL.glUniform1fARB(OpenGL.glGetUniformLocationARB(l, "breakWaterOffset"), f_13_);
		}
		if (i_9_ != -81)
			aClass361_5362 = null;
	}

	Class366_Sub1(ha_Sub3 var_ha_Sub3, Class361 class361) {
		super(var_ha_Sub3);
		aBoolean5360 = false;
		aClass361_5362 = class361;
		if (aClass361_5362.aClass69_Sub2_3100 != null && aHa_Sub3_3121.aBoolean4229 && aHa_Sub3_3121.aBoolean4174) {
			Class77 class77 = (Class296_Sub51_Sub3.method3084(19399, aHa_Sub3_3121, "uniform float time;\nuniform float scale;\nvarying vec3 wvVertex;\nvarying float waterDepth;\nvoid main() {\nwaterDepth = gl_MultiTexCoord0.z;\nvec4 ecVertex = gl_ModelViewMatrix*gl_Vertex;\nwvVertex.x = dot(gl_NormalMatrix[0], ecVertex.xyz);\nwvVertex.y = dot(gl_NormalMatrix[1], ecVertex.xyz);\nwvVertex.z = dot(gl_NormalMatrix[2], ecVertex.xyz);\ngl_TexCoord[0].x = dot(gl_TextureMatrix[0][0], gl_MultiTexCoord0)*scale;\ngl_TexCoord[0].y = dot(gl_TextureMatrix[0][1], gl_MultiTexCoord0)*scale;\ngl_TexCoord[0].z = time;\ngl_TexCoord[0].w = 1.0;\ngl_FogFragCoord = 1.0-clamp((gl_Fog.end+ecVertex.z)*gl_Fog.scale, 0.0, 1.0);\ngl_Position = ftransform();\n}\n", 35633));
			Class77 class77_14_ = (Class296_Sub51_Sub3.method3084(19399, aHa_Sub3_3121, "varying vec3 wvVertex;\nvarying float waterDepth;\nuniform vec3 sunDir;\nuniform vec4 sunColour;\nuniform float sunExponent;\nuniform float breakWaterDepth;\nuniform float breakWaterOffset;\nuniform sampler3D normalSampler;\nuniform samplerCube envMapSampler;\nvoid main() {\nvec4 wnNormal = texture3D(normalSampler, gl_TexCoord[0].xyz).rbga;\nwnNormal.xyz = 2.0*wnNormal.xyz-1.0;\nvec3 wnVector = normalize(wvVertex);\nvec3 wnReflection = reflect(wnVector, wnNormal.xyz);\nvec3 envColour = textureCube(envMapSampler, wnReflection).rgb;\nvec4 specularColour = sunColour*pow(clamp(-dot(sunDir, wnReflection), 0.0, 1.0), sunExponent);\nfloat shoreFactor = clamp(waterDepth/breakWaterDepth-breakWaterOffset*wnNormal.w, 0.0, 1.0);\nfloat ndote = dot(wnVector, wnNormal.xyz);\nfloat fresnel = pow(1.0-abs(ndote), 2.0);\nvec4 surfaceColour = vec4(envColour, fresnel*shoreFactor)+specularColour*shoreFactor;\ngl_FragColor = vec4(mix(surfaceColour.rgb, gl_Fog.color.rgb, gl_FogFragCoord), surfaceColour.a);\n}\n", 35632));
			aClass389_5364 = Class296_Sub51_Sub19.method3127(1, (new Class77[]{class77, class77_14_}), aHa_Sub3_3121);
			aBoolean5360 = aClass389_5364 != null;
		}
	}
}
