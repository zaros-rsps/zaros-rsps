package net.zaros.client;

/* Class217_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

final class Class217_Sub3 extends Class217 {
	private int anInt4556 = 1;
	private long aLong4557;
	private long aLong4558;
	private long aLong4559 = 0L;
	private long[] aLongArray4560;
	private int anInt4561;

	final long method2036(boolean bool) {
		if (bool != true)
			method2034(true, 48L);
		return aLong4557;
	}

	private final long method2039(int i) {
		long l = Class72.method771(-125) * 1000000L;
		long l_0_ = l - aLong4558;
		aLong4558 = l;
		if (l_0_ > -5000000000L && l_0_ < 5000000000L) {
			aLongArray4560[anInt4561] = l_0_;
			anInt4561 = (anInt4561 + 1) % 10;
			if (anInt4556 < 10)
				anInt4556++;
		}
		if (i != 17330)
			return -54L;
		long l_1_ = 0L;
		for (int i_2_ = 1; anInt4556 >= i_2_; i_2_++)
			l_1_ += aLongArray4560[(-i_2_ + anInt4561 + 10) % 10];
		return l_1_ / (long) anInt4556;
	}

	final int method2034(boolean bool, long l) {
		if (bool)
			return -46;
		if (aLong4559 <= aLong4557) {
			int i = 0;
			do
				aLong4559 += l;
			while (++i < 10 && aLong4559 < aLong4557);
			if (aLong4557 > aLong4559)
				aLong4559 = aLong4557;
			return i;
		}
		aLong4558 += -aLong4557 + aLong4559;
		aLong4557 += aLong4559 + -aLong4557;
		aLong4559 += l;
		return 1;
	}

	final void method2033(boolean bool) {
		if (!bool) {
			aLong4558 = 0L;
			if (aLong4557 < aLong4559)
				aLong4557 += aLong4559 - aLong4557;
		}
	}

	final long method2032(byte i) {
		aLong4557 += method2039(17330);
		int i_3_ = -112 / ((-54 - i) / 35);
		if (aLong4557 < aLong4559)
			return (aLong4559 - aLong4557) / 1000000L;
		return 0L;
	}

	Class217_Sub3() {
		aLong4557 = 0L;
		aLong4558 = 0L;
		anInt4561 = 0;
		aLongArray4560 = new long[10];
	}
}
