package net.zaros.client;
/* Class256 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.PixelGrabber;

final class Class256 {
	private boolean aBoolean2403 = false;
	private Sprite[] aClass397Array2404;
	private static int anInt2405 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| \u00c4\u00cb\u00cf\u00d6\u00dc\u00e4\u00eb\u00ef\u00f6\u00fc\u00ff\u00df\u00c1\u00c0\u00c9\u00c8\u00cd\u00cc\u00d3\u00d2\u00da\u00d9\u00e1\u00e0\u00e9\u00e8\u00ed\u00ec\u00f3\u00f2\u00fa\u00f9\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c6\u00e6\u00e3\u00c3\u00f5\u00d5\u00e7\u00c7".length();
	private int anInt2406;
	private static int[] anIntArray2407 = new int[256];
	private int[] anIntArray2408 = new int[4];
	private int anInt2409;
	private int[] anIntArray2410;

	private final void method2228(ha var_ha, String string, int[] is, int i, int i_0_, int i_1_, boolean bool) {
		if (i_1_ == 0)
			bool = false;
		i_1_ |= ~0xffffff;
		for (int i_2_ = 0; i_2_ < string.length(); i_2_++) {
			int i_3_ = anIntArray2407[string.charAt(i_2_)];
			if (bool)
				aClass397Array2404[i_3_].method4079(i + 1, i_0_ + 1, 0, -16777216, 1);
			aClass397Array2404[i_3_].method4079(i, i_0_, 0, i_1_, 1);
			i += anIntArray2410[i_3_];
		}
	}

	public static void method2229() {
		anIntArray2407 = null;
	}

	final int method2230() {
		return anInt2406 - 1;
	}

	private final void method2231(ha var_ha, Font font, FontMetrics fontmetrics, char c, int i, boolean bool) {
		int i_4_ = fontmetrics.charWidth(c);
		int i_5_ = i_4_;
		if (bool) {
			try {
				if (c == '/')
					bool = false;
				if (c == 'f' || c == 't' || c == 'w' || c == 'v' || c == 'k' || c == 'x' || c == 'y' || c == 'A' || c == 'V' || c == 'W')
					i_4_++;
			} catch (Exception exception) {
				/* empty */
			}
		}
		int i_6_ = fontmetrics.getMaxAscent();
		int i_7_ = fontmetrics.getMaxAscent() + fontmetrics.getMaxDescent();
		int i_8_ = fontmetrics.getHeight();
		Image image = Class230.aCanvas2209.createImage(i_4_, i_7_);
		Graphics graphics = image.getGraphics();
		graphics.setColor(Color.black);
		graphics.fillRect(0, 0, i_4_, i_7_);
		graphics.setColor(Color.white);
		graphics.setFont(font);
		graphics.drawString(String.valueOf(c), 0, i_6_);
		if (bool)
			graphics.drawString(String.valueOf(c), 1, i_6_);
		int[] is = new int[i_4_ * i_7_];
		PixelGrabber pixelgrabber = new PixelGrabber(image, 0, 0, i_4_, i_7_, is, 0, i_4_);
		try {
			pixelgrabber.grabPixels();
		} catch (Exception exception) {
			/* empty */
		}
		image.flush();
		Object object = null;
		int i_9_ = 0;
		while_146_ : for (int i_10_ = 0; i_10_ < i_7_; i_10_++) {
			for (int i_11_ = 0; i_11_ < i_4_; i_11_++) {
				int i_12_ = is[i_11_ + i_10_ * i_4_];
				if ((i_12_ & 0xffffff) != 0) {
					i_9_ = i_10_;
					break while_146_;
				}
			}
		}
		for (int i_13_ = 0; i_13_ < is.length; i_13_++) {
			if ((is[i_13_] & 0xffffff) == 0)
				is[i_13_] = 0;
		}
		anInt2409 = i_6_ - i_9_;
		anInt2406 = i_8_;
		anIntArray2410[i] = i_5_;
		aClass397Array2404[i] = var_ha.method1096(113, 0, i_7_, i_4_, i_4_, is);
	}

	final int method2232() {
		return anInt2409;
	}

	final void method2233(ha var_ha, String string, int i, int i_14_, int i_15_, boolean bool) {
		int i_16_ = method2234(string) / 2;
		var_ha.K(anIntArray2408);
		if (i - i_16_ <= anIntArray2408[2] && i + i_16_ >= anIntArray2408[0] && i_14_ - anInt2409 <= anIntArray2408[3] && i_14_ + anInt2406 >= anIntArray2408[1])
			method2228(var_ha, string, anIntArray2408, i - i_16_, i_14_, i_15_, bool);
	}

	final int method2234(String string) {
		int i = 0;
		for (int i_17_ = 0; i_17_ < string.length(); i_17_++) {
			int i_18_ = anIntArray2407[string.charAt(i_17_)];
			i += anIntArray2410[i_18_];
		}
		return i;
	}

	Class256(ha var_ha, int i, boolean bool, Component component) {
		aBoolean2403 = false;
		aClass397Array2404 = new Sprite[256];
		anIntArray2410 = new int[256];
		Font font = new Font("Helvetica", bool ? 1 : 0, i);
		FontMetrics fontmetrics = component.getFontMetrics(font);
		for (int i_19_ = 0; i_19_ < anInt2405; i_19_++)
			method2231(var_ha, font, fontmetrics, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| \u00c4\u00cb\u00cf\u00d6\u00dc\u00e4\u00eb\u00ef\u00f6\u00fc\u00ff\u00df\u00c1\u00c0\u00c9\u00c8\u00cd\u00cc\u00d3\u00d2\u00da\u00d9\u00e1\u00e0\u00e9\u00e8\u00ed\u00ec\u00f3\u00f2\u00fa\u00f9\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c6\u00e6\u00e3\u00c3\u00f5\u00d5\u00e7\u00c7".charAt(i_19_), i_19_, false);
		if (bool && aBoolean2403) {
			aBoolean2403 = false;
			font = new Font("Helvetica", 0, i);
			fontmetrics = component.getFontMetrics(font);
			for (int i_20_ = 0; i_20_ < anInt2405; i_20_++)
				method2231(var_ha, font, fontmetrics, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| \u00c4\u00cb\u00cf\u00d6\u00dc\u00e4\u00eb\u00ef\u00f6\u00fc\u00ff\u00df\u00c1\u00c0\u00c9\u00c8\u00cd\u00cc\u00d3\u00d2\u00da\u00d9\u00e1\u00e0\u00e9\u00e8\u00ed\u00ec\u00f3\u00f2\u00fa\u00f9\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c6\u00e6\u00e3\u00c3\u00f5\u00d5\u00e7\u00c7".charAt(i_20_), i_20_, false);
			if (!aBoolean2403) {
				aBoolean2403 = false;
				for (int i_21_ = 0; i_21_ < anInt2405; i_21_++)
					method2231(var_ha, font, fontmetrics, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| \u00c4\u00cb\u00cf\u00d6\u00dc\u00e4\u00eb\u00ef\u00f6\u00fc\u00ff\u00df\u00c1\u00c0\u00c9\u00c8\u00cd\u00cc\u00d3\u00d2\u00da\u00d9\u00e1\u00e0\u00e9\u00e8\u00ed\u00ec\u00f3\u00f2\u00fa\u00f9\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c6\u00e6\u00e3\u00c3\u00f5\u00d5\u00e7\u00c7".charAt(i_21_), i_21_, true);
			}
		}
	}

	static {
		for (int i = 0; i < 256; i++) {
			int i_22_ = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\u00a3$%^&*()-_=+[{]};:'@#~,<.>/?\\| \u00c4\u00cb\u00cf\u00d6\u00dc\u00e4\u00eb\u00ef\u00f6\u00fc\u00ff\u00df\u00c1\u00c0\u00c9\u00c8\u00cd\u00cc\u00d3\u00d2\u00da\u00d9\u00e1\u00e0\u00e9\u00e8\u00ed\u00ec\u00f3\u00f2\u00fa\u00f9\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c6\u00e6\u00e3\u00c3\u00f5\u00d5\u00e7\u00c7".indexOf(i);
			if (i_22_ == -1)
				i_22_ = 74;
			anIntArray2407[i] = i_22_;
		}
	}
}
