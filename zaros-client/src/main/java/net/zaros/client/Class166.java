package net.zaros.client;

import net.zaros.client.logic.clans.channel.ClanChannel;

/* Class166 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class166 {
	static byte[][] aByteArrayArray1691;
	static ClanChannel aClass296_Sub54_1692;

	abstract boolean method1637(int i, int i_0_);

	abstract Interface1 method1639(int i);

	public Class166() {
		/* empty */
	}

	abstract void method1640(boolean bool);

	public static void method1641(byte i) {
		if (i < 56)
			method1643(51, 35, 3, null);
		aClass296_Sub54_1692 = null;
		aByteArrayArray1691 = null;
	}

	static final void method1642(int i) {
		if (Class41_Sub1.anObject3738 == null) {
			Class123_Sub1_Sub1 class123_sub1_sub1 = new Class123_Sub1_Sub1();
			byte[] is = class123_sub1_sub1.method1061(16, 128, 128, (byte) 17);
			Class41_Sub1.anObject3738 = wrapBuffer(false, i - 4, is);
		}
		if (Class363.anObject3109 == null) {
			Class123_Sub2_Sub1 class123_sub2_sub1 = new Class123_Sub2_Sub1();
			byte[] is = class123_sub2_sub1.method1073(i ^ ~0x61db, 128, 16, 128);
			Class363.anObject3109 = wrapBuffer(false, 102, is);
		}
		if (i != 128)
			method1642(89);
	}

	static final void method1643(int i, int i_3_, int i_4_, Class338_Sub3_Sub5 class338_sub3_sub5) {
		Class247 class247 = Node.method2428(i, i_3_, i_4_);
		if (class247 != null) {
			class247.aClass338_Sub3_Sub5_2346 = class338_sub3_sub5;
			int i_5_ = Class360_Sub2.aSArray5304 == Class52.aSArray636 ? 1 : 0;
			if (class338_sub3_sub5.method3459(0)) {
				if (class338_sub3_sub5.method3469(111)) {
					class338_sub3_sub5.aClass338_Sub3_5202 = Class368_Sub16.aClass338_Sub3Array5525[i_5_];
					Class368_Sub16.aClass338_Sub3Array5525[i_5_] = class338_sub3_sub5;
				} else {
					class338_sub3_sub5.aClass338_Sub3_5202 = Class368_Sub10.aClass338_Sub3Array5481[i_5_];
					Class368_Sub10.aClass338_Sub3Array5481[i_5_] = class338_sub3_sub5;
					Class41.aBoolean388 = true;
				}
			} else {
				class338_sub3_sub5.aClass338_Sub3_5202 = Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_5_];
				Class338_Sub3_Sub3.aClass338_Sub3Array6568[i_5_] = class338_sub3_sub5;
			}
		}
	}

	static final Object wrapBuffer(boolean bool, int i, byte[] is) {
		if (is == null)
			return null;
		if (is.length > 136 && !Class206.aBoolean2071) {
			try {
				Class104 class104 = (Class104) Class104_Sub1.class.newInstance();
				class104.method903(is, (byte) -104);
				return class104;
			} catch (Throwable throwable) {
				Class206.aBoolean2071 = true;
			}
		}
		int i_6_ = -60 % ((i - 8) / 51);
		if (bool)
			return Class296_Sub27.method2675(is, true);
		return is;
	}

	abstract void method1645(byte i);
}
