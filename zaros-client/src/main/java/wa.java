import net.zaros.client.Interface13;

public class wa implements Interface13 {
	public xa aXa3651;
	public j aJ3652;
	private long aLong3653 = 0L;

	public final void method53(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, boolean bool, boolean bool_5_) {
		X(aLong3653, aJ3652.nativeid, aXa3651.nativeid, i, i_0_, i_1_, i_2_, i_3_, i_4_, bool, bool_5_);
	}

	private final native void X(long l, long l_6_, long l_7_, int i, int i_8_, int i_9_, int i_10_, int i_11_,
			int i_12_, boolean bool, boolean bool_13_);

	private final native void Z(long l, long l_14_, long l_15_, int i, int i_16_, int i_17_, int i_18_, int i_19_,
			int i_20_, boolean bool, boolean bool_21_);

	public wa(oa var_oa, j var_j, xa var_xa) {
		aLong3653 = var_oa.nativeid;
		aJ3652 = var_j;
		aXa3651 = var_xa;
	}

	public final void method52(int i, int i_22_, int i_23_, int i_24_, int i_25_, int i_26_, boolean bool,
			boolean bool_27_) {
		Z(aLong3653, aJ3652.nativeid, aXa3651.nativeid, i, i_22_, i_23_, i_24_, i_25_, i_26_, bool, bool_27_);
	}
}
