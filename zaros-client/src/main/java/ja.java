
import net.zaros.client.Class373;
import net.zaros.client.Class376;
import net.zaros.client.Interface3;

public class ja extends Class373 implements Interface3 {
	long nativeid;

	private native void m(long l, int i);

	private native void J(long l, int i);

	private native void za(long l, int i);

	public void method3901(int i, int i_0_, int i_1_, int[] is) {
		XA(nativeid, i, i_0_, i_1_, is);
	}

	private native void w(long l, int[] is);

	private native void P(long l, int i, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_);

	private native void a(long l, int i, int i_7_, int i_8_);

	private native void FA(long l, int i, int i_9_, int i_10_);

	public void method3908(int[] is) {
		w(nativeid, is);
	}

	public void method3907(int i, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_) {
		P(nativeid, i, i_11_, i_12_, i_13_, i_14_, i_15_);
	}

	private native void va(long l, int i, int i_16_, int i_17_, int[] is);

	private native void NA(long l, int i);

	public void method3910() {
		u(nativeid);
	}

	public void method3903(int i, int i_18_, int i_19_, int[] is) {
		b(nativeid, i, i_18_, i_19_, is);
	}

	private native void XA(long l, int i, int i_20_, int i_21_, int[] is);

	public void method3917(int i) {
		m(nativeid, i);
	}

	private native void t(long l, int i);

	public void method3906(int i) {
		t(nativeid, i);
	}

	public ja() {
		la();
	}

	private native void la();

	public void w(boolean bool) {
		AA(nativeid, bool);
	}

	private native void b(long l, int i, int i_22_, int i_23_, int[] is);

	protected void ize() {
		if (nativeid != 0L)
			Class376.method3955((byte) 123, this);
	}

	public void method3911(int i) {
		NA(nativeid, i);
	}

	public void method3904(int i, int i_24_, int i_25_) {
		a(nativeid, i, i_24_, i_25_);
	}

	private native void AA(long l, boolean bool);

	public void method3914(int i) {
		J(nativeid, i);
	}

	private native void l(long l, long l_26_);

	public void method3902(int i, int i_27_, int i_28_) {
		FA(nativeid, i, i_27_, i_28_);
	}

	private native void u(long l);

	public void method3909(int i) {
		VA(nativeid, i);
	}

	private native void VA(long l, int i);

	public void method3915(Class373 class373) {
		l(nativeid, ((ja) class373).nativeid);
	}

	public Class373 method3916() {
		ja var_ja_29_ = new ja();
		var_ja_29_.method3915(this);
		return var_ja_29_;
	}

	public void method3900(int i) {
		za(nativeid, i);
	}

	public void method3905(int i, int i_30_, int i_31_, int[] is) {
		va(nativeid, i, i_30_, i_31_, is);
	}
}
