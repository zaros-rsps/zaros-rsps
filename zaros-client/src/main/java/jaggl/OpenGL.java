/* OpenGL - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaggl;
import java.awt.Canvas;
import java.util.Hashtable;

public class OpenGL {
	long peer;
	private Hashtable b;
	private static Hashtable a = new Hashtable();
	private Thread c;

	public static final native String glGetString(int i);

	public static final native void glTexImage2Df(int i, int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_, int i_6_, float[] fs, int i_7_);

	public static final native void glGenerateMipmapEXT(int i);

	public static final native void glOrtho(double d, double d_8_, double d_9_, double d_10_, double d_11_, double d_12_);

	public static final native void glCullFace(int i);

	public static final native void glFinish();

	public static final native void glVertex2i(int i, int i_13_);

	public static final native void glDrawPixelsi(int i, int i_14_, int i_15_, int i_16_, int[] is, int i_17_);

	public static final native void glPixelTransferf(int i, float f);

	public static final native void glDrawElements(int i, int i_18_, int i_19_, long l);

	public static final native void glLightfv(int i, int i_20_, float[] fs, int i_21_);

	public static final native long glCreateShaderObjectARB(int i);

	public static final native void glNormal3f(float f, float f_22_, float f_23_);

	public static final native void glGetFloatv(int i, float[] fs, int i_24_);

	public static final native void glProgramLocalParameter4fARB(int i, int i_25_, float f, float f_26_, float f_27_, float f_28_);

	public static final native void glMatrixMode(int i);

	public static final native void glClear(int i);

	public static final native void glGetProgramivARB(int i, int i_29_, int[] is, int i_30_);

	public static final native int glGetError();

	public static final native void glBufferDataARBa(int i, int i_31_, long l, int i_32_);

	public static final native void glProgramLocalParameter4fvARB(int i, int i_33_, float[] fs, int i_34_);

	public static final native void glTexGenfv(int i, int i_35_, float[] fs, int i_36_);

	public static final native void glPixelStorei(int i, int i_37_);

	public static final native void glBufferSubDataARBub(int i, int i_38_, int i_39_, byte[] is, int i_40_);

	public static final native void glEnableClientState(int i);

	static final native boolean glUnmapBufferARB(int i);

	public static final native void glTranslatef(float f, float f_41_, float f_42_);

	public static final native void glDeleteRenderbuffersEXT(int i, int[] is, int i_43_);

	public static final native void glEndList();

	public static final native void glBindTexture(int i, int i_44_);

	public final synchronized boolean a() {
		if (c != Thread.currentThread())
			return false;
		detachPeer();
		a.remove(c);
		c = null;
		return true;
	}

	public static final native void glTexSubImage2Dub(int i, int i_45_, int i_46_, int i_47_, int i_48_, int i_49_, int i_50_, int i_51_, byte[] is, int i_52_);

	private final native boolean attachPeer();

	public static final native void glUniformMatrix4fvARB(int i, int i_53_, boolean bool, float[] fs, int i_54_);

	public static final native void glScissor(int i, int i_55_, int i_56_, int i_57_);

	public static final native void glGenBuffersARB(int i, int[] is, int i_58_);

	public static final native void glUniform3fARB(int i, float f, float f_59_, float f_60_);

	public static final native void glDrawBuffersARB(int i, int[] is, int i_61_);

	public static final native void glAlphaFunc(int i, float f);

	public static final native void glTexImage3Dub(int i, int i_62_, int i_63_, int i_64_, int i_65_, int i_66_, int i_67_, int i_68_, int i_69_, byte[] is, int i_70_);

	public static final native void glDepthFunc(int i);

	public static final native void glPointSize(float f);

	public final native boolean arePbuffersAvailable();

	public static final native void glColor3f(float f, float f_71_, float f_72_);

	public static final native void glLineWidth(float f);

	public static final native void glNewList(int i, int i_73_);

	public static final native void glFogf(int i, float f);

	public static final native void glTexCoord2f(float f, float f_74_);

	public static final native void glTexImage2Dub(int i, int i_75_, int i_76_, int i_77_, int i_78_, int i_79_, int i_80_, int i_81_, byte[] is, int i_82_);

	public static final native void glTexParameteri(int i, int i_83_, int i_84_);

	public static final native void glPopAttrib();

	public final native void surfaceResized(long l);

	public static final native void glEnd();

	public static final native void glTexCoord3f(float f, float f_85_, float f_86_);

	public static final native void glGetTexImageub(int i, int i_87_, int i_88_, int i_89_, byte[] is, int i_90_);

	public static final native void glTexEnvfv(int i, int i_91_, float[] fs, int i_92_);

	public static final native void glStencilFunc(int i, int i_93_, int i_94_);

	public static final native void glColor4f(float f, float f_95_, float f_96_, float f_97_);

	public static final native void glVertexPointer(int i, int i_98_, int i_99_, long l);

	private final native void detachPeer();

	public static final native void glHint(int i, int i_100_);

	public static final native void glDetachObjectARB(long l, long l_101_);

	public static final native void glRasterPos2i(int i, int i_102_);

	public static final native void glUniform1fARB(int i, float f);

	public static final native void glMaterialfv(int i, int i_103_, float[] fs, int i_104_);

	public final native void release();

	public static final native void glTexParameterf(int i, int i_105_, float f);

	public static final native void glPushAttrib(int i);

	public static final native void glBindBufferARB(int i, int i_106_);

	public final native void releasePbuffer(long l);

	public static final native void glGenTextures(int i, int[] is, int i_107_);

	public static final native void glFramebufferRenderbufferEXT(int i, int i_108_, int i_109_, int i_110_);

	public static final native void glLoadMatrixf(float[] fs, int i);

	public static final native void glUniform4fARB(int i, float f, float f_111_, float f_112_, float f_113_);

	public static final native void glPixelZoom(float f, float f_114_);

	public static final native void glTexSubImage2Di(int i, int i_115_, int i_116_, int i_117_, int i_118_, int i_119_, int i_120_, int i_121_, int[] is, int i_122_);

	public static final native void glEnable(int i);

	public static final native void glMultiTexCoord3i(int i, int i_123_, int i_124_, int i_125_);

	public static final native void glClearColor(float f, float f_126_, float f_127_, float f_128_);

	static final native long glMapBufferARB(int i, int i_129_);

	public final native boolean setSurface(long l);

	public static final native void glFlush();

	public static final native void glScalef(float f, float f_130_, float f_131_);

	public static final native void glBlitFramebufferEXT(int i, int i_132_, int i_133_, int i_134_, int i_135_, int i_136_, int i_137_, int i_138_, int i_139_, int i_140_);

	public static final native void glReadPixelsi(int i, int i_141_, int i_142_, int i_143_, int i_144_, int i_145_, int[] is, int i_146_);

	public static final native void glGenRenderbuffersEXT(int i, int[] is, int i_147_);

	public static final native void glBindFramebufferEXT(int i, int i_148_);

	public static final native void glTexImage1Dub(int i, int i_149_, int i_150_, int i_151_, int i_152_, int i_153_, int i_154_, byte[] is, int i_155_);

	public static final native void glUseProgramObjectARB(long l);

	public final native void setSwapInterval(int i);

	public static final native void glClientActiveTexture(int i);

	public static final native void glLinkProgramARB(long l);

	public static final native void glTexImage2Di(int i, int i_156_, int i_157_, int i_158_, int i_159_, int i_160_, int i_161_, int i_162_, int[] is, int i_163_);

	public static final native void glStencilOp(int i, int i_164_, int i_165_);

	public static final native long glCreateProgramObjectARB();

	public final boolean a(String string) {
		if (b == null) {
			b = new Hashtable();
			String string_166_ = glGetString(7939);
			int i = 0;
			for (;;) {
				int i_167_ = string_166_.indexOf(' ', i);
				if (i_167_ == -1)
					break;
				String string_168_ = string_166_.substring(i, i_167_).trim();
				if (string_168_.length() != 0)
					b.put(string_168_, string_168_);
				i = i_167_ + 1;
			}
			String string_169_ = string_166_.substring(i).trim();
			if (string_169_.length() != 0)
				b.put(string_169_, string_169_);
		}
		return b.containsKey(string);
	}

	public static final native void glViewport(int i, int i_170_, int i_171_, int i_172_);

	public static final native void glColorPointer(int i, int i_173_, int i_174_, long l);

	public static final native void glRotatef(float f, float f_175_, float f_176_, float f_177_);

	public static final native void glFramebufferTexture2DEXT(int i, int i_178_, int i_179_, int i_180_, int i_181_);

	public static final native void glPopMatrix();

	public static final native void glBindProgramARB(int i, int i_182_);

	public static final native int glGetUniformLocationARB(long l, String string);

	public static final native void glShaderSourceARB(long l, String string);

	public static final native void glFogfv(int i, float[] fs, int i_183_);

	public static final native void glRenderbufferStorageEXT(int i, int i_184_, int i_185_, int i_186_);

	public static final native void glVertex3f(float f, float f_187_, float f_188_);

	public static final native void glDeleteObjectARB(long l);

	public static final native void glDrawBuffer(int i);

	public static final native void glMultMatrixf(float[] fs, int i);

	public static final native void glColor4ub(byte i, byte i_189_, byte i_190_, byte i_191_);

	public static final native void glDisableClientState(int i);

	public static final native void glFogi(int i, int i_192_);

	public static final native void glMultiTexCoord2f(int i, float f, float f_193_);

	public static final native void glClearDepth(float f);

	public static final native void glDepthMask(boolean bool);

	public static final native void glAttachObjectARB(long l, long l_194_);

	public static final native void glUniformMatrix2fvARB(int i, int i_195_, boolean bool, float[] fs, int i_196_);

	public static final native void glDeleteFramebuffersEXT(int i, int[] is, int i_197_);

	public static final native void glActiveTexture(int i);

	public static final native void glTexCoord2i(int i, int i_198_);

	public static final native void glGetTexImagei(int i, int i_199_, int i_200_, int i_201_, int[] is, int i_202_);

	public static final native void glTexEnvi(int i, int i_203_, int i_204_);

	public static final native void glTexCoordPointer(int i, int i_205_, int i_206_, long l);

	public static final native void glColorMask(boolean bool, boolean bool_207_, boolean bool_208_, boolean bool_209_);

	public static final native void glTexSubImage2Df(int i, int i_210_, int i_211_, int i_212_, int i_213_, int i_214_, int i_215_, int i_216_, float[] fs, int i_217_);

	public static final native void glDisable(int i);

	public static final native void glLoadIdentity();

	public static final native void glCopyTexSubImage2D(int i, int i_218_, int i_219_, int i_220_, int i_221_, int i_222_, int i_223_, int i_224_);

	public final native long createPbuffer(int i, int i_225_);

	public static final native void glReadPixelsub(int i, int i_226_, int i_227_, int i_228_, int i_229_, int i_230_, byte[] is, int i_231_);

	public static final native void glBindRenderbufferEXT(int i, int i_232_);

	public static final native void glColor3ub(byte i, byte i_233_, byte i_234_);

	public static final native int glGenLists(int i);

	public final native long init(Canvas canvas, int i, int i_235_, int i_236_, int i_237_, int i_238_, int i_239_);

	public static final native void glGetInfoLogARB(long l, int i, int[] is, int i_240_, byte[] is_241_, int i_242_);

	public static final native void glPushMatrix();

	public static final native void glShaderSourceRawARB(long l, byte[] is);

	public final synchronized boolean b() {
		Thread thread = Thread.currentThread();
		if (attachPeer()) {
			OpenGL opengl_243_ = (OpenGL) a.put(thread, this);
			if (opengl_243_ != null)
				opengl_243_.c = null;
			c = thread;
			return true;
		}
		return false;
	}

	public static final native int glGenProgramARB();

	public static final native void glGetObjectParameterivARB(long l, int i, int[] is, int i_244_);

	public final native void swapBuffers();

	public static final native void glRenderbufferStorageMultisampleEXT(int i, int i_245_, int i_246_, int i_247_, int i_248_);

	public static final native void glUniform2fARB(int i, float f, float f_249_);

	public static final native void glNormalPointer(int i, int i_250_, long l);

	public static final native void glUniform1iARB(int i, int i_251_);

	public static final native void glBufferSubDataARBa(int i, int i_252_, int i_253_, long l);

	public static final native void glCopyPixels(int i, int i_254_, int i_255_, int i_256_, int i_257_);

	public static final native void glProgramStringARB(int i, int i_258_, String string);

	public static final native void glCompileShaderARB(long l);

	public final native long prepareSurface(Canvas canvas);

	public static final native void glLightModelfv(int i, float[] fs, int i_259_);

	public static final native void glCallList(int i);

	public static final native void glGenFramebuffersEXT(int i, int[] is, int i_260_);

	public static final native void glTexCoord3i(int i, int i_261_, int i_262_);

	public static final native void glBlendFunc(int i, int i_263_);

	public static final native void glDrawPixelsub(int i, int i_264_, int i_265_, int i_266_, byte[] is, int i_267_);

	public static final native void glGetIntegerv(int i, int[] is, int i_268_);

	public static final native void glReadBuffer(int i);

	public static final native void glDeleteTextures(int i, int[] is, int i_269_);

	public static final native void glBufferDataARBub(int i, int i_270_, byte[] is, int i_271_, int i_272_);

	public static final native void glProgramRawARB(int i, int i_273_, byte[] is);

	public static final native void glCopyTexImage2D(int i, int i_274_, int i_275_, int i_276_, int i_277_, int i_278_, int i_279_, int i_280_);

	public static final native void glShadeModel(int i);

	public static final native void glDeleteLists(int i, int i_281_);

	public static final native void glDrawArrays(int i, int i_282_, int i_283_);

	public static final native void glColorMaterial(int i, int i_284_);

	public static final native void glUniformMatrix3fvARB(int i, int i_285_, boolean bool, float[] fs, int i_286_);

	public static final native void glLightf(int i, int i_287_, float f);

	public static final native int glCheckFramebufferStatusEXT(int i);

	public static final native void glMultiTexCoord2i(int i, int i_288_, int i_289_);

	public static final native void glPolygonMode(int i, int i_290_);

	public final native void releaseSurface(Canvas canvas, long l);

	public static final native void glDeleteBuffersARB(int i, int[] is, int i_291_);

	public static final native void glTexEnvf(int i, int i_292_, float f);

	public static final native void glTexGeni(int i, int i_293_, int i_294_);

	public final native void setPbuffer(long l);

	public static final native void glBegin(int i);

	public static final native void glFramebufferTexture3DEXT(int i, int i_295_, int i_296_, int i_297_, int i_298_, int i_299_);

	public static final native void glCopyTexSubImage3D(int i, int i_300_, int i_301_, int i_302_, int i_303_, int i_304_, int i_305_, int i_306_, int i_307_);

	public static final native void glFrustum(double d, double d_308_, double d_309_, double d_310_, double d_311_, double d_312_);

	public static final native void glDeleteProgramARB(int i);

	public static final native void glVertex2f(float f, float f_313_);
}
