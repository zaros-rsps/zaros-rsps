import net.zaros.client.Model;
import net.zaros.client.Class373;
import net.zaros.client.Class376;
import net.zaros.client.Interface3;
import net.zaros.client.ha;
import net.zaros.client.s;

public class a implements Interface3 {
	public long nativeid;
	private i[] anIArray3447;
	private i[] anIArray3448 = new i[7];
	private oa anOa3449;
	public Runnable aRunnable3450;

	protected final void finalize() {
		if (nativeid != 0L)
			Class376.method3955((byte) 123, this);
	}

	public void method138(Model class178, Class373 class373, int[] is, int i) {
		UA(nativeid, ((i) class178).nativeid, ((ja) class373).nativeid, is, i);
	}

	public void method139(ha var_ha, int[] is, int[] is_0_, int[] is_1_, short[] is_2_, int i) {
		O(nativeid, var_ha, is, is_0_, is_1_, is_2_, i);
	}

	public void method140(s var_s, int i, int i_3_, int i_4_) {
		Z(nativeid, ((t) var_s).nativeid, i, i_3_, i_4_);
	}

	private final void method141() {
		W(nativeid);
	}

	private final native void f(long l, long l_5_, long l_6_, int[] is, int i, int i_7_);

	public void method142() {
		aRunnable3450 = null;
		method141();
	}

	private final native void E(long l, boolean bool);

	private final native void Z(long l, long l_8_, int i, int i_9_, int i_10_);

	private final native void O(long l, ha var_ha, int[] is, int[] is_11_, int[] is_12_, short[] is_13_, int i);

	final boolean method143(Model class178, int i, int i_14_, Class373 class373, boolean bool) {
		return R(nativeid, ((i) class178).nativeid, i, i_14_, ((ja) class373).nativeid, bool);
	}

	private final native boolean n(long l, long l_15_, int i, int i_16_, long l_17_, boolean bool, int i_18_);

	public Model method144(i var_i, byte i, int i_19_, boolean bool) {
		boolean bool_20_ = false;
		i var_i_21_;
		i var_i_22_;
		if (i > 0 && i <= 7) {
			var_i_22_ = anIArray3448[i - 1];
			var_i_21_ = anIArray3447[i - 1];
			bool_20_ = true;
		} else
			var_i_21_ = var_i_22_ = new i(anOa3449);
		var_i.ZA(var_i_21_, var_i_22_, i_19_, bool_20_, bool);
		var_i_21_.aClass89Array3452 = var_i.aClass89Array3452;
		var_i_21_.aClass232Array3453 = var_i.aClass232Array3453;
		return var_i_21_;
	}

	private final native void r(long l, long l_23_, long l_24_, int i, int i_25_, int i_26_, boolean bool);

	public void method145(Model class178, Model class178_27_, int i, int i_28_, int i_29_, boolean bool) {
		r(nativeid, ((i) class178).nativeid, ((i) class178_27_).nativeid, i, i_28_, i_29_, bool);
	}

	private final native void na(long l, ha var_ha, int i, int i_30_, int i_31_, int i_32_, int i_33_, int i_34_,
			int i_35_);

	public void method146(s var_s, int i, int i_36_) {
		H(nativeid, ((t) var_s).nativeid, i, i_36_);
	}

	private final native void ta(long l, long l_37_, int i, int i_38_, int i_39_, int i_40_, int i_41_, int i_42_,
			int i_43_, boolean[][] bools);

	public void method147(ha var_ha, int i, int i_44_, int i_45_, int i_46_, int i_47_, int i_48_, int i_49_) {
		na(nativeid, var_ha, i, i_44_, i_45_, i_46_, i_47_, i_48_, i_49_);
	}

	private final native void e(long l, long l_50_, int[] is, long l_51_);

	private final void method148() {
		M(nativeid);
	}

	private final native boolean R(long l, long l_52_, int i, int i_53_, long l_54_, boolean bool);

	public boolean method149(Model class178, int i, int i_55_, Class373 class373, boolean bool, int i_56_) {
		return n(nativeid, ((i) class178).nativeid, i, i_55_, ((ja) class373).nativeid, bool, i_56_);
	}

	public void method150(Model class178, Class373 class373, int[] is, int i, int i_57_) {
		f(nativeid, ((i) class178).nativeid, ((ja) class373).nativeid, is, i, i_57_);
	}

	private final native void HA(long l, ha var_ha, int i, int i_58_);

	private final native void M(long l);

	public void method151(Model class178, int[] is, Class373 class373) {
		e(nativeid, ((i) class178).nativeid, is, ((ja) class373).nativeid);
	}

	public void method152(s var_s, int i, int i_59_, int i_60_, int i_61_, int i_62_, int i_63_, int i_64_,
			boolean[][] bools) {
		ta(nativeid, ((t) var_s).nativeid, i, i_59_, i_60_, i_61_, i_62_, i_63_, i_64_, bools);
	}

	public final void w(boolean bool) {
		E(nativeid, bool);
	}

	private final native void W(long l);

	private final native void UA(long l, long l_65_, long l_66_, int[] is, int i);

	private final native void H(long l, long l_67_, int i, int i_68_);

	public void method153() {
		aRunnable3450 = Thread.currentThread();
		method148();
	}

	public a(oa var_oa, int i, int i_69_) {
		anIArray3447 = new i[7];
		anOa3449 = var_oa;
		for (int i_70_ = 0; i_70_ < 7; i_70_++) {
			anIArray3447[i_70_] = new i(anOa3449);
			anIArray3448[i_70_] = new i(anOa3449);
		}
		HA(nativeid, var_oa, i, i_69_);
	}
}
