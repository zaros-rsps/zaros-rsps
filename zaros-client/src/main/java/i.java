import net.zaros.client.Billboard;
import net.zaros.client.BillboardRaw;
import net.zaros.client.Model;
import net.zaros.client.Class296_Sub51_Sub4;
import net.zaros.client.Class338_Sub5;
import net.zaros.client.Class373;
import net.zaros.client.Class376;
import net.zaros.client.EffectiveVertex;
import net.zaros.client.EmissiveTriangle;
import net.zaros.client.Interface3;
import net.zaros.client.Mesh;
import net.zaros.client.r;
import net.zaros.client.s;

public class i extends Model implements Interface3 {
	private oa anOa3451;
	public EmissiveTriangle[] aClass89Array3452;
	long nativeid;
	public EffectiveVertex[] aClass232Array3453;
	private ya aYa3454;

	public native void v();

	public native void s(int i);

	public native void O(int i, int i_0_, int i_1_);

	public native void aa(short i, short i_2_);

	public native void k(int i);

	public native int fa();

	public native void P(int i, int i_3_, int i_4_, int i_5_);

	public byte[] method1733() {
		throw new RuntimeException();
	}

	public native void VA(int i);

	public void method1720() {
		if (anOa3451.anInt3476 > 1) {
			synchronized (this) {
				while (aBoolean1849) {
					try {
						this.wait();
					} catch (InterruptedException interruptedexception) {
						/* empty */
					}
				}
				aBoolean1849 = true;
			}
		}
	}

	public native int da();

	public native void ZA(i var_i_6_, i var_i_7_, int i, boolean bool, boolean bool_8_);

	public native boolean r();

	public native void H(int i, int i_9_, int i_10_);

	private native void oa(oa var_oa);

	public native void w(boolean bool);

	public native boolean NA();

	public native int ua();

	public native int RA();

	public void method1717(int i, int i_11_, int i_12_, int i_13_) {
		/* empty */
	}

	public EmissiveTriangle[] method1735() {
		return aClass89Array3452;
	}

	public void method1715(Class373 class373, Class338_Sub5 class338_sub5, int i) {
		if (class338_sub5 == null)
			anOa3451.C().method138(this, class373, null, i);
		else {
			oa.anIntArray3459[5] = 0;
			anOa3451.C().method138(this, class373, oa.anIntArray3459, i);
			class338_sub5.anInt5225 = oa.anIntArray3459[0];
			class338_sub5.anInt5223 = oa.anIntArray3459[1];
			class338_sub5.anInt5222 = oa.anIntArray3459[2];
			class338_sub5.anInt5221 = oa.anIntArray3459[3];
			class338_sub5.anInt5226 = oa.anIntArray3459[4];
			class338_sub5.aBoolean5224 = oa.anIntArray3459[5] != 0;
		}
	}

	public boolean method1737() {
		return true;
	}

	public native int HA();

	public void method1724(byte i, byte[] is) {
		throw new RuntimeException();
	}

	public void method1736(Model class178, int i, int i_14_, int i_15_, boolean bool) {
		anOa3451.C().method145(this, class178, i, i_14_, i_15_, bool);
	}

	protected void finalize() {
		if (nativeid != 0L)
			Class376.method3955((byte) 123, this);
	}

	public native int WA();

	public native int EA();

	public native void LA(int i);

	public native int V();

	public void method1726(Class373 class373, int i, boolean bool) {
		TA(((ja) class373).nativeid, i, bool);
	}

	public native int na();

	public native int G();

	private native void l(long l, int i, int[] is, int i_16_, int i_17_, int i_18_, int i_19_, boolean bool);

	public native r ba(r var_r);

	public void method1722() {
		/* empty */
	}

	public EffectiveVertex[] method1729() {
		return aClass232Array3453;
	}

	private void method1744(int[] is, Class373 class373) {
		anOa3451.C().method151(this, is, class373);
	}

	public void method1730() {
		if (anOa3451.anInt3476 > 1) {
			synchronized (this) {
				aBoolean1849 = false;
				this.notifyAll();
			}
		}
	}

	public void method1719(Class373 class373, Class338_Sub5 class338_sub5, int i, int i_20_) {
		if (class338_sub5 == null)
			anOa3451.C().method150(this, class373, null, i, i_20_);
		else {
			oa.anIntArray3459[5] = 0;
			anOa3451.C().method150(this, class373, oa.anIntArray3459, i, i_20_);
			class338_sub5.anInt5225 = oa.anIntArray3459[0];
			class338_sub5.anInt5223 = oa.anIntArray3459[1];
			class338_sub5.anInt5222 = oa.anIntArray3459[2];
			class338_sub5.anInt5221 = oa.anIntArray3459[3];
			class338_sub5.anInt5226 = oa.anIntArray3459[4];
			class338_sub5.aBoolean5224 = oa.anIntArray3459[5] != 0;
		}
	}

	private native void R(oa var_oa, ya var_ya, int i, int i_21_, int[] is, int[] is_22_, int[] is_23_, int[] is_24_,
			short[] is_25_, int i_26_, short[] is_27_, short[] is_28_, short[] is_29_, byte[] is_30_, byte[] is_31_,
			byte[] is_32_, byte[] is_33_, short[] is_34_, short[] is_35_, int[] is_36_, byte i_37_, short[] is_38_,
			int i_39_, byte[] is_40_, short[] is_41_, short[] is_42_, short[] is_43_, int[] is_44_, int[] is_45_,
			int[] is_46_, byte[] is_47_, byte[] is_48_, int[] is_49_, int[] is_50_, int[] is_51_, int[] is_52_,
			int i_53_, int i_54_, int i_55_, int i_56_, int i_57_, int i_58_, int[] is_59_);

	public void method1718(Class373 class373) {
		method1744(oa.anIntArray3465, class373);
		int i = 0;
		if (aClass89Array3452 != null) {
			for (int i_60_ = 0; i_60_ < aClass89Array3452.length; i_60_++) {
				EmissiveTriangle class89 = aClass89Array3452[i_60_];
				class89.anInt951 = oa.anIntArray3465[i++];
				class89.anInt950 = oa.anIntArray3465[i++];
				class89.anInt957 = oa.anIntArray3465[i++];
				class89.anInt961 = oa.anIntArray3465[i++];
				class89.anInt963 = oa.anIntArray3465[i++];
				class89.anInt960 = oa.anIntArray3465[i++];
				class89.anInt965 = oa.anIntArray3465[i++];
				class89.anInt948 = oa.anIntArray3465[i++];
				class89.anInt949 = oa.anIntArray3465[i++];
			}
		}
		if (aClass232Array3453 != null) {
			for (int i_61_ = 0; i_61_ < aClass232Array3453.length; i_61_++) {
				EffectiveVertex class232 = aClass232Array3453[i_61_];
				EffectiveVertex class232_62_ = class232;
				if (class232.aClass232_2223 != null)
					class232_62_ = class232.aClass232_2223;
				if (class232.aClass373_2222 != null)
					class232.aClass373_2222.method3915(class373);
				else
					class232.aClass373_2222 = class373.method3916();
				class232_62_.anInt2219 = oa.anIntArray3465[i++];
				class232_62_.anInt2217 = oa.anIntArray3465[i++];
				class232_62_.anInt2214 = oa.anIntArray3465[i++];
			}
		}
	}

	public native boolean F();

	public boolean method1732(int i, int i_63_, Class373 class373, boolean bool, int i_64_) {
		return anOa3451.C().method143(this, i, i_63_, class373, bool);
	}

	public native void p(int i, int i_65_, s var_s, s var_s_66_, int i_67_, int i_68_, int i_69_);

	public native void FA(int i);

	public void method1725(int i, int[] is, int i_70_, int i_71_, int i_72_, int i_73_, boolean bool) {
		l(nativeid, i, is, i_70_, i_71_, i_72_, i_73_, bool);
	}

	public native int ma();

	private native void TA(long l, int i, boolean bool);

	public native void C(int i);

	public native void wa();

	public native void ia(short i, short i_74_);

	i(oa var_oa, ya var_ya, Mesh class132, int i, int i_75_, int i_76_, int i_77_) {
		anOa3451 = var_oa;
		aYa3454 = var_ya;
		aClass89Array3452 = class132.emitters;
		aClass232Array3453 = class132.effectors;
		int i_78_ = (class132.emitters == null ? 0 : class132.emitters.length);
		int i_79_ = (class132.effectors == null ? 0 : class132.effectors.length);
		int i_80_ = 0;
		int[] is = new int[i_78_ * 3 + i_79_];
		for (int i_81_ = 0; i_81_ < i_78_; i_81_++) {
			is[i_80_++] = aClass89Array3452[i_81_].anInt954;
			is[i_80_++] = aClass89Array3452[i_81_].anInt953;
			is[i_80_++] = aClass89Array3452[i_81_].anInt964;
		}
		for (int i_82_ = 0; i_82_ < i_79_; i_82_++)
			is[i_80_++] = aClass232Array3453[i_82_].anInt2220;
		int i_83_ = (class132.billboards == null ? 0 : class132.billboards.length);
		int[] is_84_ = new int[i_83_ * 8];
		int i_85_ = 0;
		for (int i_86_ = 0; i_86_ < i_83_; i_86_++) {
			Billboard class385 = class132.billboards[i_86_];
			BillboardRaw class101 = Class296_Sub51_Sub4.method3086((byte) 122, class385.id);
			is_84_[i_85_++] = class385.face;
			is_84_[i_85_++] = class101.anInt1072;
			is_84_[i_85_++] = class101.anInt1073;
			is_84_[i_85_++] = class101.texture_id;
			is_84_[i_85_++] = class101.anInt1079;
			is_84_[i_85_++] = class101.anInt1076;
			is_84_[i_85_++] = class101.aBoolean1077 ? -1 : 0;
		}
		for (int i_87_ = 0; i_87_ < i_83_; i_87_++) {
			Billboard class385 = class132.billboards[i_87_];
			is_84_[i_85_++] = class385.anInt3258;
		}
		R(anOa3451, aYa3454, class132.num_vertices, class132.highest_face_index, class132.vertices_x,
				class132.vertices_y, class132.vertices_z, class132.vertices_label, class132.aShortArray1366,
				class132.num_faces, class132.faces_a, class132.faces_b, class132.faceS_c, class132.aByteArray1354,
				class132.faces_priority, class132.faces_alpha, class132.faces_texture, class132.faces_colour,
				class132.faces_material, class132.faces_label, class132.aByte1372, class132.aShortArray1352,
				class132.num_textures, class132.textures_mapping_type, class132.textures_mapping_p,
				class132.textures_mapping_m, class132.textures_mapping_n, class132.anIntArray1344,
				class132.anIntArray1365, class132.anIntArray1379, class132.aByteArray1381, class132.aByteArray1367,
				class132.anIntArray1371, class132.anIntArray1347, class132.anIntArray1348, is, i_78_, i_79_, i, i_75_,
				i_76_, i_77_, is_84_);
	}

	public i(oa var_oa) {
		anOa3451 = var_oa;
		aYa3454 = null;
		oa(var_oa);
	}

	public Model method1728(byte i, int i_88_, boolean bool) {
		return anOa3451.C().method144(this, i, i_88_, bool);
	}

	public boolean method1731(int i, int i_89_, Class373 class373, boolean bool, int i_90_, int i_91_) {
		return anOa3451.C().method149(this, i, i_89_, class373, bool, i_91_);
	}

	public native void I(int i, int[] is, int i_92_, int i_93_, int i_94_, boolean bool, int i_95_, int[] is_96_);

	public native void a(int i);

}
