/* SimplePeer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.misc;

public abstract class SimplePeer {
	private long peer;

	public final boolean a() {
		if (peer != 0L)
			return false;
		return true;
	}

	protected abstract void clear();

	public final void b() {
		if (!a())
			clear();
	}

	private final void setPeer(long l) {
		peer = l;
	}

	private static final native void init();

	protected final void finalize() throws Throwable {
		if (!a())
			b();
		super.finalize();
	}

	static {
		init();
	}
}
