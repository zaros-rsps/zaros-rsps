/* Frame - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.theora;
import jagtheora.misc.SimplePeer;

public class Frame extends SimplePeer {
	public int b;
	public int a;
	public int[] pixels;

	protected final native void clear();

	private static final native void init();

	public Frame(int i, int i_0_) {
		b = i_0_;
		a = i;
		pixels = new int[b * a];
	}

	static {
		init();
	}
}
