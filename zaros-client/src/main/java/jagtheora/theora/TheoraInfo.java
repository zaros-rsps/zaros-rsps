/* TheoraInfo - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.theora;
import jagtheora.misc.SimplePeer;

public class TheoraInfo extends SimplePeer {
	public int picX;
	public int colourSpace;
	public int picWidth;
	public byte versionSubMinor;
	public int aspectDenominator;
	public int picHeight;
	public int fpsDenominator;
	public byte versionMinor;
	public int fpsNumerator;
	public int picY;
	public int frameHeight;
	public byte versionMajor;
	public int frameWidth;
	public int aspectNumerator;
	public int pixelFormat;

	private final native void init();

	protected final native void clear();

	private static final native void initFields();

	public TheoraInfo() {
		init();
		if (this.a())
			throw new IllegalStateException();
	}

	static {
		initFields();
	}
}
