/* VorbisInfo - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jagtheora.vorbis;
import jagtheora.misc.SimplePeer;
import jagtheora.ogg.OggPacket;

public class VorbisInfo extends SimplePeer {
	public int rate;
	public int channels;

	protected final native void clear();

	private final native void init();

	private static final native void initFields();

	public final native int headerIn(VorbisComment vorbiscomment, OggPacket oggpacket);

	public VorbisInfo() {
		init();
		if (this.a())
			throw new IllegalStateException();
	}

	static {
		initFields();
	}
}
