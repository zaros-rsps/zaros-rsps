import net.zaros.client.Class376;
import net.zaros.client.Sprite;
import net.zaros.client.Interface3;
import net.zaros.client.aa;

public class j extends Sprite implements Interface3 {
	long nativeid;

	private native void h(oa var_oa, int i, int i_0_, int i_1_, int i_2_, boolean bool);

	private native void UA(long l, float f, float f_3_, float f_4_, float f_5_, float f_6_, float f_7_, int i,
			long l_8_, int i_9_, int i_10_);

	public void method4097(int i, int i_11_, int i_12_, int i_13_) {
		A(nativeid, i, i_11_, i_12_, i_13_);
	}

	private native int JA(long l);

	private native void A(long l, int i, int i_14_, int i_15_, int i_16_);

	private native void YA(long l, int i, int i_17_, int i_18_, int i_19_, int i_20_, int i_21_);

	private native void R(long l, boolean bool);

	public void method4076(int[] is) {
		CA(nativeid, is);
	}

	public void method4091(float f, float f_22_, float f_23_, float f_24_, float f_25_, float f_26_, int i, aa var_aa,
			int i_27_, int i_28_) {
		UA(nativeid, f, f_22_, f_23_, f_24_, f_25_, f_26_, i, ((na) var_aa).nativeid, i_27_, i_28_);
	}

	private native void ua(oa var_oa, int[] is, int i, int i_29_, int i_30_, int i_31_, boolean bool);

	private native int M(long l);

	public void method4084(int i, int i_32_, int i_33_) {
		N(nativeid, i, i_32_, i_33_);
	}

	private native int wa(long l);

	public void w(boolean bool) {
		R(nativeid, bool);
	}

	private native void N(long l, int i, int i_34_, int i_35_);

	public int method4088() {
		return JA(nativeid);
	}

	public void method4098(int i, int i_36_, int i_37_, int i_38_, int i_39_, int i_40_, int i_41_) {
		P(nativeid, i, i_36_, i_37_, i_38_, i_39_, i_40_, i_41_);
	}

	public int method4099() {
		return wa(nativeid);
	}

	public void method4093(int i, int i_42_, aa var_aa, int i_43_, int i_44_) {
		V(nativeid, i, i_42_, ((na) var_aa).nativeid, i_43_, i_44_);
	}

	private native void EA(oa var_oa, int i, int i_45_);

	public void method4075(float f, float f_46_, float f_47_, float f_48_, float f_49_, float f_50_, int i, int i_51_,
			int i_52_, int i_53_) {
		b(nativeid, f, f_46_, f_47_, f_48_, f_49_, f_50_, i, i_51_, i_52_, i_53_);
	}

	private native void V(long l, int i, int i_54_, long l_55_, int i_56_, int i_57_);

	j(oa var_oa, int i, int i_58_) {
		EA(var_oa, i, i_58_);
	}

	public void method4090(int i, int i_59_, int i_60_, int i_61_, int i_62_, int i_63_) {
		YA(nativeid, i, i_59_, i_60_, i_61_, i_62_, i_63_);
	}

	private native void W(long l, int i, int i_64_, int i_65_, int i_66_, int i_67_);

	j(oa var_oa, int[] is, int i, int i_68_, int i_69_, int i_70_, boolean bool) {
		ua(var_oa, is, i, i_68_, i_69_, i_70_, bool);
	}

	protected void finalize() {
		if (nativeid != 0L)
			Class376.method3955((byte) 123, this);
	}

	private native void P(long l, int i, int i_71_, int i_72_, int i_73_, int i_74_, int i_75_, int i_76_);

	public int method4087() {
		return M(nativeid);
	}

	private native void ma(oa var_oa, int[] is, byte[] is_77_, byte[] is_78_, int i, int i_79_, int i_80_, int i_81_);

	private native void b(long l, float f, float f_82_, float f_83_, float f_84_, float f_85_, float f_86_, int i,
			int i_87_, int i_88_, int i_89_);

	public void method4079(int i, int i_90_, int i_91_, int i_92_, int i_93_) {
		W(nativeid, i, i_90_, i_91_, i_92_, i_93_);
	}

	private native void CA(long l, int[] is);

	private native void RA(long l, int i, int i_94_, int i_95_, int i_96_, int i_97_, int i_98_, int i_99_, int i_100_);

	public void method4094(int i, int i_101_, int i_102_, int i_103_, int i_104_, int i_105_, int i_106_, int i_107_) {
		RA(nativeid, i, i_101_, i_102_, i_103_, i_104_, i_105_, i_106_, i_107_);
	}

	private native int I(long l);

	j(oa var_oa, int[] is, byte[] is_108_, byte[] is_109_, int i, int i_110_, int i_111_, int i_112_) {
		ma(var_oa, is, is_108_, is_109_, i, i_110_, i_111_, i_112_);
	}

	j(oa var_oa, int i, int i_113_, int i_114_, int i_115_, boolean bool) {
		h(var_oa, i, i_113_, i_114_, i_115_, bool);
	}

	public int method4092() {
		return I(nativeid);
	}
}
