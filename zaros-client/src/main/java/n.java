
import net.zaros.client.Class186;
import net.zaros.client.Class376;
import net.zaros.client.Sprite;
import net.zaros.client.Class55;
import net.zaros.client.Class92;
import net.zaros.client.Interface3;
import net.zaros.client.aa;

public class n extends Class55 implements Interface3 {
	public long nativeid;

	private final native void PA(char c, int i, int i_0_, int i_1_, boolean bool, aa var_aa, int i_2_, int i_3_);

	public final native void w(boolean bool);

	public n(oa var_oa, ya var_ya, Class92 class92, Class186[] class186s, Sprite[] class397s) {
		super(var_oa, class92);
		byte[][] is = new byte[class186s.length][];
		int[] is_4_ = new int[class186s.length];
		int[] is_5_ = new int[class186s.length];
		int[] is_6_ = new int[class186s.length];
		int[] is_7_ = new int[class186s.length];
		for (int i = 0; i < class186s.length; i++) {
			is[i] = class186s[i].aByteArray1901;
			is_4_[i] = class186s[i].anInt1899;
			is_5_[i] = class186s[i].anInt1904;
			is_6_[i] = class186s[i].anInt1903;
			is_7_[i] = class186s[i].anInt1906;
		}
		S(var_oa, var_ya, is, class186s[0].anIntArray1900, is_4_, is_5_, is_6_, is_7_);
	}

	public void a(char c, int i, int i_8_, int i_9_, boolean bool, aa var_aa, int i_10_, int i_11_) {
		PA(c, i, i_8_, i_9_, bool, var_aa, i_10_, i_11_);
	}

	protected final void finalize() {
		if (nativeid != 0L)
			Class376.method3955((byte) 123, this);
	}

	private final native void S(oa var_oa, ya var_ya, byte[][] is, int[] is_12_, int[] is_13_, int[] is_14_,
			int[] is_15_, int[] is_16_);

	public native void fa(char c, int i, int i_17_, int i_18_, boolean bool);
}
