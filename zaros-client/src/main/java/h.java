
import net.zaros.client.Class186;
import net.zaros.client.Class376;
import net.zaros.client.Sprite;
import net.zaros.client.Class55;
import net.zaros.client.Class92;
import net.zaros.client.Interface3;
import net.zaros.client.aa;

public class h extends Class55 implements Interface3 {
	long nativeid;

	public final native void w(boolean bool);

	public void a(char c, int i, int i_0_, int i_1_, boolean bool, aa var_aa, int i_2_, int i_3_) {
		NA(c, i, i_0_, i_1_, bool, var_aa, i_2_, i_3_);
	}

	protected final void finalize() {
		if (nativeid != 0L)
			Class376.method3955((byte) 123, this);
	}

	public h(oa var_oa, ya var_ya, Class92 class92, Class186[] class186s, Sprite[] class397s) {
		super(var_oa, class92);
		byte[][] is = new byte[class186s.length][];
		int[] is_4_ = new int[class186s.length];
		int[] is_5_ = new int[class186s.length];
		int[] is_6_ = new int[class186s.length];
		int[] is_7_ = new int[class186s.length];
		for (int i = 0; i < class186s.length; i++) {
			is[i] = class186s[i].aByteArray1901;
			is_4_[i] = class186s[i].anInt1899;
			is_5_[i] = class186s[i].anInt1904;
			is_6_[i] = class186s[i].anInt1903;
			is_7_[i] = class186s[i].anInt1906;
		}
		JA(var_oa, var_ya, is, is_4_, is_5_, is_6_, is_7_);
	}

	public native void fa(char c, int i, int i_8_, int i_9_, boolean bool);

	private final native void JA(oa var_oa, ya var_ya, byte[][] is, int[] is_10_, int[] is_11_, int[] is_12_,
			int[] is_13_);

	private final native void NA(char c, int i, int i_14_, int i_15_, boolean bool, aa var_aa, int i_16_, int i_17_);
}
