
import net.zaros.client.NodeDeque;
import net.zaros.client.Class296_Sub35;
import net.zaros.client.Class376;
import net.zaros.client.Interface3;
import net.zaros.client.r;
import net.zaros.client.s;

public class t extends s implements Interface3 {
	long nativeid;
	private NodeDeque aClass155_3483 = new NodeDeque();
	private oa anOa3484;
	private int anInt3485 = -1;

	public void method3357(Class296_Sub35 class296_sub35, int[] is) {
		aClass155_3483.addLast((byte) -95, class296_sub35);
		V(class296_sub35.hashCode(), class296_sub35.method2749(true), class296_sub35.method2751(-28925),
				class296_sub35.method2750(-4444), class296_sub35.method2748(63), class296_sub35.method2746(-24996), is);
	}

	private native void ga(oa var_oa, ya var_ya, int i, int i_0_, int[][] is, int[][] is_1_, int i_2_, int i_3_,
			int i_4_);

	public native void U(int i, int i_5_, int[] is, int[] is_6_, int[] is_7_, int[] is_8_, int[] is_9_, int[] is_10_,
			int[] is_11_, int[] is_12_, int i_13_, int i_14_, int i_15_, boolean bool);

	public void method3356(int i, int i_16_, int[] is, int[] is_17_, int[] is_18_, int[] is_19_, int[] is_20_,
			int[] is_21_, int[] is_22_, int[] is_23_, int[] is_24_, int[] is_25_, int[] is_26_, int i_27_, int i_28_,
			int i_29_, boolean bool) {
		boolean bool_30_ = false;
		if (is_23_ != null) {
			int[] is_31_ = is_23_;
			for (int i_32_ = 0; i_32_ < is_31_.length; i_32_++) {
				int i_33_ = is_31_[i_32_];
				if (i_33_ != -1) {
					bool_30_ = true;
					break;
				}
			}
		}
		int i_34_ = is_23_.length;
		int[] is_35_ = new int[i_34_ * 3];
		int[] is_36_ = new int[i_34_ * 3];
		int[] is_37_ = new int[i_34_ * 3];
		int[] is_38_ = new int[i_34_ * 3];
		int[] is_39_ = new int[i_34_ * 3];
		int[] is_40_ = is_24_ != null ? new int[i_34_ * 3] : null;
		int[] is_41_ = is_17_ != null ? new int[i_34_ * 3] : null;
		int[] is_42_ = is_19_ != null ? new int[i_34_ * 3] : null;
		int i_43_ = 0;
		for (int i_44_ = 0; i_44_ < i_34_; i_44_++) {
			int i_45_ = is_20_[i_44_];
			int i_46_ = is_21_[i_44_];
			int i_47_ = is_22_[i_44_];
			is_35_[i_43_] = is[i_45_];
			is_36_[i_43_] = is_18_[i_45_];
			is_37_[i_43_] = is_23_[i_44_];
			is_38_[i_43_] = is_25_[i_44_];
			is_39_[i_43_] = is_26_[i_44_];
			if (is_24_ != null)
				is_40_[i_43_] = is_24_[i_44_];
			if (is_17_ != null)
				is_41_[i_43_] = is_17_[i_45_];
			if (is_19_ != null)
				is_42_[i_43_] = is_19_[i_45_];
			i_43_++;
			is_35_[i_43_] = is[i_46_];
			is_36_[i_43_] = is_18_[i_46_];
			is_37_[i_43_] = is_23_[i_44_];
			is_38_[i_43_] = is_25_[i_44_];
			is_39_[i_43_] = is_26_[i_44_];
			if (is_24_ != null)
				is_40_[i_43_] = is_24_[i_44_];
			if (is_17_ != null)
				is_41_[i_43_] = is_17_[i_46_];
			if (is_19_ != null)
				is_42_[i_43_] = is_19_[i_46_];
			i_43_++;
			is_35_[i_43_] = is[i_47_];
			is_36_[i_43_] = is_18_[i_47_];
			is_37_[i_43_] = is_23_[i_44_];
			is_38_[i_43_] = is_25_[i_44_];
			is_39_[i_43_] = is_26_[i_44_];
			if (is_24_ != null)
				is_40_[i_43_] = is_24_[i_44_];
			if (is_17_ != null)
				is_41_[i_43_] = is_17_[i_47_];
			if (is_19_ != null)
				is_42_[i_43_] = is_19_[i_47_];
			i_43_++;
		}
		if (bool_30_ || is_40_ != null)
			U(i, i_16_, is_35_, is_41_, is_36_, is_42_, is_37_, is_40_, is_38_, is_39_, i_27_, i_28_, i_29_, bool);
	}

	t(oa var_oa, ya var_ya, int i, int i_48_, int[][] is, int[][] is_49_, int i_50_, int i_51_, int i_52_) {
		super(i, i_48_, i_50_, is);
		anOa3484 = var_oa;
		ga(anOa3484, var_ya, i, i_48_, anIntArrayArray2831, is_49_, i_50_, i_51_, i_52_);
	}

	public native void ka(int i, int i_53_, int i_54_);

	public void method3352(int i, int i_55_) {
		if (anInt3485 < 0)
			anOa3484.C().method146(this, i, i_55_);
		else
			anOa3484.C().method140(this, i, i_55_, anInt3485);
	}

	private native void V(int i, int i_56_, int i_57_, int i_58_, int i_59_, int i_60_, int[] is);

	public void method3354(int i, int i_61_, int i_62_, boolean[][] bools, boolean bool, int i_63_) {
		anInt3485 = -1;
		int i_64_ = 0;
		float[] fs = new float[aClass155_3483.method1580(37)];
		for (Class296_Sub35 class296_sub35 = (Class296_Sub35) aClass155_3483.removeFirst(
				(byte) 125); class296_sub35 != null; class296_sub35 = (Class296_Sub35) aClass155_3483.removeNext(1001))
			fs[i_64_++] = class296_sub35.method2745((byte) 100);
		q(fs);
		for (int i_65_ = 0; i_65_ < i_62_ + i_62_; i_65_++) {
			for (int i_66_ = 0; i_66_ < i_62_ + i_62_; i_66_++) {
				if (bools[i_65_][i_66_]) {
					int i_67_ = i - i_62_ + i_65_;
					int i_68_ = i_61_ - i_62_ + i_66_;
					if (i_67_ >= 0 && i_67_ < anInt2832 && i_68_ >= 0 && i_68_ < anInt2834)
						method3352(i_67_, i_68_);
				}
			}
		}
	}

	protected void finalize() {
		if (nativeid != 0L)
			Class376.method3955((byte) 123, this);
	}

	public native r fa(int i, int i_69_, r var_r);

	public native void YA();

	public native void CA(r var_r, int i, int i_70_, int i_71_, int i_72_, boolean bool);

	private native void q(float[] fs);

	public void method3350(int i, int i_73_, int i_74_, int i_75_, int i_76_, int i_77_, int i_78_, boolean[][] bools) {
		anOa3484.C().method152(this, i, i_73_, i_74_, i_75_, i_76_, i_77_, i_78_, bools);
	}

	public void method3348(int i, int i_79_, int i_80_, boolean[][] bools, boolean bool, int i_81_, int i_82_) {
		anInt3485 = i_81_;
		int i_83_ = 0;
		float[] fs = new float[aClass155_3483.method1580(-113)];
		for (Class296_Sub35 class296_sub35 = (Class296_Sub35) aClass155_3483.removeFirst(
				(byte) 119); class296_sub35 != null; class296_sub35 = (Class296_Sub35) aClass155_3483.removeNext(1001))
			fs[i_83_++] = class296_sub35.method2745((byte) 100);
		q(fs);
		for (int i_84_ = 0; i_84_ < i_80_ + i_80_; i_84_++) {
			for (int i_85_ = 0; i_85_ < i_80_ + i_80_; i_85_++) {
				if (bools[i_84_][i_85_]) {
					int i_86_ = i - i_80_ + i_84_;
					int i_87_ = i_79_ - i_80_ + i_85_;
					if (i_86_ >= 0 && i_86_ < anInt2832 && i_87_ >= 0 && i_87_ < anInt2834)
						method3352(i_86_, i_87_);
				}
			}
		}
	}

	public boolean method3351(r var_r, int i, int i_88_, int i_89_, int i_90_, boolean bool) {
		return true;
	}

	public native void wa(r var_r, int i, int i_91_, int i_92_, int i_93_, boolean bool);

	public native void w(boolean bool);
}
