/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.plugin.fistofguthix;

import lombok.Getter;
import net.zaros.plugin.fistofguthix.dialogues.FiaraDialogue;
import net.zaros.plugin.fistofguthix.dialogues.GuardianDialogue;
import net.zaros.plugin.fistofguthix.dialogues.ReggieDialogue;
import net.zaros.plugin.fistofguthix.rewards.RewardsShop;
import net.zaros.server.core.system.SystemManager;
import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.content.combat.player.registry.wrapper.magic.TeleportType;
import net.zaros.server.game.content.dialogue.Dialogue;
import net.zaros.server.game.content.dialogue.impl.misc.SimpleNPCMessage;
import net.zaros.server.game.content.minigames.MinigameProperties;
import net.zaros.server.game.content.minigames.MinigameStatus;
import net.zaros.server.game.content.minigames.MultiareaProperties;
import net.zaros.server.game.content.minigames.types.multi.MultiareaMinigame;
import net.zaros.server.game.content.minigames.util.JoinLeaveOptions;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.Entity;
import net.zaros.server.game.node.entity.npc.NPC;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.node.object.GameObject;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.ButtonOption;
import net.zaros.server.utility.rs.Hit;
import net.zaros.server.utility.rs.InteractionOption;

;

/**
 * Represents the Fist of Guthix minigame implementation.
 * 
 * @author Walied K. Yassen
 */
public final class FistOfGuthix extends MultiareaMinigame<FistOfGuthixLobby, FistOfGuthixWaiting, FistOfGuthixGame> {

	/**
	 * The game properties.
	 */
	public static final MinigameProperties PROPERTIES = new MultiareaProperties("Fist of Guthix", 750, 250, 250);

	/**
	 * The minigame singleton
	 */
	@Getter
	private static final FistOfGuthix singleton = new FistOfGuthix();

	/**
	 * The Fist of Guthix token item id.
	 */
	public static final int TOKEN = 12852;

	/**
	 * The elemental rune item id.
	 */
	public static final int ELEMENTAL_RUNE = 12850;

	/**
	 * The catalytic rune item id.
	 */
	public static final int CATALYTIC_RUNE = 12851;

	/**
	 * The heal bandage item id.
	 */
	public static final int BANDAGE = 12853;

	/**
	 * The teleport orb item id.
	 */
	public static final int TELE_ORB = 12855;

	/**
	 * Construct a new {@link FistOfGuthix} type object instance.
	 */
	private FistOfGuthix() {
		super(PROPERTIES);
	}

	/*
	 * 
	 */
	@Override
	public void setup() {
		lobbyArea = new FistOfGuthixLobby(this);
		waitingArea = new FistOfGuthixWaiting(this);
		gameArea = new FistOfGuthixGame(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.content.minigames.Minigame#tick()
	 */
	@Override
	protected void tick() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#handlePreDisconnect(org.redrune.
	 * game.node.entity.player.Player)
	 */
	@Override
	public boolean handlePreDisconnect(Player player) {
		MinigameStatus status = player.getTemporaryAttribute(AttributeKey.MINIGAME_STATUS);
		if (status == null || status != MinigameStatus.INGAME) {
			return false;
		}
		player.getTransmitter().sendMessage("You can't log out in the middle of a game. If you want to leave, use one of the exits.");
		return true;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#logout(org.redrune.game.node.
	 * entity.player.Player)
	 */
	@Override
	public void handlePostDisconnect(Player player) {
		MinigameStatus status = player.getTemporaryAttribute(AttributeKey.MINIGAME_STATUS);
		if (status == null) {
			return;
		}
		if (status == MinigameStatus.INGAME) {
			gameArea.forfeit(player, true);
		}
		super.handlePostDisconnect(player);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#handleDeath(org.redrune.game.node
	 * .entity.Entity)
	 */
	@Override
	public boolean handlePreDeath(Entity entity) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#handlePostDeath(org.redrune.game.
	 * node.entity.Entity)
	 */
	@Override
	public boolean handlePostDeath(Entity entity) {
		if (!(entity instanceof Player)) {
			return false;
		}
		Player player = (Player) entity;
		if (!gameArea.contains(player)) {
			return false;
		}
		return gameArea.handle_post_death(player);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#handleButton(org.redrune.game.
	 * node.entity.player.Player, int, int, int, int,
	 * org.redrune.utility.rs.ButtonOption)
	 */
	@Override
	public boolean handleButton(Player player, int interfaceId, int buttonId, int slotId, int itemId, ButtonOption option) {
		if (interfaceId == 732) {
			RewardsShop.handle(player, buttonId, option);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#handleInventory(org.redrune.game.
	 * node.entity.player.Player, org.redrune.game.node.item.Item, int,
	 * org.redrune.utility.rs.InteractionOption)
	 */
	@Override
	public boolean handleInventory(Player player, Item item, int slot, InteractionOption option) {
		final int id = item.getId();
		if (id != BANDAGE && id != TELE_ORB || option != InteractionOption.FIRST_OPTION) {
			return false;
		}
		MinigameStatus status = player.getTemporaryAttribute(AttributeKey.MINIGAME_STATUS);
		if (status != MinigameStatus.INGAME || ((FistOfGuthixPair) player.getTemporaryAttribute(AttributeKey.FOG_PAIR)).getStartingTicks() > 0) {
			player.getTransmitter().sendMessage("You should save that for when you need it.");
			return true;
		}
		if (id == BANDAGE) {
			player.heal(150);
			player.getTransmitter().sendMessage("You feel a bit better.");
			player.getInventory().deleteItem(slot, new Item(BANDAGE, 1));
		} else if (id == TELE_ORB) {
			gameArea.centre_teleport(player);
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.redrune.game.content.minigames.Minigame#handleModalClosing(int)
	 */
	@Override
	public boolean handleModalClosing(Player player, int interfaceId) {
		if (interfaceId == 762) {
			FistOfGuthixLobby.checkInventory(player);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#handleObject(org.redrune.game.
	 * node.entity.player.Player, org.redrune.game.node.object.GameObject,
	 * org.redrune.utility.rs.InteractionOption)
	 */
	@Override
	public boolean handleObject(Player player, GameObject object, InteractionOption option) {
		final int id = object.getId();
		MinigameStatus status = player.getTemporaryAttribute(AttributeKey.MINIGAME_STATUS);
		if (id == 30224) {
			if (checkBanned(player)) {
				// Banned, we do nothing.
			} else if (!player.getVariables().getAttribute(AttributeKey.FOG_FIARA, false)) {
				player.getManager().getDialogues().startDialogue(new SimpleNPCMessage(7602, Dialogue.E9785, "You cannot go in there unless you've talked to Lady Fiara!"));
			} else {
				if (waitingArea.contains(player)) {
					if (waitingArea.tryLeave(player)) {
						lobbyArea.tryJoin(player, JoinLeaveOptions.DONT_MOVE);
					}
				} else {
					if (FistOfGuthixLobby.checkInventory(player)) {
						if (lobbyArea.tryLeave(player, JoinLeaveOptions.DONT_MOVE)) {
							waitingArea.tryJoin(player);
						}
					}
				}
			}
			return true;
		} else if (id == 30141) {
			// energy barrier
			if (status == MinigameStatus.INGAME) {
				long time = player.putTemporaryAttribute(AttributeKey.FOG_BARRIER, 0);
				if (System.currentTimeMillis() - time < 20000) {
					player.getTransmitter().sendMessage("You have recently left a portal house and have to wait a while before going through the barrier again.");
					return true;
				}
				player.removeTemporaryAttribute(AttributeKey.FOG_BARRIER);
				// checks whether or not we are currently not the hunter.
				FistOfGuthixPair pair = player.getTemporaryAttribute(AttributeKey.FOG_PAIR);
				if (pair.getTarget() != player) {
					player.getTransmitter().sendMessage("You find youreslf unable to pass through the magical barrier.");
					return true;
				}
				// grab the useful locations that we may need.
				Location player_location = player.getLocation();
				Location object_location = object.getLocation();
				// the location where we want to walk to.
				Location target_location = object_location;
				// whether we are currently inside or outside.
				boolean from_inside = false;
				// grab the rotation of the barrier.
				int rotation = object.getRotation();
				// calculate the target location based on rotation of the barrier.
				if (rotation == 0) {
					from_inside = player_location.getX() < object_location.getX();
					if (!from_inside) {
						target_location = object_location.transform(-1, 0, 0);
					}
				} else if (rotation == 1) {
					from_inside = player_location.getY() > object_location.getY();
					if (!from_inside) {
						target_location = object_location.transform(0, +1, 0);
					}
				} else if (rotation == 2) {
					from_inside = player_location.getX() > object_location.getX();
					if (!from_inside) {
						target_location = object_location.transform(+1, 0, 0);
					}
				} else {
					from_inside = player_location.getY() < object_location.getY();
					if (!from_inside) {
						target_location = object_location.transform(0, -1, 0);
					}
				}
				// make things final so we can access them from inside of the anonymous class.
				final Location $target_location = target_location;
				final boolean $from_inside = from_inside;
				// lock the player so they cannot interrupt.
				player.getManager().getLocks().lockAll();
				// start the task of getting in.
				SystemManager.getScheduler().schedule(new ScheduledTask(1, 4) {

					@Override
					public void run() {
						int ticks = getTicksPassed();
						if (ticks == 1) {
							player.setInFightArea($from_inside);
							player.getMovement().addWalkSteps($target_location.getX(), $target_location.getY(), 2, false);
						} else if (ticks == 2) {
							if (!$from_inside) {
								player.getHitMap().applyHit(new Hit(null, 90));
								player.getTransmitter().sendMessage("You feel some of your life drain as you enter through the barrier.");
							}
						} else if (ticks == 3) {
							if ($from_inside) {
								player.removeTemporaryAttribute(AttributeKey.FOG_HOUSE);
								player.putTemporaryAttribute(AttributeKey.FOG_BARRIER, System.currentTimeMillis());
							} else {
								player.putTemporaryAttribute(AttributeKey.FOG_HOUSE, tick);
							}
							player.getManager().getLocks().unlockAll();
							stop();
						}
					}
				});
			}
		} else if (id == 30143) {
			if (status == MinigameStatus.INGAME) {
				FistOfGuthixPair pair = player.getTemporaryAttribute(AttributeKey.FOG_PAIR);
				if (player == pair.getHunter()) {
					player.getTransmitter().sendMessage("You are not allowed to take one of those in your current role.");
					return true;
				}
				gameArea.take_stone(player);
			}
			return true;
		} else if (id == 30144 || id == 30145) {
			if (status == MinigameStatus.INGAME) {
				gameArea.forfeit(player, false);
			}
			return true;
		} else if (id == 30146) {
			if (status == MinigameStatus.INGAME) {
				gameArea.house_teleport(player, object.getLocation());
			}
		} else if (id == 30203) {
			exit(player);
			return true;
		}
		return false;
	}

	/**
	 * Checks whether or not the specified {@link Player} is currently banned from
	 * accessing the game or not.
	 * 
	 * @param player
	 *               the player to check.
	 * @return <code>true</code> if they are otherwise <code>false</code>.
	 */
	private boolean checkBanned(Player player) {
		Double time = player.getVariables().getAttribute(AttributeKey.FOG_BAN);
		if (time == null) {
			return false;
		}
		if (time < System.currentTimeMillis()) {
			player.getVariables().removeAttribute(AttributeKey.FOG_BAN);
			return false;
		}
		int minutes = (int) ((time - System.currentTimeMillis()) / 60000);
		if (minutes == 0) {
			minutes = 1;
		}
		player.getTransmitter().sendMessage("You forfeited your last game so you will need to wait " + minutes + " minute.");
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#handleNPC(org.redrune.game.node.
	 * entity.player.Player, org.redrune.game.node.entity.npc.NPC,
	 * org.redrune.utility.rs.InteractionOption)
	 */
	@Override
	public boolean handleNPC(Player player, NPC npc, InteractionOption option) {
		final int id = npc.getId();
		if (id == 7600) {
			player.getManager().getDialogues().startDialogue(new FiaraDialogue());
			return true;
		} else if (id == 7601) {
			if (option == InteractionOption.FIRST_OPTION) {
				player.getManager().getDialogues().startDialogue(new ReggieDialogue());
			} else {
				player.getManager().getInterfaces().sendInterface(732, false);
			}
			return true;
		} else if (id == 7602 || id == 7603 || id == 7604) {
			player.getManager().getDialogues().startDialogue(new GuardianDialogue(npc.getId()));
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#handlePlayer(org.redrune.game.
	 * node.entity.player.Player, org.redrune.game.node.entity.player.Player,
	 * org.redrune.utility.rs.InteractionOption)
	 */
	@Override
	public boolean handlePlayer(Player player, Player other, InteractionOption option) {
		MinigameStatus status = player.getTemporaryAttribute(AttributeKey.MINIGAME_STATUS);
		if (status != MinigameStatus.INGAME) {
			return false;
		}
		FistOfGuthixPair pair = player.getTemporaryAttribute(AttributeKey.FOG_PAIR);
		if (option != InteractionOption.ATTACK_OPTION) {
			player.getTransmitter().sendMessage("You cannot do that right now.");
			return true;
		}
		if (FistOfGuthixGame.is_holding_stone(player)) {
			player.getTransmitter().sendMessage("You cannot attack with the stone!");
			return true;
		}
		if (other == pair.getHunter() || other == pair.getTarget()) {
			return false;
		}
		player.getTransmitter().sendMessage("You can only attack your opponent!");
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#handleItemOnNPC(org.redrune.game.
	 * node.entity.player.Player, org.redrune.game.node.entity.npc.NPC,
	 * org.redrune.game.node.item.Item)
	 */
	// @Override
	// public boolean handleItemOnNPC(Player player, NPC npc, Item item) {
	// return false;
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.minigames.Minigame#isTeleportAllowed(org.redrune.
	 * game.content.combat.player.registry.wrapper.magic.TeleportType)
	 */
	@Override
	public boolean isTeleportAllowed(Player player, TeleportType type) {
		MinigameStatus status = player.getTemporaryAttribute(AttributeKey.MINIGAME_STATUS);
		if (status == MinigameStatus.LOBBY) {
			return true;
		}
		player.getTransmitter().sendMessage("You find youreslf unable to teleport from this area.");
		return false;
	}
}
