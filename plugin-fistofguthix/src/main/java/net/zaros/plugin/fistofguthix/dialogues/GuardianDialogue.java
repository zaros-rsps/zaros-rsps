/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.plugin.fistofguthix.dialogues;

import net.zaros.server.game.content.dialogue.Dialogue;
import net.zaros.server.game.node.entity.player.Player;

/**
 * Represents the Fist of Guthix gaurdians dialogue.
 * 
 * @author Walied K. Yassen
 */
public final class GuardianDialogue extends Dialogue {

	/**
	 * The guardian Getorix NPC id.
	 */
	private static final int GETORIX = 7602;

	/**
	 * The guardian Pontimer NPC id.
	 */
	private static final int PONTIMER = 7603;

	/**
	 * The guardian Alran NPC id.
	 */
	private static final int ALRAN = 7604;

	/**
	 * The current guardian id.
	 */
	private final int npcId;

	/**
	 * Construct a new {@link GuardianDialogue} type object instance.
	 * 
	 * @param npcId
	 *              the guardian NPC id.
	 */
	public GuardianDialogue(int npcId) {
		this.npcId = npcId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.dialogue.Dialogue#constructMessages(org.redrune.game
	 * .node.entity.player.Player)
	 */
	@Override
	public void constructMessages(Player player) {
		npc(npcId, E9807, "Welcome to this grand cave of Guthix, traveller.");
		options(DEFAULT_OPTION, new String[] { "So, what is this place?", "Who's the weird looking giant earwig?", "I'd better find going." }, this::question_place, this::question_earwing, this::better_get_going);
	}

	private void question_place() {
		player(E9827, "So, what is this place?");
		npc(npcId, E9807, "This is a sacred site of Guthix.");
		player(E9827, "Why is it sacred?");
		npc(npcId, E9808, "Guthix places guardians in places of holy power. There is", "one such guardian here.");
		player(E9807, "But what is special about this place?");
		npc(npcId, E9808, "It exudes a dangerous magical power that we have been", "entrusted to protect.");
		player(E9807, "Where did this power come from?");
		npc(npcId, E9807, "It comes from the centre of the big cave.");
		player(E9807, "What's special about the centre?");
		npc(npcId, E9807, "It's very magical.");
		player(E9807, "Yes, I find that. Why is it magical?");
		npc(npcId, E9807, "Something to do with the Fist of Guthix.");
		player(E9807, "And what is that, exactly?");
		npc(npcId, E9836, "The truth is, none of us really know.");
		player(E9807, "This place is special to Guthix, because Fiara is here.");
		npc(npcId, E9808, "And we know it has something to do with the 'Fist of", "Guthix' because Fiara occasionally mentions it.");
		player(E9828, "But other than that, we only have theories. Do you want", "to hear about them?");
		options(DEFAULT_OPTION, new String[] { "I suppose so...", "I'd better find going." }, this::question_theory, this::better_get_going);
	}

	private void question_theory() {
		options(DEFAULT_OPTION, new String[] { "Getorix's theory.", "Pontimer's theory.", "Alran's theory.", "I'd better find going." }, this::getorixs_theory, this::pontimer_theory, this::alran_theory, this::better_get_going);
	}

	private void getorixs_theory() {
		if (npcId == GETORIX) {
			npc(npcId, E9808, "Well, this is definitely the truth. Don't believe what Alran", "and Pontimer tell you, they are just making it up.");
		} else {
			npc(npcId, E9807, "Well, this is Getorix's silly theory. Listen to it, if you must.");
		}
		npc(npcId, E9810, "At the end of the God Wars, Guthix returned to Gielinor", "and was angered by the destruction caused by the other", "gods in their vanity and quest for power. So, maybe he", "punched the ground in anger, leaving an imprint of power");
		npc(npcId, E9807, "on the ground that has lasted ever since.");
		npc(npcId, E9810, "Because he was angry at the time, the power would have", "been tainted with aggression, which is why he left Fiara as", "a guardian, to make sure the power did not escape", "unfettered");
		if (npcId != GETORIX) {
			player(E9827, "Do you believe this theory?");
			npc(npcId, E9810, "No, I think it's rubbish; if it was true, that would mean", "Guthix's hand would be a very strange shape. The", "indentation in the centre of the cave doesn't look like a", "fist to me. Getorix has just let his imagination run away");
			npc(npcId, E9807, "with him. My theory is better.");
		}
		ask_more_theories();
	}

	private void pontimer_theory() {
		if (npcId == PONTIMER) {
			npc(npcId, E9807, "If you want the true story, then this is it.");
		} else {
			npc(npcId, E9807, "This is Pontimer's personal delusion on the subject.");
		}
		npc(npcId, E9808, "After the God Wars, Guthix found something dangerous", "here and used his power to protect it.");
		player(E9827, "What did he find?");
		npc(npcId, E9810, "I don't know for certain, but it must be under that", "strange impression in the big cave. I think that mark is", "some sort of ward that Guthix has left to protect the", "site.");
		player(E9827, "So where does the Fist of Guthix come into this, then?");
		if (npcId == PONTIMER) {
			npc(npcId, 9810, "Hmmm. I'm not really sure. I think that when Fiara refers", "to the Fist of Guthix she is talking metaphorically about", "an abstract concept of fist-ness rather than the clenched", "right hand of Guthix itself.");
			player(E9807, " That sounds a bit...made up to me.");
			npc(npcId, E9786, "Why you impudent...bah. It's better than anything that", "Getorix and Alran can come up with, anyway.");
		} else {
			npc(npcId, E9786, "Exactly! That's what I told him. It just doesn't make", "sense. My theory makes much more sense.");
		}
		ask_more_theories();
	}

	private void alran_theory() {
		if (npcId == ALRAN) {
			npc(npcId, E9808, "It seems I'm the only one around here who can see the", "truth.");
			npc(npcId, E9808, "Guthix has left us a test. He deliberately created this site", "of power as a test of individual strength and balance.");
			npc(npcId, E9810, "We do know that this place has existed from around the", "time of the God Wars, so maybe Guthix felt it was", "important to test one's strength against another in order", "to face the cruelties of war.");
			player(E9828, "Does Guthix do that sort of thing? And you let anyone into", "the cave now.");
			npc(npcId, E9807, "Of course, it wouldn't be balanced otherwise.");
			player(E9827, "How do you know?");
			npc(npcId, E9828, "I have interpreted the teachings of our Lord Guthix, and", "the cryptic words of Guardian Fiara.");
			player(E9836, "'Interpreted' - you mean this is a guess?");
			npc(npcId, E9786, "How dare you question my understanding of the ways of", "balance!");
		} else {
			npc(npcId, E9808, "This is what Alran thinks, for what it's worth...which isn't", "very much, if you ask me.");
			npc(npcId, E9809, "Guthix has apparently left us a test. He deliberately", "created this site of power as a test of individual strength", "and balance.");
			player(E9827, "Does Guthix do that sort of thing?");
			npc(npcId, E9808, "Well, no! I agree with you entirely. This theory has so", "many holes that it makes no sense at all! ");
		}
		ask_more_theories();
	}

	private void ask_more_theories() {
		npc(npcId, E9807, "Do you want to hear another theory? ");
		question_theory();
	}

	private void question_earwing() {
		player(E9827, "Who's the weird looking giant earwig?");
		npc(npcId, E9786, "Shhh! Give Fiara some respect! She can hear you, you", "know.");
		player(E9827, "Fiara? It has a name?");
		npc(npcId, E9785, "Of course, Guthix chooses names for all his guardians.");
		player(E9807, "If...err...Fiara is a Guardian, what is she guarding?");
		npc(npcId, E9807, "She guards this site. It is a holy site of Guthix.");
		options(DEFAULT_OPTION, new String[] { "So, what is this place?", "How long has Fiara been here?", "I'd better find going." }, this::question_place, this::question_fiara_time, this::better_get_going);
	}

	private void question_fiara_time() {
		player(E9827, "How long has Fiara been here?");
		npc(npcId, E9808, "Errr, ages. Since the end of the God Wars I think. She<br>doesn't talk about that much. ");
		options(DEFAULT_OPTION, new String[] { "So, what is this place?", "I'd better find going." }, this::question_place, this::better_get_going);

	}

	private void better_get_going() {
		player(E9807, "I'd better find going.");
	}
}
