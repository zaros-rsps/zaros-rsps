/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.plugin.fistofguthix.rewards;

import net.zaros.plugin.fistofguthix.FistOfGuthix;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.rs.ButtonOption;

/**
 * Represents the Fist of Guthix rewards shop handler.
 * 
 * @author Walied K. Yassen
 */
public final class RewardsShop {

	public static void handle(Player player, int buttonId, ButtonOption option) {
		if (buttonId == 208) {
			// Closing is handled client side.
		} else {
			int index = (buttonId - 2) / 5;
			if (index > 9) {
				index--;
			}
			if (index > 20) {
				index--;
			}
			if (index >= RewardItem.values().length) {
				return;
			}
			RewardItem item = RewardItem.values()[index];
			if (option == ButtonOption.FIRST) {
				if (item.getRechargeCost() == -1) {
					player.getTransmitter().sendMessage("This item costs " + item.getBuyCost() + " Fist of Guthix tokens to buy. It cannot be recharged.");
				} else {
					player.getTransmitter().sendMessage("This item costs " + item.getBuyCost() + " Fist of Guthix tokens to buy and " + item.getRechargeCost() + " Fist of Guthix tokens to");
					player.getTransmitter().sendMessage("recharge.");
				}
			} else {
				if (item.getRechargeCost() == -1) {
					if (option == ButtonOption.SECOND) {
						buy(player, item, 1);
					} else if (option == ButtonOption.THIRD) {
						showInformation(player, item);
					}
				} else {
					if (option == ButtonOption.SECOND) {
						buy(player, item, 1);
					} else if (option == ButtonOption.THIRD) {
						buy(player, item, 5);
					} else if (option == ButtonOption.FOURTH) {
						buy(player, item, 10);
					} else if (option == ButtonOption.FIFTH) {
						showInformation(player, item);
					}
				}
			}

		}

	}

	private static void buy(Player player, RewardItem item, int amount) {
		int tokens = player.getInventory().getItems().getNumberOf(FistOfGuthix.TOKEN);
		if (tokens < 1 || tokens < item.getBuyCost()) {
			player.getTransmitter().sendMessage("You don't have enough Fist of Guthix tokens with you.");
			return;
		}
		int totalCost = item.getBuyCost() * amount;
		if (tokens < totalCost) {
			player.getTransmitter().sendMessage("You don't have enough Fist of Guthix tokens to buy " + amount + " of " + item.getName() + ".");
			amount = tokens / item.getBuyCost();
			totalCost = amount * item.getBuyCost();
		}
		player.getInventory().deleteItem(FistOfGuthix.TOKEN, totalCost);
		player.getInventory().addItem(item.getItemId(), amount);
		player.getTransmitter().sendMessage("You bought " + amount + " of " + item.getName() + ".");
	}

	private static void showInformation(Player player, RewardItem item) {
		for (String information : item.getInformation()) {
			player.getTransmitter().sendMessage(information);
		}
	}

	/**
	 * A private constructor which prevents the {@code RwardsShop} class type to be
	 * instanced anywhere outside this class.
	 * 
	 * @throws IllegalAccessException
	 *                                if this constructor was accessed in a illegal
	 *                                away (reflection).
	 */
	private RewardsShop() throws IllegalAccessException {
		throw new IllegalAccessException(getClass().getSimpleName() + " is not allowed to be instantiate.");
	}
}
