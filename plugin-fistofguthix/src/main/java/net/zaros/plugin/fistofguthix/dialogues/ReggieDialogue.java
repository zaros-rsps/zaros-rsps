/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.plugin.fistofguthix.dialogues;

import net.zaros.server.game.content.dialogue.Dialogue;
import net.zaros.server.game.node.entity.player.Player;

/**
 * @author Walied K. Yassen
 */
public final class ReggieDialogue extends Dialogue {

	public static final int REGGIE = 7601;

	private Player player;

	@Override
	public void constructMessages(Player player) {
		this.player = player;
		npc(REGGIE, E9848, "<p=1>Welcome, adventurer. Have you come to have a look at", "my fine wares?");
		options(DEFAULT_OPTION, new String[] { "Yes, I want to see what you're selling.", "No, I have some questions.", "No, I'm just passing by." }, this::open_shop, this::main_questions, () -> player(E9807, "No, I'm just passing by."));
	}

	private void open_shop() {
		player.getManager().getInterfaces().sendInterface(732, false);
	}

	private void main_questions() {
		options(DEFAULT_OPTION, new String[] { "What currency are you trading in?", "Can you recharge my items?", "Can you uncharge my items?", "Why do all of your wares degrade?", "I have to leave now." }, this::question_currency, this::question_recharge, this::question_uncharge, this::question_degrade, () -> player(E9807, "Sorry, but I have to leave now. Goodbye."));
	}

	private void question_currency() {
		player(E9828, "What currency are you trading in? It doesn't seem to be", "regular coins.");
		npc(REGGIE, E9808, "You are absolutely right, my friend. I exchange my goods", "for so called Fist of Guthix tokens.");
		options(DEFAULT_OPTION, new String[] { "How can I find hold of these tokens?", "What are Fist of Guthix tokens?", "I have to go now." }, this::question_token_gethold, this::question_token_whatare, () -> player(E9807, "I have to go now."));
	}

	private void question_token_gethold() {
		player(E9827, "How can I find hold of these tokens?");
		npc(REGGIE, E9808, "If you win a game in the Fist of Guthix arena, you will be", "awarded with tokens, based on how well you perform.");
		options(DEFAULT_OPTION, new String[] { "What are you selling?", "I have more questions that I want answered.", "Thanks for the help." }, this::open_shop, this::main_questions, () -> player(E9807, "Thanks for the help. I'll be leaving now."));
	}

	private void question_token_whatare() {
		player(E9827, "What are Fist of Guthix tokens?");
		npc(REGGIE, E9808, "Oh, they're nothing special in themselves. They are just tokens we use as currency.");
		options(DEFAULT_OPTION, new String[] { "What are you selling?", "How can I find hold of these tokens?", "I have another question.", "I have to go now." }, this::open_shop, this::question_token_gethold, this::main_questions, () -> player(E9807, "I have to go now."));
	}

	private void question_recharge() {
		player(E9827, "Can you recharge my items?");
		npc(REGGIE, E9849, "By items, I assume you mean items originally acquired", "from me. If so, the answer is yes, my friend. I gladly", "spread the power of Guthix, if I am able to.");
		player(E9807, "That's great.");
		npc(REGGIE, E9810, "It certainly is. The power of Guthix does not come", "cheaply, though. So, I'm afraid I will have to charge you", "some tokens for it. And I can only recharge items that", "have been completely depleted of their magical energy.");
		player(E9807, "Sounds fair. So, how do I go about doing this?");
		npc(REGGIE, E9808, "Just hand me the item you want recharged and I'll see", "what I can do.");
		options(DEFAULT_OPTION, new String[] { "I want to see what you're selling first.", "How can I find hold of these tokens?", "I still have more questions.", "Okay, I'll do that." }, this::open_shop, this::question_token_gethold, this::main_questions, () -> player(E9807, "Okay, I'll do that."));
	}

	private void question_uncharge() {
		player(E9827, "Can you uncharge my items for me?");
		npc(REGGIE, E9814, "You don't want to keep the power of Guthix? That's", "strange. But yes, of course I can do this for you. Please", "be aware that you won't find anything in return, except", "the degraded item, and it will cost you tokens to have it");
		npc(REGGIE, E9811, "charged again.");
		player(E9807, "Yes, I will keep that in mind.");
		npc(REGGIE, E9808, "Good. In that case, just hand me the item from which you", "want the charge removed.");
		options(DEFAULT_OPTION, new String[] { "I want to see what you're selling first.", "I still have more questions.", "Okay, I'll do that." }, this::open_shop, this::main_questions, () -> player(E9807, "Okay, I'll do that."));
	}

	private void question_degrade() {
		player(E9829, "Why do all of your wares degrade? I have seen plenty of", "similar items in my days, but they all seem to last a lot", "longer than yours.");
		npc(REGGIE, E9810, "A most accurate observation, my friend. But the other", "items you've come across in your travels most certainly", "have not been directly infused with the power emanating", "from the Fist of Guthix.");
		player(E9812, "Umm...I suppose not. But wouldn't the power of Guthix", "have made the items stronger?");
		npc(REGGIE, E9810, "Why, yes, they have. Keeping a shop so close to the", "source of this power has resulted in the items being", "infused with energy. This makes them stronger than they", "normally would have been, but, unfortunately, they are");
		npc(REGGIE, E9810, "hardly suitable vessels for such divine power. As they are", "being used, the power will be spent, and all that will remain", "is an empty, weakened shell. When this happens, just come", "back to me and I will gladly recharge them for you.");
		player(E9807, "...For a small fee.");
		npc(REGGIE, E9840, "Haha. Yes, my friend, for a small fee. The power of a god", "does not come cheaply, not even in small doses. If we", "were to give it out for free, we would run out of", "resources in an instant. Balance must be maintained.");
		player(E9807, "Okay, I will keep that in mind. Thank you.");
		npc(REGGIE, E9807, "Now, is there anything else I can help you with?");
		options(DEFAULT_OPTION, new String[] { "What are you selling?", "I have another question.", "I have to go now." }, this::open_shop, this::main_questions, () -> player(E9807, "I have to go now."));
	}
}
