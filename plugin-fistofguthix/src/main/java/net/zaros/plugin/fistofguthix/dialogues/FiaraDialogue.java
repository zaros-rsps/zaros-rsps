/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.plugin.fistofguthix.dialogues;

import net.zaros.plugin.fistofguthix.FistOfGuthix;
import net.zaros.server.core.system.SystemManager;
import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.content.dialogue.Dialogue;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.utility.AttributeKey;

/**
 * Represents the Fist of Guthix Fiara dialogue.
 * 
 * @author Walied K. Yassen
 */
public final class FiaraDialogue extends Dialogue {

	/**
	 * The Fiara NPC id.
	 */
	private static final int FIARA = 7600;

	/**
	 * The player who initiated this dialogue.
	 */
	private Player player;

	/**
	 * The original location before the cutscene starts.
	 */
	private Location originalLocation;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.dialogue.Dialogue#constructMessages(org.redrune.game
	 * .node.entity.player.Player)
	 */
	@Override
	public void constructMessages(Player player) {
		this.player = player;
		if (player.getVariables().getAttribute(AttributeKey.FOG_FIARA, false)) {
			npc(FIARA, E8478, "Can I help you?");
			options(DEFAULT_OPTION, new String[] { "I have more questions about the Fist of Guthix game.", "What are you?", "Never mind." }, this::questions_main, this::question_fiara, this::never_mind);
		} else {
			chatbox("As you approach the...big creature thing, it lets out a hiss that sounds", "partly like a threat and partly like a sigh.");
			player(ERM_THINKING, "Erm, excuse me...Lady Fiara, but the druids told me to", "talk to you.");
			npc(FIARA, E8381, "*Hiss* Yes...the druids. They say and do far more than", "they should.");
			player(E9811, "What's wrong with the druids? They seem to worship you.");
			npc(FIARA, E8381, "They worship everything that is green or sounds even", "slightly magical. Not just worship - they stalk. And when", "they have arrived, it is impossible to find them away. They", "are like insects.");
			player(E9836, "You want to find rid of them, eh? I might be able to-");
			npc(FIARA, E8381, "No, no. *Hiss* Now you're just like them. 'What can we do", "for you today, oh great Guardian of Guthix, Lady Fiara?'");
			player(E9761, "Erm, sorry, I guess.");
			npc(FIARA, E8381, "*Hiss* Do not worry your feeble mind with it. What was it", "you wanted? Let's just find it over with.");
			options(DEFAULT_OPTION, new String[] { "What is this place?", "What are you?", "Never mind." }, this::question_place, this::question_fiara, this::never_mind);
		}
	}

	private void questions_main() {
		options(DEFAULT_OPTION, new String[] { "Could you tell me all about the game again?", "How do I find charges?", "Why can't I find into the arena?", "I keep losing all the time!", "Never mind." }, this::question_game, this::question_charges, this::question_limits, this::question_losing, this::never_mind);
	}

	private void question_place() {
		player(E9827, "What is this place?");
		npc(FIARA, E8381, "It is the site of the Fist of Guthix.");
		player(E9827, "Fist of Guthix? What is that?");
		npc(FIARA, E8381, "It is what it is. If you do not already know, it's probably", "best that you don't learn.");
		player(E9811, "Umm, okay. What are you all doing down here then?");
		npc(FIARA, E8381, "*Hiss* Three druids are mainly just walking around NOT", "being quiet. Another one is setting up a fortress of crates", "and dragging all kinds of material junk into the cave. The", "fifth human, who isn't a druid, showed up when the");
		npc(FIARA, E8381, "following masses swarmed the cave.");
		player(E9811, "I see. But...what drew them here in the first place?");
		npc(FIARA, E8381, "The Fist of Guthix. And now they try to harness its power", "through this little business they've set up.");
		player(E9828, "Okay, but they told me to talk to you. Why did they want", "that?");
		npc(FIARA, E8381, "Because I am the guardian of this site and I need to make", "sure that their games do not find out of control.");
		player(E9807, "Alright. Can you tell me about the games, then?");
		question_game();
	}

	private void question_game() {
		npc(FIARA, E8381, "Okay, I will tell you. Now, listen carefully, so that you do", "not forget.");
		npc(FIARA, E8381, "The passageway on my left leads into the <col=dd0000>waiting room</col>. If", "you want to participate in the games, this is where you", "must go.");
		npc(FIARA, E8381, "Once enough adventurers have gathered, I will pair you up", "<col=dd0000>one-on-one</col> against each other, and send you into the", "arena. The arena is the cave of the Fist of Guthix. Allow", "me to show you.");
		action(this::start_cutscene);
	}

	private void question_charges() {
		player(E9827, "How do I find charges?");
		npc(FIARA, E8381, "You can only find charges while you are in the role of the", "<col=dd0000>hunted</col>. When the game starts, look around you - there", "should be a <col=dd0000>stone dispenser</col> nearby. Pick up a <col=dd0000>magical", "<col=dd0000>stone</col> from there and <col=dd0000>wield it</col> to gather <col=dd0000>charges</col>. The");
		npc(FIARA, E8381, "closer to the centre you are, the more <col=dd0000>charges</col> you will", "find.");
		ask_for_questions();
	}

	private void question_limits() {
		player(E9827, "Why can't I find into the game?");
		npc(FIARA, E8381, "There must be enough players in the waiting room for a", "game to start.  Also, there can only be a certain amount", "of players in the game at any time.");
		ask_for_questions();
	}

	private void question_losing() {
		player(E9785, "I keep losing all the time!");
		npc(FIARA, E8381, "That was not a question, but I will give you some tips.");
		npc(FIARA, E8381, "Remember that the <col=dd0000>closer you are to the centre</col> of the", "arena, the <col=dd0000>weaker you will become</col>. If you find yourself", "getting defeated easily and that your armour does you no", "good, you may want to try to flee to the outer edges of");
		npc(FIARA, E8381, "the arena where you will be less weakened. This also", "applies to your opponent. If it is someone who appears to", "be a lot stronger than you, he or she will still be very weak", "closer to the centre - this is where you should strike. You");
		npc(FIARA, E8381, "should also know that while you are in the very centre,", "the energy from the Fist of Guthix will help you replenish", "your run energy much faster than usual.");
		npc(FIARA, E8381, "Also, use the ruins of the past to your advantage. Hide", "behind rubble and make a run for it when your opponent is", "out of reach. Use the buildings to hide or as a means of", "escape.");
		ask_for_questions();
	}

	private void question_fiara() {
		player(E9827, "What are you?");
		npc(FIARA, E8381, "I am Fiara, a Guardian of Guthix. What are you?");
		player(E9827, "Are you guarding the Fist of Guthix, which, if I don't", "already know about, I am not fit to know about?");
		npc(FIARA, E8381, "That is correct. Was there anything else?");
		post_questions();
	}

	private void start_cutscene() {
		originalLocation = player.getLocation();
		player.getManager().getInterfaces().closeChatboxInterface();
		player.getManager().getInterfaces().closePrimaryOverlay();
		player.getManager().getInterfaces().sendBackground(120, false);
		player.getManager().getInterfaces().sendInterfaceText(120, 1, "");
		wait(3);
		action(() -> {
			player.getTransmitter().sendVarp(260, 0);
			player.getTransmitter().sendVarp(1713, 320);
			player.teleport(new Location(1728, 5752, 0));
			player.getTransmitter().sendVarcInt(779, 2697);
			player.getTransmitter().sendMinimapType(2);
		});
		wait(2);
		action(() -> {
			player.getTransmitter().sendCameraMove(97, 14, 875, 232, 232);
			player.getTransmitter().sendCameraLook(76, 29, 175, 232, 232);
			player.getManager().getInterfaces().sendBackground(170, false);
		});
		wait(1);
		npc(FIARA, E8381, "Each game is divided into <col=dd0000>two rounds</col>, and you and your", "opponent will take turns being the <col=dd0000>hunter</col> and the <col=dd0000>hunted</col>.", "I decide who starts as what.");
		action(() -> {
			player.getManager().getInterfaces().sendPrimaryOverlay(730, false);
			player.getManager().getInterfaces().sendInterfaceComponentHidden(730, 23, true);
			player.getManager().getInterfaces().sendInterfaceComponentHidden(730, 24, true);
			player.getManager().getInterfaces().sendInterfaceText(730, 26, "Hunting:");
			player.getManager().getInterfaces().sendInterfaceText(730, 17, "");
			player.getTransmitter().sendVarcString(217, "Opponent");
		});
		npc(FIARA, E8381, "While you are the <col=dd0000>hunter</col>, you will see the text <col=dd0000>'Hunting'</col> in", "the <col=dd0000>upper right corner</col> of the screen.");
		action(() -> {
			player.getManager().getInterfaces().sendInterfaceText(730, 26, "Hunted by:");
		});
		npc(FIARA, E8381, "While you are the <col=dd0000>hunted</col>, this text will say <col=dd0000>'Hunted by'</col>.");
		action(() -> {
			player.getTransmitter().sendCameraMove(88, 26, 650, 4, 4);
			player.getTransmitter().sendCameraLook(96, 34, 100, 4, 4);
		});
		wait(5);
		npc(FIARA, E8381, "The arena is large and circular, and ancient power", "emanates from its <col=dd0000>centre</col>. You will start on opposite sides", "of this arena.");
		npc(FIARA, E8381, "The goal of the <col=dd0000>hunter</col> is simple: chase down and <col=dd0000>defeat", "<col=dd0000>the hunted</col> by any means necessary.");
		action(() -> {
			player.getTransmitter().sendCameraMove(73, 19, 575, 4, 4);
			player.getTransmitter().sendCameraLook(69, 14, 100, 4, 4);
		});
		npc(FIARA, E8381, "In the role of the <col=dd0000>hunted</col>, you must pick up a <col=dd0000>stone</col> from", "the <col=dd0000>dispenser</col>. This is what you will use to gather the", "powerful energy from the centre of the arena.");
		npc(FIARA, E8381, "While you are <col=dd0000>wielding this stone</col> - and ONLY while you <col=dd0000>hold", "<col=dd0000>it in your hands</col>, NOT when it's in your backpack - it will", "draw energy from the Fist of Guthix. Your objective is to", "find as many of these <col=dd0000>charges</col> as possible.");
		action(() -> {
			player.getManager().getInterfaces().sendInterfaceText(730, 17, "Charges: 0");
		});
		npc(FIARA, E8381, "The amount of <col=dd0000>charges</col> you have gathered is listed in the", "<col=dd0000>upper right corner</col>.");
		npc(FIARA, E8381, "As the <col=dd0000>hunted</col>, however, you must be careful. Your", "opponent, the <col=dd0000>hunter</col>, is there to defeat you. While you", "are <col=dd0000>wielding the stone</col>, you will be <col=dd0000>unable to fight</col>. Also,", "you may feel very strong, but in the role of the <col=dd0000>hunted</col>,");
		npc(FIARA, E8381, "you will find your <col=dd0000>abilities severely drained</col>, more as you", "find closer to the centre - someone who otherwise may be", "an easy opponent to defeat in combat may instead prove", "most dangerous.");
		npc(FIARA, E8381, "You can of course try to fight back against your", "opponent, even though it may be difficult. Also, as the", "<col=dd0000>hunted</col>, you will be limited in your magical abilities as well.", "Only a certain spellbook can aid you in this role.");
		npc(FIARA, E8381, "Speaking of <col=dd0000>magic</col>, I should also mention that you will find", "that some spells are <col=dd0000>not as efficient</col> in the cave as they", "may be in other areas.");
		npc(FIARA, E8381, "Finally, as the <col=dd0000>hunted</col>, you will also take <col=dd0000>periodic damage</col>", "from the powerful energies in the arena.");
		player(E9827, "Okay, so the hunter only has to hunt down their target?");
		npc(FIARA, E8381, "That is correct.");
		player(E9829, "And the hunted will be weaker than usual, has to pick up a", "stone from a stone dispenser at the edge of the arena,", "and wield this stone in order to charge it?");
		npc(FIARA, E8381, "Yes, and the <col=dd0000>closer to the centre</col> of the arena you are,", "the more it will be <col=dd0000>charged</col>.");
		player(E9827, "Anything else?");
		action(() -> {
			player.getTransmitter().sendCameraMove(89, 13, 725, 4, 4);
			player.getTransmitter().sendCameraLook(90, 24, 50, 4, 4);
		});
		npc(FIARA, E8381, "Yes. You will find four small <col=dd0000>buildings</col> in the arena. Their", "doorways are blocked by <col=dd0000>magical barriers</col>. Only the <col=dd0000>hunted</col>", "can enter through these and, while inside, will be", "completely <col=dd0000>invisible</col> to everyone else. The barriers are");
		npc(FIARA, E8381, "dangerous, however, and will <col=dd0000>drain some of your health</col> as", "you pass through them.");
		npc(FIARA, E8381, "Inside these <col=dd0000>houses</col>, you will find <col=dd0000>portals</col>. You can use", "them to <col=dd0000>teleport from house to house</col> - still <col=dd0000>invisible</col> to", "everyone else - but be aware that the magic is very old", "and the portals have not been actively used for thousands");
		npc(FIARA, E8381, "of years. They may act a bit strangely from time to time.", "Also, the portals require energy to work properly, and will", "<col=dd0000>drain some charges</col> from your stone. If you don't have", "enough charges, you won't be able to use the portal.");
		player(E9808, "Okay, I will keep that in mind. So, when does the round", "end?");
		npc(FIARA, E8381, "A round can end in four different ways;");
		npc(FIARA, E8381, "One: If the <col=dd0000>hunted is defeated</col>.");
		npc(FIARA, E8381, "Two: If the hunted <col=dd0000>completely charges his or her stone</col> -", "which is unlikely to happen.");
		action(() -> {
			player.getManager().getInterfaces().sendInterfaceComponentHidden(730, 23, false);
		});
		npc(FIARA, E8381, "Three: If the <col=dd0000>time runs out</col> - each round can take no", "longer than ten minutes. You can see how much time", "there is left in the <col=dd0000>lower left corner</col> of the screen.");
		action(() -> {
			player.getTransmitter().sendCameraMove(72, 20, 725, 4, 4);
			player.getTransmitter().sendCameraLook(64, 18, 50, 4, 4);
		});
		npc(FIARA, E8381, "Four: If either player <col=dd0000>leaves the arena</col>, in which case the", "other one will be declared the winner. The forfeiter will", "also be flagged as such and receive less tokens in the next", "game they play.");
		npc(FIARA, E8381, "When the first round has ended, the second round <col=dd0000>starts", "<col=dd0000>immediately</col> and the roles switch. The one who played the", "<col=dd0000>hunter</col> in the first round will now have a chance to play", "the <col=dd0000>hunted</col> and gather <col=dd0000>charges</col>.");
		npc(FIARA, E8381, "When both rounds have been played, the one with the", "<col=dd0000>most charges wins</col>.");
		player(E9827, "Ooh, what are the rewards?");
		npc(FIARA, E8381, "The druids put a lot of value into the gathered energy,", "and you will be <col=dd0000>awarded Fist of Guthix tokens</col>. How many", "tokens you find depends on your <col=dd0000>total skill level</col> - a wide", "and balanced array of knowledge is something positive in");
		npc(FIARA, E8381, "the eyes of Guthix - and how many <col=dd0000>charges</col> you've got.");
		npc(FIARA, E8381, "The <col=dd0000>Fist of Guthix tokens</col> can then be <col=dd0000>spent on items</col> in", "the shop the druids have decided to clutter my cave with.");
		npc(FIARA, E8381, "You will also be awarded a <col=dd0000>rating</col>, which is completely", "separate from the tokens. This is simply to keep track of", "how well you play the game. If you win, your rating", "improves. If you lose, your rating will suffer. Your rating");
		npc(FIARA, E8381, "will improve significantly if you gather a lot <col=dd0000>more charges", "<col=dd0000>than your opponent</col>. Also, the rating will be further", "adjusted if the difference in combat levels is large. If you", "lose to someone much more capable than you in combat,");
		npc(FIARA, E8381, "your rating won't suffer as much if you lost to someone", "around your own level.");
		player(E9827, "Is that it?");
		npc(FIARA, E8381, "Almost. Patience is a great virtue.");
		npc(FIARA, E8381, "Finally, you cannot bring just anything you want into the", "game. <col=dd0000>Only items usable in combat</col>, but <col=dd0000>no food</col> and <col=dd0000>no", "<col=dd0000>potions</col>. I will supply you with some means of healing and a", "large amount of <col=dd0000>generic runes</col>, called <col=dd0000>elemental runes</col> and");
		npc(FIARA, E8381, "<col=dd0000>catalytic runes</col>, for your <col=dd0000>magic</col>. These <col=dd0000>runes</col> can be used", "in place of any other runes you can take with you, but do", "note that they can only be used in the Fist of Guthix cave.", "Also, any <col=dd0000>arrows</col> or other forms of <col=dd0000>ammunition</col> used in");
		npc(FIARA, E8381, "the arena will be handed back to you after the game.", "Assuming they are still intact, of course. And don't leave", "anything lying around! I don't want you to completely ruin", "this sacred site.");
		action(() -> {
			player.getManager().getInterfaces().closeChatboxInterface();
			player.getManager().getInterfaces().sendBackground(120, false);
		});
		wait(2);
		action(() -> {
			player.teleport(originalLocation);
			originalLocation = null;
			player.getTransmitter().sendMinimapType(0);
			player.getManager().getInterfaces().closePrimaryOverlay();
		});
		wait(3);
		action(() -> {
			player.getManager().getInterfaces().sendBackground(170, false);
			player.getVariables().putAttribute(AttributeKey.FOG_FIARA, true);
			FistOfGuthix.getSingleton().getLobbyArea().openOverlay(player);
		});
		npc(FIARA, E8478, "Now, do you have any questions?");
		action(this::post_questions);
	}

	private void ask_for_questions() {
		npc(FIARA, E8478, "Any more questions?");
		post_questions();
	}

	private void post_questions() {
		if (player.getVariables().getAttribute(AttributeKey.FOG_FIARA, false)) {
			options(DEFAULT_OPTION, new String[] { "I have more questions about the Fist of Guthix game.", "What are you?", "No, thanks." }, this::questions_main, this::question_fiara, () -> player(E9807, "No, thanks."));
		} else {
			options(DEFAULT_OPTION, new String[] { "What is this place?", "What are you?", "Never mind." }, this::question_place, this::question_fiara, this::never_mind);
		}
	}

	private void never_mind() {
		player(E9807, "Never mind.");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.redrune.game.content.dialogue.Dialogue#end(org.redrune.game.node.entity.
	 * player.Player)
	 */
	@Override
	public void end(Player player) {
		if (originalLocation != null) {
			player.getManager().getInterfaces().sendBackground(120, false);
			SystemManager.getScheduler().schedule(new ScheduledTask(1, 6) {

				@Override
				public void run() {
					int ticks = getTicksPassed();
					if (ticks == 4) {
						player.teleport(originalLocation);
					} else if (ticks == 6) {
						player.getManager().getInterfaces().sendBackground(170, false);
					}
				}
			});
		}
		super.end(player);
	}
}
