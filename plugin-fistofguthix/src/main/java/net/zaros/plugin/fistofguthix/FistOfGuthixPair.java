/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.plugin.fistofguthix;

import lombok.Getter;
import lombok.Setter;
import net.zaros.server.game.node.entity.player.Player;

import java.util.function.Consumer;

/**
 * Represents a Fist of Guthix paired players. Contains all the information
 * needed about those players for the minigame to work.
 * 
 * @author Walied K. Yassen
 */
public final class FistOfGuthixPair {

	/**
	 * The first player in this pair.
	 */
	private final Player first;

	/**
	 * The second player in this pair.
	 */
	private final Player second;

	/**
	 * Tells whether or not this is the final round.
	 */
	@Getter
	@Setter
	private boolean finalRound;

	/**
	 * Tells whether or not to end the current round.
	 */
	@Getter
	@Setter
	private boolean endRound;

	/**
	 * The amount of ticks passed within the current round.
	 */
	@Getter
	@Setter
	private int ticks;

	/**
	 * The ticks that has been passed while not holding the Stone of Power in hands.
	 */
	@Getter
	@Setter
	private int holdingTicks;

	/**
	 * The ticks that are left for the game to start.
	 */
	@Getter
	@Setter
	private int startingTicks;

	/**
	 * Construct a new {@link FistOfGuthixPair} type object instance.
	 * 
	 * @param first
	 *               the first player in the pair.
	 * @param second
	 *               the second player in the pair.
	 */
	public FistOfGuthixPair(Player first, Player second) {
		this.first = first;
		this.second = second;
	}

	/**
	 * Executes a specific command by both of the players.
	 * 
	 * @param consumer
	 *                 the command functional interface.
	 */
	public void execute(Consumer<Player> consumer) {
		consumer.accept(first);
		consumer.accept(second);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (first == null ? 0 : first.hashCode());
		result = prime * result + (second == null ? 0 : second.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FistOfGuthixPair other = (FistOfGuthixPair) obj;
		if (first == null) {
			if (other.first != null) {
				return false;
			}
		} else if (!first.equals(other.first)) {
			return false;
		}
		if (second == null) {
			if (other.second != null) {
				return false;
			}
		} else if (!second.equals(other.second)) {
			return false;
		}
		return true;
	}

	/**
	 * Gets the hunter player.
	 * 
	 * @return the hunter player.
	 */
	public Player getHunter() {
		return finalRound ? first : second;
	}

	/**
	 * Gets the target player.
	 * 
	 * @return the target player.
	 */
	public Player getTarget() {
		return finalRound ? second : first;
	}

}
