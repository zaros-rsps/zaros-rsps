/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.plugin.fistofguthix.rewards;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Represents the Fist of Guthix shop rewards.
 * 
 * @author Walied K. Yassen
 */
@AllArgsConstructor
public enum RewardItem {
	//
	DRUIDIC_MAGE_TOP("Druidic Mage Top", 12894, 300, 30, new String[] { "While wearing a charged set of druidic robes, your Bind, snare, and Entangle spells", "have a higher chance of hitting your target." }),
	DRUIDIC_MAGE_BOTTOM("Druidic Mage Bottom", 12901, 200, 20, new String[] { "While wearing a charged set of druidic robes, your Bind, snare, and Entangle spells", "have a higher chance of hitting your target." }),
	DRUIDIC_MAGE_HOOD("Druidic Mage Hood", 12887, 100, 10, new String[] { "While wearing a charged set of druidic robes, your Bind, snare, and Entangle spells", "have a higher chance of hitting your target." }),
	COMBAT_ROBE_TOP("Combat Robe Top", 12971, 150, 15, new String[] { "While wearing a charged set of combat robes, your robes have a smaller chance of", "any mind, chaos, death, or blood runes required for a destructive spell." }),
	COMBAT_ROBE_BOTTOM("Combat Robe Bottom", 12978, 100, 10, new String[] { "While wearing a charged set of combat robes, your robes have a smaller chance of", "supplying any mind, chaos, death, or blood runes required for a destructive spell." }),
	COMBAT_HOOD("Combat Hood", 12964, 50, 5, new String[] { "While wearing a charged set of combat robes, your robes have a smaller chance o", "supplying any mind, chaos, death, or blood runes required for a destructive spell." }),
	BATTLE_ROBE_TOP("Battle Robe Top", 12873, 1500, 150, new String[] { "While wearing a charged set of battle robes, your robes have a high chance of", "supplying any mind, chaos, death or blood runes required for a destructive spell." }),
	BATTLE_ROBE_BOTTOM("Battle Robe Bottom", 12880, 1000, 100, new String[] { "While wearing a charged set of battle robes, your robes have a high chance of", "supplying any mind, chaos, death or blood runes required for a destructive spell." }),
	BATTLE_HOOD("Battle Hood", 12866, 250, 25, new String[] { "While wearing a charged set of battle robes, your robes have a high chance of", "supplying any mind, chaos, death or blood runes required for a destructive spell." }),
	GREEN_DHIDE_COIF("Green D'hide coif", 12936, 150, 15, new String[] { "A green dhide coif." }),
	BLUE_DHIDE_COIF("Blue D'hide Coif", 12943, 200, 20, new String[] { "A blue dhide coif." }),
	RED_DHIDE_COIF("Red D'hide Coif", 12950, 300, 30, new String[] { "A red dhide coif." }),
	BLACK_DHIDE_COIF("Black D'hide Coif", 12957, 500, 50, new String[] { "A black dhide coif." }),
	BRONZE_GAUNTLETS("Bronze Gauntlets", 12985, 15, 3, new String[] { "A bronze gauntlets." }),
	IRON_GAUNTLETS("Iron Gauntlets", 12988, 30, 5, new String[] { "An iron gauntlets." }),
	STEEL_GAUNTLETS("Steel Gauntlets", 12991, 50, 8, new String[] { "A steel gauntlets" }),
	BLACK_GAUNTLETS("Black Gauntlets", 12994, 75, 13, new String[] { "A black gauntlets." }),
	MITHRIL_GAUNTLETS("Mithril Gauntlets", 12997, 100, 17, new String[] { "A mithril gauntlets." }),
	ADAMANT_GAUNTLETS("Adamant Gauntlets", 13000, 150, 25, new String[] { "An adamant gauntlets." }),
	RUNE_GAUNTLETS("Rune Gauntlets", 13003, 200, 35, new String[] { "A rune gauntlets." }),
	DRAGON_GAUNTLETS("Dragon Gauntlets", 13006, 300, 50, new String[] { "A dragon gauntlets." }),
	ADAMANT_SPIKESHIELD("Adamant Spikeshield", 12908, 50, 5, new String[] { "An adamant spiked shield that has slightly increased armour." }),
	ADAMANT_BERSERKER_SHIELD("Adamant Berserker Shield", 12915, 100, 10, new String[] { "An adamant berserker shield that has slightly increased armour." }),
	RUNE_SPIKESHIELD("Rune Spikeshield", 12929, 200, 20, new String[] { "A spiked shield that has slightly increased armour." }),
	RUNE_BERSERKER_SHIELD("Rune Berserker Shield", 12929, 300, 30, new String[] { "A rune berserker shield that has slightly increased armour." }),
	IRIT_GLOVES("Irit Gloves", 12856, 75, -1, new String[] { "While wearing these gloves, if your opponent drops a herb, it has a higher chance", "of beingirit leaf. The gloves last for 100 such drops. Requires level 50 Herblore." }),
	AVANTOE_GLOVES("Avantoe Gloves", 12857, 100, -1, new String[] { "While wearing these gloves, if your opponent drops a herb, it has a higher chance", "of being avantoe. The gloves last for 100 such drops. Requires level 60 Herblore." }),
	KWUARM_GLOVES("Kwuarm Gloves", 12858, 200, -1, new String[] { "While wearing these gloves, if your opponent drops a herb, it has a higher chance", "of being kwuarm. The gloves last for 100 such drops. Requires level 70 Herblore." }),
	CADANTINE_GLOVES("Cadantine Gloves", 12859, 200, -1, new String[] { "While wearing these gloves, if your opponent drops a herb, it has a higher chance", "of being cadantine. The gloves last for 100 such drops. Requires level 80 Herblore." }),
	SWORDFISH_GLOVES("Swordfish Gloves", 12860, 200, -1, new String[] { "While wearing these gloves, you find 100 more experience for catching a swordfish.", "The gloves last for 1,000 catches. Requires level 65 Fishing." }),
	SHARK_GLOVES("Shark Gloves", 12861, 200, -1, new String[] { "While wearing these gloves, you find 100 more experience for catching a shark.", "The gloves last for 1,000 catches. Requires level 90 Fishing." }),
	DRAGON_SLAYER_GLOVES("Dragon Slayer Gloves", 12862, 200, -1, new String[] { "While wearing these gloves, when you kill dragons as a Slayer task, you find 15%", "more Slayer experience and your accuracy will be increased by 10%. The gloves last", "for 1,000 kills. Requires level 40 Slayer." }),
	AIR_RUNECRAFTING_GLOVES("Air Runecrafting Gloves", 12863, 75, -1, new String[] { "While wearing these gloves, you find 5 more experience for crafting air runes.", "The gloves last for 1,000 essence. Requires level 10 Runecrafting." }),
	WATER_RUNECRAFTING_GLOVES("Water Runecrafting Gloves", 12864, 75, -1, new String[] { "While wearing these gloves, you find 6 more experience for crafting water runes.", "The gloves last for 1,000 essence. Requires level 20 Runecrafting." }),
	EARTH_RUNECRAFTING_GLOVES("Earth Runecrafting Gloves", 12865, 75, -1, new String[] { "While wearing these gloves, you find 6.5 more experience for crafting earth runes.", "The gloves last for 1,000 essence. Requires level 30 Runecrafting." });

	/**
	 * The reward name.
	 */
	@Getter
	private final String name;

	/**
	 * The reward item id.
	 */
	@Getter
	private final int itemId;

	/***
	 * /** The reward purchase cost in tokens.
	 */
	@Getter
	private final int buyCost;

	/**
	 * The reward re-charge cost in tokens.
	 */
	@Getter
	private final int rechargeCost;

	/**
	 * The reward information details.
	 */
	@Getter
	private final String[] information;
}
