/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.plugin.fistofguthix;

import net.zaros.server.core.system.SystemManager;
import net.zaros.server.core.task.ScheduledTask;
import net.zaros.server.game.content.ProjectileManager;
import net.zaros.server.game.content.minigames.types.multi.MinigameArena;
import net.zaros.server.game.content.minigames.util.JoinLeaveOptions;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.entity.player.data.PlayerEquipment;
import net.zaros.server.game.node.entity.render.flag.impl.ForceTextUpdate;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.game.world.region.Region;
import net.zaros.server.game.world.region.RegionManager;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.HintIcon.HintIconArrow;
import net.zaros.server.utility.rs.Hit;
import net.zaros.server.utility.rs.Projectile;
import net.zaros.server.utility.tool.Misc;

import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Represents the Fist of Guthix minigame playing arena implementation.
 * 
 * @author Walied K. Yassen
 */
public final class FistOfGuthixGame extends MinigameArena<FistOfGuthix> {

	/**
	 * The currently active pairs within this game.
	 */
	private final List<FistOfGuthixPair> pairs = new CopyOnWriteArrayList<FistOfGuthixPair>();

	/**
	 * The spawn point locations.
	 */
	private static final Location[] SPAWN_POINTS = new Location[] { new Location(1627, 5697), new Location(1638, 5721), new Location(1661, 5731), new Location(1687, 5715), new Location(1700, 5695), new Location(1694, 5676), new Location(1661, 5660), new Location(1634, 5673), new Location(1626, 5697) };

	/**
	 * The house portal success teleport location.
	 */
	private static final Location[] PORTAL_SUCCESS = new Location[] { new Location(1656, 5683), new Location(1676, 5688), new Location(1651, 5703), new Location(1667, 5704) };

	/**
	 * The house portal failure teleport locations.
	 */
	private static final Location[] PORTAL_FAILURE = new Location[] { new Location(1663, 5668), new Location(1689, 5695), new Location(1653, 5720), new Location(1633, 5699) };

	/**
	 * The Stone of Power item id(s).
	 */
	private static final int[] STONES_OF_POWER = { 12845, 12846, 12847, 12848, 12849 };

	/**
	 * The Fist of Guthix arena center location.
	 */
	private static final Location CENTER_LOCATION = new Location(1664, 5695);

	/**
	 * The player teleport location after winning.
	 */
	private static final Location WINNER_LOCATION = new Location(1701, 5602);

	/**
	 * The player teleport location after losing.
	 */
	private static final Location LOSER_LOCATION = new Location(1701, 5602);

	/**
	 * Construct a new {@link FistOfGuthixGame} type object instance.
	 * 
	 * @param minigame
	 *                 the owner minigame.
	 */
	public FistOfGuthixGame(FistOfGuthix minigame) {
		super(minigame);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.content.minigames.types.multi.MinigameArea#tick()
	 */
	@Override
	public void tick() {
		for (FistOfGuthixPair pair : pairs) {
			process_pair(pair);
		}
	}

	/**
	 * Processes the specified {@link FistOfGuthixPair}.
	 * 
	 * @param pair
	 *             the pair to process.
	 */
	private void process_pair(FistOfGuthixPair pair) {
		if (pair.getStartingTicks() > 0) {
			pair.setStartingTicks(pair.getStartingTicks() - 1);
			if (pair.getStartingTicks() > 6) {
				return;
			} else if (pair.getStartingTicks() > 0) {
				if (pair.getStartingTicks() % 2 == 0) {
					pair.execute((player) -> player.getUpdateMasks().register(new ForceTextUpdate(String.valueOf(pair.getStartingTicks() / 2), false)));
				}
			} else {
				pair.execute((player) -> {
					player.getUpdateMasks().register(new ForceTextUpdate("Go!", false));
					player.getManager().getLocks().unlockAll();
				});
				open_overlay(pair);
			}
		} else {
			pair.setTicks(pair.getTicks() + 1);
			boolean roundOver = pair.isEndRound() || get_charges(pair.getTarget()) >= 5000 || pair.getTicks() >= 1000;
			if (roundOver) {
				if (pair.isFinalRound()) {
					process_ending(pair);
				} else {
					switch_pair(pair);
				}
			} else {
				process_hunter(pair);
				process_target(pair);
			}
		}
	}

	/**
	 * Process the winning of the specified {@link FistOfGuthixPair} pair.
	 * 
	 * @param pair
	 *             the pair which to decide who of them has won.
	 */
	private void process_ending(FistOfGuthixPair pair) {
		// remove the pairs from the list to prevent further processing.
		pairs.remove(pair);
		// grab the players.
		Player hunter = pair.getHunter();
		Player target = pair.getTarget();
		// decide the winner and the loser.
		int hunterCharges = get_charges(hunter);
		int targetCharges = get_charges(target);
		if (target.getTemporaryAttribute(AttributeKey.FOG_FORFEIT, false)) {
			win(pair, true);
		} else if (hunter.getTemporaryAttribute(AttributeKey.FOG_FORFEIT, false)) {
			win(pair, false);
		} else if (hunterCharges > targetCharges) {
			win(pair, true);
		} else if (hunterCharges < targetCharges) {
			win(pair, false);
		} else {
			lose(hunter, target);
			lose(target, hunter);
		}
		// execute tryLeave() on both of pair.
		pair.execute(this::tryLeave);
		// teleport outside the arena.
		pair.execute((player) -> minigame.getLobbyArea().tryJoin(player, JoinLeaveOptions.DONT_MOVE));
		// in-case the player logout.
		pair.execute((player) -> {
			if (!player.getSession().isActive()) {
				player.save();
			}
		});
	}

	private void win(FistOfGuthixPair pair, boolean hunterWon) {
		Player hunter = pair.getHunter();
		Player target = pair.getTarget();
		if (hunterWon) {
			win(hunter, target);
			lose(target, hunter);
		} else {
			win(target, hunter);
			lose(hunter, target);
		}
	}

	/**
	 * Starts the next round for the specified {@link FistOfGuthixPair}.
	 * 
	 * @param pair
	 *             the pair to start the round for.
	 */
	private void start_round(FistOfGuthixPair pair) {
		// execute the round start message to the players.
		pair.execute((player) -> player.getTransmitter().sendMessage("<col=004a00>The " + (pair.isFinalRound() ? "second" : "first") + " round of the game is about to begin.</col>"));
		// add the hint icons to players.
		pair.getHunter().getManager().getHintIcons().sendEntityIcon(pair.getTarget(), HintIconArrow.DEFAULT_YELLOW, 0);
		pair.getTarget().getManager().getHintIcons().sendEntityIcon(pair.getHunter(), HintIconArrow.DEFAULT_YELLOW, 0);
		// add the start timer.
		pair.setStartingTicks(8);
		// lock both of the players.
		pair.execute((player) -> player.getManager().getLocks().lockAll());
	}

	/**
	 * Processes the winning procedure for the specified {@link Player}.
	 * 
	 * @param player
	 *                 the player who won.
	 * @param opponent
	 *                 the player who lost.
	 */
	private void win(Player player, Player opponent) {
		// grab the player charges.
		int playerCharges = get_charges(player);
		int opponentCharges = get_charges(opponent);
		// calculate the charges difference.
		int difference = playerCharges - opponentCharges;
		// calculate the tokens.
		int rating = Math.max(Misc.randomise(Misc.remap(difference, 0, 5000, 0, 30), 2), 10);
		int tokens = Math.max(Misc.randomise(Misc.remap(difference, 0, 5000, 10, 25), 5), 10);
		// add the tokens to the player inventory.
		player.getInventory().addItem(FistOfGuthix.TOKEN, tokens);
		// add the rating to the player.
		player.setFistOfGuthixRating(player.getFistOfGuthixRating() + rating);
		// send the messages.
		player.getTransmitter().sendMessage("<col=004a00>Congratulations, you won!</col> You had " + playerCharges + " charges and your opponent had " + opponentCharges + ".");
		player.getTransmitter().sendMessage("You have gained " + rating + " rating and " + tokens + " tokens.");
		// teleport to the winners location.
		player.teleport(WINNER_LOCATION);
		SystemManager.getScheduler().schedule(new ScheduledTask(1, 1) {

			@Override
			public void run() {
				player.sendAnimation(8996);
				player.sendGraphics(1586);
				stop();
			}
		});
	}

	/**
	 * Process the losing procedure for the specified {@link Player}.
	 * 
	 * @param player
	 *                 the player who lost.
	 * @param opponent
	 *                 the player who won.
	 */
	private void lose(Player player, Player opponent) {
		// grab the player charges.
		int playerCharges = get_charges(player);
		int opponentCharges = get_charges(opponent);
		// calculate the tokens.
		int tokens = Misc.random(1, 3);
		player.getInventory().addItem(FistOfGuthix.TOKEN, tokens);
		// calculate the charges difference.
		int difference = opponentCharges - playerCharges;
		// calculate the rating.
		int rating = Misc.remap(difference, 0, 5000, 0, 22);
		rating += Misc.random(1, 3);
		// add the tokens to the player inventory.
		player.getInventory().addItem(FistOfGuthix.TOKEN, tokens);
		// remove the rating from the player.
		player.setFistOfGuthixRating(player.getFistOfGuthixRating() - rating);
		// send the messages.
		player.getTransmitter().sendMessage("<col=dd0000>You lost.</col> You had " + playerCharges + " charges and your opponent had " + opponentCharges + ".");
		player.getTransmitter().sendMessage("You have lost " + rating + " rating and been awarded " + tokens + " Fist of Guthix tokens for good effort.");
		// teleport to the losers location.
		player.teleport(LOSER_LOCATION);
	}

	/**
	 * Processes the hunter player from the {@link FistOfGuthixPair}.
	 * 
	 * @param pair
	 *             the pair which we will grab the target player from.
	 */
	private void process_hunter(FistOfGuthixPair pair) {
		Player player = pair.getHunter();
		player.getTransmitter().sendVarp(1215, pair.getTicks());
	}

	public void house_teleport(Player player, Location current_portal) {
		int charges = get_charges(player);
		if (charges < 20) {
			player.getTransmitter().sendMessage("You need at least 20 charges to use this portal.");
			return;
		}
		add_charges(player, -20);
		player.getManager().getLocks().lockAll();
		SystemManager.getScheduler().schedule(new ScheduledTask(1, 8) {

			boolean failed = false;

			@Override
			public void run() {
				int ticks = getTicksPassed();
				if (ticks == 1) {
					player.getMovement().addWalkSteps(current_portal.getX(), current_portal.getY(), 1, false);
				} else if (ticks == 3) {
					player.sendGraphics(1591);
					player.sendAnimation(9016);
				} else if (ticks == 6) {
					failed = Misc.random(8) == 0;
					if (!failed) {
						player.sendGraphics(1592);
					}
					player.sendAnimation(9018);
				} else if (ticks == 7) {
					Location location;
					if (failed) {
						location = PORTAL_FAILURE[Misc.random(PORTAL_FAILURE.length)].transform(Misc.random(-2, 3), Misc.random(-2, 3), 0);
						player.getTransmitter().sendMessage("The ancient teleport fails and you find yourself back out in the arena.");
					} else {
						do {
							location = PORTAL_SUCCESS[Misc.random(PORTAL_SUCCESS.length)];
						} while (location.equals(current_portal));
					}
					player.teleport(location);
				} else if (ticks == 8) {
					player.getManager().getLocks().unlockAll();
					stop();
				}
			}
		});
	}

	/**
	 * Processes the target player from the {@link FistOfGuthixPair}.
	 * 
	 * @param pair
	 *             the pair which we will grab the target player from.
	 */
	private void process_target(FistOfGuthixPair pair) {
		Player player = pair.getTarget();
		player.getTransmitter().sendVarp(1215, pair.getTicks());
		if (is_holding_stone(player)) {
			pair.setHoldingTicks(pair.getHoldingTicks() + 1);
			if (pair.getHoldingTicks() % 5 == 0) {
				int distance_grade = player.getLocation().getDistance(CENTER_LOCATION) / 10;
				if (distance_grade == 0) {
					add_charges(player, 28);
				} else if (distance_grade == 1) {
					add_charges(player, 12);
				} else if (distance_grade == 2) {
					add_charges(player, 6);
				} else {
					add_charges(player, 3);
				}
			}
		} else {
			pair.setHoldingTicks(0);
		}
		process_house(player);
		int chance = Misc.random(50);
		if (chance < 3) {
			player.getHitMap().applyHit(new Hit(null, 10));
			player.sendGraphics(1585);
		}
		chance = Misc.random(10);
		if (chance == 0) {
			ProjectileManager.sendProjectile(create_projectile(player));
		}
	}

	/**
	 * Processes the teleport houses.
	 * 
	 * @param player
	 *               the player to process for.
	 */
	private void process_house(Player player) {
		Integer ticks = player.getTemporaryAttribute(AttributeKey.FOG_HOUSE);
		if (ticks == null) {
			return;
		}
		ticks = minigame.getTick() - ticks;
		if (ticks > 0 && ticks % 10 == 0) {
			player.getHitMap().applyHit(new Hit(null, 63));
		}
	}

	/**
	 * Creates a random smoke projectile for the specified {@link Player}.
	 * 
	 * @param player
	 *               the player to create for.
	 * @return the created {@link Projectile} object.
	 */
	private Projectile create_projectile(Player player) {
		int x = Misc.randomise(player.getLocation().getX(), 3);
		int y = Misc.randomise(player.getLocation().getY(), 3);
		return new Projectile(player, new Location(x, y, 0), 1583, 0, 124 >> 2, 0, 71, 16, 0);
	}

	/**
	 * Handles the death of the specified {@link Player}.
	 * 
	 * @param player
	 *               the player to handle the death for.
	 */
	public boolean handle_post_death(Player player) {
		FistOfGuthixPair pair = player.getTemporaryAttribute(AttributeKey.FOG_PAIR);
		if (pair == null) {
			return false;
		}
		player.removeTemporaryAttribute(AttributeKey.FOG_HOUSE);
		player.restoreAll();
		player.sendAnimation(-1);
		if (pair.getTarget() == player) {
			pair.setEndRound(true);
		} else {
			player.teleport(get_spawn_location());
		}
		return true;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.content.minigames.types.multi.MinigameArea#join(net.
	 * zaros.server.game.node.entity.player.Player, java.util.EnumSet)
	 */
	@Override
	public boolean join(Player player, EnumSet<JoinLeaveOptions> options) {
		player.setInFightArea(true);
		return true;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.content.minigames.types.multi.MinigameArea#leave(net.
	 * zaros.server.game.node.entity.player.Player, java.util.EnumSet)
	 */
	@Override
	public boolean leave(Player player, EnumSet<JoinLeaveOptions> options) {
		close_overlay(player);
		for (int itemId : STONES_OF_POWER) {
			player.getInventory().deleteItemAll(itemId);
		}
		if (is_holding_stone(player)) {
			player.getEquipment().deleteSlot(PlayerEquipment.SLOT_WEAPON);
		}
		player.getInventory().deleteItemAll(FistOfGuthixWaiting.ELEMENTAL_RUNE);
		player.getInventory().deleteItemAll(FistOfGuthixWaiting.CATALYTIC_RUNE);
		player.getInventory().deleteItemAll(FistOfGuthixWaiting.BANDAGE);
		player.getInventory().deleteItemAll(FistOfGuthixWaiting.TELE_ORB);
		player.getManager().getHintIcons().removeIconAtSlot(0);
		player.removeTemporaryAttribute(AttributeKey.FOG_CHARGES);
		player.removeTemporaryAttribute(AttributeKey.FOG_PAIR);
		player.removeTemporaryAttribute(AttributeKey.FOG_HOUSE);
		player.setInFightArea(false);
		player.restoreAll();
		return true;
	}

	/**
	 * Adds the specified {@link FistOfGuthixPair} to this minigame.
	 * 
	 * @param pair
	 *             the pair to add.
	 */
	public void add_pair(FistOfGuthixPair pair) {
		// grab the hunter and the target player.
		Player hunter = pair.getHunter();
		Player target = pair.getTarget();
		// unregister the players from the waiting area.
		minigame.getWaitingArea().tryLeave(hunter, JoinLeaveOptions.EFFECTLESS);
		minigame.getWaitingArea().tryLeave(target, JoinLeaveOptions.EFFECTLESS);
		// teleport the pair.
		teleport_pair(pair);
		// make the players join this area.
		pair.execute(this::tryJoin);
		// add the pair for processing.
		pairs.add(pair);
		// store the pair as a player attribute.
		pair.execute((player) -> player.putTemporaryAttribute(AttributeKey.FOG_PAIR, pair));
		// start the round.
		start_round(pair);
	}

	/**
	 * Teleports the pair to a random spawn points.
	 * 
	 * @param pair
	 *             the pair to teleport.
	 */
	private void teleport_pair(FistOfGuthixPair pair) {
		// find the spawn locations.
		Location hunter_location = get_spawn_location();
		Location target_location = get_spawn_location();
		while (hunter_location.withinDistance(target_location, 50)) {
			target_location = get_spawn_location();
		}
		// teleport the players.
		pair.getHunter().teleport(hunter_location);
		pair.getTarget().teleport(target_location);
	}

	/**
	 * Switches the pair roles between hunted and target.
	 * 
	 * @param pair
	 *             the pair to switch their roles.
	 */
	private void switch_pair(FistOfGuthixPair pair) {
		for (int itemId : STONES_OF_POWER) {
			pair.getTarget().getInventory().deleteItemAll(itemId);
		}
		if (is_holding_stone(pair.getTarget())) {
			pair.getTarget().getEquipment().deleteSlot(PlayerEquipment.SLOT_WEAPON);
		}
		pair.setTicks(0);
		pair.setHoldingTicks(0);
		pair.setEndRound(false);
		pair.setFinalRound(true);
		teleport_pair(pair);
		pair.execute(this::close_overlay);
		start_round(pair);
	}

	/**
	 * Opens the overlay for the specified {@link FistOfGuthixPair}.
	 * 
	 * @param pair
	 *             the pair to open the overlay for.
	 */
	private void open_overlay(FistOfGuthixPair pair) {
		open_overlay(pair, true);
		open_overlay(pair, false);
	}

	/**
	 * Opens the overlay for the specified player in the given
	 * {@link FistOfGuthixPair}.
	 * 
	 * @param pair
	 *               the Fist of Guthix pair.
	 * @param hunter
	 *               which player to open the overlay for, hunter or target.
	 */
	private void open_overlay(FistOfGuthixPair pair, boolean hunter) {
		Player player = hunter ? pair.getHunter() : pair.getTarget();
		player.getTransmitter().sendVarcString(217, hunter ? pair.getTarget().getDetails().getDisplayName() : pair.getHunter().getDetails().getDisplayName());
		// pair.getHunter().getTransmitter().sendVarpbit(4540, hunter ? 3 : 2 );
		player.getManager().getInterfaces().sendInterfaceComponentHidden(730, 23, false);
		player.getManager().getInterfaces().sendInterfaceComponentHidden(730, 24, hunter);
		player.getManager().getInterfaces().sendInterfaceText(730, 26, hunter ? "Hunting:" : "Hunted by:");
		player.getTransmitter().sendVarp(1215, 0);
		player.getManager().getInterfaces().sendPrimaryOverlay(730, false);
		update_charges(player);
	}

	/**
	 * Closes the overlay for the specified {@link Player}.
	 * 
	 * @param player
	 *               the player to close the overlay for.
	 */
	private void close_overlay(Player player) {
		player.getManager().getInterfaces().closePrimaryOverlay();
	}

	/**
	 * Adds the specified {@code amount} of charges to the given {@link Player}.
	 * 
	 * @param player
	 *               the player to add the charges for.
	 * @param amount
	 *               the amount of charges to add.
	 */
	private void add_charges(Player player, int amount) {
		int charges = player.getTemporaryAttribute(AttributeKey.FOG_CHARGES, 0);
		if (amount < 0 || charges < 5000) {
			charges += amount;
			player.putTemporaryAttribute(AttributeKey.FOG_CHARGES, charges);
			player.sendGraphics(1584);
			update_charges(player);
		}
	}

	/**
	 * Updates the current charges for the specified {@link Player}.
	 * 
	 * @param player
	 *               the player to update for.
	 */
	private void update_charges(Player player) {
		player.getManager().getInterfaces().sendInterfaceText(730, 17, "Charges: " + player.getTemporaryAttribute(AttributeKey.FOG_CHARGES, 0));
	}

	/**
	 * Gets amount charges the specified {@link Player} has gained so far.
	 * 
	 * @param player
	 *               the player to find the charges for.
	 * @return the amount of charges.
	 */
	private int get_charges(Player player) {
		return player.getTemporaryAttribute(AttributeKey.FOG_CHARGES, 0);
	}

	/**
	 * Makes the specified {@link Player} attempt to take a Stone of Power.
	 * 
	 * @param player
	 *               the player which we will attempt to take the stone.
	 */
	public void take_stone(Player player) {
		if (has_stone(player)) {
			player.getTransmitter().sendMessage("You already have a stone.");
			return;
		}
		player.getManager().getInterfaces().sendInterfaceComponentHidden(730, 24, true);
		player.getInventory().addItem(STONES_OF_POWER[Misc.random(STONES_OF_POWER.length)], 1);
		// player.getTransmitter().sendSynthSound(9704, 1, 0, 100, 256);
		player.getTransmitter().sendMessage("You take a magical stone from the dispenser.");
	}

	/**
	 * Performs a teleportation to the centre location using the tele-orb animation
	 * and graphics.
	 * 
	 * @param player
	 *               the player which is supposed to teleport.
	 */
	public void centre_teleport(Player player) {
		FistOfGuthixPair pair = player.getTemporaryAttribute(AttributeKey.FOG_PAIR);
		if (pair.isEndRound() && pair.isEndRound()) {
			player.getTransmitter().sendMessage("The round is ending, so there is no point in using that now.");
			return;
		}
		if (player.getTemporaryAttribute(AttributeKey.FOG_HOUSE, false)) {
			player.removeTemporaryAttribute(AttributeKey.FOG_HOUSE);
		}
		player.getInventory().deleteItem(FistOfGuthix.TELE_ORB, 1);
		player.getManager().getLocks().lockAll();
		SystemManager.getScheduler().schedule(new ScheduledTask(0, 5) {

			@Override
			public void run() {
				int ticks = getTicksPassed();
				if (ticks == 1) {
					player.sendAnimation(9012, 19);
					player.sendGraphics(1589);
				} else if (ticks == 2) {
					player.getTransmitter().sendMessage("You teleport to the centre of the arena.");
				} else if (ticks == 4) {
					player.sendGraphics(1590);
					player.sendAnimation(9013);
					player.teleport(CENTER_LOCATION.transform(Misc.random(-2, 2), Misc.random(-2, 2), 0));
					player.getManager().getLocks().unlockAll();
					stop();
				}
			}
		});
	}

	/**
	 * Forfeits the specified {@link Player} from the game.
	 * 
	 * @param player
	 *               the player who is supposed to forfeit.
	 */
	public void forfeit(Player player, boolean fromLogout) {
		// grab the fist of guthix pair.
		FistOfGuthixPair pair = player.getTemporaryAttribute(AttributeKey.FOG_PAIR);
		if (pair == null) {
			return;
		}
		// force the pair to leave the match.
		pair.setFinalRound(true);
		pair.setEndRound(true);
		// grab the opponent player.
		Player opponent = pair.getHunter() == player ? pair.getTarget() : pair.getHunter();
		opponent.getTransmitter().sendMessage("Your opponent has left, so the game is ending.");
		player.getVariables().putAttribute(AttributeKey.FOG_BAN, System.currentTimeMillis() + 120000);
		player.putTemporaryAttribute(AttributeKey.FOG_FORFEIT, true);
		if (!fromLogout) {
			player.getTransmitter().sendMessage("You forfeit and lose the game. You have been marked as a forfieter and will receieve");
			player.getTransmitter().sendMessage("a fewer tokens from your next game.");
		}
		player.restoreAll();
	}

	/**
	 * Checks whether or not the specified {@link Player} has a Stone of Power in
	 * their inventory or in their equipment or not.
	 * 
	 * @param player
	 *               the player to check.
	 * @return <code>true</code> if they do otherwise <code>false</code>.
	 */
	private boolean has_stone(Player player) {
		for (Item item : player.getInventory().getItems().getItems()) {
			if (item != null && is_stone_of_power(item.getId())) {
				return true;
			}
		}
		return is_holding_stone(player);
	}

	/**
	 * Checks whether or not the specified player is currently holding the Stone of
	 * Power.
	 * 
	 * @param player
	 *               the player to check.
	 * @return <code>true</code> if they do otherwise <code>false</code>.
	 */
	public static boolean is_holding_stone(Player player) {
		return is_stone_of_power(player.getEquipment().getIdInSlot(PlayerEquipment.SLOT_WEAPON));
	}

	/**
	 * Checks whether or not the given item {@code id} is a a valid Stone of Power
	 * item id or not..
	 * 
	 * @param id
	 *           the item id to check.
	 * @return <code>true</code> if it is otherwise <code>false</code>.
	 */
	public static boolean is_stone_of_power(int id) {
		return id >= 12845 && id <= 12849;
	}

	/**
	 * Gets a random spawn location within the game arena.
	 * 
	 * @return a random spawn {@link Location} object.
	 */
	public static Location get_spawn_location() {
		Location base_location = new Location(SPAWN_POINTS[Misc.getRandom(SPAWN_POINTS.length - 1)]);
		Region region = RegionManager.getRegionAndLoad(base_location.getRegionId());
		do {
			Location location = base_location.transform(Misc.random(-3, 3), Misc.random(-3, 3), 0);
			if (region.isFloorFree(location)) {
				return location;
			}
		} while (true);
	}
}
