/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.plugin.fistofguthix;

import net.zaros.server.game.content.minigames.types.multi.MinigameWaiting;
import net.zaros.server.game.content.minigames.util.JoinLeaveOptions;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

/**
 * Represents the Fist of Guthix waiting arena.
 * 
 * @author Walied K. Yassen
 */
public final class FistOfGuthixWaiting extends MinigameWaiting<FistOfGuthix> {

	/**
	 * The players update interval in ticks.
	 */
	private static final int PAIRING_INTERVAL = 12;

	/**
	 * The minimum players count that is required for the game to start.
	 */
	private static final int MINIMUM_SIZE = 2;

	/**
	 * The Elemental Rune item id.
	 */
	public static final int ELEMENTAL_RUNE = 12850;

	/**
	 * The Catalytic Rune item id.
	 */
	public static final int CATALYTIC_RUNE = 12851;

	/**
	 * The heal Bandage item id.
	 */
	public static final int BANDAGE = 12853;

	/**
	 * The Teleport Orb item id.
	 */
	public static final int TELE_ORB = 12855;

	/**
	 * Construct a new {@link FistOfGuthixWaiting} type object instance.
	 * 
	 * @param minigame
	 */
	public FistOfGuthixWaiting(FistOfGuthix minigame) {
		super(minigame);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.content.minigames.types.multi.MinigameArea#tick()
	 */
	@Override
	public void tick() {
		if (minigame.getTick() % PAIRING_INTERVAL == 0) {
			List<FistOfGuthixPair> pairs = makePairs();
			if (pairs.size() > 0) {
				pairs.forEach(minigame.getGameArea()::add_pair);
			}
		}
	}

	/**
	 * Creates a list of paired players to play against each other.
	 * 
	 * @return the paired players list.
	 */
	private List<FistOfGuthixPair> makePairs() {
		if (players.size() < MINIMUM_SIZE) {
			return Collections.emptyList();
		}
		// store the pairs.
		List<FistOfGuthixPair> pairs = new ArrayList<FistOfGuthixPair>();
		// create the queue and randomise the order.
		List<Player> queue = new ArrayList<Player>();
		queue.addAll(players);
		Collections.shuffle(queue);
		// start pairing players.
		while (queue.size() >= MINIMUM_SIZE) {
			Player first = queue.remove(queue.size() - 1);
			Player second = queue.remove(queue.size() - 1);
			pairs.add(new FistOfGuthixPair(first, second));
			updateOverlay(first, true);
			updateOverlay(second, true);
		}
		return pairs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.content.minigames.types.multi.MinigameArea#join(net.
	 * zaros.server.game.node.entity.player.Player, java.util.EnumSet)
	 */
	@Override
	public boolean join(Player player, EnumSet<JoinLeaveOptions> options) {
		if (player.getInventory().getFreeSlots() < 10) {
			player.getTransmitter().sendMessage("You need at least ten free inventory spaces to enter.");
			return false;
		}
		if (!options.contains(JoinLeaveOptions.DONT_MOVE)) {
			player.teleport(new Location(1653, 5600, 0));
		}
		player.getInventory().addItem(ELEMENTAL_RUNE, 1000);
		player.getInventory().addItem(CATALYTIC_RUNE, 300);
		player.getInventory().addItem(BANDAGE, 5);
		player.getInventory().addItem(TELE_ORB, 1);
		player.getTransmitter().sendVarpbit(4540, 1);
		player.restoreAll();
		openOverlay(player);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.content.minigames.types.multi.MinigameArea#leave(net.
	 * zaros.server.game.node.entity.player.Player, java.util.EnumSet)
	 */
	@Override
	public boolean leave(Player player, EnumSet<JoinLeaveOptions> options) {
		player.getInventory().deleteItemAll(ELEMENTAL_RUNE);
		player.getInventory().deleteItemAll(CATALYTIC_RUNE);
		player.getInventory().deleteItemAll(BANDAGE);
		player.getInventory().deleteItemAll(TELE_ORB);
		if (!options.contains(JoinLeaveOptions.DONT_MOVE)) {
			player.teleport(new Location(1718, 5598, 0));
		}
		closeOverlay(player);
		return true;
	}

	/**
	 * Opens the lobby for the specified {@code player}.
	 * 
	 * @param player
	 *               the player to open the overlay to.
	 */
	public void openOverlay(Player player) {
		player.getManager().getInterfaces().sendPrimaryOverlay(731);
		player.getManager().getInterfaces().sendInterfaceComponentHidden(731, 26, true);
		player.getManager().getInterfaces().sendInterfaceComponentHidden(731, 17, false);
		updateOverlay(player, false);
	}

	/**
	 * Updates the minigame overlay for the specified {@code player}.
	 * 
	 * @param player
	 *               the player to update the overlay for.
	 */
	public void updateOverlay(Player player, boolean paired) {
		player.getManager().getInterfaces().sendInterfaceText(731, 7, "Rating: " + player.getFistOfGuthixRating());
		player.getTransmitter().sendVarcInt(1416, player.getFistOfGuthixRating());
		player.getManager().getInterfaces().sendInterfaceText(731, 16, paired ? "Please wait..." : "Not enough<br>players");
	}

	/**
	 * Closes the minigame overlay for the specified {@code player}.
	 * 
	 * @param player
	 *               the player to close the overlay for.
	 */
	public void closeOverlay(Player player) {
		player.getManager().getInterfaces().closePrimaryOverlay();
	}

	/**
	 * Checks whether the game is ready to start yet or not.
	 * 
	 * @return <code>true</code> if it is otherwise <code>false</code>.
	 */
	public boolean isReady() {
		return size() >= MINIMUM_SIZE;
	}
}
