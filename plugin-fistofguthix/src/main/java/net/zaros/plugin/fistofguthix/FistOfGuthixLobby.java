/*
 * Copyright (c) 2018 Walied K. Yassen, All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.zaros.plugin.fistofguthix;

import net.zaros.server.game.content.minigames.types.multi.MinigameLobby;
import net.zaros.server.game.content.minigames.util.JoinLeaveOptions;
import net.zaros.server.game.node.Location;
import net.zaros.server.game.node.entity.player.Player;
import net.zaros.server.game.node.item.Item;
import net.zaros.server.utility.AttributeKey;
import net.zaros.server.utility.rs.constant.FoodConstants.Food;
import net.zaros.server.utility.rs.constant.PotionConstants.Potion;

import java.util.EnumSet;

import static net.zaros.server.utility.rs.constant.MagicConstants.*;

/**
 * Represents the Fist of Guthix minigame lobby.
 * 
 * @author Walied K. Yassen
 */
public final class FistOfGuthixLobby extends MinigameLobby<FistOfGuthix> {

	/**
	 * The players update interval in ticks.
	 */
	private static final int UPDATE_INTERVAL = 35;

	/**
	 * Construct a new {@link FistOfGuthixLobby} type object instance.
	 * 
	 * @param minigame
	 *                 the owner minigame.
	 */
	public FistOfGuthixLobby(FistOfGuthix minigame) {
		super(minigame);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.content.minigames.types.multi.MinigameArea#tick()
	 */
	@Override
	public void tick() {
		if (minigame.getTick() % UPDATE_INTERVAL == 0) {
			players.forEach(FistOfGuthixLobby::checkInventory);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.content.minigames.types.multi.MinigameArea#join(net.
	 * zaros.server.game.node.entity.player.Player, java.util.EnumSet)
	 */
	@Override
	public boolean join(Player player, EnumSet<JoinLeaveOptions> options) {
		if (!options.contains(JoinLeaveOptions.DONT_MOVE)) {
			player.teleport(new Location(1677, 5599, 0));
		}
		openOverlay(player);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * net.zaros.server.game.content.minigames.types.multi.MinigameArea#leave(net.
	 * zaros.server.game.node.entity.player.Player, java.util.EnumSet)
	 */
	@Override
	public boolean leave(Player player, EnumSet<JoinLeaveOptions> options) {
		if (!options.contains(JoinLeaveOptions.DONT_MOVE)) {
			player.teleport(new Location(2969, 9672, 0));
		}
		closeOverlay(player);
		return true;
	}

	/**
	 * Opens the minigame overlay for the specified player.
	 * 
	 * @param player
	 *               the player to send for.
	 */
	public void openOverlay(Player player) {
		if (player.getVariables().getAttribute(AttributeKey.FOG_FIARA, false)) {
			player.getManager().getInterfaces().sendPrimaryOverlay(731);
			player.getManager().getInterfaces().sendInterfaceComponentHidden(731, 26, true);
			player.getManager().getInterfaces().sendInterfaceComponentHidden(731, 17, true);
			updateOverlay(player);
		}
	}

	/**
	 * Updates the minigame overlay for the specified player.
	 * 
	 * @param player
	 *               the player to update for.
	 */
	public void updateOverlay(Player player) {
		if (player.getVariables().getAttribute(AttributeKey.FOG_FIARA, false)) {
			player.getManager().getInterfaces().sendInterfaceText(731, 7, "Rating: " + player.getFistOfGuthixRating());
			player.getTransmitter().sendVarcInt(1416, player.getFistOfGuthixRating());
		}
	}

	/**
	 * Closes the minigame overlay for the specified player.
	 * 
	 * @param player
	 *               the player to close for.
	 */
	public void closeOverlay(Player player) {
		player.getManager().getInterfaces().closePrimaryOverlay();
	}

	/**
	 * Checks the specified {@code player} inventory for any banned items.
	 * 
	 * @param player
	 *               the player who are we going to check their inventory.
	 * @return <code>true</code> if the inventory is okay otherwise
	 *         <code>false</code>.
	 */
	static boolean checkInventory(Player player) {
		String message = null;
		for (Item item : player.getInventory().getItems().getItems()) {
			if (item != null && (message = getItemMessage(item)) != null) {
				break;
			}
		}
		if (message == null) {
			player.getManager().getInterfaces().sendInterfaceComponentHidden(731, 26, true);
			return true;
		} else {
			player.getManager().getInterfaces().sendInterfaceComponentHidden(731, 26, false);
			player.getManager().getInterfaces().sendInterfaceText(731, 25, "The following item is not allowed in the arena: <br>" + message);
			return false;
		}
	}

	/**
	 * Gets the specified {@code item} category or name. If the item is not banned,
	 * the return result is a {@code null} reference.
	 * 
	 * @param item
	 *             the item to check.
	 * @return the item message.
	 */
	static String getItemMessage(Item item) {
		final int id = item.getId();
		if (Food.getFoodMap().containsKey(id)) {
			return "Food of any kind.";
		}
		if (Potion.getPotion(item.getId()).isPresent()) {
			return "Potions of any kind.";
		}
		switch (id) {
		case AIR_RUNE:
		case WATER_RUNE:
		case EARTH_RUNE:
		case FIRE_RUNE:
		case MIND_RUNE:
		case BODY_RUNE:
		case NATURE_RUNE:
		case CHAOS_RUNE:
		case DEATH_RUNE:
		case LAW_RUNE:
		case FistOfGuthix.TOKEN:
			return null;
		default:
			String[] ops = item.getDefinitions().getInventoryOptions();
			if (ops == null || ops[1] == null || !ops[1].startsWith("W")) {
				return item.getName();
			}
			return null;
		}
	}
}
