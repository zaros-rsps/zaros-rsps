package net.zaros.plugin.fistofguthix;

import net.zaros.server.core.event.EventListener;
import net.zaros.server.core.event.loc.LocEvent;
import net.zaros.server.core.event.loc.LocListener;
import net.zaros.server.core.event.player.area.AreaActivityEvent;
import net.zaros.server.game.content.minigames.util.JoinLeaveOptions;
import net.zaros.server.game.plugin.java.Plugin;
import net.zaros.server.game.world.World;

/**
 * Represents the Fist of Guthix minigame plugin.
 * 
 * @author Walied K. Yassen
 */
public class FistOfGuthixPlugin extends Plugin {

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.plugin.java.Plugin#onEnable()
	 */
	@Override
	public void onEnable() {
		World.getMinigameManager().register(FistOfGuthix.getSingleton());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.zaros.server.game.plugin.java.Plugin#onDisable()
	 */
	@Override
	public void onDisable() {
		World.getMinigameManager().unregister(FistOfGuthix.getSingleton());
	}

	/**
	 * @param event
	 */
	@LocListener(ids = { 20608 })
	public void onPortalLocClick(LocEvent event) {
		FistOfGuthix.getSingleton().enter(event.getPlayer());
		event.cancel();
	}

	/**
	 * @param event
	 */
	@EventListener
	public void checkAreaActivity(AreaActivityEvent event) {
		if (event.getLocation().getRegionId() == 6743) {
			FistOfGuthix.getSingleton().enter(event.getPlayer(), JoinLeaveOptions.DONT_MOVE);
			event.cancel();
		}
	}

}
